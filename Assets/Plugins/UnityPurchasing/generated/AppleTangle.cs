#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("PmcmNDQyKiI0ZyYkJCI3MyYpJCKHJHQwsH1AaxGsnUhmSZ39NF4I8tLZPUvjAMwck1FwdIyDSAqJUy6WYXdjQUQSQ0xUWgY3NysiZwQiNTN3xUP8d8VE5OdERUZFRUZFd0pBTsxezpm+DCuyQOxld0WvX3m/F06UxUZHQU5twQ/BsCQjQkZ3xrV3bUFAqzp+xMwUZ5R/g/b43QhNLLhsu0F3SEFEElpURka4Q0J3REZGuHdaaHfGhEFPbEFGQkJARUV3xvFdxvRx3gtqP/Cqy9ybtDDctTGVMHcIhkjaerRsDm9dj7mJ8v5JnhlbkYx6WMLEwlzeegBwte7cB8lrk/bXVZ9CR0TFRkhHd8VGTUXFRkZHo9buTk9sQUZCQkBFRlFZLzMzNzR9aGgwNSYkMy4kImc0MyYzIioiKTM0aXcOnzHYdFMi5jDTjmpFREZHRuTFRm3BD8GwSkZGQkJHdyV2THdOQUQS8n3qs0hJR9VM9mZRaTOSe0qcJVFjpayW8DeYSAKmYI22Kj+qoPJQUDcrImcEIjUzLiEuJCYzLigpZwYyMy8oNS4zPnZRd1NBRBJDRFRKBjdpB+GwAAo4Txl3WEFEElpkQ193UXphIGfNdC2wSsWImazkaL4ULRwjPXfFRjF3SUFEElpIRka4Q0NERUZRd1NBRBJDRFRKBjc3KyJnFSgoMykjZyQoKSMuMy4oKTRnKCFnMjQinnE4hsASnuDe/nUFvJ+SNtk55hX5szTcqZUjSIw+CHOf5Xm+P7gsj0pBTm3BD8GwSkZGQkJHRMVGRkcbAjlYCywX0QbOgzMlTFfEBsB0zcbs5DbVABQShuhoBvS/vKQ3iqHkCyDIT/NnsIzra2coN/F4RnfL8ASI75s5ZXKNYpKeSJEsk+VjZFaw5utrZyQiNTMuIS4kJjMiZzcoKy4kPisiZw4pJGl2YXdjQUQSQ0xUWgY3cnV2c3d0cR1QSnRyd3V3fnV2c3c4Bu/fvpaNIdtjLFaX5PyjXG2EWENBVEUSFHZUd1ZBRBJDTVRNBjc38Fz61AVjVW2ASFrxCtsZJI8Mx1B0cR13JXZMd05BRBJDQVRFEhR2VDcrImcVKCgzZwQGd1lQSndxd3N1MDBpJjc3KyJpJCgqaCY3NysiJCZBRBJaSUNRQ1Nsly4A0zFOubMsyjMuIS4kJjMiZyU+ZyYpPmc3JjUzyDTGJ4FcHE5o1fW/Aw+3J3/ZUrJnBAZ3xUZld0pBTm3BD8GwSkZGRmcoIWczLyJnMy8iKWcmNzcrLiQmZyYpI2ckIjUzLiEuJCYzLigpZzce4EJOO1AHEVZZM5TwzGR8AOSSKHdWQUQSQ01UTQY3NysiZw4pJGl2jl41shpJkjgY3LViRP0SyAoaSrbHU2yXLgDTMU65syzKaQfhsAAKOBUiKy4mKSQiZygpZzMvLjRnJCI19ncfqx1Ddcsv9MhamSI0uCAZIvsuIS4kJjMuKClnBjIzLyg1LjM+dk8Zd8VGVkFEElpnQ8VGT3fFRkN3JSsiZzQzJikjJjUjZzMiNSo0ZyZY1pxZABesQqoZPsNqrHHlEAsSqyNyZFIMUh5a9NOwsdvZiBf9hh8XF+3Nkp2ju5dOQHD3MjJm");
        private static int[] order = new int[] { 40,57,18,10,40,16,34,51,29,37,48,22,49,14,35,46,27,57,23,53,40,27,31,56,28,49,43,58,50,47,49,50,58,54,44,48,51,57,56,59,47,44,51,49,59,55,48,59,55,52,57,53,58,56,58,57,58,58,58,59,60 };
        private static int key = 71;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
