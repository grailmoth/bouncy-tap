#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("NcU3fwpU9jWHQDxwEbABZ7xlNJ6vQf3WyP3OQHpiVShM1dZVLA/lhW/s4u3db+zn72/s7O1kwKhjIW9j8Ia3fTbuBdh0j/6+AK8AzlDCj+pcayad/0F1pjkc9VTzFjjtLu9/FPlSrBnoQbk+ZqC+DIJiNhopFjizOp3d5heuSUhHy0zM4bRbVwK9OMZzcinhrigS2MoySQiUAixI3vGh//icTsGgo8ZEoSfJQz2E9J7umfg5LReQ/X21bvn1JYeSsSSoohuRbrbdb+zP3eDr5MdrpWsa4Ozs7Ojt7jXNGP8wGkj/C298/tI309JvB9pxqrX0LSYAHXvUx4f7psBV++IipFT8dfzCJQyfDEpMOI+OPO2KKPSbdr9idHBUk/akcu/u7O3s");
        private static int[] order = new int[] { 10,11,11,6,11,8,6,11,13,12,10,11,12,13,14 };
        private static int key = 237;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
