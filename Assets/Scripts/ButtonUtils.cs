﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;

public class ButtonUtils : MonoBehaviour {
	public void ShowLeaderboardUI(){
		Leaderboard.leaderboard.ShowLeaderboardUI();
	}
	public void ShowAchievementUI(){
		Leaderboard.leaderboard.ShowAchievementUI();
	}
	public void LoadScene(string sceneName){
		ScreenManager.screenManager.LoadGameScene(sceneName);
	}
	public void ResetScene(){
		Player player = FindObjectOfType<Player>();
		if(player.deathsSinceAd >= GameManager.DEATHS_BETWEEN_ADS){
			if(Advertising.IsInterstitialAdReady() && !PurchaseManager.purchaseManager.noAds){
				return;
			}
		}
		GameManager.gameManager.ResetScene();
	}
	public void RemoveAds(){
		if(!PurchaseManager.purchaseManager.noAds){
			PurchaseManager.purchaseManager.RemoveAds();
		} else {
			NativeUI.Alert("Thank you!","You have already paid to remove ads. We appreciate your support!");
		}
	}
	public void RestorePurchases(){
		PurchaseManager.purchaseManager.RestorePurchases();
	}
	// public void LoadSceneAndDestroyPersistentCanvas(string sceneName){
	// 	DestroyPersistentCanvas();
	// 	ScreenManager.screenManager.LoadGameScene(sceneName);
	// }

	// private void DestroyPersistentCanvas(){
	// 	GameObject.Destroy(PersistentCanvas.persistentCanvas);
	// }
}
