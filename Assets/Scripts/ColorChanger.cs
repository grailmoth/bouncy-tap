﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour {
	public Enums.gameColor myColor;
	
	// Update is called once per frame
	void Update () {
		if(gameObject.transform.position.x < GameManager.gameManager.lowerLeftCornerWP.x - (GameManager.gameManager.TILE_WIDTH+1)){
			if(gameObject.transform.position.y == GameManager.gameManager.upperLefCornerWP.y){
				GameManager.gameManager.MoveCeilingTile(gameObject);
			} else {
				GameManager.gameManager.MoveFloorTile(gameObject);
			}
		}
	}
}
