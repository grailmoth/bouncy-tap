﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCanvas : MonoBehaviour {

	// public static PersistentCanvas persistentCanvas;

	// void Awake()
	// {
	// 	if(persistentCanvas == null){
	// 		persistentCanvas = this;
	// 		DontDestroyOnLoad(gameObject);
	// 	} else if (persistentCanvas != this){
	// 		Destroy(gameObject);
	// 	}
	// }

	public List<GameObject> scoreTexts;
	public GameObject highScoreText;
	public GameObject finalScorePanel;

	public void SetScore(int currentScore){
		foreach(GameObject scoreText in scoreTexts){
			Text t = scoreText.GetComponent<Text>();
			t.text = currentScore.ToString();
		}
	}

	public void SetHighScore(int score){
		Text t = highScoreText.GetComponent<Text>();
		t.text = score.ToString();
	}
	public void SetFinalScorePanelActive(bool active){
		finalScorePanel.SetActive(active);
	}
}
