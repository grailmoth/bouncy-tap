﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DigitalRuby.Pooling;
using System.Linq;
using UnityEngine.UI;
using EasyMobile;

public class GameManager : MonoBehaviour {
	public static GameManager gameManager;
	public const int DEATHS_BETWEEN_ADS = 5;
	public bool paused;
	public Player player;
	private float PLAYER_GRAVITY_SCALE;
	public float TILE_WIDTH = 3f;
	[SerializeField]
	private GameObject gameCameraObj;
	private Camera gameCamera;
	private Vector3 _lowerLeftCornerWP;
	public Vector3 lowerLeftCornerWP{
		get{
			return _lowerLeftCornerWP;
		}
		set{
			_lowerLeftCornerWP = value;
		}
	}
	private Vector3 _upperRightCornerWP;
	public Vector3 upperRightCornerWP{
		get{
			return _upperRightCornerWP;
		}
		set{
			_upperRightCornerWP = value;
		}
	}
	private Vector3 _lowerRightCornerWP;
	public Vector3 lowerRightCornerWP{
		get{
			return _lowerRightCornerWP;
		}
		set{
			_lowerRightCornerWP = value;
		}
	}
	private Vector3 _upperLefCornerWP;
	public Vector3 upperLefCornerWP{
		get{
			return _upperLefCornerWP;
		}
		set{
			_upperLefCornerWP = value;
		}
	}
	public float spaceBetweenObstacles;
	private List<GameObject> floor;
	private List<GameObject> ceiling;
	[SerializeField]
	private GameObject[] tilePrefabs;
	[SerializeField]
	private string[] obstacleKeys;
	[SerializeField] 
	private GameObject[] obstaclePrefabs;
	[SerializeField]
	private GameObject[] pieSlicePrefabs;
	private GameObject currentPieSlicePrefab;
	private int currentPieColorCount;
	private List<GameObject> upcomingPieSlices;
	private List<GameObject> upcomingObstacles;
	public float gameSpeed;
	private Color32 Red;
	private Color32 Yellow;
	private Color32 Blue;
	private Color32 ErrorPink;
	private static readonly System.Random getrandom = new System.Random();
	private float objPos2;
	private float objPos3;
	private float objPos4;
	private float objPos1;
	private float objPos5;
	[SerializeField] GameObject[] scoreTexts;
	private int currentScore;
	public GameObject finalScorePanel;
	public GameObject highScoreText;
	private const int NOT_PRESENT = -100;
	public GameCanvas currentCanvas;
	

	void Awake(){
		if(gameManager == null){
			gameManager = this;
			DontDestroyOnLoad(gameObject);
		} else if (gameManager != this){
			Destroy(gameObject);
		}
		string sceneName = SceneManager.GetActiveScene().name;
		if(sceneName == "Game" || sceneName == "Tutorial"){
			gameCamera = gameCameraObj.GetComponent<Camera>();
			lowerLeftCornerWP = gameCamera.ScreenToWorldPoint(new Vector3(0,0,0));
			upperRightCornerWP = gameCamera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0));
			upperLefCornerWP = gameCamera.ScreenToWorldPoint(new Vector3 (0,Screen.height,0));
			lowerRightCornerWP = gameCamera.ScreenToWorldPoint(new Vector3 (Screen.width,0,0));
			objPos1 = lowerRightCornerWP.y + 5 * ((upperRightCornerWP.y - lowerRightCornerWP.y)/6);
			objPos2 = lowerRightCornerWP.y + 4 * ((upperRightCornerWP.y - lowerRightCornerWP.y)/6);
			objPos3 = lowerRightCornerWP.y + 3 * (upperRightCornerWP.y - lowerRightCornerWP.y)/6;
			objPos4 = lowerRightCornerWP.y + 2 * (upperRightCornerWP.y - lowerRightCornerWP.y)/6;
			objPos5 = lowerRightCornerWP.y + 1 * (upperRightCornerWP.y - lowerRightCornerWP.y)/6;
			Red = new Color32(252,66,119,255);
			Blue = new Color32(74,170,199,255);
			Yellow = new Color32(252,242,109,255);
			ErrorPink = new Color32(229,69,222,255);
		}
	}

	void OnEnable()
	{
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}
	// void Start(){
	// 	string sceneName = SceneManager.GetActiveScene().name;
	// 	if (sceneName == "Settings"){
	// 		int SoundEffectsInt = PlayerPrefs.GetInt(PP_SFX,2);
	// 		int MusicInt = PlayerPrefs.GetInt(PP_MUSIC,2);
	// 		if(SoundEffectsInt == 2){
	// 			sfxOn = true;
	// 		}
	// 		if(MusicInt == 2){
	// 			musicOn = true;
	// 		}
	// 		GameObject SoundEffectsButton = GameObject.FindGameObjectWithTag("SoundEffectsButton");
	// 		GameObject MusicButton = GameObject.FindGameObjectWithTag("MusicButton");
	// 		SoundEffectsButton.GetComponent<SoundControl>().SetActive(sfxOn);
	// 		MusicButton.GetComponent<SoundControl>().SetActive(musicOn);
	// 	}
	// }

	///<summary>
	///returns a random int where min <= x < max
	///</summary>
	///<param name="min">inclusive</param>
	///<param name="max">exclusive</param>
	public static int GetRandomInt(int in_min, int ex_max)
	{
		lock(getrandom) // synchronize
		{
			return getrandom.Next(in_min, ex_max);
		}
	}
	public static float GetRandomFloat(float min, float max)
	{
		lock(getrandom) // synchronize
		{
			return (float)getrandom.NextDouble()*(max-min)+min;
		}
	}

	void OnLevelFinishedLoading (Scene scene, LoadSceneMode mode){
		string sceneName = SceneManager.GetActiveScene().name;
		if(sceneName == "Game" || sceneName == "Tutorial"){
			currentCanvas = FindObjectOfType<GameCanvas>();
			player = FindObjectOfType<Player>();
			player.Load();
			if(player){
				PLAYER_GRAVITY_SCALE = player.gameObject.GetComponent<Rigidbody2D>().gravityScale;
			}
			Pause();
			floor = new List<GameObject>();
			ceiling = new List<GameObject>();
			upcomingObstacles = new List<GameObject>();
			upcomingPieSlices = new List<GameObject>();
			currentPieSlicePrefab = GetNewPieSlicePrefab(pieSlicePrefabs[GetRandomInt(0,pieSlicePrefabs.Length)]);
			currentPieColorCount = 0;
			MakeFloorAndCeiling();
			for(int i = 0; i < 6; i++){
				AddRandomObstacleAndPieSlices();
			}
			currentScore = 0;
			currentCanvas.SetScore(currentScore);
			// player.Load();
			currentCanvas.SetFinalScorePanelActive(false);
			gameCameraObj = FindObjectOfType<TouchHandler>().gameObject;
			gameCamera = gameCameraObj.GetComponent<Camera>();
			lowerLeftCornerWP = gameCamera.ScreenToWorldPoint(new Vector3(0,0,0));
			upperRightCornerWP = gameCamera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0));
			upperLefCornerWP = gameCamera.ScreenToWorldPoint(new Vector3 (0,Screen.height,0));
			lowerRightCornerWP = gameCamera.ScreenToWorldPoint(new Vector3 (Screen.width,0,0));
			objPos1 = lowerRightCornerWP.y + 5 * ((upperRightCornerWP.y - lowerRightCornerWP.y)/6); //top
			objPos2 = lowerRightCornerWP.y + 4 * ((upperRightCornerWP.y - lowerRightCornerWP.y)/6);
			objPos3 = lowerRightCornerWP.y + 3 * (upperRightCornerWP.y - lowerRightCornerWP.y)/6;
			objPos4 = lowerRightCornerWP.y + 2 * (upperRightCornerWP.y - lowerRightCornerWP.y)/6;
			objPos5 = lowerRightCornerWP.y + 1 * (upperRightCornerWP.y - lowerRightCornerWP.y)/6; //bottom
			player.ControlledStart();
		}
		
	}

	public void Pause(){
		paused = true;
		Rigidbody2D playerRB = player.gameObject.GetComponent<Rigidbody2D>();
		playerRB.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
	}
	
	public void Unpause(){
		paused = false;
		Rigidbody2D playerRB = player.gameObject.GetComponent<Rigidbody2D>();
		playerRB.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		// player.gameObject.GetComponent<Rigidbody2D>().gravityScale = PLAYER_GRAVITY_SCALE;
	}
	public void ShakeCamera(){
		gameCamera.GetComponent<Animator>().SetTrigger("CameraShake");
	}

	private void MakeFloorAndCeiling(){
		Vector3 positionBottom = new Vector3(lowerLeftCornerWP.x,lowerLeftCornerWP.y,0);
		Vector3 positionTop = new Vector3(upperLefCornerWP.x,upperLefCornerWP.y,0);
		for(int i = 0; i < tilePrefabs.Length; i++){
			for(int j = 0; j < 3; j++){
				GameObject newFloorTile = Instantiate(tilePrefabs[j],positionBottom,Quaternion.identity);
				GameObject newCeilingTile = Instantiate(tilePrefabs[j],positionTop,Quaternion.identity);
				positionBottom = positionBottom+new Vector3(TILE_WIDTH,0,0);
				positionTop = positionTop+new Vector3(TILE_WIDTH,0,0);
				floor.Add(newFloorTile);
				ceiling.Add(newCeilingTile);
			}
		}
	}
	public void AddRandomObstacleAndPieSlices(GameObject removeObstacle = null){
		if(removeObstacle != null){
			upcomingObstacles.Remove(removeObstacle);
			GameObject.Destroy(removeObstacle);
		}
		GameObject obstaclePrefab = obstaclePrefabs[GameManager.GetRandomInt(0,obstaclePrefabs.Length)];
		int r = GetRandomInt(0,4);
		float posYObstacle1 = 0;
		float posYObstacle2 = NOT_PRESENT;
		float posYPieSlice = 0;
		Quaternion rotation1 = Quaternion.identity;
		Quaternion rotation2 = Quaternion.identity;
		if( r == 3 ) {
			posYObstacle1 = objPos1;
			posYObstacle2 = objPos5;
			int rand1 = GameManager.GetRandomInt(0,3);
			posYPieSlice = rand1 == 2 ? objPos4 : rand1 == 1 ? objPos3 : objPos2;
			int rand2 = GameManager.GetRandomInt(0,2);
			int rand3 = GameManager.GetRandomInt(0,2);
			rotation1 = rand2 == 1 ? Quaternion.Euler(0,0,45) : Quaternion.Euler(0,0,-45);
			rotation2 = rand3 == 1 ? Quaternion.Euler(0,0,45) : Quaternion.Euler(0,0,-45);
		}
		else if( r == 2 ) {
			posYObstacle1 = objPos2;
			posYPieSlice = objPos4;
			int rand2 = GameManager.GetRandomInt(0,3);
			rotation1 = rand2 == 2 ? Quaternion.Euler(0,0,45) : rand2 == 2 ? Quaternion.Euler(0,0,-45) : Quaternion.identity;
		}
		else if ( r == 1 ) { 
			posYObstacle1 = objPos3;
			posYPieSlice = objPos1;
			rotation1 = GameManager.GetRandomInt(0,2) == 1 ? Quaternion.Euler(0,0,45) : Quaternion.Euler(0,0,-45);
			int rand = GameManager.GetRandomInt(0,2);
		}
		else {
			posYObstacle1 = objPos4;
			posYPieSlice = objPos2;
			int rand = GameManager.GetRandomInt(0,3);
			rotation1 = rand == 2 ? Quaternion.Euler(0,0,45) : rand == 1 ? Quaternion.Euler(0,0,-45) : Quaternion.identity;
		}
		// else {
		// 	posYObstacle1 = objPos5;
		// 	int rand1 = GameManager.GetRandomInt(0,2);
		// 	posYPieSlice = rand1 == 1 ? objPos2 : objPos3;
		// 	int rand2 = GameManager.GetRandomInt(0,2);
		// 	rotation1 = rand2 == 1 ? Quaternion.Euler(0,0,45) : Quaternion.Euler(0,0,-45);
		// }
		float posXObstacle = upcomingObstacles.Count > 0 ? upcomingObstacles[upcomingObstacles.Count - 1].transform.position.x + spaceBetweenObstacles : spaceBetweenObstacles;
		GameObject newObstacle1 = Instantiate(obstaclePrefab,new Vector3(posXObstacle,posYObstacle1,0),rotation1);
		if(posYObstacle2 != NOT_PRESENT){
			GameObject newObstacle2 = Instantiate(obstaclePrefab,new Vector3(posXObstacle,posYObstacle2,0),rotation2);
		}
		GameObject newPieSlice1 = Instantiate(currentPieSlicePrefab,new Vector3(posXObstacle,posYPieSlice,0),Quaternion.identity);
		int random = GameManager.GetRandomInt(0,2);
		// float posYPieSlice2 = random == 4 ? objPos5 : random == 3 ? objPos4 : random == 2 ? objPos3 : random == 1 ? objPos2 : objPos1;
		float posYPieSlice2 = random == 0 ? objPos4 : objPos2;
		GameObject newPieSlice2 = Instantiate(currentPieSlicePrefab,new Vector3(posXObstacle + spaceBetweenObstacles/2,posYPieSlice2,0),Quaternion.identity);
		upcomingObstacles.Add(newObstacle1);
		upcomingPieSlices.Add(newPieSlice1);
		currentPieColorCount += 2;
		if(currentPieColorCount >= 8){
			currentPieColorCount = 0;
			currentPieSlicePrefab = GetNewPieSlicePrefab(currentPieSlicePrefab);
		}
	}
	
	public Color32 GetColorValues(Enums.gameColor color){
		if(color == Enums.gameColor.Red) return Red;
		else if(color == Enums.gameColor.Yellow) return Yellow;
		else if(color == Enums.gameColor.Blue) return Blue;
		else return ErrorPink;
	}

	public void MoveFloorTile(GameObject floorTile){
		floor.Remove(floorTile);
		GameObject lastTile = floor[floor.Count -1];
		float lastTilePosX = lastTile.gameObject.transform.position.x;
		float newPosX = lastTilePosX + TILE_WIDTH;
		floorTile.transform.position = new Vector3(newPosX,floorTile.transform.position.y,floorTile.transform.position.z);
		floor.Add(floorTile);
	}
	public void MoveCeilingTile (GameObject ceilingTile){
		ceiling.Remove(ceilingTile);
		GameObject lastTile = ceiling[ceiling.Count -1];
		float lastTilePosX = lastTile.gameObject.transform.position.x;
		float newPosX = lastTilePosX + TILE_WIDTH;
		ceilingTile.transform.position = new Vector3(newPosX,ceilingTile.transform.position.y,ceilingTile.transform.position.z);
		ceiling.Add(ceilingTile);
	}
	public void ResetScene(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	private GameObject GetNewPieSlicePrefab(GameObject currentPrefab){
		int r = GetRandomInt(0,2);
		List<Enums.gameColor> colors = System.Enum.GetValues(typeof(Enums.gameColor)).Cast<Enums.gameColor>().ToList();
		colors.Remove(currentPrefab.GetComponent<PieSlice>().myColor);
		return FindByColor(pieSlicePrefabs,colors[r]);
	}

	private GameObject FindByColor(GameObject[] prefabs, Enums.gameColor color){
		foreach(GameObject prefab in prefabs){
			if(prefab.GetComponent<PieSlice>().myColor == color){
				return prefab;
			}
		}
		Debug.LogError("No prefab with color: " + color.ToString() + " in list " + prefabs.ToString());
		return null;
	}
	public void IncrementScore(int i){
			currentScore = currentScore + i;
		// foreach(GameObject scoreText in scoreTexts){
		// 	Text t = scoreText.GetComponent<Text>();
		// 	// currentScore = Int32.Parse(t.text);
		// 	t.text = currentScore.ToString();
		// }
		currentCanvas.SetScore(currentScore);
	}
	public void ShowFinalScore(){
		if(player.GetComponent<Player>().deathsSinceAd >= DEATHS_BETWEEN_ADS){
			bool isReady = Advertising.IsInterstitialAdReady() && !PurchaseManager.purchaseManager.noAds;
			if(isReady){
				Advertising.ShowInterstitialAd();
			}
		} else {
			if(StoreReview.CanRequestRating()){
				StoreReview.RequestRating();
			}
		}
		currentCanvas.SetFinalScorePanelActive(true);
		if (currentScore > player.highScore){
			player.highScore = currentScore;
			Leaderboard.leaderboard.ReportScore(player.highScore);
		}
		player.Save();
	}
}
