﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

public Transform startPoint;
public Transform endPoint;
LineRenderer laserLine;
	void Start () {
		laserLine = GetComponent<LineRenderer>();
		laserLine.startWidth = 2f;
		laserLine.endWidth = 2f;
	}
	
	void Update () {
		laserLine.SetPosition(0, startPoint.position);
		laserLine.SetPosition(1, endPoint.position);
	}
}
