﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using UnityEngine.SceneManagement;

public class Leaderboard : MonoBehaviour {

	public static Leaderboard leaderboard;

	void Awake(){
		if(leaderboard == null){
			leaderboard = this;
			DontDestroyOnLoad(gameObject);
		} else if (leaderboard != this){
			Destroy(gameObject);
		}
	}
	// Use this for initialization
	void OnEnable(){
		SceneManager.sceneLoaded += OnSceneLoaded;
	}
	public void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		GameServices.ManagedInit();
	}

	public void ShowLeaderboardUI(){
		if (GameServices.IsInitialized())
            {
                GameServices.ShowLeaderboardUI();
            }
            else
            {
                #if UNITY_ANDROID
                GameServices.Init();
                #elif UNITY_IOS
                NativeUI.Alert("Service Unavailable", "The user is not logged in.");
                #else
                Debug.Log("Cannot show leaderboards: platform not supported.");
                #endif
            }
	}
	public void ShowAchievementUI(){            
		if (GameServices.IsInitialized())
		{
			GameServices.ShowAchievementsUI();
		}
		else
		{
			#if UNITY_ANDROID
			GameServices.Init();
			#elif UNITY_IOS
			NativeUI.Alert("Service Unavailable", "The user is not logged in.");
			#else
			Debug.Log("Cannot show achievements: platform not supported.");
			#endif
		}
	}
	public void ReportScore(int score){
		if(IsLoggedIn()){
			GameServices.ReportScore(score, EM_GameServicesConstants.Leaderboard_Top_Piemakers);
		}
	}
	public bool IsLoggedIn(){
		bool isLoggedIn = GameServices.IsInitialized() && !Application.isEditor;
		return isLoggedIn;
	}
}
