﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moveable : MonoBehaviour {
	public bool shouldMove;
	private Vector3 moveVector;
	void Start()
	{
		shouldMove = true;
		moveVector = new Vector3(0f, 0f, 0f);
	}

	void LateUpdate () {
		if(!GameManager.gameManager.paused && shouldMove){
			moveVector.Set(GameManager.gameManager.gameSpeed * Time.deltaTime, 0f, 0f);
			gameObject.transform.position -= moveVector;
			// gameObject.transform.position -= new Vector3(GameManager.gameManager.gameSpeed * Time.deltaTime, 0f, 0f);
		}
	}
}
