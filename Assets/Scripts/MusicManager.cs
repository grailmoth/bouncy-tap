﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
public class MusicManager : MonoBehaviour {

	public static  MusicManager musicManager;
	public AudioClip[] levelMusicArray;
	private AudioSource audioSource;
	private float startVolume;
	private int currentMusicSceneIndex;
	private const string PP_MUSIC = "MusicOn";
	private const string PP_SFX = "SoundEffectsOn";
	public bool musicOn{
		get{
			return PlayerPrefs.GetInt(PP_MUSIC) == 1;
		}
		set{
			PlayerPrefs.SetInt(PP_MUSIC, value ? 1 : 0);
		}
	}
	public bool sfxOn{
		get{
			return PlayerPrefs.GetInt(PP_SFX) == 1;
		}
		set{
			PlayerPrefs.SetInt(PP_SFX, value ? 1 : 0);
		}
	}
	void Awake(){
		if(musicManager == null){
			musicManager = this;
			DontDestroyOnLoad(gameObject);
		} else if (musicManager != this){
			Destroy(gameObject);
		}
		currentMusicSceneIndex = SceneManager.GetActiveScene().buildIndex;
	}
	void OnEnable()
	{
		audioSource = gameObject.GetComponent<AudioSource>();
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
		startVolume = audioSource.volume;
	}
	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode){
		// audioSource.Stop ();
        audioSource.volume = startVolume;
		int previousMusicSceneIndex = currentMusicSceneIndex;
		currentMusicSceneIndex = SceneManager.GetActiveScene().buildIndex;
		currentMusicSceneIndex = currentMusicSceneIndex > 1 ? 0 : currentMusicSceneIndex;;
		AudioClip curMusic = levelMusicArray[currentMusicSceneIndex];
		string sceneName = SceneManager.GetActiveScene().name;
		int SoundEffectsInt = PlayerPrefs.GetInt(PP_SFX,2);
		int MusicInt = PlayerPrefs.GetInt(PP_MUSIC,2);
		if(SoundEffectsInt == 2){
			sfxOn = true;
		}
		if(MusicInt == 2){
			musicOn = true;
		}
		if (sceneName == "Settings"){
			GameObject SoundEffectsButton = GameObject.FindGameObjectWithTag("SoundEffectsButton");
			GameObject MusicButton = GameObject.FindGameObjectWithTag("MusicButton");
			SoundEffectsButton.GetComponent<SoundControl>().SetActive(sfxOn);
			MusicButton.GetComponent<SoundControl>().SetActive(musicOn);
		}
		if(curMusic){
			audioSource.clip = curMusic;
			audioSource.loop = true;
			if(musicOn){
				if(previousMusicSceneIndex != currentMusicSceneIndex || !audioSource.isPlaying){
					audioSource.Play();
				}
			}
		}
	}
	public void SetMusicActive(bool b){
		if(b){
			audioSource.Play();
		} else {
			audioSource.Stop();
		}
	}

	public IEnumerator FadeOut (float FadeTime) {
        while (audioSource.volume > 0) {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
 
            yield return null;
        }
 
        audioSource.Stop ();
        audioSource.volume = startVolume;
    }

	
}
