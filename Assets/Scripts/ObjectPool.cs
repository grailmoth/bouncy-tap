﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {
	public static ObjectPool pool;
	[System.Serializable]
	public struct PoolPrefabs
        {
            public string Key;
            public GameObject Prefab;
        }
	[SerializeField]
	private PoolPrefabs[] objectPrefabs;
	private List<GameObject> cachedObjects;
	void Awake()
	{
		if(pool == null){
			pool = this;
			DontDestroyOnLoad(gameObject);
		} else if (pool != this){
			Destroy(gameObject);
		}

		SaturatePool();
	}
	
	private void SaturatePool(){
		// foreach(GameObject prefab in objectPrefabs){
			
		// }
	}
}
