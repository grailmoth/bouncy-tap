﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
	public bool generateNewWhenDone;

	// Use this for initialization
	void Start () {
		generateNewWhenDone = true;
	}
	
	void Update () {
		if(gameObject.transform.position.x < GameManager.gameManager.lowerLeftCornerWP.x - 2.5){
			if(generateNewWhenDone){
				GameManager.gameManager.AddRandomObstacleAndPieSlices(gameObject);
			}
		}
	}
}
