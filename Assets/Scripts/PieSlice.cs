﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PieSlice : MonoBehaviour {
	public bool used;
	public GameObject scoreText;
	public GameObject myCanvas;
	private Animator textAnimator;
	private GameObject pieTin;
	public bool moving;
	private AudioSource coinSound;
	void Start()
	{
		used = false;
		textAnimator = scoreText.GetComponent<Animator>();
		pieTin = GameObject.FindGameObjectWithTag("PieTin");
		moving = false;
		coinSound = gameObject.GetComponent<AudioSource>();
	}
	[SerializeField]
	private Enums.gameColor _myColor;
	public Enums.gameColor myColor{
		get{
			return _myColor;
		}
		private set {
			_myColor = value;
		}
	}
	public void ShowScore(){
		textAnimator.SetTrigger("ScoreTrigger");
	}
	public void playCoinSound(){
		if(MusicManager.musicManager.sfxOn){
			coinSound.Play();
		}
	}
	public void Die(){
		playCoinSound();
		StartCoroutine(WaitForSecondsThenExecute(()=>Destroy(gameObject),2f));
	}
	public IEnumerator WaitForSecondsThenExecute(Action method, float waitTime)
         {
             yield return new  WaitForSeconds(waitTime);
             method();
			 gameObject.SetActive(false);
         }
	void Update(){
		if(moving){
			float step = 10 * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, pieTin.transform.position, step);
		}
	}
	
}
