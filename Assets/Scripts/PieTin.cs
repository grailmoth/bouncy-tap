﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PieTin : MonoBehaviour {
	public GameObject scoreTextPrefab;
	public GameObject myCanvas;
	private AudioSource audioSource;
	public AudioClip[] sounds;

	void Start () {
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	public void ShowMessage(string message, Enums.gameColor messageColor, int sliceCount){
		GameObject scoreText = Instantiate(scoreTextPrefab,new Vector3(0,0,0),Quaternion.identity);
		scoreText.GetComponent<RectTransform>().SetParent(myCanvas.transform);
		scoreText.GetComponent<Text>().text = message;
		scoreText.GetComponent<Text>().color = GameManager.gameManager.GetColorValues(messageColor);
		scoreText.GetComponent<RectTransform>().localScale = new Vector3(.1f,.1f,1f);
		Animator textAnimator = scoreText.GetComponent<Animator>();
		textAnimator.SetTrigger("ScoreTriggerSideways");
		if(sliceCount < 8) {
			audioSource.clip = sounds[0];
		} else {
			audioSource.clip = sounds[1];
		}
		if(MusicManager.musicManager.sfxOn){
			audioSource.Play();
		}
	}
}
