﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using EasyMobile;

public class Player : MonoBehaviour {
	private Rigidbody2D rb;
	public float jump;
	private Enums.gameColor _currentColor;
	private Quaternion nextSliceRotation;
	private Quaternion negativeNextSliceRotation;
	public int highScore = 0;
	private bool alive;
	private List<GameObject> slices;
	[SerializeField]
	private GameObject myLight;
	private float brightLight = 10f;
	public GameObject scoreTextPrefab;
	public GameObject myCanvas;
	public GameObject blueSparksPrefab;
	public GameObject redSparksPrefab;
	public GameObject yellowSparksPrefab;
	public GameObject skull;
	public GameObject pieTin;
	public int deathsSinceAd;
	public int stars;
	private AudioSource audioSource;
	public ParticleSystem redExplosion;
	public ParticleSystem blueExplosion;
	public ParticleSystem yellowExplosion;
	public AudioClip[] sounds;
	public bool hasSeenTutorial = false;
	// private Animator textAnimator;
	// private Enums.gameColor currentSliceColor;
	public Enums.gameColor currentColor{
		get{return _currentColor;}
		set{
			_currentColor = value;
			gameObject.GetComponent<SpriteRenderer>().color = GameManager.gameManager.GetColorValues(_currentColor);
			myLight.GetComponent<Light>().color = GameManager.gameManager.GetColorValues(_currentColor);
		}
	}
	private Coroutine currentFlare;
	// Use this for initialization
	void OnEnable()
	{
		Advertising.InterstitialAdCompleted += InterstitialAdCompletedHandler;
	}  

	// The event handler
	void InterstitialAdCompletedHandler(InterstitialAdNetwork network, AdPlacement placement)
	{
		deathsSinceAd = 0;
		Save();
	}

	// Unsubscribe
	void OnDisable()
	{
		Advertising.InterstitialAdCompleted -= InterstitialAdCompletedHandler;
	}
	// void Start () {
	// 	rb = GetComponent<Rigidbody2D>();
	// 	int r = GameManager.GetRandomInt(0,3);
	// 	if(r > 1) currentColor = Enums.gameColor.Red;
	// 	else if(r > 0) currentColor = Enums.gameColor.Blue;
	// 	else currentColor = Enums.gameColor.Yellow;
	// 	slices = new List<GameObject>();
	// 	nextSliceRotation = Quaternion.Euler(0,0,0);
	// 	// GameManager.gameManager.highScoreText.GetComponent<Text>().text = highScore.ToString();
	// 	GameManager.gameManager.currentCanvas.SetHighScore(highScore);
	// 	alive = true;
	// 	pieTin = GameObject.FindGameObjectWithTag("PieTin");
	// 	audioSource = gameObject.GetComponent<AudioSource>();
	// }
	public void ControlledStart(){
		rb = GetComponent<Rigidbody2D>();
		int r = GameManager.GetRandomInt(0,3);
		if(r > 1) currentColor = Enums.gameColor.Red;
		else if(r > 0) currentColor = Enums.gameColor.Blue;
		else currentColor = Enums.gameColor.Yellow;
		slices = new List<GameObject>();
		nextSliceRotation = Quaternion.Euler(0,0,0);
		// GameManager.gameManager.highScoreText.GetComponent<Text>().text = highScore.ToString();
		GameManager.gameManager.currentCanvas.SetHighScore(highScore);
		alive = true;
		pieTin = GameObject.FindGameObjectWithTag("PieTin");
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if(!alive) return;
		if(other.gameObject.GetComponent<ColorChanger>()){
			AddJump(jump*.8F);
			GenerateSparks(other); 
			if(currentColor != other.gameObject.GetComponent<ColorChanger>().myColor){
				if(slices.Count > 0){
					ScoreSlices();
				}
				currentColor = other.gameObject.GetComponent<ColorChanger>().myColor;
			}
			
		} else if(other.gameObject.GetComponent<Obstacle>()){
			audioSource.clip = sounds[1];
			if(MusicManager.musicManager.sfxOn){
				audioSource.Play();
			}
			Die();
		}
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if(!alive) return;
		if(other.gameObject.GetComponent<PieSlice>() && !other.gameObject.GetComponent<PieSlice>().used){
			other.gameObject.GetComponent<Moveable>().shouldMove = false;
			other.gameObject.GetComponent<PieSlice>().used = true;
			other.gameObject.GetComponent<PieSlice>().playCoinSound();
			AddSlice(other.gameObject);
			// other.gameObject.GetComponent<PieSlice>().ShowScore();
			int scoreIncrement = other.gameObject.GetComponent<PieSlice>().myColor == currentColor ? 1 : 1;
			GameManager.gameManager.IncrementScore(scoreIncrement);
			ShowMessage("+"+scoreIncrement.ToString(),other.gameObject.GetComponent<PieSlice>().myColor);
		};
	}
	private void GenerateSparks(Collision2D other){
		if(!alive) return;
		ContactPoint2D contact = other.contacts[0];
        Quaternion rot = Quaternion.Euler(new Vector3(contact.normal[1] * -90f,0f,0f));
        // Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point;
		if(other.gameObject.GetComponent<ColorChanger>().myColor == Enums.gameColor.Red) Instantiate(redSparksPrefab, pos, rot);
		else if(other.gameObject.GetComponent<ColorChanger>().myColor == Enums.gameColor.Blue) Instantiate(blueSparksPrefab, pos, rot);
		else if(other.gameObject.GetComponent<ColorChanger>().myColor == Enums.gameColor.Yellow) Instantiate(yellowSparksPrefab, pos, rot);
	}

	private void ShowMessage(string message, Enums.gameColor messageColor){
		GameObject scoreText = Instantiate(scoreTextPrefab,new Vector3(0,0,0),Quaternion.identity);
		scoreText.GetComponent<RectTransform>().SetParent(myCanvas.transform);
		scoreText.GetComponent<Text>().text = message;
		scoreText.GetComponent<Text>().color = GameManager.gameManager.GetColorValues(messageColor);
		scoreText.GetComponent<RectTransform>().localScale = new Vector3(.1f,.1f,1f);
		Animator textAnimator = scoreText.GetComponent<Animator>();
		textAnimator.SetTrigger("ScoreTrigger");
	}
	public void AddSlice(GameObject slice){
		PieSlice pieSlice = slice.GetComponent<PieSlice>();
		if(pieSlice == null){
			Debug.LogError("GameObject not a PieSlice");
			return;
		} else if ( pieSlice.myColor != currentColor){
			if(slices.Count > 0){
				ScoreSlices();
			}
			// slice.GetComponent<SpriteRenderer>().enabled = false;
			slice.GetComponent<SpriteRenderer>().enabled = false;
			slice.GetComponent<PieSlice>().Die();
			return;
		}
		if(pieSlice.myColor != currentColor && slices.Count > 0){
			ScoreSlices();
			AppendSlice(slice);
		} else if(slices.Count < 8){
			AppendSlice(slice);
		}
		if(slices.Count == 8){
			// AppendSlice(slice);
			ScoreSlices();
		}
		// currentSliceColor = pieSlice.myColor;
	}
	public void Flare(){
		if(!alive) return;
		audioSource.clip = sounds[0];
		if(MusicManager.musicManager.sfxOn){
			audioSource.Play();
		}
		myLight.GetComponent<Light>().intensity = brightLight;
		if(currentFlare != null) {
			StopCoroutine(currentFlare);
		}
		Coroutine newFlare = StartCoroutine("FlareLight");
		currentFlare = newFlare;
	}
	private IEnumerator FlareLight(){
		for (float f = brightLight; f >= .5; f -= 0.5f) 
		{	
			myLight.GetComponent<Light>().intensity = f;
			yield return null;
		}
	}
	private void AppendSlice(GameObject slice){
		slices.Add(slice);
		// slice.transform.SetParent(gameObject.transform,false);
		// slice.transform.localPosition = new Vector3(0,0,0);
		slice.transform.rotation = nextSliceRotation;
		slice.GetComponent<PieSlice>().moving = true;
		slice.GetComponent<PieSlice>().myCanvas.GetComponent<RectTransform>().rotation = Quaternion.identity;
		AdvanceNextSliceRotation();
	}
	private void AdvanceNextSliceRotation(){
		Quaternion next = Quaternion.Euler(0,0,nextSliceRotation.eulerAngles.z - 45f);
		negativeNextSliceRotation = Quaternion.Euler(0,0,-next.eulerAngles.z);
		nextSliceRotation = next;
	}
	private void ScoreSlices(){
		if(slices.Count <= 0) return;
		int scoreIncrement = slices.Count == 8 ? slices.Count * 2 : slices.Count * 3;
		GameManager.gameManager.IncrementScore(slices.Count * 3);
		if(slices.Count == 8){
			pieTin.GetComponent<PieTin>().ShowMessage("PERFECTION!!! +"+(scoreIncrement).ToString(), currentColor, slices.Count);
		} else {
			pieTin.GetComponent<PieTin>().ShowMessage("COLOR STREAK! +"+(scoreIncrement).ToString(), currentColor, slices.Count);
		}
		
		foreach(GameObject s in slices) Destroy(s);
		slices = new List<GameObject>();
		nextSliceRotation = Quaternion.identity;
	}
	public void AddJump(float j = 0){
		if(!alive) return;
		// if(deathsSinceAd >= GameManager.DEATHS_BETWEEN_ADS){
		// 	if(Advertising.IsInterstitialAdReady()){
		// 		return;
		// 	}
		// }
		if(GameManager.gameManager.paused){
			GameManager.gameManager.Unpause();
		}
		if(j == 0){
			j = jump;
		}
		if(rb){
			rb.velocity = new Vector3(0f,j,0f);
		}
	}
	private void Die(){
		deathsSinceAd++;
		// StartCoroutine(MusicManager.musicManager.FadeOut(2f));
		alive = false;
		ScoreSlices();
		GameManager.gameManager.Pause();
		SpriteRenderer mySprite = GetComponent<SpriteRenderer>();
		mySprite.enabled = false;
		gameObject.GetComponentInChildren<Light>().gameObject.SetActive(false);
		if(currentColor == Enums.gameColor.Red) redExplosion.gameObject.SetActive(true);
		else if(currentColor == Enums.gameColor.Blue) blueExplosion.gameObject.SetActive(true);
		else if(currentColor == Enums.gameColor.Yellow) yellowExplosion.gameObject.SetActive(true);
		GameManager.gameManager.ShakeCamera();
		skull.GetComponent<Animator>().SetTrigger("Die");
		StartCoroutine(WaitForSecondsThenExecute(()=>GameManager.gameManager.ShowFinalScore(),2f));
		// GameManager.gameManager.ShowFinalScore();
		// gameObject.SetActive(false);
	}
	public IEnumerator WaitForSecondsThenExecute(Action method, float waitTime)
         {
             yield return new  WaitForSeconds(waitTime);
             method();
			 SpriteRenderer s = gameObject.GetComponent<SpriteRenderer>();
			 if(s != null){
				 s.enabled = false;
			 }
         }

	public void Save(){
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/NeonLaserPiePlayerData.dat");
		PlayerData data = new PlayerData();
		data.SetPlayerData(this);
		bf.Serialize(file,data);
		file.Close();
		Leaderboard.leaderboard.ReportScore(highScore);
	}
	public void Load(){
		if(File.Exists(Application.persistentDataPath +"/NeonLaserPiePlayerData.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/NeonLaserPiePlayerData.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file); //(PlayerData) This casts the deserialized file to a PlayerData object
			file.Close();
			InitializePlayer(data);
		}
	}
	private void InitializePlayer(PlayerData data){
		highScore = data.highScore;
		deathsSinceAd = data.deathsSinceAd;
		stars = data.stars;
		hasSeenTutorial = data.hasSeenTutorial;
		GameManager.gameManager.currentCanvas.SetHighScore(highScore);
	}
}

[Serializable]
class PlayerData {
	public int highScore { get; set; }
	public int deathsSinceAd { get; set; }
	public int stars { get; set; }
	public bool hasSeenTutorial { get; set;}
	public void SetPlayerData(Player player){
		highScore = player.highScore;
		deathsSinceAd = player.deathsSinceAd;
		stars = player.stars;
		hasSeenTutorial = player.hasSeenTutorial;
	}
	
}

