﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;

public class PurchaseManager : MonoBehaviour {
	public static PurchaseManager purchaseManager;
	public bool noAds;
	void Awake(){
		if(purchaseManager == null){
			purchaseManager = this;
			DontDestroyOnLoad(gameObject);
		} else if (purchaseManager != this){
			Destroy(gameObject);
		}
		RuntimeManager.Init();
	}
	void Start()
	{
		noAds = checkNoAds();
	}
	void OnEnable()
	{            
		InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;
		InAppPurchasing.PurchaseFailed += PurchaseFailedHandler;
		InAppPurchasing.RestoreCompleted += RestoreCompletedHandler;
		InAppPurchasing.RestoreFailed += RestoreFailedHandler;
		InAppPurchasing.PurchaseDeferred += PurchaseDeferredHandler;
		InAppPurchasing.PromotionalPurchaseIntercepted += PromotionalPurchaseInterceptedHandler;
		InAppPurchasing.InitializeSucceeded += IAPInitializedHander;
	}

	// Unsubscribe when the game object is disabled
	void OnDisable()
	{            
		InAppPurchasing.PurchaseCompleted -= PurchaseCompletedHandler;
		InAppPurchasing.PurchaseFailed -= PurchaseFailedHandler;
		InAppPurchasing.RestoreCompleted -= RestoreCompletedHandler;
		InAppPurchasing.RestoreFailed -= RestoreFailedHandler;
		InAppPurchasing.PurchaseDeferred -= PurchaseDeferredHandler;
		InAppPurchasing.PromotionalPurchaseIntercepted -= PromotionalPurchaseInterceptedHandler;
		InAppPurchasing.InitializeSucceeded -= IAPInitializedHander;
	}


	public void RemoveAds(){
		if(InAppPurchasing.IsInitialized()){
			ScreenManager.screenManager.loadingSpinner.SetActive(true);
			InAppPurchasing.Purchase(EM_IAPConstants.Product_Remove_Ads);
		} else {
			NativeUI.Alert("Bummer.","We are unable to complete the transaction right now. Please check your network connection and try again later.");
		}
	}
	public bool checkNoAds(){
		if(InAppPurchasing.IsInitialized()){
			bool isOwned = InAppPurchasing.IsProductOwned(EM_IAPConstants.Product_Remove_Ads);
			return isOwned;
		}
		else {
			return false;
		}
	}
	public void RestorePurchases(){
		if(InAppPurchasing.IsInitialized()){
			ScreenManager.screenManager.loadingSpinner.SetActive(true);
			InAppPurchasing.RestorePurchases();
		} else {
			NativeUI.Alert("Bummer.","We are unable to restore you purchases right now. Please check your network connection and try again later.");
		}
	}
	void PurchaseCompletedHandler(IAPProduct product)
	{
		ScreenManager.screenManager.loadingSpinner.SetActive(false);
		// Compare product name to the generated name constants to determine which product was bought
		switch (product.Name)
		{
			case EM_IAPConstants.Product_Remove_Ads:
				// NativeUI.Alert("Success!","You have removed ads. Thanks for your support!");
				noAds = true;
				break;
		}
	}

	// Failed purchase handler
	void PurchaseFailedHandler(IAPProduct product)
	{
		ScreenManager.screenManager.loadingSpinner.SetActive(false);
		NativeUI.Alert("Bummer.","Something went wrong during your purchase. Sorry! Please try again later.");
	}
	void RestoreCompletedHandler()
	{
		ScreenManager.screenManager.loadingSpinner.SetActive(false);
		NativeUI.Alert("Success!","Purchases restored. Thank you!");
		noAds = true;
	}
	void IAPInitializedHander (){
		noAds = checkNoAds();
	}
	// Failed restoration handler
	void RestoreFailedHandler()
	{
		ScreenManager.screenManager.loadingSpinner.SetActive(false);
		NativeUI.Alert("Bummer.","Failed to resore purchases. Sorry! Please try again later.");
	}
	void PurchaseDeferredHandler(IAPProduct product)
	{
		ScreenManager.screenManager.loadingSpinner.SetActive(false);
		// NativeUI.Alert("Holding...","Purchase on hold. Once it is approved, we'll let you know here.");
		// Perform necessary actions, e.g. updating UI to inform user that
		// the purchase has been deferred...
	}
	void PromotionalPurchaseInterceptedHandler(IAPProduct product)
	{
		ScreenManager.screenManager.loadingSpinner.SetActive(false);
		Debug.Log("Promotional purchase of product " + product.Name + " has been intercepted.");

		// Here you can perform necessary actions, e.g. presenting parental gates, 
		// sending analytics events, etc.


		// Finally, you must call the ContinueApplePromotionalPurchases method
		// to continue the normal processing of the purchase!
		InAppPurchasing.ContinueApplePromotionalPurchases();
	}
}
