﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour {
	public static ScreenManager screenManager;
	public GameObject loadingSpinner;
	void Awake(){
		if(screenManager == null){
			DontDestroyOnLoad(gameObject);
			screenManager = this;
		} else if (screenManager != this){
			Destroy(gameObject);
		}
		Application.targetFrameRate = 60;
	}
	void OnEnable(){
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	public void LoadGameScene(string sceneName){
		SceneManager.LoadScene(sceneName);
		loadingSpinner.gameObject.SetActive(true);
	}

	public void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		loadingSpinner.gameObject.SetActive(false);
	}
}