﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour {
	private Animator animator;
	public bool active;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	public void SetActive(bool b){
		active = b;
		animator.SetBool("Active",active);
	}
	public void ToggleActive(){
		SetActive(!active);
	}
	public void SetGameManagerMusic(){
		MusicManager.musicManager.SetMusicActive(active);
		MusicManager.musicManager.musicOn = active;
	}
	public void SetGameManagerSFX(){
		MusicManager.musicManager.sfxOn = active;
	}
}
