﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparksSound : MonoBehaviour {

	void Awake()
	{
		if(MusicManager.musicManager.sfxOn){
			GetComponent<AudioSource>().Play();
		}	
	}
}
