﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRubyShared;
using UnityEngine.SceneManagement;

public class TouchHandler : MonoBehaviour {


	private TapGestureRecognizer tap;
	Player player;
	bool tapEnd;
	void OnEnable(){
		SceneManager.sceneLoaded += OnSceneLoaded;
	}
	public void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		CreateTapGesture();
		player = FindObjectOfType<Player>();
		tapEnd = false;
	}

	private void TapGestureCallback(GestureRecognizer gesture){
		if (gesture.State == GestureRecognizerState.Possible)
		{
			if (tapEnd == false){
				if(player){
					player.Flare();
					player.AddJump();
				}
			} else {
				tapEnd = false;
			}
		}
		if (gesture.State == GestureRecognizerState.Ended || gesture.State == GestureRecognizerState.Failed){
			tapEnd = true;
		}
	}

	private void CreateTapGesture(){
		tap = new TapGestureRecognizer();
		tap.StateUpdated += TapGestureCallback;
		FingersScript.Instance.AddGesture(tap);
	}
}
