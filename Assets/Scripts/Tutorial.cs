﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {
	private int currentSlideIndex;
	public GameObject[] tutorialSlides;
	public GameObject nextButton;
	public GameObject skipButton;
	void Start(){
		currentSlideIndex = 0;
	}
	public void AdvanceTutorial(){
		if(currentSlideIndex == tutorialSlides.Length - 1){
			FinishTutorial();
			return;
		}
		tutorialSlides[currentSlideIndex].SetActive(false);
		currentSlideIndex++;
		tutorialSlides[currentSlideIndex].SetActive(true);
		if(currentSlideIndex == tutorialSlides.Length - 1){
			nextButton.GetComponentInChildren<Text>().text = "Finish";
			skipButton.SetActive(false);
			Vector3 curPos = nextButton.GetComponent<RectTransform>().anchoredPosition;
			nextButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, curPos.y, curPos.z);
		}
	}
	public void ShowTutorial(){
		gameObject.SetActive(true);
	}
	public void FinishTutorial(){
		//Send to Unity Analytics
		HideTutorial();
	}
	public void SkipTutorial(){
		//Send to Unity Analytics
		HideTutorial();
	}
	private void HideTutorial(){
		tutorialSlides[currentSlideIndex].SetActive(false);
		currentSlideIndex = 0;
		tutorialSlides[currentSlideIndex].SetActive(true);
		gameObject.SetActive(false);
		GameManager.gameManager.player.hasSeenTutorial = true;
	}

}
