﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour {
	// public static TutorialManager tutorialManager;
	private bool viewedTutorial;
	[SerializeField]
	private Tutorial tutorial;
	// void Awake(){
	// 	if(tutorialManager == null){
	// 		tutorialManager = this;
	// 		DontDestroyOnLoad(gameObject);
	// 	} else if (tutorialManager != this){
	// 		Destroy(gameObject);
	// 	}
	// }
	void Start(){
		if (!GameManager.gameManager.player.hasSeenTutorial){
			ViewTutorial();
		}
		// this.GetComponent<Animation>().Play ();
	}
	public void ViewTutorial(){
		tutorial.gameObject.SetActive(true);
	}

}
