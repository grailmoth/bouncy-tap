�A                      	   _EMISSION      _ADDITIONAL_LIGHTS     _VERTEX_LIGHTS     _SHADOWS_ENABLED   _LOCAL_SHADOWS_ENABLED  �$  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

struct _PerCamera_Type
{
    float4 _MainLightPosition;
    half4 _MainLightColor;
    float4 hlslcc_mtx4x4_WorldToLight[4];
    half4 _AdditionalLightCount;
    float4 _AdditionalLightPosition[16];
    half4 _AdditionalLightColor[16];
    half4 _AdditionalLightDistanceAttenuation[16];
    half4 _AdditionalLightSpotDir[16];
    half4 _AdditionalLightSpotAttenuation[16];
    float4 _ScaledScreenParams;
};

struct UnityPerCamera_Type
{
    float4 _Time;
    float4 _SinTime;
    float4 _CosTime;
    float4 unity_DeltaTime;
    float3 _WorldSpaceCameraPos;
    float4 _ProjectionParams;
    float4 _ScreenParams;
    float4 _ZBufferParams;
    float4 unity_OrthoParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    half4 unity_LODFade;
    float4 unity_WorldTransformParams;
    half4 unity_LightIndicesOffsetAndCount;
    half4 unity_4LightIndices0;
    half4 unity_4LightIndices1;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_Lightmap_HDR;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
    half4 unity_ProbesOcclusion;
};

struct UnityPerFrame_Type
{
    half4 glstate_lightmodel_ambient;
    half4 unity_AmbientSky;
    half4 unity_AmbientEquator;
    half4 unity_AmbientGround;
    half4 unity_IndirectSpecColor;
    float4 unity_FogParams;
    half4 unity_FogColor;
    float4 hlslcc_mtx4x4glstate_matrix_projection[4];
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    float4 hlslcc_mtx4x4unity_MatrixInvV[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float4 unity_StereoScaleOffset;
    int unity_StereoEyeIndex;
    half4 unity_ShadowColor;
};

struct UnityPerMaterial_Type
{
    float4 _MainTex_ST;
    half4 _Color;
    half4 _SpecColor;
    half4 _EmissionColor;
    half _Cutoff;
    half _Glossiness;
    half _GlossMapScale;
    half _Metallic;
    half _BumpScale;
    half _OcclusionStrength;
};

struct _DirectionalShadowBuffer_Type
{
    float4 hlslcc_mtx4x4_WorldToShadow[4];
    float4 _DirShadowSplitSpheres0;
    float4 _DirShadowSplitSpheres1;
    float4 _DirShadowSplitSpheres2;
    float4 _DirShadowSplitSpheres3;
    float4 _DirShadowSplitSphereRadii;
    half4 _ShadowOffset0;
    half4 _ShadowOffset1;
    half4 _ShadowOffset2;
    half4 _ShadowOffset3;
    half4 _ShadowData;
    float4 _ShadowmapSize;
};

struct Mtl_VertexIn
{
    float4 POSITION0 [[ attribute(0) ]] ;
    float3 NORMAL0 [[ attribute(1) ]] ;
    float2 TEXCOORD0 [[ attribute(2) ]] ;
};

struct Mtl_VertexOut
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]];
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]];
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]];
    half3 TEXCOORD3 [[ user(TEXCOORD3) ]];
    half3 TEXCOORD4 [[ user(TEXCOORD4) ]];
    half4 TEXCOORD6 [[ user(TEXCOORD6) ]];
    float4 TEXCOORD7 [[ user(TEXCOORD7) ]];
    float4 mtl_Position [[ position ]];
};

vertex Mtl_VertexOut xlatMtlMain(
    constant _PerCamera_Type& _PerCamera [[ buffer(0) ]],
    constant UnityPerCamera_Type& UnityPerCamera [[ buffer(1) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(2) ]],
    constant UnityPerFrame_Type& UnityPerFrame [[ buffer(3) ]],
    constant UnityPerMaterial_Type& UnityPerMaterial [[ buffer(4) ]],
    constant _DirectionalShadowBuffer_Type& _DirectionalShadowBuffer [[ buffer(5) ]],
    Mtl_VertexIn input [[ stage_in ]])
{
    Mtl_VertexOut output;
    float3 u_xlat0;
    float4 u_xlat1;
    half4 u_xlat16_1;
    float4 u_xlat2;
    half3 u_xlat16_2;
    half4 u_xlat16_3;
    half3 u_xlat16_4;
    int u_xlati5;
    float3 u_xlat6;
    int u_xlati12;
    int u_xlati19;
    bool u_xlatb19;
    int u_xlati21;
    half u_xlat16_23;
    float u_xlat26;
    float u_xlat27;
    output.TEXCOORD0.xy = fma(input.TEXCOORD0.xy, UnityPerMaterial._MainTex_ST.xy, UnityPerMaterial._MainTex_ST.zw);
    u_xlat0.xyz = input.POSITION0.yyy * UnityPerDraw.hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = fma(UnityPerDraw.hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, input.POSITION0.xxx, u_xlat0.xyz);
    u_xlat0.xyz = fma(UnityPerDraw.hlslcc_mtx4x4unity_ObjectToWorld[2].xyz, input.POSITION0.zzz, u_xlat0.xyz);
    u_xlat0.xyz = u_xlat0.xyz + UnityPerDraw.hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * UnityPerFrame.hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = fma(UnityPerFrame.hlslcc_mtx4x4unity_MatrixVP[0], u_xlat0.xxxx, u_xlat1);
    u_xlat1 = fma(UnityPerFrame.hlslcc_mtx4x4unity_MatrixVP[2], u_xlat0.zzzz, u_xlat1);
    output.mtl_Position = u_xlat1 + UnityPerFrame.hlslcc_mtx4x4unity_MatrixVP[3];
    u_xlat1.xyz = (-u_xlat0.xyz) + UnityPerCamera._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat16_2.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16_2.x = half(max(float(u_xlat16_2.x), 6.10351562e-05));
    u_xlat16_2.x = rsqrt(u_xlat16_2.x);
    output.TEXCOORD4.xyz = half3(u_xlat1.xyz * float3(u_xlat16_2.xxx));
    u_xlat16_2.x = dot(input.NORMAL0.xyz, UnityPerDraw.hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat16_2.y = dot(input.NORMAL0.xyz, UnityPerDraw.hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat16_2.z = dot(input.NORMAL0.xyz, UnityPerDraw.hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat16_23 = dot(u_xlat16_2.xyz, u_xlat16_2.xyz);
    u_xlat16_23 = rsqrt(u_xlat16_23);
    u_xlat16_1.xyz = half3(u_xlat16_23) * u_xlat16_2.xyz;
    u_xlat16_1.w = half(1.0);
    u_xlat16_2.x = dot(UnityPerDraw.unity_SHAr, u_xlat16_1);
    u_xlat16_2.y = dot(UnityPerDraw.unity_SHAg, u_xlat16_1);
    u_xlat16_2.z = dot(UnityPerDraw.unity_SHAb, u_xlat16_1);
    u_xlat16_3 = u_xlat16_1.yzzx * u_xlat16_1.xyzz;
    u_xlat16_4.x = dot(UnityPerDraw.unity_SHBr, u_xlat16_3);
    u_xlat16_4.y = dot(UnityPerDraw.unity_SHBg, u_xlat16_3);
    u_xlat16_4.z = dot(UnityPerDraw.unity_SHBb, u_xlat16_3);
    u_xlat16_23 = u_xlat16_1.y * u_xlat16_1.y;
    u_xlat16_23 = fma(u_xlat16_1.x, u_xlat16_1.x, (-u_xlat16_23));
    u_xlat16_3.xyz = fma(UnityPerDraw.unity_SHC.xyz, half3(u_xlat16_23), u_xlat16_4.xyz);
    u_xlat16_2.xyz = u_xlat16_2.xyz + u_xlat16_3.xyz;
    output.TEXCOORD1.xyz = max(u_xlat16_2.xyz, half3(0.0, 0.0, 0.0));
    u_xlati21 = int(float(_PerCamera._AdditionalLightCount.x));
    u_xlat16_2.x = min(_PerCamera._AdditionalLightCount.y, UnityPerDraw.unity_LightIndicesOffsetAndCount.y);
    u_xlati5 = int(float(u_xlat16_2.x));
    u_xlat16_2.x = half(0.0);
    u_xlat16_2.y = half(0.0);
    u_xlat16_2.z = half(0.0);
    u_xlati12 = u_xlati21;
    while(true){
        u_xlatb19 = u_xlati12>=u_xlati5;
        if(u_xlatb19){break;}
        u_xlat16_23 = half(u_xlati12);
        u_xlatb19 = u_xlat16_23<half(2.0);
        u_xlat26 = float(u_xlat16_23) + -2.0;
        u_xlat26 = (u_xlatb19) ? float(u_xlat16_23) : u_xlat26;
        u_xlat16_3.xy = (bool(u_xlatb19)) ? UnityPerDraw.unity_4LightIndices0.xy : UnityPerDraw.unity_4LightIndices0.zw;
        u_xlatb19 = u_xlat26<1.0;
        u_xlat16_23 = (u_xlatb19) ? u_xlat16_3.x : u_xlat16_3.y;
        u_xlati19 = int(float(u_xlat16_23));
        u_xlat6.xyz = fma((-u_xlat0.xyz), _PerCamera._AdditionalLightPosition[u_xlati19].www, _PerCamera._AdditionalLightPosition[u_xlati19].xyz);
        u_xlat26 = dot(u_xlat6.xyz, u_xlat6.xyz);
        u_xlat26 = max(u_xlat26, 1.17549435e-38);
        u_xlat27 = rsqrt(u_xlat26);
        u_xlat6.xyz = float3(u_xlat27) * u_xlat6.xyz;
        u_xlat27 = fma(u_xlat26, float(_PerCamera._AdditionalLightDistanceAttenuation[u_xlati19].x), 1.0);
        u_xlat27 = float(1.0) / u_xlat27;
        u_xlat16_23 = half(fma(u_xlat26, float(_PerCamera._AdditionalLightDistanceAttenuation[u_xlati19].y), float(_PerCamera._AdditionalLightDistanceAttenuation[u_xlati19].z)));
        u_xlat16_23 = clamp(u_xlat16_23, 0.0h, 1.0h);
        u_xlat16_23 = half(float(u_xlat16_23) * u_xlat27);
        u_xlat16_3.x = dot(float3(_PerCamera._AdditionalLightSpotDir[u_xlati19].xyz), u_xlat6.xyz);
        u_xlat16_3.x = fma(u_xlat16_3.x, _PerCamera._AdditionalLightSpotAttenuation[u_xlati19].x, _PerCamera._AdditionalLightSpotAttenuation[u_xlati19].y);
        u_xlat16_3.x = clamp(u_xlat16_3.x, 0.0h, 1.0h);
        u_xlat16_3.x = u_xlat16_3.x * u_xlat16_3.x;
        u_xlat16_23 = u_xlat16_23 * u_xlat16_3.x;
        u_xlat16_3.xyz = half3(u_xlat16_23) * _PerCamera._AdditionalLightColor[u_xlati19].xyz;
        u_xlat16_23 = dot(float3(u_xlat16_1.xyz), u_xlat6.xyz);
        u_xlat16_23 = clamp(u_xlat16_23, 0.0h, 1.0h);
        u_xlat16_2.xyz = fma(u_xlat16_3.xyz, half3(u_xlat16_23), u_xlat16_2.xyz);
        u_xlati12 = u_xlati12 + 0x1;
    }
    output.TEXCOORD6.yzw = u_xlat16_2.xyz;
    u_xlat2 = u_xlat0.yyyy * _DirectionalShadowBuffer.hlslcc_mtx4x4_WorldToShadow[1];
    u_xlat2 = fma(_DirectionalShadowBuffer.hlslcc_mtx4x4_WorldToShadow[0], u_xlat0.xxxx, u_xlat2);
    u_xlat2 = fma(_DirectionalShadowBuffer.hlslcc_mtx4x4_WorldToShadow[2], u_xlat0.zzzz, u_xlat2);
    output.TEXCOORD7 = u_xlat2 + _DirectionalShadowBuffer.hlslcc_mtx4x4_WorldToShadow[3];
    output.TEXCOORD2.xyz = u_xlat0.xyz;
    output.TEXCOORD6.x = half(0.0);
    output.TEXCOORD3.xyz = u_xlat16_1.xyz;
    return output;
}
                                               
   _PerCamera  �        _AdditionalLightCount                    `      _AdditionalLightPosition                 p      _AdditionalLightColor                   p  #   _AdditionalLightDistanceAttenuation                 �     _AdditionalLightSpotDir                 p     _AdditionalLightSpotAttenuation                 �         UnityPerCamera  �         _WorldSpaceCameraPos                  @          UnityPerDraw(         unity_LightIndicesOffsetAndCount                 �      unity_4LightIndices0                 �   
   unity_SHAr                   �   
   unity_SHAg                   �   
   unity_SHAb                   �   
   unity_SHBr                      
   unity_SHBg                     
   unity_SHBb                     	   unity_SHC                         unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerFrame   p        unity_MatrixVP                            UnityPerMaterial4         _MainTex_ST                              _DirectionalShadowBuffer�         _WorldToShadow                              
   _PerCamera                UnityPerCamera               UnityPerDraw             UnityPerFrame                UnityPerMaterial             _DirectionalShadowBuffer          