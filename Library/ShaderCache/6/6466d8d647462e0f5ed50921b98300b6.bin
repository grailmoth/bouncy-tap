�A                      	   _EMISSION      _ADDITIONAL_LIGHTS     _MIXED_LIGHTING_SUBTRACTIVE    _SHADOWS_ENABLED   _SHADOWS_SOFT   �3  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct _PerCamera_Type
{
    float4 _MainLightPosition;
    half4 _MainLightColor;
    float4 hlslcc_mtx4x4_WorldToLight[4];
    half4 _AdditionalLightCount;
    float4 _AdditionalLightPosition[16];
    half4 _AdditionalLightColor[16];
    half4 _AdditionalLightDistanceAttenuation[16];
    half4 _AdditionalLightSpotDir[16];
    half4 _AdditionalLightSpotAttenuation[16];
    float4 _ScaledScreenParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    half4 unity_LODFade;
    float4 unity_WorldTransformParams;
    half4 unity_LightIndicesOffsetAndCount;
    half4 unity_4LightIndices0;
    half4 unity_4LightIndices1;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_Lightmap_HDR;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
    half4 unity_ProbesOcclusion;
};

struct UnityPerMaterial_Type
{
    float4 _MainTex_ST;
    half4 _Color;
    half4 _SpecColor;
    half4 _EmissionColor;
    half _Cutoff;
    half _Glossiness;
    half _GlossMapScale;
    half _Metallic;
    half _BumpScale;
    half _OcclusionStrength;
};

struct _DirectionalShadowBuffer_Type
{
    float4 hlslcc_mtx4x4_WorldToShadow[4];
    float4 _DirShadowSplitSpheres0;
    float4 _DirShadowSplitSpheres1;
    float4 _DirShadowSplitSpheres2;
    float4 _DirShadowSplitSpheres3;
    float4 _DirShadowSplitSphereRadii;
    half4 _ShadowOffset0;
    half4 _ShadowOffset1;
    half4 _ShadowOffset2;
    half4 _ShadowOffset3;
    half4 _ShadowData;
    float4 _ShadowmapSize;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half4 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
    float4 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant _PerCamera_Type& _PerCamera [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    constant UnityPerMaterial_Type& UnityPerMaterial [[ buffer(2) ]],
    constant _DirectionalShadowBuffer_Type& _DirectionalShadowBuffer [[ buffer(3) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_MainTex [[ sampler (1) ]],
    sampler sampler_EmissionMap [[ sampler (2) ]],
    sampler sampler_DirectionalShadowmapTexture [[ sampler (3) ]],
    texturecube<half, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    texture2d<half, access::sample > _EmissionMap [[ texture(2) ]] ,
    depth2d<float, access::sample > _DirectionalShadowmapTexture [[ texture(3) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half4 u_xlat16_0;
    half3 u_xlat16_1;
    half3 u_xlat16_2;
    half u_xlat16_3;
    half4 u_xlat16_4;
    float4 u_xlat5;
    half4 u_xlat16_5;
    float3 u_xlat6;
    half3 u_xlat16_7;
    half3 u_xlat16_8;
    float u_xlat9;
    half3 u_xlat16_10;
    half u_xlat16_11;
    half3 u_xlat16_14;
    half3 u_xlat16_15;
    float3 u_xlat17;
    half u_xlat16_22;
    int u_xlati22;
    bool u_xlatb22;
    half u_xlat16_26;
    int u_xlati33;
    half u_xlat16_34;
    half u_xlat16_35;
    int u_xlati35;
    bool u_xlatb35;
    half u_xlat16_37;
    float u_xlat38;
    half u_xlat16_40;
    u_xlat16_0 = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy);
    output.SV_Target0.w = u_xlat16_0.w * UnityPerMaterial._Color.w;
    u_xlat16_1.xyz = u_xlat16_0.xyz * UnityPerMaterial._Color.xyz;
    u_xlat16_2.xyz = _EmissionMap.sample(sampler_EmissionMap, input.TEXCOORD0.xy).xyz;
    u_xlat16_34 = fma((-UnityPerMaterial._Metallic), half(0.959999979), half(0.959999979));
    u_xlat16_3 = (-u_xlat16_34) + UnityPerMaterial._Glossiness;
    u_xlat16_1.xyz = half3(u_xlat16_34) * u_xlat16_1.xyz;
    u_xlat16_14.xyz = fma(u_xlat16_0.xyz, UnityPerMaterial._Color.xyz, half3(-0.0399999991, -0.0399999991, -0.0399999991));
    u_xlat16_14.xyz = fma(half3(UnityPerMaterial._Metallic), u_xlat16_14.xyz, half3(0.0399999991, 0.0399999991, 0.0399999991));
    u_xlat16_34 = u_xlat16_3 + half(1.0);
    u_xlat16_34 = clamp(u_xlat16_34, 0.0h, 1.0h);
    u_xlat16_3 = (-UnityPerMaterial._Glossiness) + half(1.0);
    u_xlat16_4.x = u_xlat16_3 * u_xlat16_3;
    u_xlat16_0.x = fma(u_xlat16_4.x, half(4.0), half(2.0));
    u_xlat16_11 = fma(u_xlat16_4.x, u_xlat16_4.x, half(-1.0));
    u_xlat5.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset0.xyz);
    u_xlat5.x = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat5.xy, saturate(u_xlat5.z), level(0.0)));
    u_xlat6.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset1.xyz);
    u_xlat5.y = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat6.xy, saturate(u_xlat6.z), level(0.0)));
    u_xlat6.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset2.xyz);
    u_xlat5.z = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat6.xy, saturate(u_xlat6.z), level(0.0)));
    u_xlat6.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset3.xyz);
    u_xlat5.w = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat6.xy, saturate(u_xlat6.z), level(0.0)));
    u_xlat16_26 = half(dot(u_xlat5, float4(0.25, 0.25, 0.25, 0.25)));
    u_xlat16_37 = (-_DirectionalShadowBuffer._ShadowData.x) + half(1.0);
    u_xlat16_26 = fma(u_xlat16_26, _DirectionalShadowBuffer._ShadowData.x, u_xlat16_37);
    u_xlatb22 = 0.0>=input.TEXCOORD7.z;
    u_xlat16_26 = (u_xlatb22) ? half(1.0) : u_xlat16_26;
    u_xlat16_37 = dot((-input.TEXCOORD4.xyz), input.TEXCOORD3.xyz);
    u_xlat16_37 = u_xlat16_37 + u_xlat16_37;
    u_xlat16_7.xyz = fma(input.TEXCOORD3.xyz, (-half3(u_xlat16_37)), (-input.TEXCOORD4.xyz));
    u_xlat16_37 = dot(input.TEXCOORD3.xyz, input.TEXCOORD4.xyz);
    u_xlat16_37 = clamp(u_xlat16_37, 0.0h, 1.0h);
    u_xlat16_4.w = (-u_xlat16_37) + half(1.0);
    u_xlat16_15.xz = u_xlat16_4.xw * u_xlat16_4.xw;
    u_xlat16_37 = u_xlat16_15.z * u_xlat16_15.z;
    u_xlat16_40 = fma((-u_xlat16_3), half(0.699999988), half(1.70000005));
    u_xlat16_3 = u_xlat16_3 * u_xlat16_40;
    u_xlat16_3 = u_xlat16_3 * half(6.0);
    u_xlat16_5 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_7.xyz), level(float(u_xlat16_3)));
    u_xlat16_3 = u_xlat16_5.w + half(-1.0);
    u_xlat16_3 = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat16_3, half(1.0));
    u_xlat16_3 = max(u_xlat16_3, half(0.0));
    u_xlat16_3 = log2(u_xlat16_3);
    u_xlat16_3 = u_xlat16_3 * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat16_3 = exp2(u_xlat16_3);
    u_xlat16_3 = u_xlat16_3 * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat16_7.xyz = u_xlat16_5.xyz * half3(u_xlat16_3);
    u_xlat16_3 = fma(u_xlat16_4.x, u_xlat16_4.x, half(1.0));
    u_xlat16_3 = half(1.0) / u_xlat16_3;
    u_xlat16_5.xyz = u_xlat16_7.xyz * half3(u_xlat16_3);
    u_xlat16_7.xyz = (-u_xlat16_14.xyz) + half3(u_xlat16_34);
    u_xlat16_7.xyz = fma(half3(u_xlat16_37), u_xlat16_7.xyz, u_xlat16_14.xyz);
    u_xlat16_5.xyz = u_xlat16_5.xyz * u_xlat16_7.xyz;
    u_xlat16_5.xyz = fma(input.TEXCOORD1.xyz, u_xlat16_1.xyz, u_xlat16_5.xyz);
    u_xlat16_34 = dot(float3(input.TEXCOORD3.xyz), _PerCamera._MainLightPosition.xyz);
    u_xlat16_34 = clamp(u_xlat16_34, 0.0h, 1.0h);
    u_xlat16_34 = u_xlat16_34 * u_xlat16_26;
    u_xlat16_4.xzw = half3(u_xlat16_34) * _PerCamera._MainLightColor.xyz;
    u_xlat16_7.xyz = half3(float3(input.TEXCOORD4.xyz) + _PerCamera._MainLightPosition.xyz);
    u_xlat16_34 = dot(u_xlat16_7.xyz, u_xlat16_7.xyz);
    u_xlat16_34 = half(max(float(u_xlat16_34), 6.10351562e-05));
    u_xlat16_34 = rsqrt(u_xlat16_34);
    u_xlat16_7.xyz = half3(u_xlat16_34) * u_xlat16_7.xyz;
    u_xlat16_34 = dot(input.TEXCOORD3.xyz, u_xlat16_7.xyz);
    u_xlat16_34 = clamp(u_xlat16_34, 0.0h, 1.0h);
    u_xlat16_3 = dot(_PerCamera._MainLightPosition.xyz, float3(u_xlat16_7.xyz));
    u_xlat16_3 = clamp(u_xlat16_3, 0.0h, 1.0h);
    u_xlat16_34 = u_xlat16_34 * u_xlat16_34;
    u_xlat16_22 = fma(u_xlat16_34, u_xlat16_11, half(1.00001001));
    u_xlat16_34 = u_xlat16_3 * u_xlat16_3;
    u_xlat16_3 = u_xlat16_22 * u_xlat16_22;
    u_xlat16_22 = max(u_xlat16_34, half(0.100000001));
    u_xlat16_22 = u_xlat16_22 * u_xlat16_3;
    u_xlat16_22 = u_xlat16_0.x * u_xlat16_22;
    u_xlat16_22 = u_xlat16_15.x / u_xlat16_22;
    u_xlat16_34 = half(float(u_xlat16_22) + -6.10351562e-05);
    u_xlat16_34 = max(u_xlat16_34, half(0.0));
    u_xlat16_34 = min(u_xlat16_34, half(100.0));
    u_xlat16_7.xyz = fma(half3(u_xlat16_34), u_xlat16_14.xyz, u_xlat16_1.xyz);
    u_xlat16_4.xzw = fma(u_xlat16_7.xyz, u_xlat16_4.xzw, u_xlat16_5.xyz);
    u_xlat16_34 = min(_PerCamera._AdditionalLightCount.x, UnityPerDraw.unity_LightIndicesOffsetAndCount.y);
    u_xlati22 = int(float(u_xlat16_34));
    u_xlat16_7.xyz = u_xlat16_4.xzw;
    u_xlati33 = 0x0;
    while(true){
        u_xlatb35 = u_xlati33>=u_xlati22;
        if(u_xlatb35){break;}
        u_xlat16_34 = half(u_xlati33);
        u_xlatb35 = u_xlat16_34<half(2.0);
        u_xlat5.x = float(u_xlat16_34) + -2.0;
        u_xlat5.x = (u_xlatb35) ? float(u_xlat16_34) : u_xlat5.x;
        u_xlat16_8.xy = (bool(u_xlatb35)) ? UnityPerDraw.unity_4LightIndices0.xy : UnityPerDraw.unity_4LightIndices0.zw;
        u_xlatb35 = u_xlat5.x<1.0;
        u_xlat16_34 = (u_xlatb35) ? u_xlat16_8.x : u_xlat16_8.y;
        u_xlati35 = int(float(u_xlat16_34));
        u_xlat5.xyz = fma((-input.TEXCOORD2.xyz), _PerCamera._AdditionalLightPosition[u_xlati35].www, _PerCamera._AdditionalLightPosition[u_xlati35].xyz);
        u_xlat38 = dot(u_xlat5.xyz, u_xlat5.xyz);
        u_xlat38 = max(u_xlat38, 1.17549435e-38);
        u_xlat6.x = rsqrt(u_xlat38);
        u_xlat17.xyz = u_xlat5.xyz * u_xlat6.xxx;
        u_xlat9 = fma(u_xlat38, float(_PerCamera._AdditionalLightDistanceAttenuation[u_xlati35].x), 1.0);
        u_xlat9 = float(1.0) / u_xlat9;
        u_xlat16_34 = half(fma(u_xlat38, float(_PerCamera._AdditionalLightDistanceAttenuation[u_xlati35].y), float(_PerCamera._AdditionalLightDistanceAttenuation[u_xlati35].z)));
        u_xlat16_34 = clamp(u_xlat16_34, 0.0h, 1.0h);
        u_xlat16_34 = half(float(u_xlat16_34) * u_xlat9);
        u_xlat16_3 = dot(float3(_PerCamera._AdditionalLightSpotDir[u_xlati35].xyz), u_xlat17.xyz);
        u_xlat16_3 = fma(u_xlat16_3, _PerCamera._AdditionalLightSpotAttenuation[u_xlati35].x, _PerCamera._AdditionalLightSpotAttenuation[u_xlati35].y);
        u_xlat16_3 = clamp(u_xlat16_3, 0.0h, 1.0h);
        u_xlat16_3 = u_xlat16_3 * u_xlat16_3;
        u_xlat16_34 = u_xlat16_34 * u_xlat16_3;
        u_xlat16_3 = dot(float3(input.TEXCOORD3.xyz), u_xlat17.xyz);
        u_xlat16_3 = clamp(u_xlat16_3, 0.0h, 1.0h);
        u_xlat16_34 = u_xlat16_34 * u_xlat16_3;
        u_xlat16_8.xyz = half3(u_xlat16_34) * _PerCamera._AdditionalLightColor[u_xlati35].xyz;
        u_xlat16_10.xyz = half3(fma(u_xlat5.xyz, u_xlat6.xxx, float3(input.TEXCOORD4.xyz)));
        u_xlat16_34 = dot(u_xlat16_10.xyz, u_xlat16_10.xyz);
        u_xlat16_34 = half(max(float(u_xlat16_34), 6.10351562e-05));
        u_xlat16_34 = rsqrt(u_xlat16_34);
        u_xlat16_10.xyz = half3(u_xlat16_34) * u_xlat16_10.xyz;
        u_xlat16_34 = dot(input.TEXCOORD3.xyz, u_xlat16_10.xyz);
        u_xlat16_34 = clamp(u_xlat16_34, 0.0h, 1.0h);
        u_xlat16_3 = dot(u_xlat17.xyz, float3(u_xlat16_10.xyz));
        u_xlat16_3 = clamp(u_xlat16_3, 0.0h, 1.0h);
        u_xlat16_34 = u_xlat16_34 * u_xlat16_34;
        u_xlat16_35 = fma(u_xlat16_34, u_xlat16_11, half(1.00001001));
        u_xlat16_34 = u_xlat16_3 * u_xlat16_3;
        u_xlat16_3 = u_xlat16_35 * u_xlat16_35;
        u_xlat16_35 = max(u_xlat16_34, half(0.100000001));
        u_xlat16_35 = u_xlat16_35 * u_xlat16_3;
        u_xlat16_35 = u_xlat16_0.x * u_xlat16_35;
        u_xlat16_35 = u_xlat16_15.x / u_xlat16_35;
        u_xlat16_34 = half(float(u_xlat16_35) + -6.10351562e-05);
        u_xlat16_34 = max(u_xlat16_34, half(0.0));
        u_xlat16_34 = min(u_xlat16_34, half(100.0));
        u_xlat16_10.xyz = fma(half3(u_xlat16_34), u_xlat16_14.xyz, u_xlat16_1.xyz);
        u_xlat16_7.xyz = fma(u_xlat16_10.xyz, u_xlat16_8.xyz, u_xlat16_7.xyz);
        u_xlati33 = u_xlati33 + 0x1;
    }
    u_xlat16_1.xyz = fma(input.TEXCOORD6.yzw, u_xlat16_1.xyz, u_xlat16_7.xyz);
    output.SV_Target0.xyz = fma(u_xlat16_2.xyz, UnityPerMaterial._EmissionColor.xyz, u_xlat16_1.xyz);
    return output;
}
                            
   _PerCamera  �        _MainLightPosition                           _MainLightColor                        _AdditionalLightCount                    `      _AdditionalLightPosition                 p      _AdditionalLightColor                   p  #   _AdditionalLightDistanceAttenuation                 �     _AdditionalLightSpotDir                 p     _AdditionalLightSpotAttenuation                 �         UnityPerDraw(         unity_LightIndicesOffsetAndCount                 �      unity_4LightIndices0                 �      unity_SpecCube0_HDR                  �          UnityPerMaterial4         _Color                         _EmissionColor                          _Glossiness                  *   	   _Metallic                    .          _DirectionalShadowBuffer�         _ShadowOffset0                   �      _ShadowOffset1                   �      _ShadowOffset2                   �      _ShadowOffset3                   �      _ShadowData                  �             unity_SpecCube0                   _MainTex                _EmissionMap                _DirectionalShadowmapTexture             
   _PerCamera                UnityPerDraw             UnityPerMaterial             _DirectionalShadowBuffer          