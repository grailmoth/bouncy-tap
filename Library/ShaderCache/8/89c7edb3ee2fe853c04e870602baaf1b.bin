�A                         DIRLIGHTMAP_COMBINED   _VERTEX_LIGHTS     _MIXED_LIGHTING_SUBTRACTIVE    _SHADOWS_ENABLED   _SHADOWS_SOFT   �#  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct _PerCamera_Type
{
    float4 _MainLightPosition;
    half4 _MainLightColor;
    float4 hlslcc_mtx4x4_WorldToLight[4];
    half4 _AdditionalLightCount;
    float4 _AdditionalLightPosition[16];
    half4 _AdditionalLightColor[16];
    half4 _AdditionalLightDistanceAttenuation[16];
    half4 _AdditionalLightSpotDir[16];
    half4 _AdditionalLightSpotAttenuation[16];
    float4 _ScaledScreenParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    half4 unity_LODFade;
    float4 unity_WorldTransformParams;
    half4 unity_LightIndicesOffsetAndCount;
    half4 unity_4LightIndices0;
    half4 unity_4LightIndices1;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_Lightmap_HDR;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
    half4 unity_ProbesOcclusion;
};

struct UnityPerMaterial_Type
{
    float4 _MainTex_ST;
    half4 _Color;
    half4 _SpecColor;
    half4 _EmissionColor;
    half _Cutoff;
    half _Glossiness;
    half _GlossMapScale;
    half _Metallic;
    half _BumpScale;
    half _OcclusionStrength;
};

struct _DirectionalShadowBuffer_Type
{
    float4 hlslcc_mtx4x4_WorldToShadow[4];
    float4 _DirShadowSplitSpheres0;
    float4 _DirShadowSplitSpheres1;
    float4 _DirShadowSplitSpheres2;
    float4 _DirShadowSplitSpheres3;
    float4 _DirShadowSplitSphereRadii;
    half4 _ShadowOffset0;
    half4 _ShadowOffset1;
    half4 _ShadowOffset2;
    half4 _ShadowOffset3;
    half4 _ShadowData;
    float4 _ShadowmapSize;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    half3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half4 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
    float4 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant _PerCamera_Type& _PerCamera [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    constant UnityPerMaterial_Type& UnityPerMaterial [[ buffer(2) ]],
    constant _DirectionalShadowBuffer_Type& _DirectionalShadowBuffer [[ buffer(3) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_MainTex [[ sampler (1) ]],
    sampler sampler_DirectionalShadowmapTexture [[ sampler (2) ]],
    texturecube<half, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    depth2d<float, access::sample > _DirectionalShadowmapTexture [[ texture(2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    half4 u_xlat16_0;
    bool u_xlatb0;
    float3 u_xlat1;
    half4 u_xlat16_1;
    half3 u_xlat16_2;
    half4 u_xlat16_3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    half u_xlat16_8;
    half u_xlat16_9;
    half u_xlat16_15;
    half u_xlat16_18;
    half u_xlat16_20;
    half u_xlat16_21;
    half u_xlat16_22;
    u_xlat0.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset0.xyz);
    u_xlat0.x = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat0.xy, saturate(u_xlat0.z), level(0.0)));
    u_xlat1.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset1.xyz);
    u_xlat0.y = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0)));
    u_xlat1.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset2.xyz);
    u_xlat0.z = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0)));
    u_xlat1.xyz = input.TEXCOORD7.xyz + float3(_DirectionalShadowBuffer._ShadowOffset3.xyz);
    u_xlat0.w = float(_DirectionalShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0)));
    u_xlat16_2.x = half(dot(u_xlat0, float4(0.25, 0.25, 0.25, 0.25)));
    u_xlat16_8 = (-_DirectionalShadowBuffer._ShadowData.x) + half(1.0);
    u_xlat16_2.x = fma(u_xlat16_2.x, _DirectionalShadowBuffer._ShadowData.x, u_xlat16_8);
    u_xlatb0 = 0.0>=input.TEXCOORD7.z;
    u_xlat16_2.x = (u_xlatb0) ? half(1.0) : u_xlat16_2.x;
    u_xlat16_8 = dot(float3(input.TEXCOORD3.xyz), _PerCamera._MainLightPosition.xyz);
    u_xlat16_8 = clamp(u_xlat16_8, 0.0h, 1.0h);
    u_xlat16_2.x = u_xlat16_8 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _PerCamera._MainLightColor.xyz;
    u_xlat16_20 = dot((-input.TEXCOORD4.xyz), input.TEXCOORD3.xyz);
    u_xlat16_20 = u_xlat16_20 + u_xlat16_20;
    u_xlat16_3.xyz = fma(input.TEXCOORD3.xyz, (-half3(u_xlat16_20)), (-input.TEXCOORD4.xyz));
    u_xlat16_20 = (-UnityPerMaterial._Glossiness) + half(1.0);
    u_xlat16_21 = fma((-u_xlat16_20), half(0.699999988), half(1.70000005));
    u_xlat16_21 = u_xlat16_20 * u_xlat16_21;
    u_xlat16_20 = u_xlat16_20 * u_xlat16_20;
    u_xlat16_21 = u_xlat16_21 * half(6.0);
    u_xlat16_0 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_3.xyz), level(float(u_xlat16_21)));
    u_xlat16_3.x = u_xlat16_0.w + half(-1.0);
    u_xlat16_3.x = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat16_3.x, half(1.0));
    u_xlat16_3.x = max(u_xlat16_3.x, half(0.0));
    u_xlat16_3.x = log2(u_xlat16_3.x);
    u_xlat16_3.x = u_xlat16_3.x * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat16_3.x = exp2(u_xlat16_3.x);
    u_xlat16_3.x = u_xlat16_3.x * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat16_0.xyz * u_xlat16_3.xxx;
    u_xlat16_21 = fma(u_xlat16_20, u_xlat16_20, half(1.0));
    u_xlat16_21 = half(1.0) / u_xlat16_21;
    u_xlat16_0.xyz = u_xlat16_3.xyz * half3(u_xlat16_21);
    u_xlat16_3.x = dot(input.TEXCOORD3.xyz, input.TEXCOORD4.xyz);
    u_xlat16_3.x = clamp(u_xlat16_3.x, 0.0h, 1.0h);
    u_xlat16_3.x = (-u_xlat16_3.x) + half(1.0);
    u_xlat16_3.x = u_xlat16_3.x * u_xlat16_3.x;
    u_xlat16_3.x = u_xlat16_3.x * u_xlat16_3.x;
    u_xlat16_9 = fma((-UnityPerMaterial._Metallic), half(0.959999979), half(0.959999979));
    u_xlat16_15 = (-u_xlat16_9) + UnityPerMaterial._Glossiness;
    u_xlat16_15 = u_xlat16_15 + half(1.0);
    u_xlat16_15 = clamp(u_xlat16_15, 0.0h, 1.0h);
    u_xlat16_1 = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy);
    u_xlat16_4.xyz = fma(u_xlat16_1.xyz, UnityPerMaterial._Color.xyz, half3(-0.0399999991, -0.0399999991, -0.0399999991));
    u_xlat16_4.xyz = fma(half3(UnityPerMaterial._Metallic), u_xlat16_4.xyz, half3(0.0399999991, 0.0399999991, 0.0399999991));
    u_xlat16_5.xyz = half3(u_xlat16_15) + (-u_xlat16_4.xyz);
    u_xlat16_3.xzw = fma(u_xlat16_3.xxx, u_xlat16_5.xyz, u_xlat16_4.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_3.xzw;
    u_xlat16_3.xzw = u_xlat16_1.xyz * UnityPerMaterial._Color.xyz;
    output.SV_Target0.w = u_xlat16_1.w * UnityPerMaterial._Color.w;
    u_xlat16_3.xyz = half3(u_xlat16_9) * u_xlat16_3.xzw;
    u_xlat16_0.xyz = fma(input.TEXCOORD1.xyz, u_xlat16_3.xyz, u_xlat16_0.xyz);
    u_xlat16_5.xyz = half3(float3(input.TEXCOORD4.xyz) + _PerCamera._MainLightPosition.xyz);
    u_xlat16_21 = dot(u_xlat16_5.xyz, u_xlat16_5.xyz);
    u_xlat16_21 = half(max(float(u_xlat16_21), 6.10351562e-05));
    u_xlat16_21 = rsqrt(u_xlat16_21);
    u_xlat16_5.xyz = half3(u_xlat16_21) * u_xlat16_5.xyz;
    u_xlat16_21 = dot(_PerCamera._MainLightPosition.xyz, float3(u_xlat16_5.xyz));
    u_xlat16_21 = clamp(u_xlat16_21, 0.0h, 1.0h);
    u_xlat16_22 = dot(input.TEXCOORD3.xyz, u_xlat16_5.xyz);
    u_xlat16_22 = clamp(u_xlat16_22, 0.0h, 1.0h);
    u_xlat16_22 = u_xlat16_22 * u_xlat16_22;
    u_xlat16_21 = u_xlat16_21 * u_xlat16_21;
    u_xlat16_18 = max(u_xlat16_21, half(0.100000001));
    u_xlat16_1.x = fma(u_xlat16_20, u_xlat16_20, half(-1.0));
    u_xlat16_1.x = fma(u_xlat16_22, u_xlat16_1.x, half(1.00001001));
    u_xlat16_21 = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat16_18 = u_xlat16_18 * u_xlat16_21;
    u_xlat16_1.x = fma(u_xlat16_20, half(4.0), half(2.0));
    u_xlat16_20 = u_xlat16_20 * u_xlat16_20;
    u_xlat16_18 = u_xlat16_18 * u_xlat16_1.x;
    u_xlat16_18 = u_xlat16_20 / u_xlat16_18;
    u_xlat16_20 = half(float(u_xlat16_18) + -6.10351562e-05);
    u_xlat16_20 = max(u_xlat16_20, half(0.0));
    u_xlat16_20 = min(u_xlat16_20, half(100.0));
    u_xlat16_4.xyz = fma(half3(u_xlat16_20), u_xlat16_4.xyz, u_xlat16_3.xyz);
    u_xlat16_2.xyz = fma(u_xlat16_4.xyz, u_xlat16_2.xyz, u_xlat16_0.xyz);
    output.SV_Target0.xyz = fma(input.TEXCOORD6.yzw, u_xlat16_3.xyz, u_xlat16_2.xyz);
    return output;
}
                            
   _PerCamera  �        _MainLightPosition                           _MainLightColor                            UnityPerDraw(        unity_SpecCube0_HDR                  �          UnityPerMaterial4         _Color                         _Glossiness                  *   	   _Metallic                    .          _DirectionalShadowBuffer�         _ShadowOffset0                   �      _ShadowOffset1                   �      _ShadowOffset2                   �      _ShadowOffset3                   �      _ShadowData                  �             unity_SpecCube0                   _MainTex                _DirectionalShadowmapTexture             
   _PerCamera                UnityPerDraw             UnityPerMaterial             _DirectionalShadowBuffer          