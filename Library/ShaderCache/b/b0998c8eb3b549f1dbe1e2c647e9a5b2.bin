�A                       �  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct _PerCamera_Type
{
    float4 _MainLightPosition;
    float4 _MainLightColor;
    float4 hlslcc_mtx4x4_WorldToLight[4];
    float4 _AdditionalLightCount;
    float4 _AdditionalLightPosition[16];
    float4 _AdditionalLightColor[16];
    float4 _AdditionalLightDistanceAttenuation[16];
    float4 _AdditionalLightSpotDir[16];
    float4 _AdditionalLightSpotAttenuation[16];
    float4 _ScaledScreenParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    float4 unity_WorldTransformParams;
    float4 unity_LightIndicesOffsetAndCount;
    float4 unity_4LightIndices0;
    float4 unity_4LightIndices1;
    float4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    float4 unity_Lightmap_HDR;
    float4 unity_SHAr;
    float4 unity_SHAg;
    float4 unity_SHAb;
    float4 unity_SHBr;
    float4 unity_SHBg;
    float4 unity_SHBb;
    float4 unity_SHC;
    float4 unity_ProbesOcclusion;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
    float4 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant _PerCamera_Type& _PerCamera [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler samplerTexture2D_6D9F0350 [[ sampler (1) ]],
    texturecube<float, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    texture2d<float, access::sample > Texture2D_6D9F0350 [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    bool u_xlatb0;
    float4 u_xlat1;
    float3 u_xlat2;
    float3 u_xlat3;
    float u_xlat9;
    float u_xlat10;
    u_xlat0.x = Texture2D_6D9F0350.sample(samplerTexture2D_6D9F0350, input.TEXCOORD8.xy).w;
    u_xlat3.x = dot((-u_xlat0.xxx), (-u_xlat0.xxx));
    u_xlat3.x = sqrt(u_xlat3.x);
    u_xlat3.x = u_xlat3.x * 100000.0;
    u_xlat3.x = min(u_xlat3.x, 1.0);
    u_xlat0.x = fma((-u_xlat3.x), u_xlat0.x, 1.0);
    u_xlat0.x = -abs(u_xlat0.x) + 0.999899983;
    u_xlatb0 = u_xlat0.x<0.0;
    if((int(u_xlatb0) * int(0xffffffffu))!=0){discard_fragment();}
    u_xlat0.x = dot(input.TEXCOORD7.xyz, input.TEXCOORD7.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * input.TEXCOORD7.xyz;
    u_xlat9 = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat9 = rsqrt(u_xlat9);
    u_xlat1.xyz = float3(u_xlat9) * input.TEXCOORD4.xyz;
    u_xlat9 = dot((-u_xlat0.xyz), u_xlat1.xyz);
    u_xlat9 = u_xlat9 + u_xlat9;
    u_xlat2.xyz = fma(u_xlat1.xyz, (-float3(u_xlat9)), (-u_xlat0.xyz));
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat3.x = dot(u_xlat1.xyz, _PerCamera._MainLightPosition.xyz);
    u_xlat3.x = clamp(u_xlat3.x, 0.0f, 1.0f);
    u_xlat3.xyz = u_xlat3.xxx * _PerCamera._MainLightColor.xyz;
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = fma(u_xlat0.x, 0.959999979, 0.0399999991);
    u_xlat1 = unity_SpecCube0.sample(samplerunity_SpecCube0, u_xlat2.xyz, level(0.0));
    u_xlat10 = u_xlat1.w + -1.0;
    u_xlat10 = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat10, 1.0);
    u_xlat10 = max(u_xlat10, 0.0);
    u_xlat10 = log2(u_xlat10);
    u_xlat10 = u_xlat10 * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat10 = exp2(u_xlat10);
    u_xlat10 = u_xlat10 * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat1.xyz = u_xlat1.xyz * float3(u_xlat10);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat1.xyz = fma(input.TEXCOORD0.xyz, float3(0.479999989, 0.479999989, 0.479999989), u_xlat1.xyz);
    u_xlat0.xyz = fma(u_xlat3.xyz, float3(0.479999989, 0.479999989, 0.479999989), u_xlat1.xyz);
    u_xlat0.xyz = fma(input.TEXCOORD1.yzw, float3(0.479999989, 0.479999989, 0.479999989), u_xlat0.xyz);
    output.SV_Target0.xyz = u_xlat0.xyz + float3(0.988235295, 1.0, 1.0);
    output.SV_Target0.w = 0.999899983;
    return output;
}
                             
   _PerCamera  �        _MainLightPosition                           _MainLightColor                             UnityPerDraw�        unity_SpecCube0_HDR                   �             unity_SpecCube0                   Texture2D_6D9F0350               
   _PerCamera                UnityPerDraw          