﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Sirenix.OdinInspector.AssetListAttribute
struct AssetListAttribute_t3126145472;
// Sirenix.OdinInspector.AssetsOnlyAttribute
struct AssetsOnlyAttribute_t3835058913;
// Sirenix.OdinInspector.BoxGroupAttribute
struct BoxGroupAttribute_t1828532190;
// Sirenix.OdinInspector.ButtonAttribute
struct ButtonAttribute_t3418195108;
// Sirenix.OdinInspector.ButtonGroupAttribute
struct ButtonGroupAttribute_t2757208248;
// Sirenix.OdinInspector.ColorPaletteAttribute
struct ColorPaletteAttribute_t392531492;
// Sirenix.OdinInspector.CustomContextMenuAttribute
struct CustomContextMenuAttribute_t4264451314;
// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct CustomValueDrawerAttribute_t4188368302;
// Sirenix.OdinInspector.DelayedPropertyAttribute
struct DelayedPropertyAttribute_t1141984869;
// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct DetailedInfoBoxAttribute_t2666061416;
// Sirenix.OdinInspector.DictionaryDrawerSettings
struct DictionaryDrawerSettings_t3408197424;
// Sirenix.OdinInspector.DisableContextMenuAttribute
struct DisableContextMenuAttribute_t184909590;
// Sirenix.OdinInspector.DisableIfAttribute
struct DisableIfAttribute_t2867643591;
// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct DisableInEditorModeAttribute_t630823232;
// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct DisableInInlineEditorsAttribute_t864950097;
// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct DisableInNonPrefabsAttribute_t431643105;
// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct DisableInPlayModeAttribute_t3352669054;
// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct DisableInPrefabAssetsAttribute_t227636478;
// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct DisableInPrefabInstancesAttribute_t3405933113;
// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct DisableInPrefabsAttribute_t3065057671;
// Sirenix.OdinInspector.DisplayAsStringAttribute
struct DisplayAsStringAttribute_t1002957046;
// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct DontApplyToListElementsAttribute_t3002873312;
// Sirenix.OdinInspector.DrawWithUnityAttribute
struct DrawWithUnityAttribute_t2993498865;
// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct EnableForPrefabOnlyAttribute_t2962074089;
// Sirenix.OdinInspector.EnableGUIAttribute
struct EnableGUIAttribute_t4210275809;
// Sirenix.OdinInspector.EnableIfAttribute
struct EnableIfAttribute_t729670423;
// Sirenix.OdinInspector.EnumPagingAttribute
struct EnumPagingAttribute_t3087485058;
// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct EnumToggleButtonsAttribute_t3538214787;
// Sirenix.OdinInspector.FilePathAttribute
struct FilePathAttribute_t2750434984;
// Sirenix.OdinInspector.FolderPathAttribute
struct FolderPathAttribute_t2308214518;
// Sirenix.OdinInspector.FoldoutGroupAttribute
struct FoldoutGroupAttribute_t1921228823;
// Sirenix.OdinInspector.GUIColorAttribute
struct GUIColorAttribute_t2790983735;
// Sirenix.OdinInspector.HideIfAttribute
struct HideIfAttribute_t2697016173;
// Sirenix.OdinInspector.HideInEditorModeAttribute
struct HideInEditorModeAttribute_t1562875995;
// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct HideInInlineEditorsAttribute_t2643132944;
// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct HideInNonPrefabsAttribute_t2267791284;
// Sirenix.OdinInspector.HideInPlayModeAttribute
struct HideInPlayModeAttribute_t3220251466;
// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct HideInPrefabAssetsAttribute_t3787194334;
// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct HideInPrefabInstancesAttribute_t271927100;
// Sirenix.OdinInspector.HideInPrefabsAttribute
struct HideInPrefabsAttribute_t2196881499;
// Sirenix.OdinInspector.HideInTablesAttribute
struct HideInTablesAttribute_t3879405200;
// Sirenix.OdinInspector.HideLabelAttribute
struct HideLabelAttribute_t3755575312;
// Sirenix.OdinInspector.HideMonoScriptAttribute
struct HideMonoScriptAttribute_t3453138722;
// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct HideNetworkBehaviourFieldsAttribute_t1543820585;
// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct HideReferenceObjectPickerAttribute_t3431975763;
// Sirenix.OdinInspector.HorizontalGroupAttribute
struct HorizontalGroupAttribute_t2886913732;
// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct IncludeMyAttributesAttribute_t2353296197;
// Sirenix.OdinInspector.IndentAttribute
struct IndentAttribute_t159299566;
// Sirenix.OdinInspector.InfoBoxAttribute
struct InfoBoxAttribute_t1629795541;
// Sirenix.OdinInspector.InlineButtonAttribute
struct InlineButtonAttribute_t2729761977;
// Sirenix.OdinInspector.InlineEditorAttribute
struct InlineEditorAttribute_t803766710;
// Sirenix.OdinInspector.InlinePropertyAttribute
struct InlinePropertyAttribute_t3336736111;
// Sirenix.OdinInspector.LabelTextAttribute
struct LabelTextAttribute_t2020830118;
// Sirenix.OdinInspector.LabelWidthAttribute
struct LabelWidthAttribute_t2595655695;
// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct ListDrawerSettingsAttribute_t1308946087;
// Sirenix.OdinInspector.MaxValueAttribute
struct MaxValueAttribute_t1325324426;
// Sirenix.OdinInspector.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t1448807924;
// Sirenix.OdinInspector.MinValueAttribute
struct MinValueAttribute_t1691372301;
// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct MultiLinePropertyAttribute_t1222183101;
// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct OnInspectorGUIAttribute_t3458889380;
// Sirenix.OdinInspector.OnValueChangedAttribute
struct OnValueChangedAttribute_t870857610;
// Sirenix.OdinInspector.PreviewFieldAttribute
struct PreviewFieldAttribute_t2856725314;
// Sirenix.OdinInspector.ProgressBarAttribute
struct ProgressBarAttribute_t2414458096;
// Sirenix.OdinInspector.PropertyGroupAttribute
struct PropertyGroupAttribute_t2009328757;
// Sirenix.OdinInspector.PropertyGroupAttribute[]
struct PropertyGroupAttributeU5BU5D_t645878296;
// Sirenix.OdinInspector.PropertyOrderAttribute
struct PropertyOrderAttribute_t2594707638;
// Sirenix.OdinInspector.PropertyRangeAttribute
struct PropertyRangeAttribute_t3334671474;
// Sirenix.OdinInspector.PropertySpaceAttribute
struct PropertySpaceAttribute_t209179749;
// Sirenix.OdinInspector.PropertyTooltipAttribute
struct PropertyTooltipAttribute_t1909666584;
// Sirenix.OdinInspector.ReadOnlyAttribute
struct ReadOnlyAttribute_t3311022813;
// Sirenix.OdinInspector.RegisterAttributeAttribute
struct RegisterAttributeAttribute_t317614997;
// Sirenix.OdinInspector.RequiredAttribute
struct RequiredAttribute_t1745431873;
// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct ResponsiveButtonGroupAttribute_t1637434916;
// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct SceneObjectsOnlyAttribute_t3518277911;
// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct ShowDrawerChainAttribute_t1144545212;
// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct ShowForPrefabOnlyAttribute_t4233203441;
// Sirenix.OdinInspector.ShowIfAttribute
struct ShowIfAttribute_t2614791958;
// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct ShowInInlineEditorsAttribute_t2931332120;
// Sirenix.OdinInspector.ShowInInspectorAttribute
struct ShowInInspectorAttribute_t2100412880;
// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct ShowOdinSerializedPropertiesInInspectorAttribute_t1684166398;
// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct ShowPropertyResolverAttribute_t357032921;
// Sirenix.OdinInspector.SuffixLabelAttribute
struct SuffixLabelAttribute_t126424634;
// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct SuppressInvalidAttributeErrorAttribute_t4105959342;
// Sirenix.OdinInspector.TabGroupAttribute
struct TabGroupAttribute_t2295443652;
// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct TabSubGroupAttribute_t867681421;
// Sirenix.OdinInspector.TableColumnWidthAttribute
struct TableColumnWidthAttribute_t1272758069;
// Sirenix.OdinInspector.TableListAttribute
struct TableListAttribute_t4098662806;
// Sirenix.OdinInspector.TableMatrixAttribute
struct TableMatrixAttribute_t391949620;
// Sirenix.OdinInspector.TitleAttribute
struct TitleAttribute_t3554241315;
// Sirenix.OdinInspector.TitleGroupAttribute
struct TitleGroupAttribute_t2185192947;
// Sirenix.OdinInspector.ToggleAttribute
struct ToggleAttribute_t3765571124;
// Sirenix.OdinInspector.ToggleGroupAttribute
struct ToggleGroupAttribute_t12117724;
// Sirenix.OdinInspector.ToggleLeftAttribute
struct ToggleLeftAttribute_t2985281771;
// Sirenix.OdinInspector.TypeFilterAttribute
struct TypeFilterAttribute_t642666696;
// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct TypeInfoBoxAttribute_t3273027527;
// Sirenix.OdinInspector.ValidateInputAttribute
struct ValidateInputAttribute_t471509373;
// Sirenix.OdinInspector.ValueDropdownAttribute
struct ValueDropdownAttribute_t2212993645;
// Sirenix.OdinInspector.VerticalGroupAttribute
struct VerticalGroupAttribute_t4076833965;
// Sirenix.OdinInspector.WrapAttribute
struct WrapAttribute_t589917550;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.Attribute
struct Attribute_t861562559;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct IList_1_t3824648540;
// System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct List_1_t3481403499;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;

extern RuntimeClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern RuntimeClass* BoxGroupAttribute_t1828532190_il2cpp_TypeInfo_var;
extern RuntimeClass* FoldoutGroupAttribute_t1921228823_il2cpp_TypeInfo_var;
extern RuntimeClass* HorizontalGroupAttribute_t2886913732_il2cpp_TypeInfo_var;
extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3481403499_il2cpp_TypeInfo_var;
extern RuntimeClass* Math_t1671470975_il2cpp_TypeInfo_var;
extern RuntimeClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern RuntimeClass* ResponsiveButtonGroupAttribute_t1637434916_il2cpp_TypeInfo_var;
extern RuntimeClass* TabGroupAttribute_t2295443652_il2cpp_TypeInfo_var;
extern RuntimeClass* TabSubGroupAttribute_t867681421_il2cpp_TypeInfo_var;
extern RuntimeClass* TitleGroupAttribute_t2185192947_il2cpp_TypeInfo_var;
extern RuntimeClass* ToggleGroupAttribute_t12117724_il2cpp_TypeInfo_var;
extern RuntimeClass* VerticalGroupAttribute_t4076833965_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1202628576;
extern String_t* _stringLiteral1299903114;
extern String_t* _stringLiteral1820882801;
extern String_t* _stringLiteral2432405111;
extern String_t* _stringLiteral2600272002;
extern String_t* _stringLiteral31325578;
extern String_t* _stringLiteral3378508766;
extern String_t* _stringLiteral3441077233;
extern String_t* _stringLiteral3452614529;
extern String_t* _stringLiteral3493619321;
extern String_t* _stringLiteral4005892038;
extern const RuntimeMethod* Enumerator_Dispose_m2026665411_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4250544074_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3483505131_RuntimeMethod_var;
extern const RuntimeMethod* InlineEditorAttribute__ctor_m3560120601_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1615589540_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m195709148_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m530823355_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2881262781_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m378240398_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2276455407_RuntimeMethod_var;
extern const RuntimeMethod* PropertyGroupAttribute_Combine_m1029103233_RuntimeMethod_var;
extern const uint32_t BoxGroupAttribute_CombineValuesWith_m1332820354_MetadataUsageId;
extern const uint32_t BoxGroupAttribute__ctor_m2509472355_MetadataUsageId;
extern const uint32_t DictionaryDrawerSettings__ctor_m752810153_MetadataUsageId;
extern const uint32_t FoldoutGroupAttribute_CombineValuesWith_m1568517065_MetadataUsageId;
extern const uint32_t HorizontalGroupAttribute_CombineValuesWith_m2321717564_MetadataUsageId;
extern const uint32_t HorizontalGroupAttribute__ctor_m2162494618_MetadataUsageId;
extern const uint32_t InlineEditorAttribute__ctor_m3560120601_MetadataUsageId;
extern const uint32_t MultiLinePropertyAttribute__ctor_m4147997088_MetadataUsageId;
extern const uint32_t PropertyGroupAttribute_Combine_m1029103233_MetadataUsageId;
extern const uint32_t ResponsiveButtonGroupAttribute_CombineValuesWith_m1418387196_MetadataUsageId;
extern const uint32_t TabGroupAttribute_CombineValuesWith_m2384920288_MetadataUsageId;
extern const uint32_t TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m330932969_MetadataUsageId;
extern const uint32_t TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m3393554505_MetadataUsageId;
extern const uint32_t TabGroupAttribute__ctor_m1778389056_MetadataUsageId;
extern const uint32_t TabGroupAttribute__ctor_m3613481846_MetadataUsageId;
extern const uint32_t TableListAttribute_get_ScrollViewHeight_m1419457301_MetadataUsageId;
extern const uint32_t TitleAttribute__ctor_m378211354_MetadataUsageId;
extern const uint32_t TitleGroupAttribute_CombineValuesWith_m4048746812_MetadataUsageId;
extern const uint32_t ToggleGroupAttribute_CombineValuesWith_m3327559737_MetadataUsageId;
extern const uint32_t ValueDropdownItem_t3457888829_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t ValueDropdownItem_t3457888829_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t VerticalGroupAttribute_CombineValuesWith_m3787726932_MetadataUsageId;
extern const uint32_t VerticalGroupAttribute__ctor_m4270404557_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct StringU5BU5D_t1281789340;


#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef LIST_1_T3481403499_H
#define LIST_1_T3481403499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct  List_1_t3481403499  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PropertyGroupAttributeU5BU5D_t645878296* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3481403499, ____items_1)); }
	inline PropertyGroupAttributeU5BU5D_t645878296* get__items_1() const { return ____items_1; }
	inline PropertyGroupAttributeU5BU5D_t645878296** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PropertyGroupAttributeU5BU5D_t645878296* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3481403499, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3481403499, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3481403499, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3481403499_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PropertyGroupAttributeU5BU5D_t645878296* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3481403499_StaticFields, ____emptyArray_5)); }
	inline PropertyGroupAttributeU5BU5D_t645878296* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PropertyGroupAttributeU5BU5D_t645878296** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PropertyGroupAttributeU5BU5D_t645878296* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3481403499_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t1281789340* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t1281789340* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t1281789340** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t1281789340* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ASSETLISTATTRIBUTE_T3126145472_H
#define ASSETLISTATTRIBUTE_T3126145472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.AssetListAttribute
struct  AssetListAttribute_t3126145472  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.AssetListAttribute::AutoPopulate
	bool ___AutoPopulate_0;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Tags
	String_t* ___Tags_1;
	// System.String Sirenix.OdinInspector.AssetListAttribute::LayerNames
	String_t* ___LayerNames_2;
	// System.String Sirenix.OdinInspector.AssetListAttribute::AssetNamePrefix
	String_t* ___AssetNamePrefix_3;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Path
	String_t* ___Path_4;
	// System.String Sirenix.OdinInspector.AssetListAttribute::CustomFilterMethod
	String_t* ___CustomFilterMethod_5;

public:
	inline static int32_t get_offset_of_AutoPopulate_0() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___AutoPopulate_0)); }
	inline bool get_AutoPopulate_0() const { return ___AutoPopulate_0; }
	inline bool* get_address_of_AutoPopulate_0() { return &___AutoPopulate_0; }
	inline void set_AutoPopulate_0(bool value)
	{
		___AutoPopulate_0 = value;
	}

	inline static int32_t get_offset_of_Tags_1() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___Tags_1)); }
	inline String_t* get_Tags_1() const { return ___Tags_1; }
	inline String_t** get_address_of_Tags_1() { return &___Tags_1; }
	inline void set_Tags_1(String_t* value)
	{
		___Tags_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tags_1), value);
	}

	inline static int32_t get_offset_of_LayerNames_2() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___LayerNames_2)); }
	inline String_t* get_LayerNames_2() const { return ___LayerNames_2; }
	inline String_t** get_address_of_LayerNames_2() { return &___LayerNames_2; }
	inline void set_LayerNames_2(String_t* value)
	{
		___LayerNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___LayerNames_2), value);
	}

	inline static int32_t get_offset_of_AssetNamePrefix_3() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___AssetNamePrefix_3)); }
	inline String_t* get_AssetNamePrefix_3() const { return ___AssetNamePrefix_3; }
	inline String_t** get_address_of_AssetNamePrefix_3() { return &___AssetNamePrefix_3; }
	inline void set_AssetNamePrefix_3(String_t* value)
	{
		___AssetNamePrefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___AssetNamePrefix_3), value);
	}

	inline static int32_t get_offset_of_Path_4() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___Path_4)); }
	inline String_t* get_Path_4() const { return ___Path_4; }
	inline String_t** get_address_of_Path_4() { return &___Path_4; }
	inline void set_Path_4(String_t* value)
	{
		___Path_4 = value;
		Il2CppCodeGenWriteBarrier((&___Path_4), value);
	}

	inline static int32_t get_offset_of_CustomFilterMethod_5() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___CustomFilterMethod_5)); }
	inline String_t* get_CustomFilterMethod_5() const { return ___CustomFilterMethod_5; }
	inline String_t** get_address_of_CustomFilterMethod_5() { return &___CustomFilterMethod_5; }
	inline void set_CustomFilterMethod_5(String_t* value)
	{
		___CustomFilterMethod_5 = value;
		Il2CppCodeGenWriteBarrier((&___CustomFilterMethod_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLISTATTRIBUTE_T3126145472_H
#ifndef ASSETSONLYATTRIBUTE_T3835058913_H
#define ASSETSONLYATTRIBUTE_T3835058913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.AssetsOnlyAttribute
struct  AssetsOnlyAttribute_t3835058913  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETSONLYATTRIBUTE_T3835058913_H
#ifndef COLORPALETTEATTRIBUTE_T392531492_H
#define COLORPALETTEATTRIBUTE_T392531492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ColorPaletteAttribute
struct  ColorPaletteAttribute_t392531492  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ColorPaletteAttribute::PaletteName
	String_t* ___PaletteName_0;
	// System.Boolean Sirenix.OdinInspector.ColorPaletteAttribute::ShowAlpha
	bool ___ShowAlpha_1;

public:
	inline static int32_t get_offset_of_PaletteName_0() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t392531492, ___PaletteName_0)); }
	inline String_t* get_PaletteName_0() const { return ___PaletteName_0; }
	inline String_t** get_address_of_PaletteName_0() { return &___PaletteName_0; }
	inline void set_PaletteName_0(String_t* value)
	{
		___PaletteName_0 = value;
		Il2CppCodeGenWriteBarrier((&___PaletteName_0), value);
	}

	inline static int32_t get_offset_of_ShowAlpha_1() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t392531492, ___ShowAlpha_1)); }
	inline bool get_ShowAlpha_1() const { return ___ShowAlpha_1; }
	inline bool* get_address_of_ShowAlpha_1() { return &___ShowAlpha_1; }
	inline void set_ShowAlpha_1(bool value)
	{
		___ShowAlpha_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPALETTEATTRIBUTE_T392531492_H
#ifndef CUSTOMCONTEXTMENUATTRIBUTE_T4264451314_H
#define CUSTOMCONTEXTMENUATTRIBUTE_T4264451314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.CustomContextMenuAttribute
struct  CustomContextMenuAttribute_t4264451314  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MenuItem
	String_t* ___MenuItem_0;
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MethodName
	String_t* ___MethodName_1;

public:
	inline static int32_t get_offset_of_MenuItem_0() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_t4264451314, ___MenuItem_0)); }
	inline String_t* get_MenuItem_0() const { return ___MenuItem_0; }
	inline String_t** get_address_of_MenuItem_0() { return &___MenuItem_0; }
	inline void set_MenuItem_0(String_t* value)
	{
		___MenuItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___MenuItem_0), value);
	}

	inline static int32_t get_offset_of_MethodName_1() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_t4264451314, ___MethodName_1)); }
	inline String_t* get_MethodName_1() const { return ___MethodName_1; }
	inline String_t** get_address_of_MethodName_1() { return &___MethodName_1; }
	inline void set_MethodName_1(String_t* value)
	{
		___MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTEXTMENUATTRIBUTE_T4264451314_H
#ifndef CUSTOMVALUEDRAWERATTRIBUTE_T4188368302_H
#define CUSTOMVALUEDRAWERATTRIBUTE_T4188368302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct  CustomValueDrawerAttribute_t4188368302  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.CustomValueDrawerAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(CustomValueDrawerAttribute_t4188368302, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVALUEDRAWERATTRIBUTE_T4188368302_H
#ifndef DELAYEDPROPERTYATTRIBUTE_T1141984869_H
#define DELAYEDPROPERTYATTRIBUTE_T1141984869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DelayedPropertyAttribute
struct  DelayedPropertyAttribute_t1141984869  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELAYEDPROPERTYATTRIBUTE_T1141984869_H
#ifndef DISABLECONTEXTMENUATTRIBUTE_T184909590_H
#define DISABLECONTEXTMENUATTRIBUTE_T184909590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableContextMenuAttribute
struct  DisableContextMenuAttribute_t184909590  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForMember
	bool ___DisableForMember_0;
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForCollectionElements
	bool ___DisableForCollectionElements_1;

public:
	inline static int32_t get_offset_of_DisableForMember_0() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t184909590, ___DisableForMember_0)); }
	inline bool get_DisableForMember_0() const { return ___DisableForMember_0; }
	inline bool* get_address_of_DisableForMember_0() { return &___DisableForMember_0; }
	inline void set_DisableForMember_0(bool value)
	{
		___DisableForMember_0 = value;
	}

	inline static int32_t get_offset_of_DisableForCollectionElements_1() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t184909590, ___DisableForCollectionElements_1)); }
	inline bool get_DisableForCollectionElements_1() const { return ___DisableForCollectionElements_1; }
	inline bool* get_address_of_DisableForCollectionElements_1() { return &___DisableForCollectionElements_1; }
	inline void set_DisableForCollectionElements_1(bool value)
	{
		___DisableForCollectionElements_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONTEXTMENUATTRIBUTE_T184909590_H
#ifndef DISABLEIFATTRIBUTE_T2867643591_H
#define DISABLEIFATTRIBUTE_T2867643591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableIfAttribute
struct  DisableIfAttribute_t2867643591  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.DisableIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.DisableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t2867643591, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t2867643591, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEIFATTRIBUTE_T2867643591_H
#ifndef DISABLEINEDITORMODEATTRIBUTE_T630823232_H
#define DISABLEINEDITORMODEATTRIBUTE_T630823232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct  DisableInEditorModeAttribute_t630823232  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINEDITORMODEATTRIBUTE_T630823232_H
#ifndef DISABLEININLINEEDITORSATTRIBUTE_T864950097_H
#define DISABLEININLINEEDITORSATTRIBUTE_T864950097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct  DisableInInlineEditorsAttribute_t864950097  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEININLINEEDITORSATTRIBUTE_T864950097_H
#ifndef DISABLEINNONPREFABSATTRIBUTE_T431643105_H
#define DISABLEINNONPREFABSATTRIBUTE_T431643105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct  DisableInNonPrefabsAttribute_t431643105  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINNONPREFABSATTRIBUTE_T431643105_H
#ifndef DISABLEINPLAYMODEATTRIBUTE_T3352669054_H
#define DISABLEINPLAYMODEATTRIBUTE_T3352669054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct  DisableInPlayModeAttribute_t3352669054  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPLAYMODEATTRIBUTE_T3352669054_H
#ifndef DISABLEINPREFABASSETSATTRIBUTE_T227636478_H
#define DISABLEINPREFABASSETSATTRIBUTE_T227636478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct  DisableInPrefabAssetsAttribute_t227636478  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPREFABASSETSATTRIBUTE_T227636478_H
#ifndef DISABLEINPREFABINSTANCESATTRIBUTE_T3405933113_H
#define DISABLEINPREFABINSTANCESATTRIBUTE_T3405933113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct  DisableInPrefabInstancesAttribute_t3405933113  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPREFABINSTANCESATTRIBUTE_T3405933113_H
#ifndef DISABLEINPREFABSATTRIBUTE_T3065057671_H
#define DISABLEINPREFABSATTRIBUTE_T3065057671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct  DisableInPrefabsAttribute_t3065057671  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPREFABSATTRIBUTE_T3065057671_H
#ifndef DISPLAYASSTRINGATTRIBUTE_T1002957046_H
#define DISPLAYASSTRINGATTRIBUTE_T1002957046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisplayAsStringAttribute
struct  DisplayAsStringAttribute_t1002957046  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.DisplayAsStringAttribute::Overflow
	bool ___Overflow_0;

public:
	inline static int32_t get_offset_of_Overflow_0() { return static_cast<int32_t>(offsetof(DisplayAsStringAttribute_t1002957046, ___Overflow_0)); }
	inline bool get_Overflow_0() const { return ___Overflow_0; }
	inline bool* get_address_of_Overflow_0() { return &___Overflow_0; }
	inline void set_Overflow_0(bool value)
	{
		___Overflow_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYASSTRINGATTRIBUTE_T1002957046_H
#ifndef DONTAPPLYTOLISTELEMENTSATTRIBUTE_T3002873312_H
#define DONTAPPLYTOLISTELEMENTSATTRIBUTE_T3002873312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct  DontApplyToListElementsAttribute_t3002873312  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTAPPLYTOLISTELEMENTSATTRIBUTE_T3002873312_H
#ifndef DRAWWITHUNITYATTRIBUTE_T2993498865_H
#define DRAWWITHUNITYATTRIBUTE_T2993498865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DrawWithUnityAttribute
struct  DrawWithUnityAttribute_t2993498865  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWWITHUNITYATTRIBUTE_T2993498865_H
#ifndef ENABLEFORPREFABONLYATTRIBUTE_T2962074089_H
#define ENABLEFORPREFABONLYATTRIBUTE_T2962074089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct  EnableForPrefabOnlyAttribute_t2962074089  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEFORPREFABONLYATTRIBUTE_T2962074089_H
#ifndef ENABLEGUIATTRIBUTE_T4210275809_H
#define ENABLEGUIATTRIBUTE_T4210275809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnableGUIAttribute
struct  EnableGUIAttribute_t4210275809  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEGUIATTRIBUTE_T4210275809_H
#ifndef ENABLEIFATTRIBUTE_T729670423_H
#define ENABLEIFATTRIBUTE_T729670423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnableIfAttribute
struct  EnableIfAttribute_t729670423  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.EnableIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.EnableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t729670423, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t729670423, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEIFATTRIBUTE_T729670423_H
#ifndef ENUMPAGINGATTRIBUTE_T3087485058_H
#define ENUMPAGINGATTRIBUTE_T3087485058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnumPagingAttribute
struct  EnumPagingAttribute_t3087485058  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMPAGINGATTRIBUTE_T3087485058_H
#ifndef ENUMTOGGLEBUTTONSATTRIBUTE_T3538214787_H
#define ENUMTOGGLEBUTTONSATTRIBUTE_T3538214787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct  EnumToggleButtonsAttribute_t3538214787  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMTOGGLEBUTTONSATTRIBUTE_T3538214787_H
#ifndef FILEPATHATTRIBUTE_T2750434984_H
#define FILEPATHATTRIBUTE_T2750434984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.FilePathAttribute
struct  FilePathAttribute_t2750434984  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FilePathAttribute::Extensions
	String_t* ___Extensions_1;
	// System.String Sirenix.OdinInspector.FilePathAttribute::ParentFolder
	String_t* ___ParentFolder_2;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireValidPath
	bool ___RequireValidPath_3;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireExistingPath
	bool ___RequireExistingPath_4;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::UseBackslashes
	bool ___UseBackslashes_5;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::<ReadOnly>k__BackingField
	bool ___U3CReadOnlyU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_Extensions_1() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___Extensions_1)); }
	inline String_t* get_Extensions_1() const { return ___Extensions_1; }
	inline String_t** get_address_of_Extensions_1() { return &___Extensions_1; }
	inline void set_Extensions_1(String_t* value)
	{
		___Extensions_1 = value;
		Il2CppCodeGenWriteBarrier((&___Extensions_1), value);
	}

	inline static int32_t get_offset_of_ParentFolder_2() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___ParentFolder_2)); }
	inline String_t* get_ParentFolder_2() const { return ___ParentFolder_2; }
	inline String_t** get_address_of_ParentFolder_2() { return &___ParentFolder_2; }
	inline void set_ParentFolder_2(String_t* value)
	{
		___ParentFolder_2 = value;
		Il2CppCodeGenWriteBarrier((&___ParentFolder_2), value);
	}

	inline static int32_t get_offset_of_RequireValidPath_3() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___RequireValidPath_3)); }
	inline bool get_RequireValidPath_3() const { return ___RequireValidPath_3; }
	inline bool* get_address_of_RequireValidPath_3() { return &___RequireValidPath_3; }
	inline void set_RequireValidPath_3(bool value)
	{
		___RequireValidPath_3 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_4() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___RequireExistingPath_4)); }
	inline bool get_RequireExistingPath_4() const { return ___RequireExistingPath_4; }
	inline bool* get_address_of_RequireExistingPath_4() { return &___RequireExistingPath_4; }
	inline void set_RequireExistingPath_4(bool value)
	{
		___RequireExistingPath_4 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_5() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___UseBackslashes_5)); }
	inline bool get_UseBackslashes_5() const { return ___UseBackslashes_5; }
	inline bool* get_address_of_UseBackslashes_5() { return &___UseBackslashes_5; }
	inline void set_UseBackslashes_5(bool value)
	{
		___UseBackslashes_5 = value;
	}

	inline static int32_t get_offset_of_U3CReadOnlyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___U3CReadOnlyU3Ek__BackingField_6)); }
	inline bool get_U3CReadOnlyU3Ek__BackingField_6() const { return ___U3CReadOnlyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CReadOnlyU3Ek__BackingField_6() { return &___U3CReadOnlyU3Ek__BackingField_6; }
	inline void set_U3CReadOnlyU3Ek__BackingField_6(bool value)
	{
		___U3CReadOnlyU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEPATHATTRIBUTE_T2750434984_H
#ifndef FOLDERPATHATTRIBUTE_T2308214518_H
#define FOLDERPATHATTRIBUTE_T2308214518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.FolderPathAttribute
struct  FolderPathAttribute_t2308214518  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FolderPathAttribute::ParentFolder
	String_t* ___ParentFolder_1;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireValidPath
	bool ___RequireValidPath_2;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireExistingPath
	bool ___RequireExistingPath_3;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::UseBackslashes
	bool ___UseBackslashes_4;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_ParentFolder_1() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___ParentFolder_1)); }
	inline String_t* get_ParentFolder_1() const { return ___ParentFolder_1; }
	inline String_t** get_address_of_ParentFolder_1() { return &___ParentFolder_1; }
	inline void set_ParentFolder_1(String_t* value)
	{
		___ParentFolder_1 = value;
		Il2CppCodeGenWriteBarrier((&___ParentFolder_1), value);
	}

	inline static int32_t get_offset_of_RequireValidPath_2() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___RequireValidPath_2)); }
	inline bool get_RequireValidPath_2() const { return ___RequireValidPath_2; }
	inline bool* get_address_of_RequireValidPath_2() { return &___RequireValidPath_2; }
	inline void set_RequireValidPath_2(bool value)
	{
		___RequireValidPath_2 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_3() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___RequireExistingPath_3)); }
	inline bool get_RequireExistingPath_3() const { return ___RequireExistingPath_3; }
	inline bool* get_address_of_RequireExistingPath_3() { return &___RequireExistingPath_3; }
	inline void set_RequireExistingPath_3(bool value)
	{
		___RequireExistingPath_3 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_4() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___UseBackslashes_4)); }
	inline bool get_UseBackslashes_4() const { return ___UseBackslashes_4; }
	inline bool* get_address_of_UseBackslashes_4() { return &___UseBackslashes_4; }
	inline void set_UseBackslashes_4(bool value)
	{
		___UseBackslashes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLDERPATHATTRIBUTE_T2308214518_H
#ifndef HIDEIFATTRIBUTE_T2697016173_H
#define HIDEIFATTRIBUTE_T2697016173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideIfAttribute
struct  HideIfAttribute_t2697016173  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.HideIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.HideIfAttribute::Value
	RuntimeObject * ___Value_1;
	// System.Boolean Sirenix.OdinInspector.HideIfAttribute::Animate
	bool ___Animate_2;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(HideIfAttribute_t2697016173, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(HideIfAttribute_t2697016173, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}

	inline static int32_t get_offset_of_Animate_2() { return static_cast<int32_t>(offsetof(HideIfAttribute_t2697016173, ___Animate_2)); }
	inline bool get_Animate_2() const { return ___Animate_2; }
	inline bool* get_address_of_Animate_2() { return &___Animate_2; }
	inline void set_Animate_2(bool value)
	{
		___Animate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEIFATTRIBUTE_T2697016173_H
#ifndef HIDEINEDITORMODEATTRIBUTE_T1562875995_H
#define HIDEINEDITORMODEATTRIBUTE_T1562875995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInEditorModeAttribute
struct  HideInEditorModeAttribute_t1562875995  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINEDITORMODEATTRIBUTE_T1562875995_H
#ifndef HIDEININLINEEDITORSATTRIBUTE_T2643132944_H
#define HIDEININLINEEDITORSATTRIBUTE_T2643132944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct  HideInInlineEditorsAttribute_t2643132944  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEININLINEEDITORSATTRIBUTE_T2643132944_H
#ifndef HIDEINNONPREFABSATTRIBUTE_T2267791284_H
#define HIDEINNONPREFABSATTRIBUTE_T2267791284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct  HideInNonPrefabsAttribute_t2267791284  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINNONPREFABSATTRIBUTE_T2267791284_H
#ifndef HIDEINPLAYMODEATTRIBUTE_T3220251466_H
#define HIDEINPLAYMODEATTRIBUTE_T3220251466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPlayModeAttribute
struct  HideInPlayModeAttribute_t3220251466  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPLAYMODEATTRIBUTE_T3220251466_H
#ifndef HIDEINPREFABASSETSATTRIBUTE_T3787194334_H
#define HIDEINPREFABASSETSATTRIBUTE_T3787194334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct  HideInPrefabAssetsAttribute_t3787194334  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPREFABASSETSATTRIBUTE_T3787194334_H
#ifndef HIDEINPREFABINSTANCESATTRIBUTE_T271927100_H
#define HIDEINPREFABINSTANCESATTRIBUTE_T271927100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct  HideInPrefabInstancesAttribute_t271927100  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPREFABINSTANCESATTRIBUTE_T271927100_H
#ifndef HIDEINPREFABSATTRIBUTE_T2196881499_H
#define HIDEINPREFABSATTRIBUTE_T2196881499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPrefabsAttribute
struct  HideInPrefabsAttribute_t2196881499  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPREFABSATTRIBUTE_T2196881499_H
#ifndef HIDEINTABLESATTRIBUTE_T3879405200_H
#define HIDEINTABLESATTRIBUTE_T3879405200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInTablesAttribute
struct  HideInTablesAttribute_t3879405200  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINTABLESATTRIBUTE_T3879405200_H
#ifndef HIDELABELATTRIBUTE_T3755575312_H
#define HIDELABELATTRIBUTE_T3755575312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideLabelAttribute
struct  HideLabelAttribute_t3755575312  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDELABELATTRIBUTE_T3755575312_H
#ifndef HIDEMONOSCRIPTATTRIBUTE_T3453138722_H
#define HIDEMONOSCRIPTATTRIBUTE_T3453138722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideMonoScriptAttribute
struct  HideMonoScriptAttribute_t3453138722  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEMONOSCRIPTATTRIBUTE_T3453138722_H
#ifndef HIDENETWORKBEHAVIOURFIELDSATTRIBUTE_T1543820585_H
#define HIDENETWORKBEHAVIOURFIELDSATTRIBUTE_T1543820585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct  HideNetworkBehaviourFieldsAttribute_t1543820585  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDENETWORKBEHAVIOURFIELDSATTRIBUTE_T1543820585_H
#ifndef HIDEREFERENCEOBJECTPICKERATTRIBUTE_T3431975763_H
#define HIDEREFERENCEOBJECTPICKERATTRIBUTE_T3431975763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct  HideReferenceObjectPickerAttribute_t3431975763  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEREFERENCEOBJECTPICKERATTRIBUTE_T3431975763_H
#ifndef INCLUDEMYATTRIBUTESATTRIBUTE_T2353296197_H
#define INCLUDEMYATTRIBUTESATTRIBUTE_T2353296197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct  IncludeMyAttributesAttribute_t2353296197  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCLUDEMYATTRIBUTESATTRIBUTE_T2353296197_H
#ifndef INDENTATTRIBUTE_T159299566_H
#define INDENTATTRIBUTE_T159299566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.IndentAttribute
struct  IndentAttribute_t159299566  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.IndentAttribute::IndentLevel
	int32_t ___IndentLevel_0;

public:
	inline static int32_t get_offset_of_IndentLevel_0() { return static_cast<int32_t>(offsetof(IndentAttribute_t159299566, ___IndentLevel_0)); }
	inline int32_t get_IndentLevel_0() const { return ___IndentLevel_0; }
	inline int32_t* get_address_of_IndentLevel_0() { return &___IndentLevel_0; }
	inline void set_IndentLevel_0(int32_t value)
	{
		___IndentLevel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDENTATTRIBUTE_T159299566_H
#ifndef INLINEBUTTONATTRIBUTE_T2729761977_H
#define INLINEBUTTONATTRIBUTE_T2729761977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineButtonAttribute
struct  InlineButtonAttribute_t2729761977  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::<MemberMethod>k__BackingField
	String_t* ___U3CMemberMethodU3Ek__BackingField_0;
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMemberMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t2729761977, ___U3CMemberMethodU3Ek__BackingField_0)); }
	inline String_t* get_U3CMemberMethodU3Ek__BackingField_0() const { return ___U3CMemberMethodU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMemberMethodU3Ek__BackingField_0() { return &___U3CMemberMethodU3Ek__BackingField_0; }
	inline void set_U3CMemberMethodU3Ek__BackingField_0(String_t* value)
	{
		___U3CMemberMethodU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberMethodU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t2729761977, ___U3CLabelU3Ek__BackingField_1)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_1() const { return ___U3CLabelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_1() { return &___U3CLabelU3Ek__BackingField_1; }
	inline void set_U3CLabelU3Ek__BackingField_1(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEBUTTONATTRIBUTE_T2729761977_H
#ifndef INLINEPROPERTYATTRIBUTE_T3336736111_H
#define INLINEPROPERTYATTRIBUTE_T3336736111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlinePropertyAttribute
struct  InlinePropertyAttribute_t3336736111  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.InlinePropertyAttribute::LabelWidth
	int32_t ___LabelWidth_0;

public:
	inline static int32_t get_offset_of_LabelWidth_0() { return static_cast<int32_t>(offsetof(InlinePropertyAttribute_t3336736111, ___LabelWidth_0)); }
	inline int32_t get_LabelWidth_0() const { return ___LabelWidth_0; }
	inline int32_t* get_address_of_LabelWidth_0() { return &___LabelWidth_0; }
	inline void set_LabelWidth_0(int32_t value)
	{
		___LabelWidth_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEPROPERTYATTRIBUTE_T3336736111_H
#ifndef LABELTEXTATTRIBUTE_T2020830118_H
#define LABELTEXTATTRIBUTE_T2020830118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.LabelTextAttribute
struct  LabelTextAttribute_t2020830118  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.LabelTextAttribute::Text
	String_t* ___Text_0;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(LabelTextAttribute_t2020830118, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELTEXTATTRIBUTE_T2020830118_H
#ifndef LABELWIDTHATTRIBUTE_T2595655695_H
#define LABELWIDTHATTRIBUTE_T2595655695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.LabelWidthAttribute
struct  LabelWidthAttribute_t2595655695  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.LabelWidthAttribute::Width
	float ___Width_0;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(LabelWidthAttribute_t2595655695, ___Width_0)); }
	inline float get_Width_0() const { return ___Width_0; }
	inline float* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(float value)
	{
		___Width_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELWIDTHATTRIBUTE_T2595655695_H
#ifndef LISTDRAWERSETTINGSATTRIBUTE_T1308946087_H
#define LISTDRAWERSETTINGSATTRIBUTE_T1308946087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct  ListDrawerSettingsAttribute_t1308946087  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideAddButton
	bool ___HideAddButton_0;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideRemoveButton
	bool ___HideRemoveButton_1;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::ListElementLabelName
	String_t* ___ListElementLabelName_2;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomAddFunction
	String_t* ___CustomAddFunction_3;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveIndexFunction
	String_t* ___CustomRemoveIndexFunction_4;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveElementFunction
	String_t* ___CustomRemoveElementFunction_5;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnBeginListElementGUI
	String_t* ___OnBeginListElementGUI_6;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnEndListElementGUI
	String_t* ___OnEndListElementGUI_7;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::AlwaysAddDefaultValue
	bool ___AlwaysAddDefaultValue_8;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::onTitleBarGUI
	String_t* ___onTitleBarGUI_9;
	// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPage
	int32_t ___numberOfItemsPerPage_10;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::paging
	bool ___paging_11;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggable
	bool ___draggable_12;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnly
	bool ___isReadOnly_13;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCount
	bool ___showItemCount_14;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::pagingHasValue
	bool ___pagingHasValue_15;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggableHasValue
	bool ___draggableHasValue_16;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnlyHasValue
	bool ___isReadOnlyHasValue_17;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCountHasValue
	bool ___showItemCountHasValue_18;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expanded
	bool ___expanded_19;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expandedHasValue
	bool ___expandedHasValue_20;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPageHasValue
	bool ___numberOfItemsPerPageHasValue_21;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabels
	bool ___showIndexLabels_22;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabelsHasValue
	bool ___showIndexLabelsHasValue_23;

public:
	inline static int32_t get_offset_of_HideAddButton_0() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___HideAddButton_0)); }
	inline bool get_HideAddButton_0() const { return ___HideAddButton_0; }
	inline bool* get_address_of_HideAddButton_0() { return &___HideAddButton_0; }
	inline void set_HideAddButton_0(bool value)
	{
		___HideAddButton_0 = value;
	}

	inline static int32_t get_offset_of_HideRemoveButton_1() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___HideRemoveButton_1)); }
	inline bool get_HideRemoveButton_1() const { return ___HideRemoveButton_1; }
	inline bool* get_address_of_HideRemoveButton_1() { return &___HideRemoveButton_1; }
	inline void set_HideRemoveButton_1(bool value)
	{
		___HideRemoveButton_1 = value;
	}

	inline static int32_t get_offset_of_ListElementLabelName_2() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___ListElementLabelName_2)); }
	inline String_t* get_ListElementLabelName_2() const { return ___ListElementLabelName_2; }
	inline String_t** get_address_of_ListElementLabelName_2() { return &___ListElementLabelName_2; }
	inline void set_ListElementLabelName_2(String_t* value)
	{
		___ListElementLabelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___ListElementLabelName_2), value);
	}

	inline static int32_t get_offset_of_CustomAddFunction_3() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___CustomAddFunction_3)); }
	inline String_t* get_CustomAddFunction_3() const { return ___CustomAddFunction_3; }
	inline String_t** get_address_of_CustomAddFunction_3() { return &___CustomAddFunction_3; }
	inline void set_CustomAddFunction_3(String_t* value)
	{
		___CustomAddFunction_3 = value;
		Il2CppCodeGenWriteBarrier((&___CustomAddFunction_3), value);
	}

	inline static int32_t get_offset_of_CustomRemoveIndexFunction_4() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___CustomRemoveIndexFunction_4)); }
	inline String_t* get_CustomRemoveIndexFunction_4() const { return ___CustomRemoveIndexFunction_4; }
	inline String_t** get_address_of_CustomRemoveIndexFunction_4() { return &___CustomRemoveIndexFunction_4; }
	inline void set_CustomRemoveIndexFunction_4(String_t* value)
	{
		___CustomRemoveIndexFunction_4 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRemoveIndexFunction_4), value);
	}

	inline static int32_t get_offset_of_CustomRemoveElementFunction_5() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___CustomRemoveElementFunction_5)); }
	inline String_t* get_CustomRemoveElementFunction_5() const { return ___CustomRemoveElementFunction_5; }
	inline String_t** get_address_of_CustomRemoveElementFunction_5() { return &___CustomRemoveElementFunction_5; }
	inline void set_CustomRemoveElementFunction_5(String_t* value)
	{
		___CustomRemoveElementFunction_5 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRemoveElementFunction_5), value);
	}

	inline static int32_t get_offset_of_OnBeginListElementGUI_6() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___OnBeginListElementGUI_6)); }
	inline String_t* get_OnBeginListElementGUI_6() const { return ___OnBeginListElementGUI_6; }
	inline String_t** get_address_of_OnBeginListElementGUI_6() { return &___OnBeginListElementGUI_6; }
	inline void set_OnBeginListElementGUI_6(String_t* value)
	{
		___OnBeginListElementGUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnBeginListElementGUI_6), value);
	}

	inline static int32_t get_offset_of_OnEndListElementGUI_7() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___OnEndListElementGUI_7)); }
	inline String_t* get_OnEndListElementGUI_7() const { return ___OnEndListElementGUI_7; }
	inline String_t** get_address_of_OnEndListElementGUI_7() { return &___OnEndListElementGUI_7; }
	inline void set_OnEndListElementGUI_7(String_t* value)
	{
		___OnEndListElementGUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnEndListElementGUI_7), value);
	}

	inline static int32_t get_offset_of_AlwaysAddDefaultValue_8() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___AlwaysAddDefaultValue_8)); }
	inline bool get_AlwaysAddDefaultValue_8() const { return ___AlwaysAddDefaultValue_8; }
	inline bool* get_address_of_AlwaysAddDefaultValue_8() { return &___AlwaysAddDefaultValue_8; }
	inline void set_AlwaysAddDefaultValue_8(bool value)
	{
		___AlwaysAddDefaultValue_8 = value;
	}

	inline static int32_t get_offset_of_onTitleBarGUI_9() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___onTitleBarGUI_9)); }
	inline String_t* get_onTitleBarGUI_9() const { return ___onTitleBarGUI_9; }
	inline String_t** get_address_of_onTitleBarGUI_9() { return &___onTitleBarGUI_9; }
	inline void set_onTitleBarGUI_9(String_t* value)
	{
		___onTitleBarGUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTitleBarGUI_9), value);
	}

	inline static int32_t get_offset_of_numberOfItemsPerPage_10() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___numberOfItemsPerPage_10)); }
	inline int32_t get_numberOfItemsPerPage_10() const { return ___numberOfItemsPerPage_10; }
	inline int32_t* get_address_of_numberOfItemsPerPage_10() { return &___numberOfItemsPerPage_10; }
	inline void set_numberOfItemsPerPage_10(int32_t value)
	{
		___numberOfItemsPerPage_10 = value;
	}

	inline static int32_t get_offset_of_paging_11() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___paging_11)); }
	inline bool get_paging_11() const { return ___paging_11; }
	inline bool* get_address_of_paging_11() { return &___paging_11; }
	inline void set_paging_11(bool value)
	{
		___paging_11 = value;
	}

	inline static int32_t get_offset_of_draggable_12() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___draggable_12)); }
	inline bool get_draggable_12() const { return ___draggable_12; }
	inline bool* get_address_of_draggable_12() { return &___draggable_12; }
	inline void set_draggable_12(bool value)
	{
		___draggable_12 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_13() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___isReadOnly_13)); }
	inline bool get_isReadOnly_13() const { return ___isReadOnly_13; }
	inline bool* get_address_of_isReadOnly_13() { return &___isReadOnly_13; }
	inline void set_isReadOnly_13(bool value)
	{
		___isReadOnly_13 = value;
	}

	inline static int32_t get_offset_of_showItemCount_14() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showItemCount_14)); }
	inline bool get_showItemCount_14() const { return ___showItemCount_14; }
	inline bool* get_address_of_showItemCount_14() { return &___showItemCount_14; }
	inline void set_showItemCount_14(bool value)
	{
		___showItemCount_14 = value;
	}

	inline static int32_t get_offset_of_pagingHasValue_15() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___pagingHasValue_15)); }
	inline bool get_pagingHasValue_15() const { return ___pagingHasValue_15; }
	inline bool* get_address_of_pagingHasValue_15() { return &___pagingHasValue_15; }
	inline void set_pagingHasValue_15(bool value)
	{
		___pagingHasValue_15 = value;
	}

	inline static int32_t get_offset_of_draggableHasValue_16() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___draggableHasValue_16)); }
	inline bool get_draggableHasValue_16() const { return ___draggableHasValue_16; }
	inline bool* get_address_of_draggableHasValue_16() { return &___draggableHasValue_16; }
	inline void set_draggableHasValue_16(bool value)
	{
		___draggableHasValue_16 = value;
	}

	inline static int32_t get_offset_of_isReadOnlyHasValue_17() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___isReadOnlyHasValue_17)); }
	inline bool get_isReadOnlyHasValue_17() const { return ___isReadOnlyHasValue_17; }
	inline bool* get_address_of_isReadOnlyHasValue_17() { return &___isReadOnlyHasValue_17; }
	inline void set_isReadOnlyHasValue_17(bool value)
	{
		___isReadOnlyHasValue_17 = value;
	}

	inline static int32_t get_offset_of_showItemCountHasValue_18() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showItemCountHasValue_18)); }
	inline bool get_showItemCountHasValue_18() const { return ___showItemCountHasValue_18; }
	inline bool* get_address_of_showItemCountHasValue_18() { return &___showItemCountHasValue_18; }
	inline void set_showItemCountHasValue_18(bool value)
	{
		___showItemCountHasValue_18 = value;
	}

	inline static int32_t get_offset_of_expanded_19() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___expanded_19)); }
	inline bool get_expanded_19() const { return ___expanded_19; }
	inline bool* get_address_of_expanded_19() { return &___expanded_19; }
	inline void set_expanded_19(bool value)
	{
		___expanded_19 = value;
	}

	inline static int32_t get_offset_of_expandedHasValue_20() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___expandedHasValue_20)); }
	inline bool get_expandedHasValue_20() const { return ___expandedHasValue_20; }
	inline bool* get_address_of_expandedHasValue_20() { return &___expandedHasValue_20; }
	inline void set_expandedHasValue_20(bool value)
	{
		___expandedHasValue_20 = value;
	}

	inline static int32_t get_offset_of_numberOfItemsPerPageHasValue_21() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___numberOfItemsPerPageHasValue_21)); }
	inline bool get_numberOfItemsPerPageHasValue_21() const { return ___numberOfItemsPerPageHasValue_21; }
	inline bool* get_address_of_numberOfItemsPerPageHasValue_21() { return &___numberOfItemsPerPageHasValue_21; }
	inline void set_numberOfItemsPerPageHasValue_21(bool value)
	{
		___numberOfItemsPerPageHasValue_21 = value;
	}

	inline static int32_t get_offset_of_showIndexLabels_22() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showIndexLabels_22)); }
	inline bool get_showIndexLabels_22() const { return ___showIndexLabels_22; }
	inline bool* get_address_of_showIndexLabels_22() { return &___showIndexLabels_22; }
	inline void set_showIndexLabels_22(bool value)
	{
		___showIndexLabels_22 = value;
	}

	inline static int32_t get_offset_of_showIndexLabelsHasValue_23() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showIndexLabelsHasValue_23)); }
	inline bool get_showIndexLabelsHasValue_23() const { return ___showIndexLabelsHasValue_23; }
	inline bool* get_address_of_showIndexLabelsHasValue_23() { return &___showIndexLabelsHasValue_23; }
	inline void set_showIndexLabelsHasValue_23(bool value)
	{
		___showIndexLabelsHasValue_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTDRAWERSETTINGSATTRIBUTE_T1308946087_H
#ifndef MAXVALUEATTRIBUTE_T1325324426_H
#define MAXVALUEATTRIBUTE_T1325324426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MaxValueAttribute
struct  MaxValueAttribute_t1325324426  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.MaxValueAttribute::MaxValue
	double ___MaxValue_0;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(MaxValueAttribute_t1325324426, ___MaxValue_0)); }
	inline double get_MaxValue_0() const { return ___MaxValue_0; }
	inline double* get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(double value)
	{
		___MaxValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXVALUEATTRIBUTE_T1325324426_H
#ifndef MINMAXSLIDERATTRIBUTE_T1448807924_H
#define MINMAXSLIDERATTRIBUTE_T1448807924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MinMaxSliderAttribute
struct  MinMaxSliderAttribute_t1448807924  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MinValue
	float ___MinValue_0;
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MaxValue
	float ___MaxValue_1;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MaxMember
	String_t* ___MaxMember_3;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMaxMember
	String_t* ___MinMaxMember_4;
	// System.Boolean Sirenix.OdinInspector.MinMaxSliderAttribute::ShowFields
	bool ___ShowFields_5;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MinValue_0)); }
	inline float get_MinValue_0() const { return ___MinValue_0; }
	inline float* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(float value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MaxValue_1)); }
	inline float get_MaxValue_1() const { return ___MaxValue_1; }
	inline float* get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(float value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinMember_2), value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___MaxMember_3), value);
	}

	inline static int32_t get_offset_of_MinMaxMember_4() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MinMaxMember_4)); }
	inline String_t* get_MinMaxMember_4() const { return ___MinMaxMember_4; }
	inline String_t** get_address_of_MinMaxMember_4() { return &___MinMaxMember_4; }
	inline void set_MinMaxMember_4(String_t* value)
	{
		___MinMaxMember_4 = value;
		Il2CppCodeGenWriteBarrier((&___MinMaxMember_4), value);
	}

	inline static int32_t get_offset_of_ShowFields_5() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___ShowFields_5)); }
	inline bool get_ShowFields_5() const { return ___ShowFields_5; }
	inline bool* get_address_of_ShowFields_5() { return &___ShowFields_5; }
	inline void set_ShowFields_5(bool value)
	{
		___ShowFields_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXSLIDERATTRIBUTE_T1448807924_H
#ifndef MINVALUEATTRIBUTE_T1691372301_H
#define MINVALUEATTRIBUTE_T1691372301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MinValueAttribute
struct  MinValueAttribute_t1691372301  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.MinValueAttribute::MinValue
	double ___MinValue_0;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinValueAttribute_t1691372301, ___MinValue_0)); }
	inline double get_MinValue_0() const { return ___MinValue_0; }
	inline double* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(double value)
	{
		___MinValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINVALUEATTRIBUTE_T1691372301_H
#ifndef MULTILINEPROPERTYATTRIBUTE_T1222183101_H
#define MULTILINEPROPERTYATTRIBUTE_T1222183101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct  MultiLinePropertyAttribute_t1222183101  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.MultiLinePropertyAttribute::Lines
	int32_t ___Lines_0;

public:
	inline static int32_t get_offset_of_Lines_0() { return static_cast<int32_t>(offsetof(MultiLinePropertyAttribute_t1222183101, ___Lines_0)); }
	inline int32_t get_Lines_0() const { return ___Lines_0; }
	inline int32_t* get_address_of_Lines_0() { return &___Lines_0; }
	inline void set_Lines_0(int32_t value)
	{
		___Lines_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILINEPROPERTYATTRIBUTE_T1222183101_H
#ifndef ONVALUECHANGEDATTRIBUTE_T870857610_H
#define ONVALUECHANGEDATTRIBUTE_T870857610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.OnValueChangedAttribute
struct  OnValueChangedAttribute_t870857610  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.OnValueChangedAttribute::MethodName
	String_t* ___MethodName_0;
	// System.Boolean Sirenix.OdinInspector.OnValueChangedAttribute::IncludeChildren
	bool ___IncludeChildren_1;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t870857610, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_0), value);
	}

	inline static int32_t get_offset_of_IncludeChildren_1() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t870857610, ___IncludeChildren_1)); }
	inline bool get_IncludeChildren_1() const { return ___IncludeChildren_1; }
	inline bool* get_address_of_IncludeChildren_1() { return &___IncludeChildren_1; }
	inline void set_IncludeChildren_1(bool value)
	{
		___IncludeChildren_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALUECHANGEDATTRIBUTE_T870857610_H
#ifndef PROPERTYGROUPATTRIBUTE_T2009328757_H
#define PROPERTYGROUPATTRIBUTE_T2009328757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyGroupAttribute
struct  PropertyGroupAttribute_t2009328757  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupID
	String_t* ___GroupID_0;
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupName
	String_t* ___GroupName_1;
	// System.Int32 Sirenix.OdinInspector.PropertyGroupAttribute::Order
	int32_t ___Order_2;

public:
	inline static int32_t get_offset_of_GroupID_0() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___GroupID_0)); }
	inline String_t* get_GroupID_0() const { return ___GroupID_0; }
	inline String_t** get_address_of_GroupID_0() { return &___GroupID_0; }
	inline void set_GroupID_0(String_t* value)
	{
		___GroupID_0 = value;
		Il2CppCodeGenWriteBarrier((&___GroupID_0), value);
	}

	inline static int32_t get_offset_of_GroupName_1() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___GroupName_1)); }
	inline String_t* get_GroupName_1() const { return ___GroupName_1; }
	inline String_t** get_address_of_GroupName_1() { return &___GroupName_1; }
	inline void set_GroupName_1(String_t* value)
	{
		___GroupName_1 = value;
		Il2CppCodeGenWriteBarrier((&___GroupName_1), value);
	}

	inline static int32_t get_offset_of_Order_2() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___Order_2)); }
	inline int32_t get_Order_2() const { return ___Order_2; }
	inline int32_t* get_address_of_Order_2() { return &___Order_2; }
	inline void set_Order_2(int32_t value)
	{
		___Order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYGROUPATTRIBUTE_T2009328757_H
#ifndef PROPERTYORDERATTRIBUTE_T2594707638_H
#define PROPERTYORDERATTRIBUTE_T2594707638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyOrderAttribute
struct  PropertyOrderAttribute_t2594707638  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.PropertyOrderAttribute::Order
	int32_t ___Order_0;

public:
	inline static int32_t get_offset_of_Order_0() { return static_cast<int32_t>(offsetof(PropertyOrderAttribute_t2594707638, ___Order_0)); }
	inline int32_t get_Order_0() const { return ___Order_0; }
	inline int32_t* get_address_of_Order_0() { return &___Order_0; }
	inline void set_Order_0(int32_t value)
	{
		___Order_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYORDERATTRIBUTE_T2594707638_H
#ifndef PROPERTYRANGEATTRIBUTE_T3334671474_H
#define PROPERTYRANGEATTRIBUTE_T3334671474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyRangeAttribute
struct  PropertyRangeAttribute_t3334671474  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MaxMember
	String_t* ___MaxMember_3;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinMember_2), value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___MaxMember_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYRANGEATTRIBUTE_T3334671474_H
#ifndef PROPERTYSPACEATTRIBUTE_T209179749_H
#define PROPERTYSPACEATTRIBUTE_T209179749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertySpaceAttribute
struct  PropertySpaceAttribute_t209179749  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceBefore
	float ___SpaceBefore_0;
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceAfter
	float ___SpaceAfter_1;

public:
	inline static int32_t get_offset_of_SpaceBefore_0() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_t209179749, ___SpaceBefore_0)); }
	inline float get_SpaceBefore_0() const { return ___SpaceBefore_0; }
	inline float* get_address_of_SpaceBefore_0() { return &___SpaceBefore_0; }
	inline void set_SpaceBefore_0(float value)
	{
		___SpaceBefore_0 = value;
	}

	inline static int32_t get_offset_of_SpaceAfter_1() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_t209179749, ___SpaceAfter_1)); }
	inline float get_SpaceAfter_1() const { return ___SpaceAfter_1; }
	inline float* get_address_of_SpaceAfter_1() { return &___SpaceAfter_1; }
	inline void set_SpaceAfter_1(float value)
	{
		___SpaceAfter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYSPACEATTRIBUTE_T209179749_H
#ifndef PROPERTYTOOLTIPATTRIBUTE_T1909666584_H
#define PROPERTYTOOLTIPATTRIBUTE_T1909666584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyTooltipAttribute
struct  PropertyTooltipAttribute_t1909666584  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.PropertyTooltipAttribute::Tooltip
	String_t* ___Tooltip_0;

public:
	inline static int32_t get_offset_of_Tooltip_0() { return static_cast<int32_t>(offsetof(PropertyTooltipAttribute_t1909666584, ___Tooltip_0)); }
	inline String_t* get_Tooltip_0() const { return ___Tooltip_0; }
	inline String_t** get_address_of_Tooltip_0() { return &___Tooltip_0; }
	inline void set_Tooltip_0(String_t* value)
	{
		___Tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((&___Tooltip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTOOLTIPATTRIBUTE_T1909666584_H
#ifndef READONLYATTRIBUTE_T3311022813_H
#define READONLYATTRIBUTE_T3311022813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ReadOnlyAttribute
struct  ReadOnlyAttribute_t3311022813  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T3311022813_H
#ifndef REGISTERATTRIBUTEATTRIBUTE_T317614997_H
#define REGISTERATTRIBUTEATTRIBUTE_T317614997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.RegisterAttributeAttribute
struct  RegisterAttributeAttribute_t317614997  : public Attribute_t861562559
{
public:
	// System.Type Sirenix.OdinInspector.RegisterAttributeAttribute::AttributeType
	Type_t * ___AttributeType_0;
	// System.String[] Sirenix.OdinInspector.RegisterAttributeAttribute::Categories
	StringU5BU5D_t1281789340* ___Categories_1;

public:
	inline static int32_t get_offset_of_AttributeType_0() { return static_cast<int32_t>(offsetof(RegisterAttributeAttribute_t317614997, ___AttributeType_0)); }
	inline Type_t * get_AttributeType_0() const { return ___AttributeType_0; }
	inline Type_t ** get_address_of_AttributeType_0() { return &___AttributeType_0; }
	inline void set_AttributeType_0(Type_t * value)
	{
		___AttributeType_0 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeType_0), value);
	}

	inline static int32_t get_offset_of_Categories_1() { return static_cast<int32_t>(offsetof(RegisterAttributeAttribute_t317614997, ___Categories_1)); }
	inline StringU5BU5D_t1281789340* get_Categories_1() const { return ___Categories_1; }
	inline StringU5BU5D_t1281789340** get_address_of_Categories_1() { return &___Categories_1; }
	inline void set_Categories_1(StringU5BU5D_t1281789340* value)
	{
		___Categories_1 = value;
		Il2CppCodeGenWriteBarrier((&___Categories_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERATTRIBUTEATTRIBUTE_T317614997_H
#ifndef SCENEOBJECTSONLYATTRIBUTE_T3518277911_H
#define SCENEOBJECTSONLYATTRIBUTE_T3518277911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct  SceneObjectsOnlyAttribute_t3518277911  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEOBJECTSONLYATTRIBUTE_T3518277911_H
#ifndef SHOWDRAWERCHAINATTRIBUTE_T1144545212_H
#define SHOWDRAWERCHAINATTRIBUTE_T1144545212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct  ShowDrawerChainAttribute_t1144545212  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWDRAWERCHAINATTRIBUTE_T1144545212_H
#ifndef SHOWFORPREFABONLYATTRIBUTE_T4233203441_H
#define SHOWFORPREFABONLYATTRIBUTE_T4233203441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct  ShowForPrefabOnlyAttribute_t4233203441  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWFORPREFABONLYATTRIBUTE_T4233203441_H
#ifndef SHOWIFATTRIBUTE_T2614791958_H
#define SHOWIFATTRIBUTE_T2614791958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowIfAttribute
struct  ShowIfAttribute_t2614791958  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ShowIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Boolean Sirenix.OdinInspector.ShowIfAttribute::Animate
	bool ___Animate_1;
	// System.Object Sirenix.OdinInspector.ShowIfAttribute::Value
	RuntimeObject * ___Value_2;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t2614791958, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Animate_1() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t2614791958, ___Animate_1)); }
	inline bool get_Animate_1() const { return ___Animate_1; }
	inline bool* get_address_of_Animate_1() { return &___Animate_1; }
	inline void set_Animate_1(bool value)
	{
		___Animate_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t2614791958, ___Value_2)); }
	inline RuntimeObject * get_Value_2() const { return ___Value_2; }
	inline RuntimeObject ** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(RuntimeObject * value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___Value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFATTRIBUTE_T2614791958_H
#ifndef SHOWININLINEEDITORSATTRIBUTE_T2931332120_H
#define SHOWININLINEEDITORSATTRIBUTE_T2931332120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct  ShowInInlineEditorsAttribute_t2931332120  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWININLINEEDITORSATTRIBUTE_T2931332120_H
#ifndef SHOWININSPECTORATTRIBUTE_T2100412880_H
#define SHOWININSPECTORATTRIBUTE_T2100412880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowInInspectorAttribute
struct  ShowInInspectorAttribute_t2100412880  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWININSPECTORATTRIBUTE_T2100412880_H
#ifndef SHOWODINSERIALIZEDPROPERTIESININSPECTORATTRIBUTE_T1684166398_H
#define SHOWODINSERIALIZEDPROPERTIESININSPECTORATTRIBUTE_T1684166398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct  ShowOdinSerializedPropertiesInInspectorAttribute_t1684166398  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWODINSERIALIZEDPROPERTIESININSPECTORATTRIBUTE_T1684166398_H
#ifndef SHOWPROPERTYRESOLVERATTRIBUTE_T357032921_H
#define SHOWPROPERTYRESOLVERATTRIBUTE_T357032921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct  ShowPropertyResolverAttribute_t357032921  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWPROPERTYRESOLVERATTRIBUTE_T357032921_H
#ifndef SUFFIXLABELATTRIBUTE_T126424634_H
#define SUFFIXLABELATTRIBUTE_T126424634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SuffixLabelAttribute
struct  SuffixLabelAttribute_t126424634  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.SuffixLabelAttribute::Label
	String_t* ___Label_0;
	// System.Boolean Sirenix.OdinInspector.SuffixLabelAttribute::Overlay
	bool ___Overlay_1;

public:
	inline static int32_t get_offset_of_Label_0() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t126424634, ___Label_0)); }
	inline String_t* get_Label_0() const { return ___Label_0; }
	inline String_t** get_address_of_Label_0() { return &___Label_0; }
	inline void set_Label_0(String_t* value)
	{
		___Label_0 = value;
		Il2CppCodeGenWriteBarrier((&___Label_0), value);
	}

	inline static int32_t get_offset_of_Overlay_1() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t126424634, ___Overlay_1)); }
	inline bool get_Overlay_1() const { return ___Overlay_1; }
	inline bool* get_address_of_Overlay_1() { return &___Overlay_1; }
	inline void set_Overlay_1(bool value)
	{
		___Overlay_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUFFIXLABELATTRIBUTE_T126424634_H
#ifndef SUPPRESSINVALIDATTRIBUTEERRORATTRIBUTE_T4105959342_H
#define SUPPRESSINVALIDATTRIBUTEERRORATTRIBUTE_T4105959342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct  SuppressInvalidAttributeErrorAttribute_t4105959342  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPRESSINVALIDATTRIBUTEERRORATTRIBUTE_T4105959342_H
#ifndef TABLECOLUMNWIDTHATTRIBUTE_T1272758069_H
#define TABLECOLUMNWIDTHATTRIBUTE_T1272758069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TableColumnWidthAttribute
struct  TableColumnWidthAttribute_t1272758069  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.TableColumnWidthAttribute::Width
	int32_t ___Width_0;
	// System.Boolean Sirenix.OdinInspector.TableColumnWidthAttribute::Resizable
	bool ___Resizable_1;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t1272758069, ___Width_0)); }
	inline int32_t get_Width_0() const { return ___Width_0; }
	inline int32_t* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(int32_t value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Resizable_1() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t1272758069, ___Resizable_1)); }
	inline bool get_Resizable_1() const { return ___Resizable_1; }
	inline bool* get_address_of_Resizable_1() { return &___Resizable_1; }
	inline void set_Resizable_1(bool value)
	{
		___Resizable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLECOLUMNWIDTHATTRIBUTE_T1272758069_H
#ifndef TABLELISTATTRIBUTE_T4098662806_H
#define TABLELISTATTRIBUTE_T4098662806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TableListAttribute
struct  TableListAttribute_t4098662806  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::NumberOfItemsPerPage
	int32_t ___NumberOfItemsPerPage_0;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::IsReadOnly
	bool ___IsReadOnly_1;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::DefaultMinColumnWidth
	int32_t ___DefaultMinColumnWidth_2;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::ShowIndexLabels
	bool ___ShowIndexLabels_3;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::DrawScrollView
	bool ___DrawScrollView_4;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MinScrollViewHeight
	int32_t ___MinScrollViewHeight_5;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MaxScrollViewHeight
	int32_t ___MaxScrollViewHeight_6;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::AlwaysExpanded
	bool ___AlwaysExpanded_7;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::HideToolbar
	bool ___HideToolbar_8;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::CellPadding
	int32_t ___CellPadding_9;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPagingHasValue
	bool ___showPagingHasValue_10;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPaging
	bool ___showPaging_11;

public:
	inline static int32_t get_offset_of_NumberOfItemsPerPage_0() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___NumberOfItemsPerPage_0)); }
	inline int32_t get_NumberOfItemsPerPage_0() const { return ___NumberOfItemsPerPage_0; }
	inline int32_t* get_address_of_NumberOfItemsPerPage_0() { return &___NumberOfItemsPerPage_0; }
	inline void set_NumberOfItemsPerPage_0(int32_t value)
	{
		___NumberOfItemsPerPage_0 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_1() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___IsReadOnly_1)); }
	inline bool get_IsReadOnly_1() const { return ___IsReadOnly_1; }
	inline bool* get_address_of_IsReadOnly_1() { return &___IsReadOnly_1; }
	inline void set_IsReadOnly_1(bool value)
	{
		___IsReadOnly_1 = value;
	}

	inline static int32_t get_offset_of_DefaultMinColumnWidth_2() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___DefaultMinColumnWidth_2)); }
	inline int32_t get_DefaultMinColumnWidth_2() const { return ___DefaultMinColumnWidth_2; }
	inline int32_t* get_address_of_DefaultMinColumnWidth_2() { return &___DefaultMinColumnWidth_2; }
	inline void set_DefaultMinColumnWidth_2(int32_t value)
	{
		___DefaultMinColumnWidth_2 = value;
	}

	inline static int32_t get_offset_of_ShowIndexLabels_3() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___ShowIndexLabels_3)); }
	inline bool get_ShowIndexLabels_3() const { return ___ShowIndexLabels_3; }
	inline bool* get_address_of_ShowIndexLabels_3() { return &___ShowIndexLabels_3; }
	inline void set_ShowIndexLabels_3(bool value)
	{
		___ShowIndexLabels_3 = value;
	}

	inline static int32_t get_offset_of_DrawScrollView_4() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___DrawScrollView_4)); }
	inline bool get_DrawScrollView_4() const { return ___DrawScrollView_4; }
	inline bool* get_address_of_DrawScrollView_4() { return &___DrawScrollView_4; }
	inline void set_DrawScrollView_4(bool value)
	{
		___DrawScrollView_4 = value;
	}

	inline static int32_t get_offset_of_MinScrollViewHeight_5() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___MinScrollViewHeight_5)); }
	inline int32_t get_MinScrollViewHeight_5() const { return ___MinScrollViewHeight_5; }
	inline int32_t* get_address_of_MinScrollViewHeight_5() { return &___MinScrollViewHeight_5; }
	inline void set_MinScrollViewHeight_5(int32_t value)
	{
		___MinScrollViewHeight_5 = value;
	}

	inline static int32_t get_offset_of_MaxScrollViewHeight_6() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___MaxScrollViewHeight_6)); }
	inline int32_t get_MaxScrollViewHeight_6() const { return ___MaxScrollViewHeight_6; }
	inline int32_t* get_address_of_MaxScrollViewHeight_6() { return &___MaxScrollViewHeight_6; }
	inline void set_MaxScrollViewHeight_6(int32_t value)
	{
		___MaxScrollViewHeight_6 = value;
	}

	inline static int32_t get_offset_of_AlwaysExpanded_7() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___AlwaysExpanded_7)); }
	inline bool get_AlwaysExpanded_7() const { return ___AlwaysExpanded_7; }
	inline bool* get_address_of_AlwaysExpanded_7() { return &___AlwaysExpanded_7; }
	inline void set_AlwaysExpanded_7(bool value)
	{
		___AlwaysExpanded_7 = value;
	}

	inline static int32_t get_offset_of_HideToolbar_8() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___HideToolbar_8)); }
	inline bool get_HideToolbar_8() const { return ___HideToolbar_8; }
	inline bool* get_address_of_HideToolbar_8() { return &___HideToolbar_8; }
	inline void set_HideToolbar_8(bool value)
	{
		___HideToolbar_8 = value;
	}

	inline static int32_t get_offset_of_CellPadding_9() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___CellPadding_9)); }
	inline int32_t get_CellPadding_9() const { return ___CellPadding_9; }
	inline int32_t* get_address_of_CellPadding_9() { return &___CellPadding_9; }
	inline void set_CellPadding_9(int32_t value)
	{
		___CellPadding_9 = value;
	}

	inline static int32_t get_offset_of_showPagingHasValue_10() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___showPagingHasValue_10)); }
	inline bool get_showPagingHasValue_10() const { return ___showPagingHasValue_10; }
	inline bool* get_address_of_showPagingHasValue_10() { return &___showPagingHasValue_10; }
	inline void set_showPagingHasValue_10(bool value)
	{
		___showPagingHasValue_10 = value;
	}

	inline static int32_t get_offset_of_showPaging_11() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___showPaging_11)); }
	inline bool get_showPaging_11() const { return ___showPaging_11; }
	inline bool* get_address_of_showPaging_11() { return &___showPaging_11; }
	inline void set_showPaging_11(bool value)
	{
		___showPaging_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLELISTATTRIBUTE_T4098662806_H
#ifndef TABLEMATRIXATTRIBUTE_T391949620_H
#define TABLEMATRIXATTRIBUTE_T391949620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TableMatrixAttribute
struct  TableMatrixAttribute_t391949620  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::IsReadOnly
	bool ___IsReadOnly_0;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::ResizableColumns
	bool ___ResizableColumns_1;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::VerticalTitle
	String_t* ___VerticalTitle_2;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::HorizontalTitle
	String_t* ___HorizontalTitle_3;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::DrawElementMethod
	String_t* ___DrawElementMethod_4;
	// System.Int32 Sirenix.OdinInspector.TableMatrixAttribute::RowHeight
	int32_t ___RowHeight_5;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::SquareCells
	bool ___SquareCells_6;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideColumnIndices
	bool ___HideColumnIndices_7;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideRowIndices
	bool ___HideRowIndices_8;

public:
	inline static int32_t get_offset_of_IsReadOnly_0() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___IsReadOnly_0)); }
	inline bool get_IsReadOnly_0() const { return ___IsReadOnly_0; }
	inline bool* get_address_of_IsReadOnly_0() { return &___IsReadOnly_0; }
	inline void set_IsReadOnly_0(bool value)
	{
		___IsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_ResizableColumns_1() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___ResizableColumns_1)); }
	inline bool get_ResizableColumns_1() const { return ___ResizableColumns_1; }
	inline bool* get_address_of_ResizableColumns_1() { return &___ResizableColumns_1; }
	inline void set_ResizableColumns_1(bool value)
	{
		___ResizableColumns_1 = value;
	}

	inline static int32_t get_offset_of_VerticalTitle_2() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___VerticalTitle_2)); }
	inline String_t* get_VerticalTitle_2() const { return ___VerticalTitle_2; }
	inline String_t** get_address_of_VerticalTitle_2() { return &___VerticalTitle_2; }
	inline void set_VerticalTitle_2(String_t* value)
	{
		___VerticalTitle_2 = value;
		Il2CppCodeGenWriteBarrier((&___VerticalTitle_2), value);
	}

	inline static int32_t get_offset_of_HorizontalTitle_3() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___HorizontalTitle_3)); }
	inline String_t* get_HorizontalTitle_3() const { return ___HorizontalTitle_3; }
	inline String_t** get_address_of_HorizontalTitle_3() { return &___HorizontalTitle_3; }
	inline void set_HorizontalTitle_3(String_t* value)
	{
		___HorizontalTitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___HorizontalTitle_3), value);
	}

	inline static int32_t get_offset_of_DrawElementMethod_4() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___DrawElementMethod_4)); }
	inline String_t* get_DrawElementMethod_4() const { return ___DrawElementMethod_4; }
	inline String_t** get_address_of_DrawElementMethod_4() { return &___DrawElementMethod_4; }
	inline void set_DrawElementMethod_4(String_t* value)
	{
		___DrawElementMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___DrawElementMethod_4), value);
	}

	inline static int32_t get_offset_of_RowHeight_5() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___RowHeight_5)); }
	inline int32_t get_RowHeight_5() const { return ___RowHeight_5; }
	inline int32_t* get_address_of_RowHeight_5() { return &___RowHeight_5; }
	inline void set_RowHeight_5(int32_t value)
	{
		___RowHeight_5 = value;
	}

	inline static int32_t get_offset_of_SquareCells_6() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___SquareCells_6)); }
	inline bool get_SquareCells_6() const { return ___SquareCells_6; }
	inline bool* get_address_of_SquareCells_6() { return &___SquareCells_6; }
	inline void set_SquareCells_6(bool value)
	{
		___SquareCells_6 = value;
	}

	inline static int32_t get_offset_of_HideColumnIndices_7() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___HideColumnIndices_7)); }
	inline bool get_HideColumnIndices_7() const { return ___HideColumnIndices_7; }
	inline bool* get_address_of_HideColumnIndices_7() { return &___HideColumnIndices_7; }
	inline void set_HideColumnIndices_7(bool value)
	{
		___HideColumnIndices_7 = value;
	}

	inline static int32_t get_offset_of_HideRowIndices_8() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___HideRowIndices_8)); }
	inline bool get_HideRowIndices_8() const { return ___HideRowIndices_8; }
	inline bool* get_address_of_HideRowIndices_8() { return &___HideRowIndices_8; }
	inline void set_HideRowIndices_8(bool value)
	{
		___HideRowIndices_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEMATRIXATTRIBUTE_T391949620_H
#ifndef TOGGLEATTRIBUTE_T3765571124_H
#define TOGGLEATTRIBUTE_T3765571124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ToggleAttribute
struct  ToggleAttribute_t3765571124  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ToggleAttribute::ToggleMemberName
	String_t* ___ToggleMemberName_0;
	// System.Boolean Sirenix.OdinInspector.ToggleAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_1;

public:
	inline static int32_t get_offset_of_ToggleMemberName_0() { return static_cast<int32_t>(offsetof(ToggleAttribute_t3765571124, ___ToggleMemberName_0)); }
	inline String_t* get_ToggleMemberName_0() const { return ___ToggleMemberName_0; }
	inline String_t** get_address_of_ToggleMemberName_0() { return &___ToggleMemberName_0; }
	inline void set_ToggleMemberName_0(String_t* value)
	{
		___ToggleMemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleMemberName_0), value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_1() { return static_cast<int32_t>(offsetof(ToggleAttribute_t3765571124, ___CollapseOthersOnExpand_1)); }
	inline bool get_CollapseOthersOnExpand_1() const { return ___CollapseOthersOnExpand_1; }
	inline bool* get_address_of_CollapseOthersOnExpand_1() { return &___CollapseOthersOnExpand_1; }
	inline void set_CollapseOthersOnExpand_1(bool value)
	{
		___CollapseOthersOnExpand_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEATTRIBUTE_T3765571124_H
#ifndef TOGGLELEFTATTRIBUTE_T2985281771_H
#define TOGGLELEFTATTRIBUTE_T2985281771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ToggleLeftAttribute
struct  ToggleLeftAttribute_t2985281771  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLELEFTATTRIBUTE_T2985281771_H
#ifndef TYPEFILTERATTRIBUTE_T642666696_H
#define TYPEFILTERATTRIBUTE_T642666696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TypeFilterAttribute
struct  TypeFilterAttribute_t642666696  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::MemberName
	String_t* ___MemberName_0;
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::DropdownTitle
	String_t* ___DropdownTitle_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t642666696, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_DropdownTitle_1() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t642666696, ___DropdownTitle_1)); }
	inline String_t* get_DropdownTitle_1() const { return ___DropdownTitle_1; }
	inline String_t** get_address_of_DropdownTitle_1() { return &___DropdownTitle_1; }
	inline void set_DropdownTitle_1(String_t* value)
	{
		___DropdownTitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___DropdownTitle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERATTRIBUTE_T642666696_H
#ifndef TYPEINFOBOXATTRIBUTE_T3273027527_H
#define TYPEINFOBOXATTRIBUTE_T3273027527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct  TypeInfoBoxAttribute_t3273027527  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.TypeInfoBoxAttribute::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(TypeInfoBoxAttribute_t3273027527, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFOBOXATTRIBUTE_T3273027527_H
#ifndef VALUEDROPDOWNATTRIBUTE_T2212993645_H
#define VALUEDROPDOWNATTRIBUTE_T2212993645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ValueDropdownAttribute
struct  ValueDropdownAttribute_t2212993645  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::NumberOfItemsBeforeEnablingSearch
	int32_t ___NumberOfItemsBeforeEnablingSearch_1;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::IsUniqueList
	bool ___IsUniqueList_2;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DrawDropdownForListElements
	bool ___DrawDropdownForListElements_3;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableListAddButtonBehaviour
	bool ___DisableListAddButtonBehaviour_4;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExcludeExistingValuesInList
	bool ___ExcludeExistingValuesInList_5;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExpandAllMenuItems
	bool ___ExpandAllMenuItems_6;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::AppendNextDrawer
	bool ___AppendNextDrawer_7;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableGUIInAppendedDrawer
	bool ___DisableGUIInAppendedDrawer_8;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DoubleClickToConfirm
	bool ___DoubleClickToConfirm_9;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::FlattenTreeView
	bool ___FlattenTreeView_10;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownWidth
	int32_t ___DropdownWidth_11;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownHeight
	int32_t ___DropdownHeight_12;
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::DropdownTitle
	String_t* ___DropdownTitle_13;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::SortDropdownItems
	bool ___SortDropdownItems_14;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::HideChildProperties
	bool ___HideChildProperties_15;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_NumberOfItemsBeforeEnablingSearch_1() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___NumberOfItemsBeforeEnablingSearch_1)); }
	inline int32_t get_NumberOfItemsBeforeEnablingSearch_1() const { return ___NumberOfItemsBeforeEnablingSearch_1; }
	inline int32_t* get_address_of_NumberOfItemsBeforeEnablingSearch_1() { return &___NumberOfItemsBeforeEnablingSearch_1; }
	inline void set_NumberOfItemsBeforeEnablingSearch_1(int32_t value)
	{
		___NumberOfItemsBeforeEnablingSearch_1 = value;
	}

	inline static int32_t get_offset_of_IsUniqueList_2() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___IsUniqueList_2)); }
	inline bool get_IsUniqueList_2() const { return ___IsUniqueList_2; }
	inline bool* get_address_of_IsUniqueList_2() { return &___IsUniqueList_2; }
	inline void set_IsUniqueList_2(bool value)
	{
		___IsUniqueList_2 = value;
	}

	inline static int32_t get_offset_of_DrawDropdownForListElements_3() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DrawDropdownForListElements_3)); }
	inline bool get_DrawDropdownForListElements_3() const { return ___DrawDropdownForListElements_3; }
	inline bool* get_address_of_DrawDropdownForListElements_3() { return &___DrawDropdownForListElements_3; }
	inline void set_DrawDropdownForListElements_3(bool value)
	{
		___DrawDropdownForListElements_3 = value;
	}

	inline static int32_t get_offset_of_DisableListAddButtonBehaviour_4() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DisableListAddButtonBehaviour_4)); }
	inline bool get_DisableListAddButtonBehaviour_4() const { return ___DisableListAddButtonBehaviour_4; }
	inline bool* get_address_of_DisableListAddButtonBehaviour_4() { return &___DisableListAddButtonBehaviour_4; }
	inline void set_DisableListAddButtonBehaviour_4(bool value)
	{
		___DisableListAddButtonBehaviour_4 = value;
	}

	inline static int32_t get_offset_of_ExcludeExistingValuesInList_5() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___ExcludeExistingValuesInList_5)); }
	inline bool get_ExcludeExistingValuesInList_5() const { return ___ExcludeExistingValuesInList_5; }
	inline bool* get_address_of_ExcludeExistingValuesInList_5() { return &___ExcludeExistingValuesInList_5; }
	inline void set_ExcludeExistingValuesInList_5(bool value)
	{
		___ExcludeExistingValuesInList_5 = value;
	}

	inline static int32_t get_offset_of_ExpandAllMenuItems_6() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___ExpandAllMenuItems_6)); }
	inline bool get_ExpandAllMenuItems_6() const { return ___ExpandAllMenuItems_6; }
	inline bool* get_address_of_ExpandAllMenuItems_6() { return &___ExpandAllMenuItems_6; }
	inline void set_ExpandAllMenuItems_6(bool value)
	{
		___ExpandAllMenuItems_6 = value;
	}

	inline static int32_t get_offset_of_AppendNextDrawer_7() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___AppendNextDrawer_7)); }
	inline bool get_AppendNextDrawer_7() const { return ___AppendNextDrawer_7; }
	inline bool* get_address_of_AppendNextDrawer_7() { return &___AppendNextDrawer_7; }
	inline void set_AppendNextDrawer_7(bool value)
	{
		___AppendNextDrawer_7 = value;
	}

	inline static int32_t get_offset_of_DisableGUIInAppendedDrawer_8() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DisableGUIInAppendedDrawer_8)); }
	inline bool get_DisableGUIInAppendedDrawer_8() const { return ___DisableGUIInAppendedDrawer_8; }
	inline bool* get_address_of_DisableGUIInAppendedDrawer_8() { return &___DisableGUIInAppendedDrawer_8; }
	inline void set_DisableGUIInAppendedDrawer_8(bool value)
	{
		___DisableGUIInAppendedDrawer_8 = value;
	}

	inline static int32_t get_offset_of_DoubleClickToConfirm_9() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DoubleClickToConfirm_9)); }
	inline bool get_DoubleClickToConfirm_9() const { return ___DoubleClickToConfirm_9; }
	inline bool* get_address_of_DoubleClickToConfirm_9() { return &___DoubleClickToConfirm_9; }
	inline void set_DoubleClickToConfirm_9(bool value)
	{
		___DoubleClickToConfirm_9 = value;
	}

	inline static int32_t get_offset_of_FlattenTreeView_10() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___FlattenTreeView_10)); }
	inline bool get_FlattenTreeView_10() const { return ___FlattenTreeView_10; }
	inline bool* get_address_of_FlattenTreeView_10() { return &___FlattenTreeView_10; }
	inline void set_FlattenTreeView_10(bool value)
	{
		___FlattenTreeView_10 = value;
	}

	inline static int32_t get_offset_of_DropdownWidth_11() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DropdownWidth_11)); }
	inline int32_t get_DropdownWidth_11() const { return ___DropdownWidth_11; }
	inline int32_t* get_address_of_DropdownWidth_11() { return &___DropdownWidth_11; }
	inline void set_DropdownWidth_11(int32_t value)
	{
		___DropdownWidth_11 = value;
	}

	inline static int32_t get_offset_of_DropdownHeight_12() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DropdownHeight_12)); }
	inline int32_t get_DropdownHeight_12() const { return ___DropdownHeight_12; }
	inline int32_t* get_address_of_DropdownHeight_12() { return &___DropdownHeight_12; }
	inline void set_DropdownHeight_12(int32_t value)
	{
		___DropdownHeight_12 = value;
	}

	inline static int32_t get_offset_of_DropdownTitle_13() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DropdownTitle_13)); }
	inline String_t* get_DropdownTitle_13() const { return ___DropdownTitle_13; }
	inline String_t** get_address_of_DropdownTitle_13() { return &___DropdownTitle_13; }
	inline void set_DropdownTitle_13(String_t* value)
	{
		___DropdownTitle_13 = value;
		Il2CppCodeGenWriteBarrier((&___DropdownTitle_13), value);
	}

	inline static int32_t get_offset_of_SortDropdownItems_14() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___SortDropdownItems_14)); }
	inline bool get_SortDropdownItems_14() const { return ___SortDropdownItems_14; }
	inline bool* get_address_of_SortDropdownItems_14() { return &___SortDropdownItems_14; }
	inline void set_SortDropdownItems_14(bool value)
	{
		___SortDropdownItems_14 = value;
	}

	inline static int32_t get_offset_of_HideChildProperties_15() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___HideChildProperties_15)); }
	inline bool get_HideChildProperties_15() const { return ___HideChildProperties_15; }
	inline bool* get_address_of_HideChildProperties_15() { return &___HideChildProperties_15; }
	inline void set_HideChildProperties_15(bool value)
	{
		___HideChildProperties_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEDROPDOWNATTRIBUTE_T2212993645_H
#ifndef VALUEDROPDOWNITEM_T3457888829_H
#define VALUEDROPDOWNITEM_T3457888829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ValueDropdownItem
struct  ValueDropdownItem_t3457888829 
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownItem::Text
	String_t* ___Text_0;
	// System.Object Sirenix.OdinInspector.ValueDropdownItem::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t3457888829, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t3457888829, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t3457888829_marshaled_pinvoke
{
	char* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};
// Native definition for COM marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t3457888829_marshaled_com
{
	Il2CppChar* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};
#endif // VALUEDROPDOWNITEM_T3457888829_H
#ifndef WRAPATTRIBUTE_T589917550_H
#define WRAPATTRIBUTE_T589917550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.WrapAttribute
struct  WrapAttribute_t589917550  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.WrapAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.WrapAttribute::Max
	double ___Max_1;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(WrapAttribute_t589917550, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(WrapAttribute_t589917550, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPATTRIBUTE_T589917550_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t257213610 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___list_0)); }
	inline List_1_t257213610 * get_list_0() const { return ___list_0; }
	inline List_1_t257213610 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t257213610 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T913802012_H
#define ENUMERATOR_T913802012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t913802012 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3319525431 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___list_0)); }
	inline List_1_t3319525431 * get_list_0() const { return ___list_0; }
	inline List_1_t3319525431 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3319525431 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T913802012_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BOXGROUPATTRIBUTE_T1828532190_H
#define BOXGROUPATTRIBUTE_T1828532190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.BoxGroupAttribute
struct  BoxGroupAttribute_t1828532190  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::ShowLabel
	bool ___ShowLabel_3;
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::CenterLabel
	bool ___CenterLabel_4;

public:
	inline static int32_t get_offset_of_ShowLabel_3() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t1828532190, ___ShowLabel_3)); }
	inline bool get_ShowLabel_3() const { return ___ShowLabel_3; }
	inline bool* get_address_of_ShowLabel_3() { return &___ShowLabel_3; }
	inline void set_ShowLabel_3(bool value)
	{
		___ShowLabel_3 = value;
	}

	inline static int32_t get_offset_of_CenterLabel_4() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t1828532190, ___CenterLabel_4)); }
	inline bool get_CenterLabel_4() const { return ___CenterLabel_4; }
	inline bool* get_address_of_CenterLabel_4() { return &___CenterLabel_4; }
	inline void set_CenterLabel_4(bool value)
	{
		___CenterLabel_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXGROUPATTRIBUTE_T1828532190_H
#ifndef BUTTONGROUPATTRIBUTE_T2757208248_H
#define BUTTONGROUPATTRIBUTE_T2757208248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonGroupAttribute
struct  ButtonGroupAttribute_t2757208248  : public PropertyGroupAttribute_t2009328757
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONGROUPATTRIBUTE_T2757208248_H
#ifndef BUTTONSIZES_T675943090_H
#define BUTTONSIZES_T675943090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonSizes
struct  ButtonSizes_t675943090 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonSizes_t675943090, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSIZES_T675943090_H
#ifndef BUTTONSTYLE_T3769135257_H
#define BUTTONSTYLE_T3769135257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonStyle
struct  ButtonStyle_t3769135257 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStyle_t3769135257, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTYLE_T3769135257_H
#ifndef DICTIONARYDISPLAYOPTIONS_T2340992491_H
#define DICTIONARYDISPLAYOPTIONS_T2340992491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DictionaryDisplayOptions
struct  DictionaryDisplayOptions_t2340992491 
{
public:
	// System.Int32 Sirenix.OdinInspector.DictionaryDisplayOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DictionaryDisplayOptions_t2340992491, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYDISPLAYOPTIONS_T2340992491_H
#ifndef FOLDOUTGROUPATTRIBUTE_T1921228823_H
#define FOLDOUTGROUPATTRIBUTE_T1921228823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.FoldoutGroupAttribute
struct  FoldoutGroupAttribute_t1921228823  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::Expanded
	bool ___Expanded_3;
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::<HasDefinedExpanded>k__BackingField
	bool ___U3CHasDefinedExpandedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_t1921228823, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}

	inline static int32_t get_offset_of_U3CHasDefinedExpandedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_t1921228823, ___U3CHasDefinedExpandedU3Ek__BackingField_4)); }
	inline bool get_U3CHasDefinedExpandedU3Ek__BackingField_4() const { return ___U3CHasDefinedExpandedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasDefinedExpandedU3Ek__BackingField_4() { return &___U3CHasDefinedExpandedU3Ek__BackingField_4; }
	inline void set_U3CHasDefinedExpandedU3Ek__BackingField_4(bool value)
	{
		___U3CHasDefinedExpandedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLDOUTGROUPATTRIBUTE_T1921228823_H
#ifndef GUICOLORATTRIBUTE_T2790983735_H
#define GUICOLORATTRIBUTE_T2790983735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.GUIColorAttribute
struct  GUIColorAttribute_t2790983735  : public Attribute_t861562559
{
public:
	// UnityEngine.Color Sirenix.OdinInspector.GUIColorAttribute::Color
	Color_t2555686324  ___Color_0;
	// System.String Sirenix.OdinInspector.GUIColorAttribute::GetColor
	String_t* ___GetColor_1;

public:
	inline static int32_t get_offset_of_Color_0() { return static_cast<int32_t>(offsetof(GUIColorAttribute_t2790983735, ___Color_0)); }
	inline Color_t2555686324  get_Color_0() const { return ___Color_0; }
	inline Color_t2555686324 * get_address_of_Color_0() { return &___Color_0; }
	inline void set_Color_0(Color_t2555686324  value)
	{
		___Color_0 = value;
	}

	inline static int32_t get_offset_of_GetColor_1() { return static_cast<int32_t>(offsetof(GUIColorAttribute_t2790983735, ___GetColor_1)); }
	inline String_t* get_GetColor_1() const { return ___GetColor_1; }
	inline String_t** get_address_of_GetColor_1() { return &___GetColor_1; }
	inline void set_GetColor_1(String_t* value)
	{
		___GetColor_1 = value;
		Il2CppCodeGenWriteBarrier((&___GetColor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUICOLORATTRIBUTE_T2790983735_H
#ifndef HORIZONTALGROUPATTRIBUTE_T2886913732_H
#define HORIZONTALGROUPATTRIBUTE_T2886913732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HorizontalGroupAttribute
struct  HorizontalGroupAttribute_t2886913732  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::Width
	float ___Width_3;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginLeft
	float ___MarginLeft_4;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginRight
	float ___MarginRight_5;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingLeft
	float ___PaddingLeft_6;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingRight
	float ___PaddingRight_7;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MinWidth
	float ___MinWidth_8;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MaxWidth
	float ___MaxWidth_9;
	// System.String Sirenix.OdinInspector.HorizontalGroupAttribute::Title
	String_t* ___Title_10;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::LabelWidth
	float ___LabelWidth_11;

public:
	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___Width_3)); }
	inline float get_Width_3() const { return ___Width_3; }
	inline float* get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(float value)
	{
		___Width_3 = value;
	}

	inline static int32_t get_offset_of_MarginLeft_4() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MarginLeft_4)); }
	inline float get_MarginLeft_4() const { return ___MarginLeft_4; }
	inline float* get_address_of_MarginLeft_4() { return &___MarginLeft_4; }
	inline void set_MarginLeft_4(float value)
	{
		___MarginLeft_4 = value;
	}

	inline static int32_t get_offset_of_MarginRight_5() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MarginRight_5)); }
	inline float get_MarginRight_5() const { return ___MarginRight_5; }
	inline float* get_address_of_MarginRight_5() { return &___MarginRight_5; }
	inline void set_MarginRight_5(float value)
	{
		___MarginRight_5 = value;
	}

	inline static int32_t get_offset_of_PaddingLeft_6() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___PaddingLeft_6)); }
	inline float get_PaddingLeft_6() const { return ___PaddingLeft_6; }
	inline float* get_address_of_PaddingLeft_6() { return &___PaddingLeft_6; }
	inline void set_PaddingLeft_6(float value)
	{
		___PaddingLeft_6 = value;
	}

	inline static int32_t get_offset_of_PaddingRight_7() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___PaddingRight_7)); }
	inline float get_PaddingRight_7() const { return ___PaddingRight_7; }
	inline float* get_address_of_PaddingRight_7() { return &___PaddingRight_7; }
	inline void set_PaddingRight_7(float value)
	{
		___PaddingRight_7 = value;
	}

	inline static int32_t get_offset_of_MinWidth_8() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MinWidth_8)); }
	inline float get_MinWidth_8() const { return ___MinWidth_8; }
	inline float* get_address_of_MinWidth_8() { return &___MinWidth_8; }
	inline void set_MinWidth_8(float value)
	{
		___MinWidth_8 = value;
	}

	inline static int32_t get_offset_of_MaxWidth_9() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MaxWidth_9)); }
	inline float get_MaxWidth_9() const { return ___MaxWidth_9; }
	inline float* get_address_of_MaxWidth_9() { return &___MaxWidth_9; }
	inline void set_MaxWidth_9(float value)
	{
		___MaxWidth_9 = value;
	}

	inline static int32_t get_offset_of_Title_10() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___Title_10)); }
	inline String_t* get_Title_10() const { return ___Title_10; }
	inline String_t** get_address_of_Title_10() { return &___Title_10; }
	inline void set_Title_10(String_t* value)
	{
		___Title_10 = value;
		Il2CppCodeGenWriteBarrier((&___Title_10), value);
	}

	inline static int32_t get_offset_of_LabelWidth_11() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___LabelWidth_11)); }
	inline float get_LabelWidth_11() const { return ___LabelWidth_11; }
	inline float* get_address_of_LabelWidth_11() { return &___LabelWidth_11; }
	inline void set_LabelWidth_11(float value)
	{
		___LabelWidth_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALGROUPATTRIBUTE_T2886913732_H
#ifndef INFOMESSAGETYPE_T2920171874_H
#define INFOMESSAGETYPE_T2920171874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InfoMessageType
struct  InfoMessageType_t2920171874 
{
public:
	// System.Int32 Sirenix.OdinInspector.InfoMessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InfoMessageType_t2920171874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFOMESSAGETYPE_T2920171874_H
#ifndef INLINEEDITORMODES_T2896600093_H
#define INLINEEDITORMODES_T2896600093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorModes
struct  InlineEditorModes_t2896600093 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorModes_t2896600093, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITORMODES_T2896600093_H
#ifndef INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#define INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorObjectFieldModes
struct  InlineEditorObjectFieldModes_t1823869811 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorObjectFieldModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorObjectFieldModes_t1823869811, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#ifndef OBJECTFIELDALIGNMENT_T2976484322_H
#define OBJECTFIELDALIGNMENT_T2976484322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ObjectFieldAlignment
struct  ObjectFieldAlignment_t2976484322 
{
public:
	// System.Int32 Sirenix.OdinInspector.ObjectFieldAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectFieldAlignment_t2976484322, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTFIELDALIGNMENT_T2976484322_H
#ifndef ONINSPECTORGUIATTRIBUTE_T3458889380_H
#define ONINSPECTORGUIATTRIBUTE_T3458889380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct  OnInspectorGUIAttribute_t3458889380  : public ShowInInspectorAttribute_t2100412880
{
public:
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::PrependMethodName
	String_t* ___PrependMethodName_0;
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::AppendMethodName
	String_t* ___AppendMethodName_1;

public:
	inline static int32_t get_offset_of_PrependMethodName_0() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t3458889380, ___PrependMethodName_0)); }
	inline String_t* get_PrependMethodName_0() const { return ___PrependMethodName_0; }
	inline String_t** get_address_of_PrependMethodName_0() { return &___PrependMethodName_0; }
	inline void set_PrependMethodName_0(String_t* value)
	{
		___PrependMethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___PrependMethodName_0), value);
	}

	inline static int32_t get_offset_of_AppendMethodName_1() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t3458889380, ___AppendMethodName_1)); }
	inline String_t* get_AppendMethodName_1() const { return ___AppendMethodName_1; }
	inline String_t** get_address_of_AppendMethodName_1() { return &___AppendMethodName_1; }
	inline void set_AppendMethodName_1(String_t* value)
	{
		___AppendMethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___AppendMethodName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINSPECTORGUIATTRIBUTE_T3458889380_H
#ifndef TABGROUPATTRIBUTE_T2295443652_H
#define TABGROUPATTRIBUTE_T2295443652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TabGroupAttribute
struct  TabGroupAttribute_t2295443652  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.String Sirenix.OdinInspector.TabGroupAttribute::TabName
	String_t* ___TabName_4;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::UseFixedHeight
	bool ___UseFixedHeight_5;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::Paddingless
	bool ___Paddingless_6;
	// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::<Tabs>k__BackingField
	List_1_t3319525431 * ___U3CTabsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_TabName_4() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___TabName_4)); }
	inline String_t* get_TabName_4() const { return ___TabName_4; }
	inline String_t** get_address_of_TabName_4() { return &___TabName_4; }
	inline void set_TabName_4(String_t* value)
	{
		___TabName_4 = value;
		Il2CppCodeGenWriteBarrier((&___TabName_4), value);
	}

	inline static int32_t get_offset_of_UseFixedHeight_5() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___UseFixedHeight_5)); }
	inline bool get_UseFixedHeight_5() const { return ___UseFixedHeight_5; }
	inline bool* get_address_of_UseFixedHeight_5() { return &___UseFixedHeight_5; }
	inline void set_UseFixedHeight_5(bool value)
	{
		___UseFixedHeight_5 = value;
	}

	inline static int32_t get_offset_of_Paddingless_6() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___Paddingless_6)); }
	inline bool get_Paddingless_6() const { return ___Paddingless_6; }
	inline bool* get_address_of_Paddingless_6() { return &___Paddingless_6; }
	inline void set_Paddingless_6(bool value)
	{
		___Paddingless_6 = value;
	}

	inline static int32_t get_offset_of_U3CTabsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___U3CTabsU3Ek__BackingField_7)); }
	inline List_1_t3319525431 * get_U3CTabsU3Ek__BackingField_7() const { return ___U3CTabsU3Ek__BackingField_7; }
	inline List_1_t3319525431 ** get_address_of_U3CTabsU3Ek__BackingField_7() { return &___U3CTabsU3Ek__BackingField_7; }
	inline void set_U3CTabsU3Ek__BackingField_7(List_1_t3319525431 * value)
	{
		___U3CTabsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTabsU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABGROUPATTRIBUTE_T2295443652_H
#ifndef TABSUBGROUPATTRIBUTE_T867681421_H
#define TABSUBGROUPATTRIBUTE_T867681421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct  TabSubGroupAttribute_t867681421  : public PropertyGroupAttribute_t2009328757
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABSUBGROUPATTRIBUTE_T867681421_H
#ifndef TITLEALIGNMENTS_T2525535876_H
#define TITLEALIGNMENTS_T2525535876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TitleAlignments
struct  TitleAlignments_t2525535876 
{
public:
	// System.Int32 Sirenix.OdinInspector.TitleAlignments::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TitleAlignments_t2525535876, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEALIGNMENTS_T2525535876_H
#ifndef TOGGLEGROUPATTRIBUTE_T12117724_H
#define TOGGLEGROUPATTRIBUTE_T12117724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ToggleGroupAttribute
struct  ToggleGroupAttribute_t12117724  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::ToggleGroupTitle
	String_t* ___ToggleGroupTitle_3;
	// System.Boolean Sirenix.OdinInspector.ToggleGroupAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_4;
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::<TitleStringMemberName>k__BackingField
	String_t* ___U3CTitleStringMemberNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_ToggleGroupTitle_3() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t12117724, ___ToggleGroupTitle_3)); }
	inline String_t* get_ToggleGroupTitle_3() const { return ___ToggleGroupTitle_3; }
	inline String_t** get_address_of_ToggleGroupTitle_3() { return &___ToggleGroupTitle_3; }
	inline void set_ToggleGroupTitle_3(String_t* value)
	{
		___ToggleGroupTitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleGroupTitle_3), value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_4() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t12117724, ___CollapseOthersOnExpand_4)); }
	inline bool get_CollapseOthersOnExpand_4() const { return ___CollapseOthersOnExpand_4; }
	inline bool* get_address_of_CollapseOthersOnExpand_4() { return &___CollapseOthersOnExpand_4; }
	inline void set_CollapseOthersOnExpand_4(bool value)
	{
		___CollapseOthersOnExpand_4 = value;
	}

	inline static int32_t get_offset_of_U3CTitleStringMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t12117724, ___U3CTitleStringMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CTitleStringMemberNameU3Ek__BackingField_5() const { return ___U3CTitleStringMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTitleStringMemberNameU3Ek__BackingField_5() { return &___U3CTitleStringMemberNameU3Ek__BackingField_5; }
	inline void set_U3CTitleStringMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CTitleStringMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleStringMemberNameU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEGROUPATTRIBUTE_T12117724_H
#ifndef VERTICALGROUPATTRIBUTE_T4076833965_H
#define VERTICALGROUPATTRIBUTE_T4076833965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.VerticalGroupAttribute
struct  VerticalGroupAttribute_t4076833965  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingTop
	float ___PaddingTop_3;
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingBottom
	float ___PaddingBottom_4;

public:
	inline static int32_t get_offset_of_PaddingTop_3() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_t4076833965, ___PaddingTop_3)); }
	inline float get_PaddingTop_3() const { return ___PaddingTop_3; }
	inline float* get_address_of_PaddingTop_3() { return &___PaddingTop_3; }
	inline void set_PaddingTop_3(float value)
	{
		___PaddingTop_3 = value;
	}

	inline static int32_t get_offset_of_PaddingBottom_4() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_t4076833965, ___PaddingBottom_4)); }
	inline float get_PaddingBottom_4() const { return ___PaddingBottom_4; }
	inline float* get_address_of_PaddingBottom_4() { return &___PaddingBottom_4; }
	inline void set_PaddingBottom_4(float value)
	{
		___PaddingBottom_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALGROUPATTRIBUTE_T4076833965_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef ATTRIBUTETARGETS_T1784037988_H
#define ATTRIBUTETARGETS_T1784037988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AttributeTargets
struct  AttributeTargets_t1784037988 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t1784037988, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETS_T1784037988_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#define NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t3489357830  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef TEXTALIGNMENT_T822270402_H
#define TEXTALIGNMENT_T822270402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAlignment
struct  TextAlignment_t822270402 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_t822270402, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENT_T822270402_H
#ifndef ATTRIBUTETARGETFLAGS_T870309191_H
#define ATTRIBUTETARGETFLAGS_T870309191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.AttributeTargetFlags
struct  AttributeTargetFlags_t870309191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETFLAGS_T870309191_H
#ifndef BUTTONATTRIBUTE_T3418195108_H
#define BUTTONATTRIBUTE_T3418195108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonAttribute
struct  ButtonAttribute_t3418195108  : public ShowInInspectorAttribute_t2100412880
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonAttribute::ButtonHeight
	int32_t ___ButtonHeight_0;
	// System.String Sirenix.OdinInspector.ButtonAttribute::Name
	String_t* ___Name_1;
	// Sirenix.OdinInspector.ButtonStyle Sirenix.OdinInspector.ButtonAttribute::Style
	int32_t ___Style_2;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::Expanded
	bool ___Expanded_3;

public:
	inline static int32_t get_offset_of_ButtonHeight_0() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___ButtonHeight_0)); }
	inline int32_t get_ButtonHeight_0() const { return ___ButtonHeight_0; }
	inline int32_t* get_address_of_ButtonHeight_0() { return &___ButtonHeight_0; }
	inline void set_ButtonHeight_0(int32_t value)
	{
		___ButtonHeight_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Style_2() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___Style_2)); }
	inline int32_t get_Style_2() const { return ___Style_2; }
	inline int32_t* get_address_of_Style_2() { return &___Style_2; }
	inline void set_Style_2(int32_t value)
	{
		___Style_2 = value;
	}

	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONATTRIBUTE_T3418195108_H
#ifndef DETAILEDINFOBOXATTRIBUTE_T2666061416_H
#define DETAILEDINFOBOXATTRIBUTE_T2666061416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct  DetailedInfoBoxAttribute_t2666061416  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Message
	String_t* ___Message_0;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Details
	String_t* ___Details_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.DetailedInfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_2;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_3;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}

	inline static int32_t get_offset_of_Details_1() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___Details_1)); }
	inline String_t* get_Details_1() const { return ___Details_1; }
	inline String_t** get_address_of_Details_1() { return &___Details_1; }
	inline void set_Details_1(String_t* value)
	{
		___Details_1 = value;
		Il2CppCodeGenWriteBarrier((&___Details_1), value);
	}

	inline static int32_t get_offset_of_InfoMessageType_2() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___InfoMessageType_2)); }
	inline int32_t get_InfoMessageType_2() const { return ___InfoMessageType_2; }
	inline int32_t* get_address_of_InfoMessageType_2() { return &___InfoMessageType_2; }
	inline void set_InfoMessageType_2(int32_t value)
	{
		___InfoMessageType_2 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_3() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___VisibleIf_3)); }
	inline String_t* get_VisibleIf_3() const { return ___VisibleIf_3; }
	inline String_t** get_address_of_VisibleIf_3() { return &___VisibleIf_3; }
	inline void set_VisibleIf_3(String_t* value)
	{
		___VisibleIf_3 = value;
		Il2CppCodeGenWriteBarrier((&___VisibleIf_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETAILEDINFOBOXATTRIBUTE_T2666061416_H
#ifndef DICTIONARYDRAWERSETTINGS_T3408197424_H
#define DICTIONARYDRAWERSETTINGS_T3408197424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DictionaryDrawerSettings
struct  DictionaryDrawerSettings_t3408197424  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::KeyLabel
	String_t* ___KeyLabel_0;
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::ValueLabel
	String_t* ___ValueLabel_1;
	// Sirenix.OdinInspector.DictionaryDisplayOptions Sirenix.OdinInspector.DictionaryDrawerSettings::DisplayMode
	int32_t ___DisplayMode_2;
	// System.Boolean Sirenix.OdinInspector.DictionaryDrawerSettings::IsReadOnly
	bool ___IsReadOnly_3;

public:
	inline static int32_t get_offset_of_KeyLabel_0() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___KeyLabel_0)); }
	inline String_t* get_KeyLabel_0() const { return ___KeyLabel_0; }
	inline String_t** get_address_of_KeyLabel_0() { return &___KeyLabel_0; }
	inline void set_KeyLabel_0(String_t* value)
	{
		___KeyLabel_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeyLabel_0), value);
	}

	inline static int32_t get_offset_of_ValueLabel_1() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___ValueLabel_1)); }
	inline String_t* get_ValueLabel_1() const { return ___ValueLabel_1; }
	inline String_t** get_address_of_ValueLabel_1() { return &___ValueLabel_1; }
	inline void set_ValueLabel_1(String_t* value)
	{
		___ValueLabel_1 = value;
		Il2CppCodeGenWriteBarrier((&___ValueLabel_1), value);
	}

	inline static int32_t get_offset_of_DisplayMode_2() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___DisplayMode_2)); }
	inline int32_t get_DisplayMode_2() const { return ___DisplayMode_2; }
	inline int32_t* get_address_of_DisplayMode_2() { return &___DisplayMode_2; }
	inline void set_DisplayMode_2(int32_t value)
	{
		___DisplayMode_2 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_3() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___IsReadOnly_3)); }
	inline bool get_IsReadOnly_3() const { return ___IsReadOnly_3; }
	inline bool* get_address_of_IsReadOnly_3() { return &___IsReadOnly_3; }
	inline void set_IsReadOnly_3(bool value)
	{
		___IsReadOnly_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYDRAWERSETTINGS_T3408197424_H
#ifndef INFOBOXATTRIBUTE_T1629795541_H
#define INFOBOXATTRIBUTE_T1629795541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InfoBoxAttribute
struct  InfoBoxAttribute_t1629795541  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::Message
	String_t* ___Message_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.InfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_1;
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_2;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t1629795541, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}

	inline static int32_t get_offset_of_InfoMessageType_1() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t1629795541, ___InfoMessageType_1)); }
	inline int32_t get_InfoMessageType_1() const { return ___InfoMessageType_1; }
	inline int32_t* get_address_of_InfoMessageType_1() { return &___InfoMessageType_1; }
	inline void set_InfoMessageType_1(int32_t value)
	{
		___InfoMessageType_1 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_2() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t1629795541, ___VisibleIf_2)); }
	inline String_t* get_VisibleIf_2() const { return ___VisibleIf_2; }
	inline String_t** get_address_of_VisibleIf_2() { return &___VisibleIf_2; }
	inline void set_VisibleIf_2(String_t* value)
	{
		___VisibleIf_2 = value;
		Il2CppCodeGenWriteBarrier((&___VisibleIf_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFOBOXATTRIBUTE_T1629795541_H
#ifndef INLINEEDITORATTRIBUTE_T803766710_H
#define INLINEEDITORATTRIBUTE_T803766710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorAttribute
struct  InlineEditorAttribute_t803766710  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::Expanded
	bool ___Expanded_0;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawHeader
	bool ___DrawHeader_1;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawGUI
	bool ___DrawGUI_2;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawPreview
	bool ___DrawPreview_3;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::MaxHeight
	float ___MaxHeight_4;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewWidth
	float ___PreviewWidth_5;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewHeight
	float ___PreviewHeight_6;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::IncrementInlineEditorDrawerDepth
	bool ___IncrementInlineEditorDrawerDepth_7;
	// Sirenix.OdinInspector.InlineEditorObjectFieldModes Sirenix.OdinInspector.InlineEditorAttribute::ObjectFieldMode
	int32_t ___ObjectFieldMode_8;

public:
	inline static int32_t get_offset_of_Expanded_0() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___Expanded_0)); }
	inline bool get_Expanded_0() const { return ___Expanded_0; }
	inline bool* get_address_of_Expanded_0() { return &___Expanded_0; }
	inline void set_Expanded_0(bool value)
	{
		___Expanded_0 = value;
	}

	inline static int32_t get_offset_of_DrawHeader_1() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___DrawHeader_1)); }
	inline bool get_DrawHeader_1() const { return ___DrawHeader_1; }
	inline bool* get_address_of_DrawHeader_1() { return &___DrawHeader_1; }
	inline void set_DrawHeader_1(bool value)
	{
		___DrawHeader_1 = value;
	}

	inline static int32_t get_offset_of_DrawGUI_2() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___DrawGUI_2)); }
	inline bool get_DrawGUI_2() const { return ___DrawGUI_2; }
	inline bool* get_address_of_DrawGUI_2() { return &___DrawGUI_2; }
	inline void set_DrawGUI_2(bool value)
	{
		___DrawGUI_2 = value;
	}

	inline static int32_t get_offset_of_DrawPreview_3() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___DrawPreview_3)); }
	inline bool get_DrawPreview_3() const { return ___DrawPreview_3; }
	inline bool* get_address_of_DrawPreview_3() { return &___DrawPreview_3; }
	inline void set_DrawPreview_3(bool value)
	{
		___DrawPreview_3 = value;
	}

	inline static int32_t get_offset_of_MaxHeight_4() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___MaxHeight_4)); }
	inline float get_MaxHeight_4() const { return ___MaxHeight_4; }
	inline float* get_address_of_MaxHeight_4() { return &___MaxHeight_4; }
	inline void set_MaxHeight_4(float value)
	{
		___MaxHeight_4 = value;
	}

	inline static int32_t get_offset_of_PreviewWidth_5() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___PreviewWidth_5)); }
	inline float get_PreviewWidth_5() const { return ___PreviewWidth_5; }
	inline float* get_address_of_PreviewWidth_5() { return &___PreviewWidth_5; }
	inline void set_PreviewWidth_5(float value)
	{
		___PreviewWidth_5 = value;
	}

	inline static int32_t get_offset_of_PreviewHeight_6() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___PreviewHeight_6)); }
	inline float get_PreviewHeight_6() const { return ___PreviewHeight_6; }
	inline float* get_address_of_PreviewHeight_6() { return &___PreviewHeight_6; }
	inline void set_PreviewHeight_6(float value)
	{
		___PreviewHeight_6 = value;
	}

	inline static int32_t get_offset_of_IncrementInlineEditorDrawerDepth_7() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___IncrementInlineEditorDrawerDepth_7)); }
	inline bool get_IncrementInlineEditorDrawerDepth_7() const { return ___IncrementInlineEditorDrawerDepth_7; }
	inline bool* get_address_of_IncrementInlineEditorDrawerDepth_7() { return &___IncrementInlineEditorDrawerDepth_7; }
	inline void set_IncrementInlineEditorDrawerDepth_7(bool value)
	{
		___IncrementInlineEditorDrawerDepth_7 = value;
	}

	inline static int32_t get_offset_of_ObjectFieldMode_8() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___ObjectFieldMode_8)); }
	inline int32_t get_ObjectFieldMode_8() const { return ___ObjectFieldMode_8; }
	inline int32_t* get_address_of_ObjectFieldMode_8() { return &___ObjectFieldMode_8; }
	inline void set_ObjectFieldMode_8(int32_t value)
	{
		___ObjectFieldMode_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITORATTRIBUTE_T803766710_H
#ifndef PREVIEWFIELDATTRIBUTE_T2856725314_H
#define PREVIEWFIELDATTRIBUTE_T2856725314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PreviewFieldAttribute
struct  PreviewFieldAttribute_t2856725314  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.PreviewFieldAttribute::Height
	float ___Height_0;
	// Sirenix.OdinInspector.ObjectFieldAlignment Sirenix.OdinInspector.PreviewFieldAttribute::Alignment
	int32_t ___Alignment_1;
	// System.Boolean Sirenix.OdinInspector.PreviewFieldAttribute::AlignmentHasValue
	bool ___AlignmentHasValue_2;

public:
	inline static int32_t get_offset_of_Height_0() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t2856725314, ___Height_0)); }
	inline float get_Height_0() const { return ___Height_0; }
	inline float* get_address_of_Height_0() { return &___Height_0; }
	inline void set_Height_0(float value)
	{
		___Height_0 = value;
	}

	inline static int32_t get_offset_of_Alignment_1() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t2856725314, ___Alignment_1)); }
	inline int32_t get_Alignment_1() const { return ___Alignment_1; }
	inline int32_t* get_address_of_Alignment_1() { return &___Alignment_1; }
	inline void set_Alignment_1(int32_t value)
	{
		___Alignment_1 = value;
	}

	inline static int32_t get_offset_of_AlignmentHasValue_2() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t2856725314, ___AlignmentHasValue_2)); }
	inline bool get_AlignmentHasValue_2() const { return ___AlignmentHasValue_2; }
	inline bool* get_address_of_AlignmentHasValue_2() { return &___AlignmentHasValue_2; }
	inline void set_AlignmentHasValue_2(bool value)
	{
		___AlignmentHasValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWFIELDATTRIBUTE_T2856725314_H
#ifndef PROGRESSBARATTRIBUTE_T2414458096_H
#define PROGRESSBARATTRIBUTE_T2414458096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ProgressBarAttribute
struct  ProgressBarAttribute_t2414458096  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MaxMember
	String_t* ___MaxMember_3;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::R
	float ___R_4;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::G
	float ___G_5;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::B
	float ___B_6;
	// System.Int32 Sirenix.OdinInspector.ProgressBarAttribute::Height
	int32_t ___Height_7;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::ColorMember
	String_t* ___ColorMember_8;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::BackgroundColorMember
	String_t* ___BackgroundColorMember_9;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::Segmented
	bool ___Segmented_10;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::CustomValueStringMember
	String_t* ___CustomValueStringMember_11;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::drawValueLabel
	bool ___drawValueLabel_12;
	// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::valueLabelAlignment
	int32_t ___valueLabelAlignment_13;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<DrawValueLabelHasValue>k__BackingField
	bool ___U3CDrawValueLabelHasValueU3Ek__BackingField_14;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<ValueLabelAlignmentHasValue>k__BackingField
	bool ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinMember_2), value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___MaxMember_3), value);
	}

	inline static int32_t get_offset_of_R_4() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___R_4)); }
	inline float get_R_4() const { return ___R_4; }
	inline float* get_address_of_R_4() { return &___R_4; }
	inline void set_R_4(float value)
	{
		___R_4 = value;
	}

	inline static int32_t get_offset_of_G_5() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___G_5)); }
	inline float get_G_5() const { return ___G_5; }
	inline float* get_address_of_G_5() { return &___G_5; }
	inline void set_G_5(float value)
	{
		___G_5 = value;
	}

	inline static int32_t get_offset_of_B_6() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___B_6)); }
	inline float get_B_6() const { return ___B_6; }
	inline float* get_address_of_B_6() { return &___B_6; }
	inline void set_B_6(float value)
	{
		___B_6 = value;
	}

	inline static int32_t get_offset_of_Height_7() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Height_7)); }
	inline int32_t get_Height_7() const { return ___Height_7; }
	inline int32_t* get_address_of_Height_7() { return &___Height_7; }
	inline void set_Height_7(int32_t value)
	{
		___Height_7 = value;
	}

	inline static int32_t get_offset_of_ColorMember_8() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___ColorMember_8)); }
	inline String_t* get_ColorMember_8() const { return ___ColorMember_8; }
	inline String_t** get_address_of_ColorMember_8() { return &___ColorMember_8; }
	inline void set_ColorMember_8(String_t* value)
	{
		___ColorMember_8 = value;
		Il2CppCodeGenWriteBarrier((&___ColorMember_8), value);
	}

	inline static int32_t get_offset_of_BackgroundColorMember_9() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___BackgroundColorMember_9)); }
	inline String_t* get_BackgroundColorMember_9() const { return ___BackgroundColorMember_9; }
	inline String_t** get_address_of_BackgroundColorMember_9() { return &___BackgroundColorMember_9; }
	inline void set_BackgroundColorMember_9(String_t* value)
	{
		___BackgroundColorMember_9 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundColorMember_9), value);
	}

	inline static int32_t get_offset_of_Segmented_10() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Segmented_10)); }
	inline bool get_Segmented_10() const { return ___Segmented_10; }
	inline bool* get_address_of_Segmented_10() { return &___Segmented_10; }
	inline void set_Segmented_10(bool value)
	{
		___Segmented_10 = value;
	}

	inline static int32_t get_offset_of_CustomValueStringMember_11() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___CustomValueStringMember_11)); }
	inline String_t* get_CustomValueStringMember_11() const { return ___CustomValueStringMember_11; }
	inline String_t** get_address_of_CustomValueStringMember_11() { return &___CustomValueStringMember_11; }
	inline void set_CustomValueStringMember_11(String_t* value)
	{
		___CustomValueStringMember_11 = value;
		Il2CppCodeGenWriteBarrier((&___CustomValueStringMember_11), value);
	}

	inline static int32_t get_offset_of_drawValueLabel_12() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___drawValueLabel_12)); }
	inline bool get_drawValueLabel_12() const { return ___drawValueLabel_12; }
	inline bool* get_address_of_drawValueLabel_12() { return &___drawValueLabel_12; }
	inline void set_drawValueLabel_12(bool value)
	{
		___drawValueLabel_12 = value;
	}

	inline static int32_t get_offset_of_valueLabelAlignment_13() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___valueLabelAlignment_13)); }
	inline int32_t get_valueLabelAlignment_13() const { return ___valueLabelAlignment_13; }
	inline int32_t* get_address_of_valueLabelAlignment_13() { return &___valueLabelAlignment_13; }
	inline void set_valueLabelAlignment_13(int32_t value)
	{
		___valueLabelAlignment_13 = value;
	}

	inline static int32_t get_offset_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___U3CDrawValueLabelHasValueU3Ek__BackingField_14)); }
	inline bool get_U3CDrawValueLabelHasValueU3Ek__BackingField_14() const { return ___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return &___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline void set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(bool value)
	{
		___U3CDrawValueLabelHasValueU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15)); }
	inline bool get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() const { return ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return &___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline void set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(bool value)
	{
		___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBARATTRIBUTE_T2414458096_H
#ifndef REQUIREDATTRIBUTE_T1745431873_H
#define REQUIREDATTRIBUTE_T1745431873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.RequiredAttribute
struct  RequiredAttribute_t1745431873  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.RequiredAttribute::ErrorMessage
	String_t* ___ErrorMessage_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.RequiredAttribute::MessageType
	int32_t ___MessageType_1;

public:
	inline static int32_t get_offset_of_ErrorMessage_0() { return static_cast<int32_t>(offsetof(RequiredAttribute_t1745431873, ___ErrorMessage_0)); }
	inline String_t* get_ErrorMessage_0() const { return ___ErrorMessage_0; }
	inline String_t** get_address_of_ErrorMessage_0() { return &___ErrorMessage_0; }
	inline void set_ErrorMessage_0(String_t* value)
	{
		___ErrorMessage_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorMessage_0), value);
	}

	inline static int32_t get_offset_of_MessageType_1() { return static_cast<int32_t>(offsetof(RequiredAttribute_t1745431873, ___MessageType_1)); }
	inline int32_t get_MessageType_1() const { return ___MessageType_1; }
	inline int32_t* get_address_of_MessageType_1() { return &___MessageType_1; }
	inline void set_MessageType_1(int32_t value)
	{
		___MessageType_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDATTRIBUTE_T1745431873_H
#ifndef RESPONSIVEBUTTONGROUPATTRIBUTE_T1637434916_H
#define RESPONSIVEBUTTONGROUPATTRIBUTE_T1637434916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct  ResponsiveButtonGroupAttribute_t1637434916  : public PropertyGroupAttribute_t2009328757
{
public:
	// Sirenix.OdinInspector.ButtonSizes Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::DefaultButtonSize
	int32_t ___DefaultButtonSize_3;
	// System.Boolean Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::UniformLayout
	bool ___UniformLayout_4;

public:
	inline static int32_t get_offset_of_DefaultButtonSize_3() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t1637434916, ___DefaultButtonSize_3)); }
	inline int32_t get_DefaultButtonSize_3() const { return ___DefaultButtonSize_3; }
	inline int32_t* get_address_of_DefaultButtonSize_3() { return &___DefaultButtonSize_3; }
	inline void set_DefaultButtonSize_3(int32_t value)
	{
		___DefaultButtonSize_3 = value;
	}

	inline static int32_t get_offset_of_UniformLayout_4() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t1637434916, ___UniformLayout_4)); }
	inline bool get_UniformLayout_4() const { return ___UniformLayout_4; }
	inline bool* get_address_of_UniformLayout_4() { return &___UniformLayout_4; }
	inline void set_UniformLayout_4(bool value)
	{
		___UniformLayout_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSIVEBUTTONGROUPATTRIBUTE_T1637434916_H
#ifndef TITLEATTRIBUTE_T3554241315_H
#define TITLEATTRIBUTE_T3554241315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TitleAttribute
struct  TitleAttribute_t3554241315  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.TitleAttribute::Title
	String_t* ___Title_0;
	// System.String Sirenix.OdinInspector.TitleAttribute::Subtitle
	String_t* ___Subtitle_1;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::Bold
	bool ___Bold_2;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::HorizontalLine
	bool ___HorizontalLine_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleAttribute::TitleAlignment
	int32_t ___TitleAlignment_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((&___Title_0), value);
	}

	inline static int32_t get_offset_of_Subtitle_1() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___Subtitle_1)); }
	inline String_t* get_Subtitle_1() const { return ___Subtitle_1; }
	inline String_t** get_address_of_Subtitle_1() { return &___Subtitle_1; }
	inline void set_Subtitle_1(String_t* value)
	{
		___Subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Subtitle_1), value);
	}

	inline static int32_t get_offset_of_Bold_2() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___Bold_2)); }
	inline bool get_Bold_2() const { return ___Bold_2; }
	inline bool* get_address_of_Bold_2() { return &___Bold_2; }
	inline void set_Bold_2(bool value)
	{
		___Bold_2 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_3() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___HorizontalLine_3)); }
	inline bool get_HorizontalLine_3() const { return ___HorizontalLine_3; }
	inline bool* get_address_of_HorizontalLine_3() { return &___HorizontalLine_3; }
	inline void set_HorizontalLine_3(bool value)
	{
		___HorizontalLine_3 = value;
	}

	inline static int32_t get_offset_of_TitleAlignment_4() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___TitleAlignment_4)); }
	inline int32_t get_TitleAlignment_4() const { return ___TitleAlignment_4; }
	inline int32_t* get_address_of_TitleAlignment_4() { return &___TitleAlignment_4; }
	inline void set_TitleAlignment_4(int32_t value)
	{
		___TitleAlignment_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEATTRIBUTE_T3554241315_H
#ifndef TITLEGROUPATTRIBUTE_T2185192947_H
#define TITLEGROUPATTRIBUTE_T2185192947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TitleGroupAttribute
struct  TitleGroupAttribute_t2185192947  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.String Sirenix.OdinInspector.TitleGroupAttribute::Subtitle
	String_t* ___Subtitle_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleGroupAttribute::Alignment
	int32_t ___Alignment_4;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::HorizontalLine
	bool ___HorizontalLine_5;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::BoldTitle
	bool ___BoldTitle_6;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::Indent
	bool ___Indent_7;

public:
	inline static int32_t get_offset_of_Subtitle_3() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___Subtitle_3)); }
	inline String_t* get_Subtitle_3() const { return ___Subtitle_3; }
	inline String_t** get_address_of_Subtitle_3() { return &___Subtitle_3; }
	inline void set_Subtitle_3(String_t* value)
	{
		___Subtitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___Subtitle_3), value);
	}

	inline static int32_t get_offset_of_Alignment_4() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___Alignment_4)); }
	inline int32_t get_Alignment_4() const { return ___Alignment_4; }
	inline int32_t* get_address_of_Alignment_4() { return &___Alignment_4; }
	inline void set_Alignment_4(int32_t value)
	{
		___Alignment_4 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_5() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___HorizontalLine_5)); }
	inline bool get_HorizontalLine_5() const { return ___HorizontalLine_5; }
	inline bool* get_address_of_HorizontalLine_5() { return &___HorizontalLine_5; }
	inline void set_HorizontalLine_5(bool value)
	{
		___HorizontalLine_5 = value;
	}

	inline static int32_t get_offset_of_BoldTitle_6() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___BoldTitle_6)); }
	inline bool get_BoldTitle_6() const { return ___BoldTitle_6; }
	inline bool* get_address_of_BoldTitle_6() { return &___BoldTitle_6; }
	inline void set_BoldTitle_6(bool value)
	{
		___BoldTitle_6 = value;
	}

	inline static int32_t get_offset_of_Indent_7() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___Indent_7)); }
	inline bool get_Indent_7() const { return ___Indent_7; }
	inline bool* get_address_of_Indent_7() { return &___Indent_7; }
	inline void set_Indent_7(bool value)
	{
		___Indent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEGROUPATTRIBUTE_T2185192947_H
#ifndef VALIDATEINPUTATTRIBUTE_T471509373_H
#define VALIDATEINPUTATTRIBUTE_T471509373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ValidateInputAttribute
struct  ValidateInputAttribute_t471509373  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::DefaultMessage
	String_t* ___DefaultMessage_0;
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::MemberName
	String_t* ___MemberName_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.ValidateInputAttribute::MessageType
	int32_t ___MessageType_2;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::IncludeChildren
	bool ___IncludeChildren_3;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::ContiniousValidationCheck
	bool ___ContiniousValidationCheck_4;

public:
	inline static int32_t get_offset_of_DefaultMessage_0() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___DefaultMessage_0)); }
	inline String_t* get_DefaultMessage_0() const { return ___DefaultMessage_0; }
	inline String_t** get_address_of_DefaultMessage_0() { return &___DefaultMessage_0; }
	inline void set_DefaultMessage_0(String_t* value)
	{
		___DefaultMessage_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMessage_0), value);
	}

	inline static int32_t get_offset_of_MemberName_1() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___MemberName_1)); }
	inline String_t* get_MemberName_1() const { return ___MemberName_1; }
	inline String_t** get_address_of_MemberName_1() { return &___MemberName_1; }
	inline void set_MemberName_1(String_t* value)
	{
		___MemberName_1 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_1), value);
	}

	inline static int32_t get_offset_of_MessageType_2() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___MessageType_2)); }
	inline int32_t get_MessageType_2() const { return ___MessageType_2; }
	inline int32_t* get_address_of_MessageType_2() { return &___MessageType_2; }
	inline void set_MessageType_2(int32_t value)
	{
		___MessageType_2 = value;
	}

	inline static int32_t get_offset_of_IncludeChildren_3() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___IncludeChildren_3)); }
	inline bool get_IncludeChildren_3() const { return ___IncludeChildren_3; }
	inline bool* get_address_of_IncludeChildren_3() { return &___IncludeChildren_3; }
	inline void set_IncludeChildren_3(bool value)
	{
		___IncludeChildren_3 = value;
	}

	inline static int32_t get_offset_of_ContiniousValidationCheck_4() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___ContiniousValidationCheck_4)); }
	inline bool get_ContiniousValidationCheck_4() const { return ___ContiniousValidationCheck_4; }
	inline bool* get_address_of_ContiniousValidationCheck_4() { return &___ContiniousValidationCheck_4; }
	inline void set_ContiniousValidationCheck_4(bool value)
	{
		___ContiniousValidationCheck_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATEINPUTATTRIBUTE_T471509373_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t426314064 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t426314064 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2999457153 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2999457153 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m1581319360_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Contains_m784383322_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m3947764094_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m816315209_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m337713592_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m3415825172 (PropertyGroupAttribute_t2009328757 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m2725665406 (BoxGroupAttribute_t1828532190 * __this, String_t* ___group0, bool ___showLabel1, bool ___centerLabel2, int32_t ___order3, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m4006092507 (ShowInInspectorAttribute_t2100412880 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_m1361156146 (FoldoutGroupAttribute_t1921228823 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
extern "C" IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_m2237284515 (FoldoutGroupAttribute_t1921228823 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_m1258023889 (HorizontalGroupAttribute_t2886913732 * __this, String_t* ___group0, float ___width1, int32_t ___marginLeft2, int32_t ___marginRight3, int32_t ___order4, const RuntimeMethod* method);
// System.Single System.Math::Max(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Math_Max_m482125436 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
extern "C" IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_MemberMethod_m501118454 (InlineButtonAttribute_t2729761977 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_Label(System.String)
extern "C" IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_Label_m3732629569 (InlineButtonAttribute_t2729761977 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m3058704252 (NotImplementedException_t3489357830 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern "C" IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_m3560120601 (InlineEditorAttribute_t803766710 * __this, int32_t ___inlineEditorMode0, int32_t ___objectFieldMode1, const RuntimeMethod* method);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Math_Max_m1873195862 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907 (ProgressBarAttribute_t2414458096 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743 (ProgressBarAttribute_t2414458096 * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.Char)
extern "C" IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m3451222878 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Math_Min_m3468062251 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m3227981734 (PropertyGroupAttribute_t2009328757 * __this, String_t* ___groupId0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m3613481846 (TabGroupAttribute_t2295443652 * __this, String_t* ___group0, String_t* ___tab1, bool ___useFixedHeight2, int32_t ___order3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m706204246 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m2549283086 (TabGroupAttribute_t2295443652 * __this, List_1_t3319525431 * ___value0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * TabGroupAttribute_get_Tabs_m3022315452 (TabGroupAttribute_t2295443652 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m1685793073 (List_1_t3319525431 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m378240398 (List_1_t3319525431 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1581319360_gshared)(__this, p0, method);
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void PropertyGroupAttribute_CombineValuesWith_m921705279 (PropertyGroupAttribute_t2009328757 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0)
inline bool List_1_Contains_m195709148 (List_1_t3319525431 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Contains_m784383322_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m2276455407 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>::.ctor(System.Int32)
inline void List_1__ctor_m2881262781 (List_1_t3481403499 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3481403499 *, int32_t, const RuntimeMethod*))List_1__ctor_m3947764094_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_t913802012  List_1_GetEnumerator_m530823355 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t913802012  (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m3483505131 (Enumerator_t913802012 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_t913802012 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TabSubGroupAttribute__ctor_m2990053763 (TabSubGroupAttribute_t867681421 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>::Add(!0)
inline void List_1_Add_m1615589540 (List_1_t3481403499 * __this, PropertyGroupAttribute_t2009328757 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3481403499 *, PropertyGroupAttribute_t2009328757 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_m4250544074 (Enumerator_t913802012 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t913802012 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m2026665411 (Enumerator_t913802012 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t913802012 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m3207051029 (ToggleGroupAttribute_t12117724 * __this, String_t* ___toggleMemberName0, int32_t ___order1, String_t* ___groupTitle2, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR void ValueDropdownItem__ctor_m3988469792 (ValueDropdownItem_t3457888829 * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m463973898 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_ToString_m1770062308 (ValueDropdownItem_t3457888829 * __this, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
extern "C" IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_m3534405043 (ValueDropdownItem_t3457888829 * __this, const RuntimeMethod* method);
// System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m752113253 (ValueDropdownItem_t3457888829 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_m2519698565 (VerticalGroupAttribute_t4076833965 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetListAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AssetListAttribute__ctor_m3404470409 (AssetListAttribute_t3126145472 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_AutoPopulate_0((bool)0);
		__this->set_Tags_1((String_t*)NULL);
		__this->set_LayerNames_2((String_t*)NULL);
		__this->set_AssetNamePrefix_3((String_t*)NULL);
		__this->set_CustomFilterMethod_5((String_t*)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetsOnlyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AssetsOnlyAttribute__ctor_m837873041 (AssetsOnlyAttribute_t3835058913 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m2725665406 (BoxGroupAttribute_t1828532190 * __this, String_t* ___group0, bool ___showLabel1, bool ___centerLabel2, int32_t ___order3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order3;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___showLabel1;
		__this->set_ShowLabel_3(L_2);
		bool L_3 = ___centerLabel2;
		__this->set_CenterLabel_4(L_3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m2509472355 (BoxGroupAttribute_t1828532190 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxGroupAttribute__ctor_m2509472355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BoxGroupAttribute__ctor_m2725665406(__this, _stringLiteral3441077233, (bool)0, (bool)0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void BoxGroupAttribute_CombineValuesWith_m1332820354 (BoxGroupAttribute_t1828532190 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxGroupAttribute_CombineValuesWith_m1332820354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BoxGroupAttribute_t1828532190 * V_0 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		V_0 = ((BoxGroupAttribute_t1828532190 *)IsInstClass((RuntimeObject*)L_0, BoxGroupAttribute_t1828532190_il2cpp_TypeInfo_var));
		bool L_1 = __this->get_ShowLabel_3();
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		BoxGroupAttribute_t1828532190 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = L_2->get_ShowLabel_3();
		if (L_3)
		{
			goto IL_0025;
		}
	}

IL_0017:
	{
		__this->set_ShowLabel_3((bool)0);
		BoxGroupAttribute_t1828532190 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_ShowLabel_3((bool)0);
	}

IL_0025:
	{
		bool L_5 = __this->get_CenterLabel_4();
		BoxGroupAttribute_t1828532190 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = L_6->get_CenterLabel_4();
		__this->set_CenterLabel_4((bool)((int32_t)((int32_t)L_5|(int32_t)L_7)));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m2596488812 (ButtonAttribute_t3418195108 * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		__this->set_ButtonHeight_0(0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m3427480150 (ButtonAttribute_t3418195108 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_0 = ___size0;
		__this->set_ButtonHeight_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m1536232302 (ButtonAttribute_t3418195108 * __this, int32_t ___buttonSize0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___buttonSize0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m1591470484 (ButtonAttribute_t3418195108 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		__this->set_ButtonHeight_0(0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m1363350575 (ButtonAttribute_t3418195108 * __this, String_t* ___name0, int32_t ___buttonSize1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m1583846392 (ButtonAttribute_t3418195108 * __this, String_t* ___name0, int32_t ___buttonSize1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonStyle)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m2530698557 (ButtonAttribute_t3418195108 * __this, int32_t ___parameterBtnStyle0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		__this->set_ButtonHeight_0(0);
		int32_t L_0 = ___parameterBtnStyle0;
		__this->set_Style_2(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32,Sirenix.OdinInspector.ButtonStyle)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m189733007 (ButtonAttribute_t3418195108 * __this, int32_t ___buttonSize0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___buttonSize0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m476970412 (ButtonAttribute_t3418195108 * __this, int32_t ___size0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___size0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonStyle)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m1355641366 (ButtonAttribute_t3418195108 * __this, String_t* ___name0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		__this->set_ButtonHeight_0(0);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m3020381524 (ButtonAttribute_t3418195108 * __this, String_t* ___name0, int32_t ___buttonSize1, int32_t ___parameterBtnStyle2, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		int32_t L_2 = ___parameterBtnStyle2;
		__this->set_Style_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32,Sirenix.OdinInspector.ButtonStyle)
extern "C" IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m508122363 (ButtonAttribute_t3418195108 * __this, String_t* ___name0, int32_t ___buttonSize1, int32_t ___parameterBtnStyle2, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		int32_t L_2 = ___parameterBtnStyle2;
		__this->set_Style_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ButtonGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ButtonGroupAttribute__ctor_m1716041723 (ButtonGroupAttribute_t2757208248 * __this, String_t* ___group0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ColorPaletteAttribute__ctor_m1079485273 (ColorPaletteAttribute_t392531492 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_PaletteName_0((String_t*)NULL);
		__this->set_ShowAlpha_1((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ColorPaletteAttribute__ctor_m3061204922 (ColorPaletteAttribute_t392531492 * __this, String_t* ___paletteName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___paletteName0;
		__this->set_PaletteName_0(L_0);
		__this->set_ShowAlpha_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void CustomContextMenuAttribute__ctor_m3999772958 (CustomContextMenuAttribute_t4264451314 * __this, String_t* ___menuItem0, String_t* ___methodName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuItem0;
		__this->set_MenuItem_0(L_0);
		String_t* L_1 = ___methodName1;
		__this->set_MethodName_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void CustomValueDrawerAttribute__ctor_m3017287949 (CustomValueDrawerAttribute_t4188368302 * __this, String_t* ___methodName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___methodName0;
		__this->set_MethodName_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DelayedPropertyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DelayedPropertyAttribute__ctor_m3997056800 (DelayedPropertyAttribute_t1141984869 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DetailedInfoBoxAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
extern "C" IL2CPP_METHOD_ATTR void DetailedInfoBoxAttribute__ctor_m20793224 (DetailedInfoBoxAttribute_t2666061416 * __this, String_t* ___message0, String_t* ___details1, int32_t ___infoMessageType2, String_t* ___visibleIf3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		String_t* L_1 = ___details1;
		__this->set_Details_1(L_1);
		int32_t L_2 = ___infoMessageType2;
		__this->set_InfoMessageType_2(L_2);
		String_t* L_3 = ___visibleIf3;
		__this->set_VisibleIf_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DictionaryDrawerSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DictionaryDrawerSettings__ctor_m752810153 (DictionaryDrawerSettings_t3408197424 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DictionaryDrawerSettings__ctor_m752810153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_KeyLabel_0(_stringLiteral2600272002);
		__this->set_ValueLabel_1(_stringLiteral3493619321);
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableContextMenuAttribute::.ctor(System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void DisableContextMenuAttribute__ctor_m3321822944 (DisableContextMenuAttribute_t184909590 * __this, bool ___disableForMember0, bool ___disableCollectionElements1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		bool L_0 = ___disableForMember0;
		__this->set_DisableForMember_0(L_0);
		bool L_1 = ___disableCollectionElements1;
		__this->set_DisableForCollectionElements_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void DisableIfAttribute__ctor_m165526205 (DisableIfAttribute_t2867643591 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR void DisableIfAttribute__ctor_m121976404 (DisableIfAttribute_t2867643591 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInEditorModeAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInEditorModeAttribute__ctor_m4038417402 (DisableInEditorModeAttribute_t630823232 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInInlineEditorsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInInlineEditorsAttribute__ctor_m600902134 (DisableInInlineEditorsAttribute_t864950097 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInNonPrefabsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInNonPrefabsAttribute__ctor_m3135843153 (DisableInNonPrefabsAttribute_t431643105 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPlayModeAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInPlayModeAttribute__ctor_m286768200 (DisableInPlayModeAttribute_t3352669054 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabAssetsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInPrefabAssetsAttribute__ctor_m1065567438 (DisableInPrefabAssetsAttribute_t227636478 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabInstancesAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInPrefabInstancesAttribute__ctor_m2720632638 (DisableInPrefabInstancesAttribute_t3405933113 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisableInPrefabsAttribute__ctor_m1958610042 (DisableInPrefabsAttribute_t3065057671 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DisplayAsStringAttribute__ctor_m45655543 (DisplayAsStringAttribute_t1002957046 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_Overflow_0((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void DisplayAsStringAttribute__ctor_m4183180658 (DisplayAsStringAttribute_t1002957046 * __this, bool ___overflow0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		bool L_0 = ___overflow0;
		__this->set_Overflow_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DontApplyToListElementsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DontApplyToListElementsAttribute__ctor_m139976037 (DontApplyToListElementsAttribute_t3002873312 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DrawWithUnityAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DrawWithUnityAttribute__ctor_m2816801489 (DrawWithUnityAttribute_t2993498865 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableForPrefabOnlyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnableForPrefabOnlyAttribute__ctor_m163777708 (EnableForPrefabOnlyAttribute_t2962074089 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableGUIAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnableGUIAttribute__ctor_m2739948196 (EnableGUIAttribute_t4210275809 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void EnableIfAttribute__ctor_m1790053990 (EnableIfAttribute_t729670423 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR void EnableIfAttribute__ctor_m2622189268 (EnableIfAttribute_t729670423 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnumPagingAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnumPagingAttribute__ctor_m1526976946 (EnumPagingAttribute_t3087485058 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnumToggleButtonsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnumToggleButtonsAttribute__ctor_m2124620312 (EnumToggleButtonsAttribute_t3538214787 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.FilePathAttribute::get_ReadOnly()
extern "C" IL2CPP_METHOD_ATTR bool FilePathAttribute_get_ReadOnly_m3086207915 (FilePathAttribute_t2750434984 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CReadOnlyU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FilePathAttribute::set_ReadOnly(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FilePathAttribute_set_ReadOnly_m819434113 (FilePathAttribute_t2750434984 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CReadOnlyU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FilePathAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FilePathAttribute__ctor_m353966591 (FilePathAttribute_t2750434984 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.FolderPathAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FolderPathAttribute__ctor_m2732121706 (FolderPathAttribute_t2308214518 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void FoldoutGroupAttribute__ctor_m1380771633 (FoldoutGroupAttribute_t1921228823 * __this, String_t* ___groupName0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupName0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void FoldoutGroupAttribute__ctor_m2497033094 (FoldoutGroupAttribute_t1921228823 * __this, String_t* ___groupName0, bool ___expanded1, int32_t ___order2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupName0;
		int32_t L_1 = ___order2;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___expanded1;
		__this->set_Expanded_3(L_2);
		FoldoutGroupAttribute_set_HasDefinedExpanded_m1361156146(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
extern "C" IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_m2237284515 (FoldoutGroupAttribute_t1921228823 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CHasDefinedExpandedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_m1361156146 (FoldoutGroupAttribute_t1921228823 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CHasDefinedExpandedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_CombineValuesWith_m1568517065 (FoldoutGroupAttribute_t1921228823 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FoldoutGroupAttribute_CombineValuesWith_m1568517065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FoldoutGroupAttribute_t1921228823 * V_0 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		V_0 = ((FoldoutGroupAttribute_t1921228823 *)IsInstClass((RuntimeObject*)L_0, FoldoutGroupAttribute_t1921228823_il2cpp_TypeInfo_var));
		FoldoutGroupAttribute_t1921228823 * L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = FoldoutGroupAttribute_get_HasDefinedExpanded_m2237284515(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		FoldoutGroupAttribute_set_HasDefinedExpanded_m1361156146(__this, (bool)1, /*hidden argument*/NULL);
		FoldoutGroupAttribute_t1921228823 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_Expanded_3();
		__this->set_Expanded_3(L_4);
	}

IL_0022:
	{
		bool L_5 = FoldoutGroupAttribute_get_HasDefinedExpanded_m2237284515(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		FoldoutGroupAttribute_t1921228823 * L_6 = V_0;
		NullCheck(L_6);
		FoldoutGroupAttribute_set_HasDefinedExpanded_m1361156146(L_6, (bool)1, /*hidden argument*/NULL);
		FoldoutGroupAttribute_t1921228823 * L_7 = V_0;
		bool L_8 = __this->get_Expanded_3();
		NullCheck(L_7);
		L_7->set_Expanded_3(L_8);
	}

IL_003d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void GUIColorAttribute__ctor_m1641865989 (GUIColorAttribute_t2790983735 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___r0;
		float L_1 = ___g1;
		float L_2 = ___b2;
		float L_3 = ___a3;
		Color_t2555686324  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m2943235014((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_Color_0(L_4);
		return;
	}
}
// System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GUIColorAttribute__ctor_m1934638039 (GUIColorAttribute_t2790983735 * __this, String_t* ___getColor0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___getColor0;
		__this->set_GetColor_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void HideIfAttribute__ctor_m3722751106 (HideIfAttribute_t2697016173 * __this, String_t* ___memberName0, bool ___animate1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		bool L_1 = ___animate1;
		__this->set_Animate_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Object,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void HideIfAttribute__ctor_m3642385390 (HideIfAttribute_t2697016173 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, bool ___animate2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInEditorModeAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInEditorModeAttribute__ctor_m1852509188 (HideInEditorModeAttribute_t1562875995 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInInlineEditorsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInInlineEditorsAttribute__ctor_m1573563686 (HideInInlineEditorsAttribute_t2643132944 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInNonPrefabsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInNonPrefabsAttribute__ctor_m2313092180 (HideInNonPrefabsAttribute_t2267791284 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPlayModeAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInPlayModeAttribute__ctor_m3971427560 (HideInPlayModeAttribute_t3220251466 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabAssetsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInPrefabAssetsAttribute__ctor_m1141559269 (HideInPrefabAssetsAttribute_t3787194334 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabInstancesAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInPrefabInstancesAttribute__ctor_m1598401271 (HideInPrefabInstancesAttribute_t271927100 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInPrefabsAttribute__ctor_m3216557455 (HideInPrefabsAttribute_t2196881499 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInTablesAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideInTablesAttribute__ctor_m2326105238 (HideInTablesAttribute_t3879405200 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideLabelAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideLabelAttribute__ctor_m806733543 (HideLabelAttribute_t3755575312 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideMonoScriptAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideMonoScriptAttribute__ctor_m817958401 (HideMonoScriptAttribute_t3453138722 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideNetworkBehaviourFieldsAttribute__ctor_m2437957289 (HideNetworkBehaviourFieldsAttribute_t1543820585 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideReferenceObjectPickerAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HideReferenceObjectPickerAttribute__ctor_m1965411022 (HideReferenceObjectPickerAttribute_t3431975763 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_m1258023889 (HorizontalGroupAttribute_t2886913732 * __this, String_t* ___group0, float ___width1, int32_t ___marginLeft2, int32_t ___marginRight3, int32_t ___order4, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order4;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		float L_2 = ___width1;
		__this->set_Width_3(L_2);
		int32_t L_3 = ___marginLeft2;
		__this->set_MarginLeft_4((((float)((float)L_3))));
		int32_t L_4 = ___marginRight3;
		__this->set_MarginRight_5((((float)((float)L_4))));
		return;
	}
}
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.Single,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_m2162494618 (HorizontalGroupAttribute_t2886913732 * __this, float ___width0, int32_t ___marginLeft1, int32_t ___marginRight2, int32_t ___order3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HorizontalGroupAttribute__ctor_m2162494618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___width0;
		int32_t L_1 = ___marginLeft1;
		int32_t L_2 = ___marginRight2;
		int32_t L_3 = ___order3;
		HorizontalGroupAttribute__ctor_m1258023889(__this, _stringLiteral1299903114, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void HorizontalGroupAttribute_CombineValuesWith_m2321717564 (HorizontalGroupAttribute_t2886913732 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HorizontalGroupAttribute_CombineValuesWith_m2321717564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	HorizontalGroupAttribute_t2886913732 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	HorizontalGroupAttribute_t2886913732 * G_B1_1 = NULL;
	{
		String_t* L_0 = __this->get_Title_10();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		PropertyGroupAttribute_t2009328757 * L_2 = ___other0;
		NullCheck(((HorizontalGroupAttribute_t2886913732 *)IsInstClass((RuntimeObject*)L_2, HorizontalGroupAttribute_t2886913732_il2cpp_TypeInfo_var)));
		String_t* L_3 = ((HorizontalGroupAttribute_t2886913732 *)IsInstClass((RuntimeObject*)L_2, HorizontalGroupAttribute_t2886913732_il2cpp_TypeInfo_var))->get_Title_10();
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_Title_10(G_B2_0);
		float L_4 = __this->get_LabelWidth_11();
		PropertyGroupAttribute_t2009328757 * L_5 = ___other0;
		NullCheck(((HorizontalGroupAttribute_t2886913732 *)IsInstClass((RuntimeObject*)L_5, HorizontalGroupAttribute_t2886913732_il2cpp_TypeInfo_var)));
		float L_6 = ((HorizontalGroupAttribute_t2886913732 *)IsInstClass((RuntimeObject*)L_5, HorizontalGroupAttribute_t2886913732_il2cpp_TypeInfo_var))->get_LabelWidth_11();
		IL2CPP_RUNTIME_CLASS_INIT(Math_t1671470975_il2cpp_TypeInfo_var);
		float L_7 = Math_Max_m482125436(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		__this->set_LabelWidth_11(L_7);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.IncludeMyAttributesAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void IncludeMyAttributesAttribute__ctor_m1946495597 (IncludeMyAttributesAttribute_t2353296197 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.IndentAttribute::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IndentAttribute__ctor_m142256480 (IndentAttribute_t159299566 * __this, int32_t ___indentLevel0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___indentLevel0;
		__this->set_IndentLevel_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
extern "C" IL2CPP_METHOD_ATTR void InfoBoxAttribute__ctor_m2241622006 (InfoBoxAttribute_t1629795541 * __this, String_t* ___message0, int32_t ___infoMessageType1, String_t* ___visibleIfMemberName2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		int32_t L_1 = ___infoMessageType1;
		__this->set_InfoMessageType_1(L_1);
		String_t* L_2 = ___visibleIfMemberName2;
		__this->set_VisibleIf_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void InfoBoxAttribute__ctor_m276302060 (InfoBoxAttribute_t1629795541 * __this, String_t* ___message0, String_t* ___visibleIfMemberName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		__this->set_InfoMessageType_1(1);
		String_t* L_1 = ___visibleIfMemberName1;
		__this->set_VisibleIf_2(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void InlineButtonAttribute__ctor_m443312221 (InlineButtonAttribute_t2729761977 * __this, String_t* ___memberMethod0, String_t* ___label1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberMethod0;
		InlineButtonAttribute_set_MemberMethod_m501118454(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___label1;
		InlineButtonAttribute_set_Label_m3732629569(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Sirenix.OdinInspector.InlineButtonAttribute::get_MemberMethod()
extern "C" IL2CPP_METHOD_ATTR String_t* InlineButtonAttribute_get_MemberMethod_m52444837 (InlineButtonAttribute_t2729761977 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CMemberMethodU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
extern "C" IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_MemberMethod_m501118454 (InlineButtonAttribute_t2729761977 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMemberMethodU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.InlineButtonAttribute::get_Label()
extern "C" IL2CPP_METHOD_ATTR String_t* InlineButtonAttribute_get_Label_m1621425047 (InlineButtonAttribute_t2729761977 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CLabelU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_Label(System.String)
extern "C" IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_Label_m3732629569 (InlineButtonAttribute_t2729761977 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLabelU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern "C" IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_m3560120601 (InlineEditorAttribute_t803766710 * __this, int32_t ___inlineEditorMode0, int32_t ___objectFieldMode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InlineEditorAttribute__ctor_m3560120601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_PreviewWidth_5((100.0f));
		__this->set_PreviewHeight_6((35.0f));
		__this->set_IncrementInlineEditorDrawerDepth_7((bool)1);
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___objectFieldMode1;
		__this->set_ObjectFieldMode_8(L_0);
		int32_t L_1 = ___inlineEditorMode0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_004a;
			}
			case 1:
			{
				goto IL_0052;
			}
			case 2:
			{
				goto IL_0061;
			}
			case 3:
			{
				goto IL_0070;
			}
			case 4:
			{
				goto IL_007f;
			}
			case 5:
			{
				goto IL_0099;
			}
		}
	}
	{
		goto IL_00af;
	}

IL_004a:
	{
		__this->set_DrawGUI_2((bool)1);
		return;
	}

IL_0052:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawHeader_1((bool)1);
		return;
	}

IL_0061:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_0070:
	{
		__this->set_Expanded_0((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_007f:
	{
		__this->set_Expanded_0((bool)1);
		__this->set_DrawPreview_3((bool)1);
		__this->set_PreviewHeight_6((170.0f));
		return;
	}

IL_0099:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawHeader_1((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_00af:
	{
		NotImplementedException_t3489357830 * L_2 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, InlineEditorAttribute__ctor_m3560120601_RuntimeMethod_var);
	}
}
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern "C" IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_m1362873773 (InlineEditorAttribute_t803766710 * __this, int32_t ___objectFieldMode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___objectFieldMode0;
		InlineEditorAttribute__ctor_m3560120601(__this, 0, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlinePropertyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void InlinePropertyAttribute__ctor_m1911134413 (InlinePropertyAttribute_t3336736111 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void LabelTextAttribute__ctor_m800366340 (LabelTextAttribute_t2020830118 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.LabelWidthAttribute::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void LabelWidthAttribute__ctor_m1648748628 (LabelWidthAttribute_t2595655695 * __this, float ___width0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___width0;
		__this->set_Width_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowPaging()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowPaging_m4101681491 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_paging_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowPaging(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowPaging_m2864641345 (ListDrawerSettingsAttribute_t1308946087 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_paging_11(L_0);
		__this->set_pagingHasValue_15((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableItems()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_DraggableItems_m3268760024 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_draggable_12();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_DraggableItems(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_DraggableItems_m2726584017 (ListDrawerSettingsAttribute_t1308946087 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_draggable_12(L_0);
		__this->set_draggableHasValue_16((bool)1);
		return;
	}
}
// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPage()
extern "C" IL2CPP_METHOD_ATTR int32_t ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_m217683958 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_numberOfItemsPerPage_10();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_NumberOfItemsPerPage(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m3822677345 (ListDrawerSettingsAttribute_t1308946087 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_numberOfItemsPerPage_10(L_0);
		__this->set_numberOfItemsPerPageHasValue_21((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnly()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_IsReadOnly_m3198464389 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isReadOnly_13();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_IsReadOnly(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_IsReadOnly_m2569086921 (ListDrawerSettingsAttribute_t1308946087 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isReadOnly_13(L_0);
		__this->set_isReadOnlyHasValue_17((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCount()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowItemCount_m368912795 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showItemCount_14();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowItemCount(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowItemCount_m872151995 (ListDrawerSettingsAttribute_t1308946087 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showItemCount_14(L_0);
		__this->set_showItemCountHasValue_18((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_Expanded()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_Expanded_m390738988 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expanded_19();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_Expanded(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_Expanded_m2510423256 (ListDrawerSettingsAttribute_t1308946087 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_expanded_19(L_0);
		__this->set_expandedHasValue_20((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabels()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowIndexLabels_m29976897 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showIndexLabels_22();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowIndexLabels(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowIndexLabels_m3244281656 (ListDrawerSettingsAttribute_t1308946087 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showIndexLabels_22(L_0);
		__this->set_showIndexLabelsHasValue_23((bool)1);
		return;
	}
}
// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_OnTitleBarGUI()
extern "C" IL2CPP_METHOD_ATTR String_t* ListDrawerSettingsAttribute_get_OnTitleBarGUI_m204934929 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_onTitleBarGUI_9();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_OnTitleBarGUI(System.String)
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_OnTitleBarGUI_m1868870673 (ListDrawerSettingsAttribute_t1308946087 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_onTitleBarGUI_9(L_0);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_PagingHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_PagingHasValue_m1767520955 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_pagingHasValue_15();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCountHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowItemCountHasValue_m319468865 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showItemCountHasValue_18();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPageHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_m844492051 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_numberOfItemsPerPageHasValue_21();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_DraggableHasValue_m2032356100 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_draggableHasValue_16();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnlyHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_m2641561480 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isReadOnlyHasValue_17();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ExpandedHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ExpandedHasValue_m703600912 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expandedHasValue_20();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabelsHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_m3832763061 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showIndexLabelsHasValue_23();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute__ctor_m1459062649 (ListDrawerSettingsAttribute_t1308946087 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.Double)
extern "C" IL2CPP_METHOD_ATTR void MaxValueAttribute__ctor_m4263346806 (MaxValueAttribute_t1325324426 * __this, double ___maxValue0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___maxValue0;
		__this->set_MaxValue_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.Single,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m969128605 (MinMaxSliderAttribute_t1448807924 * __this, float ___minValue0, float ___maxValue1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		float L_1 = ___maxValue1;
		__this->set_MaxValue_1(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Single,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m3841211788 (MinMaxSliderAttribute_t1448807924 * __this, String_t* ___minMember0, float ___maxValue1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		float L_1 = ___maxValue1;
		__this->set_MaxValue_1(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m2884500524 (MinMaxSliderAttribute_t1448807924 * __this, float ___minValue0, String_t* ___maxMember1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m1551997709 (MinMaxSliderAttribute_t1448807924 * __this, String_t* ___minMember0, String_t* ___maxMember1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m500160449 (MinMaxSliderAttribute_t1448807924 * __this, String_t* ___minMaxMember0, bool ___showFields1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMaxMember0;
		__this->set_MinMaxMember_4(L_0);
		bool L_1 = ___showFields1;
		__this->set_ShowFields_5(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.Double)
extern "C" IL2CPP_METHOD_ATTR void MinValueAttribute__ctor_m516156217 (MinValueAttribute_t1691372301 * __this, double ___minValue0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MultiLinePropertyAttribute::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MultiLinePropertyAttribute__ctor_m4147997088 (MultiLinePropertyAttribute_t1222183101 * __this, int32_t ___lines0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MultiLinePropertyAttribute__ctor_m4147997088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___lines0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_t1671470975_il2cpp_TypeInfo_var);
		int32_t L_1 = Math_Max_m1873195862(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		__this->set_Lines_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_m3697552698 (OnInspectorGUIAttribute_t3458889380 * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_m2596974277 (OnInspectorGUIAttribute_t3458889380 * __this, String_t* ___methodName0, bool ___append1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		bool L_0 = ___append1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___methodName0;
		__this->set_AppendMethodName_1(L_1);
		return;
	}

IL_0011:
	{
		String_t* L_2 = ___methodName0;
		__this->set_PrependMethodName_0(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_m3388739393 (OnInspectorGUIAttribute_t3458889380 * __this, String_t* ___prependMethodName0, String_t* ___appendMethodName1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m4006092507(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___prependMethodName0;
		__this->set_PrependMethodName_0(L_0);
		String_t* L_1 = ___appendMethodName1;
		__this->set_AppendMethodName_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnValueChangedAttribute::.ctor(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void OnValueChangedAttribute__ctor_m4012650371 (OnValueChangedAttribute_t870857610 * __this, String_t* ___methodName0, bool ___includeChildren1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___methodName0;
		__this->set_MethodName_0(L_0);
		bool L_1 = ___includeChildren1;
		__this->set_IncludeChildren_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m844682825 (PreviewFieldAttribute_t2856725314 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_Height_0((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m3561039207 (PreviewFieldAttribute_t2856725314 * __this, float ___height0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_Height_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single,Sirenix.OdinInspector.ObjectFieldAlignment)
extern "C" IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m244358225 (PreviewFieldAttribute_t2856725314 * __this, float ___height0, int32_t ___alignment1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_Height_0(L_0);
		int32_t L_1 = ___alignment1;
		__this->set_Alignment_1(L_1);
		__this->set_AlignmentHasValue_2((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(Sirenix.OdinInspector.ObjectFieldAlignment)
extern "C" IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m391269837 (PreviewFieldAttribute_t2856725314 * __this, int32_t ___alignment0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___alignment0;
		__this->set_Alignment_1(L_0);
		__this->set_AlignmentHasValue_2((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.Double,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m2516555930 (ProgressBarAttribute_t2414458096 * __this, double ___min0, double ___max1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.Double,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m307908403 (ProgressBarAttribute_t2414458096 * __this, String_t* ___minMember0, double ___max1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.String,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m3919677583 (ProgressBarAttribute_t2414458096 * __this, double ___min0, String_t* ___maxMember1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.String,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m4202340478 (ProgressBarAttribute_t2414458096 * __this, String_t* ___minMember0, String_t* ___maxMember1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabel()
extern "C" IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_DrawValueLabel_m2101994200 (ProgressBarAttribute_t2414458096 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_drawValueLabel_12();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabel(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabel_m3119307315 (ProgressBarAttribute_t2414458096 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_drawValueLabel_12(L_0);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabelHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_DrawValueLabelHasValue_m3324981496 (ProgressBarAttribute_t2414458096 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CDrawValueLabelHasValueU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_m1374150907 (ProgressBarAttribute_t2414458096 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(L_0);
		return;
	}
}
// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignment()
extern "C" IL2CPP_METHOD_ATTR int32_t ProgressBarAttribute_get_ValueLabelAlignment_m3850330682 (ProgressBarAttribute_t2414458096 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_valueLabelAlignment_13();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignment(UnityEngine.TextAlignment)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignment_m79577923 (ProgressBarAttribute_t2414458096 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_valueLabelAlignment_13(L_0);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignmentHasValue()
extern "C" IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_ValueLabelAlignmentHasValue_m3372384754 (ProgressBarAttribute_t2414458096 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m765888743 (ProgressBarAttribute_t2414458096 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m3415825172 (PropertyGroupAttribute_t2009328757 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	PropertyGroupAttribute_t2009328757 * G_B2_0 = NULL;
	PropertyGroupAttribute_t2009328757 * G_B1_0 = NULL;
	PropertyGroupAttribute_t2009328757 * G_B3_0 = NULL;
	String_t* G_B4_0 = NULL;
	PropertyGroupAttribute_t2009328757 * G_B4_1 = NULL;
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___groupId0;
		__this->set_GroupID_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_Order_2(L_1);
		String_t* L_2 = ___groupId0;
		NullCheck(L_2);
		int32_t L_3 = String_LastIndexOf_m3451222878(L_2, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		G_B1_0 = __this;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			G_B2_0 = __this;
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = V_0;
		String_t* L_6 = ___groupId0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			G_B3_0 = G_B1_0;
			goto IL_002e;
		}
	}

IL_002b:
	{
		String_t* L_8 = ___groupId0;
		G_B4_0 = L_8;
		G_B4_1 = G_B2_0;
		goto IL_0037;
	}

IL_002e:
	{
		String_t* L_9 = ___groupId0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2848979100(L_9, ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)), /*hidden argument*/NULL);
		G_B4_0 = L_11;
		G_B4_1 = G_B3_0;
	}

IL_0037:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_GroupName_1(G_B4_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m3227981734 (PropertyGroupAttribute_t2009328757 * __this, String_t* ___groupId0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// Sirenix.OdinInspector.PropertyGroupAttribute Sirenix.OdinInspector.PropertyGroupAttribute::Combine(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR PropertyGroupAttribute_t2009328757 * PropertyGroupAttribute_Combine_m1029103233 (PropertyGroupAttribute_t2009328757 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyGroupAttribute_Combine_m1029103233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral2432405111, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, PropertyGroupAttribute_Combine_m1029103233_RuntimeMethod_var);
	}

IL_000e:
	{
		PropertyGroupAttribute_t2009328757 * L_2 = ___other0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m88164663(L_2, /*hidden argument*/NULL);
		Type_t * L_4 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_3) == ((RuntimeObject*)(Type_t *)L_4)))
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, _stringLiteral31325578, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, PropertyGroupAttribute_Combine_m1029103233_RuntimeMethod_var);
	}

IL_0027:
	{
		PropertyGroupAttribute_t2009328757 * L_6 = ___other0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_GroupID_0();
		String_t* L_8 = __this->get_GroupID_0();
		bool L_9 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_10, _stringLiteral4005892038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, NULL, PropertyGroupAttribute_Combine_m1029103233_RuntimeMethod_var);
	}

IL_0045:
	{
		int32_t L_11 = __this->get_Order_2();
		if (L_11)
		{
			goto IL_005b;
		}
	}
	{
		PropertyGroupAttribute_t2009328757 * L_12 = ___other0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_Order_2();
		__this->set_Order_2(L_13);
		goto IL_007a;
	}

IL_005b:
	{
		PropertyGroupAttribute_t2009328757 * L_14 = ___other0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_Order_2();
		if (!L_15)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_16 = __this->get_Order_2();
		PropertyGroupAttribute_t2009328757 * L_17 = ___other0;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_Order_2();
		IL2CPP_RUNTIME_CLASS_INIT(Math_t1671470975_il2cpp_TypeInfo_var);
		int32_t L_19 = Math_Min_m3468062251(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		__this->set_Order_2(L_19);
	}

IL_007a:
	{
		PropertyGroupAttribute_t2009328757 * L_20 = ___other0;
		VirtActionInvoker1< PropertyGroupAttribute_t2009328757 * >::Invoke(4 /* System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute) */, __this, L_20);
		return __this;
	}
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void PropertyGroupAttribute_CombineValuesWith_m921705279 (PropertyGroupAttribute_t2009328757 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PropertyOrderAttribute__ctor_m1140048232 (PropertyOrderAttribute_t2594707638 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PropertyOrderAttribute__ctor_m1420579321 (PropertyOrderAttribute_t2594707638 * __this, int32_t ___order0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___order0;
		__this->set_Order_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.Double)
extern "C" IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m3984994847 (PropertyRangeAttribute_t3334671474 * __this, double ___min0, double ___max1, const RuntimeMethod* method)
{
	PropertyRangeAttribute_t3334671474 * G_B2_0 = NULL;
	PropertyRangeAttribute_t3334671474 * G_B1_0 = NULL;
	double G_B3_0 = 0.0;
	PropertyRangeAttribute_t3334671474 * G_B3_1 = NULL;
	PropertyRangeAttribute_t3334671474 * G_B5_0 = NULL;
	PropertyRangeAttribute_t3334671474 * G_B4_0 = NULL;
	double G_B6_0 = 0.0;
	PropertyRangeAttribute_t3334671474 * G_B6_1 = NULL;
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		double L_1 = ___max1;
		G_B1_0 = __this;
		if ((((double)L_0) < ((double)L_1)))
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		double L_2 = ___max1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		double L_3 = ___min0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_Min_0(G_B3_0);
		double L_4 = ___max1;
		double L_5 = ___min0;
		G_B4_0 = __this;
		if ((((double)L_4) > ((double)L_5)))
		{
			G_B5_0 = __this;
			goto IL_001c;
		}
	}
	{
		double L_6 = ___min0;
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_001d;
	}

IL_001c:
	{
		double L_7 = ___max1;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_001d:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_Max_1(G_B6_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.Double)
extern "C" IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m3482517564 (PropertyRangeAttribute_t3334671474 * __this, String_t* ___minMember0, double ___max1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.String)
extern "C" IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m2566193335 (PropertyRangeAttribute_t3334671474 * __this, double ___min0, String_t* ___maxMember1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m2409285254 (PropertyRangeAttribute_t3334671474 * __this, String_t* ___minMember0, String_t* ___maxMember1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m2230612460 (PropertySpaceAttribute_t209179749 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_SpaceBefore_0((8.0f));
		__this->set_SpaceAfter_1((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m642263613 (PropertySpaceAttribute_t209179749 * __this, float ___spaceBefore0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___spaceBefore0;
		__this->set_SpaceBefore_0(L_0);
		__this->set_SpaceAfter_1((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m776800641 (PropertySpaceAttribute_t209179749 * __this, float ___spaceBefore0, float ___spaceAfter1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		float L_0 = ___spaceBefore0;
		__this->set_SpaceBefore_0(L_0);
		float L_1 = ___spaceAfter1;
		__this->set_SpaceAfter_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyTooltipAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void PropertyTooltipAttribute__ctor_m3129482597 (PropertyTooltipAttribute_t1909666584 * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_Tooltip_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ReadOnlyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ReadOnlyAttribute__ctor_m3779286092 (ReadOnlyAttribute_t3311022813 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.RegisterAttributeAttribute::.ctor(System.Type,System.String[])
extern "C" IL2CPP_METHOD_ATTR void RegisterAttributeAttribute__ctor_m1270696965 (RegisterAttributeAttribute_t317614997 * __this, Type_t * ___attributeType0, StringU5BU5D_t1281789340* ___categories1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___attributeType0;
		__this->set_AttributeType_0(L_0);
		StringU5BU5D_t1281789340* L_1 = ___categories1;
		__this->set_Categories_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m2532917034 (RequiredAttribute_t1745431873 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_MessageType_1(3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType)
extern "C" IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m552950818 (RequiredAttribute_t1745431873 * __this, String_t* ___errorMessage0, int32_t ___messageType1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___errorMessage0;
		__this->set_ErrorMessage_0(L_0);
		int32_t L_1 = ___messageType1;
		__this->set_MessageType_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m2207626355 (RequiredAttribute_t1745431873 * __this, String_t* ___errorMessage0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___errorMessage0;
		__this->set_ErrorMessage_0(L_0);
		__this->set_MessageType_1(3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(Sirenix.OdinInspector.InfoMessageType)
extern "C" IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m4039865326 (RequiredAttribute_t1745431873 * __this, int32_t ___messageType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___messageType0;
		__this->set_MessageType_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ResponsiveButtonGroupAttribute__ctor_m886410614 (ResponsiveButtonGroupAttribute_t1637434916 * __this, String_t* ___group0, const RuntimeMethod* method)
{
	{
		__this->set_DefaultButtonSize_3(((int32_t)22));
		String_t* L_0 = ___group0;
		PropertyGroupAttribute__ctor_m3227981734(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void ResponsiveButtonGroupAttribute_CombineValuesWith_m1418387196 (ResponsiveButtonGroupAttribute_t1637434916 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ResponsiveButtonGroupAttribute_CombineValuesWith_m1418387196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResponsiveButtonGroupAttribute_t1637434916 * V_0 = NULL;
	ResponsiveButtonGroupAttribute_t1637434916 * G_B8_0 = NULL;
	ResponsiveButtonGroupAttribute_t1637434916 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	ResponsiveButtonGroupAttribute_t1637434916 * G_B9_1 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		V_0 = ((ResponsiveButtonGroupAttribute_t1637434916 *)IsInstClass((RuntimeObject*)L_0, ResponsiveButtonGroupAttribute_t1637434916_il2cpp_TypeInfo_var));
		PropertyGroupAttribute_t2009328757 * L_1 = ___other0;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ResponsiveButtonGroupAttribute_t1637434916 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_DefaultButtonSize_3();
		if ((((int32_t)L_3) == ((int32_t)((int32_t)22))))
		{
			goto IL_0023;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t1637434916 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_DefaultButtonSize_3();
		__this->set_DefaultButtonSize_3(L_5);
		goto IL_0039;
	}

IL_0023:
	{
		int32_t L_6 = __this->get_DefaultButtonSize_3();
		if ((((int32_t)L_6) == ((int32_t)((int32_t)22))))
		{
			goto IL_0039;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t1637434916 * L_7 = V_0;
		int32_t L_8 = __this->get_DefaultButtonSize_3();
		NullCheck(L_7);
		L_7->set_DefaultButtonSize_3(L_8);
	}

IL_0039:
	{
		bool L_9 = __this->get_UniformLayout_4();
		G_B7_0 = __this;
		if (L_9)
		{
			G_B8_0 = __this;
			goto IL_004a;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t1637434916 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = L_10->get_UniformLayout_4();
		G_B9_0 = ((int32_t)(L_11));
		G_B9_1 = G_B7_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B9_0 = 1;
		G_B9_1 = G_B8_0;
	}

IL_004b:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_UniformLayout_4((bool)G_B9_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SceneObjectsOnlyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SceneObjectsOnlyAttribute__ctor_m1658863015 (SceneObjectsOnlyAttribute_t3518277911 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowDrawerChainAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowDrawerChainAttribute__ctor_m3487071457 (ShowDrawerChainAttribute_t1144545212 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowForPrefabOnlyAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowForPrefabOnlyAttribute__ctor_m1759900172 (ShowForPrefabOnlyAttribute_t4233203441 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ShowIfAttribute__ctor_m100243748 (ShowIfAttribute_t2614791958 * __this, String_t* ___memberName0, bool ___animate1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		bool L_1 = ___animate1;
		__this->set_Animate_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Object,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ShowIfAttribute__ctor_m3831251886 (ShowIfAttribute_t2614791958 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, bool ___animate2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_2(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_1(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowInInlineEditorsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowInInlineEditorsAttribute__ctor_m427102797 (ShowInInlineEditorsAttribute_t2931332120 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m4006092507 (ShowInInspectorAttribute_t2100412880 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m2422871532 (ShowOdinSerializedPropertiesInInspectorAttribute_t1684166398 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowPropertyResolverAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowPropertyResolverAttribute__ctor_m2731794513 (ShowPropertyResolverAttribute_t357032921 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SuffixLabelAttribute::.ctor(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SuffixLabelAttribute__ctor_m2498905036 (SuffixLabelAttribute_t126424634 * __this, String_t* ___label0, bool ___overlay1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___label0;
		__this->set_Label_0(L_0);
		bool L_1 = ___overlay1;
		__this->set_Overlay_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SuppressInvalidAttributeErrorAttribute__ctor_m1078289590 (SuppressInvalidAttributeErrorAttribute_t4105959342 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m1778389056 (TabGroupAttribute_t2295443652 * __this, String_t* ___tab0, bool ___useFixedHeight1, int32_t ___order2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute__ctor_m1778389056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___tab0;
		bool L_1 = ___useFixedHeight1;
		int32_t L_2 = ___order2;
		TabGroupAttribute__ctor_m3613481846(__this, _stringLiteral1820882801, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m3613481846 (TabGroupAttribute_t2295443652 * __this, String_t* ___group0, String_t* ___tab1, bool ___useFixedHeight2, int32_t ___order3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute__ctor_m3613481846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order3;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___tab1;
		__this->set_TabName_4(L_2);
		bool L_3 = ___useFixedHeight2;
		__this->set_UseFixedHeight_5(L_3);
		List_1_t3319525431 * L_4 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_4, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		TabGroupAttribute_set_Tabs_m2549283086(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___tab1;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		List_1_t3319525431 * L_6 = TabGroupAttribute_get_Tabs_m3022315452(__this, /*hidden argument*/NULL);
		String_t* L_7 = ___tab1;
		NullCheck(L_6);
		List_1_Add_m1685793073(L_6, L_7, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
	}

IL_0031:
	{
		List_1_t3319525431 * L_8 = TabGroupAttribute_get_Tabs_m3022315452(__this, /*hidden argument*/NULL);
		List_1_t3319525431 * L_9 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m378240398(L_9, L_8, /*hidden argument*/List_1__ctor_m378240398_RuntimeMethod_var);
		TabGroupAttribute_set_Tabs_m2549283086(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * TabGroupAttribute_get_Tabs_m3022315452 (TabGroupAttribute_t2295443652 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3319525431 * L_0 = __this->get_U3CTabsU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m2549283086 (TabGroupAttribute_t2295443652 * __this, List_1_t3319525431 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t3319525431 * L_0 = ___value0;
		__this->set_U3CTabsU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void TabGroupAttribute_CombineValuesWith_m2384920288 (TabGroupAttribute_t2295443652 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute_CombineValuesWith_m2384920288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TabGroupAttribute_t2295443652 * V_0 = NULL;
	TabGroupAttribute_t2295443652 * G_B3_0 = NULL;
	TabGroupAttribute_t2295443652 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TabGroupAttribute_t2295443652 * G_B4_1 = NULL;
	TabGroupAttribute_t2295443652 * G_B6_0 = NULL;
	TabGroupAttribute_t2295443652 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	TabGroupAttribute_t2295443652 * G_B7_1 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		PropertyGroupAttribute_CombineValuesWith_m921705279(__this, L_0, /*hidden argument*/NULL);
		PropertyGroupAttribute_t2009328757 * L_1 = ___other0;
		V_0 = ((TabGroupAttribute_t2295443652 *)IsInstClass((RuntimeObject*)L_1, TabGroupAttribute_t2295443652_il2cpp_TypeInfo_var));
		TabGroupAttribute_t2295443652 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_TabName_4();
		if (!L_3)
		{
			goto IL_0068;
		}
	}
	{
		bool L_4 = __this->get_UseFixedHeight_5();
		G_B2_0 = __this;
		if (L_4)
		{
			G_B3_0 = __this;
			goto IL_0027;
		}
	}
	{
		TabGroupAttribute_t2295443652 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = L_5->get_UseFixedHeight_5();
		G_B4_0 = ((int32_t)(L_6));
		G_B4_1 = G_B2_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = 1;
		G_B4_1 = G_B3_0;
	}

IL_0028:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_UseFixedHeight_5((bool)G_B4_0);
		bool L_7 = __this->get_Paddingless_6();
		G_B5_0 = __this;
		if (L_7)
		{
			G_B6_0 = __this;
			goto IL_003e;
		}
	}
	{
		TabGroupAttribute_t2295443652 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = L_8->get_Paddingless_6();
		G_B7_0 = ((int32_t)(L_9));
		G_B7_1 = G_B5_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_003f:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_Paddingless_6((bool)G_B7_0);
		List_1_t3319525431 * L_10 = TabGroupAttribute_get_Tabs_m3022315452(__this, /*hidden argument*/NULL);
		TabGroupAttribute_t2295443652 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_TabName_4();
		NullCheck(L_10);
		bool L_13 = List_1_Contains_m195709148(L_10, L_12, /*hidden argument*/List_1_Contains_m195709148_RuntimeMethod_var);
		if (L_13)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t3319525431 * L_14 = TabGroupAttribute_get_Tabs_m3022315452(__this, /*hidden argument*/NULL);
		TabGroupAttribute_t2295443652 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = L_15->get_TabName_4();
		NullCheck(L_14);
		List_1_Add_m1685793073(L_14, L_16, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
	}

IL_0068:
	{
		return;
	}
}
// System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.GetSubGroupAttributes()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m330932969 (TabGroupAttribute_t2295443652 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_m330932969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t3481403499 * V_1 = NULL;
	Enumerator_t913802012  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		List_1_t3319525431 * L_0 = TabGroupAttribute_get_Tabs_m3022315452(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2276455407(L_0, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		List_1_t3481403499 * L_2 = (List_1_t3481403499 *)il2cpp_codegen_object_new(List_1_t3481403499_il2cpp_TypeInfo_var);
		List_1__ctor_m2881262781(L_2, L_1, /*hidden argument*/List_1__ctor_m2881262781_RuntimeMethod_var);
		V_1 = L_2;
		List_1_t3319525431 * L_3 = TabGroupAttribute_get_Tabs_m3022315452(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Enumerator_t913802012  L_4 = List_1_GetEnumerator_m530823355(L_3, /*hidden argument*/List_1_GetEnumerator_m530823355_RuntimeMethod_var);
		V_2 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_0021:
		{
			String_t* L_5 = Enumerator_get_Current_m3483505131((Enumerator_t913802012 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m3483505131_RuntimeMethod_var);
			V_3 = L_5;
			List_1_t3481403499 * L_6 = V_1;
			String_t* L_7 = ((PropertyGroupAttribute_t2009328757 *)__this)->get_GroupID_0();
			String_t* L_8 = V_3;
			String_t* L_9 = String_Concat_m3755062657(NULL /*static, unused*/, L_7, _stringLiteral3452614529, L_8, /*hidden argument*/NULL);
			int32_t L_10 = V_0;
			int32_t L_11 = L_10;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
			TabSubGroupAttribute_t867681421 * L_12 = (TabSubGroupAttribute_t867681421 *)il2cpp_codegen_object_new(TabSubGroupAttribute_t867681421_il2cpp_TypeInfo_var);
			TabSubGroupAttribute__ctor_m2990053763(L_12, L_9, L_11, /*hidden argument*/NULL);
			NullCheck(L_6);
			List_1_Add_m1615589540(L_6, L_12, /*hidden argument*/List_1_Add_m1615589540_RuntimeMethod_var);
		}

IL_004a:
		{
			bool L_13 = Enumerator_MoveNext_m4250544074((Enumerator_t913802012 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m4250544074_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_0021;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x63, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2026665411((Enumerator_t913802012 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m2026665411_RuntimeMethod_var);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0063:
	{
		List_1_t3481403499 * L_14 = V_1;
		return L_14;
	}
}
// System.String Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR String_t* TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m3393554505 (TabGroupAttribute_t2295443652 * __this, PropertyGroupAttribute_t2009328757 * ___attr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_m3393554505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TabGroupAttribute_t2295443652 * V_0 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___attr0;
		V_0 = ((TabGroupAttribute_t2295443652 *)CastclassClass((RuntimeObject*)L_0, TabGroupAttribute_t2295443652_il2cpp_TypeInfo_var));
		String_t* L_1 = ((PropertyGroupAttribute_t2009328757 *)__this)->get_GroupID_0();
		TabGroupAttribute_t2295443652 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_TabName_4();
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, _stringLiteral3452614529, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TabSubGroupAttribute__ctor_m2990053763 (TabSubGroupAttribute_t867681421 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TableColumnWidthAttribute::.ctor(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TableColumnWidthAttribute__ctor_m2764468779 (TableColumnWidthAttribute_t1272758069 * __this, int32_t ___width0, bool ___resizable1, const RuntimeMethod* method)
{
	{
		__this->set_Resizable_1((bool)1);
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_Width_0(L_0);
		bool L_1 = ___resizable1;
		__this->set_Resizable_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPaging()
extern "C" IL2CPP_METHOD_ATTR bool TableListAttribute_get_ShowPaging_m841502874 (TableListAttribute_t4098662806 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showPaging_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::set_ShowPaging(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TableListAttribute_set_ShowPaging_m2866670029 (TableListAttribute_t4098662806 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showPaging_11(L_0);
		__this->set_showPagingHasValue_10((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPagingHasValue()
extern "C" IL2CPP_METHOD_ATTR bool TableListAttribute_get_ShowPagingHasValue_m4073397477 (TableListAttribute_t4098662806 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showPagingHasValue_10();
		return L_0;
	}
}
// System.Int32 Sirenix.OdinInspector.TableListAttribute::get_ScrollViewHeight()
extern "C" IL2CPP_METHOD_ATTR int32_t TableListAttribute_get_ScrollViewHeight_m1419457301 (TableListAttribute_t4098662806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TableListAttribute_get_ScrollViewHeight_m1419457301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_MinScrollViewHeight_5();
		int32_t L_1 = __this->get_MaxScrollViewHeight_6();
		IL2CPP_RUNTIME_CLASS_INIT(Math_t1671470975_il2cpp_TypeInfo_var);
		int32_t L_2 = Math_Min_m3468062251(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::set_ScrollViewHeight(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TableListAttribute_set_ScrollViewHeight_m2760885294 (TableListAttribute_t4098662806 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_MaxScrollViewHeight_6(L_1);
		int32_t L_2 = V_0;
		__this->set_MinScrollViewHeight_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TableListAttribute__ctor_m2879256982 (TableListAttribute_t4098662806 * __this, const RuntimeMethod* method)
{
	{
		__this->set_DefaultMinColumnWidth_2(((int32_t)40));
		__this->set_DrawScrollView_4((bool)1);
		__this->set_MinScrollViewHeight_5(((int32_t)350));
		__this->set_CellPadding_9(2);
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TableMatrixAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TableMatrixAttribute__ctor_m712002330 (TableMatrixAttribute_t391949620 * __this, const RuntimeMethod* method)
{
	{
		__this->set_ResizableColumns_1((bool)1);
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TitleAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TitleAttribute__ctor_m378211354 (TitleAttribute_t3554241315 * __this, String_t* ___title0, String_t* ___subtitle1, int32_t ___titleAlignment2, bool ___horizontalLine3, bool ___bold4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TitleAttribute__ctor_m378211354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	TitleAttribute_t3554241315 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	TitleAttribute_t3554241315 * G_B1_1 = NULL;
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___title0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0011;
		}
	}
	{
		G_B2_0 = _stringLiteral1202628576;
		G_B2_1 = G_B1_1;
	}

IL_0011:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_Title_0(G_B2_0);
		String_t* L_2 = ___subtitle1;
		__this->set_Subtitle_1(L_2);
		bool L_3 = ___bold4;
		__this->set_Bold_2(L_3);
		int32_t L_4 = ___titleAlignment2;
		__this->set_TitleAlignment_4(L_4);
		bool L_5 = ___horizontalLine3;
		__this->set_HorizontalLine_3(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TitleGroupAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TitleGroupAttribute__ctor_m4155218339 (TitleGroupAttribute_t2185192947 * __this, String_t* ___title0, String_t* ___subtitle1, int32_t ___alignment2, bool ___horizontalLine3, bool ___boldTitle4, bool ___indent5, int32_t ___order6, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___title0;
		int32_t L_1 = ___order6;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___subtitle1;
		__this->set_Subtitle_3(L_2);
		int32_t L_3 = ___alignment2;
		__this->set_Alignment_4(L_3);
		bool L_4 = ___horizontalLine3;
		__this->set_HorizontalLine_5(L_4);
		bool L_5 = ___boldTitle4;
		__this->set_BoldTitle_6(L_5);
		bool L_6 = ___indent5;
		__this->set_Indent_7(L_6);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TitleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void TitleGroupAttribute_CombineValuesWith_m4048746812 (TitleGroupAttribute_t2185192947 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TitleGroupAttribute_CombineValuesWith_m4048746812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TitleGroupAttribute_t2185192947 * V_0 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		V_0 = ((TitleGroupAttribute_t2185192947 *)IsInstSealed((RuntimeObject*)L_0, TitleGroupAttribute_t2185192947_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_Subtitle_3();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		TitleGroupAttribute_t2185192947 * L_2 = V_0;
		String_t* L_3 = __this->get_Subtitle_3();
		NullCheck(L_2);
		L_2->set_Subtitle_3(L_3);
		goto IL_0029;
	}

IL_001d:
	{
		TitleGroupAttribute_t2185192947 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_Subtitle_3();
		__this->set_Subtitle_3(L_5);
	}

IL_0029:
	{
		int32_t L_6 = __this->get_Alignment_4();
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		TitleGroupAttribute_t2185192947 * L_7 = V_0;
		int32_t L_8 = __this->get_Alignment_4();
		NullCheck(L_7);
		L_7->set_Alignment_4(L_8);
		goto IL_004b;
	}

IL_003f:
	{
		TitleGroupAttribute_t2185192947 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Alignment_4();
		__this->set_Alignment_4(L_10);
	}

IL_004b:
	{
		bool L_11 = __this->get_HorizontalLine_5();
		if (L_11)
		{
			goto IL_0061;
		}
	}
	{
		TitleGroupAttribute_t2185192947 * L_12 = V_0;
		bool L_13 = __this->get_HorizontalLine_5();
		NullCheck(L_12);
		L_12->set_HorizontalLine_5(L_13);
		goto IL_006d;
	}

IL_0061:
	{
		TitleGroupAttribute_t2185192947 * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = L_14->get_HorizontalLine_5();
		__this->set_HorizontalLine_5(L_15);
	}

IL_006d:
	{
		bool L_16 = __this->get_BoldTitle_6();
		if (L_16)
		{
			goto IL_0083;
		}
	}
	{
		TitleGroupAttribute_t2185192947 * L_17 = V_0;
		bool L_18 = __this->get_BoldTitle_6();
		NullCheck(L_17);
		L_17->set_BoldTitle_6(L_18);
		goto IL_008f;
	}

IL_0083:
	{
		TitleGroupAttribute_t2185192947 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_BoldTitle_6();
		__this->set_BoldTitle_6(L_20);
	}

IL_008f:
	{
		bool L_21 = __this->get_Indent_7();
		if (!L_21)
		{
			goto IL_00a4;
		}
	}
	{
		TitleGroupAttribute_t2185192947 * L_22 = V_0;
		bool L_23 = __this->get_Indent_7();
		NullCheck(L_22);
		L_22->set_Indent_7(L_23);
		return;
	}

IL_00a4:
	{
		TitleGroupAttribute_t2185192947 * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = L_24->get_Indent_7();
		__this->set_Indent_7(L_25);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ToggleAttribute__ctor_m972388141 (ToggleAttribute_t3765571124 * __this, String_t* ___toggleMemberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___toggleMemberName0;
		__this->set_ToggleMemberName_0(L_0);
		__this->set_CollapseOthersOnExpand_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m3207051029 (ToggleGroupAttribute_t12117724 * __this, String_t* ___toggleMemberName0, int32_t ___order1, String_t* ___groupTitle2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___groupTitle2;
		__this->set_ToggleGroupTitle_3(L_2);
		__this->set_CollapseOthersOnExpand_4((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m2698652794 (ToggleGroupAttribute_t12117724 * __this, String_t* ___toggleMemberName0, String_t* ___groupTitle1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		String_t* L_1 = ___groupTitle1;
		ToggleGroupAttribute__ctor_m3207051029(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m2714052376 (ToggleGroupAttribute_t12117724 * __this, String_t* ___toggleMemberName0, int32_t ___order1, String_t* ___groupTitle2, String_t* ___titleStringMemberName3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___groupTitle2;
		__this->set_ToggleGroupTitle_3(L_2);
		__this->set_CollapseOthersOnExpand_4((bool)1);
		return;
	}
}
// System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_ToggleMemberName()
extern "C" IL2CPP_METHOD_ATTR String_t* ToggleGroupAttribute_get_ToggleMemberName_m1460560733 (ToggleGroupAttribute_t12117724 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((PropertyGroupAttribute_t2009328757 *)__this)->get_GroupName_1();
		return L_0;
	}
}
// System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_TitleStringMemberName()
extern "C" IL2CPP_METHOD_ATTR String_t* ToggleGroupAttribute_get_TitleStringMemberName_m3856509489 (ToggleGroupAttribute_t12117724 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleStringMemberNameU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::set_TitleStringMemberName(System.String)
extern "C" IL2CPP_METHOD_ATTR void ToggleGroupAttribute_set_TitleStringMemberName_m2397623711 (ToggleGroupAttribute_t12117724 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleStringMemberNameU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void ToggleGroupAttribute_CombineValuesWith_m3327559737 (ToggleGroupAttribute_t12117724 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToggleGroupAttribute_CombineValuesWith_m3327559737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ToggleGroupAttribute_t12117724 * V_0 = NULL;
	ToggleGroupAttribute_t12117724 * G_B6_0 = NULL;
	ToggleGroupAttribute_t12117724 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	ToggleGroupAttribute_t12117724 * G_B7_1 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		V_0 = ((ToggleGroupAttribute_t12117724 *)IsInstSealed((RuntimeObject*)L_0, ToggleGroupAttribute_t12117724_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_ToggleGroupTitle_3();
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		ToggleGroupAttribute_t12117724 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_ToggleGroupTitle_3();
		__this->set_ToggleGroupTitle_3(L_3);
		goto IL_0031;
	}

IL_001d:
	{
		ToggleGroupAttribute_t12117724 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_ToggleGroupTitle_3();
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		ToggleGroupAttribute_t12117724 * L_6 = V_0;
		String_t* L_7 = __this->get_ToggleGroupTitle_3();
		NullCheck(L_6);
		L_6->set_ToggleGroupTitle_3(L_7);
	}

IL_0031:
	{
		bool L_8 = __this->get_CollapseOthersOnExpand_4();
		G_B5_0 = __this;
		if (L_8)
		{
			G_B6_0 = __this;
			goto IL_0042;
		}
	}
	{
		ToggleGroupAttribute_t12117724 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = L_9->get_CollapseOthersOnExpand_4();
		G_B7_0 = ((int32_t)(L_10));
		G_B7_1 = G_B5_0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_0043:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_CollapseOthersOnExpand_4((bool)G_B7_0);
		ToggleGroupAttribute_t12117724 * L_11 = V_0;
		bool L_12 = __this->get_CollapseOthersOnExpand_4();
		NullCheck(L_11);
		L_11->set_CollapseOthersOnExpand_4(L_12);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleLeftAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ToggleLeftAttribute__ctor_m2525780918 (ToggleLeftAttribute_t2985281771 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TypeFilterAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TypeFilterAttribute__ctor_m2890620777 (TypeFilterAttribute_t642666696 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TypeInfoBoxAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TypeInfoBoxAttribute__ctor_m2300320299 (TypeInfoBoxAttribute_t3273027527 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType)
extern "C" IL2CPP_METHOD_ATTR void ValidateInputAttribute__ctor_m831487907 (ValidateInputAttribute_t471509373 * __this, String_t* ___memberName0, String_t* ___defaultMessage1, int32_t ___messageType2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_1(L_0);
		String_t* L_1 = ___defaultMessage1;
		__this->set_DefaultMessage_0(L_1);
		int32_t L_2 = ___messageType2;
		__this->set_MessageType_2(L_2);
		__this->set_IncludeChildren_3((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ValidateInputAttribute__ctor_m46766683 (ValidateInputAttribute_t471509373 * __this, String_t* ___memberName0, String_t* ___message1, int32_t ___messageType2, bool ___rejectedInvalidInput3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_1(L_0);
		String_t* L_1 = ___message1;
		__this->set_DefaultMessage_0(L_1);
		int32_t L_2 = ___messageType2;
		__this->set_MessageType_2(L_2);
		__this->set_IncludeChildren_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ValueDropdownAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ValueDropdownAttribute__ctor_m3162951477 (ValueDropdownAttribute_t2212993645 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		__this->set_NumberOfItemsBeforeEnablingSearch_1(((int32_t)10));
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		__this->set_DrawDropdownForListElements_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Sirenix.OdinInspector.ValueDropdownItem
extern "C" void ValueDropdownItem_t3457888829_marshal_pinvoke(const ValueDropdownItem_t3457888829& unmarshaled, ValueDropdownItem_t3457888829_marshaled_pinvoke& marshaled)
{
	marshaled.___Text_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Text_0());
	if (unmarshaled.get_Value_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_Value_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___Value_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			marshaled.___Value_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_Value_1());
		}
	}
	else
	{
		marshaled.___Value_1 = NULL;
	}
}
extern "C" void ValueDropdownItem_t3457888829_marshal_pinvoke_back(const ValueDropdownItem_t3457888829_marshaled_pinvoke& marshaled, ValueDropdownItem_t3457888829& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueDropdownItem_t3457888829_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Text_0(il2cpp_codegen_marshal_string_result(marshaled.___Text_0));
	if (marshaled.___Value_1 != NULL)
	{
		unmarshaled.set_Value_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___Value_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_Value_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: Sirenix.OdinInspector.ValueDropdownItem
extern "C" void ValueDropdownItem_t3457888829_marshal_pinvoke_cleanup(ValueDropdownItem_t3457888829_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Text_0);
	marshaled.___Text_0 = NULL;
	if (marshaled.___Value_1 != NULL)
	{
		(marshaled.___Value_1)->Release();
		marshaled.___Value_1 = NULL;
	}
}
// Conversion methods for marshalling of: Sirenix.OdinInspector.ValueDropdownItem
extern "C" void ValueDropdownItem_t3457888829_marshal_com(const ValueDropdownItem_t3457888829& unmarshaled, ValueDropdownItem_t3457888829_marshaled_com& marshaled)
{
	marshaled.___Text_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Text_0());
	if (unmarshaled.get_Value_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_Value_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___Value_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, true);
		}
		else
		{
			marshaled.___Value_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_Value_1());
		}
	}
	else
	{
		marshaled.___Value_1 = NULL;
	}
}
extern "C" void ValueDropdownItem_t3457888829_marshal_com_back(const ValueDropdownItem_t3457888829_marshaled_com& marshaled, ValueDropdownItem_t3457888829& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueDropdownItem_t3457888829_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Text_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Text_0));
	if (marshaled.___Value_1 != NULL)
	{
		unmarshaled.set_Value_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___Value_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_Value_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: Sirenix.OdinInspector.ValueDropdownItem
extern "C" void ValueDropdownItem_t3457888829_marshal_com_cleanup(ValueDropdownItem_t3457888829_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Text_0);
	marshaled.___Text_0 = NULL;
	if (marshaled.___Value_1 != NULL)
	{
		(marshaled.___Value_1)->Release();
		marshaled.___Value_1 = NULL;
	}
}
// System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR void ValueDropdownItem__ctor_m3988469792 (ValueDropdownItem_t3457888829 * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_1(L_1);
		return;
	}
}
extern "C"  void ValueDropdownItem__ctor_m3988469792_AdjustorThunk (RuntimeObject * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	ValueDropdownItem_t3457888829 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t3457888829 *>(__this + 1);
	ValueDropdownItem__ctor_m3988469792(_thisAdjusted, ___text0, ___value1, method);
}
// System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_ToString_m1770062308 (ValueDropdownItem_t3457888829 * __this, const RuntimeMethod* method)
{
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_Text_0();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_2 = __this->get_Value_1();
		String_t* L_3 = String_Concat_m463973898(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
	}

IL_0015:
	{
		return G_B2_0;
	}
}
extern "C"  String_t* ValueDropdownItem_ToString_m1770062308_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ValueDropdownItem_t3457888829 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t3457888829 *>(__this + 1);
	return ValueDropdownItem_ToString_m1770062308(_thisAdjusted, method);
}
// System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
extern "C" IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_m3534405043 (ValueDropdownItem_t3457888829 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Text_0();
		return L_0;
	}
}
extern "C"  String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_m3534405043_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ValueDropdownItem_t3457888829 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t3457888829 *>(__this + 1);
	return ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_m3534405043(_thisAdjusted, method);
}
// System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m752113253 (ValueDropdownItem_t3457888829 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_Value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m752113253_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ValueDropdownItem_t3457888829 * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t3457888829 *>(__this + 1);
	return ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m752113253(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_m2519698565 (VerticalGroupAttribute_t4076833965 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_m3415825172(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_m4270404557 (VerticalGroupAttribute_t4076833965 * __this, int32_t ___order0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerticalGroupAttribute__ctor_m4270404557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___order0;
		VerticalGroupAttribute__ctor_m2519698565(__this, _stringLiteral3378508766, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern "C" IL2CPP_METHOD_ATTR void VerticalGroupAttribute_CombineValuesWith_m3787726932 (VerticalGroupAttribute_t4076833965 * __this, PropertyGroupAttribute_t2009328757 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VerticalGroupAttribute_CombineValuesWith_m3787726932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VerticalGroupAttribute_t4076833965 * V_0 = NULL;
	{
		PropertyGroupAttribute_t2009328757 * L_0 = ___other0;
		V_0 = ((VerticalGroupAttribute_t4076833965 *)IsInstClass((RuntimeObject*)L_0, VerticalGroupAttribute_t4076833965_il2cpp_TypeInfo_var));
		VerticalGroupAttribute_t4076833965 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		VerticalGroupAttribute_t4076833965 * L_2 = V_0;
		NullCheck(L_2);
		float L_3 = L_2->get_PaddingTop_3();
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_0023;
		}
	}
	{
		VerticalGroupAttribute_t4076833965 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = L_4->get_PaddingTop_3();
		__this->set_PaddingTop_3(L_5);
	}

IL_0023:
	{
		VerticalGroupAttribute_t4076833965 * L_6 = V_0;
		NullCheck(L_6);
		float L_7 = L_6->get_PaddingBottom_4();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_003c;
		}
	}
	{
		VerticalGroupAttribute_t4076833965 * L_8 = V_0;
		NullCheck(L_8);
		float L_9 = L_8->get_PaddingBottom_4();
		__this->set_PaddingBottom_4(L_9);
	}

IL_003c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.WrapAttribute::.ctor(System.Double,System.Double)
extern "C" IL2CPP_METHOD_ATTR void WrapAttribute__ctor_m3543490578 (WrapAttribute_t589917550 * __this, double ___min0, double ___max1, const RuntimeMethod* method)
{
	WrapAttribute_t589917550 * G_B2_0 = NULL;
	WrapAttribute_t589917550 * G_B1_0 = NULL;
	double G_B3_0 = 0.0;
	WrapAttribute_t589917550 * G_B3_1 = NULL;
	WrapAttribute_t589917550 * G_B5_0 = NULL;
	WrapAttribute_t589917550 * G_B4_0 = NULL;
	double G_B6_0 = 0.0;
	WrapAttribute_t589917550 * G_B6_1 = NULL;
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		double L_1 = ___max1;
		G_B1_0 = __this;
		if ((((double)L_0) < ((double)L_1)))
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		double L_2 = ___max1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		double L_3 = ___min0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_Min_0(G_B3_0);
		double L_4 = ___max1;
		double L_5 = ___min0;
		G_B4_0 = __this;
		if ((((double)L_4) > ((double)L_5)))
		{
			G_B5_0 = __this;
			goto IL_001c;
		}
	}
	{
		double L_6 = ___min0;
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_001d;
	}

IL_001c:
	{
		double L_7 = ___max1;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_001d:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_Max_1(G_B6_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
