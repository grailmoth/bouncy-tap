﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct VirtActionInvoker6
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector2>
struct BaseDictionaryKeyPathProvider_1_t1761792157;
// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector3>
struct BaseDictionaryKeyPathProvider_1_t3327876098;
// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector4>
struct BaseDictionaryKeyPathProvider_1_t2924591571;
// Sirenix.Serialization.IDataReader
struct IDataReader_t212230107;
// Sirenix.Serialization.IDataWriter
struct IDataWriter_t3682210554;
// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector2>
struct MinimalBaseFormatter_1_t3461213901;
// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector3>
struct MinimalBaseFormatter_1_t732330546;
// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector4>
struct MinimalBaseFormatter_1_t329046019;
// Sirenix.Serialization.Serializer`1<System.Single>
struct Serializer_1_t3074248596;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Object,System.Object,System.Object>
struct DoubleLookupDictionary_3_t1271286727;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate>
struct DoubleLookupDictionary_3_t3686776144;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>
struct DoubleLookupDictionary_3_t650546409;
// Sirenix.Serialization.Utilities.ImmutableList
struct ImmutableList_t1819414204;
// Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25
struct U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215;
// Sirenix.Serialization.Utilities.MemberAliasFieldInfo
struct MemberAliasFieldInfo_t3617726363;
// Sirenix.Serialization.Utilities.MemberAliasMethodInfo
struct MemberAliasMethodInfo_t2838764708;
// Sirenix.Serialization.Utilities.MemberAliasPropertyInfo
struct MemberAliasPropertyInfo_t4204559890;
// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t2941737288;
// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t2941409609;
// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0
struct U3CU3Ec__DisplayClass41_0_t2941671758;
// Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42
struct U3CGetAllMembersU3Ed__42_t2156530709;
// Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43
struct U3CGetAllMembersU3Ed__43_t590446768;
// Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48
struct U3CGetBaseClassesU3Ed__48_t3606501197;
// Sirenix.Serialization.Utilities.ValueGetter`2<System.Object,System.IntPtr>
struct ValueGetter_2_t3364892844;
// Sirenix.Serialization.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr>
struct ValueGetter_2_t1938735483;
// Sirenix.Serialization.Utilities.WeakValueGetter
struct WeakValueGetter_t2669515580;
// Sirenix.Serialization.Utilities.WeakValueSetter
struct WeakValueSetter_t2630980412;
// Sirenix.Serialization.Vector2DictionaryKeyPathProvider
struct Vector2DictionaryKeyPathProvider_t1191773922;
// Sirenix.Serialization.Vector2Formatter
struct Vector2Formatter_t2520006045;
// Sirenix.Serialization.Vector3DictionaryKeyPathProvider
struct Vector3DictionaryKeyPathProvider_t2227500205;
// Sirenix.Serialization.Vector3Formatter
struct Vector3Formatter_t2520007008;
// Sirenix.Serialization.Vector4DictionaryKeyPathProvider
struct Vector4DictionaryKeyPathProvider_t4213655263;
// Sirenix.Serialization.Vector4Formatter
struct Vector4Formatter_t2519996543;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Attribute
struct Attribute_t861562559;
// System.Attribute[]
struct AttributeU5BU5D_t1575011174;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t885026589;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Delegate>>[]
struct EntryU5BU5D_t4121424904;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Object,System.Object>>>[]
struct EntryU5BU5D_t3947126251;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.Collections.Generic.HashSet`1<System.Type>>[]
struct EntryU5BU5D_t2605259679;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.String>[]
struct EntryU5BU5D_t2315109884;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.Type>[]
struct EntryU5BU5D_t679233353;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t1822382459;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Delegate>>
struct KeyCollection_t1971795116;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Object,System.Object>>>
struct KeyCollection_t3230532677;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Collections.Generic.HashSet`1<System.Type>>
struct KeyCollection_t3682916769;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.String>
struct KeyCollection_t186505928;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Type>
struct KeyCollection_t822999999;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t3348751306;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Delegate>>
struct ValueCollection_t3498163963;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Object,System.Object>>>
struct ValueCollection_t461934228;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Collections.Generic.HashSet`1<System.Type>>
struct ValueCollection_t914318320;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.String>
struct ValueCollection_t1712874775;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Type>
struct ValueCollection_t2349368846;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t3046556399;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t3943099367;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.Serializer>
struct Dictionary_2_t3407265638;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>
struct Dictionary_2_t3493241298;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t4291797753;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t633324528;
// System.Collections.Generic.HashSet`1/Slot<System.String>[]
struct SlotU5BU5D_t3850706103;
// System.Collections.Generic.HashSet`1/Slot<System.Type>[]
struct SlotU5BU5D_t2214829572;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Collections.Generic.IEnumerable`1<System.Attribute>
struct IEnumerable_1_t4136382744;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>
struct IEnumerable_1_t2359854630;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t857479137;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterInfo>
struct IEnumerable_1_t840909487;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1463797649;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>
struct IEnumerator_1_t3812572209;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t2916515228;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t296309482;
// System.Collections.Generic.IList`1<System.Type>
struct IList_1_t4297247;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3923495619;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t3327334215;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.IList
struct IList_t2094931216;
// System.Delegate
struct Delegate_t1188392813;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.Exception
struct Exception_t;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3759279471;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2447130374;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t3487522507;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t561252955;
// System.Globalization.Calendar
struct Calendar_t1661121569;
// System.Globalization.CompareInfo
struct CompareInfo_t1092934962;
// System.Globalization.CultureData
struct CultureData_t1899656083;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2405853701;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Globalization.TextInfo
struct TextInfo_t3810425522;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_t2567786569;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1707895399;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t5769829;
// System.Reflection.EventInfo/AddEventAdapter
struct AddEventAdapter_t1787725097;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1530824137;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1302094432;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2572182361;
// System.Reflection.Module
struct Module_t2987026101;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1861056598;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.TypeFilter
struct TypeFilter_t2356120900;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t3522571978;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Security.Principal.IPrincipal
struct IPrincipal_t2343618843;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_t2427220165;
// System.Threading.ExecutionContext
struct ExecutionContext_t1748372627;
// System.Threading.InternalThread
struct InternalThread_t95296544;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.Object
struct Object_t631007953;

extern RuntimeClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern RuntimeClass* Attribute_t861562559_il2cpp_TypeInfo_var;
extern RuntimeClass* BitConverter_t3118986983_il2cpp_TypeInfo_var;
extern RuntimeClass* BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern RuntimeClass* CultureInfo_t4157843068_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t3493241298_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t4291797753_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t633324528_il2cpp_TypeInfo_var;
extern RuntimeClass* DoubleLookupDictionary_3_t3686776144_il2cpp_TypeInfo_var;
extern RuntimeClass* DoubleLookupDictionary_3_t650546409_il2cpp_TypeInfo_var;
extern RuntimeClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern RuntimeClass* EventInfo_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t2447130374_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t3487522507_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t561252955_il2cpp_TypeInfo_var;
extern RuntimeClass* HashSet_1_t1048894234_il2cpp_TypeInfo_var;
extern RuntimeClass* HashSet_1_t412400163_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_1_t1017129698_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t2359854630_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t3812572209_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_t4297247_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* MemberAliasFieldInfo_t3617726363_il2cpp_TypeInfo_var;
extern RuntimeClass* MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var;
extern RuntimeClass* MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var;
extern RuntimeClass* MethodBase_t_il2cpp_TypeInfo_var;
extern RuntimeClass* MethodInfo_t_il2cpp_TypeInfo_var;
extern RuntimeClass* MinimalBaseFormatter_1_t329046019_il2cpp_TypeInfo_var;
extern RuntimeClass* MinimalBaseFormatter_1_t3461213901_il2cpp_TypeInfo_var;
extern RuntimeClass* MinimalBaseFormatter_1_t732330546_il2cpp_TypeInfo_var;
extern RuntimeClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* OverflowException_t2020128637_il2cpp_TypeInfo_var;
extern RuntimeClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern RuntimeClass* Serializer_t962918574_il2cpp_TypeInfo_var;
extern RuntimeClass* Stack_1_t3327334215_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeExtensions_t1614484489_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CGetAllMembersU3Ed__42_t2156530709_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CGetAllMembersU3Ed__43_t590446768_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CGetBaseClassesU3Ed__48_t3606501197_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec__DisplayClass22_0_t2941737288_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec__DisplayClass35_0_t2941409609_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec__DisplayClass41_0_t2941671758_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityExtensions_t2928681996_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityVersion_t744152913_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2Formatter_t2520006045_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3Formatter_t2520007008_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector4Formatter_t2519996543_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1021063921;
extern String_t* _stringLiteral1055810917;
extern String_t* _stringLiteral1065866263;
extern String_t* _stringLiteral1167972383;
extern String_t* _stringLiteral1178749465;
extern String_t* _stringLiteral1184841701;
extern String_t* _stringLiteral1202628576;
extern String_t* _stringLiteral1213614791;
extern String_t* _stringLiteral1225964229;
extern String_t* _stringLiteral1230253740;
extern String_t* _stringLiteral1231452028;
extern String_t* _stringLiteral1234456403;
extern String_t* _stringLiteral1235497039;
extern String_t* _stringLiteral1235498031;
extern String_t* _stringLiteral1236039580;
extern String_t* _stringLiteral1236128813;
extern String_t* _stringLiteral1236129805;
extern String_t* _stringLiteral1264998793;
extern String_t* _stringLiteral1274151684;
extern String_t* _stringLiteral1274151716;
extern String_t* _stringLiteral1295171237;
extern String_t* _stringLiteral130792137;
extern String_t* _stringLiteral1309047644;
extern String_t* _stringLiteral133510650;
extern String_t* _stringLiteral133937983;
extern String_t* _stringLiteral133939039;
extern String_t* _stringLiteral1357986273;
extern String_t* _stringLiteral1390680538;
extern String_t* _stringLiteral1390811594;
extern String_t* _stringLiteral1429726276;
extern String_t* _stringLiteral1443261066;
extern String_t* _stringLiteral1453402100;
extern String_t* _stringLiteral1455341775;
extern String_t* _stringLiteral1469168178;
extern String_t* _stringLiteral1469299245;
extern String_t* _stringLiteral1469561391;
extern String_t* _stringLiteral152203953;
extern String_t* _stringLiteral1535819814;
extern String_t* _stringLiteral1581745030;
extern String_t* _stringLiteral1594150229;
extern String_t* _stringLiteral1600105539;
extern String_t* _stringLiteral1636322628;
extern String_t* _stringLiteral170650980;
extern String_t* _stringLiteral1709130670;
extern String_t* _stringLiteral1715810616;
extern String_t* _stringLiteral1801699217;
extern String_t* _stringLiteral1807570937;
extern String_t* _stringLiteral1818253423;
extern String_t* _stringLiteral1904273217;
extern String_t* _stringLiteral1905736822;
extern String_t* _stringLiteral191294152;
extern String_t* _stringLiteral1929072793;
extern String_t* _stringLiteral193097982;
extern String_t* _stringLiteral1948332219;
extern String_t* _stringLiteral1956447343;
extern String_t* _stringLiteral1958770049;
extern String_t* _stringLiteral1990832630;
extern String_t* _stringLiteral2001916978;
extern String_t* _stringLiteral2018662753;
extern String_t* _stringLiteral2033559650;
extern String_t* _stringLiteral2033560642;
extern String_t* _stringLiteral2038133541;
extern String_t* _stringLiteral2097671996;
extern String_t* _stringLiteral2097807219;
extern String_t* _stringLiteral2099965756;
extern String_t* _stringLiteral2101930777;
extern String_t* _stringLiteral2149933273;
extern String_t* _stringLiteral2191246100;
extern String_t* _stringLiteral2191247092;
extern String_t* _stringLiteral22182330;
extern String_t* _stringLiteral2234161082;
extern String_t* _stringLiteral2260015710;
extern String_t* _stringLiteral228530734;
extern String_t* _stringLiteral2289751134;
extern String_t* _stringLiteral2302966210;
extern String_t* _stringLiteral2322770241;
extern String_t* _stringLiteral2409402648;
extern String_t* _stringLiteral2419720959;
extern String_t* _stringLiteral2422556946;
extern String_t* _stringLiteral2453691846;
extern String_t* _stringLiteral2456775193;
extern String_t* _stringLiteral2553217791;
extern String_t* _stringLiteral2553217811;
extern String_t* _stringLiteral2553676557;
extern String_t* _stringLiteral2554050195;
extern String_t* _stringLiteral2554266375;
extern String_t* _stringLiteral2583018776;
extern String_t* _stringLiteral2601517161;
extern String_t* _stringLiteral2606052858;
extern String_t* _stringLiteral263700689;
extern String_t* _stringLiteral2655463595;
extern String_t* _stringLiteral2695800703;
extern String_t* _stringLiteral2695801631;
extern String_t* _stringLiteral2712963504;
extern String_t* _stringLiteral2722843552;
extern String_t* _stringLiteral2734520598;
extern String_t* _stringLiteral2755855785;
extern String_t* _stringLiteral2758848599;
extern String_t* _stringLiteral2787658538;
extern String_t* _stringLiteral2789494635;
extern String_t* _stringLiteral2789625702;
extern String_t* _stringLiteral2789887848;
extern String_t* _stringLiteral280883515;
extern String_t* _stringLiteral2838662760;
extern String_t* _stringLiteral2838662761;
extern String_t* _stringLiteral2838990438;
extern String_t* _stringLiteral2873896790;
extern String_t* _stringLiteral2890035405;
extern String_t* _stringLiteral2954596464;
extern String_t* _stringLiteral2978261106;
extern String_t* _stringLiteral2993467114;
extern String_t* _stringLiteral3079309113;
extern String_t* _stringLiteral3095951133;
extern String_t* _stringLiteral3106506693;
extern String_t* _stringLiteral3119448475;
extern String_t* _stringLiteral3134897496;
extern String_t* _stringLiteral3158016519;
extern String_t* _stringLiteral3193993034;
extern String_t* _stringLiteral3194025642;
extern String_t* _stringLiteral3217749252;
extern String_t* _stringLiteral3226397847;
extern String_t* _stringLiteral3243520166;
extern String_t* _stringLiteral3267344922;
extern String_t* _stringLiteral3306367446;
extern String_t* _stringLiteral3372390906;
extern String_t* _stringLiteral3446722612;
extern String_t* _stringLiteral3446723604;
extern String_t* _stringLiteral3450517380;
extern String_t* _stringLiteral3450648441;
extern String_t* _stringLiteral3451303882;
extern String_t* _stringLiteral3451369418;
extern String_t* _stringLiteral3451434954;
extern String_t* _stringLiteral3451906199;
extern String_t* _stringLiteral3452614528;
extern String_t* _stringLiteral3452614530;
extern String_t* _stringLiteral3452614533;
extern String_t* _stringLiteral3452614535;
extern String_t* _stringLiteral3452614536;
extern String_t* _stringLiteral3452614545;
extern String_t* _stringLiteral3452614546;
extern String_t* _stringLiteral3452614548;
extern String_t* _stringLiteral3452614592;
extern String_t* _stringLiteral3452614612;
extern String_t* _stringLiteral3452614638;
extern String_t* _stringLiteral3454318535;
extern String_t* _stringLiteral3454777276;
extern String_t* _stringLiteral3454777292;
extern String_t* _stringLiteral3454842823;
extern String_t* _stringLiteral3455563711;
extern String_t* _stringLiteral3455563719;
extern String_t* _stringLiteral3458054133;
extern String_t* _stringLiteral345858392;
extern String_t* _stringLiteral3482834024;
extern String_t* _stringLiteral3497344926;
extern String_t* _stringLiteral351315815;
extern String_t* _stringLiteral3524468349;
extern String_t* _stringLiteral3528114263;
extern String_t* _stringLiteral3547057148;
extern String_t* _stringLiteral3557324734;
extern String_t* _stringLiteral3566785798;
extern String_t* _stringLiteral3576598565;
extern String_t* _stringLiteral3596048705;
extern String_t* _stringLiteral3637139173;
extern String_t* _stringLiteral3659295074;
extern String_t* _stringLiteral3664608116;
extern String_t* _stringLiteral3665265380;
extern String_t* _stringLiteral3665284438;
extern String_t* _stringLiteral3697981828;
extern String_t* _stringLiteral3707058535;
extern String_t* _stringLiteral3739023119;
extern String_t* _stringLiteral3758551614;
extern String_t* _stringLiteral3787497674;
extern String_t* _stringLiteral3820675233;
extern String_t* _stringLiteral3859429177;
extern String_t* _stringLiteral3866462980;
extern String_t* _stringLiteral3873631970;
extern String_t* _stringLiteral3873632002;
extern String_t* _stringLiteral3875954633;
extern String_t* _stringLiteral3889371638;
extern String_t* _stringLiteral3894513951;
extern String_t* _stringLiteral3897867752;
extern String_t* _stringLiteral3897952179;
extern String_t* _stringLiteral3912771868;
extern String_t* _stringLiteral3933064538;
extern String_t* _stringLiteral3963528374;
extern String_t* _stringLiteral39767380;
extern String_t* _stringLiteral3980842185;
extern String_t* _stringLiteral4002445229;
extern String_t* _stringLiteral401433094;
extern String_t* _stringLiteral4135268586;
extern String_t* _stringLiteral4158218461;
extern String_t* _stringLiteral4167518294;
extern String_t* _stringLiteral4176330965;
extern String_t* _stringLiteral4198891390;
extern String_t* _stringLiteral4201940553;
extern String_t* _stringLiteral4285936906;
extern String_t* _stringLiteral430703593;
extern String_t* _stringLiteral432237896;
extern String_t* _stringLiteral461028241;
extern String_t* _stringLiteral529403396;
extern String_t* _stringLiteral59180213;
extern String_t* _stringLiteral594767410;
extern String_t* _stringLiteral674152356;
extern String_t* _stringLiteral731347757;
extern String_t* _stringLiteral748112283;
extern String_t* _stringLiteral752912519;
extern String_t* _stringLiteral75909655;
extern String_t* _stringLiteral765372749;
extern String_t* _stringLiteral775541518;
extern String_t* _stringLiteral798688685;
extern String_t* _stringLiteral807124363;
extern String_t* _stringLiteral82367591;
extern String_t* _stringLiteral827624532;
extern String_t* _stringLiteral862263470;
extern String_t* _stringLiteral921725149;
extern String_t* _stringLiteral946927187;
extern String_t* _stringLiteral959604969;
extern String_t* _stringLiteral964622056;
extern String_t* _stringLiteral967860007;
extern const RuntimeMethod* BaseDictionaryKeyPathProvider_1__ctor_m1975566667_RuntimeMethod_var;
extern const RuntimeMethod* BaseDictionaryKeyPathProvider_1__ctor_m3157277402_RuntimeMethod_var;
extern const RuntimeMethod* BaseDictionaryKeyPathProvider_1__ctor_m607895852_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2678085394_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m3045345476_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m3699546241_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m2935595320_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m2026130383_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1231945001_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2620390247_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2733271626_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2777391954_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m444307833_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m960807489_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Count_m3815247294_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m1040169990_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m4273472624_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m1327842169_RuntimeMethod_var;
extern const RuntimeMethod* DoubleLookupDictionary_3_AddInner_m1135260050_RuntimeMethod_var;
extern const RuntimeMethod* DoubleLookupDictionary_3_TryGetInnerValue_m2712346314_RuntimeMethod_var;
extern const RuntimeMethod* DoubleLookupDictionary_3__ctor_m3657728929_RuntimeMethod_var;
extern const RuntimeMethod* DoubleLookupDictionary_3__ctor_m653869603_RuntimeMethod_var;
extern const RuntimeMethod* EmitUtilities_CreateInstanceFieldGetter_TisObject_t631007953_TisIntPtr_t_m3752444982_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Any_TisType_t_m7140321_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Concat_TisType_t_m3921564742_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Contains_TisType_t_m2042470555_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Skip_TisParameterInfo_t1861056598_m3780503321_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisAttribute_t861562559_m3360527948_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisMethodInfo_t_m1997060780_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisParameterInfo_t1861056598_m2516263013_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Where_TisMethodInfo_t_m1712952118_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m3860723828_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m576450289_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m604830276_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Add_m3328534555_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Add_m3527721678_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Clear_m1877719279_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Contains_m1669355224_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Contains_m3794231197_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1__ctor_m3725506494_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1__ctor_m708940816_RuntimeMethod_var;
extern const RuntimeMethod* MemberInfoExtensions_DeAlias_m1298842707_RuntimeMethod_var;
extern const RuntimeMethod* MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m2331120560_RuntimeMethod_var;
extern const RuntimeMethod* MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m4106349652_RuntimeMethod_var;
extern const RuntimeMethod* MemberInfoExtensions_IsStatic_m2697550895_RuntimeMethod_var;
extern const RuntimeMethod* MethodInfoExtensions_DeAliasMethod_m1948847315_RuntimeMethod_var;
extern const RuntimeMethod* MinimalBaseFormatter_1__ctor_m153188201_RuntimeMethod_var;
extern const RuntimeMethod* MinimalBaseFormatter_1__ctor_m4046092137_RuntimeMethod_var;
extern const RuntimeMethod* MinimalBaseFormatter_1__ctor_m4165236585_RuntimeMethod_var;
extern const RuntimeMethod* PropertyInfoExtensions_DeAliasProperty_m3306904115_RuntimeMethod_var;
extern const RuntimeMethod* Serializer_1_WriteValue_m1523856254_RuntimeMethod_var;
extern const RuntimeMethod* Serializer_Get_TisSingle_t1397266774_m2693733542_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m2843357855_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m506528439_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_m2267086409_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m1286321891_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GenericArgumentsContainsTypes_m411034277_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GenericParameterIsFulfilledBy_m907762269_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m1793324098_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetEnumBitmask_m2667989293_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetGenericBaseType_m225141450_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetGenericConstraintsString_m2590875364_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetGenericParameterConstraintsString_m1569308089_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetInheritanceDistance_m2457922913_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetMemberValue_m2747957946_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetOperatorMethod_m555772733_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_GetOperatorMethods_m2090576811_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_HasCustomAttribute_m3261241330_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_HasCustomAttribute_m456943493_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_ImplementsOpenGenericClass_m2650584869_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_ImplementsOpenGenericInterface_m3084334200_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_ImplementsOpenGenericType_m3675074644_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_IsCastableTo_m2813347195_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_IsFullyConstructedGenericType_m809342933_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_SetMemberValue_m278274216_RuntimeMethod_var;
extern const RuntimeMethod* TypeExtensions_TryInferGenericParameters_m1077962252_RuntimeMethod_var;
extern const RuntimeMethod* U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_m3603743745_RuntimeMethod_var;
extern const RuntimeMethod* U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m2438628648_RuntimeMethod_var;
extern const RuntimeMethod* U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_m3832819096_RuntimeMethod_var;
extern const RuntimeMethod* U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m1853955262_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m3161334231_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m2349605198_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass41_0_U3CGetOperatorMethodsU3Eb__0_m1754509797_RuntimeMethod_var;
extern const RuntimeMethod* UnityExtensions_SafeIsUnityNull_m135202044_RuntimeMethod_var;
extern const RuntimeMethod* UnsafeUtilities_MemoryCopy_m2366990917_RuntimeMethod_var;
extern const RuntimeMethod* UnsafeUtilities_StringFromBytes_m1754034413_RuntimeMethod_var;
extern const RuntimeMethod* UnsafeUtilities_StringToBytes_m87138792_RuntimeMethod_var;
extern const RuntimeMethod* ValueGetter_2_Invoke_m1728989122_RuntimeMethod_var;
extern const RuntimeType* Attribute_t861562559_0_0_0_var;
extern const RuntimeType* Boolean_t97287965_0_0_0_var;
extern const RuntimeType* Byte_t1134296376_0_0_0_var;
extern const RuntimeType* Char_t3634460470_0_0_0_var;
extern const RuntimeType* Decimal_t2948259380_0_0_0_var;
extern const RuntimeType* Double_t594665363_0_0_0_var;
extern const RuntimeType* ExtensionAttribute_t1723066603_0_0_0_var;
extern const RuntimeType* Int16_t2552820387_0_0_0_var;
extern const RuntimeType* Int32_t2950945753_0_0_0_var;
extern const RuntimeType* Int64_t3736567304_0_0_0_var;
extern const RuntimeType* IntPtr_t_0_0_0_var;
extern const RuntimeType* Nullable_1_t3772285925_0_0_0_var;
extern const RuntimeType* Object_t631007953_0_0_0_var;
extern const RuntimeType* RuntimeObject_0_0_0_var;
extern const RuntimeType* SByte_t1669577662_0_0_0_var;
extern const RuntimeType* Single_t1397266774_0_0_0_var;
extern const RuntimeType* UInt16_t2177724958_0_0_0_var;
extern const RuntimeType* UInt32_t2560061978_0_0_0_var;
extern const RuntimeType* UInt64_t4134040092_0_0_0_var;
extern const RuntimeType* UIntPtr_t_0_0_0_var;
extern const RuntimeType* Void_t1185182177_0_0_0_var;
extern const uint32_t MemberAliasFieldInfo__ctor_m4197945869_MetadataUsageId;
extern const uint32_t MemberAliasMethodInfo__ctor_m1393362309_MetadataUsageId;
extern const uint32_t MemberAliasPropertyInfo__ctor_m4188271427_MetadataUsageId;
extern const uint32_t MemberInfoExtensions_DeAlias_m1298842707_MetadataUsageId;
extern const uint32_t MemberInfoExtensions_GetAttributes_m1287245605_MetadataUsageId;
extern const uint32_t MemberInfoExtensions_GetAttributes_m3447597642_MetadataUsageId;
extern const uint32_t MemberInfoExtensions_GetNiceName_m2681496769_MetadataUsageId;
extern const uint32_t MemberInfoExtensions_IsAlias_m2394183543_MetadataUsageId;
extern const uint32_t MemberInfoExtensions_IsStatic_m2697550895_MetadataUsageId;
extern const uint32_t MethodInfoExtensions_DeAliasMethod_m1948847315_MetadataUsageId;
extern const uint32_t MethodInfoExtensions_GetFullName_m2745430701_MetadataUsageId;
extern const uint32_t MethodInfoExtensions_GetFullName_m3000052143_MetadataUsageId;
extern const uint32_t MethodInfoExtensions_GetParamsNames_m873242453_MetadataUsageId;
extern const uint32_t MethodInfoExtensions_IsAliasMethod_m3586406049_MetadataUsageId;
extern const uint32_t MethodInfoExtensions_IsExtensionMethod_m4010269726_MetadataUsageId;
extern const uint32_t PathUtilities_HasSubDirectory_m804950350_MetadataUsageId;
extern const uint32_t PropertyInfoExtensions_DeAliasProperty_m3306904115_MetadataUsageId;
extern const uint32_t PropertyInfoExtensions_IsAliasProperty_m417595411_MetadataUsageId;
extern const uint32_t PropertyInfoExtensions_IsAutoProperty_m699352069_MetadataUsageId;
extern const uint32_t StringExtensions_IsNullOrWhitespace_m3968263663_MetadataUsageId;
extern const uint32_t StringExtensions_ToTitleCase_m1099386125_MetadataUsageId;
extern const uint32_t TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274_MetadataUsageId;
extern const uint32_t TypeExtensions_AreGenericConstraintsSatisfiedBy_m3989792819_MetadataUsageId;
extern const uint32_t TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143_MetadataUsageId;
extern const uint32_t TypeExtensions_CreateNiceName_m1114677372_MetadataUsageId;
extern const uint32_t TypeExtensions_GenericArgumentsContainsTypes_m411034277_MetadataUsageId;
extern const uint32_t TypeExtensions_GenericParameterIsFulfilledBy_m678722123_MetadataUsageId;
extern const uint32_t TypeExtensions_GenericParameterIsFulfilledBy_m907762269_MetadataUsageId;
extern const uint32_t TypeExtensions_GetAllMembers_m2556387623_MetadataUsageId;
extern const uint32_t TypeExtensions_GetAllMembers_m569881445_MetadataUsageId;
extern const uint32_t TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627_MetadataUsageId;
extern const uint32_t TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118_MetadataUsageId;
extern const uint32_t TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m1793324098_MetadataUsageId;
extern const uint32_t TypeExtensions_GetBaseClasses_m302166337_MetadataUsageId;
extern const uint32_t TypeExtensions_GetBaseTypes_m1398060321_MetadataUsageId;
extern const uint32_t TypeExtensions_GetCachedNiceName_m3030179020_MetadataUsageId;
extern const uint32_t TypeExtensions_GetCastMethodDelegate_m2926053447_MetadataUsageId;
extern const uint32_t TypeExtensions_GetCastMethod_m3599746289_MetadataUsageId;
extern const uint32_t TypeExtensions_GetCompilableNiceFullName_m79608041_MetadataUsageId;
extern const uint32_t TypeExtensions_GetCompilableNiceName_m3309848918_MetadataUsageId;
extern const uint32_t TypeExtensions_GetEnumBitmask_m2667989293_MetadataUsageId;
extern const uint32_t TypeExtensions_GetGenericBaseType_m225141450_MetadataUsageId;
extern const uint32_t TypeExtensions_GetGenericBaseType_m3164880977_MetadataUsageId;
extern const uint32_t TypeExtensions_GetGenericConstraintsString_m2590875364_MetadataUsageId;
extern const uint32_t TypeExtensions_GetGenericParameterConstraintsString_m1569308089_MetadataUsageId;
extern const uint32_t TypeExtensions_GetInheritanceDistance_m2457922913_MetadataUsageId;
extern const uint32_t TypeExtensions_GetMemberValue_m2747957946_MetadataUsageId;
extern const uint32_t TypeExtensions_GetNiceFullName_m2504614971_MetadataUsageId;
extern const uint32_t TypeExtensions_GetNiceName_m2140991116_MetadataUsageId;
extern const uint32_t TypeExtensions_GetOperatorMethod_m555772733_MetadataUsageId;
extern const uint32_t TypeExtensions_GetOperatorMethods_m2090576811_MetadataUsageId;
extern const uint32_t TypeExtensions_GetReturnType_m1722435742_MetadataUsageId;
extern const uint32_t TypeExtensions_HasCastDefined_m1369193891_MetadataUsageId;
extern const uint32_t TypeExtensions_HasCustomAttribute_m3261241330_MetadataUsageId;
extern const uint32_t TypeExtensions_HasCustomAttribute_m456943493_MetadataUsageId;
extern const uint32_t TypeExtensions_HasParamaters_m257329311_MetadataUsageId;
extern const uint32_t TypeExtensions_ImplementsOpenGenericClass_m2650584869_MetadataUsageId;
extern const uint32_t TypeExtensions_ImplementsOpenGenericInterface_m3084334200_MetadataUsageId;
extern const uint32_t TypeExtensions_ImplementsOpenGenericType_m3675074644_MetadataUsageId;
extern const uint32_t TypeExtensions_InheritsFrom_m434511460_MetadataUsageId;
extern const uint32_t TypeExtensions_IsCastableTo_m2813347195_MetadataUsageId;
extern const uint32_t TypeExtensions_IsFullyConstructedGenericType_m809342933_MetadataUsageId;
extern const uint32_t TypeExtensions_IsValidIdentifierPartCharacter_m2068727156_MetadataUsageId;
extern const uint32_t TypeExtensions_IsValidIdentifierStartCharacter_m769556910_MetadataUsageId;
extern const uint32_t TypeExtensions_IsValidIdentifier_m3751677380_MetadataUsageId;
extern const uint32_t TypeExtensions_SetMemberValue_m278274216_MetadataUsageId;
extern const uint32_t TypeExtensions_TryInferGenericParameters_m1077962252_MetadataUsageId;
extern const uint32_t TypeExtensions_TypeNameGauntlet_m2028448992_MetadataUsageId;
extern const uint32_t TypeExtensions__cctor_m74757846_MetadataUsageId;
extern const uint32_t U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1317355286_MetadataUsageId;
extern const uint32_t U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_m3603743745_MetadataUsageId;
extern const uint32_t U3CGetAllMembersU3Ed__43_MoveNext_m3659538151_MetadataUsageId;
extern const uint32_t U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1610761930_MetadataUsageId;
extern const uint32_t U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m2438628648_MetadataUsageId;
extern const uint32_t U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m3985260114_MetadataUsageId;
extern const uint32_t U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3491823437_MetadataUsageId;
extern const uint32_t U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_m3832819096_MetadataUsageId;
extern const uint32_t U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_m3682386764_MetadataUsageId;
extern const uint32_t U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m1853955262_MetadataUsageId;
extern const uint32_t U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m615367899_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m3161334231_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m2349605198_MetadataUsageId;
extern const uint32_t UnityExtensions_SafeIsUnityNull_m135202044_MetadataUsageId;
extern const uint32_t UnityExtensions__cctor_m4112108510_MetadataUsageId;
extern const uint32_t UnityVersion_IsVersionOrGreater_m588543013_MetadataUsageId;
extern const uint32_t UnityVersion__cctor_m2678664592_MetadataUsageId;
extern const uint32_t UnsafeUtilities_MemoryCopy_m2366990917_MetadataUsageId;
extern const uint32_t UnsafeUtilities_StringFromBytes_m1754034413_MetadataUsageId;
extern const uint32_t UnsafeUtilities_StringToBytes_m87138792_MetadataUsageId;
extern const uint32_t Vector2DictionaryKeyPathProvider_GetPathStringFromKey_m475561918_MetadataUsageId;
extern const uint32_t Vector2DictionaryKeyPathProvider__ctor_m645920001_MetadataUsageId;
extern const uint32_t Vector2DictionaryKeyPathProvider_get_ProviderID_m4142048575_MetadataUsageId;
extern const uint32_t Vector2Formatter_Read_m1475415392_MetadataUsageId;
extern const uint32_t Vector2Formatter_Write_m4061323822_MetadataUsageId;
extern const uint32_t Vector2Formatter__cctor_m2866700392_MetadataUsageId;
extern const uint32_t Vector2Formatter__ctor_m3888705115_MetadataUsageId;
extern const uint32_t Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m3423976823_MetadataUsageId;
extern const uint32_t Vector3DictionaryKeyPathProvider__ctor_m1297447965_MetadataUsageId;
extern const uint32_t Vector3DictionaryKeyPathProvider_get_ProviderID_m2013560377_MetadataUsageId;
extern const uint32_t Vector3Formatter_Read_m1977710546_MetadataUsageId;
extern const uint32_t Vector3Formatter_Write_m2220682641_MetadataUsageId;
extern const uint32_t Vector3Formatter__cctor_m675765975_MetadataUsageId;
extern const uint32_t Vector3Formatter__ctor_m2713626732_MetadataUsageId;
extern const uint32_t Vector4DictionaryKeyPathProvider_GetPathStringFromKey_m1680229459_MetadataUsageId;
extern const uint32_t Vector4DictionaryKeyPathProvider__ctor_m4226819291_MetadataUsageId;
extern const uint32_t Vector4DictionaryKeyPathProvider_get_ProviderID_m2239077201_MetadataUsageId;
extern const uint32_t Vector4Formatter_Read_m2959555402_MetadataUsageId;
extern const uint32_t Vector4Formatter_Write_m1996430823_MetadataUsageId;
extern const uint32_t Vector4Formatter__cctor_m3471128330_MetadataUsageId;
extern const uint32_t Vector4Formatter__ctor_m1779727181_MetadataUsageId;
struct Assembly_t_marshaled_com;
struct Assembly_t_marshaled_pinvoke;
struct CultureData_t1899656083_marshaled_com;
struct CultureData_t1899656083_marshaled_pinvoke;
struct CultureInfo_t4157843068_marshaled_com;
struct CultureInfo_t4157843068_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct AttributeU5BU5D_t1575011174;
struct BooleanU5BU5D_t2897418192;
struct ByteU5BU5D_t4116647657;
struct CharU5BU5D_t3528271667;
struct DelegateU5BU5D_t1703627840;
struct ObjectU5BU5D_t2843939325;
struct FieldInfoU5BU5D_t846150980;
struct MemberInfoU5BU5D_t1302094432;
struct MethodInfoU5BU5D_t2572182361;
struct ParameterInfoU5BU5D_t390618515;
struct StringU5BU5D_t1281789340;
struct TypeU5BU5D_t3940880105;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BASEDICTIONARYKEYPATHPROVIDER_1_T1761792157_H
#define BASEDICTIONARYKEYPATHPROVIDER_1_T1761792157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector2>
struct  BaseDictionaryKeyPathProvider_1_t1761792157  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDICTIONARYKEYPATHPROVIDER_1_T1761792157_H
#ifndef BASEDICTIONARYKEYPATHPROVIDER_1_T3327876098_H
#define BASEDICTIONARYKEYPATHPROVIDER_1_T3327876098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector3>
struct  BaseDictionaryKeyPathProvider_1_t3327876098  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDICTIONARYKEYPATHPROVIDER_1_T3327876098_H
#ifndef BASEDICTIONARYKEYPATHPROVIDER_1_T2924591571_H
#define BASEDICTIONARYKEYPATHPROVIDER_1_T2924591571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector4>
struct  BaseDictionaryKeyPathProvider_1_t2924591571  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDICTIONARYKEYPATHPROVIDER_1_T2924591571_H
#ifndef MINIMALBASEFORMATTER_1_T3461213901_H
#define MINIMALBASEFORMATTER_1_T3461213901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector2>
struct  MinimalBaseFormatter_1_t3461213901  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3461213901_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3461213901_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3461213901_H
#ifndef MINIMALBASEFORMATTER_1_T732330546_H
#define MINIMALBASEFORMATTER_1_T732330546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector3>
struct  MinimalBaseFormatter_1_t732330546  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t732330546_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t732330546_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T732330546_H
#ifndef MINIMALBASEFORMATTER_1_T329046019_H
#define MINIMALBASEFORMATTER_1_T329046019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector4>
struct  MinimalBaseFormatter_1_t329046019  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t329046019_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t329046019_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T329046019_H
#ifndef SERIALIZER_T962918574_H
#define SERIALIZER_T962918574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer
struct  Serializer_t962918574  : public RuntimeObject
{
public:

public:
};

struct Serializer_t962918574_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Serialization.Serializer::PrimitiveReaderWriterTypes
	Dictionary_2_t633324528 * ___PrimitiveReaderWriterTypes_0;
	// System.Object Sirenix.Serialization.Serializer::LOCK
	RuntimeObject * ___LOCK_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.Serializer> Sirenix.Serialization.Serializer::ReaderWriterCache
	Dictionary_2_t3407265638 * ___ReaderWriterCache_2;

public:
	inline static int32_t get_offset_of_PrimitiveReaderWriterTypes_0() { return static_cast<int32_t>(offsetof(Serializer_t962918574_StaticFields, ___PrimitiveReaderWriterTypes_0)); }
	inline Dictionary_2_t633324528 * get_PrimitiveReaderWriterTypes_0() const { return ___PrimitiveReaderWriterTypes_0; }
	inline Dictionary_2_t633324528 ** get_address_of_PrimitiveReaderWriterTypes_0() { return &___PrimitiveReaderWriterTypes_0; }
	inline void set_PrimitiveReaderWriterTypes_0(Dictionary_2_t633324528 * value)
	{
		___PrimitiveReaderWriterTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveReaderWriterTypes_0), value);
	}

	inline static int32_t get_offset_of_LOCK_1() { return static_cast<int32_t>(offsetof(Serializer_t962918574_StaticFields, ___LOCK_1)); }
	inline RuntimeObject * get_LOCK_1() const { return ___LOCK_1; }
	inline RuntimeObject ** get_address_of_LOCK_1() { return &___LOCK_1; }
	inline void set_LOCK_1(RuntimeObject * value)
	{
		___LOCK_1 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_1), value);
	}

	inline static int32_t get_offset_of_ReaderWriterCache_2() { return static_cast<int32_t>(offsetof(Serializer_t962918574_StaticFields, ___ReaderWriterCache_2)); }
	inline Dictionary_2_t3407265638 * get_ReaderWriterCache_2() const { return ___ReaderWriterCache_2; }
	inline Dictionary_2_t3407265638 ** get_address_of_ReaderWriterCache_2() { return &___ReaderWriterCache_2; }
	inline void set_ReaderWriterCache_2(Dictionary_2_t3407265638 * value)
	{
		___ReaderWriterCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___ReaderWriterCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T962918574_H
#ifndef IMMUTABLELIST_T1819414204_H
#define IMMUTABLELIST_T1819414204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.ImmutableList
struct  ImmutableList_t1819414204  : public RuntimeObject
{
public:
	// System.Collections.IList Sirenix.Serialization.Utilities.ImmutableList::innerList
	RuntimeObject* ___innerList_0;

public:
	inline static int32_t get_offset_of_innerList_0() { return static_cast<int32_t>(offsetof(ImmutableList_t1819414204, ___innerList_0)); }
	inline RuntimeObject* get_innerList_0() const { return ___innerList_0; }
	inline RuntimeObject** get_address_of_innerList_0() { return &___innerList_0; }
	inline void set_innerList_0(RuntimeObject* value)
	{
		___innerList_0 = value;
		Il2CppCodeGenWriteBarrier((&___innerList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLELIST_T1819414204_H
#ifndef U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T57684215_H
#define U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T57684215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25
struct  U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Sirenix.Serialization.Utilities.ImmutableList Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>4__this
	ImmutableList_t1819414204 * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E4__this_2)); }
	inline ImmutableList_t1819414204 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ImmutableList_t1819414204 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ImmutableList_t1819414204 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T57684215_H
#ifndef LINQEXTENSIONS_T1130353347_H
#define LINQEXTENSIONS_T1130353347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.LinqExtensions
struct  LinqExtensions_t1130353347  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINQEXTENSIONS_T1130353347_H
#ifndef MEMBERINFOEXTENSIONS_T68516722_H
#define MEMBERINFOEXTENSIONS_T68516722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberInfoExtensions
struct  MemberInfoExtensions_t68516722  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFOEXTENSIONS_T68516722_H
#ifndef METHODINFOEXTENSIONS_T3469285330_H
#define METHODINFOEXTENSIONS_T3469285330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MethodInfoExtensions
struct  MethodInfoExtensions_t3469285330  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFOEXTENSIONS_T3469285330_H
#ifndef PATHUTILITIES_T4096232836_H
#define PATHUTILITIES_T4096232836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.PathUtilities
struct  PathUtilities_t4096232836  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHUTILITIES_T4096232836_H
#ifndef PROPERTYINFOEXTENSIONS_T1420793386_H
#define PROPERTYINFOEXTENSIONS_T1420793386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.PropertyInfoExtensions
struct  PropertyInfoExtensions_t1420793386  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFOEXTENSIONS_T1420793386_H
#ifndef STRINGEXTENSIONS_T508951950_H
#define STRINGEXTENSIONS_T508951950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.StringExtensions
struct  StringExtensions_t508951950  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEXTENSIONS_T508951950_H
#ifndef TYPEEXTENSIONS_T1614484489_H
#define TYPEEXTENSIONS_T1614484489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions
struct  TypeExtensions_t1614484489  : public RuntimeObject
{
public:

public:
};

struct TypeExtensions_t1614484489_StaticFields
{
public:
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfaction_LOCK
	RuntimeObject * ___GenericConstraintsSatisfaction_LOCK_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfactionInferredParameters
	Dictionary_2_t633324528 * ___GenericConstraintsSatisfactionInferredParameters_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfactionResolvedMap
	Dictionary_2_t633324528 * ___GenericConstraintsSatisfactionResolvedMap_2;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfactionProcessedParams
	HashSet_1_t1048894234 * ___GenericConstraintsSatisfactionProcessedParams_3;
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::WeaklyTypedTypeCastDelegates_LOCK
	RuntimeObject * ___WeaklyTypedTypeCastDelegates_LOCK_4;
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::StronglyTypedTypeCastDelegates_LOCK
	RuntimeObject * ___StronglyTypedTypeCastDelegates_LOCK_5;
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>> Sirenix.Serialization.Utilities.TypeExtensions::WeaklyTypedTypeCastDelegates
	DoubleLookupDictionary_3_t650546409 * ___WeaklyTypedTypeCastDelegates_6;
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate> Sirenix.Serialization.Utilities.TypeExtensions::StronglyTypedTypeCastDelegates
	DoubleLookupDictionary_3_t3686776144 * ___StronglyTypedTypeCastDelegates_7;
	// System.Collections.Generic.HashSet`1<System.String> Sirenix.Serialization.Utilities.TypeExtensions::ReservedCSharpKeywords
	HashSet_1_t412400163 * ___ReservedCSharpKeywords_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Sirenix.Serialization.Utilities.TypeExtensions::TypeNameAlternatives
	Dictionary_2_t1632706988 * ___TypeNameAlternatives_9;
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::CachedNiceNames_LOCK
	RuntimeObject * ___CachedNiceNames_LOCK_10;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> Sirenix.Serialization.Utilities.TypeExtensions::CachedNiceNames
	Dictionary_2_t4291797753 * ___CachedNiceNames_11;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions::VoidPointerType
	Type_t * ___VoidPointerType_12;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>> Sirenix.Serialization.Utilities.TypeExtensions::PrimitiveImplicitCasts
	Dictionary_2_t3493241298 * ___PrimitiveImplicitCasts_13;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::ExplicitCastIntegrals
	HashSet_1_t1048894234 * ___ExplicitCastIntegrals_14;

public:
	inline static int32_t get_offset_of_GenericConstraintsSatisfaction_LOCK_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfaction_LOCK_0)); }
	inline RuntimeObject * get_GenericConstraintsSatisfaction_LOCK_0() const { return ___GenericConstraintsSatisfaction_LOCK_0; }
	inline RuntimeObject ** get_address_of_GenericConstraintsSatisfaction_LOCK_0() { return &___GenericConstraintsSatisfaction_LOCK_0; }
	inline void set_GenericConstraintsSatisfaction_LOCK_0(RuntimeObject * value)
	{
		___GenericConstraintsSatisfaction_LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfaction_LOCK_0), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionInferredParameters_1() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfactionInferredParameters_1)); }
	inline Dictionary_2_t633324528 * get_GenericConstraintsSatisfactionInferredParameters_1() const { return ___GenericConstraintsSatisfactionInferredParameters_1; }
	inline Dictionary_2_t633324528 ** get_address_of_GenericConstraintsSatisfactionInferredParameters_1() { return &___GenericConstraintsSatisfactionInferredParameters_1; }
	inline void set_GenericConstraintsSatisfactionInferredParameters_1(Dictionary_2_t633324528 * value)
	{
		___GenericConstraintsSatisfactionInferredParameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionInferredParameters_1), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionResolvedMap_2() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfactionResolvedMap_2)); }
	inline Dictionary_2_t633324528 * get_GenericConstraintsSatisfactionResolvedMap_2() const { return ___GenericConstraintsSatisfactionResolvedMap_2; }
	inline Dictionary_2_t633324528 ** get_address_of_GenericConstraintsSatisfactionResolvedMap_2() { return &___GenericConstraintsSatisfactionResolvedMap_2; }
	inline void set_GenericConstraintsSatisfactionResolvedMap_2(Dictionary_2_t633324528 * value)
	{
		___GenericConstraintsSatisfactionResolvedMap_2 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionResolvedMap_2), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionProcessedParams_3() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfactionProcessedParams_3)); }
	inline HashSet_1_t1048894234 * get_GenericConstraintsSatisfactionProcessedParams_3() const { return ___GenericConstraintsSatisfactionProcessedParams_3; }
	inline HashSet_1_t1048894234 ** get_address_of_GenericConstraintsSatisfactionProcessedParams_3() { return &___GenericConstraintsSatisfactionProcessedParams_3; }
	inline void set_GenericConstraintsSatisfactionProcessedParams_3(HashSet_1_t1048894234 * value)
	{
		___GenericConstraintsSatisfactionProcessedParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionProcessedParams_3), value);
	}

	inline static int32_t get_offset_of_WeaklyTypedTypeCastDelegates_LOCK_4() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___WeaklyTypedTypeCastDelegates_LOCK_4)); }
	inline RuntimeObject * get_WeaklyTypedTypeCastDelegates_LOCK_4() const { return ___WeaklyTypedTypeCastDelegates_LOCK_4; }
	inline RuntimeObject ** get_address_of_WeaklyTypedTypeCastDelegates_LOCK_4() { return &___WeaklyTypedTypeCastDelegates_LOCK_4; }
	inline void set_WeaklyTypedTypeCastDelegates_LOCK_4(RuntimeObject * value)
	{
		___WeaklyTypedTypeCastDelegates_LOCK_4 = value;
		Il2CppCodeGenWriteBarrier((&___WeaklyTypedTypeCastDelegates_LOCK_4), value);
	}

	inline static int32_t get_offset_of_StronglyTypedTypeCastDelegates_LOCK_5() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___StronglyTypedTypeCastDelegates_LOCK_5)); }
	inline RuntimeObject * get_StronglyTypedTypeCastDelegates_LOCK_5() const { return ___StronglyTypedTypeCastDelegates_LOCK_5; }
	inline RuntimeObject ** get_address_of_StronglyTypedTypeCastDelegates_LOCK_5() { return &___StronglyTypedTypeCastDelegates_LOCK_5; }
	inline void set_StronglyTypedTypeCastDelegates_LOCK_5(RuntimeObject * value)
	{
		___StronglyTypedTypeCastDelegates_LOCK_5 = value;
		Il2CppCodeGenWriteBarrier((&___StronglyTypedTypeCastDelegates_LOCK_5), value);
	}

	inline static int32_t get_offset_of_WeaklyTypedTypeCastDelegates_6() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___WeaklyTypedTypeCastDelegates_6)); }
	inline DoubleLookupDictionary_3_t650546409 * get_WeaklyTypedTypeCastDelegates_6() const { return ___WeaklyTypedTypeCastDelegates_6; }
	inline DoubleLookupDictionary_3_t650546409 ** get_address_of_WeaklyTypedTypeCastDelegates_6() { return &___WeaklyTypedTypeCastDelegates_6; }
	inline void set_WeaklyTypedTypeCastDelegates_6(DoubleLookupDictionary_3_t650546409 * value)
	{
		___WeaklyTypedTypeCastDelegates_6 = value;
		Il2CppCodeGenWriteBarrier((&___WeaklyTypedTypeCastDelegates_6), value);
	}

	inline static int32_t get_offset_of_StronglyTypedTypeCastDelegates_7() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___StronglyTypedTypeCastDelegates_7)); }
	inline DoubleLookupDictionary_3_t3686776144 * get_StronglyTypedTypeCastDelegates_7() const { return ___StronglyTypedTypeCastDelegates_7; }
	inline DoubleLookupDictionary_3_t3686776144 ** get_address_of_StronglyTypedTypeCastDelegates_7() { return &___StronglyTypedTypeCastDelegates_7; }
	inline void set_StronglyTypedTypeCastDelegates_7(DoubleLookupDictionary_3_t3686776144 * value)
	{
		___StronglyTypedTypeCastDelegates_7 = value;
		Il2CppCodeGenWriteBarrier((&___StronglyTypedTypeCastDelegates_7), value);
	}

	inline static int32_t get_offset_of_ReservedCSharpKeywords_8() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___ReservedCSharpKeywords_8)); }
	inline HashSet_1_t412400163 * get_ReservedCSharpKeywords_8() const { return ___ReservedCSharpKeywords_8; }
	inline HashSet_1_t412400163 ** get_address_of_ReservedCSharpKeywords_8() { return &___ReservedCSharpKeywords_8; }
	inline void set_ReservedCSharpKeywords_8(HashSet_1_t412400163 * value)
	{
		___ReservedCSharpKeywords_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReservedCSharpKeywords_8), value);
	}

	inline static int32_t get_offset_of_TypeNameAlternatives_9() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___TypeNameAlternatives_9)); }
	inline Dictionary_2_t1632706988 * get_TypeNameAlternatives_9() const { return ___TypeNameAlternatives_9; }
	inline Dictionary_2_t1632706988 ** get_address_of_TypeNameAlternatives_9() { return &___TypeNameAlternatives_9; }
	inline void set_TypeNameAlternatives_9(Dictionary_2_t1632706988 * value)
	{
		___TypeNameAlternatives_9 = value;
		Il2CppCodeGenWriteBarrier((&___TypeNameAlternatives_9), value);
	}

	inline static int32_t get_offset_of_CachedNiceNames_LOCK_10() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___CachedNiceNames_LOCK_10)); }
	inline RuntimeObject * get_CachedNiceNames_LOCK_10() const { return ___CachedNiceNames_LOCK_10; }
	inline RuntimeObject ** get_address_of_CachedNiceNames_LOCK_10() { return &___CachedNiceNames_LOCK_10; }
	inline void set_CachedNiceNames_LOCK_10(RuntimeObject * value)
	{
		___CachedNiceNames_LOCK_10 = value;
		Il2CppCodeGenWriteBarrier((&___CachedNiceNames_LOCK_10), value);
	}

	inline static int32_t get_offset_of_CachedNiceNames_11() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___CachedNiceNames_11)); }
	inline Dictionary_2_t4291797753 * get_CachedNiceNames_11() const { return ___CachedNiceNames_11; }
	inline Dictionary_2_t4291797753 ** get_address_of_CachedNiceNames_11() { return &___CachedNiceNames_11; }
	inline void set_CachedNiceNames_11(Dictionary_2_t4291797753 * value)
	{
		___CachedNiceNames_11 = value;
		Il2CppCodeGenWriteBarrier((&___CachedNiceNames_11), value);
	}

	inline static int32_t get_offset_of_VoidPointerType_12() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___VoidPointerType_12)); }
	inline Type_t * get_VoidPointerType_12() const { return ___VoidPointerType_12; }
	inline Type_t ** get_address_of_VoidPointerType_12() { return &___VoidPointerType_12; }
	inline void set_VoidPointerType_12(Type_t * value)
	{
		___VoidPointerType_12 = value;
		Il2CppCodeGenWriteBarrier((&___VoidPointerType_12), value);
	}

	inline static int32_t get_offset_of_PrimitiveImplicitCasts_13() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___PrimitiveImplicitCasts_13)); }
	inline Dictionary_2_t3493241298 * get_PrimitiveImplicitCasts_13() const { return ___PrimitiveImplicitCasts_13; }
	inline Dictionary_2_t3493241298 ** get_address_of_PrimitiveImplicitCasts_13() { return &___PrimitiveImplicitCasts_13; }
	inline void set_PrimitiveImplicitCasts_13(Dictionary_2_t3493241298 * value)
	{
		___PrimitiveImplicitCasts_13 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveImplicitCasts_13), value);
	}

	inline static int32_t get_offset_of_ExplicitCastIntegrals_14() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___ExplicitCastIntegrals_14)); }
	inline HashSet_1_t1048894234 * get_ExplicitCastIntegrals_14() const { return ___ExplicitCastIntegrals_14; }
	inline HashSet_1_t1048894234 ** get_address_of_ExplicitCastIntegrals_14() { return &___ExplicitCastIntegrals_14; }
	inline void set_ExplicitCastIntegrals_14(HashSet_1_t1048894234 * value)
	{
		___ExplicitCastIntegrals_14 = value;
		Il2CppCodeGenWriteBarrier((&___ExplicitCastIntegrals_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T1614484489_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T2941737288_H
#define U3CU3EC__DISPLAYCLASS22_0_T2941737288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t2941737288  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::method
	MethodInfo_t * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2941737288, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T2941737288_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T2941409609_H
#define U3CU3EC__DISPLAYCLASS35_0_T2941409609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t2941409609  : public RuntimeObject
{
public:
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::openGenericInterfaceType
	Type_t * ___openGenericInterfaceType_0;

public:
	inline static int32_t get_offset_of_openGenericInterfaceType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t2941409609, ___openGenericInterfaceType_0)); }
	inline Type_t * get_openGenericInterfaceType_0() const { return ___openGenericInterfaceType_0; }
	inline Type_t ** get_address_of_openGenericInterfaceType_0() { return &___openGenericInterfaceType_0; }
	inline void set_openGenericInterfaceType_0(Type_t * value)
	{
		___openGenericInterfaceType_0 = value;
		Il2CppCodeGenWriteBarrier((&___openGenericInterfaceType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T2941409609_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T2941671758_H
#define U3CU3EC__DISPLAYCLASS41_0_T2941671758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t2941671758  : public RuntimeObject
{
public:
	// System.String Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::methodName
	String_t* ___methodName_0;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t2941671758, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T2941671758_H
#ifndef U3CGETBASECLASSESU3ED__48_T3606501197_H
#define U3CGETBASECLASSESU3ED__48_T3606501197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48
struct  U3CGetBaseClassesU3Ed__48_t3606501197  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::includeSelf
	bool ___includeSelf_5;
	// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>3__includeSelf
	bool ___U3CU3E3__includeSelf_6;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<current>5__1
	Type_t * ___U3CcurrentU3E5__1_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_includeSelf_5() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___includeSelf_5)); }
	inline bool get_includeSelf_5() const { return ___includeSelf_5; }
	inline bool* get_address_of_includeSelf_5() { return &___includeSelf_5; }
	inline void set_includeSelf_5(bool value)
	{
		___includeSelf_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__includeSelf_6() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E3__includeSelf_6)); }
	inline bool get_U3CU3E3__includeSelf_6() const { return ___U3CU3E3__includeSelf_6; }
	inline bool* get_address_of_U3CU3E3__includeSelf_6() { return &___U3CU3E3__includeSelf_6; }
	inline void set_U3CU3E3__includeSelf_6(bool value)
	{
		___U3CU3E3__includeSelf_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CcurrentU3E5__1_7)); }
	inline Type_t * get_U3CcurrentU3E5__1_7() const { return ___U3CcurrentU3E5__1_7; }
	inline Type_t ** get_address_of_U3CcurrentU3E5__1_7() { return &___U3CcurrentU3E5__1_7; }
	inline void set_U3CcurrentU3E5__1_7(Type_t * value)
	{
		___U3CcurrentU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E5__1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBASECLASSESU3ED__48_T3606501197_H
#ifndef UNITYEXTENSIONS_T2928681996_H
#define UNITYEXTENSIONS_T2928681996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.UnityExtensions
struct  UnityExtensions_t2928681996  : public RuntimeObject
{
public:

public:
};

struct UnityExtensions_t2928681996_StaticFields
{
public:
	// Sirenix.Serialization.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr> Sirenix.Serialization.Utilities.UnityExtensions::UnityObjectCachedPtrFieldGetter
	ValueGetter_2_t1938735483 * ___UnityObjectCachedPtrFieldGetter_0;

public:
	inline static int32_t get_offset_of_UnityObjectCachedPtrFieldGetter_0() { return static_cast<int32_t>(offsetof(UnityExtensions_t2928681996_StaticFields, ___UnityObjectCachedPtrFieldGetter_0)); }
	inline ValueGetter_2_t1938735483 * get_UnityObjectCachedPtrFieldGetter_0() const { return ___UnityObjectCachedPtrFieldGetter_0; }
	inline ValueGetter_2_t1938735483 ** get_address_of_UnityObjectCachedPtrFieldGetter_0() { return &___UnityObjectCachedPtrFieldGetter_0; }
	inline void set_UnityObjectCachedPtrFieldGetter_0(ValueGetter_2_t1938735483 * value)
	{
		___UnityObjectCachedPtrFieldGetter_0 = value;
		Il2CppCodeGenWriteBarrier((&___UnityObjectCachedPtrFieldGetter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXTENSIONS_T2928681996_H
#ifndef UNITYVERSION_T744152913_H
#define UNITYVERSION_T744152913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.UnityVersion
struct  UnityVersion_t744152913  : public RuntimeObject
{
public:

public:
};

struct UnityVersion_t744152913_StaticFields
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.UnityVersion::Major
	int32_t ___Major_0;
	// System.Int32 Sirenix.Serialization.Utilities.UnityVersion::Minor
	int32_t ___Minor_1;

public:
	inline static int32_t get_offset_of_Major_0() { return static_cast<int32_t>(offsetof(UnityVersion_t744152913_StaticFields, ___Major_0)); }
	inline int32_t get_Major_0() const { return ___Major_0; }
	inline int32_t* get_address_of_Major_0() { return &___Major_0; }
	inline void set_Major_0(int32_t value)
	{
		___Major_0 = value;
	}

	inline static int32_t get_offset_of_Minor_1() { return static_cast<int32_t>(offsetof(UnityVersion_t744152913_StaticFields, ___Minor_1)); }
	inline int32_t get_Minor_1() const { return ___Minor_1; }
	inline int32_t* get_address_of_Minor_1() { return &___Minor_1; }
	inline void set_Minor_1(int32_t value)
	{
		___Minor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVERSION_T744152913_H
#ifndef UNSAFEUTILITIES_T3361570935_H
#define UNSAFEUTILITIES_T3361570935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities
struct  UnsafeUtilities_t3361570935  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSAFEUTILITIES_T3361570935_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef BITCONVERTER_T3118986983_H
#define BITCONVERTER_T3118986983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.BitConverter
struct  BitConverter_t3118986983  : public RuntimeObject
{
public:

public:
};

struct BitConverter_t3118986983_StaticFields
{
public:
	// System.Boolean System.BitConverter::IsLittleEndian
	bool ___IsLittleEndian_0;

public:
	inline static int32_t get_offset_of_IsLittleEndian_0() { return static_cast<int32_t>(offsetof(BitConverter_t3118986983_StaticFields, ___IsLittleEndian_0)); }
	inline bool get_IsLittleEndian_0() const { return ___IsLittleEndian_0; }
	inline bool* get_address_of_IsLittleEndian_0() { return &___IsLittleEndian_0; }
	inline void set_IsLittleEndian_0(bool value)
	{
		___IsLittleEndian_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTER_T3118986983_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t885026589* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t1822382459 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t3348751306 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___entries_1)); }
	inline EntryU5BU5D_t885026589* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t885026589** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t885026589* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keys_7)); }
	inline KeyCollection_t1822382459 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t1822382459 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t1822382459 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___values_8)); }
	inline ValueCollection_t3348751306 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t3348751306 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t3348751306 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef DICTIONARY_2_T1782119645_H
#define DICTIONARY_2_T1782119645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Delegate>>
struct  Dictionary_2_t1782119645  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t4121424904* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t1971795116 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t3498163963 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___entries_1)); }
	inline EntryU5BU5D_t4121424904* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t4121424904** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t4121424904* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___keys_7)); }
	inline KeyCollection_t1971795116 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t1971795116 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t1971795116 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ___values_8)); }
	inline ValueCollection_t3498163963 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t3498163963 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t3498163963 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1782119645, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1782119645_H
#ifndef DICTIONARY_2_T3040857206_H
#define DICTIONARY_2_T3040857206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Object,System.Object>>>
struct  Dictionary_2_t3040857206  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t3947126251* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t3230532677 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t461934228 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___entries_1)); }
	inline EntryU5BU5D_t3947126251* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t3947126251** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t3947126251* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___keys_7)); }
	inline KeyCollection_t3230532677 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t3230532677 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t3230532677 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ___values_8)); }
	inline ValueCollection_t461934228 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t461934228 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t461934228 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3040857206, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3040857206_H
#ifndef DICTIONARY_2_T3493241298_H
#define DICTIONARY_2_T3493241298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>
struct  Dictionary_2_t3493241298  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t2605259679* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t3682916769 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t914318320 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___entries_1)); }
	inline EntryU5BU5D_t2605259679* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t2605259679** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t2605259679* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___keys_7)); }
	inline KeyCollection_t3682916769 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t3682916769 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t3682916769 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ___values_8)); }
	inline ValueCollection_t914318320 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t914318320 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t914318320 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3493241298, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3493241298_H
#ifndef DICTIONARY_2_T4291797753_H
#define DICTIONARY_2_T4291797753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct  Dictionary_2_t4291797753  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t2315109884* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t186505928 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t1712874775 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___entries_1)); }
	inline EntryU5BU5D_t2315109884* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t2315109884** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t2315109884* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___keys_7)); }
	inline KeyCollection_t186505928 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t186505928 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t186505928 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ___values_8)); }
	inline ValueCollection_t1712874775 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t1712874775 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t1712874775 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4291797753, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4291797753_H
#ifndef DICTIONARY_2_T633324528_H
#define DICTIONARY_2_T633324528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct  Dictionary_2_t633324528  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t679233353* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t822999999 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t2349368846 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___entries_1)); }
	inline EntryU5BU5D_t679233353* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t679233353** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t679233353* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___keys_7)); }
	inline KeyCollection_t822999999 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t822999999 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t822999999 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ___values_8)); }
	inline ValueCollection_t2349368846 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t2349368846 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t2349368846 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t633324528, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T633324528_H
#ifndef HASHSET_1_T412400163_H
#define HASHSET_1_T412400163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<System.String>
struct  HashSet_1_t412400163  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t385246372* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t3850706103* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t950877179 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____buckets_7)); }
	inline Int32U5BU5D_t385246372* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t385246372** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t385246372* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((&____buckets_7), value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____slots_8)); }
	inline SlotU5BU5D_t3850706103* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_t3850706103** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_t3850706103* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((&____slots_8), value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_12), value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ____siInfo_14)); }
	inline SerializationInfo_t950877179 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t950877179 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t950877179 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&____siInfo_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T412400163_H
#ifndef HASHSET_1_T1048894234_H
#define HASHSET_1_T1048894234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<System.Type>
struct  HashSet_1_t1048894234  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t385246372* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t2214829572* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t950877179 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____buckets_7)); }
	inline Int32U5BU5D_t385246372* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t385246372** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t385246372* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((&____buckets_7), value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____slots_8)); }
	inline SlotU5BU5D_t2214829572* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_t2214829572** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_t2214829572* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((&____slots_8), value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_12), value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t1048894234, ____siInfo_14)); }
	inline SerializationInfo_t950877179 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t950877179 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t950877179 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&____siInfo_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T1048894234_H
#ifndef STACK_1_T3327334215_H
#define STACK_1_T3327334215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Type>
struct  Stack_1_t3327334215  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	TypeU5BU5D_t3940880105* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t3327334215, ____array_0)); }
	inline TypeU5BU5D_t3940880105* get__array_0() const { return ____array_0; }
	inline TypeU5BU5D_t3940880105** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(TypeU5BU5D_t3940880105* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t3327334215, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t3327334215, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t3327334215, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T3327334215_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef CULTUREINFO_T4157843068_H
#define CULTUREINFO_T4157843068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t4157843068  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t435877138 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t3810425522 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t1281789340* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1092934962 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1661121569 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t4157843068 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t4116647657* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t1899656083 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___numInfo_10)); }
	inline NumberFormatInfo_t435877138 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t435877138 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t435877138 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_10), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t2405853701 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t2405853701 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t2405853701 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_11), value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textInfo_12)); }
	inline TextInfo_t3810425522 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t3810425522 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t3810425522 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_12), value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_13), value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_14), value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_15), value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_16), value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_17), value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_18), value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((&___territory_19), value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___native_calendar_names_20)); }
	inline StringU5BU5D_t1281789340* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t1281789340** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t1281789340* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((&___native_calendar_names_20), value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___compareInfo_21)); }
	inline CompareInfo_t1092934962 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t1092934962 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t1092934962 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_21), value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___calendar_24)); }
	inline Calendar_t1661121569 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t1661121569 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t1661121569 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_24), value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_culture_25)); }
	inline CultureInfo_t4157843068 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t4157843068 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t4157843068 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_25), value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_t4116647657* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_t4116647657* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_27), value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_cultureData_28)); }
	inline CultureData_t1899656083 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t1899656083 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t1899656083 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_cultureData_28), value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t4157843068_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t4157843068 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t4157843068 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t4157843068 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t4157843068 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t3046556399 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t3943099367 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t4157843068 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t4157843068 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t4157843068 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_0), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_1), value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t4157843068 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t4157843068 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t4157843068 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((&___default_current_culture_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t4157843068 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t4157843068 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t4157843068 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentUICulture_33), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t4157843068 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t4157843068 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t4157843068 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentCulture_34), value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t3046556399 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t3046556399 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t3046556399 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_35), value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t3943099367 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t3943099367 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t3943099367 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_36), value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t4157843068_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t435877138 * ___numInfo_10;
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	TextInfo_t3810425522 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1092934962 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t1661121569 * ___calendar_24;
	CultureInfo_t4157843068_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_t1899656083_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t4157843068_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t435877138 * ___numInfo_10;
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	TextInfo_t3810425522 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1092934962 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t1661121569 * ___calendar_24;
	CultureInfo_t4157843068_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_t1899656083_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
#endif // CULTUREINFO_T4157843068_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef BINDER_T2999457153_H
#define BINDER_T2999457153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t2999457153  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T2999457153_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef CRITICALFINALIZEROBJECT_T701527852_H
#define CRITICALFINALIZEROBJECT_T701527852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t701527852  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T701527852_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t3528271667* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t3528271667* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t3528271667* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkChars_0), value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkPrevious_1), value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SERIALIZER_1_T3074248596_H
#define SERIALIZER_1_T3074248596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Single>
struct  Serializer_1_t3074248596  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T3074248596_H
#ifndef DOUBLELOOKUPDICTIONARY_3_T3686776144_H
#define DOUBLELOOKUPDICTIONARY_3_T3686776144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate>
struct  DoubleLookupDictionary_3_t3686776144  : public Dictionary_2_t1782119645
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TSecondKey> Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::secondKeyComparer
	RuntimeObject* ___secondKeyComparer_14;

public:
	inline static int32_t get_offset_of_secondKeyComparer_14() { return static_cast<int32_t>(offsetof(DoubleLookupDictionary_3_t3686776144, ___secondKeyComparer_14)); }
	inline RuntimeObject* get_secondKeyComparer_14() const { return ___secondKeyComparer_14; }
	inline RuntimeObject** get_address_of_secondKeyComparer_14() { return &___secondKeyComparer_14; }
	inline void set_secondKeyComparer_14(RuntimeObject* value)
	{
		___secondKeyComparer_14 = value;
		Il2CppCodeGenWriteBarrier((&___secondKeyComparer_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELOOKUPDICTIONARY_3_T3686776144_H
#ifndef DOUBLELOOKUPDICTIONARY_3_T650546409_H
#define DOUBLELOOKUPDICTIONARY_3_T650546409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>
struct  DoubleLookupDictionary_3_t650546409  : public Dictionary_2_t3040857206
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TSecondKey> Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::secondKeyComparer
	RuntimeObject* ___secondKeyComparer_14;

public:
	inline static int32_t get_offset_of_secondKeyComparer_14() { return static_cast<int32_t>(offsetof(DoubleLookupDictionary_3_t650546409, ___secondKeyComparer_14)); }
	inline RuntimeObject* get_secondKeyComparer_14() const { return ___secondKeyComparer_14; }
	inline RuntimeObject** get_address_of_secondKeyComparer_14() { return &___secondKeyComparer_14; }
	inline void set_secondKeyComparer_14(RuntimeObject* value)
	{
		___secondKeyComparer_14 = value;
		Il2CppCodeGenWriteBarrier((&___secondKeyComparer_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELOOKUPDICTIONARY_3_T650546409_H
#ifndef VECTOR2DICTIONARYKEYPATHPROVIDER_T1191773922_H
#define VECTOR2DICTIONARYKEYPATHPROVIDER_T1191773922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector2DictionaryKeyPathProvider
struct  Vector2DictionaryKeyPathProvider_t1191773922  : public BaseDictionaryKeyPathProvider_1_t1761792157
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2DICTIONARYKEYPATHPROVIDER_T1191773922_H
#ifndef VECTOR2FORMATTER_T2520006045_H
#define VECTOR2FORMATTER_T2520006045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector2Formatter
struct  Vector2Formatter_t2520006045  : public MinimalBaseFormatter_1_t3461213901
{
public:

public:
};

struct Vector2Formatter_t2520006045_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.Vector2Formatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(Vector2Formatter_t2520006045_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2FORMATTER_T2520006045_H
#ifndef VECTOR3DICTIONARYKEYPATHPROVIDER_T2227500205_H
#define VECTOR3DICTIONARYKEYPATHPROVIDER_T2227500205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector3DictionaryKeyPathProvider
struct  Vector3DictionaryKeyPathProvider_t2227500205  : public BaseDictionaryKeyPathProvider_1_t3327876098
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3DICTIONARYKEYPATHPROVIDER_T2227500205_H
#ifndef VECTOR3FORMATTER_T2520007008_H
#define VECTOR3FORMATTER_T2520007008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector3Formatter
struct  Vector3Formatter_t2520007008  : public MinimalBaseFormatter_1_t732330546
{
public:

public:
};

struct Vector3Formatter_t2520007008_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.Vector3Formatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(Vector3Formatter_t2520007008_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3FORMATTER_T2520007008_H
#ifndef VECTOR4DICTIONARYKEYPATHPROVIDER_T4213655263_H
#define VECTOR4DICTIONARYKEYPATHPROVIDER_T4213655263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector4DictionaryKeyPathProvider
struct  Vector4DictionaryKeyPathProvider_t4213655263  : public BaseDictionaryKeyPathProvider_1_t2924591571
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4DICTIONARYKEYPATHPROVIDER_T4213655263_H
#ifndef VECTOR4FORMATTER_T2519996543_H
#define VECTOR4FORMATTER_T2519996543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector4Formatter
struct  Vector4Formatter_t2519996543  : public MinimalBaseFormatter_1_t329046019
{
public:

public:
};

struct Vector4Formatter_t2519996543_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.Vector4Formatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(Vector4Formatter_t2519996543_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4FORMATTER_T2519996543_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef EVENTINFO_T_H
#define EVENTINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.EventInfo
struct  EventInfo_t  : public MemberInfo_t
{
public:
	// System.Reflection.EventInfo/AddEventAdapter System.Reflection.EventInfo::cached_add_event
	AddEventAdapter_t1787725097 * ___cached_add_event_0;

public:
	inline static int32_t get_offset_of_cached_add_event_0() { return static_cast<int32_t>(offsetof(EventInfo_t, ___cached_add_event_0)); }
	inline AddEventAdapter_t1787725097 * get_cached_add_event_0() const { return ___cached_add_event_0; }
	inline AddEventAdapter_t1787725097 ** get_address_of_cached_add_event_0() { return &___cached_add_event_0; }
	inline void set_cached_add_event_0(AddEventAdapter_t1787725097 * value)
	{
		___cached_add_event_0 = value;
		Il2CppCodeGenWriteBarrier((&___cached_add_event_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTINFO_T_H
#ifndef FIELDINFO_T_H
#define FIELDINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFO_T_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef PROPERTYINFO_T_H
#define PROPERTYINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyInfo
struct  PropertyInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFO_T_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef THREAD_T2300836069_H
#define THREAD_T2300836069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_t2300836069  : public CriticalFinalizerObject_t701527852
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_t95296544 * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_8;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_9;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_10;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t * ___m_Delegate_12;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t1748372627 * ___m_ExecutionContext_13;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_14;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___internal_thread_6)); }
	inline InternalThread_t95296544 * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_t95296544 ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_t95296544 * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier((&___internal_thread_6), value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_ThreadStartArg_7)); }
	inline RuntimeObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(RuntimeObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ThreadStartArg_7), value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___pending_exception_8)); }
	inline RuntimeObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline RuntimeObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(RuntimeObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_8), value);
	}

	inline static int32_t get_offset_of_principal_9() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___principal_9)); }
	inline RuntimeObject* get_principal_9() const { return ___principal_9; }
	inline RuntimeObject** get_address_of_principal_9() { return &___principal_9; }
	inline void set_principal_9(RuntimeObject* value)
	{
		___principal_9 = value;
		Il2CppCodeGenWriteBarrier((&___principal_9), value);
	}

	inline static int32_t get_offset_of_principal_version_10() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___principal_version_10)); }
	inline int32_t get_principal_version_10() const { return ___principal_version_10; }
	inline int32_t* get_address_of_principal_version_10() { return &___principal_version_10; }
	inline void set_principal_version_10(int32_t value)
	{
		___principal_version_10 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_12() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_Delegate_12)); }
	inline MulticastDelegate_t * get_m_Delegate_12() const { return ___m_Delegate_12; }
	inline MulticastDelegate_t ** get_address_of_m_Delegate_12() { return &___m_Delegate_12; }
	inline void set_m_Delegate_12(MulticastDelegate_t * value)
	{
		___m_Delegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegate_12), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_13() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_ExecutionContext_13)); }
	inline ExecutionContext_t1748372627 * get_m_ExecutionContext_13() const { return ___m_ExecutionContext_13; }
	inline ExecutionContext_t1748372627 ** get_address_of_m_ExecutionContext_13() { return &___m_ExecutionContext_13; }
	inline void set_m_ExecutionContext_13(ExecutionContext_t1748372627 * value)
	{
		___m_ExecutionContext_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutionContext_13), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_14() { return static_cast<int32_t>(offsetof(Thread_t2300836069, ___m_ExecutionContextBelongsToOuterScope_14)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_14() const { return ___m_ExecutionContextBelongsToOuterScope_14; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_14() { return &___m_ExecutionContextBelongsToOuterScope_14; }
	inline void set_m_ExecutionContextBelongsToOuterScope_14(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_14 = value;
	}
};

struct Thread_t2300836069_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t1707895399 * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_t2427220165 * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_t2427220165 * ___s_asyncLocalCurrentUICulture_5;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_t2300836069_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t1707895399 * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t1707895399 ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t1707895399 * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStoreMgr_0), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_t2300836069_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_t2427220165 * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_t2427220165 ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_t2427220165 * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentCulture_4), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_t2300836069_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_t2427220165 * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_t2427220165 ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_t2427220165 * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentUICulture_5), value);
	}
};

struct Thread_t2300836069_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_t2567786569 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t4157843068 * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t4157843068 * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_t2300836069 * ___current_thread_11;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_t2567786569 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_t2567786569 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_t2567786569 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStore_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t4157843068 * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t4157843068 ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t4157843068 * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCulture_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t4157843068 * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t4157843068 ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t4157843068 * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentUICulture_3), value);
	}

	inline static int32_t get_offset_of_current_thread_11() { return static_cast<int32_t>(offsetof(Thread_t2300836069_ThreadStaticFields, ___current_thread_11)); }
	inline Thread_t2300836069 * get_current_thread_11() const { return ___current_thread_11; }
	inline Thread_t2300836069 ** get_address_of_current_thread_11() { return &___current_thread_11; }
	inline void set_current_thread_11(Thread_t2300836069 * value)
	{
		___current_thread_11 = value;
		Il2CppCodeGenWriteBarrier((&___current_thread_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_T2300836069_H
#ifndef UINT16_T2177724958_H
#define UINT16_T2177724958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t2177724958 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t2177724958, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T2177724958_H
#ifndef UINT64_T4134040092_H
#define UINT64_T4134040092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t4134040092 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t4134040092, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T4134040092_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef MEMBERALIASFIELDINFO_T3617726363_H
#define MEMBERALIASFIELDINFO_T3617726363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberAliasFieldInfo
struct  MemberAliasFieldInfo_t3617726363  : public FieldInfo_t
{
public:
	// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.MemberAliasFieldInfo::aliasedField
	FieldInfo_t * ___aliasedField_1;
	// System.String Sirenix.Serialization.Utilities.MemberAliasFieldInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedField_1() { return static_cast<int32_t>(offsetof(MemberAliasFieldInfo_t3617726363, ___aliasedField_1)); }
	inline FieldInfo_t * get_aliasedField_1() const { return ___aliasedField_1; }
	inline FieldInfo_t ** get_address_of_aliasedField_1() { return &___aliasedField_1; }
	inline void set_aliasedField_1(FieldInfo_t * value)
	{
		___aliasedField_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedField_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasFieldInfo_t3617726363, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASFIELDINFO_T3617726363_H
#ifndef MEMBERALIASPROPERTYINFO_T4204559890_H
#define MEMBERALIASPROPERTYINFO_T4204559890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberAliasPropertyInfo
struct  MemberAliasPropertyInfo_t4204559890  : public PropertyInfo_t
{
public:
	// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::aliasedProperty
	PropertyInfo_t * ___aliasedProperty_1;
	// System.String Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedProperty_1() { return static_cast<int32_t>(offsetof(MemberAliasPropertyInfo_t4204559890, ___aliasedProperty_1)); }
	inline PropertyInfo_t * get_aliasedProperty_1() const { return ___aliasedProperty_1; }
	inline PropertyInfo_t ** get_address_of_aliasedProperty_1() { return &___aliasedProperty_1; }
	inline void set_aliasedProperty_1(PropertyInfo_t * value)
	{
		___aliasedProperty_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedProperty_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasPropertyInfo_t4204559890, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASPROPERTYINFO_T4204559890_H
#ifndef OPERATOR_T1465414565_H
#define OPERATOR_T1465414565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.Operator
struct  Operator_t1465414565 
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.Operator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Operator_t1465414565, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_T1465414565_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef ARITHMETICEXCEPTION_T4283546778_H
#define ARITHMETICEXCEPTION_T4283546778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArithmeticException
struct  ArithmeticException_t4283546778  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARITHMETICEXCEPTION_T4283546778_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef FILEATTRIBUTES_T3417205536_H
#define FILEATTRIBUTES_T3417205536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t3417205536 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t3417205536, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T3417205536_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#define NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t3489357830  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T3489357830_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef CONSTRUCTORINFO_T5769829_H
#define CONSTRUCTORINFO_T5769829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ConstructorInfo
struct  ConstructorInfo_t5769829  : public MethodBase_t
{
public:

public:
};

struct ConstructorInfo_t5769829_StaticFields
{
public:
	// System.String System.Reflection.ConstructorInfo::ConstructorName
	String_t* ___ConstructorName_0;
	// System.String System.Reflection.ConstructorInfo::TypeConstructorName
	String_t* ___TypeConstructorName_1;

public:
	inline static int32_t get_offset_of_ConstructorName_0() { return static_cast<int32_t>(offsetof(ConstructorInfo_t5769829_StaticFields, ___ConstructorName_0)); }
	inline String_t* get_ConstructorName_0() const { return ___ConstructorName_0; }
	inline String_t** get_address_of_ConstructorName_0() { return &___ConstructorName_0; }
	inline void set_ConstructorName_0(String_t* value)
	{
		___ConstructorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorName_0), value);
	}

	inline static int32_t get_offset_of_TypeConstructorName_1() { return static_cast<int32_t>(offsetof(ConstructorInfo_t5769829_StaticFields, ___TypeConstructorName_1)); }
	inline String_t* get_TypeConstructorName_1() const { return ___TypeConstructorName_1; }
	inline String_t** get_address_of_TypeConstructorName_1() { return &___TypeConstructorName_1; }
	inline void set_TypeConstructorName_1(String_t* value)
	{
		___TypeConstructorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeConstructorName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORINFO_T5769829_H
#ifndef FIELDATTRIBUTES_T400321159_H
#define FIELDATTRIBUTES_T400321159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldAttributes
struct  FieldAttributes_t400321159 
{
public:
	// System.Int32 System.Reflection.FieldAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FieldAttributes_t400321159, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDATTRIBUTES_T400321159_H
#ifndef GENERICPARAMETERATTRIBUTES_T1159165531_H
#define GENERICPARAMETERATTRIBUTES_T1159165531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.GenericParameterAttributes
struct  GenericParameterAttributes_t1159165531 
{
public:
	// System.Int32 System.Reflection.GenericParameterAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GenericParameterAttributes_t1159165531, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICPARAMETERATTRIBUTES_T1159165531_H
#ifndef METHODATTRIBUTES_T2366443849_H
#define METHODATTRIBUTES_T2366443849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodAttributes
struct  MethodAttributes_t2366443849 
{
public:
	// System.Int32 System.Reflection.MethodAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MethodAttributes_t2366443849, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODATTRIBUTES_T2366443849_H
#ifndef METHODIMPLATTRIBUTES_T3646023817_H
#define METHODIMPLATTRIBUTES_T3646023817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodImplAttributes
struct  MethodImplAttributes_t3646023817 
{
public:
	// System.Int32 System.Reflection.MethodImplAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MethodImplAttributes_t3646023817, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODIMPLATTRIBUTES_T3646023817_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef PARAMETERATTRIBUTES_T1826424051_H
#define PARAMETERATTRIBUTES_T1826424051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t1826424051 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParameterAttributes_t1826424051, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T1826424051_H
#ifndef PROPERTYATTRIBUTES_T3388002996_H
#define PROPERTYATTRIBUTES_T3388002996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyAttributes
struct  PropertyAttributes_t3388002996 
{
public:
	// System.Int32 System.Reflection.PropertyAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyAttributes_t3388002996, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTES_T3388002996_H
#ifndef GCHANDLETYPE_T3432586689_H
#define GCHANDLETYPE_T3432586689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandleType
struct  GCHandleType_t3432586689 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GCHandleType_t3432586689, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLETYPE_T3432586689_H
#ifndef RUNTIMEFIELDHANDLE_T1871169219_H
#define RUNTIMEFIELDHANDLE_T1871169219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1871169219 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1871169219, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1871169219_H
#ifndef RUNTIMEMETHODHANDLE_T1133924984_H
#define RUNTIMEMETHODHANDLE_T1133924984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeMethodHandle
struct  RuntimeMethodHandle_t1133924984 
{
public:
	// System.IntPtr System.RuntimeMethodHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeMethodHandle_t1133924984, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEMETHODHANDLE_T1133924984_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MEMBERALIASMETHODINFO_T2838764708_H
#define MEMBERALIASMETHODINFO_T2838764708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberAliasMethodInfo
struct  MemberAliasMethodInfo_t2838764708  : public MethodInfo_t
{
public:
	// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::aliasedMethod
	MethodInfo_t * ___aliasedMethod_1;
	// System.String Sirenix.Serialization.Utilities.MemberAliasMethodInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedMethod_1() { return static_cast<int32_t>(offsetof(MemberAliasMethodInfo_t2838764708, ___aliasedMethod_1)); }
	inline MethodInfo_t * get_aliasedMethod_1() const { return ___aliasedMethod_1; }
	inline MethodInfo_t ** get_address_of_aliasedMethod_1() { return &___aliasedMethod_1; }
	inline void set_aliasedMethod_1(MethodInfo_t * value)
	{
		___aliasedMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedMethod_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasMethodInfo_t2838764708, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASMETHODINFO_T2838764708_H
#ifndef U3CGETALLMEMBERSU3ED__42_T2156530709_H
#define U3CGETALLMEMBERSU3ED__42_T2156530709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42
struct  U3CGetAllMembersU3Ed__42_t2156530709  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>2__current
	MemberInfo_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<currentType>5__1
	Type_t * ___U3CcurrentTypeU3E5__1_7;
	// System.Reflection.MemberInfo[] Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>7__wrap1
	MemberInfoU5BU5D_t1302094432* ___U3CU3E7__wrap1_8;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E2__current_1)); }
	inline MemberInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MemberInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MemberInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTypeU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CcurrentTypeU3E5__1_7)); }
	inline Type_t * get_U3CcurrentTypeU3E5__1_7() const { return ___U3CcurrentTypeU3E5__1_7; }
	inline Type_t ** get_address_of_U3CcurrentTypeU3E5__1_7() { return &___U3CcurrentTypeU3E5__1_7; }
	inline void set_U3CcurrentTypeU3E5__1_7(Type_t * value)
	{
		___U3CcurrentTypeU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentTypeU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_8() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E7__wrap1_8)); }
	inline MemberInfoU5BU5D_t1302094432* get_U3CU3E7__wrap1_8() const { return ___U3CU3E7__wrap1_8; }
	inline MemberInfoU5BU5D_t1302094432** get_address_of_U3CU3E7__wrap1_8() { return &___U3CU3E7__wrap1_8; }
	inline void set_U3CU3E7__wrap1_8(MemberInfoU5BU5D_t1302094432* value)
	{
		___U3CU3E7__wrap1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_9() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E7__wrap2_9)); }
	inline int32_t get_U3CU3E7__wrap2_9() const { return ___U3CU3E7__wrap2_9; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_9() { return &___U3CU3E7__wrap2_9; }
	inline void set_U3CU3E7__wrap2_9(int32_t value)
	{
		___U3CU3E7__wrap2_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLMEMBERSU3ED__42_T2156530709_H
#ifndef U3CGETALLMEMBERSU3ED__43_T590446768_H
#define U3CGETALLMEMBERSU3ED__43_T590446768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43
struct  U3CGetAllMembersU3Ed__43_t590446768  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>2__current
	MemberInfo_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.String Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::name
	String_t* ___name_7;
	// System.String Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>3__name
	String_t* ___U3CU3E3__name_8;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E2__current_1)); }
	inline MemberInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MemberInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MemberInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__name_8() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E3__name_8)); }
	inline String_t* get_U3CU3E3__name_8() const { return ___U3CU3E3__name_8; }
	inline String_t** get_address_of_U3CU3E3__name_8() { return &___U3CU3E3__name_8; }
	inline void set_U3CU3E3__name_8(String_t* value)
	{
		___U3CU3E3__name_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__name_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLMEMBERSU3ED__43_T590446768_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef MONOIOSTAT_T592533987_H
#define MONOIOSTAT_T592533987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t592533987 
{
public:
	// System.IO.FileAttributes System.IO.MonoIOStat::fileAttributes
	int32_t ___fileAttributes_0;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_1;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_2;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_3;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_4;

public:
	inline static int32_t get_offset_of_fileAttributes_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___fileAttributes_0)); }
	inline int32_t get_fileAttributes_0() const { return ___fileAttributes_0; }
	inline int32_t* get_address_of_fileAttributes_0() { return &___fileAttributes_0; }
	inline void set_fileAttributes_0(int32_t value)
	{
		___fileAttributes_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Length_1)); }
	inline int64_t get_Length_1() const { return ___Length_1; }
	inline int64_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int64_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_CreationTime_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___CreationTime_2)); }
	inline int64_t get_CreationTime_2() const { return ___CreationTime_2; }
	inline int64_t* get_address_of_CreationTime_2() { return &___CreationTime_2; }
	inline void set_CreationTime_2(int64_t value)
	{
		___CreationTime_2 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastAccessTime_3)); }
	inline int64_t get_LastAccessTime_3() const { return ___LastAccessTime_3; }
	inline int64_t* get_address_of_LastAccessTime_3() { return &___LastAccessTime_3; }
	inline void set_LastAccessTime_3(int64_t value)
	{
		___LastAccessTime_3 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastWriteTime_4)); }
	inline int64_t get_LastWriteTime_4() const { return ___LastWriteTime_4; }
	inline int64_t* get_address_of_LastWriteTime_4() { return &___LastWriteTime_4; }
	inline void set_LastWriteTime_4(int64_t value)
	{
		___LastWriteTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOIOSTAT_T592533987_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef OVERFLOWEXCEPTION_T2020128637_H
#define OVERFLOWEXCEPTION_T2020128637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.OverflowException
struct  OverflowException_t2020128637  : public ArithmeticException_t4283546778
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERFLOWEXCEPTION_T2020128637_H
#ifndef MODULE_T2987026101_H
#define MODULE_T2987026101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Module
struct  Module_t2987026101  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Module::_impl
	intptr_t ____impl_2;
	// System.Reflection.Assembly System.Reflection.Module::assembly
	Assembly_t * ___assembly_3;
	// System.String System.Reflection.Module::fqname
	String_t* ___fqname_4;
	// System.String System.Reflection.Module::name
	String_t* ___name_5;
	// System.String System.Reflection.Module::scopename
	String_t* ___scopename_6;
	// System.Boolean System.Reflection.Module::is_resource
	bool ___is_resource_7;
	// System.Int32 System.Reflection.Module::token
	int32_t ___token_8;

public:
	inline static int32_t get_offset_of__impl_2() { return static_cast<int32_t>(offsetof(Module_t2987026101, ____impl_2)); }
	inline intptr_t get__impl_2() const { return ____impl_2; }
	inline intptr_t* get_address_of__impl_2() { return &____impl_2; }
	inline void set__impl_2(intptr_t value)
	{
		____impl_2 = value;
	}

	inline static int32_t get_offset_of_assembly_3() { return static_cast<int32_t>(offsetof(Module_t2987026101, ___assembly_3)); }
	inline Assembly_t * get_assembly_3() const { return ___assembly_3; }
	inline Assembly_t ** get_address_of_assembly_3() { return &___assembly_3; }
	inline void set_assembly_3(Assembly_t * value)
	{
		___assembly_3 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_3), value);
	}

	inline static int32_t get_offset_of_fqname_4() { return static_cast<int32_t>(offsetof(Module_t2987026101, ___fqname_4)); }
	inline String_t* get_fqname_4() const { return ___fqname_4; }
	inline String_t** get_address_of_fqname_4() { return &___fqname_4; }
	inline void set_fqname_4(String_t* value)
	{
		___fqname_4 = value;
		Il2CppCodeGenWriteBarrier((&___fqname_4), value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Module_t2987026101, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_scopename_6() { return static_cast<int32_t>(offsetof(Module_t2987026101, ___scopename_6)); }
	inline String_t* get_scopename_6() const { return ___scopename_6; }
	inline String_t** get_address_of_scopename_6() { return &___scopename_6; }
	inline void set_scopename_6(String_t* value)
	{
		___scopename_6 = value;
		Il2CppCodeGenWriteBarrier((&___scopename_6), value);
	}

	inline static int32_t get_offset_of_is_resource_7() { return static_cast<int32_t>(offsetof(Module_t2987026101, ___is_resource_7)); }
	inline bool get_is_resource_7() const { return ___is_resource_7; }
	inline bool* get_address_of_is_resource_7() { return &___is_resource_7; }
	inline void set_is_resource_7(bool value)
	{
		___is_resource_7 = value;
	}

	inline static int32_t get_offset_of_token_8() { return static_cast<int32_t>(offsetof(Module_t2987026101, ___token_8)); }
	inline int32_t get_token_8() const { return ___token_8; }
	inline int32_t* get_address_of_token_8() { return &___token_8; }
	inline void set_token_8(int32_t value)
	{
		___token_8 = value;
	}
};

struct Module_t2987026101_StaticFields
{
public:
	// System.Reflection.TypeFilter System.Reflection.Module::FilterTypeName
	TypeFilter_t2356120900 * ___FilterTypeName_0;
	// System.Reflection.TypeFilter System.Reflection.Module::FilterTypeNameIgnoreCase
	TypeFilter_t2356120900 * ___FilterTypeNameIgnoreCase_1;

public:
	inline static int32_t get_offset_of_FilterTypeName_0() { return static_cast<int32_t>(offsetof(Module_t2987026101_StaticFields, ___FilterTypeName_0)); }
	inline TypeFilter_t2356120900 * get_FilterTypeName_0() const { return ___FilterTypeName_0; }
	inline TypeFilter_t2356120900 ** get_address_of_FilterTypeName_0() { return &___FilterTypeName_0; }
	inline void set_FilterTypeName_0(TypeFilter_t2356120900 * value)
	{
		___FilterTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterTypeName_0), value);
	}

	inline static int32_t get_offset_of_FilterTypeNameIgnoreCase_1() { return static_cast<int32_t>(offsetof(Module_t2987026101_StaticFields, ___FilterTypeNameIgnoreCase_1)); }
	inline TypeFilter_t2356120900 * get_FilterTypeNameIgnoreCase_1() const { return ___FilterTypeNameIgnoreCase_1; }
	inline TypeFilter_t2356120900 ** get_address_of_FilterTypeNameIgnoreCase_1() { return &___FilterTypeNameIgnoreCase_1; }
	inline void set_FilterTypeNameIgnoreCase_1(TypeFilter_t2356120900 * value)
	{
		___FilterTypeNameIgnoreCase_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterTypeNameIgnoreCase_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Module
struct Module_t2987026101_marshaled_pinvoke
{
	intptr_t ____impl_2;
	Assembly_t_marshaled_pinvoke* ___assembly_3;
	char* ___fqname_4;
	char* ___name_5;
	char* ___scopename_6;
	int32_t ___is_resource_7;
	int32_t ___token_8;
};
// Native definition for COM marshalling of System.Reflection.Module
struct Module_t2987026101_marshaled_com
{
	intptr_t ____impl_2;
	Assembly_t_marshaled_com* ___assembly_3;
	Il2CppChar* ___fqname_4;
	Il2CppChar* ___name_5;
	Il2CppChar* ___scopename_6;
	int32_t ___is_resource_7;
	int32_t ___token_8;
};
#endif // MODULE_T2987026101_H
#ifndef PARAMETERINFO_T1861056598_H
#define PARAMETERINFO_T1861056598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t1861056598  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.ParameterInfo::marshalAs
	MarshalAsAttribute_t3522571978 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t1861056598, ___marshalAs_6)); }
	inline MarshalAsAttribute_t3522571978 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline MarshalAsAttribute_t3522571978 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(MarshalAsAttribute_t3522571978 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterInfo
struct ParameterInfo_t1861056598_marshaled_pinvoke
{
	Type_t * ___ClassImpl_0;
	Il2CppIUnknown* ___DefaultValueImpl_1;
	MemberInfo_t * ___MemberImpl_2;
	char* ___NameImpl_3;
	int32_t ___PositionImpl_4;
	int32_t ___AttrsImpl_5;
	MarshalAsAttribute_t3522571978 * ___marshalAs_6;
};
// Native definition for COM marshalling of System.Reflection.ParameterInfo
struct ParameterInfo_t1861056598_marshaled_com
{
	Type_t * ___ClassImpl_0;
	Il2CppIUnknown* ___DefaultValueImpl_1;
	MemberInfo_t * ___MemberImpl_2;
	Il2CppChar* ___NameImpl_3;
	int32_t ___PositionImpl_4;
	int32_t ___AttrsImpl_5;
	MarshalAsAttribute_t3522571978 * ___marshalAs_6;
};
#endif // PARAMETERINFO_T1861056598_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t426314064 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t426314064 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2999457153 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2999457153 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef VALUEGETTER_2_T1938735483_H
#define VALUEGETTER_2_T1938735483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr>
struct  ValueGetter_2_t1938735483  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEGETTER_2_T1938735483_H
#ifndef WEAKVALUEGETTER_T2669515580_H
#define WEAKVALUEGETTER_T2669515580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.WeakValueGetter
struct  WeakValueGetter_t2669515580  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKVALUEGETTER_T2669515580_H
#ifndef WEAKVALUESETTER_T2630980412_H
#define WEAKVALUESETTER_T2630980412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.WeakValueSetter
struct  WeakValueSetter_t2630980412  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKVALUESETTER_T2630980412_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef FUNC_2_T2447130374_H
#define FUNC_2_T2447130374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Object>
struct  Func_2_t2447130374  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2447130374_H
#ifndef FUNC_2_T3487522507_H
#define FUNC_2_T3487522507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct  Func_2_t3487522507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3487522507_H
#ifndef FUNC_2_T561252955_H
#define FUNC_2_T561252955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Type,System.Boolean>
struct  Func_2_t561252955  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T561252955_H
#ifndef FILESYSTEMINFO_T3745885336_H
#define FILESYSTEMINFO_T3745885336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemInfo
struct  FileSystemInfo_t3745885336  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.MonoIOStat System.IO.FileSystemInfo::_data
	MonoIOStat_t592533987  ____data_1;
	// System.Int32 System.IO.FileSystemInfo::_dataInitialised
	int32_t ____dataInitialised_2;
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_3;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_4;
	// System.String System.IO.FileSystemInfo::_displayPath
	String_t* ____displayPath_5;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ____data_1)); }
	inline MonoIOStat_t592533987  get__data_1() const { return ____data_1; }
	inline MonoIOStat_t592533987 * get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(MonoIOStat_t592533987  value)
	{
		____data_1 = value;
	}

	inline static int32_t get_offset_of__dataInitialised_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ____dataInitialised_2)); }
	inline int32_t get__dataInitialised_2() const { return ____dataInitialised_2; }
	inline int32_t* get_address_of__dataInitialised_2() { return &____dataInitialised_2; }
	inline void set__dataInitialised_2(int32_t value)
	{
		____dataInitialised_2 = value;
	}

	inline static int32_t get_offset_of_FullPath_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___FullPath_3)); }
	inline String_t* get_FullPath_3() const { return ___FullPath_3; }
	inline String_t** get_address_of_FullPath_3() { return &___FullPath_3; }
	inline void set_FullPath_3(String_t* value)
	{
		___FullPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___FullPath_3), value);
	}

	inline static int32_t get_offset_of_OriginalPath_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___OriginalPath_4)); }
	inline String_t* get_OriginalPath_4() const { return ___OriginalPath_4; }
	inline String_t** get_address_of_OriginalPath_4() { return &___OriginalPath_4; }
	inline void set_OriginalPath_4(String_t* value)
	{
		___OriginalPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPath_4), value);
	}

	inline static int32_t get_offset_of__displayPath_5() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ____displayPath_5)); }
	inline String_t* get__displayPath_5() const { return ____displayPath_5; }
	inline String_t** get_address_of__displayPath_5() { return &____displayPath_5; }
	inline void set__displayPath_5(String_t* value)
	{
		____displayPath_5 = value;
		Il2CppCodeGenWriteBarrier((&____displayPath_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMINFO_T3745885336_H
#ifndef DIRECTORYINFO_T35957480_H
#define DIRECTORYINFO_T35957480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DirectoryInfo
struct  DirectoryInfo_t35957480  : public FileSystemInfo_t3745885336
{
public:
	// System.String System.IO.DirectoryInfo::current
	String_t* ___current_6;
	// System.String System.IO.DirectoryInfo::parent
	String_t* ___parent_7;

public:
	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___current_6)); }
	inline String_t* get_current_6() const { return ___current_6; }
	inline String_t** get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(String_t* value)
	{
		___current_6 = value;
		Il2CppCodeGenWriteBarrier((&___current_6), value);
	}

	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___parent_7)); }
	inline String_t* get_parent_7() const { return ___parent_7; }
	inline String_t** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(String_t* value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYINFO_T35957480_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t1861056598 * m_Items[1];

public:
	inline ParameterInfo_t1861056598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t1861056598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2572182361  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Attribute[]
struct AttributeU5BU5D_t1575011174  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Attribute_t861562559 * m_Items[1];

public:
	inline Attribute_t861562559 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Attribute_t861562559 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Attribute_t861562559 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Attribute_t861562559 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Attribute_t861562559 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Attribute_t861562559 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t846150980  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FieldInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t2897418192  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1302094432  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MemberInfo_t * m_Items[1];

public:
	inline MemberInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MemberInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MemberInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MemberInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MemberInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MemberInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1703627840  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t1188392813 * m_Items[1];

public:
	inline Delegate_t1188392813 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t1188392813 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes<System.Object>(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MemberInfoExtensions_GetAttributes_TisRuntimeObject_m1308126979_gshared (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* Enumerable_ToArray_TisRuntimeObject_m2312436077_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes<System.Object>(System.Reflection.MemberInfo,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MemberInfoExtensions_GetAttributes_TisRuntimeObject_m3684811251_gshared (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, bool ___inherit1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Skip_TisRuntimeObject_m2980217246_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m1996088172_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m3105409630_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_m2714930061_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(!0)
extern "C" IL2CPP_METHOD_ATTR bool HashSet_1_Contains_m3173358704_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Object,System.Object,System.Object>::TryGetInnerValue(TFirstKey,TSecondKey,TValue&)
extern "C" IL2CPP_METHOD_ATTR bool DoubleLookupDictionary_3_TryGetInnerValue_m2964253210_gshared (DoubleLookupDictionary_3_t1271286727 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject ** p2, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Func_2__ctor_m3860723828_gshared (Func_2_t2447130374 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// TValue Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Object,System.Object,System.Object>::AddInner(TFirstKey,TSecondKey,TValue)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * DoubleLookupDictionary_3_AddInner_m2252092773_gshared (DoubleLookupDictionary_3_t1271286727 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Func_2__ctor_m135484220_gshared (Func_2_t3759279471 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR bool Enumerable_Any_TisRuntimeObject_m3853239423_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t3759279471 * p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Where_TisRuntimeObject_m3454096398_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t3759279471 * p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Concat_TisRuntimeObject_m584263597_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C" IL2CPP_METHOD_ATTR bool Enumerable_Contains_TisRuntimeObject_m2774664838_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m1938428402_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m258553009_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m3993293265_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_m3919933788_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void HashSet_1_Clear_m507835370_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR bool HashSet_1_Add_m3343353213_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void Stack_1__ctor_m1722075264_gshared (Stack_1_t3923495619 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Stack_1_Pop_m756553478_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C" IL2CPP_METHOD_ATTR void Stack_1_Push_m1669856732_gshared (Stack_1_t3923495619 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t Stack_1_get_Count_m1599740434_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HashSet_1__ctor_m4231804131_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Object,System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DoubleLookupDictionary_3__ctor_m2298829807_gshared (DoubleLookupDictionary_3_t1271286727 * __this, const RuntimeMethod* method);
// Sirenix.Serialization.Utilities.ValueGetter`2<InstanceType,FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceFieldGetter<System.Object,System.IntPtr>(System.Reflection.FieldInfo)
extern "C" IL2CPP_METHOD_ATTR ValueGetter_2_t3364892844 * EmitUtilities_CreateInstanceFieldGetter_TisRuntimeObject_TisIntPtr_t_m3038257917_gshared (RuntimeObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const RuntimeMethod* method);
// FieldType Sirenix.Serialization.Utilities.ValueGetter`2<System.Object,System.IntPtr>::Invoke(InstanceType&)
extern "C" IL2CPP_METHOD_ATTR intptr_t ValueGetter_2_Invoke_m2693039288_gshared (ValueGetter_2_t3364892844 * __this, RuntimeObject ** p0, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector2>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BaseDictionaryKeyPathProvider_1__ctor_m3157277402_gshared (BaseDictionaryKeyPathProvider_1_t1761792157 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Serializer`1<System.Single>::WriteValue(T,Sirenix.Serialization.IDataWriter)
extern "C" IL2CPP_METHOD_ATTR void Serializer_1_WriteValue_m1523856254_gshared (Serializer_1_t3074248596 * __this, float p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector2>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MinimalBaseFormatter_1__ctor_m4165236585_gshared (MinimalBaseFormatter_1_t3461213901 * __this, const RuntimeMethod* method);
// Sirenix.Serialization.Serializer`1<T> Sirenix.Serialization.Serializer::Get<System.Single>()
extern "C" IL2CPP_METHOD_ATTR Serializer_1_t3074248596 * Serializer_Get_TisSingle_t1397266774_m2693733542_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector3>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BaseDictionaryKeyPathProvider_1__ctor_m1975566667_gshared (BaseDictionaryKeyPathProvider_1_t3327876098 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector3>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MinimalBaseFormatter_1__ctor_m153188201_gshared (MinimalBaseFormatter_1_t732330546 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector4>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BaseDictionaryKeyPathProvider_1__ctor_m607895852_gshared (BaseDictionaryKeyPathProvider_1_t2924591571 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector4>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MinimalBaseFormatter_1__ctor_m4046092137_gshared (MinimalBaseFormatter_1_t329046019 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>m__Finally1()
extern "C" IL2CPP_METHOD_ATTR void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m615367899 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m1959394440 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.FieldInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FieldInfo__ctor_m3305575002 (FieldInfo_t * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void System.Reflection.MethodInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MethodInfo__ctor_m2805780217 (MethodInfo_t * __this, const RuntimeMethod* method);
// System.Void System.Reflection.PropertyInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PropertyInfo__ctor_m4235916625 (PropertyInfo_t * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes<System.Attribute>(System.Reflection.MemberInfo)
inline RuntimeObject* MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m2331120560 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, MemberInfo_t *, const RuntimeMethod*))MemberInfoExtensions_GetAttributes_TisRuntimeObject_m1308126979_gshared)(__this /* static, unused */, ___member0, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.Attribute>(System.Collections.Generic.IEnumerable`1<!!0>)
inline AttributeU5BU5D_t1575011174* Enumerable_ToArray_TisAttribute_t861562559_m3360527948 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  AttributeU5BU5D_t1575011174* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m2312436077_gshared)(__this /* static, unused */, p0, method);
}
// System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes<System.Attribute>(System.Reflection.MemberInfo,System.Boolean)
inline RuntimeObject* MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m4106349652 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, bool ___inherit1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, MemberInfo_t *, bool, const RuntimeMethod*))MemberInfoExtensions_GetAttributes_TisRuntimeObject_m3684811251_gshared)(__this /* static, unused */, ___member0, ___inherit1, method);
}
// System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase)
extern "C" IL2CPP_METHOD_ATTR String_t* MethodInfoExtensions_GetFullName_m3000052143 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.StringExtensions::ToTitleCase(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* StringExtensions_ToTitleCase_m1099386125 (RuntimeObject * __this /* static, unused */, String_t* ___input0, const RuntimeMethod* method);
// System.Boolean System.Reflection.FieldInfo::get_IsStatic()
extern "C" IL2CPP_METHOD_ATTR bool FieldInfo_get_IsStatic_m3482711189 (FieldInfo_t * __this, const RuntimeMethod* method);
// System.Boolean System.Reflection.MethodBase::get_IsStatic()
extern "C" IL2CPP_METHOD_ATTR bool MethodBase_get_IsStatic_m2399864271 (MethodBase_t * __this, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsSealed()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsSealed_m3543837727 (Type_t * __this, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsAbstract()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsAbstract_m1120089130 (Type_t * __this, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C" IL2CPP_METHOD_ATTR CultureInfo_t4157843068 * CultureInfo_get_InvariantCulture_m3532445182 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.IFormatProvider,System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m1881875187 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, String_t* p1, ObjectU5BU5D_t2843939325* p2, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* p0, const RuntimeMethod* method);
// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_AliasedField()
extern "C" IL2CPP_METHOD_ATTR FieldInfo_t * MemberAliasFieldInfo_get_AliasedField_m1830311124 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method);
// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_AliasedProperty()
extern "C" IL2CPP_METHOD_ATTR PropertyInfo_t * MemberAliasPropertyInfo_get_AliasedProperty_m1519871680 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method);
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_AliasedMethod()
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * MemberAliasMethodInfo_get_AliasedMethod_m3436203498 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.MemberInfoExtensions::GetNiceName(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR String_t* MemberInfoExtensions_GetNiceName_m2681496769 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StringBuilder__ctor_m3121283359 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsExtensionMethod(System.Reflection.MethodBase)
extern "C" IL2CPP_METHOD_ATTR bool MethodInfoExtensions_IsExtensionMethod_m4010269726 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m1965104174 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetParamsNames(System.Reflection.MethodBase)
extern "C" IL2CPP_METHOD_ATTR String_t* MethodInfoExtensions_GetParamsNames_m873242453 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.Reflection.ParameterInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
inline RuntimeObject* Enumerable_Skip_TisParameterInfo_t1861056598_m3780503321 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_Skip_TisRuntimeObject_m2980217246_gshared)(__this /* static, unused */, p0, p1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.Reflection.ParameterInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
inline ParameterInfoU5BU5D_t390618515* Enumerable_ToArray_TisParameterInfo_t1861056598_m2516263013 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  ParameterInfoU5BU5D_t390618515* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m2312436077_gshared)(__this /* static, unused */, p0, method);
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetNiceName_m2140991116 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* MethodInfoExtensions_GetFullName_m2745430701 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, String_t* ___extensionMethodPrefix1, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsNested()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsNested_m3546087448 (Type_t * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// System.String System.String::TrimEnd(System.Char[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_TrimEnd_m3824727301 (String_t* __this, CharU5BU5D_t3528271667* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.IO.DirectoryInfo System.IO.DirectoryInfo::get_Parent()
extern "C" IL2CPP_METHOD_ATTR DirectoryInfo_t35957480 * DirectoryInfo_get_Parent_m3736638393 (DirectoryInfo_t35957480 * __this, const RuntimeMethod* method);
// System.Boolean System.Reflection.MethodBase::get_IsAbstract()
extern "C" IL2CPP_METHOD_ATTR bool MethodBase_get_IsAbstract_m428833029 (MethodBase_t * __this, const RuntimeMethod* method);
// System.Boolean System.Reflection.MethodBase::get_IsVirtual()
extern "C" IL2CPP_METHOD_ATTR bool MethodBase_get_IsVirtual_m2008546636 (MethodBase_t * __this, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method);
// System.Boolean System.Char::IsLower(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsLower_m3108076820 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method);
// System.Char System.Char::ToUpper(System.Char,System.Globalization.CultureInfo)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar Char_ToUpper_m3659851865 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, CultureInfo_t4157843068 * p1, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m2383614642 (StringBuilder_t * __this, Il2CppChar p0, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.Char::IsWhiteSpace(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsWhiteSpace_m2148390798 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Enter_m2249409497 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.String>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m1231945001 (Dictionary_2_t4291797753 * __this, Type_t * p0, String_t** p1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t4291797753 *, Type_t *, String_t**, const RuntimeMethod*))Dictionary_2_TryGetValue_m1996088172_gshared)(__this, p0, p1, method);
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::CreateNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_CreateNiceName_m1114677372 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_m2678085394 (Dictionary_2_t4291797753 * __this, Type_t * p0, String_t* p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4291797753 *, Type_t *, String_t*, const RuntimeMethod*))Dictionary_2_Add_m3105409630_gshared)(__this, p0, p1, method);
}
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsArray()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsArray_m2591212821 (Type_t * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::InheritsFrom(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_InheritsFrom_m434511460 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseType1, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsByRef()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsByRef_m1262524108 (Type_t * __this, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.TypeExtensions::TypeNameGauntlet(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_TypeNameGauntlet_m2028448992 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method);
// System.Int32 System.String::IndexOf(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t String_IndexOf_m1977622757 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Type System.Enum::GetUnderlyingType(System.Type)
extern "C" IL2CPP_METHOD_ATTR Type_t * Enum_GetUnderlyingType_m2480312097 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsCastableTo(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsCastableTo_m2813347195 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsPrimitive()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsPrimitive_m1114712797 (Type_t * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>::get_Item(!0)
inline HashSet_1_t1048894234 * Dictionary_2_get_Item_m1040169990 (Dictionary_2_t3493241298 * __this, Type_t * p0, const RuntimeMethod* method)
{
	return ((  HashSet_1_t1048894234 * (*) (Dictionary_2_t3493241298 *, Type_t *, const RuntimeMethod*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Type>::Contains(!0)
inline bool HashSet_1_Contains_m3794231197 (HashSet_1_t1048894234 * __this, Type_t * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t1048894234 *, Type_t *, const RuntimeMethod*))HashSet_1_Contains_m3173358704_gshared)(__this, p0, method);
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethod(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * TypeExtensions_GetCastMethod_m3599746289 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method);
// System.Int32 System.String::IndexOf(System.Char)
extern "C" IL2CPP_METHOD_ATTR int32_t String_IndexOf_m363431711 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
extern "C" IL2CPP_METHOD_ATTR StringU5BU5D_t1281789340* String_Split_m3646115398 (String_t* __this, CharU5BU5D_t3528271667* p0, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifier(System.String)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsValidIdentifier_m3751677380 (RuntimeObject * __this /* static, unused */, String_t* ___identifier0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Contains(!0)
inline bool HashSet_1_Contains_m1669355224 (HashSet_1_t412400163 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t412400163 *, String_t*, const RuntimeMethod*))HashSet_1_Contains_m3173358704_gshared)(__this, p0, method);
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierStartCharacter(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsValidIdentifierStartCharacter_m769556910 (RuntimeObject * __this /* static, unused */, Il2CppChar ___c0, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierPartCharacter(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsValidIdentifierPartCharacter_m2068727156 (RuntimeObject * __this /* static, unused */, Il2CppChar ___c0, const RuntimeMethod* method);
// System.Boolean System.Char::IsLetter(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool Char_IsLetter_m3996985877 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCastDefined(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_HasCastDefined_m1369193891 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>::TryGetInnerValue(TFirstKey,TSecondKey,TValue&)
inline bool DoubleLookupDictionary_3_TryGetInnerValue_m2712346314 (DoubleLookupDictionary_3_t650546409 * __this, Type_t * p0, Type_t * p1, Func_2_t2447130374 ** p2, const RuntimeMethod* method)
{
	return ((  bool (*) (DoubleLookupDictionary_3_t650546409 *, Type_t *, Type_t *, Func_2_t2447130374 **, const RuntimeMethod*))DoubleLookupDictionary_3_TryGetInnerValue_m2964253210_gshared)(__this, p0, p1, p2, method);
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_0__ctor_m759390448 (U3CU3Ec__DisplayClass22_0_t2941737288 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m3860723828 (Func_2_t2447130374 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t2447130374 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m3860723828_gshared)(__this, p0, p1, method);
}
// TValue Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>::AddInner(TFirstKey,TSecondKey,TValue)
inline Func_2_t2447130374 * DoubleLookupDictionary_3_AddInner_m1135260050 (DoubleLookupDictionary_3_t650546409 * __this, Type_t * p0, Type_t * p1, Func_2_t2447130374 * p2, const RuntimeMethod* method)
{
	return ((  Func_2_t2447130374 * (*) (DoubleLookupDictionary_3_t650546409 *, Type_t *, Type_t *, Func_2_t2447130374 *, const RuntimeMethod*))DoubleLookupDictionary_3_AddInner_m2252092773_gshared)(__this, p0, p1, p2, method);
}
// System.Void System.ArgumentNullException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m2751210921 (ArgumentNullException_t1615371798 * __this, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsInterface()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsInterface_m3284996719 (Type_t * __this, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericInterface(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_ImplementsOpenGenericInterface_m3084334200 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericInterfaceType1, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericClass(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_ImplementsOpenGenericClass_m2650584869 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericType1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass35_0__ctor_m563200432 (U3CU3Ec__DisplayClass35_0_t2941409609 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Type,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m604830276 (Func_2_t561252955 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t561252955 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m135484220_gshared)(__this, p0, p1, method);
}
// System.Boolean System.Linq.Enumerable::Any<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline bool Enumerable_Any_TisType_t_m7140321 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t561252955 * p1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t561252955 *, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m3853239423_gshared)(__this /* static, unused */, p0, p1, method);
}
// System.Boolean System.Type::get_IsClass()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsClass_m589177581 (Type_t * __this, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsValueType()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_IsValueType_m3108065642 (Type_t * __this, const RuntimeMethod* method);
// System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericInterface(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR TypeU5BU5D_t3940880105* TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericInterfaceType1, const RuntimeMethod* method);
// System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericClass(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR TypeU5BU5D_t3940880105* TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericType1, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m3058704252 (NotImplementedException_t3489357830 * __this, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * Type_GetMethod_m1197120913 (Type_t * __this, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass41_0__ctor_m3483633306 (U3CU3Ec__DisplayClass41_0_t2941671758 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Reflection.MethodInfo,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m576450289 (Func_2_t3487522507 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3487522507 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m135484220_gshared)(__this, p0, p1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline RuntimeObject* Enumerable_Where_TisMethodInfo_t_m1712952118 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t3487522507 * p1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t3487522507 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m3454096398_gshared)(__this /* static, unused */, p0, p1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
inline MethodInfoU5BU5D_t2572182361* Enumerable_ToArray_TisMethodInfo_t_m1997060780 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  MethodInfoU5BU5D_t2572182361* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m2312436077_gshared)(__this /* static, unused */, p0, method);
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__42__ctor_m464840207 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43__ctor_m424109788 (U3CGetAllMembersU3Ed__43_t590446768 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR Type_t * TypeExtensions_GetGenericBaseType_m225141450 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseType1, int32_t* ___depthCount2, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1809518182 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2163913788 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseClasses(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TypeExtensions_GetBaseClasses_m302166337 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, bool ___includeSelf1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
inline RuntimeObject* Enumerable_Concat_TisType_t_m3921564742 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, RuntimeObject* p1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))Enumerable_Concat_TisRuntimeObject_m584263597_gshared)(__this /* static, unused */, p0, p1, method);
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CGetBaseClassesU3Ed__48__ctor_m3292792360 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m2620390247 (Dictionary_2_t1632706988 * __this, String_t* p0, String_t** p1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t1632706988 *, String_t*, String_t**, const RuntimeMethod*))Dictionary_2_TryGetValue_m1996088172_gshared)(__this, p0, p1, method);
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCachedNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetCachedNiceName_m3030179020 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceFullName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetNiceFullName_m2504614971 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method);
// System.String System.String::Replace(System.Char,System.Char)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m3726209165 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Contains<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
inline bool Enumerable_Contains_TisType_t_m2042470555 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Type_t * p1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Type_t *, const RuntimeMethod*))Enumerable_Contains_TisRuntimeObject_m2774664838_gshared)(__this /* static, unused */, p0, p1, method);
}
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * MethodBase_Invoke_m1776411915 (MethodBase_t * __this, RuntimeObject * p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR void FieldInfo_SetValue_m2460171138 (FieldInfo_t * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Type>::Clear()
inline void Dictionary_2_Clear_m2935595320 (Dictionary_2_t633324528 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t633324528 *, const RuntimeMethod*))Dictionary_2_Clear_m1938428402_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Type>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m1327842169 (Dictionary_2_t633324528 * __this, Type_t * p0, Type_t * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t633324528 *, Type_t *, Type_t *, const RuntimeMethod*))Dictionary_2_set_Item_m258553009_gshared)(__this, p0, p1, method);
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type,System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143 (RuntimeObject * __this /* static, unused */, Type_t * ___genericType0, TypeU5BU5D_t3940880105* ___parameters1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Type>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m2026130383 (Dictionary_2_t633324528 * __this, Type_t * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t633324528 *, Type_t *, const RuntimeMethod*))Dictionary_2_ContainsKey_m3993293265_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,System.Type>::get_Count()
inline int32_t Dictionary_2_get_Count_m3815247294 (Dictionary_2_t633324528 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t633324528 *, const RuntimeMethod*))Dictionary_2_get_Count_m3919933788_gshared)(__this, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.Type,System.Type>::get_Item(!0)
inline Type_t * Dictionary_2_get_Item_m4273472624 (Dictionary_2_t633324528 * __this, Type_t * p0, const RuntimeMethod* method)
{
	return ((  Type_t * (*) (Dictionary_2_t633324528 *, Type_t *, const RuntimeMethod*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method);
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type[],System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_AreGenericConstraintsSatisfiedBy_m3989792819 (RuntimeObject * __this /* static, unused */, TypeU5BU5D_t3940880105* ___definitions0, TypeU5BU5D_t3940880105* ___parameters1, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Type>,System.Collections.Generic.HashSet`1<System.Type>)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_GenericParameterIsFulfilledBy_m907762269 (RuntimeObject * __this /* static, unused */, Type_t * ___genericParameterDefinition0, Type_t * ___parameterType1, Dictionary_2_t633324528 * ___resolvedMap2, HashSet_1_t1048894234 * ___processedParams3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Type>::Clear()
inline void HashSet_1_Clear_m1877719279 (HashSet_1_t1048894234 * __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t1048894234 *, const RuntimeMethod*))HashSet_1_Clear_m507835370_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Type>::Add(!0)
inline bool HashSet_1_Add_m3527721678 (HashSet_1_t1048894234 * __this, Type_t * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t1048894234 *, Type_t *, const RuntimeMethod*))HashSet_1_Add_m3343353213_gshared)(__this, p0, method);
}
// System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[])
extern "C" IL2CPP_METHOD_ATTR ConstructorInfo_t5769829 * Type_GetConstructor_m2219014380 (Type_t * __this, TypeU5BU5D_t3940880105* p0, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Exception__ctor_m1152696503 (Exception_t * __this, String_t* p0, const RuntimeMethod* method);
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericParameterConstraintsString(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetGenericParameterConstraintsString_m1569308089 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, bool ___useFullTypeNames1, const RuntimeMethod* method);
// System.String System.String::Join(System.String,System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Join_m2050845953 (RuntimeObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1281789340* p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void Stack_1__ctor_m2267086409 (Stack_1_t3327334215 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t3327334215 *, RuntimeObject*, const RuntimeMethod*))Stack_1__ctor_m1722075264_gshared)(__this, p0, method);
}
// !0 System.Collections.Generic.Stack`1<System.Type>::Pop()
inline Type_t * Stack_1_Pop_m2843357855 (Stack_1_t3327334215 * __this, const RuntimeMethod* method)
{
	return ((  Type_t * (*) (Stack_1_t3327334215 *, const RuntimeMethod*))Stack_1_Pop_m756553478_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(!0)
inline void Stack_1_Push_m506528439 (Stack_1_t3327334215 * __this, Type_t * p0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t3327334215 *, Type_t *, const RuntimeMethod*))Stack_1_Push_m1669856732_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
inline int32_t Stack_1_get_Count_m1286321891 (Stack_1_t3327334215 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Stack_1_t3327334215 *, const RuntimeMethod*))Stack_1_get_Count_m1599740434_gshared)(__this, method);
}
// System.Boolean System.Type::get_HasElementType()
extern "C" IL2CPP_METHOD_ATTR bool Type_get_HasElementType_m710151977 (Type_t * __this, const RuntimeMethod* method);
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsFullyConstructedGenericType(System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsFullyConstructedGenericType_m809342933 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method);
// System.UInt64 System.Convert::ToUInt64(System.Object,System.IFormatProvider)
extern "C" IL2CPP_METHOD_ATTR uint64_t Convert_ToUInt64_m3170916409 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Int64 System.Convert::ToInt64(System.Object,System.IFormatProvider)
extern "C" IL2CPP_METHOD_ATTR int64_t Convert_ToInt64_m2643251823 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Type>::.ctor()
inline void Dictionary_2__ctor_m2733271626 (Dictionary_2_t633324528 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t633324528 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.Void System.Collections.Generic.HashSet`1<System.Type>::.ctor()
inline void HashSet_1__ctor_m3725506494 (HashSet_1_t1048894234 * __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t1048894234 *, const RuntimeMethod*))HashSet_1__ctor_m4231804131_gshared)(__this, method);
}
// System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>::.ctor()
inline void DoubleLookupDictionary_3__ctor_m653869603 (DoubleLookupDictionary_3_t650546409 * __this, const RuntimeMethod* method)
{
	((  void (*) (DoubleLookupDictionary_3_t650546409 *, const RuntimeMethod*))DoubleLookupDictionary_3__ctor_m2298829807_gshared)(__this, method);
}
// System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate>::.ctor()
inline void DoubleLookupDictionary_3__ctor_m3657728929 (DoubleLookupDictionary_3_t3686776144 * __this, const RuntimeMethod* method)
{
	((  void (*) (DoubleLookupDictionary_3_t3686776144 *, const RuntimeMethod*))DoubleLookupDictionary_3__ctor_m2298829807_gshared)(__this, method);
}
// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor()
inline void HashSet_1__ctor_m708940816 (HashSet_1_t412400163 * __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t412400163 *, const RuntimeMethod*))HashSet_1__ctor_m4231804131_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Add(!0)
inline bool HashSet_1_Add_m3328534555 (HashSet_1_t412400163 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t412400163 *, String_t*, const RuntimeMethod*))HashSet_1_Add_m3343353213_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
inline void Dictionary_2__ctor_m444307833 (Dictionary_2_t1632706988 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1632706988 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_m3045345476 (Dictionary_2_t1632706988 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1632706988 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m3105409630_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.String>::.ctor()
inline void Dictionary_2__ctor_m960807489 (Dictionary_2_t4291797753 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4291797753 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>::.ctor()
inline void Dictionary_2__ctor_m2777391954 (Dictionary_2_t3493241298 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t3493241298 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>::Add(!0,!1)
inline void Dictionary_2_Add_m3699546241 (Dictionary_2_t3493241298 * __this, Type_t * p0, HashSet_1_t1048894234 * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t3493241298 *, Type_t *, HashSet_1_t1048894234 *, const RuntimeMethod*))Dictionary_2_Add_m3105409630_gshared)(__this, p0, p1, method);
}
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C" IL2CPP_METHOD_ATTR Thread_t2300836069 * Thread_get_CurrentThread_m4142136012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C" IL2CPP_METHOD_ATTR int32_t Thread_get_ManagedThreadId_m1068113671 (Thread_t2300836069 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1317355286 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>m__Finally1()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m3985260114 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TypeExtensions_GetAllMembers_m569881445 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_m1829376475 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1610761930 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3491823437 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method);
// Sirenix.Serialization.Utilities.ValueGetter`2<InstanceType,FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceFieldGetter<UnityEngine.Object,System.IntPtr>(System.Reflection.FieldInfo)
inline ValueGetter_2_t1938735483 * EmitUtilities_CreateInstanceFieldGetter_TisObject_t631007953_TisIntPtr_t_m3752444982 (RuntimeObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const RuntimeMethod* method)
{
	return ((  ValueGetter_2_t1938735483 * (*) (RuntimeObject * /* static, unused */, FieldInfo_t *, const RuntimeMethod*))EmitUtilities_CreateInstanceFieldGetter_TisRuntimeObject_TisIntPtr_t_m3038257917_gshared)(__this /* static, unused */, ___fieldInfo0, method);
}
// FieldType Sirenix.Serialization.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr>::Invoke(InstanceType&)
inline intptr_t ValueGetter_2_Invoke_m1728989122 (ValueGetter_2_t1938735483 * __this, Object_t631007953 ** p0, const RuntimeMethod* method)
{
	return ((  intptr_t (*) (ValueGetter_2_t1938735483 *, Object_t631007953 **, const RuntimeMethod*))ValueGetter_2_Invoke_m2693039288_gshared)(__this, p0, method);
}
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_m408849716 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_unityVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* Application_get_unityVersion_m1068543125 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR bool Int32_TryParse_m2404707562 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t* p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.String System.String::CreateString(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_CreateString_m1262864254 (String_t* __this, Il2CppChar ___c0, int32_t ___count1, const RuntimeMethod* method);
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern "C" IL2CPP_METHOD_ATTR GCHandle_t3351438187  GCHandle_Alloc_m3823409740 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::get_OffsetToStringData()
extern "C" IL2CPP_METHOD_ATTR int32_t RuntimeHelpers_get_OffsetToStringData_m2192601476 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.IntPtr System.Runtime.InteropServices.GCHandle::AddrOfPinnedObject()
extern "C" IL2CPP_METHOD_ATTR intptr_t GCHandle_AddrOfPinnedObject_m3427142301 (GCHandle_t3351438187 * __this, const RuntimeMethod* method);
// System.Void* System.IntPtr::ToPointer()
extern "C" IL2CPP_METHOD_ATTR void* IntPtr_ToPointer_m4157623054 (intptr_t* __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.InteropServices.GCHandle::get_IsAllocated()
extern "C" IL2CPP_METHOD_ATTR bool GCHandle_get_IsAllocated_m1058226959 (GCHandle_t3351438187 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern "C" IL2CPP_METHOD_ATTR void GCHandle_Free_m1457699368 (GCHandle_t3351438187 * __this, const RuntimeMethod* method);
// System.Int32 System.Single::CompareTo(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Single_CompareTo_m189772128 (float* __this, float p0, const RuntimeMethod* method);
// System.String System.String::Trim()
extern "C" IL2CPP_METHOD_ATTR String_t* String_Trim_m923598732 (String_t* __this, const RuntimeMethod* method);
// System.Single System.Single::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR float Single_Parse_m364357836 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String,System.IFormatProvider)
extern "C" IL2CPP_METHOD_ATTR String_t* Single_ToString_m543431371 (float* __this, String_t* p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector2>::.ctor()
inline void BaseDictionaryKeyPathProvider_1__ctor_m3157277402 (BaseDictionaryKeyPathProvider_1_t1761792157 * __this, const RuntimeMethod* method)
{
	((  void (*) (BaseDictionaryKeyPathProvider_1_t1761792157 *, const RuntimeMethod*))BaseDictionaryKeyPathProvider_1__ctor_m3157277402_gshared)(__this, method);
}
// System.Void Sirenix.Serialization.Serializer`1<System.Single>::WriteValue(T,Sirenix.Serialization.IDataWriter)
inline void Serializer_1_WriteValue_m1523856254 (Serializer_1_t3074248596 * __this, float p0, RuntimeObject* p1, const RuntimeMethod* method)
{
	((  void (*) (Serializer_1_t3074248596 *, float, RuntimeObject*, const RuntimeMethod*))Serializer_1_WriteValue_m1523856254_gshared)(__this, p0, p1, method);
}
// System.Void Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector2>::.ctor()
inline void MinimalBaseFormatter_1__ctor_m4165236585 (MinimalBaseFormatter_1_t3461213901 * __this, const RuntimeMethod* method)
{
	((  void (*) (MinimalBaseFormatter_1_t3461213901 *, const RuntimeMethod*))MinimalBaseFormatter_1__ctor_m4165236585_gshared)(__this, method);
}
// Sirenix.Serialization.Serializer`1<T> Sirenix.Serialization.Serializer::Get<System.Single>()
inline Serializer_1_t3074248596 * Serializer_Get_TisSingle_t1397266774_m2693733542 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  Serializer_1_t3074248596 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Serializer_Get_TisSingle_t1397266774_m2693733542_gshared)(__this /* static, unused */, method);
}
// System.Int32 System.String::IndexOf(System.Char,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t String_IndexOf_m2466398549 (String_t* __this, Il2CppChar p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector3>::.ctor()
inline void BaseDictionaryKeyPathProvider_1__ctor_m1975566667 (BaseDictionaryKeyPathProvider_1_t3327876098 * __this, const RuntimeMethod* method)
{
	((  void (*) (BaseDictionaryKeyPathProvider_1_t3327876098 *, const RuntimeMethod*))BaseDictionaryKeyPathProvider_1__ctor_m1975566667_gshared)(__this, method);
}
// System.Void Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector3>::.ctor()
inline void MinimalBaseFormatter_1__ctor_m153188201 (MinimalBaseFormatter_1_t732330546 * __this, const RuntimeMethod* method)
{
	((  void (*) (MinimalBaseFormatter_1_t732330546 *, const RuntimeMethod*))MinimalBaseFormatter_1__ctor_m153188201_gshared)(__this, method);
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector4>::.ctor()
inline void BaseDictionaryKeyPathProvider_1__ctor_m607895852 (BaseDictionaryKeyPathProvider_1_t2924591571 * __this, const RuntimeMethod* method)
{
	((  void (*) (BaseDictionaryKeyPathProvider_1_t2924591571 *, const RuntimeMethod*))BaseDictionaryKeyPathProvider_1__ctor_m607895852_gshared)(__this, method);
}
// System.Void Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector4>::.ctor()
inline void MinimalBaseFormatter_1__ctor_m4046092137 (MinimalBaseFormatter_1_t329046019 * __this, const RuntimeMethod* method)
{
	((  void (*) (MinimalBaseFormatter_1_t329046019 *, const RuntimeMethod*))MinimalBaseFormatter_1__ctor_m4046092137_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_m2350062166 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m1959394440 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m615367899(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_m3682386764 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_m3682386764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	ImmutableList_t1819414204 * V_2 = NULL;
	RuntimeObject * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			ImmutableList_t1819414204 * L_1 = __this->get_U3CU3E4__this_2();
			V_2 = L_1;
			int32_t L_2 = V_1;
			if (!L_2)
			{
				goto IL_0019;
			}
		}

IL_0011:
		{
			int32_t L_3 = V_1;
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_0059;
			}
		}

IL_0015:
		{
			V_0 = (bool)0;
			goto IL_0086;
		}

IL_0019:
		{
			__this->set_U3CU3E1__state_0((-1));
			ImmutableList_t1819414204 * L_4 = V_2;
			NullCheck(L_4);
			RuntimeObject* L_5 = L_4->get_innerList_0();
			NullCheck(L_5);
			RuntimeObject* L_6 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_5);
			__this->set_U3CU3E7__wrap1_3(L_6);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_0061;
		}

IL_003b:
		{
			RuntimeObject* L_7 = __this->get_U3CU3E7__wrap1_3();
			NullCheck(L_7);
			RuntimeObject * L_8 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_7);
			V_3 = L_8;
			RuntimeObject * L_9 = V_3;
			__this->set_U3CU3E2__current_1(L_9);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_0086;
		}

IL_0059:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_0061:
		{
			RuntimeObject* L_10 = __this->get_U3CU3E7__wrap1_3();
			NullCheck(L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_003b;
			}
		}

IL_006e:
		{
			U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m615367899(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_3((RuntimeObject*)NULL);
			V_0 = (bool)0;
			goto IL_0086;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_007f;
	}

FAULT_007f:
	{ // begin fault (depth: 1)
		U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m1959394440(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(127)
	} // end fault
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0086:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
// System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>m__Finally1()
extern "C" IL2CPP_METHOD_ATTR void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m615367899 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m615367899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_3();
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IDisposable_t3640265483_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		return;
	}
}
// System.Object Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4161611585 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m1853955262 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m1853955262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m1853955262_RuntimeMethod_var);
	}
}
// System.Object Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m3270096007 (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasFieldInfo__ctor_m4197945869 (MemberAliasFieldInfo_t3617726363 * __this, FieldInfo_t * ___field0, String_t* ___namePrefix1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberAliasFieldInfo__ctor_m4197945869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FieldInfo__ctor_m3305575002(__this, /*hidden argument*/NULL);
		FieldInfo_t * L_0 = ___field0;
		__this->set_aliasedField_1(L_0);
		String_t* L_1 = ___namePrefix1;
		FieldInfo_t * L_2 = __this->get_aliasedField_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_2);
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, _stringLiteral3452614533, L_3, /*hidden argument*/NULL);
		__this->set_mangledName_2(L_4);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasFieldInfo__ctor_m3751720708 (MemberAliasFieldInfo_t3617726363 * __this, FieldInfo_t * ___field0, String_t* ___namePrefix1, String_t* ___separatorString2, const RuntimeMethod* method)
{
	{
		FieldInfo__ctor_m3305575002(__this, /*hidden argument*/NULL);
		FieldInfo_t * L_0 = ___field0;
		__this->set_aliasedField_1(L_0);
		String_t* L_1 = ___namePrefix1;
		String_t* L_2 = ___separatorString2;
		FieldInfo_t * L_3 = __this->get_aliasedField_1();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		String_t* L_5 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, L_2, L_4, /*hidden argument*/NULL);
		__this->set_mangledName_2(L_5);
		return;
	}
}
// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_AliasedField()
extern "C" IL2CPP_METHOD_ATTR FieldInfo_t * MemberAliasFieldInfo_get_AliasedField_m1830311124 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		return L_0;
	}
}
// System.Reflection.Module Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Module()
extern "C" IL2CPP_METHOD_ATTR Module_t2987026101 * MemberAliasFieldInfo_get_Module_m202218777 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		Module_t2987026101 * L_1 = VirtFuncInvoker0< Module_t2987026101 * >::Invoke(23 /* System.Reflection.Module System.Reflection.MemberInfo::get_Module() */, L_0);
		return L_1;
	}
}
// System.Int32 Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_MetadataToken()
extern "C" IL2CPP_METHOD_ATTR int32_t MemberAliasFieldInfo_get_MetadataToken_m3819103798 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 System.Reflection.MemberInfo::get_MetadataToken() */, L_0);
		return L_1;
	}
}
// System.String Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* MemberAliasFieldInfo_get_Name_m2364211508 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_mangledName_2();
		return L_0;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_DeclaringType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasFieldInfo_get_DeclaringType_m3289617910 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_0);
		return L_1;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_ReflectedType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasFieldInfo_get_ReflectedType_m2154818723 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.MemberInfo::get_ReflectedType() */, L_0);
		return L_1;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_FieldType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasFieldInfo_get_FieldType_m3870881020 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(26 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_0);
		return L_1;
	}
}
// System.RuntimeFieldHandle Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_FieldHandle()
extern "C" IL2CPP_METHOD_ATTR RuntimeFieldHandle_t1871169219  MemberAliasFieldInfo_get_FieldHandle_m3209471057 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		RuntimeFieldHandle_t1871169219  L_1 = VirtFuncInvoker0< RuntimeFieldHandle_t1871169219  >::Invoke(25 /* System.RuntimeFieldHandle System.Reflection.FieldInfo::get_FieldHandle() */, L_0);
		return L_1;
	}
}
// System.Reflection.FieldAttributes Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Attributes()
extern "C" IL2CPP_METHOD_ATTR int32_t MemberAliasFieldInfo_get_Attributes_m3202479239 (MemberAliasFieldInfo_t3617726363 * __this, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(24 /* System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes() */, L_0);
		return L_1;
	}
}
// System.Object[] Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* MemberAliasFieldInfo_GetCustomAttributes_m496835223 (MemberAliasFieldInfo_t3617726363 * __this, bool ___inherit0, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		bool L_1 = ___inherit0;
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker1< ObjectU5BU5D_t2843939325*, bool >::Invoke(18 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Object[] Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* MemberAliasFieldInfo_GetCustomAttributes_m2587390645 (MemberAliasFieldInfo_t3617726363 * __this, Type_t * ___attributeType0, bool ___inherit1, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		Type_t * L_1 = ___attributeType0;
		bool L_2 = ___inherit1;
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(19 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberAliasFieldInfo::IsDefined(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool MemberAliasFieldInfo_IsDefined_m4106964884 (MemberAliasFieldInfo_t3617726363 * __this, Type_t * ___attributeType0, bool ___inherit1, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		Type_t * L_1 = ___attributeType0;
		bool L_2 = ___inherit1;
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(20 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Object Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetValue(System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * MemberAliasFieldInfo_GetValue_m2932939605 (MemberAliasFieldInfo_t3617726363 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		RuntimeObject * L_1 = ___obj0;
		NullCheck(L_0);
		RuntimeObject * L_2 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(27 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasFieldInfo_SetValue_m3586516602 (MemberAliasFieldInfo_t3617726363 * __this, RuntimeObject * ___obj0, RuntimeObject * ___value1, int32_t ___invokeAttr2, Binder_t2999457153 * ___binder3, CultureInfo_t4157843068 * ___culture4, const RuntimeMethod* method)
{
	{
		FieldInfo_t * L_0 = __this->get_aliasedField_1();
		RuntimeObject * L_1 = ___obj0;
		RuntimeObject * L_2 = ___value1;
		int32_t L_3 = ___invokeAttr2;
		Binder_t2999457153 * L_4 = ___binder3;
		CultureInfo_t4157843068 * L_5 = ___culture4;
		NullCheck(L_0);
		VirtActionInvoker5< RuntimeObject *, RuntimeObject *, int32_t, Binder_t2999457153 *, CultureInfo_t4157843068 * >::Invoke(28 /* System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasMethodInfo__ctor_m1393362309 (MemberAliasMethodInfo_t2838764708 * __this, MethodInfo_t * ___method0, String_t* ___namePrefix1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberAliasMethodInfo__ctor_m1393362309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodInfo__ctor_m2805780217(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_0 = ___method0;
		__this->set_aliasedMethod_1(L_0);
		String_t* L_1 = ___namePrefix1;
		MethodInfo_t * L_2 = __this->get_aliasedMethod_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_2);
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, _stringLiteral3452614533, L_3, /*hidden argument*/NULL);
		__this->set_mangledName_2(L_4);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasMethodInfo__ctor_m4249888889 (MemberAliasMethodInfo_t2838764708 * __this, MethodInfo_t * ___method0, String_t* ___namePrefix1, String_t* ___separatorString2, const RuntimeMethod* method)
{
	{
		MethodInfo__ctor_m2805780217(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_0 = ___method0;
		__this->set_aliasedMethod_1(L_0);
		String_t* L_1 = ___namePrefix1;
		String_t* L_2 = ___separatorString2;
		MethodInfo_t * L_3 = __this->get_aliasedMethod_1();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		String_t* L_5 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, L_2, L_4, /*hidden argument*/NULL);
		__this->set_mangledName_2(L_5);
		return;
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_AliasedMethod()
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * MemberAliasMethodInfo_get_AliasedMethod_m3436203498 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		return L_0;
	}
}
// System.Reflection.ICustomAttributeProvider Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReturnTypeCustomAttributes()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m914327719 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		RuntimeObject* L_1 = VirtFuncInvoker0< RuntimeObject* >::Invoke(67 /* System.Reflection.ICustomAttributeProvider System.Reflection.MethodInfo::get_ReturnTypeCustomAttributes() */, L_0);
		return L_1;
	}
}
// System.RuntimeMethodHandle Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_MethodHandle()
extern "C" IL2CPP_METHOD_ATTR RuntimeMethodHandle_t1133924984  MemberAliasMethodInfo_get_MethodHandle_m3514592321 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		RuntimeMethodHandle_t1133924984  L_1 = VirtFuncInvoker0< RuntimeMethodHandle_t1133924984  >::Invoke(46 /* System.RuntimeMethodHandle System.Reflection.MethodBase::get_MethodHandle() */, L_0);
		return L_1;
	}
}
// System.Reflection.MethodAttributes Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_Attributes()
extern "C" IL2CPP_METHOD_ATTR int32_t MemberAliasMethodInfo_get_Attributes_m1040077088 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(47 /* System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes() */, L_0);
		return L_1;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReturnType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasMethodInfo_get_ReturnType_m3040784307 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(65 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_0);
		return L_1;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_DeclaringType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasMethodInfo_get_DeclaringType_m2643950143 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_0);
		return L_1;
	}
}
// System.String Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* MemberAliasMethodInfo_get_Name_m1870101655 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_mangledName_2();
		return L_0;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReflectedType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasMethodInfo_get_ReflectedType_m1217426163 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.MemberInfo::get_ReflectedType() */, L_0);
		return L_1;
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetBaseDefinition()
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * MemberAliasMethodInfo_GetBaseDefinition_m108972962 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		MethodInfo_t * L_1 = VirtFuncInvoker0< MethodInfo_t * >::Invoke(68 /* System.Reflection.MethodInfo System.Reflection.MethodInfo::GetBaseDefinition() */, L_0);
		return L_1;
	}
}
// System.Object[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* MemberAliasMethodInfo_GetCustomAttributes_m2551917522 (MemberAliasMethodInfo_t2838764708 * __this, bool ___inherit0, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		bool L_1 = ___inherit0;
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker1< ObjectU5BU5D_t2843939325*, bool >::Invoke(18 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Object[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* MemberAliasMethodInfo_GetCustomAttributes_m1100566957 (MemberAliasMethodInfo_t2838764708 * __this, Type_t * ___attributeType0, bool ___inherit1, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		Type_t * L_1 = ___attributeType0;
		bool L_2 = ___inherit1;
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(19 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Reflection.MethodImplAttributes Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetMethodImplementationFlags()
extern "C" IL2CPP_METHOD_ATTR int32_t MemberAliasMethodInfo_GetMethodImplementationFlags_m1000202339 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(45 /* System.Reflection.MethodImplAttributes System.Reflection.MethodBase::GetMethodImplementationFlags() */, L_0);
		return L_1;
	}
}
// System.Reflection.ParameterInfo[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetParameters()
extern "C" IL2CPP_METHOD_ATTR ParameterInfoU5BU5D_t390618515* MemberAliasMethodInfo_GetParameters_m4084330430 (MemberAliasMethodInfo_t2838764708 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		NullCheck(L_0);
		ParameterInfoU5BU5D_t390618515* L_1 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(43 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_0);
		return L_1;
	}
}
// System.Object Sirenix.Serialization.Utilities.MemberAliasMethodInfo::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * MemberAliasMethodInfo_Invoke_m3981498025 (MemberAliasMethodInfo_t2838764708 * __this, RuntimeObject * ___obj0, int32_t ___invokeAttr1, Binder_t2999457153 * ___binder2, ObjectU5BU5D_t2843939325* ___parameters3, CultureInfo_t4157843068 * ___culture4, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		RuntimeObject * L_1 = ___obj0;
		int32_t L_2 = ___invokeAttr1;
		Binder_t2999457153 * L_3 = ___binder2;
		ObjectU5BU5D_t2843939325* L_4 = ___parameters3;
		CultureInfo_t4157843068 * L_5 = ___culture4;
		NullCheck(L_0);
		RuntimeObject * L_6 = VirtFuncInvoker5< RuntimeObject *, RuntimeObject *, int32_t, Binder_t2999457153 *, ObjectU5BU5D_t2843939325*, CultureInfo_t4157843068 * >::Invoke(48 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return L_6;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberAliasMethodInfo::IsDefined(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool MemberAliasMethodInfo_IsDefined_m4290041892 (MemberAliasMethodInfo_t2838764708 * __this, Type_t * ___attributeType0, bool ___inherit1, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_aliasedMethod_1();
		Type_t * L_1 = ___attributeType0;
		bool L_2 = ___inherit1;
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(20 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasPropertyInfo__ctor_m4188271427 (MemberAliasPropertyInfo_t4204559890 * __this, PropertyInfo_t * ___prop0, String_t* ___namePrefix1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberAliasPropertyInfo__ctor_m4188271427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyInfo__ctor_m4235916625(__this, /*hidden argument*/NULL);
		PropertyInfo_t * L_0 = ___prop0;
		__this->set_aliasedProperty_1(L_0);
		String_t* L_1 = ___namePrefix1;
		PropertyInfo_t * L_2 = __this->get_aliasedProperty_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_2);
		String_t* L_4 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, _stringLiteral3452614533, L_3, /*hidden argument*/NULL);
		__this->set_mangledName_2(L_4);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasPropertyInfo__ctor_m1327601375 (MemberAliasPropertyInfo_t4204559890 * __this, PropertyInfo_t * ___prop0, String_t* ___namePrefix1, String_t* ___separatorString2, const RuntimeMethod* method)
{
	{
		PropertyInfo__ctor_m4235916625(__this, /*hidden argument*/NULL);
		PropertyInfo_t * L_0 = ___prop0;
		__this->set_aliasedProperty_1(L_0);
		String_t* L_1 = ___namePrefix1;
		String_t* L_2 = ___separatorString2;
		PropertyInfo_t * L_3 = __this->get_aliasedProperty_1();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		String_t* L_5 = String_Concat_m3755062657(NULL /*static, unused*/, L_1, L_2, L_4, /*hidden argument*/NULL);
		__this->set_mangledName_2(L_5);
		return;
	}
}
// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_AliasedProperty()
extern "C" IL2CPP_METHOD_ATTR PropertyInfo_t * MemberAliasPropertyInfo_get_AliasedProperty_m1519871680 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		return L_0;
	}
}
// System.Reflection.Module Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Module()
extern "C" IL2CPP_METHOD_ATTR Module_t2987026101 * MemberAliasPropertyInfo_get_Module_m1716676278 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		Module_t2987026101 * L_1 = VirtFuncInvoker0< Module_t2987026101 * >::Invoke(23 /* System.Reflection.Module System.Reflection.MemberInfo::get_Module() */, L_0);
		return L_1;
	}
}
// System.Int32 Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_MetadataToken()
extern "C" IL2CPP_METHOD_ATTR int32_t MemberAliasPropertyInfo_get_MetadataToken_m2050289920 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 System.Reflection.MemberInfo::get_MetadataToken() */, L_0);
		return L_1;
	}
}
// System.String Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* MemberAliasPropertyInfo_get_Name_m1033913883 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_mangledName_2();
		return L_0;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_DeclaringType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasPropertyInfo_get_DeclaringType_m90885445 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_0);
		return L_1;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_ReflectedType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasPropertyInfo_get_ReflectedType_m3310100154 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.MemberInfo::get_ReflectedType() */, L_0);
		return L_1;
	}
}
// System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_PropertyType()
extern "C" IL2CPP_METHOD_ATTR Type_t * MemberAliasPropertyInfo_get_PropertyType_m3798783847 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(29 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_0);
		return L_1;
	}
}
// System.Reflection.PropertyAttributes Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Attributes()
extern "C" IL2CPP_METHOD_ATTR int32_t MemberAliasPropertyInfo_get_Attributes_m1932288081 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(24 /* System.Reflection.PropertyAttributes System.Reflection.PropertyInfo::get_Attributes() */, L_0);
		return L_1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_CanRead()
extern "C" IL2CPP_METHOD_ATTR bool MemberAliasPropertyInfo_get_CanRead_m949136016 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_0);
		return L_1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_CanWrite()
extern "C" IL2CPP_METHOD_ATTR bool MemberAliasPropertyInfo_get_CanWrite_m642629200 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_0);
		return L_1;
	}
}
// System.Object[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* MemberAliasPropertyInfo_GetCustomAttributes_m3461224138 (MemberAliasPropertyInfo_t4204559890 * __this, bool ___inherit0, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		bool L_1 = ___inherit0;
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker1< ObjectU5BU5D_t2843939325*, bool >::Invoke(18 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Object[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* MemberAliasPropertyInfo_GetCustomAttributes_m4098940302 (MemberAliasPropertyInfo_t4204559890 * __this, Type_t * ___attributeType0, bool ___inherit1, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		Type_t * L_1 = ___attributeType0;
		bool L_2 = ___inherit1;
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_3 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(19 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::IsDefined(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool MemberAliasPropertyInfo_IsDefined_m3781827779 (MemberAliasPropertyInfo_t4204559890 * __this, Type_t * ___attributeType0, bool ___inherit1, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		Type_t * L_1 = ___attributeType0;
		bool L_2 = ___inherit1;
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(20 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Reflection.MethodInfo[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetAccessors(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MethodInfoU5BU5D_t2572182361* MemberAliasPropertyInfo_GetAccessors_m3185754522 (MemberAliasPropertyInfo_t4204559890 * __this, bool ___nonPublic0, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		bool L_1 = ___nonPublic0;
		NullCheck(L_0);
		MethodInfoU5BU5D_t2572182361* L_2 = VirtFuncInvoker1< MethodInfoU5BU5D_t2572182361*, bool >::Invoke(30 /* System.Reflection.MethodInfo[] System.Reflection.PropertyInfo::GetAccessors(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetGetMethod(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * MemberAliasPropertyInfo_GetGetMethod_m4138888537 (MemberAliasPropertyInfo_t4204559890 * __this, bool ___nonPublic0, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		bool L_1 = ___nonPublic0;
		NullCheck(L_0);
		MethodInfo_t * L_2 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(31 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Reflection.ParameterInfo[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetIndexParameters()
extern "C" IL2CPP_METHOD_ATTR ParameterInfoU5BU5D_t390618515* MemberAliasPropertyInfo_GetIndexParameters_m2754343555 (MemberAliasPropertyInfo_t4204559890 * __this, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		NullCheck(L_0);
		ParameterInfoU5BU5D_t390618515* L_1 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(32 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_0);
		return L_1;
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetSetMethod(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * MemberAliasPropertyInfo_GetSetMethod_m223125633 (MemberAliasPropertyInfo_t4204559890 * __this, bool ___nonPublic0, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		bool L_1 = ___nonPublic0;
		NullCheck(L_0);
		MethodInfo_t * L_2 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(33 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_0, L_1);
		return L_2;
	}
}
// System.Object Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * MemberAliasPropertyInfo_GetValue_m2577182105 (MemberAliasPropertyInfo_t4204559890 * __this, RuntimeObject * ___obj0, int32_t ___invokeAttr1, Binder_t2999457153 * ___binder2, ObjectU5BU5D_t2843939325* ___index3, CultureInfo_t4157843068 * ___culture4, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		RuntimeObject * L_1 = ___obj0;
		int32_t L_2 = ___invokeAttr1;
		Binder_t2999457153 * L_3 = ___binder2;
		ObjectU5BU5D_t2843939325* L_4 = ___index3;
		CultureInfo_t4157843068 * L_5 = ___culture4;
		NullCheck(L_0);
		RuntimeObject * L_6 = VirtFuncInvoker5< RuntimeObject *, RuntimeObject *, int32_t, Binder_t2999457153 *, ObjectU5BU5D_t2843939325*, CultureInfo_t4157843068 * >::Invoke(35 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return L_6;
	}
}
// System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" IL2CPP_METHOD_ATTR void MemberAliasPropertyInfo_SetValue_m3259528209 (MemberAliasPropertyInfo_t4204559890 * __this, RuntimeObject * ___obj0, RuntimeObject * ___value1, int32_t ___invokeAttr2, Binder_t2999457153 * ___binder3, ObjectU5BU5D_t2843939325* ___index4, CultureInfo_t4157843068 * ___culture5, const RuntimeMethod* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_aliasedProperty_1();
		RuntimeObject * L_1 = ___obj0;
		RuntimeObject * L_2 = ___value1;
		int32_t L_3 = ___invokeAttr2;
		Binder_t2999457153 * L_4 = ___binder3;
		ObjectU5BU5D_t2843939325* L_5 = ___index4;
		CultureInfo_t4157843068 * L_6 = ___culture5;
		NullCheck(L_0);
		VirtActionInvoker6< RuntimeObject *, RuntimeObject *, int32_t, Binder_t2999457153 *, ObjectU5BU5D_t2843939325*, CultureInfo_t4157843068 * >::Invoke(37 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo) */, L_0, L_1, L_2, L_3, L_4, L_5, L_6);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Attribute[] Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR AttributeU5BU5D_t1575011174* MemberInfoExtensions_GetAttributes_m1287245605 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoExtensions_GetAttributes_m1287245605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MemberInfo_t * L_0 = ___member0;
		RuntimeObject* L_1 = MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m2331120560(NULL /*static, unused*/, L_0, /*hidden argument*/MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m2331120560_RuntimeMethod_var);
		AttributeU5BU5D_t1575011174* L_2 = Enumerable_ToArray_TisAttribute_t861562559_m3360527948(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToArray_TisAttribute_t861562559_m3360527948_RuntimeMethod_var);
		return L_2;
	}
}
// System.Attribute[] Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.MemberInfo,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR AttributeU5BU5D_t1575011174* MemberInfoExtensions_GetAttributes_m3447597642 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, bool ___inherit1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoExtensions_GetAttributes_m3447597642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MemberInfo_t * L_0 = ___member0;
		bool L_1 = ___inherit1;
		RuntimeObject* L_2 = MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m4106349652(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/MemberInfoExtensions_GetAttributes_TisAttribute_t861562559_m4106349652_RuntimeMethod_var);
		AttributeU5BU5D_t1575011174* L_3 = Enumerable_ToArray_TisAttribute_t861562559_m3360527948(NULL /*static, unused*/, L_2, /*hidden argument*/Enumerable_ToArray_TisAttribute_t861562559_m3360527948_RuntimeMethod_var);
		return L_3;
	}
}
// System.String Sirenix.Serialization.Utilities.MemberInfoExtensions::GetNiceName(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR String_t* MemberInfoExtensions_GetNiceName_m2681496769 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoExtensions_GetNiceName_m2681496769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodBase_t * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		MemberInfo_t * L_0 = ___member0;
		V_0 = ((MethodBase_t *)IsInstClass((RuntimeObject*)L_0, MethodBase_t_il2cpp_TypeInfo_var));
		MethodBase_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		MethodBase_t * L_2 = V_0;
		String_t* L_3 = MethodInfoExtensions_GetFullName_m3000052143(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_001a;
	}

IL_0013:
	{
		MemberInfo_t * L_4 = ___member0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_4);
		V_1 = L_5;
	}

IL_001a:
	{
		String_t* L_6 = V_1;
		String_t* L_7 = StringExtensions_ToTitleCase_m1099386125(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsStatic(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR bool MemberInfoExtensions_IsStatic_m2697550895 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoExtensions_IsStatic_m2697550895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FieldInfo_t * V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	MethodBase_t * V_2 = NULL;
	EventInfo_t * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	{
		MemberInfo_t * L_0 = ___member0;
		V_0 = ((FieldInfo_t *)IsInstClass((RuntimeObject*)L_0, FieldInfo_t_il2cpp_TypeInfo_var));
		FieldInfo_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		FieldInfo_t * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = FieldInfo_get_IsStatic_m3482711189(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0011:
	{
		MemberInfo_t * L_4 = ___member0;
		V_1 = ((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_4, PropertyInfo_t_il2cpp_TypeInfo_var));
		PropertyInfo_t * L_5 = V_1;
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		PropertyInfo_t * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_6);
		if (L_7)
		{
			goto IL_0030;
		}
	}
	{
		PropertyInfo_t * L_8 = V_1;
		NullCheck(L_8);
		MethodInfo_t * L_9 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(33 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_8, (bool)1);
		NullCheck(L_9);
		bool L_10 = MethodBase_get_IsStatic_m2399864271(L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0030:
	{
		PropertyInfo_t * L_11 = V_1;
		NullCheck(L_11);
		MethodInfo_t * L_12 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(31 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_11, (bool)1);
		NullCheck(L_12);
		bool L_13 = MethodBase_get_IsStatic_m2399864271(L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_003d:
	{
		MemberInfo_t * L_14 = ___member0;
		V_2 = ((MethodBase_t *)IsInstClass((RuntimeObject*)L_14, MethodBase_t_il2cpp_TypeInfo_var));
		MethodBase_t * L_15 = V_2;
		if (!L_15)
		{
			goto IL_004e;
		}
	}
	{
		MethodBase_t * L_16 = V_2;
		NullCheck(L_16);
		bool L_17 = MethodBase_get_IsStatic_m2399864271(L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_004e:
	{
		MemberInfo_t * L_18 = ___member0;
		V_3 = ((EventInfo_t *)IsInstClass((RuntimeObject*)L_18, EventInfo_t_il2cpp_TypeInfo_var));
		EventInfo_t * L_19 = V_3;
		if (!L_19)
		{
			goto IL_0065;
		}
	}
	{
		EventInfo_t * L_20 = V_3;
		NullCheck(L_20);
		MethodInfo_t * L_21 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(29 /* System.Reflection.MethodInfo System.Reflection.EventInfo::GetRaiseMethod(System.Boolean) */, L_20, (bool)1);
		NullCheck(L_21);
		bool L_22 = MethodBase_get_IsStatic_m2399864271(L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_0065:
	{
		MemberInfo_t * L_23 = ___member0;
		V_4 = ((Type_t *)IsInstClass((RuntimeObject*)L_23, Type_t_il2cpp_TypeInfo_var));
		Type_t * L_24 = V_4;
		if (!L_24)
		{
			goto IL_0084;
		}
	}
	{
		Type_t * L_25 = V_4;
		NullCheck(L_25);
		bool L_26 = Type_get_IsSealed_m3543837727(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0082;
		}
	}
	{
		Type_t * L_27 = V_4;
		NullCheck(L_27);
		bool L_28 = Type_get_IsAbstract_m1120089130(L_27, /*hidden argument*/NULL);
		return L_28;
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_29 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_30 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3);
		ObjectU5BU5D_t2843939325* L_31 = L_30;
		MemberInfo_t * L_32 = ___member0;
		NullCheck(L_32);
		Type_t * L_33 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_32);
		NullCheck(L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(54 /* System.String System.Type::get_FullName() */, L_33);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_34);
		ObjectU5BU5D_t2843939325* L_35 = L_31;
		MemberInfo_t * L_36 = ___member0;
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_36);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_37);
		ObjectU5BU5D_t2843939325* L_38 = L_35;
		MemberInfo_t * L_39 = ___member0;
		NullCheck(L_39);
		Type_t * L_40 = Object_GetType_m88164663(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(54 /* System.String System.Type::get_FullName() */, L_40);
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_41);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_41);
		String_t* L_42 = String_Format_m1881875187(NULL /*static, unused*/, L_29, _stringLiteral1715810616, L_38, /*hidden argument*/NULL);
		V_5 = L_42;
		String_t* L_43 = V_5;
		NotSupportedException_t1314879016 * L_44 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_44, L_43, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44, NULL, MemberInfoExtensions_IsStatic_m2697550895_RuntimeMethod_var);
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsAlias(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR bool MemberInfoExtensions_IsAlias_m2394183543 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoExtensions_IsAlias_m2394183543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		if (((MemberAliasFieldInfo_t3617726363 *)IsInstSealed((RuntimeObject*)L_0, MemberAliasFieldInfo_t3617726363_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		MemberInfo_t * L_1 = ___memberInfo0;
		if (((MemberAliasPropertyInfo_t4204559890 *)IsInstSealed((RuntimeObject*)L_1, MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		MemberInfo_t * L_2 = ___memberInfo0;
		return (bool)((!(((RuntimeObject*)(MemberAliasMethodInfo_t2838764708 *)((MemberAliasMethodInfo_t2838764708 *)IsInstSealed((RuntimeObject*)L_2, MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}

IL_001a:
	{
		return (bool)1;
	}
}
// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.MemberInfoExtensions::DeAlias(System.Reflection.MemberInfo,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MemberInfo_t * MemberInfoExtensions_DeAlias_m1298842707 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, bool ___throwOnNotAliased1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MemberInfoExtensions_DeAlias_m1298842707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemberAliasFieldInfo_t3617726363 * V_0 = NULL;
	MemberAliasPropertyInfo_t4204559890 * V_1 = NULL;
	MemberAliasMethodInfo_t2838764708 * V_2 = NULL;
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		V_0 = ((MemberAliasFieldInfo_t3617726363 *)IsInstSealed((RuntimeObject*)L_0, MemberAliasFieldInfo_t3617726363_il2cpp_TypeInfo_var));
		MemberAliasFieldInfo_t3617726363 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		MemberAliasFieldInfo_t3617726363 * L_2 = V_0;
		NullCheck(L_2);
		FieldInfo_t * L_3 = MemberAliasFieldInfo_get_AliasedField_m1830311124(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0011:
	{
		MemberInfo_t * L_4 = ___memberInfo0;
		V_1 = ((MemberAliasPropertyInfo_t4204559890 *)IsInstSealed((RuntimeObject*)L_4, MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var));
		MemberAliasPropertyInfo_t4204559890 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		MemberAliasPropertyInfo_t4204559890 * L_6 = V_1;
		NullCheck(L_6);
		PropertyInfo_t * L_7 = MemberAliasPropertyInfo_get_AliasedProperty_m1519871680(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0022:
	{
		MemberInfo_t * L_8 = ___memberInfo0;
		V_2 = ((MemberAliasMethodInfo_t2838764708 *)IsInstSealed((RuntimeObject*)L_8, MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var));
		MemberAliasMethodInfo_t2838764708 * L_9 = V_2;
		if (!L_9)
		{
			goto IL_0033;
		}
	}
	{
		MemberAliasMethodInfo_t2838764708 * L_10 = V_2;
		NullCheck(L_10);
		MethodInfo_t * L_11 = MemberAliasMethodInfo_get_AliasedMethod_m3436203498(L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0033:
	{
		bool L_12 = ___throwOnNotAliased1;
		if (!L_12)
		{
			goto IL_0051;
		}
	}
	{
		MemberInfo_t * L_13 = ___memberInfo0;
		String_t* L_14 = MemberInfoExtensions_GetNiceName_m2681496769(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2456775193, L_14, _stringLiteral3758551614, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_16, L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, NULL, MemberInfoExtensions_DeAlias_m1298842707_RuntimeMethod_var);
	}

IL_0051:
	{
		MemberInfo_t * L_17 = ___memberInfo0;
		return L_17;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* MethodInfoExtensions_GetFullName_m2745430701 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, String_t* ___extensionMethodPrefix1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfoExtensions_GetFullName_m2745430701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	bool V_1 = false;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		MethodBase_t * L_1 = ___method0;
		bool L_2 = MethodInfoExtensions_IsExtensionMethod_m4010269726(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		StringBuilder_t * L_4 = V_0;
		String_t* L_5 = ___extensionMethodPrefix1;
		NullCheck(L_4);
		StringBuilder_Append_m1965104174(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0018:
	{
		StringBuilder_t * L_6 = V_0;
		MethodBase_t * L_7 = ___method0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_7);
		NullCheck(L_6);
		StringBuilder_Append_m1965104174(L_6, L_8, /*hidden argument*/NULL);
		StringBuilder_t * L_9 = V_0;
		NullCheck(L_9);
		StringBuilder_Append_m1965104174(L_9, _stringLiteral3452614536, /*hidden argument*/NULL);
		StringBuilder_t * L_10 = V_0;
		MethodBase_t * L_11 = ___method0;
		String_t* L_12 = MethodInfoExtensions_GetParamsNames_m873242453(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		StringBuilder_Append_m1965104174(L_10, L_12, /*hidden argument*/NULL);
		StringBuilder_t * L_13 = V_0;
		NullCheck(L_13);
		StringBuilder_Append_m1965104174(L_13, _stringLiteral3452614535, /*hidden argument*/NULL);
		StringBuilder_t * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		return L_15;
	}
}
// System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetParamsNames(System.Reflection.MethodBase)
extern "C" IL2CPP_METHOD_ATTR String_t* MethodInfoExtensions_GetParamsNames_m873242453 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfoExtensions_GetParamsNames_m873242453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParameterInfoU5BU5D_t390618515* V_0 = NULL;
	StringBuilder_t * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	ParameterInfo_t1861056598 * V_4 = NULL;
	String_t* V_5 = NULL;
	ParameterInfoU5BU5D_t390618515* G_B3_0 = NULL;
	{
		MethodBase_t * L_0 = ___method0;
		bool L_1 = MethodInfoExtensions_IsExtensionMethod_m4010269726(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		MethodBase_t * L_2 = ___method0;
		NullCheck(L_2);
		ParameterInfoU5BU5D_t390618515* L_3 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(43 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_2);
		G_B3_0 = L_3;
		goto IL_0021;
	}

IL_0010:
	{
		MethodBase_t * L_4 = ___method0;
		NullCheck(L_4);
		ParameterInfoU5BU5D_t390618515* L_5 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(43 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_4);
		RuntimeObject* L_6 = Enumerable_Skip_TisParameterInfo_t1861056598_m3780503321(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_5, 1, /*hidden argument*/Enumerable_Skip_TisParameterInfo_t1861056598_m3780503321_RuntimeMethod_var);
		ParameterInfoU5BU5D_t390618515* L_7 = Enumerable_ToArray_TisParameterInfo_t1861056598_m2516263013(NULL /*static, unused*/, L_6, /*hidden argument*/Enumerable_ToArray_TisParameterInfo_t1861056598_m2516263013_RuntimeMethod_var);
		G_B3_0 = L_7;
	}

IL_0021:
	{
		V_0 = G_B3_0;
		StringBuilder_t * L_8 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_8, /*hidden argument*/NULL);
		V_1 = L_8;
		V_2 = 0;
		ParameterInfoU5BU5D_t390618515* L_9 = V_0;
		NullCheck(L_9);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))));
		goto IL_007c;
	}

IL_0030:
	{
		ParameterInfoU5BU5D_t390618515* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ParameterInfo_t1861056598 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_4 = L_13;
		ParameterInfo_t1861056598 * L_14 = V_4;
		NullCheck(L_14);
		Type_t * L_15 = VirtFuncInvoker0< Type_t * >::Invoke(8 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_16 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		StringBuilder_t * L_17 = V_1;
		String_t* L_18 = V_5;
		NullCheck(L_17);
		StringBuilder_Append_m1965104174(L_17, L_18, /*hidden argument*/NULL);
		StringBuilder_t * L_19 = V_1;
		NullCheck(L_19);
		StringBuilder_Append_m1965104174(L_19, _stringLiteral3452614528, /*hidden argument*/NULL);
		StringBuilder_t * L_20 = V_1;
		ParameterInfo_t1861056598 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Reflection.ParameterInfo::get_Name() */, L_21);
		NullCheck(L_20);
		StringBuilder_Append_m1965104174(L_20, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_2;
		int32_t L_24 = V_3;
		if ((((int32_t)L_23) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)1)))))
		{
			goto IL_0078;
		}
	}
	{
		StringBuilder_t * L_25 = V_1;
		NullCheck(L_25);
		StringBuilder_Append_m1965104174(L_25, _stringLiteral3450517380, /*hidden argument*/NULL);
	}

IL_0078:
	{
		int32_t L_26 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_007c:
	{
		int32_t L_27 = V_2;
		int32_t L_28 = V_3;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0030;
		}
	}
	{
		StringBuilder_t * L_29 = V_1;
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		return L_30;
	}
}
// System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase)
extern "C" IL2CPP_METHOD_ATTR String_t* MethodInfoExtensions_GetFullName_m3000052143 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfoExtensions_GetFullName_m3000052143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0 = ___method0;
		String_t* L_1 = MethodInfoExtensions_GetFullName_m2745430701(NULL /*static, unused*/, L_0, _stringLiteral2787658538, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsExtensionMethod(System.Reflection.MethodBase)
extern "C" IL2CPP_METHOD_ATTR bool MethodInfoExtensions_IsExtensionMethod_m4010269726 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfoExtensions_IsExtensionMethod_m4010269726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		MethodBase_t * L_0 = ___method0;
		NullCheck(L_0);
		Type_t * L_1 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_0);
		V_0 = L_1;
		Type_t * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = Type_get_IsSealed_m3543837727(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		Type_t * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_4);
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		Type_t * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Type_get_IsNested_m3546087448(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0031;
		}
	}
	{
		MethodBase_t * L_8 = ___method0;
		RuntimeTypeHandle_t3027515415  L_9 = { reinterpret_cast<intptr_t> (ExtensionAttribute_t1723066603_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_11 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(20 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_8, L_10, (bool)0);
		return L_11;
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsAliasMethod(System.Reflection.MethodInfo)
extern "C" IL2CPP_METHOD_ATTR bool MethodInfoExtensions_IsAliasMethod_m3586406049 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___methodInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfoExtensions_IsAliasMethod_m3586406049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodInfo_t * L_0 = ___methodInfo0;
		return (bool)((!(((RuntimeObject*)(MemberAliasMethodInfo_t2838764708 *)((MemberAliasMethodInfo_t2838764708 *)IsInstSealed((RuntimeObject*)L_0, MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MethodInfoExtensions::DeAliasMethod(System.Reflection.MethodInfo,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * MethodInfoExtensions_DeAliasMethod_m1948847315 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___methodInfo0, bool ___throwOnNotAliased1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MethodInfoExtensions_DeAliasMethod_m1948847315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemberAliasMethodInfo_t2838764708 * V_0 = NULL;
	{
		MethodInfo_t * L_0 = ___methodInfo0;
		V_0 = ((MemberAliasMethodInfo_t2838764708 *)IsInstSealed((RuntimeObject*)L_0, MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var));
		MemberAliasMethodInfo_t2838764708 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0018;
	}

IL_000c:
	{
		MemberAliasMethodInfo_t2838764708 * L_2 = V_0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = MemberAliasMethodInfo_get_AliasedMethod_m3436203498(L_2, /*hidden argument*/NULL);
		V_0 = ((MemberAliasMethodInfo_t2838764708 *)IsInstSealed((RuntimeObject*)L_3, MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		MemberAliasMethodInfo_t2838764708 * L_4 = V_0;
		NullCheck(L_4);
		MethodInfo_t * L_5 = MemberAliasMethodInfo_get_AliasedMethod_m3436203498(L_4, /*hidden argument*/NULL);
		if (((MemberAliasMethodInfo_t2838764708 *)IsInstSealed((RuntimeObject*)L_5, MemberAliasMethodInfo_t2838764708_il2cpp_TypeInfo_var)))
		{
			goto IL_000c;
		}
	}
	{
		MemberAliasMethodInfo_t2838764708 * L_6 = V_0;
		NullCheck(L_6);
		MethodInfo_t * L_7 = MemberAliasMethodInfo_get_AliasedMethod_m3436203498(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_002c:
	{
		bool L_8 = ___throwOnNotAliased1;
		if (!L_8)
		{
			goto IL_004a;
		}
	}
	{
		MethodInfo_t * L_9 = ___methodInfo0;
		String_t* L_10 = MemberInfoExtensions_GetNiceName_m2681496769(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_11 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1990832630, L_10, _stringLiteral3758551614, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_12 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_12, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, MethodInfoExtensions_DeAliasMethod_m1948847315_RuntimeMethod_var);
	}

IL_004a:
	{
		MethodInfo_t * L_13 = ___methodInfo0;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.Serialization.Utilities.PathUtilities::HasSubDirectory(System.IO.DirectoryInfo,System.IO.DirectoryInfo)
extern "C" IL2CPP_METHOD_ATTR bool PathUtilities_HasSubDirectory_m804950350 (RuntimeObject * __this /* static, unused */, DirectoryInfo_t35957480 * ___parentDir0, DirectoryInfo_t35957480 * ___subDir1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathUtilities_HasSubDirectory_m804950350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		DirectoryInfo_t35957480 * L_0 = ___parentDir0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_0);
		CharU5BU5D_t3528271667* L_2 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t3528271667* L_3 = L_2;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t3528271667* L_4 = L_3;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)47));
		NullCheck(L_1);
		String_t* L_5 = String_TrimEnd_m3824727301(L_1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_004b;
	}

IL_001e:
	{
		DirectoryInfo_t35957480 * L_6 = ___subDir1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_6);
		CharU5BU5D_t3528271667* L_8 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)2);
		CharU5BU5D_t3528271667* L_9 = L_8;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t3528271667* L_10 = L_9;
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)47));
		NullCheck(L_7);
		String_t* L_11 = String_TrimEnd_m3824727301(L_7, L_10, /*hidden argument*/NULL);
		String_t* L_12 = V_0;
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0043;
		}
	}
	{
		return (bool)1;
	}

IL_0043:
	{
		DirectoryInfo_t35957480 * L_14 = ___subDir1;
		NullCheck(L_14);
		DirectoryInfo_t35957480 * L_15 = DirectoryInfo_get_Parent_m3736638393(L_14, /*hidden argument*/NULL);
		___subDir1 = L_15;
	}

IL_004b:
	{
		DirectoryInfo_t35957480 * L_16 = ___subDir1;
		if (L_16)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.Serialization.Utilities.PropertyInfoExtensions::IsAutoProperty(System.Reflection.PropertyInfo,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool PropertyInfoExtensions_IsAutoProperty_m699352069 (RuntimeObject * __this /* static, unused */, PropertyInfo_t * ___propInfo0, bool ___allowVirtual1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyInfoExtensions_IsAutoProperty_m699352069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	FieldInfoU5BU5D_t846150980* V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	MethodInfo_t * V_4 = NULL;
	int32_t V_5 = 0;
	{
		PropertyInfo_t * L_0 = ___propInfo0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_0);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		PropertyInfo_t * L_2 = ___propInfo0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_2);
		if (L_3)
		{
			goto IL_0012;
		}
	}

IL_0010:
	{
		return (bool)0;
	}

IL_0012:
	{
		bool L_4 = ___allowVirtual1;
		if (L_4)
		{
			goto IL_0051;
		}
	}
	{
		PropertyInfo_t * L_5 = ___propInfo0;
		NullCheck(L_5);
		MethodInfo_t * L_6 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(31 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_5, (bool)1);
		V_3 = L_6;
		PropertyInfo_t * L_7 = ___propInfo0;
		NullCheck(L_7);
		MethodInfo_t * L_8 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(33 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_7, (bool)1);
		V_4 = L_8;
		MethodInfo_t * L_9 = V_3;
		if (!L_9)
		{
			goto IL_0039;
		}
	}
	{
		MethodInfo_t * L_10 = V_3;
		NullCheck(L_10);
		bool L_11 = MethodBase_get_IsAbstract_m428833029(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_004f;
		}
	}
	{
		MethodInfo_t * L_12 = V_3;
		NullCheck(L_12);
		bool L_13 = MethodBase_get_IsVirtual_m2008546636(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_004f;
		}
	}

IL_0039:
	{
		MethodInfo_t * L_14 = V_4;
		if (!L_14)
		{
			goto IL_0051;
		}
	}
	{
		MethodInfo_t * L_15 = V_4;
		NullCheck(L_15);
		bool L_16 = MethodBase_get_IsAbstract_m428833029(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_004f;
		}
	}
	{
		MethodInfo_t * L_17 = V_4;
		NullCheck(L_17);
		bool L_18 = MethodBase_get_IsVirtual_m2008546636(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0051;
		}
	}

IL_004f:
	{
		return (bool)0;
	}

IL_0051:
	{
		V_0 = ((int32_t)44);
		PropertyInfo_t * L_19 = ___propInfo0;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		String_t* L_21 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3452614548, L_20, _stringLiteral3452614546, /*hidden argument*/NULL);
		V_1 = L_21;
		PropertyInfo_t * L_22 = ___propInfo0;
		NullCheck(L_22);
		Type_t * L_23 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_22);
		int32_t L_24 = V_0;
		NullCheck(L_23);
		FieldInfoU5BU5D_t846150980* L_25 = VirtFuncInvoker1< FieldInfoU5BU5D_t846150980*, int32_t >::Invoke(76 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_23, L_24);
		V_2 = L_25;
		V_5 = 0;
		goto IL_0095;
	}

IL_007c:
	{
		FieldInfoU5BU5D_t846150980* L_26 = V_2;
		int32_t L_27 = V_5;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		FieldInfo_t * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_29);
		String_t* L_31 = V_1;
		NullCheck(L_30);
		bool L_32 = String_Contains_m1147431944(L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_008f;
		}
	}
	{
		return (bool)1;
	}

IL_008f:
	{
		int32_t L_33 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_0095:
	{
		int32_t L_34 = V_5;
		FieldInfoU5BU5D_t846150980* L_35 = V_2;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_007c;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.PropertyInfoExtensions::IsAliasProperty(System.Reflection.PropertyInfo)
extern "C" IL2CPP_METHOD_ATTR bool PropertyInfoExtensions_IsAliasProperty_m417595411 (RuntimeObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyInfoExtensions_IsAliasProperty_m417595411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyInfo_t * L_0 = ___propertyInfo0;
		return (bool)((!(((RuntimeObject*)(MemberAliasPropertyInfo_t4204559890 *)((MemberAliasPropertyInfo_t4204559890 *)IsInstSealed((RuntimeObject*)L_0, MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.PropertyInfoExtensions::DeAliasProperty(System.Reflection.PropertyInfo,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR PropertyInfo_t * PropertyInfoExtensions_DeAliasProperty_m3306904115 (RuntimeObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, bool ___throwOnNotAliased1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyInfoExtensions_DeAliasProperty_m3306904115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemberAliasPropertyInfo_t4204559890 * V_0 = NULL;
	{
		PropertyInfo_t * L_0 = ___propertyInfo0;
		V_0 = ((MemberAliasPropertyInfo_t4204559890 *)IsInstSealed((RuntimeObject*)L_0, MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var));
		MemberAliasPropertyInfo_t4204559890 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0018;
	}

IL_000c:
	{
		MemberAliasPropertyInfo_t4204559890 * L_2 = V_0;
		NullCheck(L_2);
		PropertyInfo_t * L_3 = MemberAliasPropertyInfo_get_AliasedProperty_m1519871680(L_2, /*hidden argument*/NULL);
		V_0 = ((MemberAliasPropertyInfo_t4204559890 *)IsInstSealed((RuntimeObject*)L_3, MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		MemberAliasPropertyInfo_t4204559890 * L_4 = V_0;
		NullCheck(L_4);
		PropertyInfo_t * L_5 = MemberAliasPropertyInfo_get_AliasedProperty_m1519871680(L_4, /*hidden argument*/NULL);
		if (((MemberAliasPropertyInfo_t4204559890 *)IsInstSealed((RuntimeObject*)L_5, MemberAliasPropertyInfo_t4204559890_il2cpp_TypeInfo_var)))
		{
			goto IL_000c;
		}
	}
	{
		MemberAliasPropertyInfo_t4204559890 * L_6 = V_0;
		NullCheck(L_6);
		PropertyInfo_t * L_7 = MemberAliasPropertyInfo_get_AliasedProperty_m1519871680(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_002c:
	{
		bool L_8 = ___throwOnNotAliased1;
		if (!L_8)
		{
			goto IL_004a;
		}
	}
	{
		PropertyInfo_t * L_9 = ___propertyInfo0;
		String_t* L_10 = MemberInfoExtensions_GetNiceName_m2681496769(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_11 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2302966210, L_10, _stringLiteral3758551614, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_12 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_12, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, PropertyInfoExtensions_DeAliasProperty_m3306904115_RuntimeMethod_var);
	}

IL_004a:
	{
		PropertyInfo_t * L_13 = ___propertyInfo0;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Utilities.StringExtensions::ToTitleCase(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* StringExtensions_ToTitleCase_m1099386125 (RuntimeObject * __this /* static, unused */, String_t* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringExtensions_ToTitleCase_m1099386125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	Il2CppChar V_3 = 0x0;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_005a;
	}

IL_000a:
	{
		String_t* L_1 = ___input0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m2986988803(L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		Il2CppChar L_4 = V_2;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)95)))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_5 = V_1;
		String_t* L_6 = ___input0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1))) >= ((int32_t)L_7)))
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_8 = ___input0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m2986988803(L_8, ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_10;
		Il2CppChar L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_12 = Char_IsLower_m3108076820(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppChar L_13 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_14 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		Il2CppChar L_15 = Char_ToUpper_m3659851865(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
	}

IL_0040:
	{
		StringBuilder_t * L_16 = V_0;
		Il2CppChar L_17 = V_3;
		NullCheck(L_16);
		StringBuilder_Append_m2383614642(L_16, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
		goto IL_0056;
	}

IL_004e:
	{
		StringBuilder_t * L_19 = V_0;
		Il2CppChar L_20 = V_2;
		NullCheck(L_19);
		StringBuilder_Append_m2383614642(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0056:
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_005a:
	{
		int32_t L_22 = V_1;
		String_t* L_23 = ___input0;
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m3847582255(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_000a;
		}
	}
	{
		StringBuilder_t * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		return L_26;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.StringExtensions::IsNullOrWhitespace(System.String)
extern "C" IL2CPP_METHOD_ATTR bool StringExtensions_IsNullOrWhitespace_m3968263663 (RuntimeObject * __this /* static, unused */, String_t* ___str0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringExtensions_IsNullOrWhitespace_m3968263663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___str0;
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		goto IL_0020;
	}

IL_000c:
	{
		String_t* L_2 = ___str0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Il2CppChar L_4 = String_get_Chars_m2986988803(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_5 = Char_IsWhiteSpace_m2148390798(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_0;
		String_t* L_8 = ___str0;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m3847582255(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_000c;
		}
	}

IL_0029:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCachedNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetCachedNiceName_m3030179020 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCachedNiceName_m3030179020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_CachedNiceNames_LOCK_10();
		V_1 = L_0;
		RuntimeObject * L_1 = V_1;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			Dictionary_2_t4291797753 * L_2 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_CachedNiceNames_11();
			Type_t * L_3 = ___type0;
			NullCheck(L_2);
			bool L_4 = Dictionary_2_TryGetValue_m1231945001(L_2, L_3, (String_t**)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1231945001_RuntimeMethod_var);
			if (L_4)
			{
				goto IL_002e;
			}
		}

IL_001b:
		{
			Type_t * L_5 = ___type0;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			String_t* L_6 = TypeExtensions_CreateNiceName_m1114677372(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			V_0 = L_6;
			Dictionary_2_t4291797753 * L_7 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_CachedNiceNames_11();
			Type_t * L_8 = ___type0;
			String_t* L_9 = V_0;
			NullCheck(L_7);
			Dictionary_2_Add_m2678085394(L_7, L_8, L_9, /*hidden argument*/Dictionary_2_Add_m2678085394_RuntimeMethod_var);
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x37, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		RuntimeObject * L_10 = V_1;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0037:
	{
		String_t* L_11 = V_0;
		return L_11;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::CreateNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_CreateNiceName_m1114677372 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_CreateNiceName_m1114677372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	TypeU5BU5D_t3940880105* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Type_t * V_6 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsArray_m2591212821(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(57 /* System.Int32 System.Type::GetArrayRank() */, L_2);
		V_4 = L_3;
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::GetElementType() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_6 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_4;
		G_B2_0 = L_6;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			G_B3_0 = L_6;
			goto IL_0027;
		}
	}
	{
		G_B4_0 = _stringLiteral3497344926;
		G_B4_1 = G_B2_0;
		goto IL_002c;
	}

IL_0027:
	{
		G_B4_0 = _stringLiteral3458054133;
		G_B4_1 = G_B3_0;
	}

IL_002c:
	{
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return L_8;
	}

IL_0032:
	{
		Type_t * L_9 = ___type0;
		RuntimeTypeHandle_t3027515415  L_10 = { reinterpret_cast<intptr_t> (Nullable_1_t3772285925_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_12 = TypeExtensions_InheritsFrom_m434511460(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_13 = ___type0;
		NullCheck(L_13);
		TypeU5BU5D_t3940880105* L_14 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_13);
		NullCheck(L_14);
		int32_t L_15 = 0;
		Type_t * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_17 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		String_t* L_18 = String_Concat_m3937257545(NULL /*static, unused*/, L_17, _stringLiteral3452614545, /*hidden argument*/NULL);
		return L_18;
	}

IL_005c:
	{
		Type_t * L_19 = ___type0;
		NullCheck(L_19);
		bool L_20 = Type_get_IsByRef_m1262524108(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_21 = ___type0;
		NullCheck(L_21);
		Type_t * L_22 = VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::GetElementType() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_23 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		String_t* L_24 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2606052858, L_23, /*hidden argument*/NULL);
		return L_24;
	}

IL_007a:
	{
		Type_t * L_25 = ___type0;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_25);
		if (L_26)
		{
			goto IL_008a;
		}
	}
	{
		Type_t * L_27 = ___type0;
		NullCheck(L_27);
		bool L_28 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_27);
		if (L_28)
		{
			goto IL_0091;
		}
	}

IL_008a:
	{
		Type_t * L_29 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_30 = TypeExtensions_TypeNameGauntlet_m2028448992(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return L_30;
	}

IL_0091:
	{
		StringBuilder_t * L_31 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_31, /*hidden argument*/NULL);
		V_0 = L_31;
		Type_t * L_32 = ___type0;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_32);
		V_1 = L_33;
		String_t* L_34 = V_1;
		NullCheck(L_34);
		int32_t L_35 = String_IndexOf_m1977622757(L_34, _stringLiteral3452614592, /*hidden argument*/NULL);
		V_2 = L_35;
		int32_t L_36 = V_2;
		if ((((int32_t)L_36) == ((int32_t)(-1))))
		{
			goto IL_00bf;
		}
	}
	{
		StringBuilder_t * L_37 = V_0;
		String_t* L_38 = V_1;
		int32_t L_39 = V_2;
		NullCheck(L_38);
		String_t* L_40 = String_Substring_m1610150815(L_38, 0, L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		StringBuilder_Append_m1965104174(L_37, L_40, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00bf:
	{
		StringBuilder_t * L_41 = V_0;
		String_t* L_42 = V_1;
		NullCheck(L_41);
		StringBuilder_Append_m1965104174(L_41, L_42, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		StringBuilder_t * L_43 = V_0;
		NullCheck(L_43);
		StringBuilder_Append_m2383614642(L_43, ((int32_t)60), /*hidden argument*/NULL);
		Type_t * L_44 = ___type0;
		NullCheck(L_44);
		TypeU5BU5D_t3940880105* L_45 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_44);
		V_3 = L_45;
		V_5 = 0;
		goto IL_0106;
	}

IL_00dc:
	{
		TypeU5BU5D_t3940880105* L_46 = V_3;
		int32_t L_47 = V_5;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		Type_t * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		V_6 = L_49;
		int32_t L_50 = V_5;
		if (!L_50)
		{
			goto IL_00f2;
		}
	}
	{
		StringBuilder_t * L_51 = V_0;
		NullCheck(L_51);
		StringBuilder_Append_m1965104174(L_51, _stringLiteral3450517380, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		StringBuilder_t * L_52 = V_0;
		Type_t * L_53 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_54 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m1965104174(L_52, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)1));
	}

IL_0106:
	{
		int32_t L_56 = V_5;
		TypeU5BU5D_t3940880105* L_57 = V_3;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_57)->max_length)))))))
		{
			goto IL_00dc;
		}
	}
	{
		StringBuilder_t * L_58 = V_0;
		NullCheck(L_58);
		StringBuilder_Append_m2383614642(L_58, ((int32_t)62), /*hidden argument*/NULL);
		StringBuilder_t * L_59 = V_0;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		return L_60;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCastDefined(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_HasCastDefined_m1369193891 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_HasCastDefined_m1369193891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___from0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(122 /* System.Boolean System.Type::get_IsEnum() */, L_0);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Type_t * L_2 = ___from0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		Type_t * L_3 = Enum_GetUnderlyingType_m2480312097(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Type_t * L_4 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_5 = TypeExtensions_IsCastableTo_m2813347195(NULL /*static, unused*/, L_3, L_4, (bool)0, /*hidden argument*/NULL);
		return L_5;
	}

IL_0016:
	{
		Type_t * L_6 = ___to1;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(122 /* System.Boolean System.Type::get_IsEnum() */, L_6);
		if (!L_7)
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_8 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		Type_t * L_9 = Enum_GetUnderlyingType_m2480312097(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Type_t * L_10 = ___from0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_11 = TypeExtensions_IsCastableTo_m2813347195(NULL /*static, unused*/, L_9, L_10, (bool)0, /*hidden argument*/NULL);
		return L_11;
	}

IL_002c:
	{
		Type_t * L_12 = ___from0;
		NullCheck(L_12);
		bool L_13 = Type_get_IsPrimitive_m1114712797(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_14 = ___from0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Type_t * L_15 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_VoidPointerType_12();
		if ((!(((RuntimeObject*)(Type_t *)L_14) == ((RuntimeObject*)(Type_t *)L_15))))
		{
			goto IL_00cb;
		}
	}

IL_003f:
	{
		Type_t * L_16 = ___to1;
		NullCheck(L_16);
		bool L_17 = Type_get_IsPrimitive_m1114712797(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_004f;
		}
	}
	{
		Type_t * L_18 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Type_t * L_19 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_VoidPointerType_12();
		if ((!(((RuntimeObject*)(Type_t *)L_18) == ((RuntimeObject*)(Type_t *)L_19))))
		{
			goto IL_00cb;
		}
	}

IL_004f:
	{
		bool L_20 = ___requireImplicitCast2;
		if (!L_20)
		{
			goto IL_0064;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Dictionary_2_t3493241298 * L_21 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_PrimitiveImplicitCasts_13();
		Type_t * L_22 = ___from0;
		NullCheck(L_21);
		HashSet_1_t1048894234 * L_23 = Dictionary_2_get_Item_m1040169990(L_21, L_22, /*hidden argument*/Dictionary_2_get_Item_m1040169990_RuntimeMethod_var);
		Type_t * L_24 = ___to1;
		NullCheck(L_23);
		bool L_25 = HashSet_1_Contains_m3794231197(L_23, L_24, /*hidden argument*/HashSet_1_Contains_m3794231197_RuntimeMethod_var);
		return L_25;
	}

IL_0064:
	{
		Type_t * L_26 = ___from0;
		RuntimeTypeHandle_t3027515415  L_27 = { reinterpret_cast<intptr_t> (IntPtr_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_26) == ((RuntimeObject*)(Type_t *)L_28))))
		{
			goto IL_008a;
		}
	}
	{
		Type_t * L_29 = ___to1;
		RuntimeTypeHandle_t3027515415  L_30 = { reinterpret_cast<intptr_t> (UIntPtr_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_31 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_29) == ((RuntimeObject*)(Type_t *)L_31))))
		{
			goto IL_0080;
		}
	}
	{
		return (bool)0;
	}

IL_0080:
	{
		Type_t * L_32 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Type_t * L_33 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_VoidPointerType_12();
		if ((!(((RuntimeObject*)(Type_t *)L_32) == ((RuntimeObject*)(Type_t *)L_33))))
		{
			goto IL_00b0;
		}
	}
	{
		return (bool)1;
	}

IL_008a:
	{
		Type_t * L_34 = ___from0;
		RuntimeTypeHandle_t3027515415  L_35 = { reinterpret_cast<intptr_t> (UIntPtr_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_34) == ((RuntimeObject*)(Type_t *)L_36))))
		{
			goto IL_00b0;
		}
	}
	{
		Type_t * L_37 = ___to1;
		RuntimeTypeHandle_t3027515415  L_38 = { reinterpret_cast<intptr_t> (IntPtr_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_37) == ((RuntimeObject*)(Type_t *)L_39))))
		{
			goto IL_00a6;
		}
	}
	{
		return (bool)0;
	}

IL_00a6:
	{
		Type_t * L_40 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Type_t * L_41 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_VoidPointerType_12();
		if ((!(((RuntimeObject*)(Type_t *)L_40) == ((RuntimeObject*)(Type_t *)L_41))))
		{
			goto IL_00b0;
		}
	}
	{
		return (bool)1;
	}

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		HashSet_1_t1048894234 * L_42 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_ExplicitCastIntegrals_14();
		Type_t * L_43 = ___from0;
		NullCheck(L_42);
		bool L_44 = HashSet_1_Contains_m3794231197(L_42, L_43, /*hidden argument*/HashSet_1_Contains_m3794231197_RuntimeMethod_var);
		if (!L_44)
		{
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		HashSet_1_t1048894234 * L_45 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_ExplicitCastIntegrals_14();
		Type_t * L_46 = ___to1;
		NullCheck(L_45);
		bool L_47 = HashSet_1_Contains_m3794231197(L_45, L_46, /*hidden argument*/HashSet_1_Contains_m3794231197_RuntimeMethod_var);
		return L_47;
	}

IL_00c9:
	{
		return (bool)0;
	}

IL_00cb:
	{
		Type_t * L_48 = ___from0;
		Type_t * L_49 = ___to1;
		bool L_50 = ___requireImplicitCast2;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		MethodInfo_t * L_51 = TypeExtensions_GetCastMethod_m3599746289(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		return (bool)((!(((RuntimeObject*)(MethodInfo_t *)L_51) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifier(System.String)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsValidIdentifier_m3751677380 (RuntimeObject * __this /* static, unused */, String_t* ___identifier0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_IsValidIdentifier_m3751677380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t1281789340* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___identifier0;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		String_t* L_1 = ___identifier0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_000d;
		}
	}

IL_000b:
	{
		return (bool)0;
	}

IL_000d:
	{
		String_t* L_3 = ___identifier0;
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m363431711(L_3, ((int32_t)46), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_6 = ___identifier0;
		CharU5BU5D_t3528271667* L_7 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_8 = L_7;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)46));
		NullCheck(L_6);
		StringU5BU5D_t1281789340* L_9 = String_Split_m3646115398(L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		V_2 = 0;
		goto IL_0040;
	}

IL_0030:
	{
		StringU5BU5D_t1281789340* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_14 = TypeExtensions_IsValidIdentifier_m3751677380(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_003c;
		}
	}
	{
		return (bool)0;
	}

IL_003c:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0040:
	{
		int32_t L_16 = V_2;
		StringU5BU5D_t1281789340* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)1;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		HashSet_1_t412400163 * L_18 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_ReservedCSharpKeywords_8();
		String_t* L_19 = ___identifier0;
		NullCheck(L_18);
		bool L_20 = HashSet_1_Contains_m1669355224(L_18, L_19, /*hidden argument*/HashSet_1_Contains_m1669355224_RuntimeMethod_var);
		if (!L_20)
		{
			goto IL_0057;
		}
	}
	{
		return (bool)0;
	}

IL_0057:
	{
		String_t* L_21 = ___identifier0;
		NullCheck(L_21);
		Il2CppChar L_22 = String_get_Chars_m2986988803(L_21, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_23 = TypeExtensions_IsValidIdentifierStartCharacter_m769556910(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0067;
		}
	}
	{
		return (bool)0;
	}

IL_0067:
	{
		V_3 = 1;
		goto IL_007f;
	}

IL_006b:
	{
		String_t* L_24 = ___identifier0;
		int32_t L_25 = V_3;
		NullCheck(L_24);
		Il2CppChar L_26 = String_get_Chars_m2986988803(L_24, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_27 = TypeExtensions_IsValidIdentifierPartCharacter_m2068727156(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_007b;
		}
	}
	{
		return (bool)0;
	}

IL_007b:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_007f:
	{
		int32_t L_29 = V_3;
		String_t* L_30 = ___identifier0;
		NullCheck(L_30);
		int32_t L_31 = String_get_Length_m3847582255(L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_29) < ((int32_t)L_31)))
		{
			goto IL_006b;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierStartCharacter(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsValidIdentifierStartCharacter_m769556910 (RuntimeObject * __this /* static, unused */, Il2CppChar ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_IsValidIdentifierStartCharacter_m769556910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___c0;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)97))))
		{
			goto IL_000a;
		}
	}
	{
		Il2CppChar L_1 = ___c0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)122))))
		{
			goto IL_0025;
		}
	}

IL_000a:
	{
		Il2CppChar L_2 = ___c0;
		if ((((int32_t)L_2) < ((int32_t)((int32_t)65))))
		{
			goto IL_0014;
		}
	}
	{
		Il2CppChar L_3 = ___c0;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0025;
		}
	}

IL_0014:
	{
		Il2CppChar L_4 = ___c0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)95))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppChar L_5 = ___c0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)64))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppChar L_6 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_7 = Char_IsLetter_m3996985877(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0025:
	{
		return (bool)1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierPartCharacter(System.Char)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsValidIdentifierPartCharacter_m2068727156 (RuntimeObject * __this /* static, unused */, Il2CppChar ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_IsValidIdentifierPartCharacter_m2068727156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___c0;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)97))))
		{
			goto IL_000a;
		}
	}
	{
		Il2CppChar L_1 = ___c0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)122))))
		{
			goto IL_002a;
		}
	}

IL_000a:
	{
		Il2CppChar L_2 = ___c0;
		if ((((int32_t)L_2) < ((int32_t)((int32_t)65))))
		{
			goto IL_0014;
		}
	}
	{
		Il2CppChar L_3 = ___c0;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)90))))
		{
			goto IL_002a;
		}
	}

IL_0014:
	{
		Il2CppChar L_4 = ___c0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)95))))
		{
			goto IL_002a;
		}
	}
	{
		Il2CppChar L_5 = ___c0;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)48))))
		{
			goto IL_0023;
		}
	}
	{
		Il2CppChar L_6 = ___c0;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)57))))
		{
			goto IL_002a;
		}
	}

IL_0023:
	{
		Il2CppChar L_7 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3634460470_il2cpp_TypeInfo_var);
		bool L_8 = Char_IsLetter_m3996985877(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_002a:
	{
		return (bool)1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsCastableTo(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsCastableTo_m2813347195 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_IsCastableTo_m2813347195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___from0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral2755855785, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_IsCastableTo_m2813347195_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___to1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral3454777292, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_IsCastableTo_m2813347195_RuntimeMethod_var);
	}

IL_001c:
	{
		Type_t * L_4 = ___from0;
		Type_t * L_5 = ___to1;
		if ((!(((RuntimeObject*)(Type_t *)L_4) == ((RuntimeObject*)(Type_t *)L_5))))
		{
			goto IL_0022;
		}
	}
	{
		return (bool)1;
	}

IL_0022:
	{
		Type_t * L_6 = ___to1;
		Type_t * L_7 = ___from0;
		NullCheck(L_6);
		bool L_8 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_6, L_7);
		if (L_8)
		{
			goto IL_0034;
		}
	}
	{
		Type_t * L_9 = ___from0;
		Type_t * L_10 = ___to1;
		bool L_11 = ___requireImplicitCast2;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_12 = TypeExtensions_HasCastDefined_m1369193891(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0034:
	{
		return (bool)1;
	}
}
// System.Func`2<System.Object,System.Object> Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethodDelegate(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Func_2_t2447130374 * TypeExtensions_GetCastMethodDelegate_m2926053447 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCastMethodDelegate_m2926053447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t2447130374 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	U3CU3Ec__DisplayClass22_0_t2941737288 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_WeaklyTypedTypeCastDelegates_LOCK_4();
		V_1 = L_0;
		RuntimeObject * L_1 = V_1;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			DoubleLookupDictionary_3_t650546409 * L_2 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_WeaklyTypedTypeCastDelegates_6();
			Type_t * L_3 = ___from0;
			Type_t * L_4 = ___to1;
			NullCheck(L_2);
			bool L_5 = DoubleLookupDictionary_3_TryGetInnerValue_m2712346314(L_2, L_3, L_4, (Func_2_t2447130374 **)(&V_0), /*hidden argument*/DoubleLookupDictionary_3_TryGetInnerValue_m2712346314_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0053;
			}
		}

IL_001c:
		{
			U3CU3Ec__DisplayClass22_0_t2941737288 * L_6 = (U3CU3Ec__DisplayClass22_0_t2941737288 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_0_t2941737288_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass22_0__ctor_m759390448(L_6, /*hidden argument*/NULL);
			V_2 = L_6;
			U3CU3Ec__DisplayClass22_0_t2941737288 * L_7 = V_2;
			Type_t * L_8 = ___from0;
			Type_t * L_9 = ___to1;
			bool L_10 = ___requireImplicitCast2;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			MethodInfo_t * L_11 = TypeExtensions_GetCastMethod_m3599746289(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
			NullCheck(L_7);
			L_7->set_method_0(L_11);
			U3CU3Ec__DisplayClass22_0_t2941737288 * L_12 = V_2;
			NullCheck(L_12);
			MethodInfo_t * L_13 = L_12->get_method_0();
			if (!L_13)
			{
				goto IL_0045;
			}
		}

IL_0038:
		{
			U3CU3Ec__DisplayClass22_0_t2941737288 * L_14 = V_2;
			intptr_t L_15 = (intptr_t)U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m3161334231_RuntimeMethod_var;
			Func_2_t2447130374 * L_16 = (Func_2_t2447130374 *)il2cpp_codegen_object_new(Func_2_t2447130374_il2cpp_TypeInfo_var);
			Func_2__ctor_m3860723828(L_16, L_14, (intptr_t)L_15, /*hidden argument*/Func_2__ctor_m3860723828_RuntimeMethod_var);
			V_0 = L_16;
		}

IL_0045:
		{
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			DoubleLookupDictionary_3_t650546409 * L_17 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_WeaklyTypedTypeCastDelegates_6();
			Type_t * L_18 = ___from0;
			Type_t * L_19 = ___to1;
			Func_2_t2447130374 * L_20 = V_0;
			NullCheck(L_17);
			DoubleLookupDictionary_3_AddInner_m1135260050(L_17, L_18, L_19, L_20, /*hidden argument*/DoubleLookupDictionary_3_AddInner_m1135260050_RuntimeMethod_var);
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		RuntimeObject * L_21 = V_1;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005c:
	{
		Func_2_t2447130374 * L_22 = V_0;
		return L_22;
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethod(System.Type,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * TypeExtensions_GetCastMethod_m3599746289 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, bool ___requireImplicitCast2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCastMethod_m3599746289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfoU5BU5D_t2572182361* V_0 = NULL;
	MethodInfoU5BU5D_t2572182361* V_1 = NULL;
	int32_t V_2 = 0;
	MethodInfo_t * V_3 = NULL;
	int32_t V_4 = 0;
	MethodInfo_t * V_5 = NULL;
	{
		Type_t * L_0 = ___from0;
		NullCheck(L_0);
		MethodInfoU5BU5D_t2572182361* L_1 = VirtFuncInvoker1< MethodInfoU5BU5D_t2572182361*, int32_t >::Invoke(72 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)26));
		V_0 = L_1;
		V_2 = 0;
		goto IL_004c;
	}

IL_000d:
	{
		MethodInfoU5BU5D_t2572182361* L_2 = V_0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		MethodInfo_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_3 = L_5;
		MethodInfo_t * L_6 = V_3;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_6);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral3306367446, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0038;
		}
	}
	{
		bool L_9 = ___requireImplicitCast2;
		if (L_9)
		{
			goto IL_0048;
		}
	}
	{
		MethodInfo_t * L_10 = V_3;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_10);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_11, _stringLiteral2655463595, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0048;
		}
	}

IL_0038:
	{
		Type_t * L_13 = ___to1;
		MethodInfo_t * L_14 = V_3;
		NullCheck(L_14);
		Type_t * L_15 = VirtFuncInvoker0< Type_t * >::Invoke(65 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_14);
		NullCheck(L_13);
		bool L_16 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_13, L_15);
		if (!L_16)
		{
			goto IL_0048;
		}
	}
	{
		MethodInfo_t * L_17 = V_3;
		return L_17;
	}

IL_0048:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_004c:
	{
		int32_t L_19 = V_2;
		MethodInfoU5BU5D_t2572182361* L_20 = V_0;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		Type_t * L_21 = ___to1;
		NullCheck(L_21);
		MethodInfoU5BU5D_t2572182361* L_22 = VirtFuncInvoker1< MethodInfoU5BU5D_t2572182361*, int32_t >::Invoke(72 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_21, ((int32_t)88));
		V_1 = L_22;
		V_4 = 0;
		goto IL_00ae;
	}

IL_0060:
	{
		MethodInfoU5BU5D_t2572182361* L_23 = V_1;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		MethodInfo_t * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_5 = L_26;
		MethodInfo_t * L_27 = V_5;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_27);
		bool L_29 = String_op_Equality_m920492651(NULL /*static, unused*/, L_28, _stringLiteral3306367446, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_008f;
		}
	}
	{
		bool L_30 = ___requireImplicitCast2;
		if (L_30)
		{
			goto IL_00a8;
		}
	}
	{
		MethodInfo_t * L_31 = V_5;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_31);
		bool L_33 = String_op_Equality_m920492651(NULL /*static, unused*/, L_32, _stringLiteral2655463595, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00a8;
		}
	}

IL_008f:
	{
		MethodInfo_t * L_34 = V_5;
		NullCheck(L_34);
		ParameterInfoU5BU5D_t390618515* L_35 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(43 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_34);
		NullCheck(L_35);
		int32_t L_36 = 0;
		ParameterInfo_t1861056598 * L_37 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		Type_t * L_38 = VirtFuncInvoker0< Type_t * >::Invoke(8 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_37);
		Type_t * L_39 = ___from0;
		NullCheck(L_38);
		bool L_40 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_38, L_39);
		if (!L_40)
		{
			goto IL_00a8;
		}
	}
	{
		MethodInfo_t * L_41 = V_5;
		return L_41;
	}

IL_00a8:
	{
		int32_t L_42 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_00ae:
	{
		int32_t L_43 = V_4;
		MethodInfoU5BU5D_t2572182361* L_44 = V_1;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_44)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_HasCustomAttribute_m456943493 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, Type_t * ___customAttributeType1, bool ___inherit2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_HasCustomAttribute_m456943493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral1904273217, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_HasCustomAttribute_m456943493_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___customAttributeType1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral191294152, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_HasCustomAttribute_m456943493_RuntimeMethod_var);
	}

IL_001c:
	{
		RuntimeTypeHandle_t3027515415  L_4 = { reinterpret_cast<intptr_t> (Attribute_t861562559_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Type_t * L_6 = ___customAttributeType1;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_5, L_6);
		if (L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___customAttributeType1;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_8);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_9, _stringLiteral1429726276, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_11 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, NULL, TypeExtensions_HasCustomAttribute_m456943493_RuntimeMethod_var);
	}

IL_0049:
	{
		MemberInfo_t * L_12 = ___memberInfo0;
		Type_t * L_13 = ___customAttributeType1;
		bool L_14 = ___inherit2;
		NullCheck(L_12);
		ObjectU5BU5D_t2843939325* L_15 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(19 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_12, L_13, L_14);
		NullCheck(L_15);
		return (bool)((!(((uint32_t)(((RuntimeArray *)L_15)->max_length)) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean,System.Attribute&)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_HasCustomAttribute_m3261241330 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, Type_t * ___customAttributeType1, bool ___inherit2, Attribute_t861562559 ** ___attribute3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_HasCustomAttribute_m3261241330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}
	{
		Type_t * L_1 = ___customAttributeType1;
		if (L_1)
		{
			goto IL_000c;
		}
	}

IL_0006:
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_HasCustomAttribute_m3261241330_RuntimeMethod_var);
	}

IL_000c:
	{
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (Attribute_t861562559_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Type_t * L_5 = ___customAttributeType1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_4, L_5);
		if (L_6)
		{
			goto IL_0039;
		}
	}
	{
		Type_t * L_7 = ___customAttributeType1;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_7);
		String_t* L_9 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_8, _stringLiteral1429726276, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, NULL, TypeExtensions_HasCustomAttribute_m3261241330_RuntimeMethod_var);
	}

IL_0039:
	{
		MemberInfo_t * L_11 = ___memberInfo0;
		Type_t * L_12 = ___customAttributeType1;
		bool L_13 = ___inherit2;
		NullCheck(L_11);
		ObjectU5BU5D_t2843939325* L_14 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(19 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_11, L_12, L_13);
		V_0 = L_14;
		ObjectU5BU5D_t2843939325* L_15 = V_0;
		NullCheck(L_15);
		if (!(((RuntimeArray *)L_15)->max_length))
		{
			goto IL_0052;
		}
	}
	{
		Attribute_t861562559 ** L_16 = ___attribute3;
		ObjectU5BU5D_t2843939325* L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = 0;
		RuntimeObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		*((RuntimeObject **)L_16) = (RuntimeObject *)((Attribute_t861562559 *)CastclassClass((RuntimeObject*)L_19, Attribute_t861562559_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_16, (RuntimeObject *)((Attribute_t861562559 *)CastclassClass((RuntimeObject*)L_19, Attribute_t861562559_il2cpp_TypeInfo_var)));
		return (bool)1;
	}

IL_0052:
	{
		Attribute_t861562559 ** L_20 = ___attribute3;
		*((RuntimeObject **)L_20) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_20, (RuntimeObject *)NULL);
		return (bool)0;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOrInherits(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_ImplementsOrInherits_m369530965 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___to1, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___to1;
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericType(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_ImplementsOpenGenericType_m3675074644 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_ImplementsOpenGenericType_m3675074644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___candidateType0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}
	{
		Type_t * L_1 = ___openGenericType1;
		if (L_1)
		{
			goto IL_000c;
		}
	}

IL_0006:
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_ImplementsOpenGenericType_m3675074644_RuntimeMethod_var);
	}

IL_000c:
	{
		Type_t * L_3 = ___openGenericType1;
		NullCheck(L_3);
		bool L_4 = Type_get_IsInterface_m3284996719(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_5 = ___candidateType0;
		Type_t * L_6 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_7 = TypeExtensions_ImplementsOpenGenericInterface_m3084334200(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_001c:
	{
		Type_t * L_8 = ___candidateType0;
		Type_t * L_9 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_10 = TypeExtensions_ImplementsOpenGenericClass_m2650584869(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericInterface(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_ImplementsOpenGenericInterface_m3084334200 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericInterfaceType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_ImplementsOpenGenericInterface_m3084334200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass35_0_t2941409609 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_0 = (U3CU3Ec__DisplayClass35_0_t2941409609 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass35_0_t2941409609_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass35_0__ctor_m563200432(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_1 = V_0;
		Type_t * L_2 = ___openGenericInterfaceType1;
		NullCheck(L_1);
		L_1->set_openGenericInterfaceType_0(L_2);
		Type_t * L_3 = ___candidateType0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = L_4->get_openGenericInterfaceType_0();
		if (L_5)
		{
			goto IL_001e;
		}
	}

IL_0018:
	{
		ArgumentNullException_t1615371798 * L_6 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, TypeExtensions_ImplementsOpenGenericInterface_m3084334200_RuntimeMethod_var);
	}

IL_001e:
	{
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_7 = V_0;
		NullCheck(L_7);
		Type_t * L_8 = L_7->get_openGenericInterfaceType_0();
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_8);
		if (!L_9)
		{
			goto IL_0038;
		}
	}
	{
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_10 = V_0;
		NullCheck(L_10);
		Type_t * L_11 = L_10->get_openGenericInterfaceType_0();
		NullCheck(L_11);
		bool L_12 = Type_get_IsInterface_m3284996719(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0058;
		}
	}

IL_0038:
	{
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_13 = V_0;
		NullCheck(L_13);
		Type_t * L_14 = L_13->get_openGenericInterfaceType_0();
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_14);
		String_t* L_16 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_15, _stringLiteral752912519, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_17 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_17, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17, NULL, TypeExtensions_ImplementsOpenGenericInterface_m3084334200_RuntimeMethod_var);
	}

IL_0058:
	{
		Type_t * L_18 = ___candidateType0;
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_19 = V_0;
		NullCheck(L_19);
		Type_t * L_20 = L_19->get_openGenericInterfaceType_0();
		NullCheck(L_18);
		bool L_21 = VirtFuncInvoker1< bool, Type_t * >::Invoke(177 /* System.Boolean System.Type::Equals(System.Type) */, L_18, L_20);
		if (L_21)
		{
			goto IL_0099;
		}
	}
	{
		Type_t * L_22 = ___candidateType0;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_22);
		if (!L_23)
		{
			goto IL_0081;
		}
	}
	{
		Type_t * L_24 = ___candidateType0;
		NullCheck(L_24);
		Type_t * L_25 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_24);
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_26 = V_0;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_openGenericInterfaceType_0();
		NullCheck(L_25);
		bool L_28 = VirtFuncInvoker1< bool, Type_t * >::Invoke(177 /* System.Boolean System.Type::Equals(System.Type) */, L_25, L_27);
		if (L_28)
		{
			goto IL_0099;
		}
	}

IL_0081:
	{
		Type_t * L_29 = ___candidateType0;
		NullCheck(L_29);
		TypeU5BU5D_t3940880105* L_30 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(79 /* System.Type[] System.Type::GetInterfaces() */, L_29);
		U3CU3Ec__DisplayClass35_0_t2941409609 * L_31 = V_0;
		intptr_t L_32 = (intptr_t)U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m2349605198_RuntimeMethod_var;
		Func_2_t561252955 * L_33 = (Func_2_t561252955 *)il2cpp_codegen_object_new(Func_2_t561252955_il2cpp_TypeInfo_var);
		Func_2__ctor_m604830276(L_33, L_31, (intptr_t)L_32, /*hidden argument*/Func_2__ctor_m604830276_RuntimeMethod_var);
		bool L_34 = Enumerable_Any_TisType_t_m7140321(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_30, L_33, /*hidden argument*/Enumerable_Any_TisType_t_m7140321_RuntimeMethod_var);
		return L_34;
	}

IL_0099:
	{
		return (bool)1;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericClass(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_ImplementsOpenGenericClass_m2650584869 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_ImplementsOpenGenericClass_m2650584869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___candidateType0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}
	{
		Type_t * L_1 = ___openGenericType1;
		if (L_1)
		{
			goto IL_000c;
		}
	}

IL_0006:
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_ImplementsOpenGenericClass_m2650584869_RuntimeMethod_var);
	}

IL_000c:
	{
		Type_t * L_3 = ___openGenericType1;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_3);
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		Type_t * L_5 = ___openGenericType1;
		NullCheck(L_5);
		bool L_6 = Type_get_IsClass_m589177581(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_7 = ___openGenericType1;
		NullCheck(L_7);
		bool L_8 = Type_get_IsValueType_m3108065642(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003f;
		}
	}

IL_0024:
	{
		Type_t * L_9 = ___openGenericType1;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_9);
		String_t* L_11 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_10, _stringLiteral3912771868, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_12 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_12, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, TypeExtensions_ImplementsOpenGenericClass_m2650584869_RuntimeMethod_var);
	}

IL_003f:
	{
		Type_t * L_13 = ___candidateType0;
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_13);
		if (!L_14)
		{
			goto IL_0050;
		}
	}
	{
		Type_t * L_15 = ___candidateType0;
		NullCheck(L_15);
		Type_t * L_16 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_15);
		Type_t * L_17 = ___openGenericType1;
		if ((((RuntimeObject*)(Type_t *)L_16) == ((RuntimeObject*)(Type_t *)L_17)))
		{
			goto IL_0067;
		}
	}

IL_0050:
	{
		Type_t * L_18 = ___candidateType0;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_18);
		if (!L_19)
		{
			goto IL_0065;
		}
	}
	{
		Type_t * L_20 = ___candidateType0;
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_20);
		Type_t * L_22 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_23 = TypeExtensions_ImplementsOpenGenericClass_m2650584869(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		return L_23;
	}

IL_0065:
	{
		return (bool)0;
	}

IL_0067:
	{
		return (bool)1;
	}
}
// System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericType(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR TypeU5BU5D_t3940880105* TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m1793324098 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m1793324098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___candidateType0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}
	{
		Type_t * L_1 = ___openGenericType1;
		if (L_1)
		{
			goto IL_000c;
		}
	}

IL_0006:
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m1793324098_RuntimeMethod_var);
	}

IL_000c:
	{
		Type_t * L_3 = ___openGenericType1;
		NullCheck(L_3);
		bool L_4 = Type_get_IsInterface_m3284996719(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_5 = ___candidateType0;
		Type_t * L_6 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_7 = TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_001c:
	{
		Type_t * L_8 = ___candidateType0;
		Type_t * L_9 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_10 = TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericClass(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR TypeU5BU5D_t3940880105* TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___candidateType0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}
	{
		Type_t * L_1 = ___openGenericType1;
		if (L_1)
		{
			goto IL_000c;
		}
	}

IL_0006:
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627_RuntimeMethod_var);
	}

IL_000c:
	{
		Type_t * L_3 = ___openGenericType1;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_3);
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		Type_t * L_5 = ___openGenericType1;
		NullCheck(L_5);
		bool L_6 = Type_get_IsClass_m589177581(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_7 = ___openGenericType1;
		NullCheck(L_7);
		bool L_8 = Type_get_IsValueType_m3108065642(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003f;
		}
	}

IL_0024:
	{
		Type_t * L_9 = ___openGenericType1;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_9);
		String_t* L_11 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_10, _stringLiteral3912771868, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_12 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_12, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627_RuntimeMethod_var);
	}

IL_003f:
	{
		Type_t * L_13 = ___candidateType0;
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_13);
		if (!L_14)
		{
			goto IL_0057;
		}
	}
	{
		Type_t * L_15 = ___candidateType0;
		NullCheck(L_15);
		Type_t * L_16 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_15);
		Type_t * L_17 = ___openGenericType1;
		if ((!(((RuntimeObject*)(Type_t *)L_16) == ((RuntimeObject*)(Type_t *)L_17))))
		{
			goto IL_0057;
		}
	}
	{
		Type_t * L_18 = ___candidateType0;
		NullCheck(L_18);
		TypeU5BU5D_t3940880105* L_19 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_18);
		return L_19;
	}

IL_0057:
	{
		Type_t * L_20 = ___candidateType0;
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_20);
		if (!L_21)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_22 = ___candidateType0;
		NullCheck(L_22);
		Type_t * L_23 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_22);
		Type_t * L_24 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_25 = TypeExtensions_ImplementsOpenGenericClass_m2650584869(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_26 = ___candidateType0;
		NullCheck(L_26);
		Type_t * L_27 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_26);
		Type_t * L_28 = ___openGenericType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_29 = TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		return L_29;
	}

IL_007a:
	{
		TypeU5BU5D_t3940880105* L_30 = (TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)0);
		return L_30;
	}
}
// System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericInterface(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR TypeU5BU5D_t3940880105* TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118 (RuntimeObject * __this /* static, unused */, Type_t * ___candidateType0, Type_t * ___openGenericInterfaceType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	TypeU5BU5D_t3940880105* V_1 = NULL;
	TypeU5BU5D_t3940880105* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	int32_t G_B11_0 = 0;
	{
		Type_t * L_0 = ___candidateType0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}
	{
		Type_t * L_1 = ___openGenericInterfaceType1;
		if (L_1)
		{
			goto IL_000c;
		}
	}

IL_0006:
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2751210921(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118_RuntimeMethod_var);
	}

IL_000c:
	{
		Type_t * L_3 = ___openGenericInterfaceType1;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_3);
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_5 = ___openGenericInterfaceType1;
		NullCheck(L_5);
		bool L_6 = Type_get_IsInterface_m3284996719(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0037;
		}
	}

IL_001c:
	{
		Type_t * L_7 = ___openGenericInterfaceType1;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_7);
		String_t* L_9 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_8, _stringLiteral752912519, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_10 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, NULL, TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118_RuntimeMethod_var);
	}

IL_0037:
	{
		Type_t * L_11 = ___candidateType0;
		Type_t * L_12 = ___openGenericInterfaceType1;
		NullCheck(L_11);
		bool L_13 = VirtFuncInvoker1< bool, Type_t * >::Invoke(177 /* System.Boolean System.Type::Equals(System.Type) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0059;
		}
	}
	{
		Type_t * L_14 = ___candidateType0;
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_14);
		if (!L_15)
		{
			goto IL_0056;
		}
	}
	{
		Type_t * L_16 = ___candidateType0;
		NullCheck(L_16);
		Type_t * L_17 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_16);
		Type_t * L_18 = ___openGenericInterfaceType1;
		NullCheck(L_17);
		bool L_19 = VirtFuncInvoker1< bool, Type_t * >::Invoke(177 /* System.Boolean System.Type::Equals(System.Type) */, L_17, L_18);
		G_B11_0 = ((int32_t)(L_19));
		goto IL_005a;
	}

IL_0056:
	{
		G_B11_0 = 0;
		goto IL_005a;
	}

IL_0059:
	{
		G_B11_0 = 1;
	}

IL_005a:
	{
		V_0 = (bool)G_B11_0;
		bool L_20 = V_0;
		if (!L_20)
		{
			goto IL_0065;
		}
	}
	{
		Type_t * L_21 = ___candidateType0;
		NullCheck(L_21);
		TypeU5BU5D_t3940880105* L_22 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_21);
		return L_22;
	}

IL_0065:
	{
		Type_t * L_23 = ___candidateType0;
		NullCheck(L_23);
		TypeU5BU5D_t3940880105* L_24 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(79 /* System.Type[] System.Type::GetInterfaces() */, L_23);
		V_2 = L_24;
		V_3 = 0;
		goto IL_0090;
	}

IL_0070:
	{
		TypeU5BU5D_t3940880105* L_25 = V_2;
		int32_t L_26 = V_3;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		Type_t * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_4 = L_28;
		Type_t * L_29 = V_4;
		NullCheck(L_29);
		bool L_30 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_29);
		if (!L_30)
		{
			goto IL_008c;
		}
	}
	{
		Type_t * L_31 = V_4;
		Type_t * L_32 = ___openGenericInterfaceType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_33 = TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		TypeU5BU5D_t3940880105* L_34 = L_33;
		V_1 = L_34;
		if (!L_34)
		{
			goto IL_008c;
		}
	}
	{
		TypeU5BU5D_t3940880105* L_35 = V_1;
		return L_35;
	}

IL_008c:
	{
		int32_t L_36 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1));
	}

IL_0090:
	{
		int32_t L_37 = V_3;
		TypeU5BU5D_t3940880105* L_38 = V_2;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_38)->max_length)))))))
		{
			goto IL_0070;
		}
	}
	{
		return (TypeU5BU5D_t3940880105*)NULL;
	}
}
// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Serialization.Utilities.Operator)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * TypeExtensions_GetOperatorMethod_m555772733 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, int32_t ___op1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetOperatorMethod_m555772733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___op1;
		switch (L_0)
		{
			case 0:
			{
				goto IL_005b;
			}
			case 1:
			{
				goto IL_0066;
			}
			case 2:
			{
				goto IL_0071;
			}
			case 3:
			{
				goto IL_007c;
			}
			case 4:
			{
				goto IL_0084;
			}
			case 5:
			{
				goto IL_008c;
			}
			case 6:
			{
				goto IL_0094;
			}
			case 7:
			{
				goto IL_009c;
			}
			case 8:
			{
				goto IL_00a4;
			}
			case 9:
			{
				goto IL_00ac;
			}
			case 10:
			{
				goto IL_00b4;
			}
			case 11:
			{
				goto IL_00bc;
			}
			case 12:
			{
				goto IL_00c4;
			}
			case 13:
			{
				goto IL_00cc;
			}
			case 14:
			{
				goto IL_00d4;
			}
			case 15:
			{
				goto IL_00dc;
			}
			case 16:
			{
				goto IL_00e4;
			}
			case 17:
			{
				goto IL_00f4;
			}
			case 18:
			{
				goto IL_00f4;
			}
			case 19:
			{
				goto IL_00ec;
			}
		}
	}
	{
		goto IL_00f6;
	}

IL_005b:
	{
		V_0 = _stringLiteral3482834024;
		goto IL_00fc;
	}

IL_0066:
	{
		V_0 = _stringLiteral2993467114;
		goto IL_00fc;
	}

IL_0071:
	{
		V_0 = _stringLiteral964622056;
		goto IL_00fc;
	}

IL_007c:
	{
		V_0 = _stringLiteral3897952179;
		goto IL_00fc;
	}

IL_0084:
	{
		V_0 = _stringLiteral4198891390;
		goto IL_00fc;
	}

IL_008c:
	{
		V_0 = _stringLiteral946927187;
		goto IL_00fc;
	}

IL_0094:
	{
		V_0 = _stringLiteral2234161082;
		goto IL_00fc;
	}

IL_009c:
	{
		V_0 = _stringLiteral3119448475;
		goto IL_00fc;
	}

IL_00a4:
	{
		V_0 = _stringLiteral2038133541;
		goto IL_00fc;
	}

IL_00ac:
	{
		V_0 = _stringLiteral1065866263;
		goto IL_00fc;
	}

IL_00b4:
	{
		V_0 = _stringLiteral152203953;
		goto IL_00fc;
	}

IL_00bc:
	{
		V_0 = _stringLiteral1600105539;
		goto IL_00fc;
	}

IL_00c4:
	{
		V_0 = _stringLiteral967860007;
		goto IL_00fc;
	}

IL_00cc:
	{
		V_0 = _stringLiteral1357986273;
		goto IL_00fc;
	}

IL_00d4:
	{
		V_0 = _stringLiteral2260015710;
		goto IL_00fc;
	}

IL_00dc:
	{
		V_0 = _stringLiteral1905736822;
		goto IL_00fc;
	}

IL_00e4:
	{
		V_0 = _stringLiteral1818253423;
		goto IL_00fc;
	}

IL_00ec:
	{
		V_0 = _stringLiteral1958770049;
		goto IL_00fc;
	}

IL_00f4:
	{
		return (MethodInfo_t *)NULL;
	}

IL_00f6:
	{
		NotImplementedException_t3489357830 * L_1 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_GetOperatorMethod_m555772733_RuntimeMethod_var);
	}

IL_00fc:
	{
		Type_t * L_2 = ___type0;
		String_t* L_3 = V_0;
		NullCheck(L_2);
		MethodInfo_t * L_4 = Type_GetMethod_m1197120913(L_2, L_3, ((int32_t)56), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Reflection.MethodInfo[] Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethods(System.Type,Sirenix.Serialization.Utilities.Operator)
extern "C" IL2CPP_METHOD_ATTR MethodInfoU5BU5D_t2572182361* TypeExtensions_GetOperatorMethods_m2090576811 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, int32_t ___op1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetOperatorMethods_m2090576811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass41_0_t2941671758 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_0 = (U3CU3Ec__DisplayClass41_0_t2941671758 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass41_0_t2941671758_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass41_0__ctor_m3483633306(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___op1;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0061;
			}
			case 1:
			{
				goto IL_0071;
			}
			case 2:
			{
				goto IL_0081;
			}
			case 3:
			{
				goto IL_0091;
			}
			case 4:
			{
				goto IL_00a1;
			}
			case 5:
			{
				goto IL_00b1;
			}
			case 6:
			{
				goto IL_00c1;
			}
			case 7:
			{
				goto IL_00d1;
			}
			case 8:
			{
				goto IL_00e1;
			}
			case 9:
			{
				goto IL_00ee;
			}
			case 10:
			{
				goto IL_00fb;
			}
			case 11:
			{
				goto IL_0108;
			}
			case 12:
			{
				goto IL_0115;
			}
			case 13:
			{
				goto IL_0122;
			}
			case 14:
			{
				goto IL_012f;
			}
			case 15:
			{
				goto IL_013c;
			}
			case 16:
			{
				goto IL_0149;
			}
			case 17:
			{
				goto IL_0163;
			}
			case 18:
			{
				goto IL_0163;
			}
			case 19:
			{
				goto IL_0156;
			}
		}
	}
	{
		goto IL_0165;
	}

IL_0061:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_methodName_0(_stringLiteral3482834024);
		goto IL_016b;
	}

IL_0071:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_methodName_0(_stringLiteral2993467114);
		goto IL_016b;
	}

IL_0081:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_methodName_0(_stringLiteral964622056);
		goto IL_016b;
	}

IL_0091:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_methodName_0(_stringLiteral3897952179);
		goto IL_016b;
	}

IL_00a1:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_6 = V_0;
		NullCheck(L_6);
		L_6->set_methodName_0(_stringLiteral4198891390);
		goto IL_016b;
	}

IL_00b1:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_methodName_0(_stringLiteral946927187);
		goto IL_016b;
	}

IL_00c1:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_8 = V_0;
		NullCheck(L_8);
		L_8->set_methodName_0(_stringLiteral2234161082);
		goto IL_016b;
	}

IL_00d1:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_methodName_0(_stringLiteral3119448475);
		goto IL_016b;
	}

IL_00e1:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_10 = V_0;
		NullCheck(L_10);
		L_10->set_methodName_0(_stringLiteral2038133541);
		goto IL_016b;
	}

IL_00ee:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_11 = V_0;
		NullCheck(L_11);
		L_11->set_methodName_0(_stringLiteral1065866263);
		goto IL_016b;
	}

IL_00fb:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_12 = V_0;
		NullCheck(L_12);
		L_12->set_methodName_0(_stringLiteral152203953);
		goto IL_016b;
	}

IL_0108:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_13 = V_0;
		NullCheck(L_13);
		L_13->set_methodName_0(_stringLiteral1600105539);
		goto IL_016b;
	}

IL_0115:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_14 = V_0;
		NullCheck(L_14);
		L_14->set_methodName_0(_stringLiteral967860007);
		goto IL_016b;
	}

IL_0122:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_15 = V_0;
		NullCheck(L_15);
		L_15->set_methodName_0(_stringLiteral1357986273);
		goto IL_016b;
	}

IL_012f:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_16 = V_0;
		NullCheck(L_16);
		L_16->set_methodName_0(_stringLiteral2260015710);
		goto IL_016b;
	}

IL_013c:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_17 = V_0;
		NullCheck(L_17);
		L_17->set_methodName_0(_stringLiteral1905736822);
		goto IL_016b;
	}

IL_0149:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_18 = V_0;
		NullCheck(L_18);
		L_18->set_methodName_0(_stringLiteral1818253423);
		goto IL_016b;
	}

IL_0156:
	{
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_19 = V_0;
		NullCheck(L_19);
		L_19->set_methodName_0(_stringLiteral1958770049);
		goto IL_016b;
	}

IL_0163:
	{
		return (MethodInfoU5BU5D_t2572182361*)NULL;
	}

IL_0165:
	{
		NotImplementedException_t3489357830 * L_20 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, NULL, TypeExtensions_GetOperatorMethods_m2090576811_RuntimeMethod_var);
	}

IL_016b:
	{
		Type_t * L_21 = ___type0;
		NullCheck(L_21);
		MethodInfoU5BU5D_t2572182361* L_22 = VirtFuncInvoker1< MethodInfoU5BU5D_t2572182361*, int32_t >::Invoke(72 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_21, ((int32_t)56));
		U3CU3Ec__DisplayClass41_0_t2941671758 * L_23 = V_0;
		intptr_t L_24 = (intptr_t)U3CU3Ec__DisplayClass41_0_U3CGetOperatorMethodsU3Eb__0_m1754509797_RuntimeMethod_var;
		Func_2_t3487522507 * L_25 = (Func_2_t3487522507 *)il2cpp_codegen_object_new(Func_2_t3487522507_il2cpp_TypeInfo_var);
		Func_2__ctor_m576450289(L_25, L_23, (intptr_t)L_24, /*hidden argument*/Func_2__ctor_m576450289_RuntimeMethod_var);
		RuntimeObject* L_26 = Enumerable_Where_TisMethodInfo_t_m1712952118(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_22, L_25, /*hidden argument*/Enumerable_Where_TisMethodInfo_t_m1712952118_RuntimeMethod_var);
		MethodInfoU5BU5D_t2572182361* L_27 = Enumerable_ToArray_TisMethodInfo_t_m1997060780(NULL /*static, unused*/, L_26, /*hidden argument*/Enumerable_ToArray_TisMethodInfo_t_m1997060780_RuntimeMethod_var);
		return L_27;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TypeExtensions_GetAllMembers_m569881445 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, int32_t ___flags1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetAllMembers_m569881445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CGetAllMembersU3Ed__42_t2156530709 * L_0 = (U3CGetAllMembersU3Ed__42_t2156530709 *)il2cpp_codegen_object_new(U3CGetAllMembersU3Ed__42_t2156530709_il2cpp_TypeInfo_var);
		U3CGetAllMembersU3Ed__42__ctor_m464840207(L_0, ((int32_t)-2), /*hidden argument*/NULL);
		U3CGetAllMembersU3Ed__42_t2156530709 * L_1 = L_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_U3CU3E3__type_4(L_2);
		U3CGetAllMembersU3Ed__42_t2156530709 * L_3 = L_1;
		int32_t L_4 = ___flags1;
		NullCheck(L_3);
		L_3->set_U3CU3E3__flags_6(L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.String,System.Reflection.BindingFlags)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TypeExtensions_GetAllMembers_m2556387623 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, String_t* ___name1, int32_t ___flags2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetAllMembers_m2556387623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CGetAllMembersU3Ed__43_t590446768 * L_0 = (U3CGetAllMembersU3Ed__43_t590446768 *)il2cpp_codegen_object_new(U3CGetAllMembersU3Ed__43_t590446768_il2cpp_TypeInfo_var);
		U3CGetAllMembersU3Ed__43__ctor_m424109788(L_0, ((int32_t)-2), /*hidden argument*/NULL);
		U3CGetAllMembersU3Ed__43_t590446768 * L_1 = L_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_U3CU3E3__type_4(L_2);
		U3CGetAllMembersU3Ed__43_t590446768 * L_3 = L_1;
		String_t* L_4 = ___name1;
		NullCheck(L_3);
		L_3->set_U3CU3E3__name_8(L_4);
		U3CGetAllMembersU3Ed__43_t590446768 * L_5 = L_3;
		int32_t L_6 = ___flags2;
		NullCheck(L_5);
		L_5->set_U3CU3E3__flags_6(L_6);
		return L_5;
	}
}
// System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR Type_t * TypeExtensions_GetGenericBaseType_m3164880977 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetGenericBaseType_m3164880977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Type_t * L_0 = ___type0;
		Type_t * L_1 = ___baseType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Type_t * L_2 = TypeExtensions_GetGenericBaseType_m225141450(NULL /*static, unused*/, L_0, L_1, (int32_t*)(&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR Type_t * TypeExtensions_GetGenericBaseType_m225141450 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseType1, int32_t* ___depthCount2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetGenericBaseType_m225141450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3243520166, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_GetGenericBaseType_m225141450_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___baseType1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral1021063921, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_GetGenericBaseType_m225141450_RuntimeMethod_var);
	}

IL_001c:
	{
		Type_t * L_4 = ___baseType1;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_4);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_6 = ___baseType1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_6);
		String_t* L_8 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2099965756, L_7, _stringLiteral959604969, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_9 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, NULL, TypeExtensions_GetGenericBaseType_m225141450_RuntimeMethod_var);
	}

IL_003f:
	{
		Type_t * L_10 = ___type0;
		Type_t * L_11 = ___baseType1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_12 = TypeExtensions_InheritsFrom_m434511460(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0083;
		}
	}
	{
		StringU5BU5D_t1281789340* L_13 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral2099965756);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2099965756);
		StringU5BU5D_t1281789340* L_15 = L_14;
		Type_t * L_16 = ___type0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_16);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_17);
		StringU5BU5D_t1281789340* L_18 = L_15;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral3659295074);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3659295074);
		StringU5BU5D_t1281789340* L_19 = L_18;
		Type_t * L_20 = ___baseType1;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_20);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_21);
		StringU5BU5D_t1281789340* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral3452614530);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614530);
		String_t* L_23 = String_Concat_m1809518182(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_24 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_24, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24, NULL, TypeExtensions_GetGenericBaseType_m225141450_RuntimeMethod_var);
	}

IL_0083:
	{
		Type_t * L_25 = ___type0;
		V_0 = L_25;
		int32_t* L_26 = ___depthCount2;
		*((int32_t*)L_26) = (int32_t)0;
		goto IL_0097;
	}

IL_008a:
	{
		int32_t* L_27 = ___depthCount2;
		int32_t* L_28 = ___depthCount2;
		int32_t L_29 = *((int32_t*)L_28);
		*((int32_t*)L_27) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
		Type_t * L_30 = V_0;
		NullCheck(L_30);
		Type_t * L_31 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_30);
		V_0 = L_31;
	}

IL_0097:
	{
		Type_t * L_32 = V_0;
		if (!L_32)
		{
			goto IL_00ab;
		}
	}
	{
		Type_t * L_33 = V_0;
		NullCheck(L_33);
		bool L_34 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_33);
		if (!L_34)
		{
			goto IL_008a;
		}
	}
	{
		Type_t * L_35 = V_0;
		NullCheck(L_35);
		Type_t * L_36 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_35);
		Type_t * L_37 = ___baseType1;
		if ((!(((RuntimeObject*)(Type_t *)L_36) == ((RuntimeObject*)(Type_t *)L_37))))
		{
			goto IL_008a;
		}
	}

IL_00ab:
	{
		Type_t * L_38 = V_0;
		if (L_38)
		{
			goto IL_00cf;
		}
	}
	{
		Type_t * L_39 = ___type0;
		NullCheck(L_39);
		String_t* L_40 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_39);
		Type_t * L_41 = ___baseType1;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_41);
		String_t* L_43 = String_Concat_m2163913788(NULL /*static, unused*/, L_40, _stringLiteral2554050195, L_42, _stringLiteral1636322628, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_44 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_44, L_43, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44, NULL, TypeExtensions_GetGenericBaseType_m225141450_RuntimeMethod_var);
	}

IL_00cf:
	{
		Type_t * L_45 = V_0;
		return L_45;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseTypes(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TypeExtensions_GetBaseTypes_m1398060321 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, bool ___includeSelf1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetBaseTypes_m1398060321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		bool L_1 = ___includeSelf1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = TypeExtensions_GetBaseClasses_m302166337(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		TypeU5BU5D_t3940880105* L_4 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(79 /* System.Type[] System.Type::GetInterfaces() */, L_3);
		RuntimeObject* L_5 = Enumerable_Concat_TisType_t_m3921564742(NULL /*static, unused*/, L_2, (RuntimeObject*)(RuntimeObject*)L_4, /*hidden argument*/Enumerable_Concat_TisType_t_m3921564742_RuntimeMethod_var);
		V_0 = L_5;
		bool L_6 = ___includeSelf1;
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		Type_t * L_7 = ___type0;
		NullCheck(L_7);
		bool L_8 = Type_get_IsInterface_m3284996719(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject* L_9 = V_0;
		TypeU5BU5D_t3940880105* L_10 = (TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t3940880105* L_11 = L_10;
		Type_t * L_12 = ___type0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		Enumerable_Concat_TisType_t_m3921564742(NULL /*static, unused*/, L_9, (RuntimeObject*)(RuntimeObject*)L_11, /*hidden argument*/Enumerable_Concat_TisType_t_m3921564742_RuntimeMethod_var);
	}

IL_002f:
	{
		RuntimeObject* L_13 = V_0;
		return L_13;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseClasses(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TypeExtensions_GetBaseClasses_m302166337 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, bool ___includeSelf1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetBaseClasses_m302166337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_0 = (U3CGetBaseClassesU3Ed__48_t3606501197 *)il2cpp_codegen_object_new(U3CGetBaseClassesU3Ed__48_t3606501197_il2cpp_TypeInfo_var);
		U3CGetBaseClassesU3Ed__48__ctor_m3292792360(L_0, ((int32_t)-2), /*hidden argument*/NULL);
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_1 = L_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_U3CU3E3__type_4(L_2);
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_3 = L_1;
		bool L_4 = ___includeSelf1;
		NullCheck(L_3);
		L_3->set_U3CU3E3__includeSelf_6(L_4);
		return L_3;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::TypeNameGauntlet(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_TypeNameGauntlet_m2028448992 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_TypeNameGauntlet_m2028448992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		V_0 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Dictionary_2_t1632706988 * L_3 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_TypeNameAlternatives_9();
		String_t* L_4 = V_0;
		NullCheck(L_3);
		bool L_5 = Dictionary_2_TryGetValue_m2620390247(L_3, L_4, (String_t**)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m2620390247_RuntimeMethod_var);
		if (!L_5)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_6 = V_1;
		V_0 = L_6;
	}

IL_001e:
	{
		String_t* L_7 = V_0;
		return L_7;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetNiceName_m2140991116 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetNiceName_m2140991116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsNested_m3546087448(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_2);
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_6 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Type_t * L_7 = ___type0;
		String_t* L_8 = TypeExtensions_GetCachedNiceName_m3030179020(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m3755062657(NULL /*static, unused*/, L_6, _stringLiteral3452614530, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_002c:
	{
		Type_t * L_10 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_11 = TypeExtensions_GetCachedNiceName_m3030179020(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceFullName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetNiceFullName_m2504614971 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetNiceFullName_m2504614971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsNested_m3546087448(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_2);
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(15 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_6 = TypeExtensions_GetNiceFullName_m2504614971(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Type_t * L_7 = ___type0;
		String_t* L_8 = TypeExtensions_GetCachedNiceName_m3030179020(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m3755062657(NULL /*static, unused*/, L_6, _stringLiteral3452614530, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_002c:
	{
		Type_t * L_10 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_11 = TypeExtensions_GetCachedNiceName_m3030179020(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		Type_t * L_12 = ___type0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(55 /* System.String System.Type::get_Namespace() */, L_12);
		if (!L_13)
		{
			goto IL_004d;
		}
	}
	{
		Type_t * L_14 = ___type0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(55 /* System.String System.Type::get_Namespace() */, L_14);
		String_t* L_16 = V_0;
		String_t* L_17 = String_Concat_m3755062657(NULL /*static, unused*/, L_15, _stringLiteral3452614530, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_004d:
	{
		String_t* L_18 = V_0;
		return L_18;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCompilableNiceName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetCompilableNiceName_m3309848918 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCompilableNiceName_m3309848918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_1 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = String_Replace_m3726209165(L_1, ((int32_t)60), ((int32_t)95), /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m3726209165(L_2, ((int32_t)62), ((int32_t)95), /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_4 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_5 = L_4;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)95));
		NullCheck(L_3);
		String_t* L_6 = String_TrimEnd_m3824727301(L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCompilableNiceFullName(System.Type)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetCompilableNiceFullName_m79608041 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetCompilableNiceFullName_m79608041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_1 = TypeExtensions_GetNiceFullName_m2504614971(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = String_Replace_m3726209165(L_1, ((int32_t)60), ((int32_t)95), /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m3726209165(L_2, ((int32_t)62), ((int32_t)95), /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_4 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_5 = L_4;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)95));
		NullCheck(L_3);
		String_t* L_6 = String_TrimEnd_m3824727301(L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::InheritsFrom(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_InheritsFrom_m434511460 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_InheritsFrom_m434511460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = ___baseType1;
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsInterface_m3284996719(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		Type_t * L_5 = ___baseType1;
		NullCheck(L_5);
		bool L_6 = Type_get_IsInterface_m3284996719(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Type_t * L_7 = ___baseType1;
		NullCheck(L_7);
		bool L_8 = Type_get_IsInterface_m3284996719(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_9 = ___type0;
		NullCheck(L_9);
		TypeU5BU5D_t3940880105* L_10 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(79 /* System.Type[] System.Type::GetInterfaces() */, L_9);
		Type_t * L_11 = ___baseType1;
		bool L_12 = Enumerable_Contains_TisType_t_m2042470555(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_10, L_11, /*hidden argument*/Enumerable_Contains_TisType_t_m2042470555_RuntimeMethod_var);
		return L_12;
	}

IL_0032:
	{
		Type_t * L_13 = ___type0;
		V_0 = L_13;
		goto IL_005e;
	}

IL_0036:
	{
		Type_t * L_14 = V_0;
		Type_t * L_15 = ___baseType1;
		if ((!(((RuntimeObject*)(Type_t *)L_14) == ((RuntimeObject*)(Type_t *)L_15))))
		{
			goto IL_003c;
		}
	}
	{
		return (bool)1;
	}

IL_003c:
	{
		Type_t * L_16 = ___baseType1;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_16);
		if (!L_17)
		{
			goto IL_0057;
		}
	}
	{
		Type_t * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_18);
		if (!L_19)
		{
			goto IL_0057;
		}
	}
	{
		Type_t * L_20 = V_0;
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_20);
		Type_t * L_22 = ___baseType1;
		if ((!(((RuntimeObject*)(Type_t *)L_21) == ((RuntimeObject*)(Type_t *)L_22))))
		{
			goto IL_0057;
		}
	}
	{
		return (bool)1;
	}

IL_0057:
	{
		Type_t * L_23 = V_0;
		NullCheck(L_23);
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_23);
		V_0 = L_24;
	}

IL_005e:
	{
		Type_t * L_25 = V_0;
		if (L_25)
		{
			goto IL_0036;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions::GetInheritanceDistance(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR int32_t TypeExtensions_GetInheritanceDistance_m2457922913 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetInheritanceDistance_m2457922913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	int32_t V_3 = 0;
	TypeU5BU5D_t3940880105* V_4 = NULL;
	int32_t V_5 = 0;
	{
		Type_t * L_0 = ___type0;
		Type_t * L_1 = ___baseType1;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		Type_t * L_3 = ___type0;
		V_1 = L_3;
		Type_t * L_4 = ___baseType1;
		V_0 = L_4;
		goto IL_0059;
	}

IL_000f:
	{
		Type_t * L_5 = ___baseType1;
		Type_t * L_6 = ___type0;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_001e;
		}
	}
	{
		Type_t * L_8 = ___baseType1;
		V_1 = L_8;
		Type_t * L_9 = ___type0;
		V_0 = L_9;
		goto IL_0059;
	}

IL_001e:
	{
		StringU5BU5D_t1281789340* L_10 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral3963528374);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3963528374);
		StringU5BU5D_t1281789340* L_12 = L_11;
		Type_t * L_13 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_14 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_14);
		StringU5BU5D_t1281789340* L_15 = L_12;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral1807570937);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1807570937);
		StringU5BU5D_t1281789340* L_16 = L_15;
		Type_t * L_17 = ___baseType1;
		String_t* L_18 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_18);
		StringU5BU5D_t1281789340* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3451906199);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3451906199);
		String_t* L_20 = String_Concat_m1809518182(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, NULL, TypeExtensions_GetInheritanceDistance_m2457922913_RuntimeMethod_var);
	}

IL_0059:
	{
		Type_t * L_22 = V_0;
		V_2 = L_22;
		V_3 = 0;
		Type_t * L_23 = V_1;
		NullCheck(L_23);
		bool L_24 = Type_get_IsInterface_m3284996719(L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_0099;
	}

IL_0067:
	{
		int32_t L_25 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
		Type_t * L_26 = V_2;
		NullCheck(L_26);
		Type_t * L_27 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_26);
		V_2 = L_27;
		Type_t * L_28 = V_2;
		NullCheck(L_28);
		TypeU5BU5D_t3940880105* L_29 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(79 /* System.Type[] System.Type::GetInterfaces() */, L_28);
		V_4 = L_29;
		V_5 = 0;
		goto IL_0091;
	}

IL_007f:
	{
		TypeU5BU5D_t3940880105* L_30 = V_4;
		int32_t L_31 = V_5;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		Type_t * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		Type_t * L_34 = V_1;
		if ((!(((RuntimeObject*)(Type_t *)L_33) == ((RuntimeObject*)(Type_t *)L_34))))
		{
			goto IL_008b;
		}
	}
	{
		V_2 = (Type_t *)NULL;
		goto IL_0099;
	}

IL_008b:
	{
		int32_t L_35 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_0091:
	{
		int32_t L_36 = V_5;
		TypeU5BU5D_t3940880105* L_37 = V_4;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_37)->max_length)))))))
		{
			goto IL_007f;
		}
	}

IL_0099:
	{
		Type_t * L_38 = V_2;
		if (!L_38)
		{
			goto IL_00ca;
		}
	}
	{
		Type_t * L_39 = V_2;
		RuntimeTypeHandle_t3027515415  L_40 = { reinterpret_cast<intptr_t> (RuntimeObject_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_39) == ((RuntimeObject*)(Type_t *)L_41))))
		{
			goto IL_0067;
		}
	}
	{
		goto IL_00ca;
	}

IL_00ab:
	{
		int32_t L_42 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
		Type_t * L_43 = V_2;
		NullCheck(L_43);
		Type_t * L_44 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_43);
		V_2 = L_44;
	}

IL_00b6:
	{
		Type_t * L_45 = V_2;
		Type_t * L_46 = V_1;
		if ((((RuntimeObject*)(Type_t *)L_45) == ((RuntimeObject*)(Type_t *)L_46)))
		{
			goto IL_00ca;
		}
	}
	{
		Type_t * L_47 = V_2;
		if (!L_47)
		{
			goto IL_00ca;
		}
	}
	{
		Type_t * L_48 = V_2;
		RuntimeTypeHandle_t3027515415  L_49 = { reinterpret_cast<intptr_t> (RuntimeObject_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_50 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_48) == ((RuntimeObject*)(Type_t *)L_50))))
		{
			goto IL_00ab;
		}
	}

IL_00ca:
	{
		int32_t L_51 = V_3;
		return L_51;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasParamaters(System.Reflection.MethodInfo,System.Collections.Generic.IList`1<System.Type>,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_HasParamaters_m257329311 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___methodInfo0, RuntimeObject* ___paramTypes1, bool ___inherit2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_HasParamaters_m257329311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParameterInfoU5BU5D_t390618515* V_0 = NULL;
	int32_t V_1 = 0;
	{
		MethodInfo_t * L_0 = ___methodInfo0;
		NullCheck(L_0);
		ParameterInfoU5BU5D_t390618515* L_1 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(43 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_0);
		V_0 = L_1;
		ParameterInfoU5BU5D_t390618515* L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3 = ___paramTypes1;
		NullCheck(L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Type>::get_Count() */, ICollection_1_t1017129698_il2cpp_TypeInfo_var, L_3);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))) == ((uint32_t)L_4))))
		{
			goto IL_0050;
		}
	}
	{
		V_1 = 0;
		goto IL_0048;
	}

IL_0016:
	{
		bool L_5 = ___inherit2;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		RuntimeObject* L_6 = ___paramTypes1;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		Type_t * L_8 = InterfaceFuncInvoker1< Type_t *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Type>::get_Item(System.Int32) */, IList_1_t4297247_il2cpp_TypeInfo_var, L_6, L_7);
		ParameterInfoU5BU5D_t390618515* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		ParameterInfo_t1861056598 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Type_t * L_13 = VirtFuncInvoker0< Type_t * >::Invoke(8 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_14 = TypeExtensions_InheritsFrom_m434511460(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0031:
	{
		ParameterInfoU5BU5D_t390618515* L_15 = V_0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		ParameterInfo_t1861056598 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(8 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_18);
		RuntimeObject* L_20 = ___paramTypes1;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		Type_t * L_22 = InterfaceFuncInvoker1< Type_t *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Type>::get_Item(System.Int32) */, IList_1_t4297247_il2cpp_TypeInfo_var, L_20, L_21);
		if ((((RuntimeObject*)(Type_t *)L_19) == ((RuntimeObject*)(Type_t *)L_22)))
		{
			goto IL_0044;
		}
	}
	{
		return (bool)0;
	}

IL_0044:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0048:
	{
		int32_t L_24 = V_1;
		ParameterInfoU5BU5D_t390618515* L_25 = V_0;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0050:
	{
		return (bool)0;
	}
}
// System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetReturnType(System.Reflection.MemberInfo)
extern "C" IL2CPP_METHOD_ATTR Type_t * TypeExtensions_GetReturnType_m1722435742 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetReturnType_m1722435742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FieldInfo_t * V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	MethodInfo_t * V_2 = NULL;
	EventInfo_t * V_3 = NULL;
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		V_0 = ((FieldInfo_t *)IsInstClass((RuntimeObject*)L_0, FieldInfo_t_il2cpp_TypeInfo_var));
		FieldInfo_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		FieldInfo_t * L_2 = V_0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(26 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_2);
		return L_3;
	}

IL_0011:
	{
		MemberInfo_t * L_4 = ___memberInfo0;
		V_1 = ((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_4, PropertyInfo_t_il2cpp_TypeInfo_var));
		PropertyInfo_t * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		PropertyInfo_t * L_6 = V_1;
		NullCheck(L_6);
		Type_t * L_7 = VirtFuncInvoker0< Type_t * >::Invoke(29 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_6);
		return L_7;
	}

IL_0022:
	{
		MemberInfo_t * L_8 = ___memberInfo0;
		V_2 = ((MethodInfo_t *)IsInstClass((RuntimeObject*)L_8, MethodInfo_t_il2cpp_TypeInfo_var));
		MethodInfo_t * L_9 = V_2;
		if (!L_9)
		{
			goto IL_0033;
		}
	}
	{
		MethodInfo_t * L_10 = V_2;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(65 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_10);
		return L_11;
	}

IL_0033:
	{
		MemberInfo_t * L_12 = ___memberInfo0;
		V_3 = ((EventInfo_t *)IsInstClass((RuntimeObject*)L_12, EventInfo_t_il2cpp_TypeInfo_var));
		EventInfo_t * L_13 = V_3;
		if (!L_13)
		{
			goto IL_0044;
		}
	}
	{
		EventInfo_t * L_14 = V_3;
		NullCheck(L_14);
		Type_t * L_15 = VirtFuncInvoker0< Type_t * >::Invoke(25 /* System.Type System.Reflection.EventInfo::get_EventHandlerType() */, L_14);
		return L_15;
	}

IL_0044:
	{
		return (Type_t *)NULL;
	}
}
// System.Object Sirenix.Serialization.Utilities.TypeExtensions::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * TypeExtensions_GetMemberValue_m2747957946 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, RuntimeObject * ___obj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetMemberValue_m2747957946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MemberInfo_t * L_0 = ___member0;
		if (!((FieldInfo_t *)IsInstClass((RuntimeObject*)L_0, FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		MemberInfo_t * L_1 = ___member0;
		RuntimeObject * L_2 = ___obj1;
		NullCheck(((FieldInfo_t *)IsInstClass((RuntimeObject*)L_1, FieldInfo_t_il2cpp_TypeInfo_var)));
		RuntimeObject * L_3 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(27 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, ((FieldInfo_t *)IsInstClass((RuntimeObject*)L_1, FieldInfo_t_il2cpp_TypeInfo_var)), L_2);
		return L_3;
	}

IL_0015:
	{
		MemberInfo_t * L_4 = ___member0;
		if (!((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_4, PropertyInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0031;
		}
	}
	{
		MemberInfo_t * L_5 = ___member0;
		NullCheck(((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_5, PropertyInfo_t_il2cpp_TypeInfo_var)));
		MethodInfo_t * L_6 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(31 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, ((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_5, PropertyInfo_t_il2cpp_TypeInfo_var)), (bool)1);
		RuntimeObject * L_7 = ___obj1;
		NullCheck(L_6);
		RuntimeObject * L_8 = MethodBase_Invoke_m1776411915(L_6, L_7, (ObjectU5BU5D_t2843939325*)(ObjectU5BU5D_t2843939325*)NULL, /*hidden argument*/NULL);
		return L_8;
	}

IL_0031:
	{
		MemberInfo_t * L_9 = ___member0;
		NullCheck(L_9);
		Type_t * L_10 = Object_GetType_m88164663(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_10);
		String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3095951133, L_11, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_13 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, NULL, TypeExtensions_GetMemberValue_m2747957946_RuntimeMethod_var);
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR void TypeExtensions_SetMemberValue_m278274216 (RuntimeObject * __this /* static, unused */, MemberInfo_t * ___member0, RuntimeObject * ___obj1, RuntimeObject * ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_SetMemberValue_m278274216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		MemberInfo_t * L_0 = ___member0;
		if (!((FieldInfo_t *)IsInstClass((RuntimeObject*)L_0, FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		MemberInfo_t * L_1 = ___member0;
		RuntimeObject * L_2 = ___obj1;
		RuntimeObject * L_3 = ___value2;
		NullCheck(((FieldInfo_t *)IsInstClass((RuntimeObject*)L_1, FieldInfo_t_il2cpp_TypeInfo_var)));
		FieldInfo_SetValue_m2460171138(((FieldInfo_t *)IsInstClass((RuntimeObject*)L_1, FieldInfo_t_il2cpp_TypeInfo_var)), L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		MemberInfo_t * L_4 = ___member0;
		if (!((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_4, PropertyInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_005c;
		}
	}
	{
		MemberInfo_t * L_5 = ___member0;
		NullCheck(((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_5, PropertyInfo_t_il2cpp_TypeInfo_var)));
		MethodInfo_t * L_6 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(33 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, ((PropertyInfo_t *)IsInstClass((RuntimeObject*)L_5, PropertyInfo_t_il2cpp_TypeInfo_var)), (bool)1);
		V_0 = L_6;
		MethodInfo_t * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		MethodInfo_t * L_8 = V_0;
		RuntimeObject * L_9 = ___obj1;
		ObjectU5BU5D_t2843939325* L_10 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_11 = L_10;
		RuntimeObject * L_12 = ___value2;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_12);
		NullCheck(L_8);
		MethodBase_Invoke_m1776411915(L_8, L_9, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0041:
	{
		MemberInfo_t * L_13 = ___member0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_13);
		String_t* L_15 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral432237896, L_14, _stringLiteral1264998793, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_16 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_16, L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, NULL, TypeExtensions_SetMemberValue_m278274216_RuntimeMethod_var);
	}

IL_005c:
	{
		MemberInfo_t * L_17 = ___member0;
		NullCheck(L_17);
		Type_t * L_18 = Object_GetType_m88164663(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_18);
		String_t* L_20 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3079309113, L_19, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_21 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, NULL, TypeExtensions_SetMemberValue_m278274216_RuntimeMethod_var);
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::TryInferGenericParameters(System.Type,System.Type[]&,System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_TryInferGenericParameters_m1077962252 (RuntimeObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, TypeU5BU5D_t3940880105** ___inferredParams1, TypeU5BU5D_t3940880105* ___knownParameters2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_TryInferGenericParameters_m1077962252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Dictionary_2_t633324528 * V_1 = NULL;
	TypeU5BU5D_t3940880105* V_2 = NULL;
	TypeU5BU5D_t3940880105* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	bool V_8 = false;
	TypeU5BU5D_t3940880105* V_9 = NULL;
	int32_t V_10 = 0;
	Type_t * V_11 = NULL;
	TypeU5BU5D_t3940880105* V_12 = NULL;
	TypeU5BU5D_t3940880105* V_13 = NULL;
	int32_t V_14 = 0;
	Type_t * V_15 = NULL;
	TypeU5BU5D_t3940880105* V_16 = NULL;
	int32_t V_17 = 0;
	Type_t * V_18 = NULL;
	Type_t * V_19 = NULL;
	TypeU5BU5D_t3940880105* V_20 = NULL;
	TypeU5BU5D_t3940880105* V_21 = NULL;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___genericTypeDefinition0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3106506693, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_TryInferGenericParameters_m1077962252_RuntimeMethod_var);
	}

IL_000e:
	{
		TypeU5BU5D_t3940880105* L_2 = ___knownParameters2;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral529403396, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_TryInferGenericParameters_m1077962252_RuntimeMethod_var);
	}

IL_001c:
	{
		Type_t * L_4 = ___genericTypeDefinition0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_4);
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		ArgumentException_t132251570 * L_6 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_6, _stringLiteral3566785798, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, TypeExtensions_TryInferGenericParameters_m1077962252_RuntimeMethod_var);
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		RuntimeObject * L_7 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfaction_LOCK_0();
		V_0 = L_7;
		RuntimeObject * L_8 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			Dictionary_2_t633324528 * L_9 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfactionInferredParameters_1();
			V_1 = L_9;
			Dictionary_2_t633324528 * L_10 = V_1;
			NullCheck(L_10);
			Dictionary_2_Clear_m2935595320(L_10, /*hidden argument*/Dictionary_2_Clear_m2935595320_RuntimeMethod_var);
			Type_t * L_11 = ___genericTypeDefinition0;
			NullCheck(L_11);
			TypeU5BU5D_t3940880105* L_12 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_11);
			V_2 = L_12;
			Type_t * L_13 = ___genericTypeDefinition0;
			NullCheck(L_13);
			bool L_14 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_13);
			if (L_14)
			{
				goto IL_00e8;
			}
		}

IL_0059:
		{
			TypeU5BU5D_t3940880105* L_15 = V_2;
			V_3 = L_15;
			Type_t * L_16 = ___genericTypeDefinition0;
			NullCheck(L_16);
			Type_t * L_17 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_16);
			___genericTypeDefinition0 = L_17;
			Type_t * L_18 = ___genericTypeDefinition0;
			NullCheck(L_18);
			TypeU5BU5D_t3940880105* L_19 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_18);
			V_2 = L_19;
			V_4 = 0;
			V_5 = 0;
			goto IL_0099;
		}

IL_0072:
		{
			TypeU5BU5D_t3940880105* L_20 = V_3;
			int32_t L_21 = V_5;
			NullCheck(L_20);
			int32_t L_22 = L_21;
			Type_t * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
			NullCheck(L_23);
			bool L_24 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_23);
			if (L_24)
			{
				goto IL_008d;
			}
		}

IL_007d:
		{
			Dictionary_2_t633324528 * L_25 = V_1;
			TypeU5BU5D_t3940880105* L_26 = V_2;
			int32_t L_27 = V_5;
			NullCheck(L_26);
			int32_t L_28 = L_27;
			Type_t * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
			TypeU5BU5D_t3940880105* L_30 = V_3;
			int32_t L_31 = V_5;
			NullCheck(L_30);
			int32_t L_32 = L_31;
			Type_t * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
			NullCheck(L_25);
			Dictionary_2_set_Item_m1327842169(L_25, L_29, L_33, /*hidden argument*/Dictionary_2_set_Item_m1327842169_RuntimeMethod_var);
			goto IL_0093;
		}

IL_008d:
		{
			int32_t L_34 = V_4;
			V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
		}

IL_0093:
		{
			int32_t L_35 = V_5;
			V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
		}

IL_0099:
		{
			int32_t L_36 = V_5;
			TypeU5BU5D_t3940880105* L_37 = V_3;
			NullCheck(L_37);
			if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_37)->max_length)))))))
			{
				goto IL_0072;
			}
		}

IL_00a0:
		{
			int32_t L_38 = V_4;
			TypeU5BU5D_t3940880105* L_39 = ___knownParameters2;
			NullCheck(L_39);
			if ((!(((uint32_t)L_38) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_39)->max_length))))))))
			{
				goto IL_00e8;
			}
		}

IL_00a7:
		{
			V_6 = 0;
			V_7 = 0;
			goto IL_00cd;
		}

IL_00af:
		{
			TypeU5BU5D_t3940880105* L_40 = V_3;
			int32_t L_41 = V_7;
			NullCheck(L_40);
			int32_t L_42 = L_41;
			Type_t * L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
			NullCheck(L_43);
			bool L_44 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_43);
			if (!L_44)
			{
				goto IL_00c7;
			}
		}

IL_00ba:
		{
			TypeU5BU5D_t3940880105* L_45 = V_3;
			int32_t L_46 = V_7;
			TypeU5BU5D_t3940880105* L_47 = ___knownParameters2;
			int32_t L_48 = V_6;
			int32_t L_49 = L_48;
			V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
			NullCheck(L_47);
			int32_t L_50 = L_49;
			Type_t * L_51 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
			NullCheck(L_45);
			ArrayElementTypeCheck (L_45, L_51);
			(L_45)->SetAt(static_cast<il2cpp_array_size_t>(L_46), (Type_t *)L_51);
		}

IL_00c7:
		{
			int32_t L_52 = V_7;
			V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
		}

IL_00cd:
		{
			int32_t L_53 = V_7;
			TypeU5BU5D_t3940880105* L_54 = V_3;
			NullCheck(L_54);
			if ((((int32_t)L_53) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_54)->max_length)))))))
			{
				goto IL_00af;
			}
		}

IL_00d4:
		{
			Type_t * L_55 = ___genericTypeDefinition0;
			TypeU5BU5D_t3940880105* L_56 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			bool L_57 = TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
			if (!L_57)
			{
				goto IL_00e8;
			}
		}

IL_00dd:
		{
			TypeU5BU5D_t3940880105** L_58 = ___inferredParams1;
			TypeU5BU5D_t3940880105* L_59 = V_3;
			*((RuntimeObject **)L_58) = (RuntimeObject *)L_59;
			Il2CppCodeGenWriteBarrier((RuntimeObject **)L_58, (RuntimeObject *)L_59);
			V_8 = (bool)1;
			IL2CPP_LEAVE(0x291, FINALLY_028a);
		}

IL_00e8:
		{
			TypeU5BU5D_t3940880105* L_60 = V_2;
			NullCheck(L_60);
			TypeU5BU5D_t3940880105* L_61 = ___knownParameters2;
			NullCheck(L_61);
			if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_60)->max_length))))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_61)->max_length))))))))
			{
				goto IL_0104;
			}
		}

IL_00f0:
		{
			Type_t * L_62 = ___genericTypeDefinition0;
			TypeU5BU5D_t3940880105* L_63 = ___knownParameters2;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			bool L_64 = TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
			if (!L_64)
			{
				goto IL_0104;
			}
		}

IL_00f9:
		{
			TypeU5BU5D_t3940880105** L_65 = ___inferredParams1;
			TypeU5BU5D_t3940880105* L_66 = ___knownParameters2;
			*((RuntimeObject **)L_65) = (RuntimeObject *)L_66;
			Il2CppCodeGenWriteBarrier((RuntimeObject **)L_65, (RuntimeObject *)L_66);
			V_8 = (bool)1;
			IL2CPP_LEAVE(0x291, FINALLY_028a);
		}

IL_0104:
		{
			TypeU5BU5D_t3940880105* L_67 = V_2;
			V_9 = L_67;
			V_10 = 0;
			goto IL_022f;
		}

IL_010f:
		{
			TypeU5BU5D_t3940880105* L_68 = V_9;
			int32_t L_69 = V_10;
			NullCheck(L_68);
			int32_t L_70 = L_69;
			Type_t * L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
			V_11 = L_71;
			Dictionary_2_t633324528 * L_72 = V_1;
			Type_t * L_73 = V_11;
			NullCheck(L_72);
			bool L_74 = Dictionary_2_ContainsKey_m2026130383(L_72, L_73, /*hidden argument*/Dictionary_2_ContainsKey_m2026130383_RuntimeMethod_var);
			if (L_74)
			{
				goto IL_0229;
			}
		}

IL_0123:
		{
			Type_t * L_75 = V_11;
			NullCheck(L_75);
			TypeU5BU5D_t3940880105* L_76 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(137 /* System.Type[] System.Type::GetGenericParameterConstraints() */, L_75);
			V_12 = L_76;
			TypeU5BU5D_t3940880105* L_77 = V_12;
			V_13 = L_77;
			V_14 = 0;
			goto IL_021e;
		}

IL_0138:
		{
			TypeU5BU5D_t3940880105* L_78 = V_13;
			int32_t L_79 = V_14;
			NullCheck(L_78);
			int32_t L_80 = L_79;
			Type_t * L_81 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
			V_15 = L_81;
			TypeU5BU5D_t3940880105* L_82 = ___knownParameters2;
			V_16 = L_82;
			V_17 = 0;
			goto IL_020d;
		}

IL_014a:
		{
			TypeU5BU5D_t3940880105* L_83 = V_16;
			int32_t L_84 = V_17;
			NullCheck(L_83);
			int32_t L_85 = L_84;
			Type_t * L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
			V_18 = L_86;
			Type_t * L_87 = V_15;
			NullCheck(L_87);
			bool L_88 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_87);
			if (!L_88)
			{
				goto IL_0207;
			}
		}

IL_015d:
		{
			Type_t * L_89 = V_15;
			NullCheck(L_89);
			Type_t * L_90 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_89);
			V_19 = L_90;
			Type_t * L_91 = V_15;
			NullCheck(L_91);
			TypeU5BU5D_t3940880105* L_92 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_91);
			V_20 = L_92;
			Type_t * L_93 = V_18;
			NullCheck(L_93);
			bool L_94 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_93);
			if (!L_94)
			{
				goto IL_018e;
			}
		}

IL_0178:
		{
			Type_t * L_95 = V_19;
			Type_t * L_96 = V_18;
			NullCheck(L_96);
			Type_t * L_97 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_96);
			if ((!(((RuntimeObject*)(Type_t *)L_95) == ((RuntimeObject*)(Type_t *)L_97))))
			{
				goto IL_018e;
			}
		}

IL_0183:
		{
			Type_t * L_98 = V_18;
			NullCheck(L_98);
			TypeU5BU5D_t3940880105* L_99 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_98);
			V_21 = L_99;
			goto IL_01ce;
		}

IL_018e:
		{
			Type_t * L_100 = V_19;
			NullCheck(L_100);
			bool L_101 = Type_get_IsInterface_m3284996719(L_100, /*hidden argument*/NULL);
			if (!L_101)
			{
				goto IL_01af;
			}
		}

IL_0197:
		{
			Type_t * L_102 = V_18;
			Type_t * L_103 = V_19;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			bool L_104 = TypeExtensions_ImplementsOpenGenericInterface_m3084334200(NULL /*static, unused*/, L_102, L_103, /*hidden argument*/NULL);
			if (!L_104)
			{
				goto IL_01af;
			}
		}

IL_01a2:
		{
			Type_t * L_105 = V_18;
			Type_t * L_106 = V_19;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			TypeU5BU5D_t3940880105* L_107 = TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118(NULL /*static, unused*/, L_105, L_106, /*hidden argument*/NULL);
			V_21 = L_107;
			goto IL_01ce;
		}

IL_01af:
		{
			Type_t * L_108 = V_19;
			NullCheck(L_108);
			bool L_109 = Type_get_IsClass_m589177581(L_108, /*hidden argument*/NULL);
			if (!L_109)
			{
				goto IL_0207;
			}
		}

IL_01b8:
		{
			Type_t * L_110 = V_18;
			Type_t * L_111 = V_19;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			bool L_112 = TypeExtensions_ImplementsOpenGenericClass_m2650584869(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
			if (!L_112)
			{
				goto IL_0207;
			}
		}

IL_01c3:
		{
			Type_t * L_113 = V_18;
			Type_t * L_114 = V_19;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			TypeU5BU5D_t3940880105* L_115 = TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627(NULL /*static, unused*/, L_113, L_114, /*hidden argument*/NULL);
			V_21 = L_115;
		}

IL_01ce:
		{
			Dictionary_2_t633324528 * L_116 = V_1;
			Type_t * L_117 = V_11;
			Type_t * L_118 = V_18;
			NullCheck(L_116);
			Dictionary_2_set_Item_m1327842169(L_116, L_117, L_118, /*hidden argument*/Dictionary_2_set_Item_m1327842169_RuntimeMethod_var);
			V_22 = 0;
			goto IL_01ff;
		}

IL_01dd:
		{
			TypeU5BU5D_t3940880105* L_119 = V_20;
			int32_t L_120 = V_22;
			NullCheck(L_119);
			int32_t L_121 = L_120;
			Type_t * L_122 = (L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_121));
			NullCheck(L_122);
			bool L_123 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_122);
			if (!L_123)
			{
				goto IL_01f9;
			}
		}

IL_01e9:
		{
			Dictionary_2_t633324528 * L_124 = V_1;
			TypeU5BU5D_t3940880105* L_125 = V_20;
			int32_t L_126 = V_22;
			NullCheck(L_125);
			int32_t L_127 = L_126;
			Type_t * L_128 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
			TypeU5BU5D_t3940880105* L_129 = V_21;
			int32_t L_130 = V_22;
			NullCheck(L_129);
			int32_t L_131 = L_130;
			Type_t * L_132 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
			NullCheck(L_124);
			Dictionary_2_set_Item_m1327842169(L_124, L_128, L_132, /*hidden argument*/Dictionary_2_set_Item_m1327842169_RuntimeMethod_var);
		}

IL_01f9:
		{
			int32_t L_133 = V_22;
			V_22 = ((int32_t)il2cpp_codegen_add((int32_t)L_133, (int32_t)1));
		}

IL_01ff:
		{
			int32_t L_134 = V_22;
			TypeU5BU5D_t3940880105* L_135 = V_20;
			NullCheck(L_135);
			if ((((int32_t)L_134) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_135)->max_length)))))))
			{
				goto IL_01dd;
			}
		}

IL_0207:
		{
			int32_t L_136 = V_17;
			V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_136, (int32_t)1));
		}

IL_020d:
		{
			int32_t L_137 = V_17;
			TypeU5BU5D_t3940880105* L_138 = V_16;
			NullCheck(L_138);
			if ((((int32_t)L_137) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_138)->max_length)))))))
			{
				goto IL_014a;
			}
		}

IL_0218:
		{
			int32_t L_139 = V_14;
			V_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_139, (int32_t)1));
		}

IL_021e:
		{
			int32_t L_140 = V_14;
			TypeU5BU5D_t3940880105* L_141 = V_13;
			NullCheck(L_141);
			if ((((int32_t)L_140) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_141)->max_length)))))))
			{
				goto IL_0138;
			}
		}

IL_0229:
		{
			int32_t L_142 = V_10;
			V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_142, (int32_t)1));
		}

IL_022f:
		{
			int32_t L_143 = V_10;
			TypeU5BU5D_t3940880105* L_144 = V_9;
			NullCheck(L_144);
			if ((((int32_t)L_143) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_144)->max_length)))))))
			{
				goto IL_010f;
			}
		}

IL_023a:
		{
			Dictionary_2_t633324528 * L_145 = V_1;
			NullCheck(L_145);
			int32_t L_146 = Dictionary_2_get_Count_m3815247294(L_145, /*hidden argument*/Dictionary_2_get_Count_m3815247294_RuntimeMethod_var);
			TypeU5BU5D_t3940880105* L_147 = V_2;
			NullCheck(L_147);
			if ((!(((uint32_t)L_146) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_147)->max_length))))))))
			{
				goto IL_0282;
			}
		}

IL_0245:
		{
			TypeU5BU5D_t3940880105** L_148 = ___inferredParams1;
			Dictionary_2_t633324528 * L_149 = V_1;
			NullCheck(L_149);
			int32_t L_150 = Dictionary_2_get_Count_m3815247294(L_149, /*hidden argument*/Dictionary_2_get_Count_m3815247294_RuntimeMethod_var);
			TypeU5BU5D_t3940880105* L_151 = (TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)L_150);
			*((RuntimeObject **)L_148) = (RuntimeObject *)L_151;
			Il2CppCodeGenWriteBarrier((RuntimeObject **)L_148, (RuntimeObject *)L_151);
			V_23 = 0;
			goto IL_026c;
		}

IL_0257:
		{
			TypeU5BU5D_t3940880105** L_152 = ___inferredParams1;
			TypeU5BU5D_t3940880105* L_153 = *((TypeU5BU5D_t3940880105**)L_152);
			int32_t L_154 = V_23;
			Dictionary_2_t633324528 * L_155 = V_1;
			TypeU5BU5D_t3940880105* L_156 = V_2;
			int32_t L_157 = V_23;
			NullCheck(L_156);
			int32_t L_158 = L_157;
			Type_t * L_159 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
			NullCheck(L_155);
			Type_t * L_160 = Dictionary_2_get_Item_m4273472624(L_155, L_159, /*hidden argument*/Dictionary_2_get_Item_m4273472624_RuntimeMethod_var);
			NullCheck(L_153);
			ArrayElementTypeCheck (L_153, L_160);
			(L_153)->SetAt(static_cast<il2cpp_array_size_t>(L_154), (Type_t *)L_160);
			int32_t L_161 = V_23;
			V_23 = ((int32_t)il2cpp_codegen_add((int32_t)L_161, (int32_t)1));
		}

IL_026c:
		{
			int32_t L_162 = V_23;
			TypeU5BU5D_t3940880105* L_163 = V_2;
			NullCheck(L_163);
			if ((((int32_t)L_162) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_163)->max_length)))))))
			{
				goto IL_0257;
			}
		}

IL_0273:
		{
			Type_t * L_164 = ___genericTypeDefinition0;
			TypeU5BU5D_t3940880105** L_165 = ___inferredParams1;
			TypeU5BU5D_t3940880105* L_166 = *((TypeU5BU5D_t3940880105**)L_165);
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			bool L_167 = TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143(NULL /*static, unused*/, L_164, L_166, /*hidden argument*/NULL);
			if (!L_167)
			{
				goto IL_0282;
			}
		}

IL_027d:
		{
			V_8 = (bool)1;
			IL2CPP_LEAVE(0x291, FINALLY_028a);
		}

IL_0282:
		{
			TypeU5BU5D_t3940880105** L_168 = ___inferredParams1;
			*((RuntimeObject **)L_168) = (RuntimeObject *)NULL;
			Il2CppCodeGenWriteBarrier((RuntimeObject **)L_168, (RuntimeObject *)NULL);
			V_8 = (bool)0;
			IL2CPP_LEAVE(0x291, FINALLY_028a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_028a;
	}

FINALLY_028a:
	{ // begin finally (depth: 1)
		RuntimeObject * L_169 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_169, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(650)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(650)
	{
		IL2CPP_JUMP_TBL(0x291, IL_0291)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0291:
	{
		bool L_170 = V_8;
		return L_170;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type,System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143 (RuntimeObject * __this /* static, unused */, Type_t * ___genericType0, TypeU5BU5D_t3940880105* ___parameters1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___genericType0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3697981828, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143_RuntimeMethod_var);
	}

IL_000e:
	{
		TypeU5BU5D_t3940880105* L_2 = ___parameters1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral3372390906, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143_RuntimeMethod_var);
	}

IL_001c:
	{
		Type_t * L_4 = ___genericType0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_4);
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		ArgumentException_t132251570 * L_6 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_6, _stringLiteral3566785798, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, TypeExtensions_AreGenericConstraintsSatisfiedBy_m660107143_RuntimeMethod_var);
	}

IL_002f:
	{
		Type_t * L_7 = ___genericType0;
		NullCheck(L_7);
		TypeU5BU5D_t3940880105* L_8 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_7);
		TypeU5BU5D_t3940880105* L_9 = ___parameters1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_10 = TypeExtensions_AreGenericConstraintsSatisfiedBy_m3989792819(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Reflection.MethodBase,System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274 (RuntimeObject * __this /* static, unused */, MethodBase_t * ___genericMethod0, TypeU5BU5D_t3940880105* ___parameters1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodBase_t * L_0 = ___genericMethod0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3665265380, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274_RuntimeMethod_var);
	}

IL_000e:
	{
		TypeU5BU5D_t3940880105* L_2 = ___parameters1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral3372390906, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274_RuntimeMethod_var);
	}

IL_001c:
	{
		MethodBase_t * L_4 = ___genericMethod0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(53 /* System.Boolean System.Reflection.MethodBase::get_IsGenericMethod() */, L_4);
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		ArgumentException_t132251570 * L_6 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_6, _stringLiteral3866462980, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, TypeExtensions_AreGenericConstraintsSatisfiedBy_m2224937274_RuntimeMethod_var);
	}

IL_002f:
	{
		MethodBase_t * L_7 = ___genericMethod0;
		NullCheck(L_7);
		TypeU5BU5D_t3940880105* L_8 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(50 /* System.Type[] System.Reflection.MethodBase::GetGenericArguments() */, L_7);
		TypeU5BU5D_t3940880105* L_9 = ___parameters1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_10 = TypeExtensions_AreGenericConstraintsSatisfiedBy_m3989792819(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type[],System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_AreGenericConstraintsSatisfiedBy_m3989792819 (RuntimeObject * __this /* static, unused */, TypeU5BU5D_t3940880105* ___definitions0, TypeU5BU5D_t3940880105* ___parameters1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_AreGenericConstraintsSatisfiedBy_m3989792819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Dictionary_2_t633324528 * V_1 = NULL;
	int32_t V_2 = 0;
	Type_t * V_3 = NULL;
	Type_t * V_4 = NULL;
	bool V_5 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		TypeU5BU5D_t3940880105* L_0 = ___definitions0;
		NullCheck(L_0);
		TypeU5BU5D_t3940880105* L_1 = ___parameters1;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))))
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		RuntimeObject * L_2 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfaction_LOCK_0();
		V_0 = L_2;
		RuntimeObject * L_3 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			Dictionary_2_t633324528 * L_4 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfactionResolvedMap_2();
			V_1 = L_4;
			Dictionary_2_t633324528 * L_5 = V_1;
			NullCheck(L_5);
			Dictionary_2_Clear_m2935595320(L_5, /*hidden argument*/Dictionary_2_Clear_m2935595320_RuntimeMethod_var);
			V_2 = 0;
			goto IL_0044;
		}

IL_0026:
		{
			TypeU5BU5D_t3940880105* L_6 = ___definitions0;
			int32_t L_7 = V_2;
			NullCheck(L_6);
			int32_t L_8 = L_7;
			Type_t * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
			V_3 = L_9;
			TypeU5BU5D_t3940880105* L_10 = ___parameters1;
			int32_t L_11 = V_2;
			NullCheck(L_10);
			int32_t L_12 = L_11;
			Type_t * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			V_4 = L_13;
			Type_t * L_14 = V_3;
			Type_t * L_15 = V_4;
			Dictionary_2_t633324528 * L_16 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			bool L_17 = TypeExtensions_GenericParameterIsFulfilledBy_m907762269(NULL /*static, unused*/, L_14, L_15, L_16, (HashSet_1_t1048894234 *)NULL, /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0040;
			}
		}

IL_003b:
		{
			V_5 = (bool)0;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}

IL_0040:
		{
			int32_t L_18 = V_2;
			V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
		}

IL_0044:
		{
			int32_t L_19 = V_2;
			TypeU5BU5D_t3940880105* L_20 = ___definitions0;
			NullCheck(L_20);
			if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
			{
				goto IL_0026;
			}
		}

IL_004a:
		{
			V_5 = (bool)1;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		RuntimeObject * L_21 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0056:
	{
		bool L_22 = V_5;
		return L_22;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_GenericParameterIsFulfilledBy_m678722123 (RuntimeObject * __this /* static, unused */, Type_t * ___genericParameterDefinition0, Type_t * ___parameterType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GenericParameterIsFulfilledBy_m678722123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfaction_LOCK_0();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		Dictionary_2_t633324528 * L_2 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfactionResolvedMap_2();
		NullCheck(L_2);
		Dictionary_2_Clear_m2935595320(L_2, /*hidden argument*/Dictionary_2_Clear_m2935595320_RuntimeMethod_var);
		Type_t * L_3 = ___genericParameterDefinition0;
		Type_t * L_4 = ___parameterType1;
		Dictionary_2_t633324528 * L_5 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfactionResolvedMap_2();
		bool L_6 = TypeExtensions_GenericParameterIsFulfilledBy_m907762269(NULL /*static, unused*/, L_3, L_4, L_5, (HashSet_1_t1048894234 *)NULL, /*hidden argument*/NULL);
		V_1 = L_6;
		IL2CPP_LEAVE(0x2D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		RuntimeObject * L_7 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x2D, IL_002d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_002d:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Type>,System.Collections.Generic.HashSet`1<System.Type>)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_GenericParameterIsFulfilledBy_m907762269 (RuntimeObject * __this /* static, unused */, Type_t * ___genericParameterDefinition0, Type_t * ___parameterType1, Dictionary_2_t633324528 * ___resolvedMap2, HashSet_1_t1048894234 * ___processedParams3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GenericParameterIsFulfilledBy_m907762269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TypeU5BU5D_t3940880105* V_1 = NULL;
	int32_t V_2 = 0;
	Type_t * V_3 = NULL;
	Type_t * V_4 = NULL;
	TypeU5BU5D_t3940880105* V_5 = NULL;
	TypeU5BU5D_t3940880105* V_6 = NULL;
	int32_t V_7 = 0;
	Type_t * V_8 = NULL;
	Type_t * V_9 = NULL;
	{
		Type_t * L_0 = ___genericParameterDefinition0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral401433094, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_GenericParameterIsFulfilledBy_m907762269_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___parameterType1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral3889371638, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, TypeExtensions_GenericParameterIsFulfilledBy_m907762269_RuntimeMethod_var);
	}

IL_001c:
	{
		Dictionary_2_t633324528 * L_4 = ___resolvedMap2;
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_5 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_5, _stringLiteral1594150229, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, TypeExtensions_GenericParameterIsFulfilledBy_m907762269_RuntimeMethod_var);
	}

IL_002a:
	{
		Type_t * L_6 = ___genericParameterDefinition0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_6);
		if (L_7)
		{
			goto IL_0038;
		}
	}
	{
		Type_t * L_8 = ___genericParameterDefinition0;
		Type_t * L_9 = ___parameterType1;
		if ((!(((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9))))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		Type_t * L_10 = ___genericParameterDefinition0;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_10);
		if (L_11)
		{
			goto IL_0042;
		}
	}
	{
		return (bool)0;
	}

IL_0042:
	{
		HashSet_1_t1048894234 * L_12 = ___processedParams3;
		if (L_12)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		HashSet_1_t1048894234 * L_13 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_GenericConstraintsSatisfactionProcessedParams_3();
		___processedParams3 = L_13;
		HashSet_1_t1048894234 * L_14 = ___processedParams3;
		NullCheck(L_14);
		HashSet_1_Clear_m1877719279(L_14, /*hidden argument*/HashSet_1_Clear_m1877719279_RuntimeMethod_var);
	}

IL_0052:
	{
		HashSet_1_t1048894234 * L_15 = ___processedParams3;
		Type_t * L_16 = ___genericParameterDefinition0;
		NullCheck(L_15);
		HashSet_1_Add_m3527721678(L_15, L_16, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		Type_t * L_17 = ___genericParameterDefinition0;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(105 /* System.Reflection.GenericParameterAttributes System.Type::get_GenericParameterAttributes() */, L_17);
		V_0 = L_18;
		int32_t L_19 = V_0;
		if (!L_19)
		{
			goto IL_00c5;
		}
	}
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_20&(int32_t)8))) == ((uint32_t)8))))
		{
			goto IL_008e;
		}
	}
	{
		Type_t * L_21 = ___parameterType1;
		NullCheck(L_21);
		bool L_22 = Type_get_IsValueType_m3108065642(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_008c;
		}
	}
	{
		Type_t * L_23 = ___parameterType1;
		NullCheck(L_23);
		bool L_24 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_23);
		if (!L_24)
		{
			goto IL_009e;
		}
	}
	{
		Type_t * L_25 = ___parameterType1;
		NullCheck(L_25);
		Type_t * L_26 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_25);
		RuntimeTypeHandle_t3027515415  L_27 = { reinterpret_cast<intptr_t> (Nullable_1_t3772285925_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_26) == ((RuntimeObject*)(Type_t *)L_28))))
		{
			goto IL_009e;
		}
	}

IL_008c:
	{
		return (bool)0;
	}

IL_008e:
	{
		int32_t L_29 = V_0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_29&(int32_t)4))) == ((uint32_t)4))))
		{
			goto IL_009e;
		}
	}
	{
		Type_t * L_30 = ___parameterType1;
		NullCheck(L_30);
		bool L_31 = Type_get_IsValueType_m3108065642(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_009e;
		}
	}
	{
		return (bool)0;
	}

IL_009e:
	{
		int32_t L_32 = V_0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_32&(int32_t)((int32_t)16)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_00c5;
		}
	}
	{
		Type_t * L_33 = ___parameterType1;
		NullCheck(L_33);
		bool L_34 = Type_get_IsAbstract_m1120089130(L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_00c3;
		}
	}
	{
		Type_t * L_35 = ___parameterType1;
		NullCheck(L_35);
		bool L_36 = Type_get_IsValueType_m3108065642(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_00c5;
		}
	}
	{
		Type_t * L_37 = ___parameterType1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_38 = ((Type_t_StaticFields*)il2cpp_codegen_static_fields_for(Type_t_il2cpp_TypeInfo_var))->get_EmptyTypes_5();
		NullCheck(L_37);
		ConstructorInfo_t5769829 * L_39 = Type_GetConstructor_m2219014380(L_37, L_38, /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_00c5;
		}
	}

IL_00c3:
	{
		return (bool)0;
	}

IL_00c5:
	{
		Dictionary_2_t633324528 * L_40 = ___resolvedMap2;
		Type_t * L_41 = ___genericParameterDefinition0;
		NullCheck(L_40);
		bool L_42 = Dictionary_2_ContainsKey_m2026130383(L_40, L_41, /*hidden argument*/Dictionary_2_ContainsKey_m2026130383_RuntimeMethod_var);
		if (!L_42)
		{
			goto IL_00df;
		}
	}
	{
		Type_t * L_43 = ___parameterType1;
		Dictionary_2_t633324528 * L_44 = ___resolvedMap2;
		Type_t * L_45 = ___genericParameterDefinition0;
		NullCheck(L_44);
		Type_t * L_46 = Dictionary_2_get_Item_m4273472624(L_44, L_45, /*hidden argument*/Dictionary_2_get_Item_m4273472624_RuntimeMethod_var);
		NullCheck(L_43);
		bool L_47 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_43, L_46);
		if (L_47)
		{
			goto IL_00df;
		}
	}
	{
		return (bool)0;
	}

IL_00df:
	{
		Type_t * L_48 = ___genericParameterDefinition0;
		NullCheck(L_48);
		TypeU5BU5D_t3940880105* L_49 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(137 /* System.Type[] System.Type::GetGenericParameterConstraints() */, L_48);
		V_1 = L_49;
		V_2 = 0;
		goto IL_0247;
	}

IL_00ed:
	{
		TypeU5BU5D_t3940880105* L_50 = V_1;
		int32_t L_51 = V_2;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		Type_t * L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		V_3 = L_53;
		Type_t * L_54 = V_3;
		NullCheck(L_54);
		bool L_55 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_54);
		if (!L_55)
		{
			goto IL_010a;
		}
	}
	{
		Dictionary_2_t633324528 * L_56 = ___resolvedMap2;
		Type_t * L_57 = V_3;
		NullCheck(L_56);
		bool L_58 = Dictionary_2_ContainsKey_m2026130383(L_56, L_57, /*hidden argument*/Dictionary_2_ContainsKey_m2026130383_RuntimeMethod_var);
		if (!L_58)
		{
			goto IL_010a;
		}
	}
	{
		Dictionary_2_t633324528 * L_59 = ___resolvedMap2;
		Type_t * L_60 = V_3;
		NullCheck(L_59);
		Type_t * L_61 = Dictionary_2_get_Item_m4273472624(L_59, L_60, /*hidden argument*/Dictionary_2_get_Item_m4273472624_RuntimeMethod_var);
		V_3 = L_61;
	}

IL_010a:
	{
		Type_t * L_62 = V_3;
		NullCheck(L_62);
		bool L_63 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_62);
		if (!L_63)
		{
			goto IL_0122;
		}
	}
	{
		Type_t * L_64 = V_3;
		Type_t * L_65 = ___parameterType1;
		Dictionary_2_t633324528 * L_66 = ___resolvedMap2;
		HashSet_1_t1048894234 * L_67 = ___processedParams3;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_68 = TypeExtensions_GenericParameterIsFulfilledBy_m907762269(NULL /*static, unused*/, L_64, L_65, L_66, L_67, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_0243;
		}
	}
	{
		return (bool)0;
	}

IL_0122:
	{
		Type_t * L_69 = V_3;
		NullCheck(L_69);
		bool L_70 = Type_get_IsClass_m589177581(L_69, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_71 = V_3;
		NullCheck(L_71);
		bool L_72 = Type_get_IsInterface_m3284996719(L_71, /*hidden argument*/NULL);
		if (L_72)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_73 = V_3;
		NullCheck(L_73);
		bool L_74 = Type_get_IsValueType_m3108065642(L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_022d;
		}
	}

IL_013d:
	{
		Type_t * L_75 = V_3;
		NullCheck(L_75);
		bool L_76 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_75);
		if (!L_76)
		{
			goto IL_0222;
		}
	}
	{
		Type_t * L_77 = V_3;
		NullCheck(L_77);
		Type_t * L_78 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_77);
		V_4 = L_78;
		Type_t * L_79 = V_3;
		NullCheck(L_79);
		TypeU5BU5D_t3940880105* L_80 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_79);
		V_5 = L_80;
		Type_t * L_81 = ___parameterType1;
		NullCheck(L_81);
		bool L_82 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_81);
		if (!L_82)
		{
			goto IL_0174;
		}
	}
	{
		Type_t * L_83 = V_4;
		Type_t * L_84 = ___parameterType1;
		NullCheck(L_84);
		Type_t * L_85 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_84);
		if ((!(((RuntimeObject*)(Type_t *)L_83) == ((RuntimeObject*)(Type_t *)L_85))))
		{
			goto IL_0174;
		}
	}
	{
		Type_t * L_86 = ___parameterType1;
		NullCheck(L_86);
		TypeU5BU5D_t3940880105* L_87 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_86);
		V_6 = L_87;
		goto IL_01ad;
	}

IL_0174:
	{
		Type_t * L_88 = V_4;
		NullCheck(L_88);
		bool L_89 = Type_get_IsClass_m589177581(L_88, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0195;
		}
	}
	{
		Type_t * L_90 = ___parameterType1;
		Type_t * L_91 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_92 = TypeExtensions_ImplementsOpenGenericClass_m2650584869(NULL /*static, unused*/, L_90, L_91, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_0193;
		}
	}
	{
		Type_t * L_93 = ___parameterType1;
		Type_t * L_94 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_95 = TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m1242983627(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
		V_6 = L_95;
		goto IL_01ad;
	}

IL_0193:
	{
		return (bool)0;
	}

IL_0195:
	{
		Type_t * L_96 = ___parameterType1;
		Type_t * L_97 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_98 = TypeExtensions_ImplementsOpenGenericInterface_m3084334200(NULL /*static, unused*/, L_96, L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_01ab;
		}
	}
	{
		Type_t * L_99 = ___parameterType1;
		Type_t * L_100 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3940880105* L_101 = TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m78921118(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		V_6 = L_101;
		goto IL_01ad;
	}

IL_01ab:
	{
		return (bool)0;
	}

IL_01ad:
	{
		V_7 = 0;
		goto IL_0218;
	}

IL_01b2:
	{
		TypeU5BU5D_t3940880105* L_102 = V_5;
		int32_t L_103 = V_7;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		Type_t * L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		V_8 = L_105;
		TypeU5BU5D_t3940880105* L_106 = V_6;
		int32_t L_107 = V_7;
		NullCheck(L_106);
		int32_t L_108 = L_107;
		Type_t * L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		V_9 = L_109;
		Type_t * L_110 = V_8;
		NullCheck(L_110);
		bool L_111 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_110);
		if (!L_111)
		{
			goto IL_01dd;
		}
	}
	{
		Dictionary_2_t633324528 * L_112 = ___resolvedMap2;
		Type_t * L_113 = V_8;
		NullCheck(L_112);
		bool L_114 = Dictionary_2_ContainsKey_m2026130383(L_112, L_113, /*hidden argument*/Dictionary_2_ContainsKey_m2026130383_RuntimeMethod_var);
		if (!L_114)
		{
			goto IL_01dd;
		}
	}
	{
		Dictionary_2_t633324528 * L_115 = ___resolvedMap2;
		Type_t * L_116 = V_8;
		NullCheck(L_115);
		Type_t * L_117 = Dictionary_2_get_Item_m4273472624(L_115, L_116, /*hidden argument*/Dictionary_2_get_Item_m4273472624_RuntimeMethod_var);
		V_8 = L_117;
	}

IL_01dd:
	{
		Type_t * L_118 = V_8;
		NullCheck(L_118);
		bool L_119 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_118);
		if (!L_119)
		{
			goto IL_01ff;
		}
	}
	{
		HashSet_1_t1048894234 * L_120 = ___processedParams3;
		Type_t * L_121 = V_8;
		NullCheck(L_120);
		bool L_122 = HashSet_1_Contains_m3794231197(L_120, L_121, /*hidden argument*/HashSet_1_Contains_m3794231197_RuntimeMethod_var);
		if (L_122)
		{
			goto IL_0212;
		}
	}
	{
		Type_t * L_123 = V_8;
		Type_t * L_124 = V_9;
		Dictionary_2_t633324528 * L_125 = ___resolvedMap2;
		HashSet_1_t1048894234 * L_126 = ___processedParams3;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_127 = TypeExtensions_GenericParameterIsFulfilledBy_m907762269(NULL /*static, unused*/, L_123, L_124, L_125, L_126, /*hidden argument*/NULL);
		if (L_127)
		{
			goto IL_0212;
		}
	}
	{
		return (bool)0;
	}

IL_01ff:
	{
		Type_t * L_128 = V_8;
		Type_t * L_129 = V_9;
		if ((((RuntimeObject*)(Type_t *)L_128) == ((RuntimeObject*)(Type_t *)L_129)))
		{
			goto IL_0212;
		}
	}
	{
		Type_t * L_130 = V_8;
		Type_t * L_131 = V_9;
		NullCheck(L_130);
		bool L_132 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_130, L_131);
		if (L_132)
		{
			goto IL_0212;
		}
	}
	{
		return (bool)0;
	}

IL_0212:
	{
		int32_t L_133 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_133, (int32_t)1));
	}

IL_0218:
	{
		int32_t L_134 = V_7;
		TypeU5BU5D_t3940880105* L_135 = V_5;
		NullCheck(L_135);
		if ((((int32_t)L_134) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_135)->max_length)))))))
		{
			goto IL_01b2;
		}
	}
	{
		goto IL_0243;
	}

IL_0222:
	{
		Type_t * L_136 = V_3;
		Type_t * L_137 = ___parameterType1;
		NullCheck(L_136);
		bool L_138 = VirtFuncInvoker1< bool, Type_t * >::Invoke(174 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_136, L_137);
		if (L_138)
		{
			goto IL_0243;
		}
	}
	{
		return (bool)0;
	}

IL_022d:
	{
		Type_t * L_139 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_140 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_139, /*hidden argument*/NULL);
		String_t* L_141 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1230253740, L_140, /*hidden argument*/NULL);
		Exception_t * L_142 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m1152696503(L_142, L_141, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_142, NULL, TypeExtensions_GenericParameterIsFulfilledBy_m907762269_RuntimeMethod_var);
	}

IL_0243:
	{
		int32_t L_143 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_143, (int32_t)1));
	}

IL_0247:
	{
		int32_t L_144 = V_2;
		TypeU5BU5D_t3940880105* L_145 = V_1;
		NullCheck(L_145);
		if ((((int32_t)L_144) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_145)->max_length)))))))
		{
			goto IL_00ed;
		}
	}
	{
		Dictionary_2_t633324528 * L_146 = ___resolvedMap2;
		Type_t * L_147 = ___genericParameterDefinition0;
		Type_t * L_148 = ___parameterType1;
		NullCheck(L_146);
		Dictionary_2_set_Item_m1327842169(L_146, L_147, L_148, /*hidden argument*/Dictionary_2_set_Item_m1327842169_RuntimeMethod_var);
		return (bool)1;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericConstraintsString(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetGenericConstraintsString_m2590875364 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, bool ___useFullTypeNames1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetGenericConstraintsString_m2590875364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TypeU5BU5D_t3940880105* V_0 = NULL;
	StringU5BU5D_t1281789340* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3243520166, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_GetGenericConstraintsString_m2590875364_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_2);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		Type_t * L_4 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_5 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2097671996, L_5, _stringLiteral1443261066, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_7 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, NULL, TypeExtensions_GetGenericConstraintsString_m2590875364_RuntimeMethod_var);
	}

IL_0031:
	{
		Type_t * L_8 = ___type0;
		NullCheck(L_8);
		TypeU5BU5D_t3940880105* L_9 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_8);
		V_0 = L_9;
		TypeU5BU5D_t3940880105* L_10 = V_0;
		NullCheck(L_10);
		StringU5BU5D_t1281789340* L_11 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))));
		V_1 = L_11;
		V_2 = 0;
		goto IL_0055;
	}

IL_0045:
	{
		StringU5BU5D_t1281789340* L_12 = V_1;
		int32_t L_13 = V_2;
		TypeU5BU5D_t3940880105* L_14 = V_0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		bool L_18 = ___useFullTypeNames1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_19 = TypeExtensions_GetGenericParameterConstraintsString_m1569308089(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_19);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (String_t*)L_19);
		int32_t L_20 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0055:
	{
		int32_t L_21 = V_2;
		TypeU5BU5D_t3940880105* L_22 = V_0;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_0045;
		}
	}
	{
		StringU5BU5D_t1281789340* L_23 = V_1;
		String_t* L_24 = String_Join_m2050845953(NULL /*static, unused*/, _stringLiteral3452614528, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericParameterConstraintsString(System.Type,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR String_t* TypeExtensions_GetGenericParameterConstraintsString_m1569308089 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, bool ___useFullTypeNames1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetGenericParameterConstraintsString_m1569308089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	TypeU5BU5D_t3940880105* V_3 = NULL;
	int32_t V_4 = 0;
	Type_t * V_5 = NULL;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3243520166, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_GetGenericParameterConstraintsString_m1569308089_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_2);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		Type_t * L_4 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_5 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2097671996, L_5, _stringLiteral4167518294, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_7 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, NULL, TypeExtensions_GetGenericParameterConstraintsString_m1569308089_RuntimeMethod_var);
	}

IL_0031:
	{
		StringBuilder_t * L_8 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		V_1 = (bool)0;
		Type_t * L_9 = ___type0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(105 /* System.Reflection.GenericParameterAttributes System.Type::get_GenericParameterAttributes() */, L_9);
		V_2 = L_10;
		int32_t L_11 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_11&(int32_t)8))) == ((uint32_t)8))))
		{
			goto IL_006b;
		}
	}
	{
		StringBuilder_t * L_12 = V_0;
		NullCheck(L_12);
		StringBuilder_t * L_13 = StringBuilder_Append_m1965104174(L_12, _stringLiteral731347757, /*hidden argument*/NULL);
		Type_t * L_14 = ___type0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_14);
		NullCheck(L_13);
		StringBuilder_t * L_16 = StringBuilder_Append_m1965104174(L_13, L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m1965104174(L_16, _stringLiteral4135268586, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_0094;
	}

IL_006b:
	{
		int32_t L_17 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_17&(int32_t)4))) == ((uint32_t)4))))
		{
			goto IL_0094;
		}
	}
	{
		StringBuilder_t * L_18 = V_0;
		NullCheck(L_18);
		StringBuilder_t * L_19 = StringBuilder_Append_m1965104174(L_18, _stringLiteral731347757, /*hidden argument*/NULL);
		Type_t * L_20 = ___type0;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_20);
		NullCheck(L_19);
		StringBuilder_t * L_22 = StringBuilder_Append_m1965104174(L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		StringBuilder_Append_m1965104174(L_22, _stringLiteral2758848599, /*hidden argument*/NULL);
		V_1 = (bool)1;
	}

IL_0094:
	{
		int32_t L_23 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_23&(int32_t)((int32_t)16)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_00d0;
		}
	}
	{
		bool L_24 = V_1;
		if (!L_24)
		{
			goto IL_00ad;
		}
	}
	{
		StringBuilder_t * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m1965104174(L_25, _stringLiteral674152356, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_00ad:
	{
		StringBuilder_t * L_26 = V_0;
		NullCheck(L_26);
		StringBuilder_t * L_27 = StringBuilder_Append_m1965104174(L_26, _stringLiteral731347757, /*hidden argument*/NULL);
		Type_t * L_28 = ___type0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_28);
		NullCheck(L_27);
		StringBuilder_t * L_30 = StringBuilder_Append_m1965104174(L_27, L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m1965104174(L_30, _stringLiteral2018662753, /*hidden argument*/NULL);
		V_1 = (bool)1;
	}

IL_00d0:
	{
		Type_t * L_31 = ___type0;
		NullCheck(L_31);
		TypeU5BU5D_t3940880105* L_32 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(137 /* System.Type[] System.Type::GetGenericParameterConstraints() */, L_31);
		V_3 = L_32;
		TypeU5BU5D_t3940880105* L_33 = V_3;
		NullCheck(L_33);
		if (!(((RuntimeArray *)L_33)->max_length))
		{
			goto IL_0172;
		}
	}
	{
		V_4 = 0;
		goto IL_0168;
	}

IL_00e6:
	{
		TypeU5BU5D_t3940880105* L_34 = V_3;
		int32_t L_35 = V_4;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		Type_t * L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		V_5 = L_37;
		bool L_38 = V_1;
		if (!L_38)
		{
			goto IL_011e;
		}
	}
	{
		StringBuilder_t * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m1965104174(L_39, _stringLiteral3450517380, /*hidden argument*/NULL);
		bool L_40 = ___useFullTypeNames1;
		if (!L_40)
		{
			goto IL_010e;
		}
	}
	{
		StringBuilder_t * L_41 = V_0;
		Type_t * L_42 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_43 = TypeExtensions_GetNiceFullName_m2504614971(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		StringBuilder_Append_m1965104174(L_41, L_43, /*hidden argument*/NULL);
		goto IL_0162;
	}

IL_010e:
	{
		StringBuilder_t * L_44 = V_0;
		Type_t * L_45 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_46 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		StringBuilder_Append_m1965104174(L_44, L_46, /*hidden argument*/NULL);
		goto IL_0162;
	}

IL_011e:
	{
		StringBuilder_t * L_47 = V_0;
		NullCheck(L_47);
		StringBuilder_t * L_48 = StringBuilder_Append_m1965104174(L_47, _stringLiteral731347757, /*hidden argument*/NULL);
		Type_t * L_49 = ___type0;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_49);
		NullCheck(L_48);
		StringBuilder_t * L_51 = StringBuilder_Append_m1965104174(L_48, L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		StringBuilder_Append_m1965104174(L_51, _stringLiteral3787497674, /*hidden argument*/NULL);
		bool L_52 = ___useFullTypeNames1;
		if (!L_52)
		{
			goto IL_0152;
		}
	}
	{
		StringBuilder_t * L_53 = V_0;
		Type_t * L_54 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_55 = TypeExtensions_GetNiceFullName_m2504614971(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		StringBuilder_Append_m1965104174(L_53, L_55, /*hidden argument*/NULL);
		goto IL_0160;
	}

IL_0152:
	{
		StringBuilder_t * L_56 = V_0;
		Type_t * L_57 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		String_t* L_58 = TypeExtensions_GetNiceName_m2140991116(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		StringBuilder_Append_m1965104174(L_56, L_58, /*hidden argument*/NULL);
	}

IL_0160:
	{
		V_1 = (bool)1;
	}

IL_0162:
	{
		int32_t L_59 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)1));
	}

IL_0168:
	{
		int32_t L_60 = V_4;
		TypeU5BU5D_t3940880105* L_61 = V_3;
		NullCheck(L_61);
		if ((((int32_t)L_60) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_61)->max_length)))))))
		{
			goto IL_00e6;
		}
	}

IL_0172:
	{
		StringBuilder_t * L_62 = V_0;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_62);
		return L_63;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericArgumentsContainsTypes(System.Type,System.Type[])
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_GenericArgumentsContainsTypes_m411034277 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t3940880105* ___types1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GenericArgumentsContainsTypes_m411034277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BooleanU5BU5D_t2897418192* V_0 = NULL;
	TypeU5BU5D_t3940880105* V_1 = NULL;
	Stack_1_t3327334215 * V_2 = NULL;
	Type_t * V_3 = NULL;
	int32_t V_4 = 0;
	Type_t * V_5 = NULL;
	bool V_6 = false;
	int32_t V_7 = 0;
	TypeU5BU5D_t3940880105* V_8 = NULL;
	int32_t V_9 = 0;
	Type_t * V_10 = NULL;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3243520166, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_GenericArgumentsContainsTypes_m411034277_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_2);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		TypeU5BU5D_t3940880105* L_4 = ___types1;
		NullCheck(L_4);
		BooleanU5BU5D_t2897418192* L_5 = (BooleanU5BU5D_t2897418192*)SZArrayNew(BooleanU5BU5D_t2897418192_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))));
		V_0 = L_5;
		Type_t * L_6 = ___type0;
		NullCheck(L_6);
		TypeU5BU5D_t3940880105* L_7 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_6);
		V_1 = L_7;
		TypeU5BU5D_t3940880105* L_8 = V_1;
		Stack_1_t3327334215 * L_9 = (Stack_1_t3327334215 *)il2cpp_codegen_object_new(Stack_1_t3327334215_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2267086409(L_9, (RuntimeObject*)(RuntimeObject*)L_8, /*hidden argument*/Stack_1__ctor_m2267086409_RuntimeMethod_var);
		V_2 = L_9;
		goto IL_00df;
	}

IL_0034:
	{
		Stack_1_t3327334215 * L_10 = V_2;
		NullCheck(L_10);
		Type_t * L_11 = Stack_1_Pop_m2843357855(L_10, /*hidden argument*/Stack_1_Pop_m2843357855_RuntimeMethod_var);
		V_3 = L_11;
		V_4 = 0;
		goto IL_0080;
	}

IL_0040:
	{
		TypeU5BU5D_t3940880105* L_12 = ___types1;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Type_t * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Type_t * L_16 = V_5;
		Type_t * L_17 = V_3;
		if ((!(((RuntimeObject*)(Type_t *)L_16) == ((RuntimeObject*)(Type_t *)L_17))))
		{
			goto IL_0052;
		}
	}
	{
		BooleanU5BU5D_t2897418192* L_18 = V_0;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (bool)1);
		goto IL_007a;
	}

IL_0052:
	{
		Type_t * L_20 = V_5;
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_20);
		if (!L_21)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_22 = V_3;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_22);
		if (!L_23)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_24 = V_3;
		NullCheck(L_24);
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_24);
		if (L_25)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = VirtFuncInvoker0< Type_t * >::Invoke(161 /* System.Type System.Type::GetGenericTypeDefinition() */, L_26);
		Type_t * L_28 = V_5;
		if ((!(((RuntimeObject*)(Type_t *)L_27) == ((RuntimeObject*)(Type_t *)L_28))))
		{
			goto IL_007a;
		}
	}
	{
		BooleanU5BU5D_t2897418192* L_29 = V_0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (bool)1);
	}

IL_007a:
	{
		int32_t L_31 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_0080:
	{
		int32_t L_32 = V_4;
		TypeU5BU5D_t3940880105* L_33 = ___types1;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		V_6 = (bool)1;
		V_7 = 0;
		goto IL_00a0;
	}

IL_008f:
	{
		BooleanU5BU5D_t2897418192* L_34 = V_0;
		int32_t L_35 = V_7;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		uint8_t L_37 = (uint8_t)(L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		if (L_37)
		{
			goto IL_009a;
		}
	}
	{
		V_6 = (bool)0;
		goto IL_00a7;
	}

IL_009a:
	{
		int32_t L_38 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00a0:
	{
		int32_t L_39 = V_7;
		BooleanU5BU5D_t2897418192* L_40 = V_0;
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_40)->max_length)))))))
		{
			goto IL_008f;
		}
	}

IL_00a7:
	{
		bool L_41 = V_6;
		if (!L_41)
		{
			goto IL_00ad;
		}
	}
	{
		return (bool)1;
	}

IL_00ad:
	{
		Type_t * L_42 = V_3;
		NullCheck(L_42);
		bool L_43 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_42);
		if (!L_43)
		{
			goto IL_00df;
		}
	}
	{
		Type_t * L_44 = V_3;
		NullCheck(L_44);
		TypeU5BU5D_t3940880105* L_45 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_44);
		V_8 = L_45;
		V_9 = 0;
		goto IL_00d7;
	}

IL_00c2:
	{
		TypeU5BU5D_t3940880105* L_46 = V_8;
		int32_t L_47 = V_9;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		Type_t * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		V_10 = L_49;
		Stack_1_t3327334215 * L_50 = V_2;
		Type_t * L_51 = V_10;
		NullCheck(L_50);
		Stack_1_Push_m506528439(L_50, L_51, /*hidden argument*/Stack_1_Push_m506528439_RuntimeMethod_var);
		int32_t L_52 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_00d7:
	{
		int32_t L_53 = V_9;
		TypeU5BU5D_t3940880105* L_54 = V_8;
		NullCheck(L_54);
		if ((((int32_t)L_53) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_54)->max_length)))))))
		{
			goto IL_00c2;
		}
	}

IL_00df:
	{
		Stack_1_t3327334215 * L_55 = V_2;
		NullCheck(L_55);
		int32_t L_56 = Stack_1_get_Count_m1286321891(L_55, /*hidden argument*/Stack_1_get_Count_m1286321891_RuntimeMethod_var);
		if ((((int32_t)L_56) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsFullyConstructedGenericType(System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsFullyConstructedGenericType_m809342933 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_IsFullyConstructedGenericType_m809342933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TypeU5BU5D_t3940880105* V_0 = NULL;
	Type_t * V_1 = NULL;
	int32_t V_2 = 0;
	Type_t * V_3 = NULL;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral3243520166, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, TypeExtensions_IsFullyConstructedGenericType_m809342933_RuntimeMethod_var);
	}

IL_000e:
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_2);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		bool L_5 = Type_get_HasElementType_m710151977(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		Type_t * L_6 = ___type0;
		NullCheck(L_6);
		Type_t * L_7 = VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::GetElementType() */, L_6);
		V_1 = L_7;
		Type_t * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_8);
		if (L_9)
		{
			goto IL_0037;
		}
	}
	{
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_11 = TypeExtensions_IsFullyConstructedGenericType_m809342933(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0039;
		}
	}

IL_0037:
	{
		return (bool)0;
	}

IL_0039:
	{
		Type_t * L_12 = ___type0;
		NullCheck(L_12);
		TypeU5BU5D_t3940880105* L_13 = VirtFuncInvoker0< TypeU5BU5D_t3940880105* >::Invoke(159 /* System.Type[] System.Type::GetGenericArguments() */, L_12);
		V_0 = L_13;
		V_2 = 0;
		goto IL_0060;
	}

IL_0044:
	{
		TypeU5BU5D_t3940880105* L_14 = V_0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_3 = L_17;
		Type_t * L_18 = V_3;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(134 /* System.Boolean System.Type::get_IsGenericParameter() */, L_18);
		if (!L_19)
		{
			goto IL_0052;
		}
	}
	{
		return (bool)0;
	}

IL_0052:
	{
		Type_t * L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_21 = TypeExtensions_IsFullyConstructedGenericType_m809342933(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_005c;
		}
	}
	{
		return (bool)0;
	}

IL_005c:
	{
		int32_t L_22 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0060:
	{
		int32_t L_23 = V_2;
		TypeU5BU5D_t3940880105* L_24 = V_0;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length)))))))
		{
			goto IL_0044;
		}
	}
	{
		Type_t * L_25 = ___type0;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(132 /* System.Boolean System.Type::get_IsGenericTypeDefinition() */, L_25);
		return (bool)((((int32_t)L_26) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsNullableType(System.Type)
extern "C" IL2CPP_METHOD_ATTR bool TypeExtensions_IsNullableType_m2386472913 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsPrimitive_m1114712797(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		bool L_3 = Type_get_IsValueType_m3108065642(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001a;
		}
	}
	{
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(122 /* System.Boolean System.Type::get_IsEnum() */, L_4);
		return (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
	}

IL_001a:
	{
		return (bool)0;
	}
}
// System.UInt64 Sirenix.Serialization.Utilities.TypeExtensions::GetEnumBitmask(System.Object,System.Type)
extern "C" IL2CPP_METHOD_ATTR uint64_t TypeExtensions_GetEnumBitmask_m2667989293 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, Type_t * ___enumType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions_GetEnumBitmask_m2667989293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___enumType1;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(122 /* System.Boolean System.Type::get_IsEnum() */, L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentException_t132251570 * L_2 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_2, _stringLiteral3217749252, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, TypeExtensions_GetEnumBitmask_m2667989293_RuntimeMethod_var);
	}

IL_0013:
	{
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_4 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint64_t L_5 = Convert_ToUInt64_m3170916409(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0031;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t2020128637_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.OverflowException)
		RuntimeObject * L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_7 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int64_t L_8 = Convert_ToInt64_m2643251823(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0031;
	} // end catch (depth: 1)

IL_0031:
	{
		uint64_t L_9 = V_0;
		return L_9;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions::.cctor()
extern "C" IL2CPP_METHOD_ATTR void TypeExtensions__cctor_m74757846 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeExtensions__cctor_m74757846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3493241298 * V_0 = NULL;
	HashSet_1_t1048894234 * V_1 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_0, /*hidden argument*/NULL);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_GenericConstraintsSatisfaction_LOCK_0(L_0);
		Dictionary_2_t633324528 * L_1 = (Dictionary_2_t633324528 *)il2cpp_codegen_object_new(Dictionary_2_t633324528_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2733271626(L_1, /*hidden argument*/Dictionary_2__ctor_m2733271626_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_GenericConstraintsSatisfactionInferredParameters_1(L_1);
		Dictionary_2_t633324528 * L_2 = (Dictionary_2_t633324528 *)il2cpp_codegen_object_new(Dictionary_2_t633324528_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2733271626(L_2, /*hidden argument*/Dictionary_2__ctor_m2733271626_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_GenericConstraintsSatisfactionResolvedMap_2(L_2);
		HashSet_1_t1048894234 * L_3 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_3, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_GenericConstraintsSatisfactionProcessedParams_3(L_3);
		RuntimeObject * L_4 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_4, /*hidden argument*/NULL);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_WeaklyTypedTypeCastDelegates_LOCK_4(L_4);
		RuntimeObject * L_5 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_5, /*hidden argument*/NULL);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_StronglyTypedTypeCastDelegates_LOCK_5(L_5);
		DoubleLookupDictionary_3_t650546409 * L_6 = (DoubleLookupDictionary_3_t650546409 *)il2cpp_codegen_object_new(DoubleLookupDictionary_3_t650546409_il2cpp_TypeInfo_var);
		DoubleLookupDictionary_3__ctor_m653869603(L_6, /*hidden argument*/DoubleLookupDictionary_3__ctor_m653869603_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_WeaklyTypedTypeCastDelegates_6(L_6);
		DoubleLookupDictionary_3_t3686776144 * L_7 = (DoubleLookupDictionary_3_t3686776144 *)il2cpp_codegen_object_new(DoubleLookupDictionary_3_t3686776144_il2cpp_TypeInfo_var);
		DoubleLookupDictionary_3__ctor_m3657728929(L_7, /*hidden argument*/DoubleLookupDictionary_3__ctor_m3657728929_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_StronglyTypedTypeCastDelegates_7(L_7);
		HashSet_1_t412400163 * L_8 = (HashSet_1_t412400163 *)il2cpp_codegen_object_new(HashSet_1_t412400163_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m708940816(L_8, /*hidden argument*/HashSet_1__ctor_m708940816_RuntimeMethod_var);
		HashSet_1_t412400163 * L_9 = L_8;
		NullCheck(L_9);
		HashSet_1_Add_m3328534555(L_9, _stringLiteral280883515, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_10 = L_9;
		NullCheck(L_10);
		HashSet_1_Add_m3328534555(L_10, _stringLiteral3455563711, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_11 = L_10;
		NullCheck(L_11);
		HashSet_1_Add_m3328534555(L_11, _stringLiteral2838662761, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_12 = L_11;
		NullCheck(L_12);
		HashSet_1_Add_m3328534555(L_12, _stringLiteral798688685, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_13 = L_12;
		NullCheck(L_13);
		HashSet_1_Add_m3328534555(L_13, _stringLiteral22182330, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_14 = L_13;
		NullCheck(L_14);
		HashSet_1_Add_m3328534555(L_14, _stringLiteral1274151684, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_15 = L_14;
		NullCheck(L_15);
		HashSet_1_Add_m3328534555(L_15, _stringLiteral2838662760, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_16 = L_15;
		NullCheck(L_16);
		HashSet_1_Add_m3328534555(L_16, _stringLiteral461028241, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_17 = L_16;
		NullCheck(L_17);
		HashSet_1_Add_m3328534555(L_17, _stringLiteral3873631970, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_18 = L_17;
		NullCheck(L_18);
		HashSet_1_Add_m3328534555(L_18, _stringLiteral4158218461, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_19 = L_18;
		NullCheck(L_19);
		HashSet_1_Add_m3328534555(L_19, _stringLiteral351315815, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_20 = L_19;
		NullCheck(L_20);
		HashSet_1_Add_m3328534555(L_20, _stringLiteral748112283, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_21 = L_20;
		NullCheck(L_21);
		HashSet_1_Add_m3328534555(L_21, _stringLiteral1055810917, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_22 = L_21;
		NullCheck(L_22);
		HashSet_1_Add_m3328534555(L_22, _stringLiteral2033559650, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_23 = L_22;
		NullCheck(L_23);
		HashSet_1_Add_m3328534555(L_23, _stringLiteral1948332219, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_24 = L_23;
		NullCheck(L_24);
		HashSet_1_Add_m3328534555(L_24, _stringLiteral3158016519, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_25 = L_24;
		NullCheck(L_25);
		HashSet_1_Add_m3328534555(L_25, _stringLiteral3454777276, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_26 = L_25;
		NullCheck(L_26);
		HashSet_1_Add_m3328534555(L_26, _stringLiteral1235497039, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_27 = L_26;
		NullCheck(L_27);
		HashSet_1_Add_m3328534555(L_27, _stringLiteral2838990438, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_28 = L_27;
		NullCheck(L_28);
		HashSet_1_Add_m3328534555(L_28, _stringLiteral1236039580, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_29 = L_28;
		NullCheck(L_29);
		HashSet_1_Add_m3328534555(L_29, _stringLiteral2734520598, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_30 = L_29;
		NullCheck(L_30);
		HashSet_1_Add_m3328534555(L_30, _stringLiteral3637139173, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_31 = L_30;
		NullCheck(L_31);
		HashSet_1_Add_m3328534555(L_31, _stringLiteral130792137, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_32 = L_31;
		NullCheck(L_32);
		HashSet_1_Add_m3328534555(L_32, _stringLiteral3875954633, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_33 = L_32;
		NullCheck(L_33);
		HashSet_1_Add_m3328534555(L_33, _stringLiteral3859429177, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_34 = L_33;
		NullCheck(L_34);
		HashSet_1_Add_m3328534555(L_34, _stringLiteral1709130670, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_35 = L_34;
		NullCheck(L_35);
		HashSet_1_Add_m3328534555(L_35, _stringLiteral807124363, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_36 = L_35;
		NullCheck(L_36);
		HashSet_1_Add_m3328534555(L_36, _stringLiteral1390811594, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_37 = L_36;
		NullCheck(L_37);
		HashSet_1_Add_m3328534555(L_37, _stringLiteral1184841701, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_38 = L_37;
		NullCheck(L_38);
		HashSet_1_Add_m3328534555(L_38, _stringLiteral2419720959, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_39 = L_38;
		NullCheck(L_39);
		HashSet_1_Add_m3328534555(L_39, _stringLiteral3454318535, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_40 = L_39;
		NullCheck(L_40);
		HashSet_1_Add_m3328534555(L_40, _stringLiteral3596048705, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_41 = L_40;
		NullCheck(L_41);
		HashSet_1_Add_m3328534555(L_41, _stringLiteral3454842823, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_42 = L_41;
		NullCheck(L_42);
		HashSet_1_Add_m3328534555(L_42, _stringLiteral2553676557, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_43 = L_42;
		NullCheck(L_43);
		HashSet_1_Add_m3328534555(L_43, _stringLiteral775541518, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_44 = L_43;
		NullCheck(L_44);
		HashSet_1_Add_m3328534555(L_44, _stringLiteral2289751134, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_45 = L_44;
		NullCheck(L_45);
		HashSet_1_Add_m3328534555(L_45, _stringLiteral3455563719, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_46 = L_45;
		NullCheck(L_46);
		HashSet_1_Add_m3328534555(L_46, _stringLiteral1167972383, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_47 = L_46;
		NullCheck(L_47);
		HashSet_1_Add_m3328534555(L_47, _stringLiteral2409402648, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_48 = L_47;
		NullCheck(L_48);
		HashSet_1_Add_m3328534555(L_48, _stringLiteral3820675233, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_49 = L_48;
		NullCheck(L_49);
		HashSet_1_Add_m3328534555(L_49, _stringLiteral2149933273, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_50 = L_49;
		NullCheck(L_50);
		HashSet_1_Add_m3328534555(L_50, _stringLiteral1202628576, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_51 = L_50;
		NullCheck(L_51);
		HashSet_1_Add_m3328534555(L_51, _stringLiteral3557324734, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_52 = L_51;
		NullCheck(L_52);
		HashSet_1_Add_m3328534555(L_52, _stringLiteral2978261106, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_53 = L_52;
		NullCheck(L_53);
		HashSet_1_Add_m3328534555(L_53, _stringLiteral2554266375, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_54 = L_53;
		NullCheck(L_54);
		HashSet_1_Add_m3328534555(L_54, _stringLiteral345858392, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_55 = L_54;
		NullCheck(L_55);
		HashSet_1_Add_m3328534555(L_55, _stringLiteral3980842185, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_56 = L_55;
		NullCheck(L_56);
		HashSet_1_Add_m3328534555(L_56, _stringLiteral1801699217, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_57 = L_56;
		NullCheck(L_57);
		HashSet_1_Add_m3328534555(L_57, _stringLiteral170650980, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_58 = L_57;
		NullCheck(L_58);
		HashSet_1_Add_m3328534555(L_58, _stringLiteral3524468349, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_59 = L_58;
		NullCheck(L_59);
		HashSet_1_Add_m3328534555(L_59, _stringLiteral827624532, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_60 = L_59;
		NullCheck(L_60);
		HashSet_1_Add_m3328534555(L_60, _stringLiteral133510650, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_61 = L_60;
		NullCheck(L_61);
		HashSet_1_Add_m3328534555(L_61, _stringLiteral3134897496, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_62 = L_61;
		NullCheck(L_62);
		HashSet_1_Add_m3328534555(L_62, _stringLiteral1225964229, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_63 = L_62;
		NullCheck(L_63);
		HashSet_1_Add_m3328534555(L_63, _stringLiteral765372749, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_64 = L_63;
		NullCheck(L_64);
		HashSet_1_Add_m3328534555(L_64, _stringLiteral1535819814, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_65 = L_64;
		NullCheck(L_65);
		HashSet_1_Add_m3328534555(L_65, _stringLiteral2873896790, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_66 = L_65;
		NullCheck(L_66);
		HashSet_1_Add_m3328534555(L_66, _stringLiteral2954596464, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_67 = L_66;
		NullCheck(L_67);
		HashSet_1_Add_m3328534555(L_67, _stringLiteral2322770241, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_68 = L_67;
		NullCheck(L_68);
		HashSet_1_Add_m3328534555(L_68, _stringLiteral1236128813, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_69 = L_68;
		NullCheck(L_69);
		HashSet_1_Add_m3328534555(L_69, _stringLiteral2001916978, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_70 = L_69;
		NullCheck(L_70);
		HashSet_1_Add_m3328534555(L_70, _stringLiteral1956447343, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_71 = L_70;
		NullCheck(L_71);
		HashSet_1_Add_m3328534555(L_71, _stringLiteral1178749465, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_72 = L_71;
		NullCheck(L_72);
		HashSet_1_Add_m3328534555(L_72, _stringLiteral82367591, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_73 = L_72;
		NullCheck(L_73);
		HashSet_1_Add_m3328534555(L_73, _stringLiteral4002445229, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_74 = L_73;
		NullCheck(L_74);
		HashSet_1_Add_m3328534555(L_74, _stringLiteral2601517161, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_75 = L_74;
		NullCheck(L_75);
		HashSet_1_Add_m3328534555(L_75, _stringLiteral2101930777, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_76 = L_75;
		NullCheck(L_76);
		HashSet_1_Add_m3328534555(L_76, _stringLiteral3894513951, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_77 = L_76;
		NullCheck(L_77);
		HashSet_1_Add_m3328534555(L_77, _stringLiteral3528114263, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_78 = L_77;
		NullCheck(L_78);
		HashSet_1_Add_m3328534555(L_78, _stringLiteral2097807219, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_79 = L_78;
		NullCheck(L_79);
		HashSet_1_Add_m3328534555(L_79, _stringLiteral2422556946, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_80 = L_79;
		NullCheck(L_80);
		HashSet_1_Add_m3328534555(L_80, _stringLiteral3576598565, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_81 = L_80;
		NullCheck(L_81);
		HashSet_1_Add_m3328534555(L_81, _stringLiteral59180213, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_82 = L_81;
		NullCheck(L_82);
		HashSet_1_Add_m3328534555(L_82, _stringLiteral2322770241, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_83 = L_82;
		NullCheck(L_83);
		HashSet_1_Add_m3328534555(L_83, _stringLiteral75909655, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_84 = L_83;
		NullCheck(L_84);
		HashSet_1_Add_m3328534555(L_84, _stringLiteral1453402100, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_85 = L_84;
		NullCheck(L_85);
		HashSet_1_Add_m3328534555(L_85, _stringLiteral430703593, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_86 = L_85;
		NullCheck(L_86);
		HashSet_1_Add_m3328534555(L_86, _stringLiteral3454842823, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_87 = L_86;
		NullCheck(L_87);
		HashSet_1_Add_m3328534555(L_87, _stringLiteral2553217791, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_88 = L_87;
		NullCheck(L_88);
		HashSet_1_Add_m3328534555(L_88, _stringLiteral2553217811, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		HashSet_1_t412400163 * L_89 = L_88;
		NullCheck(L_89);
		HashSet_1_Add_m3328534555(L_89, _stringLiteral1390680538, /*hidden argument*/HashSet_1_Add_m3328534555_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_ReservedCSharpKeywords_8(L_89);
		Dictionary_2_t1632706988 * L_90 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m444307833(L_90, /*hidden argument*/Dictionary_2__ctor_m444307833_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_91 = L_90;
		NullCheck(L_91);
		Dictionary_2_Add_m3045345476(L_91, _stringLiteral1234456403, _stringLiteral807124363, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_92 = L_91;
		NullCheck(L_92);
		Dictionary_2_Add_m3045345476(L_92, _stringLiteral1235498031, _stringLiteral1235497039, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_93 = L_92;
		NullCheck(L_93);
		Dictionary_2_Add_m3045345476(L_93, _stringLiteral1295171237, _stringLiteral1225964229, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_94 = L_93;
		NullCheck(L_94);
		Dictionary_2_Add_m3045345476(L_94, _stringLiteral263700689, _stringLiteral1535819814, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_95 = L_94;
		NullCheck(L_95);
		Dictionary_2_Add_m3045345476(L_95, _stringLiteral4176330965, _stringLiteral2553676557, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_96 = L_95;
		NullCheck(L_96);
		Dictionary_2_Add_m3045345476(L_96, _stringLiteral1455341775, _stringLiteral2409402648, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_97 = L_96;
		NullCheck(L_97);
		Dictionary_2_Add_m3045345476(L_97, _stringLiteral1274151716, _stringLiteral1274151684, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_98 = L_97;
		NullCheck(L_98);
		Dictionary_2_Add_m3045345476(L_98, _stringLiteral2789625702, _stringLiteral3576598565, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_99 = L_98;
		NullCheck(L_99);
		Dictionary_2_Add_m3045345476(L_99, _stringLiteral2789887848, _stringLiteral3894513951, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_100 = L_99;
		NullCheck(L_100);
		Dictionary_2_Add_m3045345476(L_100, _stringLiteral2789494635, _stringLiteral3528114263, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_101 = L_100;
		NullCheck(L_101);
		Dictionary_2_Add_m3045345476(L_101, _stringLiteral2033560642, _stringLiteral2033559650, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_102 = L_101;
		NullCheck(L_102);
		Dictionary_2_Add_m3045345476(L_102, _stringLiteral1236129805, _stringLiteral1236128813, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_103 = L_102;
		NullCheck(L_103);
		Dictionary_2_Add_m3045345476(L_103, _stringLiteral3873632002, _stringLiteral3873631970, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_104 = L_103;
		NullCheck(L_104);
		Dictionary_2_Add_m3045345476(L_104, _stringLiteral228530734, _stringLiteral798688685, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_105 = L_104;
		NullCheck(L_105);
		Dictionary_2_Add_m3045345476(L_105, _stringLiteral3267344922, _stringLiteral594767410, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_106 = L_105;
		NullCheck(L_106);
		Dictionary_2_Add_m3045345476(L_106, _stringLiteral3446723604, _stringLiteral3446722612, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_107 = L_106;
		NullCheck(L_107);
		Dictionary_2_Add_m3045345476(L_107, _stringLiteral1309047644, _stringLiteral1231452028, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_108 = L_107;
		NullCheck(L_108);
		Dictionary_2_Add_m3045345476(L_108, _stringLiteral3665284438, _stringLiteral2890035405, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_109 = L_108;
		NullCheck(L_109);
		Dictionary_2_Add_m3045345476(L_109, _stringLiteral3933064538, _stringLiteral2583018776, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_110 = L_109;
		NullCheck(L_110);
		Dictionary_2_Add_m3045345476(L_110, _stringLiteral39767380, _stringLiteral1213614791, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_111 = L_110;
		NullCheck(L_111);
		Dictionary_2_Add_m3045345476(L_111, _stringLiteral133939039, _stringLiteral133937983, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_112 = L_111;
		NullCheck(L_112);
		Dictionary_2_Add_m3045345476(L_112, _stringLiteral1469299245, _stringLiteral2712963504, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_113 = L_112;
		NullCheck(L_113);
		Dictionary_2_Add_m3045345476(L_113, _stringLiteral1469561391, _stringLiteral2722843552, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_114 = L_113;
		NullCheck(L_114);
		Dictionary_2_Add_m3045345476(L_114, _stringLiteral1469168178, _stringLiteral193097982, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_115 = L_114;
		NullCheck(L_115);
		Dictionary_2_Add_m3045345476(L_115, _stringLiteral3194025642, _stringLiteral3193993034, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_116 = L_115;
		NullCheck(L_116);
		Dictionary_2_Add_m3045345476(L_116, _stringLiteral2191247092, _stringLiteral2191246100, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_117 = L_116;
		NullCheck(L_117);
		Dictionary_2_Add_m3045345476(L_117, _stringLiteral2695801631, _stringLiteral2695800703, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_118 = L_117;
		NullCheck(L_118);
		Dictionary_2_Add_m3045345476(L_118, _stringLiteral2453691846, _stringLiteral3897867752, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_TypeNameAlternatives_9(L_118);
		RuntimeObject * L_119 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_119, /*hidden argument*/NULL);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_CachedNiceNames_LOCK_10(L_119);
		Dictionary_2_t4291797753 * L_120 = (Dictionary_2_t4291797753 *)il2cpp_codegen_object_new(Dictionary_2_t4291797753_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m960807489(L_120, /*hidden argument*/Dictionary_2__ctor_m960807489_RuntimeMethod_var);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_CachedNiceNames_11(L_120);
		RuntimeTypeHandle_t3027515415  L_121 = { reinterpret_cast<intptr_t> (Void_t1185182177_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_122 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		Type_t * L_123 = VirtFuncInvoker0< Type_t * >::Invoke(41 /* System.Type System.Type::MakePointerType() */, L_122);
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_VoidPointerType_12(L_123);
		Dictionary_2_t3493241298 * L_124 = (Dictionary_2_t3493241298 *)il2cpp_codegen_object_new(Dictionary_2_t3493241298_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2777391954(L_124, /*hidden argument*/Dictionary_2__ctor_m2777391954_RuntimeMethod_var);
		V_0 = L_124;
		Dictionary_2_t3493241298 * L_125 = V_0;
		RuntimeTypeHandle_t3027515415  L_126 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_127 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_126, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_128 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_128, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_128;
		HashSet_1_t1048894234 * L_129 = V_1;
		RuntimeTypeHandle_t3027515415  L_130 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_131 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_130, /*hidden argument*/NULL);
		NullCheck(L_129);
		HashSet_1_Add_m3527721678(L_129, L_131, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_132 = V_1;
		RuntimeTypeHandle_t3027515415  L_133 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_134 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_133, /*hidden argument*/NULL);
		NullCheck(L_132);
		HashSet_1_Add_m3527721678(L_132, L_134, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_135 = V_1;
		RuntimeTypeHandle_t3027515415  L_136 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_137 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_136, /*hidden argument*/NULL);
		NullCheck(L_135);
		HashSet_1_Add_m3527721678(L_135, L_137, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_138 = V_1;
		NullCheck(L_125);
		Dictionary_2_Add_m3699546241(L_125, L_127, L_138, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_139 = V_0;
		RuntimeTypeHandle_t3027515415  L_140 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_141 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_142 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_142, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_142;
		HashSet_1_t1048894234 * L_143 = V_1;
		RuntimeTypeHandle_t3027515415  L_144 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_145 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		NullCheck(L_143);
		HashSet_1_Add_m3527721678(L_143, L_145, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_146 = V_1;
		RuntimeTypeHandle_t3027515415  L_147 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_148 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_147, /*hidden argument*/NULL);
		NullCheck(L_146);
		HashSet_1_Add_m3527721678(L_146, L_148, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_149 = V_1;
		RuntimeTypeHandle_t3027515415  L_150 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_151 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_150, /*hidden argument*/NULL);
		NullCheck(L_149);
		HashSet_1_Add_m3527721678(L_149, L_151, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_152 = V_1;
		RuntimeTypeHandle_t3027515415  L_153 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_154 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_153, /*hidden argument*/NULL);
		NullCheck(L_152);
		HashSet_1_Add_m3527721678(L_152, L_154, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_155 = V_1;
		NullCheck(L_139);
		Dictionary_2_Add_m3699546241(L_139, L_141, L_155, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_156 = V_0;
		RuntimeTypeHandle_t3027515415  L_157 = { reinterpret_cast<intptr_t> (Int16_t2552820387_0_0_0_var) };
		Type_t * L_158 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_157, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_159 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_159, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_159;
		HashSet_1_t1048894234 * L_160 = V_1;
		RuntimeTypeHandle_t3027515415  L_161 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_162 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_161, /*hidden argument*/NULL);
		NullCheck(L_160);
		HashSet_1_Add_m3527721678(L_160, L_162, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_163 = V_1;
		RuntimeTypeHandle_t3027515415  L_164 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_165 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_164, /*hidden argument*/NULL);
		NullCheck(L_163);
		HashSet_1_Add_m3527721678(L_163, L_165, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_166 = V_1;
		RuntimeTypeHandle_t3027515415  L_167 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_168 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_167, /*hidden argument*/NULL);
		NullCheck(L_166);
		HashSet_1_Add_m3527721678(L_166, L_168, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_169 = V_1;
		RuntimeTypeHandle_t3027515415  L_170 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_171 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_170, /*hidden argument*/NULL);
		NullCheck(L_169);
		HashSet_1_Add_m3527721678(L_169, L_171, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_172 = V_1;
		RuntimeTypeHandle_t3027515415  L_173 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_174 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		NullCheck(L_172);
		HashSet_1_Add_m3527721678(L_172, L_174, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_175 = V_1;
		NullCheck(L_156);
		Dictionary_2_Add_m3699546241(L_156, L_158, L_175, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_176 = V_0;
		RuntimeTypeHandle_t3027515415  L_177 = { reinterpret_cast<intptr_t> (SByte_t1669577662_0_0_0_var) };
		Type_t * L_178 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_177, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_179 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_179, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_179;
		HashSet_1_t1048894234 * L_180 = V_1;
		RuntimeTypeHandle_t3027515415  L_181 = { reinterpret_cast<intptr_t> (Int16_t2552820387_0_0_0_var) };
		Type_t * L_182 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_181, /*hidden argument*/NULL);
		NullCheck(L_180);
		HashSet_1_Add_m3527721678(L_180, L_182, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_183 = V_1;
		RuntimeTypeHandle_t3027515415  L_184 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_185 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_184, /*hidden argument*/NULL);
		NullCheck(L_183);
		HashSet_1_Add_m3527721678(L_183, L_185, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_186 = V_1;
		RuntimeTypeHandle_t3027515415  L_187 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_188 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_187, /*hidden argument*/NULL);
		NullCheck(L_186);
		HashSet_1_Add_m3527721678(L_186, L_188, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_189 = V_1;
		RuntimeTypeHandle_t3027515415  L_190 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_191 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_190, /*hidden argument*/NULL);
		NullCheck(L_189);
		HashSet_1_Add_m3527721678(L_189, L_191, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_192 = V_1;
		RuntimeTypeHandle_t3027515415  L_193 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_194 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_193, /*hidden argument*/NULL);
		NullCheck(L_192);
		HashSet_1_Add_m3527721678(L_192, L_194, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_195 = V_1;
		RuntimeTypeHandle_t3027515415  L_196 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_197 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_196, /*hidden argument*/NULL);
		NullCheck(L_195);
		HashSet_1_Add_m3527721678(L_195, L_197, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_198 = V_1;
		NullCheck(L_176);
		Dictionary_2_Add_m3699546241(L_176, L_178, L_198, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_199 = V_0;
		RuntimeTypeHandle_t3027515415  L_200 = { reinterpret_cast<intptr_t> (UInt64_t4134040092_0_0_0_var) };
		Type_t * L_201 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_200, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_202 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_202, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_202;
		HashSet_1_t1048894234 * L_203 = V_1;
		RuntimeTypeHandle_t3027515415  L_204 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_205 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_204, /*hidden argument*/NULL);
		NullCheck(L_203);
		HashSet_1_Add_m3527721678(L_203, L_205, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_206 = V_1;
		RuntimeTypeHandle_t3027515415  L_207 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_208 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_207, /*hidden argument*/NULL);
		NullCheck(L_206);
		HashSet_1_Add_m3527721678(L_206, L_208, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_209 = V_1;
		RuntimeTypeHandle_t3027515415  L_210 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_211 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_210, /*hidden argument*/NULL);
		NullCheck(L_209);
		HashSet_1_Add_m3527721678(L_209, L_211, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_212 = V_1;
		NullCheck(L_199);
		Dictionary_2_Add_m3699546241(L_199, L_201, L_212, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_213 = V_0;
		RuntimeTypeHandle_t3027515415  L_214 = { reinterpret_cast<intptr_t> (UInt32_t2560061978_0_0_0_var) };
		Type_t * L_215 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_214, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_216 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_216, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_216;
		HashSet_1_t1048894234 * L_217 = V_1;
		RuntimeTypeHandle_t3027515415  L_218 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_219 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_218, /*hidden argument*/NULL);
		NullCheck(L_217);
		HashSet_1_Add_m3527721678(L_217, L_219, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_220 = V_1;
		RuntimeTypeHandle_t3027515415  L_221 = { reinterpret_cast<intptr_t> (UInt64_t4134040092_0_0_0_var) };
		Type_t * L_222 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_221, /*hidden argument*/NULL);
		NullCheck(L_220);
		HashSet_1_Add_m3527721678(L_220, L_222, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_223 = V_1;
		RuntimeTypeHandle_t3027515415  L_224 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_225 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_224, /*hidden argument*/NULL);
		NullCheck(L_223);
		HashSet_1_Add_m3527721678(L_223, L_225, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_226 = V_1;
		RuntimeTypeHandle_t3027515415  L_227 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_228 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_227, /*hidden argument*/NULL);
		NullCheck(L_226);
		HashSet_1_Add_m3527721678(L_226, L_228, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_229 = V_1;
		RuntimeTypeHandle_t3027515415  L_230 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_231 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_230, /*hidden argument*/NULL);
		NullCheck(L_229);
		HashSet_1_Add_m3527721678(L_229, L_231, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_232 = V_1;
		NullCheck(L_213);
		Dictionary_2_Add_m3699546241(L_213, L_215, L_232, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_233 = V_0;
		RuntimeTypeHandle_t3027515415  L_234 = { reinterpret_cast<intptr_t> (UInt16_t2177724958_0_0_0_var) };
		Type_t * L_235 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_234, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_236 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_236, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_236;
		HashSet_1_t1048894234 * L_237 = V_1;
		RuntimeTypeHandle_t3027515415  L_238 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_239 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_238, /*hidden argument*/NULL);
		NullCheck(L_237);
		HashSet_1_Add_m3527721678(L_237, L_239, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_240 = V_1;
		RuntimeTypeHandle_t3027515415  L_241 = { reinterpret_cast<intptr_t> (UInt32_t2560061978_0_0_0_var) };
		Type_t * L_242 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_241, /*hidden argument*/NULL);
		NullCheck(L_240);
		HashSet_1_Add_m3527721678(L_240, L_242, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_243 = V_1;
		RuntimeTypeHandle_t3027515415  L_244 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_245 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_244, /*hidden argument*/NULL);
		NullCheck(L_243);
		HashSet_1_Add_m3527721678(L_243, L_245, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_246 = V_1;
		RuntimeTypeHandle_t3027515415  L_247 = { reinterpret_cast<intptr_t> (UInt64_t4134040092_0_0_0_var) };
		Type_t * L_248 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_247, /*hidden argument*/NULL);
		NullCheck(L_246);
		HashSet_1_Add_m3527721678(L_246, L_248, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_249 = V_1;
		RuntimeTypeHandle_t3027515415  L_250 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_251 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_250, /*hidden argument*/NULL);
		NullCheck(L_249);
		HashSet_1_Add_m3527721678(L_249, L_251, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_252 = V_1;
		RuntimeTypeHandle_t3027515415  L_253 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_254 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_253, /*hidden argument*/NULL);
		NullCheck(L_252);
		HashSet_1_Add_m3527721678(L_252, L_254, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_255 = V_1;
		RuntimeTypeHandle_t3027515415  L_256 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_257 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_256, /*hidden argument*/NULL);
		NullCheck(L_255);
		HashSet_1_Add_m3527721678(L_255, L_257, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_258 = V_1;
		NullCheck(L_233);
		Dictionary_2_Add_m3699546241(L_233, L_235, L_258, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_259 = V_0;
		RuntimeTypeHandle_t3027515415  L_260 = { reinterpret_cast<intptr_t> (Byte_t1134296376_0_0_0_var) };
		Type_t * L_261 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_260, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_262 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_262, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_262;
		HashSet_1_t1048894234 * L_263 = V_1;
		RuntimeTypeHandle_t3027515415  L_264 = { reinterpret_cast<intptr_t> (Int16_t2552820387_0_0_0_var) };
		Type_t * L_265 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_264, /*hidden argument*/NULL);
		NullCheck(L_263);
		HashSet_1_Add_m3527721678(L_263, L_265, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_266 = V_1;
		RuntimeTypeHandle_t3027515415  L_267 = { reinterpret_cast<intptr_t> (UInt16_t2177724958_0_0_0_var) };
		Type_t * L_268 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_267, /*hidden argument*/NULL);
		NullCheck(L_266);
		HashSet_1_Add_m3527721678(L_266, L_268, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_269 = V_1;
		RuntimeTypeHandle_t3027515415  L_270 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_271 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_270, /*hidden argument*/NULL);
		NullCheck(L_269);
		HashSet_1_Add_m3527721678(L_269, L_271, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_272 = V_1;
		RuntimeTypeHandle_t3027515415  L_273 = { reinterpret_cast<intptr_t> (UInt32_t2560061978_0_0_0_var) };
		Type_t * L_274 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_273, /*hidden argument*/NULL);
		NullCheck(L_272);
		HashSet_1_Add_m3527721678(L_272, L_274, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_275 = V_1;
		RuntimeTypeHandle_t3027515415  L_276 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_277 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_276, /*hidden argument*/NULL);
		NullCheck(L_275);
		HashSet_1_Add_m3527721678(L_275, L_277, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_278 = V_1;
		RuntimeTypeHandle_t3027515415  L_279 = { reinterpret_cast<intptr_t> (UInt64_t4134040092_0_0_0_var) };
		Type_t * L_280 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_279, /*hidden argument*/NULL);
		NullCheck(L_278);
		HashSet_1_Add_m3527721678(L_278, L_280, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_281 = V_1;
		RuntimeTypeHandle_t3027515415  L_282 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_283 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_282, /*hidden argument*/NULL);
		NullCheck(L_281);
		HashSet_1_Add_m3527721678(L_281, L_283, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_284 = V_1;
		RuntimeTypeHandle_t3027515415  L_285 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_286 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_285, /*hidden argument*/NULL);
		NullCheck(L_284);
		HashSet_1_Add_m3527721678(L_284, L_286, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_287 = V_1;
		RuntimeTypeHandle_t3027515415  L_288 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_289 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_288, /*hidden argument*/NULL);
		NullCheck(L_287);
		HashSet_1_Add_m3527721678(L_287, L_289, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_290 = V_1;
		NullCheck(L_259);
		Dictionary_2_Add_m3699546241(L_259, L_261, L_290, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_291 = V_0;
		RuntimeTypeHandle_t3027515415  L_292 = { reinterpret_cast<intptr_t> (Char_t3634460470_0_0_0_var) };
		Type_t * L_293 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_292, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_294 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_294, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_294;
		HashSet_1_t1048894234 * L_295 = V_1;
		RuntimeTypeHandle_t3027515415  L_296 = { reinterpret_cast<intptr_t> (UInt16_t2177724958_0_0_0_var) };
		Type_t * L_297 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_296, /*hidden argument*/NULL);
		NullCheck(L_295);
		HashSet_1_Add_m3527721678(L_295, L_297, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_298 = V_1;
		RuntimeTypeHandle_t3027515415  L_299 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_300 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_299, /*hidden argument*/NULL);
		NullCheck(L_298);
		HashSet_1_Add_m3527721678(L_298, L_300, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_301 = V_1;
		RuntimeTypeHandle_t3027515415  L_302 = { reinterpret_cast<intptr_t> (UInt32_t2560061978_0_0_0_var) };
		Type_t * L_303 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_302, /*hidden argument*/NULL);
		NullCheck(L_301);
		HashSet_1_Add_m3527721678(L_301, L_303, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_304 = V_1;
		RuntimeTypeHandle_t3027515415  L_305 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_306 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_305, /*hidden argument*/NULL);
		NullCheck(L_304);
		HashSet_1_Add_m3527721678(L_304, L_306, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_307 = V_1;
		RuntimeTypeHandle_t3027515415  L_308 = { reinterpret_cast<intptr_t> (UInt64_t4134040092_0_0_0_var) };
		Type_t * L_309 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_308, /*hidden argument*/NULL);
		NullCheck(L_307);
		HashSet_1_Add_m3527721678(L_307, L_309, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_310 = V_1;
		RuntimeTypeHandle_t3027515415  L_311 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_312 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_311, /*hidden argument*/NULL);
		NullCheck(L_310);
		HashSet_1_Add_m3527721678(L_310, L_312, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_313 = V_1;
		RuntimeTypeHandle_t3027515415  L_314 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_315 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_314, /*hidden argument*/NULL);
		NullCheck(L_313);
		HashSet_1_Add_m3527721678(L_313, L_315, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_316 = V_1;
		RuntimeTypeHandle_t3027515415  L_317 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_318 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_317, /*hidden argument*/NULL);
		NullCheck(L_316);
		HashSet_1_Add_m3527721678(L_316, L_318, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_319 = V_1;
		NullCheck(L_291);
		Dictionary_2_Add_m3699546241(L_291, L_293, L_319, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_320 = V_0;
		RuntimeTypeHandle_t3027515415  L_321 = { reinterpret_cast<intptr_t> (Boolean_t97287965_0_0_0_var) };
		Type_t * L_322 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_321, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_323 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_323, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		NullCheck(L_320);
		Dictionary_2_Add_m3699546241(L_320, L_322, L_323, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_324 = V_0;
		RuntimeTypeHandle_t3027515415  L_325 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_326 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_325, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_327 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_327, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		NullCheck(L_324);
		Dictionary_2_Add_m3699546241(L_324, L_326, L_327, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_328 = V_0;
		RuntimeTypeHandle_t3027515415  L_329 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_330 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_329, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_331 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_331, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_331;
		HashSet_1_t1048894234 * L_332 = V_1;
		RuntimeTypeHandle_t3027515415  L_333 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_334 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_333, /*hidden argument*/NULL);
		NullCheck(L_332);
		HashSet_1_Add_m3527721678(L_332, L_334, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_335 = V_1;
		NullCheck(L_328);
		Dictionary_2_Add_m3699546241(L_328, L_330, L_335, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_336 = V_0;
		RuntimeTypeHandle_t3027515415  L_337 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_338 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_337, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_339 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_339, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		NullCheck(L_336);
		Dictionary_2_Add_m3699546241(L_336, L_338, L_339, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_340 = V_0;
		RuntimeTypeHandle_t3027515415  L_341 = { reinterpret_cast<intptr_t> (IntPtr_t_0_0_0_var) };
		Type_t * L_342 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_341, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_343 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_343, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		NullCheck(L_340);
		Dictionary_2_Add_m3699546241(L_340, L_342, L_343, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_344 = V_0;
		RuntimeTypeHandle_t3027515415  L_345 = { reinterpret_cast<intptr_t> (UIntPtr_t_0_0_0_var) };
		Type_t * L_346 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_345, /*hidden argument*/NULL);
		HashSet_1_t1048894234 * L_347 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_347, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		NullCheck(L_344);
		Dictionary_2_Add_m3699546241(L_344, L_346, L_347, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_348 = V_0;
		Type_t * L_349 = ((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->get_VoidPointerType_12();
		HashSet_1_t1048894234 * L_350 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_350, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		NullCheck(L_348);
		Dictionary_2_Add_m3699546241(L_348, L_349, L_350, /*hidden argument*/Dictionary_2_Add_m3699546241_RuntimeMethod_var);
		Dictionary_2_t3493241298 * L_351 = V_0;
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_PrimitiveImplicitCasts_13(L_351);
		HashSet_1_t1048894234 * L_352 = (HashSet_1_t1048894234 *)il2cpp_codegen_object_new(HashSet_1_t1048894234_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3725506494(L_352, /*hidden argument*/HashSet_1__ctor_m3725506494_RuntimeMethod_var);
		V_1 = L_352;
		HashSet_1_t1048894234 * L_353 = V_1;
		RuntimeTypeHandle_t3027515415  L_354 = { reinterpret_cast<intptr_t> (Int64_t3736567304_0_0_0_var) };
		Type_t * L_355 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_354, /*hidden argument*/NULL);
		NullCheck(L_353);
		HashSet_1_Add_m3527721678(L_353, L_355, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_356 = V_1;
		RuntimeTypeHandle_t3027515415  L_357 = { reinterpret_cast<intptr_t> (Int32_t2950945753_0_0_0_var) };
		Type_t * L_358 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_357, /*hidden argument*/NULL);
		NullCheck(L_356);
		HashSet_1_Add_m3527721678(L_356, L_358, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_359 = V_1;
		RuntimeTypeHandle_t3027515415  L_360 = { reinterpret_cast<intptr_t> (Int16_t2552820387_0_0_0_var) };
		Type_t * L_361 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_360, /*hidden argument*/NULL);
		NullCheck(L_359);
		HashSet_1_Add_m3527721678(L_359, L_361, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_362 = V_1;
		RuntimeTypeHandle_t3027515415  L_363 = { reinterpret_cast<intptr_t> (SByte_t1669577662_0_0_0_var) };
		Type_t * L_364 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_363, /*hidden argument*/NULL);
		NullCheck(L_362);
		HashSet_1_Add_m3527721678(L_362, L_364, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_365 = V_1;
		RuntimeTypeHandle_t3027515415  L_366 = { reinterpret_cast<intptr_t> (UInt64_t4134040092_0_0_0_var) };
		Type_t * L_367 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_366, /*hidden argument*/NULL);
		NullCheck(L_365);
		HashSet_1_Add_m3527721678(L_365, L_367, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_368 = V_1;
		RuntimeTypeHandle_t3027515415  L_369 = { reinterpret_cast<intptr_t> (UInt32_t2560061978_0_0_0_var) };
		Type_t * L_370 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_369, /*hidden argument*/NULL);
		NullCheck(L_368);
		HashSet_1_Add_m3527721678(L_368, L_370, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_371 = V_1;
		RuntimeTypeHandle_t3027515415  L_372 = { reinterpret_cast<intptr_t> (UInt16_t2177724958_0_0_0_var) };
		Type_t * L_373 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_372, /*hidden argument*/NULL);
		NullCheck(L_371);
		HashSet_1_Add_m3527721678(L_371, L_373, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_374 = V_1;
		RuntimeTypeHandle_t3027515415  L_375 = { reinterpret_cast<intptr_t> (Byte_t1134296376_0_0_0_var) };
		Type_t * L_376 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_375, /*hidden argument*/NULL);
		NullCheck(L_374);
		HashSet_1_Add_m3527721678(L_374, L_376, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_377 = V_1;
		RuntimeTypeHandle_t3027515415  L_378 = { reinterpret_cast<intptr_t> (Char_t3634460470_0_0_0_var) };
		Type_t * L_379 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_378, /*hidden argument*/NULL);
		NullCheck(L_377);
		HashSet_1_Add_m3527721678(L_377, L_379, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_380 = V_1;
		RuntimeTypeHandle_t3027515415  L_381 = { reinterpret_cast<intptr_t> (Decimal_t2948259380_0_0_0_var) };
		Type_t * L_382 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_381, /*hidden argument*/NULL);
		NullCheck(L_380);
		HashSet_1_Add_m3527721678(L_380, L_382, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_383 = V_1;
		RuntimeTypeHandle_t3027515415  L_384 = { reinterpret_cast<intptr_t> (Single_t1397266774_0_0_0_var) };
		Type_t * L_385 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_384, /*hidden argument*/NULL);
		NullCheck(L_383);
		HashSet_1_Add_m3527721678(L_383, L_385, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_386 = V_1;
		RuntimeTypeHandle_t3027515415  L_387 = { reinterpret_cast<intptr_t> (Double_t594665363_0_0_0_var) };
		Type_t * L_388 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_387, /*hidden argument*/NULL);
		NullCheck(L_386);
		HashSet_1_Add_m3527721678(L_386, L_388, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_389 = V_1;
		RuntimeTypeHandle_t3027515415  L_390 = { reinterpret_cast<intptr_t> (IntPtr_t_0_0_0_var) };
		Type_t * L_391 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_390, /*hidden argument*/NULL);
		NullCheck(L_389);
		HashSet_1_Add_m3527721678(L_389, L_391, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_392 = V_1;
		RuntimeTypeHandle_t3027515415  L_393 = { reinterpret_cast<intptr_t> (UIntPtr_t_0_0_0_var) };
		Type_t * L_394 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_393, /*hidden argument*/NULL);
		NullCheck(L_392);
		HashSet_1_Add_m3527721678(L_392, L_394, /*hidden argument*/HashSet_1_Add_m3527721678_RuntimeMethod_var);
		HashSet_1_t1048894234 * L_395 = V_1;
		((TypeExtensions_t1614484489_StaticFields*)il2cpp_codegen_static_fields_for(TypeExtensions_t1614484489_il2cpp_TypeInfo_var))->set_ExplicitCastIntegrals_14(L_395);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_0__ctor_m759390448 (U3CU3Ec__DisplayClass22_0_t2941737288 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::<GetCastMethodDelegate>b__0(System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m3161334231 (U3CU3Ec__DisplayClass22_0_t2941737288 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m3161334231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodInfo_t * L_0 = __this->get_method_0();
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		RuntimeObject * L_3 = ___obj0;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		NullCheck(L_0);
		RuntimeObject * L_4 = MethodBase_Invoke_m1776411915(L_0, NULL, L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass35_0__ctor_m563200432 (U3CU3Ec__DisplayClass35_0_t2941409609 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::<ImplementsOpenGenericInterface>b__0(System.Type)
extern "C" IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m2349605198 (U3CU3Ec__DisplayClass35_0_t2941409609 * __this, Type_t * ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m2349605198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___i0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(131 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Type_t * L_2 = ___i0;
		Type_t * L_3 = __this->get_openGenericInterfaceType_0();
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
		bool L_4 = TypeExtensions_ImplementsOpenGenericInterface_m3084334200(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass41_0__ctor_m3483633306 (U3CU3Ec__DisplayClass41_0_t2941671758 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::<GetOperatorMethods>b__0(System.Reflection.MethodInfo)
extern "C" IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass41_0_U3CGetOperatorMethodsU3Eb__0_m1754509797 (U3CU3Ec__DisplayClass41_0_t2941671758 * __this, MethodInfo_t * ___x0, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2 = __this->get_methodName_0();
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__42__ctor_m464840207 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		Thread_t2300836069 * L_1 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1068113671(L_1, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_2);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__42_System_IDisposable_Dispose_m3240896829 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CGetAllMembersU3Ed__42_MoveNext_m1030301410 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	MemberInfo_t * V_1 = NULL;
	MemberInfo_t * V_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0077;
			}
			case 2:
			{
				goto IL_00f4;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		Type_t * L_2 = __this->get_type_3();
		__this->set_U3CcurrentTypeU3E5__1_7(L_2);
		int32_t L_3 = __this->get_flags_5();
		if ((!(((uint32_t)((int32_t)((int32_t)L_3&(int32_t)2))) == ((uint32_t)2))))
		{
			goto IL_00a8;
		}
	}
	{
		Type_t * L_4 = __this->get_U3CcurrentTypeU3E5__1_7();
		int32_t L_5 = __this->get_flags_5();
		NullCheck(L_4);
		MemberInfoU5BU5D_t1302094432* L_6 = VirtFuncInvoker1< MemberInfoU5BU5D_t1302094432*, int32_t >::Invoke(101 /* System.Reflection.MemberInfo[] System.Type::GetMembers(System.Reflection.BindingFlags) */, L_4, L_5);
		__this->set_U3CU3E7__wrap1_8(L_6);
		__this->set_U3CU3E7__wrap2_9(0);
		goto IL_008c;
	}

IL_0059:
	{
		MemberInfoU5BU5D_t1302094432* L_7 = __this->get_U3CU3E7__wrap1_8();
		int32_t L_8 = __this->get_U3CU3E7__wrap2_9();
		NullCheck(L_7);
		int32_t L_9 = L_8;
		MemberInfo_t * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_1 = L_10;
		MemberInfo_t * L_11 = V_1;
		__this->set_U3CU3E2__current_1(L_11);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0077:
	{
		__this->set_U3CU3E1__state_0((-1));
		int32_t L_12 = __this->get_U3CU3E7__wrap2_9();
		__this->set_U3CU3E7__wrap2_9(((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)));
	}

IL_008c:
	{
		int32_t L_13 = __this->get_U3CU3E7__wrap2_9();
		MemberInfoU5BU5D_t1302094432* L_14 = __this->get_U3CU3E7__wrap1_8();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))))))
		{
			goto IL_0059;
		}
	}
	{
		__this->set_U3CU3E7__wrap1_8((MemberInfoU5BU5D_t1302094432*)NULL);
		goto IL_013c;
	}

IL_00a8:
	{
		int32_t L_15 = __this->get_flags_5();
		__this->set_flags_5(((int32_t)((int32_t)L_15|(int32_t)2)));
	}

IL_00b6:
	{
		Type_t * L_16 = __this->get_U3CcurrentTypeU3E5__1_7();
		int32_t L_17 = __this->get_flags_5();
		NullCheck(L_16);
		MemberInfoU5BU5D_t1302094432* L_18 = VirtFuncInvoker1< MemberInfoU5BU5D_t1302094432*, int32_t >::Invoke(101 /* System.Reflection.MemberInfo[] System.Type::GetMembers(System.Reflection.BindingFlags) */, L_16, L_17);
		__this->set_U3CU3E7__wrap1_8(L_18);
		__this->set_U3CU3E7__wrap2_9(0);
		goto IL_0109;
	}

IL_00d6:
	{
		MemberInfoU5BU5D_t1302094432* L_19 = __this->get_U3CU3E7__wrap1_8();
		int32_t L_20 = __this->get_U3CU3E7__wrap2_9();
		NullCheck(L_19);
		int32_t L_21 = L_20;
		MemberInfo_t * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_2 = L_22;
		MemberInfo_t * L_23 = V_2;
		__this->set_U3CU3E2__current_1(L_23);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00f4:
	{
		__this->set_U3CU3E1__state_0((-1));
		int32_t L_24 = __this->get_U3CU3E7__wrap2_9();
		__this->set_U3CU3E7__wrap2_9(((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1)));
	}

IL_0109:
	{
		int32_t L_25 = __this->get_U3CU3E7__wrap2_9();
		MemberInfoU5BU5D_t1302094432* L_26 = __this->get_U3CU3E7__wrap1_8();
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length)))))))
		{
			goto IL_00d6;
		}
	}
	{
		__this->set_U3CU3E7__wrap1_8((MemberInfoU5BU5D_t1302094432*)NULL);
		Type_t * L_27 = __this->get_U3CcurrentTypeU3E5__1_7();
		NullCheck(L_27);
		Type_t * L_28 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_27);
		__this->set_U3CcurrentTypeU3E5__1_7(L_28);
		Type_t * L_29 = __this->get_U3CcurrentTypeU3E5__1_7();
		if (L_29)
		{
			goto IL_00b6;
		}
	}

IL_013c:
	{
		return (bool)0;
	}
}
// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern "C" IL2CPP_METHOD_ATTR MemberInfo_t * U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m2233689901 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	{
		MemberInfo_t * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_m3603743745 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_m3603743745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_m3603743745_RuntimeMethod_var);
	}
}
// System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_get_Current_m482783446 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	{
		MemberInfo_t * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1317355286 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1317355286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetAllMembersU3Ed__42_t2156530709 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		Thread_t2300836069 * L_2 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Thread_get_ManagedThreadId_m1068113671(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_002e;
	}

IL_0027:
	{
		U3CGetAllMembersU3Ed__42_t2156530709 * L_4 = (U3CGetAllMembersU3Ed__42_t2156530709 *)il2cpp_codegen_object_new(U3CGetAllMembersU3Ed__42_t2156530709_il2cpp_TypeInfo_var);
		U3CGetAllMembersU3Ed__42__ctor_m464840207(L_4, 0, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002e:
	{
		U3CGetAllMembersU3Ed__42_t2156530709 * L_5 = V_0;
		Type_t * L_6 = __this->get_U3CU3E3__type_4();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CGetAllMembersU3Ed__42_t2156530709 * L_7 = V_0;
		int32_t L_8 = __this->get_U3CU3E3__flags_6();
		NullCheck(L_7);
		L_7->set_flags_5(L_8);
		U3CGetAllMembersU3Ed__42_t2156530709 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetAllMembersU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m4205290388 (U3CGetAllMembersU3Ed__42_t2156530709 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1317355286(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43__ctor_m424109788 (U3CGetAllMembersU3Ed__43_t590446768 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		Thread_t2300836069 * L_1 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1068113671(L_1, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_2);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_m1829376475 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m3985260114(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CGetAllMembersU3Ed__43_MoveNext_m3659538151 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMembersU3Ed__43_MoveNext_m3659538151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	MemberInfo_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			int32_t L_1 = V_1;
			if (!L_1)
			{
				goto IL_0015;
			}
		}

IL_000a:
		{
			int32_t L_2 = V_1;
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0073;
			}
		}

IL_000e:
		{
			V_0 = (bool)0;
			goto IL_00a0;
		}

IL_0015:
		{
			__this->set_U3CU3E1__state_0((-1));
			Type_t * L_3 = __this->get_type_3();
			int32_t L_4 = __this->get_flags_5();
			IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t1614484489_il2cpp_TypeInfo_var);
			RuntimeObject* L_5 = TypeExtensions_GetAllMembers_m569881445(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			RuntimeObject* L_6 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>::GetEnumerator() */, IEnumerable_1_t2359854630_il2cpp_TypeInfo_var, L_5);
			__this->set_U3CU3E7__wrap1_9(L_6);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_007b;
		}

IL_0042:
		{
			RuntimeObject* L_7 = __this->get_U3CU3E7__wrap1_9();
			NullCheck(L_7);
			MemberInfo_t * L_8 = InterfaceFuncInvoker0< MemberInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>::get_Current() */, IEnumerator_1_t3812572209_il2cpp_TypeInfo_var, L_7);
			V_2 = L_8;
			MemberInfo_t * L_9 = V_2;
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(14 /* System.String System.Reflection.MemberInfo::get_Name() */, L_9);
			String_t* L_11 = __this->get_name_7();
			bool L_12 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_007b;
			}
		}

IL_0061:
		{
			MemberInfo_t * L_13 = V_2;
			__this->set_U3CU3E2__current_1(L_13);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_00a0;
		}

IL_0073:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
		}

IL_007b:
		{
			RuntimeObject* L_14 = __this->get_U3CU3E7__wrap1_9();
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0042;
			}
		}

IL_0088:
		{
			U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m3985260114(__this, /*hidden argument*/NULL);
			__this->set_U3CU3E7__wrap1_9((RuntimeObject*)NULL);
			V_0 = (bool)0;
			goto IL_00a0;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_0099;
	}

FAULT_0099:
	{ // begin fault (depth: 1)
		U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_m1829376475(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(153)
	} // end fault
	IL2CPP_CLEANUP(153)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00a0:
	{
		bool L_16 = V_0;
		return L_16;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>m__Finally1()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m3985260114 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m3985260114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		RuntimeObject* L_0 = __this->get_U3CU3E7__wrap1_9();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_U3CU3E7__wrap1_9();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern "C" IL2CPP_METHOD_ATTR MemberInfo_t * U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m4148237789 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	{
		MemberInfo_t * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m2438628648 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m2438628648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m2438628648_RuntimeMethod_var);
	}
}
// System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_get_Current_m3863384546 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	{
		MemberInfo_t * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1610761930 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1610761930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetAllMembersU3Ed__43_t590446768 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		Thread_t2300836069 * L_2 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Thread_get_ManagedThreadId_m1068113671(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_002e;
	}

IL_0027:
	{
		U3CGetAllMembersU3Ed__43_t590446768 * L_4 = (U3CGetAllMembersU3Ed__43_t590446768 *)il2cpp_codegen_object_new(U3CGetAllMembersU3Ed__43_t590446768_il2cpp_TypeInfo_var);
		U3CGetAllMembersU3Ed__43__ctor_m424109788(L_4, 0, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002e:
	{
		U3CGetAllMembersU3Ed__43_t590446768 * L_5 = V_0;
		Type_t * L_6 = __this->get_U3CU3E3__type_4();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CGetAllMembersU3Ed__43_t590446768 * L_7 = V_0;
		String_t* L_8 = __this->get_U3CU3E3__name_8();
		NullCheck(L_7);
		L_7->set_name_7(L_8);
		U3CGetAllMembersU3Ed__43_t590446768 * L_9 = V_0;
		int32_t L_10 = __this->get_U3CU3E3__flags_6();
		NullCheck(L_9);
		L_9->set_flags_5(L_10);
		U3CGetAllMembersU3Ed__43_t590446768 * L_11 = V_0;
		return L_11;
	}
}
// System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetAllMembersU3Ed__43_System_Collections_IEnumerable_GetEnumerator_m1661918760 (U3CGetAllMembersU3Ed__43_t590446768 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m1610761930(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CGetBaseClassesU3Ed__48__ctor_m3292792360 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		Thread_t2300836069 * L_1 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1068113671(L_1, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_2);
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CGetBaseClassesU3Ed__48_System_IDisposable_Dispose_m1885819949 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CGetBaseClassesU3Ed__48_MoveNext_m2085731429 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0056;
			}
			case 2:
			{
				goto IL_0085;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		Type_t * L_2 = __this->get_type_3();
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Type_t * L_3 = __this->get_type_3();
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_3);
		if (L_4)
		{
			goto IL_0039;
		}
	}

IL_0037:
	{
		return (bool)0;
	}

IL_0039:
	{
		bool L_5 = __this->get_includeSelf_5();
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		Type_t * L_6 = __this->get_type_3();
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0056:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_005d:
	{
		Type_t * L_7 = __this->get_type_3();
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_7);
		__this->set_U3CcurrentU3E5__1_7(L_8);
		goto IL_009d;
	}

IL_0070:
	{
		Type_t * L_9 = __this->get_U3CcurrentU3E5__1_7();
		__this->set_U3CU3E2__current_1(L_9);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0085:
	{
		__this->set_U3CU3E1__state_0((-1));
		Type_t * L_10 = __this->get_U3CcurrentU3E5__1_7();
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(58 /* System.Type System.Type::get_BaseType() */, L_10);
		__this->set_U3CcurrentU3E5__1_7(L_11);
	}

IL_009d:
	{
		Type_t * L_12 = __this->get_U3CcurrentU3E5__1_7();
		if (L_12)
		{
			goto IL_0070;
		}
	}
	{
		return (bool)0;
	}
}
// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C" IL2CPP_METHOD_ATTR Type_t * U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m1243210188 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_m3832819096 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_m3832819096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_m3832819096_RuntimeMethod_var);
	}
}
// System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_get_Current_m3747182959 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3491823437 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3491823437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetBaseClassesU3Ed__48_t3606501197 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		Thread_t2300836069 * L_2 = Thread_get_CurrentThread_m4142136012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Thread_get_ManagedThreadId_m1068113671(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_002e;
	}

IL_0027:
	{
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_4 = (U3CGetBaseClassesU3Ed__48_t3606501197 *)il2cpp_codegen_object_new(U3CGetBaseClassesU3Ed__48_t3606501197_il2cpp_TypeInfo_var);
		U3CGetBaseClassesU3Ed__48__ctor_m3292792360(L_4, 0, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002e:
	{
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_5 = V_0;
		Type_t * L_6 = __this->get_U3CU3E3__type_4();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_7 = V_0;
		bool L_8 = __this->get_U3CU3E3__includeSelf_6();
		NullCheck(L_7);
		L_7->set_includeSelf_5(L_8);
		U3CGetBaseClassesU3Ed__48_t3606501197 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.IEnumerable.GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m3563964960 (U3CGetBaseClassesU3Ed__48_t3606501197 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3491823437(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.UnityExtensions::.cctor()
extern "C" IL2CPP_METHOD_ATTR void UnityExtensions__cctor_m4112108510 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityExtensions__cctor_m4112108510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FieldInfo_t * V_0 = NULL;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Object_t631007953_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		FieldInfo_t * L_2 = VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(73 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_1, _stringLiteral3226397847, ((int32_t)52));
		V_0 = L_2;
		FieldInfo_t * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		FieldInfo_t * L_4 = V_0;
		ValueGetter_2_t1938735483 * L_5 = EmitUtilities_CreateInstanceFieldGetter_TisObject_t631007953_TisIntPtr_t_m3752444982(NULL /*static, unused*/, L_4, /*hidden argument*/EmitUtilities_CreateInstanceFieldGetter_TisObject_t631007953_TisIntPtr_t_m3752444982_RuntimeMethod_var);
		((UnityExtensions_t2928681996_StaticFields*)il2cpp_codegen_static_fields_for(UnityExtensions_t2928681996_il2cpp_TypeInfo_var))->set_UnityObjectCachedPtrFieldGetter_0(L_5);
	}

IL_0025:
	{
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.UnityExtensions::SafeIsUnityNull(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool UnityExtensions_SafeIsUnityNull_m135202044 (RuntimeObject * __this /* static, unused */, Object_t631007953 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityExtensions_SafeIsUnityNull_m135202044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object_t631007953 * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}

IL_0005:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityExtensions_t2928681996_il2cpp_TypeInfo_var);
		ValueGetter_2_t1938735483 * L_1 = ((UnityExtensions_t2928681996_StaticFields*)il2cpp_codegen_static_fields_for(UnityExtensions_t2928681996_il2cpp_TypeInfo_var))->get_UnityObjectCachedPtrFieldGetter_0();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		NotSupportedException_t1314879016 * L_2 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_2, _stringLiteral862263470, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, UnityExtensions_SafeIsUnityNull_m135202044_RuntimeMethod_var);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityExtensions_t2928681996_il2cpp_TypeInfo_var);
		ValueGetter_2_t1938735483 * L_3 = ((UnityExtensions_t2928681996_StaticFields*)il2cpp_codegen_static_fields_for(UnityExtensions_t2928681996_il2cpp_TypeInfo_var))->get_UnityObjectCachedPtrFieldGetter_0();
		NullCheck(L_3);
		intptr_t L_4 = ValueGetter_2_Invoke_m1728989122(L_3, (Object_t631007953 **)(&___obj0), /*hidden argument*/ValueGetter_2_Invoke_m1728989122_RuntimeMethod_var);
		V_0 = (intptr_t)L_4;
		intptr_t L_5 = V_0;
		bool L_6 = IntPtr_op_Equality_m408849716(NULL /*static, unused*/, (intptr_t)L_5, (intptr_t)(0), /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.UnityVersion::.cctor()
extern "C" IL2CPP_METHOD_ATTR void UnityVersion__cctor_m2678664592 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityVersion__cctor_m2678664592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		String_t* L_0 = Application_get_unityVersion_m1068543125(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_1 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)46));
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_3 = String_Split_m3646115398(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		StringU5BU5D_t1281789340* L_4 = V_0;
		NullCheck(L_4);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_5 = Application_get_unityVersion_m1068543125(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3664608116, L_5, _stringLiteral3547057148, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0036:
	{
		StringU5BU5D_t1281789340* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 0;
		String_t* L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		bool L_10 = Int32_TryParse_m2404707562(NULL /*static, unused*/, L_9, (int32_t*)(((UnityVersion_t744152913_StaticFields*)il2cpp_codegen_static_fields_for(UnityVersion_t744152913_il2cpp_TypeInfo_var))->get_address_of_Major_0()), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_007b;
		}
	}
	{
		StringU5BU5D_t1281789340* L_11 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral921725149);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral921725149);
		StringU5BU5D_t1281789340* L_13 = L_12;
		StringU5BU5D_t1281789340* L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		String_t* L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_16);
		StringU5BU5D_t1281789340* L_17 = L_13;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral4285936906);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4285936906);
		StringU5BU5D_t1281789340* L_18 = L_17;
		String_t* L_19 = Application_get_unityVersion_m1068543125(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_19);
		StringU5BU5D_t1281789340* L_20 = L_18;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral3450648441);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3450648441);
		String_t* L_21 = String_Concat_m1809518182(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_007b:
	{
		StringU5BU5D_t1281789340* L_22 = V_0;
		NullCheck(L_22);
		int32_t L_23 = 1;
		String_t* L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		bool L_25 = Int32_TryParse_m2404707562(NULL /*static, unused*/, L_24, (int32_t*)(((UnityVersion_t744152913_StaticFields*)il2cpp_codegen_static_fields_for(UnityVersion_t744152913_il2cpp_TypeInfo_var))->get_address_of_Minor_1()), /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00c0;
		}
	}
	{
		StringU5BU5D_t1281789340* L_26 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral4201940553);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4201940553);
		StringU5BU5D_t1281789340* L_28 = L_27;
		StringU5BU5D_t1281789340* L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = 1;
		String_t* L_31 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_31);
		StringU5BU5D_t1281789340* L_32 = L_28;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral4285936906);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4285936906);
		StringU5BU5D_t1281789340* L_33 = L_32;
		String_t* L_34 = Application_get_unityVersion_m1068543125(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_34);
		StringU5BU5D_t1281789340* L_35 = L_33;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral3450648441);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3450648441);
		String_t* L_36 = String_Concat_m1809518182(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		return;
	}
}
// System.Void Sirenix.Serialization.Utilities.UnityVersion::EnsureLoaded()
extern "C" IL2CPP_METHOD_ATTR void UnityVersion_EnsureLoaded_m3326089844 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Sirenix.Serialization.Utilities.UnityVersion::IsVersionOrGreater(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool UnityVersion_IsVersionOrGreater_m588543013 (RuntimeObject * __this /* static, unused */, int32_t ___major0, int32_t ___minor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityVersion_IsVersionOrGreater_m588543013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityVersion_t744152913_il2cpp_TypeInfo_var);
		int32_t L_0 = ((UnityVersion_t744152913_StaticFields*)il2cpp_codegen_static_fields_for(UnityVersion_t744152913_il2cpp_TypeInfo_var))->get_Major_0();
		int32_t L_1 = ___major0;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityVersion_t744152913_il2cpp_TypeInfo_var);
		int32_t L_2 = ((UnityVersion_t744152913_StaticFields*)il2cpp_codegen_static_fields_for(UnityVersion_t744152913_il2cpp_TypeInfo_var))->get_Major_0();
		int32_t L_3 = ___major0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityVersion_t744152913_il2cpp_TypeInfo_var);
		int32_t L_4 = ((UnityVersion_t744152913_StaticFields*)il2cpp_codegen_static_fields_for(UnityVersion_t744152913_il2cpp_TypeInfo_var))->get_Minor_1();
		int32_t L_5 = ___minor1;
		return (bool)((((int32_t)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_001c:
	{
		return (bool)0;
	}

IL_001e:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StringFromBytes(System.Byte[],System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR String_t* UnsafeUtilities_StringFromBytes_m1754034413 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___buffer0, int32_t ___charLength1, bool ___needs16BitSupport2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnsafeUtilities_StringFromBytes_m1754034413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GCHandle_t3351438187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Il2CppChar* V_3 = NULL;
	String_t* V_4 = NULL;
	uint16_t* V_5 = NULL;
	uint16_t* V_6 = NULL;
	intptr_t V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	Il2CppChar* V_9 = NULL;
	String_t* V_10 = NULL;
	uint8_t* V_11 = NULL;
	uint8_t* V_12 = NULL;
	int32_t V_13 = 0;
	Il2CppChar* V_14 = NULL;
	String_t* V_15 = NULL;
	uint8_t* V_16 = NULL;
	uint8_t* V_17 = NULL;
	int32_t V_18 = 0;
	Il2CppChar* V_19 = NULL;
	String_t* V_20 = NULL;
	uint8_t* V_21 = NULL;
	uint8_t* V_22 = NULL;
	int32_t V_23 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___needs16BitSupport2;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_1 = ___charLength1;
		G_B3_0 = L_1;
		goto IL_0009;
	}

IL_0006:
	{
		int32_t L_2 = ___charLength1;
		G_B3_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_2, (int32_t)2));
	}

IL_0009:
	{
		V_0 = G_B3_0;
		ByteU5BU5D_t4116647657* L_3 = ___buffer0;
		NullCheck(L_3);
		int32_t L_4 = V_0;
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) >= ((int32_t)L_4)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_6);
		String_t* L_8 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral3739023119, L_7, _stringLiteral1929072793, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_9 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, NULL, UnsafeUtilities_StringFromBytes_m1754034413_RuntimeMethod_var);
	}

IL_002b:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(GCHandle_t3351438187 ));
		int32_t L_10 = ___charLength1;
		String_t* L_11 = String_CreateString_m1262864254(NULL, 0, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t4116647657* L_12 = ___buffer0;
			GCHandle_t3351438187  L_13 = GCHandle_Alloc_m3823409740(NULL /*static, unused*/, (RuntimeObject *)(RuntimeObject *)L_12, 3, /*hidden argument*/NULL);
			V_1 = L_13;
			bool L_14 = ___needs16BitSupport2;
			if (!L_14)
			{
				goto IL_0102;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
			bool L_15 = ((BitConverter_t3118986983_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t3118986983_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
			if (!L_15)
			{
				goto IL_00a0;
			}
		}

IL_0050:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_16 = V_2;
				V_4 = L_16;
				String_t* L_17 = V_4;
				V_3 = (Il2CppChar*)(((intptr_t)L_17));
				Il2CppChar* L_18 = V_3;
				if (!L_18)
				{
					goto IL_0062;
				}
			}

IL_005a:
			{
				Il2CppChar* L_19 = V_3;
				int32_t L_20 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_3 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_19, (int32_t)L_20));
			}

IL_0062:
			{
				intptr_t L_21 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_7 = (intptr_t)L_21;
				void* L_22 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_7), /*hidden argument*/NULL);
				V_5 = (uint16_t*)L_22;
				Il2CppChar* L_23 = V_3;
				V_6 = (uint16_t*)L_23;
				V_8 = 0;
				goto IL_0092;
			}

IL_007c:
			{
				uint16_t* L_24 = V_6;
				uint16_t* L_25 = (uint16_t*)L_24;
				V_6 = (uint16_t*)((uint16_t*)il2cpp_codegen_add((intptr_t)L_25, (int32_t)2));
				uint16_t* L_26 = V_5;
				uint16_t* L_27 = (uint16_t*)L_26;
				V_5 = (uint16_t*)((uint16_t*)il2cpp_codegen_add((intptr_t)L_27, (int32_t)2));
				int32_t L_28 = *((uint16_t*)L_27);
				*((int16_t*)L_25) = (int16_t)L_28;
				int32_t L_29 = V_8;
				V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)2));
			}

IL_0092:
			{
				int32_t L_30 = V_8;
				int32_t L_31 = V_0;
				if ((((int32_t)L_30) < ((int32_t)L_31)))
				{
					goto IL_007c;
				}
			}

IL_0097:
			{
				IL2CPP_LEAVE(0x1CB, FINALLY_009c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_009c;
		}

FINALLY_009c:
		{ // begin finally (depth: 2)
			V_4 = (String_t*)NULL;
			IL2CPP_END_FINALLY(156)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(156)
		{
			IL2CPP_END_CLEANUP(0x1CB, FINALLY_01ba);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_00a0:
		{
		}

IL_00a1:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_32 = V_2;
				V_10 = L_32;
				String_t* L_33 = V_10;
				V_9 = (Il2CppChar*)(((intptr_t)L_33));
				Il2CppChar* L_34 = V_9;
				if (!L_34)
				{
					goto IL_00b7;
				}
			}

IL_00ad:
			{
				Il2CppChar* L_35 = V_9;
				int32_t L_36 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_9 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_35, (int32_t)L_36));
			}

IL_00b7:
			{
				intptr_t L_37 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_7 = (intptr_t)L_37;
				void* L_38 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_7), /*hidden argument*/NULL);
				V_11 = (uint8_t*)L_38;
				Il2CppChar* L_39 = V_9;
				V_12 = (uint8_t*)L_39;
				V_13 = 0;
				goto IL_00f4;
			}

IL_00d2:
			{
				uint8_t* L_40 = V_12;
				uint8_t* L_41 = V_11;
				int32_t L_42 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_41, (int32_t)1)));
				*((int8_t*)L_40) = (int8_t)L_42;
				uint8_t* L_43 = V_12;
				uint8_t* L_44 = V_11;
				int32_t L_45 = *((uint8_t*)L_44);
				*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_43, (int32_t)1))) = (int8_t)L_45;
				uint8_t* L_46 = V_11;
				V_11 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_46, (int32_t)2));
				uint8_t* L_47 = V_12;
				V_12 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_47, (int32_t)2));
				int32_t L_48 = V_13;
				V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)2));
			}

IL_00f4:
			{
				int32_t L_49 = V_13;
				int32_t L_50 = V_0;
				if ((((int32_t)L_49) < ((int32_t)L_50)))
				{
					goto IL_00d2;
				}
			}

IL_00f9:
			{
				IL2CPP_LEAVE(0x1CB, FINALLY_00fe);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_00fe;
		}

FINALLY_00fe:
		{ // begin finally (depth: 2)
			V_10 = (String_t*)NULL;
			IL2CPP_END_FINALLY(254)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(254)
		{
			IL2CPP_END_CLEANUP(0x1CB, FINALLY_01ba);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0102:
		{
			IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
			bool L_51 = ((BitConverter_t3118986983_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t3118986983_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
			if (!L_51)
			{
				goto IL_0161;
			}
		}

IL_0109:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_52 = V_2;
				V_15 = L_52;
				String_t* L_53 = V_15;
				V_14 = (Il2CppChar*)(((intptr_t)L_53));
				Il2CppChar* L_54 = V_14;
				if (!L_54)
				{
					goto IL_011f;
				}
			}

IL_0115:
			{
				Il2CppChar* L_55 = V_14;
				int32_t L_56 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_14 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_55, (int32_t)L_56));
			}

IL_011f:
			{
				intptr_t L_57 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_7 = (intptr_t)L_57;
				void* L_58 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_7), /*hidden argument*/NULL);
				V_16 = (uint8_t*)L_58;
				Il2CppChar* L_59 = V_14;
				V_17 = (uint8_t*)L_59;
				V_18 = 0;
				goto IL_0156;
			}

IL_013a:
			{
				uint8_t* L_60 = V_17;
				uint8_t* L_61 = (uint8_t*)L_60;
				V_17 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_61, (int32_t)1));
				uint8_t* L_62 = V_16;
				uint8_t* L_63 = (uint8_t*)L_62;
				V_16 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_63, (int32_t)1));
				int32_t L_64 = *((uint8_t*)L_63);
				*((int8_t*)L_61) = (int8_t)L_64;
				uint8_t* L_65 = V_17;
				V_17 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_65, (int32_t)1));
				int32_t L_66 = V_18;
				V_18 = ((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1));
			}

IL_0156:
			{
				int32_t L_67 = V_18;
				int32_t L_68 = V_0;
				if ((((int32_t)L_67) < ((int32_t)L_68)))
				{
					goto IL_013a;
				}
			}

IL_015b:
			{
				IL2CPP_LEAVE(0x1CB, FINALLY_015d);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_015d;
		}

FINALLY_015d:
		{ // begin finally (depth: 2)
			V_15 = (String_t*)NULL;
			IL2CPP_END_FINALLY(349)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(349)
		{
			IL2CPP_END_CLEANUP(0x1CB, FINALLY_01ba);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0161:
		{
		}

IL_0162:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_69 = V_2;
				V_20 = L_69;
				String_t* L_70 = V_20;
				V_19 = (Il2CppChar*)(((intptr_t)L_70));
				Il2CppChar* L_71 = V_19;
				if (!L_71)
				{
					goto IL_0178;
				}
			}

IL_016e:
			{
				Il2CppChar* L_72 = V_19;
				int32_t L_73 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_19 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_72, (int32_t)L_73));
			}

IL_0178:
			{
				intptr_t L_74 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_7 = (intptr_t)L_74;
				void* L_75 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_7), /*hidden argument*/NULL);
				V_21 = (uint8_t*)L_75;
				Il2CppChar* L_76 = V_19;
				V_22 = (uint8_t*)L_76;
				V_23 = 0;
				goto IL_01af;
			}

IL_0193:
			{
				uint8_t* L_77 = V_22;
				V_22 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_77, (int32_t)1));
				uint8_t* L_78 = V_22;
				uint8_t* L_79 = (uint8_t*)L_78;
				V_22 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_79, (int32_t)1));
				uint8_t* L_80 = V_21;
				uint8_t* L_81 = (uint8_t*)L_80;
				V_21 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_81, (int32_t)1));
				int32_t L_82 = *((uint8_t*)L_81);
				*((int8_t*)L_79) = (int8_t)L_82;
				int32_t L_83 = V_23;
				V_23 = ((int32_t)il2cpp_codegen_add((int32_t)L_83, (int32_t)1));
			}

IL_01af:
			{
				int32_t L_84 = V_23;
				int32_t L_85 = V_0;
				if ((((int32_t)L_84) < ((int32_t)L_85)))
				{
					goto IL_0193;
				}
			}

IL_01b4:
			{
				IL2CPP_LEAVE(0x1CB, FINALLY_01b6);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_01b6;
		}

FINALLY_01b6:
		{ // begin finally (depth: 2)
			V_20 = (String_t*)NULL;
			IL2CPP_END_FINALLY(438)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(438)
		{
			IL2CPP_END_CLEANUP(0x1CB, FINALLY_01ba);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01ba;
	}

FINALLY_01ba:
	{ // begin finally (depth: 1)
		{
			bool L_86 = GCHandle_get_IsAllocated_m1058226959((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
			if (!L_86)
			{
				goto IL_01ca;
			}
		}

IL_01c3:
		{
			GCHandle_Free_m1457699368((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
		}

IL_01ca:
		{
			IL2CPP_END_FINALLY(442)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(442)
	{
		IL2CPP_JUMP_TBL(0x1CB, IL_01cb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01cb:
	{
		String_t* L_87 = V_2;
		return L_87;
	}
}
// System.Int32 Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StringToBytes(System.Byte[],System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t UnsafeUtilities_StringToBytes_m87138792 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___buffer0, String_t* ___value1, bool ___needs16BitSupport2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnsafeUtilities_StringToBytes_m87138792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GCHandle_t3351438187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppChar* V_2 = NULL;
	String_t* V_3 = NULL;
	uint16_t* V_4 = NULL;
	uint16_t* V_5 = NULL;
	intptr_t V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Il2CppChar* V_8 = NULL;
	String_t* V_9 = NULL;
	uint8_t* V_10 = NULL;
	uint8_t* V_11 = NULL;
	int32_t V_12 = 0;
	Il2CppChar* V_13 = NULL;
	String_t* V_14 = NULL;
	uint8_t* V_15 = NULL;
	uint8_t* V_16 = NULL;
	int32_t V_17 = 0;
	Il2CppChar* V_18 = NULL;
	String_t* V_19 = NULL;
	uint8_t* V_20 = NULL;
	uint8_t* V_21 = NULL;
	int32_t V_22 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___needs16BitSupport2;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		String_t* L_1 = ___value1;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0013;
	}

IL_000b:
	{
		String_t* L_3 = ___value1;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)2));
	}

IL_0013:
	{
		V_0 = G_B3_0;
		ByteU5BU5D_t4116647657* L_5 = ___buffer0;
		NullCheck(L_5);
		int32_t L_6 = V_0;
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length))))) >= ((int32_t)L_6)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_8);
		String_t* L_10 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral3739023119, L_9, _stringLiteral1929072793, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_11 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, NULL, UnsafeUtilities_StringToBytes_m87138792_RuntimeMethod_var);
	}

IL_0035:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(GCHandle_t3351438187 ));
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t4116647657* L_12 = ___buffer0;
			GCHandle_t3351438187  L_13 = GCHandle_Alloc_m3823409740(NULL /*static, unused*/, (RuntimeObject *)(RuntimeObject *)L_12, 3, /*hidden argument*/NULL);
			V_1 = L_13;
			bool L_14 = ___needs16BitSupport2;
			if (!L_14)
			{
				goto IL_0101;
			}
		}

IL_004b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
			bool L_15 = ((BitConverter_t3118986983_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t3118986983_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
			if (!L_15)
			{
				goto IL_009f;
			}
		}

IL_0052:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_16 = ___value1;
				V_3 = L_16;
				String_t* L_17 = V_3;
				V_2 = (Il2CppChar*)(((intptr_t)L_17));
				Il2CppChar* L_18 = V_2;
				if (!L_18)
				{
					goto IL_0062;
				}
			}

IL_005a:
			{
				Il2CppChar* L_19 = V_2;
				int32_t L_20 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_2 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_19, (int32_t)L_20));
			}

IL_0062:
			{
				Il2CppChar* L_21 = V_2;
				V_4 = (uint16_t*)L_21;
				intptr_t L_22 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_6 = (intptr_t)L_22;
				void* L_23 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_6), /*hidden argument*/NULL);
				V_5 = (uint16_t*)L_23;
				V_7 = 0;
				goto IL_0092;
			}

IL_007c:
			{
				uint16_t* L_24 = V_5;
				uint16_t* L_25 = (uint16_t*)L_24;
				V_5 = (uint16_t*)((uint16_t*)il2cpp_codegen_add((intptr_t)L_25, (int32_t)2));
				uint16_t* L_26 = V_4;
				uint16_t* L_27 = (uint16_t*)L_26;
				V_4 = (uint16_t*)((uint16_t*)il2cpp_codegen_add((intptr_t)L_27, (int32_t)2));
				int32_t L_28 = *((uint16_t*)L_27);
				*((int16_t*)L_25) = (int16_t)L_28;
				int32_t L_29 = V_7;
				V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)2));
			}

IL_0092:
			{
				int32_t L_30 = V_7;
				int32_t L_31 = V_0;
				if ((((int32_t)L_30) < ((int32_t)L_31)))
				{
					goto IL_007c;
				}
			}

IL_0097:
			{
				IL2CPP_LEAVE(0x1CA, FINALLY_009c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_009c;
		}

FINALLY_009c:
		{ // begin finally (depth: 2)
			V_3 = (String_t*)NULL;
			IL2CPP_END_FINALLY(156)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(156)
		{
			IL2CPP_END_CLEANUP(0x1CA, FINALLY_01b9);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_009f:
		{
		}

IL_00a0:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_32 = ___value1;
				V_9 = L_32;
				String_t* L_33 = V_9;
				V_8 = (Il2CppChar*)(((intptr_t)L_33));
				Il2CppChar* L_34 = V_8;
				if (!L_34)
				{
					goto IL_00b6;
				}
			}

IL_00ac:
			{
				Il2CppChar* L_35 = V_8;
				int32_t L_36 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_8 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_35, (int32_t)L_36));
			}

IL_00b6:
			{
				Il2CppChar* L_37 = V_8;
				V_10 = (uint8_t*)L_37;
				intptr_t L_38 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_6 = (intptr_t)L_38;
				void* L_39 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_6), /*hidden argument*/NULL);
				V_11 = (uint8_t*)L_39;
				V_12 = 0;
				goto IL_00f3;
			}

IL_00d1:
			{
				uint8_t* L_40 = V_11;
				uint8_t* L_41 = V_10;
				int32_t L_42 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_41, (int32_t)1)));
				*((int8_t*)L_40) = (int8_t)L_42;
				uint8_t* L_43 = V_11;
				uint8_t* L_44 = V_10;
				int32_t L_45 = *((uint8_t*)L_44);
				*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_43, (int32_t)1))) = (int8_t)L_45;
				uint8_t* L_46 = V_10;
				V_10 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_46, (int32_t)2));
				uint8_t* L_47 = V_11;
				V_11 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_47, (int32_t)2));
				int32_t L_48 = V_12;
				V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)2));
			}

IL_00f3:
			{
				int32_t L_49 = V_12;
				int32_t L_50 = V_0;
				if ((((int32_t)L_49) < ((int32_t)L_50)))
				{
					goto IL_00d1;
				}
			}

IL_00f8:
			{
				IL2CPP_LEAVE(0x1CA, FINALLY_00fd);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_00fd;
		}

FINALLY_00fd:
		{ // begin finally (depth: 2)
			V_9 = (String_t*)NULL;
			IL2CPP_END_FINALLY(253)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(253)
		{
			IL2CPP_END_CLEANUP(0x1CA, FINALLY_01b9);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0101:
		{
			IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
			bool L_51 = ((BitConverter_t3118986983_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t3118986983_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
			if (!L_51)
			{
				goto IL_0160;
			}
		}

IL_0108:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_52 = ___value1;
				V_14 = L_52;
				String_t* L_53 = V_14;
				V_13 = (Il2CppChar*)(((intptr_t)L_53));
				Il2CppChar* L_54 = V_13;
				if (!L_54)
				{
					goto IL_011e;
				}
			}

IL_0114:
			{
				Il2CppChar* L_55 = V_13;
				int32_t L_56 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_13 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_55, (int32_t)L_56));
			}

IL_011e:
			{
				Il2CppChar* L_57 = V_13;
				V_15 = (uint8_t*)L_57;
				intptr_t L_58 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_6 = (intptr_t)L_58;
				void* L_59 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_6), /*hidden argument*/NULL);
				V_16 = (uint8_t*)L_59;
				V_17 = 0;
				goto IL_0155;
			}

IL_0139:
			{
				uint8_t* L_60 = V_15;
				V_15 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_60, (int32_t)1));
				uint8_t* L_61 = V_16;
				uint8_t* L_62 = (uint8_t*)L_61;
				V_16 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_62, (int32_t)1));
				uint8_t* L_63 = V_15;
				uint8_t* L_64 = (uint8_t*)L_63;
				V_15 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_64, (int32_t)1));
				int32_t L_65 = *((uint8_t*)L_64);
				*((int8_t*)L_62) = (int8_t)L_65;
				int32_t L_66 = V_17;
				V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1));
			}

IL_0155:
			{
				int32_t L_67 = V_17;
				int32_t L_68 = V_0;
				if ((((int32_t)L_67) < ((int32_t)L_68)))
				{
					goto IL_0139;
				}
			}

IL_015a:
			{
				IL2CPP_LEAVE(0x1CA, FINALLY_015c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_015c;
		}

FINALLY_015c:
		{ // begin finally (depth: 2)
			V_14 = (String_t*)NULL;
			IL2CPP_END_FINALLY(348)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(348)
		{
			IL2CPP_END_CLEANUP(0x1CA, FINALLY_01b9);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0160:
		{
		}

IL_0161:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_69 = ___value1;
				V_19 = L_69;
				String_t* L_70 = V_19;
				V_18 = (Il2CppChar*)(((intptr_t)L_70));
				Il2CppChar* L_71 = V_18;
				if (!L_71)
				{
					goto IL_0177;
				}
			}

IL_016d:
			{
				Il2CppChar* L_72 = V_18;
				int32_t L_73 = RuntimeHelpers_get_OffsetToStringData_m2192601476(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_18 = (Il2CppChar*)((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_72, (int32_t)L_73));
			}

IL_0177:
			{
				Il2CppChar* L_74 = V_18;
				V_20 = (uint8_t*)L_74;
				intptr_t L_75 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
				V_6 = (intptr_t)L_75;
				void* L_76 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_6), /*hidden argument*/NULL);
				V_21 = (uint8_t*)L_76;
				V_22 = 0;
				goto IL_01ae;
			}

IL_0192:
			{
				uint8_t* L_77 = V_21;
				uint8_t* L_78 = (uint8_t*)L_77;
				V_21 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_78, (int32_t)1));
				uint8_t* L_79 = V_20;
				uint8_t* L_80 = (uint8_t*)L_79;
				V_20 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_80, (int32_t)1));
				int32_t L_81 = *((uint8_t*)L_80);
				*((int8_t*)L_78) = (int8_t)L_81;
				uint8_t* L_82 = V_20;
				V_20 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_82, (int32_t)1));
				int32_t L_83 = V_22;
				V_22 = ((int32_t)il2cpp_codegen_add((int32_t)L_83, (int32_t)1));
			}

IL_01ae:
			{
				int32_t L_84 = V_22;
				int32_t L_85 = V_0;
				if ((((int32_t)L_84) < ((int32_t)L_85)))
				{
					goto IL_0192;
				}
			}

IL_01b3:
			{
				IL2CPP_LEAVE(0x1CA, FINALLY_01b5);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_01b5;
		}

FINALLY_01b5:
		{ // begin finally (depth: 2)
			V_19 = (String_t*)NULL;
			IL2CPP_END_FINALLY(437)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(437)
		{
			IL2CPP_END_CLEANUP(0x1CA, FINALLY_01b9);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01b9;
	}

FINALLY_01b9:
	{ // begin finally (depth: 1)
		{
			bool L_86 = GCHandle_get_IsAllocated_m1058226959((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
			if (!L_86)
			{
				goto IL_01c9;
			}
		}

IL_01c2:
		{
			GCHandle_Free_m1457699368((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
		}

IL_01c9:
		{
			IL2CPP_END_FINALLY(441)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(441)
	{
		IL2CPP_JUMP_TBL(0x1CA, IL_01ca)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01ca:
	{
		int32_t L_87 = V_0;
		return L_87;
	}
}
// System.Void Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Object,System.Object,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UnsafeUtilities_MemoryCopy_m2366990917 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___from0, RuntimeObject * ___to1, int32_t ___byteCount2, int32_t ___fromByteOffset3, int32_t ___toByteOffset4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnsafeUtilities_MemoryCopy_m2366990917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GCHandle_t3351438187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GCHandle_t3351438187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint64_t* V_6 = NULL;
	uint64_t* V_7 = NULL;
	intptr_t V_8;
	memset(&V_8, 0, sizeof(V_8));
	int32_t V_9 = 0;
	uint8_t* V_10 = NULL;
	uint8_t* V_11 = NULL;
	int32_t V_12 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		il2cpp_codegen_initobj((&V_0), sizeof(GCHandle_t3351438187 ));
		il2cpp_codegen_initobj((&V_1), sizeof(GCHandle_t3351438187 ));
		int32_t L_0 = ___fromByteOffset3;
		if (((int32_t)((int32_t)L_0%(int32_t)8)))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = ___toByteOffset4;
		if (!((int32_t)((int32_t)L_1%(int32_t)8)))
		{
			goto IL_0036;
		}
	}

IL_001b:
	{
		int32_t L_2 = 8;
		RuntimeObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1581745030, L_3, _stringLiteral3707058535, /*hidden argument*/NULL);
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, UnsafeUtilities_MemoryCopy_m2366990917_RuntimeMethod_var);
	}

IL_0036:
	{
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_6 = ___byteCount2;
			V_2 = ((int32_t)((int32_t)L_6%(int32_t)8));
			int32_t L_7 = ___byteCount2;
			int32_t L_8 = V_2;
			V_3 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)L_8))/(int32_t)8));
			int32_t L_9 = ___fromByteOffset3;
			V_4 = ((int32_t)((int32_t)L_9/(int32_t)8));
			int32_t L_10 = ___toByteOffset4;
			V_5 = ((int32_t)((int32_t)L_10/(int32_t)8));
			RuntimeObject * L_11 = ___from0;
			GCHandle_t3351438187  L_12 = GCHandle_Alloc_m3823409740(NULL /*static, unused*/, L_11, 3, /*hidden argument*/NULL);
			V_0 = L_12;
			RuntimeObject * L_13 = ___to1;
			GCHandle_t3351438187  L_14 = GCHandle_Alloc_m3823409740(NULL /*static, unused*/, L_13, 3, /*hidden argument*/NULL);
			V_1 = L_14;
			intptr_t L_15 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_0), /*hidden argument*/NULL);
			V_8 = (intptr_t)L_15;
			void* L_16 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_8), /*hidden argument*/NULL);
			V_6 = (uint64_t*)L_16;
			intptr_t L_17 = GCHandle_AddrOfPinnedObject_m3427142301((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
			V_8 = (intptr_t)L_17;
			void* L_18 = IntPtr_ToPointer_m4157623054((intptr_t*)(&V_8), /*hidden argument*/NULL);
			V_7 = (uint64_t*)L_18;
			int32_t L_19 = V_4;
			if ((((int32_t)L_19) <= ((int32_t)0)))
			{
				goto IL_008f;
			}
		}

IL_0085:
		{
			uint64_t* L_20 = V_6;
			int32_t L_21 = V_4;
			V_6 = (uint64_t*)((uint64_t*)il2cpp_codegen_add((intptr_t)L_20, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)(((intptr_t)L_21)), (int32_t)8))));
		}

IL_008f:
		{
			int32_t L_22 = V_5;
			if ((((int32_t)L_22) <= ((int32_t)0)))
			{
				goto IL_009e;
			}
		}

IL_0094:
		{
			uint64_t* L_23 = V_7;
			int32_t L_24 = V_5;
			V_7 = (uint64_t*)((uint64_t*)il2cpp_codegen_add((intptr_t)L_23, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)(((intptr_t)L_24)), (int32_t)8))));
		}

IL_009e:
		{
			V_9 = 0;
			goto IL_00b9;
		}

IL_00a3:
		{
			uint64_t* L_25 = V_7;
			uint64_t* L_26 = (uint64_t*)L_25;
			V_7 = (uint64_t*)((uint64_t*)il2cpp_codegen_add((intptr_t)L_26, (int32_t)8));
			uint64_t* L_27 = V_6;
			uint64_t* L_28 = (uint64_t*)L_27;
			V_6 = (uint64_t*)((uint64_t*)il2cpp_codegen_add((intptr_t)L_28, (int32_t)8));
			int64_t L_29 = *((int64_t*)L_28);
			*((int64_t*)L_26) = (int64_t)L_29;
			int32_t L_30 = V_9;
			V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		}

IL_00b9:
		{
			int32_t L_31 = V_9;
			int32_t L_32 = V_3;
			if ((((int32_t)L_31) < ((int32_t)L_32)))
			{
				goto IL_00a3;
			}
		}

IL_00be:
		{
			int32_t L_33 = V_2;
			if ((((int32_t)L_33) <= ((int32_t)0)))
			{
				goto IL_00ea;
			}
		}

IL_00c2:
		{
			uint64_t* L_34 = V_6;
			V_10 = (uint8_t*)L_34;
			uint64_t* L_35 = V_7;
			V_11 = (uint8_t*)L_35;
			V_12 = 0;
			goto IL_00e5;
		}

IL_00cf:
		{
			uint8_t* L_36 = V_11;
			uint8_t* L_37 = (uint8_t*)L_36;
			V_11 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_37, (int32_t)1));
			uint8_t* L_38 = V_10;
			uint8_t* L_39 = (uint8_t*)L_38;
			V_10 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_39, (int32_t)1));
			int32_t L_40 = *((uint8_t*)L_39);
			*((int8_t*)L_37) = (int8_t)L_40;
			int32_t L_41 = V_12;
			V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
		}

IL_00e5:
		{
			int32_t L_42 = V_12;
			int32_t L_43 = V_2;
			if ((((int32_t)L_42) < ((int32_t)L_43)))
			{
				goto IL_00cf;
			}
		}

IL_00ea:
		{
			IL2CPP_LEAVE(0x10D, FINALLY_00ec);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00ec;
	}

FINALLY_00ec:
	{ // begin finally (depth: 1)
		{
			bool L_44 = GCHandle_get_IsAllocated_m1058226959((GCHandle_t3351438187 *)(&V_0), /*hidden argument*/NULL);
			if (!L_44)
			{
				goto IL_00fc;
			}
		}

IL_00f5:
		{
			GCHandle_Free_m1457699368((GCHandle_t3351438187 *)(&V_0), /*hidden argument*/NULL);
		}

IL_00fc:
		{
			bool L_45 = GCHandle_get_IsAllocated_m1058226959((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
			if (!L_45)
			{
				goto IL_010c;
			}
		}

IL_0105:
		{
			GCHandle_Free_m1457699368((GCHandle_t3351438187 *)(&V_1), /*hidden argument*/NULL);
		}

IL_010c:
		{
			IL2CPP_END_FINALLY(236)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(236)
	{
		IL2CPP_JUMP_TBL(0x10D, IL_010d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_010d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.WeakValueGetter::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void WeakValueGetter__ctor_m628017869 (WeakValueGetter_t2669515580 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object Sirenix.Serialization.Utilities.WeakValueGetter::Invoke(System.Object&)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * WeakValueGetter_Invoke_m2243651192 (WeakValueGetter_t2669515580 * __this, RuntimeObject ** ___instance0, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// open
					{
						typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject **, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(NULL, ___instance0, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void*, RuntimeObject **, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___instance0, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(targetMethod, targetThis, ___instance0);
							else
								result = GenericVirtFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(targetMethod, targetThis, ___instance0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___instance0);
							else
								result = VirtFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___instance0);
						}
					}
					else
					{
						typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject **, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___instance0, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// open
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject **, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(NULL, ___instance0, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void*, RuntimeObject **, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___instance0, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(targetMethod, targetThis, ___instance0);
						else
							result = GenericVirtFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(targetMethod, targetThis, ___instance0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___instance0);
						else
							result = VirtFuncInvoker1< RuntimeObject *, RuntimeObject ** >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___instance0);
					}
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject **, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___instance0, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueGetter::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* WeakValueGetter_BeginInvoke_m1861532851 (WeakValueGetter_t2669515580 * __this, RuntimeObject ** ___instance0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = *___instance0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Object Sirenix.Serialization.Utilities.WeakValueGetter::EndInvoke(System.Object&,System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * WeakValueGetter_EndInvoke_m3645412016 (WeakValueGetter_t2669515580 * __this, RuntimeObject ** ___instance0, RuntimeObject* ___result1, const RuntimeMethod* method)
{
	void* ___out_args[] = {
	___instance0,
	};
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Utilities.WeakValueSetter::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void WeakValueSetter__ctor_m125709448 (WeakValueSetter_t2630980412 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Sirenix.Serialization.Utilities.WeakValueSetter::Invoke(System.Object&,System.Object)
extern "C" IL2CPP_METHOD_ATTR void WeakValueSetter_Invoke_m1248817919 (WeakValueSetter_t2630980412 * __this, RuntimeObject ** ___instance0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject **, RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___instance0, ___value1, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, RuntimeObject **, RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___instance0, ___value1, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(targetMethod, targetThis, ___instance0, ___value1);
							else
								GenericVirtActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(targetMethod, targetThis, ___instance0, ___value1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___instance0, ___value1);
							else
								VirtActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___instance0, ___value1);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, RuntimeObject **, RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___instance0, ___value1, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 2)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject **, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___instance0, ___value1, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, RuntimeObject **, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___instance0, ___value1, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(targetMethod, targetThis, ___instance0, ___value1);
						else
							GenericVirtActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(targetMethod, targetThis, ___instance0, ___value1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___instance0, ___value1);
						else
							VirtActionInvoker2< RuntimeObject **, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___instance0, ___value1);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject **, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___instance0, ___value1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueSetter::BeginInvoke(System.Object&,System.Object,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* WeakValueSetter_BeginInvoke_m3569519556 (WeakValueSetter_t2630980412 * __this, RuntimeObject ** ___instance0, RuntimeObject * ___value1, AsyncCallback_t3962456242 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = *___instance0;
	__d_args[1] = ___value1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Sirenix.Serialization.Utilities.WeakValueSetter::EndInvoke(System.Object&,System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void WeakValueSetter_EndInvoke_m1706154800 (WeakValueSetter_t2630980412 * __this, RuntimeObject ** ___instance0, RuntimeObject* ___result1, const RuntimeMethod* method)
{
	void* ___out_args[] = {
	___instance0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Vector2DictionaryKeyPathProvider::get_ProviderID()
extern "C" IL2CPP_METHOD_ATTR String_t* Vector2DictionaryKeyPathProvider_get_ProviderID_m4142048575 (Vector2DictionaryKeyPathProvider_t1191773922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2DictionaryKeyPathProvider_get_ProviderID_m4142048575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3451434954;
	}
}
// System.Int32 Sirenix.Serialization.Vector2DictionaryKeyPathProvider::Compare(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR int32_t Vector2DictionaryKeyPathProvider_Compare_m3377316981 (Vector2DictionaryKeyPathProvider_t1191773922 * __this, Vector2_t2156229523  ___x0, Vector2_t2156229523  ___y1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = (&___x0)->get_address_of_x_0();
		Vector2_t2156229523  L_1 = ___y1;
		float L_2 = L_1.get_x_0();
		int32_t L_3 = Single_CompareTo_m189772128((float*)L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		float* L_5 = (&___x0)->get_address_of_y_1();
		Vector2_t2156229523  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		int32_t L_8 = Single_CompareTo_m189772128((float*)L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0029:
	{
		int32_t L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Vector2 Sirenix.Serialization.Vector2DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2DictionaryKeyPathProvider_GetKeyFromPathString_m1994082079 (Vector2DictionaryKeyPathProvider_t1191773922 * __this, String_t* ___pathStr0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		String_t* L_0 = ___pathStr0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m363431711(L_0, ((int32_t)124), /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___pathStr0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		String_t* L_4 = String_Substring_m1610150815(L_2, 1, ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = String_Trim_m923598732(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = ___pathStr0;
		int32_t L_7 = V_0;
		String_t* L_8 = ___pathStr0;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m3847582255(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		NullCheck(L_6);
		String_t* L_11 = String_Substring_m1610150815(L_6, ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)2)))), /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = String_Trim_m923598732(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		String_t* L_13 = V_1;
		float L_14 = Single_Parse_m364357836(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_15 = V_2;
		float L_16 = Single_Parse_m364357836(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), L_14, L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.String Sirenix.Serialization.Vector2DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR String_t* Vector2DictionaryKeyPathProvider_GetPathStringFromKey_m475561918 (Vector2DictionaryKeyPathProvider_t1191773922 * __this, Vector2_t2156229523  ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2DictionaryKeyPathProvider_GetPathStringFromKey_m475561918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		float* L_0 = (&___key0)->get_address_of_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_1 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Single_ToString_m543431371((float*)L_0, _stringLiteral3452614638, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float* L_3 = (&___key0)->get_address_of_y_1();
		CultureInfo_t4157843068 * L_4 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = Single_ToString_m543431371((float*)L_3, _stringLiteral3452614638, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3452614536);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614536);
		StringU5BU5D_t1281789340* L_8 = L_7;
		String_t* L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_9);
		StringU5BU5D_t1281789340* L_10 = L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3452614612);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614612);
		StringU5BU5D_t1281789340* L_11 = L_10;
		String_t* L_12 = V_1;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_12);
		StringU5BU5D_t1281789340* L_13 = L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral3452614535);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614535);
		String_t* L_14 = String_Concat_m1809518182(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = String_Replace_m3726209165(L_14, ((int32_t)46), ((int32_t)44), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void Sirenix.Serialization.Vector2DictionaryKeyPathProvider::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Vector2DictionaryKeyPathProvider__ctor_m645920001 (Vector2DictionaryKeyPathProvider_t1191773922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2DictionaryKeyPathProvider__ctor_m645920001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BaseDictionaryKeyPathProvider_1__ctor_m3157277402(__this, /*hidden argument*/BaseDictionaryKeyPathProvider_1__ctor_m3157277402_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Vector2Formatter::Read(UnityEngine.Vector2&,Sirenix.Serialization.IDataReader)
extern "C" IL2CPP_METHOD_ATTR void Vector2Formatter_Read_m1475415392 (Vector2Formatter_t2520006045 * __this, Vector2_t2156229523 * ___value0, RuntimeObject* ___reader1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2Formatter_Read_m1475415392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t2156229523 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_1 = ((Vector2Formatter_t2520006045_StaticFields*)il2cpp_codegen_static_fields_for(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_2 = ___reader1;
		NullCheck(L_1);
		float L_3 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_1, L_2);
		L_0->set_x_0(L_3);
		Vector2_t2156229523 * L_4 = ___value0;
		Serializer_1_t3074248596 * L_5 = ((Vector2Formatter_t2520006045_StaticFields*)il2cpp_codegen_static_fields_for(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_6 = ___reader1;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_5, L_6);
		L_4->set_y_1(L_7);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector2Formatter::Write(UnityEngine.Vector2&,Sirenix.Serialization.IDataWriter)
extern "C" IL2CPP_METHOD_ATTR void Vector2Formatter_Write_m4061323822 (Vector2Formatter_t2520006045 * __this, Vector2_t2156229523 * ___value0, RuntimeObject* ___writer1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2Formatter_Write_m4061323822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_0 = ((Vector2Formatter_t2520006045_StaticFields*)il2cpp_codegen_static_fields_for(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector2_t2156229523 * L_1 = ___value0;
		float L_2 = L_1->get_x_0();
		RuntimeObject* L_3 = ___writer1;
		NullCheck(L_0);
		Serializer_1_WriteValue_m1523856254(L_0, L_2, L_3, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		Serializer_1_t3074248596 * L_4 = ((Vector2Formatter_t2520006045_StaticFields*)il2cpp_codegen_static_fields_for(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector2_t2156229523 * L_5 = ___value0;
		float L_6 = L_5->get_y_1();
		RuntimeObject* L_7 = ___writer1;
		NullCheck(L_4);
		Serializer_1_WriteValue_m1523856254(L_4, L_6, L_7, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector2Formatter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Vector2Formatter__ctor_m3888705115 (Vector2Formatter_t2520006045 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2Formatter__ctor_m3888705115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MinimalBaseFormatter_1_t3461213901_il2cpp_TypeInfo_var);
		MinimalBaseFormatter_1__ctor_m4165236585(__this, /*hidden argument*/MinimalBaseFormatter_1__ctor_m4165236585_RuntimeMethod_var);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector2Formatter::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Vector2Formatter__cctor_m2866700392 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2Formatter__cctor_m2866700392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Serializer_t962918574_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_0 = Serializer_Get_TisSingle_t1397266774_m2693733542(NULL /*static, unused*/, /*hidden argument*/Serializer_Get_TisSingle_t1397266774_m2693733542_RuntimeMethod_var);
		((Vector2Formatter_t2520006045_StaticFields*)il2cpp_codegen_static_fields_for(Vector2Formatter_t2520006045_il2cpp_TypeInfo_var))->set_FloatSerializer_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Vector3DictionaryKeyPathProvider::get_ProviderID()
extern "C" IL2CPP_METHOD_ATTR String_t* Vector3DictionaryKeyPathProvider_get_ProviderID_m2013560377 (Vector3DictionaryKeyPathProvider_t2227500205 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3DictionaryKeyPathProvider_get_ProviderID_m2013560377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3451369418;
	}
}
// System.Int32 Sirenix.Serialization.Vector3DictionaryKeyPathProvider::Compare(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR int32_t Vector3DictionaryKeyPathProvider_Compare_m1912123530 (Vector3DictionaryKeyPathProvider_t2227500205 * __this, Vector3_t3722313464  ___x0, Vector3_t3722313464  ___y1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = (&___x0)->get_address_of_x_2();
		Vector3_t3722313464  L_1 = ___y1;
		float L_2 = L_1.get_x_2();
		int32_t L_3 = Single_CompareTo_m189772128((float*)L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		float* L_5 = (&___x0)->get_address_of_y_3();
		Vector3_t3722313464  L_6 = ___y1;
		float L_7 = L_6.get_y_3();
		int32_t L_8 = Single_CompareTo_m189772128((float*)L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0029:
	{
		int32_t L_9 = V_0;
		if (L_9)
		{
			goto IL_003f;
		}
	}
	{
		float* L_10 = (&___x0)->get_address_of_z_4();
		Vector3_t3722313464  L_11 = ___y1;
		float L_12 = L_11.get_z_4();
		int32_t L_13 = Single_CompareTo_m189772128((float*)L_10, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_003f:
	{
		int32_t L_14 = V_0;
		return L_14;
	}
}
// UnityEngine.Vector3 Sirenix.Serialization.Vector3DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3DictionaryKeyPathProvider_GetKeyFromPathString_m1421332164 (Vector3DictionaryKeyPathProvider_t2227500205 * __this, String_t* ___pathStr0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		String_t* L_0 = ___pathStr0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m363431711(L_0, ((int32_t)124), /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___pathStr0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = String_IndexOf_m2466398549(L_2, ((int32_t)124), ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = ___pathStr0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m1610150815(L_5, 1, ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = String_Trim_m923598732(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = ___pathStr0;
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		NullCheck(L_9);
		String_t* L_13 = String_Substring_m1610150815(L_9, ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)))), /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m923598732(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		String_t* L_15 = ___pathStr0;
		int32_t L_16 = V_1;
		String_t* L_17 = ___pathStr0;
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m3847582255(L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		NullCheck(L_15);
		String_t* L_20 = String_Substring_m1610150815(L_15, ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)2)))), /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = String_Trim_m923598732(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		String_t* L_22 = V_2;
		float L_23 = Single_Parse_m364357836(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		String_t* L_24 = V_3;
		float L_25 = Single_Parse_m364357836(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		String_t* L_26 = V_4;
		float L_27 = Single_Parse_m364357836(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		Vector3_t3722313464  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m3353183577((&L_28), L_23, L_25, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.String Sirenix.Serialization.Vector3DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR String_t* Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m3423976823 (Vector3DictionaryKeyPathProvider_t2227500205 * __this, Vector3_t3722313464  ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m3423976823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		float* L_0 = (&___key0)->get_address_of_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_1 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Single_ToString_m543431371((float*)L_0, _stringLiteral3452614638, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float* L_3 = (&___key0)->get_address_of_y_3();
		CultureInfo_t4157843068 * L_4 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = Single_ToString_m543431371((float*)L_3, _stringLiteral3452614638, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float* L_6 = (&___key0)->get_address_of_z_4();
		CultureInfo_t4157843068 * L_7 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Single_ToString_m543431371((float*)L_6, _stringLiteral3452614638, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		StringU5BU5D_t1281789340* L_9 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)7);
		StringU5BU5D_t1281789340* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3452614536);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614536);
		StringU5BU5D_t1281789340* L_11 = L_10;
		String_t* L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_12);
		StringU5BU5D_t1281789340* L_13 = L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral3452614612);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614612);
		StringU5BU5D_t1281789340* L_14 = L_13;
		String_t* L_15 = V_1;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_15);
		StringU5BU5D_t1281789340* L_16 = L_14;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral3452614612);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614612);
		StringU5BU5D_t1281789340* L_17 = L_16;
		String_t* L_18 = V_2;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_18);
		StringU5BU5D_t1281789340* L_19 = L_17;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3452614535);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3452614535);
		String_t* L_20 = String_Concat_m1809518182(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = String_Replace_m3726209165(L_20, ((int32_t)46), ((int32_t)44), /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void Sirenix.Serialization.Vector3DictionaryKeyPathProvider::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Vector3DictionaryKeyPathProvider__ctor_m1297447965 (Vector3DictionaryKeyPathProvider_t2227500205 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3DictionaryKeyPathProvider__ctor_m1297447965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BaseDictionaryKeyPathProvider_1__ctor_m1975566667(__this, /*hidden argument*/BaseDictionaryKeyPathProvider_1__ctor_m1975566667_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Vector3Formatter::Read(UnityEngine.Vector3&,Sirenix.Serialization.IDataReader)
extern "C" IL2CPP_METHOD_ATTR void Vector3Formatter_Read_m1977710546 (Vector3Formatter_t2520007008 * __this, Vector3_t3722313464 * ___value0, RuntimeObject* ___reader1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Formatter_Read_m1977710546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_1 = ((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_2 = ___reader1;
		NullCheck(L_1);
		float L_3 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_1, L_2);
		L_0->set_x_2(L_3);
		Vector3_t3722313464 * L_4 = ___value0;
		Serializer_1_t3074248596 * L_5 = ((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_6 = ___reader1;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_5, L_6);
		L_4->set_y_3(L_7);
		Vector3_t3722313464 * L_8 = ___value0;
		Serializer_1_t3074248596 * L_9 = ((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_10 = ___reader1;
		NullCheck(L_9);
		float L_11 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_9, L_10);
		L_8->set_z_4(L_11);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector3Formatter::Write(UnityEngine.Vector3&,Sirenix.Serialization.IDataWriter)
extern "C" IL2CPP_METHOD_ATTR void Vector3Formatter_Write_m2220682641 (Vector3Formatter_t2520007008 * __this, Vector3_t3722313464 * ___value0, RuntimeObject* ___writer1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Formatter_Write_m2220682641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_0 = ((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector3_t3722313464 * L_1 = ___value0;
		float L_2 = L_1->get_x_2();
		RuntimeObject* L_3 = ___writer1;
		NullCheck(L_0);
		Serializer_1_WriteValue_m1523856254(L_0, L_2, L_3, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		Serializer_1_t3074248596 * L_4 = ((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector3_t3722313464 * L_5 = ___value0;
		float L_6 = L_5->get_y_3();
		RuntimeObject* L_7 = ___writer1;
		NullCheck(L_4);
		Serializer_1_WriteValue_m1523856254(L_4, L_6, L_7, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		Serializer_1_t3074248596 * L_8 = ((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector3_t3722313464 * L_9 = ___value0;
		float L_10 = L_9->get_z_4();
		RuntimeObject* L_11 = ___writer1;
		NullCheck(L_8);
		Serializer_1_WriteValue_m1523856254(L_8, L_10, L_11, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector3Formatter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Vector3Formatter__ctor_m2713626732 (Vector3Formatter_t2520007008 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Formatter__ctor_m2713626732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MinimalBaseFormatter_1_t732330546_il2cpp_TypeInfo_var);
		MinimalBaseFormatter_1__ctor_m153188201(__this, /*hidden argument*/MinimalBaseFormatter_1__ctor_m153188201_RuntimeMethod_var);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector3Formatter::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Vector3Formatter__cctor_m675765975 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Formatter__cctor_m675765975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Serializer_t962918574_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_0 = Serializer_Get_TisSingle_t1397266774_m2693733542(NULL /*static, unused*/, /*hidden argument*/Serializer_Get_TisSingle_t1397266774_m2693733542_RuntimeMethod_var);
		((Vector3Formatter_t2520007008_StaticFields*)il2cpp_codegen_static_fields_for(Vector3Formatter_t2520007008_il2cpp_TypeInfo_var))->set_FloatSerializer_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.Serialization.Vector4DictionaryKeyPathProvider::get_ProviderID()
extern "C" IL2CPP_METHOD_ATTR String_t* Vector4DictionaryKeyPathProvider_get_ProviderID_m2239077201 (Vector4DictionaryKeyPathProvider_t4213655263 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4DictionaryKeyPathProvider_get_ProviderID_m2239077201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3451303882;
	}
}
// System.Int32 Sirenix.Serialization.Vector4DictionaryKeyPathProvider::Compare(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" IL2CPP_METHOD_ATTR int32_t Vector4DictionaryKeyPathProvider_Compare_m1031062350 (Vector4DictionaryKeyPathProvider_t4213655263 * __this, Vector4_t3319028937  ___x0, Vector4_t3319028937  ___y1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = (&___x0)->get_address_of_x_1();
		Vector4_t3319028937  L_1 = ___y1;
		float L_2 = L_1.get_x_1();
		int32_t L_3 = Single_CompareTo_m189772128((float*)L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		float* L_5 = (&___x0)->get_address_of_y_2();
		Vector4_t3319028937  L_6 = ___y1;
		float L_7 = L_6.get_y_2();
		int32_t L_8 = Single_CompareTo_m189772128((float*)L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0029:
	{
		int32_t L_9 = V_0;
		if (L_9)
		{
			goto IL_003f;
		}
	}
	{
		float* L_10 = (&___x0)->get_address_of_z_3();
		Vector4_t3319028937  L_11 = ___y1;
		float L_12 = L_11.get_z_3();
		int32_t L_13 = Single_CompareTo_m189772128((float*)L_10, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_003f:
	{
		int32_t L_14 = V_0;
		if (L_14)
		{
			goto IL_0055;
		}
	}
	{
		float* L_15 = (&___x0)->get_address_of_w_4();
		Vector4_t3319028937  L_16 = ___y1;
		float L_17 = L_16.get_w_4();
		int32_t L_18 = Single_CompareTo_m189772128((float*)L_15, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
	}

IL_0055:
	{
		int32_t L_19 = V_0;
		return L_19;
	}
}
// UnityEngine.Vector4 Sirenix.Serialization.Vector4DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern "C" IL2CPP_METHOD_ATTR Vector4_t3319028937  Vector4DictionaryKeyPathProvider_GetKeyFromPathString_m3465391078 (Vector4DictionaryKeyPathProvider_t4213655263 * __this, String_t* ___pathStr0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	{
		String_t* L_0 = ___pathStr0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m363431711(L_0, ((int32_t)124), /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___pathStr0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = String_IndexOf_m2466398549(L_2, ((int32_t)124), ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = ___pathStr0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = String_IndexOf_m2466398549(L_5, ((int32_t)124), ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = ___pathStr0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = String_Substring_m1610150815(L_8, 1, ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = String_Trim_m923598732(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		String_t* L_12 = ___pathStr0;
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		int32_t L_15 = V_0;
		NullCheck(L_12);
		String_t* L_16 = String_Substring_m1610150815(L_12, ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)))), /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_17 = String_Trim_m923598732(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		String_t* L_18 = ___pathStr0;
		int32_t L_19 = V_1;
		int32_t L_20 = V_2;
		int32_t L_21 = V_1;
		NullCheck(L_18);
		String_t* L_22 = String_Substring_m1610150815(L_18, ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1)))), /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = String_Trim_m923598732(L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		String_t* L_24 = ___pathStr0;
		int32_t L_25 = V_2;
		String_t* L_26 = ___pathStr0;
		NullCheck(L_26);
		int32_t L_27 = String_get_Length_m3847582255(L_26, /*hidden argument*/NULL);
		int32_t L_28 = V_2;
		NullCheck(L_24);
		String_t* L_29 = String_Substring_m1610150815(L_24, ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)2)))), /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = String_Trim_m923598732(L_29, /*hidden argument*/NULL);
		V_6 = L_30;
		String_t* L_31 = V_3;
		float L_32 = Single_Parse_m364357836(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		String_t* L_33 = V_4;
		float L_34 = Single_Parse_m364357836(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		String_t* L_35 = V_5;
		float L_36 = Single_Parse_m364357836(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		String_t* L_37 = V_6;
		float L_38 = Single_Parse_m364357836(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		Vector4_t3319028937  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector4__ctor_m2498754347((&L_39), L_32, L_34, L_36, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// System.String Sirenix.Serialization.Vector4DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector4)
extern "C" IL2CPP_METHOD_ATTR String_t* Vector4DictionaryKeyPathProvider_GetPathStringFromKey_m1680229459 (Vector4DictionaryKeyPathProvider_t4213655263 * __this, Vector4_t3319028937  ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4DictionaryKeyPathProvider_GetPathStringFromKey_m1680229459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		float* L_0 = (&___key0)->get_address_of_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_1 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Single_ToString_m543431371((float*)L_0, _stringLiteral3452614638, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float* L_3 = (&___key0)->get_address_of_y_2();
		CultureInfo_t4157843068 * L_4 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = Single_ToString_m543431371((float*)L_3, _stringLiteral3452614638, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float* L_6 = (&___key0)->get_address_of_z_3();
		CultureInfo_t4157843068 * L_7 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Single_ToString_m543431371((float*)L_6, _stringLiteral3452614638, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float* L_9 = (&___key0)->get_address_of_w_4();
		CultureInfo_t4157843068 * L_10 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_11 = Single_ToString_m543431371((float*)L_9, _stringLiteral3452614638, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		StringU5BU5D_t1281789340* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral3452614536);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614536);
		StringU5BU5D_t1281789340* L_14 = L_13;
		String_t* L_15 = V_0;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_15);
		StringU5BU5D_t1281789340* L_16 = L_14;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral3452614612);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614612);
		StringU5BU5D_t1281789340* L_17 = L_16;
		String_t* L_18 = V_1;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_18);
		StringU5BU5D_t1281789340* L_19 = L_17;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3452614612);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614612);
		StringU5BU5D_t1281789340* L_20 = L_19;
		String_t* L_21 = V_2;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_21);
		StringU5BU5D_t1281789340* L_22 = L_20;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral3452614612);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3452614612);
		StringU5BU5D_t1281789340* L_23 = L_22;
		String_t* L_24 = V_3;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_24);
		StringU5BU5D_t1281789340* L_25 = L_23;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral3452614535);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral3452614535);
		String_t* L_26 = String_Concat_m1809518182(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = String_Replace_m3726209165(L_26, ((int32_t)46), ((int32_t)44), /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Void Sirenix.Serialization.Vector4DictionaryKeyPathProvider::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Vector4DictionaryKeyPathProvider__ctor_m4226819291 (Vector4DictionaryKeyPathProvider_t4213655263 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4DictionaryKeyPathProvider__ctor_m4226819291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BaseDictionaryKeyPathProvider_1__ctor_m607895852(__this, /*hidden argument*/BaseDictionaryKeyPathProvider_1__ctor_m607895852_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.Serialization.Vector4Formatter::Read(UnityEngine.Vector4&,Sirenix.Serialization.IDataReader)
extern "C" IL2CPP_METHOD_ATTR void Vector4Formatter_Read_m2959555402 (Vector4Formatter_t2519996543 * __this, Vector4_t3319028937 * ___value0, RuntimeObject* ___reader1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4Formatter_Read_m2959555402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t3319028937 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_1 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_2 = ___reader1;
		NullCheck(L_1);
		float L_3 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_1, L_2);
		L_0->set_x_1(L_3);
		Vector4_t3319028937 * L_4 = ___value0;
		Serializer_1_t3074248596 * L_5 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_6 = ___reader1;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_5, L_6);
		L_4->set_y_2(L_7);
		Vector4_t3319028937 * L_8 = ___value0;
		Serializer_1_t3074248596 * L_9 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_10 = ___reader1;
		NullCheck(L_9);
		float L_11 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_9, L_10);
		L_8->set_z_3(L_11);
		Vector4_t3319028937 * L_12 = ___value0;
		Serializer_1_t3074248596 * L_13 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		RuntimeObject* L_14 = ___reader1;
		NullCheck(L_13);
		float L_15 = VirtFuncInvoker1< float, RuntimeObject* >::Invoke(6 /* T Sirenix.Serialization.Serializer`1<System.Single>::ReadValue(Sirenix.Serialization.IDataReader) */, L_13, L_14);
		L_12->set_w_4(L_15);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector4Formatter::Write(UnityEngine.Vector4&,Sirenix.Serialization.IDataWriter)
extern "C" IL2CPP_METHOD_ATTR void Vector4Formatter_Write_m1996430823 (Vector4Formatter_t2519996543 * __this, Vector4_t3319028937 * ___value0, RuntimeObject* ___writer1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4Formatter_Write_m1996430823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_0 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector4_t3319028937 * L_1 = ___value0;
		float L_2 = L_1->get_x_1();
		RuntimeObject* L_3 = ___writer1;
		NullCheck(L_0);
		Serializer_1_WriteValue_m1523856254(L_0, L_2, L_3, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		Serializer_1_t3074248596 * L_4 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector4_t3319028937 * L_5 = ___value0;
		float L_6 = L_5->get_y_2();
		RuntimeObject* L_7 = ___writer1;
		NullCheck(L_4);
		Serializer_1_WriteValue_m1523856254(L_4, L_6, L_7, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		Serializer_1_t3074248596 * L_8 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector4_t3319028937 * L_9 = ___value0;
		float L_10 = L_9->get_z_3();
		RuntimeObject* L_11 = ___writer1;
		NullCheck(L_8);
		Serializer_1_WriteValue_m1523856254(L_8, L_10, L_11, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		Serializer_1_t3074248596 * L_12 = ((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->get_FloatSerializer_1();
		Vector4_t3319028937 * L_13 = ___value0;
		float L_14 = L_13->get_w_4();
		RuntimeObject* L_15 = ___writer1;
		NullCheck(L_12);
		Serializer_1_WriteValue_m1523856254(L_12, L_14, L_15, /*hidden argument*/Serializer_1_WriteValue_m1523856254_RuntimeMethod_var);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector4Formatter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Vector4Formatter__ctor_m1779727181 (Vector4Formatter_t2519996543 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4Formatter__ctor_m1779727181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MinimalBaseFormatter_1_t329046019_il2cpp_TypeInfo_var);
		MinimalBaseFormatter_1__ctor_m4046092137(__this, /*hidden argument*/MinimalBaseFormatter_1__ctor_m4046092137_RuntimeMethod_var);
		return;
	}
}
// System.Void Sirenix.Serialization.Vector4Formatter::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Vector4Formatter__cctor_m3471128330 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4Formatter__cctor_m3471128330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Serializer_t962918574_il2cpp_TypeInfo_var);
		Serializer_1_t3074248596 * L_0 = Serializer_Get_TisSingle_t1397266774_m2693733542(NULL /*static, unused*/, /*hidden argument*/Serializer_Get_TisSingle_t1397266774_m2693733542_RuntimeMethod_var);
		((Vector4Formatter_t2519996543_StaticFields*)il2cpp_codegen_static_fields_for(Vector4Formatter_t2519996543_il2cpp_TypeInfo_var))->set_FloatSerializer_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
