﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t3046556399;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t3943099367;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.Exception
struct Exception_t;
// System.FormatException
struct FormatException_t154580423;
// System.Globalization.Calendar
struct Calendar_t1661121569;
// System.Globalization.CompareInfo
struct CompareInfo_t1092934962;
// System.Globalization.CultureData
struct CultureData_t1899656083;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2405853701;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// System.Globalization.TextInfo
struct TextInfo_t3810425522;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.OverflowException
struct OverflowException_t2020128637;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t1185182177;

extern RuntimeClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern RuntimeClass* CultureInfo_t4157843068_il2cpp_TypeInfo_var;
extern RuntimeClass* DurationType_t3437374716_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* FormatException_t154580423_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* OverflowException_t2020128637_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TimeSpan_t881159249_il2cpp_TypeInfo_var;
extern RuntimeClass* XmlCharType_t2277243275_il2cpp_TypeInfo_var;
extern RuntimeClass* XmlConvert_t1981561327_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255363____5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255363____EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1_FieldInfo_var;
extern String_t* _stringLiteral1126917588;
extern String_t* _stringLiteral1690701919;
extern String_t* _stringLiteral1883235005;
extern String_t* _stringLiteral2032169780;
extern String_t* _stringLiteral2691562295;
extern String_t* _stringLiteral2698841325;
extern String_t* _stringLiteral3232093604;
extern String_t* _stringLiteral3282616573;
extern String_t* _stringLiteral3457005456;
extern String_t* _stringLiteral4239484246;
extern String_t* _stringLiteral608221119;
extern String_t* _stringLiteral760917697;
extern const RuntimeMethod* XmlConvert_ToTimeSpan_m349710467_RuntimeMethod_var;
extern const RuntimeMethod* XsdDuration_ToTimeSpan_m3421138678_RuntimeMethod_var;
extern const RuntimeMethod* XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var;
extern const RuntimeMethod* XsdDuration__ctor_m1650863751_RuntimeMethod_var;
extern const uint32_t SR_GetString_m1882627594_MetadataUsageId;
extern const uint32_t XmlCharType_InitInstance_m2424289755_MetadataUsageId;
extern const uint32_t XmlCharType_SetProperties_m974705761_MetadataUsageId;
extern const uint32_t XmlCharType_get_Instance_m52052032_MetadataUsageId;
extern const uint32_t XmlCharType_get_StaticLock_m1063179012_MetadataUsageId;
extern const uint32_t XmlCharType_t2277243275_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t XmlCharType_t2277243275_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t XmlConvert_ToTimeSpan_m349710467_MetadataUsageId;
extern const uint32_t XmlConvert__cctor_m3143968102_MetadataUsageId;
extern const uint32_t XsdDuration_ToString_m2282140164_MetadataUsageId;
extern const uint32_t XsdDuration_ToTimeSpan_m3421138678_MetadataUsageId;
extern const uint32_t XsdDuration_TryParseDigits_m3033505084_MetadataUsageId;
extern const uint32_t XsdDuration_TryParse_m3581297401_MetadataUsageId;
extern const uint32_t XsdDuration_TryToTimeSpan_m349624696_MetadataUsageId;
extern const uint32_t XsdDuration__ctor_m1650863751_MetadataUsageId;
struct CultureData_t1899656083_marshaled_com;
struct CultureData_t1899656083_marshaled_pinvoke;
struct CultureInfo_t4157843068_marshaled_com;
struct CultureInfo_t4157843068_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_t4116647657;
struct CharU5BU5D_t3528271667;
struct ObjectU5BU5D_t2843939325;


#ifndef U3CMODULEU3E_T692745527_H
#define U3CMODULEU3E_T692745527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745527 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745527_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SR_T167583544_H
#define SR_T167583544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t167583544  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T167583544_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef CULTUREINFO_T4157843068_H
#define CULTUREINFO_T4157843068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t4157843068  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t435877138 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t3810425522 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t1281789340* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1092934962 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1661121569 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t4157843068 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t4116647657* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t1899656083 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___numInfo_10)); }
	inline NumberFormatInfo_t435877138 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t435877138 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t435877138 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_10), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t2405853701 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t2405853701 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t2405853701 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_11), value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textInfo_12)); }
	inline TextInfo_t3810425522 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t3810425522 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t3810425522 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_12), value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_13), value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_14), value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_15), value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_16), value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_17), value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_18), value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((&___territory_19), value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___native_calendar_names_20)); }
	inline StringU5BU5D_t1281789340* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t1281789340** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t1281789340* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((&___native_calendar_names_20), value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___compareInfo_21)); }
	inline CompareInfo_t1092934962 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t1092934962 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t1092934962 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_21), value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___calendar_24)); }
	inline Calendar_t1661121569 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t1661121569 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t1661121569 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_24), value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___parent_culture_25)); }
	inline CultureInfo_t4157843068 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t4157843068 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t4157843068 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_25), value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_t4116647657* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_t4116647657* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_27), value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_cultureData_28)); }
	inline CultureData_t1899656083 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t1899656083 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t1899656083 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_cultureData_28), value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t4157843068_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t4157843068 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t4157843068 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t4157843068 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t4157843068 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t3046556399 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t3943099367 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t4157843068 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t4157843068 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t4157843068 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_0), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_1), value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t4157843068 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t4157843068 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t4157843068 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((&___default_current_culture_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t4157843068 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t4157843068 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t4157843068 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentUICulture_33), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t4157843068 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t4157843068 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t4157843068 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentCulture_34), value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t3046556399 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t3046556399 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t3046556399 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_35), value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t3943099367 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t3943099367 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t3943099367 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_36), value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t4157843068_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t4157843068_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t435877138 * ___numInfo_10;
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	TextInfo_t3810425522 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1092934962 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t1661121569 * ___calendar_24;
	CultureInfo_t4157843068_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_t1899656083_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t4157843068_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t435877138 * ___numInfo_10;
	DateTimeFormatInfo_t2405853701 * ___dateTimeInfo_11;
	TextInfo_t3810425522 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1092934962 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t1661121569 * ___calendar_24;
	CultureInfo_t4157843068_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_t1899656083_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
#endif // CULTUREINFO_T4157843068_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t3528271667* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t3528271667* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t3528271667* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkChars_0), value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkPrevious_1), value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef RES_T3627928856_H
#define RES_T3627928856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Res
struct  Res_t3627928856  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RES_T3627928856_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T3217689075_H
#define __STATICARRAYINITTYPESIZEU3D6_T3217689075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6
struct  __StaticArrayInitTypeSizeU3D6_t3217689075 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t3217689075__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T3217689075_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef UINT64_T4134040092_H
#define UINT64_T4134040092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t4134040092 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t4134040092, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T4134040092_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef XSDDURATION_T3210533101_H
#define XSDDURATION_T3210533101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration
struct  XsdDuration_t3210533101 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration::years
	int32_t ___years_0;
	// System.Int32 System.Xml.Schema.XsdDuration::months
	int32_t ___months_1;
	// System.Int32 System.Xml.Schema.XsdDuration::days
	int32_t ___days_2;
	// System.Int32 System.Xml.Schema.XsdDuration::hours
	int32_t ___hours_3;
	// System.Int32 System.Xml.Schema.XsdDuration::minutes
	int32_t ___minutes_4;
	// System.Int32 System.Xml.Schema.XsdDuration::seconds
	int32_t ___seconds_5;
	// System.UInt32 System.Xml.Schema.XsdDuration::nanoseconds
	uint32_t ___nanoseconds_6;

public:
	inline static int32_t get_offset_of_years_0() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___years_0)); }
	inline int32_t get_years_0() const { return ___years_0; }
	inline int32_t* get_address_of_years_0() { return &___years_0; }
	inline void set_years_0(int32_t value)
	{
		___years_0 = value;
	}

	inline static int32_t get_offset_of_months_1() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___months_1)); }
	inline int32_t get_months_1() const { return ___months_1; }
	inline int32_t* get_address_of_months_1() { return &___months_1; }
	inline void set_months_1(int32_t value)
	{
		___months_1 = value;
	}

	inline static int32_t get_offset_of_days_2() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___days_2)); }
	inline int32_t get_days_2() const { return ___days_2; }
	inline int32_t* get_address_of_days_2() { return &___days_2; }
	inline void set_days_2(int32_t value)
	{
		___days_2 = value;
	}

	inline static int32_t get_offset_of_hours_3() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___hours_3)); }
	inline int32_t get_hours_3() const { return ___hours_3; }
	inline int32_t* get_address_of_hours_3() { return &___hours_3; }
	inline void set_hours_3(int32_t value)
	{
		___hours_3 = value;
	}

	inline static int32_t get_offset_of_minutes_4() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___minutes_4)); }
	inline int32_t get_minutes_4() const { return ___minutes_4; }
	inline int32_t* get_address_of_minutes_4() { return &___minutes_4; }
	inline void set_minutes_4(int32_t value)
	{
		___minutes_4 = value;
	}

	inline static int32_t get_offset_of_seconds_5() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___seconds_5)); }
	inline int32_t get_seconds_5() const { return ___seconds_5; }
	inline int32_t* get_address_of_seconds_5() { return &___seconds_5; }
	inline void set_seconds_5(int32_t value)
	{
		___seconds_5 = value;
	}

	inline static int32_t get_offset_of_nanoseconds_6() { return static_cast<int32_t>(offsetof(XsdDuration_t3210533101, ___nanoseconds_6)); }
	inline uint32_t get_nanoseconds_6() const { return ___nanoseconds_6; }
	inline uint32_t* get_address_of_nanoseconds_6() { return &___nanoseconds_6; }
	inline void set_nanoseconds_6(uint32_t value)
	{
		___nanoseconds_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_T3210533101_H
#ifndef XMLCHARTYPE_T2277243275_H
#define XMLCHARTYPE_T2277243275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t2277243275 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t4116647657* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275, ___charProperties_2)); }
	inline ByteU5BU5D_t4116647657* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t4116647657* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t2277243275_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t4116647657* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t2277243275_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t4116647657* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t4116647657* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t2277243275_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T2277243275_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255363_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255363  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=6 <PrivateImplementationDetails>::5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98
	__StaticArrayInitTypeSizeU3D6_t3217689075  ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0;
	// System.Int64 <PrivateImplementationDetails>::EBC658B067B5C785A3F0BB67D73755F6FEE7F70C
	int64_t ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1;

public:
	inline static int32_t get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0)); }
	inline __StaticArrayInitTypeSizeU3D6_t3217689075  get_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() const { return ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0; }
	inline __StaticArrayInitTypeSizeU3D6_t3217689075 * get_address_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() { return &___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0; }
	inline void set_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0(__StaticArrayInitTypeSizeU3D6_t3217689075  value)
	{
		___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0 = value;
	}

	inline static int32_t get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields, ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1)); }
	inline int64_t get_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1() const { return ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1; }
	inline int64_t* get_address_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1() { return &___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1; }
	inline void set_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1(int64_t value)
	{
		___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255363_H
#ifndef ARITHMETICEXCEPTION_T4283546778_H
#define ARITHMETICEXCEPTION_T4283546778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArithmeticException
struct  ArithmeticException_t4283546778  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARITHMETICEXCEPTION_T4283546778_H
#ifndef FORMATEXCEPTION_T154580423_H
#define FORMATEXCEPTION_T154580423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t154580423  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T154580423_H
#ifndef NUMBERSTYLES_T617258130_H
#define NUMBERSTYLES_T617258130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_t617258130 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NumberStyles_t617258130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_T617258130_H
#ifndef RUNTIMEFIELDHANDLE_T1871169219_H
#define RUNTIMEFIELDHANDLE_T1871169219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1871169219 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1871169219, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1871169219_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef DURATIONTYPE_T3437374716_H
#define DURATIONTYPE_T3437374716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration/DurationType
struct  DurationType_t3437374716 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration/DurationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DurationType_t3437374716, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONTYPE_T3437374716_H
#ifndef PARTS_T986625524_H
#define PARTS_T986625524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDuration/Parts
struct  Parts_t986625524 
{
public:
	// System.Int32 System.Xml.Schema.XsdDuration/Parts::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Parts_t986625524, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTS_T986625524_H
#ifndef XMLCONVERT_T1981561327_H
#define XMLCONVERT_T1981561327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1981561327  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t1981561327_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.XmlConvert::xmlCharType
	XmlCharType_t2277243275  ___xmlCharType_0;
	// System.Char[] System.Xml.XmlConvert::crt
	CharU5BU5D_t3528271667* ___crt_1;
	// System.Int32 System.Xml.XmlConvert::c_EncodedCharLength
	int32_t ___c_EncodedCharLength_2;
	// System.Char[] System.Xml.XmlConvert::WhitespaceChars
	CharU5BU5D_t3528271667* ___WhitespaceChars_3;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t2277243275  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t2277243275 * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t2277243275  value)
	{
		___xmlCharType_0 = value;
	}

	inline static int32_t get_offset_of_crt_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___crt_1)); }
	inline CharU5BU5D_t3528271667* get_crt_1() const { return ___crt_1; }
	inline CharU5BU5D_t3528271667** get_address_of_crt_1() { return &___crt_1; }
	inline void set_crt_1(CharU5BU5D_t3528271667* value)
	{
		___crt_1 = value;
		Il2CppCodeGenWriteBarrier((&___crt_1), value);
	}

	inline static int32_t get_offset_of_c_EncodedCharLength_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___c_EncodedCharLength_2)); }
	inline int32_t get_c_EncodedCharLength_2() const { return ___c_EncodedCharLength_2; }
	inline int32_t* get_address_of_c_EncodedCharLength_2() { return &___c_EncodedCharLength_2; }
	inline void set_c_EncodedCharLength_2(int32_t value)
	{
		___c_EncodedCharLength_2 = value;
	}

	inline static int32_t get_offset_of_WhitespaceChars_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1981561327_StaticFields, ___WhitespaceChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhitespaceChars_3() const { return ___WhitespaceChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhitespaceChars_3() { return &___WhitespaceChars_3; }
	inline void set_WhitespaceChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhitespaceChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T1981561327_H
#ifndef NUMBERFORMATINFO_T435877138_H
#define NUMBERFORMATINFO_T435877138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberFormatInfo
struct  NumberFormatInfo_t435877138  : public RuntimeObject
{
public:
	// System.Int32[] System.Globalization.NumberFormatInfo::numberGroupSizes
	Int32U5BU5D_t385246372* ___numberGroupSizes_1;
	// System.Int32[] System.Globalization.NumberFormatInfo::currencyGroupSizes
	Int32U5BU5D_t385246372* ___currencyGroupSizes_2;
	// System.Int32[] System.Globalization.NumberFormatInfo::percentGroupSizes
	Int32U5BU5D_t385246372* ___percentGroupSizes_3;
	// System.String System.Globalization.NumberFormatInfo::positiveSign
	String_t* ___positiveSign_4;
	// System.String System.Globalization.NumberFormatInfo::negativeSign
	String_t* ___negativeSign_5;
	// System.String System.Globalization.NumberFormatInfo::numberDecimalSeparator
	String_t* ___numberDecimalSeparator_6;
	// System.String System.Globalization.NumberFormatInfo::numberGroupSeparator
	String_t* ___numberGroupSeparator_7;
	// System.String System.Globalization.NumberFormatInfo::currencyGroupSeparator
	String_t* ___currencyGroupSeparator_8;
	// System.String System.Globalization.NumberFormatInfo::currencyDecimalSeparator
	String_t* ___currencyDecimalSeparator_9;
	// System.String System.Globalization.NumberFormatInfo::currencySymbol
	String_t* ___currencySymbol_10;
	// System.String System.Globalization.NumberFormatInfo::ansiCurrencySymbol
	String_t* ___ansiCurrencySymbol_11;
	// System.String System.Globalization.NumberFormatInfo::nanSymbol
	String_t* ___nanSymbol_12;
	// System.String System.Globalization.NumberFormatInfo::positiveInfinitySymbol
	String_t* ___positiveInfinitySymbol_13;
	// System.String System.Globalization.NumberFormatInfo::negativeInfinitySymbol
	String_t* ___negativeInfinitySymbol_14;
	// System.String System.Globalization.NumberFormatInfo::percentDecimalSeparator
	String_t* ___percentDecimalSeparator_15;
	// System.String System.Globalization.NumberFormatInfo::percentGroupSeparator
	String_t* ___percentGroupSeparator_16;
	// System.String System.Globalization.NumberFormatInfo::percentSymbol
	String_t* ___percentSymbol_17;
	// System.String System.Globalization.NumberFormatInfo::perMilleSymbol
	String_t* ___perMilleSymbol_18;
	// System.String[] System.Globalization.NumberFormatInfo::nativeDigits
	StringU5BU5D_t1281789340* ___nativeDigits_19;
	// System.Int32 System.Globalization.NumberFormatInfo::m_dataItem
	int32_t ___m_dataItem_20;
	// System.Int32 System.Globalization.NumberFormatInfo::numberDecimalDigits
	int32_t ___numberDecimalDigits_21;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyDecimalDigits
	int32_t ___currencyDecimalDigits_22;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyPositivePattern
	int32_t ___currencyPositivePattern_23;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyNegativePattern
	int32_t ___currencyNegativePattern_24;
	// System.Int32 System.Globalization.NumberFormatInfo::numberNegativePattern
	int32_t ___numberNegativePattern_25;
	// System.Int32 System.Globalization.NumberFormatInfo::percentPositivePattern
	int32_t ___percentPositivePattern_26;
	// System.Int32 System.Globalization.NumberFormatInfo::percentNegativePattern
	int32_t ___percentNegativePattern_27;
	// System.Int32 System.Globalization.NumberFormatInfo::percentDecimalDigits
	int32_t ___percentDecimalDigits_28;
	// System.Int32 System.Globalization.NumberFormatInfo::digitSubstitution
	int32_t ___digitSubstitution_29;
	// System.Boolean System.Globalization.NumberFormatInfo::isReadOnly
	bool ___isReadOnly_30;
	// System.Boolean System.Globalization.NumberFormatInfo::m_useUserOverride
	bool ___m_useUserOverride_31;
	// System.Boolean System.Globalization.NumberFormatInfo::m_isInvariant
	bool ___m_isInvariant_32;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsNumber
	bool ___validForParseAsNumber_33;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsCurrency
	bool ___validForParseAsCurrency_34;

public:
	inline static int32_t get_offset_of_numberGroupSizes_1() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___numberGroupSizes_1)); }
	inline Int32U5BU5D_t385246372* get_numberGroupSizes_1() const { return ___numberGroupSizes_1; }
	inline Int32U5BU5D_t385246372** get_address_of_numberGroupSizes_1() { return &___numberGroupSizes_1; }
	inline void set_numberGroupSizes_1(Int32U5BU5D_t385246372* value)
	{
		___numberGroupSizes_1 = value;
		Il2CppCodeGenWriteBarrier((&___numberGroupSizes_1), value);
	}

	inline static int32_t get_offset_of_currencyGroupSizes_2() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencyGroupSizes_2)); }
	inline Int32U5BU5D_t385246372* get_currencyGroupSizes_2() const { return ___currencyGroupSizes_2; }
	inline Int32U5BU5D_t385246372** get_address_of_currencyGroupSizes_2() { return &___currencyGroupSizes_2; }
	inline void set_currencyGroupSizes_2(Int32U5BU5D_t385246372* value)
	{
		___currencyGroupSizes_2 = value;
		Il2CppCodeGenWriteBarrier((&___currencyGroupSizes_2), value);
	}

	inline static int32_t get_offset_of_percentGroupSizes_3() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentGroupSizes_3)); }
	inline Int32U5BU5D_t385246372* get_percentGroupSizes_3() const { return ___percentGroupSizes_3; }
	inline Int32U5BU5D_t385246372** get_address_of_percentGroupSizes_3() { return &___percentGroupSizes_3; }
	inline void set_percentGroupSizes_3(Int32U5BU5D_t385246372* value)
	{
		___percentGroupSizes_3 = value;
		Il2CppCodeGenWriteBarrier((&___percentGroupSizes_3), value);
	}

	inline static int32_t get_offset_of_positiveSign_4() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___positiveSign_4)); }
	inline String_t* get_positiveSign_4() const { return ___positiveSign_4; }
	inline String_t** get_address_of_positiveSign_4() { return &___positiveSign_4; }
	inline void set_positiveSign_4(String_t* value)
	{
		___positiveSign_4 = value;
		Il2CppCodeGenWriteBarrier((&___positiveSign_4), value);
	}

	inline static int32_t get_offset_of_negativeSign_5() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___negativeSign_5)); }
	inline String_t* get_negativeSign_5() const { return ___negativeSign_5; }
	inline String_t** get_address_of_negativeSign_5() { return &___negativeSign_5; }
	inline void set_negativeSign_5(String_t* value)
	{
		___negativeSign_5 = value;
		Il2CppCodeGenWriteBarrier((&___negativeSign_5), value);
	}

	inline static int32_t get_offset_of_numberDecimalSeparator_6() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___numberDecimalSeparator_6)); }
	inline String_t* get_numberDecimalSeparator_6() const { return ___numberDecimalSeparator_6; }
	inline String_t** get_address_of_numberDecimalSeparator_6() { return &___numberDecimalSeparator_6; }
	inline void set_numberDecimalSeparator_6(String_t* value)
	{
		___numberDecimalSeparator_6 = value;
		Il2CppCodeGenWriteBarrier((&___numberDecimalSeparator_6), value);
	}

	inline static int32_t get_offset_of_numberGroupSeparator_7() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___numberGroupSeparator_7)); }
	inline String_t* get_numberGroupSeparator_7() const { return ___numberGroupSeparator_7; }
	inline String_t** get_address_of_numberGroupSeparator_7() { return &___numberGroupSeparator_7; }
	inline void set_numberGroupSeparator_7(String_t* value)
	{
		___numberGroupSeparator_7 = value;
		Il2CppCodeGenWriteBarrier((&___numberGroupSeparator_7), value);
	}

	inline static int32_t get_offset_of_currencyGroupSeparator_8() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencyGroupSeparator_8)); }
	inline String_t* get_currencyGroupSeparator_8() const { return ___currencyGroupSeparator_8; }
	inline String_t** get_address_of_currencyGroupSeparator_8() { return &___currencyGroupSeparator_8; }
	inline void set_currencyGroupSeparator_8(String_t* value)
	{
		___currencyGroupSeparator_8 = value;
		Il2CppCodeGenWriteBarrier((&___currencyGroupSeparator_8), value);
	}

	inline static int32_t get_offset_of_currencyDecimalSeparator_9() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencyDecimalSeparator_9)); }
	inline String_t* get_currencyDecimalSeparator_9() const { return ___currencyDecimalSeparator_9; }
	inline String_t** get_address_of_currencyDecimalSeparator_9() { return &___currencyDecimalSeparator_9; }
	inline void set_currencyDecimalSeparator_9(String_t* value)
	{
		___currencyDecimalSeparator_9 = value;
		Il2CppCodeGenWriteBarrier((&___currencyDecimalSeparator_9), value);
	}

	inline static int32_t get_offset_of_currencySymbol_10() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencySymbol_10)); }
	inline String_t* get_currencySymbol_10() const { return ___currencySymbol_10; }
	inline String_t** get_address_of_currencySymbol_10() { return &___currencySymbol_10; }
	inline void set_currencySymbol_10(String_t* value)
	{
		___currencySymbol_10 = value;
		Il2CppCodeGenWriteBarrier((&___currencySymbol_10), value);
	}

	inline static int32_t get_offset_of_ansiCurrencySymbol_11() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___ansiCurrencySymbol_11)); }
	inline String_t* get_ansiCurrencySymbol_11() const { return ___ansiCurrencySymbol_11; }
	inline String_t** get_address_of_ansiCurrencySymbol_11() { return &___ansiCurrencySymbol_11; }
	inline void set_ansiCurrencySymbol_11(String_t* value)
	{
		___ansiCurrencySymbol_11 = value;
		Il2CppCodeGenWriteBarrier((&___ansiCurrencySymbol_11), value);
	}

	inline static int32_t get_offset_of_nanSymbol_12() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___nanSymbol_12)); }
	inline String_t* get_nanSymbol_12() const { return ___nanSymbol_12; }
	inline String_t** get_address_of_nanSymbol_12() { return &___nanSymbol_12; }
	inline void set_nanSymbol_12(String_t* value)
	{
		___nanSymbol_12 = value;
		Il2CppCodeGenWriteBarrier((&___nanSymbol_12), value);
	}

	inline static int32_t get_offset_of_positiveInfinitySymbol_13() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___positiveInfinitySymbol_13)); }
	inline String_t* get_positiveInfinitySymbol_13() const { return ___positiveInfinitySymbol_13; }
	inline String_t** get_address_of_positiveInfinitySymbol_13() { return &___positiveInfinitySymbol_13; }
	inline void set_positiveInfinitySymbol_13(String_t* value)
	{
		___positiveInfinitySymbol_13 = value;
		Il2CppCodeGenWriteBarrier((&___positiveInfinitySymbol_13), value);
	}

	inline static int32_t get_offset_of_negativeInfinitySymbol_14() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___negativeInfinitySymbol_14)); }
	inline String_t* get_negativeInfinitySymbol_14() const { return ___negativeInfinitySymbol_14; }
	inline String_t** get_address_of_negativeInfinitySymbol_14() { return &___negativeInfinitySymbol_14; }
	inline void set_negativeInfinitySymbol_14(String_t* value)
	{
		___negativeInfinitySymbol_14 = value;
		Il2CppCodeGenWriteBarrier((&___negativeInfinitySymbol_14), value);
	}

	inline static int32_t get_offset_of_percentDecimalSeparator_15() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentDecimalSeparator_15)); }
	inline String_t* get_percentDecimalSeparator_15() const { return ___percentDecimalSeparator_15; }
	inline String_t** get_address_of_percentDecimalSeparator_15() { return &___percentDecimalSeparator_15; }
	inline void set_percentDecimalSeparator_15(String_t* value)
	{
		___percentDecimalSeparator_15 = value;
		Il2CppCodeGenWriteBarrier((&___percentDecimalSeparator_15), value);
	}

	inline static int32_t get_offset_of_percentGroupSeparator_16() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentGroupSeparator_16)); }
	inline String_t* get_percentGroupSeparator_16() const { return ___percentGroupSeparator_16; }
	inline String_t** get_address_of_percentGroupSeparator_16() { return &___percentGroupSeparator_16; }
	inline void set_percentGroupSeparator_16(String_t* value)
	{
		___percentGroupSeparator_16 = value;
		Il2CppCodeGenWriteBarrier((&___percentGroupSeparator_16), value);
	}

	inline static int32_t get_offset_of_percentSymbol_17() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentSymbol_17)); }
	inline String_t* get_percentSymbol_17() const { return ___percentSymbol_17; }
	inline String_t** get_address_of_percentSymbol_17() { return &___percentSymbol_17; }
	inline void set_percentSymbol_17(String_t* value)
	{
		___percentSymbol_17 = value;
		Il2CppCodeGenWriteBarrier((&___percentSymbol_17), value);
	}

	inline static int32_t get_offset_of_perMilleSymbol_18() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___perMilleSymbol_18)); }
	inline String_t* get_perMilleSymbol_18() const { return ___perMilleSymbol_18; }
	inline String_t** get_address_of_perMilleSymbol_18() { return &___perMilleSymbol_18; }
	inline void set_perMilleSymbol_18(String_t* value)
	{
		___perMilleSymbol_18 = value;
		Il2CppCodeGenWriteBarrier((&___perMilleSymbol_18), value);
	}

	inline static int32_t get_offset_of_nativeDigits_19() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___nativeDigits_19)); }
	inline StringU5BU5D_t1281789340* get_nativeDigits_19() const { return ___nativeDigits_19; }
	inline StringU5BU5D_t1281789340** get_address_of_nativeDigits_19() { return &___nativeDigits_19; }
	inline void set_nativeDigits_19(StringU5BU5D_t1281789340* value)
	{
		___nativeDigits_19 = value;
		Il2CppCodeGenWriteBarrier((&___nativeDigits_19), value);
	}

	inline static int32_t get_offset_of_m_dataItem_20() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___m_dataItem_20)); }
	inline int32_t get_m_dataItem_20() const { return ___m_dataItem_20; }
	inline int32_t* get_address_of_m_dataItem_20() { return &___m_dataItem_20; }
	inline void set_m_dataItem_20(int32_t value)
	{
		___m_dataItem_20 = value;
	}

	inline static int32_t get_offset_of_numberDecimalDigits_21() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___numberDecimalDigits_21)); }
	inline int32_t get_numberDecimalDigits_21() const { return ___numberDecimalDigits_21; }
	inline int32_t* get_address_of_numberDecimalDigits_21() { return &___numberDecimalDigits_21; }
	inline void set_numberDecimalDigits_21(int32_t value)
	{
		___numberDecimalDigits_21 = value;
	}

	inline static int32_t get_offset_of_currencyDecimalDigits_22() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencyDecimalDigits_22)); }
	inline int32_t get_currencyDecimalDigits_22() const { return ___currencyDecimalDigits_22; }
	inline int32_t* get_address_of_currencyDecimalDigits_22() { return &___currencyDecimalDigits_22; }
	inline void set_currencyDecimalDigits_22(int32_t value)
	{
		___currencyDecimalDigits_22 = value;
	}

	inline static int32_t get_offset_of_currencyPositivePattern_23() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencyPositivePattern_23)); }
	inline int32_t get_currencyPositivePattern_23() const { return ___currencyPositivePattern_23; }
	inline int32_t* get_address_of_currencyPositivePattern_23() { return &___currencyPositivePattern_23; }
	inline void set_currencyPositivePattern_23(int32_t value)
	{
		___currencyPositivePattern_23 = value;
	}

	inline static int32_t get_offset_of_currencyNegativePattern_24() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___currencyNegativePattern_24)); }
	inline int32_t get_currencyNegativePattern_24() const { return ___currencyNegativePattern_24; }
	inline int32_t* get_address_of_currencyNegativePattern_24() { return &___currencyNegativePattern_24; }
	inline void set_currencyNegativePattern_24(int32_t value)
	{
		___currencyNegativePattern_24 = value;
	}

	inline static int32_t get_offset_of_numberNegativePattern_25() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___numberNegativePattern_25)); }
	inline int32_t get_numberNegativePattern_25() const { return ___numberNegativePattern_25; }
	inline int32_t* get_address_of_numberNegativePattern_25() { return &___numberNegativePattern_25; }
	inline void set_numberNegativePattern_25(int32_t value)
	{
		___numberNegativePattern_25 = value;
	}

	inline static int32_t get_offset_of_percentPositivePattern_26() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentPositivePattern_26)); }
	inline int32_t get_percentPositivePattern_26() const { return ___percentPositivePattern_26; }
	inline int32_t* get_address_of_percentPositivePattern_26() { return &___percentPositivePattern_26; }
	inline void set_percentPositivePattern_26(int32_t value)
	{
		___percentPositivePattern_26 = value;
	}

	inline static int32_t get_offset_of_percentNegativePattern_27() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentNegativePattern_27)); }
	inline int32_t get_percentNegativePattern_27() const { return ___percentNegativePattern_27; }
	inline int32_t* get_address_of_percentNegativePattern_27() { return &___percentNegativePattern_27; }
	inline void set_percentNegativePattern_27(int32_t value)
	{
		___percentNegativePattern_27 = value;
	}

	inline static int32_t get_offset_of_percentDecimalDigits_28() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___percentDecimalDigits_28)); }
	inline int32_t get_percentDecimalDigits_28() const { return ___percentDecimalDigits_28; }
	inline int32_t* get_address_of_percentDecimalDigits_28() { return &___percentDecimalDigits_28; }
	inline void set_percentDecimalDigits_28(int32_t value)
	{
		___percentDecimalDigits_28 = value;
	}

	inline static int32_t get_offset_of_digitSubstitution_29() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___digitSubstitution_29)); }
	inline int32_t get_digitSubstitution_29() const { return ___digitSubstitution_29; }
	inline int32_t* get_address_of_digitSubstitution_29() { return &___digitSubstitution_29; }
	inline void set_digitSubstitution_29(int32_t value)
	{
		___digitSubstitution_29 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_30() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___isReadOnly_30)); }
	inline bool get_isReadOnly_30() const { return ___isReadOnly_30; }
	inline bool* get_address_of_isReadOnly_30() { return &___isReadOnly_30; }
	inline void set_isReadOnly_30(bool value)
	{
		___isReadOnly_30 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_31() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___m_useUserOverride_31)); }
	inline bool get_m_useUserOverride_31() const { return ___m_useUserOverride_31; }
	inline bool* get_address_of_m_useUserOverride_31() { return &___m_useUserOverride_31; }
	inline void set_m_useUserOverride_31(bool value)
	{
		___m_useUserOverride_31 = value;
	}

	inline static int32_t get_offset_of_m_isInvariant_32() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___m_isInvariant_32)); }
	inline bool get_m_isInvariant_32() const { return ___m_isInvariant_32; }
	inline bool* get_address_of_m_isInvariant_32() { return &___m_isInvariant_32; }
	inline void set_m_isInvariant_32(bool value)
	{
		___m_isInvariant_32 = value;
	}

	inline static int32_t get_offset_of_validForParseAsNumber_33() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___validForParseAsNumber_33)); }
	inline bool get_validForParseAsNumber_33() const { return ___validForParseAsNumber_33; }
	inline bool* get_address_of_validForParseAsNumber_33() { return &___validForParseAsNumber_33; }
	inline void set_validForParseAsNumber_33(bool value)
	{
		___validForParseAsNumber_33 = value;
	}

	inline static int32_t get_offset_of_validForParseAsCurrency_34() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138, ___validForParseAsCurrency_34)); }
	inline bool get_validForParseAsCurrency_34() const { return ___validForParseAsCurrency_34; }
	inline bool* get_address_of_validForParseAsCurrency_34() { return &___validForParseAsCurrency_34; }
	inline void set_validForParseAsCurrency_34(bool value)
	{
		___validForParseAsCurrency_34 = value;
	}
};

struct NumberFormatInfo_t435877138_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.NumberFormatInfo::invariantInfo
	NumberFormatInfo_t435877138 * ___invariantInfo_0;

public:
	inline static int32_t get_offset_of_invariantInfo_0() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t435877138_StaticFields, ___invariantInfo_0)); }
	inline NumberFormatInfo_t435877138 * get_invariantInfo_0() const { return ___invariantInfo_0; }
	inline NumberFormatInfo_t435877138 ** get_address_of_invariantInfo_0() { return &___invariantInfo_0; }
	inline void set_invariantInfo_0(NumberFormatInfo_t435877138 * value)
	{
		___invariantInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariantInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERFORMATINFO_T435877138_H
#ifndef OVERFLOWEXCEPTION_T2020128637_H
#define OVERFLOWEXCEPTION_T2020128637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.OverflowException
struct  OverflowException_t2020128637  : public ArithmeticException_t4283546778
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERFLOWEXCEPTION_T2020128637_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};



// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C" IL2CPP_METHOD_ATTR CultureInfo_t4157843068 * CultureInfo_get_InvariantCulture_m3532445182 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* SR_GetString_m307192714 (RuntimeObject * __this /* static, unused */, CultureInfo_t4157843068 * ___culture0, String_t* ___name1, ObjectU5BU5D_t2843939325* ___args2, const RuntimeMethod* method);
// System.String System.String::Format(System.IFormatProvider,System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m1881875187 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, String_t* p1, ObjectU5BU5D_t2843939325* p2, const RuntimeMethod* method);
// System.String SR::GetString(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* SR_GetString_m1882627594 (RuntimeObject * __this /* static, unused */, String_t* ___name0, ObjectU5BU5D_t2843939325* ___args1, const RuntimeMethod* method);
// System.Void System.Xml.Schema.XsdDuration::.ctor(System.String,System.Xml.Schema.XsdDuration/DurationType)
extern "C" IL2CPP_METHOD_ATTR void XsdDuration__ctor_m1650863751 (XsdDuration_t3210533101 * __this, String_t* ___s0, int32_t ___durationType1, const RuntimeMethod* method);
// System.Void System.Xml.Schema.XsdDuration::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void XsdDuration__ctor_m3570494855 (XsdDuration_t3210533101 * __this, String_t* ___s0, const RuntimeMethod* method);
// System.Exception System.Xml.Schema.XsdDuration::TryParse(System.String,System.Xml.Schema.XsdDuration/DurationType,System.Xml.Schema.XsdDuration&)
extern "C" IL2CPP_METHOD_ATTR Exception_t * XsdDuration_TryParse_m3581297401 (RuntimeObject * __this /* static, unused */, String_t* ___s0, int32_t ___durationType1, XsdDuration_t3210533101 * ___result2, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Years()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Years_m4035894877 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Months()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Months_m3021984581 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Days()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Days_m1425625717 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Hours()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Hours_m3544702211 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Minutes()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Minutes_m2155056537 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Seconds()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Seconds_m1180281066 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.Schema.XsdDuration::get_Nanoseconds()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Nanoseconds_m4094124966 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Boolean System.Xml.Schema.XsdDuration::get_IsNegative()
extern "C" IL2CPP_METHOD_ATTR bool XsdDuration_get_IsNegative_m2123881895 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.TimeSpan System.Xml.Schema.XsdDuration::ToTimeSpan(System.Xml.Schema.XsdDuration/DurationType)
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  XsdDuration_ToTimeSpan_m3421138678 (XsdDuration_t3210533101 * __this, int32_t ___durationType0, const RuntimeMethod* method);
// System.TimeSpan System.Xml.Schema.XsdDuration::ToTimeSpan()
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  XsdDuration_ToTimeSpan_m2400096495 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Exception System.Xml.Schema.XsdDuration::TryToTimeSpan(System.Xml.Schema.XsdDuration/DurationType,System.TimeSpan&)
extern "C" IL2CPP_METHOD_ATTR Exception_t * XsdDuration_TryToTimeSpan_m349624696 (XsdDuration_t3210533101 * __this, int32_t ___durationType0, TimeSpan_t881159249 * ___result1, const RuntimeMethod* method);
// System.Void System.TimeSpan::.ctor(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void TimeSpan__ctor_m1896986612 (TimeSpan_t881159249 * __this, int64_t p0, const RuntimeMethod* method);
// System.String System.Xml.Res::GetString(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* Res_GetString_m2746634261 (RuntimeObject * __this /* static, unused */, String_t* ___name0, ObjectU5BU5D_t2843939325* ___args1, const RuntimeMethod* method);
// System.Void System.OverflowException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void OverflowException__ctor_m694321376 (OverflowException_t2020128637 * __this, String_t* p0, const RuntimeMethod* method);
// System.String System.Xml.Schema.XsdDuration::ToString(System.Xml.Schema.XsdDuration/DurationType)
extern "C" IL2CPP_METHOD_ATTR String_t* XsdDuration_ToString_m2282140164 (XsdDuration_t3210533101 * __this, int32_t ___durationType0, const RuntimeMethod* method);
// System.String System.Xml.Schema.XsdDuration::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* XsdDuration_ToString_m3669437819 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void StringBuilder__ctor_m2367297767 (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m2383614642 (StringBuilder_t * __this, Il2CppChar p0, const RuntimeMethod* method);
// System.String System.Xml.XmlConvert::ToString(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* XmlConvert_ToString_m4082948798 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m1965104174 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t StringBuilder_get_Length_m3238060835 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void StringBuilder_set_Length_m1410065908 (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Chars(System.Int32,System.Char)
extern "C" IL2CPP_METHOD_ATTR void StringBuilder_set_Chars_m3548656617 (StringBuilder_t * __this, int32_t p0, Il2CppChar p1, const RuntimeMethod* method);
// System.Char System.Text.StringBuilder::get_Chars(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar StringBuilder_get_Chars_m1819843468 (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method);
// System.String System.String::Trim()
extern "C" IL2CPP_METHOD_ATTR String_t* String_Trim_m923598732 (String_t* __this, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m2986988803 (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.String System.Xml.Schema.XsdDuration::TryParseDigits(System.String,System.Int32&,System.Boolean,System.Int32&,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR String_t* XsdDuration_TryParseDigits_m3033505084 (RuntimeObject * __this /* static, unused */, String_t* ___s0, int32_t* ___offset1, bool ___eatDigits2, int32_t* ___result3, int32_t* ___numDigits4, const RuntimeMethod* method);
// System.Void System.FormatException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void FormatException__ctor_m4049685996 (FormatException_t154580423 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Object System.Xml.XmlCharType::get_StaticLock()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * XmlCharType_get_StaticLock_m1063179012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Enter_m984175629 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, bool* p1, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void XmlCharType_SetProperties_m974705761 (RuntimeObject * __this /* static, unused */, String_t* ___ranges0, uint8_t ___value1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void XmlCharType__ctor_m26891158 (XmlCharType_t2277243275 * __this, ByteU5BU5D_t4116647657* ___charProperties0, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::InitInstance()
extern "C" IL2CPP_METHOD_ATTR void XmlCharType_InitInstance_m2424289755 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_InvariantInfo()
extern "C" IL2CPP_METHOD_ATTR NumberFormatInfo_t435877138 * NumberFormatInfo_get_InvariantInfo_m349577018 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.Int32::ToString(System.String,System.IFormatProvider)
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m2507389746 (int32_t* __this, String_t* p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
extern "C" IL2CPP_METHOD_ATTR XmlCharType_t2277243275  XmlCharType_get_Instance_m52052032 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m3117905507 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1871169219  p1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SR::GetString(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* SR_GetString_m1882627594 (RuntimeObject * __this /* static, unused */, String_t* ___name0, ObjectU5BU5D_t2843939325* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SR_GetString_m1882627594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t4157843068_il2cpp_TypeInfo_var);
		CultureInfo_t4157843068 * L_0 = CultureInfo_get_InvariantCulture_m3532445182(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		ObjectU5BU5D_t2843939325* L_2 = ___args1;
		String_t* L_3 = SR_GetString_m307192714(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* SR_GetString_m307192714 (RuntimeObject * __this /* static, unused */, CultureInfo_t4157843068 * ___culture0, String_t* ___name1, ObjectU5BU5D_t2843939325* ___args2, const RuntimeMethod* method)
{
	{
		CultureInfo_t4157843068 * L_0 = ___culture0;
		String_t* L_1 = ___name1;
		ObjectU5BU5D_t2843939325* L_2 = ___args2;
		String_t* L_3 = String_Format_m1881875187(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Xml.Res::GetString(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* Res_GetString_m2746634261 (RuntimeObject * __this /* static, unused */, String_t* ___name0, ObjectU5BU5D_t2843939325* ___args1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args1;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		String_t* L_1 = ___name0;
		return L_1;
	}

IL_0005:
	{
		String_t* L_2 = ___name0;
		ObjectU5BU5D_t2843939325* L_3 = ___args1;
		String_t* L_4 = SR_GetString_m1882627594(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.Schema.XsdDuration::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void XsdDuration__ctor_m3570494855 (XsdDuration_t3210533101 * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		XsdDuration__ctor_m1650863751((XsdDuration_t3210533101 *)__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void XsdDuration__ctor_m3570494855_AdjustorThunk (RuntimeObject * __this, String_t* ___s0, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	XsdDuration__ctor_m3570494855(_thisAdjusted, ___s0, method);
}
// System.Void System.Xml.Schema.XsdDuration::.ctor(System.String,System.Xml.Schema.XsdDuration/DurationType)
extern "C" IL2CPP_METHOD_ATTR void XsdDuration__ctor_m1650863751 (XsdDuration_t3210533101 * __this, String_t* ___s0, int32_t ___durationType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration__ctor_m1650863751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XsdDuration_t3210533101  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t * V_1 = NULL;
	{
		String_t* L_0 = ___s0;
		int32_t L_1 = ___durationType1;
		Exception_t * L_2 = XsdDuration_TryParse_m3581297401(NULL /*static, unused*/, L_0, L_1, (XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		Exception_t * L_3 = V_1;
		if (!L_3)
		{
			goto IL_000f;
		}
	}
	{
		Exception_t * L_4 = V_1;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, XsdDuration__ctor_m1650863751_RuntimeMethod_var);
	}

IL_000f:
	{
		int32_t L_5 = XsdDuration_get_Years_m4035894877((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_years_0(L_5);
		int32_t L_6 = XsdDuration_get_Months_m3021984581((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_months_1(L_6);
		int32_t L_7 = XsdDuration_get_Days_m1425625717((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_days_2(L_7);
		int32_t L_8 = XsdDuration_get_Hours_m3544702211((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_hours_3(L_8);
		int32_t L_9 = XsdDuration_get_Minutes_m2155056537((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_minutes_4(L_9);
		int32_t L_10 = XsdDuration_get_Seconds_m1180281066((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_seconds_5(L_10);
		int32_t L_11 = XsdDuration_get_Nanoseconds_m4094124966((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		__this->set_nanoseconds_6(L_11);
		bool L_12 = XsdDuration_get_IsNegative_m2123881895((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0085;
		}
	}
	{
		uint32_t L_13 = __this->get_nanoseconds_6();
		__this->set_nanoseconds_6(((int32_t)((int32_t)L_13|(int32_t)((int32_t)-2147483648LL))));
	}

IL_0085:
	{
		return;
	}
}
extern "C"  void XsdDuration__ctor_m1650863751_AdjustorThunk (RuntimeObject * __this, String_t* ___s0, int32_t ___durationType1, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	XsdDuration__ctor_m1650863751(_thisAdjusted, ___s0, ___durationType1, method);
}
// System.Boolean System.Xml.Schema.XsdDuration::get_IsNegative()
extern "C" IL2CPP_METHOD_ATTR bool XsdDuration_get_IsNegative_m2123881895 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = __this->get_nanoseconds_6();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2147483648LL)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
extern "C"  bool XsdDuration_get_IsNegative_m2123881895_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_IsNegative_m2123881895(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Years()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Years_m4035894877 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_years_0();
		return L_0;
	}
}
extern "C"  int32_t XsdDuration_get_Years_m4035894877_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Years_m4035894877(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Months()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Months_m3021984581 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_months_1();
		return L_0;
	}
}
extern "C"  int32_t XsdDuration_get_Months_m3021984581_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Months_m3021984581(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Days()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Days_m1425625717 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_days_2();
		return L_0;
	}
}
extern "C"  int32_t XsdDuration_get_Days_m1425625717_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Days_m1425625717(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Hours()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Hours_m3544702211 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_hours_3();
		return L_0;
	}
}
extern "C"  int32_t XsdDuration_get_Hours_m3544702211_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Hours_m3544702211(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Minutes()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Minutes_m2155056537 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_minutes_4();
		return L_0;
	}
}
extern "C"  int32_t XsdDuration_get_Minutes_m2155056537_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Minutes_m2155056537(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Seconds()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Seconds_m1180281066 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_seconds_5();
		return L_0;
	}
}
extern "C"  int32_t XsdDuration_get_Seconds_m1180281066_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Seconds_m1180281066(_thisAdjusted, method);
}
// System.Int32 System.Xml.Schema.XsdDuration::get_Nanoseconds()
extern "C" IL2CPP_METHOD_ATTR int32_t XsdDuration_get_Nanoseconds_m4094124966 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = __this->get_nanoseconds_6();
		return ((int32_t)((int32_t)L_0&(int32_t)((int32_t)2147483647LL)));
	}
}
extern "C"  int32_t XsdDuration_get_Nanoseconds_m4094124966_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_get_Nanoseconds_m4094124966(_thisAdjusted, method);
}
// System.TimeSpan System.Xml.Schema.XsdDuration::ToTimeSpan()
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  XsdDuration_ToTimeSpan_m2400096495 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		TimeSpan_t881159249  L_0 = XsdDuration_ToTimeSpan_m3421138678((XsdDuration_t3210533101 *)__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  TimeSpan_t881159249  XsdDuration_ToTimeSpan_m2400096495_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_ToTimeSpan_m2400096495(_thisAdjusted, method);
}
// System.TimeSpan System.Xml.Schema.XsdDuration::ToTimeSpan(System.Xml.Schema.XsdDuration/DurationType)
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  XsdDuration_ToTimeSpan_m3421138678 (XsdDuration_t3210533101 * __this, int32_t ___durationType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration_ToTimeSpan_m3421138678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t881159249  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t * V_1 = NULL;
	{
		int32_t L_0 = ___durationType0;
		Exception_t * L_1 = XsdDuration_TryToTimeSpan_m349624696((XsdDuration_t3210533101 *)__this, L_0, (TimeSpan_t881159249 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		Exception_t * L_2 = V_1;
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		Exception_t * L_3 = V_1;
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, XsdDuration_ToTimeSpan_m3421138678_RuntimeMethod_var);
	}

IL_000f:
	{
		TimeSpan_t881159249  L_4 = V_0;
		return L_4;
	}
}
extern "C"  TimeSpan_t881159249  XsdDuration_ToTimeSpan_m3421138678_AdjustorThunk (RuntimeObject * __this, int32_t ___durationType0, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_ToTimeSpan_m3421138678(_thisAdjusted, ___durationType0, method);
}
// System.Exception System.Xml.Schema.XsdDuration::TryToTimeSpan(System.Xml.Schema.XsdDuration/DurationType,System.TimeSpan&)
extern "C" IL2CPP_METHOD_ATTR Exception_t * XsdDuration_TryToTimeSpan_m349624696 (XsdDuration_t3210533101 * __this, int32_t ___durationType0, TimeSpan_t881159249 * ___result1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration_TryToTimeSpan_m349624696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	uint64_t V_1 = 0;
	Exception_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t *)NULL;
		V_1 = (((int64_t)((int64_t)0)));
	}

IL_0005:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___durationType0;
			if ((((int32_t)L_0) == ((int32_t)2)))
			{
				goto IL_0038;
			}
		}

IL_0009:
		{
			uint64_t L_1 = V_1;
			int32_t L_2 = __this->get_years_0();
			if ((uint64_t)(L_2) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			int32_t L_3 = __this->get_months_1();
			if ((uint64_t)(L_3) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)(((uint64_t)((uint64_t)L_2))) > kIl2CppUInt64Max - (uint64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_3)))/(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if (il2cpp_codegen_check_mul_oveflow_u64(((int64_t)il2cpp_codegen_add((int64_t)(((uint64_t)((uint64_t)L_2))), (int64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_3)))/(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))))), (((int64_t)((int64_t)((int32_t)365))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_1 > kIl2CppUInt64Max - (uint64_t)((int64_t)il2cpp_codegen_multiply((int64_t)((int64_t)il2cpp_codegen_add((int64_t)(((uint64_t)((uint64_t)L_2))), (int64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_3)))/(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))))), (int64_t)(((int64_t)((int64_t)((int32_t)365)))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_1, (int64_t)((int64_t)il2cpp_codegen_multiply((int64_t)((int64_t)il2cpp_codegen_add((int64_t)(((uint64_t)((uint64_t)L_2))), (int64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_3)))/(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))))), (int64_t)(((int64_t)((int64_t)((int32_t)365))))))));
			uint64_t L_4 = V_1;
			int32_t L_5 = __this->get_months_1();
			if ((uint64_t)(L_5) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if (il2cpp_codegen_check_mul_oveflow_u64(((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_5)))%(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))), (((int64_t)((int64_t)((int32_t)30))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_4 > kIl2CppUInt64Max - (uint64_t)((int64_t)il2cpp_codegen_multiply((int64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_5)))%(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))), (int64_t)(((int64_t)((int64_t)((int32_t)30)))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_4, (int64_t)((int64_t)il2cpp_codegen_multiply((int64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_5)))%(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)12)))))), (int64_t)(((int64_t)((int64_t)((int32_t)30))))))));
		}

IL_0038:
		{
			int32_t L_6 = ___durationType0;
			if ((((int32_t)L_6) == ((int32_t)1)))
			{
				goto IL_008f;
			}
		}

IL_003c:
		{
			uint64_t L_7 = V_1;
			int32_t L_8 = __this->get_days_2();
			if ((uint64_t)(L_8) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_7 > kIl2CppUInt64Max - (uint64_t)(((uint64_t)((uint64_t)L_8))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_7, (int64_t)(((uint64_t)((uint64_t)L_8)))));
			uint64_t L_9 = V_1;
			if (il2cpp_codegen_check_mul_oveflow_u64(L_9, (((int64_t)((int64_t)((int32_t)24))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_multiply((int64_t)L_9, (int64_t)(((int64_t)((int64_t)((int32_t)24))))));
			uint64_t L_10 = V_1;
			int32_t L_11 = __this->get_hours_3();
			if ((uint64_t)(L_11) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_10 > kIl2CppUInt64Max - (uint64_t)(((uint64_t)((uint64_t)L_11))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_10, (int64_t)(((uint64_t)((uint64_t)L_11)))));
			uint64_t L_12 = V_1;
			if (il2cpp_codegen_check_mul_oveflow_u64(L_12, (((int64_t)((int64_t)((int32_t)60))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_multiply((int64_t)L_12, (int64_t)(((int64_t)((int64_t)((int32_t)60))))));
			uint64_t L_13 = V_1;
			int32_t L_14 = __this->get_minutes_4();
			if ((uint64_t)(L_14) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_13 > kIl2CppUInt64Max - (uint64_t)(((uint64_t)((uint64_t)L_14))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_13, (int64_t)(((uint64_t)((uint64_t)L_14)))));
			uint64_t L_15 = V_1;
			if (il2cpp_codegen_check_mul_oveflow_u64(L_15, (((int64_t)((int64_t)((int32_t)60))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_multiply((int64_t)L_15, (int64_t)(((int64_t)((int64_t)((int32_t)60))))));
			uint64_t L_16 = V_1;
			int32_t L_17 = __this->get_seconds_5();
			if ((uint64_t)(L_17) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_16 > kIl2CppUInt64Max - (uint64_t)(((uint64_t)((uint64_t)L_17))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_16, (int64_t)(((uint64_t)((uint64_t)L_17)))));
			uint64_t L_18 = V_1;
			if (il2cpp_codegen_check_mul_oveflow_u64(L_18, (((int64_t)((int64_t)((int32_t)10000000))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_multiply((int64_t)L_18, (int64_t)(((int64_t)((int64_t)((int32_t)10000000))))));
			uint64_t L_19 = V_1;
			int32_t L_20 = XsdDuration_get_Nanoseconds_m4094124966((XsdDuration_t3210533101 *)__this, /*hidden argument*/NULL);
			if ((uint64_t)(L_20) > std::numeric_limits<uint64_t>::max()) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if ((uint64_t)L_19 > kIl2CppUInt64Max - (uint64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_20)))/(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)100)))))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_add((int64_t)L_19, (int64_t)((int64_t)((uint64_t)(int64_t)(((uint64_t)((uint64_t)L_20)))/(uint64_t)(int64_t)(((int64_t)((int64_t)((int32_t)100))))))));
			goto IL_009b;
		}

IL_008f:
		{
			uint64_t L_21 = V_1;
			if (il2cpp_codegen_check_mul_oveflow_u64(L_21, ((int64_t)864000000000LL)))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			V_1 = ((int64_t)il2cpp_codegen_multiply((int64_t)L_21, (int64_t)((int64_t)864000000000LL)));
		}

IL_009b:
		{
			bool L_22 = XsdDuration_get_IsNegative_m2123881895((XsdDuration_t3210533101 *)__this, /*hidden argument*/NULL);
			if (!L_22)
			{
				goto IL_00d7;
			}
		}

IL_00a3:
		{
			uint64_t L_23 = V_1;
			if ((!(((uint64_t)L_23) == ((uint64_t)((int64_t)std::numeric_limits<int64_t>::min())))))
			{
				goto IL_00c5;
			}
		}

IL_00af:
		{
			TimeSpan_t881159249 * L_24 = ___result1;
			TimeSpan_t881159249  L_25;
			memset(&L_25, 0, sizeof(L_25));
			TimeSpan__ctor_m1896986612((&L_25), ((int64_t)std::numeric_limits<int64_t>::min()), /*hidden argument*/NULL);
			*(TimeSpan_t881159249 *)L_24 = L_25;
			goto IL_00e4;
		}

IL_00c5:
		{
			TimeSpan_t881159249 * L_26 = ___result1;
			uint64_t L_27 = V_1;
			if ((uint64_t)(L_27) > 9223372036854775807LL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			if (il2cpp_codegen_check_sub_overflow((int64_t)(((int64_t)((int64_t)0))), (int64_t)(((int64_t)((int64_t)L_27)))))
				IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			TimeSpan_t881159249  L_28;
			memset(&L_28, 0, sizeof(L_28));
			TimeSpan__ctor_m1896986612((&L_28), ((int64_t)il2cpp_codegen_subtract((int64_t)(((int64_t)((int64_t)0))), (int64_t)(((int64_t)((int64_t)L_27))))), /*hidden argument*/NULL);
			*(TimeSpan_t881159249 *)L_26 = L_28;
			goto IL_00e4;
		}

IL_00d7:
		{
			TimeSpan_t881159249 * L_29 = ___result1;
			uint64_t L_30 = V_1;
			if ((uint64_t)(L_30) > 9223372036854775807LL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NULL, XsdDuration_TryToTimeSpan_m349624696_RuntimeMethod_var);
			TimeSpan_t881159249  L_31;
			memset(&L_31, 0, sizeof(L_31));
			TimeSpan__ctor_m1896986612((&L_31), (((int64_t)((int64_t)L_30))), /*hidden argument*/NULL);
			*(TimeSpan_t881159249 *)L_29 = L_31;
		}

IL_00e4:
		{
			V_2 = (Exception_t *)NULL;
			goto IL_011f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t2020128637_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_00e8;
		throw e;
	}

CATCH_00e8:
	{ // begin catch(System.OverflowException)
		TimeSpan_t881159249 * L_32 = ___result1;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_33 = ((TimeSpan_t881159249_StaticFields*)il2cpp_codegen_static_fields_for(TimeSpan_t881159249_il2cpp_TypeInfo_var))->get_MinValue_21();
		*(TimeSpan_t881159249 *)L_32 = L_33;
		ObjectU5BU5D_t2843939325* L_34 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_35 = L_34;
		int32_t L_36 = ___durationType0;
		int32_t L_37 = L_36;
		RuntimeObject * L_38 = Box(DurationType_t3437374716_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_38);
		ObjectU5BU5D_t2843939325* L_39 = L_35;
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, _stringLiteral1690701919);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral1690701919);
		String_t* L_40 = Res_GetString_m2746634261(NULL /*static, unused*/, _stringLiteral1126917588, L_39, /*hidden argument*/NULL);
		OverflowException_t2020128637 * L_41 = (OverflowException_t2020128637 *)il2cpp_codegen_object_new(OverflowException_t2020128637_il2cpp_TypeInfo_var);
		OverflowException__ctor_m694321376(L_41, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		goto IL_011d;
	} // end catch (depth: 1)

IL_011d:
	{
		Exception_t * L_42 = V_0;
		return L_42;
	}

IL_011f:
	{
		Exception_t * L_43 = V_2;
		return L_43;
	}
}
extern "C"  Exception_t * XsdDuration_TryToTimeSpan_m349624696_AdjustorThunk (RuntimeObject * __this, int32_t ___durationType0, TimeSpan_t881159249 * ___result1, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_TryToTimeSpan_m349624696(_thisAdjusted, ___durationType0, ___result1, method);
}
// System.String System.Xml.Schema.XsdDuration::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* XsdDuration_ToString_m3669437819 (XsdDuration_t3210533101 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = XsdDuration_ToString_m2282140164((XsdDuration_t3210533101 *)__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* XsdDuration_ToString_m3669437819_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_ToString_m3669437819(_thisAdjusted, method);
}
// System.String System.Xml.Schema.XsdDuration::ToString(System.Xml.Schema.XsdDuration/DurationType)
extern "C" IL2CPP_METHOD_ATTR String_t* XsdDuration_ToString_m2282140164 (XsdDuration_t3210533101 * __this, int32_t ___durationType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration_ToString_m2282140164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2367297767(L_0, ((int32_t)20), /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = XsdDuration_get_IsNegative_m2123881895((XsdDuration_t3210533101 *)__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t * L_2 = V_0;
		NullCheck(L_2);
		StringBuilder_Append_m2383614642(L_2, ((int32_t)45), /*hidden argument*/NULL);
	}

IL_0019:
	{
		StringBuilder_t * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_Append_m2383614642(L_3, ((int32_t)80), /*hidden argument*/NULL);
		int32_t L_4 = ___durationType0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_5 = __this->get_years_0();
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		StringBuilder_t * L_6 = V_0;
		int32_t L_7 = __this->get_years_0();
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1981561327_il2cpp_TypeInfo_var);
		String_t* L_8 = XmlConvert_ToString_m4082948798(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m1965104174(L_6, L_8, /*hidden argument*/NULL);
		StringBuilder_t * L_9 = V_0;
		NullCheck(L_9);
		StringBuilder_Append_m2383614642(L_9, ((int32_t)89), /*hidden argument*/NULL);
	}

IL_0049:
	{
		int32_t L_10 = __this->get_months_1();
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		StringBuilder_t * L_11 = V_0;
		int32_t L_12 = __this->get_months_1();
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1981561327_il2cpp_TypeInfo_var);
		String_t* L_13 = XmlConvert_ToString_m4082948798(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Append_m1965104174(L_11, L_13, /*hidden argument*/NULL);
		StringBuilder_t * L_14 = V_0;
		NullCheck(L_14);
		StringBuilder_Append_m2383614642(L_14, ((int32_t)77), /*hidden argument*/NULL);
	}

IL_006c:
	{
		int32_t L_15 = ___durationType0;
		if ((((int32_t)L_15) == ((int32_t)1)))
		{
			goto IL_01c1;
		}
	}
	{
		int32_t L_16 = __this->get_days_2();
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		StringBuilder_t * L_17 = V_0;
		int32_t L_18 = __this->get_days_2();
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1981561327_il2cpp_TypeInfo_var);
		String_t* L_19 = XmlConvert_ToString_m4082948798(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		StringBuilder_Append_m1965104174(L_17, L_19, /*hidden argument*/NULL);
		StringBuilder_t * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m2383614642(L_20, ((int32_t)68), /*hidden argument*/NULL);
	}

IL_0096:
	{
		int32_t L_21 = __this->get_hours_3();
		if (L_21)
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_22 = __this->get_minutes_4();
		if (L_22)
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_23 = __this->get_seconds_5();
		if (L_23)
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_24 = XsdDuration_get_Nanoseconds_m4094124966((XsdDuration_t3210533101 *)__this, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_01a1;
		}
	}

IL_00b9:
	{
		StringBuilder_t * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m2383614642(L_25, ((int32_t)84), /*hidden argument*/NULL);
		int32_t L_26 = __this->get_hours_3();
		if (!L_26)
		{
			goto IL_00e5;
		}
	}
	{
		StringBuilder_t * L_27 = V_0;
		int32_t L_28 = __this->get_hours_3();
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1981561327_il2cpp_TypeInfo_var);
		String_t* L_29 = XmlConvert_ToString_m4082948798(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		StringBuilder_Append_m1965104174(L_27, L_29, /*hidden argument*/NULL);
		StringBuilder_t * L_30 = V_0;
		NullCheck(L_30);
		StringBuilder_Append_m2383614642(L_30, ((int32_t)72), /*hidden argument*/NULL);
	}

IL_00e5:
	{
		int32_t L_31 = __this->get_minutes_4();
		if (!L_31)
		{
			goto IL_0108;
		}
	}
	{
		StringBuilder_t * L_32 = V_0;
		int32_t L_33 = __this->get_minutes_4();
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1981561327_il2cpp_TypeInfo_var);
		String_t* L_34 = XmlConvert_ToString_m4082948798(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		StringBuilder_Append_m1965104174(L_32, L_34, /*hidden argument*/NULL);
		StringBuilder_t * L_35 = V_0;
		NullCheck(L_35);
		StringBuilder_Append_m2383614642(L_35, ((int32_t)77), /*hidden argument*/NULL);
	}

IL_0108:
	{
		int32_t L_36 = XsdDuration_get_Nanoseconds_m4094124966((XsdDuration_t3210533101 *)__this, /*hidden argument*/NULL);
		V_1 = L_36;
		int32_t L_37 = __this->get_seconds_5();
		if (L_37)
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_38 = V_1;
		if (!L_38)
		{
			goto IL_01a1;
		}
	}

IL_011d:
	{
		StringBuilder_t * L_39 = V_0;
		int32_t L_40 = __this->get_seconds_5();
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1981561327_il2cpp_TypeInfo_var);
		String_t* L_41 = XmlConvert_ToString_m4082948798(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		StringBuilder_Append_m1965104174(L_39, L_41, /*hidden argument*/NULL);
		int32_t L_42 = V_1;
		if (!L_42)
		{
			goto IL_0198;
		}
	}
	{
		StringBuilder_t * L_43 = V_0;
		NullCheck(L_43);
		StringBuilder_Append_m2383614642(L_43, ((int32_t)46), /*hidden argument*/NULL);
		StringBuilder_t * L_44 = V_0;
		NullCheck(L_44);
		int32_t L_45 = StringBuilder_get_Length_m3238060835(L_44, /*hidden argument*/NULL);
		V_4 = L_45;
		StringBuilder_t * L_46 = V_0;
		StringBuilder_t * L_47 = L_46;
		NullCheck(L_47);
		int32_t L_48 = StringBuilder_get_Length_m3238060835(L_47, /*hidden argument*/NULL);
		NullCheck(L_47);
		StringBuilder_set_Length_m1410065908(L_47, ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)((int32_t)9))), /*hidden argument*/NULL);
		StringBuilder_t * L_49 = V_0;
		NullCheck(L_49);
		int32_t L_50 = StringBuilder_get_Length_m3238060835(L_49, /*hidden argument*/NULL);
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_50, (int32_t)1));
		int32_t L_51 = V_3;
		V_5 = L_51;
		goto IL_0189;
	}

IL_0160:
	{
		int32_t L_52 = V_1;
		V_2 = ((int32_t)((int32_t)L_52%(int32_t)((int32_t)10)));
		StringBuilder_t * L_53 = V_0;
		int32_t L_54 = V_5;
		int32_t L_55 = V_2;
		NullCheck(L_53);
		StringBuilder_set_Chars_m3548656617(L_53, L_54, (((int32_t)((uint16_t)((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)((int32_t)48)))))), /*hidden argument*/NULL);
		int32_t L_56 = V_3;
		int32_t L_57 = V_5;
		if ((!(((uint32_t)L_56) == ((uint32_t)L_57))))
		{
			goto IL_017e;
		}
	}
	{
		int32_t L_58 = V_2;
		if (L_58)
		{
			goto IL_017e;
		}
	}
	{
		int32_t L_59 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_59, (int32_t)1));
	}

IL_017e:
	{
		int32_t L_60 = V_1;
		V_1 = ((int32_t)((int32_t)L_60/(int32_t)((int32_t)10)));
		int32_t L_61 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_61, (int32_t)1));
	}

IL_0189:
	{
		int32_t L_62 = V_5;
		int32_t L_63 = V_4;
		if ((((int32_t)L_62) >= ((int32_t)L_63)))
		{
			goto IL_0160;
		}
	}
	{
		StringBuilder_t * L_64 = V_0;
		int32_t L_65 = V_3;
		NullCheck(L_64);
		StringBuilder_set_Length_m1410065908(L_64, ((int32_t)il2cpp_codegen_add((int32_t)L_65, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_0198:
	{
		StringBuilder_t * L_66 = V_0;
		NullCheck(L_66);
		StringBuilder_Append_m2383614642(L_66, ((int32_t)83), /*hidden argument*/NULL);
	}

IL_01a1:
	{
		StringBuilder_t * L_67 = V_0;
		StringBuilder_t * L_68 = V_0;
		NullCheck(L_68);
		int32_t L_69 = StringBuilder_get_Length_m3238060835(L_68, /*hidden argument*/NULL);
		NullCheck(L_67);
		Il2CppChar L_70 = StringBuilder_get_Chars_m1819843468(L_67, ((int32_t)il2cpp_codegen_subtract((int32_t)L_69, (int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_70) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_01df;
		}
	}
	{
		StringBuilder_t * L_71 = V_0;
		NullCheck(L_71);
		StringBuilder_Append_m1965104174(L_71, _stringLiteral2691562295, /*hidden argument*/NULL);
		goto IL_01df;
	}

IL_01c1:
	{
		StringBuilder_t * L_72 = V_0;
		StringBuilder_t * L_73 = V_0;
		NullCheck(L_73);
		int32_t L_74 = StringBuilder_get_Length_m3238060835(L_73, /*hidden argument*/NULL);
		NullCheck(L_72);
		Il2CppChar L_75 = StringBuilder_get_Chars_m1819843468(L_72, ((int32_t)il2cpp_codegen_subtract((int32_t)L_74, (int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_75) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_01df;
		}
	}
	{
		StringBuilder_t * L_76 = V_0;
		NullCheck(L_76);
		StringBuilder_Append_m1965104174(L_76, _stringLiteral3457005456, /*hidden argument*/NULL);
	}

IL_01df:
	{
		StringBuilder_t * L_77 = V_0;
		NullCheck(L_77);
		String_t* L_78 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_77);
		return L_78;
	}
}
extern "C"  String_t* XsdDuration_ToString_m2282140164_AdjustorThunk (RuntimeObject * __this, int32_t ___durationType0, const RuntimeMethod* method)
{
	XsdDuration_t3210533101 * _thisAdjusted = reinterpret_cast<XsdDuration_t3210533101 *>(__this + 1);
	return XsdDuration_ToString_m2282140164(_thisAdjusted, ___durationType0, method);
}
// System.Exception System.Xml.Schema.XsdDuration::TryParse(System.String,System.Xml.Schema.XsdDuration/DurationType,System.Xml.Schema.XsdDuration&)
extern "C" IL2CPP_METHOD_ATTR Exception_t * XsdDuration_TryParse_m3581297401 (RuntimeObject * __this /* static, unused */, String_t* ___s0, int32_t ___durationType1, XsdDuration_t3210533101 * ___result2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration_TryParse_m3581297401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_4 = 0;
		XsdDuration_t3210533101 * L_0 = ___result2;
		il2cpp_codegen_initobj(L_0, sizeof(XsdDuration_t3210533101 ));
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		String_t* L_2 = String_Trim_m923598732(L_1, /*hidden argument*/NULL);
		___s0 = L_2;
		String_t* L_3 = ___s0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		V_2 = 0;
		V_3 = 0;
		int32_t L_5 = V_2;
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_7 = ___s0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		XsdDuration_t3210533101 * L_11 = ___result2;
		L_11->set_nanoseconds_6(((int32_t)-2147483648LL));
		goto IL_0047;
	}

IL_0040:
	{
		XsdDuration_t3210533101 * L_12 = ___result2;
		L_12->set_nanoseconds_6(0);
	}

IL_0047:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_15 = ___s0;
		int32_t L_16 = V_2;
		int32_t L_17 = L_16;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
		NullCheck(L_15);
		Il2CppChar L_18 = String_get_Chars_m2986988803(L_15, L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_19 = ___s0;
		String_t* L_20 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_19, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_21 = V_2;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) >= ((int32_t)L_22)))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_23 = ___s0;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m2986988803(L_23, L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)89)))))
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_26 = V_3;
		if (!L_26)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27|(int32_t)1));
		XsdDuration_t3210533101 * L_28 = ___result2;
		int32_t L_29 = V_1;
		L_28->set_years_0(L_29);
		int32_t L_30 = V_2;
		int32_t L_31 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		V_2 = L_31;
		int32_t L_32 = V_0;
		if ((((int32_t)L_31) == ((int32_t)L_32)))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_33 = ___s0;
		String_t* L_34 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_33, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_35 = V_2;
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_02b5;
		}
	}

IL_00bb:
	{
		String_t* L_37 = ___s0;
		int32_t L_38 = V_2;
		NullCheck(L_37);
		Il2CppChar L_39 = String_get_Chars_m2986988803(L_37, L_38, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_00fd;
		}
	}
	{
		int32_t L_40 = V_3;
		if (!L_40)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_41 = V_4;
		V_4 = ((int32_t)((int32_t)L_41|(int32_t)2));
		XsdDuration_t3210533101 * L_42 = ___result2;
		int32_t L_43 = V_1;
		L_42->set_months_1(L_43);
		int32_t L_44 = V_2;
		int32_t L_45 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
		V_2 = L_45;
		int32_t L_46 = V_0;
		if ((((int32_t)L_45) == ((int32_t)L_46)))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_47 = ___s0;
		String_t* L_48 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_47, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_49 = V_2;
		int32_t L_50 = V_0;
		if ((((int32_t)L_49) >= ((int32_t)L_50)))
		{
			goto IL_02b5;
		}
	}

IL_00fd:
	{
		String_t* L_51 = ___s0;
		int32_t L_52 = V_2;
		NullCheck(L_51);
		Il2CppChar L_53 = String_get_Chars_m2986988803(L_51, L_52, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_53) == ((uint32_t)((int32_t)68)))))
		{
			goto IL_013f;
		}
	}
	{
		int32_t L_54 = V_3;
		if (!L_54)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_55 = V_4;
		V_4 = ((int32_t)((int32_t)L_55|(int32_t)4));
		XsdDuration_t3210533101 * L_56 = ___result2;
		int32_t L_57 = V_1;
		L_56->set_days_2(L_57);
		int32_t L_58 = V_2;
		int32_t L_59 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
		V_2 = L_59;
		int32_t L_60 = V_0;
		if ((((int32_t)L_59) == ((int32_t)L_60)))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_61 = ___s0;
		String_t* L_62 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_61, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_63 = V_2;
		int32_t L_64 = V_0;
		if ((((int32_t)L_63) >= ((int32_t)L_64)))
		{
			goto IL_02b5;
		}
	}

IL_013f:
	{
		String_t* L_65 = ___s0;
		int32_t L_66 = V_2;
		NullCheck(L_65);
		Il2CppChar L_67 = String_get_Chars_m2986988803(L_65, L_66, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)84)))))
		{
			goto IL_0291;
		}
	}
	{
		int32_t L_68 = V_3;
		if (L_68)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_69 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_69, (int32_t)1));
		String_t* L_70 = ___s0;
		String_t* L_71 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_70, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_71)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_72 = V_2;
		int32_t L_73 = V_0;
		if ((((int32_t)L_72) >= ((int32_t)L_73)))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_74 = ___s0;
		int32_t L_75 = V_2;
		NullCheck(L_74);
		Il2CppChar L_76 = String_get_Chars_m2986988803(L_74, L_75, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_76) == ((uint32_t)((int32_t)72)))))
		{
			goto IL_01b2;
		}
	}
	{
		int32_t L_77 = V_3;
		if (!L_77)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_78 = V_4;
		V_4 = ((int32_t)((int32_t)L_78|(int32_t)8));
		XsdDuration_t3210533101 * L_79 = ___result2;
		int32_t L_80 = V_1;
		L_79->set_hours_3(L_80);
		int32_t L_81 = V_2;
		int32_t L_82 = ((int32_t)il2cpp_codegen_add((int32_t)L_81, (int32_t)1));
		V_2 = L_82;
		int32_t L_83 = V_0;
		if ((((int32_t)L_82) == ((int32_t)L_83)))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_84 = ___s0;
		String_t* L_85 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_84, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_86 = V_2;
		int32_t L_87 = V_0;
		if ((((int32_t)L_86) >= ((int32_t)L_87)))
		{
			goto IL_02b5;
		}
	}

IL_01b2:
	{
		String_t* L_88 = ___s0;
		int32_t L_89 = V_2;
		NullCheck(L_88);
		Il2CppChar L_90 = String_get_Chars_m2986988803(L_88, L_89, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_90) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_01f5;
		}
	}
	{
		int32_t L_91 = V_3;
		if (!L_91)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_92 = V_4;
		V_4 = ((int32_t)((int32_t)L_92|(int32_t)((int32_t)16)));
		XsdDuration_t3210533101 * L_93 = ___result2;
		int32_t L_94 = V_1;
		L_93->set_minutes_4(L_94);
		int32_t L_95 = V_2;
		int32_t L_96 = ((int32_t)il2cpp_codegen_add((int32_t)L_95, (int32_t)1));
		V_2 = L_96;
		int32_t L_97 = V_0;
		if ((((int32_t)L_96) == ((int32_t)L_97)))
		{
			goto IL_0298;
		}
	}
	{
		String_t* L_98 = ___s0;
		String_t* L_99 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_98, (int32_t*)(&V_2), (bool)0, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_99)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_100 = V_2;
		int32_t L_101 = V_0;
		if ((((int32_t)L_100) >= ((int32_t)L_101)))
		{
			goto IL_02b5;
		}
	}

IL_01f5:
	{
		String_t* L_102 = ___s0;
		int32_t L_103 = V_2;
		NullCheck(L_102);
		Il2CppChar L_104 = String_get_Chars_m2986988803(L_102, L_103, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_104) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_026d;
		}
	}
	{
		int32_t L_105 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_105, (int32_t)1));
		int32_t L_106 = V_4;
		V_4 = ((int32_t)((int32_t)L_106|(int32_t)((int32_t)32)));
		XsdDuration_t3210533101 * L_107 = ___result2;
		int32_t L_108 = V_1;
		L_107->set_seconds_5(L_108);
		String_t* L_109 = ___s0;
		String_t* L_110 = XsdDuration_TryParseDigits_m3033505084(NULL /*static, unused*/, L_109, (int32_t*)(&V_2), (bool)1, (int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/NULL);
		if (L_110)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_111 = V_3;
		if (L_111)
		{
			goto IL_0234;
		}
	}
	{
		V_1 = 0;
		goto IL_0234;
	}

IL_022b:
	{
		int32_t L_112 = V_1;
		V_1 = ((int32_t)((int32_t)L_112/(int32_t)((int32_t)10)));
		int32_t L_113 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_113, (int32_t)1));
	}

IL_0234:
	{
		int32_t L_114 = V_3;
		if ((((int32_t)L_114) > ((int32_t)((int32_t)9))))
		{
			goto IL_022b;
		}
	}
	{
		goto IL_0244;
	}

IL_023b:
	{
		int32_t L_115 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_115, (int32_t)((int32_t)10)));
		int32_t L_116 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_116, (int32_t)1));
	}

IL_0244:
	{
		int32_t L_117 = V_3;
		if ((((int32_t)L_117) < ((int32_t)((int32_t)9))))
		{
			goto IL_023b;
		}
	}
	{
		XsdDuration_t3210533101 * L_118 = ___result2;
		uint32_t* L_119 = L_118->get_address_of_nanoseconds_6();
		uint32_t* L_120 = L_119;
		int32_t L_121 = *((uint32_t*)L_120);
		int32_t L_122 = V_1;
		*((int32_t*)L_120) = (int32_t)((int32_t)((int32_t)L_121|(int32_t)L_122));
		int32_t L_123 = V_2;
		int32_t L_124 = V_0;
		if ((((int32_t)L_123) >= ((int32_t)L_124)))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_125 = ___s0;
		int32_t L_126 = V_2;
		NullCheck(L_125);
		Il2CppChar L_127 = String_get_Chars_m2986988803(L_125, L_126, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_127) == ((uint32_t)((int32_t)83)))))
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_128 = V_2;
		int32_t L_129 = ((int32_t)il2cpp_codegen_add((int32_t)L_128, (int32_t)1));
		V_2 = L_129;
		int32_t L_130 = V_0;
		if ((!(((uint32_t)L_129) == ((uint32_t)L_130))))
		{
			goto IL_0291;
		}
	}
	{
		goto IL_0298;
	}

IL_026d:
	{
		String_t* L_131 = ___s0;
		int32_t L_132 = V_2;
		NullCheck(L_131);
		Il2CppChar L_133 = String_get_Chars_m2986988803(L_131, L_132, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_133) == ((uint32_t)((int32_t)83)))))
		{
			goto IL_0291;
		}
	}
	{
		int32_t L_134 = V_3;
		if (!L_134)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_135 = V_4;
		V_4 = ((int32_t)((int32_t)L_135|(int32_t)((int32_t)32)));
		XsdDuration_t3210533101 * L_136 = ___result2;
		int32_t L_137 = V_1;
		L_136->set_seconds_5(L_137);
		int32_t L_138 = V_2;
		int32_t L_139 = ((int32_t)il2cpp_codegen_add((int32_t)L_138, (int32_t)1));
		V_2 = L_139;
		int32_t L_140 = V_0;
		if ((((int32_t)L_139) == ((int32_t)L_140)))
		{
			goto IL_0298;
		}
	}

IL_0291:
	{
		int32_t L_141 = V_3;
		if (L_141)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_142 = V_2;
		int32_t L_143 = V_0;
		if ((!(((uint32_t)L_142) == ((uint32_t)L_143))))
		{
			goto IL_02b5;
		}
	}

IL_0298:
	{
		int32_t L_144 = V_4;
		if (!L_144)
		{
			goto IL_02b5;
		}
	}
	{
		int32_t L_145 = ___durationType1;
		if ((!(((uint32_t)L_145) == ((uint32_t)2))))
		{
			goto IL_02a8;
		}
	}
	{
		int32_t L_146 = V_4;
		if (!((int32_t)((int32_t)L_146&(int32_t)3)))
		{
			goto IL_02b3;
		}
	}
	{
		goto IL_02b5;
	}

IL_02a8:
	{
		int32_t L_147 = ___durationType1;
		if ((!(((uint32_t)L_147) == ((uint32_t)1))))
		{
			goto IL_02b3;
		}
	}
	{
		int32_t L_148 = V_4;
		if (((int32_t)((int32_t)L_148&(int32_t)((int32_t)-4))))
		{
			goto IL_02b5;
		}
	}

IL_02b3:
	{
		return (Exception_t *)NULL;
	}

IL_02b5:
	{
		ObjectU5BU5D_t2843939325* L_149 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_150 = L_149;
		String_t* L_151 = ___s0;
		NullCheck(L_150);
		ArrayElementTypeCheck (L_150, L_151);
		(L_150)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_151);
		ObjectU5BU5D_t2843939325* L_152 = L_150;
		int32_t L_153 = ___durationType1;
		int32_t L_154 = L_153;
		RuntimeObject * L_155 = Box(DurationType_t3437374716_il2cpp_TypeInfo_var, &L_154);
		NullCheck(L_152);
		ArrayElementTypeCheck (L_152, L_155);
		(L_152)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_155);
		String_t* L_156 = Res_GetString_m2746634261(NULL /*static, unused*/, _stringLiteral760917697, L_152, /*hidden argument*/NULL);
		FormatException_t154580423 * L_157 = (FormatException_t154580423 *)il2cpp_codegen_object_new(FormatException_t154580423_il2cpp_TypeInfo_var);
		FormatException__ctor_m4049685996(L_157, L_156, /*hidden argument*/NULL);
		return L_157;
	}

IL_02d8:
	{
		ObjectU5BU5D_t2843939325* L_158 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_159 = L_158;
		String_t* L_160 = ___s0;
		NullCheck(L_159);
		ArrayElementTypeCheck (L_159, L_160);
		(L_159)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_160);
		ObjectU5BU5D_t2843939325* L_161 = L_159;
		int32_t L_162 = ___durationType1;
		int32_t L_163 = L_162;
		RuntimeObject * L_164 = Box(DurationType_t3437374716_il2cpp_TypeInfo_var, &L_163);
		NullCheck(L_161);
		ArrayElementTypeCheck (L_161, L_164);
		(L_161)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_164);
		String_t* L_165 = Res_GetString_m2746634261(NULL /*static, unused*/, _stringLiteral1126917588, L_161, /*hidden argument*/NULL);
		OverflowException_t2020128637 * L_166 = (OverflowException_t2020128637 *)il2cpp_codegen_object_new(OverflowException_t2020128637_il2cpp_TypeInfo_var);
		OverflowException__ctor_m694321376(L_166, L_165, /*hidden argument*/NULL);
		return L_166;
	}
}
// System.String System.Xml.Schema.XsdDuration::TryParseDigits(System.String,System.Int32&,System.Boolean,System.Int32&,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR String_t* XsdDuration_TryParseDigits_m3033505084 (RuntimeObject * __this /* static, unused */, String_t* ___s0, int32_t* ___offset1, bool ___eatDigits2, int32_t* ___result3, int32_t* ___numDigits4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration_TryParseDigits_m3033505084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t* L_0 = ___offset1;
		int32_t L_1 = *((int32_t*)L_0);
		V_0 = L_1;
		String_t* L_2 = ___s0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3847582255(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t* L_4 = ___result3;
		*((int32_t*)L_4) = (int32_t)0;
		int32_t* L_5 = ___numDigits4;
		*((int32_t*)L_5) = (int32_t)0;
		goto IL_0073;
	}

IL_0013:
	{
		String_t* L_6 = ___s0;
		int32_t* L_7 = ___offset1;
		int32_t L_8 = *((int32_t*)L_7);
		NullCheck(L_6);
		Il2CppChar L_9 = String_get_Chars_m2986988803(L_6, L_8, /*hidden argument*/NULL);
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)((int32_t)48)));
		int32_t* L_10 = ___result3;
		int32_t L_11 = *((int32_t*)L_10);
		int32_t L_12 = V_2;
		if ((((int32_t)L_11) <= ((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)2147483647LL), (int32_t)L_12))/(int32_t)((int32_t)10))))))
		{
			goto IL_0064;
		}
	}
	{
		bool L_13 = ___eatDigits2;
		if (L_13)
		{
			goto IL_0036;
		}
	}
	{
		return _stringLiteral1126917588;
	}

IL_0036:
	{
		int32_t* L_14 = ___numDigits4;
		int32_t* L_15 = ___offset1;
		int32_t L_16 = *((int32_t*)L_15);
		int32_t L_17 = V_0;
		*((int32_t*)L_14) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)L_17));
		goto IL_0045;
	}

IL_003f:
	{
		int32_t* L_18 = ___offset1;
		int32_t* L_19 = ___offset1;
		int32_t L_20 = *((int32_t*)L_19);
		*((int32_t*)L_18) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0045:
	{
		int32_t* L_21 = ___offset1;
		int32_t L_22 = *((int32_t*)L_21);
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_24 = ___s0;
		int32_t* L_25 = ___offset1;
		int32_t L_26 = *((int32_t*)L_25);
		NullCheck(L_24);
		Il2CppChar L_27 = String_get_Chars_m2986988803(L_24, L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_27) < ((int32_t)((int32_t)48))))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_28 = ___s0;
		int32_t* L_29 = ___offset1;
		int32_t L_30 = *((int32_t*)L_29);
		NullCheck(L_28);
		Il2CppChar L_31 = String_get_Chars_m2986988803(L_28, L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_31) <= ((int32_t)((int32_t)57))))
		{
			goto IL_003f;
		}
	}

IL_0062:
	{
		return (String_t*)NULL;
	}

IL_0064:
	{
		int32_t* L_32 = ___result3;
		int32_t* L_33 = ___result3;
		int32_t L_34 = *((int32_t*)L_33);
		int32_t L_35 = V_2;
		*((int32_t*)L_32) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_34, (int32_t)((int32_t)10))), (int32_t)L_35));
		int32_t* L_36 = ___offset1;
		int32_t* L_37 = ___offset1;
		int32_t L_38 = *((int32_t*)L_37);
		*((int32_t*)L_36) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_0073:
	{
		int32_t* L_39 = ___offset1;
		int32_t L_40 = *((int32_t*)L_39);
		int32_t L_41 = V_1;
		if ((((int32_t)L_40) >= ((int32_t)L_41)))
		{
			goto IL_0090;
		}
	}
	{
		String_t* L_42 = ___s0;
		int32_t* L_43 = ___offset1;
		int32_t L_44 = *((int32_t*)L_43);
		NullCheck(L_42);
		Il2CppChar L_45 = String_get_Chars_m2986988803(L_42, L_44, /*hidden argument*/NULL);
		if ((((int32_t)L_45) < ((int32_t)((int32_t)48))))
		{
			goto IL_0090;
		}
	}
	{
		String_t* L_46 = ___s0;
		int32_t* L_47 = ___offset1;
		int32_t L_48 = *((int32_t*)L_47);
		NullCheck(L_46);
		Il2CppChar L_49 = String_get_Chars_m2986988803(L_46, L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_49) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0013;
		}
	}

IL_0090:
	{
		int32_t* L_50 = ___numDigits4;
		int32_t* L_51 = ___offset1;
		int32_t L_52 = *((int32_t*)L_51);
		int32_t L_53 = V_0;
		*((int32_t*)L_50) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_52, (int32_t)L_53));
		return (String_t*)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlCharType
extern "C" void XmlCharType_t2277243275_marshal_pinvoke(const XmlCharType_t2277243275& unmarshaled, XmlCharType_t2277243275_marshaled_pinvoke& marshaled)
{
	if (unmarshaled.get_charProperties_2() != NULL)
	{
		il2cpp_array_size_t _unmarshaled_charProperties_Length = (unmarshaled.get_charProperties_2())->max_length;
		marshaled.___charProperties_2 = il2cpp_codegen_marshal_allocate_array<uint8_t>(_unmarshaled_charProperties_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaled_charProperties_Length); i++)
		{
			(marshaled.___charProperties_2)[i] = (unmarshaled.get_charProperties_2())->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		marshaled.___charProperties_2 = NULL;
	}
}
extern "C" void XmlCharType_t2277243275_marshal_pinvoke_back(const XmlCharType_t2277243275_marshaled_pinvoke& marshaled, XmlCharType_t2277243275& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_t2277243275_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	if (marshaled.___charProperties_2 != NULL)
	{
		if (unmarshaled.get_charProperties_2() == NULL)
		{
			unmarshaled.set_charProperties_2(reinterpret_cast<ByteU5BU5D_t4116647657*>(SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.get_charProperties_2())->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			(unmarshaled.get_charProperties_2())->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (marshaled.___charProperties_2)[i]);
		}
	}
}
// Conversion method for clean up from marshalling of: System.Xml.XmlCharType
extern "C" void XmlCharType_t2277243275_marshal_pinvoke_cleanup(XmlCharType_t2277243275_marshaled_pinvoke& marshaled)
{
	if (marshaled.___charProperties_2 != NULL)
	{
		il2cpp_codegen_marshal_free(marshaled.___charProperties_2);
		marshaled.___charProperties_2 = NULL;
	}
}
// Conversion methods for marshalling of: System.Xml.XmlCharType
extern "C" void XmlCharType_t2277243275_marshal_com(const XmlCharType_t2277243275& unmarshaled, XmlCharType_t2277243275_marshaled_com& marshaled)
{
	if (unmarshaled.get_charProperties_2() != NULL)
	{
		il2cpp_array_size_t _unmarshaled_charProperties_Length = (unmarshaled.get_charProperties_2())->max_length;
		marshaled.___charProperties_2 = il2cpp_codegen_marshal_allocate_array<uint8_t>(_unmarshaled_charProperties_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaled_charProperties_Length); i++)
		{
			(marshaled.___charProperties_2)[i] = (unmarshaled.get_charProperties_2())->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		marshaled.___charProperties_2 = NULL;
	}
}
extern "C" void XmlCharType_t2277243275_marshal_com_back(const XmlCharType_t2277243275_marshaled_com& marshaled, XmlCharType_t2277243275& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_t2277243275_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	if (marshaled.___charProperties_2 != NULL)
	{
		if (unmarshaled.get_charProperties_2() == NULL)
		{
			unmarshaled.set_charProperties_2(reinterpret_cast<ByteU5BU5D_t4116647657*>(SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.get_charProperties_2())->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			(unmarshaled.get_charProperties_2())->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (marshaled.___charProperties_2)[i]);
		}
	}
}
// Conversion method for clean up from marshalling of: System.Xml.XmlCharType
extern "C" void XmlCharType_t2277243275_marshal_com_cleanup(XmlCharType_t2277243275_marshaled_com& marshaled)
{
	if (marshaled.___charProperties_2 != NULL)
	{
		il2cpp_codegen_marshal_free(marshaled.___charProperties_2);
		marshaled.___charProperties_2 = NULL;
	}
}
// System.Object System.Xml.XmlCharType::get_StaticLock()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * XmlCharType_get_StaticLock_m1063179012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_get_StaticLock_m1063179012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = ((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_s_Lock_0();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		InterlockedCompareExchangeImpl<RuntimeObject *>((RuntimeObject **)(((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_address_of_s_Lock_0()), L_2, NULL);
	}

IL_001a:
	{
		RuntimeObject * L_3 = ((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_s_Lock_0();
		return L_3;
	}
}
// System.Void System.Xml.XmlCharType::InitInstance()
extern "C" IL2CPP_METHOD_ATTR void XmlCharType_InitInstance_m2424289755 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_InitInstance_m2424289755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject * L_0 = XmlCharType_get_StaticLock_m1063179012(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_m984175629(NULL /*static, unused*/, L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			ByteU5BU5D_t4116647657* L_2 = ((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
			il2cpp_codegen_memory_barrier();
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0019:
		{
			IL2CPP_LEAVE(0x97, FINALLY_008d);
		}

IL_001b:
		{
			ByteU5BU5D_t4116647657* L_3 = (ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)((int32_t)65536));
			il2cpp_codegen_memory_barrier();
			((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->set_s_CharProperties_1(L_3);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral4239484246, (uint8_t)1, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral608221119, (uint8_t)2, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral2698841325, (uint8_t)4, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral3282616573, (uint8_t)8, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral3232093604, (uint8_t)((int32_t)16), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral3282616573, (uint8_t)((int32_t)32), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral2032169780, (uint8_t)((int32_t)64), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m974705761(NULL /*static, unused*/, _stringLiteral1883235005, (uint8_t)((int32_t)128), /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x97, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0096;
			}
		}

IL_0090:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_m3585316909(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0097:
	{
		return;
	}
}
// System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void XmlCharType_SetProperties_m974705761 (RuntimeObject * __this /* static, unused */, String_t* ___ranges0, uint8_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_SetProperties_m974705761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0037;
	}

IL_0004:
	{
		String_t* L_0 = ___ranges0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m2986988803(L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = ___ranges0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m2986988803(L_3, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_002f;
	}

IL_0018:
	{
		ByteU5BU5D_t4116647657* L_6 = ((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		uint8_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)));
		int32_t L_9 = *((uint8_t*)L_8);
		uint8_t L_10 = ___value1;
		*((int8_t*)L_8) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9|(int32_t)L_10)))));
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
	}

IL_0037:
	{
		int32_t L_15 = V_0;
		String_t* L_16 = ___ranges0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m3847582255(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void XmlCharType__ctor_m26891158 (XmlCharType_t2277243275 * __this, ByteU5BU5D_t4116647657* ___charProperties0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___charProperties0;
		__this->set_charProperties_2(L_0);
		return;
	}
}
extern "C"  void XmlCharType__ctor_m26891158_AdjustorThunk (RuntimeObject * __this, ByteU5BU5D_t4116647657* ___charProperties0, const RuntimeMethod* method)
{
	XmlCharType_t2277243275 * _thisAdjusted = reinterpret_cast<XmlCharType_t2277243275 *>(__this + 1);
	XmlCharType__ctor_m26891158(_thisAdjusted, ___charProperties0, method);
}
// System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
extern "C" IL2CPP_METHOD_ATTR XmlCharType_t2277243275  XmlCharType_get_Instance_m52052032 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_get_Instance_m52052032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t4116647657* L_0 = ((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		XmlCharType_InitInstance_m2424289755(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000e:
	{
		ByteU5BU5D_t4116647657* L_1 = ((XmlCharType_t2277243275_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t2277243275_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		XmlCharType_t2277243275  L_2;
		memset(&L_2, 0, sizeof(L_2));
		XmlCharType__ctor_m26891158((&L_2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Xml.XmlConvert::ToString(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* XmlConvert_ToString_m4082948798 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	{
		NumberFormatInfo_t435877138 * L_0 = NumberFormatInfo_get_InvariantInfo_m349577018(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = Int32_ToString_m2507389746((int32_t*)(&___value0), (String_t*)NULL, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.Xml.XmlConvert::ToTimeSpan(System.String)
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  XmlConvert_ToTimeSpan_m349710467 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_ToTimeSpan_m349710467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XsdDuration_t3210533101  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___s0;
		XsdDuration_t3210533101  L_1;
		memset(&L_1, 0, sizeof(L_1));
		XsdDuration__ctor_m3570494855((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0009;
		throw e;
	}

CATCH_0009:
	{ // begin catch(System.Exception)
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_3 = L_2;
		String_t* L_4 = ___s0;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t2843939325* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1690701919);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral1690701919);
		String_t* L_6 = Res_GetString_m2746634261(NULL /*static, unused*/, _stringLiteral760917697, L_5, /*hidden argument*/NULL);
		FormatException_t154580423 * L_7 = (FormatException_t154580423 *)il2cpp_codegen_object_new(FormatException_t154580423_il2cpp_TypeInfo_var);
		FormatException__ctor_m4049685996(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, NULL, XmlConvert_ToTimeSpan_m349710467_RuntimeMethod_var);
	} // end catch (depth: 1)

IL_002c:
	{
		TimeSpan_t881159249  L_8 = XsdDuration_ToTimeSpan_m2400096495((XsdDuration_t3210533101 *)(&V_0), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void System.Xml.XmlConvert::.cctor()
extern "C" IL2CPP_METHOD_ATTR void XmlConvert__cctor_m3143968102 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert__cctor_m3143968102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlCharType_t2277243275  L_0 = XmlCharType_get_Instance_m52052032(NULL /*static, unused*/, /*hidden argument*/NULL);
		((XmlConvert_t1981561327_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t1981561327_il2cpp_TypeInfo_var))->set_xmlCharType_0(L_0);
		CharU5BU5D_t3528271667* L_1 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)3);
		CharU5BU5D_t3528271667* L_2 = L_1;
		RuntimeFieldHandle_t1871169219  L_3 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255363____5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_2, L_3, /*hidden argument*/NULL);
		((XmlConvert_t1981561327_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t1981561327_il2cpp_TypeInfo_var))->set_crt_1(L_2);
		((XmlConvert_t1981561327_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t1981561327_il2cpp_TypeInfo_var))->set_c_EncodedCharLength_2(7);
		CharU5BU5D_t3528271667* L_4 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)4);
		CharU5BU5D_t3528271667* L_5 = L_4;
		RuntimeFieldHandle_t1871169219  L_6 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255363____EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_5, L_6, /*hidden argument*/NULL);
		((XmlConvert_t1981561327_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t1981561327_il2cpp_TypeInfo_var))->set_WhitespaceChars_3(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
