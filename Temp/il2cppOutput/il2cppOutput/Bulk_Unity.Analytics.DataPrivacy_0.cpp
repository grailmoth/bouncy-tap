﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t885026589;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t1822382459;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t3348751306;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2285235057;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.String
struct String_t;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Uri
struct Uri_t100236324;
// System.Uri/UriInfo
struct UriInfo_t1092684687;
// System.UriParser
struct UriParser_t3890150400;
// System.Void
struct Void_t1185182177;
// UnityEngine.Analytics.DataPrivacy
struct DataPrivacy_t670264006;
// UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0
struct U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616;
// UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1
struct U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383;
// UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct OptOutStatus_t3541615854;
// UnityEngine.Analytics.DataPrivacy/RequestData
struct RequestData_t3101179086;
// UnityEngine.Analytics.DataPrivacyButton
struct DataPrivacyButton_t2259119369;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.WWW
struct WWW_t3688466362;

extern RuntimeClass* Action_1_t2019918284_il2cpp_TypeInfo_var;
extern RuntimeClass* DataPrivacy_t670264006_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* MissingMethodException_t1274661534_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* OptOutStatus_t3541615854_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimePlatform_t4159857903_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32_t2560061978_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityAction_t3245792599_il2cpp_TypeInfo_var;
extern RuntimeClass* Uri_t100236324_il2cpp_TypeInfo_var;
extern RuntimeClass* UserPostData_t478425489_il2cpp_TypeInfo_var;
extern RuntimeClass* WWW_t3688466362_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1138802964;
extern String_t* _stringLiteral1554920073;
extern String_t* _stringLiteral1644330790;
extern String_t* _stringLiteral1946891126;
extern String_t* _stringLiteral2049904153;
extern String_t* _stringLiteral2231664933;
extern String_t* _stringLiteral2263792357;
extern String_t* _stringLiteral2283129959;
extern String_t* _stringLiteral2290071638;
extern String_t* _stringLiteral2549449189;
extern String_t* _stringLiteral2651549123;
extern String_t* _stringLiteral2986767237;
extern String_t* _stringLiteral3216638377;
extern String_t* _stringLiteral3265403522;
extern String_t* _stringLiteral3481671258;
extern String_t* _stringLiteral3599900414;
extern String_t* _stringLiteral3607360360;
extern String_t* _stringLiteral3747912002;
extern String_t* _stringLiteral3855945919;
extern String_t* _stringLiteral4071441506;
extern String_t* _stringLiteral776475747;
extern String_t* _stringLiteral827492760;
extern String_t* _stringLiteral866926178;
extern String_t* _stringLiteral909076603;
extern const RuntimeMethod* Action_1_Invoke_m1933767679_RuntimeMethod_var;
extern const RuntimeMethod* Action_1_Invoke_m935934032_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m1347592571_RuntimeMethod_var;
extern const RuntimeMethod* DataPrivacyButton_OnFailure_m1914851203_RuntimeMethod_var;
extern const RuntimeMethod* DataPrivacyButton_OpenDataPrivacyUrl_m47974444_RuntimeMethod_var;
extern const RuntimeMethod* DataPrivacyButton_OpenUrl_m1642407137_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m3045345476_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m444307833_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisDataPrivacy_t670264006_m467235112_RuntimeMethod_var;
extern const RuntimeMethod* JsonUtility_FromJson_TisOptOutResponse_t2266495071_m4231913572_RuntimeMethod_var;
extern const RuntimeMethod* JsonUtility_FromJson_TisTokenData_t2077495713_m2426260624_RuntimeMethod_var;
extern const RuntimeMethod* U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_Reset_m2670016829_RuntimeMethod_var;
extern const RuntimeMethod* U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_Reset_m1433355255_RuntimeMethod_var;
extern const uint32_t DataPrivacyButton_OnFailure_m1914851203_MetadataUsageId;
extern const uint32_t DataPrivacyButton_OpenDataPrivacyUrl_m47974444_MetadataUsageId;
extern const uint32_t DataPrivacyButton__ctor_m2140043654_MetadataUsageId;
extern const uint32_t DataPrivacyUtils_OptOutStatusToJson_m3940455503_MetadataUsageId;
extern const uint32_t DataPrivacyUtils_ParseOptOutResponse_m841987952_MetadataUsageId;
extern const uint32_t DataPrivacyUtils_ParseTokenData_m1538885353_MetadataUsageId;
extern const uint32_t DataPrivacyUtils_SetOptOutStatus_m4281284745_MetadataUsageId;
extern const uint32_t DataPrivacyUtils_UserPostDataToJson_m966437446_MetadataUsageId;
extern const uint32_t DataPrivacy_FetchOptOutStatusCoroutine_m2945634055_MetadataUsageId;
extern const uint32_t DataPrivacy_FetchOptOutStatus_m2364404913_MetadataUsageId;
extern const uint32_t DataPrivacy_FetchPrivacyUrlCoroutine_m2502745193_MetadataUsageId;
extern const uint32_t DataPrivacy_FetchPrivacyUrl_m4052048955_MetadataUsageId;
extern const uint32_t DataPrivacy_GetUserAgent_m239065990_MetadataUsageId;
extern const uint32_t DataPrivacy_GetUserData_m819583367_MetadataUsageId;
extern const uint32_t DataPrivacy_Initialize_m2126437913_MetadataUsageId;
extern const uint32_t DataPrivacy_LoadFromPlayerPrefs_m1109608605_MetadataUsageId;
extern const uint32_t DataPrivacy_SaveToPlayerPrefs_m1262931573_MetadataUsageId;
extern const uint32_t DataPrivacy_get_instance_m3029115140_MetadataUsageId;
extern const uint32_t DataPrivacy_set_instance_m2747639172_MetadataUsageId;
extern const uint32_t U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_MoveNext_m1899939373_MetadataUsageId;
extern const uint32_t U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_Reset_m2670016829_MetadataUsageId;
extern const uint32_t U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_MoveNext_m3593561120_MetadataUsageId;
extern const uint32_t U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_Reset_m1433355255_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct OptOutStatus_t3541615854;;
struct OptOutStatus_t3541615854_marshaled_com;
struct OptOutStatus_t3541615854_marshaled_com;;
struct OptOutStatus_t3541615854_marshaled_pinvoke;
struct OptOutStatus_t3541615854_marshaled_pinvoke;;
struct RequestData_t3101179086;;
struct RequestData_t3101179086_marshaled_com;
struct RequestData_t3101179086_marshaled_com;;
struct RequestData_t3101179086_marshaled_pinvoke;
struct RequestData_t3101179086_marshaled_pinvoke;;

struct ByteU5BU5D_t4116647657;
struct ObjectU5BU5D_t2843939325;


#ifndef U3CMODULEU3E_T692745580_H
#define U3CMODULEU3E_T692745580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745580 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745580_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t885026589* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t1822382459 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t3348751306 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___entries_1)); }
	inline EntryU5BU5D_t885026589* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t885026589** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t885026589* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keys_7)); }
	inline KeyCollection_t1822382459 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t1822382459 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t1822382459 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___values_8)); }
	inline ValueCollection_t3348751306 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t3348751306 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t3348751306 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_55;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2285235057 * ___dataItem_56;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_57;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_58;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1188251036 * ___encoderFallback_59;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t3123823036 * ___decoderFallback_60;

public:
	inline static int32_t get_offset_of_m_codePage_55() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_codePage_55)); }
	inline int32_t get_m_codePage_55() const { return ___m_codePage_55; }
	inline int32_t* get_address_of_m_codePage_55() { return &___m_codePage_55; }
	inline void set_m_codePage_55(int32_t value)
	{
		___m_codePage_55 = value;
	}

	inline static int32_t get_offset_of_dataItem_56() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___dataItem_56)); }
	inline CodePageDataItem_t2285235057 * get_dataItem_56() const { return ___dataItem_56; }
	inline CodePageDataItem_t2285235057 ** get_address_of_dataItem_56() { return &___dataItem_56; }
	inline void set_dataItem_56(CodePageDataItem_t2285235057 * value)
	{
		___dataItem_56 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_56), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_57() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_deserializedFromEverett_57)); }
	inline bool get_m_deserializedFromEverett_57() const { return ___m_deserializedFromEverett_57; }
	inline bool* get_address_of_m_deserializedFromEverett_57() { return &___m_deserializedFromEverett_57; }
	inline void set_m_deserializedFromEverett_57(bool value)
	{
		___m_deserializedFromEverett_57 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_58() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_isReadOnly_58)); }
	inline bool get_m_isReadOnly_58() const { return ___m_isReadOnly_58; }
	inline bool* get_address_of_m_isReadOnly_58() { return &___m_isReadOnly_58; }
	inline void set_m_isReadOnly_58(bool value)
	{
		___m_isReadOnly_58 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_59() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoderFallback_59)); }
	inline EncoderFallback_t1188251036 * get_encoderFallback_59() const { return ___encoderFallback_59; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoderFallback_59() { return &___encoderFallback_59; }
	inline void set_encoderFallback_59(EncoderFallback_t1188251036 * value)
	{
		___encoderFallback_59 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_59), value);
	}

	inline static int32_t get_offset_of_decoderFallback_60() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoderFallback_60)); }
	inline DecoderFallback_t3123823036 * get_decoderFallback_60() const { return ___decoderFallback_60; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoderFallback_60() { return &___decoderFallback_60; }
	inline void set_decoderFallback_60(DecoderFallback_t3123823036 * value)
	{
		___decoderFallback_60 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_60), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t1523322056 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t1523322056 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t1523322056 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t1853889766 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_61;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t1523322056 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t1523322056 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t1523322056 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t1523322056 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t1523322056 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t1523322056 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t1523322056 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t1523322056 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t1523322056 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t1523322056 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t1523322056 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t1523322056 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t1523322056 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t1523322056 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_8)); }
	inline Hashtable_t1853889766 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t1853889766 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t1853889766 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_61() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___s_InternalSyncObject_61)); }
	inline RuntimeObject * get_s_InternalSyncObject_61() const { return ___s_InternalSyncObject_61; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_61() { return &___s_InternalSyncObject_61; }
	inline void set_s_InternalSyncObject_61(RuntimeObject * value)
	{
		___s_InternalSyncObject_61 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CFETCHPRIVACYURLCOROUTINEU3EC__ITERATOR1_T3219080383_H
#define U3CFETCHPRIVACYURLCOROUTINEU3EC__ITERATOR1_T3219080383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1
struct  U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::<postJson>__0
	String_t* ___U3CpostJsonU3E__0_0;
	// System.Byte[] UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::<bytes>__0
	ByteU5BU5D_t4116647657* ___U3CbytesU3E__0_1;
	// UnityEngine.WWW UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Action`1<System.String> UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::failure
	Action_1_t2019918284 * ___failure_3;
	// System.Action`1<System.String> UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::success
	Action_1_t2019918284 * ___success_4;
	// System.Object UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CpostJsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U3CpostJsonU3E__0_0)); }
	inline String_t* get_U3CpostJsonU3E__0_0() const { return ___U3CpostJsonU3E__0_0; }
	inline String_t** get_address_of_U3CpostJsonU3E__0_0() { return &___U3CpostJsonU3E__0_0; }
	inline void set_U3CpostJsonU3E__0_0(String_t* value)
	{
		___U3CpostJsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostJsonU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U3CbytesU3E__0_1)); }
	inline ByteU5BU5D_t4116647657* get_U3CbytesU3E__0_1() const { return ___U3CbytesU3E__0_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbytesU3E__0_1() { return &___U3CbytesU3E__0_1; }
	inline void set_U3CbytesU3E__0_1(ByteU5BU5D_t4116647657* value)
	{
		___U3CbytesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbytesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_failure_3() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___failure_3)); }
	inline Action_1_t2019918284 * get_failure_3() const { return ___failure_3; }
	inline Action_1_t2019918284 ** get_address_of_failure_3() { return &___failure_3; }
	inline void set_failure_3(Action_1_t2019918284 * value)
	{
		___failure_3 = value;
		Il2CppCodeGenWriteBarrier((&___failure_3), value);
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___success_4)); }
	inline Action_1_t2019918284 * get_success_4() const { return ___success_4; }
	inline Action_1_t2019918284 ** get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(Action_1_t2019918284 * value)
	{
		___success_4 = value;
		Il2CppCodeGenWriteBarrier((&___success_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHPRIVACYURLCOROUTINEU3EC__ITERATOR1_T3219080383_H
#ifndef DATAPRIVACYUTILS_T3678541040_H
#define DATAPRIVACYUTILS_T3678541040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils
struct  DataPrivacyUtils_t3678541040  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPRIVACYUTILS_T3678541040_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef OPTOUTSTATUS_T3541615854_H
#define OPTOUTSTATUS_T3541615854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct  OptOutStatus_t3541615854 
{
public:
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::optOut
	bool ___optOut_0;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::analyticsEnabled
	bool ___analyticsEnabled_1;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::deviceStatsEnabled
	bool ___deviceStatsEnabled_2;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::limitUserTracking
	bool ___limitUserTracking_3;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::performanceReportingEnabled
	bool ___performanceReportingEnabled_4;

public:
	inline static int32_t get_offset_of_optOut_0() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___optOut_0)); }
	inline bool get_optOut_0() const { return ___optOut_0; }
	inline bool* get_address_of_optOut_0() { return &___optOut_0; }
	inline void set_optOut_0(bool value)
	{
		___optOut_0 = value;
	}

	inline static int32_t get_offset_of_analyticsEnabled_1() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___analyticsEnabled_1)); }
	inline bool get_analyticsEnabled_1() const { return ___analyticsEnabled_1; }
	inline bool* get_address_of_analyticsEnabled_1() { return &___analyticsEnabled_1; }
	inline void set_analyticsEnabled_1(bool value)
	{
		___analyticsEnabled_1 = value;
	}

	inline static int32_t get_offset_of_deviceStatsEnabled_2() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___deviceStatsEnabled_2)); }
	inline bool get_deviceStatsEnabled_2() const { return ___deviceStatsEnabled_2; }
	inline bool* get_address_of_deviceStatsEnabled_2() { return &___deviceStatsEnabled_2; }
	inline void set_deviceStatsEnabled_2(bool value)
	{
		___deviceStatsEnabled_2 = value;
	}

	inline static int32_t get_offset_of_limitUserTracking_3() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___limitUserTracking_3)); }
	inline bool get_limitUserTracking_3() const { return ___limitUserTracking_3; }
	inline bool* get_address_of_limitUserTracking_3() { return &___limitUserTracking_3; }
	inline void set_limitUserTracking_3(bool value)
	{
		___limitUserTracking_3 = value;
	}

	inline static int32_t get_offset_of_performanceReportingEnabled_4() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___performanceReportingEnabled_4)); }
	inline bool get_performanceReportingEnabled_4() const { return ___performanceReportingEnabled_4; }
	inline bool* get_address_of_performanceReportingEnabled_4() { return &___performanceReportingEnabled_4; }
	inline void set_performanceReportingEnabled_4(bool value)
	{
		___performanceReportingEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct OptOutStatus_t3541615854_marshaled_pinvoke
{
	int32_t ___optOut_0;
	int32_t ___analyticsEnabled_1;
	int32_t ___deviceStatsEnabled_2;
	int32_t ___limitUserTracking_3;
	int32_t ___performanceReportingEnabled_4;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct OptOutStatus_t3541615854_marshaled_com
{
	int32_t ___optOut_0;
	int32_t ___analyticsEnabled_1;
	int32_t ___deviceStatsEnabled_2;
	int32_t ___limitUserTracking_3;
	int32_t ___performanceReportingEnabled_4;
};
#endif // OPTOUTSTATUS_T3541615854_H
#ifndef REQUESTDATA_T3101179086_H
#define REQUESTDATA_T3101179086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/RequestData
struct  RequestData_t3101179086 
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/RequestData::date
	String_t* ___date_0;

public:
	inline static int32_t get_offset_of_date_0() { return static_cast<int32_t>(offsetof(RequestData_t3101179086, ___date_0)); }
	inline String_t* get_date_0() const { return ___date_0; }
	inline String_t** get_address_of_date_0() { return &___date_0; }
	inline void set_date_0(String_t* value)
	{
		___date_0 = value;
		Il2CppCodeGenWriteBarrier((&___date_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/RequestData
struct RequestData_t3101179086_marshaled_pinvoke
{
	char* ___date_0;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/RequestData
struct RequestData_t3101179086_marshaled_com
{
	Il2CppChar* ___date_0;
};
#endif // REQUESTDATA_T3101179086_H
#ifndef TOKENDATA_T2077495713_H
#define TOKENDATA_T2077495713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/TokenData
struct  TokenData_t2077495713 
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/TokenData::url
	String_t* ___url_0;
	// System.String UnityEngine.Analytics.DataPrivacy/TokenData::token
	String_t* ___token_1;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(TokenData_t2077495713, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_token_1() { return static_cast<int32_t>(offsetof(TokenData_t2077495713, ___token_1)); }
	inline String_t* get_token_1() const { return ___token_1; }
	inline String_t** get_address_of_token_1() { return &___token_1; }
	inline void set_token_1(String_t* value)
	{
		___token_1 = value;
		Il2CppCodeGenWriteBarrier((&___token_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/TokenData
struct TokenData_t2077495713_marshaled_pinvoke
{
	char* ___url_0;
	char* ___token_1;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/TokenData
struct TokenData_t2077495713_marshaled_com
{
	Il2CppChar* ___url_0;
	Il2CppChar* ___token_1;
};
#endif // TOKENDATA_T2077495713_H
#ifndef USERPOSTDATA_T478425489_H
#define USERPOSTDATA_T478425489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/UserPostData
struct  UserPostData_t478425489 
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::appid
	String_t* ___appid_0;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::userid
	String_t* ___userid_1;
	// System.Int64 UnityEngine.Analytics.DataPrivacy/UserPostData::sessionid
	int64_t ___sessionid_2;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::platform
	String_t* ___platform_3;
	// System.UInt32 UnityEngine.Analytics.DataPrivacy/UserPostData::platformid
	uint32_t ___platformid_4;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::sdk_ver
	String_t* ___sdk_ver_5;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/UserPostData::debug_device
	bool ___debug_device_6;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::deviceid
	String_t* ___deviceid_7;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::plugin_ver
	String_t* ___plugin_ver_8;

public:
	inline static int32_t get_offset_of_appid_0() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___appid_0)); }
	inline String_t* get_appid_0() const { return ___appid_0; }
	inline String_t** get_address_of_appid_0() { return &___appid_0; }
	inline void set_appid_0(String_t* value)
	{
		___appid_0 = value;
		Il2CppCodeGenWriteBarrier((&___appid_0), value);
	}

	inline static int32_t get_offset_of_userid_1() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___userid_1)); }
	inline String_t* get_userid_1() const { return ___userid_1; }
	inline String_t** get_address_of_userid_1() { return &___userid_1; }
	inline void set_userid_1(String_t* value)
	{
		___userid_1 = value;
		Il2CppCodeGenWriteBarrier((&___userid_1), value);
	}

	inline static int32_t get_offset_of_sessionid_2() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___sessionid_2)); }
	inline int64_t get_sessionid_2() const { return ___sessionid_2; }
	inline int64_t* get_address_of_sessionid_2() { return &___sessionid_2; }
	inline void set_sessionid_2(int64_t value)
	{
		___sessionid_2 = value;
	}

	inline static int32_t get_offset_of_platform_3() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___platform_3)); }
	inline String_t* get_platform_3() const { return ___platform_3; }
	inline String_t** get_address_of_platform_3() { return &___platform_3; }
	inline void set_platform_3(String_t* value)
	{
		___platform_3 = value;
		Il2CppCodeGenWriteBarrier((&___platform_3), value);
	}

	inline static int32_t get_offset_of_platformid_4() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___platformid_4)); }
	inline uint32_t get_platformid_4() const { return ___platformid_4; }
	inline uint32_t* get_address_of_platformid_4() { return &___platformid_4; }
	inline void set_platformid_4(uint32_t value)
	{
		___platformid_4 = value;
	}

	inline static int32_t get_offset_of_sdk_ver_5() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___sdk_ver_5)); }
	inline String_t* get_sdk_ver_5() const { return ___sdk_ver_5; }
	inline String_t** get_address_of_sdk_ver_5() { return &___sdk_ver_5; }
	inline void set_sdk_ver_5(String_t* value)
	{
		___sdk_ver_5 = value;
		Il2CppCodeGenWriteBarrier((&___sdk_ver_5), value);
	}

	inline static int32_t get_offset_of_debug_device_6() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___debug_device_6)); }
	inline bool get_debug_device_6() const { return ___debug_device_6; }
	inline bool* get_address_of_debug_device_6() { return &___debug_device_6; }
	inline void set_debug_device_6(bool value)
	{
		___debug_device_6 = value;
	}

	inline static int32_t get_offset_of_deviceid_7() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___deviceid_7)); }
	inline String_t* get_deviceid_7() const { return ___deviceid_7; }
	inline String_t** get_address_of_deviceid_7() { return &___deviceid_7; }
	inline void set_deviceid_7(String_t* value)
	{
		___deviceid_7 = value;
		Il2CppCodeGenWriteBarrier((&___deviceid_7), value);
	}

	inline static int32_t get_offset_of_plugin_ver_8() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___plugin_ver_8)); }
	inline String_t* get_plugin_ver_8() const { return ___plugin_ver_8; }
	inline String_t** get_address_of_plugin_ver_8() { return &___plugin_ver_8; }
	inline void set_plugin_ver_8(String_t* value)
	{
		___plugin_ver_8 = value;
		Il2CppCodeGenWriteBarrier((&___plugin_ver_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/UserPostData
struct UserPostData_t478425489_marshaled_pinvoke
{
	char* ___appid_0;
	char* ___userid_1;
	int64_t ___sessionid_2;
	char* ___platform_3;
	uint32_t ___platformid_4;
	char* ___sdk_ver_5;
	int32_t ___debug_device_6;
	char* ___deviceid_7;
	char* ___plugin_ver_8;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/UserPostData
struct UserPostData_t478425489_marshaled_com
{
	Il2CppChar* ___appid_0;
	Il2CppChar* ___userid_1;
	int64_t ___sessionid_2;
	Il2CppChar* ___platform_3;
	uint32_t ___platformid_4;
	Il2CppChar* ___sdk_ver_5;
	int32_t ___debug_device_6;
	Il2CppChar* ___deviceid_7;
	Il2CppChar* ___plugin_ver_8;
};
#endif // USERPOSTDATA_T478425489_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef MEMBERACCESSEXCEPTION_T1734467078_H
#define MEMBERACCESSEXCEPTION_T1734467078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MemberAccessException
struct  MemberAccessException_t1734467078  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERACCESSEXCEPTION_T1734467078_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef FLAGS_T2372798318_H
#define FLAGS_T2372798318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Flags
struct  Flags_t2372798318 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t2372798318, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T2372798318_H
#ifndef URIFORMAT_T2031163398_H
#define URIFORMAT_T2031163398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormat
struct  UriFormat_t2031163398 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t2031163398, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMAT_T2031163398_H
#ifndef URIIDNSCOPE_T1847433844_H
#define URIIDNSCOPE_T1847433844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriIdnScope
struct  UriIdnScope_t1847433844 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_t1847433844, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIIDNSCOPE_T1847433844_H
#ifndef URIKIND_T3816567336_H
#define URIKIND_T3816567336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriKind
struct  UriKind_t3816567336 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_t3816567336, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIKIND_T3816567336_H
#ifndef U3CFETCHOPTOUTSTATUSCOROUTINEU3EC__ITERATOR0_T2643221616_H
#define U3CFETCHOPTOUTSTATUSCOROUTINEU3EC__ITERATOR0_T2643221616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0
struct  U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<localOptOutStatus>__0
	OptOutStatus_t3541615854  ___U3ClocalOptOutStatusU3E__0_0;
	// UnityEngine.Analytics.DataPrivacy/UserPostData UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<userData>__0
	UserPostData_t478425489  ___U3CuserDataU3E__0_1;
	// System.String UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<query>__0
	String_t* ___U3CqueryU3E__0_2;
	// System.Uri UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<baseUri>__0
	Uri_t100236324 * ___U3CbaseUriU3E__0_3;
	// System.Uri UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<uri>__0
	Uri_t100236324 * ___U3CuriU3E__0_4;
	// UnityEngine.WWW UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_5;
	// System.Action`1<System.Boolean> UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::optOutAction
	Action_1_t269755560 * ___optOutAction_6;
	// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<optOutStatus>__1
	OptOutStatus_t3541615854  ___U3CoptOutStatusU3E__1_7;
	// System.Object UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3ClocalOptOutStatusU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3ClocalOptOutStatusU3E__0_0)); }
	inline OptOutStatus_t3541615854  get_U3ClocalOptOutStatusU3E__0_0() const { return ___U3ClocalOptOutStatusU3E__0_0; }
	inline OptOutStatus_t3541615854 * get_address_of_U3ClocalOptOutStatusU3E__0_0() { return &___U3ClocalOptOutStatusU3E__0_0; }
	inline void set_U3ClocalOptOutStatusU3E__0_0(OptOutStatus_t3541615854  value)
	{
		___U3ClocalOptOutStatusU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CuserDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CuserDataU3E__0_1)); }
	inline UserPostData_t478425489  get_U3CuserDataU3E__0_1() const { return ___U3CuserDataU3E__0_1; }
	inline UserPostData_t478425489 * get_address_of_U3CuserDataU3E__0_1() { return &___U3CuserDataU3E__0_1; }
	inline void set_U3CuserDataU3E__0_1(UserPostData_t478425489  value)
	{
		___U3CuserDataU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CqueryU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CqueryU3E__0_2)); }
	inline String_t* get_U3CqueryU3E__0_2() const { return ___U3CqueryU3E__0_2; }
	inline String_t** get_address_of_U3CqueryU3E__0_2() { return &___U3CqueryU3E__0_2; }
	inline void set_U3CqueryU3E__0_2(String_t* value)
	{
		___U3CqueryU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CqueryU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CbaseUriU3E__0_3() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CbaseUriU3E__0_3)); }
	inline Uri_t100236324 * get_U3CbaseUriU3E__0_3() const { return ___U3CbaseUriU3E__0_3; }
	inline Uri_t100236324 ** get_address_of_U3CbaseUriU3E__0_3() { return &___U3CbaseUriU3E__0_3; }
	inline void set_U3CbaseUriU3E__0_3(Uri_t100236324 * value)
	{
		___U3CbaseUriU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbaseUriU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CuriU3E__0_4() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CuriU3E__0_4)); }
	inline Uri_t100236324 * get_U3CuriU3E__0_4() const { return ___U3CuriU3E__0_4; }
	inline Uri_t100236324 ** get_address_of_U3CuriU3E__0_4() { return &___U3CuriU3E__0_4; }
	inline void set_U3CuriU3E__0_4(Uri_t100236324 * value)
	{
		___U3CuriU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuriU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_5() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CwwwU3E__0_5)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_5() const { return ___U3CwwwU3E__0_5; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_5() { return &___U3CwwwU3E__0_5; }
	inline void set_U3CwwwU3E__0_5(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_5), value);
	}

	inline static int32_t get_offset_of_optOutAction_6() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___optOutAction_6)); }
	inline Action_1_t269755560 * get_optOutAction_6() const { return ___optOutAction_6; }
	inline Action_1_t269755560 ** get_address_of_optOutAction_6() { return &___optOutAction_6; }
	inline void set_optOutAction_6(Action_1_t269755560 * value)
	{
		___optOutAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___optOutAction_6), value);
	}

	inline static int32_t get_offset_of_U3CoptOutStatusU3E__1_7() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CoptOutStatusU3E__1_7)); }
	inline OptOutStatus_t3541615854  get_U3CoptOutStatusU3E__1_7() const { return ___U3CoptOutStatusU3E__1_7; }
	inline OptOutStatus_t3541615854 * get_address_of_U3CoptOutStatusU3E__1_7() { return &___U3CoptOutStatusU3E__1_7; }
	inline void set_U3CoptOutStatusU3E__1_7(OptOutStatus_t3541615854  value)
	{
		___U3CoptOutStatusU3E__1_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHOPTOUTSTATUSCOROUTINEU3EC__ITERATOR0_T2643221616_H
#ifndef OPTOUTRESPONSE_T2266495071_H
#define OPTOUTRESPONSE_T2266495071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/OptOutResponse
struct  OptOutResponse_t2266495071 
{
public:
	// UnityEngine.Analytics.DataPrivacy/RequestData UnityEngine.Analytics.DataPrivacy/OptOutResponse::request
	RequestData_t3101179086  ___request_0;
	// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy/OptOutResponse::status
	OptOutStatus_t3541615854  ___status_1;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(OptOutResponse_t2266495071, ___request_0)); }
	inline RequestData_t3101179086  get_request_0() const { return ___request_0; }
	inline RequestData_t3101179086 * get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RequestData_t3101179086  value)
	{
		___request_0 = value;
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(OptOutResponse_t2266495071, ___status_1)); }
	inline OptOutStatus_t3541615854  get_status_1() const { return ___status_1; }
	inline OptOutStatus_t3541615854 * get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(OptOutStatus_t3541615854  value)
	{
		___status_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/OptOutResponse
struct OptOutResponse_t2266495071_marshaled_pinvoke
{
	RequestData_t3101179086_marshaled_pinvoke ___request_0;
	OptOutStatus_t3541615854_marshaled_pinvoke ___status_1;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/OptOutResponse
struct OptOutResponse_t2266495071_marshaled_com
{
	RequestData_t3101179086_marshaled_com ___request_0;
	OptOutStatus_t3541615854_marshaled_com ___status_1;
};
#endif // OPTOUTRESPONSE_T2266495071_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef BUTTONCLICKEDEVENT_T48803504_H
#define BUTTONCLICKEDEVENT_T48803504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button/ButtonClickedEvent
struct  ButtonClickedEvent_t48803504  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCLICKEDEVENT_T48803504_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef MISSINGMEMBEREXCEPTION_T1385081665_H
#define MISSINGMEMBEREXCEPTION_T1385081665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MissingMemberException
struct  MissingMemberException_t1385081665  : public MemberAccessException_t1734467078
{
public:
	// System.String System.MissingMemberException::ClassName
	String_t* ___ClassName_17;
	// System.String System.MissingMemberException::MemberName
	String_t* ___MemberName_18;
	// System.Byte[] System.MissingMemberException::Signature
	ByteU5BU5D_t4116647657* ___Signature_19;

public:
	inline static int32_t get_offset_of_ClassName_17() { return static_cast<int32_t>(offsetof(MissingMemberException_t1385081665, ___ClassName_17)); }
	inline String_t* get_ClassName_17() const { return ___ClassName_17; }
	inline String_t** get_address_of_ClassName_17() { return &___ClassName_17; }
	inline void set_ClassName_17(String_t* value)
	{
		___ClassName_17 = value;
		Il2CppCodeGenWriteBarrier((&___ClassName_17), value);
	}

	inline static int32_t get_offset_of_MemberName_18() { return static_cast<int32_t>(offsetof(MissingMemberException_t1385081665, ___MemberName_18)); }
	inline String_t* get_MemberName_18() const { return ___MemberName_18; }
	inline String_t** get_address_of_MemberName_18() { return &___MemberName_18; }
	inline void set_MemberName_18(String_t* value)
	{
		___MemberName_18 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_18), value);
	}

	inline static int32_t get_offset_of_Signature_19() { return static_cast<int32_t>(offsetof(MissingMemberException_t1385081665, ___Signature_19)); }
	inline ByteU5BU5D_t4116647657* get_Signature_19() const { return ___Signature_19; }
	inline ByteU5BU5D_t4116647657** get_address_of_Signature_19() { return &___Signature_19; }
	inline void set_Signature_19(ByteU5BU5D_t4116647657* value)
	{
		___Signature_19 = value;
		Il2CppCodeGenWriteBarrier((&___Signature_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBEREXCEPTION_T1385081665_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t3890150400 * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_t1092684687 * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_String_16), value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalUnicodeString_17), value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Syntax_18)); }
	inline UriParser_t3890150400 * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t3890150400 ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t3890150400 * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Syntax_18), value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_DnsSafeHost_19), value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Info_21)); }
	inline UriInfo_t1092684687 * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_t1092684687 ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_t1092684687 * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Info_21), value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t3528271667* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t3528271667* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_0), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_1), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_2), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_3), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_4), value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWs_5), value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWss_6), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_7), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_8), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_9), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_10), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_11), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_12), value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_initLock_30), value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t3528271667* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t3528271667** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t3528271667* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((&___HexLowerChars_34), value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t3528271667* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t3528271667** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t3528271667* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((&____WSchars_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef ACTION_1_T269755560_H
#define ACTION_1_T269755560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Boolean>
struct  Action_1_t269755560  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T269755560_H
#ifndef ACTION_1_T2019918284_H
#define ACTION_1_T2019918284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.String>
struct  Action_1_t2019918284  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2019918284_H
#ifndef MISSINGMETHODEXCEPTION_T1274661534_H
#define MISSINGMETHODEXCEPTION_T1274661534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MissingMethodException
struct  MissingMethodException_t1274661534  : public MissingMemberException_t1385081665
{
public:
	// System.String System.MissingMethodException::signature
	String_t* ___signature_20;

public:
	inline static int32_t get_offset_of_signature_20() { return static_cast<int32_t>(offsetof(MissingMethodException_t1274661534, ___signature_20)); }
	inline String_t* get_signature_20() const { return ___signature_20; }
	inline String_t** get_address_of_signature_20() { return &___signature_20; }
	inline void set_signature_20(String_t* value)
	{
		___signature_20 = value;
		Il2CppCodeGenWriteBarrier((&___signature_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMETHODEXCEPTION_T1274661534_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UNITYACTION_T3245792599_H
#define UNITYACTION_T3245792599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t3245792599  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T3245792599_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DATAPRIVACY_T670264006_H
#define DATAPRIVACY_T670264006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy
struct  DataPrivacy_t670264006  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct DataPrivacy_t670264006_StaticFields
{
public:
	// UnityEngine.Analytics.DataPrivacy UnityEngine.Analytics.DataPrivacy::<instance>k__BackingField
	DataPrivacy_t670264006 * ___U3CinstanceU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DataPrivacy_t670264006_StaticFields, ___U3CinstanceU3Ek__BackingField_14)); }
	inline DataPrivacy_t670264006 * get_U3CinstanceU3Ek__BackingField_14() const { return ___U3CinstanceU3Ek__BackingField_14; }
	inline DataPrivacy_t670264006 ** get_address_of_U3CinstanceU3Ek__BackingField_14() { return &___U3CinstanceU3Ek__BackingField_14; }
	inline void set_U3CinstanceU3Ek__BackingField_14(DataPrivacy_t670264006 * value)
	{
		___U3CinstanceU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPRIVACY_T670264006_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef DATAPRIVACYBUTTON_T2259119369_H
#define DATAPRIVACYBUTTON_T2259119369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacyButton
struct  DataPrivacyButton_t2259119369  : public Button_t4055032469
{
public:
	// System.Boolean UnityEngine.Analytics.DataPrivacyButton::urlOpened
	bool ___urlOpened_19;

public:
	inline static int32_t get_offset_of_urlOpened_19() { return static_cast<int32_t>(offsetof(DataPrivacyButton_t2259119369, ___urlOpened_19)); }
	inline bool get_urlOpened_19() const { return ___urlOpened_19; }
	inline bool* get_address_of_urlOpened_19() { return &___urlOpened_19; }
	inline void set_urlOpened_19(bool value)
	{
		___urlOpened_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPRIVACYBUTTON_T2259119369_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};

extern "C" void RequestData_t3101179086_marshal_pinvoke(const RequestData_t3101179086& unmarshaled, RequestData_t3101179086_marshaled_pinvoke& marshaled);
extern "C" void RequestData_t3101179086_marshal_pinvoke_back(const RequestData_t3101179086_marshaled_pinvoke& marshaled, RequestData_t3101179086& unmarshaled);
extern "C" void RequestData_t3101179086_marshal_pinvoke_cleanup(RequestData_t3101179086_marshaled_pinvoke& marshaled);
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke(const OptOutStatus_t3541615854& unmarshaled, OptOutStatus_t3541615854_marshaled_pinvoke& marshaled);
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke_back(const OptOutStatus_t3541615854_marshaled_pinvoke& marshaled, OptOutStatus_t3541615854& unmarshaled);
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke_cleanup(OptOutStatus_t3541615854_marshaled_pinvoke& marshaled);
extern "C" void RequestData_t3101179086_marshal_com(const RequestData_t3101179086& unmarshaled, RequestData_t3101179086_marshaled_com& marshaled);
extern "C" void RequestData_t3101179086_marshal_com_back(const RequestData_t3101179086_marshaled_com& marshaled, RequestData_t3101179086& unmarshaled);
extern "C" void RequestData_t3101179086_marshal_com_cleanup(RequestData_t3101179086_marshaled_com& marshaled);
extern "C" void OptOutStatus_t3541615854_marshal_com(const OptOutStatus_t3541615854& unmarshaled, OptOutStatus_t3541615854_marshaled_com& marshaled);
extern "C" void OptOutStatus_t3541615854_marshal_com_back(const OptOutStatus_t3541615854_marshaled_com& marshaled, OptOutStatus_t3541615854& unmarshaled);
extern "C" void OptOutStatus_t3541615854_marshal_com_cleanup(OptOutStatus_t3541615854_marshaled_com& marshaled);

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m147650894_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m3105409630_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
extern "C" IL2CPP_METHOD_ATTR void Action_1_Invoke_m1933767679_gshared (Action_1_t269755560 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C" IL2CPP_METHOD_ATTR void Action_1_Invoke_m3161192644_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<UnityEngine.Analytics.DataPrivacy/OptOutResponse>(System.String)
extern "C" IL2CPP_METHOD_ATTR OptOutResponse_t2266495071  JsonUtility_FromJson_TisOptOutResponse_t2266495071_m4231913572_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<UnityEngine.Analytics.DataPrivacy/TokenData>(System.String)
extern "C" IL2CPP_METHOD_ATTR TokenData_t2077495713  JsonUtility_FromJson_TisTokenData_t2077495713_m2426260624_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Analytics.DataPrivacy::FetchOptOutStatusCoroutine(System.Action`1<System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DataPrivacy_FetchOptOutStatusCoroutine_m2945634055 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___optOutAction0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// UnityEngine.Analytics.DataPrivacy UnityEngine.Analytics.DataPrivacy::get_instance()
extern "C" IL2CPP_METHOD_ATTR DataPrivacy_t670264006 * DataPrivacy_get_instance_m3029115140 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Analytics.DataPrivacy>()
inline DataPrivacy_t670264006 * GameObject_AddComponent_TisDataPrivacy_t670264006_m467235112 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  DataPrivacy_t670264006 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m147650894_gshared)(__this, method);
}
// System.Void UnityEngine.Analytics.DataPrivacy::set_instance(UnityEngine.Analytics.DataPrivacy)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_set_instance_m2747639172 (RuntimeObject * __this /* static, unused */, DataPrivacy_t670264006 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" IL2CPP_METHOD_ATTR void Object_set_hideFlags_m1648752846 (Object_t631007953 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Analytics.DataPrivacy::FetchPrivacyUrlCoroutine(System.Action`1<System.String>,System.Action`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DataPrivacy_FetchPrivacyUrlCoroutine_m2502745193 (RuntimeObject * __this /* static, unused */, Action_1_t2019918284 * ___success0, Action_1_t2019918284 * ___failure1, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m1299643124 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetApplicationId()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_GetApplicationId_m1000645725 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetUserId()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_GetUserId_m3087321347 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int64 UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetSessionId()
extern "C" IL2CPP_METHOD_ATTR int64_t DataPrivacyUtils_GetSessionId_m3896160837 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" IL2CPP_METHOD_ATTR int32_t Application_get_platform_m2150679437 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_unityVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* Application_get_unityVersion_m1068543125 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C" IL2CPP_METHOD_ATTR bool Debug_get_isDebugBuild_m1389897688 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetDeviceId()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_GetDeviceId_m3628229477 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m630303134 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchOptOutStatusCoroutineU3Ec__Iterator0__ctor_m3330376850 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1__ctor_m3713895710 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy::LoadFromPlayerPrefs()
extern "C" IL2CPP_METHOD_ATTR OptOutStatus_t3541615854  DataPrivacy_LoadFromPlayerPrefs_m1109608605 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::SetOptOutStatus(UnityEngine.Analytics.DataPrivacy/OptOutStatus)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyUtils_SetOptOutStatus_m4281284745 (RuntimeObject * __this /* static, unused */, OptOutStatus_t3541615854  ___optOutStatus0, const RuntimeMethod* method);
// UnityEngine.Analytics.DataPrivacy/UserPostData UnityEngine.Analytics.DataPrivacy::GetUserData()
extern "C" IL2CPP_METHOD_ATTR UserPostData_t478425489  DataPrivacy_GetUserData_m819583367 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m3339413201 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
// System.Void System.Uri::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Uri__ctor_m800430703 (Uri_t100236324 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Uri::.ctor(System.Uri,System.String)
extern "C" IL2CPP_METHOD_ATTR void Uri__ctor_m4293005803 (Uri_t100236324 * __this, Uri_t100236324 * p0, String_t* p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
inline void Dictionary_2__ctor_m444307833 (Dictionary_2_t1632706988 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1632706988 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.String UnityEngine.Analytics.DataPrivacy::GetUserAgent()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacy_GetUserAgent_m239065990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_m3045345476 (Dictionary_2_t1632706988 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1632706988 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m3105409630_gshared)(__this, p0, p1, method);
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void WWW__ctor_m4096577320 (WWW_t3688466362 * __this, String_t* p0, ByteU5BU5D_t4116647657* p1, Dictionary_2_t1632706988 * p2, const RuntimeMethod* method);
// System.String UnityEngine.WWW::get_error()
extern "C" IL2CPP_METHOD_ATTR String_t* WWW_get_error_m3055313367 (WWW_t3688466362 * __this, const RuntimeMethod* method);
// System.String UnityEngine.WWW::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* WWW_get_text_m898164367 (WWW_t3688466362 * __this, const RuntimeMethod* method);
// System.String UnityEngine.WWW::get_url()
extern "C" IL2CPP_METHOD_ATTR String_t* WWW_get_url_m3672399347 (WWW_t3688466362 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
inline void Action_1_Invoke_m1933767679 (Action_1_t269755560 * __this, bool p0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t269755560 *, bool, const RuntimeMethod*))Action_1_Invoke_m1933767679_gshared)(__this, p0, method);
}
// UnityEngine.Analytics.DataPrivacy/OptOutResponse UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::ParseOptOutResponse(System.String)
extern "C" IL2CPP_METHOD_ATTR OptOutResponse_t2266495071  DataPrivacyUtils_ParseOptOutResponse_m841987952 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.DataPrivacy::SaveToPlayerPrefs(UnityEngine.Analytics.DataPrivacy/OptOutStatus)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_SaveToPlayerPrefs_m1262931573 (RuntimeObject * __this /* static, unused */, OptOutStatus_t3541615854  ___optOutStatus0, const RuntimeMethod* method);
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::OptOutStatusToJson(UnityEngine.Analytics.DataPrivacy/OptOutStatus)
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_OptOutStatusToJson_m3940455503 (RuntimeObject * __this /* static, unused */, OptOutStatus_t3541615854  ___optOutStatus0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::UserPostDataToJson(UnityEngine.Analytics.DataPrivacy/UserPostData)
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_UserPostDataToJson_m966437446 (RuntimeObject * __this /* static, unused */, UserPostData_t478425489  ___postData0, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C" IL2CPP_METHOD_ATTR Encoding_t1523322056 * Encoding_get_UTF8_m1008486739 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::Invoke(!0)
inline void Action_1_Invoke_m935934032 (Action_1_t2019918284 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t2019918284 *, String_t*, const RuntimeMethod*))Action_1_Invoke_m3161192644_gshared)(__this, p0, method);
}
// UnityEngine.Analytics.DataPrivacy/TokenData UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::ParseTokenData(System.String)
extern "C" IL2CPP_METHOD_ATTR TokenData_t2077495713  DataPrivacyUtils_ParseTokenData_m1538885353 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Analytics.Analytics::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool Analytics_get_enabled_m451579624 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.Analytics::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Analytics_set_enabled_m1692893694 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Analytics.Analytics::get_deviceStatsEnabled()
extern "C" IL2CPP_METHOD_ATTR bool Analytics_get_deviceStatsEnabled_m316602018 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.Analytics::set_deviceStatsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Analytics_set_deviceStatsEnabled_m1800429646 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Analytics.Analytics::get_limitUserTracking()
extern "C" IL2CPP_METHOD_ATTR bool Analytics_get_limitUserTracking_m3159512945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.Analytics::set_limitUserTracking(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Analytics_set_limitUserTracking_m2037024580 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Analytics.PerformanceReporting::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool PerformanceReporting_get_enabled_m2429669256 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.PerformanceReporting::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PerformanceReporting_set_enabled_m751379947 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.String UnityEngine.JsonUtility::ToJson(System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* JsonUtility_ToJson_m3644929270 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<UnityEngine.Analytics.DataPrivacy/OptOutResponse>(System.String)
inline OptOutResponse_t2266495071  JsonUtility_FromJson_TisOptOutResponse_t2266495071_m4231913572 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method)
{
	return ((  OptOutResponse_t2266495071  (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisOptOutResponse_t2266495071_m4231913572_gshared)(__this /* static, unused */, p0, method);
}
// !!0 UnityEngine.JsonUtility::FromJson<UnityEngine.Analytics.DataPrivacy/TokenData>(System.String)
inline TokenData_t2077495713  JsonUtility_FromJson_TisTokenData_t2077495713_m2426260624 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method)
{
	return ((  TokenData_t2077495713  (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisTokenData_t2077495713_m2426260624_gshared)(__this /* static, unused */, p0, method);
}
// System.String UnityEngine.Analytics.AnalyticsSessionInfo::get_userId()
extern "C" IL2CPP_METHOD_ATTR String_t* AnalyticsSessionInfo_get_userId_m3553273537 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int64 UnityEngine.Analytics.AnalyticsSessionInfo::get_sessionId()
extern "C" IL2CPP_METHOD_ATTR int64_t AnalyticsSessionInfo_get_sessionId_m3617445673 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_cloudProjectId()
extern "C" IL2CPP_METHOD_ATTR String_t* Application_get_cloudProjectId_m3678427659 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C" IL2CPP_METHOD_ATTR String_t* SystemInfo_get_deviceUniqueIdentifier_m3439870207 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Button::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Button__ctor_m2784091109 (Button_t4055032469 * __this, const RuntimeMethod* method);
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" IL2CPP_METHOD_ATTR ButtonClickedEvent_t48803504 * Button_get_onClick_m2332132945 (Button_t4055032469 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void UnityAction__ctor_m772160306 (UnityAction_t3245792599 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C" IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m2276267359 (UnityEvent_t2581268647 * __this, UnityAction_t3245792599 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Selectable_set_interactable_m3105888815 (Selectable_t3250028441 * __this, bool p0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C" IL2CPP_METHOD_ATTR void Application_OpenURL_m738341736 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m1347592571 (Action_1_t2019918284 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t2019918284 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method);
}
// System.Void UnityEngine.Analytics.DataPrivacy::FetchPrivacyUrl(System.Action`1<System.String>,System.Action`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_FetchPrivacyUrl_m4052048955 (RuntimeObject * __this /* static, unused */, Action_1_t2019918284 * ___success0, Action_1_t2019918284 * ___failure1, const RuntimeMethod* method);
// System.Void UnityEngine.Analytics.DataPrivacy::FetchOptOutStatus(System.Action`1<System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_FetchOptOutStatus_m2364404913 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___optOutAction0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Analytics.DataPrivacy::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy__ctor_m2779411680 (DataPrivacy_t670264006 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Analytics.DataPrivacy UnityEngine.Analytics.DataPrivacy::get_instance()
extern "C" IL2CPP_METHOD_ATTR DataPrivacy_t670264006 * DataPrivacy_get_instance_m3029115140 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_get_instance_m3029115140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataPrivacy_t670264006 * L_0 = ((DataPrivacy_t670264006_StaticFields*)il2cpp_codegen_static_fields_for(DataPrivacy_t670264006_il2cpp_TypeInfo_var))->get_U3CinstanceU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy::set_instance(UnityEngine.Analytics.DataPrivacy)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_set_instance_m2747639172 (RuntimeObject * __this /* static, unused */, DataPrivacy_t670264006 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_set_instance_m2747639172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataPrivacy_t670264006 * L_0 = ___value0;
		((DataPrivacy_t670264006_StaticFields*)il2cpp_codegen_static_fields_for(DataPrivacy_t670264006_il2cpp_TypeInfo_var))->set_U3CinstanceU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy::Start()
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_Start_m1425615916 (DataPrivacy_t670264006 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = DataPrivacy_FetchOptOutStatusCoroutine_m2945634055(NULL /*static, unused*/, (Action_1_t269755560 *)NULL, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy::Initialize()
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_Initialize_m2126437913 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_Initialize_m2126437913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		DataPrivacy_t670264006 * L_0 = DataPrivacy_get_instance_m3029115140(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_t1113636619 * L_2 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_2, _stringLiteral909076603, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		DataPrivacy_t670264006 * L_4 = GameObject_AddComponent_TisDataPrivacy_t670264006_m467235112(L_3, /*hidden argument*/GameObject_AddComponent_TisDataPrivacy_t670264006_m467235112_RuntimeMethod_var);
		DataPrivacy_set_instance_m2747639172(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = V_0;
		NullCheck(L_5);
		Object_set_hideFlags_m1648752846(L_5, ((int32_t)61), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy::FetchOptOutStatus(System.Action`1<System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_FetchOptOutStatus_m2364404913 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___optOutAction0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_FetchOptOutStatus_m2364404913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataPrivacy_t670264006 * L_0 = DataPrivacy_get_instance_m3029115140(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral3599900414, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		DataPrivacy_t670264006 * L_2 = DataPrivacy_get_instance_m3029115140(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t269755560 * L_3 = ___optOutAction0;
		RuntimeObject* L_4 = DataPrivacy_FetchOptOutStatusCoroutine_m2945634055(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		MonoBehaviour_StartCoroutine_m3411253000(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy::FetchPrivacyUrl(System.Action`1<System.String>,System.Action`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_FetchPrivacyUrl_m4052048955 (RuntimeObject * __this /* static, unused */, Action_1_t2019918284 * ___success0, Action_1_t2019918284 * ___failure1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_FetchPrivacyUrl_m4052048955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DataPrivacy_t670264006 * L_0 = DataPrivacy_get_instance_m3029115140(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral1644330790, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		DataPrivacy_t670264006 * L_2 = DataPrivacy_get_instance_m3029115140(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t2019918284 * L_3 = ___success0;
		Action_1_t2019918284 * L_4 = ___failure1;
		RuntimeObject* L_5 = DataPrivacy_FetchPrivacyUrlCoroutine_m2502745193(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		MonoBehaviour_StartCoroutine_m3411253000(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy::LoadFromPlayerPrefs()
extern "C" IL2CPP_METHOD_ATTR OptOutStatus_t3541615854  DataPrivacy_LoadFromPlayerPrefs_m1109608605 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_LoadFromPlayerPrefs_m1109608605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OptOutStatus_t3541615854  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(OptOutStatus_t3541615854 ));
		int32_t L_0 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2986767237, 1, /*hidden argument*/NULL);
		(&V_0)->set_analyticsEnabled_1((bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0));
		int32_t L_1 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2651549123, 1, /*hidden argument*/NULL);
		(&V_0)->set_deviceStatsEnabled_2((bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0));
		int32_t L_2 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2049904153, 0, /*hidden argument*/NULL);
		(&V_0)->set_limitUserTracking_3((bool)((((int32_t)L_2) == ((int32_t)1))? 1 : 0));
		int32_t L_3 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral866926178, 1, /*hidden argument*/NULL);
		(&V_0)->set_performanceReportingEnabled_4((bool)((((int32_t)L_3) == ((int32_t)1))? 1 : 0));
		int32_t L_4 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral2549449189, 0, /*hidden argument*/NULL);
		(&V_0)->set_optOut_0((bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0));
		OptOutStatus_t3541615854  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy::SaveToPlayerPrefs(UnityEngine.Analytics.DataPrivacy/OptOutStatus)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacy_SaveToPlayerPrefs_m1262931573 (RuntimeObject * __this /* static, unused */, OptOutStatus_t3541615854  ___optOutStatus0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_SaveToPlayerPrefs_m1262931573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	String_t* G_B6_1 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	String_t* G_B9_1 = NULL;
	String_t* G_B11_0 = NULL;
	String_t* G_B10_0 = NULL;
	int32_t G_B12_0 = 0;
	String_t* G_B12_1 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B13_0 = NULL;
	int32_t G_B15_0 = 0;
	String_t* G_B15_1 = NULL;
	{
		bool L_0 = (&___optOutStatus0)->get_analyticsEnabled_1();
		G_B1_0 = _stringLiteral2986767237;
		if (!L_0)
		{
			G_B2_0 = _stringLiteral2986767237;
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0018:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		bool L_1 = (&___optOutStatus0)->get_deviceStatsEnabled_2();
		G_B4_0 = _stringLiteral2651549123;
		if (!L_1)
		{
			G_B5_0 = _stringLiteral2651549123;
			goto IL_0034;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0035;
	}

IL_0034:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0035:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		bool L_2 = (&___optOutStatus0)->get_limitUserTracking_3();
		G_B7_0 = _stringLiteral2049904153;
		if (!L_2)
		{
			G_B8_0 = _stringLiteral2049904153;
			goto IL_0051;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_0052;
	}

IL_0051:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_0052:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B9_1, G_B9_0, /*hidden argument*/NULL);
		bool L_3 = (&___optOutStatus0)->get_performanceReportingEnabled_4();
		G_B10_0 = _stringLiteral866926178;
		if (!L_3)
		{
			G_B11_0 = _stringLiteral866926178;
			goto IL_006e;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		goto IL_006f;
	}

IL_006e:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_006f:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		bool L_4 = (&___optOutStatus0)->get_optOut_0();
		G_B13_0 = _stringLiteral2549449189;
		if (!L_4)
		{
			G_B14_0 = _stringLiteral2549449189;
			goto IL_008b;
		}
	}
	{
		G_B15_0 = 1;
		G_B15_1 = G_B13_0;
		goto IL_008c;
	}

IL_008b:
	{
		G_B15_0 = 0;
		G_B15_1 = G_B14_0;
	}

IL_008c:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B15_1, G_B15_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Analytics.DataPrivacy/UserPostData UnityEngine.Analytics.DataPrivacy::GetUserData()
extern "C" IL2CPP_METHOD_ATTR UserPostData_t478425489  DataPrivacy_GetUserData_m819583367 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_GetUserData_m819583367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UserPostData_t478425489  V_0;
	memset(&V_0, 0, sizeof(V_0));
	UserPostData_t478425489  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		il2cpp_codegen_initobj((&V_1), sizeof(UserPostData_t478425489 ));
		String_t* L_0 = DataPrivacyUtils_GetApplicationId_m1000645725(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_appid_0(L_0);
		String_t* L_1 = DataPrivacyUtils_GetUserId_m3087321347(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_userid_1(L_1);
		int64_t L_2 = DataPrivacyUtils_GetSessionId_m3896160837(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_sessionid_2(L_2);
		int32_t L_3 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_3;
		RuntimeObject * L_4 = Box(RuntimePlatform_t4159857903_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_2 = *(int32_t*)UnBox(L_4);
		(&V_1)->set_platform_3(L_5);
		int32_t L_6 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_platformid_4(L_6);
		String_t* L_7 = Application_get_unityVersion_m1068543125(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_sdk_ver_5(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		bool L_8 = Debug_get_isDebugBuild_m1389897688(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_debug_device_6(L_8);
		String_t* L_9 = DataPrivacyUtils_GetDeviceId_m3628229477(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_deviceid_7(L_9);
		(&V_1)->set_plugin_ver_8(_stringLiteral2283129959);
		UserPostData_t478425489  L_10 = V_1;
		V_0 = L_10;
		UserPostData_t478425489  L_11 = V_0;
		return L_11;
	}
}
// System.String UnityEngine.Analytics.DataPrivacy::GetUserAgent()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacy_GetUserAgent_m239065990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_GetUserAgent_m239065990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t2843939325* G_B2_1 = NULL;
	ObjectU5BU5D_t2843939325* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t2843939325* G_B1_1 = NULL;
	ObjectU5BU5D_t2843939325* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t2843939325* G_B3_2 = NULL;
	ObjectU5BU5D_t2843939325* G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	{
		V_0 = _stringLiteral4071441506;
		String_t* L_0 = V_0;
		ObjectU5BU5D_t2843939325* L_1 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		String_t* L_3 = Application_get_unityVersion_m1068543125(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_2;
		int32_t L_5 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		RuntimeObject * L_6 = Box(RuntimePlatform_t4159857903_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = *(int32_t*)UnBox(L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		int32_t L_9 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint32_t L_10 = ((uint32_t)L_9);
		RuntimeObject * L_11 = Box(UInt32_t2560061978_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		bool L_13 = Debug_get_isDebugBuild_m1389897688(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = 3;
		G_B1_1 = L_12;
		G_B1_2 = L_12;
		G_B1_3 = L_0;
		if (!L_13)
		{
			G_B2_0 = 3;
			G_B2_1 = L_12;
			G_B2_2 = L_12;
			G_B2_3 = L_0;
			goto IL_004e;
		}
	}
	{
		G_B3_0 = _stringLiteral1138802964;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0053;
	}

IL_004e:
	{
		String_t* L_14 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		G_B3_0 = L_14;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0053:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (RuntimeObject *)G_B3_0);
		ObjectU5BU5D_t2843939325* L_15 = G_B3_3;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral2283129959);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral2283129959);
		String_t* L_16 = String_Format_m630303134(NULL /*static, unused*/, G_B3_4, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Collections.IEnumerator UnityEngine.Analytics.DataPrivacy::FetchOptOutStatusCoroutine(System.Action`1<System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DataPrivacy_FetchOptOutStatusCoroutine_m2945634055 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___optOutAction0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_FetchOptOutStatusCoroutine_m2945634055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * V_0 = NULL;
	{
		U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * L_0 = (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 *)il2cpp_codegen_object_new(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616_il2cpp_TypeInfo_var);
		U3CFetchOptOutStatusCoroutineU3Ec__Iterator0__ctor_m3330376850(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * L_1 = V_0;
		Action_1_t269755560 * L_2 = ___optOutAction0;
		NullCheck(L_1);
		L_1->set_optOutAction_6(L_2);
		U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * L_3 = V_0;
		return L_3;
	}
}
// System.Collections.IEnumerator UnityEngine.Analytics.DataPrivacy::FetchPrivacyUrlCoroutine(System.Action`1<System.String>,System.Action`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DataPrivacy_FetchPrivacyUrlCoroutine_m2502745193 (RuntimeObject * __this /* static, unused */, Action_1_t2019918284 * ___success0, Action_1_t2019918284 * ___failure1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacy_FetchPrivacyUrlCoroutine_m2502745193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * V_0 = NULL;
	{
		U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * L_0 = (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 *)il2cpp_codegen_object_new(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383_il2cpp_TypeInfo_var);
		U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1__ctor_m3713895710(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * L_1 = V_0;
		Action_1_t2019918284 * L_2 = ___failure1;
		NullCheck(L_1);
		L_1->set_failure_3(L_2);
		U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * L_3 = V_0;
		Action_1_t2019918284 * L_4 = ___success0;
		NullCheck(L_3);
		L_3->set_success_4(L_4);
		U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchOptOutStatusCoroutineU3Ec__Iterator0__ctor_m3330376850 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_MoveNext_m1899939373 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_MoveNext_m1899939373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Dictionary_2_t1632706988 * V_1 = NULL;
	String_t* V_2 = NULL;
	OptOutResponse_t2266495071  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0145;
			}
		}
	}
	{
		goto IL_0291;
	}

IL_0021:
	{
		OptOutStatus_t3541615854  L_2 = DataPrivacy_LoadFromPlayerPrefs_m1109608605(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3ClocalOptOutStatusU3E__0_0(L_2);
		OptOutStatus_t3541615854  L_3 = __this->get_U3ClocalOptOutStatusU3E__0_0();
		DataPrivacyUtils_SetOptOutStatus_m4281284745(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		UserPostData_t478425489  L_4 = DataPrivacy_GetUserData_m819583367(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CuserDataU3E__0_1(L_4);
		UserPostData_t478425489 * L_5 = __this->get_address_of_U3CuserDataU3E__0_1();
		String_t* L_6 = L_5->get_appid_0();
		bool L_7 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral2231664933, /*hidden argument*/NULL);
	}

IL_0061:
	{
		UserPostData_t478425489 * L_8 = __this->get_address_of_U3CuserDataU3E__0_1();
		String_t* L_9 = L_8->get_userid_1();
		bool L_10 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral3265403522, /*hidden argument*/NULL);
	}

IL_0080:
	{
		UserPostData_t478425489 * L_11 = __this->get_address_of_U3CuserDataU3E__0_1();
		String_t* L_12 = L_11->get_deviceid_7();
		bool L_13 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral3481671258, /*hidden argument*/NULL);
	}

IL_009f:
	{
		UserPostData_t478425489 * L_14 = __this->get_address_of_U3CuserDataU3E__0_1();
		String_t* L_15 = L_14->get_appid_0();
		UserPostData_t478425489 * L_16 = __this->get_address_of_U3CuserDataU3E__0_1();
		String_t* L_17 = L_16->get_userid_1();
		UserPostData_t478425489 * L_18 = __this->get_address_of_U3CuserDataU3E__0_1();
		String_t* L_19 = L_18->get_deviceid_7();
		String_t* L_20 = String_Format_m3339413201(NULL /*static, unused*/, _stringLiteral3747912002, L_15, L_17, L_19, /*hidden argument*/NULL);
		__this->set_U3CqueryU3E__0_2(L_20);
		Uri_t100236324 * L_21 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_21, _stringLiteral776475747, /*hidden argument*/NULL);
		__this->set_U3CbaseUriU3E__0_3(L_21);
		Uri_t100236324 * L_22 = __this->get_U3CbaseUriU3E__0_3();
		String_t* L_23 = __this->get_U3CqueryU3E__0_2();
		Uri_t100236324 * L_24 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m4293005803(L_24, L_22, L_23, /*hidden argument*/NULL);
		__this->set_U3CuriU3E__0_4(L_24);
		Uri_t100236324 * L_25 = __this->get_U3CuriU3E__0_4();
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		Dictionary_2_t1632706988 * L_27 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m444307833(L_27, /*hidden argument*/Dictionary_2__ctor_m444307833_RuntimeMethod_var);
		V_1 = L_27;
		Dictionary_2_t1632706988 * L_28 = V_1;
		String_t* L_29 = DataPrivacy_GetUserAgent_m239065990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		Dictionary_2_Add_m3045345476(L_28, _stringLiteral827492760, L_29, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_30 = V_1;
		WWW_t3688466362 * L_31 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m4096577320(L_31, L_26, (ByteU5BU5D_t4116647657*)(ByteU5BU5D_t4116647657*)NULL, L_30, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_5(L_31);
		WWW_t3688466362 * L_32 = __this->get_U3CwwwU3E__0_5();
		__this->set_U24current_8(L_32);
		bool L_33 = __this->get_U24disposing_9();
		if (L_33)
		{
			goto IL_0140;
		}
	}
	{
		__this->set_U24PC_10(1);
	}

IL_0140:
	{
		goto IL_0293;
	}

IL_0145:
	{
		WWW_t3688466362 * L_34 = __this->get_U3CwwwU3E__0_5();
		NullCheck(L_34);
		String_t* L_35 = WWW_get_error_m3055313367(L_34, /*hidden argument*/NULL);
		bool L_36 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_016f;
		}
	}
	{
		WWW_t3688466362 * L_37 = __this->get_U3CwwwU3E__0_5();
		NullCheck(L_37);
		String_t* L_38 = WWW_get_text_m898164367(L_37, /*hidden argument*/NULL);
		bool L_39 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_01cd;
		}
	}

IL_016f:
	{
		WWW_t3688466362 * L_40 = __this->get_U3CwwwU3E__0_5();
		NullCheck(L_40);
		String_t* L_41 = WWW_get_error_m3055313367(L_40, /*hidden argument*/NULL);
		V_2 = L_41;
		String_t* L_42 = V_2;
		bool L_43 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_018c;
		}
	}
	{
		V_2 = _stringLiteral3855945919;
	}

IL_018c:
	{
		WWW_t3688466362 * L_44 = __this->get_U3CwwwU3E__0_5();
		NullCheck(L_44);
		String_t* L_45 = WWW_get_url_m3672399347(L_44, /*hidden argument*/NULL);
		String_t* L_46 = V_2;
		String_t* L_47 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral3607360360, L_45, L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Action_1_t269755560 * L_48 = __this->get_optOutAction_6();
		if (!L_48)
		{
			goto IL_01c8;
		}
	}
	{
		Action_1_t269755560 * L_49 = __this->get_optOutAction_6();
		OptOutStatus_t3541615854 * L_50 = __this->get_address_of_U3ClocalOptOutStatusU3E__0_0();
		bool L_51 = L_50->get_optOut_0();
		NullCheck(L_49);
		Action_1_Invoke_m1933767679(L_49, L_51, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
	}

IL_01c8:
	{
		goto IL_0291;
	}

IL_01cd:
	try
	{ // begin try (depth: 1)
		WWW_t3688466362 * L_52 = __this->get_U3CwwwU3E__0_5();
		NullCheck(L_52);
		String_t* L_53 = WWW_get_text_m898164367(L_52, /*hidden argument*/NULL);
		OptOutResponse_t2266495071  L_54 = DataPrivacyUtils_ParseOptOutResponse_m841987952(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		V_3 = L_54;
		OptOutStatus_t3541615854  L_55 = (&V_3)->get_status_1();
		__this->set_U3CoptOutStatusU3E__1_7(L_55);
		goto IL_0239;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_01f0;
		throw e;
	}

CATCH_01f0:
	{ // begin catch(System.Exception)
		{
			V_4 = ((Exception_t *)__exception_local);
			WWW_t3688466362 * L_56 = __this->get_U3CwwwU3E__0_5();
			NullCheck(L_56);
			String_t* L_57 = WWW_get_url_m3672399347(L_56, /*hidden argument*/NULL);
			Exception_t * L_58 = V_4;
			NullCheck(L_58);
			String_t* L_59 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_58);
			String_t* L_60 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral3607360360, L_57, L_59, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
			Action_1_t269755560 * L_61 = __this->get_optOutAction_6();
			if (!L_61)
			{
				goto IL_0234;
			}
		}

IL_021e:
		{
			Action_1_t269755560 * L_62 = __this->get_optOutAction_6();
			OptOutStatus_t3541615854 * L_63 = __this->get_address_of_U3ClocalOptOutStatusU3E__0_0();
			bool L_64 = L_63->get_optOut_0();
			NullCheck(L_62);
			Action_1_Invoke_m1933767679(L_62, L_64, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
		}

IL_0234:
		{
			goto IL_0291;
		}
	} // end catch (depth: 1)

IL_0239:
	{
		OptOutStatus_t3541615854  L_65 = __this->get_U3CoptOutStatusU3E__1_7();
		DataPrivacyUtils_SetOptOutStatus_m4281284745(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		OptOutStatus_t3541615854  L_66 = __this->get_U3CoptOutStatusU3E__1_7();
		DataPrivacy_SaveToPlayerPrefs_m1262931573(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		OptOutStatus_t3541615854  L_67 = __this->get_U3CoptOutStatusU3E__1_7();
		String_t* L_68 = DataPrivacyUtils_OptOutStatusToJson_m3940455503(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		String_t* L_69 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3216638377, L_68, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		Action_1_t269755560 * L_70 = __this->get_optOutAction_6();
		if (!L_70)
		{
			goto IL_028a;
		}
	}
	{
		Action_1_t269755560 * L_71 = __this->get_optOutAction_6();
		OptOutStatus_t3541615854 * L_72 = __this->get_address_of_U3CoptOutStatusU3E__1_7();
		bool L_73 = L_72->get_optOut_0();
		NullCheck(L_71);
		Action_1_Invoke_m1933767679(L_71, L_73, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
	}

IL_028a:
	{
		__this->set_U24PC_10((-1));
	}

IL_0291:
	{
		return (bool)0;
	}

IL_0293:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m951824822 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1711259839 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_Dispose_m4226780847 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_9((bool)1);
		__this->set_U24PC_10((-1));
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_Reset_m2670016829 (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_Reset_m2670016829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_Reset_m2670016829_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1__ctor_m3713895710 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_MoveNext_m3593561120 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_MoveNext_m3593561120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Dictionary_2_t1632706988 * V_1 = NULL;
	String_t* V_2 = NULL;
	TokenData_t2077495713  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00a4;
			}
		}
	}
	{
		goto IL_0154;
	}

IL_0021:
	{
		UserPostData_t478425489  L_2 = DataPrivacy_GetUserData_m819583367(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = DataPrivacyUtils_UserPostDataToJson_m966437446(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_U3CpostJsonU3E__0_0(L_3);
		Encoding_t1523322056 * L_4 = Encoding_get_UTF8_m1008486739(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_U3CpostJsonU3E__0_0();
		NullCheck(L_4);
		ByteU5BU5D_t4116647657* L_6 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(18 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		__this->set_U3CbytesU3E__0_1(L_6);
		ByteU5BU5D_t4116647657* L_7 = __this->get_U3CbytesU3E__0_1();
		Dictionary_2_t1632706988 * L_8 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m444307833(L_8, /*hidden argument*/Dictionary_2__ctor_m444307833_RuntimeMethod_var);
		V_1 = L_8;
		Dictionary_2_t1632706988 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m3045345476(L_9, _stringLiteral2263792357, _stringLiteral1946891126, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_10 = V_1;
		String_t* L_11 = DataPrivacy_GetUserAgent_m239065990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_Add_m3045345476(L_10, _stringLiteral827492760, L_11, /*hidden argument*/Dictionary_2_Add_m3045345476_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_12 = V_1;
		WWW_t3688466362 * L_13 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m4096577320(L_13, _stringLiteral2290071638, L_7, L_12, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_2(L_13);
		WWW_t3688466362 * L_14 = __this->get_U3CwwwU3E__0_2();
		__this->set_U24current_5(L_14);
		bool L_15 = __this->get_U24disposing_6();
		if (L_15)
		{
			goto IL_009f;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_009f:
	{
		goto IL_0156;
	}

IL_00a4:
	{
		WWW_t3688466362 * L_16 = __this->get_U3CwwwU3E__0_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m3055313367(L_16, /*hidden argument*/NULL);
		bool L_18 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ce;
		}
	}
	{
		WWW_t3688466362 * L_19 = __this->get_U3CwwwU3E__0_2();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_text_m898164367(L_19, /*hidden argument*/NULL);
		bool L_21 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_010c;
		}
	}

IL_00ce:
	{
		WWW_t3688466362 * L_22 = __this->get_U3CwwwU3E__0_2();
		NullCheck(L_22);
		String_t* L_23 = WWW_get_error_m3055313367(L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		String_t* L_24 = V_2;
		bool L_25 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00eb;
		}
	}
	{
		V_2 = _stringLiteral3855945919;
	}

IL_00eb:
	{
		Action_1_t2019918284 * L_26 = __this->get_failure_3();
		if (!L_26)
		{
			goto IL_0107;
		}
	}
	{
		Action_1_t2019918284 * L_27 = __this->get_failure_3();
		String_t* L_28 = V_2;
		NullCheck(L_27);
		Action_1_Invoke_m935934032(L_27, L_28, /*hidden argument*/Action_1_Invoke_m935934032_RuntimeMethod_var);
		goto IL_0154;
	}

IL_0107:
	{
		goto IL_014d;
	}

IL_010c:
	try
	{ // begin try (depth: 1)
		WWW_t3688466362 * L_29 = __this->get_U3CwwwU3E__0_2();
		NullCheck(L_29);
		String_t* L_30 = WWW_get_text_m898164367(L_29, /*hidden argument*/NULL);
		TokenData_t2077495713  L_31 = DataPrivacyUtils_ParseTokenData_m1538885353(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		goto IL_013b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0122;
		throw e;
	}

CATCH_0122:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t *)__exception_local);
		Action_1_t2019918284 * L_32 = __this->get_failure_3();
		Exception_t * L_33 = V_4;
		NullCheck(L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_33);
		NullCheck(L_32);
		Action_1_Invoke_m935934032(L_32, L_34, /*hidden argument*/Action_1_Invoke_m935934032_RuntimeMethod_var);
		goto IL_0154;
	} // end catch (depth: 1)

IL_013b:
	{
		Action_1_t2019918284 * L_35 = __this->get_success_4();
		String_t* L_36 = (&V_3)->get_url_0();
		NullCheck(L_35);
		Action_1_Invoke_m935934032(L_35, L_36, /*hidden argument*/Action_1_Invoke_m935934032_RuntimeMethod_var);
	}

IL_014d:
	{
		__this->set_U24PC_7((-1));
	}

IL_0154:
	{
		return (bool)0;
	}

IL_0156:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1785683198 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3464672928 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_Dispose_m1448244550 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_Reset_m1433355255 (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_Reset_m1433355255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_Reset_m1433355255_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::SetOptOutStatus(UnityEngine.Analytics.DataPrivacy/OptOutStatus)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyUtils_SetOptOutStatus_m4281284745 (RuntimeObject * __this /* static, unused */, OptOutStatus_t3541615854  ___optOutStatus0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyUtils_SetOptOutStatus_m4281284745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	Exception_t * V_1 = NULL;
	Exception_t * V_2 = NULL;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B18_0 = 0;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = Analytics_get_enabled_m451579624(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_0)
			{
				goto IL_0013;
			}
		}

IL_000a:
		{
			bool L_1 = (&___optOutStatus0)->get_analyticsEnabled_1();
			G_B3_0 = ((int32_t)(L_1));
			goto IL_0014;
		}

IL_0013:
		{
			G_B3_0 = 0;
		}

IL_0014:
		{
			Analytics_set_enabled_m1692893694(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
			goto IL_002a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001e;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t *)__exception_local);
		Exception_t * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = Analytics_get_deviceStatsEnabled_m316602018(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_003d;
			}
		}

IL_0034:
		{
			bool L_4 = (&___optOutStatus0)->get_deviceStatsEnabled_2();
			G_B8_0 = ((int32_t)(L_4));
			goto IL_003e;
		}

IL_003d:
		{
			G_B8_0 = 0;
		}

IL_003e:
		{
			Analytics_set_deviceStatsEnabled_m1800429646(NULL /*static, unused*/, (bool)G_B8_0, /*hidden argument*/NULL);
			goto IL_0054;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0048;
		throw e;
	}

CATCH_0048:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t *)__exception_local);
		Exception_t * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_0054;
	} // end catch (depth: 1)

IL_0054:
	try
	{ // begin try (depth: 1)
		{
			bool L_6 = Analytics_get_limitUserTracking_m3159512945(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (L_6)
			{
				goto IL_0067;
			}
		}

IL_005e:
		{
			bool L_7 = (&___optOutStatus0)->get_limitUserTracking_3();
			G_B13_0 = ((int32_t)(L_7));
			goto IL_0068;
		}

IL_0067:
		{
			G_B13_0 = 1;
		}

IL_0068:
		{
			Analytics_set_limitUserTracking_m2037024580(NULL /*static, unused*/, (bool)G_B13_0, /*hidden argument*/NULL);
			goto IL_007e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0072;
		throw e;
	}

CATCH_0072:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t *)__exception_local);
		Exception_t * L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_007e;
	} // end catch (depth: 1)

IL_007e:
	try
	{ // begin try (depth: 1)
		{
			bool L_9 = PerformanceReporting_get_enabled_m2429669256(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0091;
			}
		}

IL_0088:
		{
			bool L_10 = (&___optOutStatus0)->get_performanceReportingEnabled_4();
			G_B18_0 = ((int32_t)(L_10));
			goto IL_0092;
		}

IL_0091:
		{
			G_B18_0 = 0;
		}

IL_0092:
		{
			PerformanceReporting_set_enabled_m751379947(NULL /*static, unused*/, (bool)G_B18_0, /*hidden argument*/NULL);
			goto IL_00ae;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (MissingMethodException_t1274661534_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_009c;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_00a2;
		throw e;
	}

CATCH_009c:
	{ // begin catch(System.MissingMethodException)
		goto IL_00ae;
	} // end catch (depth: 1)

CATCH_00a2:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t *)__exception_local);
		Exception_t * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_00ae;
	} // end catch (depth: 1)

IL_00ae:
	{
		return;
	}
}
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::UserPostDataToJson(UnityEngine.Analytics.DataPrivacy/UserPostData)
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_UserPostDataToJson_m966437446 (RuntimeObject * __this /* static, unused */, UserPostData_t478425489  ___postData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyUtils_UserPostDataToJson_m966437446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UserPostData_t478425489  L_0 = ___postData0;
		UserPostData_t478425489  L_1 = L_0;
		RuntimeObject * L_2 = Box(UserPostData_t478425489_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = JsonUtility_ToJson_m3644929270(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::OptOutStatusToJson(UnityEngine.Analytics.DataPrivacy/OptOutStatus)
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_OptOutStatusToJson_m3940455503 (RuntimeObject * __this /* static, unused */, OptOutStatus_t3541615854  ___optOutStatus0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyUtils_OptOutStatusToJson_m3940455503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OptOutStatus_t3541615854  L_0 = ___optOutStatus0;
		OptOutStatus_t3541615854  L_1 = L_0;
		RuntimeObject * L_2 = Box(OptOutStatus_t3541615854_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = JsonUtility_ToJson_m3644929270(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Analytics.DataPrivacy/OptOutResponse UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::ParseOptOutResponse(System.String)
extern "C" IL2CPP_METHOD_ATTR OptOutResponse_t2266495071  DataPrivacyUtils_ParseOptOutResponse_m841987952 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyUtils_ParseOptOutResponse_m841987952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		OptOutResponse_t2266495071  L_1 = JsonUtility_FromJson_TisOptOutResponse_t2266495071_m4231913572(NULL /*static, unused*/, L_0, /*hidden argument*/JsonUtility_FromJson_TisOptOutResponse_t2266495071_m4231913572_RuntimeMethod_var);
		return L_1;
	}
}
// UnityEngine.Analytics.DataPrivacy/TokenData UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::ParseTokenData(System.String)
extern "C" IL2CPP_METHOD_ATTR TokenData_t2077495713  DataPrivacyUtils_ParseTokenData_m1538885353 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyUtils_ParseTokenData_m1538885353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		TokenData_t2077495713  L_1 = JsonUtility_FromJson_TisTokenData_t2077495713_m2426260624(NULL /*static, unused*/, L_0, /*hidden argument*/JsonUtility_FromJson_TisTokenData_t2077495713_m2426260624_RuntimeMethod_var);
		return L_1;
	}
}
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetUserId()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_GetUserId_m3087321347 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		String_t* L_0 = AnalyticsSessionInfo_get_userId_m3553273537(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int64 UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetSessionId()
extern "C" IL2CPP_METHOD_ATTR int64_t DataPrivacyUtils_GetSessionId_m3896160837 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		int64_t L_0 = AnalyticsSessionInfo_get_sessionId_m3617445673(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetApplicationId()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_GetApplicationId_m1000645725 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Application_get_cloudProjectId_m3678427659(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils::GetDeviceId()
extern "C" IL2CPP_METHOD_ATTR String_t* DataPrivacyUtils_GetDeviceId_m3628229477 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		String_t* L_0 = SystemInfo_get_deviceUniqueIdentifier_m3439870207(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif




// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutResponse
extern "C" void OptOutResponse_t2266495071_marshal_pinvoke(const OptOutResponse_t2266495071& unmarshaled, OptOutResponse_t2266495071_marshaled_pinvoke& marshaled)
{
	RequestData_t3101179086_marshal_pinvoke(unmarshaled.get_request_0(), marshaled.___request_0);
	OptOutStatus_t3541615854_marshal_pinvoke(unmarshaled.get_status_1(), marshaled.___status_1);
}
extern "C" void OptOutResponse_t2266495071_marshal_pinvoke_back(const OptOutResponse_t2266495071_marshaled_pinvoke& marshaled, OptOutResponse_t2266495071& unmarshaled)
{
	RequestData_t3101179086  unmarshaled_request_temp_0;
	memset(&unmarshaled_request_temp_0, 0, sizeof(unmarshaled_request_temp_0));
	RequestData_t3101179086_marshal_pinvoke_back(marshaled.___request_0, unmarshaled_request_temp_0);
	unmarshaled.set_request_0(unmarshaled_request_temp_0);
	OptOutStatus_t3541615854  unmarshaled_status_temp_1;
	memset(&unmarshaled_status_temp_1, 0, sizeof(unmarshaled_status_temp_1));
	OptOutStatus_t3541615854_marshal_pinvoke_back(marshaled.___status_1, unmarshaled_status_temp_1);
	unmarshaled.set_status_1(unmarshaled_status_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutResponse
extern "C" void OptOutResponse_t2266495071_marshal_pinvoke_cleanup(OptOutResponse_t2266495071_marshaled_pinvoke& marshaled)
{
	RequestData_t3101179086_marshal_pinvoke_cleanup(marshaled.___request_0);
	OptOutStatus_t3541615854_marshal_pinvoke_cleanup(marshaled.___status_1);
}




// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutResponse
extern "C" void OptOutResponse_t2266495071_marshal_com(const OptOutResponse_t2266495071& unmarshaled, OptOutResponse_t2266495071_marshaled_com& marshaled)
{
	RequestData_t3101179086_marshal_com(unmarshaled.get_request_0(), marshaled.___request_0);
	OptOutStatus_t3541615854_marshal_com(unmarshaled.get_status_1(), marshaled.___status_1);
}
extern "C" void OptOutResponse_t2266495071_marshal_com_back(const OptOutResponse_t2266495071_marshaled_com& marshaled, OptOutResponse_t2266495071& unmarshaled)
{
	RequestData_t3101179086  unmarshaled_request_temp_0;
	memset(&unmarshaled_request_temp_0, 0, sizeof(unmarshaled_request_temp_0));
	RequestData_t3101179086_marshal_com_back(marshaled.___request_0, unmarshaled_request_temp_0);
	unmarshaled.set_request_0(unmarshaled_request_temp_0);
	OptOutStatus_t3541615854  unmarshaled_status_temp_1;
	memset(&unmarshaled_status_temp_1, 0, sizeof(unmarshaled_status_temp_1));
	OptOutStatus_t3541615854_marshal_com_back(marshaled.___status_1, unmarshaled_status_temp_1);
	unmarshaled.set_status_1(unmarshaled_status_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutResponse
extern "C" void OptOutResponse_t2266495071_marshal_com_cleanup(OptOutResponse_t2266495071_marshaled_com& marshaled)
{
	RequestData_t3101179086_marshal_com_cleanup(marshaled.___request_0);
	OptOutStatus_t3541615854_marshal_com_cleanup(marshaled.___status_1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutStatus
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke(const OptOutStatus_t3541615854& unmarshaled, OptOutStatus_t3541615854_marshaled_pinvoke& marshaled)
{
	marshaled.___optOut_0 = static_cast<int32_t>(unmarshaled.get_optOut_0());
	marshaled.___analyticsEnabled_1 = static_cast<int32_t>(unmarshaled.get_analyticsEnabled_1());
	marshaled.___deviceStatsEnabled_2 = static_cast<int32_t>(unmarshaled.get_deviceStatsEnabled_2());
	marshaled.___limitUserTracking_3 = static_cast<int32_t>(unmarshaled.get_limitUserTracking_3());
	marshaled.___performanceReportingEnabled_4 = static_cast<int32_t>(unmarshaled.get_performanceReportingEnabled_4());
}
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke_back(const OptOutStatus_t3541615854_marshaled_pinvoke& marshaled, OptOutStatus_t3541615854& unmarshaled)
{
	bool unmarshaled_optOut_temp_0 = false;
	unmarshaled_optOut_temp_0 = static_cast<bool>(marshaled.___optOut_0);
	unmarshaled.set_optOut_0(unmarshaled_optOut_temp_0);
	bool unmarshaled_analyticsEnabled_temp_1 = false;
	unmarshaled_analyticsEnabled_temp_1 = static_cast<bool>(marshaled.___analyticsEnabled_1);
	unmarshaled.set_analyticsEnabled_1(unmarshaled_analyticsEnabled_temp_1);
	bool unmarshaled_deviceStatsEnabled_temp_2 = false;
	unmarshaled_deviceStatsEnabled_temp_2 = static_cast<bool>(marshaled.___deviceStatsEnabled_2);
	unmarshaled.set_deviceStatsEnabled_2(unmarshaled_deviceStatsEnabled_temp_2);
	bool unmarshaled_limitUserTracking_temp_3 = false;
	unmarshaled_limitUserTracking_temp_3 = static_cast<bool>(marshaled.___limitUserTracking_3);
	unmarshaled.set_limitUserTracking_3(unmarshaled_limitUserTracking_temp_3);
	bool unmarshaled_performanceReportingEnabled_temp_4 = false;
	unmarshaled_performanceReportingEnabled_temp_4 = static_cast<bool>(marshaled.___performanceReportingEnabled_4);
	unmarshaled.set_performanceReportingEnabled_4(unmarshaled_performanceReportingEnabled_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutStatus
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke_cleanup(OptOutStatus_t3541615854_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutStatus
extern "C" void OptOutStatus_t3541615854_marshal_com(const OptOutStatus_t3541615854& unmarshaled, OptOutStatus_t3541615854_marshaled_com& marshaled)
{
	marshaled.___optOut_0 = static_cast<int32_t>(unmarshaled.get_optOut_0());
	marshaled.___analyticsEnabled_1 = static_cast<int32_t>(unmarshaled.get_analyticsEnabled_1());
	marshaled.___deviceStatsEnabled_2 = static_cast<int32_t>(unmarshaled.get_deviceStatsEnabled_2());
	marshaled.___limitUserTracking_3 = static_cast<int32_t>(unmarshaled.get_limitUserTracking_3());
	marshaled.___performanceReportingEnabled_4 = static_cast<int32_t>(unmarshaled.get_performanceReportingEnabled_4());
}
extern "C" void OptOutStatus_t3541615854_marshal_com_back(const OptOutStatus_t3541615854_marshaled_com& marshaled, OptOutStatus_t3541615854& unmarshaled)
{
	bool unmarshaled_optOut_temp_0 = false;
	unmarshaled_optOut_temp_0 = static_cast<bool>(marshaled.___optOut_0);
	unmarshaled.set_optOut_0(unmarshaled_optOut_temp_0);
	bool unmarshaled_analyticsEnabled_temp_1 = false;
	unmarshaled_analyticsEnabled_temp_1 = static_cast<bool>(marshaled.___analyticsEnabled_1);
	unmarshaled.set_analyticsEnabled_1(unmarshaled_analyticsEnabled_temp_1);
	bool unmarshaled_deviceStatsEnabled_temp_2 = false;
	unmarshaled_deviceStatsEnabled_temp_2 = static_cast<bool>(marshaled.___deviceStatsEnabled_2);
	unmarshaled.set_deviceStatsEnabled_2(unmarshaled_deviceStatsEnabled_temp_2);
	bool unmarshaled_limitUserTracking_temp_3 = false;
	unmarshaled_limitUserTracking_temp_3 = static_cast<bool>(marshaled.___limitUserTracking_3);
	unmarshaled.set_limitUserTracking_3(unmarshaled_limitUserTracking_temp_3);
	bool unmarshaled_performanceReportingEnabled_temp_4 = false;
	unmarshaled_performanceReportingEnabled_temp_4 = static_cast<bool>(marshaled.___performanceReportingEnabled_4);
	unmarshaled.set_performanceReportingEnabled_4(unmarshaled_performanceReportingEnabled_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/OptOutStatus
extern "C" void OptOutStatus_t3541615854_marshal_com_cleanup(OptOutStatus_t3541615854_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/RequestData
extern "C" void RequestData_t3101179086_marshal_pinvoke(const RequestData_t3101179086& unmarshaled, RequestData_t3101179086_marshaled_pinvoke& marshaled)
{
	marshaled.___date_0 = il2cpp_codegen_marshal_string(unmarshaled.get_date_0());
}
extern "C" void RequestData_t3101179086_marshal_pinvoke_back(const RequestData_t3101179086_marshaled_pinvoke& marshaled, RequestData_t3101179086& unmarshaled)
{
	unmarshaled.set_date_0(il2cpp_codegen_marshal_string_result(marshaled.___date_0));
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/RequestData
extern "C" void RequestData_t3101179086_marshal_pinvoke_cleanup(RequestData_t3101179086_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___date_0);
	marshaled.___date_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/RequestData
extern "C" void RequestData_t3101179086_marshal_com(const RequestData_t3101179086& unmarshaled, RequestData_t3101179086_marshaled_com& marshaled)
{
	marshaled.___date_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_date_0());
}
extern "C" void RequestData_t3101179086_marshal_com_back(const RequestData_t3101179086_marshaled_com& marshaled, RequestData_t3101179086& unmarshaled)
{
	unmarshaled.set_date_0(il2cpp_codegen_marshal_bstring_result(marshaled.___date_0));
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/RequestData
extern "C" void RequestData_t3101179086_marshal_com_cleanup(RequestData_t3101179086_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___date_0);
	marshaled.___date_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/TokenData
extern "C" void TokenData_t2077495713_marshal_pinvoke(const TokenData_t2077495713& unmarshaled, TokenData_t2077495713_marshaled_pinvoke& marshaled)
{
	marshaled.___url_0 = il2cpp_codegen_marshal_string(unmarshaled.get_url_0());
	marshaled.___token_1 = il2cpp_codegen_marshal_string(unmarshaled.get_token_1());
}
extern "C" void TokenData_t2077495713_marshal_pinvoke_back(const TokenData_t2077495713_marshaled_pinvoke& marshaled, TokenData_t2077495713& unmarshaled)
{
	unmarshaled.set_url_0(il2cpp_codegen_marshal_string_result(marshaled.___url_0));
	unmarshaled.set_token_1(il2cpp_codegen_marshal_string_result(marshaled.___token_1));
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/TokenData
extern "C" void TokenData_t2077495713_marshal_pinvoke_cleanup(TokenData_t2077495713_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___url_0);
	marshaled.___url_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___token_1);
	marshaled.___token_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/TokenData
extern "C" void TokenData_t2077495713_marshal_com(const TokenData_t2077495713& unmarshaled, TokenData_t2077495713_marshaled_com& marshaled)
{
	marshaled.___url_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_url_0());
	marshaled.___token_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_token_1());
}
extern "C" void TokenData_t2077495713_marshal_com_back(const TokenData_t2077495713_marshaled_com& marshaled, TokenData_t2077495713& unmarshaled)
{
	unmarshaled.set_url_0(il2cpp_codegen_marshal_bstring_result(marshaled.___url_0));
	unmarshaled.set_token_1(il2cpp_codegen_marshal_bstring_result(marshaled.___token_1));
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/TokenData
extern "C" void TokenData_t2077495713_marshal_com_cleanup(TokenData_t2077495713_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___url_0);
	marshaled.___url_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___token_1);
	marshaled.___token_1 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/UserPostData
extern "C" void UserPostData_t478425489_marshal_pinvoke(const UserPostData_t478425489& unmarshaled, UserPostData_t478425489_marshaled_pinvoke& marshaled)
{
	marshaled.___appid_0 = il2cpp_codegen_marshal_string(unmarshaled.get_appid_0());
	marshaled.___userid_1 = il2cpp_codegen_marshal_string(unmarshaled.get_userid_1());
	marshaled.___sessionid_2 = unmarshaled.get_sessionid_2();
	marshaled.___platform_3 = il2cpp_codegen_marshal_string(unmarshaled.get_platform_3());
	marshaled.___platformid_4 = unmarshaled.get_platformid_4();
	marshaled.___sdk_ver_5 = il2cpp_codegen_marshal_string(unmarshaled.get_sdk_ver_5());
	marshaled.___debug_device_6 = static_cast<int32_t>(unmarshaled.get_debug_device_6());
	marshaled.___deviceid_7 = il2cpp_codegen_marshal_string(unmarshaled.get_deviceid_7());
	marshaled.___plugin_ver_8 = il2cpp_codegen_marshal_string(unmarshaled.get_plugin_ver_8());
}
extern "C" void UserPostData_t478425489_marshal_pinvoke_back(const UserPostData_t478425489_marshaled_pinvoke& marshaled, UserPostData_t478425489& unmarshaled)
{
	unmarshaled.set_appid_0(il2cpp_codegen_marshal_string_result(marshaled.___appid_0));
	unmarshaled.set_userid_1(il2cpp_codegen_marshal_string_result(marshaled.___userid_1));
	int64_t unmarshaled_sessionid_temp_2 = 0;
	unmarshaled_sessionid_temp_2 = marshaled.___sessionid_2;
	unmarshaled.set_sessionid_2(unmarshaled_sessionid_temp_2);
	unmarshaled.set_platform_3(il2cpp_codegen_marshal_string_result(marshaled.___platform_3));
	uint32_t unmarshaled_platformid_temp_4 = 0;
	unmarshaled_platformid_temp_4 = marshaled.___platformid_4;
	unmarshaled.set_platformid_4(unmarshaled_platformid_temp_4);
	unmarshaled.set_sdk_ver_5(il2cpp_codegen_marshal_string_result(marshaled.___sdk_ver_5));
	bool unmarshaled_debug_device_temp_6 = false;
	unmarshaled_debug_device_temp_6 = static_cast<bool>(marshaled.___debug_device_6);
	unmarshaled.set_debug_device_6(unmarshaled_debug_device_temp_6);
	unmarshaled.set_deviceid_7(il2cpp_codegen_marshal_string_result(marshaled.___deviceid_7));
	unmarshaled.set_plugin_ver_8(il2cpp_codegen_marshal_string_result(marshaled.___plugin_ver_8));
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/UserPostData
extern "C" void UserPostData_t478425489_marshal_pinvoke_cleanup(UserPostData_t478425489_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___appid_0);
	marshaled.___appid_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___userid_1);
	marshaled.___userid_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___platform_3);
	marshaled.___platform_3 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___sdk_ver_5);
	marshaled.___sdk_ver_5 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___deviceid_7);
	marshaled.___deviceid_7 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___plugin_ver_8);
	marshaled.___plugin_ver_8 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.Analytics.DataPrivacy/UserPostData
extern "C" void UserPostData_t478425489_marshal_com(const UserPostData_t478425489& unmarshaled, UserPostData_t478425489_marshaled_com& marshaled)
{
	marshaled.___appid_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_appid_0());
	marshaled.___userid_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_userid_1());
	marshaled.___sessionid_2 = unmarshaled.get_sessionid_2();
	marshaled.___platform_3 = il2cpp_codegen_marshal_bstring(unmarshaled.get_platform_3());
	marshaled.___platformid_4 = unmarshaled.get_platformid_4();
	marshaled.___sdk_ver_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_sdk_ver_5());
	marshaled.___debug_device_6 = static_cast<int32_t>(unmarshaled.get_debug_device_6());
	marshaled.___deviceid_7 = il2cpp_codegen_marshal_bstring(unmarshaled.get_deviceid_7());
	marshaled.___plugin_ver_8 = il2cpp_codegen_marshal_bstring(unmarshaled.get_plugin_ver_8());
}
extern "C" void UserPostData_t478425489_marshal_com_back(const UserPostData_t478425489_marshaled_com& marshaled, UserPostData_t478425489& unmarshaled)
{
	unmarshaled.set_appid_0(il2cpp_codegen_marshal_bstring_result(marshaled.___appid_0));
	unmarshaled.set_userid_1(il2cpp_codegen_marshal_bstring_result(marshaled.___userid_1));
	int64_t unmarshaled_sessionid_temp_2 = 0;
	unmarshaled_sessionid_temp_2 = marshaled.___sessionid_2;
	unmarshaled.set_sessionid_2(unmarshaled_sessionid_temp_2);
	unmarshaled.set_platform_3(il2cpp_codegen_marshal_bstring_result(marshaled.___platform_3));
	uint32_t unmarshaled_platformid_temp_4 = 0;
	unmarshaled_platformid_temp_4 = marshaled.___platformid_4;
	unmarshaled.set_platformid_4(unmarshaled_platformid_temp_4);
	unmarshaled.set_sdk_ver_5(il2cpp_codegen_marshal_bstring_result(marshaled.___sdk_ver_5));
	bool unmarshaled_debug_device_temp_6 = false;
	unmarshaled_debug_device_temp_6 = static_cast<bool>(marshaled.___debug_device_6);
	unmarshaled.set_debug_device_6(unmarshaled_debug_device_temp_6);
	unmarshaled.set_deviceid_7(il2cpp_codegen_marshal_bstring_result(marshaled.___deviceid_7));
	unmarshaled.set_plugin_ver_8(il2cpp_codegen_marshal_bstring_result(marshaled.___plugin_ver_8));
}
// Conversion method for clean up from marshalling of: UnityEngine.Analytics.DataPrivacy/UserPostData
extern "C" void UserPostData_t478425489_marshal_com_cleanup(UserPostData_t478425489_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___appid_0);
	marshaled.___appid_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___userid_1);
	marshaled.___userid_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___platform_3);
	marshaled.___platform_3 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___sdk_ver_5);
	marshaled.___sdk_ver_5 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___deviceid_7);
	marshaled.___deviceid_7 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___plugin_ver_8);
	marshaled.___plugin_ver_8 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Analytics.DataPrivacyButton::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyButton__ctor_m2140043654 (DataPrivacyButton_t2259119369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyButton__ctor_m2140043654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button__ctor_m2784091109(__this, /*hidden argument*/NULL);
		ButtonClickedEvent_t48803504 * L_0 = Button_get_onClick_m2332132945(__this, /*hidden argument*/NULL);
		intptr_t L_1 = (intptr_t)DataPrivacyButton_OpenDataPrivacyUrl_m47974444_RuntimeMethod_var;
		UnityAction_t3245792599 * L_2 = (UnityAction_t3245792599 *)il2cpp_codegen_object_new(UnityAction_t3245792599_il2cpp_TypeInfo_var);
		UnityAction__ctor_m772160306(L_2, __this, (intptr_t)L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_AddListener_m2276267359(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacyButton::OnFailure(System.String)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyButton_OnFailure_m1914851203 (DataPrivacyButton_t2259119369 * __this, String_t* ___reason0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyButton_OnFailure_m1914851203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Selectable_set_interactable_m3105888815(__this, (bool)1, /*hidden argument*/NULL);
		String_t* L_0 = ___reason0;
		String_t* L_1 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1554920073, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacyButton::OpenUrl(System.String)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyButton_OpenUrl_m1642407137 (DataPrivacyButton_t2259119369 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	{
		Selectable_set_interactable_m3105888815(__this, (bool)1, /*hidden argument*/NULL);
		__this->set_urlOpened_19((bool)1);
		String_t* L_0 = ___url0;
		Application_OpenURL_m738341736(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacyButton::OpenDataPrivacyUrl()
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyButton_OpenDataPrivacyUrl_m47974444 (DataPrivacyButton_t2259119369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataPrivacyButton_OpenDataPrivacyUrl_m47974444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Selectable_set_interactable_m3105888815(__this, (bool)0, /*hidden argument*/NULL);
		intptr_t L_0 = (intptr_t)DataPrivacyButton_OpenUrl_m1642407137_RuntimeMethod_var;
		Action_1_t2019918284 * L_1 = (Action_1_t2019918284 *)il2cpp_codegen_object_new(Action_1_t2019918284_il2cpp_TypeInfo_var);
		Action_1__ctor_m1347592571(L_1, __this, (intptr_t)L_0, /*hidden argument*/Action_1__ctor_m1347592571_RuntimeMethod_var);
		intptr_t L_2 = (intptr_t)DataPrivacyButton_OnFailure_m1914851203_RuntimeMethod_var;
		Action_1_t2019918284 * L_3 = (Action_1_t2019918284 *)il2cpp_codegen_object_new(Action_1_t2019918284_il2cpp_TypeInfo_var);
		Action_1__ctor_m1347592571(L_3, __this, (intptr_t)L_2, /*hidden argument*/Action_1__ctor_m1347592571_RuntimeMethod_var);
		DataPrivacy_FetchPrivacyUrl_m4052048955(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.DataPrivacyButton::OnApplicationFocus(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void DataPrivacyButton_OnApplicationFocus_m2601850289 (DataPrivacyButton_t2259119369 * __this, bool ___hasFocus0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___hasFocus0;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		bool L_1 = __this->get_urlOpened_19();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_urlOpened_19((bool)0);
		DataPrivacy_FetchOptOutStatus_m2364404913(NULL /*static, unused*/, (Action_1_t269755560 *)NULL, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
