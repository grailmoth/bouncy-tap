﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Security.Cryptography.DSAManaged
struct DSAManaged_t2800260182;
// Mono.Security.Cryptography.KeyPairPersistence
struct KeyPairPersistence_t2094547461;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t489243024;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Concurrent.ConcurrentDictionary`2<System.Runtime.Serialization.MemberHolder,System.Reflection.MemberInfo[]>
struct ConcurrentDictionary_2_t2965367907;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t3349700990;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.EventHandler`1<System.Runtime.Serialization.SafeSerializationEventArgs>
struct EventHandler_1_t2246946069;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.RuntimeFieldInfo
struct RuntimeFieldInfo_t4270975280;
// System.Runtime.Serialization.DeserializationEventHandler
struct DeserializationEventHandler_t1473997819;
// System.Runtime.Serialization.FixupHolderList
struct FixupHolderList_t3799028159;
// System.Runtime.Serialization.FixupHolder[]
struct FixupHolderU5BU5D_t415823611;
// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[]
struct BinaryTypeEnumU5BU5D_t4284693123;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t2171992254;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t2624761601;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t3040401154;
// System.Runtime.Serialization.LongList
struct LongList_t2258305620;
// System.Runtime.Serialization.ObjectHolderList
struct ObjectHolderList_t2007846727;
// System.Runtime.Serialization.ObjectHolder[]
struct ObjectHolderU5BU5D_t663564126;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Runtime.Serialization.SerializationEventHandler
struct SerializationEventHandler_t2446424469;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Runtime.Serialization.TypeLoadExceptionHolder
struct TypeLoadExceptionHolder_t2983813904;
// System.Runtime.Serialization.ValueTypeFixupInfo
struct ValueTypeFixupInfo_t1444618334;
// System.RuntimeType
struct RuntimeType_t3636489352;
// System.Security.Cryptography.CspParameters
struct CspParameters_t239852639;
// System.Security.Cryptography.DESTransform
struct DESTransform_t4088905499;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t722666473;
// System.Security.Cryptography.RNGCryptoServiceProvider
struct RNGCryptoServiceProvider_t3397414743;
// System.Security.Cryptography.RSA
struct RSA_t2385438082;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Security.Cryptography.SHA1Internal
struct SHA1Internal_t3251293021;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t4254223087;
// System.Security.Cryptography.X509Certificates.INativeCertificateHelper
struct INativeCertificateHelper_t1667236488;
// System.Security.Cryptography.X509Certificates.X509CertificateImpl
struct X509CertificateImpl_t2851385038;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef FIXUPHOLDER_T3112688910_H
#define FIXUPHOLDER_T3112688910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FixupHolder
struct  FixupHolder_t3112688910  : public RuntimeObject
{
public:
	// System.Int64 System.Runtime.Serialization.FixupHolder::m_id
	int64_t ___m_id_0;
	// System.Object System.Runtime.Serialization.FixupHolder::m_fixupInfo
	RuntimeObject * ___m_fixupInfo_1;
	// System.Int32 System.Runtime.Serialization.FixupHolder::m_fixupType
	int32_t ___m_fixupType_2;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(FixupHolder_t3112688910, ___m_id_0)); }
	inline int64_t get_m_id_0() const { return ___m_id_0; }
	inline int64_t* get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(int64_t value)
	{
		___m_id_0 = value;
	}

	inline static int32_t get_offset_of_m_fixupInfo_1() { return static_cast<int32_t>(offsetof(FixupHolder_t3112688910, ___m_fixupInfo_1)); }
	inline RuntimeObject * get_m_fixupInfo_1() const { return ___m_fixupInfo_1; }
	inline RuntimeObject ** get_address_of_m_fixupInfo_1() { return &___m_fixupInfo_1; }
	inline void set_m_fixupInfo_1(RuntimeObject * value)
	{
		___m_fixupInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fixupInfo_1), value);
	}

	inline static int32_t get_offset_of_m_fixupType_2() { return static_cast<int32_t>(offsetof(FixupHolder_t3112688910, ___m_fixupType_2)); }
	inline int32_t get_m_fixupType_2() const { return ___m_fixupType_2; }
	inline int32_t* get_address_of_m_fixupType_2() { return &___m_fixupType_2; }
	inline void set_m_fixupType_2(int32_t value)
	{
		___m_fixupType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXUPHOLDER_T3112688910_H
#ifndef FIXUPHOLDERLIST_T3799028159_H
#define FIXUPHOLDERLIST_T3799028159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FixupHolderList
struct  FixupHolderList_t3799028159  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.FixupHolder[] System.Runtime.Serialization.FixupHolderList::m_values
	FixupHolderU5BU5D_t415823611* ___m_values_0;
	// System.Int32 System.Runtime.Serialization.FixupHolderList::m_count
	int32_t ___m_count_1;

public:
	inline static int32_t get_offset_of_m_values_0() { return static_cast<int32_t>(offsetof(FixupHolderList_t3799028159, ___m_values_0)); }
	inline FixupHolderU5BU5D_t415823611* get_m_values_0() const { return ___m_values_0; }
	inline FixupHolderU5BU5D_t415823611** get_address_of_m_values_0() { return &___m_values_0; }
	inline void set_m_values_0(FixupHolderU5BU5D_t415823611* value)
	{
		___m_values_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_values_0), value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(FixupHolderList_t3799028159, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXUPHOLDERLIST_T3799028159_H
#ifndef FORMATTERCONVERTER_T2760117746_H
#define FORMATTERCONVERTER_T2760117746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FormatterConverter
struct  FormatterConverter_t2760117746  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERCONVERTER_T2760117746_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T3680548263_H
#define U3CU3EC__DISPLAYCLASS9_0_T3680548263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FormatterServices/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t3680548263  : public RuntimeObject
{
public:
	// System.Type System.Runtime.Serialization.FormatterServices/<>c__DisplayClass9_0::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t3680548263, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T3680548263_H
#ifndef BINARYASSEMBLY_T798593476_H
#define BINARYASSEMBLY_T798593476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryAssembly
struct  BinaryAssembly_t798593476  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryAssembly::assemId
	int32_t ___assemId_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryAssembly::assemblyString
	String_t* ___assemblyString_1;

public:
	inline static int32_t get_offset_of_assemId_0() { return static_cast<int32_t>(offsetof(BinaryAssembly_t798593476, ___assemId_0)); }
	inline int32_t get_assemId_0() const { return ___assemId_0; }
	inline int32_t* get_address_of_assemId_0() { return &___assemId_0; }
	inline void set_assemId_0(int32_t value)
	{
		___assemId_0 = value;
	}

	inline static int32_t get_offset_of_assemblyString_1() { return static_cast<int32_t>(offsetof(BinaryAssembly_t798593476, ___assemblyString_1)); }
	inline String_t* get_assemblyString_1() const { return ___assemblyString_1; }
	inline String_t** get_address_of_assemblyString_1() { return &___assemblyString_1; }
	inline void set_assemblyString_1(String_t* value)
	{
		___assemblyString_1 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYASSEMBLY_T798593476_H
#ifndef BINARYASSEMBLYINFO_T1476540191_H
#define BINARYASSEMBLYINFO_T1476540191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo
struct  BinaryAssemblyInfo_t1476540191  : public RuntimeObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo::assemblyString
	String_t* ___assemblyString_0;
	// System.Reflection.Assembly System.Runtime.Serialization.Formatters.Binary.BinaryAssemblyInfo::assembly
	Assembly_t * ___assembly_1;

public:
	inline static int32_t get_offset_of_assemblyString_0() { return static_cast<int32_t>(offsetof(BinaryAssemblyInfo_t1476540191, ___assemblyString_0)); }
	inline String_t* get_assemblyString_0() const { return ___assemblyString_0; }
	inline String_t** get_address_of_assemblyString_0() { return &___assemblyString_0; }
	inline void set_assemblyString_0(String_t* value)
	{
		___assemblyString_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyString_0), value);
	}

	inline static int32_t get_offset_of_assembly_1() { return static_cast<int32_t>(offsetof(BinaryAssemblyInfo_t1476540191, ___assembly_1)); }
	inline Assembly_t * get_assembly_1() const { return ___assembly_1; }
	inline Assembly_t ** get_address_of_assembly_1() { return &___assembly_1; }
	inline void set_assembly_1(Assembly_t * value)
	{
		___assembly_1 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYASSEMBLYINFO_T1476540191_H
#ifndef BINARYCONVERTER_T4078671247_H
#define BINARYCONVERTER_T4078671247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryConverter
struct  BinaryConverter_t4078671247  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T4078671247_H
#ifndef BINARYCROSSAPPDOMAINASSEMBLY_T2006046530_H
#define BINARYCROSSAPPDOMAINASSEMBLY_T2006046530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainAssembly
struct  BinaryCrossAppDomainAssembly_t2006046530  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainAssembly::assemId
	int32_t ___assemId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainAssembly::assemblyIndex
	int32_t ___assemblyIndex_1;

public:
	inline static int32_t get_offset_of_assemId_0() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainAssembly_t2006046530, ___assemId_0)); }
	inline int32_t get_assemId_0() const { return ___assemId_0; }
	inline int32_t* get_address_of_assemId_0() { return &___assemId_0; }
	inline void set_assemId_0(int32_t value)
	{
		___assemId_0 = value;
	}

	inline static int32_t get_offset_of_assemblyIndex_1() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainAssembly_t2006046530, ___assemblyIndex_1)); }
	inline int32_t get_assemblyIndex_1() const { return ___assemblyIndex_1; }
	inline int32_t* get_address_of_assemblyIndex_1() { return &___assemblyIndex_1; }
	inline void set_assemblyIndex_1(int32_t value)
	{
		___assemblyIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCROSSAPPDOMAINASSEMBLY_T2006046530_H
#ifndef BINARYCROSSAPPDOMAINMAP_T2942608216_H
#define BINARYCROSSAPPDOMAINMAP_T2942608216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainMap
struct  BinaryCrossAppDomainMap_t2942608216  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainMap::crossAppDomainArrayIndex
	int32_t ___crossAppDomainArrayIndex_0;

public:
	inline static int32_t get_offset_of_crossAppDomainArrayIndex_0() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainMap_t2942608216, ___crossAppDomainArrayIndex_0)); }
	inline int32_t get_crossAppDomainArrayIndex_0() const { return ___crossAppDomainArrayIndex_0; }
	inline int32_t* get_address_of_crossAppDomainArrayIndex_0() { return &___crossAppDomainArrayIndex_0; }
	inline void set_crossAppDomainArrayIndex_0(int32_t value)
	{
		___crossAppDomainArrayIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCROSSAPPDOMAINMAP_T2942608216_H
#ifndef BINARYCROSSAPPDOMAINSTRING_T3774953123_H
#define BINARYCROSSAPPDOMAINSTRING_T3774953123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainString
struct  BinaryCrossAppDomainString_t3774953123  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainString::objectId
	int32_t ___objectId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryCrossAppDomainString::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainString_t3774953123, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BinaryCrossAppDomainString_t3774953123, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCROSSAPPDOMAINSTRING_T3774953123_H
#ifndef BINARYOBJECT_T4088053983_H
#define BINARYOBJECT_T4088053983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObject
struct  BinaryObject_t4088053983  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObject::objectId
	int32_t ___objectId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObject::mapId
	int32_t ___mapId_1;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(BinaryObject_t4088053983, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_mapId_1() { return static_cast<int32_t>(offsetof(BinaryObject_t4088053983, ___mapId_1)); }
	inline int32_t get_mapId_1() const { return ___mapId_1; }
	inline int32_t* get_address_of_mapId_1() { return &___mapId_1; }
	inline void set_mapId_1(int32_t value)
	{
		___mapId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOBJECT_T4088053983_H
#ifndef BINARYOBJECTSTRING_T3941408528_H
#define BINARYOBJECTSTRING_T3941408528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObjectString
struct  BinaryObjectString_t3941408528  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectString::objectId
	int32_t ___objectId_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryObjectString::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(BinaryObjectString_t3941408528, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(BinaryObjectString_t3941408528, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOBJECTSTRING_T3941408528_H
#ifndef IOUTIL_T3677430991_H
#define IOUTIL_T3677430991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.IOUtil
struct  IOUtil_t3677430991  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOUTIL_T3677430991_H
#ifndef MEMBERREFERENCE_T3613131682_H
#define MEMBERREFERENCE_T3613131682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.MemberReference
struct  MemberReference_t3613131682  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.MemberReference::idRef
	int32_t ___idRef_0;

public:
	inline static int32_t get_offset_of_idRef_0() { return static_cast<int32_t>(offsetof(MemberReference_t3613131682, ___idRef_0)); }
	inline int32_t get_idRef_0() const { return ___idRef_0; }
	inline int32_t* get_address_of_idRef_0() { return &___idRef_0; }
	inline void set_idRef_0(int32_t value)
	{
		___idRef_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERREFERENCE_T3613131682_H
#ifndef MESSAGEEND_T4164135406_H
#define MESSAGEEND_T4164135406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.MessageEnd
struct  MessageEnd_t4164135406  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEND_T4164135406_H
#ifndef OBJECTNULL_T728940006_H
#define OBJECTNULL_T728940006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.ObjectNull
struct  ObjectNull_t728940006  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectNull::nullCount
	int32_t ___nullCount_0;

public:
	inline static int32_t get_offset_of_nullCount_0() { return static_cast<int32_t>(offsetof(ObjectNull_t728940006, ___nullCount_0)); }
	inline int32_t get_nullCount_0() const { return ___nullCount_0; }
	inline int32_t* get_address_of_nullCount_0() { return &___nullCount_0; }
	inline void set_nullCount_0(int32_t value)
	{
		___nullCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTNULL_T728940006_H
#ifndef LONGLIST_T2258305620_H
#define LONGLIST_T2258305620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.LongList
struct  LongList_t2258305620  : public RuntimeObject
{
public:
	// System.Int64[] System.Runtime.Serialization.LongList::m_values
	Int64U5BU5D_t2559172825* ___m_values_0;
	// System.Int32 System.Runtime.Serialization.LongList::m_count
	int32_t ___m_count_1;
	// System.Int32 System.Runtime.Serialization.LongList::m_totalItems
	int32_t ___m_totalItems_2;
	// System.Int32 System.Runtime.Serialization.LongList::m_currentItem
	int32_t ___m_currentItem_3;

public:
	inline static int32_t get_offset_of_m_values_0() { return static_cast<int32_t>(offsetof(LongList_t2258305620, ___m_values_0)); }
	inline Int64U5BU5D_t2559172825* get_m_values_0() const { return ___m_values_0; }
	inline Int64U5BU5D_t2559172825** get_address_of_m_values_0() { return &___m_values_0; }
	inline void set_m_values_0(Int64U5BU5D_t2559172825* value)
	{
		___m_values_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_values_0), value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(LongList_t2258305620, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}

	inline static int32_t get_offset_of_m_totalItems_2() { return static_cast<int32_t>(offsetof(LongList_t2258305620, ___m_totalItems_2)); }
	inline int32_t get_m_totalItems_2() const { return ___m_totalItems_2; }
	inline int32_t* get_address_of_m_totalItems_2() { return &___m_totalItems_2; }
	inline void set_m_totalItems_2(int32_t value)
	{
		___m_totalItems_2 = value;
	}

	inline static int32_t get_offset_of_m_currentItem_3() { return static_cast<int32_t>(offsetof(LongList_t2258305620, ___m_currentItem_3)); }
	inline int32_t get_m_currentItem_3() const { return ___m_currentItem_3; }
	inline int32_t* get_address_of_m_currentItem_3() { return &___m_currentItem_3; }
	inline void set_m_currentItem_3(int32_t value)
	{
		___m_currentItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGLIST_T2258305620_H
#ifndef OBJECTHOLDER_T2801344551_H
#define OBJECTHOLDER_T2801344551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectHolder
struct  ObjectHolder_t2801344551  : public RuntimeObject
{
public:
	// System.Object System.Runtime.Serialization.ObjectHolder::m_object
	RuntimeObject * ___m_object_0;
	// System.Int64 System.Runtime.Serialization.ObjectHolder::m_id
	int64_t ___m_id_1;
	// System.Int32 System.Runtime.Serialization.ObjectHolder::m_missingElementsRemaining
	int32_t ___m_missingElementsRemaining_2;
	// System.Int32 System.Runtime.Serialization.ObjectHolder::m_missingDecendents
	int32_t ___m_missingDecendents_3;
	// System.Runtime.Serialization.SerializationInfo System.Runtime.Serialization.ObjectHolder::m_serInfo
	SerializationInfo_t950877179 * ___m_serInfo_4;
	// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.ObjectHolder::m_surrogate
	RuntimeObject* ___m_surrogate_5;
	// System.Runtime.Serialization.FixupHolderList System.Runtime.Serialization.ObjectHolder::m_missingElements
	FixupHolderList_t3799028159 * ___m_missingElements_6;
	// System.Runtime.Serialization.LongList System.Runtime.Serialization.ObjectHolder::m_dependentObjects
	LongList_t2258305620 * ___m_dependentObjects_7;
	// System.Runtime.Serialization.ObjectHolder System.Runtime.Serialization.ObjectHolder::m_next
	ObjectHolder_t2801344551 * ___m_next_8;
	// System.Int32 System.Runtime.Serialization.ObjectHolder::m_flags
	int32_t ___m_flags_9;
	// System.Boolean System.Runtime.Serialization.ObjectHolder::m_markForFixupWhenAvailable
	bool ___m_markForFixupWhenAvailable_10;
	// System.Runtime.Serialization.ValueTypeFixupInfo System.Runtime.Serialization.ObjectHolder::m_valueFixup
	ValueTypeFixupInfo_t1444618334 * ___m_valueFixup_11;
	// System.Runtime.Serialization.TypeLoadExceptionHolder System.Runtime.Serialization.ObjectHolder::m_typeLoad
	TypeLoadExceptionHolder_t2983813904 * ___m_typeLoad_12;
	// System.Boolean System.Runtime.Serialization.ObjectHolder::m_reachable
	bool ___m_reachable_13;

public:
	inline static int32_t get_offset_of_m_object_0() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_object_0)); }
	inline RuntimeObject * get_m_object_0() const { return ___m_object_0; }
	inline RuntimeObject ** get_address_of_m_object_0() { return &___m_object_0; }
	inline void set_m_object_0(RuntimeObject * value)
	{
		___m_object_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_object_0), value);
	}

	inline static int32_t get_offset_of_m_id_1() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_id_1)); }
	inline int64_t get_m_id_1() const { return ___m_id_1; }
	inline int64_t* get_address_of_m_id_1() { return &___m_id_1; }
	inline void set_m_id_1(int64_t value)
	{
		___m_id_1 = value;
	}

	inline static int32_t get_offset_of_m_missingElementsRemaining_2() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_missingElementsRemaining_2)); }
	inline int32_t get_m_missingElementsRemaining_2() const { return ___m_missingElementsRemaining_2; }
	inline int32_t* get_address_of_m_missingElementsRemaining_2() { return &___m_missingElementsRemaining_2; }
	inline void set_m_missingElementsRemaining_2(int32_t value)
	{
		___m_missingElementsRemaining_2 = value;
	}

	inline static int32_t get_offset_of_m_missingDecendents_3() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_missingDecendents_3)); }
	inline int32_t get_m_missingDecendents_3() const { return ___m_missingDecendents_3; }
	inline int32_t* get_address_of_m_missingDecendents_3() { return &___m_missingDecendents_3; }
	inline void set_m_missingDecendents_3(int32_t value)
	{
		___m_missingDecendents_3 = value;
	}

	inline static int32_t get_offset_of_m_serInfo_4() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_serInfo_4)); }
	inline SerializationInfo_t950877179 * get_m_serInfo_4() const { return ___m_serInfo_4; }
	inline SerializationInfo_t950877179 ** get_address_of_m_serInfo_4() { return &___m_serInfo_4; }
	inline void set_m_serInfo_4(SerializationInfo_t950877179 * value)
	{
		___m_serInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_serInfo_4), value);
	}

	inline static int32_t get_offset_of_m_surrogate_5() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_surrogate_5)); }
	inline RuntimeObject* get_m_surrogate_5() const { return ___m_surrogate_5; }
	inline RuntimeObject** get_address_of_m_surrogate_5() { return &___m_surrogate_5; }
	inline void set_m_surrogate_5(RuntimeObject* value)
	{
		___m_surrogate_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_surrogate_5), value);
	}

	inline static int32_t get_offset_of_m_missingElements_6() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_missingElements_6)); }
	inline FixupHolderList_t3799028159 * get_m_missingElements_6() const { return ___m_missingElements_6; }
	inline FixupHolderList_t3799028159 ** get_address_of_m_missingElements_6() { return &___m_missingElements_6; }
	inline void set_m_missingElements_6(FixupHolderList_t3799028159 * value)
	{
		___m_missingElements_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_missingElements_6), value);
	}

	inline static int32_t get_offset_of_m_dependentObjects_7() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_dependentObjects_7)); }
	inline LongList_t2258305620 * get_m_dependentObjects_7() const { return ___m_dependentObjects_7; }
	inline LongList_t2258305620 ** get_address_of_m_dependentObjects_7() { return &___m_dependentObjects_7; }
	inline void set_m_dependentObjects_7(LongList_t2258305620 * value)
	{
		___m_dependentObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_dependentObjects_7), value);
	}

	inline static int32_t get_offset_of_m_next_8() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_next_8)); }
	inline ObjectHolder_t2801344551 * get_m_next_8() const { return ___m_next_8; }
	inline ObjectHolder_t2801344551 ** get_address_of_m_next_8() { return &___m_next_8; }
	inline void set_m_next_8(ObjectHolder_t2801344551 * value)
	{
		___m_next_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_next_8), value);
	}

	inline static int32_t get_offset_of_m_flags_9() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_flags_9)); }
	inline int32_t get_m_flags_9() const { return ___m_flags_9; }
	inline int32_t* get_address_of_m_flags_9() { return &___m_flags_9; }
	inline void set_m_flags_9(int32_t value)
	{
		___m_flags_9 = value;
	}

	inline static int32_t get_offset_of_m_markForFixupWhenAvailable_10() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_markForFixupWhenAvailable_10)); }
	inline bool get_m_markForFixupWhenAvailable_10() const { return ___m_markForFixupWhenAvailable_10; }
	inline bool* get_address_of_m_markForFixupWhenAvailable_10() { return &___m_markForFixupWhenAvailable_10; }
	inline void set_m_markForFixupWhenAvailable_10(bool value)
	{
		___m_markForFixupWhenAvailable_10 = value;
	}

	inline static int32_t get_offset_of_m_valueFixup_11() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_valueFixup_11)); }
	inline ValueTypeFixupInfo_t1444618334 * get_m_valueFixup_11() const { return ___m_valueFixup_11; }
	inline ValueTypeFixupInfo_t1444618334 ** get_address_of_m_valueFixup_11() { return &___m_valueFixup_11; }
	inline void set_m_valueFixup_11(ValueTypeFixupInfo_t1444618334 * value)
	{
		___m_valueFixup_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_valueFixup_11), value);
	}

	inline static int32_t get_offset_of_m_typeLoad_12() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_typeLoad_12)); }
	inline TypeLoadExceptionHolder_t2983813904 * get_m_typeLoad_12() const { return ___m_typeLoad_12; }
	inline TypeLoadExceptionHolder_t2983813904 ** get_address_of_m_typeLoad_12() { return &___m_typeLoad_12; }
	inline void set_m_typeLoad_12(TypeLoadExceptionHolder_t2983813904 * value)
	{
		___m_typeLoad_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_typeLoad_12), value);
	}

	inline static int32_t get_offset_of_m_reachable_13() { return static_cast<int32_t>(offsetof(ObjectHolder_t2801344551, ___m_reachable_13)); }
	inline bool get_m_reachable_13() const { return ___m_reachable_13; }
	inline bool* get_address_of_m_reachable_13() { return &___m_reachable_13; }
	inline void set_m_reachable_13(bool value)
	{
		___m_reachable_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTHOLDER_T2801344551_H
#ifndef OBJECTHOLDERLIST_T2007846727_H
#define OBJECTHOLDERLIST_T2007846727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectHolderList
struct  ObjectHolderList_t2007846727  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.ObjectHolder[] System.Runtime.Serialization.ObjectHolderList::m_values
	ObjectHolderU5BU5D_t663564126* ___m_values_0;
	// System.Int32 System.Runtime.Serialization.ObjectHolderList::m_count
	int32_t ___m_count_1;

public:
	inline static int32_t get_offset_of_m_values_0() { return static_cast<int32_t>(offsetof(ObjectHolderList_t2007846727, ___m_values_0)); }
	inline ObjectHolderU5BU5D_t663564126* get_m_values_0() const { return ___m_values_0; }
	inline ObjectHolderU5BU5D_t663564126** get_address_of_m_values_0() { return &___m_values_0; }
	inline void set_m_values_0(ObjectHolderU5BU5D_t663564126* value)
	{
		___m_values_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_values_0), value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(ObjectHolderList_t2007846727, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTHOLDERLIST_T2007846727_H
#ifndef OBJECTHOLDERLISTENUMERATOR_T501693956_H
#define OBJECTHOLDERLISTENUMERATOR_T501693956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectHolderListEnumerator
struct  ObjectHolderListEnumerator_t501693956  : public RuntimeObject
{
public:
	// System.Boolean System.Runtime.Serialization.ObjectHolderListEnumerator::m_isFixupEnumerator
	bool ___m_isFixupEnumerator_0;
	// System.Runtime.Serialization.ObjectHolderList System.Runtime.Serialization.ObjectHolderListEnumerator::m_list
	ObjectHolderList_t2007846727 * ___m_list_1;
	// System.Int32 System.Runtime.Serialization.ObjectHolderListEnumerator::m_startingVersion
	int32_t ___m_startingVersion_2;
	// System.Int32 System.Runtime.Serialization.ObjectHolderListEnumerator::m_currPos
	int32_t ___m_currPos_3;

public:
	inline static int32_t get_offset_of_m_isFixupEnumerator_0() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t501693956, ___m_isFixupEnumerator_0)); }
	inline bool get_m_isFixupEnumerator_0() const { return ___m_isFixupEnumerator_0; }
	inline bool* get_address_of_m_isFixupEnumerator_0() { return &___m_isFixupEnumerator_0; }
	inline void set_m_isFixupEnumerator_0(bool value)
	{
		___m_isFixupEnumerator_0 = value;
	}

	inline static int32_t get_offset_of_m_list_1() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t501693956, ___m_list_1)); }
	inline ObjectHolderList_t2007846727 * get_m_list_1() const { return ___m_list_1; }
	inline ObjectHolderList_t2007846727 ** get_address_of_m_list_1() { return &___m_list_1; }
	inline void set_m_list_1(ObjectHolderList_t2007846727 * value)
	{
		___m_list_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_1), value);
	}

	inline static int32_t get_offset_of_m_startingVersion_2() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t501693956, ___m_startingVersion_2)); }
	inline int32_t get_m_startingVersion_2() const { return ___m_startingVersion_2; }
	inline int32_t* get_address_of_m_startingVersion_2() { return &___m_startingVersion_2; }
	inline void set_m_startingVersion_2(int32_t value)
	{
		___m_startingVersion_2 = value;
	}

	inline static int32_t get_offset_of_m_currPos_3() { return static_cast<int32_t>(offsetof(ObjectHolderListEnumerator_t501693956, ___m_currPos_3)); }
	inline int32_t get_m_currPos_3() const { return ___m_currPos_3; }
	inline int32_t* get_address_of_m_currPos_3() { return &___m_currPos_3; }
	inline void set_m_currPos_3(int32_t value)
	{
		___m_currPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTHOLDERLISTENUMERATOR_T501693956_H
#ifndef OBJECTIDGENERATOR_T1260826161_H
#define OBJECTIDGENERATOR_T1260826161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectIDGenerator
struct  ObjectIDGenerator_t1260826161  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.ObjectIDGenerator::m_currentCount
	int32_t ___m_currentCount_0;
	// System.Int32 System.Runtime.Serialization.ObjectIDGenerator::m_currentSize
	int32_t ___m_currentSize_1;
	// System.Int64[] System.Runtime.Serialization.ObjectIDGenerator::m_ids
	Int64U5BU5D_t2559172825* ___m_ids_2;
	// System.Object[] System.Runtime.Serialization.ObjectIDGenerator::m_objs
	ObjectU5BU5D_t2843939325* ___m_objs_3;

public:
	inline static int32_t get_offset_of_m_currentCount_0() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t1260826161, ___m_currentCount_0)); }
	inline int32_t get_m_currentCount_0() const { return ___m_currentCount_0; }
	inline int32_t* get_address_of_m_currentCount_0() { return &___m_currentCount_0; }
	inline void set_m_currentCount_0(int32_t value)
	{
		___m_currentCount_0 = value;
	}

	inline static int32_t get_offset_of_m_currentSize_1() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t1260826161, ___m_currentSize_1)); }
	inline int32_t get_m_currentSize_1() const { return ___m_currentSize_1; }
	inline int32_t* get_address_of_m_currentSize_1() { return &___m_currentSize_1; }
	inline void set_m_currentSize_1(int32_t value)
	{
		___m_currentSize_1 = value;
	}

	inline static int32_t get_offset_of_m_ids_2() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t1260826161, ___m_ids_2)); }
	inline Int64U5BU5D_t2559172825* get_m_ids_2() const { return ___m_ids_2; }
	inline Int64U5BU5D_t2559172825** get_address_of_m_ids_2() { return &___m_ids_2; }
	inline void set_m_ids_2(Int64U5BU5D_t2559172825* value)
	{
		___m_ids_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ids_2), value);
	}

	inline static int32_t get_offset_of_m_objs_3() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t1260826161, ___m_objs_3)); }
	inline ObjectU5BU5D_t2843939325* get_m_objs_3() const { return ___m_objs_3; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_objs_3() { return &___m_objs_3; }
	inline void set_m_objs_3(ObjectU5BU5D_t2843939325* value)
	{
		___m_objs_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_objs_3), value);
	}
};

struct ObjectIDGenerator_t1260826161_StaticFields
{
public:
	// System.Int32[] System.Runtime.Serialization.ObjectIDGenerator::sizes
	Int32U5BU5D_t385246372* ___sizes_4;

public:
	inline static int32_t get_offset_of_sizes_4() { return static_cast<int32_t>(offsetof(ObjectIDGenerator_t1260826161_StaticFields, ___sizes_4)); }
	inline Int32U5BU5D_t385246372* get_sizes_4() const { return ___sizes_4; }
	inline Int32U5BU5D_t385246372** get_address_of_sizes_4() { return &___sizes_4; }
	inline void set_sizes_4(Int32U5BU5D_t385246372* value)
	{
		___sizes_4 = value;
		Il2CppCodeGenWriteBarrier((&___sizes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIDGENERATOR_T1260826161_H
#ifndef SAFESERIALIZATIONMANAGER_T2481557153_H
#define SAFESERIALIZATIONMANAGER_T2481557153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SafeSerializationManager
struct  SafeSerializationManager_t2481557153  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<System.Object> System.Runtime.Serialization.SafeSerializationManager::m_serializedStates
	RuntimeObject* ___m_serializedStates_0;
	// System.Runtime.Serialization.SerializationInfo System.Runtime.Serialization.SafeSerializationManager::m_savedSerializationInfo
	SerializationInfo_t950877179 * ___m_savedSerializationInfo_1;
	// System.Object System.Runtime.Serialization.SafeSerializationManager::m_realObject
	RuntimeObject * ___m_realObject_2;
	// System.RuntimeType System.Runtime.Serialization.SafeSerializationManager::m_realType
	RuntimeType_t3636489352 * ___m_realType_3;
	// System.EventHandler`1<System.Runtime.Serialization.SafeSerializationEventArgs> System.Runtime.Serialization.SafeSerializationManager::SerializeObjectState
	EventHandler_1_t2246946069 * ___SerializeObjectState_4;

public:
	inline static int32_t get_offset_of_m_serializedStates_0() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t2481557153, ___m_serializedStates_0)); }
	inline RuntimeObject* get_m_serializedStates_0() const { return ___m_serializedStates_0; }
	inline RuntimeObject** get_address_of_m_serializedStates_0() { return &___m_serializedStates_0; }
	inline void set_m_serializedStates_0(RuntimeObject* value)
	{
		___m_serializedStates_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_serializedStates_0), value);
	}

	inline static int32_t get_offset_of_m_savedSerializationInfo_1() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t2481557153, ___m_savedSerializationInfo_1)); }
	inline SerializationInfo_t950877179 * get_m_savedSerializationInfo_1() const { return ___m_savedSerializationInfo_1; }
	inline SerializationInfo_t950877179 ** get_address_of_m_savedSerializationInfo_1() { return &___m_savedSerializationInfo_1; }
	inline void set_m_savedSerializationInfo_1(SerializationInfo_t950877179 * value)
	{
		___m_savedSerializationInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_savedSerializationInfo_1), value);
	}

	inline static int32_t get_offset_of_m_realObject_2() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t2481557153, ___m_realObject_2)); }
	inline RuntimeObject * get_m_realObject_2() const { return ___m_realObject_2; }
	inline RuntimeObject ** get_address_of_m_realObject_2() { return &___m_realObject_2; }
	inline void set_m_realObject_2(RuntimeObject * value)
	{
		___m_realObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_realObject_2), value);
	}

	inline static int32_t get_offset_of_m_realType_3() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t2481557153, ___m_realType_3)); }
	inline RuntimeType_t3636489352 * get_m_realType_3() const { return ___m_realType_3; }
	inline RuntimeType_t3636489352 ** get_address_of_m_realType_3() { return &___m_realType_3; }
	inline void set_m_realType_3(RuntimeType_t3636489352 * value)
	{
		___m_realType_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_realType_3), value);
	}

	inline static int32_t get_offset_of_SerializeObjectState_4() { return static_cast<int32_t>(offsetof(SafeSerializationManager_t2481557153, ___SerializeObjectState_4)); }
	inline EventHandler_1_t2246946069 * get_SerializeObjectState_4() const { return ___SerializeObjectState_4; }
	inline EventHandler_1_t2246946069 ** get_address_of_SerializeObjectState_4() { return &___SerializeObjectState_4; }
	inline void set_SerializeObjectState_4(EventHandler_1_t2246946069 * value)
	{
		___SerializeObjectState_4 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeObjectState_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFESERIALIZATIONMANAGER_T2481557153_H
#ifndef SERIALIZATIONBINDER_T274213469_H
#define SERIALIZATIONBINDER_T274213469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationBinder
struct  SerializationBinder_t274213469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONBINDER_T274213469_H
#ifndef SERIALIZATIONEVENTS_T3591775508_H
#define SERIALIZATIONEVENTS_T3591775508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEvents
struct  SerializationEvents_t3591775508  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnSerializingMethods
	List_1_t3349700990 * ___m_OnSerializingMethods_0;
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnSerializedMethods
	List_1_t3349700990 * ___m_OnSerializedMethods_1;
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnDeserializingMethods
	List_1_t3349700990 * ___m_OnDeserializingMethods_2;
	// System.Collections.Generic.List`1<System.Reflection.MethodInfo> System.Runtime.Serialization.SerializationEvents::m_OnDeserializedMethods
	List_1_t3349700990 * ___m_OnDeserializedMethods_3;

public:
	inline static int32_t get_offset_of_m_OnSerializingMethods_0() { return static_cast<int32_t>(offsetof(SerializationEvents_t3591775508, ___m_OnSerializingMethods_0)); }
	inline List_1_t3349700990 * get_m_OnSerializingMethods_0() const { return ___m_OnSerializingMethods_0; }
	inline List_1_t3349700990 ** get_address_of_m_OnSerializingMethods_0() { return &___m_OnSerializingMethods_0; }
	inline void set_m_OnSerializingMethods_0(List_1_t3349700990 * value)
	{
		___m_OnSerializingMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSerializingMethods_0), value);
	}

	inline static int32_t get_offset_of_m_OnSerializedMethods_1() { return static_cast<int32_t>(offsetof(SerializationEvents_t3591775508, ___m_OnSerializedMethods_1)); }
	inline List_1_t3349700990 * get_m_OnSerializedMethods_1() const { return ___m_OnSerializedMethods_1; }
	inline List_1_t3349700990 ** get_address_of_m_OnSerializedMethods_1() { return &___m_OnSerializedMethods_1; }
	inline void set_m_OnSerializedMethods_1(List_1_t3349700990 * value)
	{
		___m_OnSerializedMethods_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSerializedMethods_1), value);
	}

	inline static int32_t get_offset_of_m_OnDeserializingMethods_2() { return static_cast<int32_t>(offsetof(SerializationEvents_t3591775508, ___m_OnDeserializingMethods_2)); }
	inline List_1_t3349700990 * get_m_OnDeserializingMethods_2() const { return ___m_OnDeserializingMethods_2; }
	inline List_1_t3349700990 ** get_address_of_m_OnDeserializingMethods_2() { return &___m_OnDeserializingMethods_2; }
	inline void set_m_OnDeserializingMethods_2(List_1_t3349700990 * value)
	{
		___m_OnDeserializingMethods_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDeserializingMethods_2), value);
	}

	inline static int32_t get_offset_of_m_OnDeserializedMethods_3() { return static_cast<int32_t>(offsetof(SerializationEvents_t3591775508, ___m_OnDeserializedMethods_3)); }
	inline List_1_t3349700990 * get_m_OnDeserializedMethods_3() const { return ___m_OnDeserializedMethods_3; }
	inline List_1_t3349700990 ** get_address_of_m_OnDeserializedMethods_3() { return &___m_OnDeserializedMethods_3; }
	inline void set_m_OnDeserializedMethods_3(List_1_t3349700990 * value)
	{
		___m_OnDeserializedMethods_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDeserializedMethods_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONEVENTS_T3591775508_H
#ifndef SERIALIZATIONEVENTSCACHE_T2327969880_H
#define SERIALIZATIONEVENTSCACHE_T2327969880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEventsCache
struct  SerializationEventsCache_t2327969880  : public RuntimeObject
{
public:

public:
};

struct SerializationEventsCache_t2327969880_StaticFields
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationEventsCache::cache
	Hashtable_t1853889766 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(SerializationEventsCache_t2327969880_StaticFields, ___cache_0)); }
	inline Hashtable_t1853889766 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t1853889766 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t1853889766 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONEVENTSCACHE_T2327969880_H
#ifndef SERIALIZATIONINFO_T950877179_H
#define SERIALIZATIONINFO_T950877179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t950877179  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfo::m_members
	StringU5BU5D_t1281789340* ___m_members_3;
	// System.Object[] System.Runtime.Serialization.SerializationInfo::m_data
	ObjectU5BU5D_t2843939325* ___m_data_4;
	// System.Type[] System.Runtime.Serialization.SerializationInfo::m_types
	TypeU5BU5D_t3940880105* ___m_types_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.SerializationInfo::m_nameToIndex
	Dictionary_2_t2736202052 * ___m_nameToIndex_6;
	// System.Int32 System.Runtime.Serialization.SerializationInfo::m_currMember
	int32_t ___m_currMember_7;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::m_converter
	RuntimeObject* ___m_converter_8;
	// System.String System.Runtime.Serialization.SerializationInfo::m_fullTypeName
	String_t* ___m_fullTypeName_9;
	// System.String System.Runtime.Serialization.SerializationInfo::m_assemName
	String_t* ___m_assemName_10;
	// System.Type System.Runtime.Serialization.SerializationInfo::objectType
	Type_t * ___objectType_11;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isFullTypeNameSetExplicit
	bool ___isFullTypeNameSetExplicit_12;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isAssemblyNameSetExplicit
	bool ___isAssemblyNameSetExplicit_13;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::requireSameTokenInPartialTrust
	bool ___requireSameTokenInPartialTrust_14;

public:
	inline static int32_t get_offset_of_m_members_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_members_3)); }
	inline StringU5BU5D_t1281789340* get_m_members_3() const { return ___m_members_3; }
	inline StringU5BU5D_t1281789340** get_address_of_m_members_3() { return &___m_members_3; }
	inline void set_m_members_3(StringU5BU5D_t1281789340* value)
	{
		___m_members_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_members_3), value);
	}

	inline static int32_t get_offset_of_m_data_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_data_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_data_4() const { return ___m_data_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_data_4() { return &___m_data_4; }
	inline void set_m_data_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_data_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_data_4), value);
	}

	inline static int32_t get_offset_of_m_types_5() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_types_5)); }
	inline TypeU5BU5D_t3940880105* get_m_types_5() const { return ___m_types_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_m_types_5() { return &___m_types_5; }
	inline void set_m_types_5(TypeU5BU5D_t3940880105* value)
	{
		___m_types_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_types_5), value);
	}

	inline static int32_t get_offset_of_m_nameToIndex_6() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_nameToIndex_6)); }
	inline Dictionary_2_t2736202052 * get_m_nameToIndex_6() const { return ___m_nameToIndex_6; }
	inline Dictionary_2_t2736202052 ** get_address_of_m_nameToIndex_6() { return &___m_nameToIndex_6; }
	inline void set_m_nameToIndex_6(Dictionary_2_t2736202052 * value)
	{
		___m_nameToIndex_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_nameToIndex_6), value);
	}

	inline static int32_t get_offset_of_m_currMember_7() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_currMember_7)); }
	inline int32_t get_m_currMember_7() const { return ___m_currMember_7; }
	inline int32_t* get_address_of_m_currMember_7() { return &___m_currMember_7; }
	inline void set_m_currMember_7(int32_t value)
	{
		___m_currMember_7 = value;
	}

	inline static int32_t get_offset_of_m_converter_8() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_converter_8)); }
	inline RuntimeObject* get_m_converter_8() const { return ___m_converter_8; }
	inline RuntimeObject** get_address_of_m_converter_8() { return &___m_converter_8; }
	inline void set_m_converter_8(RuntimeObject* value)
	{
		___m_converter_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_converter_8), value);
	}

	inline static int32_t get_offset_of_m_fullTypeName_9() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_fullTypeName_9)); }
	inline String_t* get_m_fullTypeName_9() const { return ___m_fullTypeName_9; }
	inline String_t** get_address_of_m_fullTypeName_9() { return &___m_fullTypeName_9; }
	inline void set_m_fullTypeName_9(String_t* value)
	{
		___m_fullTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_fullTypeName_9), value);
	}

	inline static int32_t get_offset_of_m_assemName_10() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___m_assemName_10)); }
	inline String_t* get_m_assemName_10() const { return ___m_assemName_10; }
	inline String_t** get_address_of_m_assemName_10() { return &___m_assemName_10; }
	inline void set_m_assemName_10(String_t* value)
	{
		___m_assemName_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_assemName_10), value);
	}

	inline static int32_t get_offset_of_objectType_11() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___objectType_11)); }
	inline Type_t * get_objectType_11() const { return ___objectType_11; }
	inline Type_t ** get_address_of_objectType_11() { return &___objectType_11; }
	inline void set_objectType_11(Type_t * value)
	{
		___objectType_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_11), value);
	}

	inline static int32_t get_offset_of_isFullTypeNameSetExplicit_12() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___isFullTypeNameSetExplicit_12)); }
	inline bool get_isFullTypeNameSetExplicit_12() const { return ___isFullTypeNameSetExplicit_12; }
	inline bool* get_address_of_isFullTypeNameSetExplicit_12() { return &___isFullTypeNameSetExplicit_12; }
	inline void set_isFullTypeNameSetExplicit_12(bool value)
	{
		___isFullTypeNameSetExplicit_12 = value;
	}

	inline static int32_t get_offset_of_isAssemblyNameSetExplicit_13() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___isAssemblyNameSetExplicit_13)); }
	inline bool get_isAssemblyNameSetExplicit_13() const { return ___isAssemblyNameSetExplicit_13; }
	inline bool* get_address_of_isAssemblyNameSetExplicit_13() { return &___isAssemblyNameSetExplicit_13; }
	inline void set_isAssemblyNameSetExplicit_13(bool value)
	{
		___isAssemblyNameSetExplicit_13 = value;
	}

	inline static int32_t get_offset_of_requireSameTokenInPartialTrust_14() { return static_cast<int32_t>(offsetof(SerializationInfo_t950877179, ___requireSameTokenInPartialTrust_14)); }
	inline bool get_requireSameTokenInPartialTrust_14() const { return ___requireSameTokenInPartialTrust_14; }
	inline bool* get_address_of_requireSameTokenInPartialTrust_14() { return &___requireSameTokenInPartialTrust_14; }
	inline void set_requireSameTokenInPartialTrust_14(bool value)
	{
		___requireSameTokenInPartialTrust_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T950877179_H
#ifndef SERIALIZATIONINFOENUMERATOR_T2232395945_H
#define SERIALIZATIONINFOENUMERATOR_T2232395945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfoEnumerator
struct  SerializationInfoEnumerator_t2232395945  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfoEnumerator::m_members
	StringU5BU5D_t1281789340* ___m_members_0;
	// System.Object[] System.Runtime.Serialization.SerializationInfoEnumerator::m_data
	ObjectU5BU5D_t2843939325* ___m_data_1;
	// System.Type[] System.Runtime.Serialization.SerializationInfoEnumerator::m_types
	TypeU5BU5D_t3940880105* ___m_types_2;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_numItems
	int32_t ___m_numItems_3;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_currItem
	int32_t ___m_currItem_4;
	// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::m_current
	bool ___m_current_5;

public:
	inline static int32_t get_offset_of_m_members_0() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t2232395945, ___m_members_0)); }
	inline StringU5BU5D_t1281789340* get_m_members_0() const { return ___m_members_0; }
	inline StringU5BU5D_t1281789340** get_address_of_m_members_0() { return &___m_members_0; }
	inline void set_m_members_0(StringU5BU5D_t1281789340* value)
	{
		___m_members_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_members_0), value);
	}

	inline static int32_t get_offset_of_m_data_1() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t2232395945, ___m_data_1)); }
	inline ObjectU5BU5D_t2843939325* get_m_data_1() const { return ___m_data_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_data_1() { return &___m_data_1; }
	inline void set_m_data_1(ObjectU5BU5D_t2843939325* value)
	{
		___m_data_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_data_1), value);
	}

	inline static int32_t get_offset_of_m_types_2() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t2232395945, ___m_types_2)); }
	inline TypeU5BU5D_t3940880105* get_m_types_2() const { return ___m_types_2; }
	inline TypeU5BU5D_t3940880105** get_address_of_m_types_2() { return &___m_types_2; }
	inline void set_m_types_2(TypeU5BU5D_t3940880105* value)
	{
		___m_types_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_types_2), value);
	}

	inline static int32_t get_offset_of_m_numItems_3() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t2232395945, ___m_numItems_3)); }
	inline int32_t get_m_numItems_3() const { return ___m_numItems_3; }
	inline int32_t* get_address_of_m_numItems_3() { return &___m_numItems_3; }
	inline void set_m_numItems_3(int32_t value)
	{
		___m_numItems_3 = value;
	}

	inline static int32_t get_offset_of_m_currItem_4() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t2232395945, ___m_currItem_4)); }
	inline int32_t get_m_currItem_4() const { return ___m_currItem_4; }
	inline int32_t* get_address_of_m_currItem_4() { return &___m_currItem_4; }
	inline void set_m_currItem_4(int32_t value)
	{
		___m_currItem_4 = value;
	}

	inline static int32_t get_offset_of_m_current_5() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_t2232395945, ___m_current_5)); }
	inline bool get_m_current_5() const { return ___m_current_5; }
	inline bool* get_address_of_m_current_5() { return &___m_current_5; }
	inline void set_m_current_5(bool value)
	{
		___m_current_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFOENUMERATOR_T2232395945_H
#ifndef SURROGATEFORCYCLICALREFERENCE_T3309482836_H
#define SURROGATEFORCYCLICALREFERENCE_T3309482836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SurrogateForCyclicalReference
struct  SurrogateForCyclicalReference_t3309482836  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.SurrogateForCyclicalReference::innerSurrogate
	RuntimeObject* ___innerSurrogate_0;

public:
	inline static int32_t get_offset_of_innerSurrogate_0() { return static_cast<int32_t>(offsetof(SurrogateForCyclicalReference_t3309482836, ___innerSurrogate_0)); }
	inline RuntimeObject* get_innerSurrogate_0() const { return ___innerSurrogate_0; }
	inline RuntimeObject** get_address_of_innerSurrogate_0() { return &___innerSurrogate_0; }
	inline void set_innerSurrogate_0(RuntimeObject* value)
	{
		___innerSurrogate_0 = value;
		Il2CppCodeGenWriteBarrier((&___innerSurrogate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURROGATEFORCYCLICALREFERENCE_T3309482836_H
#ifndef TYPELOADEXCEPTIONHOLDER_T2983813904_H
#define TYPELOADEXCEPTIONHOLDER_T2983813904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.TypeLoadExceptionHolder
struct  TypeLoadExceptionHolder_t2983813904  : public RuntimeObject
{
public:
	// System.String System.Runtime.Serialization.TypeLoadExceptionHolder::m_typeName
	String_t* ___m_typeName_0;

public:
	inline static int32_t get_offset_of_m_typeName_0() { return static_cast<int32_t>(offsetof(TypeLoadExceptionHolder_t2983813904, ___m_typeName_0)); }
	inline String_t* get_m_typeName_0() const { return ___m_typeName_0; }
	inline String_t** get_address_of_m_typeName_0() { return &___m_typeName_0; }
	inline void set_m_typeName_0(String_t* value)
	{
		___m_typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPELOADEXCEPTIONHOLDER_T2983813904_H
#ifndef VALUETYPEFIXUPINFO_T1444618334_H
#define VALUETYPEFIXUPINFO_T1444618334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ValueTypeFixupInfo
struct  ValueTypeFixupInfo_t1444618334  : public RuntimeObject
{
public:
	// System.Int64 System.Runtime.Serialization.ValueTypeFixupInfo::m_containerID
	int64_t ___m_containerID_0;
	// System.Reflection.FieldInfo System.Runtime.Serialization.ValueTypeFixupInfo::m_parentField
	FieldInfo_t * ___m_parentField_1;
	// System.Int32[] System.Runtime.Serialization.ValueTypeFixupInfo::m_parentIndex
	Int32U5BU5D_t385246372* ___m_parentIndex_2;

public:
	inline static int32_t get_offset_of_m_containerID_0() { return static_cast<int32_t>(offsetof(ValueTypeFixupInfo_t1444618334, ___m_containerID_0)); }
	inline int64_t get_m_containerID_0() const { return ___m_containerID_0; }
	inline int64_t* get_address_of_m_containerID_0() { return &___m_containerID_0; }
	inline void set_m_containerID_0(int64_t value)
	{
		___m_containerID_0 = value;
	}

	inline static int32_t get_offset_of_m_parentField_1() { return static_cast<int32_t>(offsetof(ValueTypeFixupInfo_t1444618334, ___m_parentField_1)); }
	inline FieldInfo_t * get_m_parentField_1() const { return ___m_parentField_1; }
	inline FieldInfo_t ** get_address_of_m_parentField_1() { return &___m_parentField_1; }
	inline void set_m_parentField_1(FieldInfo_t * value)
	{
		___m_parentField_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_parentField_1), value);
	}

	inline static int32_t get_offset_of_m_parentIndex_2() { return static_cast<int32_t>(offsetof(ValueTypeFixupInfo_t1444618334, ___m_parentIndex_2)); }
	inline Int32U5BU5D_t385246372* get_m_parentIndex_2() const { return ___m_parentIndex_2; }
	inline Int32U5BU5D_t385246372** get_address_of_m_parentIndex_2() { return &___m_parentIndex_2; }
	inline void set_m_parentIndex_2(Int32U5BU5D_t385246372* value)
	{
		___m_parentIndex_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_parentIndex_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUETYPEFIXUPINFO_T1444618334_H
#ifndef BINARYCOMPATIBILITY_T3378813580_H
#define BINARYCOMPATIBILITY_T3378813580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Versioning.BinaryCompatibility
struct  BinaryCompatibility_t3378813580  : public RuntimeObject
{
public:

public:
};

struct BinaryCompatibility_t3378813580_StaticFields
{
public:
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility::TargetsAtLeast_Desktop_V4_5
	bool ___TargetsAtLeast_Desktop_V4_5_0;
	// System.Boolean System.Runtime.Versioning.BinaryCompatibility::TargetsAtLeast_Desktop_V4_5_1
	bool ___TargetsAtLeast_Desktop_V4_5_1_1;

public:
	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_0() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t3378813580_StaticFields, ___TargetsAtLeast_Desktop_V4_5_0)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_0() const { return ___TargetsAtLeast_Desktop_V4_5_0; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_0() { return &___TargetsAtLeast_Desktop_V4_5_0; }
	inline void set_TargetsAtLeast_Desktop_V4_5_0(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_0 = value;
	}

	inline static int32_t get_offset_of_TargetsAtLeast_Desktop_V4_5_1_1() { return static_cast<int32_t>(offsetof(BinaryCompatibility_t3378813580_StaticFields, ___TargetsAtLeast_Desktop_V4_5_1_1)); }
	inline bool get_TargetsAtLeast_Desktop_V4_5_1_1() const { return ___TargetsAtLeast_Desktop_V4_5_1_1; }
	inline bool* get_address_of_TargetsAtLeast_Desktop_V4_5_1_1() { return &___TargetsAtLeast_Desktop_V4_5_1_1; }
	inline void set_TargetsAtLeast_Desktop_V4_5_1_1(bool value)
	{
		___TargetsAtLeast_Desktop_V4_5_1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCOMPATIBILITY_T3378813580_H
#ifndef ASYMMETRICALGORITHM_T932037087_H
#define ASYMMETRICALGORITHM_T932037087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricAlgorithm
struct  AsymmetricAlgorithm_t932037087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_0;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_1;

public:
	inline static int32_t get_offset_of_KeySizeValue_0() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___KeySizeValue_0)); }
	inline int32_t get_KeySizeValue_0() const { return ___KeySizeValue_0; }
	inline int32_t* get_address_of_KeySizeValue_0() { return &___KeySizeValue_0; }
	inline void set_KeySizeValue_0(int32_t value)
	{
		___KeySizeValue_0 = value;
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_1() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_t932037087, ___LegalKeySizesValue_1)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_1() const { return ___LegalKeySizesValue_1; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_1() { return &___LegalKeySizesValue_1; }
	inline void set_LegalKeySizesValue_1(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICALGORITHM_T932037087_H
#ifndef ASYMMETRICSIGNATUREDEFORMATTER_T2681190756_H
#define ASYMMETRICSIGNATUREDEFORMATTER_T2681190756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct  AsymmetricSignatureDeformatter_t2681190756  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICSIGNATUREDEFORMATTER_T2681190756_H
#ifndef ASYMMETRICSIGNATUREFORMATTER_T3486936014_H
#define ASYMMETRICSIGNATUREFORMATTER_T3486936014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricSignatureFormatter
struct  AsymmetricSignatureFormatter_t3486936014  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICSIGNATUREFORMATTER_T3486936014_H
#ifndef CONSTANTS_T3997854879_H
#define CONSTANTS_T3997854879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Constants
struct  Constants_t3997854879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T3997854879_H
#ifndef CRYPTOAPITRANSFORM_T1695680317_H
#define CRYPTOAPITRANSFORM_T1695680317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoAPITransform
struct  CryptoAPITransform_t1695680317  : public RuntimeObject
{
public:
	// System.Boolean System.Security.Cryptography.CryptoAPITransform::m_disposed
	bool ___m_disposed_0;

public:
	inline static int32_t get_offset_of_m_disposed_0() { return static_cast<int32_t>(offsetof(CryptoAPITransform_t1695680317, ___m_disposed_0)); }
	inline bool get_m_disposed_0() const { return ___m_disposed_0; }
	inline bool* get_address_of_m_disposed_0() { return &___m_disposed_0; }
	inline void set_m_disposed_0(bool value)
	{
		___m_disposed_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOAPITRANSFORM_T1695680317_H
#ifndef CRYPTOCONFIG_T4201145714_H
#define CRYPTOCONFIG_T4201145714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoConfig
struct  CryptoConfig_t4201145714  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOCONFIG_T4201145714_H
#ifndef CSPKEYCONTAINERINFO_T3833902945_H
#define CSPKEYCONTAINERINFO_T3833902945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CspKeyContainerInfo
struct  CspKeyContainerInfo_t3833902945  : public RuntimeObject
{
public:
	// System.Security.Cryptography.CspParameters System.Security.Cryptography.CspKeyContainerInfo::_params
	CspParameters_t239852639 * ____params_0;
	// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::_random
	bool ____random_1;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(CspKeyContainerInfo_t3833902945, ____params_0)); }
	inline CspParameters_t239852639 * get__params_0() const { return ____params_0; }
	inline CspParameters_t239852639 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(CspParameters_t239852639 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}

	inline static int32_t get_offset_of__random_1() { return static_cast<int32_t>(offsetof(CspKeyContainerInfo_t3833902945, ____random_1)); }
	inline bool get__random_1() const { return ____random_1; }
	inline bool* get_address_of__random_1() { return &____random_1; }
	inline void set__random_1(bool value)
	{
		____random_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPKEYCONTAINERINFO_T3833902945_H
#ifndef HASHALGORITHM_T1432317219_H
#define HASHALGORITHM_T1432317219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t1432317219  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t4116647657* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashValue_1)); }
	inline ByteU5BU5D_t4116647657* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_t4116647657* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_1), value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T1432317219_H
#ifndef RANDOMNUMBERGENERATOR_T386037858_H
#define RANDOMNUMBERGENERATOR_T386037858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RandomNumberGenerator
struct  RandomNumberGenerator_t386037858  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMNUMBERGENERATOR_T386037858_H
#ifndef SHA1INTERNAL_T3251293021_H
#define SHA1INTERNAL_T3251293021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SHA1Internal
struct  SHA1Internal_t3251293021  : public RuntimeObject
{
public:
	// System.UInt32[] System.Security.Cryptography.SHA1Internal::_H
	UInt32U5BU5D_t2770800703* ____H_1;
	// System.UInt64 System.Security.Cryptography.SHA1Internal::count
	uint64_t ___count_2;
	// System.Byte[] System.Security.Cryptography.SHA1Internal::_ProcessingBuffer
	ByteU5BU5D_t4116647657* ____ProcessingBuffer_3;
	// System.Int32 System.Security.Cryptography.SHA1Internal::_ProcessingBufferCount
	int32_t ____ProcessingBufferCount_4;
	// System.UInt32[] System.Security.Cryptography.SHA1Internal::buff
	UInt32U5BU5D_t2770800703* ___buff_5;

public:
	inline static int32_t get_offset_of__H_1() { return static_cast<int32_t>(offsetof(SHA1Internal_t3251293021, ____H_1)); }
	inline UInt32U5BU5D_t2770800703* get__H_1() const { return ____H_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of__H_1() { return &____H_1; }
	inline void set__H_1(UInt32U5BU5D_t2770800703* value)
	{
		____H_1 = value;
		Il2CppCodeGenWriteBarrier((&____H_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(SHA1Internal_t3251293021, ___count_2)); }
	inline uint64_t get_count_2() const { return ___count_2; }
	inline uint64_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(uint64_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of__ProcessingBuffer_3() { return static_cast<int32_t>(offsetof(SHA1Internal_t3251293021, ____ProcessingBuffer_3)); }
	inline ByteU5BU5D_t4116647657* get__ProcessingBuffer_3() const { return ____ProcessingBuffer_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__ProcessingBuffer_3() { return &____ProcessingBuffer_3; }
	inline void set__ProcessingBuffer_3(ByteU5BU5D_t4116647657* value)
	{
		____ProcessingBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____ProcessingBuffer_3), value);
	}

	inline static int32_t get_offset_of__ProcessingBufferCount_4() { return static_cast<int32_t>(offsetof(SHA1Internal_t3251293021, ____ProcessingBufferCount_4)); }
	inline int32_t get__ProcessingBufferCount_4() const { return ____ProcessingBufferCount_4; }
	inline int32_t* get_address_of__ProcessingBufferCount_4() { return &____ProcessingBufferCount_4; }
	inline void set__ProcessingBufferCount_4(int32_t value)
	{
		____ProcessingBufferCount_4 = value;
	}

	inline static int32_t get_offset_of_buff_5() { return static_cast<int32_t>(offsetof(SHA1Internal_t3251293021, ___buff_5)); }
	inline UInt32U5BU5D_t2770800703* get_buff_5() const { return ___buff_5; }
	inline UInt32U5BU5D_t2770800703** get_address_of_buff_5() { return &___buff_5; }
	inline void set_buff_5(UInt32U5BU5D_t2770800703* value)
	{
		___buff_5 = value;
		Il2CppCodeGenWriteBarrier((&___buff_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA1INTERNAL_T3251293021_H
#ifndef SIGNATUREDESCRIPTION_T1971889425_H
#define SIGNATUREDESCRIPTION_T1971889425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SignatureDescription
struct  SignatureDescription_t1971889425  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.SignatureDescription::_strKey
	String_t* ____strKey_0;
	// System.String System.Security.Cryptography.SignatureDescription::_strDigest
	String_t* ____strDigest_1;
	// System.String System.Security.Cryptography.SignatureDescription::_strFormatter
	String_t* ____strFormatter_2;
	// System.String System.Security.Cryptography.SignatureDescription::_strDeformatter
	String_t* ____strDeformatter_3;

public:
	inline static int32_t get_offset_of__strKey_0() { return static_cast<int32_t>(offsetof(SignatureDescription_t1971889425, ____strKey_0)); }
	inline String_t* get__strKey_0() const { return ____strKey_0; }
	inline String_t** get_address_of__strKey_0() { return &____strKey_0; }
	inline void set__strKey_0(String_t* value)
	{
		____strKey_0 = value;
		Il2CppCodeGenWriteBarrier((&____strKey_0), value);
	}

	inline static int32_t get_offset_of__strDigest_1() { return static_cast<int32_t>(offsetof(SignatureDescription_t1971889425, ____strDigest_1)); }
	inline String_t* get__strDigest_1() const { return ____strDigest_1; }
	inline String_t** get_address_of__strDigest_1() { return &____strDigest_1; }
	inline void set__strDigest_1(String_t* value)
	{
		____strDigest_1 = value;
		Il2CppCodeGenWriteBarrier((&____strDigest_1), value);
	}

	inline static int32_t get_offset_of__strFormatter_2() { return static_cast<int32_t>(offsetof(SignatureDescription_t1971889425, ____strFormatter_2)); }
	inline String_t* get__strFormatter_2() const { return ____strFormatter_2; }
	inline String_t** get_address_of__strFormatter_2() { return &____strFormatter_2; }
	inline void set__strFormatter_2(String_t* value)
	{
		____strFormatter_2 = value;
		Il2CppCodeGenWriteBarrier((&____strFormatter_2), value);
	}

	inline static int32_t get_offset_of__strDeformatter_3() { return static_cast<int32_t>(offsetof(SignatureDescription_t1971889425, ____strDeformatter_3)); }
	inline String_t* get__strDeformatter_3() const { return ____strDeformatter_3; }
	inline String_t** get_address_of__strDeformatter_3() { return &____strDeformatter_3; }
	inline void set__strDeformatter_3(String_t* value)
	{
		____strDeformatter_3 = value;
		Il2CppCodeGenWriteBarrier((&____strDeformatter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREDESCRIPTION_T1971889425_H
#ifndef UTILS_T1416439708_H
#define UTILS_T1416439708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Utils
struct  Utils_t1416439708  : public RuntimeObject
{
public:

public:
};

struct Utils_t1416439708_StaticFields
{
public:
	// System.Security.Cryptography.RNGCryptoServiceProvider modreq(System.Runtime.CompilerServices.IsVolatile) System.Security.Cryptography.Utils::_rng
	RNGCryptoServiceProvider_t3397414743 * ____rng_1;

public:
	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Utils_t1416439708_StaticFields, ____rng_1)); }
	inline RNGCryptoServiceProvider_t3397414743 * get__rng_1() const { return ____rng_1; }
	inline RNGCryptoServiceProvider_t3397414743 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RNGCryptoServiceProvider_t3397414743 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T1416439708_H
#ifndef X509CERTIFICATE_T713131622_H
#define X509CERTIFICATE_T713131622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate
struct  X509Certificate_t713131622  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateImpl System.Security.Cryptography.X509Certificates.X509Certificate::impl
	X509CertificateImpl_t2851385038 * ___impl_0;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::hideDates
	bool ___hideDates_1;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::issuer_name
	String_t* ___issuer_name_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::subject_name
	String_t* ___subject_name_3;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(X509Certificate_t713131622, ___impl_0)); }
	inline X509CertificateImpl_t2851385038 * get_impl_0() const { return ___impl_0; }
	inline X509CertificateImpl_t2851385038 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(X509CertificateImpl_t2851385038 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}

	inline static int32_t get_offset_of_hideDates_1() { return static_cast<int32_t>(offsetof(X509Certificate_t713131622, ___hideDates_1)); }
	inline bool get_hideDates_1() const { return ___hideDates_1; }
	inline bool* get_address_of_hideDates_1() { return &___hideDates_1; }
	inline void set_hideDates_1(bool value)
	{
		___hideDates_1 = value;
	}

	inline static int32_t get_offset_of_issuer_name_2() { return static_cast<int32_t>(offsetof(X509Certificate_t713131622, ___issuer_name_2)); }
	inline String_t* get_issuer_name_2() const { return ___issuer_name_2; }
	inline String_t** get_address_of_issuer_name_2() { return &___issuer_name_2; }
	inline void set_issuer_name_2(String_t* value)
	{
		___issuer_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_2), value);
	}

	inline static int32_t get_offset_of_subject_name_3() { return static_cast<int32_t>(offsetof(X509Certificate_t713131622, ___subject_name_3)); }
	inline String_t* get_subject_name_3() const { return ___subject_name_3; }
	inline String_t** get_address_of_subject_name_3() { return &___subject_name_3; }
	inline void set_subject_name_3(String_t* value)
	{
		___subject_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T713131622_H
#ifndef X509CERTIFICATEIMPL_T2851385038_H
#define X509CERTIFICATEIMPL_T2851385038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateImpl
struct  X509CertificateImpl_t2851385038  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509CertificateImpl::cachedCertificateHash
	ByteU5BU5D_t4116647657* ___cachedCertificateHash_0;

public:
	inline static int32_t get_offset_of_cachedCertificateHash_0() { return static_cast<int32_t>(offsetof(X509CertificateImpl_t2851385038, ___cachedCertificateHash_0)); }
	inline ByteU5BU5D_t4116647657* get_cachedCertificateHash_0() const { return ___cachedCertificateHash_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_cachedCertificateHash_0() { return &___cachedCertificateHash_0; }
	inline void set_cachedCertificateHash_0(ByteU5BU5D_t4116647657* value)
	{
		___cachedCertificateHash_0 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCertificateHash_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEIMPL_T2851385038_H
#ifndef X509CONSTANTS_T648943299_H
#define X509CONSTANTS_T648943299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Constants
struct  X509Constants_t648943299  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CONSTANTS_T648943299_H
#ifndef X509HELPER_T1273321433_H
#define X509HELPER_T1273321433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Helper
struct  X509Helper_t1273321433  : public RuntimeObject
{
public:

public:
};

struct X509Helper_t1273321433_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.INativeCertificateHelper System.Security.Cryptography.X509Certificates.X509Helper::nativeHelper
	RuntimeObject* ___nativeHelper_0;

public:
	inline static int32_t get_offset_of_nativeHelper_0() { return static_cast<int32_t>(offsetof(X509Helper_t1273321433_StaticFields, ___nativeHelper_0)); }
	inline RuntimeObject* get_nativeHelper_0() const { return ___nativeHelper_0; }
	inline RuntimeObject** get_address_of_nativeHelper_0() { return &___nativeHelper_0; }
	inline void set_nativeHelper_0(RuntimeObject* value)
	{
		___nativeHelper_0 = value;
		Il2CppCodeGenWriteBarrier((&___nativeHelper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509HELPER_T1273321433_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FIELDINFO_T_H
#define FIELDINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFO_T_H
#ifndef ONDESERIALIZEDATTRIBUTE_T1335880599_H
#define ONDESERIALIZEDATTRIBUTE_T1335880599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.OnDeserializedAttribute
struct  OnDeserializedAttribute_t1335880599  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDESERIALIZEDATTRIBUTE_T1335880599_H
#ifndef ONDESERIALIZINGATTRIBUTE_T338753086_H
#define ONDESERIALIZINGATTRIBUTE_T338753086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.OnDeserializingAttribute
struct  OnDeserializingAttribute_t338753086  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDESERIALIZINGATTRIBUTE_T338753086_H
#ifndef ONSERIALIZEDATTRIBUTE_T2595932830_H
#define ONSERIALIZEDATTRIBUTE_T2595932830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.OnSerializedAttribute
struct  OnSerializedAttribute_t2595932830  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSERIALIZEDATTRIBUTE_T2595932830_H
#ifndef ONSERIALIZINGATTRIBUTE_T2580696919_H
#define ONSERIALIZINGATTRIBUTE_T2580696919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.OnSerializingAttribute
struct  OnSerializingAttribute_t2580696919  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSERIALIZINGATTRIBUTE_T2580696919_H
#ifndef OPTIONALFIELDATTRIBUTE_T761606048_H
#define OPTIONALFIELDATTRIBUTE_T761606048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.OptionalFieldAttribute
struct  OptionalFieldAttribute_t761606048  : public Attribute_t861562559
{
public:
	// System.Int32 System.Runtime.Serialization.OptionalFieldAttribute::versionAdded
	int32_t ___versionAdded_0;

public:
	inline static int32_t get_offset_of_versionAdded_0() { return static_cast<int32_t>(offsetof(OptionalFieldAttribute_t761606048, ___versionAdded_0)); }
	inline int32_t get_versionAdded_0() const { return ___versionAdded_0; }
	inline int32_t* get_address_of_versionAdded_0() { return &___versionAdded_0; }
	inline void set_versionAdded_0(int32_t value)
	{
		___versionAdded_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONALFIELDATTRIBUTE_T761606048_H
#ifndef SERIALIZATIONENTRY_T648286436_H
#define SERIALIZATIONENTRY_T648286436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEntry
struct  SerializationEntry_t648286436 
{
public:
	// System.Type System.Runtime.Serialization.SerializationEntry::m_type
	Type_t * ___m_type_0;
	// System.Object System.Runtime.Serialization.SerializationEntry::m_value
	RuntimeObject * ___m_value_1;
	// System.String System.Runtime.Serialization.SerializationEntry::m_name
	String_t* ___m_name_2;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(SerializationEntry_t648286436, ___m_type_0)); }
	inline Type_t * get_m_type_0() const { return ___m_type_0; }
	inline Type_t ** get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(Type_t * value)
	{
		___m_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_type_0), value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(SerializationEntry_t648286436, ___m_value_1)); }
	inline RuntimeObject * get_m_value_1() const { return ___m_value_1; }
	inline RuntimeObject ** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(RuntimeObject * value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_1), value);
	}

	inline static int32_t get_offset_of_m_name_2() { return static_cast<int32_t>(offsetof(SerializationEntry_t648286436, ___m_name_2)); }
	inline String_t* get_m_name_2() const { return ___m_name_2; }
	inline String_t** get_address_of_m_name_2() { return &___m_name_2; }
	inline void set_m_name_2(String_t* value)
	{
		___m_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t648286436_marshaled_pinvoke
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	char* ___m_name_2;
};
// Native definition for COM marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_t648286436_marshaled_com
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	Il2CppChar* ___m_name_2;
};
#endif // SERIALIZATIONENTRY_T648286436_H
#ifndef DSA_T2386879874_H
#define DSA_T2386879874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSA
struct  DSA_t2386879874  : public AsymmetricAlgorithm_t932037087
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSA_T2386879874_H
#ifndef DSASIGNATUREDESCRIPTION_T1163053634_H
#define DSASIGNATUREDESCRIPTION_T1163053634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSASignatureDescription
struct  DSASignatureDescription_t1163053634  : public SignatureDescription_t1971889425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSASIGNATUREDESCRIPTION_T1163053634_H
#ifndef MD5_T3177620429_H
#define MD5_T3177620429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.MD5
struct  MD5_t3177620429  : public HashAlgorithm_t1432317219
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5_T3177620429_H
#ifndef RSAPKCS1SIGNATUREDEFORMATTER_T3767223008_H
#define RSAPKCS1SIGNATUREDEFORMATTER_T3767223008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
struct  RSAPKCS1SignatureDeformatter_t3767223008  : public AsymmetricSignatureDeformatter_t2681190756
{
public:
	// System.Security.Cryptography.RSA System.Security.Cryptography.RSAPKCS1SignatureDeformatter::rsa
	RSA_t2385438082 * ___rsa_0;
	// System.String System.Security.Cryptography.RSAPKCS1SignatureDeformatter::hashName
	String_t* ___hashName_1;

public:
	inline static int32_t get_offset_of_rsa_0() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureDeformatter_t3767223008, ___rsa_0)); }
	inline RSA_t2385438082 * get_rsa_0() const { return ___rsa_0; }
	inline RSA_t2385438082 ** get_address_of_rsa_0() { return &___rsa_0; }
	inline void set_rsa_0(RSA_t2385438082 * value)
	{
		___rsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsa_0), value);
	}

	inline static int32_t get_offset_of_hashName_1() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureDeformatter_t3767223008, ___hashName_1)); }
	inline String_t* get_hashName_1() const { return ___hashName_1; }
	inline String_t** get_address_of_hashName_1() { return &___hashName_1; }
	inline void set_hashName_1(String_t* value)
	{
		___hashName_1 = value;
		Il2CppCodeGenWriteBarrier((&___hashName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPKCS1SIGNATUREDEFORMATTER_T3767223008_H
#ifndef RSAPKCS1SIGNATUREFORMATTER_T1514197062_H
#define RSAPKCS1SIGNATUREFORMATTER_T1514197062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAPKCS1SignatureFormatter
struct  RSAPKCS1SignatureFormatter_t1514197062  : public AsymmetricSignatureFormatter_t3486936014
{
public:
	// System.Security.Cryptography.RSA System.Security.Cryptography.RSAPKCS1SignatureFormatter::rsa
	RSA_t2385438082 * ___rsa_0;
	// System.String System.Security.Cryptography.RSAPKCS1SignatureFormatter::hash
	String_t* ___hash_1;

public:
	inline static int32_t get_offset_of_rsa_0() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureFormatter_t1514197062, ___rsa_0)); }
	inline RSA_t2385438082 * get_rsa_0() const { return ___rsa_0; }
	inline RSA_t2385438082 ** get_address_of_rsa_0() { return &___rsa_0; }
	inline void set_rsa_0(RSA_t2385438082 * value)
	{
		___rsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsa_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(RSAPKCS1SignatureFormatter_t1514197062, ___hash_1)); }
	inline String_t* get_hash_1() const { return ___hash_1; }
	inline String_t** get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(String_t* value)
	{
		___hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___hash_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPKCS1SIGNATUREFORMATTER_T1514197062_H
#ifndef SHA1_T1803193667_H
#define SHA1_T1803193667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SHA1
struct  SHA1_t1803193667  : public HashAlgorithm_t1432317219
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA1_T1803193667_H
#ifndef X509CERTIFICATEIMPLMONO_T285361170_H
#define X509CERTIFICATEIMPLMONO_T285361170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateImplMono
struct  X509CertificateImplMono_t285361170  : public X509CertificateImpl_t2851385038
{
public:
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509CertificateImplMono::x509
	X509Certificate_t489243024 * ___x509_1;

public:
	inline static int32_t get_offset_of_x509_1() { return static_cast<int32_t>(offsetof(X509CertificateImplMono_t285361170, ___x509_1)); }
	inline X509Certificate_t489243024 * get_x509_1() const { return ___x509_1; }
	inline X509Certificate_t489243024 ** get_address_of_x509_1() { return &___x509_1; }
	inline void set_x509_1(X509Certificate_t489243024 * value)
	{
		___x509_1 = value;
		Il2CppCodeGenWriteBarrier((&___x509_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEIMPLMONO_T285361170_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef FORMATTERSERVICES_T305532257_H
#define FORMATTERSERVICES_T305532257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.FormatterServices
struct  FormatterServices_t305532257  : public RuntimeObject
{
public:

public:
};

struct FormatterServices_t305532257_StaticFields
{
public:
	// System.Collections.Concurrent.ConcurrentDictionary`2<System.Runtime.Serialization.MemberHolder,System.Reflection.MemberInfo[]> System.Runtime.Serialization.FormatterServices::m_MemberInfoTable
	ConcurrentDictionary_2_t2965367907 * ___m_MemberInfoTable_0;
	// System.Boolean System.Runtime.Serialization.FormatterServices::unsafeTypeForwardersIsEnabled
	bool ___unsafeTypeForwardersIsEnabled_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.Serialization.FormatterServices::unsafeTypeForwardersIsEnabledInitialized
	bool ___unsafeTypeForwardersIsEnabledInitialized_2;
	// System.Type[] System.Runtime.Serialization.FormatterServices::advancedTypes
	TypeU5BU5D_t3940880105* ___advancedTypes_3;
	// System.Reflection.Binder System.Runtime.Serialization.FormatterServices::s_binder
	Binder_t2999457153 * ___s_binder_4;

public:
	inline static int32_t get_offset_of_m_MemberInfoTable_0() { return static_cast<int32_t>(offsetof(FormatterServices_t305532257_StaticFields, ___m_MemberInfoTable_0)); }
	inline ConcurrentDictionary_2_t2965367907 * get_m_MemberInfoTable_0() const { return ___m_MemberInfoTable_0; }
	inline ConcurrentDictionary_2_t2965367907 ** get_address_of_m_MemberInfoTable_0() { return &___m_MemberInfoTable_0; }
	inline void set_m_MemberInfoTable_0(ConcurrentDictionary_2_t2965367907 * value)
	{
		___m_MemberInfoTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MemberInfoTable_0), value);
	}

	inline static int32_t get_offset_of_unsafeTypeForwardersIsEnabled_1() { return static_cast<int32_t>(offsetof(FormatterServices_t305532257_StaticFields, ___unsafeTypeForwardersIsEnabled_1)); }
	inline bool get_unsafeTypeForwardersIsEnabled_1() const { return ___unsafeTypeForwardersIsEnabled_1; }
	inline bool* get_address_of_unsafeTypeForwardersIsEnabled_1() { return &___unsafeTypeForwardersIsEnabled_1; }
	inline void set_unsafeTypeForwardersIsEnabled_1(bool value)
	{
		___unsafeTypeForwardersIsEnabled_1 = value;
	}

	inline static int32_t get_offset_of_unsafeTypeForwardersIsEnabledInitialized_2() { return static_cast<int32_t>(offsetof(FormatterServices_t305532257_StaticFields, ___unsafeTypeForwardersIsEnabledInitialized_2)); }
	inline bool get_unsafeTypeForwardersIsEnabledInitialized_2() const { return ___unsafeTypeForwardersIsEnabledInitialized_2; }
	inline bool* get_address_of_unsafeTypeForwardersIsEnabledInitialized_2() { return &___unsafeTypeForwardersIsEnabledInitialized_2; }
	inline void set_unsafeTypeForwardersIsEnabledInitialized_2(bool value)
	{
		___unsafeTypeForwardersIsEnabledInitialized_2 = value;
	}

	inline static int32_t get_offset_of_advancedTypes_3() { return static_cast<int32_t>(offsetof(FormatterServices_t305532257_StaticFields, ___advancedTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_advancedTypes_3() const { return ___advancedTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_advancedTypes_3() { return &___advancedTypes_3; }
	inline void set_advancedTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___advancedTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___advancedTypes_3), value);
	}

	inline static int32_t get_offset_of_s_binder_4() { return static_cast<int32_t>(offsetof(FormatterServices_t305532257_StaticFields, ___s_binder_4)); }
	inline Binder_t2999457153 * get_s_binder_4() const { return ___s_binder_4; }
	inline Binder_t2999457153 ** get_address_of_s_binder_4() { return &___s_binder_4; }
	inline void set_s_binder_4(Binder_t2999457153 * value)
	{
		___s_binder_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_binder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERSERVICES_T305532257_H
#ifndef BINARYARRAYTYPEENUM_T1688267430_H
#define BINARYARRAYTYPEENUM_T1688267430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryArrayTypeEnum
struct  BinaryArrayTypeEnum_t1688267430 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArrayTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinaryArrayTypeEnum_t1688267430, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYARRAYTYPEENUM_T1688267430_H
#ifndef BINARYHEADERENUM_T3966655653_H
#define BINARYHEADERENUM_T3966655653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum
struct  BinaryHeaderEnum_t3966655653 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinaryHeaderEnum_t3966655653, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYHEADERENUM_T3966655653_H
#ifndef BINARYTYPEENUM_T3485436454_H
#define BINARYTYPEENUM_T3485436454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum
struct  BinaryTypeEnum_t3485436454 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinaryTypeEnum_t3485436454, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYTYPEENUM_T3485436454_H
#ifndef INTERNALPRIMITIVETYPEE_T4093048977_H
#define INTERNALPRIMITIVETYPEE_T4093048977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE
struct  InternalPrimitiveTypeE_t4093048977 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InternalPrimitiveTypeE_t4093048977, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALPRIMITIVETYPEE_T4093048977_H
#ifndef MESSAGEENUM_T4161186303_H
#define MESSAGEENUM_T4161186303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.MessageEnum
struct  MessageEnum_t4161186303 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.MessageEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageEnum_t4161186303, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEENUM_T4161186303_H
#ifndef FORMATTERASSEMBLYSTYLE_T868039825_H
#define FORMATTERASSEMBLYSTYLE_T868039825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_t868039825 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_t868039825, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_T868039825_H
#ifndef FORMATTERTYPESTYLE_T3400733584_H
#define FORMATTERTYPESTYLE_T3400733584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterTypeStyle
struct  FormatterTypeStyle_t3400733584 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterTypeStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterTypeStyle_t3400733584, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERTYPESTYLE_T3400733584_H
#ifndef TYPEFILTERLEVEL_T977535029_H
#define TYPEFILTERLEVEL_T977535029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.TypeFilterLevel
struct  TypeFilterLevel_t977535029 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.TypeFilterLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeFilterLevel_t977535029, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERLEVEL_T977535029_H
#ifndef SERIALIZATIONEXCEPTION_T3941511869_H
#define SERIALIZATIONEXCEPTION_T3941511869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationException
struct  SerializationException_t3941511869  : public SystemException_t176217640
{
public:

public:
};

struct SerializationException_t3941511869_StaticFields
{
public:
	// System.String System.Runtime.Serialization.SerializationException::_nullMessage
	String_t* ____nullMessage_17;

public:
	inline static int32_t get_offset_of__nullMessage_17() { return static_cast<int32_t>(offsetof(SerializationException_t3941511869_StaticFields, ____nullMessage_17)); }
	inline String_t* get__nullMessage_17() const { return ____nullMessage_17; }
	inline String_t** get_address_of__nullMessage_17() { return &____nullMessage_17; }
	inline void set__nullMessage_17(String_t* value)
	{
		____nullMessage_17 = value;
		Il2CppCodeGenWriteBarrier((&____nullMessage_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONEXCEPTION_T3941511869_H
#ifndef SERIALIZATIONFIELDINFO_T1168344834_H
#define SERIALIZATIONFIELDINFO_T1168344834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationFieldInfo
struct  SerializationFieldInfo_t1168344834  : public FieldInfo_t
{
public:
	// System.Reflection.RuntimeFieldInfo System.Runtime.Serialization.SerializationFieldInfo::m_field
	RuntimeFieldInfo_t4270975280 * ___m_field_0;
	// System.String System.Runtime.Serialization.SerializationFieldInfo::m_serializationName
	String_t* ___m_serializationName_1;

public:
	inline static int32_t get_offset_of_m_field_0() { return static_cast<int32_t>(offsetof(SerializationFieldInfo_t1168344834, ___m_field_0)); }
	inline RuntimeFieldInfo_t4270975280 * get_m_field_0() const { return ___m_field_0; }
	inline RuntimeFieldInfo_t4270975280 ** get_address_of_m_field_0() { return &___m_field_0; }
	inline void set_m_field_0(RuntimeFieldInfo_t4270975280 * value)
	{
		___m_field_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_field_0), value);
	}

	inline static int32_t get_offset_of_m_serializationName_1() { return static_cast<int32_t>(offsetof(SerializationFieldInfo_t1168344834, ___m_serializationName_1)); }
	inline String_t* get_m_serializationName_1() const { return ___m_serializationName_1; }
	inline String_t** get_address_of_m_serializationName_1() { return &___m_serializationName_1; }
	inline void set_m_serializationName_1(String_t* value)
	{
		___m_serializationName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_serializationName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONFIELDINFO_T1168344834_H
#ifndef STREAMINGCONTEXTSTATES_T3580100459_H
#define STREAMINGCONTEXTSTATES_T3580100459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3580100459 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3580100459, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3580100459_H
#ifndef CIPHERMODE_T84635067_H
#define CIPHERMODE_T84635067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t84635067 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t84635067, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T84635067_H
#ifndef CSPALGORITHMTYPE_T2452657849_H
#define CSPALGORITHMTYPE_T2452657849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CspAlgorithmType
struct  CspAlgorithmType_t2452657849 
{
public:
	// System.Int32 System.Security.Cryptography.CspAlgorithmType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CspAlgorithmType_t2452657849, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSPALGORITHMTYPE_T2452657849_H
#ifndef DSACRYPTOSERVICEPROVIDER_T3992668923_H
#define DSACRYPTOSERVICEPROVIDER_T3992668923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DSACryptoServiceProvider
struct  DSACryptoServiceProvider_t3992668923  : public DSA_t2386879874
{
public:
	// Mono.Security.Cryptography.KeyPairPersistence System.Security.Cryptography.DSACryptoServiceProvider::store
	KeyPairPersistence_t2094547461 * ___store_3;
	// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::persistKey
	bool ___persistKey_4;
	// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::persisted
	bool ___persisted_5;
	// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::privateKeyExportable
	bool ___privateKeyExportable_6;
	// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::m_disposed
	bool ___m_disposed_7;
	// Mono.Security.Cryptography.DSAManaged System.Security.Cryptography.DSACryptoServiceProvider::dsa
	DSAManaged_t2800260182 * ___dsa_8;

public:
	inline static int32_t get_offset_of_store_3() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923, ___store_3)); }
	inline KeyPairPersistence_t2094547461 * get_store_3() const { return ___store_3; }
	inline KeyPairPersistence_t2094547461 ** get_address_of_store_3() { return &___store_3; }
	inline void set_store_3(KeyPairPersistence_t2094547461 * value)
	{
		___store_3 = value;
		Il2CppCodeGenWriteBarrier((&___store_3), value);
	}

	inline static int32_t get_offset_of_persistKey_4() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923, ___persistKey_4)); }
	inline bool get_persistKey_4() const { return ___persistKey_4; }
	inline bool* get_address_of_persistKey_4() { return &___persistKey_4; }
	inline void set_persistKey_4(bool value)
	{
		___persistKey_4 = value;
	}

	inline static int32_t get_offset_of_persisted_5() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923, ___persisted_5)); }
	inline bool get_persisted_5() const { return ___persisted_5; }
	inline bool* get_address_of_persisted_5() { return &___persisted_5; }
	inline void set_persisted_5(bool value)
	{
		___persisted_5 = value;
	}

	inline static int32_t get_offset_of_privateKeyExportable_6() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923, ___privateKeyExportable_6)); }
	inline bool get_privateKeyExportable_6() const { return ___privateKeyExportable_6; }
	inline bool* get_address_of_privateKeyExportable_6() { return &___privateKeyExportable_6; }
	inline void set_privateKeyExportable_6(bool value)
	{
		___privateKeyExportable_6 = value;
	}

	inline static int32_t get_offset_of_m_disposed_7() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923, ___m_disposed_7)); }
	inline bool get_m_disposed_7() const { return ___m_disposed_7; }
	inline bool* get_address_of_m_disposed_7() { return &___m_disposed_7; }
	inline void set_m_disposed_7(bool value)
	{
		___m_disposed_7 = value;
	}

	inline static int32_t get_offset_of_dsa_8() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923, ___dsa_8)); }
	inline DSAManaged_t2800260182 * get_dsa_8() const { return ___dsa_8; }
	inline DSAManaged_t2800260182 ** get_address_of_dsa_8() { return &___dsa_8; }
	inline void set_dsa_8(DSAManaged_t2800260182 * value)
	{
		___dsa_8 = value;
		Il2CppCodeGenWriteBarrier((&___dsa_8), value);
	}
};

struct DSACryptoServiceProvider_t3992668923_StaticFields
{
public:
	// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::useMachineKeyStore
	bool ___useMachineKeyStore_9;

public:
	inline static int32_t get_offset_of_useMachineKeyStore_9() { return static_cast<int32_t>(offsetof(DSACryptoServiceProvider_t3992668923_StaticFields, ___useMachineKeyStore_9)); }
	inline bool get_useMachineKeyStore_9() const { return ___useMachineKeyStore_9; }
	inline bool* get_address_of_useMachineKeyStore_9() { return &___useMachineKeyStore_9; }
	inline void set_useMachineKeyStore_9(bool value)
	{
		___useMachineKeyStore_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSACRYPTOSERVICEPROVIDER_T3992668923_H
#ifndef KEYNUMBER_T1560809490_H
#define KEYNUMBER_T1560809490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.KeyNumber
struct  KeyNumber_t1560809490 
{
public:
	// System.Int32 System.Security.Cryptography.KeyNumber::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyNumber_t1560809490, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYNUMBER_T1560809490_H
#ifndef MD5CRYPTOSERVICEPROVIDER_T3005586042_H
#define MD5CRYPTOSERVICEPROVIDER_T3005586042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.MD5CryptoServiceProvider
struct  MD5CryptoServiceProvider_t3005586042  : public MD5_t3177620429
{
public:
	// System.UInt32[] System.Security.Cryptography.MD5CryptoServiceProvider::_H
	UInt32U5BU5D_t2770800703* ____H_5;
	// System.UInt32[] System.Security.Cryptography.MD5CryptoServiceProvider::buff
	UInt32U5BU5D_t2770800703* ___buff_6;
	// System.UInt64 System.Security.Cryptography.MD5CryptoServiceProvider::count
	uint64_t ___count_7;
	// System.Byte[] System.Security.Cryptography.MD5CryptoServiceProvider::_ProcessingBuffer
	ByteU5BU5D_t4116647657* ____ProcessingBuffer_8;
	// System.Int32 System.Security.Cryptography.MD5CryptoServiceProvider::_ProcessingBufferCount
	int32_t ____ProcessingBufferCount_9;

public:
	inline static int32_t get_offset_of__H_5() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ____H_5)); }
	inline UInt32U5BU5D_t2770800703* get__H_5() const { return ____H_5; }
	inline UInt32U5BU5D_t2770800703** get_address_of__H_5() { return &____H_5; }
	inline void set__H_5(UInt32U5BU5D_t2770800703* value)
	{
		____H_5 = value;
		Il2CppCodeGenWriteBarrier((&____H_5), value);
	}

	inline static int32_t get_offset_of_buff_6() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ___buff_6)); }
	inline UInt32U5BU5D_t2770800703* get_buff_6() const { return ___buff_6; }
	inline UInt32U5BU5D_t2770800703** get_address_of_buff_6() { return &___buff_6; }
	inline void set_buff_6(UInt32U5BU5D_t2770800703* value)
	{
		___buff_6 = value;
		Il2CppCodeGenWriteBarrier((&___buff_6), value);
	}

	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ___count_7)); }
	inline uint64_t get_count_7() const { return ___count_7; }
	inline uint64_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(uint64_t value)
	{
		___count_7 = value;
	}

	inline static int32_t get_offset_of__ProcessingBuffer_8() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ____ProcessingBuffer_8)); }
	inline ByteU5BU5D_t4116647657* get__ProcessingBuffer_8() const { return ____ProcessingBuffer_8; }
	inline ByteU5BU5D_t4116647657** get_address_of__ProcessingBuffer_8() { return &____ProcessingBuffer_8; }
	inline void set__ProcessingBuffer_8(ByteU5BU5D_t4116647657* value)
	{
		____ProcessingBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&____ProcessingBuffer_8), value);
	}

	inline static int32_t get_offset_of__ProcessingBufferCount_9() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ____ProcessingBufferCount_9)); }
	inline int32_t get__ProcessingBufferCount_9() const { return ____ProcessingBufferCount_9; }
	inline int32_t* get_address_of__ProcessingBufferCount_9() { return &____ProcessingBufferCount_9; }
	inline void set__ProcessingBufferCount_9(int32_t value)
	{
		____ProcessingBufferCount_9 = value;
	}
};

struct MD5CryptoServiceProvider_t3005586042_StaticFields
{
public:
	// System.UInt32[] System.Security.Cryptography.MD5CryptoServiceProvider::K
	UInt32U5BU5D_t2770800703* ___K_10;

public:
	inline static int32_t get_offset_of_K_10() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042_StaticFields, ___K_10)); }
	inline UInt32U5BU5D_t2770800703* get_K_10() const { return ___K_10; }
	inline UInt32U5BU5D_t2770800703** get_address_of_K_10() { return &___K_10; }
	inline void set_K_10(UInt32U5BU5D_t2770800703* value)
	{
		___K_10 = value;
		Il2CppCodeGenWriteBarrier((&___K_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5CRYPTOSERVICEPROVIDER_T3005586042_H
#ifndef PADDINGMODE_T2546806710_H
#define PADDINGMODE_T2546806710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t2546806710 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_t2546806710, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T2546806710_H
#ifndef RNGCRYPTOSERVICEPROVIDER_T3397414743_H
#define RNGCRYPTOSERVICEPROVIDER_T3397414743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RNGCryptoServiceProvider
struct  RNGCryptoServiceProvider_t3397414743  : public RandomNumberGenerator_t386037858
{
public:
	// System.IntPtr System.Security.Cryptography.RNGCryptoServiceProvider::_handle
	intptr_t ____handle_1;

public:
	inline static int32_t get_offset_of__handle_1() { return static_cast<int32_t>(offsetof(RNGCryptoServiceProvider_t3397414743, ____handle_1)); }
	inline intptr_t get__handle_1() const { return ____handle_1; }
	inline intptr_t* get_address_of__handle_1() { return &____handle_1; }
	inline void set__handle_1(intptr_t value)
	{
		____handle_1 = value;
	}
};

struct RNGCryptoServiceProvider_t3397414743_StaticFields
{
public:
	// System.Object System.Security.Cryptography.RNGCryptoServiceProvider::_lock
	RuntimeObject * ____lock_0;

public:
	inline static int32_t get_offset_of__lock_0() { return static_cast<int32_t>(offsetof(RNGCryptoServiceProvider_t3397414743_StaticFields, ____lock_0)); }
	inline RuntimeObject * get__lock_0() const { return ____lock_0; }
	inline RuntimeObject ** get_address_of__lock_0() { return &____lock_0; }
	inline void set__lock_0(RuntimeObject * value)
	{
		____lock_0 = value;
		Il2CppCodeGenWriteBarrier((&____lock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RNGCRYPTOSERVICEPROVIDER_T3397414743_H
#ifndef SHA1CRYPTOSERVICEPROVIDER_T3661059764_H
#define SHA1CRYPTOSERVICEPROVIDER_T3661059764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SHA1CryptoServiceProvider
struct  SHA1CryptoServiceProvider_t3661059764  : public SHA1_t1803193667
{
public:
	// System.Security.Cryptography.SHA1Internal System.Security.Cryptography.SHA1CryptoServiceProvider::sha
	SHA1Internal_t3251293021 * ___sha_4;

public:
	inline static int32_t get_offset_of_sha_4() { return static_cast<int32_t>(offsetof(SHA1CryptoServiceProvider_t3661059764, ___sha_4)); }
	inline SHA1Internal_t3251293021 * get_sha_4() const { return ___sha_4; }
	inline SHA1Internal_t3251293021 ** get_address_of_sha_4() { return &___sha_4; }
	inline void set_sha_4(SHA1Internal_t3251293021 * value)
	{
		___sha_4 = value;
		Il2CppCodeGenWriteBarrier((&___sha_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA1CRYPTOSERVICEPROVIDER_T3661059764_H
#ifndef OIDGROUP_T1489865352_H
#define OIDGROUP_T1489865352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OidGroup
struct  OidGroup_t1489865352 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OidGroup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OidGroup_t1489865352, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDGROUP_T1489865352_H
#ifndef OIDKEYTYPE_T3827169427_H
#define OIDKEYTYPE_T3827169427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OidKeyType
struct  OidKeyType_t3827169427 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OidKeyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OidKeyType_t3827169427, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDKEYTYPE_T3827169427_H
#ifndef X509CONTENTTYPE_T691322027_H
#define X509CONTENTTYPE_T691322027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ContentType
struct  X509ContentType_t691322027 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509ContentType_t691322027, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CONTENTTYPE_T691322027_H
#ifndef X509KEYSTORAGEFLAGS_T441861693_H
#define X509KEYSTORAGEFLAGS_T441861693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
struct  X509KeyStorageFlags_t441861693 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyStorageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509KeyStorageFlags_t441861693, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYSTORAGEFLAGS_T441861693_H
#ifndef SYMMETRICTRANSFORM_T3802591842_H
#define SYMMETRICTRANSFORM_T3802591842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.SymmetricTransform
struct  SymmetricTransform_t3802591842  : public RuntimeObject
{
public:
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Cryptography.SymmetricTransform::algo
	SymmetricAlgorithm_t4254223087 * ___algo_0;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::encrypt
	bool ___encrypt_1;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::BlockSizeByte
	int32_t ___BlockSizeByte_2;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp
	ByteU5BU5D_t4116647657* ___temp_3;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp2
	ByteU5BU5D_t4116647657* ___temp2_4;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workBuff
	ByteU5BU5D_t4116647657* ___workBuff_5;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workout
	ByteU5BU5D_t4116647657* ___workout_6;
	// System.Security.Cryptography.PaddingMode Mono.Security.Cryptography.SymmetricTransform::padmode
	int32_t ___padmode_7;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::FeedBackByte
	int32_t ___FeedBackByte_8;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::m_disposed
	bool ___m_disposed_9;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::lastBlock
	bool ___lastBlock_10;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.SymmetricTransform::_rng
	RandomNumberGenerator_t386037858 * ____rng_11;

public:
	inline static int32_t get_offset_of_algo_0() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___algo_0)); }
	inline SymmetricAlgorithm_t4254223087 * get_algo_0() const { return ___algo_0; }
	inline SymmetricAlgorithm_t4254223087 ** get_address_of_algo_0() { return &___algo_0; }
	inline void set_algo_0(SymmetricAlgorithm_t4254223087 * value)
	{
		___algo_0 = value;
		Il2CppCodeGenWriteBarrier((&___algo_0), value);
	}

	inline static int32_t get_offset_of_encrypt_1() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___encrypt_1)); }
	inline bool get_encrypt_1() const { return ___encrypt_1; }
	inline bool* get_address_of_encrypt_1() { return &___encrypt_1; }
	inline void set_encrypt_1(bool value)
	{
		___encrypt_1 = value;
	}

	inline static int32_t get_offset_of_BlockSizeByte_2() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___BlockSizeByte_2)); }
	inline int32_t get_BlockSizeByte_2() const { return ___BlockSizeByte_2; }
	inline int32_t* get_address_of_BlockSizeByte_2() { return &___BlockSizeByte_2; }
	inline void set_BlockSizeByte_2(int32_t value)
	{
		___BlockSizeByte_2 = value;
	}

	inline static int32_t get_offset_of_temp_3() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___temp_3)); }
	inline ByteU5BU5D_t4116647657* get_temp_3() const { return ___temp_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_temp_3() { return &___temp_3; }
	inline void set_temp_3(ByteU5BU5D_t4116647657* value)
	{
		___temp_3 = value;
		Il2CppCodeGenWriteBarrier((&___temp_3), value);
	}

	inline static int32_t get_offset_of_temp2_4() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___temp2_4)); }
	inline ByteU5BU5D_t4116647657* get_temp2_4() const { return ___temp2_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_temp2_4() { return &___temp2_4; }
	inline void set_temp2_4(ByteU5BU5D_t4116647657* value)
	{
		___temp2_4 = value;
		Il2CppCodeGenWriteBarrier((&___temp2_4), value);
	}

	inline static int32_t get_offset_of_workBuff_5() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___workBuff_5)); }
	inline ByteU5BU5D_t4116647657* get_workBuff_5() const { return ___workBuff_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_workBuff_5() { return &___workBuff_5; }
	inline void set_workBuff_5(ByteU5BU5D_t4116647657* value)
	{
		___workBuff_5 = value;
		Il2CppCodeGenWriteBarrier((&___workBuff_5), value);
	}

	inline static int32_t get_offset_of_workout_6() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___workout_6)); }
	inline ByteU5BU5D_t4116647657* get_workout_6() const { return ___workout_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_workout_6() { return &___workout_6; }
	inline void set_workout_6(ByteU5BU5D_t4116647657* value)
	{
		___workout_6 = value;
		Il2CppCodeGenWriteBarrier((&___workout_6), value);
	}

	inline static int32_t get_offset_of_padmode_7() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___padmode_7)); }
	inline int32_t get_padmode_7() const { return ___padmode_7; }
	inline int32_t* get_address_of_padmode_7() { return &___padmode_7; }
	inline void set_padmode_7(int32_t value)
	{
		___padmode_7 = value;
	}

	inline static int32_t get_offset_of_FeedBackByte_8() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___FeedBackByte_8)); }
	inline int32_t get_FeedBackByte_8() const { return ___FeedBackByte_8; }
	inline int32_t* get_address_of_FeedBackByte_8() { return &___FeedBackByte_8; }
	inline void set_FeedBackByte_8(int32_t value)
	{
		___FeedBackByte_8 = value;
	}

	inline static int32_t get_offset_of_m_disposed_9() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___m_disposed_9)); }
	inline bool get_m_disposed_9() const { return ___m_disposed_9; }
	inline bool* get_address_of_m_disposed_9() { return &___m_disposed_9; }
	inline void set_m_disposed_9(bool value)
	{
		___m_disposed_9 = value;
	}

	inline static int32_t get_offset_of_lastBlock_10() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ___lastBlock_10)); }
	inline bool get_lastBlock_10() const { return ___lastBlock_10; }
	inline bool* get_address_of_lastBlock_10() { return &___lastBlock_10; }
	inline void set_lastBlock_10(bool value)
	{
		___lastBlock_10 = value;
	}

	inline static int32_t get_offset_of__rng_11() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591842, ____rng_11)); }
	inline RandomNumberGenerator_t386037858 * get__rng_11() const { return ____rng_11; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_11() { return &____rng_11; }
	inline void set__rng_11(RandomNumberGenerator_t386037858 * value)
	{
		____rng_11 = value;
		Il2CppCodeGenWriteBarrier((&____rng_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICTRANSFORM_T3802591842_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef BINARYARRAY_T2657325426_H
#define BINARYARRAY_T2657325426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryArray
struct  BinaryArray_t2657325426  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArray::objectId
	int32_t ___objectId_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArray::rank
	int32_t ___rank_1;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.BinaryArray::lengthA
	Int32U5BU5D_t385246372* ___lengthA_2;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.BinaryArray::lowerBoundA
	Int32U5BU5D_t385246372* ___lowerBoundA_3;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum System.Runtime.Serialization.Formatters.Binary.BinaryArray::binaryTypeEnum
	int32_t ___binaryTypeEnum_4;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryArray::typeInformation
	RuntimeObject * ___typeInformation_5;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryArray::assemId
	int32_t ___assemId_6;
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.BinaryArray::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_7;
	// System.Runtime.Serialization.Formatters.Binary.BinaryArrayTypeEnum System.Runtime.Serialization.Formatters.Binary.BinaryArray::binaryArrayTypeEnum
	int32_t ___binaryArrayTypeEnum_8;

public:
	inline static int32_t get_offset_of_objectId_0() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___objectId_0)); }
	inline int32_t get_objectId_0() const { return ___objectId_0; }
	inline int32_t* get_address_of_objectId_0() { return &___objectId_0; }
	inline void set_objectId_0(int32_t value)
	{
		___objectId_0 = value;
	}

	inline static int32_t get_offset_of_rank_1() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___rank_1)); }
	inline int32_t get_rank_1() const { return ___rank_1; }
	inline int32_t* get_address_of_rank_1() { return &___rank_1; }
	inline void set_rank_1(int32_t value)
	{
		___rank_1 = value;
	}

	inline static int32_t get_offset_of_lengthA_2() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___lengthA_2)); }
	inline Int32U5BU5D_t385246372* get_lengthA_2() const { return ___lengthA_2; }
	inline Int32U5BU5D_t385246372** get_address_of_lengthA_2() { return &___lengthA_2; }
	inline void set_lengthA_2(Int32U5BU5D_t385246372* value)
	{
		___lengthA_2 = value;
		Il2CppCodeGenWriteBarrier((&___lengthA_2), value);
	}

	inline static int32_t get_offset_of_lowerBoundA_3() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___lowerBoundA_3)); }
	inline Int32U5BU5D_t385246372* get_lowerBoundA_3() const { return ___lowerBoundA_3; }
	inline Int32U5BU5D_t385246372** get_address_of_lowerBoundA_3() { return &___lowerBoundA_3; }
	inline void set_lowerBoundA_3(Int32U5BU5D_t385246372* value)
	{
		___lowerBoundA_3 = value;
		Il2CppCodeGenWriteBarrier((&___lowerBoundA_3), value);
	}

	inline static int32_t get_offset_of_binaryTypeEnum_4() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___binaryTypeEnum_4)); }
	inline int32_t get_binaryTypeEnum_4() const { return ___binaryTypeEnum_4; }
	inline int32_t* get_address_of_binaryTypeEnum_4() { return &___binaryTypeEnum_4; }
	inline void set_binaryTypeEnum_4(int32_t value)
	{
		___binaryTypeEnum_4 = value;
	}

	inline static int32_t get_offset_of_typeInformation_5() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___typeInformation_5)); }
	inline RuntimeObject * get_typeInformation_5() const { return ___typeInformation_5; }
	inline RuntimeObject ** get_address_of_typeInformation_5() { return &___typeInformation_5; }
	inline void set_typeInformation_5(RuntimeObject * value)
	{
		___typeInformation_5 = value;
		Il2CppCodeGenWriteBarrier((&___typeInformation_5), value);
	}

	inline static int32_t get_offset_of_assemId_6() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___assemId_6)); }
	inline int32_t get_assemId_6() const { return ___assemId_6; }
	inline int32_t* get_address_of_assemId_6() { return &___assemId_6; }
	inline void set_assemId_6(int32_t value)
	{
		___assemId_6 = value;
	}

	inline static int32_t get_offset_of_binaryHeaderEnum_7() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___binaryHeaderEnum_7)); }
	inline int32_t get_binaryHeaderEnum_7() const { return ___binaryHeaderEnum_7; }
	inline int32_t* get_address_of_binaryHeaderEnum_7() { return &___binaryHeaderEnum_7; }
	inline void set_binaryHeaderEnum_7(int32_t value)
	{
		___binaryHeaderEnum_7 = value;
	}

	inline static int32_t get_offset_of_binaryArrayTypeEnum_8() { return static_cast<int32_t>(offsetof(BinaryArray_t2657325426, ___binaryArrayTypeEnum_8)); }
	inline int32_t get_binaryArrayTypeEnum_8() const { return ___binaryArrayTypeEnum_8; }
	inline int32_t* get_address_of_binaryArrayTypeEnum_8() { return &___binaryArrayTypeEnum_8; }
	inline void set_binaryArrayTypeEnum_8(int32_t value)
	{
		___binaryArrayTypeEnum_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYARRAY_T2657325426_H
#ifndef BINARYMETHODCALL_T649585812_H
#define BINARYMETHODCALL_T649585812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall
struct  BinaryMethodCall_t649585812  : public RuntimeObject
{
public:
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::methodName
	String_t* ___methodName_0;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::typeName
	String_t* ___typeName_1;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::args
	ObjectU5BU5D_t2843939325* ___args_2;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::callContext
	RuntimeObject * ___callContext_3;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::argTypes
	TypeU5BU5D_t3940880105* ___argTypes_4;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::bArgsPrimitive
	bool ___bArgsPrimitive_5;
	// System.Runtime.Serialization.Formatters.Binary.MessageEnum System.Runtime.Serialization.Formatters.Binary.BinaryMethodCall::messageEnum
	int32_t ___messageEnum_6;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}

	inline static int32_t get_offset_of_typeName_1() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___typeName_1)); }
	inline String_t* get_typeName_1() const { return ___typeName_1; }
	inline String_t** get_address_of_typeName_1() { return &___typeName_1; }
	inline void set_typeName_1(String_t* value)
	{
		___typeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___args_2)); }
	inline ObjectU5BU5D_t2843939325* get_args_2() const { return ___args_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(ObjectU5BU5D_t2843939325* value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_callContext_3() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___callContext_3)); }
	inline RuntimeObject * get_callContext_3() const { return ___callContext_3; }
	inline RuntimeObject ** get_address_of_callContext_3() { return &___callContext_3; }
	inline void set_callContext_3(RuntimeObject * value)
	{
		___callContext_3 = value;
		Il2CppCodeGenWriteBarrier((&___callContext_3), value);
	}

	inline static int32_t get_offset_of_argTypes_4() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___argTypes_4)); }
	inline TypeU5BU5D_t3940880105* get_argTypes_4() const { return ___argTypes_4; }
	inline TypeU5BU5D_t3940880105** get_address_of_argTypes_4() { return &___argTypes_4; }
	inline void set_argTypes_4(TypeU5BU5D_t3940880105* value)
	{
		___argTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___argTypes_4), value);
	}

	inline static int32_t get_offset_of_bArgsPrimitive_5() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___bArgsPrimitive_5)); }
	inline bool get_bArgsPrimitive_5() const { return ___bArgsPrimitive_5; }
	inline bool* get_address_of_bArgsPrimitive_5() { return &___bArgsPrimitive_5; }
	inline void set_bArgsPrimitive_5(bool value)
	{
		___bArgsPrimitive_5 = value;
	}

	inline static int32_t get_offset_of_messageEnum_6() { return static_cast<int32_t>(offsetof(BinaryMethodCall_t649585812, ___messageEnum_6)); }
	inline int32_t get_messageEnum_6() const { return ___messageEnum_6; }
	inline int32_t* get_address_of_messageEnum_6() { return &___messageEnum_6; }
	inline void set_messageEnum_6(int32_t value)
	{
		___messageEnum_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYMETHODCALL_T649585812_H
#ifndef BINARYMETHODRETURN_T4246095692_H
#define BINARYMETHODRETURN_T4246095692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn
struct  BinaryMethodReturn_t4246095692  : public RuntimeObject
{
public:
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::returnValue
	RuntimeObject * ___returnValue_0;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::args
	ObjectU5BU5D_t2843939325* ___args_1;
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::callContext
	RuntimeObject * ___callContext_2;
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::argTypes
	TypeU5BU5D_t3940880105* ___argTypes_3;
	// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::bArgsPrimitive
	bool ___bArgsPrimitive_4;
	// System.Runtime.Serialization.Formatters.Binary.MessageEnum System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::messageEnum
	int32_t ___messageEnum_5;
	// System.Type System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::returnType
	Type_t * ___returnType_6;

public:
	inline static int32_t get_offset_of_returnValue_0() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___returnValue_0)); }
	inline RuntimeObject * get_returnValue_0() const { return ___returnValue_0; }
	inline RuntimeObject ** get_address_of_returnValue_0() { return &___returnValue_0; }
	inline void set_returnValue_0(RuntimeObject * value)
	{
		___returnValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___returnValue_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___args_1)); }
	inline ObjectU5BU5D_t2843939325* get_args_1() const { return ___args_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(ObjectU5BU5D_t2843939325* value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}

	inline static int32_t get_offset_of_callContext_2() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___callContext_2)); }
	inline RuntimeObject * get_callContext_2() const { return ___callContext_2; }
	inline RuntimeObject ** get_address_of_callContext_2() { return &___callContext_2; }
	inline void set_callContext_2(RuntimeObject * value)
	{
		___callContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___callContext_2), value);
	}

	inline static int32_t get_offset_of_argTypes_3() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___argTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_argTypes_3() const { return ___argTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_argTypes_3() { return &___argTypes_3; }
	inline void set_argTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___argTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___argTypes_3), value);
	}

	inline static int32_t get_offset_of_bArgsPrimitive_4() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___bArgsPrimitive_4)); }
	inline bool get_bArgsPrimitive_4() const { return ___bArgsPrimitive_4; }
	inline bool* get_address_of_bArgsPrimitive_4() { return &___bArgsPrimitive_4; }
	inline void set_bArgsPrimitive_4(bool value)
	{
		___bArgsPrimitive_4 = value;
	}

	inline static int32_t get_offset_of_messageEnum_5() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___messageEnum_5)); }
	inline int32_t get_messageEnum_5() const { return ___messageEnum_5; }
	inline int32_t* get_address_of_messageEnum_5() { return &___messageEnum_5; }
	inline void set_messageEnum_5(int32_t value)
	{
		___messageEnum_5 = value;
	}

	inline static int32_t get_offset_of_returnType_6() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692, ___returnType_6)); }
	inline Type_t * get_returnType_6() const { return ___returnType_6; }
	inline Type_t ** get_address_of_returnType_6() { return &___returnType_6; }
	inline void set_returnType_6(Type_t * value)
	{
		___returnType_6 = value;
		Il2CppCodeGenWriteBarrier((&___returnType_6), value);
	}
};

struct BinaryMethodReturn_t4246095692_StaticFields
{
public:
	// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryMethodReturn::instanceOfVoid
	RuntimeObject * ___instanceOfVoid_7;

public:
	inline static int32_t get_offset_of_instanceOfVoid_7() { return static_cast<int32_t>(offsetof(BinaryMethodReturn_t4246095692_StaticFields, ___instanceOfVoid_7)); }
	inline RuntimeObject * get_instanceOfVoid_7() const { return ___instanceOfVoid_7; }
	inline RuntimeObject ** get_address_of_instanceOfVoid_7() { return &___instanceOfVoid_7; }
	inline void set_instanceOfVoid_7(RuntimeObject * value)
	{
		___instanceOfVoid_7 = value;
		Il2CppCodeGenWriteBarrier((&___instanceOfVoid_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYMETHODRETURN_T4246095692_H
#ifndef BINARYOBJECTWITHMAP_T407736568_H
#define BINARYOBJECTWITHMAP_T407736568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap
struct  BinaryObjectWithMap_t407736568  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::objectId
	int32_t ___objectId_1;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::name
	String_t* ___name_2;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::numMembers
	int32_t ___numMembers_3;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::memberNames
	StringU5BU5D_t1281789340* ___memberNames_4;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMap::assemId
	int32_t ___assemId_5;

public:
	inline static int32_t get_offset_of_binaryHeaderEnum_0() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t407736568, ___binaryHeaderEnum_0)); }
	inline int32_t get_binaryHeaderEnum_0() const { return ___binaryHeaderEnum_0; }
	inline int32_t* get_address_of_binaryHeaderEnum_0() { return &___binaryHeaderEnum_0; }
	inline void set_binaryHeaderEnum_0(int32_t value)
	{
		___binaryHeaderEnum_0 = value;
	}

	inline static int32_t get_offset_of_objectId_1() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t407736568, ___objectId_1)); }
	inline int32_t get_objectId_1() const { return ___objectId_1; }
	inline int32_t* get_address_of_objectId_1() { return &___objectId_1; }
	inline void set_objectId_1(int32_t value)
	{
		___objectId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t407736568, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_numMembers_3() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t407736568, ___numMembers_3)); }
	inline int32_t get_numMembers_3() const { return ___numMembers_3; }
	inline int32_t* get_address_of_numMembers_3() { return &___numMembers_3; }
	inline void set_numMembers_3(int32_t value)
	{
		___numMembers_3 = value;
	}

	inline static int32_t get_offset_of_memberNames_4() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t407736568, ___memberNames_4)); }
	inline StringU5BU5D_t1281789340* get_memberNames_4() const { return ___memberNames_4; }
	inline StringU5BU5D_t1281789340** get_address_of_memberNames_4() { return &___memberNames_4; }
	inline void set_memberNames_4(StringU5BU5D_t1281789340* value)
	{
		___memberNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___memberNames_4), value);
	}

	inline static int32_t get_offset_of_assemId_5() { return static_cast<int32_t>(offsetof(BinaryObjectWithMap_t407736568, ___assemId_5)); }
	inline int32_t get_assemId_5() const { return ___assemId_5; }
	inline int32_t* get_address_of_assemId_5() { return &___assemId_5; }
	inline void set_assemId_5(int32_t value)
	{
		___assemId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOBJECTWITHMAP_T407736568_H
#ifndef BINARYOBJECTWITHMAPTYPED_T1060316535_H
#define BINARYOBJECTWITHMAPTYPED_T1060316535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped
struct  BinaryObjectWithMapTyped_t1060316535  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::objectId
	int32_t ___objectId_1;
	// System.String System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::name
	String_t* ___name_2;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::numMembers
	int32_t ___numMembers_3;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::memberNames
	StringU5BU5D_t1281789340* ___memberNames_4;
	// System.Runtime.Serialization.Formatters.Binary.BinaryTypeEnum[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::binaryTypeEnumA
	BinaryTypeEnumU5BU5D_t4284693123* ___binaryTypeEnumA_5;
	// System.Object[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::typeInformationA
	ObjectU5BU5D_t2843939325* ___typeInformationA_6;
	// System.Int32[] System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::memberAssemIds
	Int32U5BU5D_t385246372* ___memberAssemIds_7;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.BinaryObjectWithMapTyped::assemId
	int32_t ___assemId_8;

public:
	inline static int32_t get_offset_of_binaryHeaderEnum_0() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___binaryHeaderEnum_0)); }
	inline int32_t get_binaryHeaderEnum_0() const { return ___binaryHeaderEnum_0; }
	inline int32_t* get_address_of_binaryHeaderEnum_0() { return &___binaryHeaderEnum_0; }
	inline void set_binaryHeaderEnum_0(int32_t value)
	{
		___binaryHeaderEnum_0 = value;
	}

	inline static int32_t get_offset_of_objectId_1() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___objectId_1)); }
	inline int32_t get_objectId_1() const { return ___objectId_1; }
	inline int32_t* get_address_of_objectId_1() { return &___objectId_1; }
	inline void set_objectId_1(int32_t value)
	{
		___objectId_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_numMembers_3() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___numMembers_3)); }
	inline int32_t get_numMembers_3() const { return ___numMembers_3; }
	inline int32_t* get_address_of_numMembers_3() { return &___numMembers_3; }
	inline void set_numMembers_3(int32_t value)
	{
		___numMembers_3 = value;
	}

	inline static int32_t get_offset_of_memberNames_4() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___memberNames_4)); }
	inline StringU5BU5D_t1281789340* get_memberNames_4() const { return ___memberNames_4; }
	inline StringU5BU5D_t1281789340** get_address_of_memberNames_4() { return &___memberNames_4; }
	inline void set_memberNames_4(StringU5BU5D_t1281789340* value)
	{
		___memberNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___memberNames_4), value);
	}

	inline static int32_t get_offset_of_binaryTypeEnumA_5() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___binaryTypeEnumA_5)); }
	inline BinaryTypeEnumU5BU5D_t4284693123* get_binaryTypeEnumA_5() const { return ___binaryTypeEnumA_5; }
	inline BinaryTypeEnumU5BU5D_t4284693123** get_address_of_binaryTypeEnumA_5() { return &___binaryTypeEnumA_5; }
	inline void set_binaryTypeEnumA_5(BinaryTypeEnumU5BU5D_t4284693123* value)
	{
		___binaryTypeEnumA_5 = value;
		Il2CppCodeGenWriteBarrier((&___binaryTypeEnumA_5), value);
	}

	inline static int32_t get_offset_of_typeInformationA_6() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___typeInformationA_6)); }
	inline ObjectU5BU5D_t2843939325* get_typeInformationA_6() const { return ___typeInformationA_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_typeInformationA_6() { return &___typeInformationA_6; }
	inline void set_typeInformationA_6(ObjectU5BU5D_t2843939325* value)
	{
		___typeInformationA_6 = value;
		Il2CppCodeGenWriteBarrier((&___typeInformationA_6), value);
	}

	inline static int32_t get_offset_of_memberAssemIds_7() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___memberAssemIds_7)); }
	inline Int32U5BU5D_t385246372* get_memberAssemIds_7() const { return ___memberAssemIds_7; }
	inline Int32U5BU5D_t385246372** get_address_of_memberAssemIds_7() { return &___memberAssemIds_7; }
	inline void set_memberAssemIds_7(Int32U5BU5D_t385246372* value)
	{
		___memberAssemIds_7 = value;
		Il2CppCodeGenWriteBarrier((&___memberAssemIds_7), value);
	}

	inline static int32_t get_offset_of_assemId_8() { return static_cast<int32_t>(offsetof(BinaryObjectWithMapTyped_t1060316535, ___assemId_8)); }
	inline int32_t get_assemId_8() const { return ___assemId_8; }
	inline int32_t* get_address_of_assemId_8() { return &___assemId_8; }
	inline void set_assemId_8(int32_t value)
	{
		___assemId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOBJECTWITHMAPTYPED_T1060316535_H
#ifndef MEMBERPRIMITIVETYPED_T455512373_H
#define MEMBERPRIMITIVETYPED_T455512373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped
struct  MemberPrimitiveTyped_t455512373  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped::primitiveTypeEnum
	int32_t ___primitiveTypeEnum_0;
	// System.Object System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveTyped::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_primitiveTypeEnum_0() { return static_cast<int32_t>(offsetof(MemberPrimitiveTyped_t455512373, ___primitiveTypeEnum_0)); }
	inline int32_t get_primitiveTypeEnum_0() const { return ___primitiveTypeEnum_0; }
	inline int32_t* get_address_of_primitiveTypeEnum_0() { return &___primitiveTypeEnum_0; }
	inline void set_primitiveTypeEnum_0(int32_t value)
	{
		___primitiveTypeEnum_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(MemberPrimitiveTyped_t455512373, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERPRIMITIVETYPED_T455512373_H
#ifndef MEMBERPRIMITIVEUNTYPED_T3912397156_H
#define MEMBERPRIMITIVEUNTYPED_T3912397156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped
struct  MemberPrimitiveUnTyped_t3912397156  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.Formatters.Binary.InternalPrimitiveTypeE System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped::typeInformation
	int32_t ___typeInformation_0;
	// System.Object System.Runtime.Serialization.Formatters.Binary.MemberPrimitiveUnTyped::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_typeInformation_0() { return static_cast<int32_t>(offsetof(MemberPrimitiveUnTyped_t3912397156, ___typeInformation_0)); }
	inline int32_t get_typeInformation_0() const { return ___typeInformation_0; }
	inline int32_t* get_address_of_typeInformation_0() { return &___typeInformation_0; }
	inline void set_typeInformation_0(int32_t value)
	{
		___typeInformation_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(MemberPrimitiveUnTyped_t3912397156, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERPRIMITIVEUNTYPED_T3912397156_H
#ifndef SERIALIZATIONHEADERRECORD_T1200968525_H
#define SERIALIZATIONHEADERRECORD_T1200968525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord
struct  SerializationHeaderRecord_t1200968525  : public RuntimeObject
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::binaryFormatterMajorVersion
	int32_t ___binaryFormatterMajorVersion_0;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::binaryFormatterMinorVersion
	int32_t ___binaryFormatterMinorVersion_1;
	// System.Runtime.Serialization.Formatters.Binary.BinaryHeaderEnum System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::binaryHeaderEnum
	int32_t ___binaryHeaderEnum_2;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::topId
	int32_t ___topId_3;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::headerId
	int32_t ___headerId_4;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::majorVersion
	int32_t ___majorVersion_5;
	// System.Int32 System.Runtime.Serialization.Formatters.Binary.SerializationHeaderRecord::minorVersion
	int32_t ___minorVersion_6;

public:
	inline static int32_t get_offset_of_binaryFormatterMajorVersion_0() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___binaryFormatterMajorVersion_0)); }
	inline int32_t get_binaryFormatterMajorVersion_0() const { return ___binaryFormatterMajorVersion_0; }
	inline int32_t* get_address_of_binaryFormatterMajorVersion_0() { return &___binaryFormatterMajorVersion_0; }
	inline void set_binaryFormatterMajorVersion_0(int32_t value)
	{
		___binaryFormatterMajorVersion_0 = value;
	}

	inline static int32_t get_offset_of_binaryFormatterMinorVersion_1() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___binaryFormatterMinorVersion_1)); }
	inline int32_t get_binaryFormatterMinorVersion_1() const { return ___binaryFormatterMinorVersion_1; }
	inline int32_t* get_address_of_binaryFormatterMinorVersion_1() { return &___binaryFormatterMinorVersion_1; }
	inline void set_binaryFormatterMinorVersion_1(int32_t value)
	{
		___binaryFormatterMinorVersion_1 = value;
	}

	inline static int32_t get_offset_of_binaryHeaderEnum_2() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___binaryHeaderEnum_2)); }
	inline int32_t get_binaryHeaderEnum_2() const { return ___binaryHeaderEnum_2; }
	inline int32_t* get_address_of_binaryHeaderEnum_2() { return &___binaryHeaderEnum_2; }
	inline void set_binaryHeaderEnum_2(int32_t value)
	{
		___binaryHeaderEnum_2 = value;
	}

	inline static int32_t get_offset_of_topId_3() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___topId_3)); }
	inline int32_t get_topId_3() const { return ___topId_3; }
	inline int32_t* get_address_of_topId_3() { return &___topId_3; }
	inline void set_topId_3(int32_t value)
	{
		___topId_3 = value;
	}

	inline static int32_t get_offset_of_headerId_4() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___headerId_4)); }
	inline int32_t get_headerId_4() const { return ___headerId_4; }
	inline int32_t* get_address_of_headerId_4() { return &___headerId_4; }
	inline void set_headerId_4(int32_t value)
	{
		___headerId_4 = value;
	}

	inline static int32_t get_offset_of_majorVersion_5() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___majorVersion_5)); }
	inline int32_t get_majorVersion_5() const { return ___majorVersion_5; }
	inline int32_t* get_address_of_majorVersion_5() { return &___majorVersion_5; }
	inline void set_majorVersion_5(int32_t value)
	{
		___majorVersion_5 = value;
	}

	inline static int32_t get_offset_of_minorVersion_6() { return static_cast<int32_t>(offsetof(SerializationHeaderRecord_t1200968525, ___minorVersion_6)); }
	inline int32_t get_minorVersion_6() const { return ___minorVersion_6; }
	inline int32_t* get_address_of_minorVersion_6() { return &___minorVersion_6; }
	inline void set_minorVersion_6(int32_t value)
	{
		___minorVersion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONHEADERRECORD_T1200968525_H
#ifndef STREAMINGCONTEXT_T3711869237_H
#define STREAMINGCONTEXT_T3711869237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t3711869237 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T3711869237_H
#ifndef SYMMETRICALGORITHM_T4254223087_H
#define SYMMETRICALGORITHM_T4254223087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t4254223087  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_t4116647657* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t722666473* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t722666473* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___IVValue_2)); }
	inline ByteU5BU5D_t4116647657* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_t4116647657* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeyValue_3)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t722666473* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t722666473* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t722666473* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t722666473** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t722666473* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t4254223087, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T4254223087_H
#ifndef DESERIALIZATIONEVENTHANDLER_T1473997819_H
#define DESERIALIZATIONEVENTHANDLER_T1473997819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DeserializationEventHandler
struct  DeserializationEventHandler_t1473997819  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESERIALIZATIONEVENTHANDLER_T1473997819_H
#ifndef MEMBERHOLDER_T2755910714_H
#define MEMBERHOLDER_T2755910714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.MemberHolder
struct  MemberHolder_t2755910714  : public RuntimeObject
{
public:
	// System.Type System.Runtime.Serialization.MemberHolder::memberType
	Type_t * ___memberType_0;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.MemberHolder::context
	StreamingContext_t3711869237  ___context_1;

public:
	inline static int32_t get_offset_of_memberType_0() { return static_cast<int32_t>(offsetof(MemberHolder_t2755910714, ___memberType_0)); }
	inline Type_t * get_memberType_0() const { return ___memberType_0; }
	inline Type_t ** get_address_of_memberType_0() { return &___memberType_0; }
	inline void set_memberType_0(Type_t * value)
	{
		___memberType_0 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(MemberHolder_t2755910714, ___context_1)); }
	inline StreamingContext_t3711869237  get_context_1() const { return ___context_1; }
	inline StreamingContext_t3711869237 * get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(StreamingContext_t3711869237  value)
	{
		___context_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERHOLDER_T2755910714_H
#ifndef OBJECTMANAGER_T1653064325_H
#define OBJECTMANAGER_T1653064325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.ObjectManager
struct  ObjectManager_t1653064325  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.DeserializationEventHandler System.Runtime.Serialization.ObjectManager::m_onDeserializationHandler
	DeserializationEventHandler_t1473997819 * ___m_onDeserializationHandler_0;
	// System.Runtime.Serialization.SerializationEventHandler System.Runtime.Serialization.ObjectManager::m_onDeserializedHandler
	SerializationEventHandler_t2446424469 * ___m_onDeserializedHandler_1;
	// System.Runtime.Serialization.ObjectHolder[] System.Runtime.Serialization.ObjectManager::m_objects
	ObjectHolderU5BU5D_t663564126* ___m_objects_2;
	// System.Object System.Runtime.Serialization.ObjectManager::m_topObject
	RuntimeObject * ___m_topObject_3;
	// System.Runtime.Serialization.ObjectHolderList System.Runtime.Serialization.ObjectManager::m_specialFixupObjects
	ObjectHolderList_t2007846727 * ___m_specialFixupObjects_4;
	// System.Int64 System.Runtime.Serialization.ObjectManager::m_fixupCount
	int64_t ___m_fixupCount_5;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.ObjectManager::m_selector
	RuntimeObject* ___m_selector_6;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.ObjectManager::m_context
	StreamingContext_t3711869237  ___m_context_7;

public:
	inline static int32_t get_offset_of_m_onDeserializationHandler_0() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_onDeserializationHandler_0)); }
	inline DeserializationEventHandler_t1473997819 * get_m_onDeserializationHandler_0() const { return ___m_onDeserializationHandler_0; }
	inline DeserializationEventHandler_t1473997819 ** get_address_of_m_onDeserializationHandler_0() { return &___m_onDeserializationHandler_0; }
	inline void set_m_onDeserializationHandler_0(DeserializationEventHandler_t1473997819 * value)
	{
		___m_onDeserializationHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_onDeserializationHandler_0), value);
	}

	inline static int32_t get_offset_of_m_onDeserializedHandler_1() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_onDeserializedHandler_1)); }
	inline SerializationEventHandler_t2446424469 * get_m_onDeserializedHandler_1() const { return ___m_onDeserializedHandler_1; }
	inline SerializationEventHandler_t2446424469 ** get_address_of_m_onDeserializedHandler_1() { return &___m_onDeserializedHandler_1; }
	inline void set_m_onDeserializedHandler_1(SerializationEventHandler_t2446424469 * value)
	{
		___m_onDeserializedHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_onDeserializedHandler_1), value);
	}

	inline static int32_t get_offset_of_m_objects_2() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_objects_2)); }
	inline ObjectHolderU5BU5D_t663564126* get_m_objects_2() const { return ___m_objects_2; }
	inline ObjectHolderU5BU5D_t663564126** get_address_of_m_objects_2() { return &___m_objects_2; }
	inline void set_m_objects_2(ObjectHolderU5BU5D_t663564126* value)
	{
		___m_objects_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_objects_2), value);
	}

	inline static int32_t get_offset_of_m_topObject_3() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_topObject_3)); }
	inline RuntimeObject * get_m_topObject_3() const { return ___m_topObject_3; }
	inline RuntimeObject ** get_address_of_m_topObject_3() { return &___m_topObject_3; }
	inline void set_m_topObject_3(RuntimeObject * value)
	{
		___m_topObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_topObject_3), value);
	}

	inline static int32_t get_offset_of_m_specialFixupObjects_4() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_specialFixupObjects_4)); }
	inline ObjectHolderList_t2007846727 * get_m_specialFixupObjects_4() const { return ___m_specialFixupObjects_4; }
	inline ObjectHolderList_t2007846727 ** get_address_of_m_specialFixupObjects_4() { return &___m_specialFixupObjects_4; }
	inline void set_m_specialFixupObjects_4(ObjectHolderList_t2007846727 * value)
	{
		___m_specialFixupObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_specialFixupObjects_4), value);
	}

	inline static int32_t get_offset_of_m_fixupCount_5() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_fixupCount_5)); }
	inline int64_t get_m_fixupCount_5() const { return ___m_fixupCount_5; }
	inline int64_t* get_address_of_m_fixupCount_5() { return &___m_fixupCount_5; }
	inline void set_m_fixupCount_5(int64_t value)
	{
		___m_fixupCount_5 = value;
	}

	inline static int32_t get_offset_of_m_selector_6() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_selector_6)); }
	inline RuntimeObject* get_m_selector_6() const { return ___m_selector_6; }
	inline RuntimeObject** get_address_of_m_selector_6() { return &___m_selector_6; }
	inline void set_m_selector_6(RuntimeObject* value)
	{
		___m_selector_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_selector_6), value);
	}

	inline static int32_t get_offset_of_m_context_7() { return static_cast<int32_t>(offsetof(ObjectManager_t1653064325, ___m_context_7)); }
	inline StreamingContext_t3711869237  get_m_context_7() const { return ___m_context_7; }
	inline StreamingContext_t3711869237 * get_address_of_m_context_7() { return &___m_context_7; }
	inline void set_m_context_7(StreamingContext_t3711869237  value)
	{
		___m_context_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTMANAGER_T1653064325_H
#ifndef SAFESERIALIZATIONEVENTARGS_T27819340_H
#define SAFESERIALIZATIONEVENTARGS_T27819340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SafeSerializationEventArgs
struct  SafeSerializationEventArgs_t27819340  : public EventArgs_t3591816995
{
public:
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.SafeSerializationEventArgs::m_streamingContext
	StreamingContext_t3711869237  ___m_streamingContext_1;
	// System.Collections.Generic.List`1<System.Object> System.Runtime.Serialization.SafeSerializationEventArgs::m_serializedStates
	List_1_t257213610 * ___m_serializedStates_2;

public:
	inline static int32_t get_offset_of_m_streamingContext_1() { return static_cast<int32_t>(offsetof(SafeSerializationEventArgs_t27819340, ___m_streamingContext_1)); }
	inline StreamingContext_t3711869237  get_m_streamingContext_1() const { return ___m_streamingContext_1; }
	inline StreamingContext_t3711869237 * get_address_of_m_streamingContext_1() { return &___m_streamingContext_1; }
	inline void set_m_streamingContext_1(StreamingContext_t3711869237  value)
	{
		___m_streamingContext_1 = value;
	}

	inline static int32_t get_offset_of_m_serializedStates_2() { return static_cast<int32_t>(offsetof(SafeSerializationEventArgs_t27819340, ___m_serializedStates_2)); }
	inline List_1_t257213610 * get_m_serializedStates_2() const { return ___m_serializedStates_2; }
	inline List_1_t257213610 ** get_address_of_m_serializedStates_2() { return &___m_serializedStates_2; }
	inline void set_m_serializedStates_2(List_1_t257213610 * value)
	{
		___m_serializedStates_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_serializedStates_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFESERIALIZATIONEVENTARGS_T27819340_H
#ifndef SERIALIZATIONEVENTHANDLER_T2446424469_H
#define SERIALIZATIONEVENTHANDLER_T2446424469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationEventHandler
struct  SerializationEventHandler_t2446424469  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONEVENTHANDLER_T2446424469_H
#ifndef SERIALIZATIONOBJECTMANAGER_T4107654543_H
#define SERIALIZATIONOBJECTMANAGER_T4107654543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationObjectManager
struct  SerializationObjectManager_t4107654543  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationObjectManager::m_objectSeenTable
	Hashtable_t1853889766 * ___m_objectSeenTable_0;
	// System.Runtime.Serialization.SerializationEventHandler System.Runtime.Serialization.SerializationObjectManager::m_onSerializedHandler
	SerializationEventHandler_t2446424469 * ___m_onSerializedHandler_1;
	// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.SerializationObjectManager::m_context
	StreamingContext_t3711869237  ___m_context_2;

public:
	inline static int32_t get_offset_of_m_objectSeenTable_0() { return static_cast<int32_t>(offsetof(SerializationObjectManager_t4107654543, ___m_objectSeenTable_0)); }
	inline Hashtable_t1853889766 * get_m_objectSeenTable_0() const { return ___m_objectSeenTable_0; }
	inline Hashtable_t1853889766 ** get_address_of_m_objectSeenTable_0() { return &___m_objectSeenTable_0; }
	inline void set_m_objectSeenTable_0(Hashtable_t1853889766 * value)
	{
		___m_objectSeenTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectSeenTable_0), value);
	}

	inline static int32_t get_offset_of_m_onSerializedHandler_1() { return static_cast<int32_t>(offsetof(SerializationObjectManager_t4107654543, ___m_onSerializedHandler_1)); }
	inline SerializationEventHandler_t2446424469 * get_m_onSerializedHandler_1() const { return ___m_onSerializedHandler_1; }
	inline SerializationEventHandler_t2446424469 ** get_address_of_m_onSerializedHandler_1() { return &___m_onSerializedHandler_1; }
	inline void set_m_onSerializedHandler_1(SerializationEventHandler_t2446424469 * value)
	{
		___m_onSerializedHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_onSerializedHandler_1), value);
	}

	inline static int32_t get_offset_of_m_context_2() { return static_cast<int32_t>(offsetof(SerializationObjectManager_t4107654543, ___m_context_2)); }
	inline StreamingContext_t3711869237  get_m_context_2() const { return ___m_context_2; }
	inline StreamingContext_t3711869237 * get_address_of_m_context_2() { return &___m_context_2; }
	inline void set_m_context_2(StreamingContext_t3711869237  value)
	{
		___m_context_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONOBJECTMANAGER_T4107654543_H
#ifndef DESTRANSFORM_T4088905499_H
#define DESTRANSFORM_T4088905499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DESTransform
struct  DESTransform_t4088905499  : public SymmetricTransform_t3802591842
{
public:
	// System.Byte[] System.Security.Cryptography.DESTransform::keySchedule
	ByteU5BU5D_t4116647657* ___keySchedule_16;
	// System.Byte[] System.Security.Cryptography.DESTransform::byteBuff
	ByteU5BU5D_t4116647657* ___byteBuff_17;
	// System.UInt32[] System.Security.Cryptography.DESTransform::dwordBuff
	UInt32U5BU5D_t2770800703* ___dwordBuff_18;

public:
	inline static int32_t get_offset_of_keySchedule_16() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499, ___keySchedule_16)); }
	inline ByteU5BU5D_t4116647657* get_keySchedule_16() const { return ___keySchedule_16; }
	inline ByteU5BU5D_t4116647657** get_address_of_keySchedule_16() { return &___keySchedule_16; }
	inline void set_keySchedule_16(ByteU5BU5D_t4116647657* value)
	{
		___keySchedule_16 = value;
		Il2CppCodeGenWriteBarrier((&___keySchedule_16), value);
	}

	inline static int32_t get_offset_of_byteBuff_17() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499, ___byteBuff_17)); }
	inline ByteU5BU5D_t4116647657* get_byteBuff_17() const { return ___byteBuff_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_byteBuff_17() { return &___byteBuff_17; }
	inline void set_byteBuff_17(ByteU5BU5D_t4116647657* value)
	{
		___byteBuff_17 = value;
		Il2CppCodeGenWriteBarrier((&___byteBuff_17), value);
	}

	inline static int32_t get_offset_of_dwordBuff_18() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499, ___dwordBuff_18)); }
	inline UInt32U5BU5D_t2770800703* get_dwordBuff_18() const { return ___dwordBuff_18; }
	inline UInt32U5BU5D_t2770800703** get_address_of_dwordBuff_18() { return &___dwordBuff_18; }
	inline void set_dwordBuff_18(UInt32U5BU5D_t2770800703* value)
	{
		___dwordBuff_18 = value;
		Il2CppCodeGenWriteBarrier((&___dwordBuff_18), value);
	}
};

struct DESTransform_t4088905499_StaticFields
{
public:
	// System.Int32 System.Security.Cryptography.DESTransform::KEY_BIT_SIZE
	int32_t ___KEY_BIT_SIZE_12;
	// System.Int32 System.Security.Cryptography.DESTransform::KEY_BYTE_SIZE
	int32_t ___KEY_BYTE_SIZE_13;
	// System.Int32 System.Security.Cryptography.DESTransform::BLOCK_BIT_SIZE
	int32_t ___BLOCK_BIT_SIZE_14;
	// System.Int32 System.Security.Cryptography.DESTransform::BLOCK_BYTE_SIZE
	int32_t ___BLOCK_BYTE_SIZE_15;
	// System.UInt32[] System.Security.Cryptography.DESTransform::spBoxes
	UInt32U5BU5D_t2770800703* ___spBoxes_19;
	// System.Byte[] System.Security.Cryptography.DESTransform::PC1
	ByteU5BU5D_t4116647657* ___PC1_20;
	// System.Byte[] System.Security.Cryptography.DESTransform::leftRotTotal
	ByteU5BU5D_t4116647657* ___leftRotTotal_21;
	// System.Byte[] System.Security.Cryptography.DESTransform::PC2
	ByteU5BU5D_t4116647657* ___PC2_22;
	// System.UInt32[] System.Security.Cryptography.DESTransform::ipTab
	UInt32U5BU5D_t2770800703* ___ipTab_23;
	// System.UInt32[] System.Security.Cryptography.DESTransform::fpTab
	UInt32U5BU5D_t2770800703* ___fpTab_24;

public:
	inline static int32_t get_offset_of_KEY_BIT_SIZE_12() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___KEY_BIT_SIZE_12)); }
	inline int32_t get_KEY_BIT_SIZE_12() const { return ___KEY_BIT_SIZE_12; }
	inline int32_t* get_address_of_KEY_BIT_SIZE_12() { return &___KEY_BIT_SIZE_12; }
	inline void set_KEY_BIT_SIZE_12(int32_t value)
	{
		___KEY_BIT_SIZE_12 = value;
	}

	inline static int32_t get_offset_of_KEY_BYTE_SIZE_13() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___KEY_BYTE_SIZE_13)); }
	inline int32_t get_KEY_BYTE_SIZE_13() const { return ___KEY_BYTE_SIZE_13; }
	inline int32_t* get_address_of_KEY_BYTE_SIZE_13() { return &___KEY_BYTE_SIZE_13; }
	inline void set_KEY_BYTE_SIZE_13(int32_t value)
	{
		___KEY_BYTE_SIZE_13 = value;
	}

	inline static int32_t get_offset_of_BLOCK_BIT_SIZE_14() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___BLOCK_BIT_SIZE_14)); }
	inline int32_t get_BLOCK_BIT_SIZE_14() const { return ___BLOCK_BIT_SIZE_14; }
	inline int32_t* get_address_of_BLOCK_BIT_SIZE_14() { return &___BLOCK_BIT_SIZE_14; }
	inline void set_BLOCK_BIT_SIZE_14(int32_t value)
	{
		___BLOCK_BIT_SIZE_14 = value;
	}

	inline static int32_t get_offset_of_BLOCK_BYTE_SIZE_15() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___BLOCK_BYTE_SIZE_15)); }
	inline int32_t get_BLOCK_BYTE_SIZE_15() const { return ___BLOCK_BYTE_SIZE_15; }
	inline int32_t* get_address_of_BLOCK_BYTE_SIZE_15() { return &___BLOCK_BYTE_SIZE_15; }
	inline void set_BLOCK_BYTE_SIZE_15(int32_t value)
	{
		___BLOCK_BYTE_SIZE_15 = value;
	}

	inline static int32_t get_offset_of_spBoxes_19() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___spBoxes_19)); }
	inline UInt32U5BU5D_t2770800703* get_spBoxes_19() const { return ___spBoxes_19; }
	inline UInt32U5BU5D_t2770800703** get_address_of_spBoxes_19() { return &___spBoxes_19; }
	inline void set_spBoxes_19(UInt32U5BU5D_t2770800703* value)
	{
		___spBoxes_19 = value;
		Il2CppCodeGenWriteBarrier((&___spBoxes_19), value);
	}

	inline static int32_t get_offset_of_PC1_20() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___PC1_20)); }
	inline ByteU5BU5D_t4116647657* get_PC1_20() const { return ___PC1_20; }
	inline ByteU5BU5D_t4116647657** get_address_of_PC1_20() { return &___PC1_20; }
	inline void set_PC1_20(ByteU5BU5D_t4116647657* value)
	{
		___PC1_20 = value;
		Il2CppCodeGenWriteBarrier((&___PC1_20), value);
	}

	inline static int32_t get_offset_of_leftRotTotal_21() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___leftRotTotal_21)); }
	inline ByteU5BU5D_t4116647657* get_leftRotTotal_21() const { return ___leftRotTotal_21; }
	inline ByteU5BU5D_t4116647657** get_address_of_leftRotTotal_21() { return &___leftRotTotal_21; }
	inline void set_leftRotTotal_21(ByteU5BU5D_t4116647657* value)
	{
		___leftRotTotal_21 = value;
		Il2CppCodeGenWriteBarrier((&___leftRotTotal_21), value);
	}

	inline static int32_t get_offset_of_PC2_22() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___PC2_22)); }
	inline ByteU5BU5D_t4116647657* get_PC2_22() const { return ___PC2_22; }
	inline ByteU5BU5D_t4116647657** get_address_of_PC2_22() { return &___PC2_22; }
	inline void set_PC2_22(ByteU5BU5D_t4116647657* value)
	{
		___PC2_22 = value;
		Il2CppCodeGenWriteBarrier((&___PC2_22), value);
	}

	inline static int32_t get_offset_of_ipTab_23() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___ipTab_23)); }
	inline UInt32U5BU5D_t2770800703* get_ipTab_23() const { return ___ipTab_23; }
	inline UInt32U5BU5D_t2770800703** get_address_of_ipTab_23() { return &___ipTab_23; }
	inline void set_ipTab_23(UInt32U5BU5D_t2770800703* value)
	{
		___ipTab_23 = value;
		Il2CppCodeGenWriteBarrier((&___ipTab_23), value);
	}

	inline static int32_t get_offset_of_fpTab_24() { return static_cast<int32_t>(offsetof(DESTransform_t4088905499_StaticFields, ___fpTab_24)); }
	inline UInt32U5BU5D_t2770800703* get_fpTab_24() const { return ___fpTab_24; }
	inline UInt32U5BU5D_t2770800703** get_address_of_fpTab_24() { return &___fpTab_24; }
	inline void set_fpTab_24(UInt32U5BU5D_t2770800703* value)
	{
		___fpTab_24 = value;
		Il2CppCodeGenWriteBarrier((&___fpTab_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTRANSFORM_T4088905499_H
#ifndef RC2TRANSFORM_T458321487_H
#define RC2TRANSFORM_T458321487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RC2Transform
struct  RC2Transform_t458321487  : public SymmetricTransform_t3802591842
{
public:
	// System.UInt16 System.Security.Cryptography.RC2Transform::R0
	uint16_t ___R0_12;
	// System.UInt16 System.Security.Cryptography.RC2Transform::R1
	uint16_t ___R1_13;
	// System.UInt16 System.Security.Cryptography.RC2Transform::R2
	uint16_t ___R2_14;
	// System.UInt16 System.Security.Cryptography.RC2Transform::R3
	uint16_t ___R3_15;
	// System.UInt16[] System.Security.Cryptography.RC2Transform::K
	UInt16U5BU5D_t3326319531* ___K_16;
	// System.Int32 System.Security.Cryptography.RC2Transform::j
	int32_t ___j_17;

public:
	inline static int32_t get_offset_of_R0_12() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487, ___R0_12)); }
	inline uint16_t get_R0_12() const { return ___R0_12; }
	inline uint16_t* get_address_of_R0_12() { return &___R0_12; }
	inline void set_R0_12(uint16_t value)
	{
		___R0_12 = value;
	}

	inline static int32_t get_offset_of_R1_13() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487, ___R1_13)); }
	inline uint16_t get_R1_13() const { return ___R1_13; }
	inline uint16_t* get_address_of_R1_13() { return &___R1_13; }
	inline void set_R1_13(uint16_t value)
	{
		___R1_13 = value;
	}

	inline static int32_t get_offset_of_R2_14() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487, ___R2_14)); }
	inline uint16_t get_R2_14() const { return ___R2_14; }
	inline uint16_t* get_address_of_R2_14() { return &___R2_14; }
	inline void set_R2_14(uint16_t value)
	{
		___R2_14 = value;
	}

	inline static int32_t get_offset_of_R3_15() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487, ___R3_15)); }
	inline uint16_t get_R3_15() const { return ___R3_15; }
	inline uint16_t* get_address_of_R3_15() { return &___R3_15; }
	inline void set_R3_15(uint16_t value)
	{
		___R3_15 = value;
	}

	inline static int32_t get_offset_of_K_16() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487, ___K_16)); }
	inline UInt16U5BU5D_t3326319531* get_K_16() const { return ___K_16; }
	inline UInt16U5BU5D_t3326319531** get_address_of_K_16() { return &___K_16; }
	inline void set_K_16(UInt16U5BU5D_t3326319531* value)
	{
		___K_16 = value;
		Il2CppCodeGenWriteBarrier((&___K_16), value);
	}

	inline static int32_t get_offset_of_j_17() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487, ___j_17)); }
	inline int32_t get_j_17() const { return ___j_17; }
	inline int32_t* get_address_of_j_17() { return &___j_17; }
	inline void set_j_17(int32_t value)
	{
		___j_17 = value;
	}
};

struct RC2Transform_t458321487_StaticFields
{
public:
	// System.Byte[] System.Security.Cryptography.RC2Transform::pitable
	ByteU5BU5D_t4116647657* ___pitable_18;

public:
	inline static int32_t get_offset_of_pitable_18() { return static_cast<int32_t>(offsetof(RC2Transform_t458321487_StaticFields, ___pitable_18)); }
	inline ByteU5BU5D_t4116647657* get_pitable_18() const { return ___pitable_18; }
	inline ByteU5BU5D_t4116647657** get_address_of_pitable_18() { return &___pitable_18; }
	inline void set_pitable_18(ByteU5BU5D_t4116647657* value)
	{
		___pitable_18 = value;
		Il2CppCodeGenWriteBarrier((&___pitable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC2TRANSFORM_T458321487_H
#ifndef TRIPLEDES_T92303514_H
#define TRIPLEDES_T92303514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.TripleDES
struct  TripleDES_t92303514  : public SymmetricAlgorithm_t4254223087
{
public:

public:
};

struct TripleDES_t92303514_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.TripleDES::s_legalBlockSizes
	KeySizesU5BU5D_t722666473* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.TripleDES::s_legalKeySizes
	KeySizesU5BU5D_t722666473* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(TripleDES_t92303514_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_9), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(TripleDES_t92303514_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t722666473* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t722666473** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t722666473* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPLEDES_T92303514_H
#ifndef TRIPLEDESTRANSFORM_T964169060_H
#define TRIPLEDESTRANSFORM_T964169060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.TripleDESTransform
struct  TripleDESTransform_t964169060  : public SymmetricTransform_t3802591842
{
public:
	// System.Security.Cryptography.DESTransform System.Security.Cryptography.TripleDESTransform::E1
	DESTransform_t4088905499 * ___E1_12;
	// System.Security.Cryptography.DESTransform System.Security.Cryptography.TripleDESTransform::D2
	DESTransform_t4088905499 * ___D2_13;
	// System.Security.Cryptography.DESTransform System.Security.Cryptography.TripleDESTransform::E3
	DESTransform_t4088905499 * ___E3_14;
	// System.Security.Cryptography.DESTransform System.Security.Cryptography.TripleDESTransform::D1
	DESTransform_t4088905499 * ___D1_15;
	// System.Security.Cryptography.DESTransform System.Security.Cryptography.TripleDESTransform::E2
	DESTransform_t4088905499 * ___E2_16;
	// System.Security.Cryptography.DESTransform System.Security.Cryptography.TripleDESTransform::D3
	DESTransform_t4088905499 * ___D3_17;

public:
	inline static int32_t get_offset_of_E1_12() { return static_cast<int32_t>(offsetof(TripleDESTransform_t964169060, ___E1_12)); }
	inline DESTransform_t4088905499 * get_E1_12() const { return ___E1_12; }
	inline DESTransform_t4088905499 ** get_address_of_E1_12() { return &___E1_12; }
	inline void set_E1_12(DESTransform_t4088905499 * value)
	{
		___E1_12 = value;
		Il2CppCodeGenWriteBarrier((&___E1_12), value);
	}

	inline static int32_t get_offset_of_D2_13() { return static_cast<int32_t>(offsetof(TripleDESTransform_t964169060, ___D2_13)); }
	inline DESTransform_t4088905499 * get_D2_13() const { return ___D2_13; }
	inline DESTransform_t4088905499 ** get_address_of_D2_13() { return &___D2_13; }
	inline void set_D2_13(DESTransform_t4088905499 * value)
	{
		___D2_13 = value;
		Il2CppCodeGenWriteBarrier((&___D2_13), value);
	}

	inline static int32_t get_offset_of_E3_14() { return static_cast<int32_t>(offsetof(TripleDESTransform_t964169060, ___E3_14)); }
	inline DESTransform_t4088905499 * get_E3_14() const { return ___E3_14; }
	inline DESTransform_t4088905499 ** get_address_of_E3_14() { return &___E3_14; }
	inline void set_E3_14(DESTransform_t4088905499 * value)
	{
		___E3_14 = value;
		Il2CppCodeGenWriteBarrier((&___E3_14), value);
	}

	inline static int32_t get_offset_of_D1_15() { return static_cast<int32_t>(offsetof(TripleDESTransform_t964169060, ___D1_15)); }
	inline DESTransform_t4088905499 * get_D1_15() const { return ___D1_15; }
	inline DESTransform_t4088905499 ** get_address_of_D1_15() { return &___D1_15; }
	inline void set_D1_15(DESTransform_t4088905499 * value)
	{
		___D1_15 = value;
		Il2CppCodeGenWriteBarrier((&___D1_15), value);
	}

	inline static int32_t get_offset_of_E2_16() { return static_cast<int32_t>(offsetof(TripleDESTransform_t964169060, ___E2_16)); }
	inline DESTransform_t4088905499 * get_E2_16() const { return ___E2_16; }
	inline DESTransform_t4088905499 ** get_address_of_E2_16() { return &___E2_16; }
	inline void set_E2_16(DESTransform_t4088905499 * value)
	{
		___E2_16 = value;
		Il2CppCodeGenWriteBarrier((&___E2_16), value);
	}

	inline static int32_t get_offset_of_D3_17() { return static_cast<int32_t>(offsetof(TripleDESTransform_t964169060, ___D3_17)); }
	inline DESTransform_t4088905499 * get_D3_17() const { return ___D3_17; }
	inline DESTransform_t4088905499 ** get_address_of_D3_17() { return &___D3_17; }
	inline void set_D3_17(DESTransform_t4088905499 * value)
	{
		___D3_17 = value;
		Il2CppCodeGenWriteBarrier((&___D3_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPLEDESTRANSFORM_T964169060_H
#ifndef TRIPLEDESCRYPTOSERVICEPROVIDER_T3595206342_H
#define TRIPLEDESCRYPTOSERVICEPROVIDER_T3595206342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.TripleDESCryptoServiceProvider
struct  TripleDESCryptoServiceProvider_t3595206342  : public TripleDES_t92303514
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPLEDESCRYPTOSERVICEPROVIDER_T3595206342_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (DSASignatureDescription_t1163053634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (SymmetricAlgorithm_t4254223087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1001[9] = 
{
	SymmetricAlgorithm_t4254223087::get_offset_of_BlockSizeValue_0(),
	SymmetricAlgorithm_t4254223087::get_offset_of_FeedbackSizeValue_1(),
	SymmetricAlgorithm_t4254223087::get_offset_of_IVValue_2(),
	SymmetricAlgorithm_t4254223087::get_offset_of_KeyValue_3(),
	SymmetricAlgorithm_t4254223087::get_offset_of_LegalBlockSizesValue_4(),
	SymmetricAlgorithm_t4254223087::get_offset_of_LegalKeySizesValue_5(),
	SymmetricAlgorithm_t4254223087::get_offset_of_KeySizeValue_6(),
	SymmetricAlgorithm_t4254223087::get_offset_of_ModeValue_7(),
	SymmetricAlgorithm_t4254223087::get_offset_of_PaddingValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (TripleDES_t92303514), -1, sizeof(TripleDES_t92303514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1002[2] = 
{
	TripleDES_t92303514_StaticFields::get_offset_of_s_legalBlockSizes_9(),
	TripleDES_t92303514_StaticFields::get_offset_of_s_legalKeySizes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (TripleDESCryptoServiceProvider_t3595206342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (CspAlgorithmType_t2452657849)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1004[3] = 
{
	CspAlgorithmType_t2452657849::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (Constants_t3997854879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1005[69] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (Utils_t1416439708), -1, sizeof(Utils_t1416439708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1006[2] = 
{
	0,
	Utils_t1416439708_StaticFields::get_offset_of__rng_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (CryptoAPITransform_t1695680317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1007[1] = 
{
	CryptoAPITransform_t1695680317::get_offset_of_m_disposed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (CryptoConfig_t4201145714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (CspKeyContainerInfo_t3833902945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1009[2] = 
{
	CspKeyContainerInfo_t3833902945::get_offset_of__params_0(),
	CspKeyContainerInfo_t3833902945::get_offset_of__random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (DESTransform_t4088905499), -1, sizeof(DESTransform_t4088905499_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1010[13] = 
{
	DESTransform_t4088905499_StaticFields::get_offset_of_KEY_BIT_SIZE_12(),
	DESTransform_t4088905499_StaticFields::get_offset_of_KEY_BYTE_SIZE_13(),
	DESTransform_t4088905499_StaticFields::get_offset_of_BLOCK_BIT_SIZE_14(),
	DESTransform_t4088905499_StaticFields::get_offset_of_BLOCK_BYTE_SIZE_15(),
	DESTransform_t4088905499::get_offset_of_keySchedule_16(),
	DESTransform_t4088905499::get_offset_of_byteBuff_17(),
	DESTransform_t4088905499::get_offset_of_dwordBuff_18(),
	DESTransform_t4088905499_StaticFields::get_offset_of_spBoxes_19(),
	DESTransform_t4088905499_StaticFields::get_offset_of_PC1_20(),
	DESTransform_t4088905499_StaticFields::get_offset_of_leftRotTotal_21(),
	DESTransform_t4088905499_StaticFields::get_offset_of_PC2_22(),
	DESTransform_t4088905499_StaticFields::get_offset_of_ipTab_23(),
	DESTransform_t4088905499_StaticFields::get_offset_of_fpTab_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (DSACryptoServiceProvider_t3992668923), -1, sizeof(DSACryptoServiceProvider_t3992668923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1011[8] = 
{
	0,
	DSACryptoServiceProvider_t3992668923::get_offset_of_store_3(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_persistKey_4(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_persisted_5(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_privateKeyExportable_6(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_m_disposed_7(),
	DSACryptoServiceProvider_t3992668923::get_offset_of_dsa_8(),
	DSACryptoServiceProvider_t3992668923_StaticFields::get_offset_of_useMachineKeyStore_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (KeyNumber_t1560809490)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1013[3] = 
{
	KeyNumber_t1560809490::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { sizeof (MD5CryptoServiceProvider_t3005586042), -1, sizeof(MD5CryptoServiceProvider_t3005586042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1014[7] = 
{
	0,
	MD5CryptoServiceProvider_t3005586042::get_offset_of__H_5(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of_buff_6(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of_count_7(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of__ProcessingBuffer_8(),
	MD5CryptoServiceProvider_t3005586042::get_offset_of__ProcessingBufferCount_9(),
	MD5CryptoServiceProvider_t3005586042_StaticFields::get_offset_of_K_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (RC2Transform_t458321487), -1, sizeof(RC2Transform_t458321487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1015[7] = 
{
	RC2Transform_t458321487::get_offset_of_R0_12(),
	RC2Transform_t458321487::get_offset_of_R1_13(),
	RC2Transform_t458321487::get_offset_of_R2_14(),
	RC2Transform_t458321487::get_offset_of_R3_15(),
	RC2Transform_t458321487::get_offset_of_K_16(),
	RC2Transform_t458321487::get_offset_of_j_17(),
	RC2Transform_t458321487_StaticFields::get_offset_of_pitable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (RNGCryptoServiceProvider_t3397414743), -1, sizeof(RNGCryptoServiceProvider_t3397414743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1016[2] = 
{
	RNGCryptoServiceProvider_t3397414743_StaticFields::get_offset_of__lock_0(),
	RNGCryptoServiceProvider_t3397414743::get_offset_of__handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (RSAPKCS1SignatureDeformatter_t3767223008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1017[2] = 
{
	RSAPKCS1SignatureDeformatter_t3767223008::get_offset_of_rsa_0(),
	RSAPKCS1SignatureDeformatter_t3767223008::get_offset_of_hashName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (RSAPKCS1SignatureFormatter_t1514197062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1018[2] = 
{
	RSAPKCS1SignatureFormatter_t1514197062::get_offset_of_rsa_0(),
	RSAPKCS1SignatureFormatter_t1514197062::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (SHA1Internal_t3251293021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1019[6] = 
{
	0,
	SHA1Internal_t3251293021::get_offset_of__H_1(),
	SHA1Internal_t3251293021::get_offset_of_count_2(),
	SHA1Internal_t3251293021::get_offset_of__ProcessingBuffer_3(),
	SHA1Internal_t3251293021::get_offset_of__ProcessingBufferCount_4(),
	SHA1Internal_t3251293021::get_offset_of_buff_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (SHA1CryptoServiceProvider_t3661059764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1020[1] = 
{
	SHA1CryptoServiceProvider_t3661059764::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (TripleDESTransform_t964169060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1021[6] = 
{
	TripleDESTransform_t964169060::get_offset_of_E1_12(),
	TripleDESTransform_t964169060::get_offset_of_D2_13(),
	TripleDESTransform_t964169060::get_offset_of_E3_14(),
	TripleDESTransform_t964169060::get_offset_of_D1_15(),
	TripleDESTransform_t964169060::get_offset_of_E2_16(),
	TripleDESTransform_t964169060::get_offset_of_D3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (X509ContentType_t691322027)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1022[9] = 
{
	X509ContentType_t691322027::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (X509KeyStorageFlags_t441861693)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1023[8] = 
{
	X509KeyStorageFlags_t441861693::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (X509Constants_t648943299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1024[40] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (OidGroup_t1489865352)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1025[13] = 
{
	OidGroup_t1489865352::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (OidKeyType_t3827169427)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1026[7] = 
{
	OidKeyType_t3827169427::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (X509Certificate_t713131622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1028[4] = 
{
	X509Certificate_t713131622::get_offset_of_impl_0(),
	X509Certificate_t713131622::get_offset_of_hideDates_1(),
	X509Certificate_t713131622::get_offset_of_issuer_name_2(),
	X509Certificate_t713131622::get_offset_of_subject_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (X509CertificateImpl_t2851385038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1029[1] = 
{
	X509CertificateImpl_t2851385038::get_offset_of_cachedCertificateHash_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (X509CertificateImplMono_t285361170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1030[1] = 
{
	X509CertificateImplMono_t285361170::get_offset_of_x509_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (X509Helper_t1273321433), -1, sizeof(X509Helper_t1273321433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1031[1] = 
{
	X509Helper_t1273321433_StaticFields::get_offset_of_nativeHelper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (BinaryCompatibility_t3378813580), -1, sizeof(BinaryCompatibility_t3378813580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1032[2] = 
{
	BinaryCompatibility_t3378813580_StaticFields::get_offset_of_TargetsAtLeast_Desktop_V4_5_0(),
	BinaryCompatibility_t3378813580_StaticFields::get_offset_of_TargetsAtLeast_Desktop_V4_5_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { sizeof (DeserializationEventHandler_t1473997819), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { sizeof (SerializationEventHandler_t2446424469), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { sizeof (FormatterConverter_t2760117746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { sizeof (FormatterServices_t305532257), -1, sizeof(FormatterServices_t305532257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1036[5] = 
{
	FormatterServices_t305532257_StaticFields::get_offset_of_m_MemberInfoTable_0(),
	FormatterServices_t305532257_StaticFields::get_offset_of_unsafeTypeForwardersIsEnabled_1(),
	FormatterServices_t305532257_StaticFields::get_offset_of_unsafeTypeForwardersIsEnabledInitialized_2(),
	FormatterServices_t305532257_StaticFields::get_offset_of_advancedTypes_3(),
	FormatterServices_t305532257_StaticFields::get_offset_of_s_binder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (U3CU3Ec__DisplayClass9_0_t3680548263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1037[1] = 
{
	U3CU3Ec__DisplayClass9_0_t3680548263::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (SurrogateForCyclicalReference_t3309482836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1038[1] = 
{
	SurrogateForCyclicalReference_t3309482836::get_offset_of_innerSurrogate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (MemberHolder_t2755910714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1046[2] = 
{
	MemberHolder_t2755910714::get_offset_of_memberType_0(),
	MemberHolder_t2755910714::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (ObjectIDGenerator_t1260826161), -1, sizeof(ObjectIDGenerator_t1260826161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1047[5] = 
{
	ObjectIDGenerator_t1260826161::get_offset_of_m_currentCount_0(),
	ObjectIDGenerator_t1260826161::get_offset_of_m_currentSize_1(),
	ObjectIDGenerator_t1260826161::get_offset_of_m_ids_2(),
	ObjectIDGenerator_t1260826161::get_offset_of_m_objs_3(),
	ObjectIDGenerator_t1260826161_StaticFields::get_offset_of_sizes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (ObjectManager_t1653064325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1048[8] = 
{
	ObjectManager_t1653064325::get_offset_of_m_onDeserializationHandler_0(),
	ObjectManager_t1653064325::get_offset_of_m_onDeserializedHandler_1(),
	ObjectManager_t1653064325::get_offset_of_m_objects_2(),
	ObjectManager_t1653064325::get_offset_of_m_topObject_3(),
	ObjectManager_t1653064325::get_offset_of_m_specialFixupObjects_4(),
	ObjectManager_t1653064325::get_offset_of_m_fixupCount_5(),
	ObjectManager_t1653064325::get_offset_of_m_selector_6(),
	ObjectManager_t1653064325::get_offset_of_m_context_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (ObjectHolder_t2801344551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1049[14] = 
{
	ObjectHolder_t2801344551::get_offset_of_m_object_0(),
	ObjectHolder_t2801344551::get_offset_of_m_id_1(),
	ObjectHolder_t2801344551::get_offset_of_m_missingElementsRemaining_2(),
	ObjectHolder_t2801344551::get_offset_of_m_missingDecendents_3(),
	ObjectHolder_t2801344551::get_offset_of_m_serInfo_4(),
	ObjectHolder_t2801344551::get_offset_of_m_surrogate_5(),
	ObjectHolder_t2801344551::get_offset_of_m_missingElements_6(),
	ObjectHolder_t2801344551::get_offset_of_m_dependentObjects_7(),
	ObjectHolder_t2801344551::get_offset_of_m_next_8(),
	ObjectHolder_t2801344551::get_offset_of_m_flags_9(),
	ObjectHolder_t2801344551::get_offset_of_m_markForFixupWhenAvailable_10(),
	ObjectHolder_t2801344551::get_offset_of_m_valueFixup_11(),
	ObjectHolder_t2801344551::get_offset_of_m_typeLoad_12(),
	ObjectHolder_t2801344551::get_offset_of_m_reachable_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (FixupHolder_t3112688910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1050[3] = 
{
	FixupHolder_t3112688910::get_offset_of_m_id_0(),
	FixupHolder_t3112688910::get_offset_of_m_fixupInfo_1(),
	FixupHolder_t3112688910::get_offset_of_m_fixupType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (FixupHolderList_t3799028159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1051[2] = 
{
	FixupHolderList_t3799028159::get_offset_of_m_values_0(),
	FixupHolderList_t3799028159::get_offset_of_m_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { sizeof (LongList_t2258305620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1052[4] = 
{
	LongList_t2258305620::get_offset_of_m_values_0(),
	LongList_t2258305620::get_offset_of_m_count_1(),
	LongList_t2258305620::get_offset_of_m_totalItems_2(),
	LongList_t2258305620::get_offset_of_m_currentItem_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (ObjectHolderList_t2007846727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1053[2] = 
{
	ObjectHolderList_t2007846727::get_offset_of_m_values_0(),
	ObjectHolderList_t2007846727::get_offset_of_m_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (ObjectHolderListEnumerator_t501693956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1054[4] = 
{
	ObjectHolderListEnumerator_t501693956::get_offset_of_m_isFixupEnumerator_0(),
	ObjectHolderListEnumerator_t501693956::get_offset_of_m_list_1(),
	ObjectHolderListEnumerator_t501693956::get_offset_of_m_startingVersion_2(),
	ObjectHolderListEnumerator_t501693956::get_offset_of_m_currPos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (TypeLoadExceptionHolder_t2983813904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1055[1] = 
{
	TypeLoadExceptionHolder_t2983813904::get_offset_of_m_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (SafeSerializationEventArgs_t27819340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1056[2] = 
{
	SafeSerializationEventArgs_t27819340::get_offset_of_m_streamingContext_1(),
	SafeSerializationEventArgs_t27819340::get_offset_of_m_serializedStates_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (SafeSerializationManager_t2481557153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1058[5] = 
{
	SafeSerializationManager_t2481557153::get_offset_of_m_serializedStates_0(),
	SafeSerializationManager_t2481557153::get_offset_of_m_savedSerializationInfo_1(),
	SafeSerializationManager_t2481557153::get_offset_of_m_realObject_2(),
	SafeSerializationManager_t2481557153::get_offset_of_m_realType_3(),
	SafeSerializationManager_t2481557153::get_offset_of_SerializeObjectState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (OptionalFieldAttribute_t761606048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1059[1] = 
{
	OptionalFieldAttribute_t761606048::get_offset_of_versionAdded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (OnSerializingAttribute_t2580696919), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (OnSerializedAttribute_t2595932830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (OnDeserializingAttribute_t338753086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { sizeof (OnDeserializedAttribute_t1335880599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { sizeof (SerializationBinder_t274213469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { sizeof (SerializationEvents_t3591775508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1065[4] = 
{
	SerializationEvents_t3591775508::get_offset_of_m_OnSerializingMethods_0(),
	SerializationEvents_t3591775508::get_offset_of_m_OnSerializedMethods_1(),
	SerializationEvents_t3591775508::get_offset_of_m_OnDeserializingMethods_2(),
	SerializationEvents_t3591775508::get_offset_of_m_OnDeserializedMethods_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (SerializationEventsCache_t2327969880), -1, sizeof(SerializationEventsCache_t2327969880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1066[1] = 
{
	SerializationEventsCache_t2327969880_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (SerializationException_t3941511869), -1, sizeof(SerializationException_t3941511869_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1067[1] = 
{
	SerializationException_t3941511869_StaticFields::get_offset_of__nullMessage_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (SerializationFieldInfo_t1168344834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1068[2] = 
{
	SerializationFieldInfo_t1168344834::get_offset_of_m_field_0(),
	SerializationFieldInfo_t1168344834::get_offset_of_m_serializationName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (SerializationInfo_t950877179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1069[15] = 
{
	0,
	0,
	0,
	SerializationInfo_t950877179::get_offset_of_m_members_3(),
	SerializationInfo_t950877179::get_offset_of_m_data_4(),
	SerializationInfo_t950877179::get_offset_of_m_types_5(),
	SerializationInfo_t950877179::get_offset_of_m_nameToIndex_6(),
	SerializationInfo_t950877179::get_offset_of_m_currMember_7(),
	SerializationInfo_t950877179::get_offset_of_m_converter_8(),
	SerializationInfo_t950877179::get_offset_of_m_fullTypeName_9(),
	SerializationInfo_t950877179::get_offset_of_m_assemName_10(),
	SerializationInfo_t950877179::get_offset_of_objectType_11(),
	SerializationInfo_t950877179::get_offset_of_isFullTypeNameSetExplicit_12(),
	SerializationInfo_t950877179::get_offset_of_isAssemblyNameSetExplicit_13(),
	SerializationInfo_t950877179::get_offset_of_requireSameTokenInPartialTrust_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (SerializationEntry_t648286436)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1070[3] = 
{
	SerializationEntry_t648286436::get_offset_of_m_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationEntry_t648286436::get_offset_of_m_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationEntry_t648286436::get_offset_of_m_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (SerializationInfoEnumerator_t2232395945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1071[6] = 
{
	SerializationInfoEnumerator_t2232395945::get_offset_of_m_members_0(),
	SerializationInfoEnumerator_t2232395945::get_offset_of_m_data_1(),
	SerializationInfoEnumerator_t2232395945::get_offset_of_m_types_2(),
	SerializationInfoEnumerator_t2232395945::get_offset_of_m_numItems_3(),
	SerializationInfoEnumerator_t2232395945::get_offset_of_m_currItem_4(),
	SerializationInfoEnumerator_t2232395945::get_offset_of_m_current_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (SerializationObjectManager_t4107654543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1072[3] = 
{
	SerializationObjectManager_t4107654543::get_offset_of_m_objectSeenTable_0(),
	SerializationObjectManager_t4107654543::get_offset_of_m_onSerializedHandler_1(),
	SerializationObjectManager_t4107654543::get_offset_of_m_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (StreamingContext_t3711869237)+ sizeof (RuntimeObject), sizeof(StreamingContext_t3711869237_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1073[2] = 
{
	StreamingContext_t3711869237::get_offset_of_m_additionalContext_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StreamingContext_t3711869237::get_offset_of_m_state_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (StreamingContextStates_t3580100459)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1074[10] = 
{
	StreamingContextStates_t3580100459::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (ValueTypeFixupInfo_t1444618334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1075[3] = 
{
	ValueTypeFixupInfo_t1444618334::get_offset_of_m_containerID_0(),
	ValueTypeFixupInfo_t1444618334::get_offset_of_m_parentField_1(),
	ValueTypeFixupInfo_t1444618334::get_offset_of_m_parentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (FormatterTypeStyle_t3400733584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1076[4] = 
{
	FormatterTypeStyle_t3400733584::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (FormatterAssemblyStyle_t868039825)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1077[3] = 
{
	FormatterAssemblyStyle_t868039825::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (TypeFilterLevel_t977535029)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1078[3] = 
{
	TypeFilterLevel_t977535029::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (BinaryConverter_t4078671247), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (IOUtil_t3677430991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (BinaryAssemblyInfo_t1476540191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1082[2] = 
{
	BinaryAssemblyInfo_t1476540191::get_offset_of_assemblyString_0(),
	BinaryAssemblyInfo_t1476540191::get_offset_of_assembly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (SerializationHeaderRecord_t1200968525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1083[7] = 
{
	SerializationHeaderRecord_t1200968525::get_offset_of_binaryFormatterMajorVersion_0(),
	SerializationHeaderRecord_t1200968525::get_offset_of_binaryFormatterMinorVersion_1(),
	SerializationHeaderRecord_t1200968525::get_offset_of_binaryHeaderEnum_2(),
	SerializationHeaderRecord_t1200968525::get_offset_of_topId_3(),
	SerializationHeaderRecord_t1200968525::get_offset_of_headerId_4(),
	SerializationHeaderRecord_t1200968525::get_offset_of_majorVersion_5(),
	SerializationHeaderRecord_t1200968525::get_offset_of_minorVersion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (BinaryAssembly_t798593476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1084[2] = 
{
	BinaryAssembly_t798593476::get_offset_of_assemId_0(),
	BinaryAssembly_t798593476::get_offset_of_assemblyString_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (BinaryCrossAppDomainAssembly_t2006046530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1085[2] = 
{
	BinaryCrossAppDomainAssembly_t2006046530::get_offset_of_assemId_0(),
	BinaryCrossAppDomainAssembly_t2006046530::get_offset_of_assemblyIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (BinaryObject_t4088053983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1086[2] = 
{
	BinaryObject_t4088053983::get_offset_of_objectId_0(),
	BinaryObject_t4088053983::get_offset_of_mapId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (BinaryMethodCall_t649585812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1087[7] = 
{
	BinaryMethodCall_t649585812::get_offset_of_methodName_0(),
	BinaryMethodCall_t649585812::get_offset_of_typeName_1(),
	BinaryMethodCall_t649585812::get_offset_of_args_2(),
	BinaryMethodCall_t649585812::get_offset_of_callContext_3(),
	BinaryMethodCall_t649585812::get_offset_of_argTypes_4(),
	BinaryMethodCall_t649585812::get_offset_of_bArgsPrimitive_5(),
	BinaryMethodCall_t649585812::get_offset_of_messageEnum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (BinaryMethodReturn_t4246095692), -1, sizeof(BinaryMethodReturn_t4246095692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1088[8] = 
{
	BinaryMethodReturn_t4246095692::get_offset_of_returnValue_0(),
	BinaryMethodReturn_t4246095692::get_offset_of_args_1(),
	BinaryMethodReturn_t4246095692::get_offset_of_callContext_2(),
	BinaryMethodReturn_t4246095692::get_offset_of_argTypes_3(),
	BinaryMethodReturn_t4246095692::get_offset_of_bArgsPrimitive_4(),
	BinaryMethodReturn_t4246095692::get_offset_of_messageEnum_5(),
	BinaryMethodReturn_t4246095692::get_offset_of_returnType_6(),
	BinaryMethodReturn_t4246095692_StaticFields::get_offset_of_instanceOfVoid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (BinaryObjectString_t3941408528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1089[2] = 
{
	BinaryObjectString_t3941408528::get_offset_of_objectId_0(),
	BinaryObjectString_t3941408528::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (BinaryCrossAppDomainString_t3774953123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1090[2] = 
{
	BinaryCrossAppDomainString_t3774953123::get_offset_of_objectId_0(),
	BinaryCrossAppDomainString_t3774953123::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { sizeof (BinaryCrossAppDomainMap_t2942608216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1091[1] = 
{
	BinaryCrossAppDomainMap_t2942608216::get_offset_of_crossAppDomainArrayIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (MemberPrimitiveTyped_t455512373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1092[2] = 
{
	MemberPrimitiveTyped_t455512373::get_offset_of_primitiveTypeEnum_0(),
	MemberPrimitiveTyped_t455512373::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (BinaryObjectWithMap_t407736568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1093[6] = 
{
	BinaryObjectWithMap_t407736568::get_offset_of_binaryHeaderEnum_0(),
	BinaryObjectWithMap_t407736568::get_offset_of_objectId_1(),
	BinaryObjectWithMap_t407736568::get_offset_of_name_2(),
	BinaryObjectWithMap_t407736568::get_offset_of_numMembers_3(),
	BinaryObjectWithMap_t407736568::get_offset_of_memberNames_4(),
	BinaryObjectWithMap_t407736568::get_offset_of_assemId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (BinaryObjectWithMapTyped_t1060316535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1094[9] = 
{
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_binaryHeaderEnum_0(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_objectId_1(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_name_2(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_numMembers_3(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_memberNames_4(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_binaryTypeEnumA_5(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_typeInformationA_6(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_memberAssemIds_7(),
	BinaryObjectWithMapTyped_t1060316535::get_offset_of_assemId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (BinaryArray_t2657325426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1095[9] = 
{
	BinaryArray_t2657325426::get_offset_of_objectId_0(),
	BinaryArray_t2657325426::get_offset_of_rank_1(),
	BinaryArray_t2657325426::get_offset_of_lengthA_2(),
	BinaryArray_t2657325426::get_offset_of_lowerBoundA_3(),
	BinaryArray_t2657325426::get_offset_of_binaryTypeEnum_4(),
	BinaryArray_t2657325426::get_offset_of_typeInformation_5(),
	BinaryArray_t2657325426::get_offset_of_assemId_6(),
	BinaryArray_t2657325426::get_offset_of_binaryHeaderEnum_7(),
	BinaryArray_t2657325426::get_offset_of_binaryArrayTypeEnum_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (MemberPrimitiveUnTyped_t3912397156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1096[2] = 
{
	MemberPrimitiveUnTyped_t3912397156::get_offset_of_typeInformation_0(),
	MemberPrimitiveUnTyped_t3912397156::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (MemberReference_t3613131682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1097[1] = 
{
	MemberReference_t3613131682::get_offset_of_idRef_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (ObjectNull_t728940006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1098[1] = 
{
	ObjectNull_t728940006::get_offset_of_nullCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (MessageEnd_t4164135406), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
