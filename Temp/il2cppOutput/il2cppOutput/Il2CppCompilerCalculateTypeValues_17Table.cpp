﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Net.Security.AsyncProtocolRequest
struct AsyncProtocolRequest_t4184368197;
// Mono.Net.Security.BufferOffsetSize
struct BufferOffsetSize_t3399811029;
// Mono.Net.Security.BufferOffsetSize2
struct BufferOffsetSize2_t2574020620;
// Mono.Net.Security.MobileAuthenticatedStream
struct MobileAuthenticatedStream_t3383979266;
// Mono.Net.Security.MobileTlsContext
struct MobileTlsContext_t1069274405;
// Mono.Net.Security.MonoTlsStream
struct MonoTlsStream_t1980138907;
// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct ServerCertValidationCallbackWrapper_t1850490691;
// Mono.Security.Interface.ICertificateValidator2
struct ICertificateValidator2_t849006458;
// Mono.Security.Interface.IMonoSslStream
struct IMonoSslStream_t1819859871;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t1375878923;
// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t2521872312;
// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_t3152003291;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t3666008581;
// Mono.Security.Protocol.Ntlm.MessageBase
struct MessageBase_t422981883;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.Dictionary`2<System.Guid,Mono.Security.Interface.MonoTlsProvider>
struct Dictionary_2_t2201020824;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,System.Tuple`2<System.Guid,System.String>>
struct Dictionary_2_t3262373276;
// System.Collections.Generic.Dictionary`2<System.String,System.UriParser>
struct Dictionary_2_t3675406699;
// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry>
struct LinkedList_1_t3068621835;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange>
struct List_1_t1270793844;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexNode>
struct List_1_t1470514689;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions>
struct List_1_t1564920337;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t156472862;
// System.IOAsyncCallback
struct IOAsyncCallback_t705871752;
// System.IOAsyncResult
struct IOAsyncResult_t3640145766;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3365920845;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1669436515;
// System.Net.Security.LocalCertSelectionCallback
struct LocalCertSelectionCallback_t1988113036;
// System.Net.Security.SslStream
struct SslStream_t2700741536;
// System.Net.ServerCertValidationCallback
struct ServerCertValidationCallback_t1488468298;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t4071955934;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.ConditionalWeakTable`2/CreateValueCallback<System.Net.HttpWebRequest,Mono.Http.NtlmSession>
struct CreateValueCallback_t1559961266;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Net.HttpWebRequest,Mono.Http.NtlmSession>
struct ConditionalWeakTable_2_t4032781979;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t923100567;
// System.Runtime.ExceptionServices.ExceptionDispatchInfo
struct ExceptionDispatchInfo_t3750997369;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t713131622;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t3399372417;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t194917408;
// System.String
struct String_t;
// System.String[0...,0...]
struct StringU5B0___U2C0___U5D_t1281789341;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t1760593541;
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t183267399;
// System.Text.RegularExpressions.ExclusiveReference
struct ExclusiveReference_t1927754563;
// System.Text.RegularExpressions.Group
struct Group_t2468205786;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t69770484;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1880820351;
// System.Text.RegularExpressions.Match
struct Match_t3408321083;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1395363720;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Text.RegularExpressions.RegexBoyerMoore
struct RegexBoyerMoore_t1480609368;
// System.Text.RegularExpressions.RegexCharClass
struct RegexCharClass_t2088948945;
// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping[]
struct LowerCaseMappingU5BU5D_t2345228286;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t4293407246;
// System.Text.RegularExpressions.RegexFC[]
struct RegexFCU5BU5D_t3722514598;
// System.Text.RegularExpressions.RegexNode
struct RegexNode_t4293407243;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t3750605839;
// System.Text.RegularExpressions.RegexRunner
struct RegexRunner_t300319648;
// System.Text.RegularExpressions.RegexRunnerFactory
struct RegexRunnerFactory_t51159052;
// System.Text.RegularExpressions.SharedReference
struct SharedReference_t2916547576;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_t540272775;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2974092902;
// System.Threading.Tasks.Task
struct Task_t3187275312;
// System.Threading.Tasks.Task`1<Mono.Net.Security.AsyncProtocolResult>
struct Task_1_t493370259;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t61518632;
// System.Threading.Tasks.Task`1<System.Nullable`1<System.Int32>>
struct Task_1_t1784080714;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t4022128754;
// System.Uri
struct Uri_t100236324;
// System.Uri/MoreInfo
struct MoreInfo_t2349391856;
// System.Uri/UriInfo
struct UriInfo_t1092684687;
// System.UriParser
struct UriParser_t3890150400;
// System.Void
struct Void_t1185182177;
// System.WeakReference
struct WeakReference_t1334886716;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct IOAsyncResult_t3640145766_marshaled_com;
struct IOAsyncResult_t3640145766_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NTLMCLIENT_T1228044663_H
#define NTLMCLIENT_T1228044663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Http.NtlmClient
struct  NtlmClient_t1228044663  : public RuntimeObject
{
public:

public:
};

struct NtlmClient_t1228044663_StaticFields
{
public:
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Net.HttpWebRequest,Mono.Http.NtlmSession> Mono.Http.NtlmClient::cache
	ConditionalWeakTable_2_t4032781979 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(NtlmClient_t1228044663_StaticFields, ___cache_0)); }
	inline ConditionalWeakTable_2_t4032781979 * get_cache_0() const { return ___cache_0; }
	inline ConditionalWeakTable_2_t4032781979 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(ConditionalWeakTable_2_t4032781979 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMCLIENT_T1228044663_H
#ifndef U3CU3EC_T1220335544_H
#define U3CU3EC_T1220335544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Http.NtlmClient/<>c
struct  U3CU3Ec_t1220335544  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1220335544_StaticFields
{
public:
	// Mono.Http.NtlmClient/<>c Mono.Http.NtlmClient/<>c::<>9
	U3CU3Ec_t1220335544 * ___U3CU3E9_0;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2/CreateValueCallback<System.Net.HttpWebRequest,Mono.Http.NtlmSession> Mono.Http.NtlmClient/<>c::<>9__1_0
	CreateValueCallback_t1559961266 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1220335544_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1220335544 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1220335544 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1220335544 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1220335544_StaticFields, ___U3CU3E9__1_0_1)); }
	inline CreateValueCallback_t1559961266 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline CreateValueCallback_t1559961266 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(CreateValueCallback_t1559961266 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1220335544_H
#ifndef NTLMSESSION_T2676553120_H
#define NTLMSESSION_T2676553120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Http.NtlmSession
struct  NtlmSession_t2676553120  : public RuntimeObject
{
public:
	// Mono.Security.Protocol.Ntlm.MessageBase Mono.Http.NtlmSession::message
	MessageBase_t422981883 * ___message_0;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(NtlmSession_t2676553120, ___message_0)); }
	inline MessageBase_t422981883 * get_message_0() const { return ___message_0; }
	inline MessageBase_t422981883 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(MessageBase_t422981883 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMSESSION_T2676553120_H
#ifndef ASYNCPROTOCOLREQUEST_T4184368197_H
#define ASYNCPROTOCOLREQUEST_T4184368197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncProtocolRequest
struct  AsyncProtocolRequest_t4184368197  : public RuntimeObject
{
public:
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.AsyncProtocolRequest::<Parent>k__BackingField
	MobileAuthenticatedStream_t3383979266 * ___U3CParentU3Ek__BackingField_0;
	// System.Boolean Mono.Net.Security.AsyncProtocolRequest::<RunSynchronously>k__BackingField
	bool ___U3CRunSynchronouslyU3Ek__BackingField_1;
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest::<UserResult>k__BackingField
	int32_t ___U3CUserResultU3Ek__BackingField_2;
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest::Started
	int32_t ___Started_3;
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest::RequestedSize
	int32_t ___RequestedSize_4;
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest::WriteRequested
	int32_t ___WriteRequested_5;
	// System.Object Mono.Net.Security.AsyncProtocolRequest::locker
	RuntimeObject * ___locker_6;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___U3CParentU3Ek__BackingField_0)); }
	inline MobileAuthenticatedStream_t3383979266 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(MobileAuthenticatedStream_t3383979266 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRunSynchronouslyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___U3CRunSynchronouslyU3Ek__BackingField_1)); }
	inline bool get_U3CRunSynchronouslyU3Ek__BackingField_1() const { return ___U3CRunSynchronouslyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CRunSynchronouslyU3Ek__BackingField_1() { return &___U3CRunSynchronouslyU3Ek__BackingField_1; }
	inline void set_U3CRunSynchronouslyU3Ek__BackingField_1(bool value)
	{
		___U3CRunSynchronouslyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUserResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___U3CUserResultU3Ek__BackingField_2)); }
	inline int32_t get_U3CUserResultU3Ek__BackingField_2() const { return ___U3CUserResultU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CUserResultU3Ek__BackingField_2() { return &___U3CUserResultU3Ek__BackingField_2; }
	inline void set_U3CUserResultU3Ek__BackingField_2(int32_t value)
	{
		___U3CUserResultU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_Started_3() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___Started_3)); }
	inline int32_t get_Started_3() const { return ___Started_3; }
	inline int32_t* get_address_of_Started_3() { return &___Started_3; }
	inline void set_Started_3(int32_t value)
	{
		___Started_3 = value;
	}

	inline static int32_t get_offset_of_RequestedSize_4() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___RequestedSize_4)); }
	inline int32_t get_RequestedSize_4() const { return ___RequestedSize_4; }
	inline int32_t* get_address_of_RequestedSize_4() { return &___RequestedSize_4; }
	inline void set_RequestedSize_4(int32_t value)
	{
		___RequestedSize_4 = value;
	}

	inline static int32_t get_offset_of_WriteRequested_5() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___WriteRequested_5)); }
	inline int32_t get_WriteRequested_5() const { return ___WriteRequested_5; }
	inline int32_t* get_address_of_WriteRequested_5() { return &___WriteRequested_5; }
	inline void set_WriteRequested_5(int32_t value)
	{
		___WriteRequested_5 = value;
	}

	inline static int32_t get_offset_of_locker_6() { return static_cast<int32_t>(offsetof(AsyncProtocolRequest_t4184368197, ___locker_6)); }
	inline RuntimeObject * get_locker_6() const { return ___locker_6; }
	inline RuntimeObject ** get_address_of_locker_6() { return &___locker_6; }
	inline void set_locker_6(RuntimeObject * value)
	{
		___locker_6 = value;
		Il2CppCodeGenWriteBarrier((&___locker_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCPROTOCOLREQUEST_T4184368197_H
#ifndef ASYNCPROTOCOLRESULT_T3382797380_H
#define ASYNCPROTOCOLRESULT_T3382797380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncProtocolResult
struct  AsyncProtocolResult_t3382797380  : public RuntimeObject
{
public:
	// System.Int32 Mono.Net.Security.AsyncProtocolResult::<UserResult>k__BackingField
	int32_t ___U3CUserResultU3Ek__BackingField_0;
	// System.Runtime.ExceptionServices.ExceptionDispatchInfo Mono.Net.Security.AsyncProtocolResult::<Error>k__BackingField
	ExceptionDispatchInfo_t3750997369 * ___U3CErrorU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUserResultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncProtocolResult_t3382797380, ___U3CUserResultU3Ek__BackingField_0)); }
	inline int32_t get_U3CUserResultU3Ek__BackingField_0() const { return ___U3CUserResultU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CUserResultU3Ek__BackingField_0() { return &___U3CUserResultU3Ek__BackingField_0; }
	inline void set_U3CUserResultU3Ek__BackingField_0(int32_t value)
	{
		___U3CUserResultU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncProtocolResult_t3382797380, ___U3CErrorU3Ek__BackingField_1)); }
	inline ExceptionDispatchInfo_t3750997369 * get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline ExceptionDispatchInfo_t3750997369 ** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(ExceptionDispatchInfo_t3750997369 * value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCPROTOCOLRESULT_T3382797380_H
#ifndef CHAINVALIDATIONHELPER_T669322361_H
#define CHAINVALIDATIONHELPER_T669322361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ChainValidationHelper
struct  ChainValidationHelper_t669322361  : public RuntimeObject
{
public:
	// System.Object Mono.Net.Security.ChainValidationHelper::sender
	RuntimeObject * ___sender_0;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.ChainValidationHelper::settings
	MonoTlsSettings_t3666008581 * ___settings_1;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.ChainValidationHelper::provider
	MonoTlsProvider_t3152003291 * ___provider_2;
	// System.Net.ServerCertValidationCallback Mono.Net.Security.ChainValidationHelper::certValidationCallback
	ServerCertValidationCallback_t1488468298 * ___certValidationCallback_3;
	// System.Net.Security.LocalCertSelectionCallback Mono.Net.Security.ChainValidationHelper::certSelectionCallback
	LocalCertSelectionCallback_t1988113036 * ___certSelectionCallback_4;
	// Mono.Net.Security.ServerCertValidationCallbackWrapper Mono.Net.Security.ChainValidationHelper::callbackWrapper
	ServerCertValidationCallbackWrapper_t1850490691 * ___callbackWrapper_5;
	// Mono.Net.Security.MonoTlsStream Mono.Net.Security.ChainValidationHelper::tlsStream
	MonoTlsStream_t1980138907 * ___tlsStream_6;
	// System.Net.HttpWebRequest Mono.Net.Security.ChainValidationHelper::request
	HttpWebRequest_t1669436515 * ___request_7;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___settings_1)); }
	inline MonoTlsSettings_t3666008581 * get_settings_1() const { return ___settings_1; }
	inline MonoTlsSettings_t3666008581 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(MonoTlsSettings_t3666008581 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}

	inline static int32_t get_offset_of_provider_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___provider_2)); }
	inline MonoTlsProvider_t3152003291 * get_provider_2() const { return ___provider_2; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_provider_2() { return &___provider_2; }
	inline void set_provider_2(MonoTlsProvider_t3152003291 * value)
	{
		___provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___provider_2), value);
	}

	inline static int32_t get_offset_of_certValidationCallback_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___certValidationCallback_3)); }
	inline ServerCertValidationCallback_t1488468298 * get_certValidationCallback_3() const { return ___certValidationCallback_3; }
	inline ServerCertValidationCallback_t1488468298 ** get_address_of_certValidationCallback_3() { return &___certValidationCallback_3; }
	inline void set_certValidationCallback_3(ServerCertValidationCallback_t1488468298 * value)
	{
		___certValidationCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___certValidationCallback_3), value);
	}

	inline static int32_t get_offset_of_certSelectionCallback_4() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___certSelectionCallback_4)); }
	inline LocalCertSelectionCallback_t1988113036 * get_certSelectionCallback_4() const { return ___certSelectionCallback_4; }
	inline LocalCertSelectionCallback_t1988113036 ** get_address_of_certSelectionCallback_4() { return &___certSelectionCallback_4; }
	inline void set_certSelectionCallback_4(LocalCertSelectionCallback_t1988113036 * value)
	{
		___certSelectionCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___certSelectionCallback_4), value);
	}

	inline static int32_t get_offset_of_callbackWrapper_5() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___callbackWrapper_5)); }
	inline ServerCertValidationCallbackWrapper_t1850490691 * get_callbackWrapper_5() const { return ___callbackWrapper_5; }
	inline ServerCertValidationCallbackWrapper_t1850490691 ** get_address_of_callbackWrapper_5() { return &___callbackWrapper_5; }
	inline void set_callbackWrapper_5(ServerCertValidationCallbackWrapper_t1850490691 * value)
	{
		___callbackWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___callbackWrapper_5), value);
	}

	inline static int32_t get_offset_of_tlsStream_6() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___tlsStream_6)); }
	inline MonoTlsStream_t1980138907 * get_tlsStream_6() const { return ___tlsStream_6; }
	inline MonoTlsStream_t1980138907 ** get_address_of_tlsStream_6() { return &___tlsStream_6; }
	inline void set_tlsStream_6(MonoTlsStream_t1980138907 * value)
	{
		___tlsStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___tlsStream_6), value);
	}

	inline static int32_t get_offset_of_request_7() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t669322361, ___request_7)); }
	inline HttpWebRequest_t1669436515 * get_request_7() const { return ___request_7; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_7() { return &___request_7; }
	inline void set_request_7(HttpWebRequest_t1669436515 * value)
	{
		___request_7 = value;
		Il2CppCodeGenWriteBarrier((&___request_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAINVALIDATIONHELPER_T669322361_H
#ifndef U3CU3EC__DISPLAYCLASS66_0_T2450223149_H
#define U3CU3EC__DISPLAYCLASS66_0_T2450223149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream/<>c__DisplayClass66_0
struct  U3CU3Ec__DisplayClass66_0_t2450223149  : public RuntimeObject
{
public:
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileAuthenticatedStream/<>c__DisplayClass66_0::<>4__this
	MobileAuthenticatedStream_t3383979266 * ___U3CU3E4__this_0;
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<>c__DisplayClass66_0::len
	int32_t ___len_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t2450223149, ___U3CU3E4__this_0)); }
	inline MobileAuthenticatedStream_t3383979266 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(MobileAuthenticatedStream_t3383979266 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_len_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t2450223149, ___len_1)); }
	inline int32_t get_len_1() const { return ___len_1; }
	inline int32_t* get_address_of_len_1() { return &___len_1; }
	inline void set_len_1(int32_t value)
	{
		___len_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS66_0_T2450223149_H
#ifndef NOREFLECTIONHELPER_T2046157126_H
#define NOREFLECTIONHELPER_T2046157126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.NoReflectionHelper
struct  NoReflectionHelper_t2046157126  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOREFLECTIONHELPER_T2046157126_H
#ifndef CALLBACKHELPERS_T1924962770_H
#define CALLBACKHELPERS_T1924962770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers
struct  CallbackHelpers_t1924962770  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKHELPERS_T1924962770_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T2247204716_H
#define U3CU3EC__DISPLAYCLASS5_0_T2247204716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t2247204716  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass5_0::callback
	MonoRemoteCertificateValidationCallback_t2521872312 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2247204716, ___callback_0)); }
	inline MonoRemoteCertificateValidationCallback_t2521872312 * get_callback_0() const { return ___callback_0; }
	inline MonoRemoteCertificateValidationCallback_t2521872312 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoRemoteCertificateValidationCallback_t2521872312 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T2247204716_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T3821182828_H
#define U3CU3EC__DISPLAYCLASS8_0_T3821182828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t3821182828  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass8_0::callback
	MonoLocalCertificateSelectionCallback_t1375878923 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t3821182828, ___callback_0)); }
	inline MonoLocalCertificateSelectionCallback_t1375878923 * get_callback_0() const { return ___callback_0; }
	inline MonoLocalCertificateSelectionCallback_t1375878923 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoLocalCertificateSelectionCallback_t1375878923 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T3821182828_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef STOPWATCH_T305734070_H
#define STOPWATCH_T305734070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Stopwatch
struct  Stopwatch_t305734070  : public RuntimeObject
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::elapsed
	int64_t ___elapsed_2;
	// System.Int64 System.Diagnostics.Stopwatch::started
	int64_t ___started_3;
	// System.Boolean System.Diagnostics.Stopwatch::is_running
	bool ___is_running_4;

public:
	inline static int32_t get_offset_of_elapsed_2() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070, ___elapsed_2)); }
	inline int64_t get_elapsed_2() const { return ___elapsed_2; }
	inline int64_t* get_address_of_elapsed_2() { return &___elapsed_2; }
	inline void set_elapsed_2(int64_t value)
	{
		___elapsed_2 = value;
	}

	inline static int32_t get_offset_of_started_3() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070, ___started_3)); }
	inline int64_t get_started_3() const { return ___started_3; }
	inline int64_t* get_address_of_started_3() { return &___started_3; }
	inline void set_started_3(int64_t value)
	{
		___started_3 = value;
	}

	inline static int32_t get_offset_of_is_running_4() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070, ___is_running_4)); }
	inline bool get_is_running_4() const { return ___is_running_4; }
	inline bool* get_address_of_is_running_4() { return &___is_running_4; }
	inline void set_is_running_4(bool value)
	{
		___is_running_4 = value;
	}
};

struct Stopwatch_t305734070_StaticFields
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::Frequency
	int64_t ___Frequency_0;
	// System.Boolean System.Diagnostics.Stopwatch::IsHighResolution
	bool ___IsHighResolution_1;

public:
	inline static int32_t get_offset_of_Frequency_0() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070_StaticFields, ___Frequency_0)); }
	inline int64_t get_Frequency_0() const { return ___Frequency_0; }
	inline int64_t* get_address_of_Frequency_0() { return &___Frequency_0; }
	inline void set_Frequency_0(int64_t value)
	{
		___Frequency_0 = value;
	}

	inline static int32_t get_offset_of_IsHighResolution_1() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070_StaticFields, ___IsHighResolution_1)); }
	inline bool get_IsHighResolution_1() const { return ___IsHighResolution_1; }
	inline bool* get_address_of_IsHighResolution_1() { return &___IsHighResolution_1; }
	inline void set_IsHighResolution_1(bool value)
	{
		___IsHighResolution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPWATCH_T305734070_H
#ifndef DOMAINNAMEHELPER_T3334322841_H
#define DOMAINNAMEHELPER_T3334322841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DomainNameHelper
struct  DomainNameHelper_t3334322841  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOMAINNAMEHELPER_T3334322841_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef IOASYNCRESULT_T3640145766_H
#define IOASYNCRESULT_T3640145766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOAsyncResult
struct  IOAsyncResult_t3640145766  : public RuntimeObject
{
public:
	// System.AsyncCallback System.IOAsyncResult::async_callback
	AsyncCallback_t3962456242 * ___async_callback_0;
	// System.Object System.IOAsyncResult::async_state
	RuntimeObject * ___async_state_1;
	// System.Threading.ManualResetEvent System.IOAsyncResult::wait_handle
	ManualResetEvent_t451242010 * ___wait_handle_2;
	// System.Boolean System.IOAsyncResult::completed_synchronously
	bool ___completed_synchronously_3;
	// System.Boolean System.IOAsyncResult::completed
	bool ___completed_4;

public:
	inline static int32_t get_offset_of_async_callback_0() { return static_cast<int32_t>(offsetof(IOAsyncResult_t3640145766, ___async_callback_0)); }
	inline AsyncCallback_t3962456242 * get_async_callback_0() const { return ___async_callback_0; }
	inline AsyncCallback_t3962456242 ** get_address_of_async_callback_0() { return &___async_callback_0; }
	inline void set_async_callback_0(AsyncCallback_t3962456242 * value)
	{
		___async_callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___async_callback_0), value);
	}

	inline static int32_t get_offset_of_async_state_1() { return static_cast<int32_t>(offsetof(IOAsyncResult_t3640145766, ___async_state_1)); }
	inline RuntimeObject * get_async_state_1() const { return ___async_state_1; }
	inline RuntimeObject ** get_address_of_async_state_1() { return &___async_state_1; }
	inline void set_async_state_1(RuntimeObject * value)
	{
		___async_state_1 = value;
		Il2CppCodeGenWriteBarrier((&___async_state_1), value);
	}

	inline static int32_t get_offset_of_wait_handle_2() { return static_cast<int32_t>(offsetof(IOAsyncResult_t3640145766, ___wait_handle_2)); }
	inline ManualResetEvent_t451242010 * get_wait_handle_2() const { return ___wait_handle_2; }
	inline ManualResetEvent_t451242010 ** get_address_of_wait_handle_2() { return &___wait_handle_2; }
	inline void set_wait_handle_2(ManualResetEvent_t451242010 * value)
	{
		___wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___wait_handle_2), value);
	}

	inline static int32_t get_offset_of_completed_synchronously_3() { return static_cast<int32_t>(offsetof(IOAsyncResult_t3640145766, ___completed_synchronously_3)); }
	inline bool get_completed_synchronously_3() const { return ___completed_synchronously_3; }
	inline bool* get_address_of_completed_synchronously_3() { return &___completed_synchronously_3; }
	inline void set_completed_synchronously_3(bool value)
	{
		___completed_synchronously_3 = value;
	}

	inline static int32_t get_offset_of_completed_4() { return static_cast<int32_t>(offsetof(IOAsyncResult_t3640145766, ___completed_4)); }
	inline bool get_completed_4() const { return ___completed_4; }
	inline bool* get_address_of_completed_4() { return &___completed_4; }
	inline void set_completed_4(bool value)
	{
		___completed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IOAsyncResult
struct IOAsyncResult_t3640145766_marshaled_pinvoke
{
	Il2CppMethodPointer ___async_callback_0;
	Il2CppIUnknown* ___async_state_1;
	ManualResetEvent_t451242010 * ___wait_handle_2;
	int32_t ___completed_synchronously_3;
	int32_t ___completed_4;
};
// Native definition for COM marshalling of System.IOAsyncResult
struct IOAsyncResult_t3640145766_marshaled_com
{
	Il2CppMethodPointer ___async_callback_0;
	Il2CppIUnknown* ___async_state_1;
	ManualResetEvent_t451242010 * ___wait_handle_2;
	int32_t ___completed_synchronously_3;
	int32_t ___completed_4;
};
#endif // IOASYNCRESULT_T3640145766_H
#ifndef IOSELECTOR_T1612260334_H
#define IOSELECTOR_T1612260334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOSelector
struct  IOSelector_t1612260334  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSELECTOR_T1612260334_H
#ifndef IPV4ADDRESSHELPER_T1053940972_H
#define IPV4ADDRESSHELPER_T1053940972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IPv4AddressHelper
struct  IPv4AddressHelper_t1053940972  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV4ADDRESSHELPER_T1053940972_H
#ifndef IPV6ADDRESSHELPER_T3063601302_H
#define IPV6ADDRESSHELPER_T3063601302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IPv6AddressHelper
struct  IPv6AddressHelper_t3063601302  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESSHELPER_T3063601302_H
#ifndef IRIHELPER_T2107623466_H
#define IRIHELPER_T2107623466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IriHelper
struct  IriHelper_t2107623466  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IRIHELPER_T2107623466_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t2760389100_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef PLATFORM_T535934311_H
#define PLATFORM_T535934311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Platform
struct  Platform_t535934311  : public RuntimeObject
{
public:

public:
};

struct Platform_t535934311_StaticFields
{
public:
	// System.Boolean System.Platform::checkedOS
	bool ___checkedOS_0;
	// System.Boolean System.Platform::isMacOS
	bool ___isMacOS_1;
	// System.Boolean System.Platform::isFreeBSD
	bool ___isFreeBSD_2;

public:
	inline static int32_t get_offset_of_checkedOS_0() { return static_cast<int32_t>(offsetof(Platform_t535934311_StaticFields, ___checkedOS_0)); }
	inline bool get_checkedOS_0() const { return ___checkedOS_0; }
	inline bool* get_address_of_checkedOS_0() { return &___checkedOS_0; }
	inline void set_checkedOS_0(bool value)
	{
		___checkedOS_0 = value;
	}

	inline static int32_t get_offset_of_isMacOS_1() { return static_cast<int32_t>(offsetof(Platform_t535934311_StaticFields, ___isMacOS_1)); }
	inline bool get_isMacOS_1() const { return ___isMacOS_1; }
	inline bool* get_address_of_isMacOS_1() { return &___isMacOS_1; }
	inline void set_isMacOS_1(bool value)
	{
		___isMacOS_1 = value;
	}

	inline static int32_t get_offset_of_isFreeBSD_2() { return static_cast<int32_t>(offsetof(Platform_t535934311_StaticFields, ___isFreeBSD_2)); }
	inline bool get_isFreeBSD_2() const { return ___isFreeBSD_2; }
	inline bool* get_address_of_isFreeBSD_2() { return &___isFreeBSD_2; }
	inline void set_isFreeBSD_2(bool value)
	{
		___isFreeBSD_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T535934311_H
#ifndef CACHEDCODEENTRY_T4228975826_H
#define CACHEDCODEENTRY_T4228975826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CachedCodeEntry
struct  CachedCodeEntry_t4228975826  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.CachedCodeEntry::_key
	String_t* ____key_0;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.CachedCodeEntry::_code
	RegexCode_t4293407246 * ____code_1;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_caps
	Hashtable_t1853889766 * ____caps_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_capnames
	Hashtable_t1853889766 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.CachedCodeEntry::_capslist
	StringU5BU5D_t1281789340* ____capslist_4;
	// System.Int32 System.Text.RegularExpressions.CachedCodeEntry::_capsize
	int32_t ____capsize_5;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.CachedCodeEntry::_factory
	RegexRunnerFactory_t51159052 * ____factory_6;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.CachedCodeEntry::_runnerref
	ExclusiveReference_t1927754563 * ____runnerref_7;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.CachedCodeEntry::_replref
	SharedReference_t2916547576 * ____replref_8;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____key_0)); }
	inline String_t* get__key_0() const { return ____key_0; }
	inline String_t** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(String_t* value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__code_1() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____code_1)); }
	inline RegexCode_t4293407246 * get__code_1() const { return ____code_1; }
	inline RegexCode_t4293407246 ** get_address_of__code_1() { return &____code_1; }
	inline void set__code_1(RegexCode_t4293407246 * value)
	{
		____code_1 = value;
		Il2CppCodeGenWriteBarrier((&____code_1), value);
	}

	inline static int32_t get_offset_of__caps_2() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____caps_2)); }
	inline Hashtable_t1853889766 * get__caps_2() const { return ____caps_2; }
	inline Hashtable_t1853889766 ** get_address_of__caps_2() { return &____caps_2; }
	inline void set__caps_2(Hashtable_t1853889766 * value)
	{
		____caps_2 = value;
		Il2CppCodeGenWriteBarrier((&____caps_2), value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____capnames_3)); }
	inline Hashtable_t1853889766 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t1853889766 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t1853889766 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_3), value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____capslist_4)); }
	inline StringU5BU5D_t1281789340* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1281789340** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1281789340* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier((&____capslist_4), value);
	}

	inline static int32_t get_offset_of__capsize_5() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____capsize_5)); }
	inline int32_t get__capsize_5() const { return ____capsize_5; }
	inline int32_t* get_address_of__capsize_5() { return &____capsize_5; }
	inline void set__capsize_5(int32_t value)
	{
		____capsize_5 = value;
	}

	inline static int32_t get_offset_of__factory_6() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____factory_6)); }
	inline RegexRunnerFactory_t51159052 * get__factory_6() const { return ____factory_6; }
	inline RegexRunnerFactory_t51159052 ** get_address_of__factory_6() { return &____factory_6; }
	inline void set__factory_6(RegexRunnerFactory_t51159052 * value)
	{
		____factory_6 = value;
		Il2CppCodeGenWriteBarrier((&____factory_6), value);
	}

	inline static int32_t get_offset_of__runnerref_7() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____runnerref_7)); }
	inline ExclusiveReference_t1927754563 * get__runnerref_7() const { return ____runnerref_7; }
	inline ExclusiveReference_t1927754563 ** get_address_of__runnerref_7() { return &____runnerref_7; }
	inline void set__runnerref_7(ExclusiveReference_t1927754563 * value)
	{
		____runnerref_7 = value;
		Il2CppCodeGenWriteBarrier((&____runnerref_7), value);
	}

	inline static int32_t get_offset_of__replref_8() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t4228975826, ____replref_8)); }
	inline SharedReference_t2916547576 * get__replref_8() const { return ____replref_8; }
	inline SharedReference_t2916547576 ** get_address_of__replref_8() { return &____replref_8; }
	inline void set__replref_8(SharedReference_t2916547576 * value)
	{
		____replref_8 = value;
		Il2CppCodeGenWriteBarrier((&____replref_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDCODEENTRY_T4228975826_H
#ifndef CAPTURE_T2232016050_H
#define CAPTURE_T2232016050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t2232016050  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Capture::_text
	String_t* ____text_0;
	// System.Int32 System.Text.RegularExpressions.Capture::_index
	int32_t ____index_1;
	// System.Int32 System.Text.RegularExpressions.Capture::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__text_0() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ____text_0)); }
	inline String_t* get__text_0() const { return ____text_0; }
	inline String_t** get_address_of__text_0() { return &____text_0; }
	inline void set__text_0(String_t* value)
	{
		____text_0 = value;
		Il2CppCodeGenWriteBarrier((&____text_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T2232016050_H
#ifndef CAPTURECOLLECTION_T1760593541_H
#define CAPTURECOLLECTION_T1760593541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CaptureCollection
struct  CaptureCollection_t1760593541  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.CaptureCollection::_group
	Group_t2468205786 * ____group_0;
	// System.Int32 System.Text.RegularExpressions.CaptureCollection::_capcount
	int32_t ____capcount_1;
	// System.Text.RegularExpressions.Capture[] System.Text.RegularExpressions.CaptureCollection::_captures
	CaptureU5BU5D_t183267399* ____captures_2;

public:
	inline static int32_t get_offset_of__group_0() { return static_cast<int32_t>(offsetof(CaptureCollection_t1760593541, ____group_0)); }
	inline Group_t2468205786 * get__group_0() const { return ____group_0; }
	inline Group_t2468205786 ** get_address_of__group_0() { return &____group_0; }
	inline void set__group_0(Group_t2468205786 * value)
	{
		____group_0 = value;
		Il2CppCodeGenWriteBarrier((&____group_0), value);
	}

	inline static int32_t get_offset_of__capcount_1() { return static_cast<int32_t>(offsetof(CaptureCollection_t1760593541, ____capcount_1)); }
	inline int32_t get__capcount_1() const { return ____capcount_1; }
	inline int32_t* get_address_of__capcount_1() { return &____capcount_1; }
	inline void set__capcount_1(int32_t value)
	{
		____capcount_1 = value;
	}

	inline static int32_t get_offset_of__captures_2() { return static_cast<int32_t>(offsetof(CaptureCollection_t1760593541, ____captures_2)); }
	inline CaptureU5BU5D_t183267399* get__captures_2() const { return ____captures_2; }
	inline CaptureU5BU5D_t183267399** get_address_of__captures_2() { return &____captures_2; }
	inline void set__captures_2(CaptureU5BU5D_t183267399* value)
	{
		____captures_2 = value;
		Il2CppCodeGenWriteBarrier((&____captures_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURECOLLECTION_T1760593541_H
#ifndef CAPTUREENUMERATOR_T3579258555_H
#define CAPTUREENUMERATOR_T3579258555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CaptureEnumerator
struct  CaptureEnumerator_t3579258555  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.CaptureEnumerator::_rcc
	CaptureCollection_t1760593541 * ____rcc_0;
	// System.Int32 System.Text.RegularExpressions.CaptureEnumerator::_curindex
	int32_t ____curindex_1;

public:
	inline static int32_t get_offset_of__rcc_0() { return static_cast<int32_t>(offsetof(CaptureEnumerator_t3579258555, ____rcc_0)); }
	inline CaptureCollection_t1760593541 * get__rcc_0() const { return ____rcc_0; }
	inline CaptureCollection_t1760593541 ** get_address_of__rcc_0() { return &____rcc_0; }
	inline void set__rcc_0(CaptureCollection_t1760593541 * value)
	{
		____rcc_0 = value;
		Il2CppCodeGenWriteBarrier((&____rcc_0), value);
	}

	inline static int32_t get_offset_of__curindex_1() { return static_cast<int32_t>(offsetof(CaptureEnumerator_t3579258555, ____curindex_1)); }
	inline int32_t get__curindex_1() const { return ____curindex_1; }
	inline int32_t* get_address_of__curindex_1() { return &____curindex_1; }
	inline void set__curindex_1(int32_t value)
	{
		____curindex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREENUMERATOR_T3579258555_H
#ifndef EXCLUSIVEREFERENCE_T1927754563_H
#define EXCLUSIVEREFERENCE_T1927754563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.ExclusiveReference
struct  ExclusiveReference_t1927754563  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexRunner System.Text.RegularExpressions.ExclusiveReference::_ref
	RegexRunner_t300319648 * ____ref_0;
	// System.Object System.Text.RegularExpressions.ExclusiveReference::_obj
	RuntimeObject * ____obj_1;
	// System.Int32 System.Text.RegularExpressions.ExclusiveReference::_locked
	int32_t ____locked_2;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(ExclusiveReference_t1927754563, ____ref_0)); }
	inline RegexRunner_t300319648 * get__ref_0() const { return ____ref_0; }
	inline RegexRunner_t300319648 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(RegexRunner_t300319648 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier((&____ref_0), value);
	}

	inline static int32_t get_offset_of__obj_1() { return static_cast<int32_t>(offsetof(ExclusiveReference_t1927754563, ____obj_1)); }
	inline RuntimeObject * get__obj_1() const { return ____obj_1; }
	inline RuntimeObject ** get_address_of__obj_1() { return &____obj_1; }
	inline void set__obj_1(RuntimeObject * value)
	{
		____obj_1 = value;
		Il2CppCodeGenWriteBarrier((&____obj_1), value);
	}

	inline static int32_t get_offset_of__locked_2() { return static_cast<int32_t>(offsetof(ExclusiveReference_t1927754563, ____locked_2)); }
	inline int32_t get__locked_2() const { return ____locked_2; }
	inline int32_t* get_address_of__locked_2() { return &____locked_2; }
	inline void set__locked_2(int32_t value)
	{
		____locked_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUSIVEREFERENCE_T1927754563_H
#ifndef GROUPCOLLECTION_T69770484_H
#define GROUPCOLLECTION_T69770484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_t69770484  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.GroupCollection::_match
	Match_t3408321083 * ____match_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.GroupCollection::_captureMap
	Hashtable_t1853889766 * ____captureMap_1;
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::_groups
	GroupU5BU5D_t1880820351* ____groups_2;

public:
	inline static int32_t get_offset_of__match_0() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ____match_0)); }
	inline Match_t3408321083 * get__match_0() const { return ____match_0; }
	inline Match_t3408321083 ** get_address_of__match_0() { return &____match_0; }
	inline void set__match_0(Match_t3408321083 * value)
	{
		____match_0 = value;
		Il2CppCodeGenWriteBarrier((&____match_0), value);
	}

	inline static int32_t get_offset_of__captureMap_1() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ____captureMap_1)); }
	inline Hashtable_t1853889766 * get__captureMap_1() const { return ____captureMap_1; }
	inline Hashtable_t1853889766 ** get_address_of__captureMap_1() { return &____captureMap_1; }
	inline void set__captureMap_1(Hashtable_t1853889766 * value)
	{
		____captureMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____captureMap_1), value);
	}

	inline static int32_t get_offset_of__groups_2() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ____groups_2)); }
	inline GroupU5BU5D_t1880820351* get__groups_2() const { return ____groups_2; }
	inline GroupU5BU5D_t1880820351** get_address_of__groups_2() { return &____groups_2; }
	inline void set__groups_2(GroupU5BU5D_t1880820351* value)
	{
		____groups_2 = value;
		Il2CppCodeGenWriteBarrier((&____groups_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_T69770484_H
#ifndef GROUPENUMERATOR_T441528474_H
#define GROUPENUMERATOR_T441528474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupEnumerator
struct  GroupEnumerator_t441528474  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.GroupEnumerator::_rgc
	GroupCollection_t69770484 * ____rgc_0;
	// System.Int32 System.Text.RegularExpressions.GroupEnumerator::_curindex
	int32_t ____curindex_1;

public:
	inline static int32_t get_offset_of__rgc_0() { return static_cast<int32_t>(offsetof(GroupEnumerator_t441528474, ____rgc_0)); }
	inline GroupCollection_t69770484 * get__rgc_0() const { return ____rgc_0; }
	inline GroupCollection_t69770484 ** get_address_of__rgc_0() { return &____rgc_0; }
	inline void set__rgc_0(GroupCollection_t69770484 * value)
	{
		____rgc_0 = value;
		Il2CppCodeGenWriteBarrier((&____rgc_0), value);
	}

	inline static int32_t get_offset_of__curindex_1() { return static_cast<int32_t>(offsetof(GroupEnumerator_t441528474, ____curindex_1)); }
	inline int32_t get__curindex_1() const { return ____curindex_1; }
	inline int32_t* get_address_of__curindex_1() { return &____curindex_1; }
	inline void set__curindex_1(int32_t value)
	{
		____curindex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPENUMERATOR_T441528474_H
#ifndef MATCHCOLLECTION_T1395363720_H
#define MATCHCOLLECTION_T1395363720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t1395363720  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.MatchCollection::_regex
	Regex_t3657309853 * ____regex_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::_matches
	ArrayList_t2718874744 * ____matches_1;
	// System.Boolean System.Text.RegularExpressions.MatchCollection::_done
	bool ____done_2;
	// System.String System.Text.RegularExpressions.MatchCollection::_input
	String_t* ____input_3;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_beginning
	int32_t ____beginning_4;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_length
	int32_t ____length_5;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_startat
	int32_t ____startat_6;
	// System.Int32 System.Text.RegularExpressions.MatchCollection::_prevlen
	int32_t ____prevlen_7;

public:
	inline static int32_t get_offset_of__regex_0() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____regex_0)); }
	inline Regex_t3657309853 * get__regex_0() const { return ____regex_0; }
	inline Regex_t3657309853 ** get_address_of__regex_0() { return &____regex_0; }
	inline void set__regex_0(Regex_t3657309853 * value)
	{
		____regex_0 = value;
		Il2CppCodeGenWriteBarrier((&____regex_0), value);
	}

	inline static int32_t get_offset_of__matches_1() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____matches_1)); }
	inline ArrayList_t2718874744 * get__matches_1() const { return ____matches_1; }
	inline ArrayList_t2718874744 ** get_address_of__matches_1() { return &____matches_1; }
	inline void set__matches_1(ArrayList_t2718874744 * value)
	{
		____matches_1 = value;
		Il2CppCodeGenWriteBarrier((&____matches_1), value);
	}

	inline static int32_t get_offset_of__done_2() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____done_2)); }
	inline bool get__done_2() const { return ____done_2; }
	inline bool* get_address_of__done_2() { return &____done_2; }
	inline void set__done_2(bool value)
	{
		____done_2 = value;
	}

	inline static int32_t get_offset_of__input_3() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____input_3)); }
	inline String_t* get__input_3() const { return ____input_3; }
	inline String_t** get_address_of__input_3() { return &____input_3; }
	inline void set__input_3(String_t* value)
	{
		____input_3 = value;
		Il2CppCodeGenWriteBarrier((&____input_3), value);
	}

	inline static int32_t get_offset_of__beginning_4() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____beginning_4)); }
	inline int32_t get__beginning_4() const { return ____beginning_4; }
	inline int32_t* get_address_of__beginning_4() { return &____beginning_4; }
	inline void set__beginning_4(int32_t value)
	{
		____beginning_4 = value;
	}

	inline static int32_t get_offset_of__length_5() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____length_5)); }
	inline int32_t get__length_5() const { return ____length_5; }
	inline int32_t* get_address_of__length_5() { return &____length_5; }
	inline void set__length_5(int32_t value)
	{
		____length_5 = value;
	}

	inline static int32_t get_offset_of__startat_6() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____startat_6)); }
	inline int32_t get__startat_6() const { return ____startat_6; }
	inline int32_t* get_address_of__startat_6() { return &____startat_6; }
	inline void set__startat_6(int32_t value)
	{
		____startat_6 = value;
	}

	inline static int32_t get_offset_of__prevlen_7() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720, ____prevlen_7)); }
	inline int32_t get__prevlen_7() const { return ____prevlen_7; }
	inline int32_t* get_address_of__prevlen_7() { return &____prevlen_7; }
	inline void set__prevlen_7(int32_t value)
	{
		____prevlen_7 = value;
	}
};

struct MatchCollection_t1395363720_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.MatchCollection::infinite
	int32_t ___infinite_8;

public:
	inline static int32_t get_offset_of_infinite_8() { return static_cast<int32_t>(offsetof(MatchCollection_t1395363720_StaticFields, ___infinite_8)); }
	inline int32_t get_infinite_8() const { return ___infinite_8; }
	inline int32_t* get_address_of_infinite_8() { return &___infinite_8; }
	inline void set_infinite_8(int32_t value)
	{
		___infinite_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHCOLLECTION_T1395363720_H
#ifndef MATCHENUMERATOR_T912837736_H
#define MATCHENUMERATOR_T912837736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchEnumerator
struct  MatchEnumerator_t912837736  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchEnumerator::_matchcoll
	MatchCollection_t1395363720 * ____matchcoll_0;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchEnumerator::_match
	Match_t3408321083 * ____match_1;
	// System.Int32 System.Text.RegularExpressions.MatchEnumerator::_curindex
	int32_t ____curindex_2;
	// System.Boolean System.Text.RegularExpressions.MatchEnumerator::_done
	bool ____done_3;

public:
	inline static int32_t get_offset_of__matchcoll_0() { return static_cast<int32_t>(offsetof(MatchEnumerator_t912837736, ____matchcoll_0)); }
	inline MatchCollection_t1395363720 * get__matchcoll_0() const { return ____matchcoll_0; }
	inline MatchCollection_t1395363720 ** get_address_of__matchcoll_0() { return &____matchcoll_0; }
	inline void set__matchcoll_0(MatchCollection_t1395363720 * value)
	{
		____matchcoll_0 = value;
		Il2CppCodeGenWriteBarrier((&____matchcoll_0), value);
	}

	inline static int32_t get_offset_of__match_1() { return static_cast<int32_t>(offsetof(MatchEnumerator_t912837736, ____match_1)); }
	inline Match_t3408321083 * get__match_1() const { return ____match_1; }
	inline Match_t3408321083 ** get_address_of__match_1() { return &____match_1; }
	inline void set__match_1(Match_t3408321083 * value)
	{
		____match_1 = value;
		Il2CppCodeGenWriteBarrier((&____match_1), value);
	}

	inline static int32_t get_offset_of__curindex_2() { return static_cast<int32_t>(offsetof(MatchEnumerator_t912837736, ____curindex_2)); }
	inline int32_t get__curindex_2() const { return ____curindex_2; }
	inline int32_t* get_address_of__curindex_2() { return &____curindex_2; }
	inline void set__curindex_2(int32_t value)
	{
		____curindex_2 = value;
	}

	inline static int32_t get_offset_of__done_3() { return static_cast<int32_t>(offsetof(MatchEnumerator_t912837736, ____done_3)); }
	inline bool get__done_3() const { return ____done_3; }
	inline bool* get_address_of__done_3() { return &____done_3; }
	inline void set__done_3(bool value)
	{
		____done_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHENUMERATOR_T912837736_H
#ifndef REGEXBOYERMOORE_T1480609368_H
#define REGEXBOYERMOORE_T1480609368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexBoyerMoore
struct  RegexBoyerMoore_t1480609368  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexBoyerMoore::_positive
	Int32U5BU5D_t385246372* ____positive_0;
	// System.Int32[] System.Text.RegularExpressions.RegexBoyerMoore::_negativeASCII
	Int32U5BU5D_t385246372* ____negativeASCII_1;
	// System.Int32[][] System.Text.RegularExpressions.RegexBoyerMoore::_negativeUnicode
	Int32U5BU5DU5BU5D_t3365920845* ____negativeUnicode_2;
	// System.String System.Text.RegularExpressions.RegexBoyerMoore::_pattern
	String_t* ____pattern_3;
	// System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::_lowASCII
	int32_t ____lowASCII_4;
	// System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::_highASCII
	int32_t ____highASCII_5;
	// System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::_rightToLeft
	bool ____rightToLeft_6;
	// System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::_caseInsensitive
	bool ____caseInsensitive_7;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexBoyerMoore::_culture
	CultureInfo_t4157843068 * ____culture_8;

public:
	inline static int32_t get_offset_of__positive_0() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____positive_0)); }
	inline Int32U5BU5D_t385246372* get__positive_0() const { return ____positive_0; }
	inline Int32U5BU5D_t385246372** get_address_of__positive_0() { return &____positive_0; }
	inline void set__positive_0(Int32U5BU5D_t385246372* value)
	{
		____positive_0 = value;
		Il2CppCodeGenWriteBarrier((&____positive_0), value);
	}

	inline static int32_t get_offset_of__negativeASCII_1() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____negativeASCII_1)); }
	inline Int32U5BU5D_t385246372* get__negativeASCII_1() const { return ____negativeASCII_1; }
	inline Int32U5BU5D_t385246372** get_address_of__negativeASCII_1() { return &____negativeASCII_1; }
	inline void set__negativeASCII_1(Int32U5BU5D_t385246372* value)
	{
		____negativeASCII_1 = value;
		Il2CppCodeGenWriteBarrier((&____negativeASCII_1), value);
	}

	inline static int32_t get_offset_of__negativeUnicode_2() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____negativeUnicode_2)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get__negativeUnicode_2() const { return ____negativeUnicode_2; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of__negativeUnicode_2() { return &____negativeUnicode_2; }
	inline void set__negativeUnicode_2(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		____negativeUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&____negativeUnicode_2), value);
	}

	inline static int32_t get_offset_of__pattern_3() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____pattern_3)); }
	inline String_t* get__pattern_3() const { return ____pattern_3; }
	inline String_t** get_address_of__pattern_3() { return &____pattern_3; }
	inline void set__pattern_3(String_t* value)
	{
		____pattern_3 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_3), value);
	}

	inline static int32_t get_offset_of__lowASCII_4() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____lowASCII_4)); }
	inline int32_t get__lowASCII_4() const { return ____lowASCII_4; }
	inline int32_t* get_address_of__lowASCII_4() { return &____lowASCII_4; }
	inline void set__lowASCII_4(int32_t value)
	{
		____lowASCII_4 = value;
	}

	inline static int32_t get_offset_of__highASCII_5() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____highASCII_5)); }
	inline int32_t get__highASCII_5() const { return ____highASCII_5; }
	inline int32_t* get_address_of__highASCII_5() { return &____highASCII_5; }
	inline void set__highASCII_5(int32_t value)
	{
		____highASCII_5 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_6() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____rightToLeft_6)); }
	inline bool get__rightToLeft_6() const { return ____rightToLeft_6; }
	inline bool* get_address_of__rightToLeft_6() { return &____rightToLeft_6; }
	inline void set__rightToLeft_6(bool value)
	{
		____rightToLeft_6 = value;
	}

	inline static int32_t get_offset_of__caseInsensitive_7() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____caseInsensitive_7)); }
	inline bool get__caseInsensitive_7() const { return ____caseInsensitive_7; }
	inline bool* get_address_of__caseInsensitive_7() { return &____caseInsensitive_7; }
	inline void set__caseInsensitive_7(bool value)
	{
		____caseInsensitive_7 = value;
	}

	inline static int32_t get_offset_of__culture_8() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t1480609368, ____culture_8)); }
	inline CultureInfo_t4157843068 * get__culture_8() const { return ____culture_8; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_8() { return &____culture_8; }
	inline void set__culture_8(CultureInfo_t4157843068 * value)
	{
		____culture_8 = value;
		Il2CppCodeGenWriteBarrier((&____culture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXBOYERMOORE_T1480609368_H
#ifndef REGEXCHARCLASS_T2088948945_H
#define REGEXCHARCLASS_T2088948945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass
struct  RegexCharClass_t2088948945  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange> System.Text.RegularExpressions.RegexCharClass::_rangelist
	List_1_t1270793844 * ____rangelist_0;
	// System.Text.StringBuilder System.Text.RegularExpressions.RegexCharClass::_categories
	StringBuilder_t * ____categories_1;
	// System.Boolean System.Text.RegularExpressions.RegexCharClass::_canonical
	bool ____canonical_2;
	// System.Boolean System.Text.RegularExpressions.RegexCharClass::_negate
	bool ____negate_3;
	// System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexCharClass::_subtractor
	RegexCharClass_t2088948945 * ____subtractor_4;

public:
	inline static int32_t get_offset_of__rangelist_0() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945, ____rangelist_0)); }
	inline List_1_t1270793844 * get__rangelist_0() const { return ____rangelist_0; }
	inline List_1_t1270793844 ** get_address_of__rangelist_0() { return &____rangelist_0; }
	inline void set__rangelist_0(List_1_t1270793844 * value)
	{
		____rangelist_0 = value;
		Il2CppCodeGenWriteBarrier((&____rangelist_0), value);
	}

	inline static int32_t get_offset_of__categories_1() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945, ____categories_1)); }
	inline StringBuilder_t * get__categories_1() const { return ____categories_1; }
	inline StringBuilder_t ** get_address_of__categories_1() { return &____categories_1; }
	inline void set__categories_1(StringBuilder_t * value)
	{
		____categories_1 = value;
		Il2CppCodeGenWriteBarrier((&____categories_1), value);
	}

	inline static int32_t get_offset_of__canonical_2() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945, ____canonical_2)); }
	inline bool get__canonical_2() const { return ____canonical_2; }
	inline bool* get_address_of__canonical_2() { return &____canonical_2; }
	inline void set__canonical_2(bool value)
	{
		____canonical_2 = value;
	}

	inline static int32_t get_offset_of__negate_3() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945, ____negate_3)); }
	inline bool get__negate_3() const { return ____negate_3; }
	inline bool* get_address_of__negate_3() { return &____negate_3; }
	inline void set__negate_3(bool value)
	{
		____negate_3 = value;
	}

	inline static int32_t get_offset_of__subtractor_4() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945, ____subtractor_4)); }
	inline RegexCharClass_t2088948945 * get__subtractor_4() const { return ____subtractor_4; }
	inline RegexCharClass_t2088948945 ** get_address_of__subtractor_4() { return &____subtractor_4; }
	inline void set__subtractor_4(RegexCharClass_t2088948945 * value)
	{
		____subtractor_4 = value;
		Il2CppCodeGenWriteBarrier((&____subtractor_4), value);
	}
};

struct RegexCharClass_t2088948945_StaticFields
{
public:
	// System.String System.Text.RegularExpressions.RegexCharClass::InternalRegexIgnoreCase
	String_t* ___InternalRegexIgnoreCase_5;
	// System.String System.Text.RegularExpressions.RegexCharClass::Space
	String_t* ___Space_6;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotSpace
	String_t* ___NotSpace_7;
	// System.String System.Text.RegularExpressions.RegexCharClass::Word
	String_t* ___Word_8;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotWord
	String_t* ___NotWord_9;
	// System.String System.Text.RegularExpressions.RegexCharClass::SpaceClass
	String_t* ___SpaceClass_10;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotSpaceClass
	String_t* ___NotSpaceClass_11;
	// System.String System.Text.RegularExpressions.RegexCharClass::WordClass
	String_t* ___WordClass_12;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotWordClass
	String_t* ___NotWordClass_13;
	// System.String System.Text.RegularExpressions.RegexCharClass::DigitClass
	String_t* ___DigitClass_14;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotDigitClass
	String_t* ___NotDigitClass_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Text.RegularExpressions.RegexCharClass::_definedCategories
	Dictionary_2_t1632706988 * ____definedCategories_16;
	// System.String[0...,0...] System.Text.RegularExpressions.RegexCharClass::_propTable
	StringU5B0___U2C0___U5D_t1281789341* ____propTable_17;
	// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping[] System.Text.RegularExpressions.RegexCharClass::_lcTable
	LowerCaseMappingU5BU5D_t2345228286* ____lcTable_18;

public:
	inline static int32_t get_offset_of_InternalRegexIgnoreCase_5() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___InternalRegexIgnoreCase_5)); }
	inline String_t* get_InternalRegexIgnoreCase_5() const { return ___InternalRegexIgnoreCase_5; }
	inline String_t** get_address_of_InternalRegexIgnoreCase_5() { return &___InternalRegexIgnoreCase_5; }
	inline void set_InternalRegexIgnoreCase_5(String_t* value)
	{
		___InternalRegexIgnoreCase_5 = value;
		Il2CppCodeGenWriteBarrier((&___InternalRegexIgnoreCase_5), value);
	}

	inline static int32_t get_offset_of_Space_6() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___Space_6)); }
	inline String_t* get_Space_6() const { return ___Space_6; }
	inline String_t** get_address_of_Space_6() { return &___Space_6; }
	inline void set_Space_6(String_t* value)
	{
		___Space_6 = value;
		Il2CppCodeGenWriteBarrier((&___Space_6), value);
	}

	inline static int32_t get_offset_of_NotSpace_7() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___NotSpace_7)); }
	inline String_t* get_NotSpace_7() const { return ___NotSpace_7; }
	inline String_t** get_address_of_NotSpace_7() { return &___NotSpace_7; }
	inline void set_NotSpace_7(String_t* value)
	{
		___NotSpace_7 = value;
		Il2CppCodeGenWriteBarrier((&___NotSpace_7), value);
	}

	inline static int32_t get_offset_of_Word_8() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___Word_8)); }
	inline String_t* get_Word_8() const { return ___Word_8; }
	inline String_t** get_address_of_Word_8() { return &___Word_8; }
	inline void set_Word_8(String_t* value)
	{
		___Word_8 = value;
		Il2CppCodeGenWriteBarrier((&___Word_8), value);
	}

	inline static int32_t get_offset_of_NotWord_9() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___NotWord_9)); }
	inline String_t* get_NotWord_9() const { return ___NotWord_9; }
	inline String_t** get_address_of_NotWord_9() { return &___NotWord_9; }
	inline void set_NotWord_9(String_t* value)
	{
		___NotWord_9 = value;
		Il2CppCodeGenWriteBarrier((&___NotWord_9), value);
	}

	inline static int32_t get_offset_of_SpaceClass_10() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___SpaceClass_10)); }
	inline String_t* get_SpaceClass_10() const { return ___SpaceClass_10; }
	inline String_t** get_address_of_SpaceClass_10() { return &___SpaceClass_10; }
	inline void set_SpaceClass_10(String_t* value)
	{
		___SpaceClass_10 = value;
		Il2CppCodeGenWriteBarrier((&___SpaceClass_10), value);
	}

	inline static int32_t get_offset_of_NotSpaceClass_11() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___NotSpaceClass_11)); }
	inline String_t* get_NotSpaceClass_11() const { return ___NotSpaceClass_11; }
	inline String_t** get_address_of_NotSpaceClass_11() { return &___NotSpaceClass_11; }
	inline void set_NotSpaceClass_11(String_t* value)
	{
		___NotSpaceClass_11 = value;
		Il2CppCodeGenWriteBarrier((&___NotSpaceClass_11), value);
	}

	inline static int32_t get_offset_of_WordClass_12() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___WordClass_12)); }
	inline String_t* get_WordClass_12() const { return ___WordClass_12; }
	inline String_t** get_address_of_WordClass_12() { return &___WordClass_12; }
	inline void set_WordClass_12(String_t* value)
	{
		___WordClass_12 = value;
		Il2CppCodeGenWriteBarrier((&___WordClass_12), value);
	}

	inline static int32_t get_offset_of_NotWordClass_13() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___NotWordClass_13)); }
	inline String_t* get_NotWordClass_13() const { return ___NotWordClass_13; }
	inline String_t** get_address_of_NotWordClass_13() { return &___NotWordClass_13; }
	inline void set_NotWordClass_13(String_t* value)
	{
		___NotWordClass_13 = value;
		Il2CppCodeGenWriteBarrier((&___NotWordClass_13), value);
	}

	inline static int32_t get_offset_of_DigitClass_14() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___DigitClass_14)); }
	inline String_t* get_DigitClass_14() const { return ___DigitClass_14; }
	inline String_t** get_address_of_DigitClass_14() { return &___DigitClass_14; }
	inline void set_DigitClass_14(String_t* value)
	{
		___DigitClass_14 = value;
		Il2CppCodeGenWriteBarrier((&___DigitClass_14), value);
	}

	inline static int32_t get_offset_of_NotDigitClass_15() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ___NotDigitClass_15)); }
	inline String_t* get_NotDigitClass_15() const { return ___NotDigitClass_15; }
	inline String_t** get_address_of_NotDigitClass_15() { return &___NotDigitClass_15; }
	inline void set_NotDigitClass_15(String_t* value)
	{
		___NotDigitClass_15 = value;
		Il2CppCodeGenWriteBarrier((&___NotDigitClass_15), value);
	}

	inline static int32_t get_offset_of__definedCategories_16() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ____definedCategories_16)); }
	inline Dictionary_2_t1632706988 * get__definedCategories_16() const { return ____definedCategories_16; }
	inline Dictionary_2_t1632706988 ** get_address_of__definedCategories_16() { return &____definedCategories_16; }
	inline void set__definedCategories_16(Dictionary_2_t1632706988 * value)
	{
		____definedCategories_16 = value;
		Il2CppCodeGenWriteBarrier((&____definedCategories_16), value);
	}

	inline static int32_t get_offset_of__propTable_17() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ____propTable_17)); }
	inline StringU5B0___U2C0___U5D_t1281789341* get__propTable_17() const { return ____propTable_17; }
	inline StringU5B0___U2C0___U5D_t1281789341** get_address_of__propTable_17() { return &____propTable_17; }
	inline void set__propTable_17(StringU5B0___U2C0___U5D_t1281789341* value)
	{
		____propTable_17 = value;
		Il2CppCodeGenWriteBarrier((&____propTable_17), value);
	}

	inline static int32_t get_offset_of__lcTable_18() { return static_cast<int32_t>(offsetof(RegexCharClass_t2088948945_StaticFields, ____lcTable_18)); }
	inline LowerCaseMappingU5BU5D_t2345228286* get__lcTable_18() const { return ____lcTable_18; }
	inline LowerCaseMappingU5BU5D_t2345228286** get_address_of__lcTable_18() { return &____lcTable_18; }
	inline void set__lcTable_18(LowerCaseMappingU5BU5D_t2345228286* value)
	{
		____lcTable_18 = value;
		Il2CppCodeGenWriteBarrier((&____lcTable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCHARCLASS_T2088948945_H
#ifndef SINGLERANGE_T4093686398_H
#define SINGLERANGE_T4093686398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/SingleRange
struct  SingleRange_t4093686398  : public RuntimeObject
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/SingleRange::_first
	Il2CppChar ____first_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/SingleRange::_last
	Il2CppChar ____last_1;

public:
	inline static int32_t get_offset_of__first_0() { return static_cast<int32_t>(offsetof(SingleRange_t4093686398, ____first_0)); }
	inline Il2CppChar get__first_0() const { return ____first_0; }
	inline Il2CppChar* get_address_of__first_0() { return &____first_0; }
	inline void set__first_0(Il2CppChar value)
	{
		____first_0 = value;
	}

	inline static int32_t get_offset_of__last_1() { return static_cast<int32_t>(offsetof(SingleRange_t4093686398, ____last_1)); }
	inline Il2CppChar get__last_1() const { return ____last_1; }
	inline Il2CppChar* get_address_of__last_1() { return &____last_1; }
	inline void set__last_1(Il2CppChar value)
	{
		____last_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERANGE_T4093686398_H
#ifndef SINGLERANGECOMPARER_T1611033177_H
#define SINGLERANGECOMPARER_T1611033177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/SingleRangeComparer
struct  SingleRangeComparer_t1611033177  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERANGECOMPARER_T1611033177_H
#ifndef REGEXCODE_T4293407246_H
#define REGEXCODE_T4293407246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCode
struct  RegexCode_t4293407246  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexCode::_codes
	Int32U5BU5D_t385246372* ____codes_48;
	// System.String[] System.Text.RegularExpressions.RegexCode::_strings
	StringU5BU5D_t1281789340* ____strings_49;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_trackcount
	int32_t ____trackcount_50;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexCode::_caps
	Hashtable_t1853889766 * ____caps_51;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_capsize
	int32_t ____capsize_52;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexCode::_fcPrefix
	RegexPrefix_t3750605839 * ____fcPrefix_53;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexCode::_bmPrefix
	RegexBoyerMoore_t1480609368 * ____bmPrefix_54;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_anchors
	int32_t ____anchors_55;
	// System.Boolean System.Text.RegularExpressions.RegexCode::_rightToLeft
	bool ____rightToLeft_56;

public:
	inline static int32_t get_offset_of__codes_48() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____codes_48)); }
	inline Int32U5BU5D_t385246372* get__codes_48() const { return ____codes_48; }
	inline Int32U5BU5D_t385246372** get_address_of__codes_48() { return &____codes_48; }
	inline void set__codes_48(Int32U5BU5D_t385246372* value)
	{
		____codes_48 = value;
		Il2CppCodeGenWriteBarrier((&____codes_48), value);
	}

	inline static int32_t get_offset_of__strings_49() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____strings_49)); }
	inline StringU5BU5D_t1281789340* get__strings_49() const { return ____strings_49; }
	inline StringU5BU5D_t1281789340** get_address_of__strings_49() { return &____strings_49; }
	inline void set__strings_49(StringU5BU5D_t1281789340* value)
	{
		____strings_49 = value;
		Il2CppCodeGenWriteBarrier((&____strings_49), value);
	}

	inline static int32_t get_offset_of__trackcount_50() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____trackcount_50)); }
	inline int32_t get__trackcount_50() const { return ____trackcount_50; }
	inline int32_t* get_address_of__trackcount_50() { return &____trackcount_50; }
	inline void set__trackcount_50(int32_t value)
	{
		____trackcount_50 = value;
	}

	inline static int32_t get_offset_of__caps_51() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____caps_51)); }
	inline Hashtable_t1853889766 * get__caps_51() const { return ____caps_51; }
	inline Hashtable_t1853889766 ** get_address_of__caps_51() { return &____caps_51; }
	inline void set__caps_51(Hashtable_t1853889766 * value)
	{
		____caps_51 = value;
		Il2CppCodeGenWriteBarrier((&____caps_51), value);
	}

	inline static int32_t get_offset_of__capsize_52() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____capsize_52)); }
	inline int32_t get__capsize_52() const { return ____capsize_52; }
	inline int32_t* get_address_of__capsize_52() { return &____capsize_52; }
	inline void set__capsize_52(int32_t value)
	{
		____capsize_52 = value;
	}

	inline static int32_t get_offset_of__fcPrefix_53() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____fcPrefix_53)); }
	inline RegexPrefix_t3750605839 * get__fcPrefix_53() const { return ____fcPrefix_53; }
	inline RegexPrefix_t3750605839 ** get_address_of__fcPrefix_53() { return &____fcPrefix_53; }
	inline void set__fcPrefix_53(RegexPrefix_t3750605839 * value)
	{
		____fcPrefix_53 = value;
		Il2CppCodeGenWriteBarrier((&____fcPrefix_53), value);
	}

	inline static int32_t get_offset_of__bmPrefix_54() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____bmPrefix_54)); }
	inline RegexBoyerMoore_t1480609368 * get__bmPrefix_54() const { return ____bmPrefix_54; }
	inline RegexBoyerMoore_t1480609368 ** get_address_of__bmPrefix_54() { return &____bmPrefix_54; }
	inline void set__bmPrefix_54(RegexBoyerMoore_t1480609368 * value)
	{
		____bmPrefix_54 = value;
		Il2CppCodeGenWriteBarrier((&____bmPrefix_54), value);
	}

	inline static int32_t get_offset_of__anchors_55() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____anchors_55)); }
	inline int32_t get__anchors_55() const { return ____anchors_55; }
	inline int32_t* get_address_of__anchors_55() { return &____anchors_55; }
	inline void set__anchors_55(int32_t value)
	{
		____anchors_55 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_56() { return static_cast<int32_t>(offsetof(RegexCode_t4293407246, ____rightToLeft_56)); }
	inline bool get__rightToLeft_56() const { return ____rightToLeft_56; }
	inline bool* get_address_of__rightToLeft_56() { return &____rightToLeft_56; }
	inline void set__rightToLeft_56(bool value)
	{
		____rightToLeft_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCODE_T4293407246_H
#ifndef REGEXFC_T4283085439_H
#define REGEXFC_T4283085439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexFC
struct  RegexFC_t4283085439  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexFC::_cc
	RegexCharClass_t2088948945 * ____cc_0;
	// System.Boolean System.Text.RegularExpressions.RegexFC::_nullable
	bool ____nullable_1;
	// System.Boolean System.Text.RegularExpressions.RegexFC::_caseInsensitive
	bool ____caseInsensitive_2;

public:
	inline static int32_t get_offset_of__cc_0() { return static_cast<int32_t>(offsetof(RegexFC_t4283085439, ____cc_0)); }
	inline RegexCharClass_t2088948945 * get__cc_0() const { return ____cc_0; }
	inline RegexCharClass_t2088948945 ** get_address_of__cc_0() { return &____cc_0; }
	inline void set__cc_0(RegexCharClass_t2088948945 * value)
	{
		____cc_0 = value;
		Il2CppCodeGenWriteBarrier((&____cc_0), value);
	}

	inline static int32_t get_offset_of__nullable_1() { return static_cast<int32_t>(offsetof(RegexFC_t4283085439, ____nullable_1)); }
	inline bool get__nullable_1() const { return ____nullable_1; }
	inline bool* get_address_of__nullable_1() { return &____nullable_1; }
	inline void set__nullable_1(bool value)
	{
		____nullable_1 = value;
	}

	inline static int32_t get_offset_of__caseInsensitive_2() { return static_cast<int32_t>(offsetof(RegexFC_t4283085439, ____caseInsensitive_2)); }
	inline bool get__caseInsensitive_2() const { return ____caseInsensitive_2; }
	inline bool* get_address_of__caseInsensitive_2() { return &____caseInsensitive_2; }
	inline void set__caseInsensitive_2(bool value)
	{
		____caseInsensitive_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXFC_T4283085439_H
#ifndef REGEXFCD_T3501104819_H
#define REGEXFCD_T3501104819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexFCD
struct  RegexFCD_t3501104819  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexFCD::_intStack
	Int32U5BU5D_t385246372* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexFCD::_intDepth
	int32_t ____intDepth_1;
	// System.Text.RegularExpressions.RegexFC[] System.Text.RegularExpressions.RegexFCD::_fcStack
	RegexFCU5BU5D_t3722514598* ____fcStack_2;
	// System.Int32 System.Text.RegularExpressions.RegexFCD::_fcDepth
	int32_t ____fcDepth_3;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_skipAllChildren
	bool ____skipAllChildren_4;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_skipchild
	bool ____skipchild_5;
	// System.Boolean System.Text.RegularExpressions.RegexFCD::_failed
	bool ____failed_6;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____intStack_0)); }
	inline Int32U5BU5D_t385246372* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t385246372* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier((&____intStack_0), value);
	}

	inline static int32_t get_offset_of__intDepth_1() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____intDepth_1)); }
	inline int32_t get__intDepth_1() const { return ____intDepth_1; }
	inline int32_t* get_address_of__intDepth_1() { return &____intDepth_1; }
	inline void set__intDepth_1(int32_t value)
	{
		____intDepth_1 = value;
	}

	inline static int32_t get_offset_of__fcStack_2() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____fcStack_2)); }
	inline RegexFCU5BU5D_t3722514598* get__fcStack_2() const { return ____fcStack_2; }
	inline RegexFCU5BU5D_t3722514598** get_address_of__fcStack_2() { return &____fcStack_2; }
	inline void set__fcStack_2(RegexFCU5BU5D_t3722514598* value)
	{
		____fcStack_2 = value;
		Il2CppCodeGenWriteBarrier((&____fcStack_2), value);
	}

	inline static int32_t get_offset_of__fcDepth_3() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____fcDepth_3)); }
	inline int32_t get__fcDepth_3() const { return ____fcDepth_3; }
	inline int32_t* get_address_of__fcDepth_3() { return &____fcDepth_3; }
	inline void set__fcDepth_3(int32_t value)
	{
		____fcDepth_3 = value;
	}

	inline static int32_t get_offset_of__skipAllChildren_4() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____skipAllChildren_4)); }
	inline bool get__skipAllChildren_4() const { return ____skipAllChildren_4; }
	inline bool* get_address_of__skipAllChildren_4() { return &____skipAllChildren_4; }
	inline void set__skipAllChildren_4(bool value)
	{
		____skipAllChildren_4 = value;
	}

	inline static int32_t get_offset_of__skipchild_5() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____skipchild_5)); }
	inline bool get__skipchild_5() const { return ____skipchild_5; }
	inline bool* get_address_of__skipchild_5() { return &____skipchild_5; }
	inline void set__skipchild_5(bool value)
	{
		____skipchild_5 = value;
	}

	inline static int32_t get_offset_of__failed_6() { return static_cast<int32_t>(offsetof(RegexFCD_t3501104819, ____failed_6)); }
	inline bool get__failed_6() const { return ____failed_6; }
	inline bool* get_address_of__failed_6() { return &____failed_6; }
	inline void set__failed_6(bool value)
	{
		____failed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXFCD_T3501104819_H
#ifndef REGEXPREFIX_T3750605839_H
#define REGEXPREFIX_T3750605839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexPrefix
struct  RegexPrefix_t3750605839  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.RegexPrefix::_prefix
	String_t* ____prefix_0;
	// System.Boolean System.Text.RegularExpressions.RegexPrefix::_caseInsensitive
	bool ____caseInsensitive_1;

public:
	inline static int32_t get_offset_of__prefix_0() { return static_cast<int32_t>(offsetof(RegexPrefix_t3750605839, ____prefix_0)); }
	inline String_t* get__prefix_0() const { return ____prefix_0; }
	inline String_t** get_address_of__prefix_0() { return &____prefix_0; }
	inline void set__prefix_0(String_t* value)
	{
		____prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefix_0), value);
	}

	inline static int32_t get_offset_of__caseInsensitive_1() { return static_cast<int32_t>(offsetof(RegexPrefix_t3750605839, ____caseInsensitive_1)); }
	inline bool get__caseInsensitive_1() const { return ____caseInsensitive_1; }
	inline bool* get_address_of__caseInsensitive_1() { return &____caseInsensitive_1; }
	inline void set__caseInsensitive_1(bool value)
	{
		____caseInsensitive_1 = value;
	}
};

struct RegexPrefix_t3750605839_StaticFields
{
public:
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexPrefix::_empty
	RegexPrefix_t3750605839 * ____empty_2;

public:
	inline static int32_t get_offset_of__empty_2() { return static_cast<int32_t>(offsetof(RegexPrefix_t3750605839_StaticFields, ____empty_2)); }
	inline RegexPrefix_t3750605839 * get__empty_2() const { return ____empty_2; }
	inline RegexPrefix_t3750605839 ** get_address_of__empty_2() { return &____empty_2; }
	inline void set__empty_2(RegexPrefix_t3750605839 * value)
	{
		____empty_2 = value;
		Il2CppCodeGenWriteBarrier((&____empty_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPREFIX_T3750605839_H
#ifndef REGEXREPLACEMENT_T2160826183_H
#define REGEXREPLACEMENT_T2160826183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexReplacement
struct  RegexReplacement_t2160826183  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.RegexReplacement::_rep
	String_t* ____rep_0;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexReplacement::_strings
	List_1_t3319525431 * ____strings_1;
	// System.Collections.Generic.List`1<System.Int32> System.Text.RegularExpressions.RegexReplacement::_rules
	List_1_t128053199 * ____rules_2;

public:
	inline static int32_t get_offset_of__rep_0() { return static_cast<int32_t>(offsetof(RegexReplacement_t2160826183, ____rep_0)); }
	inline String_t* get__rep_0() const { return ____rep_0; }
	inline String_t** get_address_of__rep_0() { return &____rep_0; }
	inline void set__rep_0(String_t* value)
	{
		____rep_0 = value;
		Il2CppCodeGenWriteBarrier((&____rep_0), value);
	}

	inline static int32_t get_offset_of__strings_1() { return static_cast<int32_t>(offsetof(RegexReplacement_t2160826183, ____strings_1)); }
	inline List_1_t3319525431 * get__strings_1() const { return ____strings_1; }
	inline List_1_t3319525431 ** get_address_of__strings_1() { return &____strings_1; }
	inline void set__strings_1(List_1_t3319525431 * value)
	{
		____strings_1 = value;
		Il2CppCodeGenWriteBarrier((&____strings_1), value);
	}

	inline static int32_t get_offset_of__rules_2() { return static_cast<int32_t>(offsetof(RegexReplacement_t2160826183, ____rules_2)); }
	inline List_1_t128053199 * get__rules_2() const { return ____rules_2; }
	inline List_1_t128053199 ** get_address_of__rules_2() { return &____rules_2; }
	inline void set__rules_2(List_1_t128053199 * value)
	{
		____rules_2 = value;
		Il2CppCodeGenWriteBarrier((&____rules_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXREPLACEMENT_T2160826183_H
#ifndef REGEXRUNNER_T300319648_H
#define REGEXRUNNER_T300319648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunner
struct  RegexRunner_t300319648  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextbeg
	int32_t ___runtextbeg_0;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextend
	int32_t ___runtextend_1;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextstart
	int32_t ___runtextstart_2;
	// System.String System.Text.RegularExpressions.RegexRunner::runtext
	String_t* ___runtext_3;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtextpos
	int32_t ___runtextpos_4;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runtrack
	Int32U5BU5D_t385246372* ___runtrack_5;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackpos
	int32_t ___runtrackpos_6;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runstack
	Int32U5BU5D_t385246372* ___runstack_7;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runstackpos
	int32_t ___runstackpos_8;
	// System.Int32[] System.Text.RegularExpressions.RegexRunner::runcrawl
	Int32U5BU5D_t385246372* ___runcrawl_9;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runcrawlpos
	int32_t ___runcrawlpos_10;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::runtrackcount
	int32_t ___runtrackcount_11;
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.RegexRunner::runmatch
	Match_t3408321083 * ___runmatch_12;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.RegexRunner::runregex
	Regex_t3657309853 * ___runregex_13;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeout
	int32_t ___timeout_14;
	// System.Boolean System.Text.RegularExpressions.RegexRunner::ignoreTimeout
	bool ___ignoreTimeout_15;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutOccursAt
	int32_t ___timeoutOccursAt_16;
	// System.Int32 System.Text.RegularExpressions.RegexRunner::timeoutChecksToSkip
	int32_t ___timeoutChecksToSkip_18;

public:
	inline static int32_t get_offset_of_runtextbeg_0() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtextbeg_0)); }
	inline int32_t get_runtextbeg_0() const { return ___runtextbeg_0; }
	inline int32_t* get_address_of_runtextbeg_0() { return &___runtextbeg_0; }
	inline void set_runtextbeg_0(int32_t value)
	{
		___runtextbeg_0 = value;
	}

	inline static int32_t get_offset_of_runtextend_1() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtextend_1)); }
	inline int32_t get_runtextend_1() const { return ___runtextend_1; }
	inline int32_t* get_address_of_runtextend_1() { return &___runtextend_1; }
	inline void set_runtextend_1(int32_t value)
	{
		___runtextend_1 = value;
	}

	inline static int32_t get_offset_of_runtextstart_2() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtextstart_2)); }
	inline int32_t get_runtextstart_2() const { return ___runtextstart_2; }
	inline int32_t* get_address_of_runtextstart_2() { return &___runtextstart_2; }
	inline void set_runtextstart_2(int32_t value)
	{
		___runtextstart_2 = value;
	}

	inline static int32_t get_offset_of_runtext_3() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtext_3)); }
	inline String_t* get_runtext_3() const { return ___runtext_3; }
	inline String_t** get_address_of_runtext_3() { return &___runtext_3; }
	inline void set_runtext_3(String_t* value)
	{
		___runtext_3 = value;
		Il2CppCodeGenWriteBarrier((&___runtext_3), value);
	}

	inline static int32_t get_offset_of_runtextpos_4() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtextpos_4)); }
	inline int32_t get_runtextpos_4() const { return ___runtextpos_4; }
	inline int32_t* get_address_of_runtextpos_4() { return &___runtextpos_4; }
	inline void set_runtextpos_4(int32_t value)
	{
		___runtextpos_4 = value;
	}

	inline static int32_t get_offset_of_runtrack_5() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtrack_5)); }
	inline Int32U5BU5D_t385246372* get_runtrack_5() const { return ___runtrack_5; }
	inline Int32U5BU5D_t385246372** get_address_of_runtrack_5() { return &___runtrack_5; }
	inline void set_runtrack_5(Int32U5BU5D_t385246372* value)
	{
		___runtrack_5 = value;
		Il2CppCodeGenWriteBarrier((&___runtrack_5), value);
	}

	inline static int32_t get_offset_of_runtrackpos_6() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtrackpos_6)); }
	inline int32_t get_runtrackpos_6() const { return ___runtrackpos_6; }
	inline int32_t* get_address_of_runtrackpos_6() { return &___runtrackpos_6; }
	inline void set_runtrackpos_6(int32_t value)
	{
		___runtrackpos_6 = value;
	}

	inline static int32_t get_offset_of_runstack_7() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runstack_7)); }
	inline Int32U5BU5D_t385246372* get_runstack_7() const { return ___runstack_7; }
	inline Int32U5BU5D_t385246372** get_address_of_runstack_7() { return &___runstack_7; }
	inline void set_runstack_7(Int32U5BU5D_t385246372* value)
	{
		___runstack_7 = value;
		Il2CppCodeGenWriteBarrier((&___runstack_7), value);
	}

	inline static int32_t get_offset_of_runstackpos_8() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runstackpos_8)); }
	inline int32_t get_runstackpos_8() const { return ___runstackpos_8; }
	inline int32_t* get_address_of_runstackpos_8() { return &___runstackpos_8; }
	inline void set_runstackpos_8(int32_t value)
	{
		___runstackpos_8 = value;
	}

	inline static int32_t get_offset_of_runcrawl_9() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runcrawl_9)); }
	inline Int32U5BU5D_t385246372* get_runcrawl_9() const { return ___runcrawl_9; }
	inline Int32U5BU5D_t385246372** get_address_of_runcrawl_9() { return &___runcrawl_9; }
	inline void set_runcrawl_9(Int32U5BU5D_t385246372* value)
	{
		___runcrawl_9 = value;
		Il2CppCodeGenWriteBarrier((&___runcrawl_9), value);
	}

	inline static int32_t get_offset_of_runcrawlpos_10() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runcrawlpos_10)); }
	inline int32_t get_runcrawlpos_10() const { return ___runcrawlpos_10; }
	inline int32_t* get_address_of_runcrawlpos_10() { return &___runcrawlpos_10; }
	inline void set_runcrawlpos_10(int32_t value)
	{
		___runcrawlpos_10 = value;
	}

	inline static int32_t get_offset_of_runtrackcount_11() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runtrackcount_11)); }
	inline int32_t get_runtrackcount_11() const { return ___runtrackcount_11; }
	inline int32_t* get_address_of_runtrackcount_11() { return &___runtrackcount_11; }
	inline void set_runtrackcount_11(int32_t value)
	{
		___runtrackcount_11 = value;
	}

	inline static int32_t get_offset_of_runmatch_12() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runmatch_12)); }
	inline Match_t3408321083 * get_runmatch_12() const { return ___runmatch_12; }
	inline Match_t3408321083 ** get_address_of_runmatch_12() { return &___runmatch_12; }
	inline void set_runmatch_12(Match_t3408321083 * value)
	{
		___runmatch_12 = value;
		Il2CppCodeGenWriteBarrier((&___runmatch_12), value);
	}

	inline static int32_t get_offset_of_runregex_13() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___runregex_13)); }
	inline Regex_t3657309853 * get_runregex_13() const { return ___runregex_13; }
	inline Regex_t3657309853 ** get_address_of_runregex_13() { return &___runregex_13; }
	inline void set_runregex_13(Regex_t3657309853 * value)
	{
		___runregex_13 = value;
		Il2CppCodeGenWriteBarrier((&___runregex_13), value);
	}

	inline static int32_t get_offset_of_timeout_14() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___timeout_14)); }
	inline int32_t get_timeout_14() const { return ___timeout_14; }
	inline int32_t* get_address_of_timeout_14() { return &___timeout_14; }
	inline void set_timeout_14(int32_t value)
	{
		___timeout_14 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeout_15() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___ignoreTimeout_15)); }
	inline bool get_ignoreTimeout_15() const { return ___ignoreTimeout_15; }
	inline bool* get_address_of_ignoreTimeout_15() { return &___ignoreTimeout_15; }
	inline void set_ignoreTimeout_15(bool value)
	{
		___ignoreTimeout_15 = value;
	}

	inline static int32_t get_offset_of_timeoutOccursAt_16() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___timeoutOccursAt_16)); }
	inline int32_t get_timeoutOccursAt_16() const { return ___timeoutOccursAt_16; }
	inline int32_t* get_address_of_timeoutOccursAt_16() { return &___timeoutOccursAt_16; }
	inline void set_timeoutOccursAt_16(int32_t value)
	{
		___timeoutOccursAt_16 = value;
	}

	inline static int32_t get_offset_of_timeoutChecksToSkip_18() { return static_cast<int32_t>(offsetof(RegexRunner_t300319648, ___timeoutChecksToSkip_18)); }
	inline int32_t get_timeoutChecksToSkip_18() const { return ___timeoutChecksToSkip_18; }
	inline int32_t* get_address_of_timeoutChecksToSkip_18() { return &___timeoutChecksToSkip_18; }
	inline void set_timeoutChecksToSkip_18(int32_t value)
	{
		___timeoutChecksToSkip_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNER_T300319648_H
#ifndef REGEXRUNNERFACTORY_T51159052_H
#define REGEXRUNNERFACTORY_T51159052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunnerFactory
struct  RegexRunnerFactory_t51159052  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNERFACTORY_T51159052_H
#ifndef REGEXWRITER_T2686991670_H
#define REGEXWRITER_T2686991670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexWriter
struct  RegexWriter_t2686991670  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_intStack
	Int32U5BU5D_t385246372* ____intStack_0;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_depth
	int32_t ____depth_1;
	// System.Int32[] System.Text.RegularExpressions.RegexWriter::_emitted
	Int32U5BU5D_t385246372* ____emitted_2;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_curpos
	int32_t ____curpos_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Text.RegularExpressions.RegexWriter::_stringhash
	Dictionary_2_t2736202052 * ____stringhash_4;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexWriter::_stringtable
	List_1_t3319525431 * ____stringtable_5;
	// System.Boolean System.Text.RegularExpressions.RegexWriter::_counting
	bool ____counting_6;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_count
	int32_t ____count_7;
	// System.Int32 System.Text.RegularExpressions.RegexWriter::_trackcount
	int32_t ____trackcount_8;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexWriter::_caps
	Hashtable_t1853889766 * ____caps_9;

public:
	inline static int32_t get_offset_of__intStack_0() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____intStack_0)); }
	inline Int32U5BU5D_t385246372* get__intStack_0() const { return ____intStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of__intStack_0() { return &____intStack_0; }
	inline void set__intStack_0(Int32U5BU5D_t385246372* value)
	{
		____intStack_0 = value;
		Il2CppCodeGenWriteBarrier((&____intStack_0), value);
	}

	inline static int32_t get_offset_of__depth_1() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____depth_1)); }
	inline int32_t get__depth_1() const { return ____depth_1; }
	inline int32_t* get_address_of__depth_1() { return &____depth_1; }
	inline void set__depth_1(int32_t value)
	{
		____depth_1 = value;
	}

	inline static int32_t get_offset_of__emitted_2() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____emitted_2)); }
	inline Int32U5BU5D_t385246372* get__emitted_2() const { return ____emitted_2; }
	inline Int32U5BU5D_t385246372** get_address_of__emitted_2() { return &____emitted_2; }
	inline void set__emitted_2(Int32U5BU5D_t385246372* value)
	{
		____emitted_2 = value;
		Il2CppCodeGenWriteBarrier((&____emitted_2), value);
	}

	inline static int32_t get_offset_of__curpos_3() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____curpos_3)); }
	inline int32_t get__curpos_3() const { return ____curpos_3; }
	inline int32_t* get_address_of__curpos_3() { return &____curpos_3; }
	inline void set__curpos_3(int32_t value)
	{
		____curpos_3 = value;
	}

	inline static int32_t get_offset_of__stringhash_4() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____stringhash_4)); }
	inline Dictionary_2_t2736202052 * get__stringhash_4() const { return ____stringhash_4; }
	inline Dictionary_2_t2736202052 ** get_address_of__stringhash_4() { return &____stringhash_4; }
	inline void set__stringhash_4(Dictionary_2_t2736202052 * value)
	{
		____stringhash_4 = value;
		Il2CppCodeGenWriteBarrier((&____stringhash_4), value);
	}

	inline static int32_t get_offset_of__stringtable_5() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____stringtable_5)); }
	inline List_1_t3319525431 * get__stringtable_5() const { return ____stringtable_5; }
	inline List_1_t3319525431 ** get_address_of__stringtable_5() { return &____stringtable_5; }
	inline void set__stringtable_5(List_1_t3319525431 * value)
	{
		____stringtable_5 = value;
		Il2CppCodeGenWriteBarrier((&____stringtable_5), value);
	}

	inline static int32_t get_offset_of__counting_6() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____counting_6)); }
	inline bool get__counting_6() const { return ____counting_6; }
	inline bool* get_address_of__counting_6() { return &____counting_6; }
	inline void set__counting_6(bool value)
	{
		____counting_6 = value;
	}

	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__trackcount_8() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____trackcount_8)); }
	inline int32_t get__trackcount_8() const { return ____trackcount_8; }
	inline int32_t* get_address_of__trackcount_8() { return &____trackcount_8; }
	inline void set__trackcount_8(int32_t value)
	{
		____trackcount_8 = value;
	}

	inline static int32_t get_offset_of__caps_9() { return static_cast<int32_t>(offsetof(RegexWriter_t2686991670, ____caps_9)); }
	inline Hashtable_t1853889766 * get__caps_9() const { return ____caps_9; }
	inline Hashtable_t1853889766 ** get_address_of__caps_9() { return &____caps_9; }
	inline void set__caps_9(Hashtable_t1853889766 * value)
	{
		____caps_9 = value;
		Il2CppCodeGenWriteBarrier((&____caps_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXWRITER_T2686991670_H
#ifndef SHAREDREFERENCE_T2916547576_H
#define SHAREDREFERENCE_T2916547576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.SharedReference
struct  SharedReference_t2916547576  : public RuntimeObject
{
public:
	// System.WeakReference System.Text.RegularExpressions.SharedReference::_ref
	WeakReference_t1334886716 * ____ref_0;
	// System.Int32 System.Text.RegularExpressions.SharedReference::_locked
	int32_t ____locked_1;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(SharedReference_t2916547576, ____ref_0)); }
	inline WeakReference_t1334886716 * get__ref_0() const { return ____ref_0; }
	inline WeakReference_t1334886716 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(WeakReference_t1334886716 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier((&____ref_0), value);
	}

	inline static int32_t get_offset_of__locked_1() { return static_cast<int32_t>(offsetof(SharedReference_t2916547576, ____locked_1)); }
	inline int32_t get__locked_1() const { return ____locked_1; }
	inline int32_t* get_address_of__locked_1() { return &____locked_1; }
	inline void set__locked_1(int32_t value)
	{
		____locked_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDREFERENCE_T2916547576_H
#ifndef UNCNAMEHELPER_T1428364228_H
#define UNCNAMEHELPER_T1428364228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UncNameHelper
struct  UncNameHelper_t1428364228  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNCNAMEHELPER_T1428364228_H
#ifndef MOREINFO_T2349391856_H
#define MOREINFO_T2349391856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/MoreInfo
struct  MoreInfo_t2349391856  : public RuntimeObject
{
public:
	// System.String System.Uri/MoreInfo::Path
	String_t* ___Path_0;
	// System.String System.Uri/MoreInfo::Query
	String_t* ___Query_1;
	// System.String System.Uri/MoreInfo::Fragment
	String_t* ___Fragment_2;
	// System.String System.Uri/MoreInfo::AbsoluteUri
	String_t* ___AbsoluteUri_3;
	// System.Int32 System.Uri/MoreInfo::Hash
	int32_t ___Hash_4;
	// System.String System.Uri/MoreInfo::RemoteUrl
	String_t* ___RemoteUrl_5;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(MoreInfo_t2349391856, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((&___Path_0), value);
	}

	inline static int32_t get_offset_of_Query_1() { return static_cast<int32_t>(offsetof(MoreInfo_t2349391856, ___Query_1)); }
	inline String_t* get_Query_1() const { return ___Query_1; }
	inline String_t** get_address_of_Query_1() { return &___Query_1; }
	inline void set_Query_1(String_t* value)
	{
		___Query_1 = value;
		Il2CppCodeGenWriteBarrier((&___Query_1), value);
	}

	inline static int32_t get_offset_of_Fragment_2() { return static_cast<int32_t>(offsetof(MoreInfo_t2349391856, ___Fragment_2)); }
	inline String_t* get_Fragment_2() const { return ___Fragment_2; }
	inline String_t** get_address_of_Fragment_2() { return &___Fragment_2; }
	inline void set_Fragment_2(String_t* value)
	{
		___Fragment_2 = value;
		Il2CppCodeGenWriteBarrier((&___Fragment_2), value);
	}

	inline static int32_t get_offset_of_AbsoluteUri_3() { return static_cast<int32_t>(offsetof(MoreInfo_t2349391856, ___AbsoluteUri_3)); }
	inline String_t* get_AbsoluteUri_3() const { return ___AbsoluteUri_3; }
	inline String_t** get_address_of_AbsoluteUri_3() { return &___AbsoluteUri_3; }
	inline void set_AbsoluteUri_3(String_t* value)
	{
		___AbsoluteUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___AbsoluteUri_3), value);
	}

	inline static int32_t get_offset_of_Hash_4() { return static_cast<int32_t>(offsetof(MoreInfo_t2349391856, ___Hash_4)); }
	inline int32_t get_Hash_4() const { return ___Hash_4; }
	inline int32_t* get_address_of_Hash_4() { return &___Hash_4; }
	inline void set_Hash_4(int32_t value)
	{
		___Hash_4 = value;
	}

	inline static int32_t get_offset_of_RemoteUrl_5() { return static_cast<int32_t>(offsetof(MoreInfo_t2349391856, ___RemoteUrl_5)); }
	inline String_t* get_RemoteUrl_5() const { return ___RemoteUrl_5; }
	inline String_t** get_address_of_RemoteUrl_5() { return &___RemoteUrl_5; }
	inline void set_RemoteUrl_5(String_t* value)
	{
		___RemoteUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteUrl_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOREINFO_T2349391856_H
#ifndef URIBUILDER_T579353065_H
#define URIBUILDER_T579353065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriBuilder
struct  UriBuilder_t579353065  : public RuntimeObject
{
public:
	// System.Boolean System.UriBuilder::_changed
	bool ____changed_0;
	// System.String System.UriBuilder::_fragment
	String_t* ____fragment_1;
	// System.String System.UriBuilder::_host
	String_t* ____host_2;
	// System.String System.UriBuilder::_password
	String_t* ____password_3;
	// System.String System.UriBuilder::_path
	String_t* ____path_4;
	// System.Int32 System.UriBuilder::_port
	int32_t ____port_5;
	// System.String System.UriBuilder::_query
	String_t* ____query_6;
	// System.String System.UriBuilder::_scheme
	String_t* ____scheme_7;
	// System.String System.UriBuilder::_schemeDelimiter
	String_t* ____schemeDelimiter_8;
	// System.Uri System.UriBuilder::_uri
	Uri_t100236324 * ____uri_9;
	// System.String System.UriBuilder::_username
	String_t* ____username_10;

public:
	inline static int32_t get_offset_of__changed_0() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____changed_0)); }
	inline bool get__changed_0() const { return ____changed_0; }
	inline bool* get_address_of__changed_0() { return &____changed_0; }
	inline void set__changed_0(bool value)
	{
		____changed_0 = value;
	}

	inline static int32_t get_offset_of__fragment_1() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____fragment_1)); }
	inline String_t* get__fragment_1() const { return ____fragment_1; }
	inline String_t** get_address_of__fragment_1() { return &____fragment_1; }
	inline void set__fragment_1(String_t* value)
	{
		____fragment_1 = value;
		Il2CppCodeGenWriteBarrier((&____fragment_1), value);
	}

	inline static int32_t get_offset_of__host_2() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____host_2)); }
	inline String_t* get__host_2() const { return ____host_2; }
	inline String_t** get_address_of__host_2() { return &____host_2; }
	inline void set__host_2(String_t* value)
	{
		____host_2 = value;
		Il2CppCodeGenWriteBarrier((&____host_2), value);
	}

	inline static int32_t get_offset_of__password_3() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____password_3)); }
	inline String_t* get__password_3() const { return ____password_3; }
	inline String_t** get_address_of__password_3() { return &____password_3; }
	inline void set__password_3(String_t* value)
	{
		____password_3 = value;
		Il2CppCodeGenWriteBarrier((&____password_3), value);
	}

	inline static int32_t get_offset_of__path_4() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____path_4)); }
	inline String_t* get__path_4() const { return ____path_4; }
	inline String_t** get_address_of__path_4() { return &____path_4; }
	inline void set__path_4(String_t* value)
	{
		____path_4 = value;
		Il2CppCodeGenWriteBarrier((&____path_4), value);
	}

	inline static int32_t get_offset_of__port_5() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____port_5)); }
	inline int32_t get__port_5() const { return ____port_5; }
	inline int32_t* get_address_of__port_5() { return &____port_5; }
	inline void set__port_5(int32_t value)
	{
		____port_5 = value;
	}

	inline static int32_t get_offset_of__query_6() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____query_6)); }
	inline String_t* get__query_6() const { return ____query_6; }
	inline String_t** get_address_of__query_6() { return &____query_6; }
	inline void set__query_6(String_t* value)
	{
		____query_6 = value;
		Il2CppCodeGenWriteBarrier((&____query_6), value);
	}

	inline static int32_t get_offset_of__scheme_7() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____scheme_7)); }
	inline String_t* get__scheme_7() const { return ____scheme_7; }
	inline String_t** get_address_of__scheme_7() { return &____scheme_7; }
	inline void set__scheme_7(String_t* value)
	{
		____scheme_7 = value;
		Il2CppCodeGenWriteBarrier((&____scheme_7), value);
	}

	inline static int32_t get_offset_of__schemeDelimiter_8() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____schemeDelimiter_8)); }
	inline String_t* get__schemeDelimiter_8() const { return ____schemeDelimiter_8; }
	inline String_t** get_address_of__schemeDelimiter_8() { return &____schemeDelimiter_8; }
	inline void set__schemeDelimiter_8(String_t* value)
	{
		____schemeDelimiter_8 = value;
		Il2CppCodeGenWriteBarrier((&____schemeDelimiter_8), value);
	}

	inline static int32_t get_offset_of__uri_9() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____uri_9)); }
	inline Uri_t100236324 * get__uri_9() const { return ____uri_9; }
	inline Uri_t100236324 ** get_address_of__uri_9() { return &____uri_9; }
	inline void set__uri_9(Uri_t100236324 * value)
	{
		____uri_9 = value;
		Il2CppCodeGenWriteBarrier((&____uri_9), value);
	}

	inline static int32_t get_offset_of__username_10() { return static_cast<int32_t>(offsetof(UriBuilder_t579353065, ____username_10)); }
	inline String_t* get__username_10() const { return ____username_10; }
	inline String_t** get_address_of__username_10() { return &____username_10; }
	inline void set__username_10(String_t* value)
	{
		____username_10 = value;
		Il2CppCodeGenWriteBarrier((&____username_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIBUILDER_T579353065_H
#ifndef URIHELPER_T3559114794_H
#define URIHELPER_T3559114794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHelper
struct  UriHelper_t3559114794  : public RuntimeObject
{
public:

public:
};

struct UriHelper_t3559114794_StaticFields
{
public:
	// System.Char[] System.UriHelper::HexUpperChars
	CharU5BU5D_t3528271667* ___HexUpperChars_0;

public:
	inline static int32_t get_offset_of_HexUpperChars_0() { return static_cast<int32_t>(offsetof(UriHelper_t3559114794_StaticFields, ___HexUpperChars_0)); }
	inline CharU5BU5D_t3528271667* get_HexUpperChars_0() const { return ___HexUpperChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_HexUpperChars_0() { return &___HexUpperChars_0; }
	inline void set_HexUpperChars_0(CharU5BU5D_t3528271667* value)
	{
		___HexUpperChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___HexUpperChars_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHELPER_T3559114794_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ASYNCHANDSHAKEREQUEST_T1887639929_H
#define ASYNCHANDSHAKEREQUEST_T1887639929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncHandshakeRequest
struct  AsyncHandshakeRequest_t1887639929  : public AsyncProtocolRequest_t4184368197
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCHANDSHAKEREQUEST_T1887639929_H
#ifndef ASYNCREADORWRITEREQUEST_T2457631485_H
#define ASYNCREADORWRITEREQUEST_T2457631485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncReadOrWriteRequest
struct  AsyncReadOrWriteRequest_t2457631485  : public AsyncProtocolRequest_t4184368197
{
public:
	// Mono.Net.Security.BufferOffsetSize Mono.Net.Security.AsyncReadOrWriteRequest::<UserBuffer>k__BackingField
	BufferOffsetSize_t3399811029 * ___U3CUserBufferU3Ek__BackingField_7;
	// System.Int32 Mono.Net.Security.AsyncReadOrWriteRequest::<CurrentSize>k__BackingField
	int32_t ___U3CCurrentSizeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CUserBufferU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AsyncReadOrWriteRequest_t2457631485, ___U3CUserBufferU3Ek__BackingField_7)); }
	inline BufferOffsetSize_t3399811029 * get_U3CUserBufferU3Ek__BackingField_7() const { return ___U3CUserBufferU3Ek__BackingField_7; }
	inline BufferOffsetSize_t3399811029 ** get_address_of_U3CUserBufferU3Ek__BackingField_7() { return &___U3CUserBufferU3Ek__BackingField_7; }
	inline void set_U3CUserBufferU3Ek__BackingField_7(BufferOffsetSize_t3399811029 * value)
	{
		___U3CUserBufferU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserBufferU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CCurrentSizeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AsyncReadOrWriteRequest_t2457631485, ___U3CCurrentSizeU3Ek__BackingField_8)); }
	inline int32_t get_U3CCurrentSizeU3Ek__BackingField_8() const { return ___U3CCurrentSizeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CCurrentSizeU3Ek__BackingField_8() { return &___U3CCurrentSizeU3Ek__BackingField_8; }
	inline void set_U3CCurrentSizeU3Ek__BackingField_8(int32_t value)
	{
		___U3CCurrentSizeU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREADORWRITEREQUEST_T2457631485_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef EXCLUDEFROMCODECOVERAGEATTRIBUTE_T3578759089_H
#define EXCLUDEFROMCODECOVERAGEATTRIBUTE_T3578759089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute
struct  ExcludeFromCodeCoverageAttribute_t3578759089  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEFROMCODECOVERAGEATTRIBUTE_T3578759089_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t156472862 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2974092902 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t156472862 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t156472862 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t156472862 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t1273022909, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2974092902 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2974092902 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2974092902 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef ASYNCMETHODBUILDERCORE_T2955600131_H
#define ASYNCMETHODBUILDERCORE_T2955600131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2955600131 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t1264377477 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2955600131, ___m_defaultContextAction_1)); }
	inline Action_t1264377477 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t1264377477 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t1264377477 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2955600131_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T2955600131_H
#ifndef CONFIGUREDTASKAWAITER_T555647845_H
#define CONFIGUREDTASKAWAITER_T555647845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct  ConfiguredTaskAwaiter_t555647845 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter::m_task
	Task_t3187275312 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t555647845, ___m_task_0)); }
	inline Task_t3187275312 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t3187275312 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t3187275312 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t555647845, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct ConfiguredTaskAwaiter_t555647845_marshaled_pinvoke
{
	Task_t3187275312 * ___m_task_0;
	int32_t ___m_continueOnCapturedContext_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct ConfiguredTaskAwaiter_t555647845_marshaled_com
{
	Task_t3187275312 * ___m_task_0;
	int32_t ___m_continueOnCapturedContext_1;
};
#endif // CONFIGUREDTASKAWAITER_T555647845_H
#ifndef CONFIGUREDTASKAWAITER_T410331069_H
#define CONFIGUREDTASKAWAITER_T410331069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Mono.Net.Security.AsyncProtocolResult>
struct  ConfiguredTaskAwaiter_t410331069 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t493370259 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t410331069, ___m_task_0)); }
	inline Task_1_t493370259 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t493370259 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t493370259 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t410331069, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T410331069_H
#ifndef CONFIGUREDTASKAWAITER_T4273446738_H
#define CONFIGUREDTASKAWAITER_T4273446738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32>
struct  ConfiguredTaskAwaiter_t4273446738 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t61518632 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t4273446738, ___m_task_0)); }
	inline Task_1_t61518632 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t61518632 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t61518632 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t4273446738, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T4273446738_H
#ifndef CONFIGUREDTASKAWAITER_T1701041524_H
#define CONFIGUREDTASKAWAITER_T1701041524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Nullable`1<System.Int32>>
struct  ConfiguredTaskAwaiter_t1701041524 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t1784080714 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t1701041524, ___m_task_0)); }
	inline Task_1_t1784080714 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t1784080714 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t1784080714 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t1701041524, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T1701041524_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef GROUP_T2468205786_H
#define GROUP_T2468205786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t2468205786  : public Capture_t2232016050
{
public:
	// System.Int32[] System.Text.RegularExpressions.Group::_caps
	Int32U5BU5D_t385246372* ____caps_4;
	// System.Int32 System.Text.RegularExpressions.Group::_capcount
	int32_t ____capcount_5;
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::_capcoll
	CaptureCollection_t1760593541 * ____capcoll_6;
	// System.String System.Text.RegularExpressions.Group::_name
	String_t* ____name_7;

public:
	inline static int32_t get_offset_of__caps_4() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____caps_4)); }
	inline Int32U5BU5D_t385246372* get__caps_4() const { return ____caps_4; }
	inline Int32U5BU5D_t385246372** get_address_of__caps_4() { return &____caps_4; }
	inline void set__caps_4(Int32U5BU5D_t385246372* value)
	{
		____caps_4 = value;
		Il2CppCodeGenWriteBarrier((&____caps_4), value);
	}

	inline static int32_t get_offset_of__capcount_5() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____capcount_5)); }
	inline int32_t get__capcount_5() const { return ____capcount_5; }
	inline int32_t* get_address_of__capcount_5() { return &____capcount_5; }
	inline void set__capcount_5(int32_t value)
	{
		____capcount_5 = value;
	}

	inline static int32_t get_offset_of__capcoll_6() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____capcoll_6)); }
	inline CaptureCollection_t1760593541 * get__capcoll_6() const { return ____capcoll_6; }
	inline CaptureCollection_t1760593541 ** get_address_of__capcoll_6() { return &____capcoll_6; }
	inline void set__capcoll_6(CaptureCollection_t1760593541 * value)
	{
		____capcoll_6 = value;
		Il2CppCodeGenWriteBarrier((&____capcoll_6), value);
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(Group_t2468205786, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}
};

struct Group_t2468205786_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::_emptygroup
	Group_t2468205786 * ____emptygroup_3;

public:
	inline static int32_t get_offset_of__emptygroup_3() { return static_cast<int32_t>(offsetof(Group_t2468205786_StaticFields, ____emptygroup_3)); }
	inline Group_t2468205786 * get__emptygroup_3() const { return ____emptygroup_3; }
	inline Group_t2468205786 ** get_address_of__emptygroup_3() { return &____emptygroup_3; }
	inline void set__emptygroup_3(Group_t2468205786 * value)
	{
		____emptygroup_3 = value;
		Il2CppCodeGenWriteBarrier((&____emptygroup_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T2468205786_H
#ifndef LOWERCASEMAPPING_T2910317575_H
#define LOWERCASEMAPPING_T2910317575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct  LowerCaseMapping_t2910317575 
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMin
	Il2CppChar ____chMin_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMax
	Il2CppChar ____chMax_1;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_lcOp
	int32_t ____lcOp_2;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_data
	int32_t ____data_3;

public:
	inline static int32_t get_offset_of__chMin_0() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____chMin_0)); }
	inline Il2CppChar get__chMin_0() const { return ____chMin_0; }
	inline Il2CppChar* get_address_of__chMin_0() { return &____chMin_0; }
	inline void set__chMin_0(Il2CppChar value)
	{
		____chMin_0 = value;
	}

	inline static int32_t get_offset_of__chMax_1() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____chMax_1)); }
	inline Il2CppChar get__chMax_1() const { return ____chMax_1; }
	inline Il2CppChar* get_address_of__chMax_1() { return &____chMax_1; }
	inline void set__chMax_1(Il2CppChar value)
	{
		____chMax_1 = value;
	}

	inline static int32_t get_offset_of__lcOp_2() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____lcOp_2)); }
	inline int32_t get__lcOp_2() const { return ____lcOp_2; }
	inline int32_t* get_address_of__lcOp_2() { return &____lcOp_2; }
	inline void set__lcOp_2(int32_t value)
	{
		____lcOp_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2910317575, ____data_3)); }
	inline int32_t get__data_3() const { return ____data_3; }
	inline int32_t* get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(int32_t value)
	{
		____data_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2910317575_marshaled_pinvoke
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2910317575_marshaled_com
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
#endif // LOWERCASEMAPPING_T2910317575_H
#ifndef REGEXINTERPRETER_T1494189873_H
#define REGEXINTERPRETER_T1494189873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexInterpreter
struct  RegexInterpreter_t1494189873  : public RegexRunner_t300319648
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runoperator
	int32_t ___runoperator_19;
	// System.Int32[] System.Text.RegularExpressions.RegexInterpreter::runcodes
	Int32U5BU5D_t385246372* ___runcodes_20;
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runcodepos
	int32_t ___runcodepos_21;
	// System.String[] System.Text.RegularExpressions.RegexInterpreter::runstrings
	StringU5BU5D_t1281789340* ___runstrings_22;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexInterpreter::runcode
	RegexCode_t4293407246 * ___runcode_23;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexInterpreter::runfcPrefix
	RegexPrefix_t3750605839 * ___runfcPrefix_24;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexInterpreter::runbmPrefix
	RegexBoyerMoore_t1480609368 * ___runbmPrefix_25;
	// System.Int32 System.Text.RegularExpressions.RegexInterpreter::runanchors
	int32_t ___runanchors_26;
	// System.Boolean System.Text.RegularExpressions.RegexInterpreter::runrtl
	bool ___runrtl_27;
	// System.Boolean System.Text.RegularExpressions.RegexInterpreter::runci
	bool ___runci_28;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexInterpreter::runculture
	CultureInfo_t4157843068 * ___runculture_29;

public:
	inline static int32_t get_offset_of_runoperator_19() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runoperator_19)); }
	inline int32_t get_runoperator_19() const { return ___runoperator_19; }
	inline int32_t* get_address_of_runoperator_19() { return &___runoperator_19; }
	inline void set_runoperator_19(int32_t value)
	{
		___runoperator_19 = value;
	}

	inline static int32_t get_offset_of_runcodes_20() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runcodes_20)); }
	inline Int32U5BU5D_t385246372* get_runcodes_20() const { return ___runcodes_20; }
	inline Int32U5BU5D_t385246372** get_address_of_runcodes_20() { return &___runcodes_20; }
	inline void set_runcodes_20(Int32U5BU5D_t385246372* value)
	{
		___runcodes_20 = value;
		Il2CppCodeGenWriteBarrier((&___runcodes_20), value);
	}

	inline static int32_t get_offset_of_runcodepos_21() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runcodepos_21)); }
	inline int32_t get_runcodepos_21() const { return ___runcodepos_21; }
	inline int32_t* get_address_of_runcodepos_21() { return &___runcodepos_21; }
	inline void set_runcodepos_21(int32_t value)
	{
		___runcodepos_21 = value;
	}

	inline static int32_t get_offset_of_runstrings_22() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runstrings_22)); }
	inline StringU5BU5D_t1281789340* get_runstrings_22() const { return ___runstrings_22; }
	inline StringU5BU5D_t1281789340** get_address_of_runstrings_22() { return &___runstrings_22; }
	inline void set_runstrings_22(StringU5BU5D_t1281789340* value)
	{
		___runstrings_22 = value;
		Il2CppCodeGenWriteBarrier((&___runstrings_22), value);
	}

	inline static int32_t get_offset_of_runcode_23() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runcode_23)); }
	inline RegexCode_t4293407246 * get_runcode_23() const { return ___runcode_23; }
	inline RegexCode_t4293407246 ** get_address_of_runcode_23() { return &___runcode_23; }
	inline void set_runcode_23(RegexCode_t4293407246 * value)
	{
		___runcode_23 = value;
		Il2CppCodeGenWriteBarrier((&___runcode_23), value);
	}

	inline static int32_t get_offset_of_runfcPrefix_24() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runfcPrefix_24)); }
	inline RegexPrefix_t3750605839 * get_runfcPrefix_24() const { return ___runfcPrefix_24; }
	inline RegexPrefix_t3750605839 ** get_address_of_runfcPrefix_24() { return &___runfcPrefix_24; }
	inline void set_runfcPrefix_24(RegexPrefix_t3750605839 * value)
	{
		___runfcPrefix_24 = value;
		Il2CppCodeGenWriteBarrier((&___runfcPrefix_24), value);
	}

	inline static int32_t get_offset_of_runbmPrefix_25() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runbmPrefix_25)); }
	inline RegexBoyerMoore_t1480609368 * get_runbmPrefix_25() const { return ___runbmPrefix_25; }
	inline RegexBoyerMoore_t1480609368 ** get_address_of_runbmPrefix_25() { return &___runbmPrefix_25; }
	inline void set_runbmPrefix_25(RegexBoyerMoore_t1480609368 * value)
	{
		___runbmPrefix_25 = value;
		Il2CppCodeGenWriteBarrier((&___runbmPrefix_25), value);
	}

	inline static int32_t get_offset_of_runanchors_26() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runanchors_26)); }
	inline int32_t get_runanchors_26() const { return ___runanchors_26; }
	inline int32_t* get_address_of_runanchors_26() { return &___runanchors_26; }
	inline void set_runanchors_26(int32_t value)
	{
		___runanchors_26 = value;
	}

	inline static int32_t get_offset_of_runrtl_27() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runrtl_27)); }
	inline bool get_runrtl_27() const { return ___runrtl_27; }
	inline bool* get_address_of_runrtl_27() { return &___runrtl_27; }
	inline void set_runrtl_27(bool value)
	{
		___runrtl_27 = value;
	}

	inline static int32_t get_offset_of_runci_28() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runci_28)); }
	inline bool get_runci_28() const { return ___runci_28; }
	inline bool* get_address_of_runci_28() { return &___runci_28; }
	inline void set_runci_28(bool value)
	{
		___runci_28 = value;
	}

	inline static int32_t get_offset_of_runculture_29() { return static_cast<int32_t>(offsetof(RegexInterpreter_t1494189873, ___runculture_29)); }
	inline CultureInfo_t4157843068 * get_runculture_29() const { return ___runculture_29; }
	inline CultureInfo_t4157843068 ** get_address_of_runculture_29() { return &___runculture_29; }
	inline void set_runculture_29(CultureInfo_t4157843068 * value)
	{
		___runculture_29 = value;
		Il2CppCodeGenWriteBarrier((&___runculture_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXINTERPRETER_T1494189873_H
#ifndef CANCELLATIONTOKEN_T784455623_H
#define CANCELLATIONTOKEN_T784455623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationToken
struct  CancellationToken_t784455623 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_t540272775 * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t784455623, ___m_source_0)); }
	inline CancellationTokenSource_t540272775 * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_t540272775 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_t540272775 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_source_0), value);
	}
};

struct CancellationToken_t784455623_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_t3252573759 * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_t784455623_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_t3252573759 * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_t3252573759 ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_t3252573759 * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActionToActionObjShunt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t784455623_marshaled_pinvoke
{
	CancellationTokenSource_t540272775 * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t784455623_marshaled_com
{
	CancellationTokenSource_t540272775 * ___m_source_0;
};
#endif // CANCELLATIONTOKEN_T784455623_H
#ifndef OFFSET_T4247613535_H
#define OFFSET_T4247613535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Offset
#pragma pack(push, tp, 1)
struct  Offset_t4247613535 
{
public:
	// System.UInt16 System.Uri/Offset::Scheme
	uint16_t ___Scheme_0;
	// System.UInt16 System.Uri/Offset::User
	uint16_t ___User_1;
	// System.UInt16 System.Uri/Offset::Host
	uint16_t ___Host_2;
	// System.UInt16 System.Uri/Offset::PortValue
	uint16_t ___PortValue_3;
	// System.UInt16 System.Uri/Offset::Path
	uint16_t ___Path_4;
	// System.UInt16 System.Uri/Offset::Query
	uint16_t ___Query_5;
	// System.UInt16 System.Uri/Offset::Fragment
	uint16_t ___Fragment_6;
	// System.UInt16 System.Uri/Offset::End
	uint16_t ___End_7;

public:
	inline static int32_t get_offset_of_Scheme_0() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___Scheme_0)); }
	inline uint16_t get_Scheme_0() const { return ___Scheme_0; }
	inline uint16_t* get_address_of_Scheme_0() { return &___Scheme_0; }
	inline void set_Scheme_0(uint16_t value)
	{
		___Scheme_0 = value;
	}

	inline static int32_t get_offset_of_User_1() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___User_1)); }
	inline uint16_t get_User_1() const { return ___User_1; }
	inline uint16_t* get_address_of_User_1() { return &___User_1; }
	inline void set_User_1(uint16_t value)
	{
		___User_1 = value;
	}

	inline static int32_t get_offset_of_Host_2() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___Host_2)); }
	inline uint16_t get_Host_2() const { return ___Host_2; }
	inline uint16_t* get_address_of_Host_2() { return &___Host_2; }
	inline void set_Host_2(uint16_t value)
	{
		___Host_2 = value;
	}

	inline static int32_t get_offset_of_PortValue_3() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___PortValue_3)); }
	inline uint16_t get_PortValue_3() const { return ___PortValue_3; }
	inline uint16_t* get_address_of_PortValue_3() { return &___PortValue_3; }
	inline void set_PortValue_3(uint16_t value)
	{
		___PortValue_3 = value;
	}

	inline static int32_t get_offset_of_Path_4() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___Path_4)); }
	inline uint16_t get_Path_4() const { return ___Path_4; }
	inline uint16_t* get_address_of_Path_4() { return &___Path_4; }
	inline void set_Path_4(uint16_t value)
	{
		___Path_4 = value;
	}

	inline static int32_t get_offset_of_Query_5() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___Query_5)); }
	inline uint16_t get_Query_5() const { return ___Query_5; }
	inline uint16_t* get_address_of_Query_5() { return &___Query_5; }
	inline void set_Query_5(uint16_t value)
	{
		___Query_5 = value;
	}

	inline static int32_t get_offset_of_Fragment_6() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___Fragment_6)); }
	inline uint16_t get_Fragment_6() const { return ___Fragment_6; }
	inline uint16_t* get_address_of_Fragment_6() { return &___Fragment_6; }
	inline void set_Fragment_6(uint16_t value)
	{
		___Fragment_6 = value;
	}

	inline static int32_t get_offset_of_End_7() { return static_cast<int32_t>(offsetof(Offset_t4247613535, ___End_7)); }
	inline uint16_t get_End_7() const { return ___End_7; }
	inline uint16_t* get_address_of_End_7() { return &___End_7; }
	inline void set_End_7(uint16_t value)
	{
		___End_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFSET_T4247613535_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ASYNCOPERATIONSTATUS_T2187364345_H
#define ASYNCOPERATIONSTATUS_T2187364345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncOperationStatus
struct  AsyncOperationStatus_t2187364345 
{
public:
	// System.Int32 Mono.Net.Security.AsyncOperationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncOperationStatus_t2187364345, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCOPERATIONSTATUS_T2187364345_H
#ifndef ASYNCREADREQUEST_T2148115577_H
#define ASYNCREADREQUEST_T2148115577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncReadRequest
struct  AsyncReadRequest_t2148115577  : public AsyncReadOrWriteRequest_t2457631485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREADREQUEST_T2148115577_H
#ifndef ASYNCWRITEREQUEST_T2930005_H
#define ASYNCWRITEREQUEST_T2930005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncWriteRequest
struct  AsyncWriteRequest_t2930005  : public AsyncReadOrWriteRequest_t2457631485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCWRITEREQUEST_T2930005_H
#ifndef OPERATIONTYPE_T993417619_H
#define OPERATIONTYPE_T993417619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream/OperationType
struct  OperationType_t993417619 
{
public:
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/OperationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperationType_t993417619, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONTYPE_T993417619_H
#ifndef MONOTLSPROVIDERFACTORY_T4084560240_H
#define MONOTLSPROVIDERFACTORY_T4084560240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsProviderFactory
struct  MonoTlsProviderFactory_t4084560240  : public RuntimeObject
{
public:

public:
};

struct MonoTlsProviderFactory_t4084560240_StaticFields
{
public:
	// System.Object Mono.Net.Security.MonoTlsProviderFactory::locker
	RuntimeObject * ___locker_0;
	// System.Boolean Mono.Net.Security.MonoTlsProviderFactory::initialized
	bool ___initialized_1;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.MonoTlsProviderFactory::defaultProvider
	MonoTlsProvider_t3152003291 * ___defaultProvider_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Tuple`2<System.Guid,System.String>> Mono.Net.Security.MonoTlsProviderFactory::providerRegistration
	Dictionary_2_t3262373276 * ___providerRegistration_3;
	// System.Collections.Generic.Dictionary`2<System.Guid,Mono.Security.Interface.MonoTlsProvider> Mono.Net.Security.MonoTlsProviderFactory::providerCache
	Dictionary_2_t2201020824 * ___providerCache_4;
	// System.Guid Mono.Net.Security.MonoTlsProviderFactory::UnityTlsId
	Guid_t  ___UnityTlsId_5;
	// System.Guid Mono.Net.Security.MonoTlsProviderFactory::AppleTlsId
	Guid_t  ___AppleTlsId_6;
	// System.Guid Mono.Net.Security.MonoTlsProviderFactory::BtlsId
	Guid_t  ___BtlsId_7;
	// System.Guid Mono.Net.Security.MonoTlsProviderFactory::LegacyId
	Guid_t  ___LegacyId_8;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_defaultProvider_2() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___defaultProvider_2)); }
	inline MonoTlsProvider_t3152003291 * get_defaultProvider_2() const { return ___defaultProvider_2; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_defaultProvider_2() { return &___defaultProvider_2; }
	inline void set_defaultProvider_2(MonoTlsProvider_t3152003291 * value)
	{
		___defaultProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultProvider_2), value);
	}

	inline static int32_t get_offset_of_providerRegistration_3() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___providerRegistration_3)); }
	inline Dictionary_2_t3262373276 * get_providerRegistration_3() const { return ___providerRegistration_3; }
	inline Dictionary_2_t3262373276 ** get_address_of_providerRegistration_3() { return &___providerRegistration_3; }
	inline void set_providerRegistration_3(Dictionary_2_t3262373276 * value)
	{
		___providerRegistration_3 = value;
		Il2CppCodeGenWriteBarrier((&___providerRegistration_3), value);
	}

	inline static int32_t get_offset_of_providerCache_4() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___providerCache_4)); }
	inline Dictionary_2_t2201020824 * get_providerCache_4() const { return ___providerCache_4; }
	inline Dictionary_2_t2201020824 ** get_address_of_providerCache_4() { return &___providerCache_4; }
	inline void set_providerCache_4(Dictionary_2_t2201020824 * value)
	{
		___providerCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___providerCache_4), value);
	}

	inline static int32_t get_offset_of_UnityTlsId_5() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___UnityTlsId_5)); }
	inline Guid_t  get_UnityTlsId_5() const { return ___UnityTlsId_5; }
	inline Guid_t * get_address_of_UnityTlsId_5() { return &___UnityTlsId_5; }
	inline void set_UnityTlsId_5(Guid_t  value)
	{
		___UnityTlsId_5 = value;
	}

	inline static int32_t get_offset_of_AppleTlsId_6() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___AppleTlsId_6)); }
	inline Guid_t  get_AppleTlsId_6() const { return ___AppleTlsId_6; }
	inline Guid_t * get_address_of_AppleTlsId_6() { return &___AppleTlsId_6; }
	inline void set_AppleTlsId_6(Guid_t  value)
	{
		___AppleTlsId_6 = value;
	}

	inline static int32_t get_offset_of_BtlsId_7() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___BtlsId_7)); }
	inline Guid_t  get_BtlsId_7() const { return ___BtlsId_7; }
	inline Guid_t * get_address_of_BtlsId_7() { return &___BtlsId_7; }
	inline void set_BtlsId_7(Guid_t  value)
	{
		___BtlsId_7 = value;
	}

	inline static int32_t get_offset_of_LegacyId_8() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t4084560240_StaticFields, ___LegacyId_8)); }
	inline Guid_t  get_LegacyId_8() const { return ___LegacyId_8; }
	inline Guid_t * get_address_of_LegacyId_8() { return &___LegacyId_8; }
	inline void set_LegacyId_8(Guid_t  value)
	{
		___LegacyId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDERFACTORY_T4084560240_H
#ifndef MONOSSLPOLICYERRORS_T2590217945_H
#define MONOSSLPOLICYERRORS_T2590217945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoSslPolicyErrors
struct  MonoSslPolicyErrors_t2590217945 
{
public:
	// System.Int32 Mono.Security.Interface.MonoSslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonoSslPolicyErrors_t2590217945, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLPOLICYERRORS_T2590217945_H
#ifndef TYPECONVERTER_T2249118273_H
#define TYPECONVERTER_T2249118273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t2249118273  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t2249118273_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t2249118273_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T2249118273_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef FORMATEXCEPTION_T154580423_H
#define FORMATEXCEPTION_T154580423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t154580423  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T154580423_H
#ifndef IOOPERATION_T344984103_H
#define IOOPERATION_T344984103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOOperation
struct  IOOperation_t344984103 
{
public:
	// System.Int32 System.IOOperation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IOOperation_t344984103, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOOPERATION_T344984103_H
#ifndef AUTHENTICATEDSTREAM_T3415418016_H
#define AUTHENTICATEDSTREAM_T3415418016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticatedStream
struct  AuthenticatedStream_t3415418016  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.Net.Security.AuthenticatedStream::_InnerStream
	Stream_t1273022909 * ____InnerStream_4;
	// System.Boolean System.Net.Security.AuthenticatedStream::_LeaveStreamOpen
	bool ____LeaveStreamOpen_5;

public:
	inline static int32_t get_offset_of__InnerStream_4() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t3415418016, ____InnerStream_4)); }
	inline Stream_t1273022909 * get__InnerStream_4() const { return ____InnerStream_4; }
	inline Stream_t1273022909 ** get_address_of__InnerStream_4() { return &____InnerStream_4; }
	inline void set__InnerStream_4(Stream_t1273022909 * value)
	{
		____InnerStream_4 = value;
		Il2CppCodeGenWriteBarrier((&____InnerStream_4), value);
	}

	inline static int32_t get_offset_of__LeaveStreamOpen_5() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t3415418016, ____LeaveStreamOpen_5)); }
	inline bool get__LeaveStreamOpen_5() const { return ____LeaveStreamOpen_5; }
	inline bool* get_address_of__LeaveStreamOpen_5() { return &____LeaveStreamOpen_5; }
	inline void set__LeaveStreamOpen_5(bool value)
	{
		____LeaveStreamOpen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDSTREAM_T3415418016_H
#ifndef WEBEXCEPTIONSTATUS_T1731416715_H
#define WEBEXCEPTIONSTATUS_T1731416715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t1731416715 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t1731416715, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T1731416715_H
#ifndef PARSINGERROR_T4196835875_H
#define PARSINGERROR_T4196835875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParsingError
struct  ParsingError_t4196835875 
{
public:
	// System.Int32 System.ParsingError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingError_t4196835875, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGERROR_T4196835875_H
#ifndef ASYNCTASKMETHODBUILDER_1_T1408804594_H
#define ASYNCTASKMETHODBUILDER_1_T1408804594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Mono.Net.Security.AsyncProtocolResult>
struct  AsyncTaskMethodBuilder_1_t1408804594 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t493370259 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1408804594, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1408804594, ___m_task_2)); }
	inline Task_1_t493370259 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t493370259 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t493370259 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t1408804594_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t493370259 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1408804594_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t493370259 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t493370259 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t493370259 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T1408804594_H
#ifndef ASYNCTASKMETHODBUILDER_1_T976952967_H
#define ASYNCTASKMETHODBUILDER_1_T976952967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Int32>
struct  AsyncTaskMethodBuilder_1_t976952967 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t61518632 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t976952967, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t976952967, ___m_task_2)); }
	inline Task_1_t61518632 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t61518632 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t61518632 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t976952967_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t61518632 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t976952967_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t61518632 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t61518632 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t61518632 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T976952967_H
#ifndef ASYNCTASKMETHODBUILDER_1_T2699515049_H
#define ASYNCTASKMETHODBUILDER_1_T2699515049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Nullable`1<System.Int32>>
struct  AsyncTaskMethodBuilder_1_t2699515049 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1784080714 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2699515049, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2699515049, ___m_task_2)); }
	inline Task_1_t1784080714 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1784080714 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1784080714 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t2699515049_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1784080714 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t2699515049_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1784080714 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1784080714 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1784080714 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T2699515049_H
#ifndef ASYNCTASKMETHODBUILDER_1_T642595793_H
#define ASYNCTASKMETHODBUILDER_1_T642595793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t642595793 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2955600131  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t4022128754 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t642595793, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2955600131  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2955600131 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2955600131  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t642595793, ___m_task_2)); }
	inline Task_1_t4022128754 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t4022128754 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t4022128754 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t642595793_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t4022128754 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t642595793_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t4022128754 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t4022128754 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t4022128754 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T642595793_H
#ifndef SSLPROTOCOLS_T928472600_H
#define SSLPROTOCOLS_T928472600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t928472600 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslProtocols_t928472600, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T928472600_H
#ifndef X509KEYUSAGEFLAGS_T1431795504_H
#define X509KEYUSAGEFLAGS_T1431795504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t1431795504 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t1431795504, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T1431795504_H
#ifndef MATCH_T3408321083_H
#define MATCH_T3408321083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t3408321083  : public Group_t2468205786
{
public:
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::_groupcoll
	GroupCollection_t69770484 * ____groupcoll_9;
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::_regex
	Regex_t3657309853 * ____regex_10;
	// System.Int32 System.Text.RegularExpressions.Match::_textbeg
	int32_t ____textbeg_11;
	// System.Int32 System.Text.RegularExpressions.Match::_textpos
	int32_t ____textpos_12;
	// System.Int32 System.Text.RegularExpressions.Match::_textend
	int32_t ____textend_13;
	// System.Int32 System.Text.RegularExpressions.Match::_textstart
	int32_t ____textstart_14;
	// System.Int32[][] System.Text.RegularExpressions.Match::_matches
	Int32U5BU5DU5BU5D_t3365920845* ____matches_15;
	// System.Int32[] System.Text.RegularExpressions.Match::_matchcount
	Int32U5BU5D_t385246372* ____matchcount_16;
	// System.Boolean System.Text.RegularExpressions.Match::_balancing
	bool ____balancing_17;

public:
	inline static int32_t get_offset_of__groupcoll_9() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____groupcoll_9)); }
	inline GroupCollection_t69770484 * get__groupcoll_9() const { return ____groupcoll_9; }
	inline GroupCollection_t69770484 ** get_address_of__groupcoll_9() { return &____groupcoll_9; }
	inline void set__groupcoll_9(GroupCollection_t69770484 * value)
	{
		____groupcoll_9 = value;
		Il2CppCodeGenWriteBarrier((&____groupcoll_9), value);
	}

	inline static int32_t get_offset_of__regex_10() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____regex_10)); }
	inline Regex_t3657309853 * get__regex_10() const { return ____regex_10; }
	inline Regex_t3657309853 ** get_address_of__regex_10() { return &____regex_10; }
	inline void set__regex_10(Regex_t3657309853 * value)
	{
		____regex_10 = value;
		Il2CppCodeGenWriteBarrier((&____regex_10), value);
	}

	inline static int32_t get_offset_of__textbeg_11() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textbeg_11)); }
	inline int32_t get__textbeg_11() const { return ____textbeg_11; }
	inline int32_t* get_address_of__textbeg_11() { return &____textbeg_11; }
	inline void set__textbeg_11(int32_t value)
	{
		____textbeg_11 = value;
	}

	inline static int32_t get_offset_of__textpos_12() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textpos_12)); }
	inline int32_t get__textpos_12() const { return ____textpos_12; }
	inline int32_t* get_address_of__textpos_12() { return &____textpos_12; }
	inline void set__textpos_12(int32_t value)
	{
		____textpos_12 = value;
	}

	inline static int32_t get_offset_of__textend_13() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textend_13)); }
	inline int32_t get__textend_13() const { return ____textend_13; }
	inline int32_t* get_address_of__textend_13() { return &____textend_13; }
	inline void set__textend_13(int32_t value)
	{
		____textend_13 = value;
	}

	inline static int32_t get_offset_of__textstart_14() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____textstart_14)); }
	inline int32_t get__textstart_14() const { return ____textstart_14; }
	inline int32_t* get_address_of__textstart_14() { return &____textstart_14; }
	inline void set__textstart_14(int32_t value)
	{
		____textstart_14 = value;
	}

	inline static int32_t get_offset_of__matches_15() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____matches_15)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get__matches_15() const { return ____matches_15; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of__matches_15() { return &____matches_15; }
	inline void set__matches_15(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		____matches_15 = value;
		Il2CppCodeGenWriteBarrier((&____matches_15), value);
	}

	inline static int32_t get_offset_of__matchcount_16() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____matchcount_16)); }
	inline Int32U5BU5D_t385246372* get__matchcount_16() const { return ____matchcount_16; }
	inline Int32U5BU5D_t385246372** get_address_of__matchcount_16() { return &____matchcount_16; }
	inline void set__matchcount_16(Int32U5BU5D_t385246372* value)
	{
		____matchcount_16 = value;
		Il2CppCodeGenWriteBarrier((&____matchcount_16), value);
	}

	inline static int32_t get_offset_of__balancing_17() { return static_cast<int32_t>(offsetof(Match_t3408321083, ____balancing_17)); }
	inline bool get__balancing_17() const { return ____balancing_17; }
	inline bool* get_address_of__balancing_17() { return &____balancing_17; }
	inline void set__balancing_17(bool value)
	{
		____balancing_17 = value;
	}
};

struct Match_t3408321083_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::_empty
	Match_t3408321083 * ____empty_8;

public:
	inline static int32_t get_offset_of__empty_8() { return static_cast<int32_t>(offsetof(Match_t3408321083_StaticFields, ____empty_8)); }
	inline Match_t3408321083 * get__empty_8() const { return ____empty_8; }
	inline Match_t3408321083 ** get_address_of__empty_8() { return &____empty_8; }
	inline void set__empty_8(Match_t3408321083 * value)
	{
		____empty_8 = value;
		Il2CppCodeGenWriteBarrier((&____empty_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T3408321083_H
#ifndef REGEXOPTIONS_T92845595_H
#define REGEXOPTIONS_T92845595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t92845595 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RegexOptions_t92845595, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T92845595_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef TIMEOUTEXCEPTION_T407538241_H
#define TIMEOUTEXCEPTION_T407538241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeoutException
struct  TimeoutException_t407538241  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEOUTEXCEPTION_T407538241_H
#ifndef UNESCAPEMODE_T3819662027_H
#define UNESCAPEMODE_T3819662027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnescapeMode
struct  UnescapeMode_t3819662027 
{
public:
	// System.Int32 System.UnescapeMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnescapeMode_t3819662027, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNESCAPEMODE_T3819662027_H
#ifndef CHECK_T1263065566_H
#define CHECK_T1263065566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Check
struct  Check_t1263065566 
{
public:
	// System.Int32 System.Uri/Check::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Check_t1263065566, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T1263065566_H
#ifndef FLAGS_T2372798318_H
#define FLAGS_T2372798318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Flags
struct  Flags_t2372798318 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t2372798318, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T2372798318_H
#ifndef URIINFO_T1092684687_H
#define URIINFO_T1092684687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/UriInfo
struct  UriInfo_t1092684687  : public RuntimeObject
{
public:
	// System.String System.Uri/UriInfo::Host
	String_t* ___Host_0;
	// System.String System.Uri/UriInfo::ScopeId
	String_t* ___ScopeId_1;
	// System.String System.Uri/UriInfo::String
	String_t* ___String_2;
	// System.Uri/Offset System.Uri/UriInfo::Offset
	Offset_t4247613535  ___Offset_3;
	// System.String System.Uri/UriInfo::DnsSafeHost
	String_t* ___DnsSafeHost_4;
	// System.Uri/MoreInfo System.Uri/UriInfo::MoreInfo
	MoreInfo_t2349391856 * ___MoreInfo_5;

public:
	inline static int32_t get_offset_of_Host_0() { return static_cast<int32_t>(offsetof(UriInfo_t1092684687, ___Host_0)); }
	inline String_t* get_Host_0() const { return ___Host_0; }
	inline String_t** get_address_of_Host_0() { return &___Host_0; }
	inline void set_Host_0(String_t* value)
	{
		___Host_0 = value;
		Il2CppCodeGenWriteBarrier((&___Host_0), value);
	}

	inline static int32_t get_offset_of_ScopeId_1() { return static_cast<int32_t>(offsetof(UriInfo_t1092684687, ___ScopeId_1)); }
	inline String_t* get_ScopeId_1() const { return ___ScopeId_1; }
	inline String_t** get_address_of_ScopeId_1() { return &___ScopeId_1; }
	inline void set_ScopeId_1(String_t* value)
	{
		___ScopeId_1 = value;
		Il2CppCodeGenWriteBarrier((&___ScopeId_1), value);
	}

	inline static int32_t get_offset_of_String_2() { return static_cast<int32_t>(offsetof(UriInfo_t1092684687, ___String_2)); }
	inline String_t* get_String_2() const { return ___String_2; }
	inline String_t** get_address_of_String_2() { return &___String_2; }
	inline void set_String_2(String_t* value)
	{
		___String_2 = value;
		Il2CppCodeGenWriteBarrier((&___String_2), value);
	}

	inline static int32_t get_offset_of_Offset_3() { return static_cast<int32_t>(offsetof(UriInfo_t1092684687, ___Offset_3)); }
	inline Offset_t4247613535  get_Offset_3() const { return ___Offset_3; }
	inline Offset_t4247613535 * get_address_of_Offset_3() { return &___Offset_3; }
	inline void set_Offset_3(Offset_t4247613535  value)
	{
		___Offset_3 = value;
	}

	inline static int32_t get_offset_of_DnsSafeHost_4() { return static_cast<int32_t>(offsetof(UriInfo_t1092684687, ___DnsSafeHost_4)); }
	inline String_t* get_DnsSafeHost_4() const { return ___DnsSafeHost_4; }
	inline String_t** get_address_of_DnsSafeHost_4() { return &___DnsSafeHost_4; }
	inline void set_DnsSafeHost_4(String_t* value)
	{
		___DnsSafeHost_4 = value;
		Il2CppCodeGenWriteBarrier((&___DnsSafeHost_4), value);
	}

	inline static int32_t get_offset_of_MoreInfo_5() { return static_cast<int32_t>(offsetof(UriInfo_t1092684687, ___MoreInfo_5)); }
	inline MoreInfo_t2349391856 * get_MoreInfo_5() const { return ___MoreInfo_5; }
	inline MoreInfo_t2349391856 ** get_address_of_MoreInfo_5() { return &___MoreInfo_5; }
	inline void set_MoreInfo_5(MoreInfo_t2349391856 * value)
	{
		___MoreInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___MoreInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIINFO_T1092684687_H
#ifndef URICOMPONENTS_T814099658_H
#define URICOMPONENTS_T814099658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriComponents
struct  UriComponents_t814099658 
{
public:
	// System.Int32 System.UriComponents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriComponents_t814099658, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URICOMPONENTS_T814099658_H
#ifndef URIFORMAT_T2031163398_H
#define URIFORMAT_T2031163398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormat
struct  UriFormat_t2031163398 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t2031163398, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMAT_T2031163398_H
#ifndef URIHOSTNAMETYPE_T881866241_H
#define URIHOSTNAMETYPE_T881866241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHostNameType
struct  UriHostNameType_t881866241 
{
public:
	// System.Int32 System.UriHostNameType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriHostNameType_t881866241, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHOSTNAMETYPE_T881866241_H
#ifndef URIIDNSCOPE_T1847433844_H
#define URIIDNSCOPE_T1847433844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriIdnScope
struct  UriIdnScope_t1847433844 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_t1847433844, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIIDNSCOPE_T1847433844_H
#ifndef URIKIND_T3816567336_H
#define URIKIND_T3816567336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriKind
struct  UriKind_t3816567336 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_t3816567336, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIKIND_T3816567336_H
#ifndef URIQUIRKSVERSION_T2829553638_H
#define URIQUIRKSVERSION_T2829553638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser/UriQuirksVersion
struct  UriQuirksVersion_t2829553638 
{
public:
	// System.Int32 System.UriParser/UriQuirksVersion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriQuirksVersion_t2829553638, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIQUIRKSVERSION_T2829553638_H
#ifndef URISYNTAXFLAGS_T3715012245_H
#define URISYNTAXFLAGS_T3715012245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriSyntaxFlags
struct  UriSyntaxFlags_t3715012245 
{
public:
	// System.Int32 System.UriSyntaxFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriSyntaxFlags_t3715012245, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URISYNTAXFLAGS_T3715012245_H
#ifndef U3CINNERREADU3ED__25_T257820306_H
#define U3CINNERREADU3ED__25_T257820306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25
struct  U3CInnerReadU3Ed__25_t257820306 
{
public:
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Nullable`1<System.Int32>> Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::<>t__builder
	AsyncTaskMethodBuilder_1_t2699515049  ___U3CU3Et__builder_1;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::<>4__this
	AsyncProtocolRequest_t4184368197 * ___U3CU3E4__this_2;
	// System.Threading.CancellationToken Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_3;
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::<requestedSize>5__1
	int32_t ___U3CrequestedSizeU3E5__1_4;
	// System.Nullable`1<System.Int32> Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::<totalRead>5__2
	Nullable_1_t378540539  ___U3CtotalReadU3E5__2_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32> Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::<>u__1
	ConfiguredTaskAwaiter_t4273446738  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t2699515049  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t2699515049 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t2699515049  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___U3CU3E4__this_2)); }
	inline AsyncProtocolRequest_t4184368197 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncProtocolRequest_t4184368197 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_cancellationToken_3() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___cancellationToken_3)); }
	inline CancellationToken_t784455623  get_cancellationToken_3() const { return ___cancellationToken_3; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_3() { return &___cancellationToken_3; }
	inline void set_cancellationToken_3(CancellationToken_t784455623  value)
	{
		___cancellationToken_3 = value;
	}

	inline static int32_t get_offset_of_U3CrequestedSizeU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___U3CrequestedSizeU3E5__1_4)); }
	inline int32_t get_U3CrequestedSizeU3E5__1_4() const { return ___U3CrequestedSizeU3E5__1_4; }
	inline int32_t* get_address_of_U3CrequestedSizeU3E5__1_4() { return &___U3CrequestedSizeU3E5__1_4; }
	inline void set_U3CrequestedSizeU3E5__1_4(int32_t value)
	{
		___U3CrequestedSizeU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CtotalReadU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___U3CtotalReadU3E5__2_5)); }
	inline Nullable_1_t378540539  get_U3CtotalReadU3E5__2_5() const { return ___U3CtotalReadU3E5__2_5; }
	inline Nullable_1_t378540539 * get_address_of_U3CtotalReadU3E5__2_5() { return &___U3CtotalReadU3E5__2_5; }
	inline void set_U3CtotalReadU3E5__2_5(Nullable_1_t378540539  value)
	{
		___U3CtotalReadU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__25_t257820306, ___U3CU3Eu__1_6)); }
	inline ConfiguredTaskAwaiter_t4273446738  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline ConfiguredTaskAwaiter_t4273446738 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(ConfiguredTaskAwaiter_t4273446738  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINNERREADU3ED__25_T257820306_H
#ifndef U3CSTARTOPERATIONU3ED__23_T3218468820_H
#define U3CSTARTOPERATIONU3ED__23_T3218468820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23
struct  U3CStartOperationU3Ed__23_t3218468820 
{
public:
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<Mono.Net.Security.AsyncProtocolResult> Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::<>t__builder
	AsyncTaskMethodBuilder_1_t1408804594  ___U3CU3Et__builder_1;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::<>4__this
	AsyncProtocolRequest_t4184368197 * ___U3CU3E4__this_2;
	// System.Threading.CancellationToken Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_3;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::<>u__1
	ConfiguredTaskAwaiter_t555647845  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__23_t3218468820, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__23_t3218468820, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t1408804594  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t1408804594 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t1408804594  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__23_t3218468820, ___U3CU3E4__this_2)); }
	inline AsyncProtocolRequest_t4184368197 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncProtocolRequest_t4184368197 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_cancellationToken_3() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__23_t3218468820, ___cancellationToken_3)); }
	inline CancellationToken_t784455623  get_cancellationToken_3() const { return ___cancellationToken_3; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_3() { return &___cancellationToken_3; }
	inline void set_cancellationToken_3(CancellationToken_t784455623  value)
	{
		___cancellationToken_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__23_t3218468820, ___U3CU3Eu__1_4)); }
	inline ConfiguredTaskAwaiter_t555647845  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline ConfiguredTaskAwaiter_t555647845 * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(ConfiguredTaskAwaiter_t555647845  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOPERATIONU3ED__23_T3218468820_H
#ifndef MOBILEAUTHENTICATEDSTREAM_T3383979266_H
#define MOBILEAUTHENTICATEDSTREAM_T3383979266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream
struct  MobileAuthenticatedStream_t3383979266  : public AuthenticatedStream_t3415418016
{
public:
	// Mono.Net.Security.MobileTlsContext Mono.Net.Security.MobileAuthenticatedStream::xobileTlsContext
	MobileTlsContext_t1069274405 * ___xobileTlsContext_6;
	// System.Runtime.ExceptionServices.ExceptionDispatchInfo Mono.Net.Security.MobileAuthenticatedStream::lastException
	ExceptionDispatchInfo_t3750997369 * ___lastException_7;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.MobileAuthenticatedStream::asyncHandshakeRequest
	AsyncProtocolRequest_t4184368197 * ___asyncHandshakeRequest_8;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.MobileAuthenticatedStream::asyncReadRequest
	AsyncProtocolRequest_t4184368197 * ___asyncReadRequest_9;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.MobileAuthenticatedStream::asyncWriteRequest
	AsyncProtocolRequest_t4184368197 * ___asyncWriteRequest_10;
	// Mono.Net.Security.BufferOffsetSize2 Mono.Net.Security.MobileAuthenticatedStream::readBuffer
	BufferOffsetSize2_t2574020620 * ___readBuffer_11;
	// Mono.Net.Security.BufferOffsetSize2 Mono.Net.Security.MobileAuthenticatedStream::writeBuffer
	BufferOffsetSize2_t2574020620 * ___writeBuffer_12;
	// System.Object Mono.Net.Security.MobileAuthenticatedStream::ioLock
	RuntimeObject * ___ioLock_13;
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream::closeRequested
	int32_t ___closeRequested_14;
	// System.Boolean Mono.Net.Security.MobileAuthenticatedStream::shutdown
	bool ___shutdown_15;
	// System.Net.Security.SslStream Mono.Net.Security.MobileAuthenticatedStream::<SslStream>k__BackingField
	SslStream_t2700741536 * ___U3CSslStreamU3Ek__BackingField_17;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MobileAuthenticatedStream::<Settings>k__BackingField
	MonoTlsSettings_t3666008581 * ___U3CSettingsU3Ek__BackingField_18;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.MobileAuthenticatedStream::<Provider>k__BackingField
	MonoTlsProvider_t3152003291 * ___U3CProviderU3Ek__BackingField_19;
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream::ID
	int32_t ___ID_21;

public:
	inline static int32_t get_offset_of_xobileTlsContext_6() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___xobileTlsContext_6)); }
	inline MobileTlsContext_t1069274405 * get_xobileTlsContext_6() const { return ___xobileTlsContext_6; }
	inline MobileTlsContext_t1069274405 ** get_address_of_xobileTlsContext_6() { return &___xobileTlsContext_6; }
	inline void set_xobileTlsContext_6(MobileTlsContext_t1069274405 * value)
	{
		___xobileTlsContext_6 = value;
		Il2CppCodeGenWriteBarrier((&___xobileTlsContext_6), value);
	}

	inline static int32_t get_offset_of_lastException_7() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___lastException_7)); }
	inline ExceptionDispatchInfo_t3750997369 * get_lastException_7() const { return ___lastException_7; }
	inline ExceptionDispatchInfo_t3750997369 ** get_address_of_lastException_7() { return &___lastException_7; }
	inline void set_lastException_7(ExceptionDispatchInfo_t3750997369 * value)
	{
		___lastException_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_7), value);
	}

	inline static int32_t get_offset_of_asyncHandshakeRequest_8() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___asyncHandshakeRequest_8)); }
	inline AsyncProtocolRequest_t4184368197 * get_asyncHandshakeRequest_8() const { return ___asyncHandshakeRequest_8; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_asyncHandshakeRequest_8() { return &___asyncHandshakeRequest_8; }
	inline void set_asyncHandshakeRequest_8(AsyncProtocolRequest_t4184368197 * value)
	{
		___asyncHandshakeRequest_8 = value;
		Il2CppCodeGenWriteBarrier((&___asyncHandshakeRequest_8), value);
	}

	inline static int32_t get_offset_of_asyncReadRequest_9() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___asyncReadRequest_9)); }
	inline AsyncProtocolRequest_t4184368197 * get_asyncReadRequest_9() const { return ___asyncReadRequest_9; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_asyncReadRequest_9() { return &___asyncReadRequest_9; }
	inline void set_asyncReadRequest_9(AsyncProtocolRequest_t4184368197 * value)
	{
		___asyncReadRequest_9 = value;
		Il2CppCodeGenWriteBarrier((&___asyncReadRequest_9), value);
	}

	inline static int32_t get_offset_of_asyncWriteRequest_10() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___asyncWriteRequest_10)); }
	inline AsyncProtocolRequest_t4184368197 * get_asyncWriteRequest_10() const { return ___asyncWriteRequest_10; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_asyncWriteRequest_10() { return &___asyncWriteRequest_10; }
	inline void set_asyncWriteRequest_10(AsyncProtocolRequest_t4184368197 * value)
	{
		___asyncWriteRequest_10 = value;
		Il2CppCodeGenWriteBarrier((&___asyncWriteRequest_10), value);
	}

	inline static int32_t get_offset_of_readBuffer_11() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___readBuffer_11)); }
	inline BufferOffsetSize2_t2574020620 * get_readBuffer_11() const { return ___readBuffer_11; }
	inline BufferOffsetSize2_t2574020620 ** get_address_of_readBuffer_11() { return &___readBuffer_11; }
	inline void set_readBuffer_11(BufferOffsetSize2_t2574020620 * value)
	{
		___readBuffer_11 = value;
		Il2CppCodeGenWriteBarrier((&___readBuffer_11), value);
	}

	inline static int32_t get_offset_of_writeBuffer_12() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___writeBuffer_12)); }
	inline BufferOffsetSize2_t2574020620 * get_writeBuffer_12() const { return ___writeBuffer_12; }
	inline BufferOffsetSize2_t2574020620 ** get_address_of_writeBuffer_12() { return &___writeBuffer_12; }
	inline void set_writeBuffer_12(BufferOffsetSize2_t2574020620 * value)
	{
		___writeBuffer_12 = value;
		Il2CppCodeGenWriteBarrier((&___writeBuffer_12), value);
	}

	inline static int32_t get_offset_of_ioLock_13() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___ioLock_13)); }
	inline RuntimeObject * get_ioLock_13() const { return ___ioLock_13; }
	inline RuntimeObject ** get_address_of_ioLock_13() { return &___ioLock_13; }
	inline void set_ioLock_13(RuntimeObject * value)
	{
		___ioLock_13 = value;
		Il2CppCodeGenWriteBarrier((&___ioLock_13), value);
	}

	inline static int32_t get_offset_of_closeRequested_14() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___closeRequested_14)); }
	inline int32_t get_closeRequested_14() const { return ___closeRequested_14; }
	inline int32_t* get_address_of_closeRequested_14() { return &___closeRequested_14; }
	inline void set_closeRequested_14(int32_t value)
	{
		___closeRequested_14 = value;
	}

	inline static int32_t get_offset_of_shutdown_15() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___shutdown_15)); }
	inline bool get_shutdown_15() const { return ___shutdown_15; }
	inline bool* get_address_of_shutdown_15() { return &___shutdown_15; }
	inline void set_shutdown_15(bool value)
	{
		___shutdown_15 = value;
	}

	inline static int32_t get_offset_of_U3CSslStreamU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___U3CSslStreamU3Ek__BackingField_17)); }
	inline SslStream_t2700741536 * get_U3CSslStreamU3Ek__BackingField_17() const { return ___U3CSslStreamU3Ek__BackingField_17; }
	inline SslStream_t2700741536 ** get_address_of_U3CSslStreamU3Ek__BackingField_17() { return &___U3CSslStreamU3Ek__BackingField_17; }
	inline void set_U3CSslStreamU3Ek__BackingField_17(SslStream_t2700741536 * value)
	{
		___U3CSslStreamU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSslStreamU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CSettingsU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___U3CSettingsU3Ek__BackingField_18)); }
	inline MonoTlsSettings_t3666008581 * get_U3CSettingsU3Ek__BackingField_18() const { return ___U3CSettingsU3Ek__BackingField_18; }
	inline MonoTlsSettings_t3666008581 ** get_address_of_U3CSettingsU3Ek__BackingField_18() { return &___U3CSettingsU3Ek__BackingField_18; }
	inline void set_U3CSettingsU3Ek__BackingField_18(MonoTlsSettings_t3666008581 * value)
	{
		___U3CSettingsU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSettingsU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___U3CProviderU3Ek__BackingField_19)); }
	inline MonoTlsProvider_t3152003291 * get_U3CProviderU3Ek__BackingField_19() const { return ___U3CProviderU3Ek__BackingField_19; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_U3CProviderU3Ek__BackingField_19() { return &___U3CProviderU3Ek__BackingField_19; }
	inline void set_U3CProviderU3Ek__BackingField_19(MonoTlsProvider_t3152003291 * value)
	{
		___U3CProviderU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_ID_21() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266, ___ID_21)); }
	inline int32_t get_ID_21() const { return ___ID_21; }
	inline int32_t* get_address_of_ID_21() { return &___ID_21; }
	inline void set_ID_21(int32_t value)
	{
		___ID_21 = value;
	}
};

struct MobileAuthenticatedStream_t3383979266_StaticFields
{
public:
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream::uniqueNameInteger
	int32_t ___uniqueNameInteger_16;
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream::nextId
	int32_t ___nextId_20;

public:
	inline static int32_t get_offset_of_uniqueNameInteger_16() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266_StaticFields, ___uniqueNameInteger_16)); }
	inline int32_t get_uniqueNameInteger_16() const { return ___uniqueNameInteger_16; }
	inline int32_t* get_address_of_uniqueNameInteger_16() { return &___uniqueNameInteger_16; }
	inline void set_uniqueNameInteger_16(int32_t value)
	{
		___uniqueNameInteger_16 = value;
	}

	inline static int32_t get_offset_of_nextId_20() { return static_cast<int32_t>(offsetof(MobileAuthenticatedStream_t3383979266_StaticFields, ___nextId_20)); }
	inline int32_t get_nextId_20() const { return ___nextId_20; }
	inline int32_t* get_address_of_nextId_20() { return &___nextId_20; }
	inline void set_nextId_20(int32_t value)
	{
		___nextId_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEAUTHENTICATEDSTREAM_T3383979266_H
#ifndef U3CINNERREADU3ED__66_T2872199449_H
#define U3CINNERREADU3ED__66_T2872199449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66
struct  U3CInnerReadU3Ed__66_t2872199449 
{
public:
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Int32> Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::<>t__builder
	AsyncTaskMethodBuilder_1_t976952967  ___U3CU3Et__builder_1;
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::<>4__this
	MobileAuthenticatedStream_t3383979266 * ___U3CU3E4__this_2;
	// System.Threading.CancellationToken Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_3;
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::requestedSize
	int32_t ___requestedSize_4;
	// System.Boolean Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::sync
	bool ___sync_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Int32> Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::<>u__1
	ConfiguredTaskAwaiter_t4273446738  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t976952967  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t976952967 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t976952967  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___U3CU3E4__this_2)); }
	inline MobileAuthenticatedStream_t3383979266 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MobileAuthenticatedStream_t3383979266 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_cancellationToken_3() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___cancellationToken_3)); }
	inline CancellationToken_t784455623  get_cancellationToken_3() const { return ___cancellationToken_3; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_3() { return &___cancellationToken_3; }
	inline void set_cancellationToken_3(CancellationToken_t784455623  value)
	{
		___cancellationToken_3 = value;
	}

	inline static int32_t get_offset_of_requestedSize_4() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___requestedSize_4)); }
	inline int32_t get_requestedSize_4() const { return ___requestedSize_4; }
	inline int32_t* get_address_of_requestedSize_4() { return &___requestedSize_4; }
	inline void set_requestedSize_4(int32_t value)
	{
		___requestedSize_4 = value;
	}

	inline static int32_t get_offset_of_sync_5() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___sync_5)); }
	inline bool get_sync_5() const { return ___sync_5; }
	inline bool* get_address_of_sync_5() { return &___sync_5; }
	inline void set_sync_5(bool value)
	{
		___sync_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CInnerReadU3Ed__66_t2872199449, ___U3CU3Eu__1_6)); }
	inline ConfiguredTaskAwaiter_t4273446738  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline ConfiguredTaskAwaiter_t4273446738 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(ConfiguredTaskAwaiter_t4273446738  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINNERREADU3ED__66_T2872199449_H
#ifndef U3CSTARTOPERATIONU3ED__58_T1428438294_H
#define U3CSTARTOPERATIONU3ED__58_T1428438294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58
struct  U3CStartOperationU3Ed__58_t1428438294 
{
public:
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Int32> Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::<>t__builder
	AsyncTaskMethodBuilder_1_t976952967  ___U3CU3Et__builder_1;
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::<>4__this
	MobileAuthenticatedStream_t3383979266 * ___U3CU3E4__this_2;
	// Mono.Net.Security.MobileAuthenticatedStream/OperationType Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::type
	int32_t ___type_3;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::asyncRequest
	AsyncProtocolRequest_t4184368197 * ___asyncRequest_4;
	// System.Threading.CancellationToken Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Mono.Net.Security.AsyncProtocolResult> Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__58::<>u__1
	ConfiguredTaskAwaiter_t410331069  ___U3CU3Eu__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t976952967  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t976952967 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t976952967  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___U3CU3E4__this_2)); }
	inline MobileAuthenticatedStream_t3383979266 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MobileAuthenticatedStream_t3383979266 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_asyncRequest_4() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___asyncRequest_4)); }
	inline AsyncProtocolRequest_t4184368197 * get_asyncRequest_4() const { return ___asyncRequest_4; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_asyncRequest_4() { return &___asyncRequest_4; }
	inline void set_asyncRequest_4(AsyncProtocolRequest_t4184368197 * value)
	{
		___asyncRequest_4 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRequest_4), value);
	}

	inline static int32_t get_offset_of_cancellationToken_5() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___cancellationToken_5)); }
	inline CancellationToken_t784455623  get_cancellationToken_5() const { return ___cancellationToken_5; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_5() { return &___cancellationToken_5; }
	inline void set_cancellationToken_5(CancellationToken_t784455623  value)
	{
		___cancellationToken_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CStartOperationU3Ed__58_t1428438294, ___U3CU3Eu__1_6)); }
	inline ConfiguredTaskAwaiter_t410331069  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline ConfiguredTaskAwaiter_t410331069 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(ConfiguredTaskAwaiter_t410331069  value)
	{
		___U3CU3Eu__1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOPERATIONU3ED__58_T1428438294_H
#ifndef MOBILETLSCONTEXT_T1069274405_H
#define MOBILETLSCONTEXT_T1069274405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileTlsContext
struct  MobileTlsContext_t1069274405  : public RuntimeObject
{
public:
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileTlsContext::parent
	MobileAuthenticatedStream_t3383979266 * ___parent_0;
	// System.Boolean Mono.Net.Security.MobileTlsContext::serverMode
	bool ___serverMode_1;
	// System.String Mono.Net.Security.MobileTlsContext::targetHost
	String_t* ___targetHost_2;
	// System.String Mono.Net.Security.MobileTlsContext::serverName
	String_t* ___serverName_3;
	// System.Security.Authentication.SslProtocols Mono.Net.Security.MobileTlsContext::enabledProtocols
	int32_t ___enabledProtocols_4;
	// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MobileTlsContext::serverCertificate
	X509Certificate_t713131622 * ___serverCertificate_5;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.MobileTlsContext::clientCertificates
	X509CertificateCollection_t3399372417 * ___clientCertificates_6;
	// System.Boolean Mono.Net.Security.MobileTlsContext::askForClientCert
	bool ___askForClientCert_7;
	// Mono.Security.Interface.ICertificateValidator2 Mono.Net.Security.MobileTlsContext::certificateValidator
	RuntimeObject* ___certificateValidator_8;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___parent_0)); }
	inline MobileAuthenticatedStream_t3383979266 * get_parent_0() const { return ___parent_0; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(MobileAuthenticatedStream_t3383979266 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_serverMode_1() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___serverMode_1)); }
	inline bool get_serverMode_1() const { return ___serverMode_1; }
	inline bool* get_address_of_serverMode_1() { return &___serverMode_1; }
	inline void set_serverMode_1(bool value)
	{
		___serverMode_1 = value;
	}

	inline static int32_t get_offset_of_targetHost_2() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___targetHost_2)); }
	inline String_t* get_targetHost_2() const { return ___targetHost_2; }
	inline String_t** get_address_of_targetHost_2() { return &___targetHost_2; }
	inline void set_targetHost_2(String_t* value)
	{
		___targetHost_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetHost_2), value);
	}

	inline static int32_t get_offset_of_serverName_3() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___serverName_3)); }
	inline String_t* get_serverName_3() const { return ___serverName_3; }
	inline String_t** get_address_of_serverName_3() { return &___serverName_3; }
	inline void set_serverName_3(String_t* value)
	{
		___serverName_3 = value;
		Il2CppCodeGenWriteBarrier((&___serverName_3), value);
	}

	inline static int32_t get_offset_of_enabledProtocols_4() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___enabledProtocols_4)); }
	inline int32_t get_enabledProtocols_4() const { return ___enabledProtocols_4; }
	inline int32_t* get_address_of_enabledProtocols_4() { return &___enabledProtocols_4; }
	inline void set_enabledProtocols_4(int32_t value)
	{
		___enabledProtocols_4 = value;
	}

	inline static int32_t get_offset_of_serverCertificate_5() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___serverCertificate_5)); }
	inline X509Certificate_t713131622 * get_serverCertificate_5() const { return ___serverCertificate_5; }
	inline X509Certificate_t713131622 ** get_address_of_serverCertificate_5() { return &___serverCertificate_5; }
	inline void set_serverCertificate_5(X509Certificate_t713131622 * value)
	{
		___serverCertificate_5 = value;
		Il2CppCodeGenWriteBarrier((&___serverCertificate_5), value);
	}

	inline static int32_t get_offset_of_clientCertificates_6() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___clientCertificates_6)); }
	inline X509CertificateCollection_t3399372417 * get_clientCertificates_6() const { return ___clientCertificates_6; }
	inline X509CertificateCollection_t3399372417 ** get_address_of_clientCertificates_6() { return &___clientCertificates_6; }
	inline void set_clientCertificates_6(X509CertificateCollection_t3399372417 * value)
	{
		___clientCertificates_6 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificates_6), value);
	}

	inline static int32_t get_offset_of_askForClientCert_7() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___askForClientCert_7)); }
	inline bool get_askForClientCert_7() const { return ___askForClientCert_7; }
	inline bool* get_address_of_askForClientCert_7() { return &___askForClientCert_7; }
	inline void set_askForClientCert_7(bool value)
	{
		___askForClientCert_7 = value;
	}

	inline static int32_t get_offset_of_certificateValidator_8() { return static_cast<int32_t>(offsetof(MobileTlsContext_t1069274405, ___certificateValidator_8)); }
	inline RuntimeObject* get_certificateValidator_8() const { return ___certificateValidator_8; }
	inline RuntimeObject** get_address_of_certificateValidator_8() { return &___certificateValidator_8; }
	inline void set_certificateValidator_8(RuntimeObject* value)
	{
		___certificateValidator_8 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValidator_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILETLSCONTEXT_T1069274405_H
#ifndef MONOTLSSTREAM_T1980138907_H
#define MONOTLSSTREAM_T1980138907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsStream
struct  MonoTlsStream_t1980138907  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.MonoTlsStream::provider
	MonoTlsProvider_t3152003291 * ___provider_0;
	// System.Net.Sockets.NetworkStream Mono.Net.Security.MonoTlsStream::networkStream
	NetworkStream_t4071955934 * ___networkStream_1;
	// System.Net.HttpWebRequest Mono.Net.Security.MonoTlsStream::request
	HttpWebRequest_t1669436515 * ___request_2;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MonoTlsStream::settings
	MonoTlsSettings_t3666008581 * ___settings_3;
	// Mono.Security.Interface.IMonoSslStream Mono.Net.Security.MonoTlsStream::sslStream
	RuntimeObject* ___sslStream_4;
	// System.Net.WebExceptionStatus Mono.Net.Security.MonoTlsStream::status
	int32_t ___status_5;
	// System.Boolean Mono.Net.Security.MonoTlsStream::<CertificateValidationFailed>k__BackingField
	bool ___U3CCertificateValidationFailedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___provider_0)); }
	inline MonoTlsProvider_t3152003291 * get_provider_0() const { return ___provider_0; }
	inline MonoTlsProvider_t3152003291 ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(MonoTlsProvider_t3152003291 * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___provider_0), value);
	}

	inline static int32_t get_offset_of_networkStream_1() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___networkStream_1)); }
	inline NetworkStream_t4071955934 * get_networkStream_1() const { return ___networkStream_1; }
	inline NetworkStream_t4071955934 ** get_address_of_networkStream_1() { return &___networkStream_1; }
	inline void set_networkStream_1(NetworkStream_t4071955934 * value)
	{
		___networkStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_1), value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___request_2)); }
	inline HttpWebRequest_t1669436515 * get_request_2() const { return ___request_2; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(HttpWebRequest_t1669436515 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}

	inline static int32_t get_offset_of_settings_3() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___settings_3)); }
	inline MonoTlsSettings_t3666008581 * get_settings_3() const { return ___settings_3; }
	inline MonoTlsSettings_t3666008581 ** get_address_of_settings_3() { return &___settings_3; }
	inline void set_settings_3(MonoTlsSettings_t3666008581 * value)
	{
		___settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___settings_3), value);
	}

	inline static int32_t get_offset_of_sslStream_4() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___sslStream_4)); }
	inline RuntimeObject* get_sslStream_4() const { return ___sslStream_4; }
	inline RuntimeObject** get_address_of_sslStream_4() { return &___sslStream_4; }
	inline void set_sslStream_4(RuntimeObject* value)
	{
		___sslStream_4 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_4), value);
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___status_5)); }
	inline int32_t get_status_5() const { return ___status_5; }
	inline int32_t* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(int32_t value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1980138907, ___U3CCertificateValidationFailedU3Ek__BackingField_6)); }
	inline bool get_U3CCertificateValidationFailedU3Ek__BackingField_6() const { return ___U3CCertificateValidationFailedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCertificateValidationFailedU3Ek__BackingField_6() { return &___U3CCertificateValidationFailedU3Ek__BackingField_6; }
	inline void set_U3CCertificateValidationFailedU3Ek__BackingField_6(bool value)
	{
		___U3CCertificateValidationFailedU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSSTREAM_T1980138907_H
#ifndef SYSTEMCERTIFICATEVALIDATOR_T3152079745_H
#define SYSTEMCERTIFICATEVALIDATOR_T3152079745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.SystemCertificateValidator
struct  SystemCertificateValidator_t3152079745  : public RuntimeObject
{
public:

public:
};

struct SystemCertificateValidator_t3152079745_StaticFields
{
public:
	// System.Boolean Mono.Net.Security.SystemCertificateValidator::is_macosx
	bool ___is_macosx_0;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags Mono.Net.Security.SystemCertificateValidator::s_flags
	int32_t ___s_flags_1;

public:
	inline static int32_t get_offset_of_is_macosx_0() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t3152079745_StaticFields, ___is_macosx_0)); }
	inline bool get_is_macosx_0() const { return ___is_macosx_0; }
	inline bool* get_address_of_is_macosx_0() { return &___is_macosx_0; }
	inline void set_is_macosx_0(bool value)
	{
		___is_macosx_0 = value;
	}

	inline static int32_t get_offset_of_s_flags_1() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t3152079745_StaticFields, ___s_flags_1)); }
	inline int32_t get_s_flags_1() const { return ___s_flags_1; }
	inline int32_t* get_address_of_s_flags_1() { return &___s_flags_1; }
	inline void set_s_flags_1(int32_t value)
	{
		___s_flags_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMCERTIFICATEVALIDATOR_T3152079745_H
#ifndef COLLECTIONCONVERTER_T3078846443_H
#define COLLECTIONCONVERTER_T3078846443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CollectionConverter
struct  CollectionConverter_t3078846443  : public TypeConverter_t2249118273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONCONVERTER_T3078846443_H
#ifndef IOSELECTORJOB_T2199748873_H
#define IOSELECTORJOB_T2199748873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOSelectorJob
struct  IOSelectorJob_t2199748873  : public RuntimeObject
{
public:
	// System.IOOperation System.IOSelectorJob::operation
	int32_t ___operation_0;
	// System.IOAsyncCallback System.IOSelectorJob::callback
	IOAsyncCallback_t705871752 * ___callback_1;
	// System.IOAsyncResult System.IOSelectorJob::state
	IOAsyncResult_t3640145766 * ___state_2;

public:
	inline static int32_t get_offset_of_operation_0() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2199748873, ___operation_0)); }
	inline int32_t get_operation_0() const { return ___operation_0; }
	inline int32_t* get_address_of_operation_0() { return &___operation_0; }
	inline void set_operation_0(int32_t value)
	{
		___operation_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2199748873, ___callback_1)); }
	inline IOAsyncCallback_t705871752 * get_callback_1() const { return ___callback_1; }
	inline IOAsyncCallback_t705871752 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(IOAsyncCallback_t705871752 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2199748873, ___state_2)); }
	inline IOAsyncResult_t3640145766 * get_state_2() const { return ___state_2; }
	inline IOAsyncResult_t3640145766 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(IOAsyncResult_t3640145766 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IOSelectorJob
struct IOSelectorJob_t2199748873_marshaled_pinvoke
{
	int32_t ___operation_0;
	Il2CppMethodPointer ___callback_1;
	IOAsyncResult_t3640145766_marshaled_pinvoke* ___state_2;
};
// Native definition for COM marshalling of System.IOSelectorJob
struct IOSelectorJob_t2199748873_marshaled_com
{
	int32_t ___operation_0;
	Il2CppMethodPointer ___callback_1;
	IOAsyncResult_t3640145766_marshaled_com* ___state_2;
};
#endif // IOSELECTORJOB_T2199748873_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_T3536885450_H
#define ASYNCTASKMETHODBUILDER_T3536885450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t3536885450 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t642595793  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t3536885450, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t642595793  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t642595793 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t642595793  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t3536885450_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t4022128754 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t3536885450_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t4022128754 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t4022128754 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t4022128754 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t3536885450_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t642595793  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t3536885450_marshaled_com
{
	AsyncTaskMethodBuilder_1_t642595793  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T3536885450_H
#ifndef MATCHSPARSE_T3706801657_H
#define MATCHSPARSE_T3706801657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchSparse
struct  MatchSparse_t3706801657  : public Match_t3408321083
{
public:
	// System.Collections.Hashtable System.Text.RegularExpressions.MatchSparse::_caps
	Hashtable_t1853889766 * ____caps_18;

public:
	inline static int32_t get_offset_of__caps_18() { return static_cast<int32_t>(offsetof(MatchSparse_t3706801657, ____caps_18)); }
	inline Hashtable_t1853889766 * get__caps_18() const { return ____caps_18; }
	inline Hashtable_t1853889766 ** get_address_of__caps_18() { return &____caps_18; }
	inline void set__caps_18(Hashtable_t1853889766 * value)
	{
		____caps_18 = value;
		Il2CppCodeGenWriteBarrier((&____caps_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHSPARSE_T3706801657_H
#ifndef REGEX_T3657309853_H
#define REGEX_T3657309853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t3657309853  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_0;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.Regex::factory
	RegexRunnerFactory_t51159052 * ___factory_1;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_2;
	// System.TimeSpan System.Text.RegularExpressions.Regex::internalMatchTimeout
	TimeSpan_t881159249  ___internalMatchTimeout_5;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::caps
	Hashtable_t1853889766 * ___caps_9;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::capnames
	Hashtable_t1853889766 * ___capnames_10;
	// System.String[] System.Text.RegularExpressions.Regex::capslist
	StringU5BU5D_t1281789340* ___capslist_11;
	// System.Int32 System.Text.RegularExpressions.Regex::capsize
	int32_t ___capsize_12;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.Regex::runnerref
	ExclusiveReference_t1927754563 * ___runnerref_13;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.Regex::replref
	SharedReference_t2916547576 * ___replref_14;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.Regex::code
	RegexCode_t4293407246 * ___code_15;
	// System.Boolean System.Text.RegularExpressions.Regex::refsInitialized
	bool ___refsInitialized_16;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_factory_1() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___factory_1)); }
	inline RegexRunnerFactory_t51159052 * get_factory_1() const { return ___factory_1; }
	inline RegexRunnerFactory_t51159052 ** get_address_of_factory_1() { return &___factory_1; }
	inline void set_factory_1(RegexRunnerFactory_t51159052 * value)
	{
		___factory_1 = value;
		Il2CppCodeGenWriteBarrier((&___factory_1), value);
	}

	inline static int32_t get_offset_of_roptions_2() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___roptions_2)); }
	inline int32_t get_roptions_2() const { return ___roptions_2; }
	inline int32_t* get_address_of_roptions_2() { return &___roptions_2; }
	inline void set_roptions_2(int32_t value)
	{
		___roptions_2 = value;
	}

	inline static int32_t get_offset_of_internalMatchTimeout_5() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___internalMatchTimeout_5)); }
	inline TimeSpan_t881159249  get_internalMatchTimeout_5() const { return ___internalMatchTimeout_5; }
	inline TimeSpan_t881159249 * get_address_of_internalMatchTimeout_5() { return &___internalMatchTimeout_5; }
	inline void set_internalMatchTimeout_5(TimeSpan_t881159249  value)
	{
		___internalMatchTimeout_5 = value;
	}

	inline static int32_t get_offset_of_caps_9() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___caps_9)); }
	inline Hashtable_t1853889766 * get_caps_9() const { return ___caps_9; }
	inline Hashtable_t1853889766 ** get_address_of_caps_9() { return &___caps_9; }
	inline void set_caps_9(Hashtable_t1853889766 * value)
	{
		___caps_9 = value;
		Il2CppCodeGenWriteBarrier((&___caps_9), value);
	}

	inline static int32_t get_offset_of_capnames_10() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___capnames_10)); }
	inline Hashtable_t1853889766 * get_capnames_10() const { return ___capnames_10; }
	inline Hashtable_t1853889766 ** get_address_of_capnames_10() { return &___capnames_10; }
	inline void set_capnames_10(Hashtable_t1853889766 * value)
	{
		___capnames_10 = value;
		Il2CppCodeGenWriteBarrier((&___capnames_10), value);
	}

	inline static int32_t get_offset_of_capslist_11() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___capslist_11)); }
	inline StringU5BU5D_t1281789340* get_capslist_11() const { return ___capslist_11; }
	inline StringU5BU5D_t1281789340** get_address_of_capslist_11() { return &___capslist_11; }
	inline void set_capslist_11(StringU5BU5D_t1281789340* value)
	{
		___capslist_11 = value;
		Il2CppCodeGenWriteBarrier((&___capslist_11), value);
	}

	inline static int32_t get_offset_of_capsize_12() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___capsize_12)); }
	inline int32_t get_capsize_12() const { return ___capsize_12; }
	inline int32_t* get_address_of_capsize_12() { return &___capsize_12; }
	inline void set_capsize_12(int32_t value)
	{
		___capsize_12 = value;
	}

	inline static int32_t get_offset_of_runnerref_13() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___runnerref_13)); }
	inline ExclusiveReference_t1927754563 * get_runnerref_13() const { return ___runnerref_13; }
	inline ExclusiveReference_t1927754563 ** get_address_of_runnerref_13() { return &___runnerref_13; }
	inline void set_runnerref_13(ExclusiveReference_t1927754563 * value)
	{
		___runnerref_13 = value;
		Il2CppCodeGenWriteBarrier((&___runnerref_13), value);
	}

	inline static int32_t get_offset_of_replref_14() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___replref_14)); }
	inline SharedReference_t2916547576 * get_replref_14() const { return ___replref_14; }
	inline SharedReference_t2916547576 ** get_address_of_replref_14() { return &___replref_14; }
	inline void set_replref_14(SharedReference_t2916547576 * value)
	{
		___replref_14 = value;
		Il2CppCodeGenWriteBarrier((&___replref_14), value);
	}

	inline static int32_t get_offset_of_code_15() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___code_15)); }
	inline RegexCode_t4293407246 * get_code_15() const { return ___code_15; }
	inline RegexCode_t4293407246 ** get_address_of_code_15() { return &___code_15; }
	inline void set_code_15(RegexCode_t4293407246 * value)
	{
		___code_15 = value;
		Il2CppCodeGenWriteBarrier((&___code_15), value);
	}

	inline static int32_t get_offset_of_refsInitialized_16() { return static_cast<int32_t>(offsetof(Regex_t3657309853, ___refsInitialized_16)); }
	inline bool get_refsInitialized_16() const { return ___refsInitialized_16; }
	inline bool* get_address_of_refsInitialized_16() { return &___refsInitialized_16; }
	inline void set_refsInitialized_16(bool value)
	{
		___refsInitialized_16 = value;
	}
};

struct Regex_t3657309853_StaticFields
{
public:
	// System.TimeSpan System.Text.RegularExpressions.Regex::MaximumMatchTimeout
	TimeSpan_t881159249  ___MaximumMatchTimeout_3;
	// System.TimeSpan System.Text.RegularExpressions.Regex::InfiniteMatchTimeout
	TimeSpan_t881159249  ___InfiniteMatchTimeout_4;
	// System.TimeSpan System.Text.RegularExpressions.Regex::FallbackDefaultMatchTimeout
	TimeSpan_t881159249  ___FallbackDefaultMatchTimeout_7;
	// System.TimeSpan System.Text.RegularExpressions.Regex::DefaultMatchTimeout
	TimeSpan_t881159249  ___DefaultMatchTimeout_8;
	// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry> System.Text.RegularExpressions.Regex::livecode
	LinkedList_1_t3068621835 * ___livecode_17;
	// System.Int32 System.Text.RegularExpressions.Regex::cacheSize
	int32_t ___cacheSize_18;

public:
	inline static int32_t get_offset_of_MaximumMatchTimeout_3() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___MaximumMatchTimeout_3)); }
	inline TimeSpan_t881159249  get_MaximumMatchTimeout_3() const { return ___MaximumMatchTimeout_3; }
	inline TimeSpan_t881159249 * get_address_of_MaximumMatchTimeout_3() { return &___MaximumMatchTimeout_3; }
	inline void set_MaximumMatchTimeout_3(TimeSpan_t881159249  value)
	{
		___MaximumMatchTimeout_3 = value;
	}

	inline static int32_t get_offset_of_InfiniteMatchTimeout_4() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___InfiniteMatchTimeout_4)); }
	inline TimeSpan_t881159249  get_InfiniteMatchTimeout_4() const { return ___InfiniteMatchTimeout_4; }
	inline TimeSpan_t881159249 * get_address_of_InfiniteMatchTimeout_4() { return &___InfiniteMatchTimeout_4; }
	inline void set_InfiniteMatchTimeout_4(TimeSpan_t881159249  value)
	{
		___InfiniteMatchTimeout_4 = value;
	}

	inline static int32_t get_offset_of_FallbackDefaultMatchTimeout_7() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___FallbackDefaultMatchTimeout_7)); }
	inline TimeSpan_t881159249  get_FallbackDefaultMatchTimeout_7() const { return ___FallbackDefaultMatchTimeout_7; }
	inline TimeSpan_t881159249 * get_address_of_FallbackDefaultMatchTimeout_7() { return &___FallbackDefaultMatchTimeout_7; }
	inline void set_FallbackDefaultMatchTimeout_7(TimeSpan_t881159249  value)
	{
		___FallbackDefaultMatchTimeout_7 = value;
	}

	inline static int32_t get_offset_of_DefaultMatchTimeout_8() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___DefaultMatchTimeout_8)); }
	inline TimeSpan_t881159249  get_DefaultMatchTimeout_8() const { return ___DefaultMatchTimeout_8; }
	inline TimeSpan_t881159249 * get_address_of_DefaultMatchTimeout_8() { return &___DefaultMatchTimeout_8; }
	inline void set_DefaultMatchTimeout_8(TimeSpan_t881159249  value)
	{
		___DefaultMatchTimeout_8 = value;
	}

	inline static int32_t get_offset_of_livecode_17() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___livecode_17)); }
	inline LinkedList_1_t3068621835 * get_livecode_17() const { return ___livecode_17; }
	inline LinkedList_1_t3068621835 ** get_address_of_livecode_17() { return &___livecode_17; }
	inline void set_livecode_17(LinkedList_1_t3068621835 * value)
	{
		___livecode_17 = value;
		Il2CppCodeGenWriteBarrier((&___livecode_17), value);
	}

	inline static int32_t get_offset_of_cacheSize_18() { return static_cast<int32_t>(offsetof(Regex_t3657309853_StaticFields, ___cacheSize_18)); }
	inline int32_t get_cacheSize_18() const { return ___cacheSize_18; }
	inline int32_t* get_address_of_cacheSize_18() { return &___cacheSize_18; }
	inline void set_cacheSize_18(int32_t value)
	{
		___cacheSize_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T3657309853_H
#ifndef REGEXMATCHTIMEOUTEXCEPTION_T1986163303_H
#define REGEXMATCHTIMEOUTEXCEPTION_T1986163303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexMatchTimeoutException
struct  RegexMatchTimeoutException_t1986163303  : public TimeoutException_t407538241
{
public:
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexInput
	String_t* ___regexInput_17;
	// System.String System.Text.RegularExpressions.RegexMatchTimeoutException::regexPattern
	String_t* ___regexPattern_18;
	// System.TimeSpan System.Text.RegularExpressions.RegexMatchTimeoutException::matchTimeout
	TimeSpan_t881159249  ___matchTimeout_19;

public:
	inline static int32_t get_offset_of_regexInput_17() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t1986163303, ___regexInput_17)); }
	inline String_t* get_regexInput_17() const { return ___regexInput_17; }
	inline String_t** get_address_of_regexInput_17() { return &___regexInput_17; }
	inline void set_regexInput_17(String_t* value)
	{
		___regexInput_17 = value;
		Il2CppCodeGenWriteBarrier((&___regexInput_17), value);
	}

	inline static int32_t get_offset_of_regexPattern_18() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t1986163303, ___regexPattern_18)); }
	inline String_t* get_regexPattern_18() const { return ___regexPattern_18; }
	inline String_t** get_address_of_regexPattern_18() { return &___regexPattern_18; }
	inline void set_regexPattern_18(String_t* value)
	{
		___regexPattern_18 = value;
		Il2CppCodeGenWriteBarrier((&___regexPattern_18), value);
	}

	inline static int32_t get_offset_of_matchTimeout_19() { return static_cast<int32_t>(offsetof(RegexMatchTimeoutException_t1986163303, ___matchTimeout_19)); }
	inline TimeSpan_t881159249  get_matchTimeout_19() const { return ___matchTimeout_19; }
	inline TimeSpan_t881159249 * get_address_of_matchTimeout_19() { return &___matchTimeout_19; }
	inline void set_matchTimeout_19(TimeSpan_t881159249  value)
	{
		___matchTimeout_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXMATCHTIMEOUTEXCEPTION_T1986163303_H
#ifndef REGEXNODE_T4293407243_H
#define REGEXNODE_T4293407243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexNode
struct  RegexNode_t4293407243  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexNode::_type
	int32_t ____type_0;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexNode> System.Text.RegularExpressions.RegexNode::_children
	List_1_t1470514689 * ____children_1;
	// System.String System.Text.RegularExpressions.RegexNode::_str
	String_t* ____str_2;
	// System.Char System.Text.RegularExpressions.RegexNode::_ch
	Il2CppChar ____ch_3;
	// System.Int32 System.Text.RegularExpressions.RegexNode::_m
	int32_t ____m_4;
	// System.Int32 System.Text.RegularExpressions.RegexNode::_n
	int32_t ____n_5;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexNode::_options
	int32_t ____options_6;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::_next
	RegexNode_t4293407243 * ____next_7;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__children_1() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____children_1)); }
	inline List_1_t1470514689 * get__children_1() const { return ____children_1; }
	inline List_1_t1470514689 ** get_address_of__children_1() { return &____children_1; }
	inline void set__children_1(List_1_t1470514689 * value)
	{
		____children_1 = value;
		Il2CppCodeGenWriteBarrier((&____children_1), value);
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__ch_3() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____ch_3)); }
	inline Il2CppChar get__ch_3() const { return ____ch_3; }
	inline Il2CppChar* get_address_of__ch_3() { return &____ch_3; }
	inline void set__ch_3(Il2CppChar value)
	{
		____ch_3 = value;
	}

	inline static int32_t get_offset_of__m_4() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____m_4)); }
	inline int32_t get__m_4() const { return ____m_4; }
	inline int32_t* get_address_of__m_4() { return &____m_4; }
	inline void set__m_4(int32_t value)
	{
		____m_4 = value;
	}

	inline static int32_t get_offset_of__n_5() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____n_5)); }
	inline int32_t get__n_5() const { return ____n_5; }
	inline int32_t* get_address_of__n_5() { return &____n_5; }
	inline void set__n_5(int32_t value)
	{
		____n_5 = value;
	}

	inline static int32_t get_offset_of__options_6() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____options_6)); }
	inline int32_t get__options_6() const { return ____options_6; }
	inline int32_t* get_address_of__options_6() { return &____options_6; }
	inline void set__options_6(int32_t value)
	{
		____options_6 = value;
	}

	inline static int32_t get_offset_of__next_7() { return static_cast<int32_t>(offsetof(RegexNode_t4293407243, ____next_7)); }
	inline RegexNode_t4293407243 * get__next_7() const { return ____next_7; }
	inline RegexNode_t4293407243 ** get_address_of__next_7() { return &____next_7; }
	inline void set__next_7(RegexNode_t4293407243 * value)
	{
		____next_7 = value;
		Il2CppCodeGenWriteBarrier((&____next_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXNODE_T4293407243_H
#ifndef REGEXPARSER_T276209658_H
#define REGEXPARSER_T276209658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexParser
struct  RegexParser_t276209658  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_stack
	RegexNode_t4293407243 * ____stack_0;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_group
	RegexNode_t4293407243 * ____group_1;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_alternation
	RegexNode_t4293407243 * ____alternation_2;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_concatenation
	RegexNode_t4293407243 * ____concatenation_3;
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::_unit
	RegexNode_t4293407243 * ____unit_4;
	// System.String System.Text.RegularExpressions.RegexParser::_pattern
	String_t* ____pattern_5;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_currentPos
	int32_t ____currentPos_6;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexParser::_culture
	CultureInfo_t4157843068 * ____culture_7;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_autocap
	int32_t ____autocap_8;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capcount
	int32_t ____capcount_9;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_captop
	int32_t ____captop_10;
	// System.Int32 System.Text.RegularExpressions.RegexParser::_capsize
	int32_t ____capsize_11;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_caps
	Hashtable_t1853889766 * ____caps_12;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexParser::_capnames
	Hashtable_t1853889766 * ____capnames_13;
	// System.Int32[] System.Text.RegularExpressions.RegexParser::_capnumlist
	Int32U5BU5D_t385246372* ____capnumlist_14;
	// System.Collections.Generic.List`1<System.String> System.Text.RegularExpressions.RegexParser::_capnamelist
	List_1_t3319525431 * ____capnamelist_15;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexParser::_options
	int32_t ____options_16;
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexOptions> System.Text.RegularExpressions.RegexParser::_optionsStack
	List_1_t1564920337 * ____optionsStack_17;
	// System.Boolean System.Text.RegularExpressions.RegexParser::_ignoreNextParen
	bool ____ignoreNextParen_18;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____stack_0)); }
	inline RegexNode_t4293407243 * get__stack_0() const { return ____stack_0; }
	inline RegexNode_t4293407243 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(RegexNode_t4293407243 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((&____stack_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____group_1)); }
	inline RegexNode_t4293407243 * get__group_1() const { return ____group_1; }
	inline RegexNode_t4293407243 ** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RegexNode_t4293407243 * value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}

	inline static int32_t get_offset_of__alternation_2() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____alternation_2)); }
	inline RegexNode_t4293407243 * get__alternation_2() const { return ____alternation_2; }
	inline RegexNode_t4293407243 ** get_address_of__alternation_2() { return &____alternation_2; }
	inline void set__alternation_2(RegexNode_t4293407243 * value)
	{
		____alternation_2 = value;
		Il2CppCodeGenWriteBarrier((&____alternation_2), value);
	}

	inline static int32_t get_offset_of__concatenation_3() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____concatenation_3)); }
	inline RegexNode_t4293407243 * get__concatenation_3() const { return ____concatenation_3; }
	inline RegexNode_t4293407243 ** get_address_of__concatenation_3() { return &____concatenation_3; }
	inline void set__concatenation_3(RegexNode_t4293407243 * value)
	{
		____concatenation_3 = value;
		Il2CppCodeGenWriteBarrier((&____concatenation_3), value);
	}

	inline static int32_t get_offset_of__unit_4() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____unit_4)); }
	inline RegexNode_t4293407243 * get__unit_4() const { return ____unit_4; }
	inline RegexNode_t4293407243 ** get_address_of__unit_4() { return &____unit_4; }
	inline void set__unit_4(RegexNode_t4293407243 * value)
	{
		____unit_4 = value;
		Il2CppCodeGenWriteBarrier((&____unit_4), value);
	}

	inline static int32_t get_offset_of__pattern_5() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____pattern_5)); }
	inline String_t* get__pattern_5() const { return ____pattern_5; }
	inline String_t** get_address_of__pattern_5() { return &____pattern_5; }
	inline void set__pattern_5(String_t* value)
	{
		____pattern_5 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_5), value);
	}

	inline static int32_t get_offset_of__currentPos_6() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____currentPos_6)); }
	inline int32_t get__currentPos_6() const { return ____currentPos_6; }
	inline int32_t* get_address_of__currentPos_6() { return &____currentPos_6; }
	inline void set__currentPos_6(int32_t value)
	{
		____currentPos_6 = value;
	}

	inline static int32_t get_offset_of__culture_7() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____culture_7)); }
	inline CultureInfo_t4157843068 * get__culture_7() const { return ____culture_7; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_7() { return &____culture_7; }
	inline void set__culture_7(CultureInfo_t4157843068 * value)
	{
		____culture_7 = value;
		Il2CppCodeGenWriteBarrier((&____culture_7), value);
	}

	inline static int32_t get_offset_of__autocap_8() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____autocap_8)); }
	inline int32_t get__autocap_8() const { return ____autocap_8; }
	inline int32_t* get_address_of__autocap_8() { return &____autocap_8; }
	inline void set__autocap_8(int32_t value)
	{
		____autocap_8 = value;
	}

	inline static int32_t get_offset_of__capcount_9() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____capcount_9)); }
	inline int32_t get__capcount_9() const { return ____capcount_9; }
	inline int32_t* get_address_of__capcount_9() { return &____capcount_9; }
	inline void set__capcount_9(int32_t value)
	{
		____capcount_9 = value;
	}

	inline static int32_t get_offset_of__captop_10() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____captop_10)); }
	inline int32_t get__captop_10() const { return ____captop_10; }
	inline int32_t* get_address_of__captop_10() { return &____captop_10; }
	inline void set__captop_10(int32_t value)
	{
		____captop_10 = value;
	}

	inline static int32_t get_offset_of__capsize_11() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____capsize_11)); }
	inline int32_t get__capsize_11() const { return ____capsize_11; }
	inline int32_t* get_address_of__capsize_11() { return &____capsize_11; }
	inline void set__capsize_11(int32_t value)
	{
		____capsize_11 = value;
	}

	inline static int32_t get_offset_of__caps_12() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____caps_12)); }
	inline Hashtable_t1853889766 * get__caps_12() const { return ____caps_12; }
	inline Hashtable_t1853889766 ** get_address_of__caps_12() { return &____caps_12; }
	inline void set__caps_12(Hashtable_t1853889766 * value)
	{
		____caps_12 = value;
		Il2CppCodeGenWriteBarrier((&____caps_12), value);
	}

	inline static int32_t get_offset_of__capnames_13() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____capnames_13)); }
	inline Hashtable_t1853889766 * get__capnames_13() const { return ____capnames_13; }
	inline Hashtable_t1853889766 ** get_address_of__capnames_13() { return &____capnames_13; }
	inline void set__capnames_13(Hashtable_t1853889766 * value)
	{
		____capnames_13 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_13), value);
	}

	inline static int32_t get_offset_of__capnumlist_14() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____capnumlist_14)); }
	inline Int32U5BU5D_t385246372* get__capnumlist_14() const { return ____capnumlist_14; }
	inline Int32U5BU5D_t385246372** get_address_of__capnumlist_14() { return &____capnumlist_14; }
	inline void set__capnumlist_14(Int32U5BU5D_t385246372* value)
	{
		____capnumlist_14 = value;
		Il2CppCodeGenWriteBarrier((&____capnumlist_14), value);
	}

	inline static int32_t get_offset_of__capnamelist_15() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____capnamelist_15)); }
	inline List_1_t3319525431 * get__capnamelist_15() const { return ____capnamelist_15; }
	inline List_1_t3319525431 ** get_address_of__capnamelist_15() { return &____capnamelist_15; }
	inline void set__capnamelist_15(List_1_t3319525431 * value)
	{
		____capnamelist_15 = value;
		Il2CppCodeGenWriteBarrier((&____capnamelist_15), value);
	}

	inline static int32_t get_offset_of__options_16() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____options_16)); }
	inline int32_t get__options_16() const { return ____options_16; }
	inline int32_t* get_address_of__options_16() { return &____options_16; }
	inline void set__options_16(int32_t value)
	{
		____options_16 = value;
	}

	inline static int32_t get_offset_of__optionsStack_17() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____optionsStack_17)); }
	inline List_1_t1564920337 * get__optionsStack_17() const { return ____optionsStack_17; }
	inline List_1_t1564920337 ** get_address_of__optionsStack_17() { return &____optionsStack_17; }
	inline void set__optionsStack_17(List_1_t1564920337 * value)
	{
		____optionsStack_17 = value;
		Il2CppCodeGenWriteBarrier((&____optionsStack_17), value);
	}

	inline static int32_t get_offset_of__ignoreNextParen_18() { return static_cast<int32_t>(offsetof(RegexParser_t276209658, ____ignoreNextParen_18)); }
	inline bool get__ignoreNextParen_18() const { return ____ignoreNextParen_18; }
	inline bool* get_address_of__ignoreNextParen_18() { return &____ignoreNextParen_18; }
	inline void set__ignoreNextParen_18(bool value)
	{
		____ignoreNextParen_18 = value;
	}
};

struct RegexParser_t276209658_StaticFields
{
public:
	// System.Byte[] System.Text.RegularExpressions.RegexParser::_category
	ByteU5BU5D_t4116647657* ____category_19;

public:
	inline static int32_t get_offset_of__category_19() { return static_cast<int32_t>(offsetof(RegexParser_t276209658_StaticFields, ____category_19)); }
	inline ByteU5BU5D_t4116647657* get__category_19() const { return ____category_19; }
	inline ByteU5BU5D_t4116647657** get_address_of__category_19() { return &____category_19; }
	inline void set__category_19(ByteU5BU5D_t4116647657* value)
	{
		____category_19 = value;
		Il2CppCodeGenWriteBarrier((&____category_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXPARSER_T276209658_H
#ifndef REGEXTREE_T2725947036_H
#define REGEXTREE_T2725947036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexTree
struct  RegexTree_t2725947036  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexTree::_root
	RegexNode_t4293407243 * ____root_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_caps
	Hashtable_t1853889766 * ____caps_1;
	// System.Int32[] System.Text.RegularExpressions.RegexTree::_capnumlist
	Int32U5BU5D_t385246372* ____capnumlist_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexTree::_capnames
	Hashtable_t1853889766 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.RegexTree::_capslist
	StringU5BU5D_t1281789340* ____capslist_4;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexTree::_options
	int32_t ____options_5;
	// System.Int32 System.Text.RegularExpressions.RegexTree::_captop
	int32_t ____captop_6;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____root_0)); }
	inline RegexNode_t4293407243 * get__root_0() const { return ____root_0; }
	inline RegexNode_t4293407243 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(RegexNode_t4293407243 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier((&____root_0), value);
	}

	inline static int32_t get_offset_of__caps_1() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____caps_1)); }
	inline Hashtable_t1853889766 * get__caps_1() const { return ____caps_1; }
	inline Hashtable_t1853889766 ** get_address_of__caps_1() { return &____caps_1; }
	inline void set__caps_1(Hashtable_t1853889766 * value)
	{
		____caps_1 = value;
		Il2CppCodeGenWriteBarrier((&____caps_1), value);
	}

	inline static int32_t get_offset_of__capnumlist_2() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____capnumlist_2)); }
	inline Int32U5BU5D_t385246372* get__capnumlist_2() const { return ____capnumlist_2; }
	inline Int32U5BU5D_t385246372** get_address_of__capnumlist_2() { return &____capnumlist_2; }
	inline void set__capnumlist_2(Int32U5BU5D_t385246372* value)
	{
		____capnumlist_2 = value;
		Il2CppCodeGenWriteBarrier((&____capnumlist_2), value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____capnames_3)); }
	inline Hashtable_t1853889766 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t1853889766 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t1853889766 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_3), value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____capslist_4)); }
	inline StringU5BU5D_t1281789340* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1281789340** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1281789340* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier((&____capslist_4), value);
	}

	inline static int32_t get_offset_of__options_5() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____options_5)); }
	inline int32_t get__options_5() const { return ____options_5; }
	inline int32_t* get_address_of__options_5() { return &____options_5; }
	inline void set__options_5(int32_t value)
	{
		____options_5 = value;
	}

	inline static int32_t get_offset_of__captop_6() { return static_cast<int32_t>(offsetof(RegexTree_t2725947036, ____captop_6)); }
	inline int32_t get__captop_6() const { return ____captop_6; }
	inline int32_t* get_address_of__captop_6() { return &____captop_6; }
	inline void set__captop_6(int32_t value)
	{
		____captop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXTREE_T2725947036_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t3890150400 * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_t1092684687 * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_String_16), value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalUnicodeString_17), value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Syntax_18)); }
	inline UriParser_t3890150400 * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t3890150400 ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t3890150400 * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Syntax_18), value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_DnsSafeHost_19), value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Info_21)); }
	inline UriInfo_t1092684687 * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_t1092684687 ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_t1092684687 * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Info_21), value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t3528271667* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t3528271667* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_0), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_1), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_2), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_3), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_4), value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWs_5), value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWss_6), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_7), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_8), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_9), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_10), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_11), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_12), value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_initLock_30), value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t3528271667* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t3528271667** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t3528271667* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((&___HexLowerChars_34), value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t3528271667* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t3528271667** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t3528271667* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((&____WSchars_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
#ifndef URIFORMATEXCEPTION_T953270471_H
#define URIFORMATEXCEPTION_T953270471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormatException
struct  UriFormatException_t953270471  : public FormatException_t154580423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMATEXCEPTION_T953270471_H
#ifndef URIPARSER_T3890150400_H
#define URIPARSER_T3890150400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser
struct  UriParser_t3890150400  : public RuntimeObject
{
public:
	// System.UriSyntaxFlags System.UriParser::m_Flags
	int32_t ___m_Flags_3;
	// System.UriSyntaxFlags modreq(System.Runtime.CompilerServices.IsVolatile) System.UriParser::m_UpdatableFlags
	int32_t ___m_UpdatableFlags_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.UriParser::m_UpdatableFlagsUsed
	bool ___m_UpdatableFlagsUsed_5;
	// System.Int32 System.UriParser::m_Port
	int32_t ___m_Port_7;
	// System.String System.UriParser::m_Scheme
	String_t* ___m_Scheme_8;

public:
	inline static int32_t get_offset_of_m_Flags_3() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___m_Flags_3)); }
	inline int32_t get_m_Flags_3() const { return ___m_Flags_3; }
	inline int32_t* get_address_of_m_Flags_3() { return &___m_Flags_3; }
	inline void set_m_Flags_3(int32_t value)
	{
		___m_Flags_3 = value;
	}

	inline static int32_t get_offset_of_m_UpdatableFlags_4() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___m_UpdatableFlags_4)); }
	inline int32_t get_m_UpdatableFlags_4() const { return ___m_UpdatableFlags_4; }
	inline int32_t* get_address_of_m_UpdatableFlags_4() { return &___m_UpdatableFlags_4; }
	inline void set_m_UpdatableFlags_4(int32_t value)
	{
		___m_UpdatableFlags_4 = value;
	}

	inline static int32_t get_offset_of_m_UpdatableFlagsUsed_5() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___m_UpdatableFlagsUsed_5)); }
	inline bool get_m_UpdatableFlagsUsed_5() const { return ___m_UpdatableFlagsUsed_5; }
	inline bool* get_address_of_m_UpdatableFlagsUsed_5() { return &___m_UpdatableFlagsUsed_5; }
	inline void set_m_UpdatableFlagsUsed_5(bool value)
	{
		___m_UpdatableFlagsUsed_5 = value;
	}

	inline static int32_t get_offset_of_m_Port_7() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___m_Port_7)); }
	inline int32_t get_m_Port_7() const { return ___m_Port_7; }
	inline int32_t* get_address_of_m_Port_7() { return &___m_Port_7; }
	inline void set_m_Port_7(int32_t value)
	{
		___m_Port_7 = value;
	}

	inline static int32_t get_offset_of_m_Scheme_8() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___m_Scheme_8)); }
	inline String_t* get_m_Scheme_8() const { return ___m_Scheme_8; }
	inline String_t** get_address_of_m_Scheme_8() { return &___m_Scheme_8; }
	inline void set_m_Scheme_8(String_t* value)
	{
		___m_Scheme_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scheme_8), value);
	}
};

struct UriParser_t3890150400_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.UriParser> System.UriParser::m_Table
	Dictionary_2_t3675406699 * ___m_Table_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.UriParser> System.UriParser::m_TempTable
	Dictionary_2_t3675406699 * ___m_TempTable_2;
	// System.UriParser System.UriParser::HttpUri
	UriParser_t3890150400 * ___HttpUri_11;
	// System.UriParser System.UriParser::HttpsUri
	UriParser_t3890150400 * ___HttpsUri_12;
	// System.UriParser System.UriParser::WsUri
	UriParser_t3890150400 * ___WsUri_13;
	// System.UriParser System.UriParser::WssUri
	UriParser_t3890150400 * ___WssUri_14;
	// System.UriParser System.UriParser::FtpUri
	UriParser_t3890150400 * ___FtpUri_15;
	// System.UriParser System.UriParser::FileUri
	UriParser_t3890150400 * ___FileUri_16;
	// System.UriParser System.UriParser::GopherUri
	UriParser_t3890150400 * ___GopherUri_17;
	// System.UriParser System.UriParser::NntpUri
	UriParser_t3890150400 * ___NntpUri_18;
	// System.UriParser System.UriParser::NewsUri
	UriParser_t3890150400 * ___NewsUri_19;
	// System.UriParser System.UriParser::MailToUri
	UriParser_t3890150400 * ___MailToUri_20;
	// System.UriParser System.UriParser::UuidUri
	UriParser_t3890150400 * ___UuidUri_21;
	// System.UriParser System.UriParser::TelnetUri
	UriParser_t3890150400 * ___TelnetUri_22;
	// System.UriParser System.UriParser::LdapUri
	UriParser_t3890150400 * ___LdapUri_23;
	// System.UriParser System.UriParser::NetTcpUri
	UriParser_t3890150400 * ___NetTcpUri_24;
	// System.UriParser System.UriParser::NetPipeUri
	UriParser_t3890150400 * ___NetPipeUri_25;
	// System.UriParser System.UriParser::VsMacrosUri
	UriParser_t3890150400 * ___VsMacrosUri_26;
	// System.UriParser/UriQuirksVersion System.UriParser::s_QuirksVersion
	int32_t ___s_QuirksVersion_27;
	// System.UriSyntaxFlags System.UriParser::HttpSyntaxFlags
	int32_t ___HttpSyntaxFlags_30;
	// System.UriSyntaxFlags System.UriParser::FileSyntaxFlags
	int32_t ___FileSyntaxFlags_32;

public:
	inline static int32_t get_offset_of_m_Table_1() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___m_Table_1)); }
	inline Dictionary_2_t3675406699 * get_m_Table_1() const { return ___m_Table_1; }
	inline Dictionary_2_t3675406699 ** get_address_of_m_Table_1() { return &___m_Table_1; }
	inline void set_m_Table_1(Dictionary_2_t3675406699 * value)
	{
		___m_Table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Table_1), value);
	}

	inline static int32_t get_offset_of_m_TempTable_2() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___m_TempTable_2)); }
	inline Dictionary_2_t3675406699 * get_m_TempTable_2() const { return ___m_TempTable_2; }
	inline Dictionary_2_t3675406699 ** get_address_of_m_TempTable_2() { return &___m_TempTable_2; }
	inline void set_m_TempTable_2(Dictionary_2_t3675406699 * value)
	{
		___m_TempTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempTable_2), value);
	}

	inline static int32_t get_offset_of_HttpUri_11() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___HttpUri_11)); }
	inline UriParser_t3890150400 * get_HttpUri_11() const { return ___HttpUri_11; }
	inline UriParser_t3890150400 ** get_address_of_HttpUri_11() { return &___HttpUri_11; }
	inline void set_HttpUri_11(UriParser_t3890150400 * value)
	{
		___HttpUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___HttpUri_11), value);
	}

	inline static int32_t get_offset_of_HttpsUri_12() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___HttpsUri_12)); }
	inline UriParser_t3890150400 * get_HttpsUri_12() const { return ___HttpsUri_12; }
	inline UriParser_t3890150400 ** get_address_of_HttpsUri_12() { return &___HttpsUri_12; }
	inline void set_HttpsUri_12(UriParser_t3890150400 * value)
	{
		___HttpsUri_12 = value;
		Il2CppCodeGenWriteBarrier((&___HttpsUri_12), value);
	}

	inline static int32_t get_offset_of_WsUri_13() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___WsUri_13)); }
	inline UriParser_t3890150400 * get_WsUri_13() const { return ___WsUri_13; }
	inline UriParser_t3890150400 ** get_address_of_WsUri_13() { return &___WsUri_13; }
	inline void set_WsUri_13(UriParser_t3890150400 * value)
	{
		___WsUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___WsUri_13), value);
	}

	inline static int32_t get_offset_of_WssUri_14() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___WssUri_14)); }
	inline UriParser_t3890150400 * get_WssUri_14() const { return ___WssUri_14; }
	inline UriParser_t3890150400 ** get_address_of_WssUri_14() { return &___WssUri_14; }
	inline void set_WssUri_14(UriParser_t3890150400 * value)
	{
		___WssUri_14 = value;
		Il2CppCodeGenWriteBarrier((&___WssUri_14), value);
	}

	inline static int32_t get_offset_of_FtpUri_15() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___FtpUri_15)); }
	inline UriParser_t3890150400 * get_FtpUri_15() const { return ___FtpUri_15; }
	inline UriParser_t3890150400 ** get_address_of_FtpUri_15() { return &___FtpUri_15; }
	inline void set_FtpUri_15(UriParser_t3890150400 * value)
	{
		___FtpUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___FtpUri_15), value);
	}

	inline static int32_t get_offset_of_FileUri_16() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___FileUri_16)); }
	inline UriParser_t3890150400 * get_FileUri_16() const { return ___FileUri_16; }
	inline UriParser_t3890150400 ** get_address_of_FileUri_16() { return &___FileUri_16; }
	inline void set_FileUri_16(UriParser_t3890150400 * value)
	{
		___FileUri_16 = value;
		Il2CppCodeGenWriteBarrier((&___FileUri_16), value);
	}

	inline static int32_t get_offset_of_GopherUri_17() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___GopherUri_17)); }
	inline UriParser_t3890150400 * get_GopherUri_17() const { return ___GopherUri_17; }
	inline UriParser_t3890150400 ** get_address_of_GopherUri_17() { return &___GopherUri_17; }
	inline void set_GopherUri_17(UriParser_t3890150400 * value)
	{
		___GopherUri_17 = value;
		Il2CppCodeGenWriteBarrier((&___GopherUri_17), value);
	}

	inline static int32_t get_offset_of_NntpUri_18() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___NntpUri_18)); }
	inline UriParser_t3890150400 * get_NntpUri_18() const { return ___NntpUri_18; }
	inline UriParser_t3890150400 ** get_address_of_NntpUri_18() { return &___NntpUri_18; }
	inline void set_NntpUri_18(UriParser_t3890150400 * value)
	{
		___NntpUri_18 = value;
		Il2CppCodeGenWriteBarrier((&___NntpUri_18), value);
	}

	inline static int32_t get_offset_of_NewsUri_19() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___NewsUri_19)); }
	inline UriParser_t3890150400 * get_NewsUri_19() const { return ___NewsUri_19; }
	inline UriParser_t3890150400 ** get_address_of_NewsUri_19() { return &___NewsUri_19; }
	inline void set_NewsUri_19(UriParser_t3890150400 * value)
	{
		___NewsUri_19 = value;
		Il2CppCodeGenWriteBarrier((&___NewsUri_19), value);
	}

	inline static int32_t get_offset_of_MailToUri_20() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___MailToUri_20)); }
	inline UriParser_t3890150400 * get_MailToUri_20() const { return ___MailToUri_20; }
	inline UriParser_t3890150400 ** get_address_of_MailToUri_20() { return &___MailToUri_20; }
	inline void set_MailToUri_20(UriParser_t3890150400 * value)
	{
		___MailToUri_20 = value;
		Il2CppCodeGenWriteBarrier((&___MailToUri_20), value);
	}

	inline static int32_t get_offset_of_UuidUri_21() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___UuidUri_21)); }
	inline UriParser_t3890150400 * get_UuidUri_21() const { return ___UuidUri_21; }
	inline UriParser_t3890150400 ** get_address_of_UuidUri_21() { return &___UuidUri_21; }
	inline void set_UuidUri_21(UriParser_t3890150400 * value)
	{
		___UuidUri_21 = value;
		Il2CppCodeGenWriteBarrier((&___UuidUri_21), value);
	}

	inline static int32_t get_offset_of_TelnetUri_22() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___TelnetUri_22)); }
	inline UriParser_t3890150400 * get_TelnetUri_22() const { return ___TelnetUri_22; }
	inline UriParser_t3890150400 ** get_address_of_TelnetUri_22() { return &___TelnetUri_22; }
	inline void set_TelnetUri_22(UriParser_t3890150400 * value)
	{
		___TelnetUri_22 = value;
		Il2CppCodeGenWriteBarrier((&___TelnetUri_22), value);
	}

	inline static int32_t get_offset_of_LdapUri_23() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___LdapUri_23)); }
	inline UriParser_t3890150400 * get_LdapUri_23() const { return ___LdapUri_23; }
	inline UriParser_t3890150400 ** get_address_of_LdapUri_23() { return &___LdapUri_23; }
	inline void set_LdapUri_23(UriParser_t3890150400 * value)
	{
		___LdapUri_23 = value;
		Il2CppCodeGenWriteBarrier((&___LdapUri_23), value);
	}

	inline static int32_t get_offset_of_NetTcpUri_24() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___NetTcpUri_24)); }
	inline UriParser_t3890150400 * get_NetTcpUri_24() const { return ___NetTcpUri_24; }
	inline UriParser_t3890150400 ** get_address_of_NetTcpUri_24() { return &___NetTcpUri_24; }
	inline void set_NetTcpUri_24(UriParser_t3890150400 * value)
	{
		___NetTcpUri_24 = value;
		Il2CppCodeGenWriteBarrier((&___NetTcpUri_24), value);
	}

	inline static int32_t get_offset_of_NetPipeUri_25() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___NetPipeUri_25)); }
	inline UriParser_t3890150400 * get_NetPipeUri_25() const { return ___NetPipeUri_25; }
	inline UriParser_t3890150400 ** get_address_of_NetPipeUri_25() { return &___NetPipeUri_25; }
	inline void set_NetPipeUri_25(UriParser_t3890150400 * value)
	{
		___NetPipeUri_25 = value;
		Il2CppCodeGenWriteBarrier((&___NetPipeUri_25), value);
	}

	inline static int32_t get_offset_of_VsMacrosUri_26() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___VsMacrosUri_26)); }
	inline UriParser_t3890150400 * get_VsMacrosUri_26() const { return ___VsMacrosUri_26; }
	inline UriParser_t3890150400 ** get_address_of_VsMacrosUri_26() { return &___VsMacrosUri_26; }
	inline void set_VsMacrosUri_26(UriParser_t3890150400 * value)
	{
		___VsMacrosUri_26 = value;
		Il2CppCodeGenWriteBarrier((&___VsMacrosUri_26), value);
	}

	inline static int32_t get_offset_of_s_QuirksVersion_27() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___s_QuirksVersion_27)); }
	inline int32_t get_s_QuirksVersion_27() const { return ___s_QuirksVersion_27; }
	inline int32_t* get_address_of_s_QuirksVersion_27() { return &___s_QuirksVersion_27; }
	inline void set_s_QuirksVersion_27(int32_t value)
	{
		___s_QuirksVersion_27 = value;
	}

	inline static int32_t get_offset_of_HttpSyntaxFlags_30() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___HttpSyntaxFlags_30)); }
	inline int32_t get_HttpSyntaxFlags_30() const { return ___HttpSyntaxFlags_30; }
	inline int32_t* get_address_of_HttpSyntaxFlags_30() { return &___HttpSyntaxFlags_30; }
	inline void set_HttpSyntaxFlags_30(int32_t value)
	{
		___HttpSyntaxFlags_30 = value;
	}

	inline static int32_t get_offset_of_FileSyntaxFlags_32() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___FileSyntaxFlags_32)); }
	inline int32_t get_FileSyntaxFlags_32() const { return ___FileSyntaxFlags_32; }
	inline int32_t* get_address_of_FileSyntaxFlags_32() { return &___FileSyntaxFlags_32; }
	inline void set_FileSyntaxFlags_32(int32_t value)
	{
		___FileSyntaxFlags_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIPARSER_T3890150400_H
#ifndef URITYPECONVERTER_T3695916615_H
#define URITYPECONVERTER_T3695916615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriTypeConverter
struct  UriTypeConverter_t3695916615  : public TypeConverter_t2249118273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URITYPECONVERTER_T3695916615_H
#ifndef U3CPROCESSOPERATIONU3ED__24_T1753638225_H
#define U3CPROCESSOPERATIONU3ED__24_T1753638225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24
struct  U3CProcessOperationU3Ed__24_t1753638225 
{
public:
	// System.Int32 Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<>t__builder
	AsyncTaskMethodBuilder_t3536885450  ___U3CU3Et__builder_1;
	// System.Threading.CancellationToken Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_2;
	// Mono.Net.Security.AsyncProtocolRequest Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<>4__this
	AsyncProtocolRequest_t4184368197 * ___U3CU3E4__this_3;
	// Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<status>5__1
	int32_t ___U3CstatusU3E5__1_4;
	// Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<newStatus>5__2
	int32_t ___U3CnewStatusU3E5__2_5;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Nullable`1<System.Int32>> Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<>u__1
	ConfiguredTaskAwaiter_t1701041524  ___U3CU3Eu__1_6;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::<>u__2
	ConfiguredTaskAwaiter_t555647845  ___U3CU3Eu__2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t3536885450  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t3536885450 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t3536885450  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___cancellationToken_2)); }
	inline CancellationToken_t784455623  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t784455623  value)
	{
		___cancellationToken_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CU3E4__this_3)); }
	inline AsyncProtocolRequest_t4184368197 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AsyncProtocolRequest_t4184368197 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AsyncProtocolRequest_t4184368197 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CstatusU3E5__1_4)); }
	inline int32_t get_U3CstatusU3E5__1_4() const { return ___U3CstatusU3E5__1_4; }
	inline int32_t* get_address_of_U3CstatusU3E5__1_4() { return &___U3CstatusU3E5__1_4; }
	inline void set_U3CstatusU3E5__1_4(int32_t value)
	{
		___U3CstatusU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewStatusU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CnewStatusU3E5__2_5)); }
	inline int32_t get_U3CnewStatusU3E5__2_5() const { return ___U3CnewStatusU3E5__2_5; }
	inline int32_t* get_address_of_U3CnewStatusU3E5__2_5() { return &___U3CnewStatusU3E5__2_5; }
	inline void set_U3CnewStatusU3E5__2_5(int32_t value)
	{
		___U3CnewStatusU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_6() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CU3Eu__1_6)); }
	inline ConfiguredTaskAwaiter_t1701041524  get_U3CU3Eu__1_6() const { return ___U3CU3Eu__1_6; }
	inline ConfiguredTaskAwaiter_t1701041524 * get_address_of_U3CU3Eu__1_6() { return &___U3CU3Eu__1_6; }
	inline void set_U3CU3Eu__1_6(ConfiguredTaskAwaiter_t1701041524  value)
	{
		___U3CU3Eu__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_7() { return static_cast<int32_t>(offsetof(U3CProcessOperationU3Ed__24_t1753638225, ___U3CU3Eu__2_7)); }
	inline ConfiguredTaskAwaiter_t555647845  get_U3CU3Eu__2_7() const { return ___U3CU3Eu__2_7; }
	inline ConfiguredTaskAwaiter_t555647845 * get_address_of_U3CU3Eu__2_7() { return &___U3CU3Eu__2_7; }
	inline void set_U3CU3Eu__2_7(ConfiguredTaskAwaiter_t555647845  value)
	{
		___U3CU3Eu__2_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSOPERATIONU3ED__24_T1753638225_H
#ifndef U3CINNERWRITEU3ED__67_T2443470766_H
#define U3CINNERWRITEU3ED__67_T2443470766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67
struct  U3CInnerWriteU3Ed__67_t2443470766 
{
public:
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::<>t__builder
	AsyncTaskMethodBuilder_t3536885450  ___U3CU3Et__builder_1;
	// System.Threading.CancellationToken Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::cancellationToken
	CancellationToken_t784455623  ___cancellationToken_2;
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::<>4__this
	MobileAuthenticatedStream_t3383979266 * ___U3CU3E4__this_3;
	// System.Boolean Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::sync
	bool ___sync_4;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::<>u__1
	ConfiguredTaskAwaiter_t555647845  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInnerWriteU3Ed__67_t2443470766, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CInnerWriteU3Ed__67_t2443470766, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t3536885450  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t3536885450 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t3536885450  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(U3CInnerWriteU3Ed__67_t2443470766, ___cancellationToken_2)); }
	inline CancellationToken_t784455623  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t784455623 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t784455623  value)
	{
		___cancellationToken_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CInnerWriteU3Ed__67_t2443470766, ___U3CU3E4__this_3)); }
	inline MobileAuthenticatedStream_t3383979266 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MobileAuthenticatedStream_t3383979266 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_sync_4() { return static_cast<int32_t>(offsetof(U3CInnerWriteU3Ed__67_t2443470766, ___sync_4)); }
	inline bool get_sync_4() const { return ___sync_4; }
	inline bool* get_address_of_sync_4() { return &___sync_4; }
	inline void set_sync_4(bool value)
	{
		___sync_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CInnerWriteU3Ed__67_t2443470766, ___U3CU3Eu__1_5)); }
	inline ConfiguredTaskAwaiter_t555647845  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline ConfiguredTaskAwaiter_t555647845 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(ConfiguredTaskAwaiter_t555647845  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINNERWRITEU3ED__67_T2443470766_H
#ifndef U3CPROCESSAUTHENTICATIONU3ED__47_T649267051_H
#define U3CPROCESSAUTHENTICATIONU3ED__47_T649267051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47
struct  U3CProcessAuthenticationU3Ed__47_t649267051 
{
public:
	// System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::<>t__builder
	AsyncTaskMethodBuilder_t3536885450  ___U3CU3Et__builder_1;
	// System.Boolean Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::serverMode
	bool ___serverMode_2;
	// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::serverCertificate
	X509Certificate_t713131622 * ___serverCertificate_3;
	// System.String Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::targetHost
	String_t* ___targetHost_4;
	// Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::<>4__this
	MobileAuthenticatedStream_t3383979266 * ___U3CU3E4__this_5;
	// System.Boolean Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::runSynchronously
	bool ___runSynchronously_6;
	// System.Security.Authentication.SslProtocols Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::enabledProtocols
	int32_t ___enabledProtocols_7;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::clientCertificates
	X509CertificateCollection_t3399372417 * ___clientCertificates_8;
	// System.Boolean Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::clientCertRequired
	bool ___clientCertRequired_9;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<Mono.Net.Security.AsyncProtocolResult> Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__47::<>u__1
	ConfiguredTaskAwaiter_t410331069  ___U3CU3Eu__1_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t3536885450  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t3536885450 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t3536885450  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_serverMode_2() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___serverMode_2)); }
	inline bool get_serverMode_2() const { return ___serverMode_2; }
	inline bool* get_address_of_serverMode_2() { return &___serverMode_2; }
	inline void set_serverMode_2(bool value)
	{
		___serverMode_2 = value;
	}

	inline static int32_t get_offset_of_serverCertificate_3() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___serverCertificate_3)); }
	inline X509Certificate_t713131622 * get_serverCertificate_3() const { return ___serverCertificate_3; }
	inline X509Certificate_t713131622 ** get_address_of_serverCertificate_3() { return &___serverCertificate_3; }
	inline void set_serverCertificate_3(X509Certificate_t713131622 * value)
	{
		___serverCertificate_3 = value;
		Il2CppCodeGenWriteBarrier((&___serverCertificate_3), value);
	}

	inline static int32_t get_offset_of_targetHost_4() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___targetHost_4)); }
	inline String_t* get_targetHost_4() const { return ___targetHost_4; }
	inline String_t** get_address_of_targetHost_4() { return &___targetHost_4; }
	inline void set_targetHost_4(String_t* value)
	{
		___targetHost_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetHost_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___U3CU3E4__this_5)); }
	inline MobileAuthenticatedStream_t3383979266 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MobileAuthenticatedStream_t3383979266 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MobileAuthenticatedStream_t3383979266 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_runSynchronously_6() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___runSynchronously_6)); }
	inline bool get_runSynchronously_6() const { return ___runSynchronously_6; }
	inline bool* get_address_of_runSynchronously_6() { return &___runSynchronously_6; }
	inline void set_runSynchronously_6(bool value)
	{
		___runSynchronously_6 = value;
	}

	inline static int32_t get_offset_of_enabledProtocols_7() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___enabledProtocols_7)); }
	inline int32_t get_enabledProtocols_7() const { return ___enabledProtocols_7; }
	inline int32_t* get_address_of_enabledProtocols_7() { return &___enabledProtocols_7; }
	inline void set_enabledProtocols_7(int32_t value)
	{
		___enabledProtocols_7 = value;
	}

	inline static int32_t get_offset_of_clientCertificates_8() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___clientCertificates_8)); }
	inline X509CertificateCollection_t3399372417 * get_clientCertificates_8() const { return ___clientCertificates_8; }
	inline X509CertificateCollection_t3399372417 ** get_address_of_clientCertificates_8() { return &___clientCertificates_8; }
	inline void set_clientCertificates_8(X509CertificateCollection_t3399372417 * value)
	{
		___clientCertificates_8 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificates_8), value);
	}

	inline static int32_t get_offset_of_clientCertRequired_9() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___clientCertRequired_9)); }
	inline bool get_clientCertRequired_9() const { return ___clientCertRequired_9; }
	inline bool* get_address_of_clientCertRequired_9() { return &___clientCertRequired_9; }
	inline void set_clientCertRequired_9(bool value)
	{
		___clientCertRequired_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_10() { return static_cast<int32_t>(offsetof(U3CProcessAuthenticationU3Ed__47_t649267051, ___U3CU3Eu__1_10)); }
	inline ConfiguredTaskAwaiter_t410331069  get_U3CU3Eu__1_10() const { return ___U3CU3Eu__1_10; }
	inline ConfiguredTaskAwaiter_t410331069 * get_address_of_U3CU3Eu__1_10() { return &___U3CU3Eu__1_10; }
	inline void set_U3CU3Eu__1_10(ConfiguredTaskAwaiter_t410331069  value)
	{
		___U3CU3Eu__1_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSAUTHENTICATIONU3ED__47_T649267051_H
#ifndef SERVERCERTVALIDATIONCALLBACKWRAPPER_T1850490691_H
#define SERVERCERTVALIDATIONCALLBACKWRAPPER_T1850490691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct  ServerCertValidationCallbackWrapper_t1850490691  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCERTVALIDATIONCALLBACKWRAPPER_T1850490691_H
#ifndef ARRAYCONVERTER_T1750795769_H
#define ARRAYCONVERTER_T1750795769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ArrayConverter
struct  ArrayConverter_t1750795769  : public CollectionConverter_t3078846443
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYCONVERTER_T1750795769_H
#ifndef IOASYNCCALLBACK_T705871752_H
#define IOASYNCCALLBACK_T705871752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOAsyncCallback
struct  IOAsyncCallback_t705871752  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOASYNCCALLBACK_T705871752_H
#ifndef MATCHEVALUATOR_T632122704_H
#define MATCHEVALUATOR_T632122704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t632122704  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHEVALUATOR_T632122704_H
#ifndef BUILTINURIPARSER_T2965675049_H
#define BUILTINURIPARSER_T2965675049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser/BuiltInUriParser
struct  BuiltInUriParser_t2965675049  : public UriParser_t3890150400
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINURIPARSER_T2965675049_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (AsyncOperationStatus_t2187364345)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[5] = 
{
	AsyncOperationStatus_t2187364345::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (AsyncProtocolResult_t3382797380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[2] = 
{
	AsyncProtocolResult_t3382797380::get_offset_of_U3CUserResultU3Ek__BackingField_0(),
	AsyncProtocolResult_t3382797380::get_offset_of_U3CErrorU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (AsyncProtocolRequest_t4184368197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[7] = 
{
	AsyncProtocolRequest_t4184368197::get_offset_of_U3CParentU3Ek__BackingField_0(),
	AsyncProtocolRequest_t4184368197::get_offset_of_U3CRunSynchronouslyU3Ek__BackingField_1(),
	AsyncProtocolRequest_t4184368197::get_offset_of_U3CUserResultU3Ek__BackingField_2(),
	AsyncProtocolRequest_t4184368197::get_offset_of_Started_3(),
	AsyncProtocolRequest_t4184368197::get_offset_of_RequestedSize_4(),
	AsyncProtocolRequest_t4184368197::get_offset_of_WriteRequested_5(),
	AsyncProtocolRequest_t4184368197::get_offset_of_locker_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (U3CStartOperationU3Ed__23_t3218468820)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[5] = 
{
	U3CStartOperationU3Ed__23_t3218468820::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__23_t3218468820::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__23_t3218468820::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__23_t3218468820::get_offset_of_cancellationToken_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__23_t3218468820::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (U3CProcessOperationU3Ed__24_t1753638225)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[8] = 
{
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_cancellationToken_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CstatusU3E5__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CnewStatusU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessOperationU3Ed__24_t1753638225::get_offset_of_U3CU3Eu__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (U3CInnerReadU3Ed__25_t257820306)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[7] = 
{
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_cancellationToken_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_U3CrequestedSizeU3E5__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_U3CtotalReadU3E5__2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__25_t257820306::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (AsyncHandshakeRequest_t1887639929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (AsyncReadOrWriteRequest_t2457631485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	AsyncReadOrWriteRequest_t2457631485::get_offset_of_U3CUserBufferU3Ek__BackingField_7(),
	AsyncReadOrWriteRequest_t2457631485::get_offset_of_U3CCurrentSizeU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (AsyncReadRequest_t2148115577), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (AsyncWriteRequest_t2930005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (ServerCertValidationCallbackWrapper_t1850490691), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (ChainValidationHelper_t669322361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[8] = 
{
	ChainValidationHelper_t669322361::get_offset_of_sender_0(),
	ChainValidationHelper_t669322361::get_offset_of_settings_1(),
	ChainValidationHelper_t669322361::get_offset_of_provider_2(),
	ChainValidationHelper_t669322361::get_offset_of_certValidationCallback_3(),
	ChainValidationHelper_t669322361::get_offset_of_certSelectionCallback_4(),
	ChainValidationHelper_t669322361::get_offset_of_callbackWrapper_5(),
	ChainValidationHelper_t669322361::get_offset_of_tlsStream_6(),
	ChainValidationHelper_t669322361::get_offset_of_request_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (MobileAuthenticatedStream_t3383979266), -1, sizeof(MobileAuthenticatedStream_t3383979266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[16] = 
{
	MobileAuthenticatedStream_t3383979266::get_offset_of_xobileTlsContext_6(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_lastException_7(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_asyncHandshakeRequest_8(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_asyncReadRequest_9(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_asyncWriteRequest_10(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_readBuffer_11(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_writeBuffer_12(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_ioLock_13(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_closeRequested_14(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_shutdown_15(),
	MobileAuthenticatedStream_t3383979266_StaticFields::get_offset_of_uniqueNameInteger_16(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_U3CSslStreamU3Ek__BackingField_17(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_U3CSettingsU3Ek__BackingField_18(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_U3CProviderU3Ek__BackingField_19(),
	MobileAuthenticatedStream_t3383979266_StaticFields::get_offset_of_nextId_20(),
	MobileAuthenticatedStream_t3383979266::get_offset_of_ID_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (OperationType_t993417619)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1713[4] = 
{
	OperationType_t993417619::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (U3CProcessAuthenticationU3Ed__47_t649267051)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[11] = 
{
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_serverMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_serverCertificate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_targetHost_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_U3CU3E4__this_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_runSynchronously_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_enabledProtocols_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_clientCertificates_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_clientCertRequired_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CProcessAuthenticationU3Ed__47_t649267051::get_offset_of_U3CU3Eu__1_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (U3CStartOperationU3Ed__58_t1428438294)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[7] = 
{
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_type_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_asyncRequest_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_cancellationToken_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CStartOperationU3Ed__58_t1428438294::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (U3CU3Ec__DisplayClass66_0_t2450223149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[2] = 
{
	U3CU3Ec__DisplayClass66_0_t2450223149::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass66_0_t2450223149::get_offset_of_len_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (U3CInnerReadU3Ed__66_t2872199449)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[7] = 
{
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_cancellationToken_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_requestedSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_sync_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerReadU3Ed__66_t2872199449::get_offset_of_U3CU3Eu__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (U3CInnerWriteU3Ed__67_t2443470766)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[6] = 
{
	U3CInnerWriteU3Ed__67_t2443470766::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerWriteU3Ed__67_t2443470766::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerWriteU3Ed__67_t2443470766::get_offset_of_cancellationToken_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerWriteU3Ed__67_t2443470766::get_offset_of_U3CU3E4__this_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerWriteU3Ed__67_t2443470766::get_offset_of_sync_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CInnerWriteU3Ed__67_t2443470766::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (MobileTlsContext_t1069274405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[9] = 
{
	MobileTlsContext_t1069274405::get_offset_of_parent_0(),
	MobileTlsContext_t1069274405::get_offset_of_serverMode_1(),
	MobileTlsContext_t1069274405::get_offset_of_targetHost_2(),
	MobileTlsContext_t1069274405::get_offset_of_serverName_3(),
	MobileTlsContext_t1069274405::get_offset_of_enabledProtocols_4(),
	MobileTlsContext_t1069274405::get_offset_of_serverCertificate_5(),
	MobileTlsContext_t1069274405::get_offset_of_clientCertificates_6(),
	MobileTlsContext_t1069274405::get_offset_of_askForClientCert_7(),
	MobileTlsContext_t1069274405::get_offset_of_certificateValidator_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (MonoTlsProviderFactory_t4084560240), -1, sizeof(MonoTlsProviderFactory_t4084560240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1720[9] = 
{
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_locker_0(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_initialized_1(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_defaultProvider_2(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_providerRegistration_3(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_providerCache_4(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_UnityTlsId_5(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_AppleTlsId_6(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_BtlsId_7(),
	MonoTlsProviderFactory_t4084560240_StaticFields::get_offset_of_LegacyId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (MonoTlsStream_t1980138907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[7] = 
{
	MonoTlsStream_t1980138907::get_offset_of_provider_0(),
	MonoTlsStream_t1980138907::get_offset_of_networkStream_1(),
	MonoTlsStream_t1980138907::get_offset_of_request_2(),
	MonoTlsStream_t1980138907::get_offset_of_settings_3(),
	MonoTlsStream_t1980138907::get_offset_of_sslStream_4(),
	MonoTlsStream_t1980138907::get_offset_of_status_5(),
	MonoTlsStream_t1980138907::get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (NoReflectionHelper_t2046157126), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (SystemCertificateValidator_t3152079745), -1, sizeof(SystemCertificateValidator_t3152079745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[2] = 
{
	SystemCertificateValidator_t3152079745_StaticFields::get_offset_of_is_macosx_0(),
	SystemCertificateValidator_t3152079745_StaticFields::get_offset_of_s_flags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (CallbackHelpers_t1924962770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (U3CU3Ec__DisplayClass5_0_t2247204716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[1] = 
{
	U3CU3Ec__DisplayClass5_0_t2247204716::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (U3CU3Ec__DisplayClass8_0_t3821182828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[1] = 
{
	U3CU3Ec__DisplayClass8_0_t3821182828::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (NtlmSession_t2676553120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	NtlmSession_t2676553120::get_offset_of_message_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (NtlmClient_t1228044663), -1, sizeof(NtlmClient_t1228044663_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	NtlmClient_t1228044663_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U3CU3Ec_t1220335544), -1, sizeof(U3CU3Ec_t1220335544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1729[2] = 
{
	U3CU3Ec_t1220335544_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1220335544_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (UriBuilder_t579353065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[11] = 
{
	UriBuilder_t579353065::get_offset_of__changed_0(),
	UriBuilder_t579353065::get_offset_of__fragment_1(),
	UriBuilder_t579353065::get_offset_of__host_2(),
	UriBuilder_t579353065::get_offset_of__password_3(),
	UriBuilder_t579353065::get_offset_of__path_4(),
	UriBuilder_t579353065::get_offset_of__port_5(),
	UriBuilder_t579353065::get_offset_of__query_6(),
	UriBuilder_t579353065::get_offset_of__scheme_7(),
	UriBuilder_t579353065::get_offset_of__schemeDelimiter_8(),
	UriBuilder_t579353065::get_offset_of__uri_9(),
	UriBuilder_t579353065::get_offset_of__username_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (IriHelper_t2107623466), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (Uri_t100236324), -1, sizeof(Uri_t100236324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1733[36] = 
{
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeFile_0(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeFtp_1(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeGopher_2(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeHttp_3(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeHttps_4(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeWs_5(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeWss_6(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeMailto_7(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNews_8(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNntp_9(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNetTcp_10(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNetPipe_11(),
	Uri_t100236324_StaticFields::get_offset_of_SchemeDelimiter_12(),
	0,
	0,
	0,
	Uri_t100236324::get_offset_of_m_String_16(),
	Uri_t100236324::get_offset_of_m_originalUnicodeString_17(),
	Uri_t100236324::get_offset_of_m_Syntax_18(),
	Uri_t100236324::get_offset_of_m_DnsSafeHost_19(),
	Uri_t100236324::get_offset_of_m_Flags_20(),
	Uri_t100236324::get_offset_of_m_Info_21(),
	Uri_t100236324::get_offset_of_m_iriParsing_22(),
	Uri_t100236324_StaticFields::get_offset_of_s_ConfigInitialized_23(),
	Uri_t100236324_StaticFields::get_offset_of_s_ConfigInitializing_24(),
	Uri_t100236324_StaticFields::get_offset_of_s_IdnScope_25(),
	Uri_t100236324_StaticFields::get_offset_of_s_IriParsing_26(),
	Uri_t100236324_StaticFields::get_offset_of_useDotNetRelativeOrAbsolute_27(),
	0,
	Uri_t100236324_StaticFields::get_offset_of_IsWindowsFileSystem_29(),
	Uri_t100236324_StaticFields::get_offset_of_s_initLock_30(),
	0,
	0,
	0,
	Uri_t100236324_StaticFields::get_offset_of_HexLowerChars_34(),
	Uri_t100236324_StaticFields::get_offset_of__WSchars_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (Flags_t2372798318)+ sizeof (RuntimeObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1734[56] = 
{
	Flags_t2372798318::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (UriInfo_t1092684687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[6] = 
{
	UriInfo_t1092684687::get_offset_of_Host_0(),
	UriInfo_t1092684687::get_offset_of_ScopeId_1(),
	UriInfo_t1092684687::get_offset_of_String_2(),
	UriInfo_t1092684687::get_offset_of_Offset_3(),
	UriInfo_t1092684687::get_offset_of_DnsSafeHost_4(),
	UriInfo_t1092684687::get_offset_of_MoreInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (Offset_t4247613535)+ sizeof (RuntimeObject), sizeof(Offset_t4247613535 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1736[8] = 
{
	Offset_t4247613535::get_offset_of_Scheme_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_User_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_Host_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_PortValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_Path_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_Query_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_Fragment_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t4247613535::get_offset_of_End_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (MoreInfo_t2349391856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[6] = 
{
	MoreInfo_t2349391856::get_offset_of_Path_0(),
	MoreInfo_t2349391856::get_offset_of_Query_1(),
	MoreInfo_t2349391856::get_offset_of_Fragment_2(),
	MoreInfo_t2349391856::get_offset_of_AbsoluteUri_3(),
	MoreInfo_t2349391856::get_offset_of_Hash_4(),
	MoreInfo_t2349391856::get_offset_of_RemoteUrl_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (Check_t1263065566)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[10] = 
{
	Check_t1263065566::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (UriFormatException_t953270471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (UriKind_t3816567336)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1740[4] = 
{
	UriKind_t3816567336::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (UriComponents_t814099658)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[18] = 
{
	UriComponents_t814099658::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (UriFormat_t2031163398)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[4] = 
{
	UriFormat_t2031163398::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (UriIdnScope_t1847433844)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[4] = 
{
	UriIdnScope_t1847433844::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (ParsingError_t4196835875)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1744[15] = 
{
	ParsingError_t4196835875::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (UnescapeMode_t3819662027)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1745[8] = 
{
	UnescapeMode_t3819662027::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (UriHelper_t3559114794), -1, sizeof(UriHelper_t3559114794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1746[1] = 
{
	UriHelper_t3559114794_StaticFields::get_offset_of_HexUpperChars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (UriHostNameType_t881866241)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[6] = 
{
	UriHostNameType_t881866241::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (UriParser_t3890150400), -1, sizeof(UriParser_t3890150400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1748[42] = 
{
	0,
	UriParser_t3890150400_StaticFields::get_offset_of_m_Table_1(),
	UriParser_t3890150400_StaticFields::get_offset_of_m_TempTable_2(),
	UriParser_t3890150400::get_offset_of_m_Flags_3(),
	UriParser_t3890150400::get_offset_of_m_UpdatableFlags_4(),
	UriParser_t3890150400::get_offset_of_m_UpdatableFlagsUsed_5(),
	0,
	UriParser_t3890150400::get_offset_of_m_Port_7(),
	UriParser_t3890150400::get_offset_of_m_Scheme_8(),
	0,
	0,
	UriParser_t3890150400_StaticFields::get_offset_of_HttpUri_11(),
	UriParser_t3890150400_StaticFields::get_offset_of_HttpsUri_12(),
	UriParser_t3890150400_StaticFields::get_offset_of_WsUri_13(),
	UriParser_t3890150400_StaticFields::get_offset_of_WssUri_14(),
	UriParser_t3890150400_StaticFields::get_offset_of_FtpUri_15(),
	UriParser_t3890150400_StaticFields::get_offset_of_FileUri_16(),
	UriParser_t3890150400_StaticFields::get_offset_of_GopherUri_17(),
	UriParser_t3890150400_StaticFields::get_offset_of_NntpUri_18(),
	UriParser_t3890150400_StaticFields::get_offset_of_NewsUri_19(),
	UriParser_t3890150400_StaticFields::get_offset_of_MailToUri_20(),
	UriParser_t3890150400_StaticFields::get_offset_of_UuidUri_21(),
	UriParser_t3890150400_StaticFields::get_offset_of_TelnetUri_22(),
	UriParser_t3890150400_StaticFields::get_offset_of_LdapUri_23(),
	UriParser_t3890150400_StaticFields::get_offset_of_NetTcpUri_24(),
	UriParser_t3890150400_StaticFields::get_offset_of_NetPipeUri_25(),
	UriParser_t3890150400_StaticFields::get_offset_of_VsMacrosUri_26(),
	UriParser_t3890150400_StaticFields::get_offset_of_s_QuirksVersion_27(),
	0,
	0,
	UriParser_t3890150400_StaticFields::get_offset_of_HttpSyntaxFlags_30(),
	0,
	UriParser_t3890150400_StaticFields::get_offset_of_FileSyntaxFlags_32(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (UriQuirksVersion_t2829553638)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1749[3] = 
{
	UriQuirksVersion_t2829553638::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (BuiltInUriParser_t2965675049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (DomainNameHelper_t3334322841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (IPv4AddressHelper_t1053940972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (IPv6AddressHelper_t3063601302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (UncNameHelper_t1428364228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (UriSyntaxFlags_t3715012245)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1755[30] = 
{
	UriSyntaxFlags_t3715012245::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (IOOperation_t344984103)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[3] = 
{
	IOOperation_t344984103::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (IOAsyncCallback_t705871752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (IOAsyncResult_t3640145766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[5] = 
{
	IOAsyncResult_t3640145766::get_offset_of_async_callback_0(),
	IOAsyncResult_t3640145766::get_offset_of_async_state_1(),
	IOAsyncResult_t3640145766::get_offset_of_wait_handle_2(),
	IOAsyncResult_t3640145766::get_offset_of_completed_synchronously_3(),
	IOAsyncResult_t3640145766::get_offset_of_completed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (IOSelectorJob_t2199748873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[3] = 
{
	IOSelectorJob_t2199748873::get_offset_of_operation_0(),
	IOSelectorJob_t2199748873::get_offset_of_callback_1(),
	IOSelectorJob_t2199748873::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (IOSelector_t1612260334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (Platform_t535934311), -1, sizeof(Platform_t535934311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1761[3] = 
{
	Platform_t535934311_StaticFields::get_offset_of_checkedOS_0(),
	Platform_t535934311_StaticFields::get_offset_of_isMacOS_1(),
	Platform_t535934311_StaticFields::get_offset_of_isFreeBSD_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (UriTypeConverter_t3695916615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (Regex_t3657309853), -1, sizeof(Regex_t3657309853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1763[20] = 
{
	Regex_t3657309853::get_offset_of_pattern_0(),
	Regex_t3657309853::get_offset_of_factory_1(),
	Regex_t3657309853::get_offset_of_roptions_2(),
	Regex_t3657309853_StaticFields::get_offset_of_MaximumMatchTimeout_3(),
	Regex_t3657309853_StaticFields::get_offset_of_InfiniteMatchTimeout_4(),
	Regex_t3657309853::get_offset_of_internalMatchTimeout_5(),
	0,
	Regex_t3657309853_StaticFields::get_offset_of_FallbackDefaultMatchTimeout_7(),
	Regex_t3657309853_StaticFields::get_offset_of_DefaultMatchTimeout_8(),
	Regex_t3657309853::get_offset_of_caps_9(),
	Regex_t3657309853::get_offset_of_capnames_10(),
	Regex_t3657309853::get_offset_of_capslist_11(),
	Regex_t3657309853::get_offset_of_capsize_12(),
	Regex_t3657309853::get_offset_of_runnerref_13(),
	Regex_t3657309853::get_offset_of_replref_14(),
	Regex_t3657309853::get_offset_of_code_15(),
	Regex_t3657309853::get_offset_of_refsInitialized_16(),
	Regex_t3657309853_StaticFields::get_offset_of_livecode_17(),
	Regex_t3657309853_StaticFields::get_offset_of_cacheSize_18(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (MatchEvaluator_t632122704), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (CachedCodeEntry_t4228975826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[9] = 
{
	CachedCodeEntry_t4228975826::get_offset_of__key_0(),
	CachedCodeEntry_t4228975826::get_offset_of__code_1(),
	CachedCodeEntry_t4228975826::get_offset_of__caps_2(),
	CachedCodeEntry_t4228975826::get_offset_of__capnames_3(),
	CachedCodeEntry_t4228975826::get_offset_of__capslist_4(),
	CachedCodeEntry_t4228975826::get_offset_of__capsize_5(),
	CachedCodeEntry_t4228975826::get_offset_of__factory_6(),
	CachedCodeEntry_t4228975826::get_offset_of__runnerref_7(),
	CachedCodeEntry_t4228975826::get_offset_of__replref_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (ExclusiveReference_t1927754563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[3] = 
{
	ExclusiveReference_t1927754563::get_offset_of__ref_0(),
	ExclusiveReference_t1927754563::get_offset_of__obj_1(),
	ExclusiveReference_t1927754563::get_offset_of__locked_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (SharedReference_t2916547576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[2] = 
{
	SharedReference_t2916547576::get_offset_of__ref_0(),
	SharedReference_t2916547576::get_offset_of__locked_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (RegexBoyerMoore_t1480609368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[10] = 
{
	RegexBoyerMoore_t1480609368::get_offset_of__positive_0(),
	RegexBoyerMoore_t1480609368::get_offset_of__negativeASCII_1(),
	RegexBoyerMoore_t1480609368::get_offset_of__negativeUnicode_2(),
	RegexBoyerMoore_t1480609368::get_offset_of__pattern_3(),
	RegexBoyerMoore_t1480609368::get_offset_of__lowASCII_4(),
	RegexBoyerMoore_t1480609368::get_offset_of__highASCII_5(),
	RegexBoyerMoore_t1480609368::get_offset_of__rightToLeft_6(),
	RegexBoyerMoore_t1480609368::get_offset_of__caseInsensitive_7(),
	RegexBoyerMoore_t1480609368::get_offset_of__culture_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (Capture_t2232016050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[3] = 
{
	Capture_t2232016050::get_offset_of__text_0(),
	Capture_t2232016050::get_offset_of__index_1(),
	Capture_t2232016050::get_offset_of__length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (CaptureCollection_t1760593541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[3] = 
{
	CaptureCollection_t1760593541::get_offset_of__group_0(),
	CaptureCollection_t1760593541::get_offset_of__capcount_1(),
	CaptureCollection_t1760593541::get_offset_of__captures_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (CaptureEnumerator_t3579258555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[2] = 
{
	CaptureEnumerator_t3579258555::get_offset_of__rcc_0(),
	CaptureEnumerator_t3579258555::get_offset_of__curindex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (RegexCharClass_t2088948945), -1, sizeof(RegexCharClass_t2088948945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1772[19] = 
{
	RegexCharClass_t2088948945::get_offset_of__rangelist_0(),
	RegexCharClass_t2088948945::get_offset_of__categories_1(),
	RegexCharClass_t2088948945::get_offset_of__canonical_2(),
	RegexCharClass_t2088948945::get_offset_of__negate_3(),
	RegexCharClass_t2088948945::get_offset_of__subtractor_4(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_InternalRegexIgnoreCase_5(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_Space_6(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_NotSpace_7(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_Word_8(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_NotWord_9(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_SpaceClass_10(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_NotSpaceClass_11(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_WordClass_12(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_NotWordClass_13(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_DigitClass_14(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of_NotDigitClass_15(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of__definedCategories_16(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of__propTable_17(),
	RegexCharClass_t2088948945_StaticFields::get_offset_of__lcTable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (LowerCaseMapping_t2910317575)+ sizeof (RuntimeObject), sizeof(LowerCaseMapping_t2910317575_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[4] = 
{
	LowerCaseMapping_t2910317575::get_offset_of__chMin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2910317575::get_offset_of__chMax_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2910317575::get_offset_of__lcOp_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2910317575::get_offset_of__data_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (SingleRangeComparer_t1611033177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (SingleRange_t4093686398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[2] = 
{
	SingleRange_t4093686398::get_offset_of__first_0(),
	SingleRange_t4093686398::get_offset_of__last_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (RegexCode_t4293407246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[57] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RegexCode_t4293407246::get_offset_of__codes_48(),
	RegexCode_t4293407246::get_offset_of__strings_49(),
	RegexCode_t4293407246::get_offset_of__trackcount_50(),
	RegexCode_t4293407246::get_offset_of__caps_51(),
	RegexCode_t4293407246::get_offset_of__capsize_52(),
	RegexCode_t4293407246::get_offset_of__fcPrefix_53(),
	RegexCode_t4293407246::get_offset_of__bmPrefix_54(),
	RegexCode_t4293407246::get_offset_of__anchors_55(),
	RegexCode_t4293407246::get_offset_of__rightToLeft_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (RegexFCD_t3501104819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[7] = 
{
	RegexFCD_t3501104819::get_offset_of__intStack_0(),
	RegexFCD_t3501104819::get_offset_of__intDepth_1(),
	RegexFCD_t3501104819::get_offset_of__fcStack_2(),
	RegexFCD_t3501104819::get_offset_of__fcDepth_3(),
	RegexFCD_t3501104819::get_offset_of__skipAllChildren_4(),
	RegexFCD_t3501104819::get_offset_of__skipchild_5(),
	RegexFCD_t3501104819::get_offset_of__failed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (RegexFC_t4283085439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[3] = 
{
	RegexFC_t4283085439::get_offset_of__cc_0(),
	RegexFC_t4283085439::get_offset_of__nullable_1(),
	RegexFC_t4283085439::get_offset_of__caseInsensitive_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (RegexPrefix_t3750605839), -1, sizeof(RegexPrefix_t3750605839_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1779[3] = 
{
	RegexPrefix_t3750605839::get_offset_of__prefix_0(),
	RegexPrefix_t3750605839::get_offset_of__caseInsensitive_1(),
	RegexPrefix_t3750605839_StaticFields::get_offset_of__empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (Group_t2468205786), -1, sizeof(Group_t2468205786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1780[5] = 
{
	Group_t2468205786_StaticFields::get_offset_of__emptygroup_3(),
	Group_t2468205786::get_offset_of__caps_4(),
	Group_t2468205786::get_offset_of__capcount_5(),
	Group_t2468205786::get_offset_of__capcoll_6(),
	Group_t2468205786::get_offset_of__name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (GroupCollection_t69770484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[3] = 
{
	GroupCollection_t69770484::get_offset_of__match_0(),
	GroupCollection_t69770484::get_offset_of__captureMap_1(),
	GroupCollection_t69770484::get_offset_of__groups_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (GroupEnumerator_t441528474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[2] = 
{
	GroupEnumerator_t441528474::get_offset_of__rgc_0(),
	GroupEnumerator_t441528474::get_offset_of__curindex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (RegexInterpreter_t1494189873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[11] = 
{
	RegexInterpreter_t1494189873::get_offset_of_runoperator_19(),
	RegexInterpreter_t1494189873::get_offset_of_runcodes_20(),
	RegexInterpreter_t1494189873::get_offset_of_runcodepos_21(),
	RegexInterpreter_t1494189873::get_offset_of_runstrings_22(),
	RegexInterpreter_t1494189873::get_offset_of_runcode_23(),
	RegexInterpreter_t1494189873::get_offset_of_runfcPrefix_24(),
	RegexInterpreter_t1494189873::get_offset_of_runbmPrefix_25(),
	RegexInterpreter_t1494189873::get_offset_of_runanchors_26(),
	RegexInterpreter_t1494189873::get_offset_of_runrtl_27(),
	RegexInterpreter_t1494189873::get_offset_of_runci_28(),
	RegexInterpreter_t1494189873::get_offset_of_runculture_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (Match_t3408321083), -1, sizeof(Match_t3408321083_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1784[10] = 
{
	Match_t3408321083_StaticFields::get_offset_of__empty_8(),
	Match_t3408321083::get_offset_of__groupcoll_9(),
	Match_t3408321083::get_offset_of__regex_10(),
	Match_t3408321083::get_offset_of__textbeg_11(),
	Match_t3408321083::get_offset_of__textpos_12(),
	Match_t3408321083::get_offset_of__textend_13(),
	Match_t3408321083::get_offset_of__textstart_14(),
	Match_t3408321083::get_offset_of__matches_15(),
	Match_t3408321083::get_offset_of__matchcount_16(),
	Match_t3408321083::get_offset_of__balancing_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (MatchSparse_t3706801657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[1] = 
{
	MatchSparse_t3706801657::get_offset_of__caps_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (MatchCollection_t1395363720), -1, sizeof(MatchCollection_t1395363720_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1786[9] = 
{
	MatchCollection_t1395363720::get_offset_of__regex_0(),
	MatchCollection_t1395363720::get_offset_of__matches_1(),
	MatchCollection_t1395363720::get_offset_of__done_2(),
	MatchCollection_t1395363720::get_offset_of__input_3(),
	MatchCollection_t1395363720::get_offset_of__beginning_4(),
	MatchCollection_t1395363720::get_offset_of__length_5(),
	MatchCollection_t1395363720::get_offset_of__startat_6(),
	MatchCollection_t1395363720::get_offset_of__prevlen_7(),
	MatchCollection_t1395363720_StaticFields::get_offset_of_infinite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (MatchEnumerator_t912837736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[4] = 
{
	MatchEnumerator_t912837736::get_offset_of__matchcoll_0(),
	MatchEnumerator_t912837736::get_offset_of__match_1(),
	MatchEnumerator_t912837736::get_offset_of__curindex_2(),
	MatchEnumerator_t912837736::get_offset_of__done_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (RegexMatchTimeoutException_t1986163303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[3] = 
{
	RegexMatchTimeoutException_t1986163303::get_offset_of_regexInput_17(),
	RegexMatchTimeoutException_t1986163303::get_offset_of_regexPattern_18(),
	RegexMatchTimeoutException_t1986163303::get_offset_of_matchTimeout_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (RegexNode_t4293407243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[8] = 
{
	RegexNode_t4293407243::get_offset_of__type_0(),
	RegexNode_t4293407243::get_offset_of__children_1(),
	RegexNode_t4293407243::get_offset_of__str_2(),
	RegexNode_t4293407243::get_offset_of__ch_3(),
	RegexNode_t4293407243::get_offset_of__m_4(),
	RegexNode_t4293407243::get_offset_of__n_5(),
	RegexNode_t4293407243::get_offset_of__options_6(),
	RegexNode_t4293407243::get_offset_of__next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (RegexOptions_t92845595)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[11] = 
{
	RegexOptions_t92845595::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (RegexParser_t276209658), -1, sizeof(RegexParser_t276209658_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1791[20] = 
{
	RegexParser_t276209658::get_offset_of__stack_0(),
	RegexParser_t276209658::get_offset_of__group_1(),
	RegexParser_t276209658::get_offset_of__alternation_2(),
	RegexParser_t276209658::get_offset_of__concatenation_3(),
	RegexParser_t276209658::get_offset_of__unit_4(),
	RegexParser_t276209658::get_offset_of__pattern_5(),
	RegexParser_t276209658::get_offset_of__currentPos_6(),
	RegexParser_t276209658::get_offset_of__culture_7(),
	RegexParser_t276209658::get_offset_of__autocap_8(),
	RegexParser_t276209658::get_offset_of__capcount_9(),
	RegexParser_t276209658::get_offset_of__captop_10(),
	RegexParser_t276209658::get_offset_of__capsize_11(),
	RegexParser_t276209658::get_offset_of__caps_12(),
	RegexParser_t276209658::get_offset_of__capnames_13(),
	RegexParser_t276209658::get_offset_of__capnumlist_14(),
	RegexParser_t276209658::get_offset_of__capnamelist_15(),
	RegexParser_t276209658::get_offset_of__options_16(),
	RegexParser_t276209658::get_offset_of__optionsStack_17(),
	RegexParser_t276209658::get_offset_of__ignoreNextParen_18(),
	RegexParser_t276209658_StaticFields::get_offset_of__category_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (RegexReplacement_t2160826183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[3] = 
{
	RegexReplacement_t2160826183::get_offset_of__rep_0(),
	RegexReplacement_t2160826183::get_offset_of__strings_1(),
	RegexReplacement_t2160826183::get_offset_of__rules_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (RegexRunner_t300319648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[19] = 
{
	RegexRunner_t300319648::get_offset_of_runtextbeg_0(),
	RegexRunner_t300319648::get_offset_of_runtextend_1(),
	RegexRunner_t300319648::get_offset_of_runtextstart_2(),
	RegexRunner_t300319648::get_offset_of_runtext_3(),
	RegexRunner_t300319648::get_offset_of_runtextpos_4(),
	RegexRunner_t300319648::get_offset_of_runtrack_5(),
	RegexRunner_t300319648::get_offset_of_runtrackpos_6(),
	RegexRunner_t300319648::get_offset_of_runstack_7(),
	RegexRunner_t300319648::get_offset_of_runstackpos_8(),
	RegexRunner_t300319648::get_offset_of_runcrawl_9(),
	RegexRunner_t300319648::get_offset_of_runcrawlpos_10(),
	RegexRunner_t300319648::get_offset_of_runtrackcount_11(),
	RegexRunner_t300319648::get_offset_of_runmatch_12(),
	RegexRunner_t300319648::get_offset_of_runregex_13(),
	RegexRunner_t300319648::get_offset_of_timeout_14(),
	RegexRunner_t300319648::get_offset_of_ignoreTimeout_15(),
	RegexRunner_t300319648::get_offset_of_timeoutOccursAt_16(),
	0,
	RegexRunner_t300319648::get_offset_of_timeoutChecksToSkip_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (RegexRunnerFactory_t51159052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (RegexTree_t2725947036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[7] = 
{
	RegexTree_t2725947036::get_offset_of__root_0(),
	RegexTree_t2725947036::get_offset_of__caps_1(),
	RegexTree_t2725947036::get_offset_of__capnumlist_2(),
	RegexTree_t2725947036::get_offset_of__capnames_3(),
	RegexTree_t2725947036::get_offset_of__capslist_4(),
	RegexTree_t2725947036::get_offset_of__options_5(),
	RegexTree_t2725947036::get_offset_of__captop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (RegexWriter_t2686991670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[10] = 
{
	RegexWriter_t2686991670::get_offset_of__intStack_0(),
	RegexWriter_t2686991670::get_offset_of__depth_1(),
	RegexWriter_t2686991670::get_offset_of__emitted_2(),
	RegexWriter_t2686991670::get_offset_of__curpos_3(),
	RegexWriter_t2686991670::get_offset_of__stringhash_4(),
	RegexWriter_t2686991670::get_offset_of__stringtable_5(),
	RegexWriter_t2686991670::get_offset_of__counting_6(),
	RegexWriter_t2686991670::get_offset_of__count_7(),
	RegexWriter_t2686991670::get_offset_of__trackcount_8(),
	RegexWriter_t2686991670::get_offset_of__caps_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (Stopwatch_t305734070), -1, sizeof(Stopwatch_t305734070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[5] = 
{
	Stopwatch_t305734070_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t305734070_StaticFields::get_offset_of_IsHighResolution_1(),
	Stopwatch_t305734070::get_offset_of_elapsed_2(),
	Stopwatch_t305734070::get_offset_of_started_3(),
	Stopwatch_t305734070::get_offset_of_is_running_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ExcludeFromCodeCoverageAttribute_t3578759089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (ArrayConverter_t1750795769), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
