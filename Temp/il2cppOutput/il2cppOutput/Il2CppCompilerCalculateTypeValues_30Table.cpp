﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t1264377477;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t4177122770;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_t3050575418;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t3521823603;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Dictionary_2_t3502080855;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Byte>>
struct List_1_t4078445860;
// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelQOS>
struct List_1_t3363058862;
// System.Collections.Generic.List`1<UnityEngine.Networking.ConnectionConfig>
struct List_1_t1351088715;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot>
struct List_1_t2090095927;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t100236324;
// System.Void
struct Void_t1185182177;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t3163629820;
// UnityEngine.Analytics.UnityAnalyticsHandler
struct UnityAnalyticsHandler_t3011359618;
// UnityEngine.Networking.ConnectionConfig
struct ConnectionConfig_t4173981269;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t320639760;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;




#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef U3CMODULEU3E_T692745552_H
#define U3CMODULEU3E_T692745552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745552 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745552_H
#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYADSSETTINGS_T2370518495_H
#define UNITYADSSETTINGS_T2370518495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnityAdsSettings
struct  UnityAdsSettings_t2370518495  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSSETTINGS_T2370518495_H
#ifndef ANALYTICS_T661012366_H
#define ANALYTICS_T661012366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.Analytics
struct  Analytics_t661012366  : public RuntimeObject
{
public:

public:
};

struct Analytics_t661012366_StaticFields
{
public:
	// UnityEngine.Analytics.UnityAnalyticsHandler UnityEngine.Analytics.Analytics::s_UnityAnalyticsHandler
	UnityAnalyticsHandler_t3011359618 * ___s_UnityAnalyticsHandler_0;

public:
	inline static int32_t get_offset_of_s_UnityAnalyticsHandler_0() { return static_cast<int32_t>(offsetof(Analytics_t661012366_StaticFields, ___s_UnityAnalyticsHandler_0)); }
	inline UnityAnalyticsHandler_t3011359618 * get_s_UnityAnalyticsHandler_0() const { return ___s_UnityAnalyticsHandler_0; }
	inline UnityAnalyticsHandler_t3011359618 ** get_address_of_s_UnityAnalyticsHandler_0() { return &___s_UnityAnalyticsHandler_0; }
	inline void set_s_UnityAnalyticsHandler_0(UnityAnalyticsHandler_t3011359618 * value)
	{
		___s_UnityAnalyticsHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityAnalyticsHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICS_T661012366_H
#ifndef ANALYTICSSESSIONINFO_T2322308579_H
#define ANALYTICSSESSIONINFO_T2322308579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_t2322308579  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_t2322308579_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t3163629820 * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_t2322308579_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t3163629820 * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t3163629820 ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t3163629820 * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_T2322308579_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef HOSTTOPOLOGY_T4059263395_H
#define HOSTTOPOLOGY_T4059263395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.HostTopology
struct  HostTopology_t4059263395  : public RuntimeObject
{
public:
	// UnityEngine.Networking.ConnectionConfig UnityEngine.Networking.HostTopology::m_DefConfig
	ConnectionConfig_t4173981269 * ___m_DefConfig_0;
	// System.Int32 UnityEngine.Networking.HostTopology::m_MaxDefConnections
	int32_t ___m_MaxDefConnections_1;
	// System.Collections.Generic.List`1<UnityEngine.Networking.ConnectionConfig> UnityEngine.Networking.HostTopology::m_SpecialConnections
	List_1_t1351088715 * ___m_SpecialConnections_2;
	// System.UInt16 UnityEngine.Networking.HostTopology::m_ReceivedMessagePoolSize
	uint16_t ___m_ReceivedMessagePoolSize_3;
	// System.UInt16 UnityEngine.Networking.HostTopology::m_SentMessagePoolSize
	uint16_t ___m_SentMessagePoolSize_4;
	// System.Single UnityEngine.Networking.HostTopology::m_MessagePoolSizeGrowthFactor
	float ___m_MessagePoolSizeGrowthFactor_5;

public:
	inline static int32_t get_offset_of_m_DefConfig_0() { return static_cast<int32_t>(offsetof(HostTopology_t4059263395, ___m_DefConfig_0)); }
	inline ConnectionConfig_t4173981269 * get_m_DefConfig_0() const { return ___m_DefConfig_0; }
	inline ConnectionConfig_t4173981269 ** get_address_of_m_DefConfig_0() { return &___m_DefConfig_0; }
	inline void set_m_DefConfig_0(ConnectionConfig_t4173981269 * value)
	{
		___m_DefConfig_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefConfig_0), value);
	}

	inline static int32_t get_offset_of_m_MaxDefConnections_1() { return static_cast<int32_t>(offsetof(HostTopology_t4059263395, ___m_MaxDefConnections_1)); }
	inline int32_t get_m_MaxDefConnections_1() const { return ___m_MaxDefConnections_1; }
	inline int32_t* get_address_of_m_MaxDefConnections_1() { return &___m_MaxDefConnections_1; }
	inline void set_m_MaxDefConnections_1(int32_t value)
	{
		___m_MaxDefConnections_1 = value;
	}

	inline static int32_t get_offset_of_m_SpecialConnections_2() { return static_cast<int32_t>(offsetof(HostTopology_t4059263395, ___m_SpecialConnections_2)); }
	inline List_1_t1351088715 * get_m_SpecialConnections_2() const { return ___m_SpecialConnections_2; }
	inline List_1_t1351088715 ** get_address_of_m_SpecialConnections_2() { return &___m_SpecialConnections_2; }
	inline void set_m_SpecialConnections_2(List_1_t1351088715 * value)
	{
		___m_SpecialConnections_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpecialConnections_2), value);
	}

	inline static int32_t get_offset_of_m_ReceivedMessagePoolSize_3() { return static_cast<int32_t>(offsetof(HostTopology_t4059263395, ___m_ReceivedMessagePoolSize_3)); }
	inline uint16_t get_m_ReceivedMessagePoolSize_3() const { return ___m_ReceivedMessagePoolSize_3; }
	inline uint16_t* get_address_of_m_ReceivedMessagePoolSize_3() { return &___m_ReceivedMessagePoolSize_3; }
	inline void set_m_ReceivedMessagePoolSize_3(uint16_t value)
	{
		___m_ReceivedMessagePoolSize_3 = value;
	}

	inline static int32_t get_offset_of_m_SentMessagePoolSize_4() { return static_cast<int32_t>(offsetof(HostTopology_t4059263395, ___m_SentMessagePoolSize_4)); }
	inline uint16_t get_m_SentMessagePoolSize_4() const { return ___m_SentMessagePoolSize_4; }
	inline uint16_t* get_address_of_m_SentMessagePoolSize_4() { return &___m_SentMessagePoolSize_4; }
	inline void set_m_SentMessagePoolSize_4(uint16_t value)
	{
		___m_SentMessagePoolSize_4 = value;
	}

	inline static int32_t get_offset_of_m_MessagePoolSizeGrowthFactor_5() { return static_cast<int32_t>(offsetof(HostTopology_t4059263395, ___m_MessagePoolSizeGrowthFactor_5)); }
	inline float get_m_MessagePoolSizeGrowthFactor_5() const { return ___m_MessagePoolSizeGrowthFactor_5; }
	inline float* get_address_of_m_MessagePoolSizeGrowthFactor_5() { return &___m_MessagePoolSizeGrowthFactor_5; }
	inline void set_m_MessagePoolSizeGrowthFactor_5(float value)
	{
		___m_MessagePoolSizeGrowthFactor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTTOPOLOGY_T4059263395_H
#ifndef NETWORKACCESSTOKEN_T320639760_H
#define NETWORKACCESSTOKEN_T320639760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Types.NetworkAccessToken
struct  NetworkAccessToken_t320639760  : public RuntimeObject
{
public:
	// System.Byte[] UnityEngine.Networking.Types.NetworkAccessToken::array
	ByteU5BU5D_t4116647657* ___array_1;

public:
	inline static int32_t get_offset_of_array_1() { return static_cast<int32_t>(offsetof(NetworkAccessToken_t320639760, ___array_1)); }
	inline ByteU5BU5D_t4116647657* get_array_1() const { return ___array_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_array_1() { return &___array_1; }
	inline void set_array_1(ByteU5BU5D_t4116647657* value)
	{
		___array_1 = value;
		Il2CppCodeGenWriteBarrier((&___array_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKACCESSTOKEN_T320639760_H
#ifndef UTILITY_T2761513741_H
#define UTILITY_T2761513741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Utility
struct  Utility_t2761513741  : public RuntimeObject
{
public:

public:
};

struct Utility_t2761513741_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken> UnityEngine.Networking.Utility::s_dictTokens
	Dictionary_2_t3502080855 * ___s_dictTokens_0;

public:
	inline static int32_t get_offset_of_s_dictTokens_0() { return static_cast<int32_t>(offsetof(Utility_t2761513741_StaticFields, ___s_dictTokens_0)); }
	inline Dictionary_2_t3502080855 * get_s_dictTokens_0() const { return ___s_dictTokens_0; }
	inline Dictionary_2_t3502080855 ** get_address_of_s_dictTokens_0() { return &___s_dictTokens_0; }
	inline void set_s_dictTokens_0(Dictionary_2_t3502080855 * value)
	{
		___s_dictTokens_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_dictTokens_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T2761513741_H
#ifndef JSON_T4111971237_H
#define JSON_T4111971237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MiniJSON.Json
struct  Json_t4111971237  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T4111971237_H
#ifndef PARSER_T4254629878_H
#define PARSER_T4254629878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MiniJSON.Json/Parser
struct  Parser_t4254629878  : public RuntimeObject
{
public:
	// System.IO.StringReader UnityEngine.Purchasing.MiniJSON.Json/Parser::json
	StringReader_t3465604688 * ___json_0;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(Parser_t4254629878, ___json_0)); }
	inline StringReader_t3465604688 * get_json_0() const { return ___json_0; }
	inline StringReader_t3465604688 ** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(StringReader_t3465604688 * value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier((&___json_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T4254629878_H
#ifndef SERIALIZER_T2325318676_H
#define SERIALIZER_T2325318676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MiniJSON.Json/Serializer
struct  Serializer_t2325318676  : public RuntimeObject
{
public:
	// System.Text.StringBuilder UnityEngine.Purchasing.MiniJSON.Json/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t2325318676, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T2325318676_H
#ifndef MINIJSONEXTENSIONS_T2206121176_H
#define MINIJSONEXTENSIONS_T2206121176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions
struct  MiniJsonExtensions_t2206121176  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSONEXTENSIONS_T2206121176_H
#ifndef MINIJSON_T2055087936_H
#define MINIJSON_T2055087936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MiniJson
struct  MiniJson_t2055087936  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSON_T2055087936_H
#ifndef REMOTESETTINGS_T1718627291_H
#define REMOTESETTINGS_T1718627291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t1718627291  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t1718627291_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t1027848393 * ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_t1264377477 * ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_t3050575418 * ___Completed_2;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t1027848393 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t1027848393 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t1027848393 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}

	inline static int32_t get_offset_of_BeforeFetchFromServer_1() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___BeforeFetchFromServer_1)); }
	inline Action_t1264377477 * get_BeforeFetchFromServer_1() const { return ___BeforeFetchFromServer_1; }
	inline Action_t1264377477 ** get_address_of_BeforeFetchFromServer_1() { return &___BeforeFetchFromServer_1; }
	inline void set_BeforeFetchFromServer_1(Action_t1264377477 * value)
	{
		___BeforeFetchFromServer_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeforeFetchFromServer_1), value);
	}

	inline static int32_t get_offset_of_Completed_2() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Completed_2)); }
	inline Action_3_t3050575418 * get_Completed_2() const { return ___Completed_2; }
	inline Action_3_t3050575418 ** get_address_of_Completed_2() { return &___Completed_2; }
	inline void set_Completed_2(Action_3_t3050575418 * value)
	{
		___Completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T1718627291_H
#ifndef XRSETTINGS_T335224468_H
#define XRSETTINGS_T335224468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.XRSettings
struct  XRSettings_t335224468  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSETTINGS_T335224468_H
#ifndef U24ARRAYTYPEU3D4_T1629360955_H
#define U24ARRAYTYPEU3D4_T1629360955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=4
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D4_t1629360955 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D4_t1629360955__padding[4];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D4_T1629360955_H
#ifndef ASSETLISTATTRIBUTE_T3126145472_H
#define ASSETLISTATTRIBUTE_T3126145472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.AssetListAttribute
struct  AssetListAttribute_t3126145472  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.AssetListAttribute::AutoPopulate
	bool ___AutoPopulate_0;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Tags
	String_t* ___Tags_1;
	// System.String Sirenix.OdinInspector.AssetListAttribute::LayerNames
	String_t* ___LayerNames_2;
	// System.String Sirenix.OdinInspector.AssetListAttribute::AssetNamePrefix
	String_t* ___AssetNamePrefix_3;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Path
	String_t* ___Path_4;
	// System.String Sirenix.OdinInspector.AssetListAttribute::CustomFilterMethod
	String_t* ___CustomFilterMethod_5;

public:
	inline static int32_t get_offset_of_AutoPopulate_0() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___AutoPopulate_0)); }
	inline bool get_AutoPopulate_0() const { return ___AutoPopulate_0; }
	inline bool* get_address_of_AutoPopulate_0() { return &___AutoPopulate_0; }
	inline void set_AutoPopulate_0(bool value)
	{
		___AutoPopulate_0 = value;
	}

	inline static int32_t get_offset_of_Tags_1() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___Tags_1)); }
	inline String_t* get_Tags_1() const { return ___Tags_1; }
	inline String_t** get_address_of_Tags_1() { return &___Tags_1; }
	inline void set_Tags_1(String_t* value)
	{
		___Tags_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tags_1), value);
	}

	inline static int32_t get_offset_of_LayerNames_2() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___LayerNames_2)); }
	inline String_t* get_LayerNames_2() const { return ___LayerNames_2; }
	inline String_t** get_address_of_LayerNames_2() { return &___LayerNames_2; }
	inline void set_LayerNames_2(String_t* value)
	{
		___LayerNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___LayerNames_2), value);
	}

	inline static int32_t get_offset_of_AssetNamePrefix_3() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___AssetNamePrefix_3)); }
	inline String_t* get_AssetNamePrefix_3() const { return ___AssetNamePrefix_3; }
	inline String_t** get_address_of_AssetNamePrefix_3() { return &___AssetNamePrefix_3; }
	inline void set_AssetNamePrefix_3(String_t* value)
	{
		___AssetNamePrefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___AssetNamePrefix_3), value);
	}

	inline static int32_t get_offset_of_Path_4() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___Path_4)); }
	inline String_t* get_Path_4() const { return ___Path_4; }
	inline String_t** get_address_of_Path_4() { return &___Path_4; }
	inline void set_Path_4(String_t* value)
	{
		___Path_4 = value;
		Il2CppCodeGenWriteBarrier((&___Path_4), value);
	}

	inline static int32_t get_offset_of_CustomFilterMethod_5() { return static_cast<int32_t>(offsetof(AssetListAttribute_t3126145472, ___CustomFilterMethod_5)); }
	inline String_t* get_CustomFilterMethod_5() const { return ___CustomFilterMethod_5; }
	inline String_t** get_address_of_CustomFilterMethod_5() { return &___CustomFilterMethod_5; }
	inline void set_CustomFilterMethod_5(String_t* value)
	{
		___CustomFilterMethod_5 = value;
		Il2CppCodeGenWriteBarrier((&___CustomFilterMethod_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLISTATTRIBUTE_T3126145472_H
#ifndef ASSETSONLYATTRIBUTE_T3835058913_H
#define ASSETSONLYATTRIBUTE_T3835058913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.AssetsOnlyAttribute
struct  AssetsOnlyAttribute_t3835058913  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETSONLYATTRIBUTE_T3835058913_H
#ifndef CUSTOMVALUEDRAWERATTRIBUTE_T4188368302_H
#define CUSTOMVALUEDRAWERATTRIBUTE_T4188368302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct  CustomValueDrawerAttribute_t4188368302  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.CustomValueDrawerAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(CustomValueDrawerAttribute_t4188368302, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVALUEDRAWERATTRIBUTE_T4188368302_H
#ifndef DISABLEININLINEEDITORSATTRIBUTE_T864950097_H
#define DISABLEININLINEEDITORSATTRIBUTE_T864950097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct  DisableInInlineEditorsAttribute_t864950097  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEININLINEEDITORSATTRIBUTE_T864950097_H
#ifndef DISABLEINNONPREFABSATTRIBUTE_T431643105_H
#define DISABLEINNONPREFABSATTRIBUTE_T431643105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct  DisableInNonPrefabsAttribute_t431643105  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINNONPREFABSATTRIBUTE_T431643105_H
#ifndef DISABLEINPREFABASSETSATTRIBUTE_T227636478_H
#define DISABLEINPREFABASSETSATTRIBUTE_T227636478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct  DisableInPrefabAssetsAttribute_t227636478  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPREFABASSETSATTRIBUTE_T227636478_H
#ifndef DISABLEINPREFABINSTANCESATTRIBUTE_T3405933113_H
#define DISABLEINPREFABINSTANCESATTRIBUTE_T3405933113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct  DisableInPrefabInstancesAttribute_t3405933113  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPREFABINSTANCESATTRIBUTE_T3405933113_H
#ifndef DISABLEINPREFABSATTRIBUTE_T3065057671_H
#define DISABLEINPREFABSATTRIBUTE_T3065057671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct  DisableInPrefabsAttribute_t3065057671  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPREFABSATTRIBUTE_T3065057671_H
#ifndef ENABLEGUIATTRIBUTE_T4210275809_H
#define ENABLEGUIATTRIBUTE_T4210275809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnableGUIAttribute
struct  EnableGUIAttribute_t4210275809  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEGUIATTRIBUTE_T4210275809_H
#ifndef ENUMPAGINGATTRIBUTE_T3087485058_H
#define ENUMPAGINGATTRIBUTE_T3087485058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnumPagingAttribute
struct  EnumPagingAttribute_t3087485058  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMPAGINGATTRIBUTE_T3087485058_H
#ifndef FILEPATHATTRIBUTE_T2750434984_H
#define FILEPATHATTRIBUTE_T2750434984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.FilePathAttribute
struct  FilePathAttribute_t2750434984  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FilePathAttribute::Extensions
	String_t* ___Extensions_1;
	// System.String Sirenix.OdinInspector.FilePathAttribute::ParentFolder
	String_t* ___ParentFolder_2;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireValidPath
	bool ___RequireValidPath_3;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireExistingPath
	bool ___RequireExistingPath_4;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::UseBackslashes
	bool ___UseBackslashes_5;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::<ReadOnly>k__BackingField
	bool ___U3CReadOnlyU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_Extensions_1() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___Extensions_1)); }
	inline String_t* get_Extensions_1() const { return ___Extensions_1; }
	inline String_t** get_address_of_Extensions_1() { return &___Extensions_1; }
	inline void set_Extensions_1(String_t* value)
	{
		___Extensions_1 = value;
		Il2CppCodeGenWriteBarrier((&___Extensions_1), value);
	}

	inline static int32_t get_offset_of_ParentFolder_2() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___ParentFolder_2)); }
	inline String_t* get_ParentFolder_2() const { return ___ParentFolder_2; }
	inline String_t** get_address_of_ParentFolder_2() { return &___ParentFolder_2; }
	inline void set_ParentFolder_2(String_t* value)
	{
		___ParentFolder_2 = value;
		Il2CppCodeGenWriteBarrier((&___ParentFolder_2), value);
	}

	inline static int32_t get_offset_of_RequireValidPath_3() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___RequireValidPath_3)); }
	inline bool get_RequireValidPath_3() const { return ___RequireValidPath_3; }
	inline bool* get_address_of_RequireValidPath_3() { return &___RequireValidPath_3; }
	inline void set_RequireValidPath_3(bool value)
	{
		___RequireValidPath_3 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_4() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___RequireExistingPath_4)); }
	inline bool get_RequireExistingPath_4() const { return ___RequireExistingPath_4; }
	inline bool* get_address_of_RequireExistingPath_4() { return &___RequireExistingPath_4; }
	inline void set_RequireExistingPath_4(bool value)
	{
		___RequireExistingPath_4 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_5() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___UseBackslashes_5)); }
	inline bool get_UseBackslashes_5() const { return ___UseBackslashes_5; }
	inline bool* get_address_of_UseBackslashes_5() { return &___UseBackslashes_5; }
	inline void set_UseBackslashes_5(bool value)
	{
		___UseBackslashes_5 = value;
	}

	inline static int32_t get_offset_of_U3CReadOnlyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FilePathAttribute_t2750434984, ___U3CReadOnlyU3Ek__BackingField_6)); }
	inline bool get_U3CReadOnlyU3Ek__BackingField_6() const { return ___U3CReadOnlyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CReadOnlyU3Ek__BackingField_6() { return &___U3CReadOnlyU3Ek__BackingField_6; }
	inline void set_U3CReadOnlyU3Ek__BackingField_6(bool value)
	{
		___U3CReadOnlyU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEPATHATTRIBUTE_T2750434984_H
#ifndef FOLDERPATHATTRIBUTE_T2308214518_H
#define FOLDERPATHATTRIBUTE_T2308214518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.FolderPathAttribute
struct  FolderPathAttribute_t2308214518  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FolderPathAttribute::ParentFolder
	String_t* ___ParentFolder_1;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireValidPath
	bool ___RequireValidPath_2;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireExistingPath
	bool ___RequireExistingPath_3;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::UseBackslashes
	bool ___UseBackslashes_4;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_ParentFolder_1() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___ParentFolder_1)); }
	inline String_t* get_ParentFolder_1() const { return ___ParentFolder_1; }
	inline String_t** get_address_of_ParentFolder_1() { return &___ParentFolder_1; }
	inline void set_ParentFolder_1(String_t* value)
	{
		___ParentFolder_1 = value;
		Il2CppCodeGenWriteBarrier((&___ParentFolder_1), value);
	}

	inline static int32_t get_offset_of_RequireValidPath_2() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___RequireValidPath_2)); }
	inline bool get_RequireValidPath_2() const { return ___RequireValidPath_2; }
	inline bool* get_address_of_RequireValidPath_2() { return &___RequireValidPath_2; }
	inline void set_RequireValidPath_2(bool value)
	{
		___RequireValidPath_2 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_3() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___RequireExistingPath_3)); }
	inline bool get_RequireExistingPath_3() const { return ___RequireExistingPath_3; }
	inline bool* get_address_of_RequireExistingPath_3() { return &___RequireExistingPath_3; }
	inline void set_RequireExistingPath_3(bool value)
	{
		___RequireExistingPath_3 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_4() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t2308214518, ___UseBackslashes_4)); }
	inline bool get_UseBackslashes_4() const { return ___UseBackslashes_4; }
	inline bool* get_address_of_UseBackslashes_4() { return &___UseBackslashes_4; }
	inline void set_UseBackslashes_4(bool value)
	{
		___UseBackslashes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLDERPATHATTRIBUTE_T2308214518_H
#ifndef HIDEININLINEEDITORSATTRIBUTE_T2643132944_H
#define HIDEININLINEEDITORSATTRIBUTE_T2643132944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct  HideInInlineEditorsAttribute_t2643132944  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEININLINEEDITORSATTRIBUTE_T2643132944_H
#ifndef HIDEINNONPREFABSATTRIBUTE_T2267791284_H
#define HIDEINNONPREFABSATTRIBUTE_T2267791284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct  HideInNonPrefabsAttribute_t2267791284  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINNONPREFABSATTRIBUTE_T2267791284_H
#ifndef HIDEINPREFABASSETSATTRIBUTE_T3787194334_H
#define HIDEINPREFABASSETSATTRIBUTE_T3787194334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct  HideInPrefabAssetsAttribute_t3787194334  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPREFABASSETSATTRIBUTE_T3787194334_H
#ifndef HIDEINPREFABINSTANCESATTRIBUTE_T271927100_H
#define HIDEINPREFABINSTANCESATTRIBUTE_T271927100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct  HideInPrefabInstancesAttribute_t271927100  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPREFABINSTANCESATTRIBUTE_T271927100_H
#ifndef HIDEINPREFABSATTRIBUTE_T2196881499_H
#define HIDEINPREFABSATTRIBUTE_T2196881499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPrefabsAttribute
struct  HideInPrefabsAttribute_t2196881499  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPREFABSATTRIBUTE_T2196881499_H
#ifndef HIDEINTABLESATTRIBUTE_T3879405200_H
#define HIDEINTABLESATTRIBUTE_T3879405200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInTablesAttribute
struct  HideInTablesAttribute_t3879405200  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINTABLESATTRIBUTE_T3879405200_H
#ifndef HIDENETWORKBEHAVIOURFIELDSATTRIBUTE_T1543820585_H
#define HIDENETWORKBEHAVIOURFIELDSATTRIBUTE_T1543820585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct  HideNetworkBehaviourFieldsAttribute_t1543820585  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDENETWORKBEHAVIOURFIELDSATTRIBUTE_T1543820585_H
#ifndef INCLUDEMYATTRIBUTESATTRIBUTE_T2353296197_H
#define INCLUDEMYATTRIBUTESATTRIBUTE_T2353296197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct  IncludeMyAttributesAttribute_t2353296197  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCLUDEMYATTRIBUTESATTRIBUTE_T2353296197_H
#ifndef INLINEPROPERTYATTRIBUTE_T3336736111_H
#define INLINEPROPERTYATTRIBUTE_T3336736111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlinePropertyAttribute
struct  InlinePropertyAttribute_t3336736111  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.InlinePropertyAttribute::LabelWidth
	int32_t ___LabelWidth_0;

public:
	inline static int32_t get_offset_of_LabelWidth_0() { return static_cast<int32_t>(offsetof(InlinePropertyAttribute_t3336736111, ___LabelWidth_0)); }
	inline int32_t get_LabelWidth_0() const { return ___LabelWidth_0; }
	inline int32_t* get_address_of_LabelWidth_0() { return &___LabelWidth_0; }
	inline void set_LabelWidth_0(int32_t value)
	{
		___LabelWidth_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEPROPERTYATTRIBUTE_T3336736111_H
#ifndef LABELWIDTHATTRIBUTE_T2595655695_H
#define LABELWIDTHATTRIBUTE_T2595655695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.LabelWidthAttribute
struct  LabelWidthAttribute_t2595655695  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.LabelWidthAttribute::Width
	float ___Width_0;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(LabelWidthAttribute_t2595655695, ___Width_0)); }
	inline float get_Width_0() const { return ___Width_0; }
	inline float* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(float value)
	{
		___Width_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELWIDTHATTRIBUTE_T2595655695_H
#ifndef PROPERTYGROUPATTRIBUTE_T2009328757_H
#define PROPERTYGROUPATTRIBUTE_T2009328757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyGroupAttribute
struct  PropertyGroupAttribute_t2009328757  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupID
	String_t* ___GroupID_0;
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupName
	String_t* ___GroupName_1;
	// System.Int32 Sirenix.OdinInspector.PropertyGroupAttribute::Order
	int32_t ___Order_2;

public:
	inline static int32_t get_offset_of_GroupID_0() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___GroupID_0)); }
	inline String_t* get_GroupID_0() const { return ___GroupID_0; }
	inline String_t** get_address_of_GroupID_0() { return &___GroupID_0; }
	inline void set_GroupID_0(String_t* value)
	{
		___GroupID_0 = value;
		Il2CppCodeGenWriteBarrier((&___GroupID_0), value);
	}

	inline static int32_t get_offset_of_GroupName_1() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___GroupName_1)); }
	inline String_t* get_GroupName_1() const { return ___GroupName_1; }
	inline String_t** get_address_of_GroupName_1() { return &___GroupName_1; }
	inline void set_GroupName_1(String_t* value)
	{
		___GroupName_1 = value;
		Il2CppCodeGenWriteBarrier((&___GroupName_1), value);
	}

	inline static int32_t get_offset_of_Order_2() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___Order_2)); }
	inline int32_t get_Order_2() const { return ___Order_2; }
	inline int32_t* get_address_of_Order_2() { return &___Order_2; }
	inline void set_Order_2(int32_t value)
	{
		___Order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYGROUPATTRIBUTE_T2009328757_H
#ifndef PROPERTYRANGEATTRIBUTE_T3334671474_H
#define PROPERTYRANGEATTRIBUTE_T3334671474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyRangeAttribute
struct  PropertyRangeAttribute_t3334671474  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MaxMember
	String_t* ___MaxMember_3;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinMember_2), value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t3334671474, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___MaxMember_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYRANGEATTRIBUTE_T3334671474_H
#ifndef PROPERTYSPACEATTRIBUTE_T209179749_H
#define PROPERTYSPACEATTRIBUTE_T209179749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertySpaceAttribute
struct  PropertySpaceAttribute_t209179749  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceBefore
	float ___SpaceBefore_0;
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceAfter
	float ___SpaceAfter_1;

public:
	inline static int32_t get_offset_of_SpaceBefore_0() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_t209179749, ___SpaceBefore_0)); }
	inline float get_SpaceBefore_0() const { return ___SpaceBefore_0; }
	inline float* get_address_of_SpaceBefore_0() { return &___SpaceBefore_0; }
	inline void set_SpaceBefore_0(float value)
	{
		___SpaceBefore_0 = value;
	}

	inline static int32_t get_offset_of_SpaceAfter_1() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_t209179749, ___SpaceAfter_1)); }
	inline float get_SpaceAfter_1() const { return ___SpaceAfter_1; }
	inline float* get_address_of_SpaceAfter_1() { return &___SpaceAfter_1; }
	inline void set_SpaceAfter_1(float value)
	{
		___SpaceAfter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYSPACEATTRIBUTE_T209179749_H
#ifndef REGISTERATTRIBUTEATTRIBUTE_T317614997_H
#define REGISTERATTRIBUTEATTRIBUTE_T317614997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.RegisterAttributeAttribute
struct  RegisterAttributeAttribute_t317614997  : public Attribute_t861562559
{
public:
	// System.Type Sirenix.OdinInspector.RegisterAttributeAttribute::AttributeType
	Type_t * ___AttributeType_0;
	// System.String[] Sirenix.OdinInspector.RegisterAttributeAttribute::Categories
	StringU5BU5D_t1281789340* ___Categories_1;

public:
	inline static int32_t get_offset_of_AttributeType_0() { return static_cast<int32_t>(offsetof(RegisterAttributeAttribute_t317614997, ___AttributeType_0)); }
	inline Type_t * get_AttributeType_0() const { return ___AttributeType_0; }
	inline Type_t ** get_address_of_AttributeType_0() { return &___AttributeType_0; }
	inline void set_AttributeType_0(Type_t * value)
	{
		___AttributeType_0 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeType_0), value);
	}

	inline static int32_t get_offset_of_Categories_1() { return static_cast<int32_t>(offsetof(RegisterAttributeAttribute_t317614997, ___Categories_1)); }
	inline StringU5BU5D_t1281789340* get_Categories_1() const { return ___Categories_1; }
	inline StringU5BU5D_t1281789340** get_address_of_Categories_1() { return &___Categories_1; }
	inline void set_Categories_1(StringU5BU5D_t1281789340* value)
	{
		___Categories_1 = value;
		Il2CppCodeGenWriteBarrier((&___Categories_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERATTRIBUTEATTRIBUTE_T317614997_H
#ifndef SHOWININLINEEDITORSATTRIBUTE_T2931332120_H
#define SHOWININLINEEDITORSATTRIBUTE_T2931332120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct  ShowInInlineEditorsAttribute_t2931332120  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWININLINEEDITORSATTRIBUTE_T2931332120_H
#ifndef SHOWININSPECTORATTRIBUTE_T2100412880_H
#define SHOWININSPECTORATTRIBUTE_T2100412880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowInInspectorAttribute
struct  ShowInInspectorAttribute_t2100412880  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWININSPECTORATTRIBUTE_T2100412880_H
#ifndef SHOWPROPERTYRESOLVERATTRIBUTE_T357032921_H
#define SHOWPROPERTYRESOLVERATTRIBUTE_T357032921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct  ShowPropertyResolverAttribute_t357032921  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWPROPERTYRESOLVERATTRIBUTE_T357032921_H
#ifndef SUFFIXLABELATTRIBUTE_T126424634_H
#define SUFFIXLABELATTRIBUTE_T126424634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SuffixLabelAttribute
struct  SuffixLabelAttribute_t126424634  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.SuffixLabelAttribute::Label
	String_t* ___Label_0;
	// System.Boolean Sirenix.OdinInspector.SuffixLabelAttribute::Overlay
	bool ___Overlay_1;

public:
	inline static int32_t get_offset_of_Label_0() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t126424634, ___Label_0)); }
	inline String_t* get_Label_0() const { return ___Label_0; }
	inline String_t** get_address_of_Label_0() { return &___Label_0; }
	inline void set_Label_0(String_t* value)
	{
		___Label_0 = value;
		Il2CppCodeGenWriteBarrier((&___Label_0), value);
	}

	inline static int32_t get_offset_of_Overlay_1() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t126424634, ___Overlay_1)); }
	inline bool get_Overlay_1() const { return ___Overlay_1; }
	inline bool* get_address_of_Overlay_1() { return &___Overlay_1; }
	inline void set_Overlay_1(bool value)
	{
		___Overlay_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUFFIXLABELATTRIBUTE_T126424634_H
#ifndef TABLECOLUMNWIDTHATTRIBUTE_T1272758069_H
#define TABLECOLUMNWIDTHATTRIBUTE_T1272758069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TableColumnWidthAttribute
struct  TableColumnWidthAttribute_t1272758069  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.TableColumnWidthAttribute::Width
	int32_t ___Width_0;
	// System.Boolean Sirenix.OdinInspector.TableColumnWidthAttribute::Resizable
	bool ___Resizable_1;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t1272758069, ___Width_0)); }
	inline int32_t get_Width_0() const { return ___Width_0; }
	inline int32_t* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(int32_t value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Resizable_1() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t1272758069, ___Resizable_1)); }
	inline bool get_Resizable_1() const { return ___Resizable_1; }
	inline bool* get_address_of_Resizable_1() { return &___Resizable_1; }
	inline void set_Resizable_1(bool value)
	{
		___Resizable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLECOLUMNWIDTHATTRIBUTE_T1272758069_H
#ifndef TABLELISTATTRIBUTE_T4098662806_H
#define TABLELISTATTRIBUTE_T4098662806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TableListAttribute
struct  TableListAttribute_t4098662806  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::NumberOfItemsPerPage
	int32_t ___NumberOfItemsPerPage_0;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::IsReadOnly
	bool ___IsReadOnly_1;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::DefaultMinColumnWidth
	int32_t ___DefaultMinColumnWidth_2;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::ShowIndexLabels
	bool ___ShowIndexLabels_3;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::DrawScrollView
	bool ___DrawScrollView_4;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MinScrollViewHeight
	int32_t ___MinScrollViewHeight_5;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MaxScrollViewHeight
	int32_t ___MaxScrollViewHeight_6;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::AlwaysExpanded
	bool ___AlwaysExpanded_7;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::HideToolbar
	bool ___HideToolbar_8;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::CellPadding
	int32_t ___CellPadding_9;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPagingHasValue
	bool ___showPagingHasValue_10;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPaging
	bool ___showPaging_11;

public:
	inline static int32_t get_offset_of_NumberOfItemsPerPage_0() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___NumberOfItemsPerPage_0)); }
	inline int32_t get_NumberOfItemsPerPage_0() const { return ___NumberOfItemsPerPage_0; }
	inline int32_t* get_address_of_NumberOfItemsPerPage_0() { return &___NumberOfItemsPerPage_0; }
	inline void set_NumberOfItemsPerPage_0(int32_t value)
	{
		___NumberOfItemsPerPage_0 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_1() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___IsReadOnly_1)); }
	inline bool get_IsReadOnly_1() const { return ___IsReadOnly_1; }
	inline bool* get_address_of_IsReadOnly_1() { return &___IsReadOnly_1; }
	inline void set_IsReadOnly_1(bool value)
	{
		___IsReadOnly_1 = value;
	}

	inline static int32_t get_offset_of_DefaultMinColumnWidth_2() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___DefaultMinColumnWidth_2)); }
	inline int32_t get_DefaultMinColumnWidth_2() const { return ___DefaultMinColumnWidth_2; }
	inline int32_t* get_address_of_DefaultMinColumnWidth_2() { return &___DefaultMinColumnWidth_2; }
	inline void set_DefaultMinColumnWidth_2(int32_t value)
	{
		___DefaultMinColumnWidth_2 = value;
	}

	inline static int32_t get_offset_of_ShowIndexLabels_3() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___ShowIndexLabels_3)); }
	inline bool get_ShowIndexLabels_3() const { return ___ShowIndexLabels_3; }
	inline bool* get_address_of_ShowIndexLabels_3() { return &___ShowIndexLabels_3; }
	inline void set_ShowIndexLabels_3(bool value)
	{
		___ShowIndexLabels_3 = value;
	}

	inline static int32_t get_offset_of_DrawScrollView_4() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___DrawScrollView_4)); }
	inline bool get_DrawScrollView_4() const { return ___DrawScrollView_4; }
	inline bool* get_address_of_DrawScrollView_4() { return &___DrawScrollView_4; }
	inline void set_DrawScrollView_4(bool value)
	{
		___DrawScrollView_4 = value;
	}

	inline static int32_t get_offset_of_MinScrollViewHeight_5() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___MinScrollViewHeight_5)); }
	inline int32_t get_MinScrollViewHeight_5() const { return ___MinScrollViewHeight_5; }
	inline int32_t* get_address_of_MinScrollViewHeight_5() { return &___MinScrollViewHeight_5; }
	inline void set_MinScrollViewHeight_5(int32_t value)
	{
		___MinScrollViewHeight_5 = value;
	}

	inline static int32_t get_offset_of_MaxScrollViewHeight_6() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___MaxScrollViewHeight_6)); }
	inline int32_t get_MaxScrollViewHeight_6() const { return ___MaxScrollViewHeight_6; }
	inline int32_t* get_address_of_MaxScrollViewHeight_6() { return &___MaxScrollViewHeight_6; }
	inline void set_MaxScrollViewHeight_6(int32_t value)
	{
		___MaxScrollViewHeight_6 = value;
	}

	inline static int32_t get_offset_of_AlwaysExpanded_7() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___AlwaysExpanded_7)); }
	inline bool get_AlwaysExpanded_7() const { return ___AlwaysExpanded_7; }
	inline bool* get_address_of_AlwaysExpanded_7() { return &___AlwaysExpanded_7; }
	inline void set_AlwaysExpanded_7(bool value)
	{
		___AlwaysExpanded_7 = value;
	}

	inline static int32_t get_offset_of_HideToolbar_8() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___HideToolbar_8)); }
	inline bool get_HideToolbar_8() const { return ___HideToolbar_8; }
	inline bool* get_address_of_HideToolbar_8() { return &___HideToolbar_8; }
	inline void set_HideToolbar_8(bool value)
	{
		___HideToolbar_8 = value;
	}

	inline static int32_t get_offset_of_CellPadding_9() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___CellPadding_9)); }
	inline int32_t get_CellPadding_9() const { return ___CellPadding_9; }
	inline int32_t* get_address_of_CellPadding_9() { return &___CellPadding_9; }
	inline void set_CellPadding_9(int32_t value)
	{
		___CellPadding_9 = value;
	}

	inline static int32_t get_offset_of_showPagingHasValue_10() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___showPagingHasValue_10)); }
	inline bool get_showPagingHasValue_10() const { return ___showPagingHasValue_10; }
	inline bool* get_address_of_showPagingHasValue_10() { return &___showPagingHasValue_10; }
	inline void set_showPagingHasValue_10(bool value)
	{
		___showPagingHasValue_10 = value;
	}

	inline static int32_t get_offset_of_showPaging_11() { return static_cast<int32_t>(offsetof(TableListAttribute_t4098662806, ___showPaging_11)); }
	inline bool get_showPaging_11() const { return ___showPaging_11; }
	inline bool* get_address_of_showPaging_11() { return &___showPaging_11; }
	inline void set_showPaging_11(bool value)
	{
		___showPaging_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLELISTATTRIBUTE_T4098662806_H
#ifndef TABLEMATRIXATTRIBUTE_T391949620_H
#define TABLEMATRIXATTRIBUTE_T391949620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TableMatrixAttribute
struct  TableMatrixAttribute_t391949620  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::IsReadOnly
	bool ___IsReadOnly_0;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::ResizableColumns
	bool ___ResizableColumns_1;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::VerticalTitle
	String_t* ___VerticalTitle_2;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::HorizontalTitle
	String_t* ___HorizontalTitle_3;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::DrawElementMethod
	String_t* ___DrawElementMethod_4;
	// System.Int32 Sirenix.OdinInspector.TableMatrixAttribute::RowHeight
	int32_t ___RowHeight_5;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::SquareCells
	bool ___SquareCells_6;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideColumnIndices
	bool ___HideColumnIndices_7;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideRowIndices
	bool ___HideRowIndices_8;

public:
	inline static int32_t get_offset_of_IsReadOnly_0() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___IsReadOnly_0)); }
	inline bool get_IsReadOnly_0() const { return ___IsReadOnly_0; }
	inline bool* get_address_of_IsReadOnly_0() { return &___IsReadOnly_0; }
	inline void set_IsReadOnly_0(bool value)
	{
		___IsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_ResizableColumns_1() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___ResizableColumns_1)); }
	inline bool get_ResizableColumns_1() const { return ___ResizableColumns_1; }
	inline bool* get_address_of_ResizableColumns_1() { return &___ResizableColumns_1; }
	inline void set_ResizableColumns_1(bool value)
	{
		___ResizableColumns_1 = value;
	}

	inline static int32_t get_offset_of_VerticalTitle_2() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___VerticalTitle_2)); }
	inline String_t* get_VerticalTitle_2() const { return ___VerticalTitle_2; }
	inline String_t** get_address_of_VerticalTitle_2() { return &___VerticalTitle_2; }
	inline void set_VerticalTitle_2(String_t* value)
	{
		___VerticalTitle_2 = value;
		Il2CppCodeGenWriteBarrier((&___VerticalTitle_2), value);
	}

	inline static int32_t get_offset_of_HorizontalTitle_3() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___HorizontalTitle_3)); }
	inline String_t* get_HorizontalTitle_3() const { return ___HorizontalTitle_3; }
	inline String_t** get_address_of_HorizontalTitle_3() { return &___HorizontalTitle_3; }
	inline void set_HorizontalTitle_3(String_t* value)
	{
		___HorizontalTitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___HorizontalTitle_3), value);
	}

	inline static int32_t get_offset_of_DrawElementMethod_4() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___DrawElementMethod_4)); }
	inline String_t* get_DrawElementMethod_4() const { return ___DrawElementMethod_4; }
	inline String_t** get_address_of_DrawElementMethod_4() { return &___DrawElementMethod_4; }
	inline void set_DrawElementMethod_4(String_t* value)
	{
		___DrawElementMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___DrawElementMethod_4), value);
	}

	inline static int32_t get_offset_of_RowHeight_5() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___RowHeight_5)); }
	inline int32_t get_RowHeight_5() const { return ___RowHeight_5; }
	inline int32_t* get_address_of_RowHeight_5() { return &___RowHeight_5; }
	inline void set_RowHeight_5(int32_t value)
	{
		___RowHeight_5 = value;
	}

	inline static int32_t get_offset_of_SquareCells_6() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___SquareCells_6)); }
	inline bool get_SquareCells_6() const { return ___SquareCells_6; }
	inline bool* get_address_of_SquareCells_6() { return &___SquareCells_6; }
	inline void set_SquareCells_6(bool value)
	{
		___SquareCells_6 = value;
	}

	inline static int32_t get_offset_of_HideColumnIndices_7() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___HideColumnIndices_7)); }
	inline bool get_HideColumnIndices_7() const { return ___HideColumnIndices_7; }
	inline bool* get_address_of_HideColumnIndices_7() { return &___HideColumnIndices_7; }
	inline void set_HideColumnIndices_7(bool value)
	{
		___HideColumnIndices_7 = value;
	}

	inline static int32_t get_offset_of_HideRowIndices_8() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_t391949620, ___HideRowIndices_8)); }
	inline bool get_HideRowIndices_8() const { return ___HideRowIndices_8; }
	inline bool* get_address_of_HideRowIndices_8() { return &___HideRowIndices_8; }
	inline void set_HideRowIndices_8(bool value)
	{
		___HideRowIndices_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEMATRIXATTRIBUTE_T391949620_H
#ifndef TYPEFILTERATTRIBUTE_T642666696_H
#define TYPEFILTERATTRIBUTE_T642666696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TypeFilterAttribute
struct  TypeFilterAttribute_t642666696  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::MemberName
	String_t* ___MemberName_0;
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::DropdownTitle
	String_t* ___DropdownTitle_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t642666696, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_DropdownTitle_1() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t642666696, ___DropdownTitle_1)); }
	inline String_t* get_DropdownTitle_1() const { return ___DropdownTitle_1; }
	inline String_t** get_address_of_DropdownTitle_1() { return &___DropdownTitle_1; }
	inline void set_DropdownTitle_1(String_t* value)
	{
		___DropdownTitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___DropdownTitle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTERATTRIBUTE_T642666696_H
#ifndef TYPEINFOBOXATTRIBUTE_T3273027527_H
#define TYPEINFOBOXATTRIBUTE_T3273027527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct  TypeInfoBoxAttribute_t3273027527  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.TypeInfoBoxAttribute::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(TypeInfoBoxAttribute_t3273027527, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFOBOXATTRIBUTE_T3273027527_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=4 <PrivateImplementationDetails>::$field-95D7E9C7483D5AF10DF20044FCD3E580073E1D4B
	U24ArrayTypeU3D4_t1629360955  ___U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0)); }
	inline U24ArrayTypeU3D4_t1629360955  get_U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0() const { return ___U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0; }
	inline U24ArrayTypeU3D4_t1629360955 * get_address_of_U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0() { return &___U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0; }
	inline void set_U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0(U24ArrayTypeU3D4_t1629360955  value)
	{
		___U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef BOXGROUPATTRIBUTE_T1828532190_H
#define BOXGROUPATTRIBUTE_T1828532190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.BoxGroupAttribute
struct  BoxGroupAttribute_t1828532190  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::ShowLabel
	bool ___ShowLabel_3;
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::CenterLabel
	bool ___CenterLabel_4;

public:
	inline static int32_t get_offset_of_ShowLabel_3() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t1828532190, ___ShowLabel_3)); }
	inline bool get_ShowLabel_3() const { return ___ShowLabel_3; }
	inline bool* get_address_of_ShowLabel_3() { return &___ShowLabel_3; }
	inline void set_ShowLabel_3(bool value)
	{
		___ShowLabel_3 = value;
	}

	inline static int32_t get_offset_of_CenterLabel_4() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t1828532190, ___CenterLabel_4)); }
	inline bool get_CenterLabel_4() const { return ___CenterLabel_4; }
	inline bool* get_address_of_CenterLabel_4() { return &___CenterLabel_4; }
	inline void set_CenterLabel_4(bool value)
	{
		___CenterLabel_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXGROUPATTRIBUTE_T1828532190_H
#ifndef BUTTONGROUPATTRIBUTE_T2757208248_H
#define BUTTONGROUPATTRIBUTE_T2757208248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonGroupAttribute
struct  ButtonGroupAttribute_t2757208248  : public PropertyGroupAttribute_t2009328757
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONGROUPATTRIBUTE_T2757208248_H
#ifndef BUTTONSIZES_T675943090_H
#define BUTTONSIZES_T675943090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonSizes
struct  ButtonSizes_t675943090 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonSizes_t675943090, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSIZES_T675943090_H
#ifndef BUTTONSTYLE_T3769135257_H
#define BUTTONSTYLE_T3769135257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonStyle
struct  ButtonStyle_t3769135257 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStyle_t3769135257, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTYLE_T3769135257_H
#ifndef INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#define INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorObjectFieldModes
struct  InlineEditorObjectFieldModes_t1823869811 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorObjectFieldModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorObjectFieldModes_t1823869811, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#ifndef OBJECTFIELDALIGNMENT_T2976484322_H
#define OBJECTFIELDALIGNMENT_T2976484322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ObjectFieldAlignment
struct  ObjectFieldAlignment_t2976484322 
{
public:
	// System.Int32 Sirenix.OdinInspector.ObjectFieldAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectFieldAlignment_t2976484322, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTFIELDALIGNMENT_T2976484322_H
#ifndef VERTICALGROUPATTRIBUTE_T4076833965_H
#define VERTICALGROUPATTRIBUTE_T4076833965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.VerticalGroupAttribute
struct  VerticalGroupAttribute_t4076833965  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingTop
	float ___PaddingTop_3;
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingBottom
	float ___PaddingBottom_4;

public:
	inline static int32_t get_offset_of_PaddingTop_3() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_t4076833965, ___PaddingTop_3)); }
	inline float get_PaddingTop_3() const { return ___PaddingTop_3; }
	inline float* get_address_of_PaddingTop_3() { return &___PaddingTop_3; }
	inline void set_PaddingTop_3(float value)
	{
		___PaddingTop_3 = value;
	}

	inline static int32_t get_offset_of_PaddingBottom_4() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_t4076833965, ___PaddingBottom_4)); }
	inline float get_PaddingBottom_4() const { return ___PaddingBottom_4; }
	inline float* get_address_of_PaddingBottom_4() { return &___PaddingBottom_4; }
	inline void set_PaddingBottom_4(float value)
	{
		___PaddingBottom_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALGROUPATTRIBUTE_T4076833965_H
#ifndef ATTRIBUTETARGETS_T1784037988_H
#define ATTRIBUTETARGETS_T1784037988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AttributeTargets
struct  AttributeTargets_t1784037988 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t1784037988, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETS_T1784037988_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef ANALYTICSRESULT_T2273004240_H
#define ANALYTICSRESULT_T2273004240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsResult
struct  AnalyticsResult_t2273004240 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyticsResult_t2273004240, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSRESULT_T2273004240_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef CUSTOMEVENTDATA_T317522481_H
#define CUSTOMEVENTDATA_T317522481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.CustomEventData
struct  CustomEventData_t317522481  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Analytics.CustomEventData::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CustomEventData_t317522481, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.CustomEventData
struct CustomEventData_t317522481_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Analytics.CustomEventData
struct CustomEventData_t317522481_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // CUSTOMEVENTDATA_T317522481_H
#ifndef UNITYANALYTICSHANDLER_T3011359618_H
#define UNITYANALYTICSHANDLER_T3011359618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.UnityAnalyticsHandler
struct  UnityAnalyticsHandler_t3011359618  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Analytics.UnityAnalyticsHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityAnalyticsHandler_t3011359618, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.UnityAnalyticsHandler
struct UnityAnalyticsHandler_t3011359618_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Analytics.UnityAnalyticsHandler
struct UnityAnalyticsHandler_t3011359618_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UNITYANALYTICSHANDLER_T3011359618_H
#ifndef CONNECTIONACKSTYPE_T3955378167_H
#define CONNECTIONACKSTYPE_T3955378167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ConnectionAcksType
struct  ConnectionAcksType_t3955378167 
{
public:
	// System.Int32 UnityEngine.Networking.ConnectionAcksType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionAcksType_t3955378167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONACKSTYPE_T3955378167_H
#ifndef NETWORKERROR_T2038193525_H
#define NETWORKERROR_T2038193525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkError
struct  NetworkError_t2038193525 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkError_t2038193525, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKERROR_T2038193525_H
#ifndef NETWORKEVENTTYPE_T3383948383_H
#define NETWORKEVENTTYPE_T3383948383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkEventType
struct  NetworkEventType_t3383948383 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkEventType_t3383948383, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKEVENTTYPE_T3383948383_H
#ifndef QOSTYPE_T3566496866_H
#define QOSTYPE_T3566496866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.QosType
struct  QosType_t3566496866 
{
public:
	// System.Int32 UnityEngine.Networking.QosType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QosType_t3566496866, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QOSTYPE_T3566496866_H
#ifndef REACTORMODEL_T89779108_H
#define REACTORMODEL_T89779108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ReactorModel
struct  ReactorModel_t89779108 
{
public:
	// System.Int32 UnityEngine.Networking.ReactorModel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReactorModel_t89779108, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTORMODEL_T89779108_H
#ifndef APPID_T663952513_H
#define APPID_T663952513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Types.AppID
struct  AppID_t663952513 
{
public:
	// System.UInt64 UnityEngine.Networking.Types.AppID::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppID_t663952513, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPID_T663952513_H
#ifndef HOSTPRIORITY_T1616615738_H
#define HOSTPRIORITY_T1616615738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Types.HostPriority
struct  HostPriority_t1616615738 
{
public:
	// System.Int32 UnityEngine.Networking.Types.HostPriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HostPriority_t1616615738, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTPRIORITY_T1616615738_H
#ifndef NETWORKID_T4216585621_H
#define NETWORKID_T4216585621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Types.NetworkID
struct  NetworkID_t4216585621 
{
public:
	// System.UInt64 UnityEngine.Networking.Types.NetworkID::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkID_t4216585621, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKID_T4216585621_H
#ifndef NODEID_T2347816311_H
#define NODEID_T2347816311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Types.NodeID
struct  NodeID_t2347816311 
{
public:
	// System.UInt16 UnityEngine.Networking.Types.NodeID::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NodeID_t2347816311, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEID_T2347816311_H
#ifndef SOURCEID_T1070216020_H
#define SOURCEID_T1070216020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Types.SourceID
struct  SourceID_t1070216020 
{
public:
	// System.UInt64 UnityEngine.Networking.Types.SourceID::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SourceID_t1070216020, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOURCEID_T1070216020_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TOKEN_T4214352417_H
#define TOKEN_T4214352417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t4214352417 
{
public:
	// System.Int32 UnityEngine.Purchasing.MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t4214352417, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T4214352417_H
#ifndef REMOTECONFIGSETTINGS_T1247263429_H
#define REMOTECONFIGSETTINGS_T1247263429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteConfigSettings
struct  RemoteConfigSettings_t1247263429  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_t269755560 * ___Updated_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t1247263429, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_Updated_1() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t1247263429, ___Updated_1)); }
	inline Action_1_t269755560 * get_Updated_1() const { return ___Updated_1; }
	inline Action_1_t269755560 ** get_address_of_Updated_1() { return &___Updated_1; }
	inline void set_Updated_1(Action_1_t269755560 * value)
	{
		___Updated_1 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t1247263429_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t1247263429_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
#endif // REMOTECONFIGSETTINGS_T1247263429_H
#ifndef TEXTALIGNMENT_T822270402_H
#define TEXTALIGNMENT_T822270402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAlignment
struct  TextAlignment_t822270402 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_t822270402, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENT_T822270402_H
#ifndef GAMEVIEWRENDERMODE_T3810192523_H
#define GAMEVIEWRENDERMODE_T3810192523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.GameViewRenderMode
struct  GameViewRenderMode_t3810192523 
{
public:
	// System.Int32 UnityEngine.XR.GameViewRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameViewRenderMode_t3810192523, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEVIEWRENDERMODE_T3810192523_H
#ifndef ATTRIBUTETARGETFLAGS_T870309191_H
#define ATTRIBUTETARGETFLAGS_T870309191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.AttributeTargetFlags
struct  AttributeTargetFlags_t870309191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETARGETFLAGS_T870309191_H
#ifndef BUTTONATTRIBUTE_T3418195108_H
#define BUTTONATTRIBUTE_T3418195108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonAttribute
struct  ButtonAttribute_t3418195108  : public ShowInInspectorAttribute_t2100412880
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonAttribute::ButtonHeight
	int32_t ___ButtonHeight_0;
	// System.String Sirenix.OdinInspector.ButtonAttribute::Name
	String_t* ___Name_1;
	// Sirenix.OdinInspector.ButtonStyle Sirenix.OdinInspector.ButtonAttribute::Style
	int32_t ___Style_2;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::Expanded
	bool ___Expanded_3;

public:
	inline static int32_t get_offset_of_ButtonHeight_0() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___ButtonHeight_0)); }
	inline int32_t get_ButtonHeight_0() const { return ___ButtonHeight_0; }
	inline int32_t* get_address_of_ButtonHeight_0() { return &___ButtonHeight_0; }
	inline void set_ButtonHeight_0(int32_t value)
	{
		___ButtonHeight_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Style_2() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___Style_2)); }
	inline int32_t get_Style_2() const { return ___Style_2; }
	inline int32_t* get_address_of_Style_2() { return &___Style_2; }
	inline void set_Style_2(int32_t value)
	{
		___Style_2 = value;
	}

	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(ButtonAttribute_t3418195108, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONATTRIBUTE_T3418195108_H
#ifndef PREVIEWFIELDATTRIBUTE_T2856725314_H
#define PREVIEWFIELDATTRIBUTE_T2856725314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PreviewFieldAttribute
struct  PreviewFieldAttribute_t2856725314  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.PreviewFieldAttribute::Height
	float ___Height_0;
	// Sirenix.OdinInspector.ObjectFieldAlignment Sirenix.OdinInspector.PreviewFieldAttribute::Alignment
	int32_t ___Alignment_1;
	// System.Boolean Sirenix.OdinInspector.PreviewFieldAttribute::AlignmentHasValue
	bool ___AlignmentHasValue_2;

public:
	inline static int32_t get_offset_of_Height_0() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t2856725314, ___Height_0)); }
	inline float get_Height_0() const { return ___Height_0; }
	inline float* get_address_of_Height_0() { return &___Height_0; }
	inline void set_Height_0(float value)
	{
		___Height_0 = value;
	}

	inline static int32_t get_offset_of_Alignment_1() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t2856725314, ___Alignment_1)); }
	inline int32_t get_Alignment_1() const { return ___Alignment_1; }
	inline int32_t* get_address_of_Alignment_1() { return &___Alignment_1; }
	inline void set_Alignment_1(int32_t value)
	{
		___Alignment_1 = value;
	}

	inline static int32_t get_offset_of_AlignmentHasValue_2() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t2856725314, ___AlignmentHasValue_2)); }
	inline bool get_AlignmentHasValue_2() const { return ___AlignmentHasValue_2; }
	inline bool* get_address_of_AlignmentHasValue_2() { return &___AlignmentHasValue_2; }
	inline void set_AlignmentHasValue_2(bool value)
	{
		___AlignmentHasValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWFIELDATTRIBUTE_T2856725314_H
#ifndef PROGRESSBARATTRIBUTE_T2414458096_H
#define PROGRESSBARATTRIBUTE_T2414458096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ProgressBarAttribute
struct  ProgressBarAttribute_t2414458096  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MaxMember
	String_t* ___MaxMember_3;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::R
	float ___R_4;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::G
	float ___G_5;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::B
	float ___B_6;
	// System.Int32 Sirenix.OdinInspector.ProgressBarAttribute::Height
	int32_t ___Height_7;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::ColorMember
	String_t* ___ColorMember_8;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::BackgroundColorMember
	String_t* ___BackgroundColorMember_9;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::Segmented
	bool ___Segmented_10;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::CustomValueStringMember
	String_t* ___CustomValueStringMember_11;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::drawValueLabel
	bool ___drawValueLabel_12;
	// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::valueLabelAlignment
	int32_t ___valueLabelAlignment_13;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<DrawValueLabelHasValue>k__BackingField
	bool ___U3CDrawValueLabelHasValueU3Ek__BackingField_14;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<ValueLabelAlignmentHasValue>k__BackingField
	bool ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinMember_2), value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___MaxMember_3), value);
	}

	inline static int32_t get_offset_of_R_4() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___R_4)); }
	inline float get_R_4() const { return ___R_4; }
	inline float* get_address_of_R_4() { return &___R_4; }
	inline void set_R_4(float value)
	{
		___R_4 = value;
	}

	inline static int32_t get_offset_of_G_5() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___G_5)); }
	inline float get_G_5() const { return ___G_5; }
	inline float* get_address_of_G_5() { return &___G_5; }
	inline void set_G_5(float value)
	{
		___G_5 = value;
	}

	inline static int32_t get_offset_of_B_6() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___B_6)); }
	inline float get_B_6() const { return ___B_6; }
	inline float* get_address_of_B_6() { return &___B_6; }
	inline void set_B_6(float value)
	{
		___B_6 = value;
	}

	inline static int32_t get_offset_of_Height_7() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Height_7)); }
	inline int32_t get_Height_7() const { return ___Height_7; }
	inline int32_t* get_address_of_Height_7() { return &___Height_7; }
	inline void set_Height_7(int32_t value)
	{
		___Height_7 = value;
	}

	inline static int32_t get_offset_of_ColorMember_8() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___ColorMember_8)); }
	inline String_t* get_ColorMember_8() const { return ___ColorMember_8; }
	inline String_t** get_address_of_ColorMember_8() { return &___ColorMember_8; }
	inline void set_ColorMember_8(String_t* value)
	{
		___ColorMember_8 = value;
		Il2CppCodeGenWriteBarrier((&___ColorMember_8), value);
	}

	inline static int32_t get_offset_of_BackgroundColorMember_9() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___BackgroundColorMember_9)); }
	inline String_t* get_BackgroundColorMember_9() const { return ___BackgroundColorMember_9; }
	inline String_t** get_address_of_BackgroundColorMember_9() { return &___BackgroundColorMember_9; }
	inline void set_BackgroundColorMember_9(String_t* value)
	{
		___BackgroundColorMember_9 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundColorMember_9), value);
	}

	inline static int32_t get_offset_of_Segmented_10() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___Segmented_10)); }
	inline bool get_Segmented_10() const { return ___Segmented_10; }
	inline bool* get_address_of_Segmented_10() { return &___Segmented_10; }
	inline void set_Segmented_10(bool value)
	{
		___Segmented_10 = value;
	}

	inline static int32_t get_offset_of_CustomValueStringMember_11() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___CustomValueStringMember_11)); }
	inline String_t* get_CustomValueStringMember_11() const { return ___CustomValueStringMember_11; }
	inline String_t** get_address_of_CustomValueStringMember_11() { return &___CustomValueStringMember_11; }
	inline void set_CustomValueStringMember_11(String_t* value)
	{
		___CustomValueStringMember_11 = value;
		Il2CppCodeGenWriteBarrier((&___CustomValueStringMember_11), value);
	}

	inline static int32_t get_offset_of_drawValueLabel_12() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___drawValueLabel_12)); }
	inline bool get_drawValueLabel_12() const { return ___drawValueLabel_12; }
	inline bool* get_address_of_drawValueLabel_12() { return &___drawValueLabel_12; }
	inline void set_drawValueLabel_12(bool value)
	{
		___drawValueLabel_12 = value;
	}

	inline static int32_t get_offset_of_valueLabelAlignment_13() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___valueLabelAlignment_13)); }
	inline int32_t get_valueLabelAlignment_13() const { return ___valueLabelAlignment_13; }
	inline int32_t* get_address_of_valueLabelAlignment_13() { return &___valueLabelAlignment_13; }
	inline void set_valueLabelAlignment_13(int32_t value)
	{
		___valueLabelAlignment_13 = value;
	}

	inline static int32_t get_offset_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___U3CDrawValueLabelHasValueU3Ek__BackingField_14)); }
	inline bool get_U3CDrawValueLabelHasValueU3Ek__BackingField_14() const { return ___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return &___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline void set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(bool value)
	{
		___U3CDrawValueLabelHasValueU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t2414458096, ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15)); }
	inline bool get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() const { return ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return &___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline void set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(bool value)
	{
		___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBARATTRIBUTE_T2414458096_H
#ifndef RESPONSIVEBUTTONGROUPATTRIBUTE_T1637434916_H
#define RESPONSIVEBUTTONGROUPATTRIBUTE_T1637434916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct  ResponsiveButtonGroupAttribute_t1637434916  : public PropertyGroupAttribute_t2009328757
{
public:
	// Sirenix.OdinInspector.ButtonSizes Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::DefaultButtonSize
	int32_t ___DefaultButtonSize_3;
	// System.Boolean Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::UniformLayout
	bool ___UniformLayout_4;

public:
	inline static int32_t get_offset_of_DefaultButtonSize_3() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t1637434916, ___DefaultButtonSize_3)); }
	inline int32_t get_DefaultButtonSize_3() const { return ___DefaultButtonSize_3; }
	inline int32_t* get_address_of_DefaultButtonSize_3() { return &___DefaultButtonSize_3; }
	inline void set_DefaultButtonSize_3(int32_t value)
	{
		___DefaultButtonSize_3 = value;
	}

	inline static int32_t get_offset_of_UniformLayout_4() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t1637434916, ___UniformLayout_4)); }
	inline bool get_UniformLayout_4() const { return ___UniformLayout_4; }
	inline bool* get_address_of_UniformLayout_4() { return &___UniformLayout_4; }
	inline void set_UniformLayout_4(bool value)
	{
		___UniformLayout_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSIVEBUTTONGROUPATTRIBUTE_T1637434916_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CHANNELQOS_T1890984120_H
#define CHANNELQOS_T1890984120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelQOS
struct  ChannelQOS_t1890984120  : public RuntimeObject
{
public:
	// UnityEngine.Networking.QosType UnityEngine.Networking.ChannelQOS::m_Type
	int32_t ___m_Type_0;
	// System.Boolean UnityEngine.Networking.ChannelQOS::m_BelongsSharedOrderChannel
	bool ___m_BelongsSharedOrderChannel_1;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(ChannelQOS_t1890984120, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BelongsSharedOrderChannel_1() { return static_cast<int32_t>(offsetof(ChannelQOS_t1890984120, ___m_BelongsSharedOrderChannel_1)); }
	inline bool get_m_BelongsSharedOrderChannel_1() const { return ___m_BelongsSharedOrderChannel_1; }
	inline bool* get_address_of_m_BelongsSharedOrderChannel_1() { return &___m_BelongsSharedOrderChannel_1; }
	inline void set_m_BelongsSharedOrderChannel_1(bool value)
	{
		___m_BelongsSharedOrderChannel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELQOS_T1890984120_H
#ifndef CONNECTIONCONFIG_T4173981269_H
#define CONNECTIONCONFIG_T4173981269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ConnectionConfig
struct  ConnectionConfig_t4173981269  : public RuntimeObject
{
public:
	// System.UInt16 UnityEngine.Networking.ConnectionConfig::m_PacketSize
	uint16_t ___m_PacketSize_1;
	// System.UInt16 UnityEngine.Networking.ConnectionConfig::m_FragmentSize
	uint16_t ___m_FragmentSize_2;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_ResendTimeout
	uint32_t ___m_ResendTimeout_3;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_DisconnectTimeout
	uint32_t ___m_DisconnectTimeout_4;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_ConnectTimeout
	uint32_t ___m_ConnectTimeout_5;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_MinUpdateTimeout
	uint32_t ___m_MinUpdateTimeout_6;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_PingTimeout
	uint32_t ___m_PingTimeout_7;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_ReducedPingTimeout
	uint32_t ___m_ReducedPingTimeout_8;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_AllCostTimeout
	uint32_t ___m_AllCostTimeout_9;
	// System.Byte UnityEngine.Networking.ConnectionConfig::m_NetworkDropThreshold
	uint8_t ___m_NetworkDropThreshold_10;
	// System.Byte UnityEngine.Networking.ConnectionConfig::m_OverflowDropThreshold
	uint8_t ___m_OverflowDropThreshold_11;
	// System.Byte UnityEngine.Networking.ConnectionConfig::m_MaxConnectionAttempt
	uint8_t ___m_MaxConnectionAttempt_12;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_AckDelay
	uint32_t ___m_AckDelay_13;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_SendDelay
	uint32_t ___m_SendDelay_14;
	// System.UInt16 UnityEngine.Networking.ConnectionConfig::m_MaxCombinedReliableMessageSize
	uint16_t ___m_MaxCombinedReliableMessageSize_15;
	// System.UInt16 UnityEngine.Networking.ConnectionConfig::m_MaxCombinedReliableMessageCount
	uint16_t ___m_MaxCombinedReliableMessageCount_16;
	// System.UInt16 UnityEngine.Networking.ConnectionConfig::m_MaxSentMessageQueueSize
	uint16_t ___m_MaxSentMessageQueueSize_17;
	// UnityEngine.Networking.ConnectionAcksType UnityEngine.Networking.ConnectionConfig::m_AcksType
	int32_t ___m_AcksType_18;
	// System.Boolean UnityEngine.Networking.ConnectionConfig::m_UsePlatformSpecificProtocols
	bool ___m_UsePlatformSpecificProtocols_19;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_InitialBandwidth
	uint32_t ___m_InitialBandwidth_20;
	// System.Single UnityEngine.Networking.ConnectionConfig::m_BandwidthPeakFactor
	float ___m_BandwidthPeakFactor_21;
	// System.UInt16 UnityEngine.Networking.ConnectionConfig::m_WebSocketReceiveBufferMaxSize
	uint16_t ___m_WebSocketReceiveBufferMaxSize_22;
	// System.UInt32 UnityEngine.Networking.ConnectionConfig::m_UdpSocketReceiveBufferMaxSize
	uint32_t ___m_UdpSocketReceiveBufferMaxSize_23;
	// System.String UnityEngine.Networking.ConnectionConfig::m_SSLCertFilePath
	String_t* ___m_SSLCertFilePath_24;
	// System.String UnityEngine.Networking.ConnectionConfig::m_SSLPrivateKeyFilePath
	String_t* ___m_SSLPrivateKeyFilePath_25;
	// System.String UnityEngine.Networking.ConnectionConfig::m_SSLCAFilePath
	String_t* ___m_SSLCAFilePath_26;
	// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelQOS> UnityEngine.Networking.ConnectionConfig::m_Channels
	List_1_t3363058862 * ___m_Channels_27;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Byte>> UnityEngine.Networking.ConnectionConfig::m_SharedOrderChannels
	List_1_t4078445860 * ___m_SharedOrderChannels_28;

public:
	inline static int32_t get_offset_of_m_PacketSize_1() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_PacketSize_1)); }
	inline uint16_t get_m_PacketSize_1() const { return ___m_PacketSize_1; }
	inline uint16_t* get_address_of_m_PacketSize_1() { return &___m_PacketSize_1; }
	inline void set_m_PacketSize_1(uint16_t value)
	{
		___m_PacketSize_1 = value;
	}

	inline static int32_t get_offset_of_m_FragmentSize_2() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_FragmentSize_2)); }
	inline uint16_t get_m_FragmentSize_2() const { return ___m_FragmentSize_2; }
	inline uint16_t* get_address_of_m_FragmentSize_2() { return &___m_FragmentSize_2; }
	inline void set_m_FragmentSize_2(uint16_t value)
	{
		___m_FragmentSize_2 = value;
	}

	inline static int32_t get_offset_of_m_ResendTimeout_3() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_ResendTimeout_3)); }
	inline uint32_t get_m_ResendTimeout_3() const { return ___m_ResendTimeout_3; }
	inline uint32_t* get_address_of_m_ResendTimeout_3() { return &___m_ResendTimeout_3; }
	inline void set_m_ResendTimeout_3(uint32_t value)
	{
		___m_ResendTimeout_3 = value;
	}

	inline static int32_t get_offset_of_m_DisconnectTimeout_4() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_DisconnectTimeout_4)); }
	inline uint32_t get_m_DisconnectTimeout_4() const { return ___m_DisconnectTimeout_4; }
	inline uint32_t* get_address_of_m_DisconnectTimeout_4() { return &___m_DisconnectTimeout_4; }
	inline void set_m_DisconnectTimeout_4(uint32_t value)
	{
		___m_DisconnectTimeout_4 = value;
	}

	inline static int32_t get_offset_of_m_ConnectTimeout_5() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_ConnectTimeout_5)); }
	inline uint32_t get_m_ConnectTimeout_5() const { return ___m_ConnectTimeout_5; }
	inline uint32_t* get_address_of_m_ConnectTimeout_5() { return &___m_ConnectTimeout_5; }
	inline void set_m_ConnectTimeout_5(uint32_t value)
	{
		___m_ConnectTimeout_5 = value;
	}

	inline static int32_t get_offset_of_m_MinUpdateTimeout_6() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_MinUpdateTimeout_6)); }
	inline uint32_t get_m_MinUpdateTimeout_6() const { return ___m_MinUpdateTimeout_6; }
	inline uint32_t* get_address_of_m_MinUpdateTimeout_6() { return &___m_MinUpdateTimeout_6; }
	inline void set_m_MinUpdateTimeout_6(uint32_t value)
	{
		___m_MinUpdateTimeout_6 = value;
	}

	inline static int32_t get_offset_of_m_PingTimeout_7() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_PingTimeout_7)); }
	inline uint32_t get_m_PingTimeout_7() const { return ___m_PingTimeout_7; }
	inline uint32_t* get_address_of_m_PingTimeout_7() { return &___m_PingTimeout_7; }
	inline void set_m_PingTimeout_7(uint32_t value)
	{
		___m_PingTimeout_7 = value;
	}

	inline static int32_t get_offset_of_m_ReducedPingTimeout_8() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_ReducedPingTimeout_8)); }
	inline uint32_t get_m_ReducedPingTimeout_8() const { return ___m_ReducedPingTimeout_8; }
	inline uint32_t* get_address_of_m_ReducedPingTimeout_8() { return &___m_ReducedPingTimeout_8; }
	inline void set_m_ReducedPingTimeout_8(uint32_t value)
	{
		___m_ReducedPingTimeout_8 = value;
	}

	inline static int32_t get_offset_of_m_AllCostTimeout_9() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_AllCostTimeout_9)); }
	inline uint32_t get_m_AllCostTimeout_9() const { return ___m_AllCostTimeout_9; }
	inline uint32_t* get_address_of_m_AllCostTimeout_9() { return &___m_AllCostTimeout_9; }
	inline void set_m_AllCostTimeout_9(uint32_t value)
	{
		___m_AllCostTimeout_9 = value;
	}

	inline static int32_t get_offset_of_m_NetworkDropThreshold_10() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_NetworkDropThreshold_10)); }
	inline uint8_t get_m_NetworkDropThreshold_10() const { return ___m_NetworkDropThreshold_10; }
	inline uint8_t* get_address_of_m_NetworkDropThreshold_10() { return &___m_NetworkDropThreshold_10; }
	inline void set_m_NetworkDropThreshold_10(uint8_t value)
	{
		___m_NetworkDropThreshold_10 = value;
	}

	inline static int32_t get_offset_of_m_OverflowDropThreshold_11() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_OverflowDropThreshold_11)); }
	inline uint8_t get_m_OverflowDropThreshold_11() const { return ___m_OverflowDropThreshold_11; }
	inline uint8_t* get_address_of_m_OverflowDropThreshold_11() { return &___m_OverflowDropThreshold_11; }
	inline void set_m_OverflowDropThreshold_11(uint8_t value)
	{
		___m_OverflowDropThreshold_11 = value;
	}

	inline static int32_t get_offset_of_m_MaxConnectionAttempt_12() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_MaxConnectionAttempt_12)); }
	inline uint8_t get_m_MaxConnectionAttempt_12() const { return ___m_MaxConnectionAttempt_12; }
	inline uint8_t* get_address_of_m_MaxConnectionAttempt_12() { return &___m_MaxConnectionAttempt_12; }
	inline void set_m_MaxConnectionAttempt_12(uint8_t value)
	{
		___m_MaxConnectionAttempt_12 = value;
	}

	inline static int32_t get_offset_of_m_AckDelay_13() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_AckDelay_13)); }
	inline uint32_t get_m_AckDelay_13() const { return ___m_AckDelay_13; }
	inline uint32_t* get_address_of_m_AckDelay_13() { return &___m_AckDelay_13; }
	inline void set_m_AckDelay_13(uint32_t value)
	{
		___m_AckDelay_13 = value;
	}

	inline static int32_t get_offset_of_m_SendDelay_14() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_SendDelay_14)); }
	inline uint32_t get_m_SendDelay_14() const { return ___m_SendDelay_14; }
	inline uint32_t* get_address_of_m_SendDelay_14() { return &___m_SendDelay_14; }
	inline void set_m_SendDelay_14(uint32_t value)
	{
		___m_SendDelay_14 = value;
	}

	inline static int32_t get_offset_of_m_MaxCombinedReliableMessageSize_15() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_MaxCombinedReliableMessageSize_15)); }
	inline uint16_t get_m_MaxCombinedReliableMessageSize_15() const { return ___m_MaxCombinedReliableMessageSize_15; }
	inline uint16_t* get_address_of_m_MaxCombinedReliableMessageSize_15() { return &___m_MaxCombinedReliableMessageSize_15; }
	inline void set_m_MaxCombinedReliableMessageSize_15(uint16_t value)
	{
		___m_MaxCombinedReliableMessageSize_15 = value;
	}

	inline static int32_t get_offset_of_m_MaxCombinedReliableMessageCount_16() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_MaxCombinedReliableMessageCount_16)); }
	inline uint16_t get_m_MaxCombinedReliableMessageCount_16() const { return ___m_MaxCombinedReliableMessageCount_16; }
	inline uint16_t* get_address_of_m_MaxCombinedReliableMessageCount_16() { return &___m_MaxCombinedReliableMessageCount_16; }
	inline void set_m_MaxCombinedReliableMessageCount_16(uint16_t value)
	{
		___m_MaxCombinedReliableMessageCount_16 = value;
	}

	inline static int32_t get_offset_of_m_MaxSentMessageQueueSize_17() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_MaxSentMessageQueueSize_17)); }
	inline uint16_t get_m_MaxSentMessageQueueSize_17() const { return ___m_MaxSentMessageQueueSize_17; }
	inline uint16_t* get_address_of_m_MaxSentMessageQueueSize_17() { return &___m_MaxSentMessageQueueSize_17; }
	inline void set_m_MaxSentMessageQueueSize_17(uint16_t value)
	{
		___m_MaxSentMessageQueueSize_17 = value;
	}

	inline static int32_t get_offset_of_m_AcksType_18() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_AcksType_18)); }
	inline int32_t get_m_AcksType_18() const { return ___m_AcksType_18; }
	inline int32_t* get_address_of_m_AcksType_18() { return &___m_AcksType_18; }
	inline void set_m_AcksType_18(int32_t value)
	{
		___m_AcksType_18 = value;
	}

	inline static int32_t get_offset_of_m_UsePlatformSpecificProtocols_19() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_UsePlatformSpecificProtocols_19)); }
	inline bool get_m_UsePlatformSpecificProtocols_19() const { return ___m_UsePlatformSpecificProtocols_19; }
	inline bool* get_address_of_m_UsePlatformSpecificProtocols_19() { return &___m_UsePlatformSpecificProtocols_19; }
	inline void set_m_UsePlatformSpecificProtocols_19(bool value)
	{
		___m_UsePlatformSpecificProtocols_19 = value;
	}

	inline static int32_t get_offset_of_m_InitialBandwidth_20() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_InitialBandwidth_20)); }
	inline uint32_t get_m_InitialBandwidth_20() const { return ___m_InitialBandwidth_20; }
	inline uint32_t* get_address_of_m_InitialBandwidth_20() { return &___m_InitialBandwidth_20; }
	inline void set_m_InitialBandwidth_20(uint32_t value)
	{
		___m_InitialBandwidth_20 = value;
	}

	inline static int32_t get_offset_of_m_BandwidthPeakFactor_21() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_BandwidthPeakFactor_21)); }
	inline float get_m_BandwidthPeakFactor_21() const { return ___m_BandwidthPeakFactor_21; }
	inline float* get_address_of_m_BandwidthPeakFactor_21() { return &___m_BandwidthPeakFactor_21; }
	inline void set_m_BandwidthPeakFactor_21(float value)
	{
		___m_BandwidthPeakFactor_21 = value;
	}

	inline static int32_t get_offset_of_m_WebSocketReceiveBufferMaxSize_22() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_WebSocketReceiveBufferMaxSize_22)); }
	inline uint16_t get_m_WebSocketReceiveBufferMaxSize_22() const { return ___m_WebSocketReceiveBufferMaxSize_22; }
	inline uint16_t* get_address_of_m_WebSocketReceiveBufferMaxSize_22() { return &___m_WebSocketReceiveBufferMaxSize_22; }
	inline void set_m_WebSocketReceiveBufferMaxSize_22(uint16_t value)
	{
		___m_WebSocketReceiveBufferMaxSize_22 = value;
	}

	inline static int32_t get_offset_of_m_UdpSocketReceiveBufferMaxSize_23() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_UdpSocketReceiveBufferMaxSize_23)); }
	inline uint32_t get_m_UdpSocketReceiveBufferMaxSize_23() const { return ___m_UdpSocketReceiveBufferMaxSize_23; }
	inline uint32_t* get_address_of_m_UdpSocketReceiveBufferMaxSize_23() { return &___m_UdpSocketReceiveBufferMaxSize_23; }
	inline void set_m_UdpSocketReceiveBufferMaxSize_23(uint32_t value)
	{
		___m_UdpSocketReceiveBufferMaxSize_23 = value;
	}

	inline static int32_t get_offset_of_m_SSLCertFilePath_24() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_SSLCertFilePath_24)); }
	inline String_t* get_m_SSLCertFilePath_24() const { return ___m_SSLCertFilePath_24; }
	inline String_t** get_address_of_m_SSLCertFilePath_24() { return &___m_SSLCertFilePath_24; }
	inline void set_m_SSLCertFilePath_24(String_t* value)
	{
		___m_SSLCertFilePath_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSLCertFilePath_24), value);
	}

	inline static int32_t get_offset_of_m_SSLPrivateKeyFilePath_25() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_SSLPrivateKeyFilePath_25)); }
	inline String_t* get_m_SSLPrivateKeyFilePath_25() const { return ___m_SSLPrivateKeyFilePath_25; }
	inline String_t** get_address_of_m_SSLPrivateKeyFilePath_25() { return &___m_SSLPrivateKeyFilePath_25; }
	inline void set_m_SSLPrivateKeyFilePath_25(String_t* value)
	{
		___m_SSLPrivateKeyFilePath_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSLPrivateKeyFilePath_25), value);
	}

	inline static int32_t get_offset_of_m_SSLCAFilePath_26() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_SSLCAFilePath_26)); }
	inline String_t* get_m_SSLCAFilePath_26() const { return ___m_SSLCAFilePath_26; }
	inline String_t** get_address_of_m_SSLCAFilePath_26() { return &___m_SSLCAFilePath_26; }
	inline void set_m_SSLCAFilePath_26(String_t* value)
	{
		___m_SSLCAFilePath_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSLCAFilePath_26), value);
	}

	inline static int32_t get_offset_of_m_Channels_27() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_Channels_27)); }
	inline List_1_t3363058862 * get_m_Channels_27() const { return ___m_Channels_27; }
	inline List_1_t3363058862 ** get_address_of_m_Channels_27() { return &___m_Channels_27; }
	inline void set_m_Channels_27(List_1_t3363058862 * value)
	{
		___m_Channels_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_27), value);
	}

	inline static int32_t get_offset_of_m_SharedOrderChannels_28() { return static_cast<int32_t>(offsetof(ConnectionConfig_t4173981269, ___m_SharedOrderChannels_28)); }
	inline List_1_t4078445860 * get_m_SharedOrderChannels_28() const { return ___m_SharedOrderChannels_28; }
	inline List_1_t4078445860 ** get_address_of_m_SharedOrderChannels_28() { return &___m_SharedOrderChannels_28; }
	inline void set_m_SharedOrderChannels_28(List_1_t4078445860 * value)
	{
		___m_SharedOrderChannels_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_SharedOrderChannels_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCONFIG_T4173981269_H
#ifndef GLOBALCONFIG_T833512557_H
#define GLOBALCONFIG_T833512557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.GlobalConfig
struct  GlobalConfig_t833512557  : public RuntimeObject
{
public:
	// System.UInt32 UnityEngine.Networking.GlobalConfig::m_ThreadAwakeTimeout
	uint32_t ___m_ThreadAwakeTimeout_3;
	// UnityEngine.Networking.ReactorModel UnityEngine.Networking.GlobalConfig::m_ReactorModel
	int32_t ___m_ReactorModel_4;
	// System.UInt16 UnityEngine.Networking.GlobalConfig::m_ReactorMaximumReceivedMessages
	uint16_t ___m_ReactorMaximumReceivedMessages_5;
	// System.UInt16 UnityEngine.Networking.GlobalConfig::m_ReactorMaximumSentMessages
	uint16_t ___m_ReactorMaximumSentMessages_6;
	// System.UInt16 UnityEngine.Networking.GlobalConfig::m_MaxPacketSize
	uint16_t ___m_MaxPacketSize_7;
	// System.UInt16 UnityEngine.Networking.GlobalConfig::m_MaxHosts
	uint16_t ___m_MaxHosts_8;
	// System.Byte UnityEngine.Networking.GlobalConfig::m_ThreadPoolSize
	uint8_t ___m_ThreadPoolSize_9;
	// System.UInt32 UnityEngine.Networking.GlobalConfig::m_MinTimerTimeout
	uint32_t ___m_MinTimerTimeout_10;
	// System.UInt32 UnityEngine.Networking.GlobalConfig::m_MaxTimerTimeout
	uint32_t ___m_MaxTimerTimeout_11;
	// System.UInt32 UnityEngine.Networking.GlobalConfig::m_MinNetSimulatorTimeout
	uint32_t ___m_MinNetSimulatorTimeout_12;
	// System.UInt32 UnityEngine.Networking.GlobalConfig::m_MaxNetSimulatorTimeout
	uint32_t ___m_MaxNetSimulatorTimeout_13;
	// System.Action`2<System.Int32,System.Int32> UnityEngine.Networking.GlobalConfig::m_ConnectionReadyForSend
	Action_2_t4177122770 * ___m_ConnectionReadyForSend_14;
	// System.Action`1<System.Int32> UnityEngine.Networking.GlobalConfig::m_NetworkEventAvailable
	Action_1_t3123413348 * ___m_NetworkEventAvailable_15;

public:
	inline static int32_t get_offset_of_m_ThreadAwakeTimeout_3() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_ThreadAwakeTimeout_3)); }
	inline uint32_t get_m_ThreadAwakeTimeout_3() const { return ___m_ThreadAwakeTimeout_3; }
	inline uint32_t* get_address_of_m_ThreadAwakeTimeout_3() { return &___m_ThreadAwakeTimeout_3; }
	inline void set_m_ThreadAwakeTimeout_3(uint32_t value)
	{
		___m_ThreadAwakeTimeout_3 = value;
	}

	inline static int32_t get_offset_of_m_ReactorModel_4() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_ReactorModel_4)); }
	inline int32_t get_m_ReactorModel_4() const { return ___m_ReactorModel_4; }
	inline int32_t* get_address_of_m_ReactorModel_4() { return &___m_ReactorModel_4; }
	inline void set_m_ReactorModel_4(int32_t value)
	{
		___m_ReactorModel_4 = value;
	}

	inline static int32_t get_offset_of_m_ReactorMaximumReceivedMessages_5() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_ReactorMaximumReceivedMessages_5)); }
	inline uint16_t get_m_ReactorMaximumReceivedMessages_5() const { return ___m_ReactorMaximumReceivedMessages_5; }
	inline uint16_t* get_address_of_m_ReactorMaximumReceivedMessages_5() { return &___m_ReactorMaximumReceivedMessages_5; }
	inline void set_m_ReactorMaximumReceivedMessages_5(uint16_t value)
	{
		___m_ReactorMaximumReceivedMessages_5 = value;
	}

	inline static int32_t get_offset_of_m_ReactorMaximumSentMessages_6() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_ReactorMaximumSentMessages_6)); }
	inline uint16_t get_m_ReactorMaximumSentMessages_6() const { return ___m_ReactorMaximumSentMessages_6; }
	inline uint16_t* get_address_of_m_ReactorMaximumSentMessages_6() { return &___m_ReactorMaximumSentMessages_6; }
	inline void set_m_ReactorMaximumSentMessages_6(uint16_t value)
	{
		___m_ReactorMaximumSentMessages_6 = value;
	}

	inline static int32_t get_offset_of_m_MaxPacketSize_7() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_MaxPacketSize_7)); }
	inline uint16_t get_m_MaxPacketSize_7() const { return ___m_MaxPacketSize_7; }
	inline uint16_t* get_address_of_m_MaxPacketSize_7() { return &___m_MaxPacketSize_7; }
	inline void set_m_MaxPacketSize_7(uint16_t value)
	{
		___m_MaxPacketSize_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxHosts_8() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_MaxHosts_8)); }
	inline uint16_t get_m_MaxHosts_8() const { return ___m_MaxHosts_8; }
	inline uint16_t* get_address_of_m_MaxHosts_8() { return &___m_MaxHosts_8; }
	inline void set_m_MaxHosts_8(uint16_t value)
	{
		___m_MaxHosts_8 = value;
	}

	inline static int32_t get_offset_of_m_ThreadPoolSize_9() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_ThreadPoolSize_9)); }
	inline uint8_t get_m_ThreadPoolSize_9() const { return ___m_ThreadPoolSize_9; }
	inline uint8_t* get_address_of_m_ThreadPoolSize_9() { return &___m_ThreadPoolSize_9; }
	inline void set_m_ThreadPoolSize_9(uint8_t value)
	{
		___m_ThreadPoolSize_9 = value;
	}

	inline static int32_t get_offset_of_m_MinTimerTimeout_10() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_MinTimerTimeout_10)); }
	inline uint32_t get_m_MinTimerTimeout_10() const { return ___m_MinTimerTimeout_10; }
	inline uint32_t* get_address_of_m_MinTimerTimeout_10() { return &___m_MinTimerTimeout_10; }
	inline void set_m_MinTimerTimeout_10(uint32_t value)
	{
		___m_MinTimerTimeout_10 = value;
	}

	inline static int32_t get_offset_of_m_MaxTimerTimeout_11() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_MaxTimerTimeout_11)); }
	inline uint32_t get_m_MaxTimerTimeout_11() const { return ___m_MaxTimerTimeout_11; }
	inline uint32_t* get_address_of_m_MaxTimerTimeout_11() { return &___m_MaxTimerTimeout_11; }
	inline void set_m_MaxTimerTimeout_11(uint32_t value)
	{
		___m_MaxTimerTimeout_11 = value;
	}

	inline static int32_t get_offset_of_m_MinNetSimulatorTimeout_12() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_MinNetSimulatorTimeout_12)); }
	inline uint32_t get_m_MinNetSimulatorTimeout_12() const { return ___m_MinNetSimulatorTimeout_12; }
	inline uint32_t* get_address_of_m_MinNetSimulatorTimeout_12() { return &___m_MinNetSimulatorTimeout_12; }
	inline void set_m_MinNetSimulatorTimeout_12(uint32_t value)
	{
		___m_MinNetSimulatorTimeout_12 = value;
	}

	inline static int32_t get_offset_of_m_MaxNetSimulatorTimeout_13() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_MaxNetSimulatorTimeout_13)); }
	inline uint32_t get_m_MaxNetSimulatorTimeout_13() const { return ___m_MaxNetSimulatorTimeout_13; }
	inline uint32_t* get_address_of_m_MaxNetSimulatorTimeout_13() { return &___m_MaxNetSimulatorTimeout_13; }
	inline void set_m_MaxNetSimulatorTimeout_13(uint32_t value)
	{
		___m_MaxNetSimulatorTimeout_13 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionReadyForSend_14() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_ConnectionReadyForSend_14)); }
	inline Action_2_t4177122770 * get_m_ConnectionReadyForSend_14() const { return ___m_ConnectionReadyForSend_14; }
	inline Action_2_t4177122770 ** get_address_of_m_ConnectionReadyForSend_14() { return &___m_ConnectionReadyForSend_14; }
	inline void set_m_ConnectionReadyForSend_14(Action_2_t4177122770 * value)
	{
		___m_ConnectionReadyForSend_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionReadyForSend_14), value);
	}

	inline static int32_t get_offset_of_m_NetworkEventAvailable_15() { return static_cast<int32_t>(offsetof(GlobalConfig_t833512557, ___m_NetworkEventAvailable_15)); }
	inline Action_1_t3123413348 * get_m_NetworkEventAvailable_15() const { return ___m_NetworkEventAvailable_15; }
	inline Action_1_t3123413348 ** get_address_of_m_NetworkEventAvailable_15() { return &___m_NetworkEventAvailable_15; }
	inline void set_m_NetworkEventAvailable_15(Action_1_t3123413348 * value)
	{
		___m_NetworkEventAvailable_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkEventAvailable_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALCONFIG_T833512557_H
#ifndef MATCHINFO_T221301733_H
#define MATCHINFO_T221301733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.MatchInfo
struct  MatchInfo_t221301733  : public RuntimeObject
{
public:
	// System.String UnityEngine.Networking.Match.MatchInfo::<address>k__BackingField
	String_t* ___U3CaddressU3Ek__BackingField_0;
	// System.Int32 UnityEngine.Networking.Match.MatchInfo::<port>k__BackingField
	int32_t ___U3CportU3Ek__BackingField_1;
	// System.Int32 UnityEngine.Networking.Match.MatchInfo::<domain>k__BackingField
	int32_t ___U3CdomainU3Ek__BackingField_2;
	// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.MatchInfo::<networkId>k__BackingField
	uint64_t ___U3CnetworkIdU3Ek__BackingField_3;
	// UnityEngine.Networking.Types.NetworkAccessToken UnityEngine.Networking.Match.MatchInfo::<accessToken>k__BackingField
	NetworkAccessToken_t320639760 * ___U3CaccessTokenU3Ek__BackingField_4;
	// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchInfo::<nodeId>k__BackingField
	uint16_t ___U3CnodeIdU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.Match.MatchInfo::<usingRelay>k__BackingField
	bool ___U3CusingRelayU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CaddressU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CaddressU3Ek__BackingField_0)); }
	inline String_t* get_U3CaddressU3Ek__BackingField_0() const { return ___U3CaddressU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CaddressU3Ek__BackingField_0() { return &___U3CaddressU3Ek__BackingField_0; }
	inline void set_U3CaddressU3Ek__BackingField_0(String_t* value)
	{
		___U3CaddressU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaddressU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CportU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CportU3Ek__BackingField_1)); }
	inline int32_t get_U3CportU3Ek__BackingField_1() const { return ___U3CportU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CportU3Ek__BackingField_1() { return &___U3CportU3Ek__BackingField_1; }
	inline void set_U3CportU3Ek__BackingField_1(int32_t value)
	{
		___U3CportU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CdomainU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CdomainU3Ek__BackingField_2)); }
	inline int32_t get_U3CdomainU3Ek__BackingField_2() const { return ___U3CdomainU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CdomainU3Ek__BackingField_2() { return &___U3CdomainU3Ek__BackingField_2; }
	inline void set_U3CdomainU3Ek__BackingField_2(int32_t value)
	{
		___U3CdomainU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CnetworkIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CnetworkIdU3Ek__BackingField_3)); }
	inline uint64_t get_U3CnetworkIdU3Ek__BackingField_3() const { return ___U3CnetworkIdU3Ek__BackingField_3; }
	inline uint64_t* get_address_of_U3CnetworkIdU3Ek__BackingField_3() { return &___U3CnetworkIdU3Ek__BackingField_3; }
	inline void set_U3CnetworkIdU3Ek__BackingField_3(uint64_t value)
	{
		___U3CnetworkIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CaccessTokenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CaccessTokenU3Ek__BackingField_4)); }
	inline NetworkAccessToken_t320639760 * get_U3CaccessTokenU3Ek__BackingField_4() const { return ___U3CaccessTokenU3Ek__BackingField_4; }
	inline NetworkAccessToken_t320639760 ** get_address_of_U3CaccessTokenU3Ek__BackingField_4() { return &___U3CaccessTokenU3Ek__BackingField_4; }
	inline void set_U3CaccessTokenU3Ek__BackingField_4(NetworkAccessToken_t320639760 * value)
	{
		___U3CaccessTokenU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaccessTokenU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CnodeIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CnodeIdU3Ek__BackingField_5)); }
	inline uint16_t get_U3CnodeIdU3Ek__BackingField_5() const { return ___U3CnodeIdU3Ek__BackingField_5; }
	inline uint16_t* get_address_of_U3CnodeIdU3Ek__BackingField_5() { return &___U3CnodeIdU3Ek__BackingField_5; }
	inline void set_U3CnodeIdU3Ek__BackingField_5(uint16_t value)
	{
		___U3CnodeIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CusingRelayU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MatchInfo_t221301733, ___U3CusingRelayU3Ek__BackingField_6)); }
	inline bool get_U3CusingRelayU3Ek__BackingField_6() const { return ___U3CusingRelayU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CusingRelayU3Ek__BackingField_6() { return &___U3CusingRelayU3Ek__BackingField_6; }
	inline void set_U3CusingRelayU3Ek__BackingField_6(bool value)
	{
		___U3CusingRelayU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINFO_T221301733_H
#ifndef MATCHINFOSNAPSHOT_T3166422189_H
#define MATCHINFOSNAPSHOT_T3166422189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.MatchInfoSnapshot
struct  MatchInfoSnapshot_t3166422189  : public RuntimeObject
{
public:
	// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.MatchInfoSnapshot::<networkId>k__BackingField
	uint64_t ___U3CnetworkIdU3Ek__BackingField_0;
	// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchInfoSnapshot::<hostNodeId>k__BackingField
	uint16_t ___U3ChostNodeIdU3Ek__BackingField_1;
	// System.String UnityEngine.Networking.Match.MatchInfoSnapshot::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_2;
	// System.Int32 UnityEngine.Networking.Match.MatchInfoSnapshot::<averageEloScore>k__BackingField
	int32_t ___U3CaverageEloScoreU3Ek__BackingField_3;
	// System.Int32 UnityEngine.Networking.Match.MatchInfoSnapshot::<maxSize>k__BackingField
	int32_t ___U3CmaxSizeU3Ek__BackingField_4;
	// System.Int32 UnityEngine.Networking.Match.MatchInfoSnapshot::<currentSize>k__BackingField
	int32_t ___U3CcurrentSizeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.Match.MatchInfoSnapshot::<isPrivate>k__BackingField
	bool ___U3CisPrivateU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.MatchInfoSnapshot::<matchAttributes>k__BackingField
	Dictionary_2_t3521823603 * ___U3CmatchAttributesU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot> UnityEngine.Networking.Match.MatchInfoSnapshot::<directConnectInfos>k__BackingField
	List_1_t2090095927 * ___U3CdirectConnectInfosU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CnetworkIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CnetworkIdU3Ek__BackingField_0)); }
	inline uint64_t get_U3CnetworkIdU3Ek__BackingField_0() const { return ___U3CnetworkIdU3Ek__BackingField_0; }
	inline uint64_t* get_address_of_U3CnetworkIdU3Ek__BackingField_0() { return &___U3CnetworkIdU3Ek__BackingField_0; }
	inline void set_U3CnetworkIdU3Ek__BackingField_0(uint64_t value)
	{
		___U3CnetworkIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3ChostNodeIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3ChostNodeIdU3Ek__BackingField_1)); }
	inline uint16_t get_U3ChostNodeIdU3Ek__BackingField_1() const { return ___U3ChostNodeIdU3Ek__BackingField_1; }
	inline uint16_t* get_address_of_U3ChostNodeIdU3Ek__BackingField_1() { return &___U3ChostNodeIdU3Ek__BackingField_1; }
	inline void set_U3ChostNodeIdU3Ek__BackingField_1(uint16_t value)
	{
		___U3ChostNodeIdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CnameU3Ek__BackingField_2)); }
	inline String_t* get_U3CnameU3Ek__BackingField_2() const { return ___U3CnameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_2() { return &___U3CnameU3Ek__BackingField_2; }
	inline void set_U3CnameU3Ek__BackingField_2(String_t* value)
	{
		___U3CnameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CaverageEloScoreU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CaverageEloScoreU3Ek__BackingField_3)); }
	inline int32_t get_U3CaverageEloScoreU3Ek__BackingField_3() const { return ___U3CaverageEloScoreU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CaverageEloScoreU3Ek__BackingField_3() { return &___U3CaverageEloScoreU3Ek__BackingField_3; }
	inline void set_U3CaverageEloScoreU3Ek__BackingField_3(int32_t value)
	{
		___U3CaverageEloScoreU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CmaxSizeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CmaxSizeU3Ek__BackingField_4)); }
	inline int32_t get_U3CmaxSizeU3Ek__BackingField_4() const { return ___U3CmaxSizeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CmaxSizeU3Ek__BackingField_4() { return &___U3CmaxSizeU3Ek__BackingField_4; }
	inline void set_U3CmaxSizeU3Ek__BackingField_4(int32_t value)
	{
		___U3CmaxSizeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentSizeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CcurrentSizeU3Ek__BackingField_5)); }
	inline int32_t get_U3CcurrentSizeU3Ek__BackingField_5() const { return ___U3CcurrentSizeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CcurrentSizeU3Ek__BackingField_5() { return &___U3CcurrentSizeU3Ek__BackingField_5; }
	inline void set_U3CcurrentSizeU3Ek__BackingField_5(int32_t value)
	{
		___U3CcurrentSizeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CisPrivateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CisPrivateU3Ek__BackingField_6)); }
	inline bool get_U3CisPrivateU3Ek__BackingField_6() const { return ___U3CisPrivateU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisPrivateU3Ek__BackingField_6() { return &___U3CisPrivateU3Ek__BackingField_6; }
	inline void set_U3CisPrivateU3Ek__BackingField_6(bool value)
	{
		___U3CisPrivateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CmatchAttributesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CmatchAttributesU3Ek__BackingField_7)); }
	inline Dictionary_2_t3521823603 * get_U3CmatchAttributesU3Ek__BackingField_7() const { return ___U3CmatchAttributesU3Ek__BackingField_7; }
	inline Dictionary_2_t3521823603 ** get_address_of_U3CmatchAttributesU3Ek__BackingField_7() { return &___U3CmatchAttributesU3Ek__BackingField_7; }
	inline void set_U3CmatchAttributesU3Ek__BackingField_7(Dictionary_2_t3521823603 * value)
	{
		___U3CmatchAttributesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatchAttributesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdirectConnectInfosU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MatchInfoSnapshot_t3166422189, ___U3CdirectConnectInfosU3Ek__BackingField_8)); }
	inline List_1_t2090095927 * get_U3CdirectConnectInfosU3Ek__BackingField_8() const { return ___U3CdirectConnectInfosU3Ek__BackingField_8; }
	inline List_1_t2090095927 ** get_address_of_U3CdirectConnectInfosU3Ek__BackingField_8() { return &___U3CdirectConnectInfosU3Ek__BackingField_8; }
	inline void set_U3CdirectConnectInfosU3Ek__BackingField_8(List_1_t2090095927 * value)
	{
		___U3CdirectConnectInfosU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdirectConnectInfosU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINFOSNAPSHOT_T3166422189_H
#ifndef MATCHINFODIRECTCONNECTSNAPSHOT_T618021185_H
#define MATCHINFODIRECTCONNECTSNAPSHOT_T618021185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot
struct  MatchInfoDirectConnectSnapshot_t618021185  : public RuntimeObject
{
public:
	// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::<nodeId>k__BackingField
	uint16_t ___U3CnodeIdU3Ek__BackingField_0;
	// System.String UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::<publicAddress>k__BackingField
	String_t* ___U3CpublicAddressU3Ek__BackingField_1;
	// System.String UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::<privateAddress>k__BackingField
	String_t* ___U3CprivateAddressU3Ek__BackingField_2;
	// UnityEngine.Networking.Types.HostPriority UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::<hostPriority>k__BackingField
	int32_t ___U3ChostPriorityU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CnodeIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MatchInfoDirectConnectSnapshot_t618021185, ___U3CnodeIdU3Ek__BackingField_0)); }
	inline uint16_t get_U3CnodeIdU3Ek__BackingField_0() const { return ___U3CnodeIdU3Ek__BackingField_0; }
	inline uint16_t* get_address_of_U3CnodeIdU3Ek__BackingField_0() { return &___U3CnodeIdU3Ek__BackingField_0; }
	inline void set_U3CnodeIdU3Ek__BackingField_0(uint16_t value)
	{
		___U3CnodeIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CpublicAddressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MatchInfoDirectConnectSnapshot_t618021185, ___U3CpublicAddressU3Ek__BackingField_1)); }
	inline String_t* get_U3CpublicAddressU3Ek__BackingField_1() const { return ___U3CpublicAddressU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CpublicAddressU3Ek__BackingField_1() { return &___U3CpublicAddressU3Ek__BackingField_1; }
	inline void set_U3CpublicAddressU3Ek__BackingField_1(String_t* value)
	{
		___U3CpublicAddressU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpublicAddressU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CprivateAddressU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MatchInfoDirectConnectSnapshot_t618021185, ___U3CprivateAddressU3Ek__BackingField_2)); }
	inline String_t* get_U3CprivateAddressU3Ek__BackingField_2() const { return ___U3CprivateAddressU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CprivateAddressU3Ek__BackingField_2() { return &___U3CprivateAddressU3Ek__BackingField_2; }
	inline void set_U3CprivateAddressU3Ek__BackingField_2(String_t* value)
	{
		___U3CprivateAddressU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprivateAddressU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3ChostPriorityU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MatchInfoDirectConnectSnapshot_t618021185, ___U3ChostPriorityU3Ek__BackingField_3)); }
	inline int32_t get_U3ChostPriorityU3Ek__BackingField_3() const { return ___U3ChostPriorityU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3ChostPriorityU3Ek__BackingField_3() { return &___U3ChostPriorityU3Ek__BackingField_3; }
	inline void set_U3ChostPriorityU3Ek__BackingField_3(int32_t value)
	{
		___U3ChostPriorityU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHINFODIRECTCONNECTSNAPSHOT_T618021185_H
#ifndef REQUEST_T2696089890_H
#define REQUEST_T2696089890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.Request
struct  Request_t2696089890  : public RuntimeObject
{
public:
	// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Match.Request::<sourceId>k__BackingField
	uint64_t ___U3CsourceIdU3Ek__BackingField_1;
	// System.String UnityEngine.Networking.Match.Request::<projectId>k__BackingField
	String_t* ___U3CprojectIdU3Ek__BackingField_2;
	// System.String UnityEngine.Networking.Match.Request::<accessTokenString>k__BackingField
	String_t* ___U3CaccessTokenStringU3Ek__BackingField_3;
	// System.Int32 UnityEngine.Networking.Match.Request::<domain>k__BackingField
	int32_t ___U3CdomainU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CsourceIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t2696089890, ___U3CsourceIdU3Ek__BackingField_1)); }
	inline uint64_t get_U3CsourceIdU3Ek__BackingField_1() const { return ___U3CsourceIdU3Ek__BackingField_1; }
	inline uint64_t* get_address_of_U3CsourceIdU3Ek__BackingField_1() { return &___U3CsourceIdU3Ek__BackingField_1; }
	inline void set_U3CsourceIdU3Ek__BackingField_1(uint64_t value)
	{
		___U3CsourceIdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CprojectIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t2696089890, ___U3CprojectIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CprojectIdU3Ek__BackingField_2() const { return ___U3CprojectIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CprojectIdU3Ek__BackingField_2() { return &___U3CprojectIdU3Ek__BackingField_2; }
	inline void set_U3CprojectIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CprojectIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprojectIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CaccessTokenStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t2696089890, ___U3CaccessTokenStringU3Ek__BackingField_3)); }
	inline String_t* get_U3CaccessTokenStringU3Ek__BackingField_3() const { return ___U3CaccessTokenStringU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CaccessTokenStringU3Ek__BackingField_3() { return &___U3CaccessTokenStringU3Ek__BackingField_3; }
	inline void set_U3CaccessTokenStringU3Ek__BackingField_3(String_t* value)
	{
		___U3CaccessTokenStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaccessTokenStringU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdomainU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t2696089890, ___U3CdomainU3Ek__BackingField_4)); }
	inline int32_t get_U3CdomainU3Ek__BackingField_4() const { return ___U3CdomainU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CdomainU3Ek__BackingField_4() { return &___U3CdomainU3Ek__BackingField_4; }
	inline void set_U3CdomainU3Ek__BackingField_4(int32_t value)
	{
		___U3CdomainU3Ek__BackingField_4 = value;
	}
};

struct Request_t2696089890_StaticFields
{
public:
	// System.Int32 UnityEngine.Networking.Match.Request::currentVersion
	int32_t ___currentVersion_0;

public:
	inline static int32_t get_offset_of_currentVersion_0() { return static_cast<int32_t>(offsetof(Request_t2696089890_StaticFields, ___currentVersion_0)); }
	inline int32_t get_currentVersion_0() const { return ___currentVersion_0; }
	inline int32_t* get_address_of_currentVersion_0() { return &___currentVersion_0; }
	inline void set_currentVersion_0(int32_t value)
	{
		___currentVersion_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T2696089890_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BASICRESPONSEDELEGATE_T2196726690_H
#define BASICRESPONSEDELEGATE_T2196726690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.NetworkMatch/BasicResponseDelegate
struct  BasicResponseDelegate_t2196726690  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICRESPONSEDELEGATE_T2196726690_H
#ifndef SETMATCHATTRIBUTESREQUEST_T2732203151_H
#define SETMATCHATTRIBUTESREQUEST_T2732203151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.SetMatchAttributesRequest
struct  SetMatchAttributesRequest_t2732203151  : public Request_t2696089890
{
public:
	// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.SetMatchAttributesRequest::<networkId>k__BackingField
	uint64_t ___U3CnetworkIdU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.Match.SetMatchAttributesRequest::<isListed>k__BackingField
	bool ___U3CisListedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CnetworkIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SetMatchAttributesRequest_t2732203151, ___U3CnetworkIdU3Ek__BackingField_5)); }
	inline uint64_t get_U3CnetworkIdU3Ek__BackingField_5() const { return ___U3CnetworkIdU3Ek__BackingField_5; }
	inline uint64_t* get_address_of_U3CnetworkIdU3Ek__BackingField_5() { return &___U3CnetworkIdU3Ek__BackingField_5; }
	inline void set_U3CnetworkIdU3Ek__BackingField_5(uint64_t value)
	{
		___U3CnetworkIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CisListedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SetMatchAttributesRequest_t2732203151, ___U3CisListedU3Ek__BackingField_6)); }
	inline bool get_U3CisListedU3Ek__BackingField_6() const { return ___U3CisListedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisListedU3Ek__BackingField_6() { return &___U3CisListedU3Ek__BackingField_6; }
	inline void set_U3CisListedU3Ek__BackingField_6(bool value)
	{
		___U3CisListedU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMATCHATTRIBUTESREQUEST_T2732203151_H
#ifndef UNITYPURCHASINGCALLBACK_T953216184_H
#define UNITYPURCHASINGCALLBACK_T953216184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityPurchasingCallback
struct  UnityPurchasingCallback_t953216184  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPURCHASINGCALLBACK_T953216184_H
#ifndef UPDATEDEVENTHANDLER_T1027848393_H
#define UPDATEDEVENTHANDLER_T1027848393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t1027848393  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T1027848393_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef NETWORKMATCH_T2930480025_H
#define NETWORKMATCH_T2930480025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Match.NetworkMatch
struct  NetworkMatch_t2930480025  : public MonoBehaviour_t3962482529
{
public:
	// System.Uri UnityEngine.Networking.Match.NetworkMatch::m_BaseUri
	Uri_t100236324 * ___m_BaseUri_4;

public:
	inline static int32_t get_offset_of_m_BaseUri_4() { return static_cast<int32_t>(offsetof(NetworkMatch_t2930480025, ___m_BaseUri_4)); }
	inline Uri_t100236324 * get_m_BaseUri_4() const { return ___m_BaseUri_4; }
	inline Uri_t100236324 ** get_address_of_m_BaseUri_4() { return &___m_BaseUri_4; }
	inline void set_m_BaseUri_4(Uri_t100236324 * value)
	{
		___m_BaseUri_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseUri_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMATCH_T2930480025_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (SetMatchAttributesRequest_t2732203151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[2] = 
{
	SetMatchAttributesRequest_t2732203151::get_offset_of_U3CnetworkIdU3Ek__BackingField_5(),
	SetMatchAttributesRequest_t2732203151::get_offset_of_U3CisListedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (AppID_t663952513)+ sizeof (RuntimeObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3001[2] = 
{
	AppID_t663952513::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (SourceID_t1070216020)+ sizeof (RuntimeObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3002[2] = 
{
	SourceID_t1070216020::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (NetworkID_t4216585621)+ sizeof (RuntimeObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3003[2] = 
{
	NetworkID_t4216585621::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (NodeID_t2347816311)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3004[2] = 
{
	NodeID_t2347816311::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (HostPriority_t1616615738)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3005[2] = 
{
	HostPriority_t1616615738::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (NetworkAccessToken_t320639760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[2] = 
{
	0,
	NetworkAccessToken_t320639760::get_offset_of_array_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (Utility_t2761513741), -1, sizeof(Utility_t2761513741_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3007[1] = 
{
	Utility_t2761513741_StaticFields::get_offset_of_s_dictTokens_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (MatchInfo_t221301733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[7] = 
{
	MatchInfo_t221301733::get_offset_of_U3CaddressU3Ek__BackingField_0(),
	MatchInfo_t221301733::get_offset_of_U3CportU3Ek__BackingField_1(),
	MatchInfo_t221301733::get_offset_of_U3CdomainU3Ek__BackingField_2(),
	MatchInfo_t221301733::get_offset_of_U3CnetworkIdU3Ek__BackingField_3(),
	MatchInfo_t221301733::get_offset_of_U3CaccessTokenU3Ek__BackingField_4(),
	MatchInfo_t221301733::get_offset_of_U3CnodeIdU3Ek__BackingField_5(),
	MatchInfo_t221301733::get_offset_of_U3CusingRelayU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (MatchInfoSnapshot_t3166422189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[9] = 
{
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CnetworkIdU3Ek__BackingField_0(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3ChostNodeIdU3Ek__BackingField_1(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CnameU3Ek__BackingField_2(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CaverageEloScoreU3Ek__BackingField_3(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CmaxSizeU3Ek__BackingField_4(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CcurrentSizeU3Ek__BackingField_5(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CisPrivateU3Ek__BackingField_6(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CmatchAttributesU3Ek__BackingField_7(),
	MatchInfoSnapshot_t3166422189::get_offset_of_U3CdirectConnectInfosU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (MatchInfoDirectConnectSnapshot_t618021185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[4] = 
{
	MatchInfoDirectConnectSnapshot_t618021185::get_offset_of_U3CnodeIdU3Ek__BackingField_0(),
	MatchInfoDirectConnectSnapshot_t618021185::get_offset_of_U3CpublicAddressU3Ek__BackingField_1(),
	MatchInfoDirectConnectSnapshot_t618021185::get_offset_of_U3CprivateAddressU3Ek__BackingField_2(),
	MatchInfoDirectConnectSnapshot_t618021185::get_offset_of_U3ChostPriorityU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (NetworkMatch_t2930480025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[1] = 
{
	NetworkMatch_t2930480025::get_offset_of_m_BaseUri_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (BasicResponseDelegate_t2196726690), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (NetworkEventType_t3383948383)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3016[6] = 
{
	NetworkEventType_t3383948383::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (QosType_t3566496866)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3017[12] = 
{
	QosType_t3566496866::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (NetworkError_t2038193525)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3018[14] = 
{
	NetworkError_t2038193525::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (ReactorModel_t89779108)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3019[3] = 
{
	ReactorModel_t89779108::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (ConnectionAcksType_t3955378167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3020[5] = 
{
	ConnectionAcksType_t3955378167::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (ChannelQOS_t1890984120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3021[2] = 
{
	ChannelQOS_t1890984120::get_offset_of_m_Type_0(),
	ChannelQOS_t1890984120::get_offset_of_m_BelongsSharedOrderChannel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (ConnectionConfig_t4173981269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[29] = 
{
	0,
	ConnectionConfig_t4173981269::get_offset_of_m_PacketSize_1(),
	ConnectionConfig_t4173981269::get_offset_of_m_FragmentSize_2(),
	ConnectionConfig_t4173981269::get_offset_of_m_ResendTimeout_3(),
	ConnectionConfig_t4173981269::get_offset_of_m_DisconnectTimeout_4(),
	ConnectionConfig_t4173981269::get_offset_of_m_ConnectTimeout_5(),
	ConnectionConfig_t4173981269::get_offset_of_m_MinUpdateTimeout_6(),
	ConnectionConfig_t4173981269::get_offset_of_m_PingTimeout_7(),
	ConnectionConfig_t4173981269::get_offset_of_m_ReducedPingTimeout_8(),
	ConnectionConfig_t4173981269::get_offset_of_m_AllCostTimeout_9(),
	ConnectionConfig_t4173981269::get_offset_of_m_NetworkDropThreshold_10(),
	ConnectionConfig_t4173981269::get_offset_of_m_OverflowDropThreshold_11(),
	ConnectionConfig_t4173981269::get_offset_of_m_MaxConnectionAttempt_12(),
	ConnectionConfig_t4173981269::get_offset_of_m_AckDelay_13(),
	ConnectionConfig_t4173981269::get_offset_of_m_SendDelay_14(),
	ConnectionConfig_t4173981269::get_offset_of_m_MaxCombinedReliableMessageSize_15(),
	ConnectionConfig_t4173981269::get_offset_of_m_MaxCombinedReliableMessageCount_16(),
	ConnectionConfig_t4173981269::get_offset_of_m_MaxSentMessageQueueSize_17(),
	ConnectionConfig_t4173981269::get_offset_of_m_AcksType_18(),
	ConnectionConfig_t4173981269::get_offset_of_m_UsePlatformSpecificProtocols_19(),
	ConnectionConfig_t4173981269::get_offset_of_m_InitialBandwidth_20(),
	ConnectionConfig_t4173981269::get_offset_of_m_BandwidthPeakFactor_21(),
	ConnectionConfig_t4173981269::get_offset_of_m_WebSocketReceiveBufferMaxSize_22(),
	ConnectionConfig_t4173981269::get_offset_of_m_UdpSocketReceiveBufferMaxSize_23(),
	ConnectionConfig_t4173981269::get_offset_of_m_SSLCertFilePath_24(),
	ConnectionConfig_t4173981269::get_offset_of_m_SSLPrivateKeyFilePath_25(),
	ConnectionConfig_t4173981269::get_offset_of_m_SSLCAFilePath_26(),
	ConnectionConfig_t4173981269::get_offset_of_m_Channels_27(),
	ConnectionConfig_t4173981269::get_offset_of_m_SharedOrderChannels_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (HostTopology_t4059263395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[6] = 
{
	HostTopology_t4059263395::get_offset_of_m_DefConfig_0(),
	HostTopology_t4059263395::get_offset_of_m_MaxDefConnections_1(),
	HostTopology_t4059263395::get_offset_of_m_SpecialConnections_2(),
	HostTopology_t4059263395::get_offset_of_m_ReceivedMessagePoolSize_3(),
	HostTopology_t4059263395::get_offset_of_m_SentMessagePoolSize_4(),
	HostTopology_t4059263395::get_offset_of_m_MessagePoolSizeGrowthFactor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (GlobalConfig_t833512557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[16] = 
{
	0,
	0,
	0,
	GlobalConfig_t833512557::get_offset_of_m_ThreadAwakeTimeout_3(),
	GlobalConfig_t833512557::get_offset_of_m_ReactorModel_4(),
	GlobalConfig_t833512557::get_offset_of_m_ReactorMaximumReceivedMessages_5(),
	GlobalConfig_t833512557::get_offset_of_m_ReactorMaximumSentMessages_6(),
	GlobalConfig_t833512557::get_offset_of_m_MaxPacketSize_7(),
	GlobalConfig_t833512557::get_offset_of_m_MaxHosts_8(),
	GlobalConfig_t833512557::get_offset_of_m_ThreadPoolSize_9(),
	GlobalConfig_t833512557::get_offset_of_m_MinTimerTimeout_10(),
	GlobalConfig_t833512557::get_offset_of_m_MaxTimerTimeout_11(),
	GlobalConfig_t833512557::get_offset_of_m_MinNetSimulatorTimeout_12(),
	GlobalConfig_t833512557::get_offset_of_m_MaxNetSimulatorTimeout_13(),
	GlobalConfig_t833512557::get_offset_of_m_ConnectionReadyForSend_14(),
	GlobalConfig_t833512557::get_offset_of_m_NetworkEventAvailable_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3025[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D95D7E9C7483D5AF10DF20044FCD3E580073E1D4B_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (U24ArrayTypeU3D4_t1629360955)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D4_t1629360955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (AnalyticsSessionState_t681173134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3028[5] = 
{
	AnalyticsSessionState_t681173134::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (AnalyticsSessionInfo_t2322308579), -1, sizeof(AnalyticsSessionInfo_t2322308579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3029[1] = 
{
	AnalyticsSessionInfo_t2322308579_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (AnalyticsResult_t2273004240)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3031[9] = 
{
	AnalyticsResult_t2273004240::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (Analytics_t661012366), -1, sizeof(Analytics_t661012366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3032[1] = 
{
	Analytics_t661012366_StaticFields::get_offset_of_s_UnityAnalyticsHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (UnityAnalyticsHandler_t3011359618), sizeof(UnityAnalyticsHandler_t3011359618_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3033[1] = 
{
	UnityAnalyticsHandler_t3011359618::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (CustomEventData_t317522481), sizeof(CustomEventData_t317522481_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3034[1] = 
{
	CustomEventData_t317522481::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3035[3] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
	RemoteSettings_t1718627291_StaticFields::get_offset_of_BeforeFetchFromServer_1(),
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (RemoteConfigSettings_t1247263429), sizeof(RemoteConfigSettings_t1247263429_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3037[2] = 
{
	RemoteConfigSettings_t1247263429::get_offset_of_m_Ptr_0(),
	RemoteConfigSettings_t1247263429::get_offset_of_Updated_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (UnityAdsSettings_t2370518495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (WWW_t3688466362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[1] = 
{
	WWW_t3688466362::get_offset_of__uwr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (U3CModuleU3E_t692745551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (GameViewRenderMode_t3810192523)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3043[5] = 
{
	GameViewRenderMode_t3810192523::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (XRSettings_t335224468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (U3CModuleU3E_t692745553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (MiniJson_t2055087936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (UnityPurchasingCallback_t953216184), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (Json_t4111971237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (Parser_t4254629878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[1] = 
{
	Parser_t4254629878::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (TOKEN_t4214352417)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3052[13] = 
{
	TOKEN_t4214352417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (Serializer_t2325318676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[1] = 
{
	Serializer_t2325318676::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (MiniJsonExtensions_t2206121176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (AssetListAttribute_t3126145472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[6] = 
{
	AssetListAttribute_t3126145472::get_offset_of_AutoPopulate_0(),
	AssetListAttribute_t3126145472::get_offset_of_Tags_1(),
	AssetListAttribute_t3126145472::get_offset_of_LayerNames_2(),
	AssetListAttribute_t3126145472::get_offset_of_AssetNamePrefix_3(),
	AssetListAttribute_t3126145472::get_offset_of_Path_4(),
	AssetListAttribute_t3126145472::get_offset_of_CustomFilterMethod_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (AssetsOnlyAttribute_t3835058913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (BoxGroupAttribute_t1828532190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[2] = 
{
	BoxGroupAttribute_t1828532190::get_offset_of_ShowLabel_3(),
	BoxGroupAttribute_t1828532190::get_offset_of_CenterLabel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (ButtonAttribute_t3418195108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[4] = 
{
	ButtonAttribute_t3418195108::get_offset_of_ButtonHeight_0(),
	ButtonAttribute_t3418195108::get_offset_of_Name_1(),
	ButtonAttribute_t3418195108::get_offset_of_Style_2(),
	ButtonAttribute_t3418195108::get_offset_of_Expanded_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (ButtonGroupAttribute_t2757208248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (ButtonStyle_t3769135257)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3061[4] = 
{
	ButtonStyle_t3769135257::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (CustomValueDrawerAttribute_t4188368302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[1] = 
{
	CustomValueDrawerAttribute_t4188368302::get_offset_of_MethodName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (DisableInInlineEditorsAttribute_t864950097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (DisableInNonPrefabsAttribute_t431643105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (DisableInPrefabAssetsAttribute_t227636478), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (DisableInPrefabInstancesAttribute_t3405933113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (DisableInPrefabsAttribute_t3065057671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (EnableGUIAttribute_t4210275809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (EnumPagingAttribute_t3087485058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (FilePathAttribute_t2750434984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[7] = 
{
	FilePathAttribute_t2750434984::get_offset_of_AbsolutePath_0(),
	FilePathAttribute_t2750434984::get_offset_of_Extensions_1(),
	FilePathAttribute_t2750434984::get_offset_of_ParentFolder_2(),
	FilePathAttribute_t2750434984::get_offset_of_RequireValidPath_3(),
	FilePathAttribute_t2750434984::get_offset_of_RequireExistingPath_4(),
	FilePathAttribute_t2750434984::get_offset_of_UseBackslashes_5(),
	FilePathAttribute_t2750434984::get_offset_of_U3CReadOnlyU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (FolderPathAttribute_t2308214518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[5] = 
{
	FolderPathAttribute_t2308214518::get_offset_of_AbsolutePath_0(),
	FolderPathAttribute_t2308214518::get_offset_of_ParentFolder_1(),
	FolderPathAttribute_t2308214518::get_offset_of_RequireValidPath_2(),
	FolderPathAttribute_t2308214518::get_offset_of_RequireExistingPath_3(),
	FolderPathAttribute_t2308214518::get_offset_of_UseBackslashes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (HideInInlineEditorsAttribute_t2643132944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (HideInNonPrefabsAttribute_t2267791284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (HideInPrefabAssetsAttribute_t3787194334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (HideInPrefabInstancesAttribute_t271927100), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (HideInPrefabsAttribute_t2196881499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (HideInTablesAttribute_t3879405200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (HideNetworkBehaviourFieldsAttribute_t1543820585), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (InlinePropertyAttribute_t3336736111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[1] = 
{
	InlinePropertyAttribute_t3336736111::get_offset_of_LabelWidth_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (LabelWidthAttribute_t2595655695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[1] = 
{
	LabelWidthAttribute_t2595655695::get_offset_of_Width_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (PreviewFieldAttribute_t2856725314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[3] = 
{
	PreviewFieldAttribute_t2856725314::get_offset_of_Height_0(),
	PreviewFieldAttribute_t2856725314::get_offset_of_Alignment_1(),
	PreviewFieldAttribute_t2856725314::get_offset_of_AlignmentHasValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ProgressBarAttribute_t2414458096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[16] = 
{
	ProgressBarAttribute_t2414458096::get_offset_of_Min_0(),
	ProgressBarAttribute_t2414458096::get_offset_of_Max_1(),
	ProgressBarAttribute_t2414458096::get_offset_of_MinMember_2(),
	ProgressBarAttribute_t2414458096::get_offset_of_MaxMember_3(),
	ProgressBarAttribute_t2414458096::get_offset_of_R_4(),
	ProgressBarAttribute_t2414458096::get_offset_of_G_5(),
	ProgressBarAttribute_t2414458096::get_offset_of_B_6(),
	ProgressBarAttribute_t2414458096::get_offset_of_Height_7(),
	ProgressBarAttribute_t2414458096::get_offset_of_ColorMember_8(),
	ProgressBarAttribute_t2414458096::get_offset_of_BackgroundColorMember_9(),
	ProgressBarAttribute_t2414458096::get_offset_of_Segmented_10(),
	ProgressBarAttribute_t2414458096::get_offset_of_CustomValueStringMember_11(),
	ProgressBarAttribute_t2414458096::get_offset_of_drawValueLabel_12(),
	ProgressBarAttribute_t2414458096::get_offset_of_valueLabelAlignment_13(),
	ProgressBarAttribute_t2414458096::get_offset_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14(),
	ProgressBarAttribute_t2414458096::get_offset_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (PropertyRangeAttribute_t3334671474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[4] = 
{
	PropertyRangeAttribute_t3334671474::get_offset_of_Min_0(),
	PropertyRangeAttribute_t3334671474::get_offset_of_Max_1(),
	PropertyRangeAttribute_t3334671474::get_offset_of_MinMember_2(),
	PropertyRangeAttribute_t3334671474::get_offset_of_MaxMember_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (PropertySpaceAttribute_t209179749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[2] = 
{
	PropertySpaceAttribute_t209179749::get_offset_of_SpaceBefore_0(),
	PropertySpaceAttribute_t209179749::get_offset_of_SpaceAfter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (ResponsiveButtonGroupAttribute_t1637434916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[2] = 
{
	ResponsiveButtonGroupAttribute_t1637434916::get_offset_of_DefaultButtonSize_3(),
	ResponsiveButtonGroupAttribute_t1637434916::get_offset_of_UniformLayout_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (ShowInInlineEditorsAttribute_t2931332120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (ShowPropertyResolverAttribute_t357032921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (SuffixLabelAttribute_t126424634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[2] = 
{
	SuffixLabelAttribute_t126424634::get_offset_of_Label_0(),
	SuffixLabelAttribute_t126424634::get_offset_of_Overlay_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (TableColumnWidthAttribute_t1272758069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[2] = 
{
	TableColumnWidthAttribute_t1272758069::get_offset_of_Width_0(),
	TableColumnWidthAttribute_t1272758069::get_offset_of_Resizable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (TableListAttribute_t4098662806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[12] = 
{
	TableListAttribute_t4098662806::get_offset_of_NumberOfItemsPerPage_0(),
	TableListAttribute_t4098662806::get_offset_of_IsReadOnly_1(),
	TableListAttribute_t4098662806::get_offset_of_DefaultMinColumnWidth_2(),
	TableListAttribute_t4098662806::get_offset_of_ShowIndexLabels_3(),
	TableListAttribute_t4098662806::get_offset_of_DrawScrollView_4(),
	TableListAttribute_t4098662806::get_offset_of_MinScrollViewHeight_5(),
	TableListAttribute_t4098662806::get_offset_of_MaxScrollViewHeight_6(),
	TableListAttribute_t4098662806::get_offset_of_AlwaysExpanded_7(),
	TableListAttribute_t4098662806::get_offset_of_HideToolbar_8(),
	TableListAttribute_t4098662806::get_offset_of_CellPadding_9(),
	TableListAttribute_t4098662806::get_offset_of_showPagingHasValue_10(),
	TableListAttribute_t4098662806::get_offset_of_showPaging_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (TableMatrixAttribute_t391949620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[9] = 
{
	TableMatrixAttribute_t391949620::get_offset_of_IsReadOnly_0(),
	TableMatrixAttribute_t391949620::get_offset_of_ResizableColumns_1(),
	TableMatrixAttribute_t391949620::get_offset_of_VerticalTitle_2(),
	TableMatrixAttribute_t391949620::get_offset_of_HorizontalTitle_3(),
	TableMatrixAttribute_t391949620::get_offset_of_DrawElementMethod_4(),
	TableMatrixAttribute_t391949620::get_offset_of_RowHeight_5(),
	TableMatrixAttribute_t391949620::get_offset_of_SquareCells_6(),
	TableMatrixAttribute_t391949620::get_offset_of_HideColumnIndices_7(),
	TableMatrixAttribute_t391949620::get_offset_of_HideRowIndices_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (TypeFilterAttribute_t642666696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[2] = 
{
	TypeFilterAttribute_t642666696::get_offset_of_MemberName_0(),
	TypeFilterAttribute_t642666696::get_offset_of_DropdownTitle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (TypeInfoBoxAttribute_t3273027527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[1] = 
{
	TypeInfoBoxAttribute_t3273027527::get_offset_of_Message_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (VerticalGroupAttribute_t4076833965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[2] = 
{
	VerticalGroupAttribute_t4076833965::get_offset_of_PaddingTop_3(),
	VerticalGroupAttribute_t4076833965::get_offset_of_PaddingBottom_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (AttributeTargetFlags_t870309191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (IncludeMyAttributesAttribute_t2353296197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (InlineEditorObjectFieldModes_t1823869811)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3097[5] = 
{
	InlineEditorObjectFieldModes_t1823869811::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (ObjectFieldAlignment_t2976484322)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3098[4] = 
{
	ObjectFieldAlignment_t2976484322::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (RegisterAttributeAttribute_t317614997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3099[2] = 
{
	RegisterAttributeAttribute_t317614997::get_offset_of_AttributeType_0(),
	RegisterAttributeAttribute_t317614997::get_offset_of_Categories_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
