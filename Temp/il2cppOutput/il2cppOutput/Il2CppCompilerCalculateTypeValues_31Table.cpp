﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.String
struct String_t;




#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COLOREXTENSIONS_T628298469_H
#define COLOREXTENSIONS_T628298469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.ColorExtensions
struct  ColorExtensions_t628298469  : public RuntimeObject
{
public:

public:
};

struct ColorExtensions_t628298469_StaticFields
{
public:
	// System.Char[] Sirenix.Utilities.ColorExtensions::trimRGBStart
	CharU5BU5D_t3528271667* ___trimRGBStart_0;

public:
	inline static int32_t get_offset_of_trimRGBStart_0() { return static_cast<int32_t>(offsetof(ColorExtensions_t628298469_StaticFields, ___trimRGBStart_0)); }
	inline CharU5BU5D_t3528271667* get_trimRGBStart_0() const { return ___trimRGBStart_0; }
	inline CharU5BU5D_t3528271667** get_address_of_trimRGBStart_0() { return &___trimRGBStart_0; }
	inline void set_trimRGBStart_0(CharU5BU5D_t3528271667* value)
	{
		___trimRGBStart_0 = value;
		Il2CppCodeGenWriteBarrier((&___trimRGBStart_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOREXTENSIONS_T628298469_H
#ifndef DELEGATEEXTENSIONS_T3908113990_H
#define DELEGATEEXTENSIONS_T3908113990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DelegateExtensions
struct  DelegateExtensions_t3908113990  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEEXTENSIONS_T3908113990_H
#ifndef FIELDINFOEXTENSIONS_T3875362345_H
#define FIELDINFOEXTENSIONS_T3875362345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.FieldInfoExtensions
struct  FieldInfoExtensions_t3875362345  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFOEXTENSIONS_T3875362345_H
#ifndef GARBAGEFREEITERATORS_T2664762835_H
#define GARBAGEFREEITERATORS_T2664762835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GarbageFreeIterators
struct  GarbageFreeIterators_t2664762835  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GARBAGEFREEITERATORS_T2664762835_H
#ifndef LINQEXTENSIONS_T650130004_H
#define LINQEXTENSIONS_T650130004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.LinqExtensions
struct  LinqExtensions_t650130004  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINQEXTENSIONS_T650130004_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef COLORPALETTEATTRIBUTE_T392531492_H
#define COLORPALETTEATTRIBUTE_T392531492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ColorPaletteAttribute
struct  ColorPaletteAttribute_t392531492  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ColorPaletteAttribute::PaletteName
	String_t* ___PaletteName_0;
	// System.Boolean Sirenix.OdinInspector.ColorPaletteAttribute::ShowAlpha
	bool ___ShowAlpha_1;

public:
	inline static int32_t get_offset_of_PaletteName_0() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t392531492, ___PaletteName_0)); }
	inline String_t* get_PaletteName_0() const { return ___PaletteName_0; }
	inline String_t** get_address_of_PaletteName_0() { return &___PaletteName_0; }
	inline void set_PaletteName_0(String_t* value)
	{
		___PaletteName_0 = value;
		Il2CppCodeGenWriteBarrier((&___PaletteName_0), value);
	}

	inline static int32_t get_offset_of_ShowAlpha_1() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t392531492, ___ShowAlpha_1)); }
	inline bool get_ShowAlpha_1() const { return ___ShowAlpha_1; }
	inline bool* get_address_of_ShowAlpha_1() { return &___ShowAlpha_1; }
	inline void set_ShowAlpha_1(bool value)
	{
		___ShowAlpha_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPALETTEATTRIBUTE_T392531492_H
#ifndef CUSTOMCONTEXTMENUATTRIBUTE_T4264451314_H
#define CUSTOMCONTEXTMENUATTRIBUTE_T4264451314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.CustomContextMenuAttribute
struct  CustomContextMenuAttribute_t4264451314  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MenuItem
	String_t* ___MenuItem_0;
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MethodName
	String_t* ___MethodName_1;

public:
	inline static int32_t get_offset_of_MenuItem_0() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_t4264451314, ___MenuItem_0)); }
	inline String_t* get_MenuItem_0() const { return ___MenuItem_0; }
	inline String_t** get_address_of_MenuItem_0() { return &___MenuItem_0; }
	inline void set_MenuItem_0(String_t* value)
	{
		___MenuItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___MenuItem_0), value);
	}

	inline static int32_t get_offset_of_MethodName_1() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_t4264451314, ___MethodName_1)); }
	inline String_t* get_MethodName_1() const { return ___MethodName_1; }
	inline String_t** get_address_of_MethodName_1() { return &___MethodName_1; }
	inline void set_MethodName_1(String_t* value)
	{
		___MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCONTEXTMENUATTRIBUTE_T4264451314_H
#ifndef DELAYEDPROPERTYATTRIBUTE_T1141984869_H
#define DELAYEDPROPERTYATTRIBUTE_T1141984869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DelayedPropertyAttribute
struct  DelayedPropertyAttribute_t1141984869  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELAYEDPROPERTYATTRIBUTE_T1141984869_H
#ifndef DISABLECONTEXTMENUATTRIBUTE_T184909590_H
#define DISABLECONTEXTMENUATTRIBUTE_T184909590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableContextMenuAttribute
struct  DisableContextMenuAttribute_t184909590  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForMember
	bool ___DisableForMember_0;
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForCollectionElements
	bool ___DisableForCollectionElements_1;

public:
	inline static int32_t get_offset_of_DisableForMember_0() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t184909590, ___DisableForMember_0)); }
	inline bool get_DisableForMember_0() const { return ___DisableForMember_0; }
	inline bool* get_address_of_DisableForMember_0() { return &___DisableForMember_0; }
	inline void set_DisableForMember_0(bool value)
	{
		___DisableForMember_0 = value;
	}

	inline static int32_t get_offset_of_DisableForCollectionElements_1() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t184909590, ___DisableForCollectionElements_1)); }
	inline bool get_DisableForCollectionElements_1() const { return ___DisableForCollectionElements_1; }
	inline bool* get_address_of_DisableForCollectionElements_1() { return &___DisableForCollectionElements_1; }
	inline void set_DisableForCollectionElements_1(bool value)
	{
		___DisableForCollectionElements_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECONTEXTMENUATTRIBUTE_T184909590_H
#ifndef DISABLEIFATTRIBUTE_T2867643591_H
#define DISABLEIFATTRIBUTE_T2867643591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableIfAttribute
struct  DisableIfAttribute_t2867643591  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.DisableIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.DisableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t2867643591, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t2867643591, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEIFATTRIBUTE_T2867643591_H
#ifndef DISABLEINEDITORMODEATTRIBUTE_T630823232_H
#define DISABLEINEDITORMODEATTRIBUTE_T630823232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct  DisableInEditorModeAttribute_t630823232  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINEDITORMODEATTRIBUTE_T630823232_H
#ifndef DISABLEINPLAYMODEATTRIBUTE_T3352669054_H
#define DISABLEINPLAYMODEATTRIBUTE_T3352669054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct  DisableInPlayModeAttribute_t3352669054  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEINPLAYMODEATTRIBUTE_T3352669054_H
#ifndef DISPLAYASSTRINGATTRIBUTE_T1002957046_H
#define DISPLAYASSTRINGATTRIBUTE_T1002957046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DisplayAsStringAttribute
struct  DisplayAsStringAttribute_t1002957046  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.DisplayAsStringAttribute::Overflow
	bool ___Overflow_0;

public:
	inline static int32_t get_offset_of_Overflow_0() { return static_cast<int32_t>(offsetof(DisplayAsStringAttribute_t1002957046, ___Overflow_0)); }
	inline bool get_Overflow_0() const { return ___Overflow_0; }
	inline bool* get_address_of_Overflow_0() { return &___Overflow_0; }
	inline void set_Overflow_0(bool value)
	{
		___Overflow_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYASSTRINGATTRIBUTE_T1002957046_H
#ifndef DONTAPPLYTOLISTELEMENTSATTRIBUTE_T3002873312_H
#define DONTAPPLYTOLISTELEMENTSATTRIBUTE_T3002873312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct  DontApplyToListElementsAttribute_t3002873312  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTAPPLYTOLISTELEMENTSATTRIBUTE_T3002873312_H
#ifndef DRAWWITHUNITYATTRIBUTE_T2993498865_H
#define DRAWWITHUNITYATTRIBUTE_T2993498865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DrawWithUnityAttribute
struct  DrawWithUnityAttribute_t2993498865  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWWITHUNITYATTRIBUTE_T2993498865_H
#ifndef ENABLEFORPREFABONLYATTRIBUTE_T2962074089_H
#define ENABLEFORPREFABONLYATTRIBUTE_T2962074089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct  EnableForPrefabOnlyAttribute_t2962074089  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEFORPREFABONLYATTRIBUTE_T2962074089_H
#ifndef ENABLEIFATTRIBUTE_T729670423_H
#define ENABLEIFATTRIBUTE_T729670423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnableIfAttribute
struct  EnableIfAttribute_t729670423  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.EnableIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.EnableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t729670423, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t729670423, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEIFATTRIBUTE_T729670423_H
#ifndef ENUMTOGGLEBUTTONSATTRIBUTE_T3538214787_H
#define ENUMTOGGLEBUTTONSATTRIBUTE_T3538214787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct  EnumToggleButtonsAttribute_t3538214787  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMTOGGLEBUTTONSATTRIBUTE_T3538214787_H
#ifndef HIDEIFATTRIBUTE_T2697016173_H
#define HIDEIFATTRIBUTE_T2697016173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideIfAttribute
struct  HideIfAttribute_t2697016173  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.HideIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.HideIfAttribute::Value
	RuntimeObject * ___Value_1;
	// System.Boolean Sirenix.OdinInspector.HideIfAttribute::Animate
	bool ___Animate_2;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(HideIfAttribute_t2697016173, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(HideIfAttribute_t2697016173, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}

	inline static int32_t get_offset_of_Animate_2() { return static_cast<int32_t>(offsetof(HideIfAttribute_t2697016173, ___Animate_2)); }
	inline bool get_Animate_2() const { return ___Animate_2; }
	inline bool* get_address_of_Animate_2() { return &___Animate_2; }
	inline void set_Animate_2(bool value)
	{
		___Animate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEIFATTRIBUTE_T2697016173_H
#ifndef HIDEINEDITORMODEATTRIBUTE_T1562875995_H
#define HIDEINEDITORMODEATTRIBUTE_T1562875995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInEditorModeAttribute
struct  HideInEditorModeAttribute_t1562875995  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINEDITORMODEATTRIBUTE_T1562875995_H
#ifndef HIDEINPLAYMODEATTRIBUTE_T3220251466_H
#define HIDEINPLAYMODEATTRIBUTE_T3220251466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideInPlayModeAttribute
struct  HideInPlayModeAttribute_t3220251466  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINPLAYMODEATTRIBUTE_T3220251466_H
#ifndef HIDELABELATTRIBUTE_T3755575312_H
#define HIDELABELATTRIBUTE_T3755575312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideLabelAttribute
struct  HideLabelAttribute_t3755575312  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDELABELATTRIBUTE_T3755575312_H
#ifndef HIDEMONOSCRIPTATTRIBUTE_T3453138722_H
#define HIDEMONOSCRIPTATTRIBUTE_T3453138722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideMonoScriptAttribute
struct  HideMonoScriptAttribute_t3453138722  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEMONOSCRIPTATTRIBUTE_T3453138722_H
#ifndef HIDEREFERENCEOBJECTPICKERATTRIBUTE_T3431975763_H
#define HIDEREFERENCEOBJECTPICKERATTRIBUTE_T3431975763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct  HideReferenceObjectPickerAttribute_t3431975763  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEREFERENCEOBJECTPICKERATTRIBUTE_T3431975763_H
#ifndef INDENTATTRIBUTE_T159299566_H
#define INDENTATTRIBUTE_T159299566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.IndentAttribute
struct  IndentAttribute_t159299566  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.IndentAttribute::IndentLevel
	int32_t ___IndentLevel_0;

public:
	inline static int32_t get_offset_of_IndentLevel_0() { return static_cast<int32_t>(offsetof(IndentAttribute_t159299566, ___IndentLevel_0)); }
	inline int32_t get_IndentLevel_0() const { return ___IndentLevel_0; }
	inline int32_t* get_address_of_IndentLevel_0() { return &___IndentLevel_0; }
	inline void set_IndentLevel_0(int32_t value)
	{
		___IndentLevel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDENTATTRIBUTE_T159299566_H
#ifndef INLINEBUTTONATTRIBUTE_T2729761977_H
#define INLINEBUTTONATTRIBUTE_T2729761977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineButtonAttribute
struct  InlineButtonAttribute_t2729761977  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::<MemberMethod>k__BackingField
	String_t* ___U3CMemberMethodU3Ek__BackingField_0;
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMemberMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t2729761977, ___U3CMemberMethodU3Ek__BackingField_0)); }
	inline String_t* get_U3CMemberMethodU3Ek__BackingField_0() const { return ___U3CMemberMethodU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMemberMethodU3Ek__BackingField_0() { return &___U3CMemberMethodU3Ek__BackingField_0; }
	inline void set_U3CMemberMethodU3Ek__BackingField_0(String_t* value)
	{
		___U3CMemberMethodU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberMethodU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t2729761977, ___U3CLabelU3Ek__BackingField_1)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_1() const { return ___U3CLabelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_1() { return &___U3CLabelU3Ek__BackingField_1; }
	inline void set_U3CLabelU3Ek__BackingField_1(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEBUTTONATTRIBUTE_T2729761977_H
#ifndef LABELTEXTATTRIBUTE_T2020830118_H
#define LABELTEXTATTRIBUTE_T2020830118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.LabelTextAttribute
struct  LabelTextAttribute_t2020830118  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.LabelTextAttribute::Text
	String_t* ___Text_0;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(LabelTextAttribute_t2020830118, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELTEXTATTRIBUTE_T2020830118_H
#ifndef LISTDRAWERSETTINGSATTRIBUTE_T1308946087_H
#define LISTDRAWERSETTINGSATTRIBUTE_T1308946087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct  ListDrawerSettingsAttribute_t1308946087  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideAddButton
	bool ___HideAddButton_0;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideRemoveButton
	bool ___HideRemoveButton_1;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::ListElementLabelName
	String_t* ___ListElementLabelName_2;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomAddFunction
	String_t* ___CustomAddFunction_3;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveIndexFunction
	String_t* ___CustomRemoveIndexFunction_4;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveElementFunction
	String_t* ___CustomRemoveElementFunction_5;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnBeginListElementGUI
	String_t* ___OnBeginListElementGUI_6;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnEndListElementGUI
	String_t* ___OnEndListElementGUI_7;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::AlwaysAddDefaultValue
	bool ___AlwaysAddDefaultValue_8;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::onTitleBarGUI
	String_t* ___onTitleBarGUI_9;
	// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPage
	int32_t ___numberOfItemsPerPage_10;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::paging
	bool ___paging_11;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggable
	bool ___draggable_12;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnly
	bool ___isReadOnly_13;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCount
	bool ___showItemCount_14;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::pagingHasValue
	bool ___pagingHasValue_15;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggableHasValue
	bool ___draggableHasValue_16;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnlyHasValue
	bool ___isReadOnlyHasValue_17;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCountHasValue
	bool ___showItemCountHasValue_18;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expanded
	bool ___expanded_19;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expandedHasValue
	bool ___expandedHasValue_20;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPageHasValue
	bool ___numberOfItemsPerPageHasValue_21;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabels
	bool ___showIndexLabels_22;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabelsHasValue
	bool ___showIndexLabelsHasValue_23;

public:
	inline static int32_t get_offset_of_HideAddButton_0() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___HideAddButton_0)); }
	inline bool get_HideAddButton_0() const { return ___HideAddButton_0; }
	inline bool* get_address_of_HideAddButton_0() { return &___HideAddButton_0; }
	inline void set_HideAddButton_0(bool value)
	{
		___HideAddButton_0 = value;
	}

	inline static int32_t get_offset_of_HideRemoveButton_1() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___HideRemoveButton_1)); }
	inline bool get_HideRemoveButton_1() const { return ___HideRemoveButton_1; }
	inline bool* get_address_of_HideRemoveButton_1() { return &___HideRemoveButton_1; }
	inline void set_HideRemoveButton_1(bool value)
	{
		___HideRemoveButton_1 = value;
	}

	inline static int32_t get_offset_of_ListElementLabelName_2() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___ListElementLabelName_2)); }
	inline String_t* get_ListElementLabelName_2() const { return ___ListElementLabelName_2; }
	inline String_t** get_address_of_ListElementLabelName_2() { return &___ListElementLabelName_2; }
	inline void set_ListElementLabelName_2(String_t* value)
	{
		___ListElementLabelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___ListElementLabelName_2), value);
	}

	inline static int32_t get_offset_of_CustomAddFunction_3() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___CustomAddFunction_3)); }
	inline String_t* get_CustomAddFunction_3() const { return ___CustomAddFunction_3; }
	inline String_t** get_address_of_CustomAddFunction_3() { return &___CustomAddFunction_3; }
	inline void set_CustomAddFunction_3(String_t* value)
	{
		___CustomAddFunction_3 = value;
		Il2CppCodeGenWriteBarrier((&___CustomAddFunction_3), value);
	}

	inline static int32_t get_offset_of_CustomRemoveIndexFunction_4() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___CustomRemoveIndexFunction_4)); }
	inline String_t* get_CustomRemoveIndexFunction_4() const { return ___CustomRemoveIndexFunction_4; }
	inline String_t** get_address_of_CustomRemoveIndexFunction_4() { return &___CustomRemoveIndexFunction_4; }
	inline void set_CustomRemoveIndexFunction_4(String_t* value)
	{
		___CustomRemoveIndexFunction_4 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRemoveIndexFunction_4), value);
	}

	inline static int32_t get_offset_of_CustomRemoveElementFunction_5() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___CustomRemoveElementFunction_5)); }
	inline String_t* get_CustomRemoveElementFunction_5() const { return ___CustomRemoveElementFunction_5; }
	inline String_t** get_address_of_CustomRemoveElementFunction_5() { return &___CustomRemoveElementFunction_5; }
	inline void set_CustomRemoveElementFunction_5(String_t* value)
	{
		___CustomRemoveElementFunction_5 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRemoveElementFunction_5), value);
	}

	inline static int32_t get_offset_of_OnBeginListElementGUI_6() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___OnBeginListElementGUI_6)); }
	inline String_t* get_OnBeginListElementGUI_6() const { return ___OnBeginListElementGUI_6; }
	inline String_t** get_address_of_OnBeginListElementGUI_6() { return &___OnBeginListElementGUI_6; }
	inline void set_OnBeginListElementGUI_6(String_t* value)
	{
		___OnBeginListElementGUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnBeginListElementGUI_6), value);
	}

	inline static int32_t get_offset_of_OnEndListElementGUI_7() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___OnEndListElementGUI_7)); }
	inline String_t* get_OnEndListElementGUI_7() const { return ___OnEndListElementGUI_7; }
	inline String_t** get_address_of_OnEndListElementGUI_7() { return &___OnEndListElementGUI_7; }
	inline void set_OnEndListElementGUI_7(String_t* value)
	{
		___OnEndListElementGUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnEndListElementGUI_7), value);
	}

	inline static int32_t get_offset_of_AlwaysAddDefaultValue_8() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___AlwaysAddDefaultValue_8)); }
	inline bool get_AlwaysAddDefaultValue_8() const { return ___AlwaysAddDefaultValue_8; }
	inline bool* get_address_of_AlwaysAddDefaultValue_8() { return &___AlwaysAddDefaultValue_8; }
	inline void set_AlwaysAddDefaultValue_8(bool value)
	{
		___AlwaysAddDefaultValue_8 = value;
	}

	inline static int32_t get_offset_of_onTitleBarGUI_9() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___onTitleBarGUI_9)); }
	inline String_t* get_onTitleBarGUI_9() const { return ___onTitleBarGUI_9; }
	inline String_t** get_address_of_onTitleBarGUI_9() { return &___onTitleBarGUI_9; }
	inline void set_onTitleBarGUI_9(String_t* value)
	{
		___onTitleBarGUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTitleBarGUI_9), value);
	}

	inline static int32_t get_offset_of_numberOfItemsPerPage_10() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___numberOfItemsPerPage_10)); }
	inline int32_t get_numberOfItemsPerPage_10() const { return ___numberOfItemsPerPage_10; }
	inline int32_t* get_address_of_numberOfItemsPerPage_10() { return &___numberOfItemsPerPage_10; }
	inline void set_numberOfItemsPerPage_10(int32_t value)
	{
		___numberOfItemsPerPage_10 = value;
	}

	inline static int32_t get_offset_of_paging_11() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___paging_11)); }
	inline bool get_paging_11() const { return ___paging_11; }
	inline bool* get_address_of_paging_11() { return &___paging_11; }
	inline void set_paging_11(bool value)
	{
		___paging_11 = value;
	}

	inline static int32_t get_offset_of_draggable_12() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___draggable_12)); }
	inline bool get_draggable_12() const { return ___draggable_12; }
	inline bool* get_address_of_draggable_12() { return &___draggable_12; }
	inline void set_draggable_12(bool value)
	{
		___draggable_12 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_13() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___isReadOnly_13)); }
	inline bool get_isReadOnly_13() const { return ___isReadOnly_13; }
	inline bool* get_address_of_isReadOnly_13() { return &___isReadOnly_13; }
	inline void set_isReadOnly_13(bool value)
	{
		___isReadOnly_13 = value;
	}

	inline static int32_t get_offset_of_showItemCount_14() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showItemCount_14)); }
	inline bool get_showItemCount_14() const { return ___showItemCount_14; }
	inline bool* get_address_of_showItemCount_14() { return &___showItemCount_14; }
	inline void set_showItemCount_14(bool value)
	{
		___showItemCount_14 = value;
	}

	inline static int32_t get_offset_of_pagingHasValue_15() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___pagingHasValue_15)); }
	inline bool get_pagingHasValue_15() const { return ___pagingHasValue_15; }
	inline bool* get_address_of_pagingHasValue_15() { return &___pagingHasValue_15; }
	inline void set_pagingHasValue_15(bool value)
	{
		___pagingHasValue_15 = value;
	}

	inline static int32_t get_offset_of_draggableHasValue_16() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___draggableHasValue_16)); }
	inline bool get_draggableHasValue_16() const { return ___draggableHasValue_16; }
	inline bool* get_address_of_draggableHasValue_16() { return &___draggableHasValue_16; }
	inline void set_draggableHasValue_16(bool value)
	{
		___draggableHasValue_16 = value;
	}

	inline static int32_t get_offset_of_isReadOnlyHasValue_17() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___isReadOnlyHasValue_17)); }
	inline bool get_isReadOnlyHasValue_17() const { return ___isReadOnlyHasValue_17; }
	inline bool* get_address_of_isReadOnlyHasValue_17() { return &___isReadOnlyHasValue_17; }
	inline void set_isReadOnlyHasValue_17(bool value)
	{
		___isReadOnlyHasValue_17 = value;
	}

	inline static int32_t get_offset_of_showItemCountHasValue_18() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showItemCountHasValue_18)); }
	inline bool get_showItemCountHasValue_18() const { return ___showItemCountHasValue_18; }
	inline bool* get_address_of_showItemCountHasValue_18() { return &___showItemCountHasValue_18; }
	inline void set_showItemCountHasValue_18(bool value)
	{
		___showItemCountHasValue_18 = value;
	}

	inline static int32_t get_offset_of_expanded_19() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___expanded_19)); }
	inline bool get_expanded_19() const { return ___expanded_19; }
	inline bool* get_address_of_expanded_19() { return &___expanded_19; }
	inline void set_expanded_19(bool value)
	{
		___expanded_19 = value;
	}

	inline static int32_t get_offset_of_expandedHasValue_20() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___expandedHasValue_20)); }
	inline bool get_expandedHasValue_20() const { return ___expandedHasValue_20; }
	inline bool* get_address_of_expandedHasValue_20() { return &___expandedHasValue_20; }
	inline void set_expandedHasValue_20(bool value)
	{
		___expandedHasValue_20 = value;
	}

	inline static int32_t get_offset_of_numberOfItemsPerPageHasValue_21() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___numberOfItemsPerPageHasValue_21)); }
	inline bool get_numberOfItemsPerPageHasValue_21() const { return ___numberOfItemsPerPageHasValue_21; }
	inline bool* get_address_of_numberOfItemsPerPageHasValue_21() { return &___numberOfItemsPerPageHasValue_21; }
	inline void set_numberOfItemsPerPageHasValue_21(bool value)
	{
		___numberOfItemsPerPageHasValue_21 = value;
	}

	inline static int32_t get_offset_of_showIndexLabels_22() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showIndexLabels_22)); }
	inline bool get_showIndexLabels_22() const { return ___showIndexLabels_22; }
	inline bool* get_address_of_showIndexLabels_22() { return &___showIndexLabels_22; }
	inline void set_showIndexLabels_22(bool value)
	{
		___showIndexLabels_22 = value;
	}

	inline static int32_t get_offset_of_showIndexLabelsHasValue_23() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_t1308946087, ___showIndexLabelsHasValue_23)); }
	inline bool get_showIndexLabelsHasValue_23() const { return ___showIndexLabelsHasValue_23; }
	inline bool* get_address_of_showIndexLabelsHasValue_23() { return &___showIndexLabelsHasValue_23; }
	inline void set_showIndexLabelsHasValue_23(bool value)
	{
		___showIndexLabelsHasValue_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTDRAWERSETTINGSATTRIBUTE_T1308946087_H
#ifndef MAXVALUEATTRIBUTE_T1325324426_H
#define MAXVALUEATTRIBUTE_T1325324426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MaxValueAttribute
struct  MaxValueAttribute_t1325324426  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.MaxValueAttribute::MaxValue
	double ___MaxValue_0;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(MaxValueAttribute_t1325324426, ___MaxValue_0)); }
	inline double get_MaxValue_0() const { return ___MaxValue_0; }
	inline double* get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(double value)
	{
		___MaxValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXVALUEATTRIBUTE_T1325324426_H
#ifndef MINMAXSLIDERATTRIBUTE_T1448807924_H
#define MINMAXSLIDERATTRIBUTE_T1448807924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MinMaxSliderAttribute
struct  MinMaxSliderAttribute_t1448807924  : public Attribute_t861562559
{
public:
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MinValue
	float ___MinValue_0;
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MaxValue
	float ___MaxValue_1;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MaxMember
	String_t* ___MaxMember_3;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMaxMember
	String_t* ___MinMaxMember_4;
	// System.Boolean Sirenix.OdinInspector.MinMaxSliderAttribute::ShowFields
	bool ___ShowFields_5;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MinValue_0)); }
	inline float get_MinValue_0() const { return ___MinValue_0; }
	inline float* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(float value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MaxValue_1)); }
	inline float get_MaxValue_1() const { return ___MaxValue_1; }
	inline float* get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(float value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinMember_2), value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___MaxMember_3), value);
	}

	inline static int32_t get_offset_of_MinMaxMember_4() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___MinMaxMember_4)); }
	inline String_t* get_MinMaxMember_4() const { return ___MinMaxMember_4; }
	inline String_t** get_address_of_MinMaxMember_4() { return &___MinMaxMember_4; }
	inline void set_MinMaxMember_4(String_t* value)
	{
		___MinMaxMember_4 = value;
		Il2CppCodeGenWriteBarrier((&___MinMaxMember_4), value);
	}

	inline static int32_t get_offset_of_ShowFields_5() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t1448807924, ___ShowFields_5)); }
	inline bool get_ShowFields_5() const { return ___ShowFields_5; }
	inline bool* get_address_of_ShowFields_5() { return &___ShowFields_5; }
	inline void set_ShowFields_5(bool value)
	{
		___ShowFields_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXSLIDERATTRIBUTE_T1448807924_H
#ifndef MINVALUEATTRIBUTE_T1691372301_H
#define MINVALUEATTRIBUTE_T1691372301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MinValueAttribute
struct  MinValueAttribute_t1691372301  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.MinValueAttribute::MinValue
	double ___MinValue_0;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinValueAttribute_t1691372301, ___MinValue_0)); }
	inline double get_MinValue_0() const { return ___MinValue_0; }
	inline double* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(double value)
	{
		___MinValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINVALUEATTRIBUTE_T1691372301_H
#ifndef MULTILINEPROPERTYATTRIBUTE_T1222183101_H
#define MULTILINEPROPERTYATTRIBUTE_T1222183101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct  MultiLinePropertyAttribute_t1222183101  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.MultiLinePropertyAttribute::Lines
	int32_t ___Lines_0;

public:
	inline static int32_t get_offset_of_Lines_0() { return static_cast<int32_t>(offsetof(MultiLinePropertyAttribute_t1222183101, ___Lines_0)); }
	inline int32_t get_Lines_0() const { return ___Lines_0; }
	inline int32_t* get_address_of_Lines_0() { return &___Lines_0; }
	inline void set_Lines_0(int32_t value)
	{
		___Lines_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILINEPROPERTYATTRIBUTE_T1222183101_H
#ifndef ONVALUECHANGEDATTRIBUTE_T870857610_H
#define ONVALUECHANGEDATTRIBUTE_T870857610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.OnValueChangedAttribute
struct  OnValueChangedAttribute_t870857610  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.OnValueChangedAttribute::MethodName
	String_t* ___MethodName_0;
	// System.Boolean Sirenix.OdinInspector.OnValueChangedAttribute::IncludeChildren
	bool ___IncludeChildren_1;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t870857610, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_0), value);
	}

	inline static int32_t get_offset_of_IncludeChildren_1() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_t870857610, ___IncludeChildren_1)); }
	inline bool get_IncludeChildren_1() const { return ___IncludeChildren_1; }
	inline bool* get_address_of_IncludeChildren_1() { return &___IncludeChildren_1; }
	inline void set_IncludeChildren_1(bool value)
	{
		___IncludeChildren_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALUECHANGEDATTRIBUTE_T870857610_H
#ifndef PROPERTYGROUPATTRIBUTE_T2009328757_H
#define PROPERTYGROUPATTRIBUTE_T2009328757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyGroupAttribute
struct  PropertyGroupAttribute_t2009328757  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupID
	String_t* ___GroupID_0;
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupName
	String_t* ___GroupName_1;
	// System.Int32 Sirenix.OdinInspector.PropertyGroupAttribute::Order
	int32_t ___Order_2;

public:
	inline static int32_t get_offset_of_GroupID_0() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___GroupID_0)); }
	inline String_t* get_GroupID_0() const { return ___GroupID_0; }
	inline String_t** get_address_of_GroupID_0() { return &___GroupID_0; }
	inline void set_GroupID_0(String_t* value)
	{
		___GroupID_0 = value;
		Il2CppCodeGenWriteBarrier((&___GroupID_0), value);
	}

	inline static int32_t get_offset_of_GroupName_1() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___GroupName_1)); }
	inline String_t* get_GroupName_1() const { return ___GroupName_1; }
	inline String_t** get_address_of_GroupName_1() { return &___GroupName_1; }
	inline void set_GroupName_1(String_t* value)
	{
		___GroupName_1 = value;
		Il2CppCodeGenWriteBarrier((&___GroupName_1), value);
	}

	inline static int32_t get_offset_of_Order_2() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t2009328757, ___Order_2)); }
	inline int32_t get_Order_2() const { return ___Order_2; }
	inline int32_t* get_address_of_Order_2() { return &___Order_2; }
	inline void set_Order_2(int32_t value)
	{
		___Order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYGROUPATTRIBUTE_T2009328757_H
#ifndef PROPERTYORDERATTRIBUTE_T2594707638_H
#define PROPERTYORDERATTRIBUTE_T2594707638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyOrderAttribute
struct  PropertyOrderAttribute_t2594707638  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.OdinInspector.PropertyOrderAttribute::Order
	int32_t ___Order_0;

public:
	inline static int32_t get_offset_of_Order_0() { return static_cast<int32_t>(offsetof(PropertyOrderAttribute_t2594707638, ___Order_0)); }
	inline int32_t get_Order_0() const { return ___Order_0; }
	inline int32_t* get_address_of_Order_0() { return &___Order_0; }
	inline void set_Order_0(int32_t value)
	{
		___Order_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYORDERATTRIBUTE_T2594707638_H
#ifndef PROPERTYTOOLTIPATTRIBUTE_T1909666584_H
#define PROPERTYTOOLTIPATTRIBUTE_T1909666584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.PropertyTooltipAttribute
struct  PropertyTooltipAttribute_t1909666584  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.PropertyTooltipAttribute::Tooltip
	String_t* ___Tooltip_0;

public:
	inline static int32_t get_offset_of_Tooltip_0() { return static_cast<int32_t>(offsetof(PropertyTooltipAttribute_t1909666584, ___Tooltip_0)); }
	inline String_t* get_Tooltip_0() const { return ___Tooltip_0; }
	inline String_t** get_address_of_Tooltip_0() { return &___Tooltip_0; }
	inline void set_Tooltip_0(String_t* value)
	{
		___Tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((&___Tooltip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTOOLTIPATTRIBUTE_T1909666584_H
#ifndef READONLYATTRIBUTE_T3311022813_H
#define READONLYATTRIBUTE_T3311022813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ReadOnlyAttribute
struct  ReadOnlyAttribute_t3311022813  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T3311022813_H
#ifndef SCENEOBJECTSONLYATTRIBUTE_T3518277911_H
#define SCENEOBJECTSONLYATTRIBUTE_T3518277911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct  SceneObjectsOnlyAttribute_t3518277911  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEOBJECTSONLYATTRIBUTE_T3518277911_H
#ifndef SHOWDRAWERCHAINATTRIBUTE_T1144545212_H
#define SHOWDRAWERCHAINATTRIBUTE_T1144545212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct  ShowDrawerChainAttribute_t1144545212  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWDRAWERCHAINATTRIBUTE_T1144545212_H
#ifndef SHOWFORPREFABONLYATTRIBUTE_T4233203441_H
#define SHOWFORPREFABONLYATTRIBUTE_T4233203441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct  ShowForPrefabOnlyAttribute_t4233203441  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWFORPREFABONLYATTRIBUTE_T4233203441_H
#ifndef SHOWIFATTRIBUTE_T2614791958_H
#define SHOWIFATTRIBUTE_T2614791958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowIfAttribute
struct  ShowIfAttribute_t2614791958  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ShowIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Boolean Sirenix.OdinInspector.ShowIfAttribute::Animate
	bool ___Animate_1;
	// System.Object Sirenix.OdinInspector.ShowIfAttribute::Value
	RuntimeObject * ___Value_2;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t2614791958, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_Animate_1() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t2614791958, ___Animate_1)); }
	inline bool get_Animate_1() const { return ___Animate_1; }
	inline bool* get_address_of_Animate_1() { return &___Animate_1; }
	inline void set_Animate_1(bool value)
	{
		___Animate_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t2614791958, ___Value_2)); }
	inline RuntimeObject * get_Value_2() const { return ___Value_2; }
	inline RuntimeObject ** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(RuntimeObject * value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___Value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFATTRIBUTE_T2614791958_H
#ifndef SHOWININSPECTORATTRIBUTE_T2100412880_H
#define SHOWININSPECTORATTRIBUTE_T2100412880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowInInspectorAttribute
struct  ShowInInspectorAttribute_t2100412880  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWININSPECTORATTRIBUTE_T2100412880_H
#ifndef SHOWODINSERIALIZEDPROPERTIESININSPECTORATTRIBUTE_T1684166398_H
#define SHOWODINSERIALIZEDPROPERTIESININSPECTORATTRIBUTE_T1684166398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct  ShowOdinSerializedPropertiesInInspectorAttribute_t1684166398  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWODINSERIALIZEDPROPERTIESININSPECTORATTRIBUTE_T1684166398_H
#ifndef SUPPRESSINVALIDATTRIBUTEERRORATTRIBUTE_T4105959342_H
#define SUPPRESSINVALIDATTRIBUTEERRORATTRIBUTE_T4105959342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct  SuppressInvalidAttributeErrorAttribute_t4105959342  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPRESSINVALIDATTRIBUTEERRORATTRIBUTE_T4105959342_H
#ifndef TOGGLEATTRIBUTE_T3765571124_H
#define TOGGLEATTRIBUTE_T3765571124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ToggleAttribute
struct  ToggleAttribute_t3765571124  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ToggleAttribute::ToggleMemberName
	String_t* ___ToggleMemberName_0;
	// System.Boolean Sirenix.OdinInspector.ToggleAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_1;

public:
	inline static int32_t get_offset_of_ToggleMemberName_0() { return static_cast<int32_t>(offsetof(ToggleAttribute_t3765571124, ___ToggleMemberName_0)); }
	inline String_t* get_ToggleMemberName_0() const { return ___ToggleMemberName_0; }
	inline String_t** get_address_of_ToggleMemberName_0() { return &___ToggleMemberName_0; }
	inline void set_ToggleMemberName_0(String_t* value)
	{
		___ToggleMemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleMemberName_0), value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_1() { return static_cast<int32_t>(offsetof(ToggleAttribute_t3765571124, ___CollapseOthersOnExpand_1)); }
	inline bool get_CollapseOthersOnExpand_1() const { return ___CollapseOthersOnExpand_1; }
	inline bool* get_address_of_CollapseOthersOnExpand_1() { return &___CollapseOthersOnExpand_1; }
	inline void set_CollapseOthersOnExpand_1(bool value)
	{
		___CollapseOthersOnExpand_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEATTRIBUTE_T3765571124_H
#ifndef TOGGLELEFTATTRIBUTE_T2985281771_H
#define TOGGLELEFTATTRIBUTE_T2985281771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ToggleLeftAttribute
struct  ToggleLeftAttribute_t2985281771  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLELEFTATTRIBUTE_T2985281771_H
#ifndef VALUEDROPDOWNATTRIBUTE_T2212993645_H
#define VALUEDROPDOWNATTRIBUTE_T2212993645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ValueDropdownAttribute
struct  ValueDropdownAttribute_t2212993645  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::NumberOfItemsBeforeEnablingSearch
	int32_t ___NumberOfItemsBeforeEnablingSearch_1;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::IsUniqueList
	bool ___IsUniqueList_2;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DrawDropdownForListElements
	bool ___DrawDropdownForListElements_3;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableListAddButtonBehaviour
	bool ___DisableListAddButtonBehaviour_4;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExcludeExistingValuesInList
	bool ___ExcludeExistingValuesInList_5;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExpandAllMenuItems
	bool ___ExpandAllMenuItems_6;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::AppendNextDrawer
	bool ___AppendNextDrawer_7;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableGUIInAppendedDrawer
	bool ___DisableGUIInAppendedDrawer_8;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DoubleClickToConfirm
	bool ___DoubleClickToConfirm_9;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::FlattenTreeView
	bool ___FlattenTreeView_10;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownWidth
	int32_t ___DropdownWidth_11;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownHeight
	int32_t ___DropdownHeight_12;
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::DropdownTitle
	String_t* ___DropdownTitle_13;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::SortDropdownItems
	bool ___SortDropdownItems_14;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::HideChildProperties
	bool ___HideChildProperties_15;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_NumberOfItemsBeforeEnablingSearch_1() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___NumberOfItemsBeforeEnablingSearch_1)); }
	inline int32_t get_NumberOfItemsBeforeEnablingSearch_1() const { return ___NumberOfItemsBeforeEnablingSearch_1; }
	inline int32_t* get_address_of_NumberOfItemsBeforeEnablingSearch_1() { return &___NumberOfItemsBeforeEnablingSearch_1; }
	inline void set_NumberOfItemsBeforeEnablingSearch_1(int32_t value)
	{
		___NumberOfItemsBeforeEnablingSearch_1 = value;
	}

	inline static int32_t get_offset_of_IsUniqueList_2() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___IsUniqueList_2)); }
	inline bool get_IsUniqueList_2() const { return ___IsUniqueList_2; }
	inline bool* get_address_of_IsUniqueList_2() { return &___IsUniqueList_2; }
	inline void set_IsUniqueList_2(bool value)
	{
		___IsUniqueList_2 = value;
	}

	inline static int32_t get_offset_of_DrawDropdownForListElements_3() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DrawDropdownForListElements_3)); }
	inline bool get_DrawDropdownForListElements_3() const { return ___DrawDropdownForListElements_3; }
	inline bool* get_address_of_DrawDropdownForListElements_3() { return &___DrawDropdownForListElements_3; }
	inline void set_DrawDropdownForListElements_3(bool value)
	{
		___DrawDropdownForListElements_3 = value;
	}

	inline static int32_t get_offset_of_DisableListAddButtonBehaviour_4() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DisableListAddButtonBehaviour_4)); }
	inline bool get_DisableListAddButtonBehaviour_4() const { return ___DisableListAddButtonBehaviour_4; }
	inline bool* get_address_of_DisableListAddButtonBehaviour_4() { return &___DisableListAddButtonBehaviour_4; }
	inline void set_DisableListAddButtonBehaviour_4(bool value)
	{
		___DisableListAddButtonBehaviour_4 = value;
	}

	inline static int32_t get_offset_of_ExcludeExistingValuesInList_5() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___ExcludeExistingValuesInList_5)); }
	inline bool get_ExcludeExistingValuesInList_5() const { return ___ExcludeExistingValuesInList_5; }
	inline bool* get_address_of_ExcludeExistingValuesInList_5() { return &___ExcludeExistingValuesInList_5; }
	inline void set_ExcludeExistingValuesInList_5(bool value)
	{
		___ExcludeExistingValuesInList_5 = value;
	}

	inline static int32_t get_offset_of_ExpandAllMenuItems_6() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___ExpandAllMenuItems_6)); }
	inline bool get_ExpandAllMenuItems_6() const { return ___ExpandAllMenuItems_6; }
	inline bool* get_address_of_ExpandAllMenuItems_6() { return &___ExpandAllMenuItems_6; }
	inline void set_ExpandAllMenuItems_6(bool value)
	{
		___ExpandAllMenuItems_6 = value;
	}

	inline static int32_t get_offset_of_AppendNextDrawer_7() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___AppendNextDrawer_7)); }
	inline bool get_AppendNextDrawer_7() const { return ___AppendNextDrawer_7; }
	inline bool* get_address_of_AppendNextDrawer_7() { return &___AppendNextDrawer_7; }
	inline void set_AppendNextDrawer_7(bool value)
	{
		___AppendNextDrawer_7 = value;
	}

	inline static int32_t get_offset_of_DisableGUIInAppendedDrawer_8() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DisableGUIInAppendedDrawer_8)); }
	inline bool get_DisableGUIInAppendedDrawer_8() const { return ___DisableGUIInAppendedDrawer_8; }
	inline bool* get_address_of_DisableGUIInAppendedDrawer_8() { return &___DisableGUIInAppendedDrawer_8; }
	inline void set_DisableGUIInAppendedDrawer_8(bool value)
	{
		___DisableGUIInAppendedDrawer_8 = value;
	}

	inline static int32_t get_offset_of_DoubleClickToConfirm_9() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DoubleClickToConfirm_9)); }
	inline bool get_DoubleClickToConfirm_9() const { return ___DoubleClickToConfirm_9; }
	inline bool* get_address_of_DoubleClickToConfirm_9() { return &___DoubleClickToConfirm_9; }
	inline void set_DoubleClickToConfirm_9(bool value)
	{
		___DoubleClickToConfirm_9 = value;
	}

	inline static int32_t get_offset_of_FlattenTreeView_10() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___FlattenTreeView_10)); }
	inline bool get_FlattenTreeView_10() const { return ___FlattenTreeView_10; }
	inline bool* get_address_of_FlattenTreeView_10() { return &___FlattenTreeView_10; }
	inline void set_FlattenTreeView_10(bool value)
	{
		___FlattenTreeView_10 = value;
	}

	inline static int32_t get_offset_of_DropdownWidth_11() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DropdownWidth_11)); }
	inline int32_t get_DropdownWidth_11() const { return ___DropdownWidth_11; }
	inline int32_t* get_address_of_DropdownWidth_11() { return &___DropdownWidth_11; }
	inline void set_DropdownWidth_11(int32_t value)
	{
		___DropdownWidth_11 = value;
	}

	inline static int32_t get_offset_of_DropdownHeight_12() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DropdownHeight_12)); }
	inline int32_t get_DropdownHeight_12() const { return ___DropdownHeight_12; }
	inline int32_t* get_address_of_DropdownHeight_12() { return &___DropdownHeight_12; }
	inline void set_DropdownHeight_12(int32_t value)
	{
		___DropdownHeight_12 = value;
	}

	inline static int32_t get_offset_of_DropdownTitle_13() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___DropdownTitle_13)); }
	inline String_t* get_DropdownTitle_13() const { return ___DropdownTitle_13; }
	inline String_t** get_address_of_DropdownTitle_13() { return &___DropdownTitle_13; }
	inline void set_DropdownTitle_13(String_t* value)
	{
		___DropdownTitle_13 = value;
		Il2CppCodeGenWriteBarrier((&___DropdownTitle_13), value);
	}

	inline static int32_t get_offset_of_SortDropdownItems_14() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___SortDropdownItems_14)); }
	inline bool get_SortDropdownItems_14() const { return ___SortDropdownItems_14; }
	inline bool* get_address_of_SortDropdownItems_14() { return &___SortDropdownItems_14; }
	inline void set_SortDropdownItems_14(bool value)
	{
		___SortDropdownItems_14 = value;
	}

	inline static int32_t get_offset_of_HideChildProperties_15() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_t2212993645, ___HideChildProperties_15)); }
	inline bool get_HideChildProperties_15() const { return ___HideChildProperties_15; }
	inline bool* get_address_of_HideChildProperties_15() { return &___HideChildProperties_15; }
	inline void set_HideChildProperties_15(bool value)
	{
		___HideChildProperties_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEDROPDOWNATTRIBUTE_T2212993645_H
#ifndef VALUEDROPDOWNITEM_T3457888829_H
#define VALUEDROPDOWNITEM_T3457888829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ValueDropdownItem
struct  ValueDropdownItem_t3457888829 
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownItem::Text
	String_t* ___Text_0;
	// System.Object Sirenix.OdinInspector.ValueDropdownItem::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t3457888829, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t3457888829, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t3457888829_marshaled_pinvoke
{
	char* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};
// Native definition for COM marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t3457888829_marshaled_com
{
	Il2CppChar* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};
#endif // VALUEDROPDOWNITEM_T3457888829_H
#ifndef WRAPATTRIBUTE_T589917550_H
#define WRAPATTRIBUTE_T589917550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.WrapAttribute
struct  WrapAttribute_t589917550  : public Attribute_t861562559
{
public:
	// System.Double Sirenix.OdinInspector.WrapAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.WrapAttribute::Max
	double ___Max_1;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(WrapAttribute_t589917550, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(WrapAttribute_t589917550, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPATTRIBUTE_T589917550_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BUTTONSIZES_T675943090_H
#define BUTTONSIZES_T675943090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ButtonSizes
struct  ButtonSizes_t675943090 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonSizes_t675943090, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSIZES_T675943090_H
#ifndef DICTIONARYDISPLAYOPTIONS_T2340992491_H
#define DICTIONARYDISPLAYOPTIONS_T2340992491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DictionaryDisplayOptions
struct  DictionaryDisplayOptions_t2340992491 
{
public:
	// System.Int32 Sirenix.OdinInspector.DictionaryDisplayOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DictionaryDisplayOptions_t2340992491, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYDISPLAYOPTIONS_T2340992491_H
#ifndef FOLDOUTGROUPATTRIBUTE_T1921228823_H
#define FOLDOUTGROUPATTRIBUTE_T1921228823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.FoldoutGroupAttribute
struct  FoldoutGroupAttribute_t1921228823  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::Expanded
	bool ___Expanded_3;
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::<HasDefinedExpanded>k__BackingField
	bool ___U3CHasDefinedExpandedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_t1921228823, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}

	inline static int32_t get_offset_of_U3CHasDefinedExpandedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_t1921228823, ___U3CHasDefinedExpandedU3Ek__BackingField_4)); }
	inline bool get_U3CHasDefinedExpandedU3Ek__BackingField_4() const { return ___U3CHasDefinedExpandedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasDefinedExpandedU3Ek__BackingField_4() { return &___U3CHasDefinedExpandedU3Ek__BackingField_4; }
	inline void set_U3CHasDefinedExpandedU3Ek__BackingField_4(bool value)
	{
		___U3CHasDefinedExpandedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLDOUTGROUPATTRIBUTE_T1921228823_H
#ifndef GUICOLORATTRIBUTE_T2790983735_H
#define GUICOLORATTRIBUTE_T2790983735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.GUIColorAttribute
struct  GUIColorAttribute_t2790983735  : public Attribute_t861562559
{
public:
	// UnityEngine.Color Sirenix.OdinInspector.GUIColorAttribute::Color
	Color_t2555686324  ___Color_0;
	// System.String Sirenix.OdinInspector.GUIColorAttribute::GetColor
	String_t* ___GetColor_1;

public:
	inline static int32_t get_offset_of_Color_0() { return static_cast<int32_t>(offsetof(GUIColorAttribute_t2790983735, ___Color_0)); }
	inline Color_t2555686324  get_Color_0() const { return ___Color_0; }
	inline Color_t2555686324 * get_address_of_Color_0() { return &___Color_0; }
	inline void set_Color_0(Color_t2555686324  value)
	{
		___Color_0 = value;
	}

	inline static int32_t get_offset_of_GetColor_1() { return static_cast<int32_t>(offsetof(GUIColorAttribute_t2790983735, ___GetColor_1)); }
	inline String_t* get_GetColor_1() const { return ___GetColor_1; }
	inline String_t** get_address_of_GetColor_1() { return &___GetColor_1; }
	inline void set_GetColor_1(String_t* value)
	{
		___GetColor_1 = value;
		Il2CppCodeGenWriteBarrier((&___GetColor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUICOLORATTRIBUTE_T2790983735_H
#ifndef HORIZONTALGROUPATTRIBUTE_T2886913732_H
#define HORIZONTALGROUPATTRIBUTE_T2886913732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.HorizontalGroupAttribute
struct  HorizontalGroupAttribute_t2886913732  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::Width
	float ___Width_3;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginLeft
	float ___MarginLeft_4;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginRight
	float ___MarginRight_5;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingLeft
	float ___PaddingLeft_6;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingRight
	float ___PaddingRight_7;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MinWidth
	float ___MinWidth_8;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MaxWidth
	float ___MaxWidth_9;
	// System.String Sirenix.OdinInspector.HorizontalGroupAttribute::Title
	String_t* ___Title_10;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::LabelWidth
	float ___LabelWidth_11;

public:
	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___Width_3)); }
	inline float get_Width_3() const { return ___Width_3; }
	inline float* get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(float value)
	{
		___Width_3 = value;
	}

	inline static int32_t get_offset_of_MarginLeft_4() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MarginLeft_4)); }
	inline float get_MarginLeft_4() const { return ___MarginLeft_4; }
	inline float* get_address_of_MarginLeft_4() { return &___MarginLeft_4; }
	inline void set_MarginLeft_4(float value)
	{
		___MarginLeft_4 = value;
	}

	inline static int32_t get_offset_of_MarginRight_5() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MarginRight_5)); }
	inline float get_MarginRight_5() const { return ___MarginRight_5; }
	inline float* get_address_of_MarginRight_5() { return &___MarginRight_5; }
	inline void set_MarginRight_5(float value)
	{
		___MarginRight_5 = value;
	}

	inline static int32_t get_offset_of_PaddingLeft_6() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___PaddingLeft_6)); }
	inline float get_PaddingLeft_6() const { return ___PaddingLeft_6; }
	inline float* get_address_of_PaddingLeft_6() { return &___PaddingLeft_6; }
	inline void set_PaddingLeft_6(float value)
	{
		___PaddingLeft_6 = value;
	}

	inline static int32_t get_offset_of_PaddingRight_7() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___PaddingRight_7)); }
	inline float get_PaddingRight_7() const { return ___PaddingRight_7; }
	inline float* get_address_of_PaddingRight_7() { return &___PaddingRight_7; }
	inline void set_PaddingRight_7(float value)
	{
		___PaddingRight_7 = value;
	}

	inline static int32_t get_offset_of_MinWidth_8() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MinWidth_8)); }
	inline float get_MinWidth_8() const { return ___MinWidth_8; }
	inline float* get_address_of_MinWidth_8() { return &___MinWidth_8; }
	inline void set_MinWidth_8(float value)
	{
		___MinWidth_8 = value;
	}

	inline static int32_t get_offset_of_MaxWidth_9() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___MaxWidth_9)); }
	inline float get_MaxWidth_9() const { return ___MaxWidth_9; }
	inline float* get_address_of_MaxWidth_9() { return &___MaxWidth_9; }
	inline void set_MaxWidth_9(float value)
	{
		___MaxWidth_9 = value;
	}

	inline static int32_t get_offset_of_Title_10() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___Title_10)); }
	inline String_t* get_Title_10() const { return ___Title_10; }
	inline String_t** get_address_of_Title_10() { return &___Title_10; }
	inline void set_Title_10(String_t* value)
	{
		___Title_10 = value;
		Il2CppCodeGenWriteBarrier((&___Title_10), value);
	}

	inline static int32_t get_offset_of_LabelWidth_11() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t2886913732, ___LabelWidth_11)); }
	inline float get_LabelWidth_11() const { return ___LabelWidth_11; }
	inline float* get_address_of_LabelWidth_11() { return &___LabelWidth_11; }
	inline void set_LabelWidth_11(float value)
	{
		___LabelWidth_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALGROUPATTRIBUTE_T2886913732_H
#ifndef INFOMESSAGETYPE_T2920171874_H
#define INFOMESSAGETYPE_T2920171874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InfoMessageType
struct  InfoMessageType_t2920171874 
{
public:
	// System.Int32 Sirenix.OdinInspector.InfoMessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InfoMessageType_t2920171874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFOMESSAGETYPE_T2920171874_H
#ifndef INLINEEDITORMODES_T2896600093_H
#define INLINEEDITORMODES_T2896600093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorModes
struct  InlineEditorModes_t2896600093 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorModes_t2896600093, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITORMODES_T2896600093_H
#ifndef INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#define INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorObjectFieldModes
struct  InlineEditorObjectFieldModes_t1823869811 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorObjectFieldModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorObjectFieldModes_t1823869811, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITOROBJECTFIELDMODES_T1823869811_H
#ifndef ONINSPECTORGUIATTRIBUTE_T3458889380_H
#define ONINSPECTORGUIATTRIBUTE_T3458889380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct  OnInspectorGUIAttribute_t3458889380  : public ShowInInspectorAttribute_t2100412880
{
public:
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::PrependMethodName
	String_t* ___PrependMethodName_0;
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::AppendMethodName
	String_t* ___AppendMethodName_1;

public:
	inline static int32_t get_offset_of_PrependMethodName_0() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t3458889380, ___PrependMethodName_0)); }
	inline String_t* get_PrependMethodName_0() const { return ___PrependMethodName_0; }
	inline String_t** get_address_of_PrependMethodName_0() { return &___PrependMethodName_0; }
	inline void set_PrependMethodName_0(String_t* value)
	{
		___PrependMethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___PrependMethodName_0), value);
	}

	inline static int32_t get_offset_of_AppendMethodName_1() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t3458889380, ___AppendMethodName_1)); }
	inline String_t* get_AppendMethodName_1() const { return ___AppendMethodName_1; }
	inline String_t** get_address_of_AppendMethodName_1() { return &___AppendMethodName_1; }
	inline void set_AppendMethodName_1(String_t* value)
	{
		___AppendMethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___AppendMethodName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINSPECTORGUIATTRIBUTE_T3458889380_H
#ifndef TABGROUPATTRIBUTE_T2295443652_H
#define TABGROUPATTRIBUTE_T2295443652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TabGroupAttribute
struct  TabGroupAttribute_t2295443652  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.String Sirenix.OdinInspector.TabGroupAttribute::TabName
	String_t* ___TabName_4;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::UseFixedHeight
	bool ___UseFixedHeight_5;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::Paddingless
	bool ___Paddingless_6;
	// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::<Tabs>k__BackingField
	List_1_t3319525431 * ___U3CTabsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_TabName_4() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___TabName_4)); }
	inline String_t* get_TabName_4() const { return ___TabName_4; }
	inline String_t** get_address_of_TabName_4() { return &___TabName_4; }
	inline void set_TabName_4(String_t* value)
	{
		___TabName_4 = value;
		Il2CppCodeGenWriteBarrier((&___TabName_4), value);
	}

	inline static int32_t get_offset_of_UseFixedHeight_5() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___UseFixedHeight_5)); }
	inline bool get_UseFixedHeight_5() const { return ___UseFixedHeight_5; }
	inline bool* get_address_of_UseFixedHeight_5() { return &___UseFixedHeight_5; }
	inline void set_UseFixedHeight_5(bool value)
	{
		___UseFixedHeight_5 = value;
	}

	inline static int32_t get_offset_of_Paddingless_6() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___Paddingless_6)); }
	inline bool get_Paddingless_6() const { return ___Paddingless_6; }
	inline bool* get_address_of_Paddingless_6() { return &___Paddingless_6; }
	inline void set_Paddingless_6(bool value)
	{
		___Paddingless_6 = value;
	}

	inline static int32_t get_offset_of_U3CTabsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TabGroupAttribute_t2295443652, ___U3CTabsU3Ek__BackingField_7)); }
	inline List_1_t3319525431 * get_U3CTabsU3Ek__BackingField_7() const { return ___U3CTabsU3Ek__BackingField_7; }
	inline List_1_t3319525431 ** get_address_of_U3CTabsU3Ek__BackingField_7() { return &___U3CTabsU3Ek__BackingField_7; }
	inline void set_U3CTabsU3Ek__BackingField_7(List_1_t3319525431 * value)
	{
		___U3CTabsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTabsU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABGROUPATTRIBUTE_T2295443652_H
#ifndef TABSUBGROUPATTRIBUTE_T867681421_H
#define TABSUBGROUPATTRIBUTE_T867681421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct  TabSubGroupAttribute_t867681421  : public PropertyGroupAttribute_t2009328757
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABSUBGROUPATTRIBUTE_T867681421_H
#ifndef TITLEALIGNMENTS_T2525535876_H
#define TITLEALIGNMENTS_T2525535876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TitleAlignments
struct  TitleAlignments_t2525535876 
{
public:
	// System.Int32 Sirenix.OdinInspector.TitleAlignments::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TitleAlignments_t2525535876, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEALIGNMENTS_T2525535876_H
#ifndef TOGGLEGROUPATTRIBUTE_T12117724_H
#define TOGGLEGROUPATTRIBUTE_T12117724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ToggleGroupAttribute
struct  ToggleGroupAttribute_t12117724  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::ToggleGroupTitle
	String_t* ___ToggleGroupTitle_3;
	// System.Boolean Sirenix.OdinInspector.ToggleGroupAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_4;
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::<TitleStringMemberName>k__BackingField
	String_t* ___U3CTitleStringMemberNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_ToggleGroupTitle_3() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t12117724, ___ToggleGroupTitle_3)); }
	inline String_t* get_ToggleGroupTitle_3() const { return ___ToggleGroupTitle_3; }
	inline String_t** get_address_of_ToggleGroupTitle_3() { return &___ToggleGroupTitle_3; }
	inline void set_ToggleGroupTitle_3(String_t* value)
	{
		___ToggleGroupTitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleGroupTitle_3), value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_4() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t12117724, ___CollapseOthersOnExpand_4)); }
	inline bool get_CollapseOthersOnExpand_4() const { return ___CollapseOthersOnExpand_4; }
	inline bool* get_address_of_CollapseOthersOnExpand_4() { return &___CollapseOthersOnExpand_4; }
	inline void set_CollapseOthersOnExpand_4(bool value)
	{
		___CollapseOthersOnExpand_4 = value;
	}

	inline static int32_t get_offset_of_U3CTitleStringMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t12117724, ___U3CTitleStringMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CTitleStringMemberNameU3Ek__BackingField_5() const { return ___U3CTitleStringMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTitleStringMemberNameU3Ek__BackingField_5() { return &___U3CTitleStringMemberNameU3Ek__BackingField_5; }
	inline void set_U3CTitleStringMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CTitleStringMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleStringMemberNameU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEGROUPATTRIBUTE_T12117724_H
#ifndef DETAILEDINFOBOXATTRIBUTE_T2666061416_H
#define DETAILEDINFOBOXATTRIBUTE_T2666061416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct  DetailedInfoBoxAttribute_t2666061416  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Message
	String_t* ___Message_0;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Details
	String_t* ___Details_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.DetailedInfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_2;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_3;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}

	inline static int32_t get_offset_of_Details_1() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___Details_1)); }
	inline String_t* get_Details_1() const { return ___Details_1; }
	inline String_t** get_address_of_Details_1() { return &___Details_1; }
	inline void set_Details_1(String_t* value)
	{
		___Details_1 = value;
		Il2CppCodeGenWriteBarrier((&___Details_1), value);
	}

	inline static int32_t get_offset_of_InfoMessageType_2() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___InfoMessageType_2)); }
	inline int32_t get_InfoMessageType_2() const { return ___InfoMessageType_2; }
	inline int32_t* get_address_of_InfoMessageType_2() { return &___InfoMessageType_2; }
	inline void set_InfoMessageType_2(int32_t value)
	{
		___InfoMessageType_2 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_3() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t2666061416, ___VisibleIf_3)); }
	inline String_t* get_VisibleIf_3() const { return ___VisibleIf_3; }
	inline String_t** get_address_of_VisibleIf_3() { return &___VisibleIf_3; }
	inline void set_VisibleIf_3(String_t* value)
	{
		___VisibleIf_3 = value;
		Il2CppCodeGenWriteBarrier((&___VisibleIf_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETAILEDINFOBOXATTRIBUTE_T2666061416_H
#ifndef DICTIONARYDRAWERSETTINGS_T3408197424_H
#define DICTIONARYDRAWERSETTINGS_T3408197424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.DictionaryDrawerSettings
struct  DictionaryDrawerSettings_t3408197424  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::KeyLabel
	String_t* ___KeyLabel_0;
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::ValueLabel
	String_t* ___ValueLabel_1;
	// Sirenix.OdinInspector.DictionaryDisplayOptions Sirenix.OdinInspector.DictionaryDrawerSettings::DisplayMode
	int32_t ___DisplayMode_2;
	// System.Boolean Sirenix.OdinInspector.DictionaryDrawerSettings::IsReadOnly
	bool ___IsReadOnly_3;

public:
	inline static int32_t get_offset_of_KeyLabel_0() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___KeyLabel_0)); }
	inline String_t* get_KeyLabel_0() const { return ___KeyLabel_0; }
	inline String_t** get_address_of_KeyLabel_0() { return &___KeyLabel_0; }
	inline void set_KeyLabel_0(String_t* value)
	{
		___KeyLabel_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeyLabel_0), value);
	}

	inline static int32_t get_offset_of_ValueLabel_1() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___ValueLabel_1)); }
	inline String_t* get_ValueLabel_1() const { return ___ValueLabel_1; }
	inline String_t** get_address_of_ValueLabel_1() { return &___ValueLabel_1; }
	inline void set_ValueLabel_1(String_t* value)
	{
		___ValueLabel_1 = value;
		Il2CppCodeGenWriteBarrier((&___ValueLabel_1), value);
	}

	inline static int32_t get_offset_of_DisplayMode_2() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___DisplayMode_2)); }
	inline int32_t get_DisplayMode_2() const { return ___DisplayMode_2; }
	inline int32_t* get_address_of_DisplayMode_2() { return &___DisplayMode_2; }
	inline void set_DisplayMode_2(int32_t value)
	{
		___DisplayMode_2 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_3() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_t3408197424, ___IsReadOnly_3)); }
	inline bool get_IsReadOnly_3() const { return ___IsReadOnly_3; }
	inline bool* get_address_of_IsReadOnly_3() { return &___IsReadOnly_3; }
	inline void set_IsReadOnly_3(bool value)
	{
		___IsReadOnly_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYDRAWERSETTINGS_T3408197424_H
#ifndef INFOBOXATTRIBUTE_T1629795541_H
#define INFOBOXATTRIBUTE_T1629795541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InfoBoxAttribute
struct  InfoBoxAttribute_t1629795541  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::Message
	String_t* ___Message_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.InfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_1;
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_2;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t1629795541, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}

	inline static int32_t get_offset_of_InfoMessageType_1() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t1629795541, ___InfoMessageType_1)); }
	inline int32_t get_InfoMessageType_1() const { return ___InfoMessageType_1; }
	inline int32_t* get_address_of_InfoMessageType_1() { return &___InfoMessageType_1; }
	inline void set_InfoMessageType_1(int32_t value)
	{
		___InfoMessageType_1 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_2() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t1629795541, ___VisibleIf_2)); }
	inline String_t* get_VisibleIf_2() const { return ___VisibleIf_2; }
	inline String_t** get_address_of_VisibleIf_2() { return &___VisibleIf_2; }
	inline void set_VisibleIf_2(String_t* value)
	{
		___VisibleIf_2 = value;
		Il2CppCodeGenWriteBarrier((&___VisibleIf_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFOBOXATTRIBUTE_T1629795541_H
#ifndef INLINEEDITORATTRIBUTE_T803766710_H
#define INLINEEDITORATTRIBUTE_T803766710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.InlineEditorAttribute
struct  InlineEditorAttribute_t803766710  : public Attribute_t861562559
{
public:
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::Expanded
	bool ___Expanded_0;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawHeader
	bool ___DrawHeader_1;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawGUI
	bool ___DrawGUI_2;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawPreview
	bool ___DrawPreview_3;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::MaxHeight
	float ___MaxHeight_4;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewWidth
	float ___PreviewWidth_5;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewHeight
	float ___PreviewHeight_6;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::IncrementInlineEditorDrawerDepth
	bool ___IncrementInlineEditorDrawerDepth_7;
	// Sirenix.OdinInspector.InlineEditorObjectFieldModes Sirenix.OdinInspector.InlineEditorAttribute::ObjectFieldMode
	int32_t ___ObjectFieldMode_8;

public:
	inline static int32_t get_offset_of_Expanded_0() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___Expanded_0)); }
	inline bool get_Expanded_0() const { return ___Expanded_0; }
	inline bool* get_address_of_Expanded_0() { return &___Expanded_0; }
	inline void set_Expanded_0(bool value)
	{
		___Expanded_0 = value;
	}

	inline static int32_t get_offset_of_DrawHeader_1() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___DrawHeader_1)); }
	inline bool get_DrawHeader_1() const { return ___DrawHeader_1; }
	inline bool* get_address_of_DrawHeader_1() { return &___DrawHeader_1; }
	inline void set_DrawHeader_1(bool value)
	{
		___DrawHeader_1 = value;
	}

	inline static int32_t get_offset_of_DrawGUI_2() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___DrawGUI_2)); }
	inline bool get_DrawGUI_2() const { return ___DrawGUI_2; }
	inline bool* get_address_of_DrawGUI_2() { return &___DrawGUI_2; }
	inline void set_DrawGUI_2(bool value)
	{
		___DrawGUI_2 = value;
	}

	inline static int32_t get_offset_of_DrawPreview_3() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___DrawPreview_3)); }
	inline bool get_DrawPreview_3() const { return ___DrawPreview_3; }
	inline bool* get_address_of_DrawPreview_3() { return &___DrawPreview_3; }
	inline void set_DrawPreview_3(bool value)
	{
		___DrawPreview_3 = value;
	}

	inline static int32_t get_offset_of_MaxHeight_4() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___MaxHeight_4)); }
	inline float get_MaxHeight_4() const { return ___MaxHeight_4; }
	inline float* get_address_of_MaxHeight_4() { return &___MaxHeight_4; }
	inline void set_MaxHeight_4(float value)
	{
		___MaxHeight_4 = value;
	}

	inline static int32_t get_offset_of_PreviewWidth_5() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___PreviewWidth_5)); }
	inline float get_PreviewWidth_5() const { return ___PreviewWidth_5; }
	inline float* get_address_of_PreviewWidth_5() { return &___PreviewWidth_5; }
	inline void set_PreviewWidth_5(float value)
	{
		___PreviewWidth_5 = value;
	}

	inline static int32_t get_offset_of_PreviewHeight_6() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___PreviewHeight_6)); }
	inline float get_PreviewHeight_6() const { return ___PreviewHeight_6; }
	inline float* get_address_of_PreviewHeight_6() { return &___PreviewHeight_6; }
	inline void set_PreviewHeight_6(float value)
	{
		___PreviewHeight_6 = value;
	}

	inline static int32_t get_offset_of_IncrementInlineEditorDrawerDepth_7() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___IncrementInlineEditorDrawerDepth_7)); }
	inline bool get_IncrementInlineEditorDrawerDepth_7() const { return ___IncrementInlineEditorDrawerDepth_7; }
	inline bool* get_address_of_IncrementInlineEditorDrawerDepth_7() { return &___IncrementInlineEditorDrawerDepth_7; }
	inline void set_IncrementInlineEditorDrawerDepth_7(bool value)
	{
		___IncrementInlineEditorDrawerDepth_7 = value;
	}

	inline static int32_t get_offset_of_ObjectFieldMode_8() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_t803766710, ___ObjectFieldMode_8)); }
	inline int32_t get_ObjectFieldMode_8() const { return ___ObjectFieldMode_8; }
	inline int32_t* get_address_of_ObjectFieldMode_8() { return &___ObjectFieldMode_8; }
	inline void set_ObjectFieldMode_8(int32_t value)
	{
		___ObjectFieldMode_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INLINEEDITORATTRIBUTE_T803766710_H
#ifndef REQUIREDATTRIBUTE_T1745431873_H
#define REQUIREDATTRIBUTE_T1745431873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.RequiredAttribute
struct  RequiredAttribute_t1745431873  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.RequiredAttribute::ErrorMessage
	String_t* ___ErrorMessage_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.RequiredAttribute::MessageType
	int32_t ___MessageType_1;

public:
	inline static int32_t get_offset_of_ErrorMessage_0() { return static_cast<int32_t>(offsetof(RequiredAttribute_t1745431873, ___ErrorMessage_0)); }
	inline String_t* get_ErrorMessage_0() const { return ___ErrorMessage_0; }
	inline String_t** get_address_of_ErrorMessage_0() { return &___ErrorMessage_0; }
	inline void set_ErrorMessage_0(String_t* value)
	{
		___ErrorMessage_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorMessage_0), value);
	}

	inline static int32_t get_offset_of_MessageType_1() { return static_cast<int32_t>(offsetof(RequiredAttribute_t1745431873, ___MessageType_1)); }
	inline int32_t get_MessageType_1() const { return ___MessageType_1; }
	inline int32_t* get_address_of_MessageType_1() { return &___MessageType_1; }
	inline void set_MessageType_1(int32_t value)
	{
		___MessageType_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDATTRIBUTE_T1745431873_H
#ifndef TITLEATTRIBUTE_T3554241315_H
#define TITLEATTRIBUTE_T3554241315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TitleAttribute
struct  TitleAttribute_t3554241315  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.TitleAttribute::Title
	String_t* ___Title_0;
	// System.String Sirenix.OdinInspector.TitleAttribute::Subtitle
	String_t* ___Subtitle_1;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::Bold
	bool ___Bold_2;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::HorizontalLine
	bool ___HorizontalLine_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleAttribute::TitleAlignment
	int32_t ___TitleAlignment_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((&___Title_0), value);
	}

	inline static int32_t get_offset_of_Subtitle_1() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___Subtitle_1)); }
	inline String_t* get_Subtitle_1() const { return ___Subtitle_1; }
	inline String_t** get_address_of_Subtitle_1() { return &___Subtitle_1; }
	inline void set_Subtitle_1(String_t* value)
	{
		___Subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Subtitle_1), value);
	}

	inline static int32_t get_offset_of_Bold_2() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___Bold_2)); }
	inline bool get_Bold_2() const { return ___Bold_2; }
	inline bool* get_address_of_Bold_2() { return &___Bold_2; }
	inline void set_Bold_2(bool value)
	{
		___Bold_2 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_3() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___HorizontalLine_3)); }
	inline bool get_HorizontalLine_3() const { return ___HorizontalLine_3; }
	inline bool* get_address_of_HorizontalLine_3() { return &___HorizontalLine_3; }
	inline void set_HorizontalLine_3(bool value)
	{
		___HorizontalLine_3 = value;
	}

	inline static int32_t get_offset_of_TitleAlignment_4() { return static_cast<int32_t>(offsetof(TitleAttribute_t3554241315, ___TitleAlignment_4)); }
	inline int32_t get_TitleAlignment_4() const { return ___TitleAlignment_4; }
	inline int32_t* get_address_of_TitleAlignment_4() { return &___TitleAlignment_4; }
	inline void set_TitleAlignment_4(int32_t value)
	{
		___TitleAlignment_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEATTRIBUTE_T3554241315_H
#ifndef TITLEGROUPATTRIBUTE_T2185192947_H
#define TITLEGROUPATTRIBUTE_T2185192947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.TitleGroupAttribute
struct  TitleGroupAttribute_t2185192947  : public PropertyGroupAttribute_t2009328757
{
public:
	// System.String Sirenix.OdinInspector.TitleGroupAttribute::Subtitle
	String_t* ___Subtitle_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleGroupAttribute::Alignment
	int32_t ___Alignment_4;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::HorizontalLine
	bool ___HorizontalLine_5;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::BoldTitle
	bool ___BoldTitle_6;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::Indent
	bool ___Indent_7;

public:
	inline static int32_t get_offset_of_Subtitle_3() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___Subtitle_3)); }
	inline String_t* get_Subtitle_3() const { return ___Subtitle_3; }
	inline String_t** get_address_of_Subtitle_3() { return &___Subtitle_3; }
	inline void set_Subtitle_3(String_t* value)
	{
		___Subtitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___Subtitle_3), value);
	}

	inline static int32_t get_offset_of_Alignment_4() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___Alignment_4)); }
	inline int32_t get_Alignment_4() const { return ___Alignment_4; }
	inline int32_t* get_address_of_Alignment_4() { return &___Alignment_4; }
	inline void set_Alignment_4(int32_t value)
	{
		___Alignment_4 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_5() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___HorizontalLine_5)); }
	inline bool get_HorizontalLine_5() const { return ___HorizontalLine_5; }
	inline bool* get_address_of_HorizontalLine_5() { return &___HorizontalLine_5; }
	inline void set_HorizontalLine_5(bool value)
	{
		___HorizontalLine_5 = value;
	}

	inline static int32_t get_offset_of_BoldTitle_6() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___BoldTitle_6)); }
	inline bool get_BoldTitle_6() const { return ___BoldTitle_6; }
	inline bool* get_address_of_BoldTitle_6() { return &___BoldTitle_6; }
	inline void set_BoldTitle_6(bool value)
	{
		___BoldTitle_6 = value;
	}

	inline static int32_t get_offset_of_Indent_7() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_t2185192947, ___Indent_7)); }
	inline bool get_Indent_7() const { return ___Indent_7; }
	inline bool* get_address_of_Indent_7() { return &___Indent_7; }
	inline void set_Indent_7(bool value)
	{
		___Indent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEGROUPATTRIBUTE_T2185192947_H
#ifndef VALIDATEINPUTATTRIBUTE_T471509373_H
#define VALIDATEINPUTATTRIBUTE_T471509373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.ValidateInputAttribute
struct  ValidateInputAttribute_t471509373  : public Attribute_t861562559
{
public:
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::DefaultMessage
	String_t* ___DefaultMessage_0;
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::MemberName
	String_t* ___MemberName_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.ValidateInputAttribute::MessageType
	int32_t ___MessageType_2;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::IncludeChildren
	bool ___IncludeChildren_3;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::ContiniousValidationCheck
	bool ___ContiniousValidationCheck_4;

public:
	inline static int32_t get_offset_of_DefaultMessage_0() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___DefaultMessage_0)); }
	inline String_t* get_DefaultMessage_0() const { return ___DefaultMessage_0; }
	inline String_t** get_address_of_DefaultMessage_0() { return &___DefaultMessage_0; }
	inline void set_DefaultMessage_0(String_t* value)
	{
		___DefaultMessage_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMessage_0), value);
	}

	inline static int32_t get_offset_of_MemberName_1() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___MemberName_1)); }
	inline String_t* get_MemberName_1() const { return ___MemberName_1; }
	inline String_t** get_address_of_MemberName_1() { return &___MemberName_1; }
	inline void set_MemberName_1(String_t* value)
	{
		___MemberName_1 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_1), value);
	}

	inline static int32_t get_offset_of_MessageType_2() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___MessageType_2)); }
	inline int32_t get_MessageType_2() const { return ___MessageType_2; }
	inline int32_t* get_address_of_MessageType_2() { return &___MessageType_2; }
	inline void set_MessageType_2(int32_t value)
	{
		___MessageType_2 = value;
	}

	inline static int32_t get_offset_of_IncludeChildren_3() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___IncludeChildren_3)); }
	inline bool get_IncludeChildren_3() const { return ___IncludeChildren_3; }
	inline bool* get_address_of_IncludeChildren_3() { return &___IncludeChildren_3; }
	inline void set_IncludeChildren_3(bool value)
	{
		___IncludeChildren_3 = value;
	}

	inline static int32_t get_offset_of_ContiniousValidationCheck_4() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_t471509373, ___ContiniousValidationCheck_4)); }
	inline bool get_ContiniousValidationCheck_4() const { return ___ContiniousValidationCheck_4; }
	inline bool* get_address_of_ContiniousValidationCheck_4() { return &___ContiniousValidationCheck_4; }
	inline void set_ContiniousValidationCheck_4(bool value)
	{
		___ContiniousValidationCheck_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATEINPUTATTRIBUTE_T471509373_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (TitleAlignments_t2525535876)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3100[5] = 
{
	TitleAlignments_t2525535876::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (TitleGroupAttribute_t2185192947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[5] = 
{
	TitleGroupAttribute_t2185192947::get_offset_of_Subtitle_3(),
	TitleGroupAttribute_t2185192947::get_offset_of_Alignment_4(),
	TitleGroupAttribute_t2185192947::get_offset_of_HorizontalLine_5(),
	TitleGroupAttribute_t2185192947::get_offset_of_BoldTitle_6(),
	TitleGroupAttribute_t2185192947::get_offset_of_Indent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (ButtonSizes_t675943090)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3102[5] = 
{
	ButtonSizes_t675943090::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (ColorPaletteAttribute_t392531492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[2] = 
{
	ColorPaletteAttribute_t392531492::get_offset_of_PaletteName_0(),
	ColorPaletteAttribute_t392531492::get_offset_of_ShowAlpha_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (CustomContextMenuAttribute_t4264451314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[2] = 
{
	CustomContextMenuAttribute_t4264451314::get_offset_of_MenuItem_0(),
	CustomContextMenuAttribute_t4264451314::get_offset_of_MethodName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (DelayedPropertyAttribute_t1141984869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (DetailedInfoBoxAttribute_t2666061416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[4] = 
{
	DetailedInfoBoxAttribute_t2666061416::get_offset_of_Message_0(),
	DetailedInfoBoxAttribute_t2666061416::get_offset_of_Details_1(),
	DetailedInfoBoxAttribute_t2666061416::get_offset_of_InfoMessageType_2(),
	DetailedInfoBoxAttribute_t2666061416::get_offset_of_VisibleIf_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (DictionaryDisplayOptions_t2340992491)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3107[5] = 
{
	DictionaryDisplayOptions_t2340992491::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (DictionaryDrawerSettings_t3408197424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[4] = 
{
	DictionaryDrawerSettings_t3408197424::get_offset_of_KeyLabel_0(),
	DictionaryDrawerSettings_t3408197424::get_offset_of_ValueLabel_1(),
	DictionaryDrawerSettings_t3408197424::get_offset_of_DisplayMode_2(),
	DictionaryDrawerSettings_t3408197424::get_offset_of_IsReadOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (DisableContextMenuAttribute_t184909590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[2] = 
{
	DisableContextMenuAttribute_t184909590::get_offset_of_DisableForMember_0(),
	DisableContextMenuAttribute_t184909590::get_offset_of_DisableForCollectionElements_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (DisableIfAttribute_t2867643591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[2] = 
{
	DisableIfAttribute_t2867643591::get_offset_of_MemberName_0(),
	DisableIfAttribute_t2867643591::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (DisableInEditorModeAttribute_t630823232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (DisableInPlayModeAttribute_t3352669054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (DisplayAsStringAttribute_t1002957046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[1] = 
{
	DisplayAsStringAttribute_t1002957046::get_offset_of_Overflow_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (DontApplyToListElementsAttribute_t3002873312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (DrawWithUnityAttribute_t2993498865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (InlineButtonAttribute_t2729761977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[2] = 
{
	InlineButtonAttribute_t2729761977::get_offset_of_U3CMemberMethodU3Ek__BackingField_0(),
	InlineButtonAttribute_t2729761977::get_offset_of_U3CLabelU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (ShowForPrefabOnlyAttribute_t4233203441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (EnableForPrefabOnlyAttribute_t2962074089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (EnableIfAttribute_t729670423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[2] = 
{
	EnableIfAttribute_t729670423::get_offset_of_MemberName_0(),
	EnableIfAttribute_t729670423::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (EnumToggleButtonsAttribute_t3538214787), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (HideIfAttribute_t2697016173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[3] = 
{
	HideIfAttribute_t2697016173::get_offset_of_MemberName_0(),
	HideIfAttribute_t2697016173::get_offset_of_Value_1(),
	HideIfAttribute_t2697016173::get_offset_of_Animate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (HideInPlayModeAttribute_t3220251466), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (HideInEditorModeAttribute_t1562875995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (HideMonoScriptAttribute_t3453138722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (HideReferenceObjectPickerAttribute_t3431975763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (HorizontalGroupAttribute_t2886913732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[9] = 
{
	HorizontalGroupAttribute_t2886913732::get_offset_of_Width_3(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_MarginLeft_4(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_MarginRight_5(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_PaddingLeft_6(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_PaddingRight_7(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_MinWidth_8(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_MaxWidth_9(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_Title_10(),
	HorizontalGroupAttribute_t2886913732::get_offset_of_LabelWidth_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (InlineEditorAttribute_t803766710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[9] = 
{
	InlineEditorAttribute_t803766710::get_offset_of_Expanded_0(),
	InlineEditorAttribute_t803766710::get_offset_of_DrawHeader_1(),
	InlineEditorAttribute_t803766710::get_offset_of_DrawGUI_2(),
	InlineEditorAttribute_t803766710::get_offset_of_DrawPreview_3(),
	InlineEditorAttribute_t803766710::get_offset_of_MaxHeight_4(),
	InlineEditorAttribute_t803766710::get_offset_of_PreviewWidth_5(),
	InlineEditorAttribute_t803766710::get_offset_of_PreviewHeight_6(),
	InlineEditorAttribute_t803766710::get_offset_of_IncrementInlineEditorDrawerDepth_7(),
	InlineEditorAttribute_t803766710::get_offset_of_ObjectFieldMode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (SuppressInvalidAttributeErrorAttribute_t4105959342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (InlineEditorModes_t2896600093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3129[7] = 
{
	InlineEditorModes_t2896600093::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (ShowDrawerChainAttribute_t1144545212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (ShowOdinSerializedPropertiesInInspectorAttribute_t1684166398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (FoldoutGroupAttribute_t1921228823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[2] = 
{
	FoldoutGroupAttribute_t1921228823::get_offset_of_Expanded_3(),
	FoldoutGroupAttribute_t1921228823::get_offset_of_U3CHasDefinedExpandedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (GUIColorAttribute_t2790983735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[2] = 
{
	GUIColorAttribute_t2790983735::get_offset_of_Color_0(),
	GUIColorAttribute_t2790983735::get_offset_of_GetColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (HideLabelAttribute_t3755575312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (IndentAttribute_t159299566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[1] = 
{
	IndentAttribute_t159299566::get_offset_of_IndentLevel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (InfoBoxAttribute_t1629795541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[3] = 
{
	InfoBoxAttribute_t1629795541::get_offset_of_Message_0(),
	InfoBoxAttribute_t1629795541::get_offset_of_InfoMessageType_1(),
	InfoBoxAttribute_t1629795541::get_offset_of_VisibleIf_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (MinMaxSliderAttribute_t1448807924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[6] = 
{
	MinMaxSliderAttribute_t1448807924::get_offset_of_MinValue_0(),
	MinMaxSliderAttribute_t1448807924::get_offset_of_MaxValue_1(),
	MinMaxSliderAttribute_t1448807924::get_offset_of_MinMember_2(),
	MinMaxSliderAttribute_t1448807924::get_offset_of_MaxMember_3(),
	MinMaxSliderAttribute_t1448807924::get_offset_of_MinMaxMember_4(),
	MinMaxSliderAttribute_t1448807924::get_offset_of_ShowFields_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (ToggleLeftAttribute_t2985281771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (LabelTextAttribute_t2020830118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[1] = 
{
	LabelTextAttribute_t2020830118::get_offset_of_Text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (ListDrawerSettingsAttribute_t1308946087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[24] = 
{
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_HideAddButton_0(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_HideRemoveButton_1(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_ListElementLabelName_2(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_CustomAddFunction_3(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_CustomRemoveIndexFunction_4(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_CustomRemoveElementFunction_5(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_OnBeginListElementGUI_6(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_OnEndListElementGUI_7(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_AlwaysAddDefaultValue_8(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_onTitleBarGUI_9(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_numberOfItemsPerPage_10(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_paging_11(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_draggable_12(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_isReadOnly_13(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_showItemCount_14(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_pagingHasValue_15(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_draggableHasValue_16(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_isReadOnlyHasValue_17(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_showItemCountHasValue_18(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_expanded_19(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_expandedHasValue_20(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_numberOfItemsPerPageHasValue_21(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_showIndexLabels_22(),
	ListDrawerSettingsAttribute_t1308946087::get_offset_of_showIndexLabelsHasValue_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (MaxValueAttribute_t1325324426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[1] = 
{
	MaxValueAttribute_t1325324426::get_offset_of_MaxValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (MinValueAttribute_t1691372301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[1] = 
{
	MinValueAttribute_t1691372301::get_offset_of_MinValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (MultiLinePropertyAttribute_t1222183101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[1] = 
{
	MultiLinePropertyAttribute_t1222183101::get_offset_of_Lines_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (PropertyTooltipAttribute_t1909666584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[1] = 
{
	PropertyTooltipAttribute_t1909666584::get_offset_of_Tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (ReadOnlyAttribute_t3311022813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (OnInspectorGUIAttribute_t3458889380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[2] = 
{
	OnInspectorGUIAttribute_t3458889380::get_offset_of_PrependMethodName_0(),
	OnInspectorGUIAttribute_t3458889380::get_offset_of_AppendMethodName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (OnValueChangedAttribute_t870857610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[2] = 
{
	OnValueChangedAttribute_t870857610::get_offset_of_MethodName_0(),
	OnValueChangedAttribute_t870857610::get_offset_of_IncludeChildren_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (PropertyGroupAttribute_t2009328757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[3] = 
{
	PropertyGroupAttribute_t2009328757::get_offset_of_GroupID_0(),
	PropertyGroupAttribute_t2009328757::get_offset_of_GroupName_1(),
	PropertyGroupAttribute_t2009328757::get_offset_of_Order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (PropertyOrderAttribute_t2594707638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[1] = 
{
	PropertyOrderAttribute_t2594707638::get_offset_of_Order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (RequiredAttribute_t1745431873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[2] = 
{
	RequiredAttribute_t1745431873::get_offset_of_ErrorMessage_0(),
	RequiredAttribute_t1745431873::get_offset_of_MessageType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (SceneObjectsOnlyAttribute_t3518277911), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (ValueDropdownAttribute_t2212993645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[16] = 
{
	ValueDropdownAttribute_t2212993645::get_offset_of_MemberName_0(),
	ValueDropdownAttribute_t2212993645::get_offset_of_NumberOfItemsBeforeEnablingSearch_1(),
	ValueDropdownAttribute_t2212993645::get_offset_of_IsUniqueList_2(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DrawDropdownForListElements_3(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DisableListAddButtonBehaviour_4(),
	ValueDropdownAttribute_t2212993645::get_offset_of_ExcludeExistingValuesInList_5(),
	ValueDropdownAttribute_t2212993645::get_offset_of_ExpandAllMenuItems_6(),
	ValueDropdownAttribute_t2212993645::get_offset_of_AppendNextDrawer_7(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DisableGUIInAppendedDrawer_8(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DoubleClickToConfirm_9(),
	ValueDropdownAttribute_t2212993645::get_offset_of_FlattenTreeView_10(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DropdownWidth_11(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DropdownHeight_12(),
	ValueDropdownAttribute_t2212993645::get_offset_of_DropdownTitle_13(),
	ValueDropdownAttribute_t2212993645::get_offset_of_SortDropdownItems_14(),
	ValueDropdownAttribute_t2212993645::get_offset_of_HideChildProperties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (ValueDropdownItem_t3457888829)+ sizeof (RuntimeObject), sizeof(ValueDropdownItem_t3457888829_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3155[2] = 
{
	ValueDropdownItem_t3457888829::get_offset_of_Text_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ValueDropdownItem_t3457888829::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (ShowInInspectorAttribute_t2100412880), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (TabGroupAttribute_t2295443652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[5] = 
{
	0,
	TabGroupAttribute_t2295443652::get_offset_of_TabName_4(),
	TabGroupAttribute_t2295443652::get_offset_of_UseFixedHeight_5(),
	TabGroupAttribute_t2295443652::get_offset_of_Paddingless_6(),
	TabGroupAttribute_t2295443652::get_offset_of_U3CTabsU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (TabSubGroupAttribute_t867681421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (TitleAttribute_t3554241315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[5] = 
{
	TitleAttribute_t3554241315::get_offset_of_Title_0(),
	TitleAttribute_t3554241315::get_offset_of_Subtitle_1(),
	TitleAttribute_t3554241315::get_offset_of_Bold_2(),
	TitleAttribute_t3554241315::get_offset_of_HorizontalLine_3(),
	TitleAttribute_t3554241315::get_offset_of_TitleAlignment_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (ToggleAttribute_t3765571124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[2] = 
{
	ToggleAttribute_t3765571124::get_offset_of_ToggleMemberName_0(),
	ToggleAttribute_t3765571124::get_offset_of_CollapseOthersOnExpand_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (ToggleGroupAttribute_t12117724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[3] = 
{
	ToggleGroupAttribute_t12117724::get_offset_of_ToggleGroupTitle_3(),
	ToggleGroupAttribute_t12117724::get_offset_of_CollapseOthersOnExpand_4(),
	ToggleGroupAttribute_t12117724::get_offset_of_U3CTitleStringMemberNameU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (ValidateInputAttribute_t471509373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[5] = 
{
	ValidateInputAttribute_t471509373::get_offset_of_DefaultMessage_0(),
	ValidateInputAttribute_t471509373::get_offset_of_MemberName_1(),
	ValidateInputAttribute_t471509373::get_offset_of_MessageType_2(),
	ValidateInputAttribute_t471509373::get_offset_of_IncludeChildren_3(),
	ValidateInputAttribute_t471509373::get_offset_of_ContiniousValidationCheck_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (ShowIfAttribute_t2614791958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[3] = 
{
	ShowIfAttribute_t2614791958::get_offset_of_MemberName_0(),
	ShowIfAttribute_t2614791958::get_offset_of_Animate_1(),
	ShowIfAttribute_t2614791958::get_offset_of_Value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (WrapAttribute_t589917550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[2] = 
{
	WrapAttribute_t589917550::get_offset_of_Min_0(),
	WrapAttribute_t589917550::get_offset_of_Max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (InfoMessageType_t2920171874)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3166[5] = 
{
	InfoMessageType_t2920171874::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (ColorExtensions_t628298469), -1, sizeof(ColorExtensions_t628298469_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3169[1] = 
{
	ColorExtensions_t628298469_StaticFields::get_offset_of_trimRGBStart_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (DelegateExtensions_t3908113990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (FieldInfoExtensions_t3875362345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (GarbageFreeIterators_t2664762835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (LinqExtensions_t650130004), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3187[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3192[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3193[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3197[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
