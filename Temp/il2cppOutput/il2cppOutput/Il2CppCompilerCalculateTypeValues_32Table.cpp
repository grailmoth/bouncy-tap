﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Sirenix.Utilities.Cache`1<Sirenix.Utilities.MemberFinder>
struct Cache_1_t1308278200;
// Sirenix.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate>
struct DoubleLookupDictionary_3_t658514733;
// Sirenix.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>
struct DoubleLookupDictionary_3_t1917252294;
// Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance[]
struct GUILayoutOptionsInstanceU5BU5D_t768349783;
// Sirenix.Utilities.ImmutableList
struct ImmutableList_t4021141801;
// Sirenix.Utilities.ImmutableList`1<System.Reflection.Assembly>
struct ImmutableList_1_t2953650640;
// Sirenix.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr>
struct ValueGetter_2_t3789590669;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance,UnityEngine.GUILayoutOption[]>
struct Dictionary_2_t1782032584;
// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,Sirenix.Utilities.AssemblyTypeFlags>
struct Dictionary_2_t246714273;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>
struct Dictionary_2_t3493241298;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t4291797753;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t633324528;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>
struct IEnumerator_1_t3812572209;
// System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>
struct List_1_t2683290888;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t1279540245;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.IList
struct IList_t2094931216;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_t2217434578;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t3487522507;
// System.Func`2<System.Type,System.String>
struct Func_2_t2311415679;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1302094432;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ARRAYUTILITIES_T3509714228_H
#define ARRAYUTILITIES_T3509714228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.ArrayUtilities
struct  ArrayUtilities_t3509714228  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYUTILITIES_T3509714228_H
#ifndef ASSEMBLYUTILITIES_T2880599301_H
#define ASSEMBLYUTILITIES_T2880599301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.AssemblyUtilities
struct  AssemblyUtilities_t2880599301  : public RuntimeObject
{
public:

public:
};

struct AssemblyUtilities_t2880599301_StaticFields
{
public:
	// System.String[] Sirenix.Utilities.AssemblyUtilities::userAssemblyPrefixes
	StringU5BU5D_t1281789340* ___userAssemblyPrefixes_0;
	// System.String[] Sirenix.Utilities.AssemblyUtilities::pluginAssemblyPrefixes
	StringU5BU5D_t1281789340* ___pluginAssemblyPrefixes_1;
	// System.Reflection.Assembly Sirenix.Utilities.AssemblyUtilities::unityEngineAssembly
	Assembly_t * ___unityEngineAssembly_2;
	// System.Object Sirenix.Utilities.AssemblyUtilities::LOCK
	RuntimeObject * ___LOCK_3;
	// System.IO.DirectoryInfo Sirenix.Utilities.AssemblyUtilities::projectFolderDirectory
	DirectoryInfo_t35957480 * ___projectFolderDirectory_4;
	// System.IO.DirectoryInfo Sirenix.Utilities.AssemblyUtilities::scriptAssembliesDirectory
	DirectoryInfo_t35957480 * ___scriptAssembliesDirectory_5;
	// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,Sirenix.Utilities.AssemblyTypeFlags> Sirenix.Utilities.AssemblyUtilities::assemblyTypeFlagLookup
	Dictionary_2_t246714273 * ___assemblyTypeFlagLookup_6;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::allAssemblies
	List_1_t1279540245 * ___allAssemblies_7;
	// Sirenix.Utilities.ImmutableList`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::allAssembliesImmutable
	ImmutableList_1_t2953650640 * ___allAssembliesImmutable_8;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::userAssemblies
	List_1_t1279540245 * ___userAssemblies_9;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::userEditorAssemblies
	List_1_t1279540245 * ___userEditorAssemblies_10;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::pluginAssemblies
	List_1_t1279540245 * ___pluginAssemblies_11;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::pluginEditorAssemblies
	List_1_t1279540245 * ___pluginEditorAssemblies_12;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::unityAssemblies
	List_1_t1279540245 * ___unityAssemblies_13;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::unityEditorAssemblies
	List_1_t1279540245 * ___unityEditorAssemblies_14;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Utilities.AssemblyUtilities::otherAssemblies
	List_1_t1279540245 * ___otherAssemblies_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Sirenix.Utilities.AssemblyUtilities::stringTypeLookup
	Dictionary_2_t2269201059 * ___stringTypeLookup_16;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::userTypes
	List_1_t3956019502 * ___userTypes_17;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::userEditorTypes
	List_1_t3956019502 * ___userEditorTypes_18;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::pluginTypes
	List_1_t3956019502 * ___pluginTypes_19;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::pluginEditorTypes
	List_1_t3956019502 * ___pluginEditorTypes_20;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::unityTypes
	List_1_t3956019502 * ___unityTypes_21;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::unityEditorTypes
	List_1_t3956019502 * ___unityEditorTypes_22;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.AssemblyUtilities::otherTypes
	List_1_t3956019502 * ___otherTypes_23;
	// System.String Sirenix.Utilities.AssemblyUtilities::dataPath
	String_t* ___dataPath_24;
	// System.String Sirenix.Utilities.AssemblyUtilities::scriptAssembliesPath
	String_t* ___scriptAssembliesPath_25;

public:
	inline static int32_t get_offset_of_userAssemblyPrefixes_0() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___userAssemblyPrefixes_0)); }
	inline StringU5BU5D_t1281789340* get_userAssemblyPrefixes_0() const { return ___userAssemblyPrefixes_0; }
	inline StringU5BU5D_t1281789340** get_address_of_userAssemblyPrefixes_0() { return &___userAssemblyPrefixes_0; }
	inline void set_userAssemblyPrefixes_0(StringU5BU5D_t1281789340* value)
	{
		___userAssemblyPrefixes_0 = value;
		Il2CppCodeGenWriteBarrier((&___userAssemblyPrefixes_0), value);
	}

	inline static int32_t get_offset_of_pluginAssemblyPrefixes_1() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___pluginAssemblyPrefixes_1)); }
	inline StringU5BU5D_t1281789340* get_pluginAssemblyPrefixes_1() const { return ___pluginAssemblyPrefixes_1; }
	inline StringU5BU5D_t1281789340** get_address_of_pluginAssemblyPrefixes_1() { return &___pluginAssemblyPrefixes_1; }
	inline void set_pluginAssemblyPrefixes_1(StringU5BU5D_t1281789340* value)
	{
		___pluginAssemblyPrefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___pluginAssemblyPrefixes_1), value);
	}

	inline static int32_t get_offset_of_unityEngineAssembly_2() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___unityEngineAssembly_2)); }
	inline Assembly_t * get_unityEngineAssembly_2() const { return ___unityEngineAssembly_2; }
	inline Assembly_t ** get_address_of_unityEngineAssembly_2() { return &___unityEngineAssembly_2; }
	inline void set_unityEngineAssembly_2(Assembly_t * value)
	{
		___unityEngineAssembly_2 = value;
		Il2CppCodeGenWriteBarrier((&___unityEngineAssembly_2), value);
	}

	inline static int32_t get_offset_of_LOCK_3() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___LOCK_3)); }
	inline RuntimeObject * get_LOCK_3() const { return ___LOCK_3; }
	inline RuntimeObject ** get_address_of_LOCK_3() { return &___LOCK_3; }
	inline void set_LOCK_3(RuntimeObject * value)
	{
		___LOCK_3 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_3), value);
	}

	inline static int32_t get_offset_of_projectFolderDirectory_4() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___projectFolderDirectory_4)); }
	inline DirectoryInfo_t35957480 * get_projectFolderDirectory_4() const { return ___projectFolderDirectory_4; }
	inline DirectoryInfo_t35957480 ** get_address_of_projectFolderDirectory_4() { return &___projectFolderDirectory_4; }
	inline void set_projectFolderDirectory_4(DirectoryInfo_t35957480 * value)
	{
		___projectFolderDirectory_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectFolderDirectory_4), value);
	}

	inline static int32_t get_offset_of_scriptAssembliesDirectory_5() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___scriptAssembliesDirectory_5)); }
	inline DirectoryInfo_t35957480 * get_scriptAssembliesDirectory_5() const { return ___scriptAssembliesDirectory_5; }
	inline DirectoryInfo_t35957480 ** get_address_of_scriptAssembliesDirectory_5() { return &___scriptAssembliesDirectory_5; }
	inline void set_scriptAssembliesDirectory_5(DirectoryInfo_t35957480 * value)
	{
		___scriptAssembliesDirectory_5 = value;
		Il2CppCodeGenWriteBarrier((&___scriptAssembliesDirectory_5), value);
	}

	inline static int32_t get_offset_of_assemblyTypeFlagLookup_6() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___assemblyTypeFlagLookup_6)); }
	inline Dictionary_2_t246714273 * get_assemblyTypeFlagLookup_6() const { return ___assemblyTypeFlagLookup_6; }
	inline Dictionary_2_t246714273 ** get_address_of_assemblyTypeFlagLookup_6() { return &___assemblyTypeFlagLookup_6; }
	inline void set_assemblyTypeFlagLookup_6(Dictionary_2_t246714273 * value)
	{
		___assemblyTypeFlagLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyTypeFlagLookup_6), value);
	}

	inline static int32_t get_offset_of_allAssemblies_7() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___allAssemblies_7)); }
	inline List_1_t1279540245 * get_allAssemblies_7() const { return ___allAssemblies_7; }
	inline List_1_t1279540245 ** get_address_of_allAssemblies_7() { return &___allAssemblies_7; }
	inline void set_allAssemblies_7(List_1_t1279540245 * value)
	{
		___allAssemblies_7 = value;
		Il2CppCodeGenWriteBarrier((&___allAssemblies_7), value);
	}

	inline static int32_t get_offset_of_allAssembliesImmutable_8() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___allAssembliesImmutable_8)); }
	inline ImmutableList_1_t2953650640 * get_allAssembliesImmutable_8() const { return ___allAssembliesImmutable_8; }
	inline ImmutableList_1_t2953650640 ** get_address_of_allAssembliesImmutable_8() { return &___allAssembliesImmutable_8; }
	inline void set_allAssembliesImmutable_8(ImmutableList_1_t2953650640 * value)
	{
		___allAssembliesImmutable_8 = value;
		Il2CppCodeGenWriteBarrier((&___allAssembliesImmutable_8), value);
	}

	inline static int32_t get_offset_of_userAssemblies_9() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___userAssemblies_9)); }
	inline List_1_t1279540245 * get_userAssemblies_9() const { return ___userAssemblies_9; }
	inline List_1_t1279540245 ** get_address_of_userAssemblies_9() { return &___userAssemblies_9; }
	inline void set_userAssemblies_9(List_1_t1279540245 * value)
	{
		___userAssemblies_9 = value;
		Il2CppCodeGenWriteBarrier((&___userAssemblies_9), value);
	}

	inline static int32_t get_offset_of_userEditorAssemblies_10() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___userEditorAssemblies_10)); }
	inline List_1_t1279540245 * get_userEditorAssemblies_10() const { return ___userEditorAssemblies_10; }
	inline List_1_t1279540245 ** get_address_of_userEditorAssemblies_10() { return &___userEditorAssemblies_10; }
	inline void set_userEditorAssemblies_10(List_1_t1279540245 * value)
	{
		___userEditorAssemblies_10 = value;
		Il2CppCodeGenWriteBarrier((&___userEditorAssemblies_10), value);
	}

	inline static int32_t get_offset_of_pluginAssemblies_11() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___pluginAssemblies_11)); }
	inline List_1_t1279540245 * get_pluginAssemblies_11() const { return ___pluginAssemblies_11; }
	inline List_1_t1279540245 ** get_address_of_pluginAssemblies_11() { return &___pluginAssemblies_11; }
	inline void set_pluginAssemblies_11(List_1_t1279540245 * value)
	{
		___pluginAssemblies_11 = value;
		Il2CppCodeGenWriteBarrier((&___pluginAssemblies_11), value);
	}

	inline static int32_t get_offset_of_pluginEditorAssemblies_12() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___pluginEditorAssemblies_12)); }
	inline List_1_t1279540245 * get_pluginEditorAssemblies_12() const { return ___pluginEditorAssemblies_12; }
	inline List_1_t1279540245 ** get_address_of_pluginEditorAssemblies_12() { return &___pluginEditorAssemblies_12; }
	inline void set_pluginEditorAssemblies_12(List_1_t1279540245 * value)
	{
		___pluginEditorAssemblies_12 = value;
		Il2CppCodeGenWriteBarrier((&___pluginEditorAssemblies_12), value);
	}

	inline static int32_t get_offset_of_unityAssemblies_13() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___unityAssemblies_13)); }
	inline List_1_t1279540245 * get_unityAssemblies_13() const { return ___unityAssemblies_13; }
	inline List_1_t1279540245 ** get_address_of_unityAssemblies_13() { return &___unityAssemblies_13; }
	inline void set_unityAssemblies_13(List_1_t1279540245 * value)
	{
		___unityAssemblies_13 = value;
		Il2CppCodeGenWriteBarrier((&___unityAssemblies_13), value);
	}

	inline static int32_t get_offset_of_unityEditorAssemblies_14() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___unityEditorAssemblies_14)); }
	inline List_1_t1279540245 * get_unityEditorAssemblies_14() const { return ___unityEditorAssemblies_14; }
	inline List_1_t1279540245 ** get_address_of_unityEditorAssemblies_14() { return &___unityEditorAssemblies_14; }
	inline void set_unityEditorAssemblies_14(List_1_t1279540245 * value)
	{
		___unityEditorAssemblies_14 = value;
		Il2CppCodeGenWriteBarrier((&___unityEditorAssemblies_14), value);
	}

	inline static int32_t get_offset_of_otherAssemblies_15() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___otherAssemblies_15)); }
	inline List_1_t1279540245 * get_otherAssemblies_15() const { return ___otherAssemblies_15; }
	inline List_1_t1279540245 ** get_address_of_otherAssemblies_15() { return &___otherAssemblies_15; }
	inline void set_otherAssemblies_15(List_1_t1279540245 * value)
	{
		___otherAssemblies_15 = value;
		Il2CppCodeGenWriteBarrier((&___otherAssemblies_15), value);
	}

	inline static int32_t get_offset_of_stringTypeLookup_16() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___stringTypeLookup_16)); }
	inline Dictionary_2_t2269201059 * get_stringTypeLookup_16() const { return ___stringTypeLookup_16; }
	inline Dictionary_2_t2269201059 ** get_address_of_stringTypeLookup_16() { return &___stringTypeLookup_16; }
	inline void set_stringTypeLookup_16(Dictionary_2_t2269201059 * value)
	{
		___stringTypeLookup_16 = value;
		Il2CppCodeGenWriteBarrier((&___stringTypeLookup_16), value);
	}

	inline static int32_t get_offset_of_userTypes_17() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___userTypes_17)); }
	inline List_1_t3956019502 * get_userTypes_17() const { return ___userTypes_17; }
	inline List_1_t3956019502 ** get_address_of_userTypes_17() { return &___userTypes_17; }
	inline void set_userTypes_17(List_1_t3956019502 * value)
	{
		___userTypes_17 = value;
		Il2CppCodeGenWriteBarrier((&___userTypes_17), value);
	}

	inline static int32_t get_offset_of_userEditorTypes_18() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___userEditorTypes_18)); }
	inline List_1_t3956019502 * get_userEditorTypes_18() const { return ___userEditorTypes_18; }
	inline List_1_t3956019502 ** get_address_of_userEditorTypes_18() { return &___userEditorTypes_18; }
	inline void set_userEditorTypes_18(List_1_t3956019502 * value)
	{
		___userEditorTypes_18 = value;
		Il2CppCodeGenWriteBarrier((&___userEditorTypes_18), value);
	}

	inline static int32_t get_offset_of_pluginTypes_19() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___pluginTypes_19)); }
	inline List_1_t3956019502 * get_pluginTypes_19() const { return ___pluginTypes_19; }
	inline List_1_t3956019502 ** get_address_of_pluginTypes_19() { return &___pluginTypes_19; }
	inline void set_pluginTypes_19(List_1_t3956019502 * value)
	{
		___pluginTypes_19 = value;
		Il2CppCodeGenWriteBarrier((&___pluginTypes_19), value);
	}

	inline static int32_t get_offset_of_pluginEditorTypes_20() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___pluginEditorTypes_20)); }
	inline List_1_t3956019502 * get_pluginEditorTypes_20() const { return ___pluginEditorTypes_20; }
	inline List_1_t3956019502 ** get_address_of_pluginEditorTypes_20() { return &___pluginEditorTypes_20; }
	inline void set_pluginEditorTypes_20(List_1_t3956019502 * value)
	{
		___pluginEditorTypes_20 = value;
		Il2CppCodeGenWriteBarrier((&___pluginEditorTypes_20), value);
	}

	inline static int32_t get_offset_of_unityTypes_21() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___unityTypes_21)); }
	inline List_1_t3956019502 * get_unityTypes_21() const { return ___unityTypes_21; }
	inline List_1_t3956019502 ** get_address_of_unityTypes_21() { return &___unityTypes_21; }
	inline void set_unityTypes_21(List_1_t3956019502 * value)
	{
		___unityTypes_21 = value;
		Il2CppCodeGenWriteBarrier((&___unityTypes_21), value);
	}

	inline static int32_t get_offset_of_unityEditorTypes_22() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___unityEditorTypes_22)); }
	inline List_1_t3956019502 * get_unityEditorTypes_22() const { return ___unityEditorTypes_22; }
	inline List_1_t3956019502 ** get_address_of_unityEditorTypes_22() { return &___unityEditorTypes_22; }
	inline void set_unityEditorTypes_22(List_1_t3956019502 * value)
	{
		___unityEditorTypes_22 = value;
		Il2CppCodeGenWriteBarrier((&___unityEditorTypes_22), value);
	}

	inline static int32_t get_offset_of_otherTypes_23() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___otherTypes_23)); }
	inline List_1_t3956019502 * get_otherTypes_23() const { return ___otherTypes_23; }
	inline List_1_t3956019502 ** get_address_of_otherTypes_23() { return &___otherTypes_23; }
	inline void set_otherTypes_23(List_1_t3956019502 * value)
	{
		___otherTypes_23 = value;
		Il2CppCodeGenWriteBarrier((&___otherTypes_23), value);
	}

	inline static int32_t get_offset_of_dataPath_24() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___dataPath_24)); }
	inline String_t* get_dataPath_24() const { return ___dataPath_24; }
	inline String_t** get_address_of_dataPath_24() { return &___dataPath_24; }
	inline void set_dataPath_24(String_t* value)
	{
		___dataPath_24 = value;
		Il2CppCodeGenWriteBarrier((&___dataPath_24), value);
	}

	inline static int32_t get_offset_of_scriptAssembliesPath_25() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2880599301_StaticFields, ___scriptAssembliesPath_25)); }
	inline String_t* get_scriptAssembliesPath_25() const { return ___scriptAssembliesPath_25; }
	inline String_t** get_address_of_scriptAssembliesPath_25() { return &___scriptAssembliesPath_25; }
	inline void set_scriptAssembliesPath_25(String_t* value)
	{
		___scriptAssembliesPath_25 = value;
		Il2CppCodeGenWriteBarrier((&___scriptAssembliesPath_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYUTILITIES_T2880599301_H
#ifndef U3CU3EC_T506473177_H
#define U3CU3EC_T506473177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.AssemblyUtilities/<>c
struct  U3CU3Ec_t506473177  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t506473177_StaticFields
{
public:
	// Sirenix.Utilities.AssemblyUtilities/<>c Sirenix.Utilities.AssemblyUtilities/<>c::<>9
	U3CU3Ec_t506473177 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t506473177_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t506473177 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t506473177 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t506473177 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T506473177_H
#ifndef DEEPREFLECTION_T387836551_H
#define DEEPREFLECTION_T387836551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DeepReflection
struct  DeepReflection_t387836551  : public RuntimeObject
{
public:

public:
};

struct DeepReflection_t387836551_StaticFields
{
public:
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection::WeakListGetItem
	MethodInfo_t * ___WeakListGetItem_0;
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection::WeakListSetItem
	MethodInfo_t * ___WeakListSetItem_1;
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceGetDelegate1MethodInfo
	MethodInfo_t * ___CreateWeakAliasForInstanceGetDelegate1MethodInfo_2;
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceGetDelegate2MethodInfo
	MethodInfo_t * ___CreateWeakAliasForInstanceGetDelegate2MethodInfo_3;
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection::CreateWeakAliasForStaticGetDelegateMethodInfo
	MethodInfo_t * ___CreateWeakAliasForStaticGetDelegateMethodInfo_4;
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceSetDelegate1MethodInfo
	MethodInfo_t * ___CreateWeakAliasForInstanceSetDelegate1MethodInfo_5;

public:
	inline static int32_t get_offset_of_WeakListGetItem_0() { return static_cast<int32_t>(offsetof(DeepReflection_t387836551_StaticFields, ___WeakListGetItem_0)); }
	inline MethodInfo_t * get_WeakListGetItem_0() const { return ___WeakListGetItem_0; }
	inline MethodInfo_t ** get_address_of_WeakListGetItem_0() { return &___WeakListGetItem_0; }
	inline void set_WeakListGetItem_0(MethodInfo_t * value)
	{
		___WeakListGetItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___WeakListGetItem_0), value);
	}

	inline static int32_t get_offset_of_WeakListSetItem_1() { return static_cast<int32_t>(offsetof(DeepReflection_t387836551_StaticFields, ___WeakListSetItem_1)); }
	inline MethodInfo_t * get_WeakListSetItem_1() const { return ___WeakListSetItem_1; }
	inline MethodInfo_t ** get_address_of_WeakListSetItem_1() { return &___WeakListSetItem_1; }
	inline void set_WeakListSetItem_1(MethodInfo_t * value)
	{
		___WeakListSetItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___WeakListSetItem_1), value);
	}

	inline static int32_t get_offset_of_CreateWeakAliasForInstanceGetDelegate1MethodInfo_2() { return static_cast<int32_t>(offsetof(DeepReflection_t387836551_StaticFields, ___CreateWeakAliasForInstanceGetDelegate1MethodInfo_2)); }
	inline MethodInfo_t * get_CreateWeakAliasForInstanceGetDelegate1MethodInfo_2() const { return ___CreateWeakAliasForInstanceGetDelegate1MethodInfo_2; }
	inline MethodInfo_t ** get_address_of_CreateWeakAliasForInstanceGetDelegate1MethodInfo_2() { return &___CreateWeakAliasForInstanceGetDelegate1MethodInfo_2; }
	inline void set_CreateWeakAliasForInstanceGetDelegate1MethodInfo_2(MethodInfo_t * value)
	{
		___CreateWeakAliasForInstanceGetDelegate1MethodInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___CreateWeakAliasForInstanceGetDelegate1MethodInfo_2), value);
	}

	inline static int32_t get_offset_of_CreateWeakAliasForInstanceGetDelegate2MethodInfo_3() { return static_cast<int32_t>(offsetof(DeepReflection_t387836551_StaticFields, ___CreateWeakAliasForInstanceGetDelegate2MethodInfo_3)); }
	inline MethodInfo_t * get_CreateWeakAliasForInstanceGetDelegate2MethodInfo_3() const { return ___CreateWeakAliasForInstanceGetDelegate2MethodInfo_3; }
	inline MethodInfo_t ** get_address_of_CreateWeakAliasForInstanceGetDelegate2MethodInfo_3() { return &___CreateWeakAliasForInstanceGetDelegate2MethodInfo_3; }
	inline void set_CreateWeakAliasForInstanceGetDelegate2MethodInfo_3(MethodInfo_t * value)
	{
		___CreateWeakAliasForInstanceGetDelegate2MethodInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___CreateWeakAliasForInstanceGetDelegate2MethodInfo_3), value);
	}

	inline static int32_t get_offset_of_CreateWeakAliasForStaticGetDelegateMethodInfo_4() { return static_cast<int32_t>(offsetof(DeepReflection_t387836551_StaticFields, ___CreateWeakAliasForStaticGetDelegateMethodInfo_4)); }
	inline MethodInfo_t * get_CreateWeakAliasForStaticGetDelegateMethodInfo_4() const { return ___CreateWeakAliasForStaticGetDelegateMethodInfo_4; }
	inline MethodInfo_t ** get_address_of_CreateWeakAliasForStaticGetDelegateMethodInfo_4() { return &___CreateWeakAliasForStaticGetDelegateMethodInfo_4; }
	inline void set_CreateWeakAliasForStaticGetDelegateMethodInfo_4(MethodInfo_t * value)
	{
		___CreateWeakAliasForStaticGetDelegateMethodInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___CreateWeakAliasForStaticGetDelegateMethodInfo_4), value);
	}

	inline static int32_t get_offset_of_CreateWeakAliasForInstanceSetDelegate1MethodInfo_5() { return static_cast<int32_t>(offsetof(DeepReflection_t387836551_StaticFields, ___CreateWeakAliasForInstanceSetDelegate1MethodInfo_5)); }
	inline MethodInfo_t * get_CreateWeakAliasForInstanceSetDelegate1MethodInfo_5() const { return ___CreateWeakAliasForInstanceSetDelegate1MethodInfo_5; }
	inline MethodInfo_t ** get_address_of_CreateWeakAliasForInstanceSetDelegate1MethodInfo_5() { return &___CreateWeakAliasForInstanceSetDelegate1MethodInfo_5; }
	inline void set_CreateWeakAliasForInstanceSetDelegate1MethodInfo_5(MethodInfo_t * value)
	{
		___CreateWeakAliasForInstanceSetDelegate1MethodInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___CreateWeakAliasForInstanceSetDelegate1MethodInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEEPREFLECTION_T387836551_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T833354673_H
#define U3CU3EC__DISPLAYCLASS21_0_T833354673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DeepReflection/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t833354673  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep> Sirenix.Utilities.DeepReflection/<>c__DisplayClass21_0::memberPath
	List_1_t2683290888 * ___memberPath_0;

public:
	inline static int32_t get_offset_of_memberPath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t833354673, ___memberPath_0)); }
	inline List_1_t2683290888 * get_memberPath_0() const { return ___memberPath_0; }
	inline List_1_t2683290888 ** get_address_of_memberPath_0() { return &___memberPath_0; }
	inline void set_memberPath_0(List_1_t2683290888 * value)
	{
		___memberPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___memberPath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T833354673_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T2789669809_H
#define U3CU3EC__DISPLAYCLASS22_0_T2789669809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DeepReflection/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t2789669809  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep> Sirenix.Utilities.DeepReflection/<>c__DisplayClass22_0::memberPath
	List_1_t2683290888 * ___memberPath_0;

public:
	inline static int32_t get_offset_of_memberPath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2789669809, ___memberPath_0)); }
	inline List_1_t2683290888 * get_memberPath_0() const { return ___memberPath_0; }
	inline List_1_t2683290888 ** get_address_of_memberPath_0() { return &___memberPath_0; }
	inline void set_memberPath_0(List_1_t2683290888 * value)
	{
		___memberPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___memberPath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T2789669809_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T451017649_H
#define U3CU3EC__DISPLAYCLASS23_0_T451017649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DeepReflection/<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t451017649  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep> Sirenix.Utilities.DeepReflection/<>c__DisplayClass23_0::memberPath
	List_1_t2683290888 * ___memberPath_0;

public:
	inline static int32_t get_offset_of_memberPath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t451017649, ___memberPath_0)); }
	inline List_1_t2683290888 * get_memberPath_0() const { return ___memberPath_0; }
	inline List_1_t2683290888 ** get_address_of_memberPath_0() { return &___memberPath_0; }
	inline void set_memberPath_0(List_1_t2683290888 * value)
	{
		___memberPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___memberPath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T451017649_H
#ifndef EMITUTILITIES_T1690387612_H
#define EMITUTILITIES_T1690387612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities
struct  EmitUtilities_t1690387612  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMITUTILITIES_T1690387612_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T3252940178_H
#define U3CU3EC__DISPLAYCLASS11_0_T3252940178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t3252940178  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass11_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t3252940178, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T3252940178_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T2849655651_H
#define U3CU3EC__DISPLAYCLASS12_0_T2849655651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t2849655651  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass12_0::propertyInfo
	PropertyInfo_t * ___propertyInfo_0;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t2849655651, ___propertyInfo_0)); }
	inline PropertyInfo_t * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInfo_t ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInfo_t * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T2849655651_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T120772296_H
#define U3CU3EC__DISPLAYCLASS13_0_T120772296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t120772296  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass13_0::propertyInfo
	PropertyInfo_t * ___propertyInfo_0;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t120772296, ___propertyInfo_0)); }
	inline PropertyInfo_t * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInfo_t ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInfo_t * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T120772296_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T3255102866_H
#define U3CU3EC__DISPLAYCLASS21_0_T3255102866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t3255102866  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass21_0::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t3255102866, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T3255102866_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2047393244_H
#define U3CU3EC__DISPLAYCLASS3_0_T2047393244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2047393244  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass3_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2047393244, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2047393244_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T2047000028_H
#define U3CU3EC__DISPLAYCLASS5_0_T2047000028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t2047000028  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass5_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2047000028, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T2047000028_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T2047720924_H
#define U3CU3EC__DISPLAYCLASS8_0_T2047720924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.EmitUtilities/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t2047720924  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Utilities.EmitUtilities/<>c__DisplayClass8_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t2047720924, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T2047720924_H
#ifndef GUILAYOUTOPTIONS_T1479889820_H
#define GUILAYOUTOPTIONS_T1479889820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GUILayoutOptions
struct  GUILayoutOptions_t1479889820  : public RuntimeObject
{
public:

public:
};

struct GUILayoutOptions_t1479889820_StaticFields
{
public:
	// System.Int32 Sirenix.Utilities.GUILayoutOptions::CurrentCacheIndex
	int32_t ___CurrentCacheIndex_0;
	// Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance[] Sirenix.Utilities.GUILayoutOptions::GUILayoutOptionsInstanceCache
	GUILayoutOptionsInstanceU5BU5D_t768349783* ___GUILayoutOptionsInstanceCache_1;
	// System.Collections.Generic.Dictionary`2<Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance,UnityEngine.GUILayoutOption[]> Sirenix.Utilities.GUILayoutOptions::GUILayoutOptionsCache
	Dictionary_2_t1782032584 * ___GUILayoutOptionsCache_2;
	// UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions::EmptyGUIOptions
	GUILayoutOptionU5BU5D_t2510215842* ___EmptyGUIOptions_3;

public:
	inline static int32_t get_offset_of_CurrentCacheIndex_0() { return static_cast<int32_t>(offsetof(GUILayoutOptions_t1479889820_StaticFields, ___CurrentCacheIndex_0)); }
	inline int32_t get_CurrentCacheIndex_0() const { return ___CurrentCacheIndex_0; }
	inline int32_t* get_address_of_CurrentCacheIndex_0() { return &___CurrentCacheIndex_0; }
	inline void set_CurrentCacheIndex_0(int32_t value)
	{
		___CurrentCacheIndex_0 = value;
	}

	inline static int32_t get_offset_of_GUILayoutOptionsInstanceCache_1() { return static_cast<int32_t>(offsetof(GUILayoutOptions_t1479889820_StaticFields, ___GUILayoutOptionsInstanceCache_1)); }
	inline GUILayoutOptionsInstanceU5BU5D_t768349783* get_GUILayoutOptionsInstanceCache_1() const { return ___GUILayoutOptionsInstanceCache_1; }
	inline GUILayoutOptionsInstanceU5BU5D_t768349783** get_address_of_GUILayoutOptionsInstanceCache_1() { return &___GUILayoutOptionsInstanceCache_1; }
	inline void set_GUILayoutOptionsInstanceCache_1(GUILayoutOptionsInstanceU5BU5D_t768349783* value)
	{
		___GUILayoutOptionsInstanceCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___GUILayoutOptionsInstanceCache_1), value);
	}

	inline static int32_t get_offset_of_GUILayoutOptionsCache_2() { return static_cast<int32_t>(offsetof(GUILayoutOptions_t1479889820_StaticFields, ___GUILayoutOptionsCache_2)); }
	inline Dictionary_2_t1782032584 * get_GUILayoutOptionsCache_2() const { return ___GUILayoutOptionsCache_2; }
	inline Dictionary_2_t1782032584 ** get_address_of_GUILayoutOptionsCache_2() { return &___GUILayoutOptionsCache_2; }
	inline void set_GUILayoutOptionsCache_2(Dictionary_2_t1782032584 * value)
	{
		___GUILayoutOptionsCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___GUILayoutOptionsCache_2), value);
	}

	inline static int32_t get_offset_of_EmptyGUIOptions_3() { return static_cast<int32_t>(offsetof(GUILayoutOptions_t1479889820_StaticFields, ___EmptyGUIOptions_3)); }
	inline GUILayoutOptionU5BU5D_t2510215842* get_EmptyGUIOptions_3() const { return ___EmptyGUIOptions_3; }
	inline GUILayoutOptionU5BU5D_t2510215842** get_address_of_EmptyGUIOptions_3() { return &___EmptyGUIOptions_3; }
	inline void set_EmptyGUIOptions_3(GUILayoutOptionU5BU5D_t2510215842* value)
	{
		___EmptyGUIOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyGUIOptions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTIONS_T1479889820_H
#ifndef IMMUTABLELIST_T4021141801_H
#define IMMUTABLELIST_T4021141801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.ImmutableList
struct  ImmutableList_t4021141801  : public RuntimeObject
{
public:
	// System.Collections.IList Sirenix.Utilities.ImmutableList::innerList
	RuntimeObject* ___innerList_0;

public:
	inline static int32_t get_offset_of_innerList_0() { return static_cast<int32_t>(offsetof(ImmutableList_t4021141801, ___innerList_0)); }
	inline RuntimeObject* get_innerList_0() const { return ___innerList_0; }
	inline RuntimeObject** get_address_of_innerList_0() { return &___innerList_0; }
	inline void set_innerList_0(RuntimeObject* value)
	{
		___innerList_0 = value;
		Il2CppCodeGenWriteBarrier((&___innerList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLELIST_T4021141801_H
#ifndef U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T1906396239_H
#define U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T1906396239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25
struct  U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Sirenix.Utilities.ImmutableList Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>4__this
	ImmutableList_t4021141801 * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator Sirenix.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239, ___U3CU3E4__this_2)); }
	inline ImmutableList_t4021141801 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ImmutableList_t4021141801 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ImmutableList_t4021141801 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T1906396239_H
#ifndef LISTEXTENSIONS_T855869321_H
#define LISTEXTENSIONS_T855869321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.ListExtensions
struct  ListExtensions_t855869321  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTEXTENSIONS_T855869321_H
#ifndef MATHUTILITIES_T4293246924_H
#define MATHUTILITIES_T4293246924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MathUtilities
struct  MathUtilities_t4293246924  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILITIES_T4293246924_H
#ifndef U3CU3EC_T250125476_H
#define U3CU3EC_T250125476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberFinder/<>c
struct  U3CU3Ec_t250125476  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t250125476_StaticFields
{
public:
	// Sirenix.Utilities.MemberFinder/<>c Sirenix.Utilities.MemberFinder/<>c::<>9
	U3CU3Ec_t250125476 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Utilities.MemberFinder/<>c::<>9__40_1
	Func_2_t2217434578 * ___U3CU3E9__40_1_1;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Sirenix.Utilities.MemberFinder/<>c::<>9__40_3
	Func_2_t3487522507 * ___U3CU3E9__40_3_2;
	// System.Func`2<System.Type,System.String> Sirenix.Utilities.MemberFinder/<>c::<>9__40_10
	Func_2_t2311415679 * ___U3CU3E9__40_10_3;
	// System.Func`2<System.Type,System.String> Sirenix.Utilities.MemberFinder/<>c::<>9__40_11
	Func_2_t2311415679 * ___U3CU3E9__40_11_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t250125476_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t250125476 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t250125476 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t250125476 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t250125476_StaticFields, ___U3CU3E9__40_1_1)); }
	inline Func_2_t2217434578 * get_U3CU3E9__40_1_1() const { return ___U3CU3E9__40_1_1; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__40_1_1() { return &___U3CU3E9__40_1_1; }
	inline void set_U3CU3E9__40_1_1(Func_2_t2217434578 * value)
	{
		___U3CU3E9__40_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t250125476_StaticFields, ___U3CU3E9__40_3_2)); }
	inline Func_2_t3487522507 * get_U3CU3E9__40_3_2() const { return ___U3CU3E9__40_3_2; }
	inline Func_2_t3487522507 ** get_address_of_U3CU3E9__40_3_2() { return &___U3CU3E9__40_3_2; }
	inline void set_U3CU3E9__40_3_2(Func_2_t3487522507 * value)
	{
		___U3CU3E9__40_3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_3_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_10_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t250125476_StaticFields, ___U3CU3E9__40_10_3)); }
	inline Func_2_t2311415679 * get_U3CU3E9__40_10_3() const { return ___U3CU3E9__40_10_3; }
	inline Func_2_t2311415679 ** get_address_of_U3CU3E9__40_10_3() { return &___U3CU3E9__40_10_3; }
	inline void set_U3CU3E9__40_10_3(Func_2_t2311415679 * value)
	{
		___U3CU3E9__40_10_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_10_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_11_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t250125476_StaticFields, ___U3CU3E9__40_11_4)); }
	inline Func_2_t2311415679 * get_U3CU3E9__40_11_4() const { return ___U3CU3E9__40_11_4; }
	inline Func_2_t2311415679 ** get_address_of_U3CU3E9__40_11_4() { return &___U3CU3E9__40_11_4; }
	inline void set_U3CU3E9__40_11_4(Func_2_t2311415679 * value)
	{
		___U3CU3E9__40_11_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_11_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T250125476_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T3503402160_H
#define U3CU3EC__DISPLAYCLASS40_0_T3503402160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberFinder/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t3503402160  : public RuntimeObject
{
public:
	// System.Boolean Sirenix.Utilities.MemberFinder/<>c__DisplayClass40_0::isMethod
	bool ___isMethod_0;
	// System.Boolean Sirenix.Utilities.MemberFinder/<>c__DisplayClass40_0::isField
	bool ___isField_1;
	// System.Boolean Sirenix.Utilities.MemberFinder/<>c__DisplayClass40_0::isProperty
	bool ___isProperty_2;

public:
	inline static int32_t get_offset_of_isMethod_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t3503402160, ___isMethod_0)); }
	inline bool get_isMethod_0() const { return ___isMethod_0; }
	inline bool* get_address_of_isMethod_0() { return &___isMethod_0; }
	inline void set_isMethod_0(bool value)
	{
		___isMethod_0 = value;
	}

	inline static int32_t get_offset_of_isField_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t3503402160, ___isField_1)); }
	inline bool get_isField_1() const { return ___isField_1; }
	inline bool* get_address_of_isField_1() { return &___isField_1; }
	inline void set_isField_1(bool value)
	{
		___isField_1 = value;
	}

	inline static int32_t get_offset_of_isProperty_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t3503402160, ___isProperty_2)); }
	inline bool get_isProperty_2() const { return ___isProperty_2; }
	inline bool* get_address_of_isProperty_2() { return &___isProperty_2; }
	inline void set_isProperty_2(bool value)
	{
		___isProperty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T3503402160_H
#ifndef MEMBERFINDEREXTENSIONS_T1527015663_H
#define MEMBERFINDEREXTENSIONS_T1527015663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberFinderExtensions
struct  MemberFinderExtensions_t1527015663  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERFINDEREXTENSIONS_T1527015663_H
#ifndef MEMBERINFOEXTENSIONS_T253183112_H
#define MEMBERINFOEXTENSIONS_T253183112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberInfoExtensions
struct  MemberInfoExtensions_t253183112  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFOEXTENSIONS_T253183112_H
#ifndef METHODINFOEXTENSIONS_T3112246279_H
#define METHODINFOEXTENSIONS_T3112246279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MethodInfoExtensions
struct  MethodInfoExtensions_t3112246279  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFOEXTENSIONS_T3112246279_H
#ifndef MULTIDIMARRAYUTILITIES_T2087245842_H
#define MULTIDIMARRAYUTILITIES_T2087245842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MultiDimArrayUtilities
struct  MultiDimArrayUtilities_t2087245842  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIDIMARRAYUTILITIES_T2087245842_H
#ifndef PATHUTILITIES_T3087207691_H
#define PATHUTILITIES_T3087207691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.PathUtilities
struct  PathUtilities_t3087207691  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHUTILITIES_T3087207691_H
#ifndef PROPERTYINFOEXTENSIONS_T1537936810_H
#define PROPERTYINFOEXTENSIONS_T1537936810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.PropertyInfoExtensions
struct  PropertyInfoExtensions_t1537936810  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFOEXTENSIONS_T1537936810_H
#ifndef RECTEXTENSIONS_T3723780234_H
#define RECTEXTENSIONS_T3723780234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.RectExtensions
struct  RectExtensions_t3723780234  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTEXTENSIONS_T3723780234_H
#ifndef STRINGEXTENSIONS_T741703721_H
#define STRINGEXTENSIONS_T741703721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.StringExtensions
struct  StringExtensions_t741703721  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEXTENSIONS_T741703721_H
#ifndef TYPEEXTENSIONS_T2252489766_H
#define TYPEEXTENSIONS_T2252489766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions
struct  TypeExtensions_t2252489766  : public RuntimeObject
{
public:

public:
};

struct TypeExtensions_t2252489766_StaticFields
{
public:
	// System.Object Sirenix.Utilities.TypeExtensions::GenericConstraintsSatisfaction_LOCK
	RuntimeObject * ___GenericConstraintsSatisfaction_LOCK_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Utilities.TypeExtensions::GenericConstraintsSatisfactionInferredParameters
	Dictionary_2_t633324528 * ___GenericConstraintsSatisfactionInferredParameters_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Utilities.TypeExtensions::GenericConstraintsSatisfactionResolvedMap
	Dictionary_2_t633324528 * ___GenericConstraintsSatisfactionResolvedMap_2;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Utilities.TypeExtensions::GenericConstraintsSatisfactionProcessedParams
	HashSet_1_t1048894234 * ___GenericConstraintsSatisfactionProcessedParams_3;
	// System.Object Sirenix.Utilities.TypeExtensions::WeaklyTypedTypeCastDelegates_LOCK
	RuntimeObject * ___WeaklyTypedTypeCastDelegates_LOCK_4;
	// System.Object Sirenix.Utilities.TypeExtensions::StronglyTypedTypeCastDelegates_LOCK
	RuntimeObject * ___StronglyTypedTypeCastDelegates_LOCK_5;
	// Sirenix.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>> Sirenix.Utilities.TypeExtensions::WeaklyTypedTypeCastDelegates
	DoubleLookupDictionary_3_t1917252294 * ___WeaklyTypedTypeCastDelegates_6;
	// Sirenix.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate> Sirenix.Utilities.TypeExtensions::StronglyTypedTypeCastDelegates
	DoubleLookupDictionary_3_t658514733 * ___StronglyTypedTypeCastDelegates_7;
	// System.Collections.Generic.HashSet`1<System.String> Sirenix.Utilities.TypeExtensions::ReservedCSharpKeywords
	HashSet_1_t412400163 * ___ReservedCSharpKeywords_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Sirenix.Utilities.TypeExtensions::TypeNameAlternatives
	Dictionary_2_t1632706988 * ___TypeNameAlternatives_9;
	// System.Object Sirenix.Utilities.TypeExtensions::CachedNiceNames_LOCK
	RuntimeObject * ___CachedNiceNames_LOCK_10;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> Sirenix.Utilities.TypeExtensions::CachedNiceNames
	Dictionary_2_t4291797753 * ___CachedNiceNames_11;
	// System.Type Sirenix.Utilities.TypeExtensions::VoidPointerType
	Type_t * ___VoidPointerType_12;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>> Sirenix.Utilities.TypeExtensions::PrimitiveImplicitCasts
	Dictionary_2_t3493241298 * ___PrimitiveImplicitCasts_13;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Utilities.TypeExtensions::ExplicitCastIntegrals
	HashSet_1_t1048894234 * ___ExplicitCastIntegrals_14;

public:
	inline static int32_t get_offset_of_GenericConstraintsSatisfaction_LOCK_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___GenericConstraintsSatisfaction_LOCK_0)); }
	inline RuntimeObject * get_GenericConstraintsSatisfaction_LOCK_0() const { return ___GenericConstraintsSatisfaction_LOCK_0; }
	inline RuntimeObject ** get_address_of_GenericConstraintsSatisfaction_LOCK_0() { return &___GenericConstraintsSatisfaction_LOCK_0; }
	inline void set_GenericConstraintsSatisfaction_LOCK_0(RuntimeObject * value)
	{
		___GenericConstraintsSatisfaction_LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfaction_LOCK_0), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionInferredParameters_1() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___GenericConstraintsSatisfactionInferredParameters_1)); }
	inline Dictionary_2_t633324528 * get_GenericConstraintsSatisfactionInferredParameters_1() const { return ___GenericConstraintsSatisfactionInferredParameters_1; }
	inline Dictionary_2_t633324528 ** get_address_of_GenericConstraintsSatisfactionInferredParameters_1() { return &___GenericConstraintsSatisfactionInferredParameters_1; }
	inline void set_GenericConstraintsSatisfactionInferredParameters_1(Dictionary_2_t633324528 * value)
	{
		___GenericConstraintsSatisfactionInferredParameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionInferredParameters_1), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionResolvedMap_2() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___GenericConstraintsSatisfactionResolvedMap_2)); }
	inline Dictionary_2_t633324528 * get_GenericConstraintsSatisfactionResolvedMap_2() const { return ___GenericConstraintsSatisfactionResolvedMap_2; }
	inline Dictionary_2_t633324528 ** get_address_of_GenericConstraintsSatisfactionResolvedMap_2() { return &___GenericConstraintsSatisfactionResolvedMap_2; }
	inline void set_GenericConstraintsSatisfactionResolvedMap_2(Dictionary_2_t633324528 * value)
	{
		___GenericConstraintsSatisfactionResolvedMap_2 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionResolvedMap_2), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionProcessedParams_3() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___GenericConstraintsSatisfactionProcessedParams_3)); }
	inline HashSet_1_t1048894234 * get_GenericConstraintsSatisfactionProcessedParams_3() const { return ___GenericConstraintsSatisfactionProcessedParams_3; }
	inline HashSet_1_t1048894234 ** get_address_of_GenericConstraintsSatisfactionProcessedParams_3() { return &___GenericConstraintsSatisfactionProcessedParams_3; }
	inline void set_GenericConstraintsSatisfactionProcessedParams_3(HashSet_1_t1048894234 * value)
	{
		___GenericConstraintsSatisfactionProcessedParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionProcessedParams_3), value);
	}

	inline static int32_t get_offset_of_WeaklyTypedTypeCastDelegates_LOCK_4() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___WeaklyTypedTypeCastDelegates_LOCK_4)); }
	inline RuntimeObject * get_WeaklyTypedTypeCastDelegates_LOCK_4() const { return ___WeaklyTypedTypeCastDelegates_LOCK_4; }
	inline RuntimeObject ** get_address_of_WeaklyTypedTypeCastDelegates_LOCK_4() { return &___WeaklyTypedTypeCastDelegates_LOCK_4; }
	inline void set_WeaklyTypedTypeCastDelegates_LOCK_4(RuntimeObject * value)
	{
		___WeaklyTypedTypeCastDelegates_LOCK_4 = value;
		Il2CppCodeGenWriteBarrier((&___WeaklyTypedTypeCastDelegates_LOCK_4), value);
	}

	inline static int32_t get_offset_of_StronglyTypedTypeCastDelegates_LOCK_5() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___StronglyTypedTypeCastDelegates_LOCK_5)); }
	inline RuntimeObject * get_StronglyTypedTypeCastDelegates_LOCK_5() const { return ___StronglyTypedTypeCastDelegates_LOCK_5; }
	inline RuntimeObject ** get_address_of_StronglyTypedTypeCastDelegates_LOCK_5() { return &___StronglyTypedTypeCastDelegates_LOCK_5; }
	inline void set_StronglyTypedTypeCastDelegates_LOCK_5(RuntimeObject * value)
	{
		___StronglyTypedTypeCastDelegates_LOCK_5 = value;
		Il2CppCodeGenWriteBarrier((&___StronglyTypedTypeCastDelegates_LOCK_5), value);
	}

	inline static int32_t get_offset_of_WeaklyTypedTypeCastDelegates_6() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___WeaklyTypedTypeCastDelegates_6)); }
	inline DoubleLookupDictionary_3_t1917252294 * get_WeaklyTypedTypeCastDelegates_6() const { return ___WeaklyTypedTypeCastDelegates_6; }
	inline DoubleLookupDictionary_3_t1917252294 ** get_address_of_WeaklyTypedTypeCastDelegates_6() { return &___WeaklyTypedTypeCastDelegates_6; }
	inline void set_WeaklyTypedTypeCastDelegates_6(DoubleLookupDictionary_3_t1917252294 * value)
	{
		___WeaklyTypedTypeCastDelegates_6 = value;
		Il2CppCodeGenWriteBarrier((&___WeaklyTypedTypeCastDelegates_6), value);
	}

	inline static int32_t get_offset_of_StronglyTypedTypeCastDelegates_7() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___StronglyTypedTypeCastDelegates_7)); }
	inline DoubleLookupDictionary_3_t658514733 * get_StronglyTypedTypeCastDelegates_7() const { return ___StronglyTypedTypeCastDelegates_7; }
	inline DoubleLookupDictionary_3_t658514733 ** get_address_of_StronglyTypedTypeCastDelegates_7() { return &___StronglyTypedTypeCastDelegates_7; }
	inline void set_StronglyTypedTypeCastDelegates_7(DoubleLookupDictionary_3_t658514733 * value)
	{
		___StronglyTypedTypeCastDelegates_7 = value;
		Il2CppCodeGenWriteBarrier((&___StronglyTypedTypeCastDelegates_7), value);
	}

	inline static int32_t get_offset_of_ReservedCSharpKeywords_8() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___ReservedCSharpKeywords_8)); }
	inline HashSet_1_t412400163 * get_ReservedCSharpKeywords_8() const { return ___ReservedCSharpKeywords_8; }
	inline HashSet_1_t412400163 ** get_address_of_ReservedCSharpKeywords_8() { return &___ReservedCSharpKeywords_8; }
	inline void set_ReservedCSharpKeywords_8(HashSet_1_t412400163 * value)
	{
		___ReservedCSharpKeywords_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReservedCSharpKeywords_8), value);
	}

	inline static int32_t get_offset_of_TypeNameAlternatives_9() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___TypeNameAlternatives_9)); }
	inline Dictionary_2_t1632706988 * get_TypeNameAlternatives_9() const { return ___TypeNameAlternatives_9; }
	inline Dictionary_2_t1632706988 ** get_address_of_TypeNameAlternatives_9() { return &___TypeNameAlternatives_9; }
	inline void set_TypeNameAlternatives_9(Dictionary_2_t1632706988 * value)
	{
		___TypeNameAlternatives_9 = value;
		Il2CppCodeGenWriteBarrier((&___TypeNameAlternatives_9), value);
	}

	inline static int32_t get_offset_of_CachedNiceNames_LOCK_10() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___CachedNiceNames_LOCK_10)); }
	inline RuntimeObject * get_CachedNiceNames_LOCK_10() const { return ___CachedNiceNames_LOCK_10; }
	inline RuntimeObject ** get_address_of_CachedNiceNames_LOCK_10() { return &___CachedNiceNames_LOCK_10; }
	inline void set_CachedNiceNames_LOCK_10(RuntimeObject * value)
	{
		___CachedNiceNames_LOCK_10 = value;
		Il2CppCodeGenWriteBarrier((&___CachedNiceNames_LOCK_10), value);
	}

	inline static int32_t get_offset_of_CachedNiceNames_11() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___CachedNiceNames_11)); }
	inline Dictionary_2_t4291797753 * get_CachedNiceNames_11() const { return ___CachedNiceNames_11; }
	inline Dictionary_2_t4291797753 ** get_address_of_CachedNiceNames_11() { return &___CachedNiceNames_11; }
	inline void set_CachedNiceNames_11(Dictionary_2_t4291797753 * value)
	{
		___CachedNiceNames_11 = value;
		Il2CppCodeGenWriteBarrier((&___CachedNiceNames_11), value);
	}

	inline static int32_t get_offset_of_VoidPointerType_12() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___VoidPointerType_12)); }
	inline Type_t * get_VoidPointerType_12() const { return ___VoidPointerType_12; }
	inline Type_t ** get_address_of_VoidPointerType_12() { return &___VoidPointerType_12; }
	inline void set_VoidPointerType_12(Type_t * value)
	{
		___VoidPointerType_12 = value;
		Il2CppCodeGenWriteBarrier((&___VoidPointerType_12), value);
	}

	inline static int32_t get_offset_of_PrimitiveImplicitCasts_13() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___PrimitiveImplicitCasts_13)); }
	inline Dictionary_2_t3493241298 * get_PrimitiveImplicitCasts_13() const { return ___PrimitiveImplicitCasts_13; }
	inline Dictionary_2_t3493241298 ** get_address_of_PrimitiveImplicitCasts_13() { return &___PrimitiveImplicitCasts_13; }
	inline void set_PrimitiveImplicitCasts_13(Dictionary_2_t3493241298 * value)
	{
		___PrimitiveImplicitCasts_13 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveImplicitCasts_13), value);
	}

	inline static int32_t get_offset_of_ExplicitCastIntegrals_14() { return static_cast<int32_t>(offsetof(TypeExtensions_t2252489766_StaticFields, ___ExplicitCastIntegrals_14)); }
	inline HashSet_1_t1048894234 * get_ExplicitCastIntegrals_14() const { return ___ExplicitCastIntegrals_14; }
	inline HashSet_1_t1048894234 ** get_address_of_ExplicitCastIntegrals_14() { return &___ExplicitCastIntegrals_14; }
	inline void set_ExplicitCastIntegrals_14(HashSet_1_t1048894234 * value)
	{
		___ExplicitCastIntegrals_14 = value;
		Il2CppCodeGenWriteBarrier((&___ExplicitCastIntegrals_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T2252489766_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T614021754_H
#define U3CU3EC__DISPLAYCLASS22_0_T614021754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t614021754  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Sirenix.Utilities.TypeExtensions/<>c__DisplayClass22_0::method
	MethodInfo_t * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t614021754, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T614021754_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T187054714_H
#define U3CU3EC__DISPLAYCLASS29_0_T187054714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t187054714  : public RuntimeObject
{
public:
	// System.Type Sirenix.Utilities.TypeExtensions/<>c__DisplayClass29_0::openGenericInterfaceType
	Type_t * ___openGenericInterfaceType_0;

public:
	inline static int32_t get_offset_of_openGenericInterfaceType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t187054714, ___openGenericInterfaceType_0)); }
	inline Type_t * get_openGenericInterfaceType_0() const { return ___openGenericInterfaceType_0; }
	inline Type_t ** get_address_of_openGenericInterfaceType_0() { return &___openGenericInterfaceType_0; }
	inline void set_openGenericInterfaceType_0(Type_t * value)
	{
		___openGenericInterfaceType_0 = value;
		Il2CppCodeGenWriteBarrier((&___openGenericInterfaceType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T187054714_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T2151264021_H
#define U3CU3EC__DISPLAYCLASS35_0_T2151264021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t2151264021  : public RuntimeObject
{
public:
	// System.String Sirenix.Utilities.TypeExtensions/<>c__DisplayClass35_0::methodName
	String_t* ___methodName_0;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t2151264021, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T2151264021_H
#ifndef U3CGETBASECLASSESU3ED__42_T657414095_H
#define U3CGETBASECLASSESU3ED__42_T657414095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42
struct  U3CGetBaseClassesU3Ed__42_t657414095  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::type
	Type_t * ___type_3;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Boolean Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::includeSelf
	bool ___includeSelf_5;
	// System.Boolean Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::<>3__includeSelf
	bool ___U3CU3E3__includeSelf_6;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::<current>5__1
	Type_t * ___U3CcurrentU3E5__1_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_includeSelf_5() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___includeSelf_5)); }
	inline bool get_includeSelf_5() const { return ___includeSelf_5; }
	inline bool* get_address_of_includeSelf_5() { return &___includeSelf_5; }
	inline void set_includeSelf_5(bool value)
	{
		___includeSelf_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__includeSelf_6() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___U3CU3E3__includeSelf_6)); }
	inline bool get_U3CU3E3__includeSelf_6() const { return ___U3CU3E3__includeSelf_6; }
	inline bool* get_address_of_U3CU3E3__includeSelf_6() { return &___U3CU3E3__includeSelf_6; }
	inline void set_U3CU3E3__includeSelf_6(bool value)
	{
		___U3CU3E3__includeSelf_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__42_t657414095, ___U3CcurrentU3E5__1_7)); }
	inline Type_t * get_U3CcurrentU3E5__1_7() const { return ___U3CcurrentU3E5__1_7; }
	inline Type_t ** get_address_of_U3CcurrentU3E5__1_7() { return &___U3CcurrentU3E5__1_7; }
	inline void set_U3CcurrentU3E5__1_7(Type_t * value)
	{
		___U3CcurrentU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E5__1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBASECLASSESU3ED__42_T657414095_H
#ifndef UNITYEXTENSIONS_T2544498428_H
#define UNITYEXTENSIONS_T2544498428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.UnityExtensions
struct  UnityExtensions_t2544498428  : public RuntimeObject
{
public:

public:
};

struct UnityExtensions_t2544498428_StaticFields
{
public:
	// Sirenix.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr> Sirenix.Utilities.UnityExtensions::UnityObjectCachedPtrFieldGetter
	ValueGetter_2_t3789590669 * ___UnityObjectCachedPtrFieldGetter_0;

public:
	inline static int32_t get_offset_of_UnityObjectCachedPtrFieldGetter_0() { return static_cast<int32_t>(offsetof(UnityExtensions_t2544498428_StaticFields, ___UnityObjectCachedPtrFieldGetter_0)); }
	inline ValueGetter_2_t3789590669 * get_UnityObjectCachedPtrFieldGetter_0() const { return ___UnityObjectCachedPtrFieldGetter_0; }
	inline ValueGetter_2_t3789590669 ** get_address_of_UnityObjectCachedPtrFieldGetter_0() { return &___UnityObjectCachedPtrFieldGetter_0; }
	inline void set_UnityObjectCachedPtrFieldGetter_0(ValueGetter_2_t3789590669 * value)
	{
		___UnityObjectCachedPtrFieldGetter_0 = value;
		Il2CppCodeGenWriteBarrier((&___UnityObjectCachedPtrFieldGetter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXTENSIONS_T2544498428_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GLOBALCONFIGATTRIBUTE_T397999009_H
#define GLOBALCONFIGATTRIBUTE_T397999009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GlobalConfigAttribute
struct  GlobalConfigAttribute_t397999009  : public Attribute_t861562559
{
public:
	// System.String Sirenix.Utilities.GlobalConfigAttribute::assetPath
	String_t* ___assetPath_0;
	// System.Boolean Sirenix.Utilities.GlobalConfigAttribute::<UseAsset>k__BackingField
	bool ___U3CUseAssetU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_assetPath_0() { return static_cast<int32_t>(offsetof(GlobalConfigAttribute_t397999009, ___assetPath_0)); }
	inline String_t* get_assetPath_0() const { return ___assetPath_0; }
	inline String_t** get_address_of_assetPath_0() { return &___assetPath_0; }
	inline void set_assetPath_0(String_t* value)
	{
		___assetPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetPath_0), value);
	}

	inline static int32_t get_offset_of_U3CUseAssetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GlobalConfigAttribute_t397999009, ___U3CUseAssetU3Ek__BackingField_1)); }
	inline bool get_U3CUseAssetU3Ek__BackingField_1() const { return ___U3CUseAssetU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CUseAssetU3Ek__BackingField_1() { return &___U3CUseAssetU3Ek__BackingField_1; }
	inline void set_U3CUseAssetU3Ek__BackingField_1(bool value)
	{
		___U3CUseAssetU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALCONFIGATTRIBUTE_T397999009_H
#ifndef PERSISTENTASSEMBLYATTRIBUTE_T4217629867_H
#define PERSISTENTASSEMBLYATTRIBUTE_T4217629867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.PersistentAssemblyAttribute
struct  PersistentAssemblyAttribute_t4217629867  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTASSEMBLYATTRIBUTE_T4217629867_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FIELDINFO_T_H
#define FIELDINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFO_T_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef PROPERTYINFO_T_H
#define PROPERTYINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyInfo
struct  PropertyInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFO_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ASSEMBLYTYPEFLAGS_T3245991756_H
#define ASSEMBLYTYPEFLAGS_T3245991756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.AssemblyTypeFlags
struct  AssemblyTypeFlags_t3245991756 
{
public:
	// System.Int32 Sirenix.Utilities.AssemblyTypeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssemblyTypeFlags_t3245991756, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYTYPEFLAGS_T3245991756_H
#ifndef PATHSTEPTYPE_T1879353226_H
#define PATHSTEPTYPE_T1879353226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DeepReflection/PathStepType
struct  PathStepType_t1879353226 
{
public:
	// System.Int32 Sirenix.Utilities.DeepReflection/PathStepType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathStepType_t1879353226, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHSTEPTYPE_T1879353226_H
#ifndef GUILAYOUTOPTIONTYPE_T3981261802_H
#define GUILAYOUTOPTIONTYPE_T3981261802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType
struct  GUILayoutOptionType_t3981261802 
{
public:
	// System.Int32 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GUILayoutOptionType_t3981261802, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTIONTYPE_T3981261802_H
#ifndef MEMBERALIASFIELDINFO_T1859654097_H
#define MEMBERALIASFIELDINFO_T1859654097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberAliasFieldInfo
struct  MemberAliasFieldInfo_t1859654097  : public FieldInfo_t
{
public:
	// System.Reflection.FieldInfo Sirenix.Utilities.MemberAliasFieldInfo::aliasedField
	FieldInfo_t * ___aliasedField_1;
	// System.String Sirenix.Utilities.MemberAliasFieldInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedField_1() { return static_cast<int32_t>(offsetof(MemberAliasFieldInfo_t1859654097, ___aliasedField_1)); }
	inline FieldInfo_t * get_aliasedField_1() const { return ___aliasedField_1; }
	inline FieldInfo_t ** get_address_of_aliasedField_1() { return &___aliasedField_1; }
	inline void set_aliasedField_1(FieldInfo_t * value)
	{
		___aliasedField_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedField_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasFieldInfo_t1859654097, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASFIELDINFO_T1859654097_H
#ifndef MEMBERALIASPROPERTYINFO_T1306835646_H
#define MEMBERALIASPROPERTYINFO_T1306835646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberAliasPropertyInfo
struct  MemberAliasPropertyInfo_t1306835646  : public PropertyInfo_t
{
public:
	// System.Reflection.PropertyInfo Sirenix.Utilities.MemberAliasPropertyInfo::aliasedProperty
	PropertyInfo_t * ___aliasedProperty_1;
	// System.String Sirenix.Utilities.MemberAliasPropertyInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedProperty_1() { return static_cast<int32_t>(offsetof(MemberAliasPropertyInfo_t1306835646, ___aliasedProperty_1)); }
	inline PropertyInfo_t * get_aliasedProperty_1() const { return ___aliasedProperty_1; }
	inline PropertyInfo_t ** get_address_of_aliasedProperty_1() { return &___aliasedProperty_1; }
	inline void set_aliasedProperty_1(PropertyInfo_t * value)
	{
		___aliasedProperty_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedProperty_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasPropertyInfo_t1306835646, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASPROPERTYINFO_T1306835646_H
#ifndef CONDITIONFLAGS_T2544164368_H
#define CONDITIONFLAGS_T2544164368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberFinder/ConditionFlags
struct  ConditionFlags_t2544164368 
{
public:
	// System.Int32 Sirenix.Utilities.MemberFinder/ConditionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConditionFlags_t2544164368, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONFLAGS_T2544164368_H
#ifndef OPERATOR_T248706993_H
#define OPERATOR_T248706993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.Operator
struct  Operator_t248706993 
{
public:
	// System.Int32 Sirenix.Utilities.Operator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Operator_t248706993, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_T248706993_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef U3CGETTYPESU3ED__38_T2134305148_H
#define U3CGETTYPESU3ED__38_T2134305148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38
struct  U3CGetTypesU3Ed__38_t2134305148  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Sirenix.Utilities.AssemblyTypeFlags Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::assemblyTypeFlags
	int32_t ___assemblyTypeFlags_3;
	// Sirenix.Utilities.AssemblyTypeFlags Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<>3__assemblyTypeFlags
	int32_t ___U3CU3E3__assemblyTypeFlags_4;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__1
	int32_t ___U3CiU3E5__1_5;
	// System.Boolean Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeUserEditorTypes>5__2
	bool ___U3CincludeUserEditorTypesU3E5__2_6;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__3
	int32_t ___U3CiU3E5__3_7;
	// System.Boolean Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<includePluginTypes>5__4
	bool ___U3CincludePluginTypesU3E5__4_8;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__5
	int32_t ___U3CiU3E5__5_9;
	// System.Boolean Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<includePluginEditorTypes>5__6
	bool ___U3CincludePluginEditorTypesU3E5__6_10;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__7
	int32_t ___U3CiU3E5__7_11;
	// System.Boolean Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeUnityTypes>5__8
	bool ___U3CincludeUnityTypesU3E5__8_12;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__9
	int32_t ___U3CiU3E5__9_13;
	// System.Boolean Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeUnityEditorTypes>5__10
	bool ___U3CincludeUnityEditorTypesU3E5__10_14;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__11
	int32_t ___U3CiU3E5__11_15;
	// System.Boolean Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeOtherTypes>5__12
	bool ___U3CincludeOtherTypesU3E5__12_16;
	// System.Int32 Sirenix.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__13
	int32_t ___U3CiU3E5__13_17;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_assemblyTypeFlags_3() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___assemblyTypeFlags_3)); }
	inline int32_t get_assemblyTypeFlags_3() const { return ___assemblyTypeFlags_3; }
	inline int32_t* get_address_of_assemblyTypeFlags_3() { return &___assemblyTypeFlags_3; }
	inline void set_assemblyTypeFlags_3(int32_t value)
	{
		___assemblyTypeFlags_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__assemblyTypeFlags_4() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CU3E3__assemblyTypeFlags_4)); }
	inline int32_t get_U3CU3E3__assemblyTypeFlags_4() const { return ___U3CU3E3__assemblyTypeFlags_4; }
	inline int32_t* get_address_of_U3CU3E3__assemblyTypeFlags_4() { return &___U3CU3E3__assemblyTypeFlags_4; }
	inline void set_U3CU3E3__assemblyTypeFlags_4(int32_t value)
	{
		___U3CU3E3__assemblyTypeFlags_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__1_5)); }
	inline int32_t get_U3CiU3E5__1_5() const { return ___U3CiU3E5__1_5; }
	inline int32_t* get_address_of_U3CiU3E5__1_5() { return &___U3CiU3E5__1_5; }
	inline void set_U3CiU3E5__1_5(int32_t value)
	{
		___U3CiU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CincludeUserEditorTypesU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CincludeUserEditorTypesU3E5__2_6)); }
	inline bool get_U3CincludeUserEditorTypesU3E5__2_6() const { return ___U3CincludeUserEditorTypesU3E5__2_6; }
	inline bool* get_address_of_U3CincludeUserEditorTypesU3E5__2_6() { return &___U3CincludeUserEditorTypesU3E5__2_6; }
	inline void set_U3CincludeUserEditorTypesU3E5__2_6(bool value)
	{
		___U3CincludeUserEditorTypesU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__3_7)); }
	inline int32_t get_U3CiU3E5__3_7() const { return ___U3CiU3E5__3_7; }
	inline int32_t* get_address_of_U3CiU3E5__3_7() { return &___U3CiU3E5__3_7; }
	inline void set_U3CiU3E5__3_7(int32_t value)
	{
		___U3CiU3E5__3_7 = value;
	}

	inline static int32_t get_offset_of_U3CincludePluginTypesU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CincludePluginTypesU3E5__4_8)); }
	inline bool get_U3CincludePluginTypesU3E5__4_8() const { return ___U3CincludePluginTypesU3E5__4_8; }
	inline bool* get_address_of_U3CincludePluginTypesU3E5__4_8() { return &___U3CincludePluginTypesU3E5__4_8; }
	inline void set_U3CincludePluginTypesU3E5__4_8(bool value)
	{
		___U3CincludePluginTypesU3E5__4_8 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__5_9)); }
	inline int32_t get_U3CiU3E5__5_9() const { return ___U3CiU3E5__5_9; }
	inline int32_t* get_address_of_U3CiU3E5__5_9() { return &___U3CiU3E5__5_9; }
	inline void set_U3CiU3E5__5_9(int32_t value)
	{
		___U3CiU3E5__5_9 = value;
	}

	inline static int32_t get_offset_of_U3CincludePluginEditorTypesU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CincludePluginEditorTypesU3E5__6_10)); }
	inline bool get_U3CincludePluginEditorTypesU3E5__6_10() const { return ___U3CincludePluginEditorTypesU3E5__6_10; }
	inline bool* get_address_of_U3CincludePluginEditorTypesU3E5__6_10() { return &___U3CincludePluginEditorTypesU3E5__6_10; }
	inline void set_U3CincludePluginEditorTypesU3E5__6_10(bool value)
	{
		___U3CincludePluginEditorTypesU3E5__6_10 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__7_11() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__7_11)); }
	inline int32_t get_U3CiU3E5__7_11() const { return ___U3CiU3E5__7_11; }
	inline int32_t* get_address_of_U3CiU3E5__7_11() { return &___U3CiU3E5__7_11; }
	inline void set_U3CiU3E5__7_11(int32_t value)
	{
		___U3CiU3E5__7_11 = value;
	}

	inline static int32_t get_offset_of_U3CincludeUnityTypesU3E5__8_12() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CincludeUnityTypesU3E5__8_12)); }
	inline bool get_U3CincludeUnityTypesU3E5__8_12() const { return ___U3CincludeUnityTypesU3E5__8_12; }
	inline bool* get_address_of_U3CincludeUnityTypesU3E5__8_12() { return &___U3CincludeUnityTypesU3E5__8_12; }
	inline void set_U3CincludeUnityTypesU3E5__8_12(bool value)
	{
		___U3CincludeUnityTypesU3E5__8_12 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__9_13() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__9_13)); }
	inline int32_t get_U3CiU3E5__9_13() const { return ___U3CiU3E5__9_13; }
	inline int32_t* get_address_of_U3CiU3E5__9_13() { return &___U3CiU3E5__9_13; }
	inline void set_U3CiU3E5__9_13(int32_t value)
	{
		___U3CiU3E5__9_13 = value;
	}

	inline static int32_t get_offset_of_U3CincludeUnityEditorTypesU3E5__10_14() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CincludeUnityEditorTypesU3E5__10_14)); }
	inline bool get_U3CincludeUnityEditorTypesU3E5__10_14() const { return ___U3CincludeUnityEditorTypesU3E5__10_14; }
	inline bool* get_address_of_U3CincludeUnityEditorTypesU3E5__10_14() { return &___U3CincludeUnityEditorTypesU3E5__10_14; }
	inline void set_U3CincludeUnityEditorTypesU3E5__10_14(bool value)
	{
		___U3CincludeUnityEditorTypesU3E5__10_14 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__11_15() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__11_15)); }
	inline int32_t get_U3CiU3E5__11_15() const { return ___U3CiU3E5__11_15; }
	inline int32_t* get_address_of_U3CiU3E5__11_15() { return &___U3CiU3E5__11_15; }
	inline void set_U3CiU3E5__11_15(int32_t value)
	{
		___U3CiU3E5__11_15 = value;
	}

	inline static int32_t get_offset_of_U3CincludeOtherTypesU3E5__12_16() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CincludeOtherTypesU3E5__12_16)); }
	inline bool get_U3CincludeOtherTypesU3E5__12_16() const { return ___U3CincludeOtherTypesU3E5__12_16; }
	inline bool* get_address_of_U3CincludeOtherTypesU3E5__12_16() { return &___U3CincludeOtherTypesU3E5__12_16; }
	inline void set_U3CincludeOtherTypesU3E5__12_16(bool value)
	{
		___U3CincludeOtherTypesU3E5__12_16 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__13_17() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t2134305148, ___U3CiU3E5__13_17)); }
	inline int32_t get_U3CiU3E5__13_17() const { return ___U3CiU3E5__13_17; }
	inline int32_t* get_address_of_U3CiU3E5__13_17() { return &___U3CiU3E5__13_17; }
	inline void set_U3CiU3E5__13_17(int32_t value)
	{
		___U3CiU3E5__13_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTYPESU3ED__38_T2134305148_H
#ifndef PATHSTEP_T1211216146_H
#define PATHSTEP_T1211216146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.DeepReflection/PathStep
struct  PathStep_t1211216146 
{
public:
	// Sirenix.Utilities.DeepReflection/PathStepType Sirenix.Utilities.DeepReflection/PathStep::StepType
	int32_t ___StepType_0;
	// System.Reflection.MemberInfo Sirenix.Utilities.DeepReflection/PathStep::Member
	MemberInfo_t * ___Member_1;
	// System.Int32 Sirenix.Utilities.DeepReflection/PathStep::ElementIndex
	int32_t ___ElementIndex_2;
	// System.Type Sirenix.Utilities.DeepReflection/PathStep::ElementType
	Type_t * ___ElementType_3;
	// System.Reflection.MethodInfo Sirenix.Utilities.DeepReflection/PathStep::StrongListGetItemMethod
	MethodInfo_t * ___StrongListGetItemMethod_4;

public:
	inline static int32_t get_offset_of_StepType_0() { return static_cast<int32_t>(offsetof(PathStep_t1211216146, ___StepType_0)); }
	inline int32_t get_StepType_0() const { return ___StepType_0; }
	inline int32_t* get_address_of_StepType_0() { return &___StepType_0; }
	inline void set_StepType_0(int32_t value)
	{
		___StepType_0 = value;
	}

	inline static int32_t get_offset_of_Member_1() { return static_cast<int32_t>(offsetof(PathStep_t1211216146, ___Member_1)); }
	inline MemberInfo_t * get_Member_1() const { return ___Member_1; }
	inline MemberInfo_t ** get_address_of_Member_1() { return &___Member_1; }
	inline void set_Member_1(MemberInfo_t * value)
	{
		___Member_1 = value;
		Il2CppCodeGenWriteBarrier((&___Member_1), value);
	}

	inline static int32_t get_offset_of_ElementIndex_2() { return static_cast<int32_t>(offsetof(PathStep_t1211216146, ___ElementIndex_2)); }
	inline int32_t get_ElementIndex_2() const { return ___ElementIndex_2; }
	inline int32_t* get_address_of_ElementIndex_2() { return &___ElementIndex_2; }
	inline void set_ElementIndex_2(int32_t value)
	{
		___ElementIndex_2 = value;
	}

	inline static int32_t get_offset_of_ElementType_3() { return static_cast<int32_t>(offsetof(PathStep_t1211216146, ___ElementType_3)); }
	inline Type_t * get_ElementType_3() const { return ___ElementType_3; }
	inline Type_t ** get_address_of_ElementType_3() { return &___ElementType_3; }
	inline void set_ElementType_3(Type_t * value)
	{
		___ElementType_3 = value;
		Il2CppCodeGenWriteBarrier((&___ElementType_3), value);
	}

	inline static int32_t get_offset_of_StrongListGetItemMethod_4() { return static_cast<int32_t>(offsetof(PathStep_t1211216146, ___StrongListGetItemMethod_4)); }
	inline MethodInfo_t * get_StrongListGetItemMethod_4() const { return ___StrongListGetItemMethod_4; }
	inline MethodInfo_t ** get_address_of_StrongListGetItemMethod_4() { return &___StrongListGetItemMethod_4; }
	inline void set_StrongListGetItemMethod_4(MethodInfo_t * value)
	{
		___StrongListGetItemMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___StrongListGetItemMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Utilities.DeepReflection/PathStep
struct PathStep_t1211216146_marshaled_pinvoke
{
	int32_t ___StepType_0;
	MemberInfo_t * ___Member_1;
	int32_t ___ElementIndex_2;
	Type_t * ___ElementType_3;
	MethodInfo_t * ___StrongListGetItemMethod_4;
};
// Native definition for COM marshalling of Sirenix.Utilities.DeepReflection/PathStep
struct PathStep_t1211216146_marshaled_com
{
	int32_t ___StepType_0;
	MemberInfo_t * ___Member_1;
	int32_t ___ElementIndex_2;
	Type_t * ___ElementType_3;
	MethodInfo_t * ___StrongListGetItemMethod_4;
};
#endif // PATHSTEP_T1211216146_H
#ifndef FLAGS_T2566607416_H
#define FLAGS_T2566607416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.Flags
struct  Flags_t2566607416  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T2566607416_H
#ifndef GUILAYOUTOPTIONSINSTANCE_T2844476386_H
#define GUILAYOUTOPTIONSINSTANCE_T2844476386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance
struct  GUILayoutOptionsInstance_t2844476386  : public RuntimeObject
{
public:
	// System.Single Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::value
	float ___value_0;
	// Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Parent
	GUILayoutOptionsInstance_t2844476386 * ___Parent_1;
	// Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::GUILayoutOptionType
	int32_t ___GUILayoutOptionType_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(GUILayoutOptionsInstance_t2844476386, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_Parent_1() { return static_cast<int32_t>(offsetof(GUILayoutOptionsInstance_t2844476386, ___Parent_1)); }
	inline GUILayoutOptionsInstance_t2844476386 * get_Parent_1() const { return ___Parent_1; }
	inline GUILayoutOptionsInstance_t2844476386 ** get_address_of_Parent_1() { return &___Parent_1; }
	inline void set_Parent_1(GUILayoutOptionsInstance_t2844476386 * value)
	{
		___Parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_1), value);
	}

	inline static int32_t get_offset_of_GUILayoutOptionType_2() { return static_cast<int32_t>(offsetof(GUILayoutOptionsInstance_t2844476386, ___GUILayoutOptionType_2)); }
	inline int32_t get_GUILayoutOptionType_2() const { return ___GUILayoutOptionType_2; }
	inline int32_t* get_address_of_GUILayoutOptionType_2() { return &___GUILayoutOptionType_2; }
	inline void set_GUILayoutOptionType_2(int32_t value)
	{
		___GUILayoutOptionType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTIONSINSTANCE_T2844476386_H
#ifndef MEMBERALIASMETHODINFO_T2815159109_H
#define MEMBERALIASMETHODINFO_T2815159109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberAliasMethodInfo
struct  MemberAliasMethodInfo_t2815159109  : public MethodInfo_t
{
public:
	// System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasMethodInfo::aliasedMethod
	MethodInfo_t * ___aliasedMethod_1;
	// System.String Sirenix.Utilities.MemberAliasMethodInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedMethod_1() { return static_cast<int32_t>(offsetof(MemberAliasMethodInfo_t2815159109, ___aliasedMethod_1)); }
	inline MethodInfo_t * get_aliasedMethod_1() const { return ___aliasedMethod_1; }
	inline MethodInfo_t ** get_address_of_aliasedMethod_1() { return &___aliasedMethod_1; }
	inline void set_aliasedMethod_1(MethodInfo_t * value)
	{
		___aliasedMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedMethod_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasMethodInfo_t2815159109, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASMETHODINFO_T2815159109_H
#ifndef MEMBERFINDER_T2978535442_H
#define MEMBERFINDER_T2978535442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.MemberFinder
struct  MemberFinder_t2978535442  : public RuntimeObject
{
public:
	// System.Type Sirenix.Utilities.MemberFinder::type
	Type_t * ___type_0;
	// Sirenix.Utilities.Cache`1<Sirenix.Utilities.MemberFinder> Sirenix.Utilities.MemberFinder::cache
	Cache_1_t1308278200 * ___cache_1;
	// Sirenix.Utilities.MemberFinder/ConditionFlags Sirenix.Utilities.MemberFinder::conditionFlags
	int32_t ___conditionFlags_2;
	// System.String Sirenix.Utilities.MemberFinder::name
	String_t* ___name_3;
	// System.Type Sirenix.Utilities.MemberFinder::returnType
	Type_t * ___returnType_4;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Utilities.MemberFinder::paramTypes
	List_1_t3956019502 * ___paramTypes_5;
	// System.Boolean Sirenix.Utilities.MemberFinder::returnTypeCanInherit
	bool ___returnTypeCanInherit_6;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_cache_1() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___cache_1)); }
	inline Cache_1_t1308278200 * get_cache_1() const { return ___cache_1; }
	inline Cache_1_t1308278200 ** get_address_of_cache_1() { return &___cache_1; }
	inline void set_cache_1(Cache_1_t1308278200 * value)
	{
		___cache_1 = value;
		Il2CppCodeGenWriteBarrier((&___cache_1), value);
	}

	inline static int32_t get_offset_of_conditionFlags_2() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___conditionFlags_2)); }
	inline int32_t get_conditionFlags_2() const { return ___conditionFlags_2; }
	inline int32_t* get_address_of_conditionFlags_2() { return &___conditionFlags_2; }
	inline void set_conditionFlags_2(int32_t value)
	{
		___conditionFlags_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_returnType_4() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___returnType_4)); }
	inline Type_t * get_returnType_4() const { return ___returnType_4; }
	inline Type_t ** get_address_of_returnType_4() { return &___returnType_4; }
	inline void set_returnType_4(Type_t * value)
	{
		___returnType_4 = value;
		Il2CppCodeGenWriteBarrier((&___returnType_4), value);
	}

	inline static int32_t get_offset_of_paramTypes_5() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___paramTypes_5)); }
	inline List_1_t3956019502 * get_paramTypes_5() const { return ___paramTypes_5; }
	inline List_1_t3956019502 ** get_address_of_paramTypes_5() { return &___paramTypes_5; }
	inline void set_paramTypes_5(List_1_t3956019502 * value)
	{
		___paramTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___paramTypes_5), value);
	}

	inline static int32_t get_offset_of_returnTypeCanInherit_6() { return static_cast<int32_t>(offsetof(MemberFinder_t2978535442, ___returnTypeCanInherit_6)); }
	inline bool get_returnTypeCanInherit_6() const { return ___returnTypeCanInherit_6; }
	inline bool* get_address_of_returnTypeCanInherit_6() { return &___returnTypeCanInherit_6; }
	inline void set_returnTypeCanInherit_6(bool value)
	{
		___returnTypeCanInherit_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERFINDER_T2978535442_H
#ifndef U3CGETALLMEMBERSU3ED__36_T2070106349_H
#define U3CGETALLMEMBERSU3ED__36_T2070106349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36
struct  U3CGetAllMembersU3Ed__36_t2070106349  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MemberInfo Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>2__current
	MemberInfo_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::type
	Type_t * ___type_3;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<currentType>5__1
	Type_t * ___U3CcurrentTypeU3E5__1_7;
	// System.Reflection.MemberInfo[] Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>7__wrap1
	MemberInfoU5BU5D_t1302094432* ___U3CU3E7__wrap1_8;
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3E2__current_1)); }
	inline MemberInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MemberInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MemberInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTypeU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CcurrentTypeU3E5__1_7)); }
	inline Type_t * get_U3CcurrentTypeU3E5__1_7() const { return ___U3CcurrentTypeU3E5__1_7; }
	inline Type_t ** get_address_of_U3CcurrentTypeU3E5__1_7() { return &___U3CcurrentTypeU3E5__1_7; }
	inline void set_U3CcurrentTypeU3E5__1_7(Type_t * value)
	{
		___U3CcurrentTypeU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentTypeU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_8() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3E7__wrap1_8)); }
	inline MemberInfoU5BU5D_t1302094432* get_U3CU3E7__wrap1_8() const { return ___U3CU3E7__wrap1_8; }
	inline MemberInfoU5BU5D_t1302094432** get_address_of_U3CU3E7__wrap1_8() { return &___U3CU3E7__wrap1_8; }
	inline void set_U3CU3E7__wrap1_8(MemberInfoU5BU5D_t1302094432* value)
	{
		___U3CU3E7__wrap1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_9() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__36_t2070106349, ___U3CU3E7__wrap2_9)); }
	inline int32_t get_U3CU3E7__wrap2_9() const { return ___U3CU3E7__wrap2_9; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_9() { return &___U3CU3E7__wrap2_9; }
	inline void set_U3CU3E7__wrap2_9(int32_t value)
	{
		___U3CU3E7__wrap2_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLMEMBERSU3ED__36_T2070106349_H
#ifndef U3CGETALLMEMBERSU3ED__37_T2070106348_H
#define U3CGETALLMEMBERSU3ED__37_T2070106348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37
struct  U3CGetAllMembersU3Ed__37_t2070106348  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MemberInfo Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>2__current
	MemberInfo_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::type
	Type_t * ___type_3;
	// System.Type Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.String Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::name
	String_t* ___name_7;
	// System.String Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>3__name
	String_t* ___U3CU3E3__name_8;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3E2__current_1)); }
	inline MemberInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MemberInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MemberInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__name_8() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3E3__name_8)); }
	inline String_t* get_U3CU3E3__name_8() const { return ___U3CU3E3__name_8; }
	inline String_t** get_address_of_U3CU3E3__name_8() { return &___U3CU3E3__name_8; }
	inline void set_U3CU3E3__name_8(String_t* value)
	{
		___U3CU3E3__name_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__name_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__37_t2070106348, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLMEMBERSU3ED__37_T2070106348_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef WEAKVALUEGETTER_T3388029152_H
#define WEAKVALUEGETTER_T3388029152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.WeakValueGetter
struct  WeakValueGetter_t3388029152  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKVALUEGETTER_T3388029152_H
#ifndef WEAKVALUESETTER_T2229090528_H
#define WEAKVALUESETTER_T2229090528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.WeakValueSetter
struct  WeakValueSetter_t2229090528  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKVALUESETTER_T2229090528_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3202[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (MemberInfoExtensions_t253183112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (MethodInfoExtensions_t3112246279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (Operator_t248706993)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3206[21] = 
{
	Operator_t248706993::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (PathUtilities_t3087207691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (PropertyInfoExtensions_t1537936810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (RectExtensions_t3723780234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (StringExtensions_t741703721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (TypeExtensions_t2252489766), -1, sizeof(TypeExtensions_t2252489766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3211[15] = 
{
	TypeExtensions_t2252489766_StaticFields::get_offset_of_GenericConstraintsSatisfaction_LOCK_0(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_GenericConstraintsSatisfactionInferredParameters_1(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_GenericConstraintsSatisfactionResolvedMap_2(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_GenericConstraintsSatisfactionProcessedParams_3(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_WeaklyTypedTypeCastDelegates_LOCK_4(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_StronglyTypedTypeCastDelegates_LOCK_5(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_WeaklyTypedTypeCastDelegates_6(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_StronglyTypedTypeCastDelegates_7(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_ReservedCSharpKeywords_8(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_TypeNameAlternatives_9(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_CachedNiceNames_LOCK_10(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_CachedNiceNames_11(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_VoidPointerType_12(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_PrimitiveImplicitCasts_13(),
	TypeExtensions_t2252489766_StaticFields::get_offset_of_ExplicitCastIntegrals_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (U3CU3Ec__DisplayClass22_0_t614021754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[1] = 
{
	U3CU3Ec__DisplayClass22_0_t614021754::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3213[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (U3CU3Ec__DisplayClass29_0_t187054714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[1] = 
{
	U3CU3Ec__DisplayClass29_0_t187054714::get_offset_of_openGenericInterfaceType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (U3CU3Ec__DisplayClass35_0_t2151264021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3215[1] = 
{
	U3CU3Ec__DisplayClass35_0_t2151264021::get_offset_of_methodName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (U3CGetAllMembersU3Ed__36_t2070106349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[10] = 
{
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_type_3(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_flags_5(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3E3__flags_6(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CcurrentTypeU3E5__1_7(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3E7__wrap1_8(),
	U3CGetAllMembersU3Ed__36_t2070106349::get_offset_of_U3CU3E7__wrap2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (U3CGetAllMembersU3Ed__37_t2070106348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3217[10] = 
{
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_type_3(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_flags_5(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3E3__flags_6(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_name_7(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3E3__name_8(),
	U3CGetAllMembersU3Ed__37_t2070106348::get_offset_of_U3CU3E7__wrap1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (U3CGetBaseClassesU3Ed__42_t657414095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3219[8] = 
{
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_U3CU3E1__state_0(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_U3CU3E2__current_1(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_type_3(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_U3CU3E3__type_4(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_includeSelf_5(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_U3CU3E3__includeSelf_6(),
	U3CGetBaseClassesU3Ed__42_t657414095::get_offset_of_U3CcurrentU3E5__1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (UnityExtensions_t2544498428), -1, sizeof(UnityExtensions_t2544498428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3220[1] = 
{
	UnityExtensions_t2544498428_StaticFields::get_offset_of_UnityObjectCachedPtrFieldGetter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (ArrayUtilities_t3509714228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (AssemblyTypeFlags_t3245991756)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3222[13] = 
{
	AssemblyTypeFlags_t3245991756::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (AssemblyUtilities_t2880599301), -1, sizeof(AssemblyUtilities_t2880599301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3223[26] = 
{
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_userAssemblyPrefixes_0(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_pluginAssemblyPrefixes_1(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_unityEngineAssembly_2(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_LOCK_3(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_projectFolderDirectory_4(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_scriptAssembliesDirectory_5(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_assemblyTypeFlagLookup_6(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_allAssemblies_7(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_allAssembliesImmutable_8(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_userAssemblies_9(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_userEditorAssemblies_10(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_pluginAssemblies_11(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_pluginEditorAssemblies_12(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_unityAssemblies_13(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_unityEditorAssemblies_14(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_otherAssemblies_15(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_stringTypeLookup_16(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_userTypes_17(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_userEditorTypes_18(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_pluginTypes_19(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_pluginEditorTypes_20(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_unityTypes_21(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_unityEditorTypes_22(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_otherTypes_23(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_dataPath_24(),
	AssemblyUtilities_t2880599301_StaticFields::get_offset_of_scriptAssembliesPath_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (U3CU3Ec_t506473177), -1, sizeof(U3CU3Ec_t506473177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3224[1] = 
{
	U3CU3Ec_t506473177_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (U3CGetTypesU3Ed__38_t2134305148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[18] = 
{
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CU3E1__state_0(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CU3E2__current_1(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_assemblyTypeFlags_3(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CU3E3__assemblyTypeFlags_4(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__1_5(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CincludeUserEditorTypesU3E5__2_6(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__3_7(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CincludePluginTypesU3E5__4_8(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__5_9(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CincludePluginEditorTypesU3E5__6_10(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__7_11(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CincludeUnityTypesU3E5__8_12(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__9_13(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CincludeUnityEditorTypesU3E5__10_14(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__11_15(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CincludeOtherTypesU3E5__12_16(),
	U3CGetTypesU3Ed__38_t2134305148::get_offset_of_U3CiU3E5__13_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (DeepReflection_t387836551), -1, sizeof(DeepReflection_t387836551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3228[6] = 
{
	DeepReflection_t387836551_StaticFields::get_offset_of_WeakListGetItem_0(),
	DeepReflection_t387836551_StaticFields::get_offset_of_WeakListSetItem_1(),
	DeepReflection_t387836551_StaticFields::get_offset_of_CreateWeakAliasForInstanceGetDelegate1MethodInfo_2(),
	DeepReflection_t387836551_StaticFields::get_offset_of_CreateWeakAliasForInstanceGetDelegate2MethodInfo_3(),
	DeepReflection_t387836551_StaticFields::get_offset_of_CreateWeakAliasForStaticGetDelegateMethodInfo_4(),
	DeepReflection_t387836551_StaticFields::get_offset_of_CreateWeakAliasForInstanceSetDelegate1MethodInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (PathStepType_t1879353226)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3229[5] = 
{
	PathStepType_t1879353226::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (PathStep_t1211216146)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[5] = 
{
	PathStep_t1211216146::get_offset_of_StepType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathStep_t1211216146::get_offset_of_Member_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathStep_t1211216146::get_offset_of_ElementIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathStep_t1211216146::get_offset_of_ElementType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathStep_t1211216146::get_offset_of_StrongListGetItemMethod_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3233[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3235[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3236[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3238[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (U3CU3Ec__DisplayClass21_0_t833354673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[1] = 
{
	U3CU3Ec__DisplayClass21_0_t833354673::get_offset_of_memberPath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (U3CU3Ec__DisplayClass22_0_t2789669809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[1] = 
{
	U3CU3Ec__DisplayClass22_0_t2789669809::get_offset_of_memberPath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (U3CU3Ec__DisplayClass23_0_t451017649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[1] = 
{
	U3CU3Ec__DisplayClass23_0_t451017649::get_offset_of_memberPath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (WeakValueGetter_t3388029152), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (WeakValueSetter_t2229090528), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (EmitUtilities_t1690387612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (U3CU3Ec__DisplayClass3_0_t2047393244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[1] = 
{
	U3CU3Ec__DisplayClass3_0_t2047393244::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (U3CU3Ec__DisplayClass5_0_t2047000028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[1] = 
{
	U3CU3Ec__DisplayClass5_0_t2047000028::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3257[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (U3CU3Ec__DisplayClass8_0_t2047720924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[1] = 
{
	U3CU3Ec__DisplayClass8_0_t2047720924::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3260[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (U3CU3Ec__DisplayClass11_0_t3252940178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3261[1] = 
{
	U3CU3Ec__DisplayClass11_0_t3252940178::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (U3CU3Ec__DisplayClass12_0_t2849655651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2849655651::get_offset_of_propertyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (U3CU3Ec__DisplayClass13_0_t120772296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[1] = 
{
	U3CU3Ec__DisplayClass13_0_t120772296::get_offset_of_propertyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3264[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3265[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3266[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (U3CU3Ec__DisplayClass21_0_t3255102866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3269[1] = 
{
	U3CU3Ec__DisplayClass21_0_t3255102866::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3270[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3271[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (Flags_t2566607416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3274[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (GlobalConfigAttribute_t397999009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3275[2] = 
{
	GlobalConfigAttribute_t397999009::get_offset_of_assetPath_0(),
	GlobalConfigAttribute_t397999009::get_offset_of_U3CUseAssetU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (GUILayoutOptions_t1479889820), -1, sizeof(GUILayoutOptions_t1479889820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3276[4] = 
{
	GUILayoutOptions_t1479889820_StaticFields::get_offset_of_CurrentCacheIndex_0(),
	GUILayoutOptions_t1479889820_StaticFields::get_offset_of_GUILayoutOptionsInstanceCache_1(),
	GUILayoutOptions_t1479889820_StaticFields::get_offset_of_GUILayoutOptionsCache_2(),
	GUILayoutOptions_t1479889820_StaticFields::get_offset_of_EmptyGUIOptions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (GUILayoutOptionType_t3981261802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3277[9] = 
{
	GUILayoutOptionType_t3981261802::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (GUILayoutOptionsInstance_t2844476386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[3] = 
{
	GUILayoutOptionsInstance_t2844476386::get_offset_of_value_0(),
	GUILayoutOptionsInstance_t2844476386::get_offset_of_Parent_1(),
	GUILayoutOptionsInstance_t2844476386::get_offset_of_GUILayoutOptionType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3280[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (ImmutableList_t4021141801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[1] = 
{
	ImmutableList_t4021141801::get_offset_of_innerList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[4] = 
{
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239::get_offset_of_U3CU3E1__state_0(),
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239::get_offset_of_U3CU3E2__current_1(),
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239::get_offset_of_U3CU3E4__this_2(),
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t1906396239::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3285[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (ListExtensions_t855869321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3288[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (MathUtilities_t4293246924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3289[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (MemberFinderExtensions_t1527015663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (MemberFinder_t2978535442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[7] = 
{
	MemberFinder_t2978535442::get_offset_of_type_0(),
	MemberFinder_t2978535442::get_offset_of_cache_1(),
	MemberFinder_t2978535442::get_offset_of_conditionFlags_2(),
	MemberFinder_t2978535442::get_offset_of_name_3(),
	MemberFinder_t2978535442::get_offset_of_returnType_4(),
	MemberFinder_t2978535442::get_offset_of_paramTypes_5(),
	MemberFinder_t2978535442::get_offset_of_returnTypeCanInherit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (ConditionFlags_t2544164368)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3292[11] = 
{
	ConditionFlags_t2544164368::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (U3CU3Ec__DisplayClass40_0_t3503402160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[3] = 
{
	U3CU3Ec__DisplayClass40_0_t3503402160::get_offset_of_isMethod_0(),
	U3CU3Ec__DisplayClass40_0_t3503402160::get_offset_of_isField_1(),
	U3CU3Ec__DisplayClass40_0_t3503402160::get_offset_of_isProperty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (U3CU3Ec_t250125476), -1, sizeof(U3CU3Ec_t250125476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3294[5] = 
{
	U3CU3Ec_t250125476_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t250125476_StaticFields::get_offset_of_U3CU3E9__40_1_1(),
	U3CU3Ec_t250125476_StaticFields::get_offset_of_U3CU3E9__40_3_2(),
	U3CU3Ec_t250125476_StaticFields::get_offset_of_U3CU3E9__40_10_3(),
	U3CU3Ec_t250125476_StaticFields::get_offset_of_U3CU3E9__40_11_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (MultiDimArrayUtilities_t2087245842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (PersistentAssemblyAttribute_t4217629867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (MemberAliasFieldInfo_t1859654097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3297[3] = 
{
	0,
	MemberAliasFieldInfo_t1859654097::get_offset_of_aliasedField_1(),
	MemberAliasFieldInfo_t1859654097::get_offset_of_mangledName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (MemberAliasMethodInfo_t2815159109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3298[3] = 
{
	0,
	MemberAliasMethodInfo_t2815159109::get_offset_of_aliasedMethod_1(),
	MemberAliasMethodInfo_t2815159109::get_offset_of_mangledName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (MemberAliasPropertyInfo_t1306835646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[3] = 
{
	0,
	MemberAliasPropertyInfo_t1306835646::get_offset_of_aliasedProperty_1(),
	MemberAliasPropertyInfo_t1306835646::get_offset_of_mangledName_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
