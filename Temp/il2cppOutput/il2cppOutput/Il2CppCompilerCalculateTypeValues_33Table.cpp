﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t84807007;
// Sirenix.Serialization.DataFormat[]
struct DataFormatU5BU5D_t2757125862;
// Sirenix.Serialization.GlobalSerializationConfig
struct GlobalSerializationConfig_t3411570497;
// Sirenix.Serialization.ILogger
struct ILogger_t1896901244;
// Sirenix.Utilities.GlobalConfigAttribute
struct GlobalConfigAttribute_t397999009;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>
struct Dictionary_2_t1333685985;
// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate>
struct Dictionary_2_t2550447661;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>
struct Dictionary_2_t2559669019;
// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkIdentity>
struct HashSet_1_t1864468531;
// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>
struct HashSet_1_t3646266945;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>
struct List_1_t3051899460;
// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct List_1_t517180936;
// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct List_1_t3843830149;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient>
struct List_1_t935303414;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController>
struct List_1_t1968562558;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo>
struct List_1_t2451613916;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert>
struct List_1_t324891242;
// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>
struct Queue_1_t1426084212;
// System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Stack_1_t3215144862;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t120437468;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Net.EndPoint
struct EndPoint_t982345378;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t2683512874;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// UnityEngine.ChannelPurchase.IPurchaseListener
struct IPurchaseListener_t3176597229;
// UnityEngine.Networking.ChannelBuffer[]
struct ChannelBufferU5BU5D_t2631829696;
// UnityEngine.Networking.HostTopology
struct HostTopology_t4059263395;
// UnityEngine.Networking.LocalClient
struct LocalClient_t1191103892;
// UnityEngine.Networking.NetBuffer
struct NetBuffer_t2156033743;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_t2705220091;
// UnityEngine.Networking.NetworkMessage
struct NetworkMessage_t1192515889;
// UnityEngine.Networking.NetworkMessageDelegate
struct NetworkMessageDelegate_t360140524;
// UnityEngine.Networking.NetworkMessageHandlers
struct NetworkMessageHandlers_t82575973;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t1574750186;
// UnityEngine.Networking.NetworkScene
struct NetworkScene_t3519296737;
// UnityEngine.Networking.NetworkServer
struct NetworkServer_t2920297688;
// UnityEngine.Networking.NetworkSystem.CRCMessage
struct CRCMessage_t4148217304;
// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage
struct ClientAuthorityMessage_t2167651785;
// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage
struct ObjectDestroyMessage_t1358562099;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage
struct ObjectSpawnFinishedMessage_t2314084871;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage
struct ObjectSpawnMessage_t10889831;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage
struct ObjectSpawnSceneMessage_t2191101100;
// UnityEngine.Networking.NetworkSystem.OwnerMessage
struct OwnerMessage_t4130858210;
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[]
struct PeerInfoMessageU5BU5D_t2256646024;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_t3928387057;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt[]
struct AppleInAppPurchaseReceiptU5BU5D_t180321090;
// UnityEngine.Purchasing.Security.AppleReceiptParser
struct AppleReceiptParser_t1990668756;
// UnityEngine.Purchasing.Security.AppleValidator
struct AppleValidator_t513798189;
// UnityEngine.Purchasing.Security.DistinguishedName
struct DistinguishedName_t1591151536;
// UnityEngine.Purchasing.Security.GooglePlayValidator
struct GooglePlayValidator_t216820094;
// UnityEngine.Purchasing.Security.RSAKey
struct RSAKey_t3751505760;
// UnityEngine.Purchasing.Security.UnityChannelReceiptParser
struct UnityChannelReceiptParser_t1594844111;
// UnityEngine.Purchasing.Security.UnityChannelValidator
struct UnityChannelValidator_t2457973196;
// UnityEngine.Purchasing.Security.X509Cert
struct X509Cert_t3147783796;
// UnityEngine.Store.ILoginListener
struct ILoginListener_t828010398;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T692745565_H
#define U3CMODULEU3E_T692745565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745565 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745565_H
#ifndef U3CMODULEU3E_T692745559_H
#define U3CMODULEU3E_T692745559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745559 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745559_H
#ifndef U3CMODULEU3E_T692745561_H
#define U3CMODULEU3E_T692745561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745561 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745561_H
#ifndef U3CMODULEU3E_T692745558_H
#define U3CMODULEU3E_T692745558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745558 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745558_H
#ifndef U3CMODULEU3E_T692745569_H
#define U3CMODULEU3E_T692745569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745569 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745569_H
#ifndef U3CMODULEU3E_T692745557_H
#define U3CMODULEU3E_T692745557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745557 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745557_H
#ifndef U3CMODULEU3E_T692745562_H
#define U3CMODULEU3E_T692745562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745562 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745562_H
#ifndef U3CMODULEU3E_T692745563_H
#define U3CMODULEU3E_T692745563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745563 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745563_H
#ifndef U3CMODULEU3E_T692745568_H
#define U3CMODULEU3E_T692745568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745568 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745568_H
#ifndef U3CMODULEU3E_T692745567_H
#define U3CMODULEU3E_T692745567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745567 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745567_H
#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef U3CMODULEU3E_T692745566_H
#define U3CMODULEU3E_T692745566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745566 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745566_H
#ifndef U3CMODULEU3E_T692745560_H
#define U3CMODULEU3E_T692745560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745560 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745560_H
#ifndef U3CMODULEU3E_T692745564_H
#define U3CMODULEU3E_T692745564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745564 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745564_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1NODE_T84807007_H
#define ASN1NODE_T84807007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Node
struct  Asn1Node_t84807007  : public RuntimeObject
{
public:
	// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::tag
	uint8_t ___tag_0;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::dataOffset
	int64_t ___dataOffset_1;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::dataLength
	int64_t ___dataLength_2;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::lengthFieldBytes
	int64_t ___lengthFieldBytes_3;
	// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Node::data
	ByteU5BU5D_t4116647657* ___data_4;
	// System.Collections.ArrayList LipingShare.LCLib.Asn1Processor.Asn1Node::childNodeList
	ArrayList_t2718874744 * ___childNodeList_5;
	// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::unusedBits
	uint8_t ___unusedBits_6;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::deepness
	int64_t ___deepness_7;
	// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::path
	String_t* ___path_8;
	// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::parentNode
	Asn1Node_t84807007 * ___parentNode_9;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::requireRecalculatePar
	bool ___requireRecalculatePar_10;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::isIndefiniteLength
	bool ___isIndefiniteLength_11;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::parseEncapsulatedData
	bool ___parseEncapsulatedData_12;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___tag_0)); }
	inline uint8_t get_tag_0() const { return ___tag_0; }
	inline uint8_t* get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(uint8_t value)
	{
		___tag_0 = value;
	}

	inline static int32_t get_offset_of_dataOffset_1() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___dataOffset_1)); }
	inline int64_t get_dataOffset_1() const { return ___dataOffset_1; }
	inline int64_t* get_address_of_dataOffset_1() { return &___dataOffset_1; }
	inline void set_dataOffset_1(int64_t value)
	{
		___dataOffset_1 = value;
	}

	inline static int32_t get_offset_of_dataLength_2() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___dataLength_2)); }
	inline int64_t get_dataLength_2() const { return ___dataLength_2; }
	inline int64_t* get_address_of_dataLength_2() { return &___dataLength_2; }
	inline void set_dataLength_2(int64_t value)
	{
		___dataLength_2 = value;
	}

	inline static int32_t get_offset_of_lengthFieldBytes_3() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___lengthFieldBytes_3)); }
	inline int64_t get_lengthFieldBytes_3() const { return ___lengthFieldBytes_3; }
	inline int64_t* get_address_of_lengthFieldBytes_3() { return &___lengthFieldBytes_3; }
	inline void set_lengthFieldBytes_3(int64_t value)
	{
		___lengthFieldBytes_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___data_4)); }
	inline ByteU5BU5D_t4116647657* get_data_4() const { return ___data_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ByteU5BU5D_t4116647657* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_childNodeList_5() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___childNodeList_5)); }
	inline ArrayList_t2718874744 * get_childNodeList_5() const { return ___childNodeList_5; }
	inline ArrayList_t2718874744 ** get_address_of_childNodeList_5() { return &___childNodeList_5; }
	inline void set_childNodeList_5(ArrayList_t2718874744 * value)
	{
		___childNodeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___childNodeList_5), value);
	}

	inline static int32_t get_offset_of_unusedBits_6() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___unusedBits_6)); }
	inline uint8_t get_unusedBits_6() const { return ___unusedBits_6; }
	inline uint8_t* get_address_of_unusedBits_6() { return &___unusedBits_6; }
	inline void set_unusedBits_6(uint8_t value)
	{
		___unusedBits_6 = value;
	}

	inline static int32_t get_offset_of_deepness_7() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___deepness_7)); }
	inline int64_t get_deepness_7() const { return ___deepness_7; }
	inline int64_t* get_address_of_deepness_7() { return &___deepness_7; }
	inline void set_deepness_7(int64_t value)
	{
		___deepness_7 = value;
	}

	inline static int32_t get_offset_of_path_8() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___path_8)); }
	inline String_t* get_path_8() const { return ___path_8; }
	inline String_t** get_address_of_path_8() { return &___path_8; }
	inline void set_path_8(String_t* value)
	{
		___path_8 = value;
		Il2CppCodeGenWriteBarrier((&___path_8), value);
	}

	inline static int32_t get_offset_of_parentNode_9() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___parentNode_9)); }
	inline Asn1Node_t84807007 * get_parentNode_9() const { return ___parentNode_9; }
	inline Asn1Node_t84807007 ** get_address_of_parentNode_9() { return &___parentNode_9; }
	inline void set_parentNode_9(Asn1Node_t84807007 * value)
	{
		___parentNode_9 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_9), value);
	}

	inline static int32_t get_offset_of_requireRecalculatePar_10() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___requireRecalculatePar_10)); }
	inline bool get_requireRecalculatePar_10() const { return ___requireRecalculatePar_10; }
	inline bool* get_address_of_requireRecalculatePar_10() { return &___requireRecalculatePar_10; }
	inline void set_requireRecalculatePar_10(bool value)
	{
		___requireRecalculatePar_10 = value;
	}

	inline static int32_t get_offset_of_isIndefiniteLength_11() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___isIndefiniteLength_11)); }
	inline bool get_isIndefiniteLength_11() const { return ___isIndefiniteLength_11; }
	inline bool* get_address_of_isIndefiniteLength_11() { return &___isIndefiniteLength_11; }
	inline void set_isIndefiniteLength_11(bool value)
	{
		___isIndefiniteLength_11 = value;
	}

	inline static int32_t get_offset_of_parseEncapsulatedData_12() { return static_cast<int32_t>(offsetof(Asn1Node_t84807007, ___parseEncapsulatedData_12)); }
	inline bool get_parseEncapsulatedData_12() const { return ___parseEncapsulatedData_12; }
	inline bool* get_address_of_parseEncapsulatedData_12() { return &___parseEncapsulatedData_12; }
	inline void set_parseEncapsulatedData_12(bool value)
	{
		___parseEncapsulatedData_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1NODE_T84807007_H
#ifndef ASN1PARSER_T1261554413_H
#define ASN1PARSER_T1261554413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Parser
struct  Asn1Parser_t1261554413  : public RuntimeObject
{
public:
	// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Parser::rawData
	ByteU5BU5D_t4116647657* ___rawData_0;
	// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Parser::rootNode
	Asn1Node_t84807007 * ___rootNode_1;

public:
	inline static int32_t get_offset_of_rawData_0() { return static_cast<int32_t>(offsetof(Asn1Parser_t1261554413, ___rawData_0)); }
	inline ByteU5BU5D_t4116647657* get_rawData_0() const { return ___rawData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_rawData_0() { return &___rawData_0; }
	inline void set_rawData_0(ByteU5BU5D_t4116647657* value)
	{
		___rawData_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawData_0), value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(Asn1Parser_t1261554413, ___rootNode_1)); }
	inline Asn1Node_t84807007 * get_rootNode_1() const { return ___rootNode_1; }
	inline Asn1Node_t84807007 ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(Asn1Node_t84807007 * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootNode_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1PARSER_T1261554413_H
#ifndef ASN1UTIL_T417944685_H
#define ASN1UTIL_T417944685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Util
struct  Asn1Util_t417944685  : public RuntimeObject
{
public:

public:
};

struct Asn1Util_t417944685_StaticFields
{
public:
	// System.Char[] LipingShare.LCLib.Asn1Processor.Asn1Util::hexDigits
	CharU5BU5D_t3528271667* ___hexDigits_0;

public:
	inline static int32_t get_offset_of_hexDigits_0() { return static_cast<int32_t>(offsetof(Asn1Util_t417944685_StaticFields, ___hexDigits_0)); }
	inline CharU5BU5D_t3528271667* get_hexDigits_0() const { return ___hexDigits_0; }
	inline CharU5BU5D_t3528271667** get_address_of_hexDigits_0() { return &___hexDigits_0; }
	inline void set_hexDigits_0(CharU5BU5D_t3528271667* value)
	{
		___hexDigits_0 = value;
		Il2CppCodeGenWriteBarrier((&___hexDigits_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1UTIL_T417944685_H
#ifndef OID_T864847193_H
#define OID_T864847193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Oid
struct  Oid_t864847193  : public RuntimeObject
{
public:

public:
};

struct Oid_t864847193_StaticFields
{
public:
	// System.Collections.Specialized.StringDictionary LipingShare.LCLib.Asn1Processor.Oid::oidDictionary
	StringDictionary_t120437468 * ___oidDictionary_0;

public:
	inline static int32_t get_offset_of_oidDictionary_0() { return static_cast<int32_t>(offsetof(Oid_t864847193_StaticFields, ___oidDictionary_0)); }
	inline StringDictionary_t120437468 * get_oidDictionary_0() const { return ___oidDictionary_0; }
	inline StringDictionary_t120437468 ** get_address_of_oidDictionary_0() { return &___oidDictionary_0; }
	inline void set_oidDictionary_0(StringDictionary_t120437468 * value)
	{
		___oidDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___oidDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_T864847193_H
#ifndef CUSTOMLOGGER_T3390755501_H
#define CUSTOMLOGGER_T3390755501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CustomLogger
struct  CustomLogger_t3390755501  : public RuntimeObject
{
public:
	// System.Action`1<System.String> Sirenix.Serialization.CustomLogger::logWarningDelegate
	Action_1_t2019918284 * ___logWarningDelegate_0;
	// System.Action`1<System.String> Sirenix.Serialization.CustomLogger::logErrorDelegate
	Action_1_t2019918284 * ___logErrorDelegate_1;
	// System.Action`1<System.Exception> Sirenix.Serialization.CustomLogger::logExceptionDelegate
	Action_1_t1609204844 * ___logExceptionDelegate_2;

public:
	inline static int32_t get_offset_of_logWarningDelegate_0() { return static_cast<int32_t>(offsetof(CustomLogger_t3390755501, ___logWarningDelegate_0)); }
	inline Action_1_t2019918284 * get_logWarningDelegate_0() const { return ___logWarningDelegate_0; }
	inline Action_1_t2019918284 ** get_address_of_logWarningDelegate_0() { return &___logWarningDelegate_0; }
	inline void set_logWarningDelegate_0(Action_1_t2019918284 * value)
	{
		___logWarningDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___logWarningDelegate_0), value);
	}

	inline static int32_t get_offset_of_logErrorDelegate_1() { return static_cast<int32_t>(offsetof(CustomLogger_t3390755501, ___logErrorDelegate_1)); }
	inline Action_1_t2019918284 * get_logErrorDelegate_1() const { return ___logErrorDelegate_1; }
	inline Action_1_t2019918284 ** get_address_of_logErrorDelegate_1() { return &___logErrorDelegate_1; }
	inline void set_logErrorDelegate_1(Action_1_t2019918284 * value)
	{
		___logErrorDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___logErrorDelegate_1), value);
	}

	inline static int32_t get_offset_of_logExceptionDelegate_2() { return static_cast<int32_t>(offsetof(CustomLogger_t3390755501, ___logExceptionDelegate_2)); }
	inline Action_1_t1609204844 * get_logExceptionDelegate_2() const { return ___logExceptionDelegate_2; }
	inline Action_1_t1609204844 ** get_address_of_logExceptionDelegate_2() { return &___logExceptionDelegate_2; }
	inline void set_logExceptionDelegate_2(Action_1_t1609204844 * value)
	{
		___logExceptionDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___logExceptionDelegate_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLOGGER_T3390755501_H
#ifndef DEFAULTLOGGERS_T3565000847_H
#define DEFAULTLOGGERS_T3565000847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DefaultLoggers
struct  DefaultLoggers_t3565000847  : public RuntimeObject
{
public:

public:
};

struct DefaultLoggers_t3565000847_StaticFields
{
public:
	// System.Object Sirenix.Serialization.DefaultLoggers::LOCK
	RuntimeObject * ___LOCK_0;
	// Sirenix.Serialization.ILogger modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.DefaultLoggers::unityLogger
	RuntimeObject* ___unityLogger_1;

public:
	inline static int32_t get_offset_of_LOCK_0() { return static_cast<int32_t>(offsetof(DefaultLoggers_t3565000847_StaticFields, ___LOCK_0)); }
	inline RuntimeObject * get_LOCK_0() const { return ___LOCK_0; }
	inline RuntimeObject ** get_address_of_LOCK_0() { return &___LOCK_0; }
	inline void set_LOCK_0(RuntimeObject * value)
	{
		___LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_0), value);
	}

	inline static int32_t get_offset_of_unityLogger_1() { return static_cast<int32_t>(offsetof(DefaultLoggers_t3565000847_StaticFields, ___unityLogger_1)); }
	inline RuntimeObject* get_unityLogger_1() const { return ___unityLogger_1; }
	inline RuntimeObject** get_address_of_unityLogger_1() { return &___unityLogger_1; }
	inline void set_unityLogger_1(RuntimeObject* value)
	{
		___unityLogger_1 = value;
		Il2CppCodeGenWriteBarrier((&___unityLogger_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTLOGGERS_T3565000847_H
#ifndef SIRENIXASSETPATHS_T1482981763_H
#define SIRENIXASSETPATHS_T1482981763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.SirenixAssetPaths
struct  SirenixAssetPaths_t1482981763  : public RuntimeObject
{
public:

public:
};

struct SirenixAssetPaths_t1482981763_StaticFields
{
public:
	// System.String Sirenix.Utilities.SirenixAssetPaths::DefaultSirenixPluginPath
	String_t* ___DefaultSirenixPluginPath_0;
	// System.String Sirenix.Utilities.SirenixAssetPaths::OdinPath
	String_t* ___OdinPath_1;
	// System.String Sirenix.Utilities.SirenixAssetPaths::SirenixAssetsPath
	String_t* ___SirenixAssetsPath_2;
	// System.String Sirenix.Utilities.SirenixAssetPaths::SirenixPluginPath
	String_t* ___SirenixPluginPath_3;
	// System.String Sirenix.Utilities.SirenixAssetPaths::SirenixAssembliesPath
	String_t* ___SirenixAssembliesPath_4;
	// System.String Sirenix.Utilities.SirenixAssetPaths::OdinResourcesPath
	String_t* ___OdinResourcesPath_5;
	// System.String Sirenix.Utilities.SirenixAssetPaths::OdinEditorConfigsPath
	String_t* ___OdinEditorConfigsPath_6;
	// System.String Sirenix.Utilities.SirenixAssetPaths::OdinResourcesConfigsPath
	String_t* ___OdinResourcesConfigsPath_7;
	// System.String Sirenix.Utilities.SirenixAssetPaths::OdinTempPath
	String_t* ___OdinTempPath_8;

public:
	inline static int32_t get_offset_of_DefaultSirenixPluginPath_0() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___DefaultSirenixPluginPath_0)); }
	inline String_t* get_DefaultSirenixPluginPath_0() const { return ___DefaultSirenixPluginPath_0; }
	inline String_t** get_address_of_DefaultSirenixPluginPath_0() { return &___DefaultSirenixPluginPath_0; }
	inline void set_DefaultSirenixPluginPath_0(String_t* value)
	{
		___DefaultSirenixPluginPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultSirenixPluginPath_0), value);
	}

	inline static int32_t get_offset_of_OdinPath_1() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___OdinPath_1)); }
	inline String_t* get_OdinPath_1() const { return ___OdinPath_1; }
	inline String_t** get_address_of_OdinPath_1() { return &___OdinPath_1; }
	inline void set_OdinPath_1(String_t* value)
	{
		___OdinPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___OdinPath_1), value);
	}

	inline static int32_t get_offset_of_SirenixAssetsPath_2() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___SirenixAssetsPath_2)); }
	inline String_t* get_SirenixAssetsPath_2() const { return ___SirenixAssetsPath_2; }
	inline String_t** get_address_of_SirenixAssetsPath_2() { return &___SirenixAssetsPath_2; }
	inline void set_SirenixAssetsPath_2(String_t* value)
	{
		___SirenixAssetsPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___SirenixAssetsPath_2), value);
	}

	inline static int32_t get_offset_of_SirenixPluginPath_3() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___SirenixPluginPath_3)); }
	inline String_t* get_SirenixPluginPath_3() const { return ___SirenixPluginPath_3; }
	inline String_t** get_address_of_SirenixPluginPath_3() { return &___SirenixPluginPath_3; }
	inline void set_SirenixPluginPath_3(String_t* value)
	{
		___SirenixPluginPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___SirenixPluginPath_3), value);
	}

	inline static int32_t get_offset_of_SirenixAssembliesPath_4() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___SirenixAssembliesPath_4)); }
	inline String_t* get_SirenixAssembliesPath_4() const { return ___SirenixAssembliesPath_4; }
	inline String_t** get_address_of_SirenixAssembliesPath_4() { return &___SirenixAssembliesPath_4; }
	inline void set_SirenixAssembliesPath_4(String_t* value)
	{
		___SirenixAssembliesPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___SirenixAssembliesPath_4), value);
	}

	inline static int32_t get_offset_of_OdinResourcesPath_5() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___OdinResourcesPath_5)); }
	inline String_t* get_OdinResourcesPath_5() const { return ___OdinResourcesPath_5; }
	inline String_t** get_address_of_OdinResourcesPath_5() { return &___OdinResourcesPath_5; }
	inline void set_OdinResourcesPath_5(String_t* value)
	{
		___OdinResourcesPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___OdinResourcesPath_5), value);
	}

	inline static int32_t get_offset_of_OdinEditorConfigsPath_6() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___OdinEditorConfigsPath_6)); }
	inline String_t* get_OdinEditorConfigsPath_6() const { return ___OdinEditorConfigsPath_6; }
	inline String_t** get_address_of_OdinEditorConfigsPath_6() { return &___OdinEditorConfigsPath_6; }
	inline void set_OdinEditorConfigsPath_6(String_t* value)
	{
		___OdinEditorConfigsPath_6 = value;
		Il2CppCodeGenWriteBarrier((&___OdinEditorConfigsPath_6), value);
	}

	inline static int32_t get_offset_of_OdinResourcesConfigsPath_7() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___OdinResourcesConfigsPath_7)); }
	inline String_t* get_OdinResourcesConfigsPath_7() const { return ___OdinResourcesConfigsPath_7; }
	inline String_t** get_address_of_OdinResourcesConfigsPath_7() { return &___OdinResourcesConfigsPath_7; }
	inline void set_OdinResourcesConfigsPath_7(String_t* value)
	{
		___OdinResourcesConfigsPath_7 = value;
		Il2CppCodeGenWriteBarrier((&___OdinResourcesConfigsPath_7), value);
	}

	inline static int32_t get_offset_of_OdinTempPath_8() { return static_cast<int32_t>(offsetof(SirenixAssetPaths_t1482981763_StaticFields, ___OdinTempPath_8)); }
	inline String_t* get_OdinTempPath_8() const { return ___OdinTempPath_8; }
	inline String_t** get_address_of_OdinTempPath_8() { return &___OdinTempPath_8; }
	inline void set_OdinTempPath_8(String_t* value)
	{
		___OdinTempPath_8 = value;
		Il2CppCodeGenWriteBarrier((&___OdinTempPath_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIRENIXASSETPATHS_T1482981763_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T1198674588_H
#define U3CU3EC__DISPLAYCLASS10_0_T1198674588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t1198674588  : public RuntimeObject
{
public:
	// System.Char[] Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass10_0::invalids
	CharU5BU5D_t3528271667* ___invalids_0;
	// System.Char Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass10_0::replace
	Il2CppChar ___replace_1;

public:
	inline static int32_t get_offset_of_invalids_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1198674588, ___invalids_0)); }
	inline CharU5BU5D_t3528271667* get_invalids_0() const { return ___invalids_0; }
	inline CharU5BU5D_t3528271667** get_address_of_invalids_0() { return &___invalids_0; }
	inline void set_invalids_0(CharU5BU5D_t3528271667* value)
	{
		___invalids_0 = value;
		Il2CppCodeGenWriteBarrier((&___invalids_0), value);
	}

	inline static int32_t get_offset_of_replace_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1198674588, ___replace_1)); }
	inline Il2CppChar get_replace_1() const { return ___replace_1; }
	inline Il2CppChar* get_address_of_replace_1() { return &___replace_1; }
	inline void set_replace_1(Il2CppChar value)
	{
		___replace_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T1198674588_H
#ifndef STRINGUTILITIES_T1488277914_H
#define STRINGUTILITIES_T1488277914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.StringUtilities
struct  StringUtilities_t1488277914  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILITIES_T1488277914_H
#ifndef UNITYVERSION_T2571170002_H
#define UNITYVERSION_T2571170002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.UnityVersion
struct  UnityVersion_t2571170002  : public RuntimeObject
{
public:

public:
};

struct UnityVersion_t2571170002_StaticFields
{
public:
	// System.Int32 Sirenix.Utilities.UnityVersion::Major
	int32_t ___Major_0;
	// System.Int32 Sirenix.Utilities.UnityVersion::Minor
	int32_t ___Minor_1;

public:
	inline static int32_t get_offset_of_Major_0() { return static_cast<int32_t>(offsetof(UnityVersion_t2571170002_StaticFields, ___Major_0)); }
	inline int32_t get_Major_0() const { return ___Major_0; }
	inline int32_t* get_address_of_Major_0() { return &___Major_0; }
	inline void set_Major_0(int32_t value)
	{
		___Major_0 = value;
	}

	inline static int32_t get_offset_of_Minor_1() { return static_cast<int32_t>(offsetof(UnityVersion_t2571170002_StaticFields, ___Minor_1)); }
	inline int32_t get_Minor_1() const { return ___Minor_1; }
	inline int32_t* get_address_of_Minor_1() { return &___Minor_1; }
	inline void set_Minor_1(int32_t value)
	{
		___Minor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVERSION_T2571170002_H
#ifndef UNSAFEUTILITIES_T4279943386_H
#define UNSAFEUTILITIES_T4279943386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.Unsafe.UnsafeUtilities
struct  UnsafeUtilities_t4279943386  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSAFEUTILITIES_T4279943386_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef PURCHASESERVICE_T2241894207_H
#define PURCHASESERVICE_T2241894207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ChannelPurchase.PurchaseService
struct  PurchaseService_t2241894207  : public RuntimeObject
{
public:

public:
};

struct PurchaseService_t2241894207_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.ChannelPurchase.PurchaseService::serviceClass
	AndroidJavaClass_t32045322 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(PurchaseService_t2241894207_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_t32045322 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_t32045322 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASESERVICE_T2241894207_H
#ifndef CLIENTSCENE_T3640716971_H
#define CLIENTSCENE_T3640716971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientScene
struct  ClientScene_t3640716971  : public RuntimeObject
{
public:

public:
};

struct ClientScene_t3640716971_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController> UnityEngine.Networking.ClientScene::s_LocalPlayers
	List_1_t1968562558 * ___s_LocalPlayers_0;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.ClientScene::s_ReadyConnection
	NetworkConnection_t2705220091 * ___s_ReadyConnection_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.ClientScene::s_SpawnableObjects
	Dictionary_2_t2559669019 * ___s_SpawnableObjects_2;
	// System.Boolean UnityEngine.Networking.ClientScene::s_IsReady
	bool ___s_IsReady_3;
	// System.Boolean UnityEngine.Networking.ClientScene::s_IsSpawnFinished
	bool ___s_IsSpawnFinished_4;
	// UnityEngine.Networking.NetworkScene UnityEngine.Networking.ClientScene::s_NetworkScene
	NetworkScene_t3519296737 * ___s_NetworkScene_5;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnSceneMessage
	ObjectSpawnSceneMessage_t2191101100 * ___s_ObjectSpawnSceneMessage_6;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnFinishedMessage
	ObjectSpawnFinishedMessage_t2314084871 * ___s_ObjectSpawnFinishedMessage_7;
	// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage UnityEngine.Networking.ClientScene::s_ObjectDestroyMessage
	ObjectDestroyMessage_t1358562099 * ___s_ObjectDestroyMessage_8;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnMessage
	ObjectSpawnMessage_t10889831 * ___s_ObjectSpawnMessage_9;
	// UnityEngine.Networking.NetworkSystem.OwnerMessage UnityEngine.Networking.ClientScene::s_OwnerMessage
	OwnerMessage_t4130858210 * ___s_OwnerMessage_10;
	// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage UnityEngine.Networking.ClientScene::s_ClientAuthorityMessage
	ClientAuthorityMessage_t2167651785 * ___s_ClientAuthorityMessage_11;
	// System.Int32 UnityEngine.Networking.ClientScene::s_ReconnectId
	int32_t ___s_ReconnectId_12;
	// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[] UnityEngine.Networking.ClientScene::s_Peers
	PeerInfoMessageU5BU5D_t2256646024* ___s_Peers_13;
	// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner> UnityEngine.Networking.ClientScene::s_PendingOwnerIds
	List_1_t517180936 * ___s_PendingOwnerIds_14;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache0
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache0_15;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache1
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache1_16;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache2
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache2_17;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache3
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache3_18;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache4
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache4_19;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache5
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache5_20;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache6
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache6_21;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache7
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache7_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache8
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache8_23;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache9
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache9_24;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cacheA
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cacheA_25;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cacheB
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cacheB_26;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cacheC
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cacheC_27;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cacheD
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cacheD_28;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cacheE
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cacheE_29;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cacheF
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cacheF_30;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache10
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache10_31;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache11
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache11_32;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mg$cache12
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache12_33;

public:
	inline static int32_t get_offset_of_s_LocalPlayers_0() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_LocalPlayers_0)); }
	inline List_1_t1968562558 * get_s_LocalPlayers_0() const { return ___s_LocalPlayers_0; }
	inline List_1_t1968562558 ** get_address_of_s_LocalPlayers_0() { return &___s_LocalPlayers_0; }
	inline void set_s_LocalPlayers_0(List_1_t1968562558 * value)
	{
		___s_LocalPlayers_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalPlayers_0), value);
	}

	inline static int32_t get_offset_of_s_ReadyConnection_1() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ReadyConnection_1)); }
	inline NetworkConnection_t2705220091 * get_s_ReadyConnection_1() const { return ___s_ReadyConnection_1; }
	inline NetworkConnection_t2705220091 ** get_address_of_s_ReadyConnection_1() { return &___s_ReadyConnection_1; }
	inline void set_s_ReadyConnection_1(NetworkConnection_t2705220091 * value)
	{
		___s_ReadyConnection_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadyConnection_1), value);
	}

	inline static int32_t get_offset_of_s_SpawnableObjects_2() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_SpawnableObjects_2)); }
	inline Dictionary_2_t2559669019 * get_s_SpawnableObjects_2() const { return ___s_SpawnableObjects_2; }
	inline Dictionary_2_t2559669019 ** get_address_of_s_SpawnableObjects_2() { return &___s_SpawnableObjects_2; }
	inline void set_s_SpawnableObjects_2(Dictionary_2_t2559669019 * value)
	{
		___s_SpawnableObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_SpawnableObjects_2), value);
	}

	inline static int32_t get_offset_of_s_IsReady_3() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_IsReady_3)); }
	inline bool get_s_IsReady_3() const { return ___s_IsReady_3; }
	inline bool* get_address_of_s_IsReady_3() { return &___s_IsReady_3; }
	inline void set_s_IsReady_3(bool value)
	{
		___s_IsReady_3 = value;
	}

	inline static int32_t get_offset_of_s_IsSpawnFinished_4() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_IsSpawnFinished_4)); }
	inline bool get_s_IsSpawnFinished_4() const { return ___s_IsSpawnFinished_4; }
	inline bool* get_address_of_s_IsSpawnFinished_4() { return &___s_IsSpawnFinished_4; }
	inline void set_s_IsSpawnFinished_4(bool value)
	{
		___s_IsSpawnFinished_4 = value;
	}

	inline static int32_t get_offset_of_s_NetworkScene_5() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_NetworkScene_5)); }
	inline NetworkScene_t3519296737 * get_s_NetworkScene_5() const { return ___s_NetworkScene_5; }
	inline NetworkScene_t3519296737 ** get_address_of_s_NetworkScene_5() { return &___s_NetworkScene_5; }
	inline void set_s_NetworkScene_5(NetworkScene_t3519296737 * value)
	{
		___s_NetworkScene_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_NetworkScene_5), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnSceneMessage_6() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ObjectSpawnSceneMessage_6)); }
	inline ObjectSpawnSceneMessage_t2191101100 * get_s_ObjectSpawnSceneMessage_6() const { return ___s_ObjectSpawnSceneMessage_6; }
	inline ObjectSpawnSceneMessage_t2191101100 ** get_address_of_s_ObjectSpawnSceneMessage_6() { return &___s_ObjectSpawnSceneMessage_6; }
	inline void set_s_ObjectSpawnSceneMessage_6(ObjectSpawnSceneMessage_t2191101100 * value)
	{
		___s_ObjectSpawnSceneMessage_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnSceneMessage_6), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnFinishedMessage_7() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ObjectSpawnFinishedMessage_7)); }
	inline ObjectSpawnFinishedMessage_t2314084871 * get_s_ObjectSpawnFinishedMessage_7() const { return ___s_ObjectSpawnFinishedMessage_7; }
	inline ObjectSpawnFinishedMessage_t2314084871 ** get_address_of_s_ObjectSpawnFinishedMessage_7() { return &___s_ObjectSpawnFinishedMessage_7; }
	inline void set_s_ObjectSpawnFinishedMessage_7(ObjectSpawnFinishedMessage_t2314084871 * value)
	{
		___s_ObjectSpawnFinishedMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnFinishedMessage_7), value);
	}

	inline static int32_t get_offset_of_s_ObjectDestroyMessage_8() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ObjectDestroyMessage_8)); }
	inline ObjectDestroyMessage_t1358562099 * get_s_ObjectDestroyMessage_8() const { return ___s_ObjectDestroyMessage_8; }
	inline ObjectDestroyMessage_t1358562099 ** get_address_of_s_ObjectDestroyMessage_8() { return &___s_ObjectDestroyMessage_8; }
	inline void set_s_ObjectDestroyMessage_8(ObjectDestroyMessage_t1358562099 * value)
	{
		___s_ObjectDestroyMessage_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectDestroyMessage_8), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnMessage_9() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ObjectSpawnMessage_9)); }
	inline ObjectSpawnMessage_t10889831 * get_s_ObjectSpawnMessage_9() const { return ___s_ObjectSpawnMessage_9; }
	inline ObjectSpawnMessage_t10889831 ** get_address_of_s_ObjectSpawnMessage_9() { return &___s_ObjectSpawnMessage_9; }
	inline void set_s_ObjectSpawnMessage_9(ObjectSpawnMessage_t10889831 * value)
	{
		___s_ObjectSpawnMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnMessage_9), value);
	}

	inline static int32_t get_offset_of_s_OwnerMessage_10() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_OwnerMessage_10)); }
	inline OwnerMessage_t4130858210 * get_s_OwnerMessage_10() const { return ___s_OwnerMessage_10; }
	inline OwnerMessage_t4130858210 ** get_address_of_s_OwnerMessage_10() { return &___s_OwnerMessage_10; }
	inline void set_s_OwnerMessage_10(OwnerMessage_t4130858210 * value)
	{
		___s_OwnerMessage_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_OwnerMessage_10), value);
	}

	inline static int32_t get_offset_of_s_ClientAuthorityMessage_11() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ClientAuthorityMessage_11)); }
	inline ClientAuthorityMessage_t2167651785 * get_s_ClientAuthorityMessage_11() const { return ___s_ClientAuthorityMessage_11; }
	inline ClientAuthorityMessage_t2167651785 ** get_address_of_s_ClientAuthorityMessage_11() { return &___s_ClientAuthorityMessage_11; }
	inline void set_s_ClientAuthorityMessage_11(ClientAuthorityMessage_t2167651785 * value)
	{
		___s_ClientAuthorityMessage_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClientAuthorityMessage_11), value);
	}

	inline static int32_t get_offset_of_s_ReconnectId_12() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_ReconnectId_12)); }
	inline int32_t get_s_ReconnectId_12() const { return ___s_ReconnectId_12; }
	inline int32_t* get_address_of_s_ReconnectId_12() { return &___s_ReconnectId_12; }
	inline void set_s_ReconnectId_12(int32_t value)
	{
		___s_ReconnectId_12 = value;
	}

	inline static int32_t get_offset_of_s_Peers_13() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_Peers_13)); }
	inline PeerInfoMessageU5BU5D_t2256646024* get_s_Peers_13() const { return ___s_Peers_13; }
	inline PeerInfoMessageU5BU5D_t2256646024** get_address_of_s_Peers_13() { return &___s_Peers_13; }
	inline void set_s_Peers_13(PeerInfoMessageU5BU5D_t2256646024* value)
	{
		___s_Peers_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_Peers_13), value);
	}

	inline static int32_t get_offset_of_s_PendingOwnerIds_14() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___s_PendingOwnerIds_14)); }
	inline List_1_t517180936 * get_s_PendingOwnerIds_14() const { return ___s_PendingOwnerIds_14; }
	inline List_1_t517180936 ** get_address_of_s_PendingOwnerIds_14() { return &___s_PendingOwnerIds_14; }
	inline void set_s_PendingOwnerIds_14(List_1_t517180936 * value)
	{
		___s_PendingOwnerIds_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_PendingOwnerIds_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_16() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache1_16)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache1_16() const { return ___U3CU3Ef__mgU24cache1_16; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache1_16() { return &___U3CU3Ef__mgU24cache1_16; }
	inline void set_U3CU3Ef__mgU24cache1_16(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_17() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache2_17)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache2_17() const { return ___U3CU3Ef__mgU24cache2_17; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache2_17() { return &___U3CU3Ef__mgU24cache2_17; }
	inline void set_U3CU3Ef__mgU24cache2_17(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_18() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache3_18)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache3_18() const { return ___U3CU3Ef__mgU24cache3_18; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache3_18() { return &___U3CU3Ef__mgU24cache3_18; }
	inline void set_U3CU3Ef__mgU24cache3_18(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache3_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_19() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache4_19)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache4_19() const { return ___U3CU3Ef__mgU24cache4_19; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache4_19() { return &___U3CU3Ef__mgU24cache4_19; }
	inline void set_U3CU3Ef__mgU24cache4_19(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache4_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_20() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache5_20)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache5_20() const { return ___U3CU3Ef__mgU24cache5_20; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache5_20() { return &___U3CU3Ef__mgU24cache5_20; }
	inline void set_U3CU3Ef__mgU24cache5_20(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache5_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_21() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache6_21)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache6_21() const { return ___U3CU3Ef__mgU24cache6_21; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache6_21() { return &___U3CU3Ef__mgU24cache6_21; }
	inline void set_U3CU3Ef__mgU24cache6_21(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache6_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_22() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache7_22)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache7_22() const { return ___U3CU3Ef__mgU24cache7_22; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache7_22() { return &___U3CU3Ef__mgU24cache7_22; }
	inline void set_U3CU3Ef__mgU24cache7_22(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache7_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_23() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache8_23)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache8_23() const { return ___U3CU3Ef__mgU24cache8_23; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache8_23() { return &___U3CU3Ef__mgU24cache8_23; }
	inline void set_U3CU3Ef__mgU24cache8_23(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache8_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_24() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache9_24)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache9_24() const { return ___U3CU3Ef__mgU24cache9_24; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache9_24() { return &___U3CU3Ef__mgU24cache9_24; }
	inline void set_U3CU3Ef__mgU24cache9_24(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache9_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_25() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cacheA_25)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cacheA_25() const { return ___U3CU3Ef__mgU24cacheA_25; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cacheA_25() { return &___U3CU3Ef__mgU24cacheA_25; }
	inline void set_U3CU3Ef__mgU24cacheA_25(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cacheA_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_26() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cacheB_26)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cacheB_26() const { return ___U3CU3Ef__mgU24cacheB_26; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cacheB_26() { return &___U3CU3Ef__mgU24cacheB_26; }
	inline void set_U3CU3Ef__mgU24cacheB_26(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cacheB_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_27() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cacheC_27)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cacheC_27() const { return ___U3CU3Ef__mgU24cacheC_27; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cacheC_27() { return &___U3CU3Ef__mgU24cacheC_27; }
	inline void set_U3CU3Ef__mgU24cacheC_27(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cacheC_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_28() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cacheD_28)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cacheD_28() const { return ___U3CU3Ef__mgU24cacheD_28; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cacheD_28() { return &___U3CU3Ef__mgU24cacheD_28; }
	inline void set_U3CU3Ef__mgU24cacheD_28(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cacheD_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_29() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cacheE_29)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cacheE_29() const { return ___U3CU3Ef__mgU24cacheE_29; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cacheE_29() { return &___U3CU3Ef__mgU24cacheE_29; }
	inline void set_U3CU3Ef__mgU24cacheE_29(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cacheE_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_30() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cacheF_30)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cacheF_30() const { return ___U3CU3Ef__mgU24cacheF_30; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cacheF_30() { return &___U3CU3Ef__mgU24cacheF_30; }
	inline void set_U3CU3Ef__mgU24cacheF_30(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cacheF_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_31() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache10_31)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache10_31() const { return ___U3CU3Ef__mgU24cache10_31; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache10_31() { return &___U3CU3Ef__mgU24cache10_31; }
	inline void set_U3CU3Ef__mgU24cache10_31(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache10_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_32() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache11_32)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache11_32() const { return ___U3CU3Ef__mgU24cache11_32; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache11_32() { return &___U3CU3Ef__mgU24cache11_32; }
	inline void set_U3CU3Ef__mgU24cache11_32(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache11_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_33() { return static_cast<int32_t>(offsetof(ClientScene_t3640716971_StaticFields, ___U3CU3Ef__mgU24cache12_33)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache12_33() const { return ___U3CU3Ef__mgU24cache12_33; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache12_33() { return &___U3CU3Ef__mgU24cache12_33; }
	inline void set_U3CU3Ef__mgU24cache12_33(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache12_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSCENE_T3640716971_H
#ifndef DOTNETCOMPATIBILITY_T842745520_H
#define DOTNETCOMPATIBILITY_T842745520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DotNetCompatibility
struct  DotNetCompatibility_t842745520  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTNETCOMPATIBILITY_T842745520_H
#ifndef FACEBOOKSTOREBINDINGS_T197114713_H
#define FACEBOOKSTOREBINDINGS_T197114713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FacebookStoreBindings
struct  FacebookStoreBindings_t197114713  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSTOREBINDINGS_T197114713_H
#ifndef OSXSTOREBINDINGS_T1795895004_H
#define OSXSTOREBINDINGS_T1795895004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.OSXStoreBindings
struct  OSXStoreBindings_t1795895004  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSXSTOREBINDINGS_T1795895004_H
#ifndef APPLERECEIPTPARSER_T1990668756_H
#define APPLERECEIPTPARSER_T1990668756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleReceiptParser
struct  AppleReceiptParser_t1990668756  : public RuntimeObject
{
public:

public:
};

struct AppleReceiptParser_t1990668756_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.Security.AppleReceiptParser::_mostRecentReceiptData
	Dictionary_2_t2865362463 * ____mostRecentReceiptData_0;

public:
	inline static int32_t get_offset_of__mostRecentReceiptData_0() { return static_cast<int32_t>(offsetof(AppleReceiptParser_t1990668756_StaticFields, ____mostRecentReceiptData_0)); }
	inline Dictionary_2_t2865362463 * get__mostRecentReceiptData_0() const { return ____mostRecentReceiptData_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__mostRecentReceiptData_0() { return &____mostRecentReceiptData_0; }
	inline void set__mostRecentReceiptData_0(Dictionary_2_t2865362463 * value)
	{
		____mostRecentReceiptData_0 = value;
		Il2CppCodeGenWriteBarrier((&____mostRecentReceiptData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLERECEIPTPARSER_T1990668756_H
#ifndef APPLEVALIDATOR_T513798189_H
#define APPLEVALIDATOR_T513798189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleValidator
struct  AppleValidator_t513798189  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Security.X509Cert UnityEngine.Purchasing.Security.AppleValidator::cert
	X509Cert_t3147783796 * ___cert_0;
	// UnityEngine.Purchasing.Security.AppleReceiptParser UnityEngine.Purchasing.Security.AppleValidator::parser
	AppleReceiptParser_t1990668756 * ___parser_1;

public:
	inline static int32_t get_offset_of_cert_0() { return static_cast<int32_t>(offsetof(AppleValidator_t513798189, ___cert_0)); }
	inline X509Cert_t3147783796 * get_cert_0() const { return ___cert_0; }
	inline X509Cert_t3147783796 ** get_address_of_cert_0() { return &___cert_0; }
	inline void set_cert_0(X509Cert_t3147783796 * value)
	{
		___cert_0 = value;
		Il2CppCodeGenWriteBarrier((&___cert_0), value);
	}

	inline static int32_t get_offset_of_parser_1() { return static_cast<int32_t>(offsetof(AppleValidator_t513798189, ___parser_1)); }
	inline AppleReceiptParser_t1990668756 * get_parser_1() const { return ___parser_1; }
	inline AppleReceiptParser_t1990668756 ** get_address_of_parser_1() { return &___parser_1; }
	inline void set_parser_1(AppleReceiptParser_t1990668756 * value)
	{
		___parser_1 = value;
		Il2CppCodeGenWriteBarrier((&___parser_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLEVALIDATOR_T513798189_H
#ifndef CROSSPLATFORMVALIDATOR_T4140375513_H
#define CROSSPLATFORMVALIDATOR_T4140375513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct  CrossPlatformValidator_t4140375513  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Security.GooglePlayValidator UnityEngine.Purchasing.Security.CrossPlatformValidator::google
	GooglePlayValidator_t216820094 * ___google_0;
	// UnityEngine.Purchasing.Security.UnityChannelValidator UnityEngine.Purchasing.Security.CrossPlatformValidator::unityChannel
	UnityChannelValidator_t2457973196 * ___unityChannel_1;
	// UnityEngine.Purchasing.Security.AppleValidator UnityEngine.Purchasing.Security.CrossPlatformValidator::apple
	AppleValidator_t513798189 * ___apple_2;
	// System.String UnityEngine.Purchasing.Security.CrossPlatformValidator::googleBundleId
	String_t* ___googleBundleId_3;
	// System.String UnityEngine.Purchasing.Security.CrossPlatformValidator::appleBundleId
	String_t* ___appleBundleId_4;

public:
	inline static int32_t get_offset_of_google_0() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t4140375513, ___google_0)); }
	inline GooglePlayValidator_t216820094 * get_google_0() const { return ___google_0; }
	inline GooglePlayValidator_t216820094 ** get_address_of_google_0() { return &___google_0; }
	inline void set_google_0(GooglePlayValidator_t216820094 * value)
	{
		___google_0 = value;
		Il2CppCodeGenWriteBarrier((&___google_0), value);
	}

	inline static int32_t get_offset_of_unityChannel_1() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t4140375513, ___unityChannel_1)); }
	inline UnityChannelValidator_t2457973196 * get_unityChannel_1() const { return ___unityChannel_1; }
	inline UnityChannelValidator_t2457973196 ** get_address_of_unityChannel_1() { return &___unityChannel_1; }
	inline void set_unityChannel_1(UnityChannelValidator_t2457973196 * value)
	{
		___unityChannel_1 = value;
		Il2CppCodeGenWriteBarrier((&___unityChannel_1), value);
	}

	inline static int32_t get_offset_of_apple_2() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t4140375513, ___apple_2)); }
	inline AppleValidator_t513798189 * get_apple_2() const { return ___apple_2; }
	inline AppleValidator_t513798189 ** get_address_of_apple_2() { return &___apple_2; }
	inline void set_apple_2(AppleValidator_t513798189 * value)
	{
		___apple_2 = value;
		Il2CppCodeGenWriteBarrier((&___apple_2), value);
	}

	inline static int32_t get_offset_of_googleBundleId_3() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t4140375513, ___googleBundleId_3)); }
	inline String_t* get_googleBundleId_3() const { return ___googleBundleId_3; }
	inline String_t** get_address_of_googleBundleId_3() { return &___googleBundleId_3; }
	inline void set_googleBundleId_3(String_t* value)
	{
		___googleBundleId_3 = value;
		Il2CppCodeGenWriteBarrier((&___googleBundleId_3), value);
	}

	inline static int32_t get_offset_of_appleBundleId_4() { return static_cast<int32_t>(offsetof(CrossPlatformValidator_t4140375513, ___appleBundleId_4)); }
	inline String_t* get_appleBundleId_4() const { return ___appleBundleId_4; }
	inline String_t** get_address_of_appleBundleId_4() { return &___appleBundleId_4; }
	inline void set_appleBundleId_4(String_t* value)
	{
		___appleBundleId_4 = value;
		Il2CppCodeGenWriteBarrier((&___appleBundleId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMVALIDATOR_T4140375513_H
#ifndef DISTINGUISHEDNAME_T1591151536_H
#define DISTINGUISHEDNAME_T1591151536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.DistinguishedName
struct  DistinguishedName_t1591151536  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<Country>k__BackingField
	String_t* ___U3CCountryU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<Organization>k__BackingField
	String_t* ___U3COrganizationU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<OrganizationalUnit>k__BackingField
	String_t* ___U3COrganizationalUnitU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<Dnq>k__BackingField
	String_t* ___U3CDnqU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<State>k__BackingField
	String_t* ___U3CStateU3Ek__BackingField_4;
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<CommonName>k__BackingField
	String_t* ___U3CCommonNameU3Ek__BackingField_5;
	// System.String UnityEngine.Purchasing.Security.DistinguishedName::<SerialNumber>k__BackingField
	String_t* ___U3CSerialNumberU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CCountryU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3CCountryU3Ek__BackingField_0)); }
	inline String_t* get_U3CCountryU3Ek__BackingField_0() const { return ___U3CCountryU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCountryU3Ek__BackingField_0() { return &___U3CCountryU3Ek__BackingField_0; }
	inline void set_U3CCountryU3Ek__BackingField_0(String_t* value)
	{
		___U3CCountryU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCountryU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COrganizationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3COrganizationU3Ek__BackingField_1)); }
	inline String_t* get_U3COrganizationU3Ek__BackingField_1() const { return ___U3COrganizationU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3COrganizationU3Ek__BackingField_1() { return &___U3COrganizationU3Ek__BackingField_1; }
	inline void set_U3COrganizationU3Ek__BackingField_1(String_t* value)
	{
		___U3COrganizationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3COrganizationU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COrganizationalUnitU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3COrganizationalUnitU3Ek__BackingField_2)); }
	inline String_t* get_U3COrganizationalUnitU3Ek__BackingField_2() const { return ___U3COrganizationalUnitU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3COrganizationalUnitU3Ek__BackingField_2() { return &___U3COrganizationalUnitU3Ek__BackingField_2; }
	inline void set_U3COrganizationalUnitU3Ek__BackingField_2(String_t* value)
	{
		___U3COrganizationalUnitU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COrganizationalUnitU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CDnqU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3CDnqU3Ek__BackingField_3)); }
	inline String_t* get_U3CDnqU3Ek__BackingField_3() const { return ___U3CDnqU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDnqU3Ek__BackingField_3() { return &___U3CDnqU3Ek__BackingField_3; }
	inline void set_U3CDnqU3Ek__BackingField_3(String_t* value)
	{
		___U3CDnqU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDnqU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3CStateU3Ek__BackingField_4)); }
	inline String_t* get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(String_t* value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCommonNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3CCommonNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CCommonNameU3Ek__BackingField_5() const { return ___U3CCommonNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCommonNameU3Ek__BackingField_5() { return &___U3CCommonNameU3Ek__BackingField_5; }
	inline void set_U3CCommonNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CCommonNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommonNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSerialNumberU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DistinguishedName_t1591151536, ___U3CSerialNumberU3Ek__BackingField_6)); }
	inline String_t* get_U3CSerialNumberU3Ek__BackingField_6() const { return ___U3CSerialNumberU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CSerialNumberU3Ek__BackingField_6() { return &___U3CSerialNumberU3Ek__BackingField_6; }
	inline void set_U3CSerialNumberU3Ek__BackingField_6(String_t* value)
	{
		___U3CSerialNumberU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSerialNumberU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTINGUISHEDNAME_T1591151536_H
#ifndef GOOGLEPLAYVALIDATOR_T216820094_H
#define GOOGLEPLAYVALIDATOR_T216820094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GooglePlayValidator
struct  GooglePlayValidator_t216820094  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Security.RSAKey UnityEngine.Purchasing.Security.GooglePlayValidator::key
	RSAKey_t3751505760 * ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(GooglePlayValidator_t216820094, ___key_0)); }
	inline RSAKey_t3751505760 * get_key_0() const { return ___key_0; }
	inline RSAKey_t3751505760 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RSAKey_t3751505760 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYVALIDATOR_T216820094_H
#ifndef OBFUSCATOR_T3490180744_H
#define OBFUSCATOR_T3490180744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.Obfuscator
struct  Obfuscator_t3490180744  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBFUSCATOR_T3490180744_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T4106353832_H
#define U3CU3EC__DISPLAYCLASS1_0_T4106353832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.Obfuscator/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t4106353832  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Security.Obfuscator/<>c__DisplayClass1_0::key
	int32_t ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t4106353832, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T4106353832_H
#ifndef PKCS7_T350312378_H
#define PKCS7_T350312378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.PKCS7
struct  PKCS7_t350312378  : public RuntimeObject
{
public:
	// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.PKCS7::root
	Asn1Node_t84807007 * ___root_0;
	// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.PKCS7::<data>k__BackingField
	Asn1Node_t84807007 * ___U3CdataU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo> UnityEngine.Purchasing.Security.PKCS7::<sinfos>k__BackingField
	List_1_t2451613916 * ___U3CsinfosU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert> UnityEngine.Purchasing.Security.PKCS7::<certChain>k__BackingField
	List_1_t324891242 * ___U3CcertChainU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Purchasing.Security.PKCS7::validStructure
	bool ___validStructure_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(PKCS7_t350312378, ___root_0)); }
	inline Asn1Node_t84807007 * get_root_0() const { return ___root_0; }
	inline Asn1Node_t84807007 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(Asn1Node_t84807007 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U3CdataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PKCS7_t350312378, ___U3CdataU3Ek__BackingField_1)); }
	inline Asn1Node_t84807007 * get_U3CdataU3Ek__BackingField_1() const { return ___U3CdataU3Ek__BackingField_1; }
	inline Asn1Node_t84807007 ** get_address_of_U3CdataU3Ek__BackingField_1() { return &___U3CdataU3Ek__BackingField_1; }
	inline void set_U3CdataU3Ek__BackingField_1(Asn1Node_t84807007 * value)
	{
		___U3CdataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsinfosU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PKCS7_t350312378, ___U3CsinfosU3Ek__BackingField_2)); }
	inline List_1_t2451613916 * get_U3CsinfosU3Ek__BackingField_2() const { return ___U3CsinfosU3Ek__BackingField_2; }
	inline List_1_t2451613916 ** get_address_of_U3CsinfosU3Ek__BackingField_2() { return &___U3CsinfosU3Ek__BackingField_2; }
	inline void set_U3CsinfosU3Ek__BackingField_2(List_1_t2451613916 * value)
	{
		___U3CsinfosU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsinfosU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcertChainU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PKCS7_t350312378, ___U3CcertChainU3Ek__BackingField_3)); }
	inline List_1_t324891242 * get_U3CcertChainU3Ek__BackingField_3() const { return ___U3CcertChainU3Ek__BackingField_3; }
	inline List_1_t324891242 ** get_address_of_U3CcertChainU3Ek__BackingField_3() { return &___U3CcertChainU3Ek__BackingField_3; }
	inline void set_U3CcertChainU3Ek__BackingField_3(List_1_t324891242 * value)
	{
		___U3CcertChainU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcertChainU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_validStructure_4() { return static_cast<int32_t>(offsetof(PKCS7_t350312378, ___validStructure_4)); }
	inline bool get_validStructure_4() const { return ___validStructure_4; }
	inline bool* get_address_of_validStructure_4() { return &___validStructure_4; }
	inline void set_validStructure_4(bool value)
	{
		___validStructure_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS7_T350312378_H
#ifndef RSAKEY_T3751505760_H
#define RSAKEY_T3751505760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.RSAKey
struct  RSAKey_t3751505760  : public RuntimeObject
{
public:
	// System.Security.Cryptography.RSACryptoServiceProvider UnityEngine.Purchasing.Security.RSAKey::<rsa>k__BackingField
	RSACryptoServiceProvider_t2683512874 * ___U3CrsaU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CrsaU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RSAKey_t3751505760, ___U3CrsaU3Ek__BackingField_0)); }
	inline RSACryptoServiceProvider_t2683512874 * get_U3CrsaU3Ek__BackingField_0() const { return ___U3CrsaU3Ek__BackingField_0; }
	inline RSACryptoServiceProvider_t2683512874 ** get_address_of_U3CrsaU3Ek__BackingField_0() { return &___U3CrsaU3Ek__BackingField_0; }
	inline void set_U3CrsaU3Ek__BackingField_0(RSACryptoServiceProvider_t2683512874 * value)
	{
		___U3CrsaU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrsaU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAKEY_T3751505760_H
#ifndef SIGNERINFO_T979539174_H
#define SIGNERINFO_T979539174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.SignerInfo
struct  SignerInfo_t979539174  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Security.SignerInfo::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.SignerInfo::<IssuerSerialNumber>k__BackingField
	String_t* ___U3CIssuerSerialNumberU3Ek__BackingField_1;
	// System.Byte[] UnityEngine.Purchasing.Security.SignerInfo::<EncryptedDigest>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CEncryptedDigestU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SignerInfo_t979539174, ___U3CVersionU3Ek__BackingField_0)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_0() const { return ___U3CVersionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_0() { return &___U3CVersionU3Ek__BackingField_0; }
	inline void set_U3CVersionU3Ek__BackingField_0(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIssuerSerialNumberU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SignerInfo_t979539174, ___U3CIssuerSerialNumberU3Ek__BackingField_1)); }
	inline String_t* get_U3CIssuerSerialNumberU3Ek__BackingField_1() const { return ___U3CIssuerSerialNumberU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CIssuerSerialNumberU3Ek__BackingField_1() { return &___U3CIssuerSerialNumberU3Ek__BackingField_1; }
	inline void set_U3CIssuerSerialNumberU3Ek__BackingField_1(String_t* value)
	{
		___U3CIssuerSerialNumberU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIssuerSerialNumberU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CEncryptedDigestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SignerInfo_t979539174, ___U3CEncryptedDigestU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t4116647657* get_U3CEncryptedDigestU3Ek__BackingField_2() const { return ___U3CEncryptedDigestU3Ek__BackingField_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CEncryptedDigestU3Ek__BackingField_2() { return &___U3CEncryptedDigestU3Ek__BackingField_2; }
	inline void set_U3CEncryptedDigestU3Ek__BackingField_2(ByteU5BU5D_t4116647657* value)
	{
		___U3CEncryptedDigestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEncryptedDigestU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFO_T979539174_H
#ifndef UNITYCHANNELRECEIPTPARSER_T1594844111_H
#define UNITYCHANNELRECEIPTPARSER_T1594844111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.UnityChannelReceiptParser
struct  UnityChannelReceiptParser_t1594844111  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELRECEIPTPARSER_T1594844111_H
#ifndef UNITYCHANNELVALIDATOR_T2457973196_H
#define UNITYCHANNELVALIDATOR_T2457973196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.UnityChannelValidator
struct  UnityChannelValidator_t2457973196  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Security.RSAKey UnityEngine.Purchasing.Security.UnityChannelValidator::key
	RSAKey_t3751505760 * ___key_0;
	// UnityEngine.Purchasing.Security.UnityChannelReceiptParser UnityEngine.Purchasing.Security.UnityChannelValidator::parser
	UnityChannelReceiptParser_t1594844111 * ___parser_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(UnityChannelValidator_t2457973196, ___key_0)); }
	inline RSAKey_t3751505760 * get_key_0() const { return ___key_0; }
	inline RSAKey_t3751505760 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RSAKey_t3751505760 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_parser_1() { return static_cast<int32_t>(offsetof(UnityChannelValidator_t2457973196, ___parser_1)); }
	inline UnityChannelReceiptParser_t1594844111 * get_parser_1() const { return ___parser_1; }
	inline UnityChannelReceiptParser_t1594844111 ** get_address_of_parser_1() { return &___parser_1; }
	inline void set_parser_1(UnityChannelReceiptParser_t1594844111 * value)
	{
		___parser_1 = value;
		Il2CppCodeGenWriteBarrier((&___parser_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELVALIDATOR_T2457973196_H
#ifndef TIZENSTOREBINDINGS_T4059756115_H
#define TIZENSTOREBINDINGS_T4059756115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TizenStoreBindings
struct  TizenStoreBindings_t4059756115  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIZENSTOREBINDINGS_T4059756115_H
#ifndef IOSSTOREBINDINGS_T3204725121_H
#define IOSSTOREBINDINGS_T3204725121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.iOSStoreBindings
struct  iOSStoreBindings_t3204725121  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSTOREBINDINGS_T3204725121_H
#ifndef APPINFO_T2433711276_H
#define APPINFO_T2433711276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.AppInfo
struct  AppInfo_t2433711276  : public RuntimeObject
{
public:
	// System.String UnityEngine.Store.AppInfo::<appId>k__BackingField
	String_t* ___U3CappIdU3Ek__BackingField_0;
	// System.String UnityEngine.Store.AppInfo::<appKey>k__BackingField
	String_t* ___U3CappKeyU3Ek__BackingField_1;
	// System.String UnityEngine.Store.AppInfo::<clientId>k__BackingField
	String_t* ___U3CclientIdU3Ek__BackingField_2;
	// System.String UnityEngine.Store.AppInfo::<clientKey>k__BackingField
	String_t* ___U3CclientKeyU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Store.AppInfo::<debug>k__BackingField
	bool ___U3CdebugU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CappIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CappIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CappIdU3Ek__BackingField_0() const { return ___U3CappIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CappIdU3Ek__BackingField_0() { return &___U3CappIdU3Ek__BackingField_0; }
	inline void set_U3CappIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CappIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CappKeyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CappKeyU3Ek__BackingField_1)); }
	inline String_t* get_U3CappKeyU3Ek__BackingField_1() const { return ___U3CappKeyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CappKeyU3Ek__BackingField_1() { return &___U3CappKeyU3Ek__BackingField_1; }
	inline void set_U3CappKeyU3Ek__BackingField_1(String_t* value)
	{
		___U3CappKeyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappKeyU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CclientIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CclientIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CclientIdU3Ek__BackingField_2() const { return ___U3CclientIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CclientIdU3Ek__BackingField_2() { return &___U3CclientIdU3Ek__BackingField_2; }
	inline void set_U3CclientIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CclientIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclientIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CclientKeyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CclientKeyU3Ek__BackingField_3)); }
	inline String_t* get_U3CclientKeyU3Ek__BackingField_3() const { return ___U3CclientKeyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CclientKeyU3Ek__BackingField_3() { return &___U3CclientKeyU3Ek__BackingField_3; }
	inline void set_U3CclientKeyU3Ek__BackingField_3(String_t* value)
	{
		___U3CclientKeyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclientKeyU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdebugU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppInfo_t2433711276, ___U3CdebugU3Ek__BackingField_4)); }
	inline bool get_U3CdebugU3Ek__BackingField_4() const { return ___U3CdebugU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CdebugU3Ek__BackingField_4() { return &___U3CdebugU3Ek__BackingField_4; }
	inline void set_U3CdebugU3Ek__BackingField_4(bool value)
	{
		___U3CdebugU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINFO_T2433711276_H
#ifndef STORESERVICE_T295887430_H
#define STORESERVICE_T295887430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.StoreService
struct  StoreService_t295887430  : public RuntimeObject
{
public:

public:
};

struct StoreService_t295887430_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.Store.StoreService::serviceClass
	AndroidJavaClass_t32045322 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(StoreService_t295887430_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_t32045322 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_t32045322 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORESERVICE_T295887430_H
#ifndef USERINFO_T2886425993_H
#define USERINFO_T2886425993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.UserInfo
struct  UserInfo_t2886425993  : public RuntimeObject
{
public:
	// System.String UnityEngine.Store.UserInfo::<channel>k__BackingField
	String_t* ___U3CchannelU3Ek__BackingField_0;
	// System.String UnityEngine.Store.UserInfo::<userId>k__BackingField
	String_t* ___U3CuserIdU3Ek__BackingField_1;
	// System.String UnityEngine.Store.UserInfo::<userLoginToken>k__BackingField
	String_t* ___U3CuserLoginTokenU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CchannelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserInfo_t2886425993, ___U3CchannelU3Ek__BackingField_0)); }
	inline String_t* get_U3CchannelU3Ek__BackingField_0() const { return ___U3CchannelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CchannelU3Ek__BackingField_0() { return &___U3CchannelU3Ek__BackingField_0; }
	inline void set_U3CchannelU3Ek__BackingField_0(String_t* value)
	{
		___U3CchannelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchannelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CuserIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserInfo_t2886425993, ___U3CuserIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CuserIdU3Ek__BackingField_1() const { return ___U3CuserIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CuserIdU3Ek__BackingField_1() { return &___U3CuserIdU3Ek__BackingField_1; }
	inline void set_U3CuserIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CuserIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CuserLoginTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserInfo_t2886425993, ___U3CuserLoginTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CuserLoginTokenU3Ek__BackingField_2() const { return ___U3CuserLoginTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CuserLoginTokenU3Ek__BackingField_2() { return &___U3CuserLoginTokenU3Ek__BackingField_2; }
	inline void set_U3CuserLoginTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CuserLoginTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserLoginTokenU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERINFO_T2886425993_H
#ifndef __STATICARRAYINITTYPESIZEU3D18_T1904425263_H
#define __STATICARRAYINITTYPESIZEU3D18_T1904425263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=18
struct  __StaticArrayInitTypeSizeU3D18_t1904425263 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D18_t1904425263__padding[18];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D18_T1904425263_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T2711125392_H
#define __STATICARRAYINITTYPESIZEU3D32_T2711125392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct  __StaticArrayInitTypeSizeU3D32_t2711125392 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t2711125392__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T2711125392_H
#ifndef RELATIVEOID_T4118021937_H
#define RELATIVEOID_T4118021937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.RelativeOid
struct  RelativeOid_t4118021937  : public Oid_t864847193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIVEOID_T4118021937_H
#ifndef GLOBALCONFIGATTRIBUTE_T397999009_H
#define GLOBALCONFIGATTRIBUTE_T397999009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GlobalConfigAttribute
struct  GlobalConfigAttribute_t397999009  : public Attribute_t861562559
{
public:
	// System.String Sirenix.Utilities.GlobalConfigAttribute::assetPath
	String_t* ___assetPath_0;
	// System.Boolean Sirenix.Utilities.GlobalConfigAttribute::<UseAsset>k__BackingField
	bool ___U3CUseAssetU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_assetPath_0() { return static_cast<int32_t>(offsetof(GlobalConfigAttribute_t397999009, ___assetPath_0)); }
	inline String_t* get_assetPath_0() const { return ___assetPath_0; }
	inline String_t** get_address_of_assetPath_0() { return &___assetPath_0; }
	inline void set_assetPath_0(String_t* value)
	{
		___assetPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetPath_0), value);
	}

	inline static int32_t get_offset_of_U3CUseAssetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GlobalConfigAttribute_t397999009, ___U3CUseAssetU3Ek__BackingField_1)); }
	inline bool get_U3CUseAssetU3Ek__BackingField_1() const { return ___U3CUseAssetU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CUseAssetU3Ek__BackingField_1() { return &___U3CUseAssetU3Ek__BackingField_1; }
	inline void set_U3CUseAssetU3Ek__BackingField_1(bool value)
	{
		___U3CUseAssetU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALCONFIGATTRIBUTE_T397999009_H
#ifndef SIRENIXBUILDNAMEATTRIBUTE_T1047153296_H
#define SIRENIXBUILDNAMEATTRIBUTE_T1047153296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.SirenixBuildNameAttribute
struct  SirenixBuildNameAttribute_t1047153296  : public Attribute_t861562559
{
public:
	// System.String Sirenix.Utilities.SirenixBuildNameAttribute::<BuildName>k__BackingField
	String_t* ___U3CBuildNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBuildNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SirenixBuildNameAttribute_t1047153296, ___U3CBuildNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CBuildNameU3Ek__BackingField_0() const { return ___U3CBuildNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CBuildNameU3Ek__BackingField_0() { return &___U3CBuildNameU3Ek__BackingField_0; }
	inline void set_U3CBuildNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CBuildNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBuildNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIRENIXBUILDNAMEATTRIBUTE_T1047153296_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PURCHASEFORWARDCALLBACK_T3627744015_H
#define PURCHASEFORWARDCALLBACK_T3627744015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ChannelPurchase.PurchaseForwardCallback
struct  PurchaseForwardCallback_t3627744015  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.ChannelPurchase.IPurchaseListener UnityEngine.ChannelPurchase.PurchaseForwardCallback::purchaseListener
	RuntimeObject* ___purchaseListener_0;

public:
	inline static int32_t get_offset_of_purchaseListener_0() { return static_cast<int32_t>(offsetof(PurchaseForwardCallback_t3627744015, ___purchaseListener_0)); }
	inline RuntimeObject* get_purchaseListener_0() const { return ___purchaseListener_0; }
	inline RuntimeObject** get_address_of_purchaseListener_0() { return &___purchaseListener_0; }
	inline void set_purchaseListener_0(RuntimeObject* value)
	{
		___purchaseListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseListener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEFORWARDCALLBACK_T3627744015_H
#ifndef CHANNELPACKET_T1579824718_H
#define CHANNELPACKET_T1579824718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelPacket
struct  ChannelPacket_t1579824718 
{
public:
	// System.Int32 UnityEngine.Networking.ChannelPacket::m_Position
	int32_t ___m_Position_0;
	// System.Byte[] UnityEngine.Networking.ChannelPacket::m_Buffer
	ByteU5BU5D_t4116647657* ___m_Buffer_1;
	// System.Boolean UnityEngine.Networking.ChannelPacket::m_IsReliable
	bool ___m_IsReliable_2;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(ChannelPacket_t1579824718, ___m_Position_0)); }
	inline int32_t get_m_Position_0() const { return ___m_Position_0; }
	inline int32_t* get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(int32_t value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(ChannelPacket_t1579824718, ___m_Buffer_1)); }
	inline ByteU5BU5D_t4116647657* get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(ByteU5BU5D_t4116647657* value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_1), value);
	}

	inline static int32_t get_offset_of_m_IsReliable_2() { return static_cast<int32_t>(offsetof(ChannelPacket_t1579824718, ___m_IsReliable_2)); }
	inline bool get_m_IsReliable_2() const { return ___m_IsReliable_2; }
	inline bool* get_address_of_m_IsReliable_2() { return &___m_IsReliable_2; }
	inline void set_m_IsReliable_2(bool value)
	{
		___m_IsReliable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.ChannelPacket
struct ChannelPacket_t1579824718_marshaled_pinvoke
{
	int32_t ___m_Position_0;
	uint8_t* ___m_Buffer_1;
	int32_t ___m_IsReliable_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.ChannelPacket
struct ChannelPacket_t1579824718_marshaled_com
{
	int32_t ___m_Position_0;
	uint8_t* ___m_Buffer_1;
	int32_t ___m_IsReliable_2;
};
#endif // CHANNELPACKET_T1579824718_H
#ifndef CLIENTATTRIBUTE_T145541712_H
#define CLIENTATTRIBUTE_T145541712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientAttribute
struct  ClientAttribute_t145541712  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTATTRIBUTE_T145541712_H
#ifndef CLIENTCALLBACKATTRIBUTE_T1632794324_H
#define CLIENTCALLBACKATTRIBUTE_T1632794324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientCallbackAttribute
struct  ClientCallbackAttribute_t1632794324  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCALLBACKATTRIBUTE_T1632794324_H
#ifndef INTERNALMSG_T2371755407_H
#define INTERNALMSG_T2371755407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LocalClient/InternalMsg
struct  InternalMsg_t2371755407 
{
public:
	// System.Byte[] UnityEngine.Networking.LocalClient/InternalMsg::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Int32 UnityEngine.Networking.LocalClient/InternalMsg::channelId
	int32_t ___channelId_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(InternalMsg_t2371755407, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_channelId_1() { return static_cast<int32_t>(offsetof(InternalMsg_t2371755407, ___channelId_1)); }
	inline int32_t get_channelId_1() const { return ___channelId_1; }
	inline int32_t* get_address_of_channelId_1() { return &___channelId_1; }
	inline void set_channelId_1(int32_t value)
	{
		___channelId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.LocalClient/InternalMsg
struct InternalMsg_t2371755407_marshaled_pinvoke
{
	uint8_t* ___buffer_0;
	int32_t ___channelId_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.LocalClient/InternalMsg
struct InternalMsg_t2371755407_marshaled_com
{
	uint8_t* ___buffer_0;
	int32_t ___channelId_1;
};
#endif // INTERNALMSG_T2371755407_H
#ifndef NETWORKINSTANCEID_T786350175_H
#define NETWORKINSTANCEID_T786350175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkInstanceId
struct  NetworkInstanceId_t786350175 
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkInstanceId::m_Value
	uint32_t ___m_Value_0;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t786350175, ___m_Value_0)); }
	inline uint32_t get_m_Value_0() const { return ___m_Value_0; }
	inline uint32_t* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(uint32_t value)
	{
		___m_Value_0 = value;
	}
};

struct NetworkInstanceId_t786350175_StaticFields
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Invalid
	NetworkInstanceId_t786350175  ___Invalid_1;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Zero
	NetworkInstanceId_t786350175  ___Zero_2;

public:
	inline static int32_t get_offset_of_Invalid_1() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t786350175_StaticFields, ___Invalid_1)); }
	inline NetworkInstanceId_t786350175  get_Invalid_1() const { return ___Invalid_1; }
	inline NetworkInstanceId_t786350175 * get_address_of_Invalid_1() { return &___Invalid_1; }
	inline void set_Invalid_1(NetworkInstanceId_t786350175  value)
	{
		___Invalid_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t786350175_StaticFields, ___Zero_2)); }
	inline NetworkInstanceId_t786350175  get_Zero_2() const { return ___Zero_2; }
	inline NetworkInstanceId_t786350175 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(NetworkInstanceId_t786350175  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINSTANCEID_T786350175_H
#ifndef IAPSECURITYEXCEPTION_T1844591500_H
#define IAPSECURITYEXCEPTION_T1844591500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.IAPSecurityException
struct  IAPSecurityException_t1844591500  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPSECURITYEXCEPTION_T1844591500_H
#ifndef LOGINFORWARDCALLBACK_T717703418_H
#define LOGINFORWARDCALLBACK_T717703418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.LoginForwardCallback
struct  LoginForwardCallback_t717703418  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.Store.ILoginListener UnityEngine.Store.LoginForwardCallback::loginListener
	RuntimeObject* ___loginListener_0;

public:
	inline static int32_t get_offset_of_loginListener_0() { return static_cast<int32_t>(offsetof(LoginForwardCallback_t717703418, ___loginListener_0)); }
	inline RuntimeObject* get_loginListener_0() const { return ___loginListener_0; }
	inline RuntimeObject** get_address_of_loginListener_0() { return &___loginListener_0; }
	inline void set_loginListener_0(RuntimeObject* value)
	{
		___loginListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___loginListener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINFORWARDCALLBACK_T717703418_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=18 <PrivateImplementationDetails>::83843267F79E9F8816E76A442D8E5C1457F5E016
	__StaticArrayInitTypeSizeU3D18_t1904425263  ___83843267F79E9F8816E76A442D8E5C1457F5E016_0;

public:
	inline static int32_t get_offset_of_U383843267F79E9F8816E76A442D8E5C1457F5E016_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___83843267F79E9F8816E76A442D8E5C1457F5E016_0)); }
	inline __StaticArrayInitTypeSizeU3D18_t1904425263  get_U383843267F79E9F8816E76A442D8E5C1457F5E016_0() const { return ___83843267F79E9F8816E76A442D8E5C1457F5E016_0; }
	inline __StaticArrayInitTypeSizeU3D18_t1904425263 * get_address_of_U383843267F79E9F8816E76A442D8E5C1457F5E016_0() { return &___83843267F79E9F8816E76A442D8E5C1457F5E016_0; }
	inline void set_U383843267F79E9F8816E76A442D8E5C1457F5E016_0(__StaticArrayInitTypeSizeU3D18_t1904425263  value)
	{
		___83843267F79E9F8816E76A442D8E5C1457F5E016_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::59F5BD34B6C013DEACC784F69C67E95150033A84
	__StaticArrayInitTypeSizeU3D32_t2711125392  ___59F5BD34B6C013DEACC784F69C67E95150033A84_0;

public:
	inline static int32_t get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___59F5BD34B6C013DEACC784F69C67E95150033A84_0)); }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392  get_U359F5BD34B6C013DEACC784F69C67E95150033A84_0() const { return ___59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline __StaticArrayInitTypeSizeU3D32_t2711125392 * get_address_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0() { return &___59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline void set_U359F5BD34B6C013DEACC784F69C67E95150033A84_0(__StaticArrayInitTypeSizeU3D32_t2711125392  value)
	{
		___59F5BD34B6C013DEACC784F69C67E95150033A84_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef DATAFORMAT_T4143835455_H
#define DATAFORMAT_T4143835455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DataFormat
struct  DataFormat_t4143835455 
{
public:
	// System.Int32 Sirenix.Serialization.DataFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataFormat_t4143835455, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAFORMAT_T4143835455_H
#ifndef ERRORHANDLINGPOLICY_T3882907225_H
#define ERRORHANDLINGPOLICY_T3882907225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ErrorHandlingPolicy
struct  ErrorHandlingPolicy_t3882907225 
{
public:
	// System.Int32 Sirenix.Serialization.ErrorHandlingPolicy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ErrorHandlingPolicy_t3882907225, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORHANDLINGPOLICY_T3882907225_H
#ifndef LOGGINGPOLICY_T1861080991_H
#define LOGGINGPOLICY_T1861080991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.LoggingPolicy
struct  LoggingPolicy_t1861080991 
{
public:
	// System.Int32 Sirenix.Serialization.LoggingPolicy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoggingPolicy_t1861080991, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGINGPOLICY_T1861080991_H
#ifndef SIRENIXEDITORCONFIGATTRIBUTE_T128736660_H
#define SIRENIXEDITORCONFIGATTRIBUTE_T128736660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.SirenixEditorConfigAttribute
struct  SirenixEditorConfigAttribute_t128736660  : public GlobalConfigAttribute_t397999009
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIRENIXEDITORCONFIGATTRIBUTE_T128736660_H
#ifndef SIRENIXGLOBALCONFIGATTRIBUTE_T4086608552_H
#define SIRENIXGLOBALCONFIGATTRIBUTE_T4086608552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.SirenixGlobalConfigAttribute
struct  SirenixGlobalConfigAttribute_t4086608552  : public GlobalConfigAttribute_t397999009
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIRENIXGLOBALCONFIGATTRIBUTE_T4086608552_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef CHANNELBUFFER_T2335345901_H
#define CHANNELBUFFER_T2335345901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelBuffer
struct  ChannelBuffer_t2335345901  : public RuntimeObject
{
public:
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.ChannelBuffer::m_Connection
	NetworkConnection_t2705220091 * ___m_Connection_0;
	// UnityEngine.Networking.ChannelPacket UnityEngine.Networking.ChannelBuffer::m_CurrentPacket
	ChannelPacket_t1579824718  ___m_CurrentPacket_1;
	// System.Single UnityEngine.Networking.ChannelBuffer::m_LastFlushTime
	float ___m_LastFlushTime_2;
	// System.Byte UnityEngine.Networking.ChannelBuffer::m_ChannelId
	uint8_t ___m_ChannelId_3;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::m_MaxPacketSize
	int32_t ___m_MaxPacketSize_4;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_IsReliable
	bool ___m_IsReliable_5;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_AllowFragmentation
	bool ___m_AllowFragmentation_6;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_IsBroken
	bool ___m_IsBroken_7;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::m_MaxPendingPacketCount
	int32_t ___m_MaxPendingPacketCount_8;
	// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket> UnityEngine.Networking.ChannelBuffer::m_PendingPackets
	Queue_1_t1426084212 * ___m_PendingPackets_12;
	// System.Single UnityEngine.Networking.ChannelBuffer::maxDelay
	float ___maxDelay_15;
	// System.Single UnityEngine.Networking.ChannelBuffer::m_LastBufferedMessageCountTimer
	float ___m_LastBufferedMessageCountTimer_16;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numMsgsOut>k__BackingField
	int32_t ___U3CnumMsgsOutU3Ek__BackingField_17;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBufferedMsgsOut>k__BackingField
	int32_t ___U3CnumBufferedMsgsOutU3Ek__BackingField_18;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBytesOut>k__BackingField
	int32_t ___U3CnumBytesOutU3Ek__BackingField_19;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numMsgsIn>k__BackingField
	int32_t ___U3CnumMsgsInU3Ek__BackingField_20;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBytesIn>k__BackingField
	int32_t ___U3CnumBytesInU3Ek__BackingField_21;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBufferedPerSecond>k__BackingField
	int32_t ___U3CnumBufferedPerSecondU3Ek__BackingField_22;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<lastBufferedPerSecond>k__BackingField
	int32_t ___U3ClastBufferedPerSecondU3Ek__BackingField_23;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_Disposed
	bool ___m_Disposed_27;
	// UnityEngine.Networking.NetBuffer UnityEngine.Networking.ChannelBuffer::fragmentBuffer
	NetBuffer_t2156033743 * ___fragmentBuffer_28;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::readingFragment
	bool ___readingFragment_29;

public:
	inline static int32_t get_offset_of_m_Connection_0() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_Connection_0)); }
	inline NetworkConnection_t2705220091 * get_m_Connection_0() const { return ___m_Connection_0; }
	inline NetworkConnection_t2705220091 ** get_address_of_m_Connection_0() { return &___m_Connection_0; }
	inline void set_m_Connection_0(NetworkConnection_t2705220091 * value)
	{
		___m_Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_0), value);
	}

	inline static int32_t get_offset_of_m_CurrentPacket_1() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_CurrentPacket_1)); }
	inline ChannelPacket_t1579824718  get_m_CurrentPacket_1() const { return ___m_CurrentPacket_1; }
	inline ChannelPacket_t1579824718 * get_address_of_m_CurrentPacket_1() { return &___m_CurrentPacket_1; }
	inline void set_m_CurrentPacket_1(ChannelPacket_t1579824718  value)
	{
		___m_CurrentPacket_1 = value;
	}

	inline static int32_t get_offset_of_m_LastFlushTime_2() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_LastFlushTime_2)); }
	inline float get_m_LastFlushTime_2() const { return ___m_LastFlushTime_2; }
	inline float* get_address_of_m_LastFlushTime_2() { return &___m_LastFlushTime_2; }
	inline void set_m_LastFlushTime_2(float value)
	{
		___m_LastFlushTime_2 = value;
	}

	inline static int32_t get_offset_of_m_ChannelId_3() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_ChannelId_3)); }
	inline uint8_t get_m_ChannelId_3() const { return ___m_ChannelId_3; }
	inline uint8_t* get_address_of_m_ChannelId_3() { return &___m_ChannelId_3; }
	inline void set_m_ChannelId_3(uint8_t value)
	{
		___m_ChannelId_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxPacketSize_4() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_MaxPacketSize_4)); }
	inline int32_t get_m_MaxPacketSize_4() const { return ___m_MaxPacketSize_4; }
	inline int32_t* get_address_of_m_MaxPacketSize_4() { return &___m_MaxPacketSize_4; }
	inline void set_m_MaxPacketSize_4(int32_t value)
	{
		___m_MaxPacketSize_4 = value;
	}

	inline static int32_t get_offset_of_m_IsReliable_5() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_IsReliable_5)); }
	inline bool get_m_IsReliable_5() const { return ___m_IsReliable_5; }
	inline bool* get_address_of_m_IsReliable_5() { return &___m_IsReliable_5; }
	inline void set_m_IsReliable_5(bool value)
	{
		___m_IsReliable_5 = value;
	}

	inline static int32_t get_offset_of_m_AllowFragmentation_6() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_AllowFragmentation_6)); }
	inline bool get_m_AllowFragmentation_6() const { return ___m_AllowFragmentation_6; }
	inline bool* get_address_of_m_AllowFragmentation_6() { return &___m_AllowFragmentation_6; }
	inline void set_m_AllowFragmentation_6(bool value)
	{
		___m_AllowFragmentation_6 = value;
	}

	inline static int32_t get_offset_of_m_IsBroken_7() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_IsBroken_7)); }
	inline bool get_m_IsBroken_7() const { return ___m_IsBroken_7; }
	inline bool* get_address_of_m_IsBroken_7() { return &___m_IsBroken_7; }
	inline void set_m_IsBroken_7(bool value)
	{
		___m_IsBroken_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxPendingPacketCount_8() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_MaxPendingPacketCount_8)); }
	inline int32_t get_m_MaxPendingPacketCount_8() const { return ___m_MaxPendingPacketCount_8; }
	inline int32_t* get_address_of_m_MaxPendingPacketCount_8() { return &___m_MaxPendingPacketCount_8; }
	inline void set_m_MaxPendingPacketCount_8(int32_t value)
	{
		___m_MaxPendingPacketCount_8 = value;
	}

	inline static int32_t get_offset_of_m_PendingPackets_12() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_PendingPackets_12)); }
	inline Queue_1_t1426084212 * get_m_PendingPackets_12() const { return ___m_PendingPackets_12; }
	inline Queue_1_t1426084212 ** get_address_of_m_PendingPackets_12() { return &___m_PendingPackets_12; }
	inline void set_m_PendingPackets_12(Queue_1_t1426084212 * value)
	{
		___m_PendingPackets_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingPackets_12), value);
	}

	inline static int32_t get_offset_of_maxDelay_15() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___maxDelay_15)); }
	inline float get_maxDelay_15() const { return ___maxDelay_15; }
	inline float* get_address_of_maxDelay_15() { return &___maxDelay_15; }
	inline void set_maxDelay_15(float value)
	{
		___maxDelay_15 = value;
	}

	inline static int32_t get_offset_of_m_LastBufferedMessageCountTimer_16() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_LastBufferedMessageCountTimer_16)); }
	inline float get_m_LastBufferedMessageCountTimer_16() const { return ___m_LastBufferedMessageCountTimer_16; }
	inline float* get_address_of_m_LastBufferedMessageCountTimer_16() { return &___m_LastBufferedMessageCountTimer_16; }
	inline void set_m_LastBufferedMessageCountTimer_16(float value)
	{
		___m_LastBufferedMessageCountTimer_16 = value;
	}

	inline static int32_t get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3CnumMsgsOutU3Ek__BackingField_17)); }
	inline int32_t get_U3CnumMsgsOutU3Ek__BackingField_17() const { return ___U3CnumMsgsOutU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CnumMsgsOutU3Ek__BackingField_17() { return &___U3CnumMsgsOutU3Ek__BackingField_17; }
	inline void set_U3CnumMsgsOutU3Ek__BackingField_17(int32_t value)
	{
		___U3CnumMsgsOutU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3CnumBufferedMsgsOutU3Ek__BackingField_18)); }
	inline int32_t get_U3CnumBufferedMsgsOutU3Ek__BackingField_18() const { return ___U3CnumBufferedMsgsOutU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18() { return &___U3CnumBufferedMsgsOutU3Ek__BackingField_18; }
	inline void set_U3CnumBufferedMsgsOutU3Ek__BackingField_18(int32_t value)
	{
		___U3CnumBufferedMsgsOutU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CnumBytesOutU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3CnumBytesOutU3Ek__BackingField_19)); }
	inline int32_t get_U3CnumBytesOutU3Ek__BackingField_19() const { return ___U3CnumBytesOutU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CnumBytesOutU3Ek__BackingField_19() { return &___U3CnumBytesOutU3Ek__BackingField_19; }
	inline void set_U3CnumBytesOutU3Ek__BackingField_19(int32_t value)
	{
		___U3CnumBytesOutU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CnumMsgsInU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3CnumMsgsInU3Ek__BackingField_20)); }
	inline int32_t get_U3CnumMsgsInU3Ek__BackingField_20() const { return ___U3CnumMsgsInU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CnumMsgsInU3Ek__BackingField_20() { return &___U3CnumMsgsInU3Ek__BackingField_20; }
	inline void set_U3CnumMsgsInU3Ek__BackingField_20(int32_t value)
	{
		___U3CnumMsgsInU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CnumBytesInU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3CnumBytesInU3Ek__BackingField_21)); }
	inline int32_t get_U3CnumBytesInU3Ek__BackingField_21() const { return ___U3CnumBytesInU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CnumBytesInU3Ek__BackingField_21() { return &___U3CnumBytesInU3Ek__BackingField_21; }
	inline void set_U3CnumBytesInU3Ek__BackingField_21(int32_t value)
	{
		___U3CnumBytesInU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3CnumBufferedPerSecondU3Ek__BackingField_22)); }
	inline int32_t get_U3CnumBufferedPerSecondU3Ek__BackingField_22() const { return ___U3CnumBufferedPerSecondU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CnumBufferedPerSecondU3Ek__BackingField_22() { return &___U3CnumBufferedPerSecondU3Ek__BackingField_22; }
	inline void set_U3CnumBufferedPerSecondU3Ek__BackingField_22(int32_t value)
	{
		___U3CnumBufferedPerSecondU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___U3ClastBufferedPerSecondU3Ek__BackingField_23)); }
	inline int32_t get_U3ClastBufferedPerSecondU3Ek__BackingField_23() const { return ___U3ClastBufferedPerSecondU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3ClastBufferedPerSecondU3Ek__BackingField_23() { return &___U3ClastBufferedPerSecondU3Ek__BackingField_23; }
	inline void set_U3ClastBufferedPerSecondU3Ek__BackingField_23(int32_t value)
	{
		___U3ClastBufferedPerSecondU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_m_Disposed_27() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___m_Disposed_27)); }
	inline bool get_m_Disposed_27() const { return ___m_Disposed_27; }
	inline bool* get_address_of_m_Disposed_27() { return &___m_Disposed_27; }
	inline void set_m_Disposed_27(bool value)
	{
		___m_Disposed_27 = value;
	}

	inline static int32_t get_offset_of_fragmentBuffer_28() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___fragmentBuffer_28)); }
	inline NetBuffer_t2156033743 * get_fragmentBuffer_28() const { return ___fragmentBuffer_28; }
	inline NetBuffer_t2156033743 ** get_address_of_fragmentBuffer_28() { return &___fragmentBuffer_28; }
	inline void set_fragmentBuffer_28(NetBuffer_t2156033743 * value)
	{
		___fragmentBuffer_28 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentBuffer_28), value);
	}

	inline static int32_t get_offset_of_readingFragment_29() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901, ___readingFragment_29)); }
	inline bool get_readingFragment_29() const { return ___readingFragment_29; }
	inline bool* get_address_of_readingFragment_29() { return &___readingFragment_29; }
	inline void set_readingFragment_29(bool value)
	{
		___readingFragment_29 = value;
	}
};

struct ChannelBuffer_t2335345901_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket> UnityEngine.Networking.ChannelBuffer::s_FreePackets
	List_1_t3051899460 * ___s_FreePackets_13;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::pendingPacketCount
	int32_t ___pendingPacketCount_14;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.ChannelBuffer::s_SendWriter
	NetworkWriter_t3928387057 * ___s_SendWriter_24;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.ChannelBuffer::s_FragmentWriter
	NetworkWriter_t3928387057 * ___s_FragmentWriter_25;

public:
	inline static int32_t get_offset_of_s_FreePackets_13() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901_StaticFields, ___s_FreePackets_13)); }
	inline List_1_t3051899460 * get_s_FreePackets_13() const { return ___s_FreePackets_13; }
	inline List_1_t3051899460 ** get_address_of_s_FreePackets_13() { return &___s_FreePackets_13; }
	inline void set_s_FreePackets_13(List_1_t3051899460 * value)
	{
		___s_FreePackets_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_FreePackets_13), value);
	}

	inline static int32_t get_offset_of_pendingPacketCount_14() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901_StaticFields, ___pendingPacketCount_14)); }
	inline int32_t get_pendingPacketCount_14() const { return ___pendingPacketCount_14; }
	inline int32_t* get_address_of_pendingPacketCount_14() { return &___pendingPacketCount_14; }
	inline void set_pendingPacketCount_14(int32_t value)
	{
		___pendingPacketCount_14 = value;
	}

	inline static int32_t get_offset_of_s_SendWriter_24() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901_StaticFields, ___s_SendWriter_24)); }
	inline NetworkWriter_t3928387057 * get_s_SendWriter_24() const { return ___s_SendWriter_24; }
	inline NetworkWriter_t3928387057 ** get_address_of_s_SendWriter_24() { return &___s_SendWriter_24; }
	inline void set_s_SendWriter_24(NetworkWriter_t3928387057 * value)
	{
		___s_SendWriter_24 = value;
		Il2CppCodeGenWriteBarrier((&___s_SendWriter_24), value);
	}

	inline static int32_t get_offset_of_s_FragmentWriter_25() { return static_cast<int32_t>(offsetof(ChannelBuffer_t2335345901_StaticFields, ___s_FragmentWriter_25)); }
	inline NetworkWriter_t3928387057 * get_s_FragmentWriter_25() const { return ___s_FragmentWriter_25; }
	inline NetworkWriter_t3928387057 ** get_address_of_s_FragmentWriter_25() { return &___s_FragmentWriter_25; }
	inline void set_s_FragmentWriter_25(NetworkWriter_t3928387057 * value)
	{
		___s_FragmentWriter_25 = value;
		Il2CppCodeGenWriteBarrier((&___s_FragmentWriter_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELBUFFER_T2335345901_H
#ifndef PENDINGOWNER_T3340073490_H
#define PENDINGOWNER_T3340073490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientScene/PendingOwner
struct  PendingOwner_t3340073490 
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.ClientScene/PendingOwner::netId
	NetworkInstanceId_t786350175  ___netId_0;
	// System.Int16 UnityEngine.Networking.ClientScene/PendingOwner::playerControllerId
	int16_t ___playerControllerId_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(PendingOwner_t3340073490, ___netId_0)); }
	inline NetworkInstanceId_t786350175  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t786350175 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t786350175  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(PendingOwner_t3340073490, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PENDINGOWNER_T3340073490_H
#ifndef CONNECTSTATE_T1049972864_H
#define CONNECTSTATE_T1049972864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient/ConnectState
struct  ConnectState_t1049972864 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkClient/ConnectState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectState_t1049972864, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTSTATE_T1049972864_H
#ifndef NETWORKERROR_T2038193525_H
#define NETWORKERROR_T2038193525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkError
struct  NetworkError_t2038193525 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkError_t2038193525, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKERROR_T2038193525_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef APPLEINAPPPURCHASERECEIPT_T3844914963_H
#define APPLEINAPPPURCHASERECEIPT_T3844914963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct  AppleInAppPurchaseReceipt_t3844914963  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<quantity>k__BackingField
	int32_t ___U3CquantityU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<productID>k__BackingField
	String_t* ___U3CproductIDU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<originalTransactionIdentifier>k__BackingField
	String_t* ___U3CoriginalTransactionIdentifierU3Ek__BackingField_3;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<purchaseDate>k__BackingField
	DateTime_t3738529785  ___U3CpurchaseDateU3Ek__BackingField_4;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<originalPurchaseDate>k__BackingField
	DateTime_t3738529785  ___U3CoriginalPurchaseDateU3Ek__BackingField_5;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<subscriptionExpirationDate>k__BackingField
	DateTime_t3738529785  ___U3CsubscriptionExpirationDateU3Ek__BackingField_6;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<cancellationDate>k__BackingField
	DateTime_t3738529785  ___U3CcancellationDateU3Ek__BackingField_7;
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<isFreeTrial>k__BackingField
	int32_t ___U3CisFreeTrialU3Ek__BackingField_8;
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<productType>k__BackingField
	int32_t ___U3CproductTypeU3Ek__BackingField_9;
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<isIntroductoryPricePeriod>k__BackingField
	int32_t ___U3CisIntroductoryPricePeriodU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CquantityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CquantityU3Ek__BackingField_0)); }
	inline int32_t get_U3CquantityU3Ek__BackingField_0() const { return ___U3CquantityU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CquantityU3Ek__BackingField_0() { return &___U3CquantityU3Ek__BackingField_0; }
	inline void set_U3CquantityU3Ek__BackingField_0(int32_t value)
	{
		___U3CquantityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CproductIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CproductIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CproductIDU3Ek__BackingField_1() const { return ___U3CproductIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CproductIDU3Ek__BackingField_1() { return &___U3CproductIDU3Ek__BackingField_1; }
	inline void set_U3CproductIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CproductIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIDU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CtransactionIDU3Ek__BackingField_2)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_2() const { return ___U3CtransactionIDU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_2() { return &___U3CtransactionIDU3Ek__BackingField_2; }
	inline void set_U3CtransactionIDU3Ek__BackingField_2(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CoriginalTransactionIdentifierU3Ek__BackingField_3)); }
	inline String_t* get_U3CoriginalTransactionIdentifierU3Ek__BackingField_3() const { return ___U3CoriginalTransactionIdentifierU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3() { return &___U3CoriginalTransactionIdentifierU3Ek__BackingField_3; }
	inline void set_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(String_t* value)
	{
		___U3CoriginalTransactionIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalTransactionIdentifierU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CpurchaseDateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CpurchaseDateU3Ek__BackingField_4)); }
	inline DateTime_t3738529785  get_U3CpurchaseDateU3Ek__BackingField_4() const { return ___U3CpurchaseDateU3Ek__BackingField_4; }
	inline DateTime_t3738529785 * get_address_of_U3CpurchaseDateU3Ek__BackingField_4() { return &___U3CpurchaseDateU3Ek__BackingField_4; }
	inline void set_U3CpurchaseDateU3Ek__BackingField_4(DateTime_t3738529785  value)
	{
		___U3CpurchaseDateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CoriginalPurchaseDateU3Ek__BackingField_5)); }
	inline DateTime_t3738529785  get_U3CoriginalPurchaseDateU3Ek__BackingField_5() const { return ___U3CoriginalPurchaseDateU3Ek__BackingField_5; }
	inline DateTime_t3738529785 * get_address_of_U3CoriginalPurchaseDateU3Ek__BackingField_5() { return &___U3CoriginalPurchaseDateU3Ek__BackingField_5; }
	inline void set_U3CoriginalPurchaseDateU3Ek__BackingField_5(DateTime_t3738529785  value)
	{
		___U3CoriginalPurchaseDateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CsubscriptionExpirationDateU3Ek__BackingField_6)); }
	inline DateTime_t3738529785  get_U3CsubscriptionExpirationDateU3Ek__BackingField_6() const { return ___U3CsubscriptionExpirationDateU3Ek__BackingField_6; }
	inline DateTime_t3738529785 * get_address_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6() { return &___U3CsubscriptionExpirationDateU3Ek__BackingField_6; }
	inline void set_U3CsubscriptionExpirationDateU3Ek__BackingField_6(DateTime_t3738529785  value)
	{
		___U3CsubscriptionExpirationDateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CcancellationDateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CcancellationDateU3Ek__BackingField_7)); }
	inline DateTime_t3738529785  get_U3CcancellationDateU3Ek__BackingField_7() const { return ___U3CcancellationDateU3Ek__BackingField_7; }
	inline DateTime_t3738529785 * get_address_of_U3CcancellationDateU3Ek__BackingField_7() { return &___U3CcancellationDateU3Ek__BackingField_7; }
	inline void set_U3CcancellationDateU3Ek__BackingField_7(DateTime_t3738529785  value)
	{
		___U3CcancellationDateU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CisFreeTrialU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CisFreeTrialU3Ek__BackingField_8)); }
	inline int32_t get_U3CisFreeTrialU3Ek__BackingField_8() const { return ___U3CisFreeTrialU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CisFreeTrialU3Ek__BackingField_8() { return &___U3CisFreeTrialU3Ek__BackingField_8; }
	inline void set_U3CisFreeTrialU3Ek__BackingField_8(int32_t value)
	{
		___U3CisFreeTrialU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CproductTypeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CproductTypeU3Ek__BackingField_9)); }
	inline int32_t get_U3CproductTypeU3Ek__BackingField_9() const { return ___U3CproductTypeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CproductTypeU3Ek__BackingField_9() { return &___U3CproductTypeU3Ek__BackingField_9; }
	inline void set_U3CproductTypeU3Ek__BackingField_9(int32_t value)
	{
		___U3CproductTypeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CisIntroductoryPricePeriodU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t3844914963, ___U3CisIntroductoryPricePeriodU3Ek__BackingField_10)); }
	inline int32_t get_U3CisIntroductoryPricePeriodU3Ek__BackingField_10() const { return ___U3CisIntroductoryPricePeriodU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CisIntroductoryPricePeriodU3Ek__BackingField_10() { return &___U3CisIntroductoryPricePeriodU3Ek__BackingField_10; }
	inline void set_U3CisIntroductoryPricePeriodU3Ek__BackingField_10(int32_t value)
	{
		___U3CisIntroductoryPricePeriodU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLEINAPPPURCHASERECEIPT_T3844914963_H
#ifndef APPLERECEIPT_T1677859958_H
#define APPLERECEIPT_T1677859958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleReceipt
struct  AppleReceipt_t1677859958  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Security.AppleReceipt::<bundleID>k__BackingField
	String_t* ___U3CbundleIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.AppleReceipt::<appVersion>k__BackingField
	String_t* ___U3CappVersionU3Ek__BackingField_1;
	// System.Byte[] UnityEngine.Purchasing.Security.AppleReceipt::<opaque>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CopaqueU3Ek__BackingField_2;
	// System.Byte[] UnityEngine.Purchasing.Security.AppleReceipt::<hash>k__BackingField
	ByteU5BU5D_t4116647657* ___U3ChashU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Security.AppleReceipt::<originalApplicationVersion>k__BackingField
	String_t* ___U3CoriginalApplicationVersionU3Ek__BackingField_4;
	// System.DateTime UnityEngine.Purchasing.Security.AppleReceipt::<receiptCreationDate>k__BackingField
	DateTime_t3738529785  ___U3CreceiptCreationDateU3Ek__BackingField_5;
	// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt[] UnityEngine.Purchasing.Security.AppleReceipt::inAppPurchaseReceipts
	AppleInAppPurchaseReceiptU5BU5D_t180321090* ___inAppPurchaseReceipts_6;

public:
	inline static int32_t get_offset_of_U3CbundleIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___U3CbundleIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CbundleIDU3Ek__BackingField_0() const { return ___U3CbundleIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CbundleIDU3Ek__BackingField_0() { return &___U3CbundleIDU3Ek__BackingField_0; }
	inline void set_U3CbundleIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CbundleIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbundleIDU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CappVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___U3CappVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CappVersionU3Ek__BackingField_1() const { return ___U3CappVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CappVersionU3Ek__BackingField_1() { return &___U3CappVersionU3Ek__BackingField_1; }
	inline void set_U3CappVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CappVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappVersionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CopaqueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___U3CopaqueU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t4116647657* get_U3CopaqueU3Ek__BackingField_2() const { return ___U3CopaqueU3Ek__BackingField_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CopaqueU3Ek__BackingField_2() { return &___U3CopaqueU3Ek__BackingField_2; }
	inline void set_U3CopaqueU3Ek__BackingField_2(ByteU5BU5D_t4116647657* value)
	{
		___U3CopaqueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CopaqueU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3ChashU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___U3ChashU3Ek__BackingField_3)); }
	inline ByteU5BU5D_t4116647657* get_U3ChashU3Ek__BackingField_3() const { return ___U3ChashU3Ek__BackingField_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3ChashU3Ek__BackingField_3() { return &___U3ChashU3Ek__BackingField_3; }
	inline void set_U3ChashU3Ek__BackingField_3(ByteU5BU5D_t4116647657* value)
	{
		___U3ChashU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChashU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___U3CoriginalApplicationVersionU3Ek__BackingField_4)); }
	inline String_t* get_U3CoriginalApplicationVersionU3Ek__BackingField_4() const { return ___U3CoriginalApplicationVersionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CoriginalApplicationVersionU3Ek__BackingField_4() { return &___U3CoriginalApplicationVersionU3Ek__BackingField_4; }
	inline void set_U3CoriginalApplicationVersionU3Ek__BackingField_4(String_t* value)
	{
		___U3CoriginalApplicationVersionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalApplicationVersionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___U3CreceiptCreationDateU3Ek__BackingField_5)); }
	inline DateTime_t3738529785  get_U3CreceiptCreationDateU3Ek__BackingField_5() const { return ___U3CreceiptCreationDateU3Ek__BackingField_5; }
	inline DateTime_t3738529785 * get_address_of_U3CreceiptCreationDateU3Ek__BackingField_5() { return &___U3CreceiptCreationDateU3Ek__BackingField_5; }
	inline void set_U3CreceiptCreationDateU3Ek__BackingField_5(DateTime_t3738529785  value)
	{
		___U3CreceiptCreationDateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_inAppPurchaseReceipts_6() { return static_cast<int32_t>(offsetof(AppleReceipt_t1677859958, ___inAppPurchaseReceipts_6)); }
	inline AppleInAppPurchaseReceiptU5BU5D_t180321090* get_inAppPurchaseReceipts_6() const { return ___inAppPurchaseReceipts_6; }
	inline AppleInAppPurchaseReceiptU5BU5D_t180321090** get_address_of_inAppPurchaseReceipts_6() { return &___inAppPurchaseReceipts_6; }
	inline void set_inAppPurchaseReceipts_6(AppleInAppPurchaseReceiptU5BU5D_t180321090* value)
	{
		___inAppPurchaseReceipts_6 = value;
		Il2CppCodeGenWriteBarrier((&___inAppPurchaseReceipts_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLERECEIPT_T1677859958_H
#ifndef GENERICVALIDATIONEXCEPTION_T812892937_H
#define GENERICVALIDATIONEXCEPTION_T812892937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GenericValidationException
struct  GenericValidationException_t812892937  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVALIDATIONEXCEPTION_T812892937_H
#ifndef GOOGLEPURCHASESTATE_T2722375002_H
#define GOOGLEPURCHASESTATE_T2722375002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GooglePurchaseState
struct  GooglePurchaseState_t2722375002 
{
public:
	// System.Int32 UnityEngine.Purchasing.Security.GooglePurchaseState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GooglePurchaseState_t2722375002, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPURCHASESTATE_T2722375002_H
#ifndef INVALIDBUNDLEIDEXCEPTION_T3226027545_H
#define INVALIDBUNDLEIDEXCEPTION_T3226027545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidBundleIdException
struct  InvalidBundleIdException_t3226027545  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDBUNDLEIDEXCEPTION_T3226027545_H
#ifndef INVALIDPKCS7DATA_T466532846_H
#define INVALIDPKCS7DATA_T466532846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidPKCS7Data
struct  InvalidPKCS7Data_t466532846  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPKCS7DATA_T466532846_H
#ifndef INVALIDPUBLICKEYEXCEPTION_T2327414933_H
#define INVALIDPUBLICKEYEXCEPTION_T2327414933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidPublicKeyException
struct  InvalidPublicKeyException_t2327414933  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPUBLICKEYEXCEPTION_T2327414933_H
#ifndef INVALIDRSADATA_T2177780709_H
#define INVALIDRSADATA_T2177780709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidRSAData
struct  InvalidRSAData_t2177780709  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDRSADATA_T2177780709_H
#ifndef INVALIDRECEIPTDATAEXCEPTION_T3771786961_H
#define INVALIDRECEIPTDATAEXCEPTION_T3771786961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidReceiptDataException
struct  InvalidReceiptDataException_t3771786961  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDRECEIPTDATAEXCEPTION_T3771786961_H
#ifndef INVALIDSIGNATUREEXCEPTION_T1538311828_H
#define INVALIDSIGNATUREEXCEPTION_T1538311828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidSignatureException
struct  InvalidSignatureException_t1538311828  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDSIGNATUREEXCEPTION_T1538311828_H
#ifndef INVALIDTIMEFORMAT_T1714182330_H
#define INVALIDTIMEFORMAT_T1714182330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidTimeFormat
struct  InvalidTimeFormat_t1714182330  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDTIMEFORMAT_T1714182330_H
#ifndef INVALIDX509DATA_T1483908844_H
#define INVALIDX509DATA_T1483908844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidX509Data
struct  InvalidX509Data_t1483908844  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDX509DATA_T1483908844_H
#ifndef MISSINGSTORESECRETEXCEPTION_T989446356_H
#define MISSINGSTORESECRETEXCEPTION_T989446356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.MissingStoreSecretException
struct  MissingStoreSecretException_t989446356  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGSTORESECRETEXCEPTION_T989446356_H
#ifndef STORENOTSUPPORTEDEXCEPTION_T2389606540_H
#define STORENOTSUPPORTEDEXCEPTION_T2389606540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.StoreNotSupportedException
struct  StoreNotSupportedException_t2389606540  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORENOTSUPPORTEDEXCEPTION_T2389606540_H
#ifndef UNITYCHANNELRECEIPT_T4072121997_H
#define UNITYCHANNELRECEIPT_T4072121997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.UnityChannelReceipt
struct  UnityChannelReceipt_t4072121997  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<productID>k__BackingField
	String_t* ___U3CproductIDU3Ek__BackingField_1;
	// System.DateTime UnityEngine.Purchasing.Security.UnityChannelReceipt::<purchaseDate>k__BackingField
	DateTime_t3738529785  ___U3CpurchaseDateU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<packageName>k__BackingField
	String_t* ___U3CpackageNameU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_4;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<clientId>k__BackingField
	String_t* ___U3CclientIdU3Ek__BackingField_5;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<payFee>k__BackingField
	String_t* ___U3CpayFeeU3Ek__BackingField_6;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<orderAttemptId>k__BackingField
	String_t* ___U3CorderAttemptIdU3Ek__BackingField_7;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<country>k__BackingField
	String_t* ___U3CcountryU3Ek__BackingField_8;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<currency>k__BackingField
	String_t* ___U3CcurrencyU3Ek__BackingField_9;
	// System.String UnityEngine.Purchasing.Security.UnityChannelReceipt::<quantity>k__BackingField
	String_t* ___U3CquantityU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CtransactionIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_0() const { return ___U3CtransactionIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_0() { return &___U3CtransactionIDU3Ek__BackingField_0; }
	inline void set_U3CtransactionIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CproductIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CproductIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CproductIDU3Ek__BackingField_1() const { return ___U3CproductIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CproductIDU3Ek__BackingField_1() { return &___U3CproductIDU3Ek__BackingField_1; }
	inline void set_U3CproductIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CproductIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIDU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CpurchaseDateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CpurchaseDateU3Ek__BackingField_2)); }
	inline DateTime_t3738529785  get_U3CpurchaseDateU3Ek__BackingField_2() const { return ___U3CpurchaseDateU3Ek__BackingField_2; }
	inline DateTime_t3738529785 * get_address_of_U3CpurchaseDateU3Ek__BackingField_2() { return &___U3CpurchaseDateU3Ek__BackingField_2; }
	inline void set_U3CpurchaseDateU3Ek__BackingField_2(DateTime_t3738529785  value)
	{
		___U3CpurchaseDateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CpackageNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CpackageNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CpackageNameU3Ek__BackingField_3() const { return ___U3CpackageNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpackageNameU3Ek__BackingField_3() { return &___U3CpackageNameU3Ek__BackingField_3; }
	inline void set_U3CpackageNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CpackageNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpackageNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CstatusU3Ek__BackingField_4)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_4() const { return ___U3CstatusU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_4() { return &___U3CstatusU3Ek__BackingField_4; }
	inline void set_U3CstatusU3Ek__BackingField_4(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CclientIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CclientIdU3Ek__BackingField_5)); }
	inline String_t* get_U3CclientIdU3Ek__BackingField_5() const { return ___U3CclientIdU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CclientIdU3Ek__BackingField_5() { return &___U3CclientIdU3Ek__BackingField_5; }
	inline void set_U3CclientIdU3Ek__BackingField_5(String_t* value)
	{
		___U3CclientIdU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclientIdU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpayFeeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CpayFeeU3Ek__BackingField_6)); }
	inline String_t* get_U3CpayFeeU3Ek__BackingField_6() const { return ___U3CpayFeeU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CpayFeeU3Ek__BackingField_6() { return &___U3CpayFeeU3Ek__BackingField_6; }
	inline void set_U3CpayFeeU3Ek__BackingField_6(String_t* value)
	{
		___U3CpayFeeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpayFeeU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CorderAttemptIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CorderAttemptIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CorderAttemptIdU3Ek__BackingField_7() const { return ___U3CorderAttemptIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CorderAttemptIdU3Ek__BackingField_7() { return &___U3CorderAttemptIdU3Ek__BackingField_7; }
	inline void set_U3CorderAttemptIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CorderAttemptIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CorderAttemptIdU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CcountryU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CcountryU3Ek__BackingField_8)); }
	inline String_t* get_U3CcountryU3Ek__BackingField_8() const { return ___U3CcountryU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CcountryU3Ek__BackingField_8() { return &___U3CcountryU3Ek__BackingField_8; }
	inline void set_U3CcountryU3Ek__BackingField_8(String_t* value)
	{
		___U3CcountryU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcountryU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CcurrencyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CcurrencyU3Ek__BackingField_9)); }
	inline String_t* get_U3CcurrencyU3Ek__BackingField_9() const { return ___U3CcurrencyU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CcurrencyU3Ek__BackingField_9() { return &___U3CcurrencyU3Ek__BackingField_9; }
	inline void set_U3CcurrencyU3Ek__BackingField_9(String_t* value)
	{
		___U3CcurrencyU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrencyU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CquantityU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UnityChannelReceipt_t4072121997, ___U3CquantityU3Ek__BackingField_10)); }
	inline String_t* get_U3CquantityU3Ek__BackingField_10() const { return ___U3CquantityU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CquantityU3Ek__BackingField_10() { return &___U3CquantityU3Ek__BackingField_10; }
	inline void set_U3CquantityU3Ek__BackingField_10(String_t* value)
	{
		___U3CquantityU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquantityU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELRECEIPT_T4072121997_H
#ifndef UNSUPPORTEDSIGNERINFOVERSION_T1875534306_H
#define UNSUPPORTEDSIGNERINFOVERSION_T1875534306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.UnsupportedSignerInfoVersion
struct  UnsupportedSignerInfoVersion_t1875534306  : public IAPSecurityException_t1844591500
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDSIGNERINFOVERSION_T1875534306_H
#ifndef X509CERT_T3147783796_H
#define X509CERT_T3147783796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.X509Cert
struct  X509Cert_t3147783796  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Security.X509Cert::<SerialNumber>k__BackingField
	String_t* ___U3CSerialNumberU3Ek__BackingField_0;
	// System.DateTime UnityEngine.Purchasing.Security.X509Cert::<ValidAfter>k__BackingField
	DateTime_t3738529785  ___U3CValidAfterU3Ek__BackingField_1;
	// System.DateTime UnityEngine.Purchasing.Security.X509Cert::<ValidBefore>k__BackingField
	DateTime_t3738529785  ___U3CValidBeforeU3Ek__BackingField_2;
	// UnityEngine.Purchasing.Security.RSAKey UnityEngine.Purchasing.Security.X509Cert::<PubKey>k__BackingField
	RSAKey_t3751505760 * ___U3CPubKeyU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Purchasing.Security.X509Cert::<SelfSigned>k__BackingField
	bool ___U3CSelfSignedU3Ek__BackingField_4;
	// UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::<Subject>k__BackingField
	DistinguishedName_t1591151536 * ___U3CSubjectU3Ek__BackingField_5;
	// UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::<Issuer>k__BackingField
	DistinguishedName_t1591151536 * ___U3CIssuerU3Ek__BackingField_6;
	// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.X509Cert::TbsCertificate
	Asn1Node_t84807007 * ___TbsCertificate_7;
	// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.X509Cert::<Signature>k__BackingField
	Asn1Node_t84807007 * ___U3CSignatureU3Ek__BackingField_8;
	// System.Byte[] UnityEngine.Purchasing.Security.X509Cert::rawTBSCertificate
	ByteU5BU5D_t4116647657* ___rawTBSCertificate_9;

public:
	inline static int32_t get_offset_of_U3CSerialNumberU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CSerialNumberU3Ek__BackingField_0)); }
	inline String_t* get_U3CSerialNumberU3Ek__BackingField_0() const { return ___U3CSerialNumberU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSerialNumberU3Ek__BackingField_0() { return &___U3CSerialNumberU3Ek__BackingField_0; }
	inline void set_U3CSerialNumberU3Ek__BackingField_0(String_t* value)
	{
		___U3CSerialNumberU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSerialNumberU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValidAfterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CValidAfterU3Ek__BackingField_1)); }
	inline DateTime_t3738529785  get_U3CValidAfterU3Ek__BackingField_1() const { return ___U3CValidAfterU3Ek__BackingField_1; }
	inline DateTime_t3738529785 * get_address_of_U3CValidAfterU3Ek__BackingField_1() { return &___U3CValidAfterU3Ek__BackingField_1; }
	inline void set_U3CValidAfterU3Ek__BackingField_1(DateTime_t3738529785  value)
	{
		___U3CValidAfterU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CValidBeforeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CValidBeforeU3Ek__BackingField_2)); }
	inline DateTime_t3738529785  get_U3CValidBeforeU3Ek__BackingField_2() const { return ___U3CValidBeforeU3Ek__BackingField_2; }
	inline DateTime_t3738529785 * get_address_of_U3CValidBeforeU3Ek__BackingField_2() { return &___U3CValidBeforeU3Ek__BackingField_2; }
	inline void set_U3CValidBeforeU3Ek__BackingField_2(DateTime_t3738529785  value)
	{
		___U3CValidBeforeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPubKeyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CPubKeyU3Ek__BackingField_3)); }
	inline RSAKey_t3751505760 * get_U3CPubKeyU3Ek__BackingField_3() const { return ___U3CPubKeyU3Ek__BackingField_3; }
	inline RSAKey_t3751505760 ** get_address_of_U3CPubKeyU3Ek__BackingField_3() { return &___U3CPubKeyU3Ek__BackingField_3; }
	inline void set_U3CPubKeyU3Ek__BackingField_3(RSAKey_t3751505760 * value)
	{
		___U3CPubKeyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPubKeyU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CSelfSignedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CSelfSignedU3Ek__BackingField_4)); }
	inline bool get_U3CSelfSignedU3Ek__BackingField_4() const { return ___U3CSelfSignedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CSelfSignedU3Ek__BackingField_4() { return &___U3CSelfSignedU3Ek__BackingField_4; }
	inline void set_U3CSelfSignedU3Ek__BackingField_4(bool value)
	{
		___U3CSelfSignedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CSubjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CSubjectU3Ek__BackingField_5)); }
	inline DistinguishedName_t1591151536 * get_U3CSubjectU3Ek__BackingField_5() const { return ___U3CSubjectU3Ek__BackingField_5; }
	inline DistinguishedName_t1591151536 ** get_address_of_U3CSubjectU3Ek__BackingField_5() { return &___U3CSubjectU3Ek__BackingField_5; }
	inline void set_U3CSubjectU3Ek__BackingField_5(DistinguishedName_t1591151536 * value)
	{
		___U3CSubjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CIssuerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CIssuerU3Ek__BackingField_6)); }
	inline DistinguishedName_t1591151536 * get_U3CIssuerU3Ek__BackingField_6() const { return ___U3CIssuerU3Ek__BackingField_6; }
	inline DistinguishedName_t1591151536 ** get_address_of_U3CIssuerU3Ek__BackingField_6() { return &___U3CIssuerU3Ek__BackingField_6; }
	inline void set_U3CIssuerU3Ek__BackingField_6(DistinguishedName_t1591151536 * value)
	{
		___U3CIssuerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIssuerU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_TbsCertificate_7() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___TbsCertificate_7)); }
	inline Asn1Node_t84807007 * get_TbsCertificate_7() const { return ___TbsCertificate_7; }
	inline Asn1Node_t84807007 ** get_address_of_TbsCertificate_7() { return &___TbsCertificate_7; }
	inline void set_TbsCertificate_7(Asn1Node_t84807007 * value)
	{
		___TbsCertificate_7 = value;
		Il2CppCodeGenWriteBarrier((&___TbsCertificate_7), value);
	}

	inline static int32_t get_offset_of_U3CSignatureU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___U3CSignatureU3Ek__BackingField_8)); }
	inline Asn1Node_t84807007 * get_U3CSignatureU3Ek__BackingField_8() const { return ___U3CSignatureU3Ek__BackingField_8; }
	inline Asn1Node_t84807007 ** get_address_of_U3CSignatureU3Ek__BackingField_8() { return &___U3CSignatureU3Ek__BackingField_8; }
	inline void set_U3CSignatureU3Ek__BackingField_8(Asn1Node_t84807007 * value)
	{
		___U3CSignatureU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignatureU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_rawTBSCertificate_9() { return static_cast<int32_t>(offsetof(X509Cert_t3147783796, ___rawTBSCertificate_9)); }
	inline ByteU5BU5D_t4116647657* get_rawTBSCertificate_9() const { return ___rawTBSCertificate_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_rawTBSCertificate_9() { return &___rawTBSCertificate_9; }
	inline void set_rawTBSCertificate_9(ByteU5BU5D_t4116647657* value)
	{
		___rawTBSCertificate_9 = value;
		Il2CppCodeGenWriteBarrier((&___rawTBSCertificate_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERT_T3147783796_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NETWORKCLIENT_T3758195968_H
#define NETWORKCLIENT_T3758195968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient
struct  NetworkClient_t3758195968  : public RuntimeObject
{
public:
	// System.Type UnityEngine.Networking.NetworkClient::m_NetworkConnectionClass
	Type_t * ___m_NetworkConnectionClass_0;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkClient::m_HostTopology
	HostTopology_t4059263395 * ___m_HostTopology_4;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_HostPort
	int32_t ___m_HostPort_5;
	// System.Boolean UnityEngine.Networking.NetworkClient::m_UseSimulator
	bool ___m_UseSimulator_6;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_7;
	// System.Single UnityEngine.Networking.NetworkClient::m_PacketLoss
	float ___m_PacketLoss_8;
	// System.String UnityEngine.Networking.NetworkClient::m_ServerIp
	String_t* ___m_ServerIp_9;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ServerPort
	int32_t ___m_ServerPort_10;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientId
	int32_t ___m_ClientId_11;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientConnectionId
	int32_t ___m_ClientConnectionId_12;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_StatResetTime
	int32_t ___m_StatResetTime_13;
	// System.Net.EndPoint UnityEngine.Networking.NetworkClient::m_RemoteEndPoint
	EndPoint_t982345378 * ___m_RemoteEndPoint_14;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkClient::m_MessageHandlers
	NetworkMessageHandlers_t82575973 * ___m_MessageHandlers_16;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkClient::m_Connection
	NetworkConnection_t2705220091 * ___m_Connection_17;
	// System.Byte[] UnityEngine.Networking.NetworkClient::m_MsgBuffer
	ByteU5BU5D_t4116647657* ___m_MsgBuffer_18;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkClient::m_MsgReader
	NetworkReader_t1574750186 * ___m_MsgReader_19;
	// UnityEngine.Networking.NetworkClient/ConnectState UnityEngine.Networking.NetworkClient::m_AsyncConnect
	int32_t ___m_AsyncConnect_20;
	// System.String UnityEngine.Networking.NetworkClient::m_RequestedServerHost
	String_t* ___m_RequestedServerHost_21;

public:
	inline static int32_t get_offset_of_m_NetworkConnectionClass_0() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_NetworkConnectionClass_0)); }
	inline Type_t * get_m_NetworkConnectionClass_0() const { return ___m_NetworkConnectionClass_0; }
	inline Type_t ** get_address_of_m_NetworkConnectionClass_0() { return &___m_NetworkConnectionClass_0; }
	inline void set_m_NetworkConnectionClass_0(Type_t * value)
	{
		___m_NetworkConnectionClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkConnectionClass_0), value);
	}

	inline static int32_t get_offset_of_m_HostTopology_4() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_HostTopology_4)); }
	inline HostTopology_t4059263395 * get_m_HostTopology_4() const { return ___m_HostTopology_4; }
	inline HostTopology_t4059263395 ** get_address_of_m_HostTopology_4() { return &___m_HostTopology_4; }
	inline void set_m_HostTopology_4(HostTopology_t4059263395 * value)
	{
		___m_HostTopology_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_HostTopology_4), value);
	}

	inline static int32_t get_offset_of_m_HostPort_5() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_HostPort_5)); }
	inline int32_t get_m_HostPort_5() const { return ___m_HostPort_5; }
	inline int32_t* get_address_of_m_HostPort_5() { return &___m_HostPort_5; }
	inline void set_m_HostPort_5(int32_t value)
	{
		___m_HostPort_5 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_6() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_UseSimulator_6)); }
	inline bool get_m_UseSimulator_6() const { return ___m_UseSimulator_6; }
	inline bool* get_address_of_m_UseSimulator_6() { return &___m_UseSimulator_6; }
	inline void set_m_UseSimulator_6(bool value)
	{
		___m_UseSimulator_6 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_7() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_SimulatedLatency_7)); }
	inline int32_t get_m_SimulatedLatency_7() const { return ___m_SimulatedLatency_7; }
	inline int32_t* get_address_of_m_SimulatedLatency_7() { return &___m_SimulatedLatency_7; }
	inline void set_m_SimulatedLatency_7(int32_t value)
	{
		___m_SimulatedLatency_7 = value;
	}

	inline static int32_t get_offset_of_m_PacketLoss_8() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_PacketLoss_8)); }
	inline float get_m_PacketLoss_8() const { return ___m_PacketLoss_8; }
	inline float* get_address_of_m_PacketLoss_8() { return &___m_PacketLoss_8; }
	inline void set_m_PacketLoss_8(float value)
	{
		___m_PacketLoss_8 = value;
	}

	inline static int32_t get_offset_of_m_ServerIp_9() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ServerIp_9)); }
	inline String_t* get_m_ServerIp_9() const { return ___m_ServerIp_9; }
	inline String_t** get_address_of_m_ServerIp_9() { return &___m_ServerIp_9; }
	inline void set_m_ServerIp_9(String_t* value)
	{
		___m_ServerIp_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerIp_9), value);
	}

	inline static int32_t get_offset_of_m_ServerPort_10() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ServerPort_10)); }
	inline int32_t get_m_ServerPort_10() const { return ___m_ServerPort_10; }
	inline int32_t* get_address_of_m_ServerPort_10() { return &___m_ServerPort_10; }
	inline void set_m_ServerPort_10(int32_t value)
	{
		___m_ServerPort_10 = value;
	}

	inline static int32_t get_offset_of_m_ClientId_11() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ClientId_11)); }
	inline int32_t get_m_ClientId_11() const { return ___m_ClientId_11; }
	inline int32_t* get_address_of_m_ClientId_11() { return &___m_ClientId_11; }
	inline void set_m_ClientId_11(int32_t value)
	{
		___m_ClientId_11 = value;
	}

	inline static int32_t get_offset_of_m_ClientConnectionId_12() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ClientConnectionId_12)); }
	inline int32_t get_m_ClientConnectionId_12() const { return ___m_ClientConnectionId_12; }
	inline int32_t* get_address_of_m_ClientConnectionId_12() { return &___m_ClientConnectionId_12; }
	inline void set_m_ClientConnectionId_12(int32_t value)
	{
		___m_ClientConnectionId_12 = value;
	}

	inline static int32_t get_offset_of_m_StatResetTime_13() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_StatResetTime_13)); }
	inline int32_t get_m_StatResetTime_13() const { return ___m_StatResetTime_13; }
	inline int32_t* get_address_of_m_StatResetTime_13() { return &___m_StatResetTime_13; }
	inline void set_m_StatResetTime_13(int32_t value)
	{
		___m_StatResetTime_13 = value;
	}

	inline static int32_t get_offset_of_m_RemoteEndPoint_14() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_RemoteEndPoint_14)); }
	inline EndPoint_t982345378 * get_m_RemoteEndPoint_14() const { return ___m_RemoteEndPoint_14; }
	inline EndPoint_t982345378 ** get_address_of_m_RemoteEndPoint_14() { return &___m_RemoteEndPoint_14; }
	inline void set_m_RemoteEndPoint_14(EndPoint_t982345378 * value)
	{
		___m_RemoteEndPoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemoteEndPoint_14), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_16() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_MessageHandlers_16)); }
	inline NetworkMessageHandlers_t82575973 * get_m_MessageHandlers_16() const { return ___m_MessageHandlers_16; }
	inline NetworkMessageHandlers_t82575973 ** get_address_of_m_MessageHandlers_16() { return &___m_MessageHandlers_16; }
	inline void set_m_MessageHandlers_16(NetworkMessageHandlers_t82575973 * value)
	{
		___m_MessageHandlers_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_16), value);
	}

	inline static int32_t get_offset_of_m_Connection_17() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_Connection_17)); }
	inline NetworkConnection_t2705220091 * get_m_Connection_17() const { return ___m_Connection_17; }
	inline NetworkConnection_t2705220091 ** get_address_of_m_Connection_17() { return &___m_Connection_17; }
	inline void set_m_Connection_17(NetworkConnection_t2705220091 * value)
	{
		___m_Connection_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_17), value);
	}

	inline static int32_t get_offset_of_m_MsgBuffer_18() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_MsgBuffer_18)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgBuffer_18() const { return ___m_MsgBuffer_18; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgBuffer_18() { return &___m_MsgBuffer_18; }
	inline void set_m_MsgBuffer_18(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBuffer_18), value);
	}

	inline static int32_t get_offset_of_m_MsgReader_19() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_MsgReader_19)); }
	inline NetworkReader_t1574750186 * get_m_MsgReader_19() const { return ___m_MsgReader_19; }
	inline NetworkReader_t1574750186 ** get_address_of_m_MsgReader_19() { return &___m_MsgReader_19; }
	inline void set_m_MsgReader_19(NetworkReader_t1574750186 * value)
	{
		___m_MsgReader_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgReader_19), value);
	}

	inline static int32_t get_offset_of_m_AsyncConnect_20() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_AsyncConnect_20)); }
	inline int32_t get_m_AsyncConnect_20() const { return ___m_AsyncConnect_20; }
	inline int32_t* get_address_of_m_AsyncConnect_20() { return &___m_AsyncConnect_20; }
	inline void set_m_AsyncConnect_20(int32_t value)
	{
		___m_AsyncConnect_20 = value;
	}

	inline static int32_t get_offset_of_m_RequestedServerHost_21() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_RequestedServerHost_21)); }
	inline String_t* get_m_RequestedServerHost_21() const { return ___m_RequestedServerHost_21; }
	inline String_t** get_address_of_m_RequestedServerHost_21() { return &___m_RequestedServerHost_21; }
	inline void set_m_RequestedServerHost_21(String_t* value)
	{
		___m_RequestedServerHost_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_RequestedServerHost_21), value);
	}
};

struct NetworkClient_t3758195968_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient> UnityEngine.Networking.NetworkClient::s_Clients
	List_1_t935303414 * ___s_Clients_2;
	// System.Boolean UnityEngine.Networking.NetworkClient::s_IsActive
	bool ___s_IsActive_3;
	// UnityEngine.Networking.NetworkSystem.CRCMessage UnityEngine.Networking.NetworkClient::s_CRCMessage
	CRCMessage_t4148217304 * ___s_CRCMessage_15;
	// System.AsyncCallback UnityEngine.Networking.NetworkClient::<>f__mg$cache0
	AsyncCallback_t3962456242 * ___U3CU3Ef__mgU24cache0_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkClient::<>f__mg$cache1
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache1_23;

public:
	inline static int32_t get_offset_of_s_Clients_2() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___s_Clients_2)); }
	inline List_1_t935303414 * get_s_Clients_2() const { return ___s_Clients_2; }
	inline List_1_t935303414 ** get_address_of_s_Clients_2() { return &___s_Clients_2; }
	inline void set_s_Clients_2(List_1_t935303414 * value)
	{
		___s_Clients_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Clients_2), value);
	}

	inline static int32_t get_offset_of_s_IsActive_3() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___s_IsActive_3)); }
	inline bool get_s_IsActive_3() const { return ___s_IsActive_3; }
	inline bool* get_address_of_s_IsActive_3() { return &___s_IsActive_3; }
	inline void set_s_IsActive_3(bool value)
	{
		___s_IsActive_3 = value;
	}

	inline static int32_t get_offset_of_s_CRCMessage_15() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___s_CRCMessage_15)); }
	inline CRCMessage_t4148217304 * get_s_CRCMessage_15() const { return ___s_CRCMessage_15; }
	inline CRCMessage_t4148217304 ** get_address_of_s_CRCMessage_15() { return &___s_CRCMessage_15; }
	inline void set_s_CRCMessage_15(CRCMessage_t4148217304 * value)
	{
		___s_CRCMessage_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_CRCMessage_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_22() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___U3CU3Ef__mgU24cache0_22)); }
	inline AsyncCallback_t3962456242 * get_U3CU3Ef__mgU24cache0_22() const { return ___U3CU3Ef__mgU24cache0_22; }
	inline AsyncCallback_t3962456242 ** get_address_of_U3CU3Ef__mgU24cache0_22() { return &___U3CU3Ef__mgU24cache0_22; }
	inline void set_U3CU3Ef__mgU24cache0_22(AsyncCallback_t3962456242 * value)
	{
		___U3CU3Ef__mgU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_23() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___U3CU3Ef__mgU24cache1_23)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache1_23() const { return ___U3CU3Ef__mgU24cache1_23; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache1_23() { return &___U3CU3Ef__mgU24cache1_23; }
	inline void set_U3CU3Ef__mgU24cache1_23(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCLIENT_T3758195968_H
#ifndef NETWORKCONNECTION_T2705220091_H
#define NETWORKCONNECTION_T2705220091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkConnection
struct  NetworkConnection_t2705220091  : public RuntimeObject
{
public:
	// UnityEngine.Networking.ChannelBuffer[] UnityEngine.Networking.NetworkConnection::m_Channels
	ChannelBufferU5BU5D_t2631829696* ___m_Channels_0;
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController> UnityEngine.Networking.NetworkConnection::m_PlayerControllers
	List_1_t1968562558 * ___m_PlayerControllers_1;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.NetworkConnection::m_NetMsg
	NetworkMessage_t1192515889 * ___m_NetMsg_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.NetworkConnection::m_VisList
	HashSet_1_t1864468531 * ___m_VisList_3;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkConnection::m_Writer
	NetworkWriter_t3928387057 * ___m_Writer_4;
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate> UnityEngine.Networking.NetworkConnection::m_MessageHandlersDict
	Dictionary_2_t2550447661 * ___m_MessageHandlersDict_5;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkConnection::m_MessageHandlers
	NetworkMessageHandlers_t82575973 * ___m_MessageHandlers_6;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId> UnityEngine.Networking.NetworkConnection::m_ClientOwnedObjects
	HashSet_1_t3646266945 * ___m_ClientOwnedObjects_7;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.NetworkConnection::m_MessageInfo
	NetworkMessage_t1192515889 * ___m_MessageInfo_8;
	// UnityEngine.Networking.NetworkError UnityEngine.Networking.NetworkConnection::error
	int32_t ___error_10;
	// System.Int32 UnityEngine.Networking.NetworkConnection::hostId
	int32_t ___hostId_11;
	// System.Int32 UnityEngine.Networking.NetworkConnection::connectionId
	int32_t ___connectionId_12;
	// System.Boolean UnityEngine.Networking.NetworkConnection::isReady
	bool ___isReady_13;
	// System.String UnityEngine.Networking.NetworkConnection::address
	String_t* ___address_14;
	// System.Single UnityEngine.Networking.NetworkConnection::lastMessageTime
	float ___lastMessageTime_15;
	// System.Boolean UnityEngine.Networking.NetworkConnection::logNetworkMessages
	bool ___logNetworkMessages_16;
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat> UnityEngine.Networking.NetworkConnection::m_PacketStats
	Dictionary_2_t1333685985 * ___m_PacketStats_17;
	// System.Boolean UnityEngine.Networking.NetworkConnection::m_Disposed
	bool ___m_Disposed_18;

public:
	inline static int32_t get_offset_of_m_Channels_0() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_Channels_0)); }
	inline ChannelBufferU5BU5D_t2631829696* get_m_Channels_0() const { return ___m_Channels_0; }
	inline ChannelBufferU5BU5D_t2631829696** get_address_of_m_Channels_0() { return &___m_Channels_0; }
	inline void set_m_Channels_0(ChannelBufferU5BU5D_t2631829696* value)
	{
		___m_Channels_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_0), value);
	}

	inline static int32_t get_offset_of_m_PlayerControllers_1() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_PlayerControllers_1)); }
	inline List_1_t1968562558 * get_m_PlayerControllers_1() const { return ___m_PlayerControllers_1; }
	inline List_1_t1968562558 ** get_address_of_m_PlayerControllers_1() { return &___m_PlayerControllers_1; }
	inline void set_m_PlayerControllers_1(List_1_t1968562558 * value)
	{
		___m_PlayerControllers_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerControllers_1), value);
	}

	inline static int32_t get_offset_of_m_NetMsg_2() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_NetMsg_2)); }
	inline NetworkMessage_t1192515889 * get_m_NetMsg_2() const { return ___m_NetMsg_2; }
	inline NetworkMessage_t1192515889 ** get_address_of_m_NetMsg_2() { return &___m_NetMsg_2; }
	inline void set_m_NetMsg_2(NetworkMessage_t1192515889 * value)
	{
		___m_NetMsg_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetMsg_2), value);
	}

	inline static int32_t get_offset_of_m_VisList_3() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_VisList_3)); }
	inline HashSet_1_t1864468531 * get_m_VisList_3() const { return ___m_VisList_3; }
	inline HashSet_1_t1864468531 ** get_address_of_m_VisList_3() { return &___m_VisList_3; }
	inline void set_m_VisList_3(HashSet_1_t1864468531 * value)
	{
		___m_VisList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisList_3), value);
	}

	inline static int32_t get_offset_of_m_Writer_4() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_Writer_4)); }
	inline NetworkWriter_t3928387057 * get_m_Writer_4() const { return ___m_Writer_4; }
	inline NetworkWriter_t3928387057 ** get_address_of_m_Writer_4() { return &___m_Writer_4; }
	inline void set_m_Writer_4(NetworkWriter_t3928387057 * value)
	{
		___m_Writer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Writer_4), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlersDict_5() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_MessageHandlersDict_5)); }
	inline Dictionary_2_t2550447661 * get_m_MessageHandlersDict_5() const { return ___m_MessageHandlersDict_5; }
	inline Dictionary_2_t2550447661 ** get_address_of_m_MessageHandlersDict_5() { return &___m_MessageHandlersDict_5; }
	inline void set_m_MessageHandlersDict_5(Dictionary_2_t2550447661 * value)
	{
		___m_MessageHandlersDict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlersDict_5), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_6() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_MessageHandlers_6)); }
	inline NetworkMessageHandlers_t82575973 * get_m_MessageHandlers_6() const { return ___m_MessageHandlers_6; }
	inline NetworkMessageHandlers_t82575973 ** get_address_of_m_MessageHandlers_6() { return &___m_MessageHandlers_6; }
	inline void set_m_MessageHandlers_6(NetworkMessageHandlers_t82575973 * value)
	{
		___m_MessageHandlers_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_6), value);
	}

	inline static int32_t get_offset_of_m_ClientOwnedObjects_7() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_ClientOwnedObjects_7)); }
	inline HashSet_1_t3646266945 * get_m_ClientOwnedObjects_7() const { return ___m_ClientOwnedObjects_7; }
	inline HashSet_1_t3646266945 ** get_address_of_m_ClientOwnedObjects_7() { return &___m_ClientOwnedObjects_7; }
	inline void set_m_ClientOwnedObjects_7(HashSet_1_t3646266945 * value)
	{
		___m_ClientOwnedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientOwnedObjects_7), value);
	}

	inline static int32_t get_offset_of_m_MessageInfo_8() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_MessageInfo_8)); }
	inline NetworkMessage_t1192515889 * get_m_MessageInfo_8() const { return ___m_MessageInfo_8; }
	inline NetworkMessage_t1192515889 ** get_address_of_m_MessageInfo_8() { return &___m_MessageInfo_8; }
	inline void set_m_MessageInfo_8(NetworkMessage_t1192515889 * value)
	{
		___m_MessageInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageInfo_8), value);
	}

	inline static int32_t get_offset_of_error_10() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___error_10)); }
	inline int32_t get_error_10() const { return ___error_10; }
	inline int32_t* get_address_of_error_10() { return &___error_10; }
	inline void set_error_10(int32_t value)
	{
		___error_10 = value;
	}

	inline static int32_t get_offset_of_hostId_11() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___hostId_11)); }
	inline int32_t get_hostId_11() const { return ___hostId_11; }
	inline int32_t* get_address_of_hostId_11() { return &___hostId_11; }
	inline void set_hostId_11(int32_t value)
	{
		___hostId_11 = value;
	}

	inline static int32_t get_offset_of_connectionId_12() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___connectionId_12)); }
	inline int32_t get_connectionId_12() const { return ___connectionId_12; }
	inline int32_t* get_address_of_connectionId_12() { return &___connectionId_12; }
	inline void set_connectionId_12(int32_t value)
	{
		___connectionId_12 = value;
	}

	inline static int32_t get_offset_of_isReady_13() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___isReady_13)); }
	inline bool get_isReady_13() const { return ___isReady_13; }
	inline bool* get_address_of_isReady_13() { return &___isReady_13; }
	inline void set_isReady_13(bool value)
	{
		___isReady_13 = value;
	}

	inline static int32_t get_offset_of_address_14() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___address_14)); }
	inline String_t* get_address_14() const { return ___address_14; }
	inline String_t** get_address_of_address_14() { return &___address_14; }
	inline void set_address_14(String_t* value)
	{
		___address_14 = value;
		Il2CppCodeGenWriteBarrier((&___address_14), value);
	}

	inline static int32_t get_offset_of_lastMessageTime_15() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___lastMessageTime_15)); }
	inline float get_lastMessageTime_15() const { return ___lastMessageTime_15; }
	inline float* get_address_of_lastMessageTime_15() { return &___lastMessageTime_15; }
	inline void set_lastMessageTime_15(float value)
	{
		___lastMessageTime_15 = value;
	}

	inline static int32_t get_offset_of_logNetworkMessages_16() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___logNetworkMessages_16)); }
	inline bool get_logNetworkMessages_16() const { return ___logNetworkMessages_16; }
	inline bool* get_address_of_logNetworkMessages_16() { return &___logNetworkMessages_16; }
	inline void set_logNetworkMessages_16(bool value)
	{
		___logNetworkMessages_16 = value;
	}

	inline static int32_t get_offset_of_m_PacketStats_17() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_PacketStats_17)); }
	inline Dictionary_2_t1333685985 * get_m_PacketStats_17() const { return ___m_PacketStats_17; }
	inline Dictionary_2_t1333685985 ** get_address_of_m_PacketStats_17() { return &___m_PacketStats_17; }
	inline void set_m_PacketStats_17(Dictionary_2_t1333685985 * value)
	{
		___m_PacketStats_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_PacketStats_17), value);
	}

	inline static int32_t get_offset_of_m_Disposed_18() { return static_cast<int32_t>(offsetof(NetworkConnection_t2705220091, ___m_Disposed_18)); }
	inline bool get_m_Disposed_18() const { return ___m_Disposed_18; }
	inline bool* get_address_of_m_Disposed_18() { return &___m_Disposed_18; }
	inline void set_m_Disposed_18(bool value)
	{
		___m_Disposed_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONNECTION_T2705220091_H
#ifndef GOOGLEPLAYRECEIPT_T609765622_H
#define GOOGLEPLAYRECEIPT_T609765622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GooglePlayReceipt
struct  GooglePlayReceipt_t609765622  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<productID>k__BackingField
	String_t* ___U3CproductIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<packageName>k__BackingField
	String_t* ___U3CpackageNameU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<purchaseToken>k__BackingField
	String_t* ___U3CpurchaseTokenU3Ek__BackingField_3;
	// System.DateTime UnityEngine.Purchasing.Security.GooglePlayReceipt::<purchaseDate>k__BackingField
	DateTime_t3738529785  ___U3CpurchaseDateU3Ek__BackingField_4;
	// UnityEngine.Purchasing.Security.GooglePurchaseState UnityEngine.Purchasing.Security.GooglePlayReceipt::<purchaseState>k__BackingField
	int32_t ___U3CpurchaseStateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CproductIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t609765622, ___U3CproductIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CproductIDU3Ek__BackingField_0() const { return ___U3CproductIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CproductIDU3Ek__BackingField_0() { return &___U3CproductIDU3Ek__BackingField_0; }
	inline void set_U3CproductIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CproductIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIDU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t609765622, ___U3CtransactionIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_1() const { return ___U3CtransactionIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_1() { return &___U3CtransactionIDU3Ek__BackingField_1; }
	inline void set_U3CtransactionIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CpackageNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t609765622, ___U3CpackageNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CpackageNameU3Ek__BackingField_2() const { return ___U3CpackageNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CpackageNameU3Ek__BackingField_2() { return &___U3CpackageNameU3Ek__BackingField_2; }
	inline void set_U3CpackageNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CpackageNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpackageNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpurchaseTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t609765622, ___U3CpurchaseTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3CpurchaseTokenU3Ek__BackingField_3() const { return ___U3CpurchaseTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpurchaseTokenU3Ek__BackingField_3() { return &___U3CpurchaseTokenU3Ek__BackingField_3; }
	inline void set_U3CpurchaseTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3CpurchaseTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpurchaseTokenU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CpurchaseDateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t609765622, ___U3CpurchaseDateU3Ek__BackingField_4)); }
	inline DateTime_t3738529785  get_U3CpurchaseDateU3Ek__BackingField_4() const { return ___U3CpurchaseDateU3Ek__BackingField_4; }
	inline DateTime_t3738529785 * get_address_of_U3CpurchaseDateU3Ek__BackingField_4() { return &___U3CpurchaseDateU3Ek__BackingField_4; }
	inline void set_U3CpurchaseDateU3Ek__BackingField_4(DateTime_t3738529785  value)
	{
		___U3CpurchaseDateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpurchaseStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t609765622, ___U3CpurchaseStateU3Ek__BackingField_5)); }
	inline int32_t get_U3CpurchaseStateU3Ek__BackingField_5() const { return ___U3CpurchaseStateU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CpurchaseStateU3Ek__BackingField_5() { return &___U3CpurchaseStateU3Ek__BackingField_5; }
	inline void set_U3CpurchaseStateU3Ek__BackingField_5(int32_t value)
	{
		___U3CpurchaseStateU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYRECEIPT_T609765622_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef GLOBALCONFIG_1_T3696447990_H
#define GLOBALCONFIG_1_T3696447990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.GlobalConfig`1<Sirenix.Serialization.GlobalSerializationConfig>
struct  GlobalConfig_1_t3696447990  : public ScriptableObject_t2528358522
{
public:

public:
};

struct GlobalConfig_1_t3696447990_StaticFields
{
public:
	// Sirenix.Utilities.GlobalConfigAttribute Sirenix.Utilities.GlobalConfig`1::configAttribute
	GlobalConfigAttribute_t397999009 * ___configAttribute_4;
	// T Sirenix.Utilities.GlobalConfig`1::instance
	GlobalSerializationConfig_t3411570497 * ___instance_5;

public:
	inline static int32_t get_offset_of_configAttribute_4() { return static_cast<int32_t>(offsetof(GlobalConfig_1_t3696447990_StaticFields, ___configAttribute_4)); }
	inline GlobalConfigAttribute_t397999009 * get_configAttribute_4() const { return ___configAttribute_4; }
	inline GlobalConfigAttribute_t397999009 ** get_address_of_configAttribute_4() { return &___configAttribute_4; }
	inline void set_configAttribute_4(GlobalConfigAttribute_t397999009 * value)
	{
		___configAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___configAttribute_4), value);
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(GlobalConfig_1_t3696447990_StaticFields, ___instance_5)); }
	inline GlobalSerializationConfig_t3411570497 * get_instance_5() const { return ___instance_5; }
	inline GlobalSerializationConfig_t3411570497 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(GlobalSerializationConfig_t3411570497 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALCONFIG_1_T3696447990_H
#ifndef PROJECTPATHFINDER_T922228573_H
#define PROJECTPATHFINDER_T922228573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Utilities.ProjectPathFinder
struct  ProjectPathFinder_t922228573  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTPATHFINDER_T922228573_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef LOCALCLIENT_T1191103892_H
#define LOCALCLIENT_T1191103892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LocalClient
struct  LocalClient_t1191103892  : public NetworkClient_t3758195968
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg> UnityEngine.Networking.LocalClient::m_InternalMsgs
	List_1_t3843830149 * ___m_InternalMsgs_24;
	// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg> UnityEngine.Networking.LocalClient::m_InternalMsgs2
	List_1_t3843830149 * ___m_InternalMsgs2_25;
	// System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg> UnityEngine.Networking.LocalClient::m_FreeMessages
	Stack_1_t3215144862 * ___m_FreeMessages_26;
	// UnityEngine.Networking.NetworkServer UnityEngine.Networking.LocalClient::m_LocalServer
	NetworkServer_t2920297688 * ___m_LocalServer_27;
	// System.Boolean UnityEngine.Networking.LocalClient::m_Connected
	bool ___m_Connected_28;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.LocalClient::s_InternalMessage
	NetworkMessage_t1192515889 * ___s_InternalMessage_29;

public:
	inline static int32_t get_offset_of_m_InternalMsgs_24() { return static_cast<int32_t>(offsetof(LocalClient_t1191103892, ___m_InternalMsgs_24)); }
	inline List_1_t3843830149 * get_m_InternalMsgs_24() const { return ___m_InternalMsgs_24; }
	inline List_1_t3843830149 ** get_address_of_m_InternalMsgs_24() { return &___m_InternalMsgs_24; }
	inline void set_m_InternalMsgs_24(List_1_t3843830149 * value)
	{
		___m_InternalMsgs_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalMsgs_24), value);
	}

	inline static int32_t get_offset_of_m_InternalMsgs2_25() { return static_cast<int32_t>(offsetof(LocalClient_t1191103892, ___m_InternalMsgs2_25)); }
	inline List_1_t3843830149 * get_m_InternalMsgs2_25() const { return ___m_InternalMsgs2_25; }
	inline List_1_t3843830149 ** get_address_of_m_InternalMsgs2_25() { return &___m_InternalMsgs2_25; }
	inline void set_m_InternalMsgs2_25(List_1_t3843830149 * value)
	{
		___m_InternalMsgs2_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalMsgs2_25), value);
	}

	inline static int32_t get_offset_of_m_FreeMessages_26() { return static_cast<int32_t>(offsetof(LocalClient_t1191103892, ___m_FreeMessages_26)); }
	inline Stack_1_t3215144862 * get_m_FreeMessages_26() const { return ___m_FreeMessages_26; }
	inline Stack_1_t3215144862 ** get_address_of_m_FreeMessages_26() { return &___m_FreeMessages_26; }
	inline void set_m_FreeMessages_26(Stack_1_t3215144862 * value)
	{
		___m_FreeMessages_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FreeMessages_26), value);
	}

	inline static int32_t get_offset_of_m_LocalServer_27() { return static_cast<int32_t>(offsetof(LocalClient_t1191103892, ___m_LocalServer_27)); }
	inline NetworkServer_t2920297688 * get_m_LocalServer_27() const { return ___m_LocalServer_27; }
	inline NetworkServer_t2920297688 ** get_address_of_m_LocalServer_27() { return &___m_LocalServer_27; }
	inline void set_m_LocalServer_27(NetworkServer_t2920297688 * value)
	{
		___m_LocalServer_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalServer_27), value);
	}

	inline static int32_t get_offset_of_m_Connected_28() { return static_cast<int32_t>(offsetof(LocalClient_t1191103892, ___m_Connected_28)); }
	inline bool get_m_Connected_28() const { return ___m_Connected_28; }
	inline bool* get_address_of_m_Connected_28() { return &___m_Connected_28; }
	inline void set_m_Connected_28(bool value)
	{
		___m_Connected_28 = value;
	}

	inline static int32_t get_offset_of_s_InternalMessage_29() { return static_cast<int32_t>(offsetof(LocalClient_t1191103892, ___s_InternalMessage_29)); }
	inline NetworkMessage_t1192515889 * get_s_InternalMessage_29() const { return ___s_InternalMessage_29; }
	inline NetworkMessage_t1192515889 ** get_address_of_s_InternalMessage_29() { return &___s_InternalMessage_29; }
	inline void set_s_InternalMessage_29(NetworkMessage_t1192515889 * value)
	{
		___s_InternalMessage_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalMessage_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALCLIENT_T1191103892_H
#ifndef ULOCALCONNECTIONTOCLIENT_T1858816613_H
#define ULOCALCONNECTIONTOCLIENT_T1858816613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ULocalConnectionToClient
struct  ULocalConnectionToClient_t1858816613  : public NetworkConnection_t2705220091
{
public:
	// UnityEngine.Networking.LocalClient UnityEngine.Networking.ULocalConnectionToClient::m_LocalClient
	LocalClient_t1191103892 * ___m_LocalClient_19;

public:
	inline static int32_t get_offset_of_m_LocalClient_19() { return static_cast<int32_t>(offsetof(ULocalConnectionToClient_t1858816613, ___m_LocalClient_19)); }
	inline LocalClient_t1191103892 * get_m_LocalClient_19() const { return ___m_LocalClient_19; }
	inline LocalClient_t1191103892 ** get_address_of_m_LocalClient_19() { return &___m_LocalClient_19; }
	inline void set_m_LocalClient_19(LocalClient_t1191103892 * value)
	{
		___m_LocalClient_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalClient_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULOCALCONNECTIONTOCLIENT_T1858816613_H
#ifndef UNITYNATIVEPURCHASINGCALLBACK_T3388716826_H
#define UNITYNATIVEPURCHASINGCALLBACK_T3388716826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityNativePurchasingCallback
struct  UnityNativePurchasingCallback_t3388716826  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYNATIVEPURCHASINGCALLBACK_T3388716826_H
#ifndef GLOBALSERIALIZATIONCONFIG_T3411570497_H
#define GLOBALSERIALIZATIONCONFIG_T3411570497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GlobalSerializationConfig
struct  GlobalSerializationConfig_t3411570497  : public GlobalConfig_1_t3696447990
{
public:
	// System.Boolean Sirenix.Serialization.GlobalSerializationConfig::HideSerializationCautionaryMessage
	bool ___HideSerializationCautionaryMessage_9;
	// System.Boolean Sirenix.Serialization.GlobalSerializationConfig::HideOdinSerializeAttributeWarningMessages
	bool ___HideOdinSerializeAttributeWarningMessages_10;
	// System.Boolean Sirenix.Serialization.GlobalSerializationConfig::HideNonSerializedShowInInspectorWarningMessages
	bool ___HideNonSerializedShowInInspectorWarningMessages_11;
	// Sirenix.Serialization.DataFormat Sirenix.Serialization.GlobalSerializationConfig::buildSerializationFormat
	int32_t ___buildSerializationFormat_12;
	// Sirenix.Serialization.DataFormat Sirenix.Serialization.GlobalSerializationConfig::editorSerializationFormat
	int32_t ___editorSerializationFormat_13;
	// Sirenix.Serialization.LoggingPolicy Sirenix.Serialization.GlobalSerializationConfig::loggingPolicy
	int32_t ___loggingPolicy_14;
	// Sirenix.Serialization.ErrorHandlingPolicy Sirenix.Serialization.GlobalSerializationConfig::errorHandlingPolicy
	int32_t ___errorHandlingPolicy_15;

public:
	inline static int32_t get_offset_of_HideSerializationCautionaryMessage_9() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___HideSerializationCautionaryMessage_9)); }
	inline bool get_HideSerializationCautionaryMessage_9() const { return ___HideSerializationCautionaryMessage_9; }
	inline bool* get_address_of_HideSerializationCautionaryMessage_9() { return &___HideSerializationCautionaryMessage_9; }
	inline void set_HideSerializationCautionaryMessage_9(bool value)
	{
		___HideSerializationCautionaryMessage_9 = value;
	}

	inline static int32_t get_offset_of_HideOdinSerializeAttributeWarningMessages_10() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___HideOdinSerializeAttributeWarningMessages_10)); }
	inline bool get_HideOdinSerializeAttributeWarningMessages_10() const { return ___HideOdinSerializeAttributeWarningMessages_10; }
	inline bool* get_address_of_HideOdinSerializeAttributeWarningMessages_10() { return &___HideOdinSerializeAttributeWarningMessages_10; }
	inline void set_HideOdinSerializeAttributeWarningMessages_10(bool value)
	{
		___HideOdinSerializeAttributeWarningMessages_10 = value;
	}

	inline static int32_t get_offset_of_HideNonSerializedShowInInspectorWarningMessages_11() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___HideNonSerializedShowInInspectorWarningMessages_11)); }
	inline bool get_HideNonSerializedShowInInspectorWarningMessages_11() const { return ___HideNonSerializedShowInInspectorWarningMessages_11; }
	inline bool* get_address_of_HideNonSerializedShowInInspectorWarningMessages_11() { return &___HideNonSerializedShowInInspectorWarningMessages_11; }
	inline void set_HideNonSerializedShowInInspectorWarningMessages_11(bool value)
	{
		___HideNonSerializedShowInInspectorWarningMessages_11 = value;
	}

	inline static int32_t get_offset_of_buildSerializationFormat_12() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___buildSerializationFormat_12)); }
	inline int32_t get_buildSerializationFormat_12() const { return ___buildSerializationFormat_12; }
	inline int32_t* get_address_of_buildSerializationFormat_12() { return &___buildSerializationFormat_12; }
	inline void set_buildSerializationFormat_12(int32_t value)
	{
		___buildSerializationFormat_12 = value;
	}

	inline static int32_t get_offset_of_editorSerializationFormat_13() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___editorSerializationFormat_13)); }
	inline int32_t get_editorSerializationFormat_13() const { return ___editorSerializationFormat_13; }
	inline int32_t* get_address_of_editorSerializationFormat_13() { return &___editorSerializationFormat_13; }
	inline void set_editorSerializationFormat_13(int32_t value)
	{
		___editorSerializationFormat_13 = value;
	}

	inline static int32_t get_offset_of_loggingPolicy_14() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___loggingPolicy_14)); }
	inline int32_t get_loggingPolicy_14() const { return ___loggingPolicy_14; }
	inline int32_t* get_address_of_loggingPolicy_14() { return &___loggingPolicy_14; }
	inline void set_loggingPolicy_14(int32_t value)
	{
		___loggingPolicy_14 = value;
	}

	inline static int32_t get_offset_of_errorHandlingPolicy_15() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497, ___errorHandlingPolicy_15)); }
	inline int32_t get_errorHandlingPolicy_15() const { return ___errorHandlingPolicy_15; }
	inline int32_t* get_address_of_errorHandlingPolicy_15() { return &___errorHandlingPolicy_15; }
	inline void set_errorHandlingPolicy_15(int32_t value)
	{
		___errorHandlingPolicy_15 = value;
	}
};

struct GlobalSerializationConfig_t3411570497_StaticFields
{
public:
	// Sirenix.Serialization.DataFormat[] Sirenix.Serialization.GlobalSerializationConfig::BuildFormats
	DataFormatU5BU5D_t2757125862* ___BuildFormats_8;

public:
	inline static int32_t get_offset_of_BuildFormats_8() { return static_cast<int32_t>(offsetof(GlobalSerializationConfig_t3411570497_StaticFields, ___BuildFormats_8)); }
	inline DataFormatU5BU5D_t2757125862* get_BuildFormats_8() const { return ___BuildFormats_8; }
	inline DataFormatU5BU5D_t2757125862** get_address_of_BuildFormats_8() { return &___BuildFormats_8; }
	inline void set_BuildFormats_8(DataFormatU5BU5D_t2757125862* value)
	{
		___BuildFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___BuildFormats_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALSERIALIZATIONCONFIG_T3411570497_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MAINTHREADDISPATCHER_T3211665238_H
#define MAINTHREADDISPATCHER_T3211665238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.MainThreadDispatcher
struct  MainThreadDispatcher_t3211665238  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct MainThreadDispatcher_t3211665238_StaticFields
{
public:
	// System.String UnityEngine.Store.MainThreadDispatcher::OBJECT_NAME
	String_t* ___OBJECT_NAME_4;
	// System.Collections.Generic.List`1<System.Action> UnityEngine.Store.MainThreadDispatcher::s_Callbacks
	List_1_t2736452219 * ___s_Callbacks_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.Store.MainThreadDispatcher::s_CallbacksPending
	bool ___s_CallbacksPending_6;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3211665238_StaticFields, ___OBJECT_NAME_4)); }
	inline String_t* get_OBJECT_NAME_4() const { return ___OBJECT_NAME_4; }
	inline String_t** get_address_of_OBJECT_NAME_4() { return &___OBJECT_NAME_4; }
	inline void set_OBJECT_NAME_4(String_t* value)
	{
		___OBJECT_NAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___OBJECT_NAME_4), value);
	}

	inline static int32_t get_offset_of_s_Callbacks_5() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3211665238_StaticFields, ___s_Callbacks_5)); }
	inline List_1_t2736452219 * get_s_Callbacks_5() const { return ___s_Callbacks_5; }
	inline List_1_t2736452219 ** get_address_of_s_Callbacks_5() { return &___s_Callbacks_5; }
	inline void set_s_Callbacks_5(List_1_t2736452219 * value)
	{
		___s_Callbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Callbacks_5), value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_6() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3211665238_StaticFields, ___s_CallbacksPending_6)); }
	inline bool get_s_CallbacksPending_6() const { return ___s_CallbacksPending_6; }
	inline bool* get_address_of_s_CallbacksPending_6() { return &___s_CallbacksPending_6; }
	inline void set_s_CallbacksPending_6(bool value)
	{
		___s_CallbacksPending_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADDISPATCHER_T3211665238_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (ProjectPathFinder_t922228573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (SirenixAssetPaths_t1482981763), -1, sizeof(SirenixAssetPaths_t1482981763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3301[9] = 
{
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_DefaultSirenixPluginPath_0(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_OdinPath_1(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_SirenixAssetsPath_2(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_SirenixPluginPath_3(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_SirenixAssembliesPath_4(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_OdinResourcesPath_5(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_OdinEditorConfigsPath_6(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_OdinResourcesConfigsPath_7(),
	SirenixAssetPaths_t1482981763_StaticFields::get_offset_of_OdinTempPath_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (U3CU3Ec__DisplayClass10_0_t1198674588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[2] = 
{
	U3CU3Ec__DisplayClass10_0_t1198674588::get_offset_of_invalids_0(),
	U3CU3Ec__DisplayClass10_0_t1198674588::get_offset_of_replace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (SirenixBuildNameAttribute_t1047153296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[1] = 
{
	SirenixBuildNameAttribute_t1047153296::get_offset_of_U3CBuildNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (SirenixEditorConfigAttribute_t128736660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (SirenixGlobalConfigAttribute_t4086608552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (StringUtilities_t1488277914), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (UnityVersion_t2571170002), -1, sizeof(UnityVersion_t2571170002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3307[2] = 
{
	UnityVersion_t2571170002_StaticFields::get_offset_of_Major_0(),
	UnityVersion_t2571170002_StaticFields::get_offset_of_Minor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (UnsafeUtilities_t4279943386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3310[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U383843267F79E9F8816E76A442D8E5C1457F5E016_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (__StaticArrayInitTypeSizeU3D18_t1904425263)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D18_t1904425263 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (U3CModuleU3E_t692745556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (MainThreadDispatcher_t3211665238), -1, sizeof(MainThreadDispatcher_t3211665238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3313[3] = 
{
	MainThreadDispatcher_t3211665238_StaticFields::get_offset_of_OBJECT_NAME_4(),
	MainThreadDispatcher_t3211665238_StaticFields::get_offset_of_s_Callbacks_5(),
	MainThreadDispatcher_t3211665238_StaticFields::get_offset_of_s_CallbacksPending_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (AppInfo_t2433711276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[5] = 
{
	AppInfo_t2433711276::get_offset_of_U3CappIdU3Ek__BackingField_0(),
	AppInfo_t2433711276::get_offset_of_U3CappKeyU3Ek__BackingField_1(),
	AppInfo_t2433711276::get_offset_of_U3CclientIdU3Ek__BackingField_2(),
	AppInfo_t2433711276::get_offset_of_U3CclientKeyU3Ek__BackingField_3(),
	AppInfo_t2433711276::get_offset_of_U3CdebugU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (StoreService_t295887430), -1, sizeof(StoreService_t295887430_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3315[1] = 
{
	StoreService_t295887430_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (LoginForwardCallback_t717703418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[1] = 
{
	LoginForwardCallback_t717703418::get_offset_of_loginListener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (UserInfo_t2886425993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[3] = 
{
	UserInfo_t2886425993::get_offset_of_U3CchannelU3Ek__BackingField_0(),
	UserInfo_t2886425993::get_offset_of_U3CuserIdU3Ek__BackingField_1(),
	UserInfo_t2886425993::get_offset_of_U3CuserLoginTokenU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (U3CModuleU3E_t692745557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (iOSStoreBindings_t3204725121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (OSXStoreBindings_t1795895004), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (U3CModuleU3E_t692745558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (PurchaseService_t2241894207), -1, sizeof(PurchaseService_t2241894207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3324[1] = 
{
	PurchaseService_t2241894207_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (PurchaseForwardCallback_t3627744015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3326[1] = 
{
	PurchaseForwardCallback_t3627744015::get_offset_of_purchaseListener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (U3CModuleU3E_t692745559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (FacebookStoreBindings_t197114713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (U3CModuleU3E_t692745560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (DistinguishedName_t1591151536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[7] = 
{
	DistinguishedName_t1591151536::get_offset_of_U3CCountryU3Ek__BackingField_0(),
	DistinguishedName_t1591151536::get_offset_of_U3COrganizationU3Ek__BackingField_1(),
	DistinguishedName_t1591151536::get_offset_of_U3COrganizationalUnitU3Ek__BackingField_2(),
	DistinguishedName_t1591151536::get_offset_of_U3CDnqU3Ek__BackingField_3(),
	DistinguishedName_t1591151536::get_offset_of_U3CStateU3Ek__BackingField_4(),
	DistinguishedName_t1591151536::get_offset_of_U3CCommonNameU3Ek__BackingField_5(),
	DistinguishedName_t1591151536::get_offset_of_U3CSerialNumberU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (X509Cert_t3147783796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3332[10] = 
{
	X509Cert_t3147783796::get_offset_of_U3CSerialNumberU3Ek__BackingField_0(),
	X509Cert_t3147783796::get_offset_of_U3CValidAfterU3Ek__BackingField_1(),
	X509Cert_t3147783796::get_offset_of_U3CValidBeforeU3Ek__BackingField_2(),
	X509Cert_t3147783796::get_offset_of_U3CPubKeyU3Ek__BackingField_3(),
	X509Cert_t3147783796::get_offset_of_U3CSelfSignedU3Ek__BackingField_4(),
	X509Cert_t3147783796::get_offset_of_U3CSubjectU3Ek__BackingField_5(),
	X509Cert_t3147783796::get_offset_of_U3CIssuerU3Ek__BackingField_6(),
	X509Cert_t3147783796::get_offset_of_TbsCertificate_7(),
	X509Cert_t3147783796::get_offset_of_U3CSignatureU3Ek__BackingField_8(),
	X509Cert_t3147783796::get_offset_of_rawTBSCertificate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (InvalidX509Data_t1483908844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (PKCS7_t350312378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[5] = 
{
	PKCS7_t350312378::get_offset_of_root_0(),
	PKCS7_t350312378::get_offset_of_U3CdataU3Ek__BackingField_1(),
	PKCS7_t350312378::get_offset_of_U3CsinfosU3Ek__BackingField_2(),
	PKCS7_t350312378::get_offset_of_U3CcertChainU3Ek__BackingField_3(),
	PKCS7_t350312378::get_offset_of_validStructure_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (SignerInfo_t979539174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[3] = 
{
	SignerInfo_t979539174::get_offset_of_U3CVersionU3Ek__BackingField_0(),
	SignerInfo_t979539174::get_offset_of_U3CIssuerSerialNumberU3Ek__BackingField_1(),
	SignerInfo_t979539174::get_offset_of_U3CEncryptedDigestU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (IAPSecurityException_t1844591500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (InvalidSignatureException_t1538311828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (InvalidPKCS7Data_t466532846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (InvalidTimeFormat_t1714182330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (UnsupportedSignerInfoVersion_t1875534306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (RSAKey_t3751505760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[1] = 
{
	RSAKey_t3751505760::get_offset_of_U3CrsaU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (InvalidRSAData_t2177780709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (GooglePlayValidator_t216820094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[1] = 
{
	GooglePlayValidator_t216820094::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (AppleValidator_t513798189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[2] = 
{
	AppleValidator_t513798189::get_offset_of_cert_0(),
	AppleValidator_t513798189::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (AppleReceiptParser_t1990668756), -1, sizeof(AppleReceiptParser_t1990668756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3345[1] = 
{
	AppleReceiptParser_t1990668756_StaticFields::get_offset_of__mostRecentReceiptData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (AppleReceipt_t1677859958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[7] = 
{
	AppleReceipt_t1677859958::get_offset_of_U3CbundleIDU3Ek__BackingField_0(),
	AppleReceipt_t1677859958::get_offset_of_U3CappVersionU3Ek__BackingField_1(),
	AppleReceipt_t1677859958::get_offset_of_U3CopaqueU3Ek__BackingField_2(),
	AppleReceipt_t1677859958::get_offset_of_U3ChashU3Ek__BackingField_3(),
	AppleReceipt_t1677859958::get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4(),
	AppleReceipt_t1677859958::get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5(),
	AppleReceipt_t1677859958::get_offset_of_inAppPurchaseReceipts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (AppleInAppPurchaseReceipt_t3844914963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[11] = 
{
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CquantityU3Ek__BackingField_0(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CproductIDU3Ek__BackingField_1(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CtransactionIDU3Ek__BackingField_2(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CcancellationDateU3Ek__BackingField_7(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CisFreeTrialU3Ek__BackingField_8(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CproductTypeU3Ek__BackingField_9(),
	AppleInAppPurchaseReceipt_t3844914963::get_offset_of_U3CisIntroductoryPricePeriodU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (Obfuscator_t3490180744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (U3CU3Ec__DisplayClass1_0_t4106353832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[1] = 
{
	U3CU3Ec__DisplayClass1_0_t4106353832::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (StoreNotSupportedException_t2389606540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (InvalidBundleIdException_t3226027545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (InvalidReceiptDataException_t3771786961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (MissingStoreSecretException_t989446356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (InvalidPublicKeyException_t2327414933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (GenericValidationException_t812892937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (CrossPlatformValidator_t4140375513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[5] = 
{
	CrossPlatformValidator_t4140375513::get_offset_of_google_0(),
	CrossPlatformValidator_t4140375513::get_offset_of_unityChannel_1(),
	CrossPlatformValidator_t4140375513::get_offset_of_apple_2(),
	CrossPlatformValidator_t4140375513::get_offset_of_googleBundleId_3(),
	CrossPlatformValidator_t4140375513::get_offset_of_appleBundleId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (GooglePurchaseState_t2722375002)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3358[4] = 
{
	GooglePurchaseState_t2722375002::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (GooglePlayReceipt_t609765622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[6] = 
{
	GooglePlayReceipt_t609765622::get_offset_of_U3CproductIDU3Ek__BackingField_0(),
	GooglePlayReceipt_t609765622::get_offset_of_U3CtransactionIDU3Ek__BackingField_1(),
	GooglePlayReceipt_t609765622::get_offset_of_U3CpackageNameU3Ek__BackingField_2(),
	GooglePlayReceipt_t609765622::get_offset_of_U3CpurchaseTokenU3Ek__BackingField_3(),
	GooglePlayReceipt_t609765622::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	GooglePlayReceipt_t609765622::get_offset_of_U3CpurchaseStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (UnityChannelReceipt_t4072121997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3360[11] = 
{
	UnityChannelReceipt_t4072121997::get_offset_of_U3CtransactionIDU3Ek__BackingField_0(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CproductIDU3Ek__BackingField_1(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CpurchaseDateU3Ek__BackingField_2(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CpackageNameU3Ek__BackingField_3(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CstatusU3Ek__BackingField_4(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CclientIdU3Ek__BackingField_5(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CpayFeeU3Ek__BackingField_6(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CorderAttemptIdU3Ek__BackingField_7(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CcountryU3Ek__BackingField_8(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CcurrencyU3Ek__BackingField_9(),
	UnityChannelReceipt_t4072121997::get_offset_of_U3CquantityU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (UnityChannelValidator_t2457973196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[2] = 
{
	UnityChannelValidator_t2457973196::get_offset_of_key_0(),
	UnityChannelValidator_t2457973196::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (UnityChannelReceiptParser_t1594844111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (Asn1Node_t84807007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[13] = 
{
	Asn1Node_t84807007::get_offset_of_tag_0(),
	Asn1Node_t84807007::get_offset_of_dataOffset_1(),
	Asn1Node_t84807007::get_offset_of_dataLength_2(),
	Asn1Node_t84807007::get_offset_of_lengthFieldBytes_3(),
	Asn1Node_t84807007::get_offset_of_data_4(),
	Asn1Node_t84807007::get_offset_of_childNodeList_5(),
	Asn1Node_t84807007::get_offset_of_unusedBits_6(),
	Asn1Node_t84807007::get_offset_of_deepness_7(),
	Asn1Node_t84807007::get_offset_of_path_8(),
	Asn1Node_t84807007::get_offset_of_parentNode_9(),
	Asn1Node_t84807007::get_offset_of_requireRecalculatePar_10(),
	Asn1Node_t84807007::get_offset_of_isIndefiniteLength_11(),
	Asn1Node_t84807007::get_offset_of_parseEncapsulatedData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (Asn1Parser_t1261554413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[2] = 
{
	Asn1Parser_t1261554413::get_offset_of_rawData_0(),
	Asn1Parser_t1261554413::get_offset_of_rootNode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (Asn1Util_t417944685), -1, sizeof(Asn1Util_t417944685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3366[1] = 
{
	Asn1Util_t417944685_StaticFields::get_offset_of_hexDigits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (Oid_t864847193), -1, sizeof(Oid_t864847193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3367[1] = 
{
	Oid_t864847193_StaticFields::get_offset_of_oidDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (RelativeOid_t4118021937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3369[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2711125392)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2711125392 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (U3CModuleU3E_t692745561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (CustomLogger_t3390755501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[3] = 
{
	CustomLogger_t3390755501::get_offset_of_logWarningDelegate_0(),
	CustomLogger_t3390755501::get_offset_of_logErrorDelegate_1(),
	CustomLogger_t3390755501::get_offset_of_logExceptionDelegate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (DataFormat_t4143835455)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3373[4] = 
{
	DataFormat_t4143835455::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (DefaultLoggers_t3565000847), -1, sizeof(DefaultLoggers_t3565000847_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3374[2] = 
{
	DefaultLoggers_t3565000847_StaticFields::get_offset_of_LOCK_0(),
	DefaultLoggers_t3565000847_StaticFields::get_offset_of_unityLogger_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (ErrorHandlingPolicy_t3882907225)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3375[4] = 
{
	ErrorHandlingPolicy_t3882907225::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (GlobalSerializationConfig_t3411570497), -1, sizeof(GlobalSerializationConfig_t3411570497_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3376[10] = 
{
	0,
	0,
	GlobalSerializationConfig_t3411570497_StaticFields::get_offset_of_BuildFormats_8(),
	GlobalSerializationConfig_t3411570497::get_offset_of_HideSerializationCautionaryMessage_9(),
	GlobalSerializationConfig_t3411570497::get_offset_of_HideOdinSerializeAttributeWarningMessages_10(),
	GlobalSerializationConfig_t3411570497::get_offset_of_HideNonSerializedShowInInspectorWarningMessages_11(),
	GlobalSerializationConfig_t3411570497::get_offset_of_buildSerializationFormat_12(),
	GlobalSerializationConfig_t3411570497::get_offset_of_editorSerializationFormat_13(),
	GlobalSerializationConfig_t3411570497::get_offset_of_loggingPolicy_14(),
	GlobalSerializationConfig_t3411570497::get_offset_of_errorHandlingPolicy_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (LoggingPolicy_t1861080991)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3378[4] = 
{
	LoggingPolicy_t1861080991::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (U3CModuleU3E_t692745562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (U3CModuleU3E_t692745563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (U3CModuleU3E_t692745564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (U3CModuleU3E_t692745565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (U3CModuleU3E_t692745566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (U3CModuleU3E_t692745567), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (U3CModuleU3E_t692745568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (UnityNativePurchasingCallback_t3388716826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (TizenStoreBindings_t4059756115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (U3CModuleU3E_t692745569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (ChannelBuffer_t2335345901), -1, sizeof(ChannelBuffer_t2335345901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3390[30] = 
{
	ChannelBuffer_t2335345901::get_offset_of_m_Connection_0(),
	ChannelBuffer_t2335345901::get_offset_of_m_CurrentPacket_1(),
	ChannelBuffer_t2335345901::get_offset_of_m_LastFlushTime_2(),
	ChannelBuffer_t2335345901::get_offset_of_m_ChannelId_3(),
	ChannelBuffer_t2335345901::get_offset_of_m_MaxPacketSize_4(),
	ChannelBuffer_t2335345901::get_offset_of_m_IsReliable_5(),
	ChannelBuffer_t2335345901::get_offset_of_m_AllowFragmentation_6(),
	ChannelBuffer_t2335345901::get_offset_of_m_IsBroken_7(),
	ChannelBuffer_t2335345901::get_offset_of_m_MaxPendingPacketCount_8(),
	0,
	0,
	0,
	ChannelBuffer_t2335345901::get_offset_of_m_PendingPackets_12(),
	ChannelBuffer_t2335345901_StaticFields::get_offset_of_s_FreePackets_13(),
	ChannelBuffer_t2335345901_StaticFields::get_offset_of_pendingPacketCount_14(),
	ChannelBuffer_t2335345901::get_offset_of_maxDelay_15(),
	ChannelBuffer_t2335345901::get_offset_of_m_LastBufferedMessageCountTimer_16(),
	ChannelBuffer_t2335345901::get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17(),
	ChannelBuffer_t2335345901::get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18(),
	ChannelBuffer_t2335345901::get_offset_of_U3CnumBytesOutU3Ek__BackingField_19(),
	ChannelBuffer_t2335345901::get_offset_of_U3CnumMsgsInU3Ek__BackingField_20(),
	ChannelBuffer_t2335345901::get_offset_of_U3CnumBytesInU3Ek__BackingField_21(),
	ChannelBuffer_t2335345901::get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22(),
	ChannelBuffer_t2335345901::get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23(),
	ChannelBuffer_t2335345901_StaticFields::get_offset_of_s_SendWriter_24(),
	ChannelBuffer_t2335345901_StaticFields::get_offset_of_s_FragmentWriter_25(),
	0,
	ChannelBuffer_t2335345901::get_offset_of_m_Disposed_27(),
	ChannelBuffer_t2335345901::get_offset_of_fragmentBuffer_28(),
	ChannelBuffer_t2335345901::get_offset_of_readingFragment_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (ChannelPacket_t1579824718)+ sizeof (RuntimeObject), sizeof(ChannelPacket_t1579824718_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3391[3] = 
{
	ChannelPacket_t1579824718::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelPacket_t1579824718::get_offset_of_m_Buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelPacket_t1579824718::get_offset_of_m_IsReliable_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (ClientScene_t3640716971), -1, sizeof(ClientScene_t3640716971_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3392[34] = 
{
	ClientScene_t3640716971_StaticFields::get_offset_of_s_LocalPlayers_0(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ReadyConnection_1(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_SpawnableObjects_2(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_IsReady_3(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_IsSpawnFinished_4(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_NetworkScene_5(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ObjectSpawnSceneMessage_6(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ObjectSpawnFinishedMessage_7(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ObjectDestroyMessage_8(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ObjectSpawnMessage_9(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_OwnerMessage_10(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ClientAuthorityMessage_11(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_ReconnectId_12(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_Peers_13(),
	ClientScene_t3640716971_StaticFields::get_offset_of_s_PendingOwnerIds_14(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_16(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_17(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_18(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_19(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_20(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_21(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_22(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_23(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_24(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_25(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_26(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_27(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_28(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_29(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_30(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_31(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_32(),
	ClientScene_t3640716971_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (PendingOwner_t3340073490)+ sizeof (RuntimeObject), sizeof(PendingOwner_t3340073490 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3393[2] = 
{
	PendingOwner_t3340073490::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PendingOwner_t3340073490::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (ClientAttribute_t145541712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (ClientCallbackAttribute_t1632794324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (DotNetCompatibility_t842745520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (LocalClient_t1191103892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[6] = 
{
	LocalClient_t1191103892::get_offset_of_m_InternalMsgs_24(),
	LocalClient_t1191103892::get_offset_of_m_InternalMsgs2_25(),
	LocalClient_t1191103892::get_offset_of_m_FreeMessages_26(),
	LocalClient_t1191103892::get_offset_of_m_LocalServer_27(),
	LocalClient_t1191103892::get_offset_of_m_Connected_28(),
	LocalClient_t1191103892::get_offset_of_s_InternalMessage_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (InternalMsg_t2371755407)+ sizeof (RuntimeObject), sizeof(InternalMsg_t2371755407_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3398[2] = 
{
	InternalMsg_t2371755407::get_offset_of_buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalMsg_t2371755407::get_offset_of_channelId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (ULocalConnectionToClient_t1858816613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[1] = 
{
	ULocalConnectionToClient_t1858816613::get_offset_of_m_LocalClient_19(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
