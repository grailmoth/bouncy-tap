﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Sirenix.Serialization.DeserializationContext
struct DeserializationContext_t2871073323;
// Sirenix.Serialization.JsonTextReader
struct JsonTextReader_t2625489622;
// Sirenix.Serialization.SerializationContext
struct SerializationContext_t2406416806;
// Sirenix.Serialization.Serializer`1<System.Object>
struct Serializer_1_t462120690;
// Sirenix.Serialization.TwoWaySerializationBinder
struct TwoWaySerializationBinder_t4250048501;
// System.Action`2<System.Collections.ArrayList,System.Runtime.Serialization.StreamingContext>[]
struct Action_2U5BU5D_t3672272034;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t1371231800;
// System.Collections.Generic.Dictionary`2<System.Char,System.Nullable`1<Sirenix.Serialization.EntryType>>
struct Dictionary_2_t2243406842;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Type>
struct Dictionary_2_t1372658091;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker>
struct Dictionary_2_t100189446;
// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate>
struct Dictionary_2_t3632739877;
// System.Collections.Generic.Dictionary`2<System.Type,System.Int32>
struct Dictionary_2_t1100325521;
// System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode>
struct List_1_t1861728361;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t2103082695;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.Queue`1<System.Char>
struct Queue_1_t3480719964;
// System.Collections.Generic.Stack`1<Sirenix.Serialization.NodeInfo>
struct Stack_1_t2097916431;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_t2217434578;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.StreamReader
struct StreamReader_t4009935899;
// System.IO.StreamWriter
struct StreamWriter_t1266378904;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t3299519057;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;

struct Object_t631007953_marshaled_com;



#ifndef U3CMODULEU3E_T692745573_H
#define U3CMODULEU3E_T692745573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745573 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745573_H
#ifndef U3CMODULEU3E_T692745572_H
#define U3CMODULEU3E_T692745572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745572 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745572_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ARRAYFORMATTERLOCATOR_T2706966186_H
#define ARRAYFORMATTERLOCATOR_T2706966186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ArrayFormatterLocator
struct  ArrayFormatterLocator_t2706966186  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYFORMATTERLOCATOR_T2706966186_H
#ifndef BASEDATAREADERWRITER_T3725085398_H
#define BASEDATAREADERWRITER_T3725085398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDataReaderWriter
struct  BaseDataReaderWriter_t3725085398  : public RuntimeObject
{
public:
	// Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.BaseDataReaderWriter::binder
	TwoWaySerializationBinder_t4250048501 * ___binder_0;
	// System.Collections.Generic.Stack`1<Sirenix.Serialization.NodeInfo> Sirenix.Serialization.BaseDataReaderWriter::nodes
	Stack_1_t2097916431 * ___nodes_1;

public:
	inline static int32_t get_offset_of_binder_0() { return static_cast<int32_t>(offsetof(BaseDataReaderWriter_t3725085398, ___binder_0)); }
	inline TwoWaySerializationBinder_t4250048501 * get_binder_0() const { return ___binder_0; }
	inline TwoWaySerializationBinder_t4250048501 ** get_address_of_binder_0() { return &___binder_0; }
	inline void set_binder_0(TwoWaySerializationBinder_t4250048501 * value)
	{
		___binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___binder_0), value);
	}

	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(BaseDataReaderWriter_t3725085398, ___nodes_1)); }
	inline Stack_1_t2097916431 * get_nodes_1() const { return ___nodes_1; }
	inline Stack_1_t2097916431 ** get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(Stack_1_t2097916431 * value)
	{
		___nodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDATAREADERWRITER_T3725085398_H
#ifndef BASEFORMATTER_1_T3032283482_H
#define BASEFORMATTER_1_T3032283482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseFormatter`1<System.Collections.ArrayList>
struct  BaseFormatter_1_t3032283482  : public RuntimeObject
{
public:

public:
};

struct BaseFormatter_1_t3032283482_StaticFields
{
public:
	// System.Action`2<T,System.Runtime.Serialization.StreamingContext>[] Sirenix.Serialization.BaseFormatter`1::OnSerializingCallbacks
	Action_2U5BU5D_t3672272034* ___OnSerializingCallbacks_0;
	// System.Action`2<T,System.Runtime.Serialization.StreamingContext>[] Sirenix.Serialization.BaseFormatter`1::OnSerializedCallbacks
	Action_2U5BU5D_t3672272034* ___OnSerializedCallbacks_1;
	// System.Action`2<T,System.Runtime.Serialization.StreamingContext>[] Sirenix.Serialization.BaseFormatter`1::OnDeserializingCallbacks
	Action_2U5BU5D_t3672272034* ___OnDeserializingCallbacks_2;
	// System.Action`2<T,System.Runtime.Serialization.StreamingContext>[] Sirenix.Serialization.BaseFormatter`1::OnDeserializedCallbacks
	Action_2U5BU5D_t3672272034* ___OnDeserializedCallbacks_3;
	// System.Boolean Sirenix.Serialization.BaseFormatter`1::IsValueType
	bool ___IsValueType_4;
	// System.Boolean Sirenix.Serialization.BaseFormatter`1::ImplementsISerializationCallbackReceiver
	bool ___ImplementsISerializationCallbackReceiver_5;
	// System.Boolean Sirenix.Serialization.BaseFormatter`1::ImplementsIDeserializationCallback
	bool ___ImplementsIDeserializationCallback_6;
	// System.Boolean Sirenix.Serialization.BaseFormatter`1::ImplementsIObjectReference
	bool ___ImplementsIObjectReference_7;

public:
	inline static int32_t get_offset_of_OnSerializingCallbacks_0() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___OnSerializingCallbacks_0)); }
	inline Action_2U5BU5D_t3672272034* get_OnSerializingCallbacks_0() const { return ___OnSerializingCallbacks_0; }
	inline Action_2U5BU5D_t3672272034** get_address_of_OnSerializingCallbacks_0() { return &___OnSerializingCallbacks_0; }
	inline void set_OnSerializingCallbacks_0(Action_2U5BU5D_t3672272034* value)
	{
		___OnSerializingCallbacks_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnSerializingCallbacks_0), value);
	}

	inline static int32_t get_offset_of_OnSerializedCallbacks_1() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___OnSerializedCallbacks_1)); }
	inline Action_2U5BU5D_t3672272034* get_OnSerializedCallbacks_1() const { return ___OnSerializedCallbacks_1; }
	inline Action_2U5BU5D_t3672272034** get_address_of_OnSerializedCallbacks_1() { return &___OnSerializedCallbacks_1; }
	inline void set_OnSerializedCallbacks_1(Action_2U5BU5D_t3672272034* value)
	{
		___OnSerializedCallbacks_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnSerializedCallbacks_1), value);
	}

	inline static int32_t get_offset_of_OnDeserializingCallbacks_2() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___OnDeserializingCallbacks_2)); }
	inline Action_2U5BU5D_t3672272034* get_OnDeserializingCallbacks_2() const { return ___OnDeserializingCallbacks_2; }
	inline Action_2U5BU5D_t3672272034** get_address_of_OnDeserializingCallbacks_2() { return &___OnDeserializingCallbacks_2; }
	inline void set_OnDeserializingCallbacks_2(Action_2U5BU5D_t3672272034* value)
	{
		___OnDeserializingCallbacks_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeserializingCallbacks_2), value);
	}

	inline static int32_t get_offset_of_OnDeserializedCallbacks_3() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___OnDeserializedCallbacks_3)); }
	inline Action_2U5BU5D_t3672272034* get_OnDeserializedCallbacks_3() const { return ___OnDeserializedCallbacks_3; }
	inline Action_2U5BU5D_t3672272034** get_address_of_OnDeserializedCallbacks_3() { return &___OnDeserializedCallbacks_3; }
	inline void set_OnDeserializedCallbacks_3(Action_2U5BU5D_t3672272034* value)
	{
		___OnDeserializedCallbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeserializedCallbacks_3), value);
	}

	inline static int32_t get_offset_of_IsValueType_4() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___IsValueType_4)); }
	inline bool get_IsValueType_4() const { return ___IsValueType_4; }
	inline bool* get_address_of_IsValueType_4() { return &___IsValueType_4; }
	inline void set_IsValueType_4(bool value)
	{
		___IsValueType_4 = value;
	}

	inline static int32_t get_offset_of_ImplementsISerializationCallbackReceiver_5() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___ImplementsISerializationCallbackReceiver_5)); }
	inline bool get_ImplementsISerializationCallbackReceiver_5() const { return ___ImplementsISerializationCallbackReceiver_5; }
	inline bool* get_address_of_ImplementsISerializationCallbackReceiver_5() { return &___ImplementsISerializationCallbackReceiver_5; }
	inline void set_ImplementsISerializationCallbackReceiver_5(bool value)
	{
		___ImplementsISerializationCallbackReceiver_5 = value;
	}

	inline static int32_t get_offset_of_ImplementsIDeserializationCallback_6() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___ImplementsIDeserializationCallback_6)); }
	inline bool get_ImplementsIDeserializationCallback_6() const { return ___ImplementsIDeserializationCallback_6; }
	inline bool* get_address_of_ImplementsIDeserializationCallback_6() { return &___ImplementsIDeserializationCallback_6; }
	inline void set_ImplementsIDeserializationCallback_6(bool value)
	{
		___ImplementsIDeserializationCallback_6 = value;
	}

	inline static int32_t get_offset_of_ImplementsIObjectReference_7() { return static_cast<int32_t>(offsetof(BaseFormatter_1_t3032283482_StaticFields, ___ImplementsIObjectReference_7)); }
	inline bool get_ImplementsIObjectReference_7() const { return ___ImplementsIObjectReference_7; }
	inline bool* get_address_of_ImplementsIObjectReference_7() { return &___ImplementsIObjectReference_7; }
	inline void set_ImplementsIObjectReference_7(bool value)
	{
		___ImplementsIObjectReference_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFORMATTER_1_T3032283482_H
#ifndef U3CU3EC_T630232976_H
#define U3CU3EC_T630232976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BinaryDataReader/<>c
struct  U3CU3Ec_t630232976  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t630232976_StaticFields
{
public:
	// Sirenix.Serialization.BinaryDataReader/<>c Sirenix.Serialization.BinaryDataReader/<>c::<>9
	U3CU3Ec_t630232976 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t630232976_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t630232976 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t630232976 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t630232976 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T630232976_H
#ifndef U3CU3EC_T4251451849_H
#define U3CU3EC_T4251451849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BinaryDataWriter/<>c
struct  U3CU3Ec_t4251451849  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4251451849_StaticFields
{
public:
	// Sirenix.Serialization.BinaryDataWriter/<>c Sirenix.Serialization.BinaryDataWriter/<>c::<>9
	U3CU3Ec_t4251451849 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4251451849_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4251451849 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4251451849 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4251451849 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4251451849_H
#ifndef CUSTOMSERIALIZATIONPOLICY_T180435788_H
#define CUSTOMSERIALIZATIONPOLICY_T180435788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CustomSerializationPolicy
struct  CustomSerializationPolicy_t180435788  : public RuntimeObject
{
public:
	// System.String Sirenix.Serialization.CustomSerializationPolicy::id
	String_t* ___id_0;
	// System.Boolean Sirenix.Serialization.CustomSerializationPolicy::allowNonSerializableTypes
	bool ___allowNonSerializableTypes_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.CustomSerializationPolicy::shouldSerializeFunc
	Func_2_t2217434578 * ___shouldSerializeFunc_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CustomSerializationPolicy_t180435788, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_allowNonSerializableTypes_1() { return static_cast<int32_t>(offsetof(CustomSerializationPolicy_t180435788, ___allowNonSerializableTypes_1)); }
	inline bool get_allowNonSerializableTypes_1() const { return ___allowNonSerializableTypes_1; }
	inline bool* get_address_of_allowNonSerializableTypes_1() { return &___allowNonSerializableTypes_1; }
	inline void set_allowNonSerializableTypes_1(bool value)
	{
		___allowNonSerializableTypes_1 = value;
	}

	inline static int32_t get_offset_of_shouldSerializeFunc_2() { return static_cast<int32_t>(offsetof(CustomSerializationPolicy_t180435788, ___shouldSerializeFunc_2)); }
	inline Func_2_t2217434578 * get_shouldSerializeFunc_2() const { return ___shouldSerializeFunc_2; }
	inline Func_2_t2217434578 ** get_address_of_shouldSerializeFunc_2() { return &___shouldSerializeFunc_2; }
	inline void set_shouldSerializeFunc_2(Func_2_t2217434578 * value)
	{
		___shouldSerializeFunc_2 = value;
		Il2CppCodeGenWriteBarrier((&___shouldSerializeFunc_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMSERIALIZATIONPOLICY_T180435788_H
#ifndef DELEGATEFORMATTERLOCATOR_T1064339770_H
#define DELEGATEFORMATTERLOCATOR_T1064339770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DelegateFormatterLocator
struct  DelegateFormatterLocator_t1064339770  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEFORMATTERLOCATOR_T1064339770_H
#ifndef FORMATTEREMITTER_T2521268614_H
#define FORMATTEREMITTER_T2521268614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterEmitter
struct  FormatterEmitter_t2521268614  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTEREMITTER_T2521268614_H
#ifndef GENERICCOLLECTIONFORMATTER_T29413212_H
#define GENERICCOLLECTIONFORMATTER_T29413212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GenericCollectionFormatter
struct  GenericCollectionFormatter_t29413212  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOLLECTIONFORMATTER_T29413212_H
#ifndef GENERICCOLLECTIONFORMATTERLOCATOR_T1557485371_H
#define GENERICCOLLECTIONFORMATTERLOCATOR_T1557485371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GenericCollectionFormatterLocator
struct  GenericCollectionFormatterLocator_t1557485371  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOLLECTIONFORMATTERLOCATOR_T1557485371_H
#ifndef ISERIALIZABLEFORMATTERLOCATOR_T480903605_H
#define ISERIALIZABLEFORMATTERLOCATOR_T480903605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ISerializableFormatterLocator
struct  ISerializableFormatterLocator_t480903605  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISERIALIZABLEFORMATTERLOCATOR_T480903605_H
#ifndef JSONCONFIG_T1669950243_H
#define JSONCONFIG_T1669950243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.JsonConfig
struct  JsonConfig_t1669950243  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONFIG_T1669950243_H
#ifndef MINIMALBASEFORMATTER_1_T748546867_H
#define MINIMALBASEFORMATTER_1_T748546867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<System.DateTime>
struct  MinimalBaseFormatter_1_t748546867  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t748546867_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t748546867_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T748546867_H
#ifndef MINIMALBASEFORMATTER_1_T239304589_H
#define MINIMALBASEFORMATTER_1_T239304589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<System.DateTimeOffset>
struct  MinimalBaseFormatter_1_t239304589  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t239304589_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t239304589_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T239304589_H
#ifndef MINIMALBASEFORMATTER_1_T2186143627_H
#define MINIMALBASEFORMATTER_1_T2186143627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<System.TimeSpan>
struct  MinimalBaseFormatter_1_t2186143627  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t2186143627_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t2186143627_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T2186143627_H
#ifndef MINIMALBASEFORMATTER_1_T3788929138_H
#define MINIMALBASEFORMATTER_1_T3788929138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<System.Type>
struct  MinimalBaseFormatter_1_t3788929138  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3788929138_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3788929138_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3788929138_H
#ifndef SELFFORMATTERLOCATOR_T1132301583_H
#define SELFFORMATTERLOCATOR_T1132301583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SelfFormatterLocator
struct  SelfFormatterLocator_t1132301583  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFFORMATTERLOCATOR_T1132301583_H
#ifndef SERIALIZATIONNODEDATAREADERWRITERCONFIG_T3461079505_H
#define SERIALIZATIONNODEDATAREADERWRITERCONFIG_T3461079505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationNodeDataReaderWriterConfig
struct  SerializationNodeDataReaderWriterConfig_t3461079505  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONNODEDATAREADERWRITERCONFIG_T3461079505_H
#ifndef TYPEFORMATTERLOCATOR_T3026799695_H
#define TYPEFORMATTERLOCATOR_T3026799695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.TypeFormatterLocator
struct  TypeFormatterLocator_t3026799695  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFORMATTERLOCATOR_T3026799695_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FACTORY_T3975828591_H
#define FACTORY_T3975828591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Default.Factory
struct  Factory_t3975828591  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORY_T3975828591_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef ALWAYSFORMATSSELFATTRIBUTE_T2404549866_H
#define ALWAYSFORMATSSELFATTRIBUTE_T2404549866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.AlwaysFormatsSelfAttribute
struct  AlwaysFormatsSelfAttribute_t2404549866  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALWAYSFORMATSSELFATTRIBUTE_T2404549866_H
#ifndef ARRAYLISTFORMATTER_T2213559614_H
#define ARRAYLISTFORMATTER_T2213559614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ArrayListFormatter
struct  ArrayListFormatter_t2213559614  : public BaseFormatter_1_t3032283482
{
public:

public:
};

struct ArrayListFormatter_t2213559614_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Object> Sirenix.Serialization.ArrayListFormatter::ObjectSerializer
	Serializer_1_t462120690 * ___ObjectSerializer_8;

public:
	inline static int32_t get_offset_of_ObjectSerializer_8() { return static_cast<int32_t>(offsetof(ArrayListFormatter_t2213559614_StaticFields, ___ObjectSerializer_8)); }
	inline Serializer_1_t462120690 * get_ObjectSerializer_8() const { return ___ObjectSerializer_8; }
	inline Serializer_1_t462120690 ** get_address_of_ObjectSerializer_8() { return &___ObjectSerializer_8; }
	inline void set_ObjectSerializer_8(Serializer_1_t462120690 * value)
	{
		___ObjectSerializer_8 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectSerializer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLISTFORMATTER_T2213559614_H
#ifndef BASEDATAREADER_T3963898873_H
#define BASEDATAREADER_T3963898873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDataReader
struct  BaseDataReader_t3963898873  : public BaseDataReaderWriter_t3725085398
{
public:
	// Sirenix.Serialization.DeserializationContext Sirenix.Serialization.BaseDataReader::context
	DeserializationContext_t2871073323 * ___context_2;
	// System.IO.Stream Sirenix.Serialization.BaseDataReader::stream
	Stream_t1273022909 * ___stream_3;

public:
	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(BaseDataReader_t3963898873, ___context_2)); }
	inline DeserializationContext_t2871073323 * get_context_2() const { return ___context_2; }
	inline DeserializationContext_t2871073323 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(DeserializationContext_t2871073323 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(BaseDataReader_t3963898873, ___stream_3)); }
	inline Stream_t1273022909 * get_stream_3() const { return ___stream_3; }
	inline Stream_t1273022909 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_t1273022909 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDATAREADER_T3963898873_H
#ifndef BASEDATAWRITER_T1131909494_H
#define BASEDATAWRITER_T1131909494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDataWriter
struct  BaseDataWriter_t1131909494  : public BaseDataReaderWriter_t3725085398
{
public:
	// Sirenix.Serialization.SerializationContext Sirenix.Serialization.BaseDataWriter::context
	SerializationContext_t2406416806 * ___context_2;
	// System.IO.Stream Sirenix.Serialization.BaseDataWriter::stream
	Stream_t1273022909 * ___stream_3;

public:
	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(BaseDataWriter_t1131909494, ___context_2)); }
	inline SerializationContext_t2406416806 * get_context_2() const { return ___context_2; }
	inline SerializationContext_t2406416806 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(SerializationContext_t2406416806 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(BaseDataWriter_t1131909494, ___stream_3)); }
	inline Stream_t1273022909 * get_stream_3() const { return ___stream_3; }
	inline Stream_t1273022909 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_t1273022909 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDATAWRITER_T1131909494_H
#ifndef CUSTOMFORMATTERATTRIBUTE_T1016321002_H
#define CUSTOMFORMATTERATTRIBUTE_T1016321002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CustomFormatterAttribute
struct  CustomFormatterAttribute_t1016321002  : public Attribute_t861562559
{
public:
	// System.Int32 Sirenix.Serialization.CustomFormatterAttribute::Priority
	int32_t ___Priority_0;

public:
	inline static int32_t get_offset_of_Priority_0() { return static_cast<int32_t>(offsetof(CustomFormatterAttribute_t1016321002, ___Priority_0)); }
	inline int32_t get_Priority_0() const { return ___Priority_0; }
	inline int32_t* get_address_of_Priority_0() { return &___Priority_0; }
	inline void set_Priority_0(int32_t value)
	{
		___Priority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMFORMATTERATTRIBUTE_T1016321002_H
#ifndef DATETIMEFORMATTER_T1343700026_H
#define DATETIMEFORMATTER_T1343700026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DateTimeFormatter
struct  DateTimeFormatter_t1343700026  : public MinimalBaseFormatter_1_t748546867
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEFORMATTER_T1343700026_H
#ifndef DATETIMEOFFSETFORMATTER_T344272922_H
#define DATETIMEOFFSETFORMATTER_T344272922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DateTimeOffsetFormatter
struct  DateTimeOffsetFormatter_t344272922  : public MinimalBaseFormatter_1_t239304589
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSETFORMATTER_T344272922_H
#ifndef EMITTEDFORMATTERATTRIBUTE_T3475119202_H
#define EMITTEDFORMATTERATTRIBUTE_T3475119202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.EmittedFormatterAttribute
struct  EmittedFormatterAttribute_t3475119202  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMITTEDFORMATTERATTRIBUTE_T3475119202_H
#ifndef REGISTERFORMATTERATTRIBUTE_T697622418_H
#define REGISTERFORMATTERATTRIBUTE_T697622418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.RegisterFormatterAttribute
struct  RegisterFormatterAttribute_t697622418  : public Attribute_t861562559
{
public:
	// System.Type Sirenix.Serialization.RegisterFormatterAttribute::<FormatterType>k__BackingField
	Type_t * ___U3CFormatterTypeU3Ek__BackingField_0;
	// System.Int32 Sirenix.Serialization.RegisterFormatterAttribute::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFormatterTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterFormatterAttribute_t697622418, ___U3CFormatterTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFormatterTypeU3Ek__BackingField_0() const { return ___U3CFormatterTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFormatterTypeU3Ek__BackingField_0() { return &___U3CFormatterTypeU3Ek__BackingField_0; }
	inline void set_U3CFormatterTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFormatterTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatterTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterFormatterAttribute_t697622418, ___U3CPriorityU3Ek__BackingField_1)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERFORMATTERATTRIBUTE_T697622418_H
#ifndef REGISTERFORMATTERLOCATORATTRIBUTE_T2636356893_H
#define REGISTERFORMATTERLOCATORATTRIBUTE_T2636356893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.RegisterFormatterLocatorAttribute
struct  RegisterFormatterLocatorAttribute_t2636356893  : public Attribute_t861562559
{
public:
	// System.Type Sirenix.Serialization.RegisterFormatterLocatorAttribute::<FormatterLocatorType>k__BackingField
	Type_t * ___U3CFormatterLocatorTypeU3Ek__BackingField_0;
	// System.Int32 Sirenix.Serialization.RegisterFormatterLocatorAttribute::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFormatterLocatorTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterFormatterLocatorAttribute_t2636356893, ___U3CFormatterLocatorTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFormatterLocatorTypeU3Ek__BackingField_0() const { return ___U3CFormatterLocatorTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFormatterLocatorTypeU3Ek__BackingField_0() { return &___U3CFormatterLocatorTypeU3Ek__BackingField_0; }
	inline void set_U3CFormatterLocatorTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFormatterLocatorTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatterLocatorTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterFormatterLocatorAttribute_t2636356893, ___U3CPriorityU3Ek__BackingField_1)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERFORMATTERLOCATORATTRIBUTE_T2636356893_H
#ifndef TIMESPANFORMATTER_T3901306678_H
#define TIMESPANFORMATTER_T3901306678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.TimeSpanFormatter
struct  TimeSpanFormatter_t3901306678  : public MinimalBaseFormatter_1_t2186143627
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPANFORMATTER_T3901306678_H
#ifndef TYPEFORMATTER_T3590974233_H
#define TYPEFORMATTER_T3590974233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.TypeFormatter
struct  TypeFormatter_t3590974233  : public MinimalBaseFormatter_1_t3788929138
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFORMATTER_T3590974233_H
#ifndef DECIMAL_T2948259380_H
#define DECIMAL_T2948259380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2948259380 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t2948259380_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t2770800703* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t2948259380  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t2948259380  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2948259380  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2948259380  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t2948259380  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t2948259380  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t2948259380  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t2770800703* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t2770800703** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t2770800703* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___Zero_7)); }
	inline Decimal_t2948259380  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t2948259380 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t2948259380  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___One_8)); }
	inline Decimal_t2948259380  get_One_8() const { return ___One_8; }
	inline Decimal_t2948259380 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t2948259380  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinusOne_9)); }
	inline Decimal_t2948259380  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t2948259380 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t2948259380  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValue_10)); }
	inline Decimal_t2948259380  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t2948259380 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t2948259380  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinValue_11)); }
	inline Decimal_t2948259380  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t2948259380 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t2948259380  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t2948259380  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t2948259380 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t2948259380  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t2948259380  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t2948259380 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t2948259380  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2948259380_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T1062055256_H
#define NULLABLE_1_T1062055256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Char>
struct  Nullable_1_t1062055256 
{
public:
	// T System.Nullable`1::value
	Il2CppChar ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1062055256, ___value_0)); }
	inline Il2CppChar get_value_0() const { return ___value_0; }
	inline Il2CppChar* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Il2CppChar value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1062055256, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1062055256_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255369  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifndef BINARYDATAWRITER_T769175716_H
#define BINARYDATAWRITER_T769175716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BinaryDataWriter
struct  BinaryDataWriter_t769175716  : public BaseDataWriter_t1131909494
{
public:
	// System.Byte[] Sirenix.Serialization.BinaryDataWriter::buffer
	ByteU5BU5D_t4116647657* ___buffer_6;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Int32> Sirenix.Serialization.BinaryDataWriter::types
	Dictionary_2_t1100325521 * ___types_7;

public:
	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(BinaryDataWriter_t769175716, ___buffer_6)); }
	inline ByteU5BU5D_t4116647657* get_buffer_6() const { return ___buffer_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(ByteU5BU5D_t4116647657* value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_types_7() { return static_cast<int32_t>(offsetof(BinaryDataWriter_t769175716, ___types_7)); }
	inline Dictionary_2_t1100325521 * get_types_7() const { return ___types_7; }
	inline Dictionary_2_t1100325521 ** get_address_of_types_7() { return &___types_7; }
	inline void set_types_7(Dictionary_2_t1100325521 * value)
	{
		___types_7 = value;
		Il2CppCodeGenWriteBarrier((&___types_7), value);
	}
};

struct BinaryDataWriter_t769175716_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate> Sirenix.Serialization.BinaryDataWriter::PrimitiveGetBytesMethods
	Dictionary_2_t3632739877 * ___PrimitiveGetBytesMethods_4;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Int32> Sirenix.Serialization.BinaryDataWriter::PrimitiveSizes
	Dictionary_2_t1100325521 * ___PrimitiveSizes_5;

public:
	inline static int32_t get_offset_of_PrimitiveGetBytesMethods_4() { return static_cast<int32_t>(offsetof(BinaryDataWriter_t769175716_StaticFields, ___PrimitiveGetBytesMethods_4)); }
	inline Dictionary_2_t3632739877 * get_PrimitiveGetBytesMethods_4() const { return ___PrimitiveGetBytesMethods_4; }
	inline Dictionary_2_t3632739877 ** get_address_of_PrimitiveGetBytesMethods_4() { return &___PrimitiveGetBytesMethods_4; }
	inline void set_PrimitiveGetBytesMethods_4(Dictionary_2_t3632739877 * value)
	{
		___PrimitiveGetBytesMethods_4 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveGetBytesMethods_4), value);
	}

	inline static int32_t get_offset_of_PrimitiveSizes_5() { return static_cast<int32_t>(offsetof(BinaryDataWriter_t769175716_StaticFields, ___PrimitiveSizes_5)); }
	inline Dictionary_2_t1100325521 * get_PrimitiveSizes_5() const { return ___PrimitiveSizes_5; }
	inline Dictionary_2_t1100325521 ** get_address_of_PrimitiveSizes_5() { return &___PrimitiveSizes_5; }
	inline void set_PrimitiveSizes_5(Dictionary_2_t1100325521 * value)
	{
		___PrimitiveSizes_5 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveSizes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYDATAWRITER_T769175716_H
#ifndef BINARYENTRYTYPE_T3247007447_H
#define BINARYENTRYTYPE_T3247007447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BinaryEntryType
struct  BinaryEntryType_t3247007447 
{
public:
	// System.Byte Sirenix.Serialization.BinaryEntryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinaryEntryType_t3247007447, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYENTRYTYPE_T3247007447_H
#ifndef CUSTOMGENERICFORMATTERATTRIBUTE_T4231702514_H
#define CUSTOMGENERICFORMATTERATTRIBUTE_T4231702514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CustomGenericFormatterAttribute
struct  CustomGenericFormatterAttribute_t4231702514  : public CustomFormatterAttribute_t1016321002
{
public:
	// System.Type Sirenix.Serialization.CustomGenericFormatterAttribute::SerializedGenericTypeDefinition
	Type_t * ___SerializedGenericTypeDefinition_1;

public:
	inline static int32_t get_offset_of_SerializedGenericTypeDefinition_1() { return static_cast<int32_t>(offsetof(CustomGenericFormatterAttribute_t4231702514, ___SerializedGenericTypeDefinition_1)); }
	inline Type_t * get_SerializedGenericTypeDefinition_1() const { return ___SerializedGenericTypeDefinition_1; }
	inline Type_t ** get_address_of_SerializedGenericTypeDefinition_1() { return &___SerializedGenericTypeDefinition_1; }
	inline void set_SerializedGenericTypeDefinition_1(Type_t * value)
	{
		___SerializedGenericTypeDefinition_1 = value;
		Il2CppCodeGenWriteBarrier((&___SerializedGenericTypeDefinition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMGENERICFORMATTERATTRIBUTE_T4231702514_H
#ifndef DATAFORMAT_T4143835455_H
#define DATAFORMAT_T4143835455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DataFormat
struct  DataFormat_t4143835455 
{
public:
	// System.Int32 Sirenix.Serialization.DataFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataFormat_t4143835455, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAFORMAT_T4143835455_H
#ifndef ENTRYTYPE_T2784073430_H
#define ENTRYTYPE_T2784073430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.EntryType
struct  EntryType_t2784073430 
{
public:
	// System.Byte Sirenix.Serialization.EntryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntryType_t2784073430, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRYTYPE_T2784073430_H
#ifndef FORMATTERLOCATIONSTEP_T2859269825_H
#define FORMATTERLOCATIONSTEP_T2859269825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterLocationStep
struct  FormatterLocationStep_t2859269825 
{
public:
	// System.Int32 Sirenix.Serialization.FormatterLocationStep::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterLocationStep_t2859269825, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERLOCATIONSTEP_T2859269825_H
#ifndef JSONDATAWRITER_T2586654915_H
#define JSONDATAWRITER_T2586654915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.JsonDataWriter
struct  JsonDataWriter_t2586654915  : public BaseDataWriter_t1131909494
{
public:
	// System.Boolean Sirenix.Serialization.JsonDataWriter::justStarted
	bool ___justStarted_4;
	// System.Boolean Sirenix.Serialization.JsonDataWriter::forceNoSeparatorNextLine
	bool ___forceNoSeparatorNextLine_5;
	// System.Text.StringBuilder Sirenix.Serialization.JsonDataWriter::escapeStringBuilder
	StringBuilder_t * ___escapeStringBuilder_6;
	// System.IO.StreamWriter Sirenix.Serialization.JsonDataWriter::writer
	StreamWriter_t1266378904 * ___writer_7;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate> Sirenix.Serialization.JsonDataWriter::primitiveTypeWriters
	Dictionary_2_t3632739877 * ___primitiveTypeWriters_8;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Int32> Sirenix.Serialization.JsonDataWriter::seenTypes
	Dictionary_2_t1100325521 * ___seenTypes_9;
	// System.Boolean Sirenix.Serialization.JsonDataWriter::<FormatAsReadable>k__BackingField
	bool ___U3CFormatAsReadableU3Ek__BackingField_10;
	// System.Boolean Sirenix.Serialization.JsonDataWriter::<EnableTypeOptimization>k__BackingField
	bool ___U3CEnableTypeOptimizationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_justStarted_4() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___justStarted_4)); }
	inline bool get_justStarted_4() const { return ___justStarted_4; }
	inline bool* get_address_of_justStarted_4() { return &___justStarted_4; }
	inline void set_justStarted_4(bool value)
	{
		___justStarted_4 = value;
	}

	inline static int32_t get_offset_of_forceNoSeparatorNextLine_5() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___forceNoSeparatorNextLine_5)); }
	inline bool get_forceNoSeparatorNextLine_5() const { return ___forceNoSeparatorNextLine_5; }
	inline bool* get_address_of_forceNoSeparatorNextLine_5() { return &___forceNoSeparatorNextLine_5; }
	inline void set_forceNoSeparatorNextLine_5(bool value)
	{
		___forceNoSeparatorNextLine_5 = value;
	}

	inline static int32_t get_offset_of_escapeStringBuilder_6() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___escapeStringBuilder_6)); }
	inline StringBuilder_t * get_escapeStringBuilder_6() const { return ___escapeStringBuilder_6; }
	inline StringBuilder_t ** get_address_of_escapeStringBuilder_6() { return &___escapeStringBuilder_6; }
	inline void set_escapeStringBuilder_6(StringBuilder_t * value)
	{
		___escapeStringBuilder_6 = value;
		Il2CppCodeGenWriteBarrier((&___escapeStringBuilder_6), value);
	}

	inline static int32_t get_offset_of_writer_7() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___writer_7)); }
	inline StreamWriter_t1266378904 * get_writer_7() const { return ___writer_7; }
	inline StreamWriter_t1266378904 ** get_address_of_writer_7() { return &___writer_7; }
	inline void set_writer_7(StreamWriter_t1266378904 * value)
	{
		___writer_7 = value;
		Il2CppCodeGenWriteBarrier((&___writer_7), value);
	}

	inline static int32_t get_offset_of_primitiveTypeWriters_8() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___primitiveTypeWriters_8)); }
	inline Dictionary_2_t3632739877 * get_primitiveTypeWriters_8() const { return ___primitiveTypeWriters_8; }
	inline Dictionary_2_t3632739877 ** get_address_of_primitiveTypeWriters_8() { return &___primitiveTypeWriters_8; }
	inline void set_primitiveTypeWriters_8(Dictionary_2_t3632739877 * value)
	{
		___primitiveTypeWriters_8 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypeWriters_8), value);
	}

	inline static int32_t get_offset_of_seenTypes_9() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___seenTypes_9)); }
	inline Dictionary_2_t1100325521 * get_seenTypes_9() const { return ___seenTypes_9; }
	inline Dictionary_2_t1100325521 ** get_address_of_seenTypes_9() { return &___seenTypes_9; }
	inline void set_seenTypes_9(Dictionary_2_t1100325521 * value)
	{
		___seenTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___seenTypes_9), value);
	}

	inline static int32_t get_offset_of_U3CFormatAsReadableU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___U3CFormatAsReadableU3Ek__BackingField_10)); }
	inline bool get_U3CFormatAsReadableU3Ek__BackingField_10() const { return ___U3CFormatAsReadableU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CFormatAsReadableU3Ek__BackingField_10() { return &___U3CFormatAsReadableU3Ek__BackingField_10; }
	inline void set_U3CFormatAsReadableU3Ek__BackingField_10(bool value)
	{
		___U3CFormatAsReadableU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CEnableTypeOptimizationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonDataWriter_t2586654915, ___U3CEnableTypeOptimizationU3Ek__BackingField_11)); }
	inline bool get_U3CEnableTypeOptimizationU3Ek__BackingField_11() const { return ___U3CEnableTypeOptimizationU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CEnableTypeOptimizationU3Ek__BackingField_11() { return &___U3CEnableTypeOptimizationU3Ek__BackingField_11; }
	inline void set_U3CEnableTypeOptimizationU3Ek__BackingField_11(bool value)
	{
		___U3CEnableTypeOptimizationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATAWRITER_T2586654915_H
#ifndef JSONTEXTREADER_T2625489622_H
#define JSONTEXTREADER_T2625489622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.JsonTextReader
struct  JsonTextReader_t2625489622  : public RuntimeObject
{
public:
	// System.IO.StreamReader Sirenix.Serialization.JsonTextReader::reader
	StreamReader_t4009935899 * ___reader_2;
	// System.Int32 Sirenix.Serialization.JsonTextReader::bufferIndex
	int32_t ___bufferIndex_3;
	// System.Char[] Sirenix.Serialization.JsonTextReader::buffer
	CharU5BU5D_t3528271667* ___buffer_4;
	// System.Nullable`1<System.Char> Sirenix.Serialization.JsonTextReader::lastReadChar
	Nullable_1_t1062055256  ___lastReadChar_5;
	// System.Nullable`1<System.Char> Sirenix.Serialization.JsonTextReader::peekedChar
	Nullable_1_t1062055256  ___peekedChar_6;
	// System.Collections.Generic.Queue`1<System.Char> Sirenix.Serialization.JsonTextReader::emergencyPlayback
	Queue_1_t3480719964 * ___emergencyPlayback_7;
	// Sirenix.Serialization.DeserializationContext Sirenix.Serialization.JsonTextReader::<Context>k__BackingField
	DeserializationContext_t2871073323 * ___U3CContextU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___reader_2)); }
	inline StreamReader_t4009935899 * get_reader_2() const { return ___reader_2; }
	inline StreamReader_t4009935899 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(StreamReader_t4009935899 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_bufferIndex_3() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___bufferIndex_3)); }
	inline int32_t get_bufferIndex_3() const { return ___bufferIndex_3; }
	inline int32_t* get_address_of_bufferIndex_3() { return &___bufferIndex_3; }
	inline void set_bufferIndex_3(int32_t value)
	{
		___bufferIndex_3 = value;
	}

	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___buffer_4)); }
	inline CharU5BU5D_t3528271667* get_buffer_4() const { return ___buffer_4; }
	inline CharU5BU5D_t3528271667** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(CharU5BU5D_t3528271667* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_4), value);
	}

	inline static int32_t get_offset_of_lastReadChar_5() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___lastReadChar_5)); }
	inline Nullable_1_t1062055256  get_lastReadChar_5() const { return ___lastReadChar_5; }
	inline Nullable_1_t1062055256 * get_address_of_lastReadChar_5() { return &___lastReadChar_5; }
	inline void set_lastReadChar_5(Nullable_1_t1062055256  value)
	{
		___lastReadChar_5 = value;
	}

	inline static int32_t get_offset_of_peekedChar_6() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___peekedChar_6)); }
	inline Nullable_1_t1062055256  get_peekedChar_6() const { return ___peekedChar_6; }
	inline Nullable_1_t1062055256 * get_address_of_peekedChar_6() { return &___peekedChar_6; }
	inline void set_peekedChar_6(Nullable_1_t1062055256  value)
	{
		___peekedChar_6 = value;
	}

	inline static int32_t get_offset_of_emergencyPlayback_7() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___emergencyPlayback_7)); }
	inline Queue_1_t3480719964 * get_emergencyPlayback_7() const { return ___emergencyPlayback_7; }
	inline Queue_1_t3480719964 ** get_address_of_emergencyPlayback_7() { return &___emergencyPlayback_7; }
	inline void set_emergencyPlayback_7(Queue_1_t3480719964 * value)
	{
		___emergencyPlayback_7 = value;
		Il2CppCodeGenWriteBarrier((&___emergencyPlayback_7), value);
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622, ___U3CContextU3Ek__BackingField_8)); }
	inline DeserializationContext_t2871073323 * get_U3CContextU3Ek__BackingField_8() const { return ___U3CContextU3Ek__BackingField_8; }
	inline DeserializationContext_t2871073323 ** get_address_of_U3CContextU3Ek__BackingField_8() { return &___U3CContextU3Ek__BackingField_8; }
	inline void set_U3CContextU3Ek__BackingField_8(DeserializationContext_t2871073323 * value)
	{
		___U3CContextU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextU3Ek__BackingField_8), value);
	}
};

struct JsonTextReader_t2625489622_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Char,System.Nullable`1<Sirenix.Serialization.EntryType>> Sirenix.Serialization.JsonTextReader::EntryDelineators
	Dictionary_2_t2243406842 * ___EntryDelineators_0;
	// System.Collections.Generic.Dictionary`2<System.Char,System.Char> Sirenix.Serialization.JsonTextReader::UnescapeDictionary
	Dictionary_2_t1371231800 * ___UnescapeDictionary_1;

public:
	inline static int32_t get_offset_of_EntryDelineators_0() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622_StaticFields, ___EntryDelineators_0)); }
	inline Dictionary_2_t2243406842 * get_EntryDelineators_0() const { return ___EntryDelineators_0; }
	inline Dictionary_2_t2243406842 ** get_address_of_EntryDelineators_0() { return &___EntryDelineators_0; }
	inline void set_EntryDelineators_0(Dictionary_2_t2243406842 * value)
	{
		___EntryDelineators_0 = value;
		Il2CppCodeGenWriteBarrier((&___EntryDelineators_0), value);
	}

	inline static int32_t get_offset_of_UnescapeDictionary_1() { return static_cast<int32_t>(offsetof(JsonTextReader_t2625489622_StaticFields, ___UnescapeDictionary_1)); }
	inline Dictionary_2_t1371231800 * get_UnescapeDictionary_1() const { return ___UnescapeDictionary_1; }
	inline Dictionary_2_t1371231800 ** get_address_of_UnescapeDictionary_1() { return &___UnescapeDictionary_1; }
	inline void set_UnescapeDictionary_1(Dictionary_2_t1371231800 * value)
	{
		___UnescapeDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnescapeDictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTREADER_T2625489622_H
#ifndef SERIALIZATIONNODEDATAWRITER_T3531837839_H
#define SERIALIZATIONNODEDATAWRITER_T3531837839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationNodeDataWriter
struct  SerializationNodeDataWriter_t3531837839  : public BaseDataWriter_t1131909494
{
public:
	// System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationNodeDataWriter::nodes
	List_1_t1861728361 * ___nodes_4;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate> Sirenix.Serialization.SerializationNodeDataWriter::primitiveTypeWriters
	Dictionary_2_t3632739877 * ___primitiveTypeWriters_5;

public:
	inline static int32_t get_offset_of_nodes_4() { return static_cast<int32_t>(offsetof(SerializationNodeDataWriter_t3531837839, ___nodes_4)); }
	inline List_1_t1861728361 * get_nodes_4() const { return ___nodes_4; }
	inline List_1_t1861728361 ** get_address_of_nodes_4() { return &___nodes_4; }
	inline void set_nodes_4(List_1_t1861728361 * value)
	{
		___nodes_4 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_4), value);
	}

	inline static int32_t get_offset_of_primitiveTypeWriters_5() { return static_cast<int32_t>(offsetof(SerializationNodeDataWriter_t3531837839, ___primitiveTypeWriters_5)); }
	inline Dictionary_2_t3632739877 * get_primitiveTypeWriters_5() const { return ___primitiveTypeWriters_5; }
	inline Dictionary_2_t3632739877 ** get_address_of_primitiveTypeWriters_5() { return &___primitiveTypeWriters_5; }
	inline void set_primitiveTypeWriters_5(Dictionary_2_t3632739877 * value)
	{
		___primitiveTypeWriters_5 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypeWriters_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONNODEDATAWRITER_T3531837839_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef WINPRODUCTDESCRIPTION_T2080881907_H
#define WINPRODUCTDESCRIPTION_T2080881907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Default.WinProductDescription
struct  WinProductDescription_t2080881907  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<platformSpecificID>k__BackingField
	String_t* ___U3CplatformSpecificIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<price>k__BackingField
	String_t* ___U3CpriceU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<ISOCurrencyCode>k__BackingField
	String_t* ___U3CISOCurrencyCodeU3Ek__BackingField_4;
	// System.Decimal UnityEngine.Purchasing.Default.WinProductDescription::<priceDecimal>k__BackingField
	Decimal_t2948259380  ___U3CpriceDecimalU3Ek__BackingField_5;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_6;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_7;
	// System.Boolean UnityEngine.Purchasing.Default.WinProductDescription::<consumable>k__BackingField
	bool ___U3CconsumableU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CplatformSpecificIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CplatformSpecificIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CplatformSpecificIDU3Ek__BackingField_0() const { return ___U3CplatformSpecificIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CplatformSpecificIDU3Ek__BackingField_0() { return &___U3CplatformSpecificIDU3Ek__BackingField_0; }
	inline void set_U3CplatformSpecificIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CplatformSpecificIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplatformSpecificIDU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CpriceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CpriceU3Ek__BackingField_1)); }
	inline String_t* get_U3CpriceU3Ek__BackingField_1() const { return ___U3CpriceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CpriceU3Ek__BackingField_1() { return &___U3CpriceU3Ek__BackingField_1; }
	inline void set_U3CpriceU3Ek__BackingField_1(String_t* value)
	{
		___U3CpriceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpriceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CtitleU3Ek__BackingField_2)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_2() const { return ___U3CtitleU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_2() { return &___U3CtitleU3Ek__BackingField_2; }
	inline void set_U3CtitleU3Ek__BackingField_2(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CdescriptionU3Ek__BackingField_3)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_3() const { return ___U3CdescriptionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_3() { return &___U3CdescriptionU3Ek__BackingField_3; }
	inline void set_U3CdescriptionU3Ek__BackingField_3(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CISOCurrencyCodeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CISOCurrencyCodeU3Ek__BackingField_4)); }
	inline String_t* get_U3CISOCurrencyCodeU3Ek__BackingField_4() const { return ___U3CISOCurrencyCodeU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CISOCurrencyCodeU3Ek__BackingField_4() { return &___U3CISOCurrencyCodeU3Ek__BackingField_4; }
	inline void set_U3CISOCurrencyCodeU3Ek__BackingField_4(String_t* value)
	{
		___U3CISOCurrencyCodeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CISOCurrencyCodeU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CpriceDecimalU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CpriceDecimalU3Ek__BackingField_5)); }
	inline Decimal_t2948259380  get_U3CpriceDecimalU3Ek__BackingField_5() const { return ___U3CpriceDecimalU3Ek__BackingField_5; }
	inline Decimal_t2948259380 * get_address_of_U3CpriceDecimalU3Ek__BackingField_5() { return &___U3CpriceDecimalU3Ek__BackingField_5; }
	inline void set_U3CpriceDecimalU3Ek__BackingField_5(Decimal_t2948259380  value)
	{
		___U3CpriceDecimalU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CreceiptU3Ek__BackingField_6)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_6() const { return ___U3CreceiptU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_6() { return &___U3CreceiptU3Ek__BackingField_6; }
	inline void set_U3CreceiptU3Ek__BackingField_6(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CtransactionIDU3Ek__BackingField_7)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_7() const { return ___U3CtransactionIDU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_7() { return &___U3CtransactionIDU3Ek__BackingField_7; }
	inline void set_U3CtransactionIDU3Ek__BackingField_7(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CconsumableU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CconsumableU3Ek__BackingField_8)); }
	inline bool get_U3CconsumableU3Ek__BackingField_8() const { return ___U3CconsumableU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CconsumableU3Ek__BackingField_8() { return &___U3CconsumableU3Ek__BackingField_8; }
	inline void set_U3CconsumableU3Ek__BackingField_8(bool value)
	{
		___U3CconsumableU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINPRODUCTDESCRIPTION_T2080881907_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef SERIALIZATIONDATA_T3163410965_H
#define SERIALIZATIONDATA_T3163410965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationData
struct  SerializationData_t3163410965 
{
public:
	// Sirenix.Serialization.DataFormat Sirenix.Serialization.SerializationData::SerializedFormat
	int32_t ___SerializedFormat_3;
	// System.Byte[] Sirenix.Serialization.SerializationData::SerializedBytes
	ByteU5BU5D_t4116647657* ___SerializedBytes_4;
	// System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.SerializationData::ReferencedUnityObjects
	List_1_t2103082695 * ___ReferencedUnityObjects_5;
	// System.String Sirenix.Serialization.SerializationData::SerializedBytesString
	String_t* ___SerializedBytesString_6;
	// UnityEngine.Object Sirenix.Serialization.SerializationData::Prefab
	Object_t631007953 * ___Prefab_7;
	// System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.SerializationData::PrefabModificationsReferencedUnityObjects
	List_1_t2103082695 * ___PrefabModificationsReferencedUnityObjects_8;
	// System.Collections.Generic.List`1<System.String> Sirenix.Serialization.SerializationData::PrefabModifications
	List_1_t3319525431 * ___PrefabModifications_9;
	// System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationData::SerializationNodes
	List_1_t1861728361 * ___SerializationNodes_10;

public:
	inline static int32_t get_offset_of_SerializedFormat_3() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializedFormat_3)); }
	inline int32_t get_SerializedFormat_3() const { return ___SerializedFormat_3; }
	inline int32_t* get_address_of_SerializedFormat_3() { return &___SerializedFormat_3; }
	inline void set_SerializedFormat_3(int32_t value)
	{
		___SerializedFormat_3 = value;
	}

	inline static int32_t get_offset_of_SerializedBytes_4() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializedBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_SerializedBytes_4() const { return ___SerializedBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_SerializedBytes_4() { return &___SerializedBytes_4; }
	inline void set_SerializedBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___SerializedBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___SerializedBytes_4), value);
	}

	inline static int32_t get_offset_of_ReferencedUnityObjects_5() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___ReferencedUnityObjects_5)); }
	inline List_1_t2103082695 * get_ReferencedUnityObjects_5() const { return ___ReferencedUnityObjects_5; }
	inline List_1_t2103082695 ** get_address_of_ReferencedUnityObjects_5() { return &___ReferencedUnityObjects_5; }
	inline void set_ReferencedUnityObjects_5(List_1_t2103082695 * value)
	{
		___ReferencedUnityObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedUnityObjects_5), value);
	}

	inline static int32_t get_offset_of_SerializedBytesString_6() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializedBytesString_6)); }
	inline String_t* get_SerializedBytesString_6() const { return ___SerializedBytesString_6; }
	inline String_t** get_address_of_SerializedBytesString_6() { return &___SerializedBytesString_6; }
	inline void set_SerializedBytesString_6(String_t* value)
	{
		___SerializedBytesString_6 = value;
		Il2CppCodeGenWriteBarrier((&___SerializedBytesString_6), value);
	}

	inline static int32_t get_offset_of_Prefab_7() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___Prefab_7)); }
	inline Object_t631007953 * get_Prefab_7() const { return ___Prefab_7; }
	inline Object_t631007953 ** get_address_of_Prefab_7() { return &___Prefab_7; }
	inline void set_Prefab_7(Object_t631007953 * value)
	{
		___Prefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_7), value);
	}

	inline static int32_t get_offset_of_PrefabModificationsReferencedUnityObjects_8() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___PrefabModificationsReferencedUnityObjects_8)); }
	inline List_1_t2103082695 * get_PrefabModificationsReferencedUnityObjects_8() const { return ___PrefabModificationsReferencedUnityObjects_8; }
	inline List_1_t2103082695 ** get_address_of_PrefabModificationsReferencedUnityObjects_8() { return &___PrefabModificationsReferencedUnityObjects_8; }
	inline void set_PrefabModificationsReferencedUnityObjects_8(List_1_t2103082695 * value)
	{
		___PrefabModificationsReferencedUnityObjects_8 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabModificationsReferencedUnityObjects_8), value);
	}

	inline static int32_t get_offset_of_PrefabModifications_9() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___PrefabModifications_9)); }
	inline List_1_t3319525431 * get_PrefabModifications_9() const { return ___PrefabModifications_9; }
	inline List_1_t3319525431 ** get_address_of_PrefabModifications_9() { return &___PrefabModifications_9; }
	inline void set_PrefabModifications_9(List_1_t3319525431 * value)
	{
		___PrefabModifications_9 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabModifications_9), value);
	}

	inline static int32_t get_offset_of_SerializationNodes_10() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializationNodes_10)); }
	inline List_1_t1861728361 * get_SerializationNodes_10() const { return ___SerializationNodes_10; }
	inline List_1_t1861728361 ** get_address_of_SerializationNodes_10() { return &___SerializationNodes_10; }
	inline void set_SerializationNodes_10(List_1_t1861728361 * value)
	{
		___SerializationNodes_10 = value;
		Il2CppCodeGenWriteBarrier((&___SerializationNodes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Serialization.SerializationData
struct SerializationData_t3163410965_marshaled_pinvoke
{
	int32_t ___SerializedFormat_3;
	uint8_t* ___SerializedBytes_4;
	List_1_t2103082695 * ___ReferencedUnityObjects_5;
	char* ___SerializedBytesString_6;
	Object_t631007953_marshaled_pinvoke ___Prefab_7;
	List_1_t2103082695 * ___PrefabModificationsReferencedUnityObjects_8;
	List_1_t3319525431 * ___PrefabModifications_9;
	List_1_t1861728361 * ___SerializationNodes_10;
};
// Native definition for COM marshalling of Sirenix.Serialization.SerializationData
struct SerializationData_t3163410965_marshaled_com
{
	int32_t ___SerializedFormat_3;
	uint8_t* ___SerializedBytes_4;
	List_1_t2103082695 * ___ReferencedUnityObjects_5;
	Il2CppChar* ___SerializedBytesString_6;
	Object_t631007953_marshaled_com* ___Prefab_7;
	List_1_t2103082695 * ___PrefabModificationsReferencedUnityObjects_8;
	List_1_t3319525431 * ___PrefabModifications_9;
	List_1_t1861728361 * ___SerializationNodes_10;
};
#endif // SERIALIZATIONDATA_T3163410965_H
#ifndef SERIALIZATIONNODE_T389653619_H
#define SERIALIZATIONNODE_T389653619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationNode
struct  SerializationNode_t389653619 
{
public:
	// System.String Sirenix.Serialization.SerializationNode::Name
	String_t* ___Name_0;
	// Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNode::Entry
	uint8_t ___Entry_1;
	// System.String Sirenix.Serialization.SerializationNode::Data
	String_t* ___Data_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SerializationNode_t389653619, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Entry_1() { return static_cast<int32_t>(offsetof(SerializationNode_t389653619, ___Entry_1)); }
	inline uint8_t get_Entry_1() const { return ___Entry_1; }
	inline uint8_t* get_address_of_Entry_1() { return &___Entry_1; }
	inline void set_Entry_1(uint8_t value)
	{
		___Entry_1 = value;
	}

	inline static int32_t get_offset_of_Data_2() { return static_cast<int32_t>(offsetof(SerializationNode_t389653619, ___Data_2)); }
	inline String_t* get_Data_2() const { return ___Data_2; }
	inline String_t** get_address_of_Data_2() { return &___Data_2; }
	inline void set_Data_2(String_t* value)
	{
		___Data_2 = value;
		Il2CppCodeGenWriteBarrier((&___Data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Serialization.SerializationNode
struct SerializationNode_t389653619_marshaled_pinvoke
{
	char* ___Name_0;
	uint8_t ___Entry_1;
	char* ___Data_2;
};
// Native definition for COM marshalling of Sirenix.Serialization.SerializationNode
struct SerializationNode_t389653619_marshaled_com
{
	Il2CppChar* ___Name_0;
	uint8_t ___Entry_1;
	Il2CppChar* ___Data_2;
};
#endif // SERIALIZATIONNODE_T389653619_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T211668216_H
#define NULLABLE_1_T211668216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Sirenix.Serialization.EntryType>
struct  Nullable_1_t211668216 
{
public:
	// T System.Nullable`1::value
	uint8_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t211668216, ___value_0)); }
	inline uint8_t get_value_0() const { return ___value_0; }
	inline uint8_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint8_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t211668216, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T211668216_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef SERIALIZEDCOMPONENT_T2886965603_H
#define SERIALIZEDCOMPONENT_T2886965603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedComponent
struct  SerializedComponent_t2886965603  : public Component_t1923634451
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedComponent::serializationData
	SerializationData_t3163410965  ___serializationData_4;

public:
	inline static int32_t get_offset_of_serializationData_4() { return static_cast<int32_t>(offsetof(SerializedComponent_t2886965603, ___serializationData_4)); }
	inline SerializationData_t3163410965  get_serializationData_4() const { return ___serializationData_4; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_4() { return &___serializationData_4; }
	inline void set_serializationData_4(SerializationData_t3163410965  value)
	{
		___serializationData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDCOMPONENT_T2886965603_H
#ifndef SERIALIZEDSCRIPTABLEOBJECT_T3922853626_H
#define SERIALIZEDSCRIPTABLEOBJECT_T3922853626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedScriptableObject
struct  SerializedScriptableObject_t3922853626  : public ScriptableObject_t2528358522
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedScriptableObject::serializationData
	SerializationData_t3163410965  ___serializationData_4;

public:
	inline static int32_t get_offset_of_serializationData_4() { return static_cast<int32_t>(offsetof(SerializedScriptableObject_t3922853626, ___serializationData_4)); }
	inline SerializationData_t3163410965  get_serializationData_4() const { return ___serializationData_4; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_4() { return &___serializationData_4; }
	inline void set_serializationData_4(SerializationData_t3163410965  value)
	{
		___serializationData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDSCRIPTABLEOBJECT_T3922853626_H
#ifndef SERIALIZEDUNITYOBJECT_T415701860_H
#define SERIALIZEDUNITYOBJECT_T415701860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedUnityObject
struct  SerializedUnityObject_t415701860  : public Object_t631007953
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedUnityObject::serializationData
	SerializationData_t3163410965  ___serializationData_4;

public:
	inline static int32_t get_offset_of_serializationData_4() { return static_cast<int32_t>(offsetof(SerializedUnityObject_t415701860, ___serializationData_4)); }
	inline SerializationData_t3163410965  get_serializationData_4() const { return ___serializationData_4; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_4() { return &___serializationData_4; }
	inline void set_serializationData_4(SerializationData_t3163410965  value)
	{
		___serializationData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDUNITYOBJECT_T415701860_H
#ifndef BINARYDATAREADER_T2404603355_H
#define BINARYDATAREADER_T2404603355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BinaryDataReader
struct  BinaryDataReader_t2404603355  : public BaseDataReader_t3963898873
{
public:
	// System.Byte[] Sirenix.Serialization.BinaryDataReader::buffer
	ByteU5BU5D_t4116647657* ___buffer_5;
	// System.Nullable`1<Sirenix.Serialization.EntryType> Sirenix.Serialization.BinaryDataReader::peekedEntryType
	Nullable_1_t211668216  ___peekedEntryType_6;
	// Sirenix.Serialization.BinaryEntryType Sirenix.Serialization.BinaryDataReader::peekedBinaryEntryType
	uint8_t ___peekedBinaryEntryType_7;
	// System.String Sirenix.Serialization.BinaryDataReader::peekedEntryName
	String_t* ___peekedEntryName_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Type> Sirenix.Serialization.BinaryDataReader::types
	Dictionary_2_t1372658091 * ___types_9;

public:
	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(BinaryDataReader_t2404603355, ___buffer_5)); }
	inline ByteU5BU5D_t4116647657* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_t4116647657* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_peekedEntryType_6() { return static_cast<int32_t>(offsetof(BinaryDataReader_t2404603355, ___peekedEntryType_6)); }
	inline Nullable_1_t211668216  get_peekedEntryType_6() const { return ___peekedEntryType_6; }
	inline Nullable_1_t211668216 * get_address_of_peekedEntryType_6() { return &___peekedEntryType_6; }
	inline void set_peekedEntryType_6(Nullable_1_t211668216  value)
	{
		___peekedEntryType_6 = value;
	}

	inline static int32_t get_offset_of_peekedBinaryEntryType_7() { return static_cast<int32_t>(offsetof(BinaryDataReader_t2404603355, ___peekedBinaryEntryType_7)); }
	inline uint8_t get_peekedBinaryEntryType_7() const { return ___peekedBinaryEntryType_7; }
	inline uint8_t* get_address_of_peekedBinaryEntryType_7() { return &___peekedBinaryEntryType_7; }
	inline void set_peekedBinaryEntryType_7(uint8_t value)
	{
		___peekedBinaryEntryType_7 = value;
	}

	inline static int32_t get_offset_of_peekedEntryName_8() { return static_cast<int32_t>(offsetof(BinaryDataReader_t2404603355, ___peekedEntryName_8)); }
	inline String_t* get_peekedEntryName_8() const { return ___peekedEntryName_8; }
	inline String_t** get_address_of_peekedEntryName_8() { return &___peekedEntryName_8; }
	inline void set_peekedEntryName_8(String_t* value)
	{
		___peekedEntryName_8 = value;
		Il2CppCodeGenWriteBarrier((&___peekedEntryName_8), value);
	}

	inline static int32_t get_offset_of_types_9() { return static_cast<int32_t>(offsetof(BinaryDataReader_t2404603355, ___types_9)); }
	inline Dictionary_2_t1372658091 * get_types_9() const { return ___types_9; }
	inline Dictionary_2_t1372658091 ** get_address_of_types_9() { return &___types_9; }
	inline void set_types_9(Dictionary_2_t1372658091 * value)
	{
		___types_9 = value;
		Il2CppCodeGenWriteBarrier((&___types_9), value);
	}
};

struct BinaryDataReader_t2404603355_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate> Sirenix.Serialization.BinaryDataReader::PrimitiveFromByteMethods
	Dictionary_2_t3632739877 * ___PrimitiveFromByteMethods_4;

public:
	inline static int32_t get_offset_of_PrimitiveFromByteMethods_4() { return static_cast<int32_t>(offsetof(BinaryDataReader_t2404603355_StaticFields, ___PrimitiveFromByteMethods_4)); }
	inline Dictionary_2_t3632739877 * get_PrimitiveFromByteMethods_4() const { return ___PrimitiveFromByteMethods_4; }
	inline Dictionary_2_t3632739877 ** get_address_of_PrimitiveFromByteMethods_4() { return &___PrimitiveFromByteMethods_4; }
	inline void set_PrimitiveFromByteMethods_4(Dictionary_2_t3632739877 * value)
	{
		___PrimitiveFromByteMethods_4 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveFromByteMethods_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYDATAREADER_T2404603355_H
#ifndef JSONDATAREADER_T4095320770_H
#define JSONDATAREADER_T4095320770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.JsonDataReader
struct  JsonDataReader_t4095320770  : public BaseDataReader_t3963898873
{
public:
	// Sirenix.Serialization.JsonTextReader Sirenix.Serialization.JsonDataReader::reader
	JsonTextReader_t2625489622 * ___reader_4;
	// System.Nullable`1<Sirenix.Serialization.EntryType> Sirenix.Serialization.JsonDataReader::peekedEntryType
	Nullable_1_t211668216  ___peekedEntryType_5;
	// System.String Sirenix.Serialization.JsonDataReader::peekedEntryName
	String_t* ___peekedEntryName_6;
	// System.String Sirenix.Serialization.JsonDataReader::peekedEntryContent
	String_t* ___peekedEntryContent_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Type> Sirenix.Serialization.JsonDataReader::seenTypes
	Dictionary_2_t1372658091 * ___seenTypes_8;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate> Sirenix.Serialization.JsonDataReader::primitiveArrayReaders
	Dictionary_2_t3632739877 * ___primitiveArrayReaders_9;

public:
	inline static int32_t get_offset_of_reader_4() { return static_cast<int32_t>(offsetof(JsonDataReader_t4095320770, ___reader_4)); }
	inline JsonTextReader_t2625489622 * get_reader_4() const { return ___reader_4; }
	inline JsonTextReader_t2625489622 ** get_address_of_reader_4() { return &___reader_4; }
	inline void set_reader_4(JsonTextReader_t2625489622 * value)
	{
		___reader_4 = value;
		Il2CppCodeGenWriteBarrier((&___reader_4), value);
	}

	inline static int32_t get_offset_of_peekedEntryType_5() { return static_cast<int32_t>(offsetof(JsonDataReader_t4095320770, ___peekedEntryType_5)); }
	inline Nullable_1_t211668216  get_peekedEntryType_5() const { return ___peekedEntryType_5; }
	inline Nullable_1_t211668216 * get_address_of_peekedEntryType_5() { return &___peekedEntryType_5; }
	inline void set_peekedEntryType_5(Nullable_1_t211668216  value)
	{
		___peekedEntryType_5 = value;
	}

	inline static int32_t get_offset_of_peekedEntryName_6() { return static_cast<int32_t>(offsetof(JsonDataReader_t4095320770, ___peekedEntryName_6)); }
	inline String_t* get_peekedEntryName_6() const { return ___peekedEntryName_6; }
	inline String_t** get_address_of_peekedEntryName_6() { return &___peekedEntryName_6; }
	inline void set_peekedEntryName_6(String_t* value)
	{
		___peekedEntryName_6 = value;
		Il2CppCodeGenWriteBarrier((&___peekedEntryName_6), value);
	}

	inline static int32_t get_offset_of_peekedEntryContent_7() { return static_cast<int32_t>(offsetof(JsonDataReader_t4095320770, ___peekedEntryContent_7)); }
	inline String_t* get_peekedEntryContent_7() const { return ___peekedEntryContent_7; }
	inline String_t** get_address_of_peekedEntryContent_7() { return &___peekedEntryContent_7; }
	inline void set_peekedEntryContent_7(String_t* value)
	{
		___peekedEntryContent_7 = value;
		Il2CppCodeGenWriteBarrier((&___peekedEntryContent_7), value);
	}

	inline static int32_t get_offset_of_seenTypes_8() { return static_cast<int32_t>(offsetof(JsonDataReader_t4095320770, ___seenTypes_8)); }
	inline Dictionary_2_t1372658091 * get_seenTypes_8() const { return ___seenTypes_8; }
	inline Dictionary_2_t1372658091 ** get_address_of_seenTypes_8() { return &___seenTypes_8; }
	inline void set_seenTypes_8(Dictionary_2_t1372658091 * value)
	{
		___seenTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___seenTypes_8), value);
	}

	inline static int32_t get_offset_of_primitiveArrayReaders_9() { return static_cast<int32_t>(offsetof(JsonDataReader_t4095320770, ___primitiveArrayReaders_9)); }
	inline Dictionary_2_t3632739877 * get_primitiveArrayReaders_9() const { return ___primitiveArrayReaders_9; }
	inline Dictionary_2_t3632739877 ** get_address_of_primitiveArrayReaders_9() { return &___primitiveArrayReaders_9; }
	inline void set_primitiveArrayReaders_9(Dictionary_2_t3632739877 * value)
	{
		___primitiveArrayReaders_9 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveArrayReaders_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATAREADER_T4095320770_H
#ifndef SERIALIZATIONNODEDATAREADER_T1897650348_H
#define SERIALIZATIONNODEDATAREADER_T1897650348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationNodeDataReader
struct  SerializationNodeDataReader_t1897650348  : public BaseDataReader_t3963898873
{
public:
	// System.String Sirenix.Serialization.SerializationNodeDataReader::peekedEntryName
	String_t* ___peekedEntryName_4;
	// System.Nullable`1<Sirenix.Serialization.EntryType> Sirenix.Serialization.SerializationNodeDataReader::peekedEntryType
	Nullable_1_t211668216  ___peekedEntryType_5;
	// System.String Sirenix.Serialization.SerializationNodeDataReader::peekedEntryData
	String_t* ___peekedEntryData_6;
	// System.Int32 Sirenix.Serialization.SerializationNodeDataReader::currentIndex
	int32_t ___currentIndex_7;
	// System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationNodeDataReader::nodes
	List_1_t1861728361 * ___nodes_8;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Delegate> Sirenix.Serialization.SerializationNodeDataReader::primitiveTypeReaders
	Dictionary_2_t3632739877 * ___primitiveTypeReaders_9;

public:
	inline static int32_t get_offset_of_peekedEntryName_4() { return static_cast<int32_t>(offsetof(SerializationNodeDataReader_t1897650348, ___peekedEntryName_4)); }
	inline String_t* get_peekedEntryName_4() const { return ___peekedEntryName_4; }
	inline String_t** get_address_of_peekedEntryName_4() { return &___peekedEntryName_4; }
	inline void set_peekedEntryName_4(String_t* value)
	{
		___peekedEntryName_4 = value;
		Il2CppCodeGenWriteBarrier((&___peekedEntryName_4), value);
	}

	inline static int32_t get_offset_of_peekedEntryType_5() { return static_cast<int32_t>(offsetof(SerializationNodeDataReader_t1897650348, ___peekedEntryType_5)); }
	inline Nullable_1_t211668216  get_peekedEntryType_5() const { return ___peekedEntryType_5; }
	inline Nullable_1_t211668216 * get_address_of_peekedEntryType_5() { return &___peekedEntryType_5; }
	inline void set_peekedEntryType_5(Nullable_1_t211668216  value)
	{
		___peekedEntryType_5 = value;
	}

	inline static int32_t get_offset_of_peekedEntryData_6() { return static_cast<int32_t>(offsetof(SerializationNodeDataReader_t1897650348, ___peekedEntryData_6)); }
	inline String_t* get_peekedEntryData_6() const { return ___peekedEntryData_6; }
	inline String_t** get_address_of_peekedEntryData_6() { return &___peekedEntryData_6; }
	inline void set_peekedEntryData_6(String_t* value)
	{
		___peekedEntryData_6 = value;
		Il2CppCodeGenWriteBarrier((&___peekedEntryData_6), value);
	}

	inline static int32_t get_offset_of_currentIndex_7() { return static_cast<int32_t>(offsetof(SerializationNodeDataReader_t1897650348, ___currentIndex_7)); }
	inline int32_t get_currentIndex_7() const { return ___currentIndex_7; }
	inline int32_t* get_address_of_currentIndex_7() { return &___currentIndex_7; }
	inline void set_currentIndex_7(int32_t value)
	{
		___currentIndex_7 = value;
	}

	inline static int32_t get_offset_of_nodes_8() { return static_cast<int32_t>(offsetof(SerializationNodeDataReader_t1897650348, ___nodes_8)); }
	inline List_1_t1861728361 * get_nodes_8() const { return ___nodes_8; }
	inline List_1_t1861728361 ** get_address_of_nodes_8() { return &___nodes_8; }
	inline void set_nodes_8(List_1_t1861728361 * value)
	{
		___nodes_8 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_8), value);
	}

	inline static int32_t get_offset_of_primitiveTypeReaders_9() { return static_cast<int32_t>(offsetof(SerializationNodeDataReader_t1897650348, ___primitiveTypeReaders_9)); }
	inline Dictionary_2_t3632739877 * get_primitiveTypeReaders_9() const { return ___primitiveTypeReaders_9; }
	inline Dictionary_2_t3632739877 ** get_address_of_primitiveTypeReaders_9() { return &___primitiveTypeReaders_9; }
	inline void set_primitiveTypeReaders_9(Dictionary_2_t3632739877 * value)
	{
		___primitiveTypeReaders_9 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypeReaders_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONNODEDATAREADER_T1897650348_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef STATEMACHINEBEHAVIOUR_T957311111_H
#define STATEMACHINEBEHAVIOUR_T957311111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t957311111  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T957311111_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef SERIALIZEDBEHAVIOUR_T3674147902_H
#define SERIALIZEDBEHAVIOUR_T3674147902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedBehaviour
struct  SerializedBehaviour_t3674147902  : public Behaviour_t1437897464
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedBehaviour::serializationData
	SerializationData_t3163410965  ___serializationData_4;

public:
	inline static int32_t get_offset_of_serializationData_4() { return static_cast<int32_t>(offsetof(SerializedBehaviour_t3674147902, ___serializationData_4)); }
	inline SerializationData_t3163410965  get_serializationData_4() const { return ___serializationData_4; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_4() { return &___serializationData_4; }
	inline void set_serializationData_4(SerializationData_t3163410965  value)
	{
		___serializationData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDBEHAVIOUR_T3674147902_H
#ifndef SERIALIZEDSTATEMACHINEBEHAVIOUR_T4029396201_H
#define SERIALIZEDSTATEMACHINEBEHAVIOUR_T4029396201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedStateMachineBehaviour
struct  SerializedStateMachineBehaviour_t4029396201  : public StateMachineBehaviour_t957311111
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedStateMachineBehaviour::serializationData
	SerializationData_t3163410965  ___serializationData_4;

public:
	inline static int32_t get_offset_of_serializationData_4() { return static_cast<int32_t>(offsetof(SerializedStateMachineBehaviour_t4029396201, ___serializationData_4)); }
	inline SerializationData_t3163410965  get_serializationData_4() const { return ___serializationData_4; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_4() { return &___serializationData_4; }
	inline void set_serializationData_4(SerializationData_t3163410965  value)
	{
		___serializationData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDSTATEMACHINEBEHAVIOUR_T4029396201_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SERIALIZEDMONOBEHAVIOUR_T720414636_H
#define SERIALIZEDMONOBEHAVIOUR_T720414636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedMonoBehaviour
struct  SerializedMonoBehaviour_t720414636  : public MonoBehaviour_t3962482529
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedMonoBehaviour::serializationData
	SerializationData_t3163410965  ___serializationData_4;

public:
	inline static int32_t get_offset_of_serializationData_4() { return static_cast<int32_t>(offsetof(SerializedMonoBehaviour_t720414636, ___serializationData_4)); }
	inline SerializationData_t3163410965  get_serializationData_4() const { return ___serializationData_4; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_4() { return &___serializationData_4; }
	inline void set_serializationData_4(SerializationData_t3163410965  value)
	{
		___serializationData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDMONOBEHAVIOUR_T720414636_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef NETWORKBEHAVIOUR_T204670959_H
#define NETWORKBEHAVIOUR_T204670959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour
struct  NetworkBehaviour_t204670959  : public MonoBehaviour_t3962482529
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkBehaviour::m_SyncVarDirtyBits
	uint32_t ___m_SyncVarDirtyBits_4;
	// System.Single UnityEngine.Networking.NetworkBehaviour::m_LastSendTime
	float ___m_LastSendTime_5;
	// System.Boolean UnityEngine.Networking.NetworkBehaviour::m_SyncVarGuard
	bool ___m_SyncVarGuard_6;
	// UnityEngine.Networking.NetworkIdentity UnityEngine.Networking.NetworkBehaviour::m_MyView
	NetworkIdentity_t3299519057 * ___m_MyView_8;

public:
	inline static int32_t get_offset_of_m_SyncVarDirtyBits_4() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_SyncVarDirtyBits_4)); }
	inline uint32_t get_m_SyncVarDirtyBits_4() const { return ___m_SyncVarDirtyBits_4; }
	inline uint32_t* get_address_of_m_SyncVarDirtyBits_4() { return &___m_SyncVarDirtyBits_4; }
	inline void set_m_SyncVarDirtyBits_4(uint32_t value)
	{
		___m_SyncVarDirtyBits_4 = value;
	}

	inline static int32_t get_offset_of_m_LastSendTime_5() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_LastSendTime_5)); }
	inline float get_m_LastSendTime_5() const { return ___m_LastSendTime_5; }
	inline float* get_address_of_m_LastSendTime_5() { return &___m_LastSendTime_5; }
	inline void set_m_LastSendTime_5(float value)
	{
		___m_LastSendTime_5 = value;
	}

	inline static int32_t get_offset_of_m_SyncVarGuard_6() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_SyncVarGuard_6)); }
	inline bool get_m_SyncVarGuard_6() const { return ___m_SyncVarGuard_6; }
	inline bool* get_address_of_m_SyncVarGuard_6() { return &___m_SyncVarGuard_6; }
	inline void set_m_SyncVarGuard_6(bool value)
	{
		___m_SyncVarGuard_6 = value;
	}

	inline static int32_t get_offset_of_m_MyView_8() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959, ___m_MyView_8)); }
	inline NetworkIdentity_t3299519057 * get_m_MyView_8() const { return ___m_MyView_8; }
	inline NetworkIdentity_t3299519057 ** get_address_of_m_MyView_8() { return &___m_MyView_8; }
	inline void set_m_MyView_8(NetworkIdentity_t3299519057 * value)
	{
		___m_MyView_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MyView_8), value);
	}
};

struct NetworkBehaviour_t204670959_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker> UnityEngine.Networking.NetworkBehaviour::s_CmdHandlerDelegates
	Dictionary_2_t100189446 * ___s_CmdHandlerDelegates_9;

public:
	inline static int32_t get_offset_of_s_CmdHandlerDelegates_9() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t204670959_StaticFields, ___s_CmdHandlerDelegates_9)); }
	inline Dictionary_2_t100189446 * get_s_CmdHandlerDelegates_9() const { return ___s_CmdHandlerDelegates_9; }
	inline Dictionary_2_t100189446 ** get_address_of_s_CmdHandlerDelegates_9() { return &___s_CmdHandlerDelegates_9; }
	inline void set_s_CmdHandlerDelegates_9(Dictionary_2_t100189446 * value)
	{
		___s_CmdHandlerDelegates_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CmdHandlerDelegates_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKBEHAVIOUR_T204670959_H
#ifndef SERIALIZEDNETWORKBEHAVIOUR_T2723310828_H
#define SERIALIZEDNETWORKBEHAVIOUR_T2723310828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.OdinInspector.SerializedNetworkBehaviour
struct  SerializedNetworkBehaviour_t2723310828  : public NetworkBehaviour_t204670959
{
public:
	// Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedNetworkBehaviour::serializationData
	SerializationData_t3163410965  ___serializationData_10;

public:
	inline static int32_t get_offset_of_serializationData_10() { return static_cast<int32_t>(offsetof(SerializedNetworkBehaviour_t2723310828, ___serializationData_10)); }
	inline SerializationData_t3163410965  get_serializationData_10() const { return ___serializationData_10; }
	inline SerializationData_t3163410965 * get_address_of_serializationData_10() { return &___serializationData_10; }
	inline void set_serializationData_10(SerializationData_t3163410965  value)
	{
		___serializationData_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDNETWORKBEHAVIOUR_T2723310828_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_4)); }
	inline Graphic_t1660335611 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_t1660335611 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_5)); }
	inline Color_t2555686324  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t2555686324  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_6)); }
	inline Vector2_t2156229523  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_t2156229523  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3703[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3710[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_5(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_6(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3711[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (U3CModuleU3E_t692745572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (Factory_t3975828591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (WinProductDescription_t2080881907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[9] = 
{
	WinProductDescription_t2080881907::get_offset_of_U3CplatformSpecificIDU3Ek__BackingField_0(),
	WinProductDescription_t2080881907::get_offset_of_U3CpriceU3Ek__BackingField_1(),
	WinProductDescription_t2080881907::get_offset_of_U3CtitleU3Ek__BackingField_2(),
	WinProductDescription_t2080881907::get_offset_of_U3CdescriptionU3Ek__BackingField_3(),
	WinProductDescription_t2080881907::get_offset_of_U3CISOCurrencyCodeU3Ek__BackingField_4(),
	WinProductDescription_t2080881907::get_offset_of_U3CpriceDecimalU3Ek__BackingField_5(),
	WinProductDescription_t2080881907::get_offset_of_U3CreceiptU3Ek__BackingField_6(),
	WinProductDescription_t2080881907::get_offset_of_U3CtransactionIDU3Ek__BackingField_7(),
	WinProductDescription_t2080881907::get_offset_of_U3CconsumableU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (U3CModuleU3E_t692745573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (SerializedBehaviour_t3674147902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3720[1] = 
{
	SerializedBehaviour_t3674147902::get_offset_of_serializationData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (SerializedComponent_t2886965603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3721[1] = 
{
	SerializedComponent_t2886965603::get_offset_of_serializationData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (SerializedMonoBehaviour_t720414636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[1] = 
{
	SerializedMonoBehaviour_t720414636::get_offset_of_serializationData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (SerializedNetworkBehaviour_t2723310828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[1] = 
{
	SerializedNetworkBehaviour_t2723310828::get_offset_of_serializationData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (SerializedScriptableObject_t3922853626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[1] = 
{
	SerializedScriptableObject_t3922853626::get_offset_of_serializationData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (SerializedStateMachineBehaviour_t4029396201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3725[1] = 
{
	SerializedStateMachineBehaviour_t4029396201::get_offset_of_serializationData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (SerializedUnityObject_t415701860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[1] = 
{
	SerializedUnityObject_t415701860::get_offset_of_serializationData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (ArrayFormatterLocator_t2706966186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (DelegateFormatterLocator_t1064339770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (FormatterLocationStep_t2859269825)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3733[3] = 
{
	FormatterLocationStep_t2859269825::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (GenericCollectionFormatterLocator_t1557485371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (ISerializableFormatterLocator_t480903605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (RegisterFormatterAttribute_t697622418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[2] = 
{
	RegisterFormatterAttribute_t697622418::get_offset_of_U3CFormatterTypeU3Ek__BackingField_0(),
	RegisterFormatterAttribute_t697622418::get_offset_of_U3CPriorityU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (RegisterFormatterLocatorAttribute_t2636356893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3737[2] = 
{
	RegisterFormatterLocatorAttribute_t2636356893::get_offset_of_U3CFormatterLocatorTypeU3Ek__BackingField_0(),
	RegisterFormatterLocatorAttribute_t2636356893::get_offset_of_U3CPriorityU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (SelfFormatterLocator_t1132301583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (TypeFormatterLocator_t3026799695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (BaseDataReader_t3963898873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[2] = 
{
	BaseDataReader_t3963898873::get_offset_of_context_2(),
	BaseDataReader_t3963898873::get_offset_of_stream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (BaseDataReaderWriter_t3725085398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3741[2] = 
{
	BaseDataReaderWriter_t3725085398::get_offset_of_binder_0(),
	BaseDataReaderWriter_t3725085398::get_offset_of_nodes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (BaseDataWriter_t1131909494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3742[2] = 
{
	BaseDataWriter_t1131909494::get_offset_of_context_2(),
	BaseDataWriter_t1131909494::get_offset_of_stream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (BinaryDataReader_t2404603355), -1, sizeof(BinaryDataReader_t2404603355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3743[6] = 
{
	BinaryDataReader_t2404603355_StaticFields::get_offset_of_PrimitiveFromByteMethods_4(),
	BinaryDataReader_t2404603355::get_offset_of_buffer_5(),
	BinaryDataReader_t2404603355::get_offset_of_peekedEntryType_6(),
	BinaryDataReader_t2404603355::get_offset_of_peekedBinaryEntryType_7(),
	BinaryDataReader_t2404603355::get_offset_of_peekedEntryName_8(),
	BinaryDataReader_t2404603355::get_offset_of_types_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (U3CU3Ec_t630232976), -1, sizeof(U3CU3Ec_t630232976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3744[1] = 
{
	U3CU3Ec_t630232976_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (BinaryDataWriter_t769175716), -1, sizeof(BinaryDataWriter_t769175716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3745[4] = 
{
	BinaryDataWriter_t769175716_StaticFields::get_offset_of_PrimitiveGetBytesMethods_4(),
	BinaryDataWriter_t769175716_StaticFields::get_offset_of_PrimitiveSizes_5(),
	BinaryDataWriter_t769175716::get_offset_of_buffer_6(),
	BinaryDataWriter_t769175716::get_offset_of_types_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (U3CU3Ec_t4251451849), -1, sizeof(U3CU3Ec_t4251451849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3746[1] = 
{
	U3CU3Ec_t4251451849_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (BinaryEntryType_t3247007447)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3747[53] = 
{
	BinaryEntryType_t3247007447::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (JsonConfig_t1669950243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (JsonDataReader_t4095320770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[6] = 
{
	JsonDataReader_t4095320770::get_offset_of_reader_4(),
	JsonDataReader_t4095320770::get_offset_of_peekedEntryType_5(),
	JsonDataReader_t4095320770::get_offset_of_peekedEntryName_6(),
	JsonDataReader_t4095320770::get_offset_of_peekedEntryContent_7(),
	JsonDataReader_t4095320770::get_offset_of_seenTypes_8(),
	JsonDataReader_t4095320770::get_offset_of_primitiveArrayReaders_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (JsonDataWriter_t2586654915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[8] = 
{
	JsonDataWriter_t2586654915::get_offset_of_justStarted_4(),
	JsonDataWriter_t2586654915::get_offset_of_forceNoSeparatorNextLine_5(),
	JsonDataWriter_t2586654915::get_offset_of_escapeStringBuilder_6(),
	JsonDataWriter_t2586654915::get_offset_of_writer_7(),
	JsonDataWriter_t2586654915::get_offset_of_primitiveTypeWriters_8(),
	JsonDataWriter_t2586654915::get_offset_of_seenTypes_9(),
	JsonDataWriter_t2586654915::get_offset_of_U3CFormatAsReadableU3Ek__BackingField_10(),
	JsonDataWriter_t2586654915::get_offset_of_U3CEnableTypeOptimizationU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (JsonTextReader_t2625489622), -1, sizeof(JsonTextReader_t2625489622_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3753[9] = 
{
	JsonTextReader_t2625489622_StaticFields::get_offset_of_EntryDelineators_0(),
	JsonTextReader_t2625489622_StaticFields::get_offset_of_UnescapeDictionary_1(),
	JsonTextReader_t2625489622::get_offset_of_reader_2(),
	JsonTextReader_t2625489622::get_offset_of_bufferIndex_3(),
	JsonTextReader_t2625489622::get_offset_of_buffer_4(),
	JsonTextReader_t2625489622::get_offset_of_lastReadChar_5(),
	JsonTextReader_t2625489622::get_offset_of_peekedChar_6(),
	JsonTextReader_t2625489622::get_offset_of_emergencyPlayback_7(),
	JsonTextReader_t2625489622::get_offset_of_U3CContextU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (SerializationNode_t389653619)+ sizeof (RuntimeObject), sizeof(SerializationNode_t389653619_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3754[3] = 
{
	SerializationNode_t389653619::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationNode_t389653619::get_offset_of_Entry_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationNode_t389653619::get_offset_of_Data_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (SerializationNodeDataReader_t1897650348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[6] = 
{
	SerializationNodeDataReader_t1897650348::get_offset_of_peekedEntryName_4(),
	SerializationNodeDataReader_t1897650348::get_offset_of_peekedEntryType_5(),
	SerializationNodeDataReader_t1897650348::get_offset_of_peekedEntryData_6(),
	SerializationNodeDataReader_t1897650348::get_offset_of_currentIndex_7(),
	SerializationNodeDataReader_t1897650348::get_offset_of_nodes_8(),
	SerializationNodeDataReader_t1897650348::get_offset_of_primitiveTypeReaders_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (SerializationNodeDataReaderWriterConfig_t3461079505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (SerializationNodeDataWriter_t3531837839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[2] = 
{
	SerializationNodeDataWriter_t3531837839::get_offset_of_nodes_4(),
	SerializationNodeDataWriter_t3531837839::get_offset_of_primitiveTypeWriters_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (ArrayListFormatter_t2213559614), -1, sizeof(ArrayListFormatter_t2213559614_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3759[1] = 
{
	ArrayListFormatter_t2213559614_StaticFields::get_offset_of_ObjectSerializer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (DateTimeFormatter_t1343700026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (DateTimeOffsetFormatter_t344272922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3766[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3767[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3768[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (EmittedFormatterAttribute_t3475119202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (FormatterEmitter_t2521268614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3773[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (GenericCollectionFormatter_t29413212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3777[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3778[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3783[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3784[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3785[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3786[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3787[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3793[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (TimeSpanFormatter_t3901306678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (TypeFormatter_t3590974233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (CustomSerializationPolicy_t180435788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3796[3] = 
{
	CustomSerializationPolicy_t180435788::get_offset_of_id_0(),
	CustomSerializationPolicy_t180435788::get_offset_of_allowNonSerializableTypes_1(),
	CustomSerializationPolicy_t180435788::get_offset_of_shouldSerializeFunc_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (AlwaysFormatsSelfAttribute_t2404549866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (CustomFormatterAttribute_t1016321002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3798[1] = 
{
	CustomFormatterAttribute_t1016321002::get_offset_of_Priority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (CustomGenericFormatterAttribute_t4231702514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3799[1] = 
{
	CustomGenericFormatterAttribute_t4231702514::get_offset_of_SerializedGenericTypeDefinition_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
