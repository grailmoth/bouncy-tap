﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Sirenix.Serialization.DebugContext
struct DebugContext_t3396391800;
// Sirenix.Serialization.IExternalGuidReferenceResolver
struct IExternalGuidReferenceResolver_t89089555;
// Sirenix.Serialization.IExternalIndexReferenceResolver
struct IExternalIndexReferenceResolver_t833955781;
// Sirenix.Serialization.IExternalStringReferenceResolver
struct IExternalStringReferenceResolver_t2808018703;
// Sirenix.Serialization.IFormatterLocator
struct IFormatterLocator_t2941603356;
// Sirenix.Serialization.IFormatter`1<UnityEngine.Keyframe>
struct IFormatter_1_t1402656079;
// Sirenix.Serialization.ILogger
struct ILogger_t1896901244;
// Sirenix.Serialization.ISerializationPolicy
struct ISerializationPolicy_t4036202176;
// Sirenix.Serialization.SerializationConfig
struct SerializationConfig_t4249313805;
// Sirenix.Serialization.Serializer`1<System.Byte>
struct Serializer_1_t2811278198;
// Sirenix.Serialization.Serializer`1<System.Int32>
struct Serializer_1_t332960279;
// Sirenix.Serialization.Serializer`1<System.Object>
struct Serializer_1_t462120690;
// Sirenix.Serialization.Serializer`1<System.Single>
struct Serializer_1_t3074248596;
// Sirenix.Serialization.Serializer`1<UnityEngine.Color>
struct Serializer_1_t4232668146;
// Sirenix.Serialization.Serializer`1<UnityEngine.GradientAlphaKey[]>
struct Serializer_1_t3663464341;
// Sirenix.Serialization.Serializer`1<UnityEngine.GradientColorKey[]>
struct Serializer_1_t1515746270;
// Sirenix.Serialization.Serializer`1<UnityEngine.Keyframe[]>
struct Serializer_1_t2745506293;
// Sirenix.Serialization.Serializer`1<UnityEngine.Vector3>
struct Serializer_1_t1104327990;
// Sirenix.Serialization.Serializer`1<UnityEngine.WrapMode>
struct Serializer_1_t2407432524;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<Sirenix.Serialization.ISerializationPolicy,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>
struct DoubleLookupDictionary_3_t3967655779;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<Sirenix.Serialization.ISerializationPolicy,System.Type,System.Reflection.MemberInfo[]>
struct DoubleLookupDictionary_3_t2104492171;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter>
struct DoubleLookupDictionary_3_t16962639;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<Sirenix.Serialization.DataFormat,Sirenix.Serialization.IDataReader>
struct Dictionary_2_t1472822928;
// System.Collections.Generic.Dictionary`2<Sirenix.Serialization.DataFormat,Sirenix.Serialization.IDataWriter>
struct Dictionary_2_t647836079;
// System.Collections.Generic.Dictionary`2<Sirenix.Serialization.IDictionaryKeyPathProvider,System.String>
struct Dictionary_2_t2638760182;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.Object,System.String>
struct Dictionary_2_t3194856973;
// System.Collections.Generic.Dictionary`2<System.Reflection.MemberInfo,Sirenix.Serialization.Utilities.WeakValueGetter>
struct Dictionary_2_t2475076971;
// System.Collections.Generic.Dictionary`2<System.Reflection.MemberInfo,Sirenix.Serialization.Utilities.WeakValueSetter>
struct Dictionary_2_t2436541803;
// System.Collections.Generic.Dictionary`2<System.Reflection.MemberInfo,System.Boolean>
struct Dictionary_2_t4197816652;
// System.Collections.Generic.Dictionary`2<System.String,Sirenix.Serialization.IDictionaryKeyPathProvider>
struct Dictionary_2_t491616426;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.Assembly>
struct Dictionary_2_t3887689098;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;
// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.IDictionaryKeyPathProvider>
struct Dictionary_2_t3150707191;
// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.IFormatter>
struct Dictionary_2_t1386134988;
// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.Serializer>
struct Dictionary_2_t3407265638;
// System.Collections.Generic.Dictionary`2<System.Type,System.Boolean>
struct Dictionary_2_t2541635029;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t4291797753;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t633324528;
// System.Collections.Generic.Dictionary`2<UnityEngine.Object,System.Int32>
struct Dictionary_2_t2872194676;
// System.Collections.Generic.HashSet`1<System.Char>
struct HashSet_1_t2199409944;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t2916515228;
// System.Collections.Generic.List`1<Sirenix.Serialization.FormatterLocator/FormatterInfo>
struct List_1_t68755297;
// System.Collections.Generic.List`1<Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo>
struct List_1_t1843968280;
// System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode>
struct List_1_t1861728361;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t2103082695;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Comparison`1<Sirenix.Serialization.PrefabModification>
struct Comparison_1_t1847336727;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.Func`2<Sirenix.Serialization.SerializationNode,System.String>
struct Func_2_t596561144;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_t2217434578;
// System.Func`2<System.Reflection.MemberInfo,System.Reflection.MemberInfo>
struct Func_2_t1205181058;
// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_t3967597302;
// System.Func`2<System.Type,System.String>
struct Func_2_t2311415679;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t2171992254;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;
// UnityEngine.Object
struct Object_t631007953;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct Object_t631007953_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BASEDICTIONARYKEYPATHPROVIDER_1_T1761792157_H
#define BASEDICTIONARYKEYPATHPROVIDER_1_T1761792157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector2>
struct  BaseDictionaryKeyPathProvider_1_t1761792157  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDICTIONARYKEYPATHPROVIDER_1_T1761792157_H
#ifndef BASEDICTIONARYKEYPATHPROVIDER_1_T3327876098_H
#define BASEDICTIONARYKEYPATHPROVIDER_1_T3327876098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector3>
struct  BaseDictionaryKeyPathProvider_1_t3327876098  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDICTIONARYKEYPATHPROVIDER_1_T3327876098_H
#ifndef BASEDICTIONARYKEYPATHPROVIDER_1_T2924591571_H
#define BASEDICTIONARYKEYPATHPROVIDER_1_T2924591571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BaseDictionaryKeyPathProvider`1<UnityEngine.Vector4>
struct  BaseDictionaryKeyPathProvider_1_t2924591571  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDICTIONARYKEYPATHPROVIDER_1_T2924591571_H
#ifndef CACHEDMEMORYSTREAM_T1924712323_H
#define CACHEDMEMORYSTREAM_T1924712323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CachedMemoryStream
struct  CachedMemoryStream_t1924712323  : public RuntimeObject
{
public:
	// System.IO.MemoryStream Sirenix.Serialization.CachedMemoryStream::memoryStream
	MemoryStream_t94973147 * ___memoryStream_2;

public:
	inline static int32_t get_offset_of_memoryStream_2() { return static_cast<int32_t>(offsetof(CachedMemoryStream_t1924712323, ___memoryStream_2)); }
	inline MemoryStream_t94973147 * get_memoryStream_2() const { return ___memoryStream_2; }
	inline MemoryStream_t94973147 ** get_address_of_memoryStream_2() { return &___memoryStream_2; }
	inline void set_memoryStream_2(MemoryStream_t94973147 * value)
	{
		___memoryStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___memoryStream_2), value);
	}
};

struct CachedMemoryStream_t1924712323_StaticFields
{
public:
	// System.Int32 Sirenix.Serialization.CachedMemoryStream::InitialCapacity
	int32_t ___InitialCapacity_0;
	// System.Int32 Sirenix.Serialization.CachedMemoryStream::MaxCapacity
	int32_t ___MaxCapacity_1;

public:
	inline static int32_t get_offset_of_InitialCapacity_0() { return static_cast<int32_t>(offsetof(CachedMemoryStream_t1924712323_StaticFields, ___InitialCapacity_0)); }
	inline int32_t get_InitialCapacity_0() const { return ___InitialCapacity_0; }
	inline int32_t* get_address_of_InitialCapacity_0() { return &___InitialCapacity_0; }
	inline void set_InitialCapacity_0(int32_t value)
	{
		___InitialCapacity_0 = value;
	}

	inline static int32_t get_offset_of_MaxCapacity_1() { return static_cast<int32_t>(offsetof(CachedMemoryStream_t1924712323_StaticFields, ___MaxCapacity_1)); }
	inline int32_t get_MaxCapacity_1() const { return ___MaxCapacity_1; }
	inline int32_t* get_address_of_MaxCapacity_1() { return &___MaxCapacity_1; }
	inline void set_MaxCapacity_1(int32_t value)
	{
		___MaxCapacity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDMEMORYSTREAM_T1924712323_H
#ifndef COROUTINEFORMATTER_T1506639270_H
#define COROUTINEFORMATTER_T1506639270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CoroutineFormatter
struct  CoroutineFormatter_t1506639270  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINEFORMATTER_T1506639270_H
#ifndef DICTIONARYKEYUTILITY_T927095224_H
#define DICTIONARYKEYUTILITY_T927095224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DictionaryKeyUtility
struct  DictionaryKeyUtility_t927095224  : public RuntimeObject
{
public:

public:
};

struct DictionaryKeyUtility_t927095224_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Boolean> Sirenix.Serialization.DictionaryKeyUtility::GetSupportedDictionaryKeyTypesResults
	Dictionary_2_t2541635029 * ___GetSupportedDictionaryKeyTypesResults_0;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.DictionaryKeyUtility::BaseSupportedDictionaryKeyTypes
	HashSet_1_t1048894234 * ___BaseSupportedDictionaryKeyTypes_1;
	// System.Collections.Generic.HashSet`1<System.Char> Sirenix.Serialization.DictionaryKeyUtility::AllowedSpecialKeyStrChars
	HashSet_1_t2199409944 * ___AllowedSpecialKeyStrChars_2;
	// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.IDictionaryKeyPathProvider> Sirenix.Serialization.DictionaryKeyUtility::TypeToKeyPathProviders
	Dictionary_2_t3150707191 * ___TypeToKeyPathProviders_3;
	// System.Collections.Generic.Dictionary`2<System.String,Sirenix.Serialization.IDictionaryKeyPathProvider> Sirenix.Serialization.DictionaryKeyUtility::IDToKeyPathProviders
	Dictionary_2_t491616426 * ___IDToKeyPathProviders_4;
	// System.Collections.Generic.Dictionary`2<Sirenix.Serialization.IDictionaryKeyPathProvider,System.String> Sirenix.Serialization.DictionaryKeyUtility::ProviderToID
	Dictionary_2_t2638760182 * ___ProviderToID_5;
	// System.Collections.Generic.Dictionary`2<System.Object,System.String> Sirenix.Serialization.DictionaryKeyUtility::ObjectsToTempKeys
	Dictionary_2_t3194856973 * ___ObjectsToTempKeys_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Sirenix.Serialization.DictionaryKeyUtility::TempKeysToObjects
	Dictionary_2_t2865362463 * ___TempKeysToObjects_7;
	// System.Int64 Sirenix.Serialization.DictionaryKeyUtility::tempKeyCounter
	int64_t ___tempKeyCounter_8;

public:
	inline static int32_t get_offset_of_GetSupportedDictionaryKeyTypesResults_0() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___GetSupportedDictionaryKeyTypesResults_0)); }
	inline Dictionary_2_t2541635029 * get_GetSupportedDictionaryKeyTypesResults_0() const { return ___GetSupportedDictionaryKeyTypesResults_0; }
	inline Dictionary_2_t2541635029 ** get_address_of_GetSupportedDictionaryKeyTypesResults_0() { return &___GetSupportedDictionaryKeyTypesResults_0; }
	inline void set_GetSupportedDictionaryKeyTypesResults_0(Dictionary_2_t2541635029 * value)
	{
		___GetSupportedDictionaryKeyTypesResults_0 = value;
		Il2CppCodeGenWriteBarrier((&___GetSupportedDictionaryKeyTypesResults_0), value);
	}

	inline static int32_t get_offset_of_BaseSupportedDictionaryKeyTypes_1() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___BaseSupportedDictionaryKeyTypes_1)); }
	inline HashSet_1_t1048894234 * get_BaseSupportedDictionaryKeyTypes_1() const { return ___BaseSupportedDictionaryKeyTypes_1; }
	inline HashSet_1_t1048894234 ** get_address_of_BaseSupportedDictionaryKeyTypes_1() { return &___BaseSupportedDictionaryKeyTypes_1; }
	inline void set_BaseSupportedDictionaryKeyTypes_1(HashSet_1_t1048894234 * value)
	{
		___BaseSupportedDictionaryKeyTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___BaseSupportedDictionaryKeyTypes_1), value);
	}

	inline static int32_t get_offset_of_AllowedSpecialKeyStrChars_2() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___AllowedSpecialKeyStrChars_2)); }
	inline HashSet_1_t2199409944 * get_AllowedSpecialKeyStrChars_2() const { return ___AllowedSpecialKeyStrChars_2; }
	inline HashSet_1_t2199409944 ** get_address_of_AllowedSpecialKeyStrChars_2() { return &___AllowedSpecialKeyStrChars_2; }
	inline void set_AllowedSpecialKeyStrChars_2(HashSet_1_t2199409944 * value)
	{
		___AllowedSpecialKeyStrChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___AllowedSpecialKeyStrChars_2), value);
	}

	inline static int32_t get_offset_of_TypeToKeyPathProviders_3() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___TypeToKeyPathProviders_3)); }
	inline Dictionary_2_t3150707191 * get_TypeToKeyPathProviders_3() const { return ___TypeToKeyPathProviders_3; }
	inline Dictionary_2_t3150707191 ** get_address_of_TypeToKeyPathProviders_3() { return &___TypeToKeyPathProviders_3; }
	inline void set_TypeToKeyPathProviders_3(Dictionary_2_t3150707191 * value)
	{
		___TypeToKeyPathProviders_3 = value;
		Il2CppCodeGenWriteBarrier((&___TypeToKeyPathProviders_3), value);
	}

	inline static int32_t get_offset_of_IDToKeyPathProviders_4() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___IDToKeyPathProviders_4)); }
	inline Dictionary_2_t491616426 * get_IDToKeyPathProviders_4() const { return ___IDToKeyPathProviders_4; }
	inline Dictionary_2_t491616426 ** get_address_of_IDToKeyPathProviders_4() { return &___IDToKeyPathProviders_4; }
	inline void set_IDToKeyPathProviders_4(Dictionary_2_t491616426 * value)
	{
		___IDToKeyPathProviders_4 = value;
		Il2CppCodeGenWriteBarrier((&___IDToKeyPathProviders_4), value);
	}

	inline static int32_t get_offset_of_ProviderToID_5() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___ProviderToID_5)); }
	inline Dictionary_2_t2638760182 * get_ProviderToID_5() const { return ___ProviderToID_5; }
	inline Dictionary_2_t2638760182 ** get_address_of_ProviderToID_5() { return &___ProviderToID_5; }
	inline void set_ProviderToID_5(Dictionary_2_t2638760182 * value)
	{
		___ProviderToID_5 = value;
		Il2CppCodeGenWriteBarrier((&___ProviderToID_5), value);
	}

	inline static int32_t get_offset_of_ObjectsToTempKeys_6() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___ObjectsToTempKeys_6)); }
	inline Dictionary_2_t3194856973 * get_ObjectsToTempKeys_6() const { return ___ObjectsToTempKeys_6; }
	inline Dictionary_2_t3194856973 ** get_address_of_ObjectsToTempKeys_6() { return &___ObjectsToTempKeys_6; }
	inline void set_ObjectsToTempKeys_6(Dictionary_2_t3194856973 * value)
	{
		___ObjectsToTempKeys_6 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectsToTempKeys_6), value);
	}

	inline static int32_t get_offset_of_TempKeysToObjects_7() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___TempKeysToObjects_7)); }
	inline Dictionary_2_t2865362463 * get_TempKeysToObjects_7() const { return ___TempKeysToObjects_7; }
	inline Dictionary_2_t2865362463 ** get_address_of_TempKeysToObjects_7() { return &___TempKeysToObjects_7; }
	inline void set_TempKeysToObjects_7(Dictionary_2_t2865362463 * value)
	{
		___TempKeysToObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___TempKeysToObjects_7), value);
	}

	inline static int32_t get_offset_of_tempKeyCounter_8() { return static_cast<int32_t>(offsetof(DictionaryKeyUtility_t927095224_StaticFields, ___tempKeyCounter_8)); }
	inline int64_t get_tempKeyCounter_8() const { return ___tempKeyCounter_8; }
	inline int64_t* get_address_of_tempKeyCounter_8() { return &___tempKeyCounter_8; }
	inline void set_tempKeyCounter_8(int64_t value)
	{
		___tempKeyCounter_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYKEYUTILITY_T927095224_H
#ifndef U3CU3EC_T3635135514_H
#define U3CU3EC_T3635135514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DictionaryKeyUtility/<>c
struct  U3CU3Ec_t3635135514  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3635135514_StaticFields
{
public:
	// Sirenix.Serialization.DictionaryKeyUtility/<>c Sirenix.Serialization.DictionaryKeyUtility/<>c::<>9
	U3CU3Ec_t3635135514 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3635135514_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3635135514 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3635135514 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3635135514 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3635135514_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T1051472719_H
#define U3CU3EC__DISPLAYCLASS12_0_T1051472719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DictionaryKeyUtility/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t1051472719  : public RuntimeObject
{
public:
	// System.Reflection.Assembly Sirenix.Serialization.DictionaryKeyUtility/<>c__DisplayClass12_0::ass
	Assembly_t * ___ass_0;

public:
	inline static int32_t get_offset_of_ass_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t1051472719, ___ass_0)); }
	inline Assembly_t * get_ass_0() const { return ___ass_0; }
	inline Assembly_t ** get_address_of_ass_0() { return &___ass_0; }
	inline void set_ass_0(Assembly_t * value)
	{
		___ass_0 = value;
		Il2CppCodeGenWriteBarrier((&___ass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T1051472719_H
#ifndef FORMATTERLOCATOR_T3795817683_H
#define FORMATTERLOCATOR_T3795817683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterLocator
struct  FormatterLocator_t3795817683  : public RuntimeObject
{
public:

public:
};

struct FormatterLocator_t3795817683_StaticFields
{
public:
	// System.Object Sirenix.Serialization.FormatterLocator::LOCK
	RuntimeObject * ___LOCK_0;
	// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.IFormatter> Sirenix.Serialization.FormatterLocator::FormatterInstances
	Dictionary_2_t1386134988 * ___FormatterInstances_1;
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter> Sirenix.Serialization.FormatterLocator::TypeFormatterMap
	DoubleLookupDictionary_3_t16962639 * ___TypeFormatterMap_2;
	// System.Collections.Generic.List`1<Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo> Sirenix.Serialization.FormatterLocator::FormatterLocators
	List_1_t1843968280 * ___FormatterLocators_3;
	// System.Collections.Generic.List`1<Sirenix.Serialization.FormatterLocator/FormatterInfo> Sirenix.Serialization.FormatterLocator::FormatterInfos
	List_1_t68755297 * ___FormatterInfos_4;

public:
	inline static int32_t get_offset_of_LOCK_0() { return static_cast<int32_t>(offsetof(FormatterLocator_t3795817683_StaticFields, ___LOCK_0)); }
	inline RuntimeObject * get_LOCK_0() const { return ___LOCK_0; }
	inline RuntimeObject ** get_address_of_LOCK_0() { return &___LOCK_0; }
	inline void set_LOCK_0(RuntimeObject * value)
	{
		___LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_0), value);
	}

	inline static int32_t get_offset_of_FormatterInstances_1() { return static_cast<int32_t>(offsetof(FormatterLocator_t3795817683_StaticFields, ___FormatterInstances_1)); }
	inline Dictionary_2_t1386134988 * get_FormatterInstances_1() const { return ___FormatterInstances_1; }
	inline Dictionary_2_t1386134988 ** get_address_of_FormatterInstances_1() { return &___FormatterInstances_1; }
	inline void set_FormatterInstances_1(Dictionary_2_t1386134988 * value)
	{
		___FormatterInstances_1 = value;
		Il2CppCodeGenWriteBarrier((&___FormatterInstances_1), value);
	}

	inline static int32_t get_offset_of_TypeFormatterMap_2() { return static_cast<int32_t>(offsetof(FormatterLocator_t3795817683_StaticFields, ___TypeFormatterMap_2)); }
	inline DoubleLookupDictionary_3_t16962639 * get_TypeFormatterMap_2() const { return ___TypeFormatterMap_2; }
	inline DoubleLookupDictionary_3_t16962639 ** get_address_of_TypeFormatterMap_2() { return &___TypeFormatterMap_2; }
	inline void set_TypeFormatterMap_2(DoubleLookupDictionary_3_t16962639 * value)
	{
		___TypeFormatterMap_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeFormatterMap_2), value);
	}

	inline static int32_t get_offset_of_FormatterLocators_3() { return static_cast<int32_t>(offsetof(FormatterLocator_t3795817683_StaticFields, ___FormatterLocators_3)); }
	inline List_1_t1843968280 * get_FormatterLocators_3() const { return ___FormatterLocators_3; }
	inline List_1_t1843968280 ** get_address_of_FormatterLocators_3() { return &___FormatterLocators_3; }
	inline void set_FormatterLocators_3(List_1_t1843968280 * value)
	{
		___FormatterLocators_3 = value;
		Il2CppCodeGenWriteBarrier((&___FormatterLocators_3), value);
	}

	inline static int32_t get_offset_of_FormatterInfos_4() { return static_cast<int32_t>(offsetof(FormatterLocator_t3795817683_StaticFields, ___FormatterInfos_4)); }
	inline List_1_t68755297 * get_FormatterInfos_4() const { return ___FormatterInfos_4; }
	inline List_1_t68755297 ** get_address_of_FormatterInfos_4() { return &___FormatterInfos_4; }
	inline void set_FormatterInfos_4(List_1_t68755297 * value)
	{
		___FormatterInfos_4 = value;
		Il2CppCodeGenWriteBarrier((&___FormatterInfos_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERLOCATOR_T3795817683_H
#ifndef U3CU3EC_T3893634997_H
#define U3CU3EC_T3893634997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterLocator/<>c
struct  U3CU3Ec_t3893634997  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3893634997_StaticFields
{
public:
	// Sirenix.Serialization.FormatterLocator/<>c Sirenix.Serialization.FormatterLocator/<>c::<>9
	U3CU3Ec_t3893634997 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.String> Sirenix.Serialization.FormatterLocator/<>c::<>9__13_0
	Func_2_t2311415679 * ___U3CU3E9__13_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3893634997_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3893634997 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3893634997 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3893634997 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3893634997_StaticFields, ___U3CU3E9__13_0_1)); }
	inline Func_2_t2311415679 * get_U3CU3E9__13_0_1() const { return ___U3CU3E9__13_0_1; }
	inline Func_2_t2311415679 ** get_address_of_U3CU3E9__13_0_1() { return &___U3CU3E9__13_0_1; }
	inline void set_U3CU3E9__13_0_1(Func_2_t2311415679 * value)
	{
		___U3CU3E9__13_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3893634997_H
#ifndef U3CGETALLPOSSIBLEMISSINGAOTTYPESU3ED__14_T1172278837_H
#define U3CGETALLPOSSIBLEMISSINGAOTTYPESU3ED__14_T1172278837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14
struct  U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Type Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<arg>5__1
	Type_t * ___U3CargU3E5__1_5;
	// System.Type[] Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>7__wrap1
	TypeU5BU5D_t3940880105* ___U3CU3E7__wrap1_6;
	// System.Int32 Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_7;
	// System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>7__wrap3
	RuntimeObject* ___U3CU3E7__wrap3_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CargU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CargU3E5__1_5)); }
	inline Type_t * get_U3CargU3E5__1_5() const { return ___U3CargU3E5__1_5; }
	inline Type_t ** get_address_of_U3CargU3E5__1_5() { return &___U3CargU3E5__1_5; }
	inline void set_U3CargU3E5__1_5(Type_t * value)
	{
		___U3CargU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CargU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3E7__wrap1_6)); }
	inline TypeU5BU5D_t3940880105* get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(TypeU5BU5D_t3940880105* value)
	{
		___U3CU3E7__wrap1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_7() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3E7__wrap2_7)); }
	inline int32_t get_U3CU3E7__wrap2_7() const { return ___U3CU3E7__wrap2_7; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_7() { return &___U3CU3E7__wrap2_7; }
	inline void set_U3CU3E7__wrap2_7(int32_t value)
	{
		___U3CU3E7__wrap2_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_8() { return static_cast<int32_t>(offsetof(U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837, ___U3CU3E7__wrap3_8)); }
	inline RuntimeObject* get_U3CU3E7__wrap3_8() const { return ___U3CU3E7__wrap3_8; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap3_8() { return &___U3CU3E7__wrap3_8; }
	inline void set_U3CU3E7__wrap3_8(RuntimeObject* value)
	{
		___U3CU3E7__wrap3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLPOSSIBLEMISSINGAOTTYPESU3ED__14_T1172278837_H
#ifndef FORMATTERUTILITIES_T2227968676_H
#define FORMATTERUTILITIES_T2227968676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterUtilities
struct  FormatterUtilities_t2227968676  : public RuntimeObject
{
public:

public:
};

struct FormatterUtilities_t2227968676_StaticFields
{
public:
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<Sirenix.Serialization.ISerializationPolicy,System.Type,System.Reflection.MemberInfo[]> Sirenix.Serialization.FormatterUtilities::MemberArrayCache
	DoubleLookupDictionary_3_t2104492171 * ___MemberArrayCache_0;
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<Sirenix.Serialization.ISerializationPolicy,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> Sirenix.Serialization.FormatterUtilities::MemberMapCache
	DoubleLookupDictionary_3_t3967655779 * ___MemberMapCache_1;
	// System.Object Sirenix.Serialization.FormatterUtilities::LOCK
	RuntimeObject * ___LOCK_2;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.FormatterUtilities::PrimitiveArrayTypes
	HashSet_1_t1048894234 * ___PrimitiveArrayTypes_3;
	// System.Reflection.FieldInfo Sirenix.Serialization.FormatterUtilities::UnityObjectRuntimeErrorStringField
	FieldInfo_t * ___UnityObjectRuntimeErrorStringField_4;

public:
	inline static int32_t get_offset_of_MemberArrayCache_0() { return static_cast<int32_t>(offsetof(FormatterUtilities_t2227968676_StaticFields, ___MemberArrayCache_0)); }
	inline DoubleLookupDictionary_3_t2104492171 * get_MemberArrayCache_0() const { return ___MemberArrayCache_0; }
	inline DoubleLookupDictionary_3_t2104492171 ** get_address_of_MemberArrayCache_0() { return &___MemberArrayCache_0; }
	inline void set_MemberArrayCache_0(DoubleLookupDictionary_3_t2104492171 * value)
	{
		___MemberArrayCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberArrayCache_0), value);
	}

	inline static int32_t get_offset_of_MemberMapCache_1() { return static_cast<int32_t>(offsetof(FormatterUtilities_t2227968676_StaticFields, ___MemberMapCache_1)); }
	inline DoubleLookupDictionary_3_t3967655779 * get_MemberMapCache_1() const { return ___MemberMapCache_1; }
	inline DoubleLookupDictionary_3_t3967655779 ** get_address_of_MemberMapCache_1() { return &___MemberMapCache_1; }
	inline void set_MemberMapCache_1(DoubleLookupDictionary_3_t3967655779 * value)
	{
		___MemberMapCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___MemberMapCache_1), value);
	}

	inline static int32_t get_offset_of_LOCK_2() { return static_cast<int32_t>(offsetof(FormatterUtilities_t2227968676_StaticFields, ___LOCK_2)); }
	inline RuntimeObject * get_LOCK_2() const { return ___LOCK_2; }
	inline RuntimeObject ** get_address_of_LOCK_2() { return &___LOCK_2; }
	inline void set_LOCK_2(RuntimeObject * value)
	{
		___LOCK_2 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_2), value);
	}

	inline static int32_t get_offset_of_PrimitiveArrayTypes_3() { return static_cast<int32_t>(offsetof(FormatterUtilities_t2227968676_StaticFields, ___PrimitiveArrayTypes_3)); }
	inline HashSet_1_t1048894234 * get_PrimitiveArrayTypes_3() const { return ___PrimitiveArrayTypes_3; }
	inline HashSet_1_t1048894234 ** get_address_of_PrimitiveArrayTypes_3() { return &___PrimitiveArrayTypes_3; }
	inline void set_PrimitiveArrayTypes_3(HashSet_1_t1048894234 * value)
	{
		___PrimitiveArrayTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveArrayTypes_3), value);
	}

	inline static int32_t get_offset_of_UnityObjectRuntimeErrorStringField_4() { return static_cast<int32_t>(offsetof(FormatterUtilities_t2227968676_StaticFields, ___UnityObjectRuntimeErrorStringField_4)); }
	inline FieldInfo_t * get_UnityObjectRuntimeErrorStringField_4() const { return ___UnityObjectRuntimeErrorStringField_4; }
	inline FieldInfo_t ** get_address_of_UnityObjectRuntimeErrorStringField_4() { return &___UnityObjectRuntimeErrorStringField_4; }
	inline void set_UnityObjectRuntimeErrorStringField_4(FieldInfo_t * value)
	{
		___UnityObjectRuntimeErrorStringField_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityObjectRuntimeErrorStringField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERUTILITIES_T2227968676_H
#ifndef U3CU3EC_T1396354011_H
#define U3CU3EC_T1396354011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterUtilities/<>c
struct  U3CU3Ec_t1396354011  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1396354011_StaticFields
{
public:
	// Sirenix.Serialization.FormatterUtilities/<>c Sirenix.Serialization.FormatterUtilities/<>c::<>9
	U3CU3Ec_t1396354011 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.String> Sirenix.Serialization.FormatterUtilities/<>c::<>9__15_0
	Func_2_t3967597302 * ___U3CU3E9__15_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Reflection.MemberInfo> Sirenix.Serialization.FormatterUtilities/<>c::<>9__15_1
	Func_2_t1205181058 * ___U3CU3E9__15_1_2;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.FormatterUtilities/<>c::<>9__16_0
	Func_2_t2217434578 * ___U3CU3E9__16_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1396354011_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1396354011 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1396354011 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1396354011 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1396354011_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Func_2_t3967597302 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Func_2_t3967597302 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Func_2_t3967597302 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1396354011_StaticFields, ___U3CU3E9__15_1_2)); }
	inline Func_2_t1205181058 * get_U3CU3E9__15_1_2() const { return ___U3CU3E9__15_1_2; }
	inline Func_2_t1205181058 ** get_address_of_U3CU3E9__15_1_2() { return &___U3CU3E9__15_1_2; }
	inline void set_U3CU3E9__15_1_2(Func_2_t1205181058 * value)
	{
		___U3CU3E9__15_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1396354011_StaticFields, ___U3CU3E9__16_0_3)); }
	inline Func_2_t2217434578 * get_U3CU3E9__16_0_3() const { return ___U3CU3E9__16_0_3; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__16_0_3() { return &___U3CU3E9__16_0_3; }
	inline void set_U3CU3E9__16_0_3(Func_2_t2217434578 * value)
	{
		___U3CU3E9__16_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1396354011_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T3363221677_H
#define U3CU3EC__DISPLAYCLASS16_0_T3363221677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterUtilities/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t3363221677  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Sirenix.Serialization.FormatterUtilities/<>c__DisplayClass16_0::member
	MemberInfo_t * ___member_0;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t3363221677, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T3363221677_H
#ifndef MINIMALBASEFORMATTER_1_T56771448_H
#define MINIMALBASEFORMATTER_1_T56771448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.AnimationCurve>
struct  MinimalBaseFormatter_1_t56771448  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t56771448_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t56771448_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T56771448_H
#ifndef MINIMALBASEFORMATTER_1_T3571822288_H
#define MINIMALBASEFORMATTER_1_T3571822288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Bounds>
struct  MinimalBaseFormatter_1_t3571822288  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3571822288_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3571822288_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3571822288_H
#ifndef MINIMALBASEFORMATTER_1_T3905485670_H
#define MINIMALBASEFORMATTER_1_T3905485670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Color32>
struct  MinimalBaseFormatter_1_t3905485670  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3905485670_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3905485670_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3905485670_H
#ifndef MINIMALBASEFORMATTER_1_T3860670702_H
#define MINIMALBASEFORMATTER_1_T3860670702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Color>
struct  MinimalBaseFormatter_1_t3860670702  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3860670702_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3860670702_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3860670702_H
#ifndef MINIMALBASEFORMATTER_1_T77117006_H
#define MINIMALBASEFORMATTER_1_T77117006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Gradient>
struct  MinimalBaseFormatter_1_t77117006  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t77117006_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t77117006_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T77117006_H
#ifndef MINIMALBASEFORMATTER_1_T3929727004_H
#define MINIMALBASEFORMATTER_1_T3929727004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.GradientAlphaKey>
struct  MinimalBaseFormatter_1_t3929727004  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3929727004_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3929727004_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3929727004_H
#ifndef MINIMALBASEFORMATTER_1_T2117536967_H
#define MINIMALBASEFORMATTER_1_T2117536967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.GradientColorKey>
struct  MinimalBaseFormatter_1_t2117536967  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t2117536967_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t2117536967_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T2117536967_H
#ifndef MINIMALBASEFORMATTER_1_T1216427324_H
#define MINIMALBASEFORMATTER_1_T1216427324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Keyframe>
struct  MinimalBaseFormatter_1_t1216427324  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t1216427324_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t1216427324_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T1216427324_H
#ifndef MINIMALBASEFORMATTER_1_T503952000_H
#define MINIMALBASEFORMATTER_1_T503952000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.LayerMask>
struct  MinimalBaseFormatter_1_t503952000  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t503952000_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t503952000_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T503952000_H
#ifndef MINIMALBASEFORMATTER_1_T3606912709_H
#define MINIMALBASEFORMATTER_1_T3606912709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Quaternion>
struct  MinimalBaseFormatter_1_t3606912709  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3606912709_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3606912709_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3606912709_H
#ifndef MINIMALBASEFORMATTER_1_T3665464237_H
#define MINIMALBASEFORMATTER_1_T3665464237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Rect>
struct  MinimalBaseFormatter_1_t3665464237  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3665464237_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3665464237_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3665464237_H
#ifndef MINIMALBASEFORMATTER_1_T3444015952_H
#define MINIMALBASEFORMATTER_1_T3444015952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.UI.ColorBlock>
struct  MinimalBaseFormatter_1_t3444015952  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3444015952_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3444015952_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3444015952_H
#ifndef MINIMALBASEFORMATTER_1_T3461213901_H
#define MINIMALBASEFORMATTER_1_T3461213901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector2>
struct  MinimalBaseFormatter_1_t3461213901  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t3461213901_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t3461213901_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T3461213901_H
#ifndef MINIMALBASEFORMATTER_1_T732330546_H
#define MINIMALBASEFORMATTER_1_T732330546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector3>
struct  MinimalBaseFormatter_1_t732330546  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t732330546_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t732330546_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T732330546_H
#ifndef MINIMALBASEFORMATTER_1_T329046019_H
#define MINIMALBASEFORMATTER_1_T329046019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector4>
struct  MinimalBaseFormatter_1_t329046019  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t329046019_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t329046019_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T329046019_H
#ifndef U3CU3EC_T2854026858_H
#define U3CU3EC_T2854026858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.PrefabModification/<>c
struct  U3CU3Ec_t2854026858  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2854026858_StaticFields
{
public:
	// Sirenix.Serialization.PrefabModification/<>c Sirenix.Serialization.PrefabModification/<>c::<>9
	U3CU3Ec_t2854026858 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.PrefabModification/<>c::<>9__13_0
	Func_2_t2217434578 * ___U3CU3E9__13_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.PrefabModification/<>c::<>9__16_0
	Func_2_t2217434578 * ___U3CU3E9__16_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2854026858_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2854026858 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2854026858 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2854026858 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2854026858_StaticFields, ___U3CU3E9__13_0_1)); }
	inline Func_2_t2217434578 * get_U3CU3E9__13_0_1() const { return ___U3CU3E9__13_0_1; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__13_0_1() { return &___U3CU3E9__13_0_1; }
	inline void set_U3CU3E9__13_0_1(Func_2_t2217434578 * value)
	{
		___U3CU3E9__13_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2854026858_StaticFields, ___U3CU3E9__16_0_2)); }
	inline Func_2_t2217434578 * get_U3CU3E9__16_0_2() const { return ___U3CU3E9__16_0_2; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__16_0_2() { return &___U3CU3E9__16_0_2; }
	inline void set_U3CU3E9__16_0_2(Func_2_t2217434578 * value)
	{
		___U3CU3E9__16_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2854026858_H
#ifndef PROPERBITCONVERTER_T1060665852_H
#define PROPERBITCONVERTER_T1060665852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ProperBitConverter
struct  ProperBitConverter_t1060665852  : public RuntimeObject
{
public:

public:
};

struct ProperBitConverter_t1060665852_StaticFields
{
public:
	// System.UInt32[] Sirenix.Serialization.ProperBitConverter::ByteToHexCharLookupLowerCase
	UInt32U5BU5D_t2770800703* ___ByteToHexCharLookupLowerCase_0;
	// System.UInt32[] Sirenix.Serialization.ProperBitConverter::ByteToHexCharLookupUpperCase
	UInt32U5BU5D_t2770800703* ___ByteToHexCharLookupUpperCase_1;
	// System.Byte[] Sirenix.Serialization.ProperBitConverter::HexToByteLookup
	ByteU5BU5D_t4116647657* ___HexToByteLookup_2;

public:
	inline static int32_t get_offset_of_ByteToHexCharLookupLowerCase_0() { return static_cast<int32_t>(offsetof(ProperBitConverter_t1060665852_StaticFields, ___ByteToHexCharLookupLowerCase_0)); }
	inline UInt32U5BU5D_t2770800703* get_ByteToHexCharLookupLowerCase_0() const { return ___ByteToHexCharLookupLowerCase_0; }
	inline UInt32U5BU5D_t2770800703** get_address_of_ByteToHexCharLookupLowerCase_0() { return &___ByteToHexCharLookupLowerCase_0; }
	inline void set_ByteToHexCharLookupLowerCase_0(UInt32U5BU5D_t2770800703* value)
	{
		___ByteToHexCharLookupLowerCase_0 = value;
		Il2CppCodeGenWriteBarrier((&___ByteToHexCharLookupLowerCase_0), value);
	}

	inline static int32_t get_offset_of_ByteToHexCharLookupUpperCase_1() { return static_cast<int32_t>(offsetof(ProperBitConverter_t1060665852_StaticFields, ___ByteToHexCharLookupUpperCase_1)); }
	inline UInt32U5BU5D_t2770800703* get_ByteToHexCharLookupUpperCase_1() const { return ___ByteToHexCharLookupUpperCase_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_ByteToHexCharLookupUpperCase_1() { return &___ByteToHexCharLookupUpperCase_1; }
	inline void set_ByteToHexCharLookupUpperCase_1(UInt32U5BU5D_t2770800703* value)
	{
		___ByteToHexCharLookupUpperCase_1 = value;
		Il2CppCodeGenWriteBarrier((&___ByteToHexCharLookupUpperCase_1), value);
	}

	inline static int32_t get_offset_of_HexToByteLookup_2() { return static_cast<int32_t>(offsetof(ProperBitConverter_t1060665852_StaticFields, ___HexToByteLookup_2)); }
	inline ByteU5BU5D_t4116647657* get_HexToByteLookup_2() const { return ___HexToByteLookup_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_HexToByteLookup_2() { return &___HexToByteLookup_2; }
	inline void set_HexToByteLookup_2(ByteU5BU5D_t4116647657* value)
	{
		___HexToByteLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___HexToByteLookup_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERBITCONVERTER_T1060665852_H
#ifndef SERIALIZATIONCONFIG_T4249313805_H
#define SERIALIZATIONCONFIG_T4249313805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationConfig
struct  SerializationConfig_t4249313805  : public RuntimeObject
{
public:
	// System.Object Sirenix.Serialization.SerializationConfig::LOCK
	RuntimeObject * ___LOCK_0;
	// Sirenix.Serialization.ISerializationPolicy modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.SerializationConfig::serializationPolicy
	RuntimeObject* ___serializationPolicy_1;
	// Sirenix.Serialization.DebugContext modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.SerializationConfig::debugContext
	DebugContext_t3396391800 * ___debugContext_2;

public:
	inline static int32_t get_offset_of_LOCK_0() { return static_cast<int32_t>(offsetof(SerializationConfig_t4249313805, ___LOCK_0)); }
	inline RuntimeObject * get_LOCK_0() const { return ___LOCK_0; }
	inline RuntimeObject ** get_address_of_LOCK_0() { return &___LOCK_0; }
	inline void set_LOCK_0(RuntimeObject * value)
	{
		___LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_0), value);
	}

	inline static int32_t get_offset_of_serializationPolicy_1() { return static_cast<int32_t>(offsetof(SerializationConfig_t4249313805, ___serializationPolicy_1)); }
	inline RuntimeObject* get_serializationPolicy_1() const { return ___serializationPolicy_1; }
	inline RuntimeObject** get_address_of_serializationPolicy_1() { return &___serializationPolicy_1; }
	inline void set_serializationPolicy_1(RuntimeObject* value)
	{
		___serializationPolicy_1 = value;
		Il2CppCodeGenWriteBarrier((&___serializationPolicy_1), value);
	}

	inline static int32_t get_offset_of_debugContext_2() { return static_cast<int32_t>(offsetof(SerializationConfig_t4249313805, ___debugContext_2)); }
	inline DebugContext_t3396391800 * get_debugContext_2() const { return ___debugContext_2; }
	inline DebugContext_t3396391800 ** get_address_of_debugContext_2() { return &___debugContext_2; }
	inline void set_debugContext_2(DebugContext_t3396391800 * value)
	{
		___debugContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONCONFIG_T4249313805_H
#ifndef SERIALIZATIONPOLICIES_T1787867284_H
#define SERIALIZATIONPOLICIES_T1787867284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationPolicies
struct  SerializationPolicies_t1787867284  : public RuntimeObject
{
public:

public:
};

struct SerializationPolicies_t1787867284_StaticFields
{
public:
	// System.Object Sirenix.Serialization.SerializationPolicies::LOCK
	RuntimeObject * ___LOCK_0;
	// Sirenix.Serialization.ISerializationPolicy modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.SerializationPolicies::everythingPolicy
	RuntimeObject* ___everythingPolicy_1;
	// Sirenix.Serialization.ISerializationPolicy modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.SerializationPolicies::unityPolicy
	RuntimeObject* ___unityPolicy_2;
	// Sirenix.Serialization.ISerializationPolicy modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.SerializationPolicies::strictPolicy
	RuntimeObject* ___strictPolicy_3;

public:
	inline static int32_t get_offset_of_LOCK_0() { return static_cast<int32_t>(offsetof(SerializationPolicies_t1787867284_StaticFields, ___LOCK_0)); }
	inline RuntimeObject * get_LOCK_0() const { return ___LOCK_0; }
	inline RuntimeObject ** get_address_of_LOCK_0() { return &___LOCK_0; }
	inline void set_LOCK_0(RuntimeObject * value)
	{
		___LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_0), value);
	}

	inline static int32_t get_offset_of_everythingPolicy_1() { return static_cast<int32_t>(offsetof(SerializationPolicies_t1787867284_StaticFields, ___everythingPolicy_1)); }
	inline RuntimeObject* get_everythingPolicy_1() const { return ___everythingPolicy_1; }
	inline RuntimeObject** get_address_of_everythingPolicy_1() { return &___everythingPolicy_1; }
	inline void set_everythingPolicy_1(RuntimeObject* value)
	{
		___everythingPolicy_1 = value;
		Il2CppCodeGenWriteBarrier((&___everythingPolicy_1), value);
	}

	inline static int32_t get_offset_of_unityPolicy_2() { return static_cast<int32_t>(offsetof(SerializationPolicies_t1787867284_StaticFields, ___unityPolicy_2)); }
	inline RuntimeObject* get_unityPolicy_2() const { return ___unityPolicy_2; }
	inline RuntimeObject** get_address_of_unityPolicy_2() { return &___unityPolicy_2; }
	inline void set_unityPolicy_2(RuntimeObject* value)
	{
		___unityPolicy_2 = value;
		Il2CppCodeGenWriteBarrier((&___unityPolicy_2), value);
	}

	inline static int32_t get_offset_of_strictPolicy_3() { return static_cast<int32_t>(offsetof(SerializationPolicies_t1787867284_StaticFields, ___strictPolicy_3)); }
	inline RuntimeObject* get_strictPolicy_3() const { return ___strictPolicy_3; }
	inline RuntimeObject** get_address_of_strictPolicy_3() { return &___strictPolicy_3; }
	inline void set_strictPolicy_3(RuntimeObject* value)
	{
		___strictPolicy_3 = value;
		Il2CppCodeGenWriteBarrier((&___strictPolicy_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPOLICIES_T1787867284_H
#ifndef U3CU3EC_T2078298886_H
#define U3CU3EC_T2078298886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationPolicies/<>c
struct  U3CU3Ec_t2078298886  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2078298886_StaticFields
{
public:
	// Sirenix.Serialization.SerializationPolicies/<>c Sirenix.Serialization.SerializationPolicies/<>c::<>9
	U3CU3Ec_t2078298886 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.SerializationPolicies/<>c::<>9__6_0
	Func_2_t2217434578 * ___U3CU3E9__6_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.SerializationPolicies/<>c::<>9__10_0
	Func_2_t2217434578 * ___U3CU3E9__10_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2078298886_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2078298886 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2078298886 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2078298886 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2078298886_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t2217434578 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t2217434578 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2078298886_StaticFields, ___U3CU3E9__10_0_2)); }
	inline Func_2_t2217434578 * get_U3CU3E9__10_0_2() const { return ___U3CU3E9__10_0_2; }
	inline Func_2_t2217434578 ** get_address_of_U3CU3E9__10_0_2() { return &___U3CU3E9__10_0_2; }
	inline void set_U3CU3E9__10_0_2(Func_2_t2217434578 * value)
	{
		___U3CU3E9__10_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2078298886_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T478529023_H
#define U3CU3EC__DISPLAYCLASS8_0_T478529023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationPolicies/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t478529023  : public RuntimeObject
{
public:
	// System.Type Sirenix.Serialization.SerializationPolicies/<>c__DisplayClass8_0::tupleInterface
	Type_t * ___tupleInterface_0;

public:
	inline static int32_t get_offset_of_tupleInterface_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t478529023, ___tupleInterface_0)); }
	inline Type_t * get_tupleInterface_0() const { return ___tupleInterface_0; }
	inline Type_t ** get_address_of_tupleInterface_0() { return &___tupleInterface_0; }
	inline void set_tupleInterface_0(Type_t * value)
	{
		___tupleInterface_0 = value;
		Il2CppCodeGenWriteBarrier((&___tupleInterface_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T478529023_H
#ifndef SERIALIZATIONUTILITY_T407367459_H
#define SERIALIZATIONUTILITY_T407367459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationUtility
struct  SerializationUtility_t407367459  : public RuntimeObject
{
public:

public:
};

struct SerializationUtility_t407367459_StaticFields
{
public:
	// System.Object Sirenix.Serialization.SerializationUtility::CACHE_LOCK
	RuntimeObject * ___CACHE_LOCK_0;
	// System.Collections.Generic.Dictionary`2<System.Object,System.Object> Sirenix.Serialization.SerializationUtility::CacheMappings
	Dictionary_2_t132545152 * ___CacheMappings_1;

public:
	inline static int32_t get_offset_of_CACHE_LOCK_0() { return static_cast<int32_t>(offsetof(SerializationUtility_t407367459_StaticFields, ___CACHE_LOCK_0)); }
	inline RuntimeObject * get_CACHE_LOCK_0() const { return ___CACHE_LOCK_0; }
	inline RuntimeObject ** get_address_of_CACHE_LOCK_0() { return &___CACHE_LOCK_0; }
	inline void set_CACHE_LOCK_0(RuntimeObject * value)
	{
		___CACHE_LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___CACHE_LOCK_0), value);
	}

	inline static int32_t get_offset_of_CacheMappings_1() { return static_cast<int32_t>(offsetof(SerializationUtility_t407367459_StaticFields, ___CacheMappings_1)); }
	inline Dictionary_2_t132545152 * get_CacheMappings_1() const { return ___CacheMappings_1; }
	inline Dictionary_2_t132545152 ** get_address_of_CacheMappings_1() { return &___CacheMappings_1; }
	inline void set_CacheMappings_1(Dictionary_2_t132545152 * value)
	{
		___CacheMappings_1 = value;
		Il2CppCodeGenWriteBarrier((&___CacheMappings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONUTILITY_T407367459_H
#ifndef SERIALIZER_T962918574_H
#define SERIALIZER_T962918574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer
struct  Serializer_t962918574  : public RuntimeObject
{
public:

public:
};

struct Serializer_t962918574_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Serialization.Serializer::PrimitiveReaderWriterTypes
	Dictionary_2_t633324528 * ___PrimitiveReaderWriterTypes_0;
	// System.Object Sirenix.Serialization.Serializer::LOCK
	RuntimeObject * ___LOCK_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Sirenix.Serialization.Serializer> Sirenix.Serialization.Serializer::ReaderWriterCache
	Dictionary_2_t3407265638 * ___ReaderWriterCache_2;

public:
	inline static int32_t get_offset_of_PrimitiveReaderWriterTypes_0() { return static_cast<int32_t>(offsetof(Serializer_t962918574_StaticFields, ___PrimitiveReaderWriterTypes_0)); }
	inline Dictionary_2_t633324528 * get_PrimitiveReaderWriterTypes_0() const { return ___PrimitiveReaderWriterTypes_0; }
	inline Dictionary_2_t633324528 ** get_address_of_PrimitiveReaderWriterTypes_0() { return &___PrimitiveReaderWriterTypes_0; }
	inline void set_PrimitiveReaderWriterTypes_0(Dictionary_2_t633324528 * value)
	{
		___PrimitiveReaderWriterTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveReaderWriterTypes_0), value);
	}

	inline static int32_t get_offset_of_LOCK_1() { return static_cast<int32_t>(offsetof(Serializer_t962918574_StaticFields, ___LOCK_1)); }
	inline RuntimeObject * get_LOCK_1() const { return ___LOCK_1; }
	inline RuntimeObject ** get_address_of_LOCK_1() { return &___LOCK_1; }
	inline void set_LOCK_1(RuntimeObject * value)
	{
		___LOCK_1 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_1), value);
	}

	inline static int32_t get_offset_of_ReaderWriterCache_2() { return static_cast<int32_t>(offsetof(Serializer_t962918574_StaticFields, ___ReaderWriterCache_2)); }
	inline Dictionary_2_t3407265638 * get_ReaderWriterCache_2() const { return ___ReaderWriterCache_2; }
	inline Dictionary_2_t3407265638 ** get_address_of_ReaderWriterCache_2() { return &___ReaderWriterCache_2; }
	inline void set_ReaderWriterCache_2(Dictionary_2_t3407265638 * value)
	{
		___ReaderWriterCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___ReaderWriterCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T962918574_H
#ifndef TWOWAYSERIALIZATIONBINDER_T4250048501_H
#define TWOWAYSERIALIZATIONBINDER_T4250048501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.TwoWaySerializationBinder
struct  TwoWaySerializationBinder_t4250048501  : public RuntimeObject
{
public:

public:
};

struct TwoWaySerializationBinder_t4250048501_StaticFields
{
public:
	// Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.TwoWaySerializationBinder::Default
	TwoWaySerializationBinder_t4250048501 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(TwoWaySerializationBinder_t4250048501_StaticFields, ___Default_0)); }
	inline TwoWaySerializationBinder_t4250048501 * get_Default_0() const { return ___Default_0; }
	inline TwoWaySerializationBinder_t4250048501 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(TwoWaySerializationBinder_t4250048501 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOWAYSERIALIZATIONBINDER_T4250048501_H
#ifndef UNITYREFERENCERESOLVER_T2497123750_H
#define UNITYREFERENCERESOLVER_T2497123750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UnityReferenceResolver
struct  UnityReferenceResolver_t2497123750  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Object,System.Int32> Sirenix.Serialization.UnityReferenceResolver::referenceIndexMapping
	Dictionary_2_t2872194676 * ___referenceIndexMapping_0;
	// System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.UnityReferenceResolver::referencedUnityObjects
	List_1_t2103082695 * ___referencedUnityObjects_1;

public:
	inline static int32_t get_offset_of_referenceIndexMapping_0() { return static_cast<int32_t>(offsetof(UnityReferenceResolver_t2497123750, ___referenceIndexMapping_0)); }
	inline Dictionary_2_t2872194676 * get_referenceIndexMapping_0() const { return ___referenceIndexMapping_0; }
	inline Dictionary_2_t2872194676 ** get_address_of_referenceIndexMapping_0() { return &___referenceIndexMapping_0; }
	inline void set_referenceIndexMapping_0(Dictionary_2_t2872194676 * value)
	{
		___referenceIndexMapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___referenceIndexMapping_0), value);
	}

	inline static int32_t get_offset_of_referencedUnityObjects_1() { return static_cast<int32_t>(offsetof(UnityReferenceResolver_t2497123750, ___referencedUnityObjects_1)); }
	inline List_1_t2103082695 * get_referencedUnityObjects_1() const { return ___referencedUnityObjects_1; }
	inline List_1_t2103082695 ** get_address_of_referencedUnityObjects_1() { return &___referencedUnityObjects_1; }
	inline void set_referencedUnityObjects_1(List_1_t2103082695 * value)
	{
		___referencedUnityObjects_1 = value;
		Il2CppCodeGenWriteBarrier((&___referencedUnityObjects_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREFERENCERESOLVER_T2497123750_H
#ifndef UNITYSERIALIZATIONINITIALIZER_T3556939650_H
#define UNITYSERIALIZATIONINITIALIZER_T3556939650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UnitySerializationInitializer
struct  UnitySerializationInitializer_t3556939650  : public RuntimeObject
{
public:

public:
};

struct UnitySerializationInitializer_t3556939650_StaticFields
{
public:
	// System.Object Sirenix.Serialization.UnitySerializationInitializer::LOCK
	RuntimeObject * ___LOCK_0;
	// System.Boolean Sirenix.Serialization.UnitySerializationInitializer::initialized
	bool ___initialized_1;

public:
	inline static int32_t get_offset_of_LOCK_0() { return static_cast<int32_t>(offsetof(UnitySerializationInitializer_t3556939650_StaticFields, ___LOCK_0)); }
	inline RuntimeObject * get_LOCK_0() const { return ___LOCK_0; }
	inline RuntimeObject ** get_address_of_LOCK_0() { return &___LOCK_0; }
	inline void set_LOCK_0(RuntimeObject * value)
	{
		___LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(UnitySerializationInitializer_t3556939650_StaticFields, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSERIALIZATIONINITIALIZER_T3556939650_H
#ifndef UNITYSERIALIZATIONUTILITY_T2251762831_H
#define UNITYSERIALIZATIONUTILITY_T2251762831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UnitySerializationUtility
struct  UnitySerializationUtility_t2251762831  : public RuntimeObject
{
public:

public:
};

struct UnitySerializationUtility_t2251762831_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Sirenix.Serialization.DataFormat,Sirenix.Serialization.IDataReader> Sirenix.Serialization.UnitySerializationUtility::UnityReaders
	Dictionary_2_t1472822928 * ___UnityReaders_0;
	// System.Collections.Generic.Dictionary`2<Sirenix.Serialization.DataFormat,Sirenix.Serialization.IDataWriter> Sirenix.Serialization.UnitySerializationUtility::UnityWriters
	Dictionary_2_t647836079 * ___UnityWriters_1;
	// System.Collections.Generic.Dictionary`2<System.Reflection.MemberInfo,Sirenix.Serialization.Utilities.WeakValueGetter> Sirenix.Serialization.UnitySerializationUtility::UnityMemberGetters
	Dictionary_2_t2475076971 * ___UnityMemberGetters_2;
	// System.Collections.Generic.Dictionary`2<System.Reflection.MemberInfo,Sirenix.Serialization.Utilities.WeakValueSetter> Sirenix.Serialization.UnitySerializationUtility::UnityMemberSetters
	Dictionary_2_t2436541803 * ___UnityMemberSetters_3;
	// System.Collections.Generic.Dictionary`2<System.Reflection.MemberInfo,System.Boolean> Sirenix.Serialization.UnitySerializationUtility::UnityWillSerializeMembersCache
	Dictionary_2_t4197816652 * ___UnityWillSerializeMembersCache_4;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Boolean> Sirenix.Serialization.UnitySerializationUtility::UnityWillSerializeTypesCache
	Dictionary_2_t2541635029 * ___UnityWillSerializeTypesCache_5;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.UnitySerializationUtility::UnityNeverSerializesTypes
	HashSet_1_t1048894234 * ___UnityNeverSerializesTypes_6;

public:
	inline static int32_t get_offset_of_UnityReaders_0() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityReaders_0)); }
	inline Dictionary_2_t1472822928 * get_UnityReaders_0() const { return ___UnityReaders_0; }
	inline Dictionary_2_t1472822928 ** get_address_of_UnityReaders_0() { return &___UnityReaders_0; }
	inline void set_UnityReaders_0(Dictionary_2_t1472822928 * value)
	{
		___UnityReaders_0 = value;
		Il2CppCodeGenWriteBarrier((&___UnityReaders_0), value);
	}

	inline static int32_t get_offset_of_UnityWriters_1() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityWriters_1)); }
	inline Dictionary_2_t647836079 * get_UnityWriters_1() const { return ___UnityWriters_1; }
	inline Dictionary_2_t647836079 ** get_address_of_UnityWriters_1() { return &___UnityWriters_1; }
	inline void set_UnityWriters_1(Dictionary_2_t647836079 * value)
	{
		___UnityWriters_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnityWriters_1), value);
	}

	inline static int32_t get_offset_of_UnityMemberGetters_2() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityMemberGetters_2)); }
	inline Dictionary_2_t2475076971 * get_UnityMemberGetters_2() const { return ___UnityMemberGetters_2; }
	inline Dictionary_2_t2475076971 ** get_address_of_UnityMemberGetters_2() { return &___UnityMemberGetters_2; }
	inline void set_UnityMemberGetters_2(Dictionary_2_t2475076971 * value)
	{
		___UnityMemberGetters_2 = value;
		Il2CppCodeGenWriteBarrier((&___UnityMemberGetters_2), value);
	}

	inline static int32_t get_offset_of_UnityMemberSetters_3() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityMemberSetters_3)); }
	inline Dictionary_2_t2436541803 * get_UnityMemberSetters_3() const { return ___UnityMemberSetters_3; }
	inline Dictionary_2_t2436541803 ** get_address_of_UnityMemberSetters_3() { return &___UnityMemberSetters_3; }
	inline void set_UnityMemberSetters_3(Dictionary_2_t2436541803 * value)
	{
		___UnityMemberSetters_3 = value;
		Il2CppCodeGenWriteBarrier((&___UnityMemberSetters_3), value);
	}

	inline static int32_t get_offset_of_UnityWillSerializeMembersCache_4() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityWillSerializeMembersCache_4)); }
	inline Dictionary_2_t4197816652 * get_UnityWillSerializeMembersCache_4() const { return ___UnityWillSerializeMembersCache_4; }
	inline Dictionary_2_t4197816652 ** get_address_of_UnityWillSerializeMembersCache_4() { return &___UnityWillSerializeMembersCache_4; }
	inline void set_UnityWillSerializeMembersCache_4(Dictionary_2_t4197816652 * value)
	{
		___UnityWillSerializeMembersCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityWillSerializeMembersCache_4), value);
	}

	inline static int32_t get_offset_of_UnityWillSerializeTypesCache_5() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityWillSerializeTypesCache_5)); }
	inline Dictionary_2_t2541635029 * get_UnityWillSerializeTypesCache_5() const { return ___UnityWillSerializeTypesCache_5; }
	inline Dictionary_2_t2541635029 ** get_address_of_UnityWillSerializeTypesCache_5() { return &___UnityWillSerializeTypesCache_5; }
	inline void set_UnityWillSerializeTypesCache_5(Dictionary_2_t2541635029 * value)
	{
		___UnityWillSerializeTypesCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___UnityWillSerializeTypesCache_5), value);
	}

	inline static int32_t get_offset_of_UnityNeverSerializesTypes_6() { return static_cast<int32_t>(offsetof(UnitySerializationUtility_t2251762831_StaticFields, ___UnityNeverSerializesTypes_6)); }
	inline HashSet_1_t1048894234 * get_UnityNeverSerializesTypes_6() const { return ___UnityNeverSerializesTypes_6; }
	inline HashSet_1_t1048894234 ** get_address_of_UnityNeverSerializesTypes_6() { return &___UnityNeverSerializesTypes_6; }
	inline void set_UnityNeverSerializesTypes_6(HashSet_1_t1048894234 * value)
	{
		___UnityNeverSerializesTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___UnityNeverSerializesTypes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSERIALIZATIONUTILITY_T2251762831_H
#ifndef U3CU3EC_T1820010292_H
#define U3CU3EC_T1820010292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UnitySerializationUtility/<>c
struct  U3CU3Ec_t1820010292  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1820010292_StaticFields
{
public:
	// Sirenix.Serialization.UnitySerializationUtility/<>c Sirenix.Serialization.UnitySerializationUtility/<>c::<>9
	U3CU3Ec_t1820010292 * ___U3CU3E9_0;
	// System.Func`2<Sirenix.Serialization.SerializationNode,System.String> Sirenix.Serialization.UnitySerializationUtility/<>c::<>9__20_0
	Func_2_t596561144 * ___U3CU3E9__20_0_1;
	// System.Comparison`1<Sirenix.Serialization.PrefabModification> Sirenix.Serialization.UnitySerializationUtility/<>c::<>9__21_0
	Comparison_1_t1847336727 * ___U3CU3E9__21_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1820010292_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1820010292 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1820010292 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1820010292 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1820010292_StaticFields, ___U3CU3E9__20_0_1)); }
	inline Func_2_t596561144 * get_U3CU3E9__20_0_1() const { return ___U3CU3E9__20_0_1; }
	inline Func_2_t596561144 ** get_address_of_U3CU3E9__20_0_1() { return &___U3CU3E9__20_0_1; }
	inline void set_U3CU3E9__20_0_1(Func_2_t596561144 * value)
	{
		___U3CU3E9__20_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1820010292_StaticFields, ___U3CU3E9__21_0_2)); }
	inline Comparison_1_t1847336727 * get_U3CU3E9__21_0_2() const { return ___U3CU3E9__21_0_2; }
	inline Comparison_1_t1847336727 ** get_address_of_U3CU3E9__21_0_2() { return &___U3CU3E9__21_0_2; }
	inline void set_U3CU3E9__21_0_2(Comparison_1_t1847336727 * value)
	{
		___U3CU3E9__21_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__21_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1820010292_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANIMATIONCURVEFORMATTER_T513376465_H
#define ANIMATIONCURVEFORMATTER_T513376465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.AnimationCurveFormatter
struct  AnimationCurveFormatter_t513376465  : public MinimalBaseFormatter_1_t56771448
{
public:

public:
};

struct AnimationCurveFormatter_t513376465_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<UnityEngine.Keyframe[]> Sirenix.Serialization.AnimationCurveFormatter::KeyframeSerializer
	Serializer_1_t2745506293 * ___KeyframeSerializer_1;
	// Sirenix.Serialization.Serializer`1<UnityEngine.WrapMode> Sirenix.Serialization.AnimationCurveFormatter::WrapModeSerializer
	Serializer_1_t2407432524 * ___WrapModeSerializer_2;

public:
	inline static int32_t get_offset_of_KeyframeSerializer_1() { return static_cast<int32_t>(offsetof(AnimationCurveFormatter_t513376465_StaticFields, ___KeyframeSerializer_1)); }
	inline Serializer_1_t2745506293 * get_KeyframeSerializer_1() const { return ___KeyframeSerializer_1; }
	inline Serializer_1_t2745506293 ** get_address_of_KeyframeSerializer_1() { return &___KeyframeSerializer_1; }
	inline void set_KeyframeSerializer_1(Serializer_1_t2745506293 * value)
	{
		___KeyframeSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___KeyframeSerializer_1), value);
	}

	inline static int32_t get_offset_of_WrapModeSerializer_2() { return static_cast<int32_t>(offsetof(AnimationCurveFormatter_t513376465_StaticFields, ___WrapModeSerializer_2)); }
	inline Serializer_1_t2407432524 * get_WrapModeSerializer_2() const { return ___WrapModeSerializer_2; }
	inline Serializer_1_t2407432524 ** get_address_of_WrapModeSerializer_2() { return &___WrapModeSerializer_2; }
	inline void set_WrapModeSerializer_2(Serializer_1_t2407432524 * value)
	{
		___WrapModeSerializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___WrapModeSerializer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVEFORMATTER_T513376465_H
#ifndef BINDTYPENAMETOTYPEATTRIBUTE_T3668200987_H
#define BINDTYPENAMETOTYPEATTRIBUTE_T3668200987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BindTypeNameToTypeAttribute
struct  BindTypeNameToTypeAttribute_t3668200987  : public Attribute_t861562559
{
public:
	// System.Type Sirenix.Serialization.BindTypeNameToTypeAttribute::NewType
	Type_t * ___NewType_0;
	// System.String Sirenix.Serialization.BindTypeNameToTypeAttribute::OldTypeName
	String_t* ___OldTypeName_1;

public:
	inline static int32_t get_offset_of_NewType_0() { return static_cast<int32_t>(offsetof(BindTypeNameToTypeAttribute_t3668200987, ___NewType_0)); }
	inline Type_t * get_NewType_0() const { return ___NewType_0; }
	inline Type_t ** get_address_of_NewType_0() { return &___NewType_0; }
	inline void set_NewType_0(Type_t * value)
	{
		___NewType_0 = value;
		Il2CppCodeGenWriteBarrier((&___NewType_0), value);
	}

	inline static int32_t get_offset_of_OldTypeName_1() { return static_cast<int32_t>(offsetof(BindTypeNameToTypeAttribute_t3668200987, ___OldTypeName_1)); }
	inline String_t* get_OldTypeName_1() const { return ___OldTypeName_1; }
	inline String_t** get_address_of_OldTypeName_1() { return &___OldTypeName_1; }
	inline void set_OldTypeName_1(String_t* value)
	{
		___OldTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___OldTypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDTYPENAMETOTYPEATTRIBUTE_T3668200987_H
#ifndef BOUNDSFORMATTER_T3935229287_H
#define BOUNDSFORMATTER_T3935229287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BoundsFormatter
struct  BoundsFormatter_t3935229287  : public MinimalBaseFormatter_1_t3571822288
{
public:

public:
};

struct BoundsFormatter_t3935229287_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<UnityEngine.Vector3> Sirenix.Serialization.BoundsFormatter::Vector3Serializer
	Serializer_1_t1104327990 * ___Vector3Serializer_1;

public:
	inline static int32_t get_offset_of_Vector3Serializer_1() { return static_cast<int32_t>(offsetof(BoundsFormatter_t3935229287_StaticFields, ___Vector3Serializer_1)); }
	inline Serializer_1_t1104327990 * get_Vector3Serializer_1() const { return ___Vector3Serializer_1; }
	inline Serializer_1_t1104327990 ** get_address_of_Vector3Serializer_1() { return &___Vector3Serializer_1; }
	inline void set_Vector3Serializer_1(Serializer_1_t1104327990 * value)
	{
		___Vector3Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___Vector3Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSFORMATTER_T3935229287_H
#ifndef COLOR32FORMATTER_T348647114_H
#define COLOR32FORMATTER_T348647114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Color32Formatter
struct  Color32Formatter_t348647114  : public MinimalBaseFormatter_1_t3905485670
{
public:

public:
};

struct Color32Formatter_t348647114_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Byte> Sirenix.Serialization.Color32Formatter::ByteSerializer
	Serializer_1_t2811278198 * ___ByteSerializer_1;

public:
	inline static int32_t get_offset_of_ByteSerializer_1() { return static_cast<int32_t>(offsetof(Color32Formatter_t348647114_StaticFields, ___ByteSerializer_1)); }
	inline Serializer_1_t2811278198 * get_ByteSerializer_1() const { return ___ByteSerializer_1; }
	inline Serializer_1_t2811278198 ** get_address_of_ByteSerializer_1() { return &___ByteSerializer_1; }
	inline void set_ByteSerializer_1(Serializer_1_t2811278198 * value)
	{
		___ByteSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___ByteSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32FORMATTER_T348647114_H
#ifndef COLORBLOCKFORMATTER_T831895006_H
#define COLORBLOCKFORMATTER_T831895006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ColorBlockFormatter
struct  ColorBlockFormatter_t831895006  : public MinimalBaseFormatter_1_t3444015952
{
public:

public:
};

struct ColorBlockFormatter_t831895006_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.ColorBlockFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;
	// Sirenix.Serialization.Serializer`1<UnityEngine.Color> Sirenix.Serialization.ColorBlockFormatter::ColorSerializer
	Serializer_1_t4232668146 * ___ColorSerializer_2;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(ColorBlockFormatter_t831895006_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}

	inline static int32_t get_offset_of_ColorSerializer_2() { return static_cast<int32_t>(offsetof(ColorBlockFormatter_t831895006_StaticFields, ___ColorSerializer_2)); }
	inline Serializer_1_t4232668146 * get_ColorSerializer_2() const { return ___ColorSerializer_2; }
	inline Serializer_1_t4232668146 ** get_address_of_ColorSerializer_2() { return &___ColorSerializer_2; }
	inline void set_ColorSerializer_2(Serializer_1_t4232668146 * value)
	{
		___ColorSerializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___ColorSerializer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCKFORMATTER_T831895006_H
#ifndef COLORFORMATTER_T3218611348_H
#define COLORFORMATTER_T3218611348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ColorFormatter
struct  ColorFormatter_t3218611348  : public MinimalBaseFormatter_1_t3860670702
{
public:

public:
};

struct ColorFormatter_t3218611348_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.ColorFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(ColorFormatter_t3218611348_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORFORMATTER_T3218611348_H
#ifndef DEFAULTSERIALIZATIONBINDER_T116316556_H
#define DEFAULTSERIALIZATIONBINDER_T116316556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DefaultSerializationBinder
struct  DefaultSerializationBinder_t116316556  : public TwoWaySerializationBinder_t4250048501
{
public:

public:
};

struct DefaultSerializationBinder_t116316556_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.Assembly> Sirenix.Serialization.DefaultSerializationBinder::assemblyNameLookUp
	Dictionary_2_t3887689098 * ___assemblyNameLookUp_1;
	// System.Object Sirenix.Serialization.DefaultSerializationBinder::TYPEMAP_LOCK
	RuntimeObject * ___TYPEMAP_LOCK_2;
	// System.Object Sirenix.Serialization.DefaultSerializationBinder::NAMEMAP_LOCK
	RuntimeObject * ___NAMEMAP_LOCK_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Sirenix.Serialization.DefaultSerializationBinder::typeMap
	Dictionary_2_t2269201059 * ___typeMap_4;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> Sirenix.Serialization.DefaultSerializationBinder::nameMap
	Dictionary_2_t4291797753 * ___nameMap_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Sirenix.Serialization.DefaultSerializationBinder::customTypeNameToTypeBindings
	Dictionary_2_t2269201059 * ___customTypeNameToTypeBindings_6;

public:
	inline static int32_t get_offset_of_assemblyNameLookUp_1() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t116316556_StaticFields, ___assemblyNameLookUp_1)); }
	inline Dictionary_2_t3887689098 * get_assemblyNameLookUp_1() const { return ___assemblyNameLookUp_1; }
	inline Dictionary_2_t3887689098 ** get_address_of_assemblyNameLookUp_1() { return &___assemblyNameLookUp_1; }
	inline void set_assemblyNameLookUp_1(Dictionary_2_t3887689098 * value)
	{
		___assemblyNameLookUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyNameLookUp_1), value);
	}

	inline static int32_t get_offset_of_TYPEMAP_LOCK_2() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t116316556_StaticFields, ___TYPEMAP_LOCK_2)); }
	inline RuntimeObject * get_TYPEMAP_LOCK_2() const { return ___TYPEMAP_LOCK_2; }
	inline RuntimeObject ** get_address_of_TYPEMAP_LOCK_2() { return &___TYPEMAP_LOCK_2; }
	inline void set_TYPEMAP_LOCK_2(RuntimeObject * value)
	{
		___TYPEMAP_LOCK_2 = value;
		Il2CppCodeGenWriteBarrier((&___TYPEMAP_LOCK_2), value);
	}

	inline static int32_t get_offset_of_NAMEMAP_LOCK_3() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t116316556_StaticFields, ___NAMEMAP_LOCK_3)); }
	inline RuntimeObject * get_NAMEMAP_LOCK_3() const { return ___NAMEMAP_LOCK_3; }
	inline RuntimeObject ** get_address_of_NAMEMAP_LOCK_3() { return &___NAMEMAP_LOCK_3; }
	inline void set_NAMEMAP_LOCK_3(RuntimeObject * value)
	{
		___NAMEMAP_LOCK_3 = value;
		Il2CppCodeGenWriteBarrier((&___NAMEMAP_LOCK_3), value);
	}

	inline static int32_t get_offset_of_typeMap_4() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t116316556_StaticFields, ___typeMap_4)); }
	inline Dictionary_2_t2269201059 * get_typeMap_4() const { return ___typeMap_4; }
	inline Dictionary_2_t2269201059 ** get_address_of_typeMap_4() { return &___typeMap_4; }
	inline void set_typeMap_4(Dictionary_2_t2269201059 * value)
	{
		___typeMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___typeMap_4), value);
	}

	inline static int32_t get_offset_of_nameMap_5() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t116316556_StaticFields, ___nameMap_5)); }
	inline Dictionary_2_t4291797753 * get_nameMap_5() const { return ___nameMap_5; }
	inline Dictionary_2_t4291797753 ** get_address_of_nameMap_5() { return &___nameMap_5; }
	inline void set_nameMap_5(Dictionary_2_t4291797753 * value)
	{
		___nameMap_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameMap_5), value);
	}

	inline static int32_t get_offset_of_customTypeNameToTypeBindings_6() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t116316556_StaticFields, ___customTypeNameToTypeBindings_6)); }
	inline Dictionary_2_t2269201059 * get_customTypeNameToTypeBindings_6() const { return ___customTypeNameToTypeBindings_6; }
	inline Dictionary_2_t2269201059 ** get_address_of_customTypeNameToTypeBindings_6() { return &___customTypeNameToTypeBindings_6; }
	inline void set_customTypeNameToTypeBindings_6(Dictionary_2_t2269201059 * value)
	{
		___customTypeNameToTypeBindings_6 = value;
		Il2CppCodeGenWriteBarrier((&___customTypeNameToTypeBindings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSERIALIZATIONBINDER_T116316556_H
#ifndef EMITTEDASSEMBLYATTRIBUTE_T416105211_H
#define EMITTEDASSEMBLYATTRIBUTE_T416105211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.EmittedAssemblyAttribute
struct  EmittedAssemblyAttribute_t416105211  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMITTEDASSEMBLYATTRIBUTE_T416105211_H
#ifndef EXCLUDEDATAFROMINSPECTORATTRIBUTE_T1238714384_H
#define EXCLUDEDATAFROMINSPECTORATTRIBUTE_T1238714384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ExcludeDataFromInspectorAttribute
struct  ExcludeDataFromInspectorAttribute_t1238714384  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEDATAFROMINSPECTORATTRIBUTE_T1238714384_H
#ifndef FORMATTERINFO_T2891647851_H
#define FORMATTERINFO_T2891647851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterLocator/FormatterInfo
struct  FormatterInfo_t2891647851 
{
public:
	// System.Type Sirenix.Serialization.FormatterLocator/FormatterInfo::FormatterType
	Type_t * ___FormatterType_0;
	// System.Type Sirenix.Serialization.FormatterLocator/FormatterInfo::TargetType
	Type_t * ___TargetType_1;
	// System.Boolean Sirenix.Serialization.FormatterLocator/FormatterInfo::AskIfCanFormatTypes
	bool ___AskIfCanFormatTypes_2;
	// System.Int32 Sirenix.Serialization.FormatterLocator/FormatterInfo::Priority
	int32_t ___Priority_3;

public:
	inline static int32_t get_offset_of_FormatterType_0() { return static_cast<int32_t>(offsetof(FormatterInfo_t2891647851, ___FormatterType_0)); }
	inline Type_t * get_FormatterType_0() const { return ___FormatterType_0; }
	inline Type_t ** get_address_of_FormatterType_0() { return &___FormatterType_0; }
	inline void set_FormatterType_0(Type_t * value)
	{
		___FormatterType_0 = value;
		Il2CppCodeGenWriteBarrier((&___FormatterType_0), value);
	}

	inline static int32_t get_offset_of_TargetType_1() { return static_cast<int32_t>(offsetof(FormatterInfo_t2891647851, ___TargetType_1)); }
	inline Type_t * get_TargetType_1() const { return ___TargetType_1; }
	inline Type_t ** get_address_of_TargetType_1() { return &___TargetType_1; }
	inline void set_TargetType_1(Type_t * value)
	{
		___TargetType_1 = value;
		Il2CppCodeGenWriteBarrier((&___TargetType_1), value);
	}

	inline static int32_t get_offset_of_AskIfCanFormatTypes_2() { return static_cast<int32_t>(offsetof(FormatterInfo_t2891647851, ___AskIfCanFormatTypes_2)); }
	inline bool get_AskIfCanFormatTypes_2() const { return ___AskIfCanFormatTypes_2; }
	inline bool* get_address_of_AskIfCanFormatTypes_2() { return &___AskIfCanFormatTypes_2; }
	inline void set_AskIfCanFormatTypes_2(bool value)
	{
		___AskIfCanFormatTypes_2 = value;
	}

	inline static int32_t get_offset_of_Priority_3() { return static_cast<int32_t>(offsetof(FormatterInfo_t2891647851, ___Priority_3)); }
	inline int32_t get_Priority_3() const { return ___Priority_3; }
	inline int32_t* get_address_of_Priority_3() { return &___Priority_3; }
	inline void set_Priority_3(int32_t value)
	{
		___Priority_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Serialization.FormatterLocator/FormatterInfo
struct FormatterInfo_t2891647851_marshaled_pinvoke
{
	Type_t * ___FormatterType_0;
	Type_t * ___TargetType_1;
	int32_t ___AskIfCanFormatTypes_2;
	int32_t ___Priority_3;
};
// Native definition for COM marshalling of Sirenix.Serialization.FormatterLocator/FormatterInfo
struct FormatterInfo_t2891647851_marshaled_com
{
	Type_t * ___FormatterType_0;
	Type_t * ___TargetType_1;
	int32_t ___AskIfCanFormatTypes_2;
	int32_t ___Priority_3;
};
#endif // FORMATTERINFO_T2891647851_H
#ifndef FORMATTERLOCATORINFO_T371893538_H
#define FORMATTERLOCATORINFO_T371893538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo
struct  FormatterLocatorInfo_t371893538 
{
public:
	// Sirenix.Serialization.IFormatterLocator Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo::LocatorInstance
	RuntimeObject* ___LocatorInstance_0;
	// System.Int32 Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_LocatorInstance_0() { return static_cast<int32_t>(offsetof(FormatterLocatorInfo_t371893538, ___LocatorInstance_0)); }
	inline RuntimeObject* get_LocatorInstance_0() const { return ___LocatorInstance_0; }
	inline RuntimeObject** get_address_of_LocatorInstance_0() { return &___LocatorInstance_0; }
	inline void set_LocatorInstance_0(RuntimeObject* value)
	{
		___LocatorInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___LocatorInstance_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(FormatterLocatorInfo_t371893538, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo
struct FormatterLocatorInfo_t371893538_marshaled_pinvoke
{
	RuntimeObject* ___LocatorInstance_0;
	int32_t ___Priority_1;
};
// Native definition for COM marshalling of Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo
struct FormatterLocatorInfo_t371893538_marshaled_com
{
	RuntimeObject* ___LocatorInstance_0;
	int32_t ___Priority_1;
};
#endif // FORMATTERLOCATORINFO_T371893538_H
#ifndef GRADIENTALPHAKEYFORMATTER_T2425658533_H
#define GRADIENTALPHAKEYFORMATTER_T2425658533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GradientAlphaKeyFormatter
struct  GradientAlphaKeyFormatter_t2425658533  : public MinimalBaseFormatter_1_t3929727004
{
public:

public:
};

struct GradientAlphaKeyFormatter_t2425658533_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.GradientAlphaKeyFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(GradientAlphaKeyFormatter_t2425658533_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTALPHAKEYFORMATTER_T2425658533_H
#ifndef GRADIENTCOLORKEYFORMATTER_T645871939_H
#define GRADIENTCOLORKEYFORMATTER_T645871939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GradientColorKeyFormatter
struct  GradientColorKeyFormatter_t645871939  : public MinimalBaseFormatter_1_t2117536967
{
public:

public:
};

struct GradientColorKeyFormatter_t645871939_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<UnityEngine.Color> Sirenix.Serialization.GradientColorKeyFormatter::ColorSerializer
	Serializer_1_t4232668146 * ___ColorSerializer_1;
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.GradientColorKeyFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_2;

public:
	inline static int32_t get_offset_of_ColorSerializer_1() { return static_cast<int32_t>(offsetof(GradientColorKeyFormatter_t645871939_StaticFields, ___ColorSerializer_1)); }
	inline Serializer_1_t4232668146 * get_ColorSerializer_1() const { return ___ColorSerializer_1; }
	inline Serializer_1_t4232668146 ** get_address_of_ColorSerializer_1() { return &___ColorSerializer_1; }
	inline void set_ColorSerializer_1(Serializer_1_t4232668146 * value)
	{
		___ColorSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___ColorSerializer_1), value);
	}

	inline static int32_t get_offset_of_FloatSerializer_2() { return static_cast<int32_t>(offsetof(GradientColorKeyFormatter_t645871939_StaticFields, ___FloatSerializer_2)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_2() const { return ___FloatSerializer_2; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_2() { return &___FloatSerializer_2; }
	inline void set_FloatSerializer_2(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTCOLORKEYFORMATTER_T645871939_H
#ifndef GRADIENTFORMATTER_T3582296489_H
#define GRADIENTFORMATTER_T3582296489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GradientFormatter
struct  GradientFormatter_t3582296489  : public MinimalBaseFormatter_1_t77117006
{
public:

public:
};

struct GradientFormatter_t3582296489_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<UnityEngine.GradientAlphaKey[]> Sirenix.Serialization.GradientFormatter::AlphaKeysSerializer
	Serializer_1_t3663464341 * ___AlphaKeysSerializer_1;
	// Sirenix.Serialization.Serializer`1<UnityEngine.GradientColorKey[]> Sirenix.Serialization.GradientFormatter::ColorKeysSerializer
	Serializer_1_t1515746270 * ___ColorKeysSerializer_2;
	// System.Reflection.PropertyInfo Sirenix.Serialization.GradientFormatter::ModeProperty
	PropertyInfo_t * ___ModeProperty_3;
	// Sirenix.Serialization.Serializer`1<System.Object> Sirenix.Serialization.GradientFormatter::EnumSerializer
	Serializer_1_t462120690 * ___EnumSerializer_4;

public:
	inline static int32_t get_offset_of_AlphaKeysSerializer_1() { return static_cast<int32_t>(offsetof(GradientFormatter_t3582296489_StaticFields, ___AlphaKeysSerializer_1)); }
	inline Serializer_1_t3663464341 * get_AlphaKeysSerializer_1() const { return ___AlphaKeysSerializer_1; }
	inline Serializer_1_t3663464341 ** get_address_of_AlphaKeysSerializer_1() { return &___AlphaKeysSerializer_1; }
	inline void set_AlphaKeysSerializer_1(Serializer_1_t3663464341 * value)
	{
		___AlphaKeysSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaKeysSerializer_1), value);
	}

	inline static int32_t get_offset_of_ColorKeysSerializer_2() { return static_cast<int32_t>(offsetof(GradientFormatter_t3582296489_StaticFields, ___ColorKeysSerializer_2)); }
	inline Serializer_1_t1515746270 * get_ColorKeysSerializer_2() const { return ___ColorKeysSerializer_2; }
	inline Serializer_1_t1515746270 ** get_address_of_ColorKeysSerializer_2() { return &___ColorKeysSerializer_2; }
	inline void set_ColorKeysSerializer_2(Serializer_1_t1515746270 * value)
	{
		___ColorKeysSerializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___ColorKeysSerializer_2), value);
	}

	inline static int32_t get_offset_of_ModeProperty_3() { return static_cast<int32_t>(offsetof(GradientFormatter_t3582296489_StaticFields, ___ModeProperty_3)); }
	inline PropertyInfo_t * get_ModeProperty_3() const { return ___ModeProperty_3; }
	inline PropertyInfo_t ** get_address_of_ModeProperty_3() { return &___ModeProperty_3; }
	inline void set_ModeProperty_3(PropertyInfo_t * value)
	{
		___ModeProperty_3 = value;
		Il2CppCodeGenWriteBarrier((&___ModeProperty_3), value);
	}

	inline static int32_t get_offset_of_EnumSerializer_4() { return static_cast<int32_t>(offsetof(GradientFormatter_t3582296489_StaticFields, ___EnumSerializer_4)); }
	inline Serializer_1_t462120690 * get_EnumSerializer_4() const { return ___EnumSerializer_4; }
	inline Serializer_1_t462120690 ** get_address_of_EnumSerializer_4() { return &___EnumSerializer_4; }
	inline void set_EnumSerializer_4(Serializer_1_t462120690 * value)
	{
		___EnumSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___EnumSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTFORMATTER_T3582296489_H
#ifndef KEYFRAMEFORMATTER_T2209210672_H
#define KEYFRAMEFORMATTER_T2209210672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.KeyframeFormatter
struct  KeyframeFormatter_t2209210672  : public MinimalBaseFormatter_1_t1216427324
{
public:

public:
};

struct KeyframeFormatter_t2209210672_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.KeyframeFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;
	// Sirenix.Serialization.Serializer`1<System.Int32> Sirenix.Serialization.KeyframeFormatter::IntSerializer
	Serializer_1_t332960279 * ___IntSerializer_2;
	// System.Boolean Sirenix.Serialization.KeyframeFormatter::Is_In_2018_1_Or_Above
	bool ___Is_In_2018_1_Or_Above_3;
	// Sirenix.Serialization.IFormatter`1<UnityEngine.Keyframe> Sirenix.Serialization.KeyframeFormatter::Formatter
	RuntimeObject* ___Formatter_4;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(KeyframeFormatter_t2209210672_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}

	inline static int32_t get_offset_of_IntSerializer_2() { return static_cast<int32_t>(offsetof(KeyframeFormatter_t2209210672_StaticFields, ___IntSerializer_2)); }
	inline Serializer_1_t332960279 * get_IntSerializer_2() const { return ___IntSerializer_2; }
	inline Serializer_1_t332960279 ** get_address_of_IntSerializer_2() { return &___IntSerializer_2; }
	inline void set_IntSerializer_2(Serializer_1_t332960279 * value)
	{
		___IntSerializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___IntSerializer_2), value);
	}

	inline static int32_t get_offset_of_Is_In_2018_1_Or_Above_3() { return static_cast<int32_t>(offsetof(KeyframeFormatter_t2209210672_StaticFields, ___Is_In_2018_1_Or_Above_3)); }
	inline bool get_Is_In_2018_1_Or_Above_3() const { return ___Is_In_2018_1_Or_Above_3; }
	inline bool* get_address_of_Is_In_2018_1_Or_Above_3() { return &___Is_In_2018_1_Or_Above_3; }
	inline void set_Is_In_2018_1_Or_Above_3(bool value)
	{
		___Is_In_2018_1_Or_Above_3 = value;
	}

	inline static int32_t get_offset_of_Formatter_4() { return static_cast<int32_t>(offsetof(KeyframeFormatter_t2209210672_StaticFields, ___Formatter_4)); }
	inline RuntimeObject* get_Formatter_4() const { return ___Formatter_4; }
	inline RuntimeObject** get_address_of_Formatter_4() { return &___Formatter_4; }
	inline void set_Formatter_4(RuntimeObject* value)
	{
		___Formatter_4 = value;
		Il2CppCodeGenWriteBarrier((&___Formatter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAMEFORMATTER_T2209210672_H
#ifndef LAYERMASKFORMATTER_T2206937223_H
#define LAYERMASKFORMATTER_T2206937223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.LayerMaskFormatter
struct  LayerMaskFormatter_t2206937223  : public MinimalBaseFormatter_1_t503952000
{
public:

public:
};

struct LayerMaskFormatter_t2206937223_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Int32> Sirenix.Serialization.LayerMaskFormatter::IntSerializer
	Serializer_1_t332960279 * ___IntSerializer_1;

public:
	inline static int32_t get_offset_of_IntSerializer_1() { return static_cast<int32_t>(offsetof(LayerMaskFormatter_t2206937223_StaticFields, ___IntSerializer_1)); }
	inline Serializer_1_t332960279 * get_IntSerializer_1() const { return ___IntSerializer_1; }
	inline Serializer_1_t332960279 ** get_address_of_IntSerializer_1() { return &___IntSerializer_1; }
	inline void set_IntSerializer_1(Serializer_1_t332960279 * value)
	{
		___IntSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___IntSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASKFORMATTER_T2206937223_H
#ifndef NODEINFO_T1254526976_H
#define NODEINFO_T1254526976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.NodeInfo
struct  NodeInfo_t1254526976 
{
public:
	// System.String Sirenix.Serialization.NodeInfo::Name
	String_t* ___Name_1;
	// System.Int32 Sirenix.Serialization.NodeInfo::Id
	int32_t ___Id_2;
	// System.Type Sirenix.Serialization.NodeInfo::Type
	Type_t * ___Type_3;
	// System.Boolean Sirenix.Serialization.NodeInfo::IsArray
	bool ___IsArray_4;
	// System.Boolean Sirenix.Serialization.NodeInfo::IsEmpty
	bool ___IsEmpty_5;

public:
	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(NodeInfo_t1254526976, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Id_2() { return static_cast<int32_t>(offsetof(NodeInfo_t1254526976, ___Id_2)); }
	inline int32_t get_Id_2() const { return ___Id_2; }
	inline int32_t* get_address_of_Id_2() { return &___Id_2; }
	inline void set_Id_2(int32_t value)
	{
		___Id_2 = value;
	}

	inline static int32_t get_offset_of_Type_3() { return static_cast<int32_t>(offsetof(NodeInfo_t1254526976, ___Type_3)); }
	inline Type_t * get_Type_3() const { return ___Type_3; }
	inline Type_t ** get_address_of_Type_3() { return &___Type_3; }
	inline void set_Type_3(Type_t * value)
	{
		___Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___Type_3), value);
	}

	inline static int32_t get_offset_of_IsArray_4() { return static_cast<int32_t>(offsetof(NodeInfo_t1254526976, ___IsArray_4)); }
	inline bool get_IsArray_4() const { return ___IsArray_4; }
	inline bool* get_address_of_IsArray_4() { return &___IsArray_4; }
	inline void set_IsArray_4(bool value)
	{
		___IsArray_4 = value;
	}

	inline static int32_t get_offset_of_IsEmpty_5() { return static_cast<int32_t>(offsetof(NodeInfo_t1254526976, ___IsEmpty_5)); }
	inline bool get_IsEmpty_5() const { return ___IsEmpty_5; }
	inline bool* get_address_of_IsEmpty_5() { return &___IsEmpty_5; }
	inline void set_IsEmpty_5(bool value)
	{
		___IsEmpty_5 = value;
	}
};

struct NodeInfo_t1254526976_StaticFields
{
public:
	// Sirenix.Serialization.NodeInfo Sirenix.Serialization.NodeInfo::Empty
	NodeInfo_t1254526976  ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(NodeInfo_t1254526976_StaticFields, ___Empty_0)); }
	inline NodeInfo_t1254526976  get_Empty_0() const { return ___Empty_0; }
	inline NodeInfo_t1254526976 * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(NodeInfo_t1254526976  value)
	{
		___Empty_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Serialization.NodeInfo
struct NodeInfo_t1254526976_marshaled_pinvoke
{
	char* ___Name_1;
	int32_t ___Id_2;
	Type_t * ___Type_3;
	int32_t ___IsArray_4;
	int32_t ___IsEmpty_5;
};
// Native definition for COM marshalling of Sirenix.Serialization.NodeInfo
struct NodeInfo_t1254526976_marshaled_com
{
	Il2CppChar* ___Name_1;
	int32_t ___Id_2;
	Type_t * ___Type_3;
	int32_t ___IsArray_4;
	int32_t ___IsEmpty_5;
};
#endif // NODEINFO_T1254526976_H
#ifndef ODINSERIALIZEATTRIBUTE_T3257861414_H
#define ODINSERIALIZEATTRIBUTE_T3257861414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.OdinSerializeAttribute
struct  OdinSerializeAttribute_t3257861414  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ODINSERIALIZEATTRIBUTE_T3257861414_H
#ifndef PREVIOUSLYSERIALIZEDASATTRIBUTE_T1757059606_H
#define PREVIOUSLYSERIALIZEDASATTRIBUTE_T1757059606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.PreviouslySerializedAsAttribute
struct  PreviouslySerializedAsAttribute_t1757059606  : public Attribute_t861562559
{
public:
	// System.String Sirenix.Serialization.PreviouslySerializedAsAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PreviouslySerializedAsAttribute_t1757059606, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIOUSLYSERIALIZEDASATTRIBUTE_T1757059606_H
#ifndef DOUBLEBYTEUNION_T2294834964_H
#define DOUBLEBYTEUNION_T2294834964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ProperBitConverter/DoubleByteUnion
struct  DoubleByteUnion_t2294834964 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte0
			uint8_t ___Byte0_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___Byte0_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte1_1_OffsetPadding[1];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte1
			uint8_t ___Byte1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte1_1_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___Byte1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte2_2_OffsetPadding[2];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte2
			uint8_t ___Byte2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte2_2_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___Byte2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte3_3_OffsetPadding[3];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte3
			uint8_t ___Byte3_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte3_3_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___Byte3_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte4_4_OffsetPadding[4];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte4
			uint8_t ___Byte4_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte4_4_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___Byte4_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte5_5_OffsetPadding[5];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte5
			uint8_t ___Byte5_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte5_5_OffsetPadding_forAlignmentOnly[5];
			uint8_t ___Byte5_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte6_6_OffsetPadding[6];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte6
			uint8_t ___Byte6_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte6_6_OffsetPadding_forAlignmentOnly[6];
			uint8_t ___Byte6_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte7_7_OffsetPadding[7];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Byte7
			uint8_t ___Byte7_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte7_7_OffsetPadding_forAlignmentOnly[7];
			uint8_t ___Byte7_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double Sirenix.Serialization.ProperBitConverter/DoubleByteUnion::Value
			double ___Value_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___Value_8_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_Byte0_0() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte0_0)); }
	inline uint8_t get_Byte0_0() const { return ___Byte0_0; }
	inline uint8_t* get_address_of_Byte0_0() { return &___Byte0_0; }
	inline void set_Byte0_0(uint8_t value)
	{
		___Byte0_0 = value;
	}

	inline static int32_t get_offset_of_Byte1_1() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte1_1)); }
	inline uint8_t get_Byte1_1() const { return ___Byte1_1; }
	inline uint8_t* get_address_of_Byte1_1() { return &___Byte1_1; }
	inline void set_Byte1_1(uint8_t value)
	{
		___Byte1_1 = value;
	}

	inline static int32_t get_offset_of_Byte2_2() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte2_2)); }
	inline uint8_t get_Byte2_2() const { return ___Byte2_2; }
	inline uint8_t* get_address_of_Byte2_2() { return &___Byte2_2; }
	inline void set_Byte2_2(uint8_t value)
	{
		___Byte2_2 = value;
	}

	inline static int32_t get_offset_of_Byte3_3() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte3_3)); }
	inline uint8_t get_Byte3_3() const { return ___Byte3_3; }
	inline uint8_t* get_address_of_Byte3_3() { return &___Byte3_3; }
	inline void set_Byte3_3(uint8_t value)
	{
		___Byte3_3 = value;
	}

	inline static int32_t get_offset_of_Byte4_4() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte4_4)); }
	inline uint8_t get_Byte4_4() const { return ___Byte4_4; }
	inline uint8_t* get_address_of_Byte4_4() { return &___Byte4_4; }
	inline void set_Byte4_4(uint8_t value)
	{
		___Byte4_4 = value;
	}

	inline static int32_t get_offset_of_Byte5_5() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte5_5)); }
	inline uint8_t get_Byte5_5() const { return ___Byte5_5; }
	inline uint8_t* get_address_of_Byte5_5() { return &___Byte5_5; }
	inline void set_Byte5_5(uint8_t value)
	{
		___Byte5_5 = value;
	}

	inline static int32_t get_offset_of_Byte6_6() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte6_6)); }
	inline uint8_t get_Byte6_6() const { return ___Byte6_6; }
	inline uint8_t* get_address_of_Byte6_6() { return &___Byte6_6; }
	inline void set_Byte6_6(uint8_t value)
	{
		___Byte6_6 = value;
	}

	inline static int32_t get_offset_of_Byte7_7() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Byte7_7)); }
	inline uint8_t get_Byte7_7() const { return ___Byte7_7; }
	inline uint8_t* get_address_of_Byte7_7() { return &___Byte7_7; }
	inline void set_Byte7_7(uint8_t value)
	{
		___Byte7_7 = value;
	}

	inline static int32_t get_offset_of_Value_8() { return static_cast<int32_t>(offsetof(DoubleByteUnion_t2294834964, ___Value_8)); }
	inline double get_Value_8() const { return ___Value_8; }
	inline double* get_address_of_Value_8() { return &___Value_8; }
	inline void set_Value_8(double value)
	{
		___Value_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEBYTEUNION_T2294834964_H
#ifndef SINGLEBYTEUNION_T1581541998_H
#define SINGLEBYTEUNION_T1581541998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ProperBitConverter/SingleByteUnion
struct  SingleByteUnion_t1581541998 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte Sirenix.Serialization.ProperBitConverter/SingleByteUnion::Byte0
			uint8_t ___Byte0_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___Byte0_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte1_1_OffsetPadding[1];
			// System.Byte Sirenix.Serialization.ProperBitConverter/SingleByteUnion::Byte1
			uint8_t ___Byte1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte1_1_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___Byte1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte2_2_OffsetPadding[2];
			// System.Byte Sirenix.Serialization.ProperBitConverter/SingleByteUnion::Byte2
			uint8_t ___Byte2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte2_2_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___Byte2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte3_3_OffsetPadding[3];
			// System.Byte Sirenix.Serialization.ProperBitConverter/SingleByteUnion::Byte3
			uint8_t ___Byte3_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte3_3_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___Byte3_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single Sirenix.Serialization.ProperBitConverter/SingleByteUnion::Value
			float ___Value_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___Value_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_Byte0_0() { return static_cast<int32_t>(offsetof(SingleByteUnion_t1581541998, ___Byte0_0)); }
	inline uint8_t get_Byte0_0() const { return ___Byte0_0; }
	inline uint8_t* get_address_of_Byte0_0() { return &___Byte0_0; }
	inline void set_Byte0_0(uint8_t value)
	{
		___Byte0_0 = value;
	}

	inline static int32_t get_offset_of_Byte1_1() { return static_cast<int32_t>(offsetof(SingleByteUnion_t1581541998, ___Byte1_1)); }
	inline uint8_t get_Byte1_1() const { return ___Byte1_1; }
	inline uint8_t* get_address_of_Byte1_1() { return &___Byte1_1; }
	inline void set_Byte1_1(uint8_t value)
	{
		___Byte1_1 = value;
	}

	inline static int32_t get_offset_of_Byte2_2() { return static_cast<int32_t>(offsetof(SingleByteUnion_t1581541998, ___Byte2_2)); }
	inline uint8_t get_Byte2_2() const { return ___Byte2_2; }
	inline uint8_t* get_address_of_Byte2_2() { return &___Byte2_2; }
	inline void set_Byte2_2(uint8_t value)
	{
		___Byte2_2 = value;
	}

	inline static int32_t get_offset_of_Byte3_3() { return static_cast<int32_t>(offsetof(SingleByteUnion_t1581541998, ___Byte3_3)); }
	inline uint8_t get_Byte3_3() const { return ___Byte3_3; }
	inline uint8_t* get_address_of_Byte3_3() { return &___Byte3_3; }
	inline void set_Byte3_3(uint8_t value)
	{
		___Byte3_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(SingleByteUnion_t1581541998, ___Value_4)); }
	inline float get_Value_4() const { return ___Value_4; }
	inline float* get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(float value)
	{
		___Value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEBYTEUNION_T1581541998_H
#ifndef QUATERNIONFORMATTER_T251372450_H
#define QUATERNIONFORMATTER_T251372450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.QuaternionFormatter
struct  QuaternionFormatter_t251372450  : public MinimalBaseFormatter_1_t3606912709
{
public:

public:
};

struct QuaternionFormatter_t251372450_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.QuaternionFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(QuaternionFormatter_t251372450_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONFORMATTER_T251372450_H
#ifndef RECTFORMATTER_T2640669641_H
#define RECTFORMATTER_T2640669641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.RectFormatter
struct  RectFormatter_t2640669641  : public MinimalBaseFormatter_1_t3665464237
{
public:

public:
};

struct RectFormatter_t2640669641_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.RectFormatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(RectFormatter_t2640669641_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTFORMATTER_T2640669641_H
#ifndef REGISTERDICTIONARYKEYPATHPROVIDERATTRIBUTE_T2832569773_H
#define REGISTERDICTIONARYKEYPATHPROVIDERATTRIBUTE_T2832569773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute
struct  RegisterDictionaryKeyPathProviderAttribute_t2832569773  : public Attribute_t861562559
{
public:
	// System.Type Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute::ProviderType
	Type_t * ___ProviderType_0;

public:
	inline static int32_t get_offset_of_ProviderType_0() { return static_cast<int32_t>(offsetof(RegisterDictionaryKeyPathProviderAttribute_t2832569773, ___ProviderType_0)); }
	inline Type_t * get_ProviderType_0() const { return ___ProviderType_0; }
	inline Type_t ** get_address_of_ProviderType_0() { return &___ProviderType_0; }
	inline void set_ProviderType_0(Type_t * value)
	{
		___ProviderType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ProviderType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERDICTIONARYKEYPATHPROVIDERATTRIBUTE_T2832569773_H
#ifndef SERIALIZATIONABORTEXCEPTION_T679951102_H
#define SERIALIZATIONABORTEXCEPTION_T679951102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationAbortException
struct  SerializationAbortException_t679951102  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONABORTEXCEPTION_T679951102_H
#ifndef SERIALIZER_1_T1774269787_H
#define SERIALIZER_1_T1774269787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Boolean>
struct  Serializer_1_t1774269787  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T1774269787_H
#ifndef SERIALIZER_1_T2811278198_H
#define SERIALIZER_1_T2811278198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Byte>
struct  Serializer_1_t2811278198  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T2811278198_H
#ifndef SERIALIZER_1_T1016474996_H
#define SERIALIZER_1_T1016474996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Char>
struct  Serializer_1_t1016474996  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T1016474996_H
#ifndef SERIALIZER_1_T330273906_H
#define SERIALIZER_1_T330273906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Decimal>
struct  Serializer_1_t330273906  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T330273906_H
#ifndef SERIALIZER_1_T2271647185_H
#define SERIALIZER_1_T2271647185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Double>
struct  Serializer_1_t2271647185  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T2271647185_H
#ifndef SERIALIZER_1_T575547413_H
#define SERIALIZER_1_T575547413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Guid>
struct  Serializer_1_t575547413  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T575547413_H
#ifndef SERIALIZER_1_T4229802209_H
#define SERIALIZER_1_T4229802209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Int16>
struct  Serializer_1_t4229802209  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T4229802209_H
#ifndef SERIALIZER_1_T332960279_H
#define SERIALIZER_1_T332960279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Int32>
struct  Serializer_1_t332960279  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T332960279_H
#ifndef SERIALIZER_1_T1118581830_H
#define SERIALIZER_1_T1118581830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Int64>
struct  Serializer_1_t1118581830  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T1118581830_H
#ifndef SERIALIZER_1_T2517132003_H
#define SERIALIZER_1_T2517132003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.IntPtr>
struct  Serializer_1_t2517132003  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T2517132003_H
#ifndef SERIALIZER_1_T3346559484_H
#define SERIALIZER_1_T3346559484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.SByte>
struct  Serializer_1_t3346559484  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T3346559484_H
#ifndef SERIALIZER_1_T3074248596_H
#define SERIALIZER_1_T3074248596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.Single>
struct  Serializer_1_t3074248596  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T3074248596_H
#ifndef SERIALIZER_1_T3524432511_H
#define SERIALIZER_1_T3524432511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.String>
struct  Serializer_1_t3524432511  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T3524432511_H
#ifndef SERIALIZER_1_T3854706780_H
#define SERIALIZER_1_T3854706780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.UInt16>
struct  Serializer_1_t3854706780  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T3854706780_H
#ifndef SERIALIZER_1_T4237043800_H
#define SERIALIZER_1_T4237043800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.UInt32>
struct  Serializer_1_t4237043800  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T4237043800_H
#ifndef SERIALIZER_1_T1516054618_H
#define SERIALIZER_1_T1516054618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.UInt64>
struct  Serializer_1_t1516054618  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T1516054618_H
#ifndef SERIALIZER_1_T3723831450_H
#define SERIALIZER_1_T3723831450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Serializer`1<System.UIntPtr>
struct  Serializer_1_t3723831450  : public Serializer_t962918574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_1_T3723831450_H
#ifndef VECTOR2DICTIONARYKEYPATHPROVIDER_T1191773922_H
#define VECTOR2DICTIONARYKEYPATHPROVIDER_T1191773922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector2DictionaryKeyPathProvider
struct  Vector2DictionaryKeyPathProvider_t1191773922  : public BaseDictionaryKeyPathProvider_1_t1761792157
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2DICTIONARYKEYPATHPROVIDER_T1191773922_H
#ifndef VECTOR2FORMATTER_T2520006045_H
#define VECTOR2FORMATTER_T2520006045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector2Formatter
struct  Vector2Formatter_t2520006045  : public MinimalBaseFormatter_1_t3461213901
{
public:

public:
};

struct Vector2Formatter_t2520006045_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.Vector2Formatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(Vector2Formatter_t2520006045_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2FORMATTER_T2520006045_H
#ifndef VECTOR3DICTIONARYKEYPATHPROVIDER_T2227500205_H
#define VECTOR3DICTIONARYKEYPATHPROVIDER_T2227500205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector3DictionaryKeyPathProvider
struct  Vector3DictionaryKeyPathProvider_t2227500205  : public BaseDictionaryKeyPathProvider_1_t3327876098
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3DICTIONARYKEYPATHPROVIDER_T2227500205_H
#ifndef VECTOR3FORMATTER_T2520007008_H
#define VECTOR3FORMATTER_T2520007008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector3Formatter
struct  Vector3Formatter_t2520007008  : public MinimalBaseFormatter_1_t732330546
{
public:

public:
};

struct Vector3Formatter_t2520007008_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.Vector3Formatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(Vector3Formatter_t2520007008_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3FORMATTER_T2520007008_H
#ifndef VECTOR4DICTIONARYKEYPATHPROVIDER_T4213655263_H
#define VECTOR4DICTIONARYKEYPATHPROVIDER_T4213655263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector4DictionaryKeyPathProvider
struct  Vector4DictionaryKeyPathProvider_t4213655263  : public BaseDictionaryKeyPathProvider_1_t2924591571
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4DICTIONARYKEYPATHPROVIDER_T4213655263_H
#ifndef VECTOR4FORMATTER_T2519996543_H
#define VECTOR4FORMATTER_T2519996543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector4Formatter
struct  Vector4Formatter_t2519996543  : public MinimalBaseFormatter_1_t329046019
{
public:

public:
};

struct Vector4Formatter_t2519996543_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Single> Sirenix.Serialization.Vector4Formatter::FloatSerializer
	Serializer_1_t3074248596 * ___FloatSerializer_1;

public:
	inline static int32_t get_offset_of_FloatSerializer_1() { return static_cast<int32_t>(offsetof(Vector4Formatter_t2519996543_StaticFields, ___FloatSerializer_1)); }
	inline Serializer_1_t3074248596 * get_FloatSerializer_1() const { return ___FloatSerializer_1; }
	inline Serializer_1_t3074248596 ** get_address_of_FloatSerializer_1() { return &___FloatSerializer_1; }
	inline void set_FloatSerializer_1(Serializer_1_t3074248596 * value)
	{
		___FloatSerializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___FloatSerializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4FORMATTER_T2519996543_H
#ifndef ENUMERATOR_T3332884175_H
#define ENUMERATOR_T3332884175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,Sirenix.Serialization.IDictionaryKeyPathProvider>
struct  Enumerator_t3332884175 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::dictionary
	Dictionary_2_t3150707191 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::version
	int32_t ___version_2;
	// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::currentKey
	Type_t * ___currentKey_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3332884175, ___dictionary_0)); }
	inline Dictionary_2_t3150707191 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3150707191 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3150707191 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3332884175, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3332884175, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentKey_3() { return static_cast<int32_t>(offsetof(Enumerator_t3332884175, ___currentKey_3)); }
	inline Type_t * get_currentKey_3() const { return ___currentKey_3; }
	inline Type_t ** get_address_of_currentKey_3() { return &___currentKey_3; }
	inline void set_currentKey_3(Type_t * value)
	{
		___currentKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3332884175_H
#ifndef ENUMERATOR_T2754071505_H
#define ENUMERATOR_T2754071505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Enumerator<System.Type>
struct  Enumerator_t2754071505 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator::_set
	HashSet_1_t1048894234 * ____set_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.HashSet`1/Enumerator::_current
	Type_t * ____current_3;

public:
	inline static int32_t get_offset_of__set_0() { return static_cast<int32_t>(offsetof(Enumerator_t2754071505, ____set_0)); }
	inline HashSet_1_t1048894234 * get__set_0() const { return ____set_0; }
	inline HashSet_1_t1048894234 ** get_address_of__set_0() { return &____set_0; }
	inline void set__set_0(HashSet_1_t1048894234 * value)
	{
		____set_0 = value;
		Il2CppCodeGenWriteBarrier((&____set_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2754071505, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2754071505, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2754071505, ____current_3)); }
	inline Type_t * get__current_3() const { return ____current_3; }
	inline Type_t ** get_address_of__current_3() { return &____current_3; }
	inline void set__current_3(Type_t * value)
	{
		____current_3 = value;
		Il2CppCodeGenWriteBarrier((&____current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2754071505_H
#ifndef DECIMAL_T2948259380_H
#define DECIMAL_T2948259380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2948259380 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t2948259380_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t2770800703* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t2948259380  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t2948259380  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2948259380  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2948259380  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t2948259380  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t2948259380  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t2948259380  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t2770800703* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t2770800703** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t2770800703* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___Zero_7)); }
	inline Decimal_t2948259380  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t2948259380 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t2948259380  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___One_8)); }
	inline Decimal_t2948259380  get_One_8() const { return ___One_8; }
	inline Decimal_t2948259380 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t2948259380  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinusOne_9)); }
	inline Decimal_t2948259380  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t2948259380 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t2948259380  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValue_10)); }
	inline Decimal_t2948259380  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t2948259380 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t2948259380  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinValue_11)); }
	inline Decimal_t2948259380  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t2948259380 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t2948259380  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t2948259380  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t2948259380 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t2948259380  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t2948259380  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t2948259380 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t2948259380  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2948259380_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEANSERIALIZER_T790606778_H
#define BOOLEANSERIALIZER_T790606778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.BooleanSerializer
struct  BooleanSerializer_t790606778  : public Serializer_1_t1774269787
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANSERIALIZER_T790606778_H
#ifndef BYTESERIALIZER_T506145348_H
#define BYTESERIALIZER_T506145348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ByteSerializer
struct  ByteSerializer_t506145348  : public Serializer_1_t2811278198
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESERIALIZER_T506145348_H
#ifndef CHARSERIALIZER_T2198227356_H
#define CHARSERIALIZER_T2198227356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.CharSerializer
struct  CharSerializer_t2198227356  : public Serializer_1_t1016474996
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARSERIALIZER_T2198227356_H
#ifndef DATAFORMAT_T4143835455_H
#define DATAFORMAT_T4143835455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DataFormat
struct  DataFormat_t4143835455 
{
public:
	// System.Int32 Sirenix.Serialization.DataFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataFormat_t4143835455, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAFORMAT_T4143835455_H
#ifndef DECIMALSERIALIZER_T164561086_H
#define DECIMALSERIALIZER_T164561086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DecimalSerializer
struct  DecimalSerializer_t164561086  : public Serializer_1_t330273906
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALSERIALIZER_T164561086_H
#ifndef U3CGETPERSISTENTPATHKEYTYPESU3ED__14_T225224530_H
#define U3CGETPERSISTENTPATHKEYTYPESU3ED__14_T225224530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14
struct  U3CGetPersistentPathKeyTypesU3Ed__14_t225224530  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.HashSet`1/Enumerator<System.Type> Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>7__wrap1
	Enumerator_t2754071505  ___U3CU3E7__wrap1_3;
	// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,Sirenix.Serialization.IDictionaryKeyPathProvider> Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>7__wrap2
	Enumerator_t3332884175  ___U3CU3E7__wrap2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetPersistentPathKeyTypesU3Ed__14_t225224530, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetPersistentPathKeyTypesU3Ed__14_t225224530, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetPersistentPathKeyTypesU3Ed__14_t225224530, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetPersistentPathKeyTypesU3Ed__14_t225224530, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_t2754071505  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_t2754071505 * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_t2754071505  value)
	{
		___U3CU3E7__wrap1_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_4() { return static_cast<int32_t>(offsetof(U3CGetPersistentPathKeyTypesU3Ed__14_t225224530, ___U3CU3E7__wrap2_4)); }
	inline Enumerator_t3332884175  get_U3CU3E7__wrap2_4() const { return ___U3CU3E7__wrap2_4; }
	inline Enumerator_t3332884175 * get_address_of_U3CU3E7__wrap2_4() { return &___U3CU3E7__wrap2_4; }
	inline void set_U3CU3E7__wrap2_4(Enumerator_t3332884175  value)
	{
		___U3CU3E7__wrap2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPERSISTENTPATHKEYTYPESU3ED__14_T225224530_H
#ifndef DOUBLESERIALIZER_T2315099714_H
#define DOUBLESERIALIZER_T2315099714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DoubleSerializer
struct  DoubleSerializer_t2315099714  : public Serializer_1_t2271647185
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLESERIALIZER_T2315099714_H
#ifndef ENTRYTYPE_T2784073430_H
#define ENTRYTYPE_T2784073430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.EntryType
struct  EntryType_t2784073430 
{
public:
	// System.Byte Sirenix.Serialization.EntryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntryType_t2784073430, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRYTYPE_T2784073430_H
#ifndef ERRORHANDLINGPOLICY_T3882907225_H
#define ERRORHANDLINGPOLICY_T3882907225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ErrorHandlingPolicy
struct  ErrorHandlingPolicy_t3882907225 
{
public:
	// System.Int32 Sirenix.Serialization.ErrorHandlingPolicy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ErrorHandlingPolicy_t3882907225, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORHANDLINGPOLICY_T3882907225_H
#ifndef GUIDSERIALIZER_T3639950450_H
#define GUIDSERIALIZER_T3639950450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.GuidSerializer
struct  GuidSerializer_t3639950450  : public Serializer_1_t575547413
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDSERIALIZER_T3639950450_H
#ifndef INT16SERIALIZER_T2342241136_H
#define INT16SERIALIZER_T2342241136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Int16Serializer
struct  Int16Serializer_t2342241136  : public Serializer_1_t4229802209
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16SERIALIZER_T2342241136_H
#ifndef INT32SERIALIZER_T1822548190_H
#define INT32SERIALIZER_T1822548190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Int32Serializer
struct  Int32Serializer_t1822548190  : public Serializer_1_t332960279
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32SERIALIZER_T1822548190_H
#ifndef INT64SERIALIZER_T1751010994_H
#define INT64SERIALIZER_T1751010994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Int64Serializer
struct  Int64Serializer_t1751010994  : public Serializer_1_t1118581830
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64SERIALIZER_T1751010994_H
#ifndef INTPTRSERIALIZER_T2096452954_H
#define INTPTRSERIALIZER_T2096452954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.IntPtrSerializer
struct  IntPtrSerializer_t2096452954  : public Serializer_1_t2517132003
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTRSERIALIZER_T2096452954_H
#ifndef LOGGINGPOLICY_T1861080991_H
#define LOGGINGPOLICY_T1861080991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.LoggingPolicy
struct  LoggingPolicy_t1861080991 
{
public:
	// System.Int32 Sirenix.Serialization.LoggingPolicy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoggingPolicy_t1861080991, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGINGPOLICY_T1861080991_H
#ifndef PREFABMODIFICATIONTYPE_T3062331806_H
#define PREFABMODIFICATIONTYPE_T3062331806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.PrefabModificationType
struct  PrefabModificationType_t3062331806 
{
public:
	// System.Int32 Sirenix.Serialization.PrefabModificationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrefabModificationType_t3062331806, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABMODIFICATIONTYPE_T3062331806_H
#ifndef DECIMALBYTEUNION_T2790115858_H
#define DECIMALBYTEUNION_T2790115858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ProperBitConverter/DecimalByteUnion
struct  DecimalByteUnion_t2790115858 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte0
			uint8_t ___Byte0_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___Byte0_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte1_1_OffsetPadding[1];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte1
			uint8_t ___Byte1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte1_1_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___Byte1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte2_2_OffsetPadding[2];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte2
			uint8_t ___Byte2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte2_2_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___Byte2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte3_3_OffsetPadding[3];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte3
			uint8_t ___Byte3_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte3_3_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___Byte3_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte4_4_OffsetPadding[4];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte4
			uint8_t ___Byte4_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte4_4_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___Byte4_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte5_5_OffsetPadding[5];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte5
			uint8_t ___Byte5_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte5_5_OffsetPadding_forAlignmentOnly[5];
			uint8_t ___Byte5_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte6_6_OffsetPadding[6];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte6
			uint8_t ___Byte6_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte6_6_OffsetPadding_forAlignmentOnly[6];
			uint8_t ___Byte6_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte7_7_OffsetPadding[7];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte7
			uint8_t ___Byte7_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte7_7_OffsetPadding_forAlignmentOnly[7];
			uint8_t ___Byte7_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte8_8_OffsetPadding[8];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte8
			uint8_t ___Byte8_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte8_8_OffsetPadding_forAlignmentOnly[8];
			uint8_t ___Byte8_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte9_9_OffsetPadding[9];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte9
			uint8_t ___Byte9_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte9_9_OffsetPadding_forAlignmentOnly[9];
			uint8_t ___Byte9_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte10_10_OffsetPadding[10];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte10
			uint8_t ___Byte10_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte10_10_OffsetPadding_forAlignmentOnly[10];
			uint8_t ___Byte10_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte11_11_OffsetPadding[11];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte11
			uint8_t ___Byte11_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte11_11_OffsetPadding_forAlignmentOnly[11];
			uint8_t ___Byte11_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte12_12_OffsetPadding[12];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte12
			uint8_t ___Byte12_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte12_12_OffsetPadding_forAlignmentOnly[12];
			uint8_t ___Byte12_12_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte13_13_OffsetPadding[13];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte13
			uint8_t ___Byte13_13;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte13_13_OffsetPadding_forAlignmentOnly[13];
			uint8_t ___Byte13_13_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte14_14_OffsetPadding[14];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte14
			uint8_t ___Byte14_14;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte14_14_OffsetPadding_forAlignmentOnly[14];
			uint8_t ___Byte14_14_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte15_15_OffsetPadding[15];
			// System.Byte Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Byte15
			uint8_t ___Byte15_15;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte15_15_OffsetPadding_forAlignmentOnly[15];
			uint8_t ___Byte15_15_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Decimal Sirenix.Serialization.ProperBitConverter/DecimalByteUnion::Value
			Decimal_t2948259380  ___Value_16;
		};
		#pragma pack(pop, tp)
		struct
		{
			Decimal_t2948259380  ___Value_16_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_Byte0_0() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte0_0)); }
	inline uint8_t get_Byte0_0() const { return ___Byte0_0; }
	inline uint8_t* get_address_of_Byte0_0() { return &___Byte0_0; }
	inline void set_Byte0_0(uint8_t value)
	{
		___Byte0_0 = value;
	}

	inline static int32_t get_offset_of_Byte1_1() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte1_1)); }
	inline uint8_t get_Byte1_1() const { return ___Byte1_1; }
	inline uint8_t* get_address_of_Byte1_1() { return &___Byte1_1; }
	inline void set_Byte1_1(uint8_t value)
	{
		___Byte1_1 = value;
	}

	inline static int32_t get_offset_of_Byte2_2() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte2_2)); }
	inline uint8_t get_Byte2_2() const { return ___Byte2_2; }
	inline uint8_t* get_address_of_Byte2_2() { return &___Byte2_2; }
	inline void set_Byte2_2(uint8_t value)
	{
		___Byte2_2 = value;
	}

	inline static int32_t get_offset_of_Byte3_3() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte3_3)); }
	inline uint8_t get_Byte3_3() const { return ___Byte3_3; }
	inline uint8_t* get_address_of_Byte3_3() { return &___Byte3_3; }
	inline void set_Byte3_3(uint8_t value)
	{
		___Byte3_3 = value;
	}

	inline static int32_t get_offset_of_Byte4_4() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte4_4)); }
	inline uint8_t get_Byte4_4() const { return ___Byte4_4; }
	inline uint8_t* get_address_of_Byte4_4() { return &___Byte4_4; }
	inline void set_Byte4_4(uint8_t value)
	{
		___Byte4_4 = value;
	}

	inline static int32_t get_offset_of_Byte5_5() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte5_5)); }
	inline uint8_t get_Byte5_5() const { return ___Byte5_5; }
	inline uint8_t* get_address_of_Byte5_5() { return &___Byte5_5; }
	inline void set_Byte5_5(uint8_t value)
	{
		___Byte5_5 = value;
	}

	inline static int32_t get_offset_of_Byte6_6() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte6_6)); }
	inline uint8_t get_Byte6_6() const { return ___Byte6_6; }
	inline uint8_t* get_address_of_Byte6_6() { return &___Byte6_6; }
	inline void set_Byte6_6(uint8_t value)
	{
		___Byte6_6 = value;
	}

	inline static int32_t get_offset_of_Byte7_7() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte7_7)); }
	inline uint8_t get_Byte7_7() const { return ___Byte7_7; }
	inline uint8_t* get_address_of_Byte7_7() { return &___Byte7_7; }
	inline void set_Byte7_7(uint8_t value)
	{
		___Byte7_7 = value;
	}

	inline static int32_t get_offset_of_Byte8_8() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte8_8)); }
	inline uint8_t get_Byte8_8() const { return ___Byte8_8; }
	inline uint8_t* get_address_of_Byte8_8() { return &___Byte8_8; }
	inline void set_Byte8_8(uint8_t value)
	{
		___Byte8_8 = value;
	}

	inline static int32_t get_offset_of_Byte9_9() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte9_9)); }
	inline uint8_t get_Byte9_9() const { return ___Byte9_9; }
	inline uint8_t* get_address_of_Byte9_9() { return &___Byte9_9; }
	inline void set_Byte9_9(uint8_t value)
	{
		___Byte9_9 = value;
	}

	inline static int32_t get_offset_of_Byte10_10() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte10_10)); }
	inline uint8_t get_Byte10_10() const { return ___Byte10_10; }
	inline uint8_t* get_address_of_Byte10_10() { return &___Byte10_10; }
	inline void set_Byte10_10(uint8_t value)
	{
		___Byte10_10 = value;
	}

	inline static int32_t get_offset_of_Byte11_11() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte11_11)); }
	inline uint8_t get_Byte11_11() const { return ___Byte11_11; }
	inline uint8_t* get_address_of_Byte11_11() { return &___Byte11_11; }
	inline void set_Byte11_11(uint8_t value)
	{
		___Byte11_11 = value;
	}

	inline static int32_t get_offset_of_Byte12_12() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte12_12)); }
	inline uint8_t get_Byte12_12() const { return ___Byte12_12; }
	inline uint8_t* get_address_of_Byte12_12() { return &___Byte12_12; }
	inline void set_Byte12_12(uint8_t value)
	{
		___Byte12_12 = value;
	}

	inline static int32_t get_offset_of_Byte13_13() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte13_13)); }
	inline uint8_t get_Byte13_13() const { return ___Byte13_13; }
	inline uint8_t* get_address_of_Byte13_13() { return &___Byte13_13; }
	inline void set_Byte13_13(uint8_t value)
	{
		___Byte13_13 = value;
	}

	inline static int32_t get_offset_of_Byte14_14() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte14_14)); }
	inline uint8_t get_Byte14_14() const { return ___Byte14_14; }
	inline uint8_t* get_address_of_Byte14_14() { return &___Byte14_14; }
	inline void set_Byte14_14(uint8_t value)
	{
		___Byte14_14 = value;
	}

	inline static int32_t get_offset_of_Byte15_15() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Byte15_15)); }
	inline uint8_t get_Byte15_15() const { return ___Byte15_15; }
	inline uint8_t* get_address_of_Byte15_15() { return &___Byte15_15; }
	inline void set_Byte15_15(uint8_t value)
	{
		___Byte15_15 = value;
	}

	inline static int32_t get_offset_of_Value_16() { return static_cast<int32_t>(offsetof(DecimalByteUnion_t2790115858, ___Value_16)); }
	inline Decimal_t2948259380  get_Value_16() const { return ___Value_16; }
	inline Decimal_t2948259380 * get_address_of_Value_16() { return &___Value_16; }
	inline void set_Value_16(Decimal_t2948259380  value)
	{
		___Value_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALBYTEUNION_T2790115858_H
#ifndef GUIDBYTEUNION_T27368242_H
#define GUIDBYTEUNION_T27368242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.ProperBitConverter/GuidByteUnion
struct  GuidByteUnion_t27368242 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte0
			uint8_t ___Byte0_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___Byte0_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte1_1_OffsetPadding[1];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte1
			uint8_t ___Byte1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte1_1_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___Byte1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte2_2_OffsetPadding[2];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte2
			uint8_t ___Byte2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte2_2_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___Byte2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte3_3_OffsetPadding[3];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte3
			uint8_t ___Byte3_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte3_3_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___Byte3_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte4_4_OffsetPadding[4];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte4
			uint8_t ___Byte4_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte4_4_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___Byte4_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte5_5_OffsetPadding[5];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte5
			uint8_t ___Byte5_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte5_5_OffsetPadding_forAlignmentOnly[5];
			uint8_t ___Byte5_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte6_6_OffsetPadding[6];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte6
			uint8_t ___Byte6_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte6_6_OffsetPadding_forAlignmentOnly[6];
			uint8_t ___Byte6_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte7_7_OffsetPadding[7];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte7
			uint8_t ___Byte7_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte7_7_OffsetPadding_forAlignmentOnly[7];
			uint8_t ___Byte7_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte8_8_OffsetPadding[8];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte8
			uint8_t ___Byte8_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte8_8_OffsetPadding_forAlignmentOnly[8];
			uint8_t ___Byte8_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte9_9_OffsetPadding[9];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte9
			uint8_t ___Byte9_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte9_9_OffsetPadding_forAlignmentOnly[9];
			uint8_t ___Byte9_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte10_10_OffsetPadding[10];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte10
			uint8_t ___Byte10_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte10_10_OffsetPadding_forAlignmentOnly[10];
			uint8_t ___Byte10_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte11_11_OffsetPadding[11];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte11
			uint8_t ___Byte11_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte11_11_OffsetPadding_forAlignmentOnly[11];
			uint8_t ___Byte11_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte12_12_OffsetPadding[12];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte12
			uint8_t ___Byte12_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte12_12_OffsetPadding_forAlignmentOnly[12];
			uint8_t ___Byte12_12_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte13_13_OffsetPadding[13];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte13
			uint8_t ___Byte13_13;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte13_13_OffsetPadding_forAlignmentOnly[13];
			uint8_t ___Byte13_13_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte14_14_OffsetPadding[14];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte14
			uint8_t ___Byte14_14;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte14_14_OffsetPadding_forAlignmentOnly[14];
			uint8_t ___Byte14_14_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Byte15_15_OffsetPadding[15];
			// System.Byte Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Byte15
			uint8_t ___Byte15_15;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Byte15_15_OffsetPadding_forAlignmentOnly[15];
			uint8_t ___Byte15_15_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Guid Sirenix.Serialization.ProperBitConverter/GuidByteUnion::Value
			Guid_t  ___Value_16;
		};
		#pragma pack(pop, tp)
		struct
		{
			Guid_t  ___Value_16_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_Byte0_0() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte0_0)); }
	inline uint8_t get_Byte0_0() const { return ___Byte0_0; }
	inline uint8_t* get_address_of_Byte0_0() { return &___Byte0_0; }
	inline void set_Byte0_0(uint8_t value)
	{
		___Byte0_0 = value;
	}

	inline static int32_t get_offset_of_Byte1_1() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte1_1)); }
	inline uint8_t get_Byte1_1() const { return ___Byte1_1; }
	inline uint8_t* get_address_of_Byte1_1() { return &___Byte1_1; }
	inline void set_Byte1_1(uint8_t value)
	{
		___Byte1_1 = value;
	}

	inline static int32_t get_offset_of_Byte2_2() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte2_2)); }
	inline uint8_t get_Byte2_2() const { return ___Byte2_2; }
	inline uint8_t* get_address_of_Byte2_2() { return &___Byte2_2; }
	inline void set_Byte2_2(uint8_t value)
	{
		___Byte2_2 = value;
	}

	inline static int32_t get_offset_of_Byte3_3() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte3_3)); }
	inline uint8_t get_Byte3_3() const { return ___Byte3_3; }
	inline uint8_t* get_address_of_Byte3_3() { return &___Byte3_3; }
	inline void set_Byte3_3(uint8_t value)
	{
		___Byte3_3 = value;
	}

	inline static int32_t get_offset_of_Byte4_4() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte4_4)); }
	inline uint8_t get_Byte4_4() const { return ___Byte4_4; }
	inline uint8_t* get_address_of_Byte4_4() { return &___Byte4_4; }
	inline void set_Byte4_4(uint8_t value)
	{
		___Byte4_4 = value;
	}

	inline static int32_t get_offset_of_Byte5_5() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte5_5)); }
	inline uint8_t get_Byte5_5() const { return ___Byte5_5; }
	inline uint8_t* get_address_of_Byte5_5() { return &___Byte5_5; }
	inline void set_Byte5_5(uint8_t value)
	{
		___Byte5_5 = value;
	}

	inline static int32_t get_offset_of_Byte6_6() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte6_6)); }
	inline uint8_t get_Byte6_6() const { return ___Byte6_6; }
	inline uint8_t* get_address_of_Byte6_6() { return &___Byte6_6; }
	inline void set_Byte6_6(uint8_t value)
	{
		___Byte6_6 = value;
	}

	inline static int32_t get_offset_of_Byte7_7() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte7_7)); }
	inline uint8_t get_Byte7_7() const { return ___Byte7_7; }
	inline uint8_t* get_address_of_Byte7_7() { return &___Byte7_7; }
	inline void set_Byte7_7(uint8_t value)
	{
		___Byte7_7 = value;
	}

	inline static int32_t get_offset_of_Byte8_8() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte8_8)); }
	inline uint8_t get_Byte8_8() const { return ___Byte8_8; }
	inline uint8_t* get_address_of_Byte8_8() { return &___Byte8_8; }
	inline void set_Byte8_8(uint8_t value)
	{
		___Byte8_8 = value;
	}

	inline static int32_t get_offset_of_Byte9_9() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte9_9)); }
	inline uint8_t get_Byte9_9() const { return ___Byte9_9; }
	inline uint8_t* get_address_of_Byte9_9() { return &___Byte9_9; }
	inline void set_Byte9_9(uint8_t value)
	{
		___Byte9_9 = value;
	}

	inline static int32_t get_offset_of_Byte10_10() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte10_10)); }
	inline uint8_t get_Byte10_10() const { return ___Byte10_10; }
	inline uint8_t* get_address_of_Byte10_10() { return &___Byte10_10; }
	inline void set_Byte10_10(uint8_t value)
	{
		___Byte10_10 = value;
	}

	inline static int32_t get_offset_of_Byte11_11() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte11_11)); }
	inline uint8_t get_Byte11_11() const { return ___Byte11_11; }
	inline uint8_t* get_address_of_Byte11_11() { return &___Byte11_11; }
	inline void set_Byte11_11(uint8_t value)
	{
		___Byte11_11 = value;
	}

	inline static int32_t get_offset_of_Byte12_12() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte12_12)); }
	inline uint8_t get_Byte12_12() const { return ___Byte12_12; }
	inline uint8_t* get_address_of_Byte12_12() { return &___Byte12_12; }
	inline void set_Byte12_12(uint8_t value)
	{
		___Byte12_12 = value;
	}

	inline static int32_t get_offset_of_Byte13_13() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte13_13)); }
	inline uint8_t get_Byte13_13() const { return ___Byte13_13; }
	inline uint8_t* get_address_of_Byte13_13() { return &___Byte13_13; }
	inline void set_Byte13_13(uint8_t value)
	{
		___Byte13_13 = value;
	}

	inline static int32_t get_offset_of_Byte14_14() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte14_14)); }
	inline uint8_t get_Byte14_14() const { return ___Byte14_14; }
	inline uint8_t* get_address_of_Byte14_14() { return &___Byte14_14; }
	inline void set_Byte14_14(uint8_t value)
	{
		___Byte14_14 = value;
	}

	inline static int32_t get_offset_of_Byte15_15() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Byte15_15)); }
	inline uint8_t get_Byte15_15() const { return ___Byte15_15; }
	inline uint8_t* get_address_of_Byte15_15() { return &___Byte15_15; }
	inline void set_Byte15_15(uint8_t value)
	{
		___Byte15_15 = value;
	}

	inline static int32_t get_offset_of_Value_16() { return static_cast<int32_t>(offsetof(GuidByteUnion_t27368242, ___Value_16)); }
	inline Guid_t  get_Value_16() const { return ___Value_16; }
	inline Guid_t * get_address_of_Value_16() { return &___Value_16; }
	inline void set_Value_16(Guid_t  value)
	{
		___Value_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDBYTEUNION_T27368242_H
#ifndef SBYTESERIALIZER_T51836165_H
#define SBYTESERIALIZER_T51836165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SByteSerializer
struct  SByteSerializer_t51836165  : public Serializer_1_t3346559484
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SBYTESERIALIZER_T51836165_H
#ifndef SINGLESERIALIZER_T2369242016_H
#define SINGLESERIALIZER_T2369242016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SingleSerializer
struct  SingleSerializer_t2369242016  : public Serializer_1_t3074248596
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLESERIALIZER_T2369242016_H
#ifndef STRINGSERIALIZER_T2493313920_H
#define STRINGSERIALIZER_T2493313920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.StringSerializer
struct  StringSerializer_t2493313920  : public Serializer_1_t3524432511
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSERIALIZER_T2493313920_H
#ifndef UINT16SERIALIZER_T2923984882_H
#define UINT16SERIALIZER_T2923984882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UInt16Serializer
struct  UInt16Serializer_t2923984882  : public Serializer_1_t3854706780
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16SERIALIZER_T2923984882_H
#ifndef UINT32SERIALIZER_T428763452_H
#define UINT32SERIALIZER_T428763452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UInt32Serializer
struct  UInt32Serializer_t428763452  : public Serializer_1_t4237043800
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32SERIALIZER_T428763452_H
#ifndef UINT64SERIALIZER_T4258453427_H
#define UINT64SERIALIZER_T4258453427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UInt64Serializer
struct  UInt64Serializer_t4258453427  : public Serializer_1_t1516054618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64SERIALIZER_T4258453427_H
#ifndef UINTPTRSERIALIZER_T2734292566_H
#define UINTPTRSERIALIZER_T2734292566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UIntPtrSerializer
struct  UIntPtrSerializer_t2734292566  : public Serializer_1_t3723831450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTRSERIALIZER_T2734292566_H
#ifndef STREAMINGCONTEXTSTATES_T3580100459_H
#define STREAMINGCONTEXTSTATES_T3580100459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3580100459 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3580100459, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3580100459_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DEBUGCONTEXT_T3396391800_H
#define DEBUGCONTEXT_T3396391800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DebugContext
struct  DebugContext_t3396391800  : public RuntimeObject
{
public:
	// System.Object Sirenix.Serialization.DebugContext::LOCK
	RuntimeObject * ___LOCK_0;
	// Sirenix.Serialization.ILogger modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.DebugContext::logger
	RuntimeObject* ___logger_1;
	// Sirenix.Serialization.LoggingPolicy modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.DebugContext::loggingPolicy
	int32_t ___loggingPolicy_2;
	// Sirenix.Serialization.ErrorHandlingPolicy modreq(System.Runtime.CompilerServices.IsVolatile) Sirenix.Serialization.DebugContext::errorHandlingPolicy
	int32_t ___errorHandlingPolicy_3;

public:
	inline static int32_t get_offset_of_LOCK_0() { return static_cast<int32_t>(offsetof(DebugContext_t3396391800, ___LOCK_0)); }
	inline RuntimeObject * get_LOCK_0() const { return ___LOCK_0; }
	inline RuntimeObject ** get_address_of_LOCK_0() { return &___LOCK_0; }
	inline void set_LOCK_0(RuntimeObject * value)
	{
		___LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_0), value);
	}

	inline static int32_t get_offset_of_logger_1() { return static_cast<int32_t>(offsetof(DebugContext_t3396391800, ___logger_1)); }
	inline RuntimeObject* get_logger_1() const { return ___logger_1; }
	inline RuntimeObject** get_address_of_logger_1() { return &___logger_1; }
	inline void set_logger_1(RuntimeObject* value)
	{
		___logger_1 = value;
		Il2CppCodeGenWriteBarrier((&___logger_1), value);
	}

	inline static int32_t get_offset_of_loggingPolicy_2() { return static_cast<int32_t>(offsetof(DebugContext_t3396391800, ___loggingPolicy_2)); }
	inline int32_t get_loggingPolicy_2() const { return ___loggingPolicy_2; }
	inline int32_t* get_address_of_loggingPolicy_2() { return &___loggingPolicy_2; }
	inline void set_loggingPolicy_2(int32_t value)
	{
		___loggingPolicy_2 = value;
	}

	inline static int32_t get_offset_of_errorHandlingPolicy_3() { return static_cast<int32_t>(offsetof(DebugContext_t3396391800, ___errorHandlingPolicy_3)); }
	inline int32_t get_errorHandlingPolicy_3() const { return ___errorHandlingPolicy_3; }
	inline int32_t* get_address_of_errorHandlingPolicy_3() { return &___errorHandlingPolicy_3; }
	inline void set_errorHandlingPolicy_3(int32_t value)
	{
		___errorHandlingPolicy_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGCONTEXT_T3396391800_H
#ifndef PREFABMODIFICATION_T2072405548_H
#define PREFABMODIFICATION_T2072405548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.PrefabModification
struct  PrefabModification_t2072405548  : public RuntimeObject
{
public:
	// Sirenix.Serialization.PrefabModificationType Sirenix.Serialization.PrefabModification::ModificationType
	int32_t ___ModificationType_0;
	// System.String Sirenix.Serialization.PrefabModification::Path
	String_t* ___Path_1;
	// System.Collections.Generic.List`1<System.String> Sirenix.Serialization.PrefabModification::ReferencePaths
	List_1_t3319525431 * ___ReferencePaths_2;
	// System.Object Sirenix.Serialization.PrefabModification::ModifiedValue
	RuntimeObject * ___ModifiedValue_3;
	// System.Int32 Sirenix.Serialization.PrefabModification::NewLength
	int32_t ___NewLength_4;
	// System.Object[] Sirenix.Serialization.PrefabModification::DictionaryKeysAdded
	ObjectU5BU5D_t2843939325* ___DictionaryKeysAdded_5;
	// System.Object[] Sirenix.Serialization.PrefabModification::DictionaryKeysRemoved
	ObjectU5BU5D_t2843939325* ___DictionaryKeysRemoved_6;

public:
	inline static int32_t get_offset_of_ModificationType_0() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___ModificationType_0)); }
	inline int32_t get_ModificationType_0() const { return ___ModificationType_0; }
	inline int32_t* get_address_of_ModificationType_0() { return &___ModificationType_0; }
	inline void set_ModificationType_0(int32_t value)
	{
		___ModificationType_0 = value;
	}

	inline static int32_t get_offset_of_Path_1() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___Path_1)); }
	inline String_t* get_Path_1() const { return ___Path_1; }
	inline String_t** get_address_of_Path_1() { return &___Path_1; }
	inline void set_Path_1(String_t* value)
	{
		___Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___Path_1), value);
	}

	inline static int32_t get_offset_of_ReferencePaths_2() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___ReferencePaths_2)); }
	inline List_1_t3319525431 * get_ReferencePaths_2() const { return ___ReferencePaths_2; }
	inline List_1_t3319525431 ** get_address_of_ReferencePaths_2() { return &___ReferencePaths_2; }
	inline void set_ReferencePaths_2(List_1_t3319525431 * value)
	{
		___ReferencePaths_2 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencePaths_2), value);
	}

	inline static int32_t get_offset_of_ModifiedValue_3() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___ModifiedValue_3)); }
	inline RuntimeObject * get_ModifiedValue_3() const { return ___ModifiedValue_3; }
	inline RuntimeObject ** get_address_of_ModifiedValue_3() { return &___ModifiedValue_3; }
	inline void set_ModifiedValue_3(RuntimeObject * value)
	{
		___ModifiedValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___ModifiedValue_3), value);
	}

	inline static int32_t get_offset_of_NewLength_4() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___NewLength_4)); }
	inline int32_t get_NewLength_4() const { return ___NewLength_4; }
	inline int32_t* get_address_of_NewLength_4() { return &___NewLength_4; }
	inline void set_NewLength_4(int32_t value)
	{
		___NewLength_4 = value;
	}

	inline static int32_t get_offset_of_DictionaryKeysAdded_5() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___DictionaryKeysAdded_5)); }
	inline ObjectU5BU5D_t2843939325* get_DictionaryKeysAdded_5() const { return ___DictionaryKeysAdded_5; }
	inline ObjectU5BU5D_t2843939325** get_address_of_DictionaryKeysAdded_5() { return &___DictionaryKeysAdded_5; }
	inline void set_DictionaryKeysAdded_5(ObjectU5BU5D_t2843939325* value)
	{
		___DictionaryKeysAdded_5 = value;
		Il2CppCodeGenWriteBarrier((&___DictionaryKeysAdded_5), value);
	}

	inline static int32_t get_offset_of_DictionaryKeysRemoved_6() { return static_cast<int32_t>(offsetof(PrefabModification_t2072405548, ___DictionaryKeysRemoved_6)); }
	inline ObjectU5BU5D_t2843939325* get_DictionaryKeysRemoved_6() const { return ___DictionaryKeysRemoved_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_DictionaryKeysRemoved_6() { return &___DictionaryKeysRemoved_6; }
	inline void set_DictionaryKeysRemoved_6(ObjectU5BU5D_t2843939325* value)
	{
		___DictionaryKeysRemoved_6 = value;
		Il2CppCodeGenWriteBarrier((&___DictionaryKeysRemoved_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABMODIFICATION_T2072405548_H
#ifndef SERIALIZATIONDATA_T3163410965_H
#define SERIALIZATIONDATA_T3163410965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationData
struct  SerializationData_t3163410965 
{
public:
	// Sirenix.Serialization.DataFormat Sirenix.Serialization.SerializationData::SerializedFormat
	int32_t ___SerializedFormat_3;
	// System.Byte[] Sirenix.Serialization.SerializationData::SerializedBytes
	ByteU5BU5D_t4116647657* ___SerializedBytes_4;
	// System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.SerializationData::ReferencedUnityObjects
	List_1_t2103082695 * ___ReferencedUnityObjects_5;
	// System.String Sirenix.Serialization.SerializationData::SerializedBytesString
	String_t* ___SerializedBytesString_6;
	// UnityEngine.Object Sirenix.Serialization.SerializationData::Prefab
	Object_t631007953 * ___Prefab_7;
	// System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.SerializationData::PrefabModificationsReferencedUnityObjects
	List_1_t2103082695 * ___PrefabModificationsReferencedUnityObjects_8;
	// System.Collections.Generic.List`1<System.String> Sirenix.Serialization.SerializationData::PrefabModifications
	List_1_t3319525431 * ___PrefabModifications_9;
	// System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationData::SerializationNodes
	List_1_t1861728361 * ___SerializationNodes_10;

public:
	inline static int32_t get_offset_of_SerializedFormat_3() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializedFormat_3)); }
	inline int32_t get_SerializedFormat_3() const { return ___SerializedFormat_3; }
	inline int32_t* get_address_of_SerializedFormat_3() { return &___SerializedFormat_3; }
	inline void set_SerializedFormat_3(int32_t value)
	{
		___SerializedFormat_3 = value;
	}

	inline static int32_t get_offset_of_SerializedBytes_4() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializedBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_SerializedBytes_4() const { return ___SerializedBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_SerializedBytes_4() { return &___SerializedBytes_4; }
	inline void set_SerializedBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___SerializedBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___SerializedBytes_4), value);
	}

	inline static int32_t get_offset_of_ReferencedUnityObjects_5() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___ReferencedUnityObjects_5)); }
	inline List_1_t2103082695 * get_ReferencedUnityObjects_5() const { return ___ReferencedUnityObjects_5; }
	inline List_1_t2103082695 ** get_address_of_ReferencedUnityObjects_5() { return &___ReferencedUnityObjects_5; }
	inline void set_ReferencedUnityObjects_5(List_1_t2103082695 * value)
	{
		___ReferencedUnityObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedUnityObjects_5), value);
	}

	inline static int32_t get_offset_of_SerializedBytesString_6() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializedBytesString_6)); }
	inline String_t* get_SerializedBytesString_6() const { return ___SerializedBytesString_6; }
	inline String_t** get_address_of_SerializedBytesString_6() { return &___SerializedBytesString_6; }
	inline void set_SerializedBytesString_6(String_t* value)
	{
		___SerializedBytesString_6 = value;
		Il2CppCodeGenWriteBarrier((&___SerializedBytesString_6), value);
	}

	inline static int32_t get_offset_of_Prefab_7() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___Prefab_7)); }
	inline Object_t631007953 * get_Prefab_7() const { return ___Prefab_7; }
	inline Object_t631007953 ** get_address_of_Prefab_7() { return &___Prefab_7; }
	inline void set_Prefab_7(Object_t631007953 * value)
	{
		___Prefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_7), value);
	}

	inline static int32_t get_offset_of_PrefabModificationsReferencedUnityObjects_8() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___PrefabModificationsReferencedUnityObjects_8)); }
	inline List_1_t2103082695 * get_PrefabModificationsReferencedUnityObjects_8() const { return ___PrefabModificationsReferencedUnityObjects_8; }
	inline List_1_t2103082695 ** get_address_of_PrefabModificationsReferencedUnityObjects_8() { return &___PrefabModificationsReferencedUnityObjects_8; }
	inline void set_PrefabModificationsReferencedUnityObjects_8(List_1_t2103082695 * value)
	{
		___PrefabModificationsReferencedUnityObjects_8 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabModificationsReferencedUnityObjects_8), value);
	}

	inline static int32_t get_offset_of_PrefabModifications_9() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___PrefabModifications_9)); }
	inline List_1_t3319525431 * get_PrefabModifications_9() const { return ___PrefabModifications_9; }
	inline List_1_t3319525431 ** get_address_of_PrefabModifications_9() { return &___PrefabModifications_9; }
	inline void set_PrefabModifications_9(List_1_t3319525431 * value)
	{
		___PrefabModifications_9 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabModifications_9), value);
	}

	inline static int32_t get_offset_of_SerializationNodes_10() { return static_cast<int32_t>(offsetof(SerializationData_t3163410965, ___SerializationNodes_10)); }
	inline List_1_t1861728361 * get_SerializationNodes_10() const { return ___SerializationNodes_10; }
	inline List_1_t1861728361 ** get_address_of_SerializationNodes_10() { return &___SerializationNodes_10; }
	inline void set_SerializationNodes_10(List_1_t1861728361 * value)
	{
		___SerializationNodes_10 = value;
		Il2CppCodeGenWriteBarrier((&___SerializationNodes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sirenix.Serialization.SerializationData
struct SerializationData_t3163410965_marshaled_pinvoke
{
	int32_t ___SerializedFormat_3;
	uint8_t* ___SerializedBytes_4;
	List_1_t2103082695 * ___ReferencedUnityObjects_5;
	char* ___SerializedBytesString_6;
	Object_t631007953_marshaled_pinvoke ___Prefab_7;
	List_1_t2103082695 * ___PrefabModificationsReferencedUnityObjects_8;
	List_1_t3319525431 * ___PrefabModifications_9;
	List_1_t1861728361 * ___SerializationNodes_10;
};
// Native definition for COM marshalling of Sirenix.Serialization.SerializationData
struct SerializationData_t3163410965_marshaled_com
{
	int32_t ___SerializedFormat_3;
	uint8_t* ___SerializedBytes_4;
	List_1_t2103082695 * ___ReferencedUnityObjects_5;
	Il2CppChar* ___SerializedBytesString_6;
	Object_t631007953_marshaled_com* ___Prefab_7;
	List_1_t2103082695 * ___PrefabModificationsReferencedUnityObjects_8;
	List_1_t3319525431 * ___PrefabModifications_9;
	List_1_t1861728361 * ___SerializationNodes_10;
};
#endif // SERIALIZATIONDATA_T3163410965_H
#ifndef STREAMINGCONTEXT_T3711869237_H
#define STREAMINGCONTEXT_T3711869237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t3711869237 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T3711869237_H
#ifndef DESERIALIZATIONCONTEXT_T2871073323_H
#define DESERIALIZATIONCONTEXT_T2871073323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.DeserializationContext
struct  DeserializationContext_t2871073323  : public RuntimeObject
{
public:
	// Sirenix.Serialization.SerializationConfig Sirenix.Serialization.DeserializationContext::config
	SerializationConfig_t4249313805 * ___config_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object> Sirenix.Serialization.DeserializationContext::internalIdReferenceMap
	Dictionary_2_t1968819495 * ___internalIdReferenceMap_1;
	// System.Runtime.Serialization.StreamingContext Sirenix.Serialization.DeserializationContext::streamingContext
	StreamingContext_t3711869237  ___streamingContext_2;
	// System.Runtime.Serialization.IFormatterConverter Sirenix.Serialization.DeserializationContext::formatterConverter
	RuntimeObject* ___formatterConverter_3;
	// Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.DeserializationContext::<StringReferenceResolver>k__BackingField
	RuntimeObject* ___U3CStringReferenceResolverU3Ek__BackingField_4;
	// Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.DeserializationContext::<GuidReferenceResolver>k__BackingField
	RuntimeObject* ___U3CGuidReferenceResolverU3Ek__BackingField_5;
	// Sirenix.Serialization.IExternalIndexReferenceResolver Sirenix.Serialization.DeserializationContext::<IndexReferenceResolver>k__BackingField
	RuntimeObject* ___U3CIndexReferenceResolverU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___config_0)); }
	inline SerializationConfig_t4249313805 * get_config_0() const { return ___config_0; }
	inline SerializationConfig_t4249313805 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(SerializationConfig_t4249313805 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_internalIdReferenceMap_1() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___internalIdReferenceMap_1)); }
	inline Dictionary_2_t1968819495 * get_internalIdReferenceMap_1() const { return ___internalIdReferenceMap_1; }
	inline Dictionary_2_t1968819495 ** get_address_of_internalIdReferenceMap_1() { return &___internalIdReferenceMap_1; }
	inline void set_internalIdReferenceMap_1(Dictionary_2_t1968819495 * value)
	{
		___internalIdReferenceMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalIdReferenceMap_1), value);
	}

	inline static int32_t get_offset_of_streamingContext_2() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___streamingContext_2)); }
	inline StreamingContext_t3711869237  get_streamingContext_2() const { return ___streamingContext_2; }
	inline StreamingContext_t3711869237 * get_address_of_streamingContext_2() { return &___streamingContext_2; }
	inline void set_streamingContext_2(StreamingContext_t3711869237  value)
	{
		___streamingContext_2 = value;
	}

	inline static int32_t get_offset_of_formatterConverter_3() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___formatterConverter_3)); }
	inline RuntimeObject* get_formatterConverter_3() const { return ___formatterConverter_3; }
	inline RuntimeObject** get_address_of_formatterConverter_3() { return &___formatterConverter_3; }
	inline void set_formatterConverter_3(RuntimeObject* value)
	{
		___formatterConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___formatterConverter_3), value);
	}

	inline static int32_t get_offset_of_U3CStringReferenceResolverU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___U3CStringReferenceResolverU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CStringReferenceResolverU3Ek__BackingField_4() const { return ___U3CStringReferenceResolverU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CStringReferenceResolverU3Ek__BackingField_4() { return &___U3CStringReferenceResolverU3Ek__BackingField_4; }
	inline void set_U3CStringReferenceResolverU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CStringReferenceResolverU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringReferenceResolverU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CGuidReferenceResolverU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___U3CGuidReferenceResolverU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CGuidReferenceResolverU3Ek__BackingField_5() const { return ___U3CGuidReferenceResolverU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CGuidReferenceResolverU3Ek__BackingField_5() { return &___U3CGuidReferenceResolverU3Ek__BackingField_5; }
	inline void set_U3CGuidReferenceResolverU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CGuidReferenceResolverU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGuidReferenceResolverU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CIndexReferenceResolverU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DeserializationContext_t2871073323, ___U3CIndexReferenceResolverU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CIndexReferenceResolverU3Ek__BackingField_6() const { return ___U3CIndexReferenceResolverU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CIndexReferenceResolverU3Ek__BackingField_6() { return &___U3CIndexReferenceResolverU3Ek__BackingField_6; }
	inline void set_U3CIndexReferenceResolverU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CIndexReferenceResolverU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexReferenceResolverU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESERIALIZATIONCONTEXT_T2871073323_H
#ifndef SERIALIZATIONCONTEXT_T2406416806_H
#define SERIALIZATIONCONTEXT_T2406416806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.SerializationContext
struct  SerializationContext_t2406416806  : public RuntimeObject
{
public:
	// Sirenix.Serialization.SerializationConfig Sirenix.Serialization.SerializationContext::config
	SerializationConfig_t4249313805 * ___config_0;
	// System.Collections.Generic.Dictionary`2<System.Object,System.Int32> Sirenix.Serialization.SerializationContext::internalReferenceIdMap
	Dictionary_2_t3384741 * ___internalReferenceIdMap_1;
	// System.Runtime.Serialization.StreamingContext Sirenix.Serialization.SerializationContext::streamingContext
	StreamingContext_t3711869237  ___streamingContext_2;
	// System.Runtime.Serialization.IFormatterConverter Sirenix.Serialization.SerializationContext::formatterConverter
	RuntimeObject* ___formatterConverter_3;
	// Sirenix.Serialization.IExternalIndexReferenceResolver Sirenix.Serialization.SerializationContext::<IndexReferenceResolver>k__BackingField
	RuntimeObject* ___U3CIndexReferenceResolverU3Ek__BackingField_4;
	// Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.SerializationContext::<StringReferenceResolver>k__BackingField
	RuntimeObject* ___U3CStringReferenceResolverU3Ek__BackingField_5;
	// Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.SerializationContext::<GuidReferenceResolver>k__BackingField
	RuntimeObject* ___U3CGuidReferenceResolverU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___config_0)); }
	inline SerializationConfig_t4249313805 * get_config_0() const { return ___config_0; }
	inline SerializationConfig_t4249313805 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(SerializationConfig_t4249313805 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_internalReferenceIdMap_1() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___internalReferenceIdMap_1)); }
	inline Dictionary_2_t3384741 * get_internalReferenceIdMap_1() const { return ___internalReferenceIdMap_1; }
	inline Dictionary_2_t3384741 ** get_address_of_internalReferenceIdMap_1() { return &___internalReferenceIdMap_1; }
	inline void set_internalReferenceIdMap_1(Dictionary_2_t3384741 * value)
	{
		___internalReferenceIdMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalReferenceIdMap_1), value);
	}

	inline static int32_t get_offset_of_streamingContext_2() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___streamingContext_2)); }
	inline StreamingContext_t3711869237  get_streamingContext_2() const { return ___streamingContext_2; }
	inline StreamingContext_t3711869237 * get_address_of_streamingContext_2() { return &___streamingContext_2; }
	inline void set_streamingContext_2(StreamingContext_t3711869237  value)
	{
		___streamingContext_2 = value;
	}

	inline static int32_t get_offset_of_formatterConverter_3() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___formatterConverter_3)); }
	inline RuntimeObject* get_formatterConverter_3() const { return ___formatterConverter_3; }
	inline RuntimeObject** get_address_of_formatterConverter_3() { return &___formatterConverter_3; }
	inline void set_formatterConverter_3(RuntimeObject* value)
	{
		___formatterConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___formatterConverter_3), value);
	}

	inline static int32_t get_offset_of_U3CIndexReferenceResolverU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___U3CIndexReferenceResolverU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CIndexReferenceResolverU3Ek__BackingField_4() const { return ___U3CIndexReferenceResolverU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CIndexReferenceResolverU3Ek__BackingField_4() { return &___U3CIndexReferenceResolverU3Ek__BackingField_4; }
	inline void set_U3CIndexReferenceResolverU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CIndexReferenceResolverU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexReferenceResolverU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStringReferenceResolverU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___U3CStringReferenceResolverU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CStringReferenceResolverU3Ek__BackingField_5() const { return ___U3CStringReferenceResolverU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CStringReferenceResolverU3Ek__BackingField_5() { return &___U3CStringReferenceResolverU3Ek__BackingField_5; }
	inline void set_U3CStringReferenceResolverU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CStringReferenceResolverU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringReferenceResolverU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CGuidReferenceResolverU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SerializationContext_t2406416806, ___U3CGuidReferenceResolverU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CGuidReferenceResolverU3Ek__BackingField_6() const { return ___U3CGuidReferenceResolverU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CGuidReferenceResolverU3Ek__BackingField_6() { return &___U3CGuidReferenceResolverU3Ek__BackingField_6; }
	inline void set_U3CGuidReferenceResolverU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CGuidReferenceResolverU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGuidReferenceResolverU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONCONTEXT_T2406416806_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (BindTypeNameToTypeAttribute_t3668200987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3800[2] = 
{
	BindTypeNameToTypeAttribute_t3668200987::get_offset_of_NewType_0(),
	BindTypeNameToTypeAttribute_t3668200987::get_offset_of_OldTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (DefaultSerializationBinder_t116316556), -1, sizeof(DefaultSerializationBinder_t116316556_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3801[6] = 
{
	DefaultSerializationBinder_t116316556_StaticFields::get_offset_of_assemblyNameLookUp_1(),
	DefaultSerializationBinder_t116316556_StaticFields::get_offset_of_TYPEMAP_LOCK_2(),
	DefaultSerializationBinder_t116316556_StaticFields::get_offset_of_NAMEMAP_LOCK_3(),
	DefaultSerializationBinder_t116316556_StaticFields::get_offset_of_typeMap_4(),
	DefaultSerializationBinder_t116316556_StaticFields::get_offset_of_nameMap_5(),
	DefaultSerializationBinder_t116316556_StaticFields::get_offset_of_customTypeNameToTypeBindings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (DeserializationContext_t2871073323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3802[7] = 
{
	DeserializationContext_t2871073323::get_offset_of_config_0(),
	DeserializationContext_t2871073323::get_offset_of_internalIdReferenceMap_1(),
	DeserializationContext_t2871073323::get_offset_of_streamingContext_2(),
	DeserializationContext_t2871073323::get_offset_of_formatterConverter_3(),
	DeserializationContext_t2871073323::get_offset_of_U3CStringReferenceResolverU3Ek__BackingField_4(),
	DeserializationContext_t2871073323::get_offset_of_U3CGuidReferenceResolverU3Ek__BackingField_5(),
	DeserializationContext_t2871073323::get_offset_of_U3CIndexReferenceResolverU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (EntryType_t2784073430)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3803[18] = 
{
	EntryType_t2784073430::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (ExcludeDataFromInspectorAttribute_t1238714384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (NodeInfo_t1254526976)+ sizeof (RuntimeObject), -1, sizeof(NodeInfo_t1254526976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3810[6] = 
{
	NodeInfo_t1254526976_StaticFields::get_offset_of_Empty_0(),
	NodeInfo_t1254526976::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NodeInfo_t1254526976::get_offset_of_Id_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NodeInfo_t1254526976::get_offset_of_Type_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NodeInfo_t1254526976::get_offset_of_IsArray_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NodeInfo_t1254526976::get_offset_of_IsEmpty_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (PreviouslySerializedAsAttribute_t1757059606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3811[1] = 
{
	PreviouslySerializedAsAttribute_t1757059606::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (SerializationAbortException_t679951102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (SerializationConfig_t4249313805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3813[3] = 
{
	SerializationConfig_t4249313805::get_offset_of_LOCK_0(),
	SerializationConfig_t4249313805::get_offset_of_serializationPolicy_1(),
	SerializationConfig_t4249313805::get_offset_of_debugContext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (DebugContext_t3396391800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3814[4] = 
{
	DebugContext_t3396391800::get_offset_of_LOCK_0(),
	DebugContext_t3396391800::get_offset_of_logger_1(),
	DebugContext_t3396391800::get_offset_of_loggingPolicy_2(),
	DebugContext_t3396391800::get_offset_of_errorHandlingPolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (SerializationContext_t2406416806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3815[7] = 
{
	SerializationContext_t2406416806::get_offset_of_config_0(),
	SerializationContext_t2406416806::get_offset_of_internalReferenceIdMap_1(),
	SerializationContext_t2406416806::get_offset_of_streamingContext_2(),
	SerializationContext_t2406416806::get_offset_of_formatterConverter_3(),
	SerializationContext_t2406416806::get_offset_of_U3CIndexReferenceResolverU3Ek__BackingField_4(),
	SerializationContext_t2406416806::get_offset_of_U3CStringReferenceResolverU3Ek__BackingField_5(),
	SerializationContext_t2406416806::get_offset_of_U3CGuidReferenceResolverU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (OdinSerializeAttribute_t3257861414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (TwoWaySerializationBinder_t4250048501), -1, sizeof(TwoWaySerializationBinder_t4250048501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3817[1] = 
{
	TwoWaySerializationBinder_t4250048501_StaticFields::get_offset_of_Default_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (SerializationPolicies_t1787867284), -1, sizeof(SerializationPolicies_t1787867284_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3818[4] = 
{
	SerializationPolicies_t1787867284_StaticFields::get_offset_of_LOCK_0(),
	SerializationPolicies_t1787867284_StaticFields::get_offset_of_everythingPolicy_1(),
	SerializationPolicies_t1787867284_StaticFields::get_offset_of_unityPolicy_2(),
	SerializationPolicies_t1787867284_StaticFields::get_offset_of_strictPolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (U3CU3Ec_t2078298886), -1, sizeof(U3CU3Ec_t2078298886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3819[3] = 
{
	U3CU3Ec_t2078298886_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2078298886_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
	U3CU3Ec_t2078298886_StaticFields::get_offset_of_U3CU3E9__10_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (U3CU3Ec__DisplayClass8_0_t478529023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3820[1] = 
{
	U3CU3Ec__DisplayClass8_0_t478529023::get_offset_of_tupleInterface_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (BooleanSerializer_t790606778), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (ByteSerializer_t506145348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (CharSerializer_t2198227356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (DecimalSerializer_t164561086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (DoubleSerializer_t2315099714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (GuidSerializer_t3639950450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (Int16Serializer_t2342241136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (Int32Serializer_t1822548190), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (Int64Serializer_t1751010994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (IntPtrSerializer_t2096452954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (SByteSerializer_t51836165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (Serializer_t962918574), -1, sizeof(Serializer_t962918574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3834[3] = 
{
	Serializer_t962918574_StaticFields::get_offset_of_PrimitiveReaderWriterTypes_0(),
	Serializer_t962918574_StaticFields::get_offset_of_LOCK_1(),
	Serializer_t962918574_StaticFields::get_offset_of_ReaderWriterCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (SingleSerializer_t2369242016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (StringSerializer_t2493313920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (UInt16Serializer_t2923984882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (UInt32Serializer_t428763452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (UInt64Serializer_t4258453427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (UIntPtrSerializer_t2734292566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (AnimationCurveFormatter_t513376465), -1, sizeof(AnimationCurveFormatter_t513376465_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3842[2] = 
{
	AnimationCurveFormatter_t513376465_StaticFields::get_offset_of_KeyframeSerializer_1(),
	AnimationCurveFormatter_t513376465_StaticFields::get_offset_of_WrapModeSerializer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (BoundsFormatter_t3935229287), -1, sizeof(BoundsFormatter_t3935229287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3843[1] = 
{
	BoundsFormatter_t3935229287_StaticFields::get_offset_of_Vector3Serializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (Color32Formatter_t348647114), -1, sizeof(Color32Formatter_t348647114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3844[1] = 
{
	Color32Formatter_t348647114_StaticFields::get_offset_of_ByteSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (ColorBlockFormatter_t831895006), -1, sizeof(ColorBlockFormatter_t831895006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3845[2] = 
{
	ColorBlockFormatter_t831895006_StaticFields::get_offset_of_FloatSerializer_1(),
	ColorBlockFormatter_t831895006_StaticFields::get_offset_of_ColorSerializer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (ColorFormatter_t3218611348), -1, sizeof(ColorFormatter_t3218611348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3846[1] = 
{
	ColorFormatter_t3218611348_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (CoroutineFormatter_t1506639270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (RegisterDictionaryKeyPathProviderAttribute_t2832569773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3851[1] = 
{
	RegisterDictionaryKeyPathProviderAttribute_t2832569773::get_offset_of_ProviderType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (Vector2DictionaryKeyPathProvider_t1191773922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (Vector3DictionaryKeyPathProvider_t2227500205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (Vector4DictionaryKeyPathProvider_t4213655263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (GradientAlphaKeyFormatter_t2425658533), -1, sizeof(GradientAlphaKeyFormatter_t2425658533_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3855[1] = 
{
	GradientAlphaKeyFormatter_t2425658533_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (GradientColorKeyFormatter_t645871939), -1, sizeof(GradientColorKeyFormatter_t645871939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3856[2] = 
{
	GradientColorKeyFormatter_t645871939_StaticFields::get_offset_of_ColorSerializer_1(),
	GradientColorKeyFormatter_t645871939_StaticFields::get_offset_of_FloatSerializer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (GradientFormatter_t3582296489), -1, sizeof(GradientFormatter_t3582296489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3857[4] = 
{
	GradientFormatter_t3582296489_StaticFields::get_offset_of_AlphaKeysSerializer_1(),
	GradientFormatter_t3582296489_StaticFields::get_offset_of_ColorKeysSerializer_2(),
	GradientFormatter_t3582296489_StaticFields::get_offset_of_ModeProperty_3(),
	GradientFormatter_t3582296489_StaticFields::get_offset_of_EnumSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (KeyframeFormatter_t2209210672), -1, sizeof(KeyframeFormatter_t2209210672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3860[4] = 
{
	KeyframeFormatter_t2209210672_StaticFields::get_offset_of_FloatSerializer_1(),
	KeyframeFormatter_t2209210672_StaticFields::get_offset_of_IntSerializer_2(),
	KeyframeFormatter_t2209210672_StaticFields::get_offset_of_Is_In_2018_1_Or_Above_3(),
	KeyframeFormatter_t2209210672_StaticFields::get_offset_of_Formatter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (LayerMaskFormatter_t2206937223), -1, sizeof(LayerMaskFormatter_t2206937223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3861[1] = 
{
	LayerMaskFormatter_t2206937223_StaticFields::get_offset_of_IntSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (QuaternionFormatter_t251372450), -1, sizeof(QuaternionFormatter_t251372450_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3862[1] = 
{
	QuaternionFormatter_t251372450_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (RectFormatter_t2640669641), -1, sizeof(RectFormatter_t2640669641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3863[1] = 
{
	RectFormatter_t2640669641_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (SerializationData_t3163410965)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[11] = 
{
	0,
	0,
	0,
	SerializationData_t3163410965::get_offset_of_SerializedFormat_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_SerializedBytes_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_ReferencedUnityObjects_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_SerializedBytesString_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_Prefab_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_PrefabModificationsReferencedUnityObjects_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_PrefabModifications_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SerializationData_t3163410965::get_offset_of_SerializationNodes_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (UnityReferenceResolver_t2497123750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3866[2] = 
{
	UnityReferenceResolver_t2497123750::get_offset_of_referenceIndexMapping_0(),
	UnityReferenceResolver_t2497123750::get_offset_of_referencedUnityObjects_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (UnitySerializationInitializer_t3556939650), -1, sizeof(UnitySerializationInitializer_t3556939650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3867[2] = 
{
	UnitySerializationInitializer_t3556939650_StaticFields::get_offset_of_LOCK_0(),
	UnitySerializationInitializer_t3556939650_StaticFields::get_offset_of_initialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (Vector2Formatter_t2520006045), -1, sizeof(Vector2Formatter_t2520006045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3868[1] = 
{
	Vector2Formatter_t2520006045_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (Vector3Formatter_t2520007008), -1, sizeof(Vector3Formatter_t2520007008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3869[1] = 
{
	Vector3Formatter_t2520007008_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (Vector4Formatter_t2519996543), -1, sizeof(Vector4Formatter_t2519996543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3870[1] = 
{
	Vector4Formatter_t2519996543_StaticFields::get_offset_of_FloatSerializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3871[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (CachedMemoryStream_t1924712323), -1, sizeof(CachedMemoryStream_t1924712323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3872[3] = 
{
	CachedMemoryStream_t1924712323_StaticFields::get_offset_of_InitialCapacity_0(),
	CachedMemoryStream_t1924712323_StaticFields::get_offset_of_MaxCapacity_1(),
	CachedMemoryStream_t1924712323::get_offset_of_memoryStream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (EmittedAssemblyAttribute_t416105211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (FormatterLocator_t3795817683), -1, sizeof(FormatterLocator_t3795817683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3874[5] = 
{
	FormatterLocator_t3795817683_StaticFields::get_offset_of_LOCK_0(),
	FormatterLocator_t3795817683_StaticFields::get_offset_of_FormatterInstances_1(),
	FormatterLocator_t3795817683_StaticFields::get_offset_of_TypeFormatterMap_2(),
	FormatterLocator_t3795817683_StaticFields::get_offset_of_FormatterLocators_3(),
	FormatterLocator_t3795817683_StaticFields::get_offset_of_FormatterInfos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (FormatterInfo_t2891647851)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3875[4] = 
{
	FormatterInfo_t2891647851::get_offset_of_FormatterType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FormatterInfo_t2891647851::get_offset_of_TargetType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FormatterInfo_t2891647851::get_offset_of_AskIfCanFormatTypes_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FormatterInfo_t2891647851::get_offset_of_Priority_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (FormatterLocatorInfo_t371893538)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[2] = 
{
	FormatterLocatorInfo_t371893538::get_offset_of_LocatorInstance_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FormatterLocatorInfo_t371893538::get_offset_of_Priority_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (U3CU3Ec_t3893634997), -1, sizeof(U3CU3Ec_t3893634997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3877[2] = 
{
	U3CU3Ec_t3893634997_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3893634997_StaticFields::get_offset_of_U3CU3E9__13_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[9] = 
{
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_type_3(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CargU3E5__1_5(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3E7__wrap1_6(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3E7__wrap2_7(),
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t1172278837::get_offset_of_U3CU3E7__wrap3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (FormatterUtilities_t2227968676), -1, sizeof(FormatterUtilities_t2227968676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3879[6] = 
{
	FormatterUtilities_t2227968676_StaticFields::get_offset_of_MemberArrayCache_0(),
	FormatterUtilities_t2227968676_StaticFields::get_offset_of_MemberMapCache_1(),
	FormatterUtilities_t2227968676_StaticFields::get_offset_of_LOCK_2(),
	FormatterUtilities_t2227968676_StaticFields::get_offset_of_PrimitiveArrayTypes_3(),
	FormatterUtilities_t2227968676_StaticFields::get_offset_of_UnityObjectRuntimeErrorStringField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (U3CU3Ec_t1396354011), -1, sizeof(U3CU3Ec_t1396354011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3880[4] = 
{
	U3CU3Ec_t1396354011_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1396354011_StaticFields::get_offset_of_U3CU3E9__15_0_1(),
	U3CU3Ec_t1396354011_StaticFields::get_offset_of_U3CU3E9__15_1_2(),
	U3CU3Ec_t1396354011_StaticFields::get_offset_of_U3CU3E9__16_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (U3CU3Ec__DisplayClass16_0_t3363221677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3881[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3363221677::get_offset_of_member_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (DictionaryKeyUtility_t927095224), -1, sizeof(DictionaryKeyUtility_t927095224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3882[9] = 
{
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_GetSupportedDictionaryKeyTypesResults_0(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_BaseSupportedDictionaryKeyTypes_1(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_AllowedSpecialKeyStrChars_2(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_TypeToKeyPathProviders_3(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_IDToKeyPathProviders_4(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_ProviderToID_5(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_ObjectsToTempKeys_6(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_TempKeysToObjects_7(),
	DictionaryKeyUtility_t927095224_StaticFields::get_offset_of_tempKeyCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (U3CU3Ec__DisplayClass12_0_t1051472719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3886[1] = 
{
	U3CU3Ec__DisplayClass12_0_t1051472719::get_offset_of_ass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (U3CU3Ec_t3635135514), -1, sizeof(U3CU3Ec_t3635135514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3887[1] = 
{
	U3CU3Ec_t3635135514_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (U3CGetPersistentPathKeyTypesU3Ed__14_t225224530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3888[5] = 
{
	U3CGetPersistentPathKeyTypesU3Ed__14_t225224530::get_offset_of_U3CU3E1__state_0(),
	U3CGetPersistentPathKeyTypesU3Ed__14_t225224530::get_offset_of_U3CU3E2__current_1(),
	U3CGetPersistentPathKeyTypesU3Ed__14_t225224530::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetPersistentPathKeyTypesU3Ed__14_t225224530::get_offset_of_U3CU3E7__wrap1_3(),
	U3CGetPersistentPathKeyTypesU3Ed__14_t225224530::get_offset_of_U3CU3E7__wrap2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (PrefabModification_t2072405548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3889[7] = 
{
	PrefabModification_t2072405548::get_offset_of_ModificationType_0(),
	PrefabModification_t2072405548::get_offset_of_Path_1(),
	PrefabModification_t2072405548::get_offset_of_ReferencePaths_2(),
	PrefabModification_t2072405548::get_offset_of_ModifiedValue_3(),
	PrefabModification_t2072405548::get_offset_of_NewLength_4(),
	PrefabModification_t2072405548::get_offset_of_DictionaryKeysAdded_5(),
	PrefabModification_t2072405548::get_offset_of_DictionaryKeysRemoved_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (U3CU3Ec_t2854026858), -1, sizeof(U3CU3Ec_t2854026858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3890[3] = 
{
	U3CU3Ec_t2854026858_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2854026858_StaticFields::get_offset_of_U3CU3E9__13_0_1(),
	U3CU3Ec_t2854026858_StaticFields::get_offset_of_U3CU3E9__16_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (PrefabModificationType_t3062331806)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3891[4] = 
{
	PrefabModificationType_t3062331806::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (ProperBitConverter_t1060665852), -1, sizeof(ProperBitConverter_t1060665852_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3892[3] = 
{
	ProperBitConverter_t1060665852_StaticFields::get_offset_of_ByteToHexCharLookupLowerCase_0(),
	ProperBitConverter_t1060665852_StaticFields::get_offset_of_ByteToHexCharLookupUpperCase_1(),
	ProperBitConverter_t1060665852_StaticFields::get_offset_of_HexToByteLookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (SingleByteUnion_t1581541998)+ sizeof (RuntimeObject), sizeof(SingleByteUnion_t1581541998 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3893[5] = 
{
	SingleByteUnion_t1581541998::get_offset_of_Byte0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SingleByteUnion_t1581541998::get_offset_of_Byte1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SingleByteUnion_t1581541998::get_offset_of_Byte2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SingleByteUnion_t1581541998::get_offset_of_Byte3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SingleByteUnion_t1581541998::get_offset_of_Value_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (DoubleByteUnion_t2294834964)+ sizeof (RuntimeObject), sizeof(DoubleByteUnion_t2294834964 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3894[9] = 
{
	DoubleByteUnion_t2294834964::get_offset_of_Byte0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte4_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte5_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte6_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Byte7_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleByteUnion_t2294834964::get_offset_of_Value_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (DecimalByteUnion_t2790115858)+ sizeof (RuntimeObject), sizeof(DecimalByteUnion_t2790115858 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3895[17] = 
{
	DecimalByteUnion_t2790115858::get_offset_of_Byte0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte4_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte5_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte6_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte7_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte8_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte9_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte10_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte11_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte12_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte13_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte14_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Byte15_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalByteUnion_t2790115858::get_offset_of_Value_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (GuidByteUnion_t27368242)+ sizeof (RuntimeObject), sizeof(GuidByteUnion_t27368242 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3896[17] = 
{
	GuidByteUnion_t27368242::get_offset_of_Byte0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte4_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte5_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte6_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte7_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte8_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte9_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte10_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte11_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte12_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte13_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte14_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Byte15_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GuidByteUnion_t27368242::get_offset_of_Value_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (SerializationUtility_t407367459), -1, sizeof(SerializationUtility_t407367459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3897[2] = 
{
	SerializationUtility_t407367459_StaticFields::get_offset_of_CACHE_LOCK_0(),
	SerializationUtility_t407367459_StaticFields::get_offset_of_CacheMappings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (UnitySerializationUtility_t2251762831), -1, sizeof(UnitySerializationUtility_t2251762831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3898[7] = 
{
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityReaders_0(),
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityWriters_1(),
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityMemberGetters_2(),
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityMemberSetters_3(),
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityWillSerializeMembersCache_4(),
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityWillSerializeTypesCache_5(),
	UnitySerializationUtility_t2251762831_StaticFields::get_offset_of_UnityNeverSerializesTypes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (U3CU3Ec_t1820010292), -1, sizeof(U3CU3Ec_t1820010292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3899[3] = 
{
	U3CU3Ec_t1820010292_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1820010292_StaticFields::get_offset_of_U3CU3E9__20_0_1(),
	U3CU3Ec_t1820010292_StaticFields::get_offset_of_U3CU3E9__21_0_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
