﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate>
struct DoubleLookupDictionary_3_t3686776144;
// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>>
struct DoubleLookupDictionary_3_t650546409;
// Sirenix.Serialization.Utilities.ImmutableList
struct ImmutableList_t1819414204;
// Sirenix.Serialization.Utilities.ImmutableList`1<System.Reflection.Assembly>
struct ImmutableList_1_t1522597721;
// Sirenix.Serialization.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr>
struct ValueGetter_2_t1938735483;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,Sirenix.Serialization.Utilities.AssemblyTypeFlags>
struct Dictionary_2_t4012063963;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>>
struct Dictionary_2_t3493241298;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t4291797753;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t633324528;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>
struct IEnumerator_1_t3812572209;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t1279540245;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.IList
struct IList_t2094931216;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1302094432;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// Uniject.IUtil
struct IUtil_t1069285358;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.Purchasing.IUnityCallback
struct IUnityCallback_t3880085028;




#ifndef U3CMODULEU3E_T692745574_H
#define U3CMODULEU3E_T692745574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745574 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745574_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T4008831682_H
#define U3CU3EC__DISPLAYCLASS27_0_T4008831682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t4008831682  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass27_0::member
	MemberInfo_t * ___member_0;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t4008831682, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T4008831682_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T3961777515_H
#define U3CU3EC__DISPLAYCLASS28_0_T3961777515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t3961777515  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass28_0::member
	MemberInfo_t * ___member_0;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t3961777515, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T3961777515_H
#ifndef ASSEMBLYUTILITIES_T2715239875_H
#define ASSEMBLYUTILITIES_T2715239875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.AssemblyUtilities
struct  AssemblyUtilities_t2715239875  : public RuntimeObject
{
public:

public:
};

struct AssemblyUtilities_t2715239875_StaticFields
{
public:
	// System.String[] Sirenix.Serialization.Utilities.AssemblyUtilities::userAssemblyPrefixes
	StringU5BU5D_t1281789340* ___userAssemblyPrefixes_0;
	// System.String[] Sirenix.Serialization.Utilities.AssemblyUtilities::pluginAssemblyPrefixes
	StringU5BU5D_t1281789340* ___pluginAssemblyPrefixes_1;
	// System.Reflection.Assembly Sirenix.Serialization.Utilities.AssemblyUtilities::unityEngineAssembly
	Assembly_t * ___unityEngineAssembly_2;
	// System.Object Sirenix.Serialization.Utilities.AssemblyUtilities::LOCK
	RuntimeObject * ___LOCK_3;
	// System.IO.DirectoryInfo Sirenix.Serialization.Utilities.AssemblyUtilities::projectFolderDirectory
	DirectoryInfo_t35957480 * ___projectFolderDirectory_4;
	// System.IO.DirectoryInfo Sirenix.Serialization.Utilities.AssemblyUtilities::scriptAssembliesDirectory
	DirectoryInfo_t35957480 * ___scriptAssembliesDirectory_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::stringTypeLookup
	Dictionary_2_t2269201059 * ___stringTypeLookup_6;
	// System.Collections.Generic.Dictionary`2<System.Reflection.Assembly,Sirenix.Serialization.Utilities.AssemblyTypeFlags> Sirenix.Serialization.Utilities.AssemblyUtilities::assemblyTypeFlagLookup
	Dictionary_2_t4012063963 * ___assemblyTypeFlagLookup_7;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::allAssemblies
	List_1_t1279540245 * ___allAssemblies_8;
	// Sirenix.Serialization.Utilities.ImmutableList`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::allAssembliesImmutable
	ImmutableList_1_t1522597721 * ___allAssembliesImmutable_9;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::userAssemblies
	List_1_t1279540245 * ___userAssemblies_10;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::userEditorAssemblies
	List_1_t1279540245 * ___userEditorAssemblies_11;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::pluginAssemblies
	List_1_t1279540245 * ___pluginAssemblies_12;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::pluginEditorAssemblies
	List_1_t1279540245 * ___pluginEditorAssemblies_13;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::unityAssemblies
	List_1_t1279540245 * ___unityAssemblies_14;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::unityEditorAssemblies
	List_1_t1279540245 * ___unityEditorAssemblies_15;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> Sirenix.Serialization.Utilities.AssemblyUtilities::otherAssemblies
	List_1_t1279540245 * ___otherAssemblies_16;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::userTypes
	List_1_t3956019502 * ___userTypes_17;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::userEditorTypes
	List_1_t3956019502 * ___userEditorTypes_18;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::pluginTypes
	List_1_t3956019502 * ___pluginTypes_19;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::pluginEditorTypes
	List_1_t3956019502 * ___pluginEditorTypes_20;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::unityTypes
	List_1_t3956019502 * ___unityTypes_21;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::unityEditorTypes
	List_1_t3956019502 * ___unityEditorTypes_22;
	// System.Collections.Generic.List`1<System.Type> Sirenix.Serialization.Utilities.AssemblyUtilities::otherTypes
	List_1_t3956019502 * ___otherTypes_23;
	// System.String Sirenix.Serialization.Utilities.AssemblyUtilities::dataPath
	String_t* ___dataPath_24;
	// System.String Sirenix.Serialization.Utilities.AssemblyUtilities::scriptAssembliesPath
	String_t* ___scriptAssembliesPath_25;

public:
	inline static int32_t get_offset_of_userAssemblyPrefixes_0() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___userAssemblyPrefixes_0)); }
	inline StringU5BU5D_t1281789340* get_userAssemblyPrefixes_0() const { return ___userAssemblyPrefixes_0; }
	inline StringU5BU5D_t1281789340** get_address_of_userAssemblyPrefixes_0() { return &___userAssemblyPrefixes_0; }
	inline void set_userAssemblyPrefixes_0(StringU5BU5D_t1281789340* value)
	{
		___userAssemblyPrefixes_0 = value;
		Il2CppCodeGenWriteBarrier((&___userAssemblyPrefixes_0), value);
	}

	inline static int32_t get_offset_of_pluginAssemblyPrefixes_1() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___pluginAssemblyPrefixes_1)); }
	inline StringU5BU5D_t1281789340* get_pluginAssemblyPrefixes_1() const { return ___pluginAssemblyPrefixes_1; }
	inline StringU5BU5D_t1281789340** get_address_of_pluginAssemblyPrefixes_1() { return &___pluginAssemblyPrefixes_1; }
	inline void set_pluginAssemblyPrefixes_1(StringU5BU5D_t1281789340* value)
	{
		___pluginAssemblyPrefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___pluginAssemblyPrefixes_1), value);
	}

	inline static int32_t get_offset_of_unityEngineAssembly_2() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___unityEngineAssembly_2)); }
	inline Assembly_t * get_unityEngineAssembly_2() const { return ___unityEngineAssembly_2; }
	inline Assembly_t ** get_address_of_unityEngineAssembly_2() { return &___unityEngineAssembly_2; }
	inline void set_unityEngineAssembly_2(Assembly_t * value)
	{
		___unityEngineAssembly_2 = value;
		Il2CppCodeGenWriteBarrier((&___unityEngineAssembly_2), value);
	}

	inline static int32_t get_offset_of_LOCK_3() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___LOCK_3)); }
	inline RuntimeObject * get_LOCK_3() const { return ___LOCK_3; }
	inline RuntimeObject ** get_address_of_LOCK_3() { return &___LOCK_3; }
	inline void set_LOCK_3(RuntimeObject * value)
	{
		___LOCK_3 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_3), value);
	}

	inline static int32_t get_offset_of_projectFolderDirectory_4() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___projectFolderDirectory_4)); }
	inline DirectoryInfo_t35957480 * get_projectFolderDirectory_4() const { return ___projectFolderDirectory_4; }
	inline DirectoryInfo_t35957480 ** get_address_of_projectFolderDirectory_4() { return &___projectFolderDirectory_4; }
	inline void set_projectFolderDirectory_4(DirectoryInfo_t35957480 * value)
	{
		___projectFolderDirectory_4 = value;
		Il2CppCodeGenWriteBarrier((&___projectFolderDirectory_4), value);
	}

	inline static int32_t get_offset_of_scriptAssembliesDirectory_5() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___scriptAssembliesDirectory_5)); }
	inline DirectoryInfo_t35957480 * get_scriptAssembliesDirectory_5() const { return ___scriptAssembliesDirectory_5; }
	inline DirectoryInfo_t35957480 ** get_address_of_scriptAssembliesDirectory_5() { return &___scriptAssembliesDirectory_5; }
	inline void set_scriptAssembliesDirectory_5(DirectoryInfo_t35957480 * value)
	{
		___scriptAssembliesDirectory_5 = value;
		Il2CppCodeGenWriteBarrier((&___scriptAssembliesDirectory_5), value);
	}

	inline static int32_t get_offset_of_stringTypeLookup_6() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___stringTypeLookup_6)); }
	inline Dictionary_2_t2269201059 * get_stringTypeLookup_6() const { return ___stringTypeLookup_6; }
	inline Dictionary_2_t2269201059 ** get_address_of_stringTypeLookup_6() { return &___stringTypeLookup_6; }
	inline void set_stringTypeLookup_6(Dictionary_2_t2269201059 * value)
	{
		___stringTypeLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___stringTypeLookup_6), value);
	}

	inline static int32_t get_offset_of_assemblyTypeFlagLookup_7() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___assemblyTypeFlagLookup_7)); }
	inline Dictionary_2_t4012063963 * get_assemblyTypeFlagLookup_7() const { return ___assemblyTypeFlagLookup_7; }
	inline Dictionary_2_t4012063963 ** get_address_of_assemblyTypeFlagLookup_7() { return &___assemblyTypeFlagLookup_7; }
	inline void set_assemblyTypeFlagLookup_7(Dictionary_2_t4012063963 * value)
	{
		___assemblyTypeFlagLookup_7 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyTypeFlagLookup_7), value);
	}

	inline static int32_t get_offset_of_allAssemblies_8() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___allAssemblies_8)); }
	inline List_1_t1279540245 * get_allAssemblies_8() const { return ___allAssemblies_8; }
	inline List_1_t1279540245 ** get_address_of_allAssemblies_8() { return &___allAssemblies_8; }
	inline void set_allAssemblies_8(List_1_t1279540245 * value)
	{
		___allAssemblies_8 = value;
		Il2CppCodeGenWriteBarrier((&___allAssemblies_8), value);
	}

	inline static int32_t get_offset_of_allAssembliesImmutable_9() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___allAssembliesImmutable_9)); }
	inline ImmutableList_1_t1522597721 * get_allAssembliesImmutable_9() const { return ___allAssembliesImmutable_9; }
	inline ImmutableList_1_t1522597721 ** get_address_of_allAssembliesImmutable_9() { return &___allAssembliesImmutable_9; }
	inline void set_allAssembliesImmutable_9(ImmutableList_1_t1522597721 * value)
	{
		___allAssembliesImmutable_9 = value;
		Il2CppCodeGenWriteBarrier((&___allAssembliesImmutable_9), value);
	}

	inline static int32_t get_offset_of_userAssemblies_10() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___userAssemblies_10)); }
	inline List_1_t1279540245 * get_userAssemblies_10() const { return ___userAssemblies_10; }
	inline List_1_t1279540245 ** get_address_of_userAssemblies_10() { return &___userAssemblies_10; }
	inline void set_userAssemblies_10(List_1_t1279540245 * value)
	{
		___userAssemblies_10 = value;
		Il2CppCodeGenWriteBarrier((&___userAssemblies_10), value);
	}

	inline static int32_t get_offset_of_userEditorAssemblies_11() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___userEditorAssemblies_11)); }
	inline List_1_t1279540245 * get_userEditorAssemblies_11() const { return ___userEditorAssemblies_11; }
	inline List_1_t1279540245 ** get_address_of_userEditorAssemblies_11() { return &___userEditorAssemblies_11; }
	inline void set_userEditorAssemblies_11(List_1_t1279540245 * value)
	{
		___userEditorAssemblies_11 = value;
		Il2CppCodeGenWriteBarrier((&___userEditorAssemblies_11), value);
	}

	inline static int32_t get_offset_of_pluginAssemblies_12() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___pluginAssemblies_12)); }
	inline List_1_t1279540245 * get_pluginAssemblies_12() const { return ___pluginAssemblies_12; }
	inline List_1_t1279540245 ** get_address_of_pluginAssemblies_12() { return &___pluginAssemblies_12; }
	inline void set_pluginAssemblies_12(List_1_t1279540245 * value)
	{
		___pluginAssemblies_12 = value;
		Il2CppCodeGenWriteBarrier((&___pluginAssemblies_12), value);
	}

	inline static int32_t get_offset_of_pluginEditorAssemblies_13() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___pluginEditorAssemblies_13)); }
	inline List_1_t1279540245 * get_pluginEditorAssemblies_13() const { return ___pluginEditorAssemblies_13; }
	inline List_1_t1279540245 ** get_address_of_pluginEditorAssemblies_13() { return &___pluginEditorAssemblies_13; }
	inline void set_pluginEditorAssemblies_13(List_1_t1279540245 * value)
	{
		___pluginEditorAssemblies_13 = value;
		Il2CppCodeGenWriteBarrier((&___pluginEditorAssemblies_13), value);
	}

	inline static int32_t get_offset_of_unityAssemblies_14() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___unityAssemblies_14)); }
	inline List_1_t1279540245 * get_unityAssemblies_14() const { return ___unityAssemblies_14; }
	inline List_1_t1279540245 ** get_address_of_unityAssemblies_14() { return &___unityAssemblies_14; }
	inline void set_unityAssemblies_14(List_1_t1279540245 * value)
	{
		___unityAssemblies_14 = value;
		Il2CppCodeGenWriteBarrier((&___unityAssemblies_14), value);
	}

	inline static int32_t get_offset_of_unityEditorAssemblies_15() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___unityEditorAssemblies_15)); }
	inline List_1_t1279540245 * get_unityEditorAssemblies_15() const { return ___unityEditorAssemblies_15; }
	inline List_1_t1279540245 ** get_address_of_unityEditorAssemblies_15() { return &___unityEditorAssemblies_15; }
	inline void set_unityEditorAssemblies_15(List_1_t1279540245 * value)
	{
		___unityEditorAssemblies_15 = value;
		Il2CppCodeGenWriteBarrier((&___unityEditorAssemblies_15), value);
	}

	inline static int32_t get_offset_of_otherAssemblies_16() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___otherAssemblies_16)); }
	inline List_1_t1279540245 * get_otherAssemblies_16() const { return ___otherAssemblies_16; }
	inline List_1_t1279540245 ** get_address_of_otherAssemblies_16() { return &___otherAssemblies_16; }
	inline void set_otherAssemblies_16(List_1_t1279540245 * value)
	{
		___otherAssemblies_16 = value;
		Il2CppCodeGenWriteBarrier((&___otherAssemblies_16), value);
	}

	inline static int32_t get_offset_of_userTypes_17() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___userTypes_17)); }
	inline List_1_t3956019502 * get_userTypes_17() const { return ___userTypes_17; }
	inline List_1_t3956019502 ** get_address_of_userTypes_17() { return &___userTypes_17; }
	inline void set_userTypes_17(List_1_t3956019502 * value)
	{
		___userTypes_17 = value;
		Il2CppCodeGenWriteBarrier((&___userTypes_17), value);
	}

	inline static int32_t get_offset_of_userEditorTypes_18() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___userEditorTypes_18)); }
	inline List_1_t3956019502 * get_userEditorTypes_18() const { return ___userEditorTypes_18; }
	inline List_1_t3956019502 ** get_address_of_userEditorTypes_18() { return &___userEditorTypes_18; }
	inline void set_userEditorTypes_18(List_1_t3956019502 * value)
	{
		___userEditorTypes_18 = value;
		Il2CppCodeGenWriteBarrier((&___userEditorTypes_18), value);
	}

	inline static int32_t get_offset_of_pluginTypes_19() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___pluginTypes_19)); }
	inline List_1_t3956019502 * get_pluginTypes_19() const { return ___pluginTypes_19; }
	inline List_1_t3956019502 ** get_address_of_pluginTypes_19() { return &___pluginTypes_19; }
	inline void set_pluginTypes_19(List_1_t3956019502 * value)
	{
		___pluginTypes_19 = value;
		Il2CppCodeGenWriteBarrier((&___pluginTypes_19), value);
	}

	inline static int32_t get_offset_of_pluginEditorTypes_20() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___pluginEditorTypes_20)); }
	inline List_1_t3956019502 * get_pluginEditorTypes_20() const { return ___pluginEditorTypes_20; }
	inline List_1_t3956019502 ** get_address_of_pluginEditorTypes_20() { return &___pluginEditorTypes_20; }
	inline void set_pluginEditorTypes_20(List_1_t3956019502 * value)
	{
		___pluginEditorTypes_20 = value;
		Il2CppCodeGenWriteBarrier((&___pluginEditorTypes_20), value);
	}

	inline static int32_t get_offset_of_unityTypes_21() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___unityTypes_21)); }
	inline List_1_t3956019502 * get_unityTypes_21() const { return ___unityTypes_21; }
	inline List_1_t3956019502 ** get_address_of_unityTypes_21() { return &___unityTypes_21; }
	inline void set_unityTypes_21(List_1_t3956019502 * value)
	{
		___unityTypes_21 = value;
		Il2CppCodeGenWriteBarrier((&___unityTypes_21), value);
	}

	inline static int32_t get_offset_of_unityEditorTypes_22() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___unityEditorTypes_22)); }
	inline List_1_t3956019502 * get_unityEditorTypes_22() const { return ___unityEditorTypes_22; }
	inline List_1_t3956019502 ** get_address_of_unityEditorTypes_22() { return &___unityEditorTypes_22; }
	inline void set_unityEditorTypes_22(List_1_t3956019502 * value)
	{
		___unityEditorTypes_22 = value;
		Il2CppCodeGenWriteBarrier((&___unityEditorTypes_22), value);
	}

	inline static int32_t get_offset_of_otherTypes_23() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___otherTypes_23)); }
	inline List_1_t3956019502 * get_otherTypes_23() const { return ___otherTypes_23; }
	inline List_1_t3956019502 ** get_address_of_otherTypes_23() { return &___otherTypes_23; }
	inline void set_otherTypes_23(List_1_t3956019502 * value)
	{
		___otherTypes_23 = value;
		Il2CppCodeGenWriteBarrier((&___otherTypes_23), value);
	}

	inline static int32_t get_offset_of_dataPath_24() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___dataPath_24)); }
	inline String_t* get_dataPath_24() const { return ___dataPath_24; }
	inline String_t** get_address_of_dataPath_24() { return &___dataPath_24; }
	inline void set_dataPath_24(String_t* value)
	{
		___dataPath_24 = value;
		Il2CppCodeGenWriteBarrier((&___dataPath_24), value);
	}

	inline static int32_t get_offset_of_scriptAssembliesPath_25() { return static_cast<int32_t>(offsetof(AssemblyUtilities_t2715239875_StaticFields, ___scriptAssembliesPath_25)); }
	inline String_t* get_scriptAssembliesPath_25() const { return ___scriptAssembliesPath_25; }
	inline String_t** get_address_of_scriptAssembliesPath_25() { return &___scriptAssembliesPath_25; }
	inline void set_scriptAssembliesPath_25(String_t* value)
	{
		___scriptAssembliesPath_25 = value;
		Il2CppCodeGenWriteBarrier((&___scriptAssembliesPath_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYUTILITIES_T2715239875_H
#ifndef U3CU3EC_T3287483264_H
#define U3CU3EC_T3287483264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.AssemblyUtilities/<>c
struct  U3CU3Ec_t3287483264  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3287483264_StaticFields
{
public:
	// Sirenix.Serialization.Utilities.AssemblyUtilities/<>c Sirenix.Serialization.Utilities.AssemblyUtilities/<>c::<>9
	U3CU3Ec_t3287483264 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3287483264_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3287483264 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3287483264 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3287483264 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3287483264_H
#ifndef EMITUTILITIES_T2178190973_H
#define EMITUTILITIES_T2178190973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities
struct  EmitUtilities_t2178190973  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMITUTILITIES_T2178190973_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T3735155750_H
#define U3CU3EC__DISPLAYCLASS11_0_T3735155750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t3735155750  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass11_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t3735155750, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T3735155750_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T3735155749_H
#define U3CU3EC__DISPLAYCLASS12_0_T3735155749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t3735155749  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass12_0::propertyInfo
	PropertyInfo_t * ___propertyInfo_0;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t3735155749, ___propertyInfo_0)); }
	inline PropertyInfo_t * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInfo_t ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInfo_t * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T3735155749_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T3735155748_H
#define U3CU3EC__DISPLAYCLASS13_0_T3735155748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t3735155748  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass13_0::propertyInfo
	PropertyInfo_t * ___propertyInfo_0;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t3735155748, ___propertyInfo_0)); }
	inline PropertyInfo_t * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInfo_t ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInfo_t * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T3735155748_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T3601265702_H
#define U3CU3EC__DISPLAYCLASS21_0_T3601265702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t3601265702  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass21_0::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t3601265702, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T3601265702_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T3011262210_H
#define U3CU3EC__DISPLAYCLASS3_0_T3011262210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t3011262210  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass3_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t3011262210, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T3011262210_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T3393599234_H
#define U3CU3EC__DISPLAYCLASS5_0_T3393599234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t3393599234  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass5_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t3393599234, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T3393599234_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T3055892226_H
#define U3CU3EC__DISPLAYCLASS8_0_T3055892226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t3055892226  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass8_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t3055892226, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T3055892226_H
#ifndef FIELDINFOEXTENSIONS_T401171748_H
#define FIELDINFOEXTENSIONS_T401171748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.FieldInfoExtensions
struct  FieldInfoExtensions_t401171748  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFOEXTENSIONS_T401171748_H
#ifndef GARBAGEFREEITERATORS_T3847886896_H
#define GARBAGEFREEITERATORS_T3847886896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.GarbageFreeIterators
struct  GarbageFreeIterators_t3847886896  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GARBAGEFREEITERATORS_T3847886896_H
#ifndef IMMUTABLELIST_T1819414204_H
#define IMMUTABLELIST_T1819414204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.ImmutableList
struct  ImmutableList_t1819414204  : public RuntimeObject
{
public:
	// System.Collections.IList Sirenix.Serialization.Utilities.ImmutableList::innerList
	RuntimeObject* ___innerList_0;

public:
	inline static int32_t get_offset_of_innerList_0() { return static_cast<int32_t>(offsetof(ImmutableList_t1819414204, ___innerList_0)); }
	inline RuntimeObject* get_innerList_0() const { return ___innerList_0; }
	inline RuntimeObject** get_address_of_innerList_0() { return &___innerList_0; }
	inline void set_innerList_0(RuntimeObject* value)
	{
		___innerList_0 = value;
		Il2CppCodeGenWriteBarrier((&___innerList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLELIST_T1819414204_H
#ifndef U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T57684215_H
#define U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T57684215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25
struct  U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Sirenix.Serialization.Utilities.ImmutableList Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>4__this
	ImmutableList_t1819414204 * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList/<System-Collections-Generic-IEnumerable<System-Object>-GetEnumerator>d__25::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E4__this_2)); }
	inline ImmutableList_t1819414204 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ImmutableList_t1819414204 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ImmutableList_t1819414204 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSYSTEMU2DCOLLECTIONSU2DGENERICU2DIENUMERABLEU3CSYSTEMU2DOBJECTU3EU2DGETENUMERATORU3ED__25_T57684215_H
#ifndef LINQEXTENSIONS_T1130353347_H
#define LINQEXTENSIONS_T1130353347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.LinqExtensions
struct  LinqExtensions_t1130353347  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINQEXTENSIONS_T1130353347_H
#ifndef MEMBERINFOEXTENSIONS_T68516722_H
#define MEMBERINFOEXTENSIONS_T68516722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberInfoExtensions
struct  MemberInfoExtensions_t68516722  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFOEXTENSIONS_T68516722_H
#ifndef METHODINFOEXTENSIONS_T3469285330_H
#define METHODINFOEXTENSIONS_T3469285330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MethodInfoExtensions
struct  MethodInfoExtensions_t3469285330  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFOEXTENSIONS_T3469285330_H
#ifndef PATHUTILITIES_T4096232836_H
#define PATHUTILITIES_T4096232836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.PathUtilities
struct  PathUtilities_t4096232836  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHUTILITIES_T4096232836_H
#ifndef PROPERTYINFOEXTENSIONS_T1420793386_H
#define PROPERTYINFOEXTENSIONS_T1420793386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.PropertyInfoExtensions
struct  PropertyInfoExtensions_t1420793386  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFOEXTENSIONS_T1420793386_H
#ifndef STRINGEXTENSIONS_T508951950_H
#define STRINGEXTENSIONS_T508951950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.StringExtensions
struct  StringExtensions_t508951950  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEXTENSIONS_T508951950_H
#ifndef TYPEEXTENSIONS_T1614484489_H
#define TYPEEXTENSIONS_T1614484489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions
struct  TypeExtensions_t1614484489  : public RuntimeObject
{
public:

public:
};

struct TypeExtensions_t1614484489_StaticFields
{
public:
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfaction_LOCK
	RuntimeObject * ___GenericConstraintsSatisfaction_LOCK_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfactionInferredParameters
	Dictionary_2_t633324528 * ___GenericConstraintsSatisfactionInferredParameters_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfactionResolvedMap
	Dictionary_2_t633324528 * ___GenericConstraintsSatisfactionResolvedMap_2;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GenericConstraintsSatisfactionProcessedParams
	HashSet_1_t1048894234 * ___GenericConstraintsSatisfactionProcessedParams_3;
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::WeaklyTypedTypeCastDelegates_LOCK
	RuntimeObject * ___WeaklyTypedTypeCastDelegates_LOCK_4;
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::StronglyTypedTypeCastDelegates_LOCK
	RuntimeObject * ___StronglyTypedTypeCastDelegates_LOCK_5;
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Func`2<System.Object,System.Object>> Sirenix.Serialization.Utilities.TypeExtensions::WeaklyTypedTypeCastDelegates
	DoubleLookupDictionary_3_t650546409 * ___WeaklyTypedTypeCastDelegates_6;
	// Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<System.Type,System.Type,System.Delegate> Sirenix.Serialization.Utilities.TypeExtensions::StronglyTypedTypeCastDelegates
	DoubleLookupDictionary_3_t3686776144 * ___StronglyTypedTypeCastDelegates_7;
	// System.Collections.Generic.HashSet`1<System.String> Sirenix.Serialization.Utilities.TypeExtensions::ReservedCSharpKeywords
	HashSet_1_t412400163 * ___ReservedCSharpKeywords_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Sirenix.Serialization.Utilities.TypeExtensions::TypeNameAlternatives
	Dictionary_2_t1632706988 * ___TypeNameAlternatives_9;
	// System.Object Sirenix.Serialization.Utilities.TypeExtensions::CachedNiceNames_LOCK
	RuntimeObject * ___CachedNiceNames_LOCK_10;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> Sirenix.Serialization.Utilities.TypeExtensions::CachedNiceNames
	Dictionary_2_t4291797753 * ___CachedNiceNames_11;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions::VoidPointerType
	Type_t * ___VoidPointerType_12;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.HashSet`1<System.Type>> Sirenix.Serialization.Utilities.TypeExtensions::PrimitiveImplicitCasts
	Dictionary_2_t3493241298 * ___PrimitiveImplicitCasts_13;
	// System.Collections.Generic.HashSet`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::ExplicitCastIntegrals
	HashSet_1_t1048894234 * ___ExplicitCastIntegrals_14;

public:
	inline static int32_t get_offset_of_GenericConstraintsSatisfaction_LOCK_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfaction_LOCK_0)); }
	inline RuntimeObject * get_GenericConstraintsSatisfaction_LOCK_0() const { return ___GenericConstraintsSatisfaction_LOCK_0; }
	inline RuntimeObject ** get_address_of_GenericConstraintsSatisfaction_LOCK_0() { return &___GenericConstraintsSatisfaction_LOCK_0; }
	inline void set_GenericConstraintsSatisfaction_LOCK_0(RuntimeObject * value)
	{
		___GenericConstraintsSatisfaction_LOCK_0 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfaction_LOCK_0), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionInferredParameters_1() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfactionInferredParameters_1)); }
	inline Dictionary_2_t633324528 * get_GenericConstraintsSatisfactionInferredParameters_1() const { return ___GenericConstraintsSatisfactionInferredParameters_1; }
	inline Dictionary_2_t633324528 ** get_address_of_GenericConstraintsSatisfactionInferredParameters_1() { return &___GenericConstraintsSatisfactionInferredParameters_1; }
	inline void set_GenericConstraintsSatisfactionInferredParameters_1(Dictionary_2_t633324528 * value)
	{
		___GenericConstraintsSatisfactionInferredParameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionInferredParameters_1), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionResolvedMap_2() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfactionResolvedMap_2)); }
	inline Dictionary_2_t633324528 * get_GenericConstraintsSatisfactionResolvedMap_2() const { return ___GenericConstraintsSatisfactionResolvedMap_2; }
	inline Dictionary_2_t633324528 ** get_address_of_GenericConstraintsSatisfactionResolvedMap_2() { return &___GenericConstraintsSatisfactionResolvedMap_2; }
	inline void set_GenericConstraintsSatisfactionResolvedMap_2(Dictionary_2_t633324528 * value)
	{
		___GenericConstraintsSatisfactionResolvedMap_2 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionResolvedMap_2), value);
	}

	inline static int32_t get_offset_of_GenericConstraintsSatisfactionProcessedParams_3() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___GenericConstraintsSatisfactionProcessedParams_3)); }
	inline HashSet_1_t1048894234 * get_GenericConstraintsSatisfactionProcessedParams_3() const { return ___GenericConstraintsSatisfactionProcessedParams_3; }
	inline HashSet_1_t1048894234 ** get_address_of_GenericConstraintsSatisfactionProcessedParams_3() { return &___GenericConstraintsSatisfactionProcessedParams_3; }
	inline void set_GenericConstraintsSatisfactionProcessedParams_3(HashSet_1_t1048894234 * value)
	{
		___GenericConstraintsSatisfactionProcessedParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___GenericConstraintsSatisfactionProcessedParams_3), value);
	}

	inline static int32_t get_offset_of_WeaklyTypedTypeCastDelegates_LOCK_4() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___WeaklyTypedTypeCastDelegates_LOCK_4)); }
	inline RuntimeObject * get_WeaklyTypedTypeCastDelegates_LOCK_4() const { return ___WeaklyTypedTypeCastDelegates_LOCK_4; }
	inline RuntimeObject ** get_address_of_WeaklyTypedTypeCastDelegates_LOCK_4() { return &___WeaklyTypedTypeCastDelegates_LOCK_4; }
	inline void set_WeaklyTypedTypeCastDelegates_LOCK_4(RuntimeObject * value)
	{
		___WeaklyTypedTypeCastDelegates_LOCK_4 = value;
		Il2CppCodeGenWriteBarrier((&___WeaklyTypedTypeCastDelegates_LOCK_4), value);
	}

	inline static int32_t get_offset_of_StronglyTypedTypeCastDelegates_LOCK_5() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___StronglyTypedTypeCastDelegates_LOCK_5)); }
	inline RuntimeObject * get_StronglyTypedTypeCastDelegates_LOCK_5() const { return ___StronglyTypedTypeCastDelegates_LOCK_5; }
	inline RuntimeObject ** get_address_of_StronglyTypedTypeCastDelegates_LOCK_5() { return &___StronglyTypedTypeCastDelegates_LOCK_5; }
	inline void set_StronglyTypedTypeCastDelegates_LOCK_5(RuntimeObject * value)
	{
		___StronglyTypedTypeCastDelegates_LOCK_5 = value;
		Il2CppCodeGenWriteBarrier((&___StronglyTypedTypeCastDelegates_LOCK_5), value);
	}

	inline static int32_t get_offset_of_WeaklyTypedTypeCastDelegates_6() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___WeaklyTypedTypeCastDelegates_6)); }
	inline DoubleLookupDictionary_3_t650546409 * get_WeaklyTypedTypeCastDelegates_6() const { return ___WeaklyTypedTypeCastDelegates_6; }
	inline DoubleLookupDictionary_3_t650546409 ** get_address_of_WeaklyTypedTypeCastDelegates_6() { return &___WeaklyTypedTypeCastDelegates_6; }
	inline void set_WeaklyTypedTypeCastDelegates_6(DoubleLookupDictionary_3_t650546409 * value)
	{
		___WeaklyTypedTypeCastDelegates_6 = value;
		Il2CppCodeGenWriteBarrier((&___WeaklyTypedTypeCastDelegates_6), value);
	}

	inline static int32_t get_offset_of_StronglyTypedTypeCastDelegates_7() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___StronglyTypedTypeCastDelegates_7)); }
	inline DoubleLookupDictionary_3_t3686776144 * get_StronglyTypedTypeCastDelegates_7() const { return ___StronglyTypedTypeCastDelegates_7; }
	inline DoubleLookupDictionary_3_t3686776144 ** get_address_of_StronglyTypedTypeCastDelegates_7() { return &___StronglyTypedTypeCastDelegates_7; }
	inline void set_StronglyTypedTypeCastDelegates_7(DoubleLookupDictionary_3_t3686776144 * value)
	{
		___StronglyTypedTypeCastDelegates_7 = value;
		Il2CppCodeGenWriteBarrier((&___StronglyTypedTypeCastDelegates_7), value);
	}

	inline static int32_t get_offset_of_ReservedCSharpKeywords_8() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___ReservedCSharpKeywords_8)); }
	inline HashSet_1_t412400163 * get_ReservedCSharpKeywords_8() const { return ___ReservedCSharpKeywords_8; }
	inline HashSet_1_t412400163 ** get_address_of_ReservedCSharpKeywords_8() { return &___ReservedCSharpKeywords_8; }
	inline void set_ReservedCSharpKeywords_8(HashSet_1_t412400163 * value)
	{
		___ReservedCSharpKeywords_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReservedCSharpKeywords_8), value);
	}

	inline static int32_t get_offset_of_TypeNameAlternatives_9() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___TypeNameAlternatives_9)); }
	inline Dictionary_2_t1632706988 * get_TypeNameAlternatives_9() const { return ___TypeNameAlternatives_9; }
	inline Dictionary_2_t1632706988 ** get_address_of_TypeNameAlternatives_9() { return &___TypeNameAlternatives_9; }
	inline void set_TypeNameAlternatives_9(Dictionary_2_t1632706988 * value)
	{
		___TypeNameAlternatives_9 = value;
		Il2CppCodeGenWriteBarrier((&___TypeNameAlternatives_9), value);
	}

	inline static int32_t get_offset_of_CachedNiceNames_LOCK_10() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___CachedNiceNames_LOCK_10)); }
	inline RuntimeObject * get_CachedNiceNames_LOCK_10() const { return ___CachedNiceNames_LOCK_10; }
	inline RuntimeObject ** get_address_of_CachedNiceNames_LOCK_10() { return &___CachedNiceNames_LOCK_10; }
	inline void set_CachedNiceNames_LOCK_10(RuntimeObject * value)
	{
		___CachedNiceNames_LOCK_10 = value;
		Il2CppCodeGenWriteBarrier((&___CachedNiceNames_LOCK_10), value);
	}

	inline static int32_t get_offset_of_CachedNiceNames_11() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___CachedNiceNames_11)); }
	inline Dictionary_2_t4291797753 * get_CachedNiceNames_11() const { return ___CachedNiceNames_11; }
	inline Dictionary_2_t4291797753 ** get_address_of_CachedNiceNames_11() { return &___CachedNiceNames_11; }
	inline void set_CachedNiceNames_11(Dictionary_2_t4291797753 * value)
	{
		___CachedNiceNames_11 = value;
		Il2CppCodeGenWriteBarrier((&___CachedNiceNames_11), value);
	}

	inline static int32_t get_offset_of_VoidPointerType_12() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___VoidPointerType_12)); }
	inline Type_t * get_VoidPointerType_12() const { return ___VoidPointerType_12; }
	inline Type_t ** get_address_of_VoidPointerType_12() { return &___VoidPointerType_12; }
	inline void set_VoidPointerType_12(Type_t * value)
	{
		___VoidPointerType_12 = value;
		Il2CppCodeGenWriteBarrier((&___VoidPointerType_12), value);
	}

	inline static int32_t get_offset_of_PrimitiveImplicitCasts_13() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___PrimitiveImplicitCasts_13)); }
	inline Dictionary_2_t3493241298 * get_PrimitiveImplicitCasts_13() const { return ___PrimitiveImplicitCasts_13; }
	inline Dictionary_2_t3493241298 ** get_address_of_PrimitiveImplicitCasts_13() { return &___PrimitiveImplicitCasts_13; }
	inline void set_PrimitiveImplicitCasts_13(Dictionary_2_t3493241298 * value)
	{
		___PrimitiveImplicitCasts_13 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveImplicitCasts_13), value);
	}

	inline static int32_t get_offset_of_ExplicitCastIntegrals_14() { return static_cast<int32_t>(offsetof(TypeExtensions_t1614484489_StaticFields, ___ExplicitCastIntegrals_14)); }
	inline HashSet_1_t1048894234 * get_ExplicitCastIntegrals_14() const { return ___ExplicitCastIntegrals_14; }
	inline HashSet_1_t1048894234 ** get_address_of_ExplicitCastIntegrals_14() { return &___ExplicitCastIntegrals_14; }
	inline void set_ExplicitCastIntegrals_14(HashSet_1_t1048894234 * value)
	{
		___ExplicitCastIntegrals_14 = value;
		Il2CppCodeGenWriteBarrier((&___ExplicitCastIntegrals_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T1614484489_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T2941737288_H
#define U3CU3EC__DISPLAYCLASS22_0_T2941737288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t2941737288  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::method
	MethodInfo_t * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2941737288, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T2941737288_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T2941409609_H
#define U3CU3EC__DISPLAYCLASS35_0_T2941409609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t2941409609  : public RuntimeObject
{
public:
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::openGenericInterfaceType
	Type_t * ___openGenericInterfaceType_0;

public:
	inline static int32_t get_offset_of_openGenericInterfaceType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t2941409609, ___openGenericInterfaceType_0)); }
	inline Type_t * get_openGenericInterfaceType_0() const { return ___openGenericInterfaceType_0; }
	inline Type_t ** get_address_of_openGenericInterfaceType_0() { return &___openGenericInterfaceType_0; }
	inline void set_openGenericInterfaceType_0(Type_t * value)
	{
		___openGenericInterfaceType_0 = value;
		Il2CppCodeGenWriteBarrier((&___openGenericInterfaceType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T2941409609_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T2941671758_H
#define U3CU3EC__DISPLAYCLASS41_0_T2941671758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t2941671758  : public RuntimeObject
{
public:
	// System.String Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::methodName
	String_t* ___methodName_0;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t2941671758, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T2941671758_H
#ifndef U3CGETBASECLASSESU3ED__48_T3606501197_H
#define U3CGETBASECLASSESU3ED__48_T3606501197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48
struct  U3CGetBaseClassesU3Ed__48_t3606501197  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::includeSelf
	bool ___includeSelf_5;
	// System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<>3__includeSelf
	bool ___U3CU3E3__includeSelf_6;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::<current>5__1
	Type_t * ___U3CcurrentU3E5__1_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_includeSelf_5() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___includeSelf_5)); }
	inline bool get_includeSelf_5() const { return ___includeSelf_5; }
	inline bool* get_address_of_includeSelf_5() { return &___includeSelf_5; }
	inline void set_includeSelf_5(bool value)
	{
		___includeSelf_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__includeSelf_6() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CU3E3__includeSelf_6)); }
	inline bool get_U3CU3E3__includeSelf_6() const { return ___U3CU3E3__includeSelf_6; }
	inline bool* get_address_of_U3CU3E3__includeSelf_6() { return &___U3CU3E3__includeSelf_6; }
	inline void set_U3CU3E3__includeSelf_6(bool value)
	{
		___U3CU3E3__includeSelf_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CGetBaseClassesU3Ed__48_t3606501197, ___U3CcurrentU3E5__1_7)); }
	inline Type_t * get_U3CcurrentU3E5__1_7() const { return ___U3CcurrentU3E5__1_7; }
	inline Type_t ** get_address_of_U3CcurrentU3E5__1_7() { return &___U3CcurrentU3E5__1_7; }
	inline void set_U3CcurrentU3E5__1_7(Type_t * value)
	{
		___U3CcurrentU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E5__1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBASECLASSESU3ED__48_T3606501197_H
#ifndef UNITYEXTENSIONS_T2928681996_H
#define UNITYEXTENSIONS_T2928681996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.UnityExtensions
struct  UnityExtensions_t2928681996  : public RuntimeObject
{
public:

public:
};

struct UnityExtensions_t2928681996_StaticFields
{
public:
	// Sirenix.Serialization.Utilities.ValueGetter`2<UnityEngine.Object,System.IntPtr> Sirenix.Serialization.Utilities.UnityExtensions::UnityObjectCachedPtrFieldGetter
	ValueGetter_2_t1938735483 * ___UnityObjectCachedPtrFieldGetter_0;

public:
	inline static int32_t get_offset_of_UnityObjectCachedPtrFieldGetter_0() { return static_cast<int32_t>(offsetof(UnityExtensions_t2928681996_StaticFields, ___UnityObjectCachedPtrFieldGetter_0)); }
	inline ValueGetter_2_t1938735483 * get_UnityObjectCachedPtrFieldGetter_0() const { return ___UnityObjectCachedPtrFieldGetter_0; }
	inline ValueGetter_2_t1938735483 ** get_address_of_UnityObjectCachedPtrFieldGetter_0() { return &___UnityObjectCachedPtrFieldGetter_0; }
	inline void set_UnityObjectCachedPtrFieldGetter_0(ValueGetter_2_t1938735483 * value)
	{
		___UnityObjectCachedPtrFieldGetter_0 = value;
		Il2CppCodeGenWriteBarrier((&___UnityObjectCachedPtrFieldGetter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXTENSIONS_T2928681996_H
#ifndef UNITYVERSION_T744152913_H
#define UNITYVERSION_T744152913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.UnityVersion
struct  UnityVersion_t744152913  : public RuntimeObject
{
public:

public:
};

struct UnityVersion_t744152913_StaticFields
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.UnityVersion::Major
	int32_t ___Major_0;
	// System.Int32 Sirenix.Serialization.Utilities.UnityVersion::Minor
	int32_t ___Minor_1;

public:
	inline static int32_t get_offset_of_Major_0() { return static_cast<int32_t>(offsetof(UnityVersion_t744152913_StaticFields, ___Major_0)); }
	inline int32_t get_Major_0() const { return ___Major_0; }
	inline int32_t* get_address_of_Major_0() { return &___Major_0; }
	inline void set_Major_0(int32_t value)
	{
		___Major_0 = value;
	}

	inline static int32_t get_offset_of_Minor_1() { return static_cast<int32_t>(offsetof(UnityVersion_t744152913_StaticFields, ___Minor_1)); }
	inline int32_t get_Minor_1() const { return ___Minor_1; }
	inline int32_t* get_address_of_Minor_1() { return &___Minor_1; }
	inline void set_Minor_1(int32_t value)
	{
		___Minor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVERSION_T744152913_H
#ifndef UNSAFEUTILITIES_T3361570935_H
#define UNSAFEUTILITIES_T3361570935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities
struct  UnsafeUtilities_t3361570935  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSAFEUTILITIES_T3361570935_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef AMAZONAPPSTORESTOREEXTENSIONS_T4153153958_H
#define AMAZONAPPSTORESTOREEXTENSIONS_T4153153958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AmazonAppStoreStoreExtensions
struct  AmazonAppStoreStoreExtensions_t4153153958  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AmazonAppStoreStoreExtensions::android
	AndroidJavaObject_t4131667876 * ___android_0;

public:
	inline static int32_t get_offset_of_android_0() { return static_cast<int32_t>(offsetof(AmazonAppStoreStoreExtensions_t4153153958, ___android_0)); }
	inline AndroidJavaObject_t4131667876 * get_android_0() const { return ___android_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_android_0() { return &___android_0; }
	inline void set_android_0(AndroidJavaObject_t4131667876 * value)
	{
		___android_0 = value;
		Il2CppCodeGenWriteBarrier((&___android_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONAPPSTORESTOREEXTENSIONS_T4153153958_H
#ifndef ANDROIDJAVASTORE_T1912360766_H
#define ANDROIDJAVASTORE_T1912360766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AndroidJavaStore
struct  AndroidJavaStore_t1912360766  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AndroidJavaStore::m_Store
	AndroidJavaObject_t4131667876 * ___m_Store_0;

public:
	inline static int32_t get_offset_of_m_Store_0() { return static_cast<int32_t>(offsetof(AndroidJavaStore_t1912360766, ___m_Store_0)); }
	inline AndroidJavaObject_t4131667876 * get_m_Store_0() const { return ___m_Store_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_Store_0() { return &___m_Store_0; }
	inline void set_m_Store_0(AndroidJavaObject_t4131667876 * value)
	{
		___m_Store_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Store_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVASTORE_T1912360766_H
#ifndef FAKEAMAZONEXTENSIONS_T2007213879_H
#define FAKEAMAZONEXTENSIONS_T2007213879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAmazonExtensions
struct  FakeAmazonExtensions_t2007213879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEAMAZONEXTENSIONS_T2007213879_H
#ifndef FAKEMOOLAHCONFIGURATION_T983287713_H
#define FAKEMOOLAHCONFIGURATION_T983287713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeMoolahConfiguration
struct  FakeMoolahConfiguration_t983287713  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.FakeMoolahConfiguration::m_appKey
	String_t* ___m_appKey_0;
	// System.String UnityEngine.Purchasing.FakeMoolahConfiguration::m_hashKey
	String_t* ___m_hashKey_1;

public:
	inline static int32_t get_offset_of_m_appKey_0() { return static_cast<int32_t>(offsetof(FakeMoolahConfiguration_t983287713, ___m_appKey_0)); }
	inline String_t* get_m_appKey_0() const { return ___m_appKey_0; }
	inline String_t** get_address_of_m_appKey_0() { return &___m_appKey_0; }
	inline void set_m_appKey_0(String_t* value)
	{
		___m_appKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_appKey_0), value);
	}

	inline static int32_t get_offset_of_m_hashKey_1() { return static_cast<int32_t>(offsetof(FakeMoolahConfiguration_t983287713, ___m_hashKey_1)); }
	inline String_t* get_m_hashKey_1() const { return ___m_hashKey_1; }
	inline String_t** get_address_of_m_hashKey_1() { return &___m_hashKey_1; }
	inline void set_m_hashKey_1(String_t* value)
	{
		___m_hashKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMOOLAHCONFIGURATION_T983287713_H
#ifndef FAKEMOOLAHEXTENSIONS_T4194088837_H
#define FAKEMOOLAHEXTENSIONS_T4194088837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeMoolahExtensions
struct  FakeMoolahExtensions_t4194088837  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMOOLAHEXTENSIONS_T4194088837_H
#ifndef JSONSERIALIZER_T4060786200_H
#define JSONSERIALIZER_T4060786200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JSONSerializer
struct  JSONSerializer_t4060786200  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T4060786200_H
#ifndef SCRIPTINGUNITYCALLBACK_T4035349519_H
#define SCRIPTINGUNITYCALLBACK_T4035349519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ScriptingUnityCallback
struct  ScriptingUnityCallback_t4035349519  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IUnityCallback UnityEngine.Purchasing.ScriptingUnityCallback::forwardTo
	RuntimeObject* ___forwardTo_0;
	// Uniject.IUtil UnityEngine.Purchasing.ScriptingUnityCallback::util
	RuntimeObject* ___util_1;

public:
	inline static int32_t get_offset_of_forwardTo_0() { return static_cast<int32_t>(offsetof(ScriptingUnityCallback_t4035349519, ___forwardTo_0)); }
	inline RuntimeObject* get_forwardTo_0() const { return ___forwardTo_0; }
	inline RuntimeObject** get_address_of_forwardTo_0() { return &___forwardTo_0; }
	inline void set_forwardTo_0(RuntimeObject* value)
	{
		___forwardTo_0 = value;
		Il2CppCodeGenWriteBarrier((&___forwardTo_0), value);
	}

	inline static int32_t get_offset_of_util_1() { return static_cast<int32_t>(offsetof(ScriptingUnityCallback_t4035349519, ___util_1)); }
	inline RuntimeObject* get_util_1() const { return ___util_1; }
	inline RuntimeObject** get_address_of_util_1() { return &___util_1; }
	inline void set_util_1(RuntimeObject* value)
	{
		___util_1 = value;
		Il2CppCodeGenWriteBarrier((&___util_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGUNITYCALLBACK_T4035349519_H
#ifndef SERIALIZATIONEXTENSIONS_T4283915465_H
#define SERIALIZATIONEXTENSIONS_T4283915465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SerializationExtensions
struct  SerializationExtensions_t4283915465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONEXTENSIONS_T4283915465_H
#ifndef __STATICARRAYINITTYPESIZEU3D256_T1757367636_H
#define __STATICARRAYINITTYPESIZEU3D256_T1757367636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct  __StaticArrayInitTypeSizeU3D256_t1757367636 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_t1757367636__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D256_T1757367636_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FIELDINFO_T_H
#define FIELDINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFO_T_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef PROPERTYINFO_T_H
#define PROPERTYINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyInfo
struct  PropertyInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFO_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef JAVABRIDGE_T3489274022_H
#define JAVABRIDGE_T3489274022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JavaBridge
struct  JavaBridge_t3489274022  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.Purchasing.IUnityCallback UnityEngine.Purchasing.JavaBridge::forwardTo
	RuntimeObject* ___forwardTo_0;

public:
	inline static int32_t get_offset_of_forwardTo_0() { return static_cast<int32_t>(offsetof(JavaBridge_t3489274022, ___forwardTo_0)); }
	inline RuntimeObject* get_forwardTo_0() const { return ___forwardTo_0; }
	inline RuntimeObject** get_address_of_forwardTo_0() { return &___forwardTo_0; }
	inline void set_forwardTo_0(RuntimeObject* value)
	{
		___forwardTo_0 = value;
		Il2CppCodeGenWriteBarrier((&___forwardTo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVABRIDGE_T3489274022_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255370  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::3CDA7449B0586AB873C75C04BB11D4864F5D7392
	__StaticArrayInitTypeSizeU3D256_t1757367636  ___3CDA7449B0586AB873C75C04BB11D4864F5D7392_0;

public:
	inline static int32_t get_offset_of_U33CDA7449B0586AB873C75C04BB11D4864F5D7392_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___3CDA7449B0586AB873C75C04BB11D4864F5D7392_0)); }
	inline __StaticArrayInitTypeSizeU3D256_t1757367636  get_U33CDA7449B0586AB873C75C04BB11D4864F5D7392_0() const { return ___3CDA7449B0586AB873C75C04BB11D4864F5D7392_0; }
	inline __StaticArrayInitTypeSizeU3D256_t1757367636 * get_address_of_U33CDA7449B0586AB873C75C04BB11D4864F5D7392_0() { return &___3CDA7449B0586AB873C75C04BB11D4864F5D7392_0; }
	inline void set_U33CDA7449B0586AB873C75C04BB11D4864F5D7392_0(__StaticArrayInitTypeSizeU3D256_t1757367636  value)
	{
		___3CDA7449B0586AB873C75C04BB11D4864F5D7392_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#ifndef ASSEMBLYTYPEFLAGS_T2716374150_H
#define ASSEMBLYTYPEFLAGS_T2716374150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.AssemblyTypeFlags
struct  AssemblyTypeFlags_t2716374150 
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyTypeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssemblyTypeFlags_t2716374150, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYTYPEFLAGS_T2716374150_H
#ifndef MEMBERALIASFIELDINFO_T3617726363_H
#define MEMBERALIASFIELDINFO_T3617726363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberAliasFieldInfo
struct  MemberAliasFieldInfo_t3617726363  : public FieldInfo_t
{
public:
	// System.Reflection.FieldInfo Sirenix.Serialization.Utilities.MemberAliasFieldInfo::aliasedField
	FieldInfo_t * ___aliasedField_1;
	// System.String Sirenix.Serialization.Utilities.MemberAliasFieldInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedField_1() { return static_cast<int32_t>(offsetof(MemberAliasFieldInfo_t3617726363, ___aliasedField_1)); }
	inline FieldInfo_t * get_aliasedField_1() const { return ___aliasedField_1; }
	inline FieldInfo_t ** get_address_of_aliasedField_1() { return &___aliasedField_1; }
	inline void set_aliasedField_1(FieldInfo_t * value)
	{
		___aliasedField_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedField_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasFieldInfo_t3617726363, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASFIELDINFO_T3617726363_H
#ifndef MEMBERALIASPROPERTYINFO_T4204559890_H
#define MEMBERALIASPROPERTYINFO_T4204559890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberAliasPropertyInfo
struct  MemberAliasPropertyInfo_t4204559890  : public PropertyInfo_t
{
public:
	// System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::aliasedProperty
	PropertyInfo_t * ___aliasedProperty_1;
	// System.String Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedProperty_1() { return static_cast<int32_t>(offsetof(MemberAliasPropertyInfo_t4204559890, ___aliasedProperty_1)); }
	inline PropertyInfo_t * get_aliasedProperty_1() const { return ___aliasedProperty_1; }
	inline PropertyInfo_t ** get_address_of_aliasedProperty_1() { return &___aliasedProperty_1; }
	inline void set_aliasedProperty_1(PropertyInfo_t * value)
	{
		___aliasedProperty_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedProperty_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasPropertyInfo_t4204559890, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASPROPERTYINFO_T4204559890_H
#ifndef OPERATOR_T1465414565_H
#define OPERATOR_T1465414565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.Operator
struct  Operator_t1465414565 
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.Operator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Operator_t1465414565, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_T1465414565_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef ANDROIDSTORE_T1462224562_H
#define ANDROIDSTORE_T1462224562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AndroidStore
struct  AndroidStore_t1462224562 
{
public:
	// System.Int32 UnityEngine.Purchasing.AndroidStore::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AndroidStore_t1462224562, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDSTORE_T1462224562_H
#ifndef CLOUDMOOLAHMODE_T3412107490_H
#define CLOUDMOOLAHMODE_T3412107490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudMoolahMode
struct  CloudMoolahMode_t3412107490 
{
public:
	// System.Int32 UnityEngine.Purchasing.CloudMoolahMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloudMoolahMode_t3412107490, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDMOOLAHMODE_T3412107490_H
#ifndef RESTORETRANSACTIONIDSTATE_T1240344018_H
#define RESTORETRANSACTIONIDSTATE_T1240344018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.RestoreTransactionIDState
struct  RestoreTransactionIDState_t1240344018 
{
public:
	// System.Int32 UnityEngine.Purchasing.RestoreTransactionIDState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RestoreTransactionIDState_t1240344018, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTORETRANSACTIONIDSTATE_T1240344018_H
#ifndef TRADESEQSTATE_T912436049_H
#define TRADESEQSTATE_T912436049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TradeSeqState
struct  TradeSeqState_t912436049 
{
public:
	// System.Int32 UnityEngine.Purchasing.TradeSeqState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TradeSeqState_t912436049, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRADESEQSTATE_T912436049_H
#ifndef VALIDATERECEIPTSTATE_T3921901150_H
#define VALIDATERECEIPTSTATE_T3921901150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ValidateReceiptState
struct  ValidateReceiptState_t3921901150 
{
public:
	// System.Int32 UnityEngine.Purchasing.ValidateReceiptState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidateReceiptState_t3921901150, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATERECEIPTSTATE_T3921901150_H
#ifndef U3CGETTYPESU3ED__38_T280155023_H
#define U3CGETTYPESU3ED__38_T280155023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38
struct  U3CGetTypesU3Ed__38_t280155023  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Sirenix.Serialization.Utilities.AssemblyTypeFlags Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::assemblyTypeFlags
	int32_t ___assemblyTypeFlags_3;
	// Sirenix.Serialization.Utilities.AssemblyTypeFlags Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<>3__assemblyTypeFlags
	int32_t ___U3CU3E3__assemblyTypeFlags_4;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__1
	int32_t ___U3CiU3E5__1_5;
	// System.Boolean Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeUserEditorTypes>5__2
	bool ___U3CincludeUserEditorTypesU3E5__2_6;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__3
	int32_t ___U3CiU3E5__3_7;
	// System.Boolean Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<includePluginTypes>5__4
	bool ___U3CincludePluginTypesU3E5__4_8;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__5
	int32_t ___U3CiU3E5__5_9;
	// System.Boolean Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<includePluginEditorTypes>5__6
	bool ___U3CincludePluginEditorTypesU3E5__6_10;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__7
	int32_t ___U3CiU3E5__7_11;
	// System.Boolean Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeUnityTypes>5__8
	bool ___U3CincludeUnityTypesU3E5__8_12;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__9
	int32_t ___U3CiU3E5__9_13;
	// System.Boolean Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeUnityEditorTypes>5__10
	bool ___U3CincludeUnityEditorTypesU3E5__10_14;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__11
	int32_t ___U3CiU3E5__11_15;
	// System.Boolean Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<includeOtherTypes>5__12
	bool ___U3CincludeOtherTypesU3E5__12_16;
	// System.Int32 Sirenix.Serialization.Utilities.AssemblyUtilities/<GetTypes>d__38::<i>5__13
	int32_t ___U3CiU3E5__13_17;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_assemblyTypeFlags_3() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___assemblyTypeFlags_3)); }
	inline int32_t get_assemblyTypeFlags_3() const { return ___assemblyTypeFlags_3; }
	inline int32_t* get_address_of_assemblyTypeFlags_3() { return &___assemblyTypeFlags_3; }
	inline void set_assemblyTypeFlags_3(int32_t value)
	{
		___assemblyTypeFlags_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__assemblyTypeFlags_4() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CU3E3__assemblyTypeFlags_4)); }
	inline int32_t get_U3CU3E3__assemblyTypeFlags_4() const { return ___U3CU3E3__assemblyTypeFlags_4; }
	inline int32_t* get_address_of_U3CU3E3__assemblyTypeFlags_4() { return &___U3CU3E3__assemblyTypeFlags_4; }
	inline void set_U3CU3E3__assemblyTypeFlags_4(int32_t value)
	{
		___U3CU3E3__assemblyTypeFlags_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__1_5)); }
	inline int32_t get_U3CiU3E5__1_5() const { return ___U3CiU3E5__1_5; }
	inline int32_t* get_address_of_U3CiU3E5__1_5() { return &___U3CiU3E5__1_5; }
	inline void set_U3CiU3E5__1_5(int32_t value)
	{
		___U3CiU3E5__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CincludeUserEditorTypesU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CincludeUserEditorTypesU3E5__2_6)); }
	inline bool get_U3CincludeUserEditorTypesU3E5__2_6() const { return ___U3CincludeUserEditorTypesU3E5__2_6; }
	inline bool* get_address_of_U3CincludeUserEditorTypesU3E5__2_6() { return &___U3CincludeUserEditorTypesU3E5__2_6; }
	inline void set_U3CincludeUserEditorTypesU3E5__2_6(bool value)
	{
		___U3CincludeUserEditorTypesU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__3_7)); }
	inline int32_t get_U3CiU3E5__3_7() const { return ___U3CiU3E5__3_7; }
	inline int32_t* get_address_of_U3CiU3E5__3_7() { return &___U3CiU3E5__3_7; }
	inline void set_U3CiU3E5__3_7(int32_t value)
	{
		___U3CiU3E5__3_7 = value;
	}

	inline static int32_t get_offset_of_U3CincludePluginTypesU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CincludePluginTypesU3E5__4_8)); }
	inline bool get_U3CincludePluginTypesU3E5__4_8() const { return ___U3CincludePluginTypesU3E5__4_8; }
	inline bool* get_address_of_U3CincludePluginTypesU3E5__4_8() { return &___U3CincludePluginTypesU3E5__4_8; }
	inline void set_U3CincludePluginTypesU3E5__4_8(bool value)
	{
		___U3CincludePluginTypesU3E5__4_8 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__5_9)); }
	inline int32_t get_U3CiU3E5__5_9() const { return ___U3CiU3E5__5_9; }
	inline int32_t* get_address_of_U3CiU3E5__5_9() { return &___U3CiU3E5__5_9; }
	inline void set_U3CiU3E5__5_9(int32_t value)
	{
		___U3CiU3E5__5_9 = value;
	}

	inline static int32_t get_offset_of_U3CincludePluginEditorTypesU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CincludePluginEditorTypesU3E5__6_10)); }
	inline bool get_U3CincludePluginEditorTypesU3E5__6_10() const { return ___U3CincludePluginEditorTypesU3E5__6_10; }
	inline bool* get_address_of_U3CincludePluginEditorTypesU3E5__6_10() { return &___U3CincludePluginEditorTypesU3E5__6_10; }
	inline void set_U3CincludePluginEditorTypesU3E5__6_10(bool value)
	{
		___U3CincludePluginEditorTypesU3E5__6_10 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__7_11() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__7_11)); }
	inline int32_t get_U3CiU3E5__7_11() const { return ___U3CiU3E5__7_11; }
	inline int32_t* get_address_of_U3CiU3E5__7_11() { return &___U3CiU3E5__7_11; }
	inline void set_U3CiU3E5__7_11(int32_t value)
	{
		___U3CiU3E5__7_11 = value;
	}

	inline static int32_t get_offset_of_U3CincludeUnityTypesU3E5__8_12() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CincludeUnityTypesU3E5__8_12)); }
	inline bool get_U3CincludeUnityTypesU3E5__8_12() const { return ___U3CincludeUnityTypesU3E5__8_12; }
	inline bool* get_address_of_U3CincludeUnityTypesU3E5__8_12() { return &___U3CincludeUnityTypesU3E5__8_12; }
	inline void set_U3CincludeUnityTypesU3E5__8_12(bool value)
	{
		___U3CincludeUnityTypesU3E5__8_12 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__9_13() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__9_13)); }
	inline int32_t get_U3CiU3E5__9_13() const { return ___U3CiU3E5__9_13; }
	inline int32_t* get_address_of_U3CiU3E5__9_13() { return &___U3CiU3E5__9_13; }
	inline void set_U3CiU3E5__9_13(int32_t value)
	{
		___U3CiU3E5__9_13 = value;
	}

	inline static int32_t get_offset_of_U3CincludeUnityEditorTypesU3E5__10_14() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CincludeUnityEditorTypesU3E5__10_14)); }
	inline bool get_U3CincludeUnityEditorTypesU3E5__10_14() const { return ___U3CincludeUnityEditorTypesU3E5__10_14; }
	inline bool* get_address_of_U3CincludeUnityEditorTypesU3E5__10_14() { return &___U3CincludeUnityEditorTypesU3E5__10_14; }
	inline void set_U3CincludeUnityEditorTypesU3E5__10_14(bool value)
	{
		___U3CincludeUnityEditorTypesU3E5__10_14 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__11_15() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__11_15)); }
	inline int32_t get_U3CiU3E5__11_15() const { return ___U3CiU3E5__11_15; }
	inline int32_t* get_address_of_U3CiU3E5__11_15() { return &___U3CiU3E5__11_15; }
	inline void set_U3CiU3E5__11_15(int32_t value)
	{
		___U3CiU3E5__11_15 = value;
	}

	inline static int32_t get_offset_of_U3CincludeOtherTypesU3E5__12_16() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CincludeOtherTypesU3E5__12_16)); }
	inline bool get_U3CincludeOtherTypesU3E5__12_16() const { return ___U3CincludeOtherTypesU3E5__12_16; }
	inline bool* get_address_of_U3CincludeOtherTypesU3E5__12_16() { return &___U3CincludeOtherTypesU3E5__12_16; }
	inline void set_U3CincludeOtherTypesU3E5__12_16(bool value)
	{
		___U3CincludeOtherTypesU3E5__12_16 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__13_17() { return static_cast<int32_t>(offsetof(U3CGetTypesU3Ed__38_t280155023, ___U3CiU3E5__13_17)); }
	inline int32_t get_U3CiU3E5__13_17() const { return ___U3CiU3E5__13_17; }
	inline int32_t* get_address_of_U3CiU3E5__13_17() { return &___U3CiU3E5__13_17; }
	inline void set_U3CiU3E5__13_17(int32_t value)
	{
		___U3CiU3E5__13_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTYPESU3ED__38_T280155023_H
#ifndef FLAGS_T416255677_H
#define FLAGS_T416255677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.Flags
struct  Flags_t416255677  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T416255677_H
#ifndef MEMBERALIASMETHODINFO_T2838764708_H
#define MEMBERALIASMETHODINFO_T2838764708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.MemberAliasMethodInfo
struct  MemberAliasMethodInfo_t2838764708  : public MethodInfo_t
{
public:
	// System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::aliasedMethod
	MethodInfo_t * ___aliasedMethod_1;
	// System.String Sirenix.Serialization.Utilities.MemberAliasMethodInfo::mangledName
	String_t* ___mangledName_2;

public:
	inline static int32_t get_offset_of_aliasedMethod_1() { return static_cast<int32_t>(offsetof(MemberAliasMethodInfo_t2838764708, ___aliasedMethod_1)); }
	inline MethodInfo_t * get_aliasedMethod_1() const { return ___aliasedMethod_1; }
	inline MethodInfo_t ** get_address_of_aliasedMethod_1() { return &___aliasedMethod_1; }
	inline void set_aliasedMethod_1(MethodInfo_t * value)
	{
		___aliasedMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliasedMethod_1), value);
	}

	inline static int32_t get_offset_of_mangledName_2() { return static_cast<int32_t>(offsetof(MemberAliasMethodInfo_t2838764708, ___mangledName_2)); }
	inline String_t* get_mangledName_2() const { return ___mangledName_2; }
	inline String_t** get_address_of_mangledName_2() { return &___mangledName_2; }
	inline void set_mangledName_2(String_t* value)
	{
		___mangledName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mangledName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERALIASMETHODINFO_T2838764708_H
#ifndef U3CGETALLMEMBERSU3ED__42_T2156530709_H
#define U3CGETALLMEMBERSU3ED__42_T2156530709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42
struct  U3CGetAllMembersU3Ed__42_t2156530709  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>2__current
	MemberInfo_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<currentType>5__1
	Type_t * ___U3CcurrentTypeU3E5__1_7;
	// System.Reflection.MemberInfo[] Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>7__wrap1
	MemberInfoU5BU5D_t1302094432* ___U3CU3E7__wrap1_8;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E2__current_1)); }
	inline MemberInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MemberInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MemberInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTypeU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CcurrentTypeU3E5__1_7)); }
	inline Type_t * get_U3CcurrentTypeU3E5__1_7() const { return ___U3CcurrentTypeU3E5__1_7; }
	inline Type_t ** get_address_of_U3CcurrentTypeU3E5__1_7() { return &___U3CcurrentTypeU3E5__1_7; }
	inline void set_U3CcurrentTypeU3E5__1_7(Type_t * value)
	{
		___U3CcurrentTypeU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentTypeU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_8() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E7__wrap1_8)); }
	inline MemberInfoU5BU5D_t1302094432* get_U3CU3E7__wrap1_8() const { return ___U3CU3E7__wrap1_8; }
	inline MemberInfoU5BU5D_t1302094432** get_address_of_U3CU3E7__wrap1_8() { return &___U3CU3E7__wrap1_8; }
	inline void set_U3CU3E7__wrap1_8(MemberInfoU5BU5D_t1302094432* value)
	{
		___U3CU3E7__wrap1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_9() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__42_t2156530709, ___U3CU3E7__wrap2_9)); }
	inline int32_t get_U3CU3E7__wrap2_9() const { return ___U3CU3E7__wrap2_9; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_9() { return &___U3CU3E7__wrap2_9; }
	inline void set_U3CU3E7__wrap2_9(int32_t value)
	{
		___U3CU3E7__wrap2_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLMEMBERSU3ED__42_T2156530709_H
#ifndef U3CGETALLMEMBERSU3ED__43_T590446768_H
#define U3CGETALLMEMBERSU3ED__43_T590446768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43
struct  U3CGetAllMembersU3Ed__43_t590446768  : public RuntimeObject
{
public:
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>2__current
	MemberInfo_t * ___U3CU3E2__current_1;
	// System.Int32 Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::type
	Type_t * ___type_3;
	// System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::flags
	int32_t ___flags_5;
	// System.Reflection.BindingFlags Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>3__flags
	int32_t ___U3CU3E3__flags_6;
	// System.String Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::name
	String_t* ___name_7;
	// System.String Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>3__name
	String_t* ___U3CU3E3__name_8;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E2__current_1)); }
	inline MemberInfo_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MemberInfo_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MemberInfo_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___flags_5)); }
	inline int32_t get_flags_5() const { return ___flags_5; }
	inline int32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(int32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__flags_6() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E3__flags_6)); }
	inline int32_t get_U3CU3E3__flags_6() const { return ___U3CU3E3__flags_6; }
	inline int32_t* get_address_of_U3CU3E3__flags_6() { return &___U3CU3E3__flags_6; }
	inline void set_U3CU3E3__flags_6(int32_t value)
	{
		___U3CU3E3__flags_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__name_8() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E3__name_8)); }
	inline String_t* get_U3CU3E3__name_8() const { return ___U3CU3E3__name_8; }
	inline String_t** get_address_of_U3CU3E3__name_8() { return &___U3CU3E3__name_8; }
	inline void set_U3CU3E3__name_8(String_t* value)
	{
		___U3CU3E3__name_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__name_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CGetAllMembersU3Ed__43_t590446768, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLMEMBERSU3ED__43_T590446768_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef WEAKVALUEGETTER_T2669515580_H
#define WEAKVALUEGETTER_T2669515580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.WeakValueGetter
struct  WeakValueGetter_t2669515580  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKVALUEGETTER_T2669515580_H
#ifndef WEAKVALUESETTER_T2630980412_H
#define WEAKVALUESETTER_T2630980412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Utilities.WeakValueSetter
struct  WeakValueSetter_t2630980412  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKVALUESETTER_T2630980412_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (U3CU3Ec__DisplayClass27_0_t4008831682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[1] = 
{
	U3CU3Ec__DisplayClass27_0_t4008831682::get_offset_of_member_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (U3CU3Ec__DisplayClass28_0_t3961777515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3901[1] = 
{
	U3CU3Ec__DisplayClass28_0_t3961777515::get_offset_of_member_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (FieldInfoExtensions_t401171748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (GarbageFreeIterators_t3847886896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3904[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3906[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3907[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (LinqExtensions_t1130353347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (MemberInfoExtensions_t68516722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (MethodInfoExtensions_t3469285330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (Operator_t1465414565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3912[21] = 
{
	Operator_t1465414565::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (PathUtilities_t4096232836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (PropertyInfoExtensions_t1420793386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (StringExtensions_t508951950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (TypeExtensions_t1614484489), -1, sizeof(TypeExtensions_t1614484489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3916[15] = 
{
	TypeExtensions_t1614484489_StaticFields::get_offset_of_GenericConstraintsSatisfaction_LOCK_0(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_GenericConstraintsSatisfactionInferredParameters_1(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_GenericConstraintsSatisfactionResolvedMap_2(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_GenericConstraintsSatisfactionProcessedParams_3(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_WeaklyTypedTypeCastDelegates_LOCK_4(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_StronglyTypedTypeCastDelegates_LOCK_5(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_WeaklyTypedTypeCastDelegates_6(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_StronglyTypedTypeCastDelegates_7(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_ReservedCSharpKeywords_8(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_TypeNameAlternatives_9(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_CachedNiceNames_LOCK_10(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_CachedNiceNames_11(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_VoidPointerType_12(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_PrimitiveImplicitCasts_13(),
	TypeExtensions_t1614484489_StaticFields::get_offset_of_ExplicitCastIntegrals_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (U3CU3Ec__DisplayClass22_0_t2941737288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3917[1] = 
{
	U3CU3Ec__DisplayClass22_0_t2941737288::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3918[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (U3CU3Ec__DisplayClass35_0_t2941409609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3919[1] = 
{
	U3CU3Ec__DisplayClass35_0_t2941409609::get_offset_of_openGenericInterfaceType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (U3CU3Ec__DisplayClass41_0_t2941671758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3920[1] = 
{
	U3CU3Ec__DisplayClass41_0_t2941671758::get_offset_of_methodName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (U3CGetAllMembersU3Ed__42_t2156530709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[10] = 
{
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_type_3(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_flags_5(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3E3__flags_6(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CcurrentTypeU3E5__1_7(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3E7__wrap1_8(),
	U3CGetAllMembersU3Ed__42_t2156530709::get_offset_of_U3CU3E7__wrap2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (U3CGetAllMembersU3Ed__43_t590446768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[10] = 
{
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_type_3(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3E3__type_4(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_flags_5(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3E3__flags_6(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_name_7(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3E3__name_8(),
	U3CGetAllMembersU3Ed__43_t590446768::get_offset_of_U3CU3E7__wrap1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3923[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (U3CGetBaseClassesU3Ed__48_t3606501197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3924[8] = 
{
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_U3CU3E1__state_0(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_U3CU3E2__current_1(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_type_3(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_U3CU3E3__type_4(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_includeSelf_5(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_U3CU3E3__includeSelf_6(),
	U3CGetBaseClassesU3Ed__48_t3606501197::get_offset_of_U3CcurrentU3E5__1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (UnityExtensions_t2928681996), -1, sizeof(UnityExtensions_t2928681996_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3925[1] = 
{
	UnityExtensions_t2928681996_StaticFields::get_offset_of_UnityObjectCachedPtrFieldGetter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (AssemblyTypeFlags_t2716374150)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3926[13] = 
{
	AssemblyTypeFlags_t2716374150::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (AssemblyUtilities_t2715239875), -1, sizeof(AssemblyUtilities_t2715239875_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3927[26] = 
{
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_userAssemblyPrefixes_0(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_pluginAssemblyPrefixes_1(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_unityEngineAssembly_2(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_LOCK_3(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_projectFolderDirectory_4(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_scriptAssembliesDirectory_5(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_stringTypeLookup_6(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_assemblyTypeFlagLookup_7(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_allAssemblies_8(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_allAssembliesImmutable_9(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_userAssemblies_10(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_userEditorAssemblies_11(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_pluginAssemblies_12(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_pluginEditorAssemblies_13(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_unityAssemblies_14(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_unityEditorAssemblies_15(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_otherAssemblies_16(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_userTypes_17(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_userEditorTypes_18(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_pluginTypes_19(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_pluginEditorTypes_20(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_unityTypes_21(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_unityEditorTypes_22(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_otherTypes_23(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_dataPath_24(),
	AssemblyUtilities_t2715239875_StaticFields::get_offset_of_scriptAssembliesPath_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (U3CU3Ec_t3287483264), -1, sizeof(U3CU3Ec_t3287483264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3928[1] = 
{
	U3CU3Ec_t3287483264_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (U3CGetTypesU3Ed__38_t280155023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3929[18] = 
{
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CU3E1__state_0(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CU3E2__current_1(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_assemblyTypeFlags_3(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CU3E3__assemblyTypeFlags_4(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__1_5(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CincludeUserEditorTypesU3E5__2_6(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__3_7(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CincludePluginTypesU3E5__4_8(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__5_9(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CincludePluginEditorTypesU3E5__6_10(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__7_11(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CincludeUnityTypesU3E5__8_12(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__9_13(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CincludeUnityEditorTypesU3E5__10_14(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__11_15(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CincludeOtherTypesU3E5__12_16(),
	U3CGetTypesU3Ed__38_t280155023::get_offset_of_U3CiU3E5__13_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3931[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (WeakValueGetter_t2669515580), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (WeakValueSetter_t2630980412), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (EmitUtilities_t2178190973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (U3CU3Ec__DisplayClass3_0_t3011262210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3942[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3011262210::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3943[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (U3CU3Ec__DisplayClass5_0_t3393599234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3944[1] = 
{
	U3CU3Ec__DisplayClass5_0_t3393599234::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3945[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3946[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (U3CU3Ec__DisplayClass8_0_t3055892226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3947[1] = 
{
	U3CU3Ec__DisplayClass8_0_t3055892226::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3948[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3949[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (U3CU3Ec__DisplayClass11_0_t3735155750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3950[1] = 
{
	U3CU3Ec__DisplayClass11_0_t3735155750::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (U3CU3Ec__DisplayClass12_0_t3735155749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3951[1] = 
{
	U3CU3Ec__DisplayClass12_0_t3735155749::get_offset_of_propertyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (U3CU3Ec__DisplayClass13_0_t3735155748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3952[1] = 
{
	U3CU3Ec__DisplayClass13_0_t3735155748::get_offset_of_propertyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3953[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3954[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3955[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (U3CU3Ec__DisplayClass21_0_t3601265702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[1] = 
{
	U3CU3Ec__DisplayClass21_0_t3601265702::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3960[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3961[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (Flags_t416255677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3962[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (ImmutableList_t1819414204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3966[1] = 
{
	ImmutableList_t1819414204::get_offset_of_innerList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3967[4] = 
{
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215::get_offset_of_U3CU3E1__state_0(),
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215::get_offset_of_U3CU3E2__current_1(),
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215::get_offset_of_U3CU3E4__this_2(),
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_t57684215::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (MemberAliasFieldInfo_t3617726363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3970[3] = 
{
	0,
	MemberAliasFieldInfo_t3617726363::get_offset_of_aliasedField_1(),
	MemberAliasFieldInfo_t3617726363::get_offset_of_mangledName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (MemberAliasMethodInfo_t2838764708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3971[3] = 
{
	0,
	MemberAliasMethodInfo_t2838764708::get_offset_of_aliasedMethod_1(),
	MemberAliasMethodInfo_t2838764708::get_offset_of_mangledName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (MemberAliasPropertyInfo_t4204559890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3972[3] = 
{
	0,
	MemberAliasPropertyInfo_t4204559890::get_offset_of_aliasedProperty_1(),
	MemberAliasPropertyInfo_t4204559890::get_offset_of_mangledName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (UnityVersion_t744152913), -1, sizeof(UnityVersion_t744152913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3974[2] = 
{
	UnityVersion_t744152913_StaticFields::get_offset_of_Major_0(),
	UnityVersion_t744152913_StaticFields::get_offset_of_Minor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (UnsafeUtilities_t3361570935), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255370), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3976[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U33CDA7449B0586AB873C75C04BB11D4864F5D7392_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (__StaticArrayInitTypeSizeU3D256_t1757367636)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_t1757367636 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (U3CModuleU3E_t692745574), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (AndroidJavaStore_t1912360766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3980[1] = 
{
	AndroidJavaStore_t1912360766::get_offset_of_m_Store_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (AndroidStore_t1462224562)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3981[7] = 
{
	AndroidStore_t1462224562::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (JavaBridge_t3489274022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3984[1] = 
{
	JavaBridge_t3489274022::get_offset_of_forwardTo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (SerializationExtensions_t4283915465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (JSONSerializer_t4060786200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (ScriptingUnityCallback_t4035349519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3987[2] = 
{
	ScriptingUnityCallback_t4035349519::get_offset_of_forwardTo_0(),
	ScriptingUnityCallback_t4035349519::get_offset_of_util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (AmazonAppStoreStoreExtensions_t4153153958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3988[1] = 
{
	AmazonAppStoreStoreExtensions_t4153153958::get_offset_of_android_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (FakeAmazonExtensions_t2007213879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (FakeMoolahConfiguration_t983287713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3992[2] = 
{
	FakeMoolahConfiguration_t983287713::get_offset_of_m_appKey_0(),
	FakeMoolahConfiguration_t983287713::get_offset_of_m_hashKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (FakeMoolahExtensions_t4194088837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (CloudMoolahMode_t3412107490)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3994[4] = 
{
	CloudMoolahMode_t3412107490::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (ValidateReceiptState_t3921901150)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3996[4] = 
{
	ValidateReceiptState_t3921901150::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (RestoreTransactionIDState_t1240344018)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3997[5] = 
{
	RestoreTransactionIDState_t1240344018::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (TradeSeqState_t912436049)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3998[7] = 
{
	TradeSeqState_t912436049::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
