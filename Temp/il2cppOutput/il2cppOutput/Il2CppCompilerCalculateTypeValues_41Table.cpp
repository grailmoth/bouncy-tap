﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t1264377477;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t2394327294;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_t3199643908;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct List_1_t1741830302;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>
struct List_1_t2186087874;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>
struct List_1_t1811801880;
// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform>
struct List_1_t1336965349;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride>
struct ReadOnlyCollection_1_t4273630488;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t1761491126;
// System.Func`2<System.Reflection.FieldInfo,System.Int32>
struct Func_2_t320181618;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean>
struct Func_2_t3556253065;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String>
struct Func_2_t1011448493;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>
struct Func_2_t1244879711;
// System.Func`3<UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Matrix4x4>
struct Func_3_t2888966892;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// Uniject.IUtil
struct IUtil_t1069285358;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.ILogger
struct ILogger_t2607134790;
// UnityEngine.Purchasing.Default.IWindowsIAP
struct IWindowsIAP_t4053602182;
// UnityEngine.Purchasing.EventQueue
struct EventQueue_t2155959417;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t764570979;
// UnityEngine.Purchasing.Extension.PurchaseFailureDescription
struct PurchaseFailureDescription_t437632294;
// UnityEngine.Purchasing.Extension.UnityUtil
struct UnityUtil_t103543446;
// UnityEngine.Purchasing.FakeStore
struct FakeStore_t3710170489;
// UnityEngine.Purchasing.INativeFacebookStore
struct INativeFacebookStore_t2453197597;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t2607409403;
// UnityEngine.Purchasing.INativeTizenStore
struct INativeTizenStore_t2727987947;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t339727138;
// UnityEngine.Purchasing.ProfileData
struct ProfileData_t3386201266;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_t2580735509;
// UnityEngine.Purchasing.StoreCatalogImpl
struct StoreCatalogImpl_t2383312173;
// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct DialogRequest_t599015159;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RenderTexture[][]
struct RenderTextureU5BU5DU5BU5D_t847993469;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusion
struct AmbientOcclusion_t1140100160;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusionModeParameter
struct AmbientOcclusionModeParameter_t3592449485;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQualityParameter
struct AmbientOcclusionQualityParameter_t3820917191;
// UnityEngine.Rendering.PostProcessing.AutoExposure
struct AutoExposure_t2470830169;
// UnityEngine.Rendering.PostProcessing.Bloom
struct Bloom_t2742718173;
// UnityEngine.Rendering.PostProcessing.BloomRenderer/Level[]
struct LevelU5BU5D_t1986557484;
// UnityEngine.Rendering.PostProcessing.BoolParameter
struct BoolParameter_t2299103272;
// UnityEngine.Rendering.PostProcessing.ChromaticAberration
struct ChromaticAberration_t2835309264;
// UnityEngine.Rendering.PostProcessing.ColorGrading
struct ColorGrading_t1956993830;
// UnityEngine.Rendering.PostProcessing.ColorParameter
struct ColorParameter_t2998827320;
// UnityEngine.Rendering.PostProcessing.DepthOfField
struct DepthOfField_t1057712103;
// UnityEngine.Rendering.PostProcessing.EyeAdaptationParameter
struct EyeAdaptationParameter_t2946234496;
// UnityEngine.Rendering.PostProcessing.FloatParameter
struct FloatParameter_t1840207740;
// UnityEngine.Rendering.PostProcessing.GradingModeParameter
struct GradingModeParameter_t3659529292;
// UnityEngine.Rendering.PostProcessing.Grain
struct Grain_t3181688601;
// UnityEngine.Rendering.PostProcessing.HableCurve
struct HableCurve_t1612137718;
// UnityEngine.Rendering.PostProcessing.IAmbientOcclusionMethod[]
struct IAmbientOcclusionMethodU5BU5D_t106627570;
// UnityEngine.Rendering.PostProcessing.IntParameter
struct IntParameter_t773781776;
// UnityEngine.Rendering.PostProcessing.KernelSizeParameter
struct KernelSizeParameter_t3218545409;
// UnityEngine.Rendering.PostProcessing.LensDistortion
struct LensDistortion_t708814765;
// UnityEngine.Rendering.PostProcessing.MotionBlur
struct MotionBlur_t2809166275;
// UnityEngine.Rendering.PostProcessing.PostProcessResources
struct PostProcessResources_t1163236733;
// UnityEngine.Rendering.PostProcessing.PropertySheet
struct PropertySheet_t3821403501;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPresetParameter
struct ScreenSpaceReflectionPresetParameter_t2494457668;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolutionParameter
struct ScreenSpaceReflectionResolutionParameter_t1804578420;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections
struct ScreenSpaceReflections_t3117296337;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset[]
struct QualityPresetU5BU5D_t2343555814;
// UnityEngine.Rendering.PostProcessing.SplineParameter
struct SplineParameter_t905443520;
// UnityEngine.Rendering.PostProcessing.TextureParameter
struct TextureParameter_t4267400415;
// UnityEngine.Rendering.PostProcessing.TonemapperParameter
struct TonemapperParameter_t2646255172;
// UnityEngine.Rendering.PostProcessing.Vector2Parameter
struct Vector2Parameter_t1794608574;
// UnityEngine.Rendering.PostProcessing.Vector4Parameter
struct Vector4Parameter_t1505856958;
// UnityEngine.Rendering.PostProcessing.Vignette
struct Vignette_t2084058635;
// UnityEngine.Rendering.PostProcessing.VignetteModeParameter
struct VignetteModeParameter_t1229959487;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t2742279485;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifndef U3CMODULEU3E_T692745576_H
#define U3CMODULEU3E_T692745576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745576 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745576_H
#ifndef U3CMODULEU3E_T692745575_H
#define U3CMODULEU3E_T692745575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745575 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745575_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ABSTRACTSTORE_T285429589_H
#define ABSTRACTSTORE_T285429589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractStore
struct  AbstractStore_t285429589  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTSTORE_T285429589_H
#ifndef U3CDELAYEDCOROUTINEU3ED__49_T3667477287_H
#define U3CDELAYEDCOROUTINEU3ED__49_T3667477287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__49
struct  U3CDelayedCoroutineU3Ed__49_t3667477287  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__49::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Collections.IEnumerator UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__49::coroutine
	RuntimeObject* ___coroutine_2;
	// System.Int32 UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__49::delay
	int32_t ___delay_3;
	// UnityEngine.Purchasing.Extension.UnityUtil UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__49::<>4__this
	UnityUtil_t103543446 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__49_t3667477287, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__49_t3667477287, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_coroutine_2() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__49_t3667477287, ___coroutine_2)); }
	inline RuntimeObject* get_coroutine_2() const { return ___coroutine_2; }
	inline RuntimeObject** get_address_of_coroutine_2() { return &___coroutine_2; }
	inline void set_coroutine_2(RuntimeObject* value)
	{
		___coroutine_2 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__49_t3667477287, ___delay_3)); }
	inline int32_t get_delay_3() const { return ___delay_3; }
	inline int32_t* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(int32_t value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__49_t3667477287, ___U3CU3E4__this_4)); }
	inline UnityUtil_t103543446 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline UnityUtil_t103543446 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(UnityUtil_t103543446 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDCOROUTINEU3ED__49_T3667477287_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T1261164119_H
#define U3CU3EC__DISPLAYCLASS6_0_T1261164119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t1261164119  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::subject
	String_t* ___subject_0;
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::payload
	String_t* ___payload_1;
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::receipt
	String_t* ___receipt_2;
	// System.String UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::transactionId
	String_t* ___transactionId_3;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___subject_0)); }
	inline String_t* get_subject_0() const { return ___subject_0; }
	inline String_t** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(String_t* value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier((&___subject_0), value);
	}

	inline static int32_t get_offset_of_payload_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___payload_1)); }
	inline String_t* get_payload_1() const { return ___payload_1; }
	inline String_t** get_address_of_payload_1() { return &___payload_1; }
	inline void set_payload_1(String_t* value)
	{
		___payload_1 = value;
		Il2CppCodeGenWriteBarrier((&___payload_1), value);
	}

	inline static int32_t get_offset_of_receipt_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___receipt_2)); }
	inline String_t* get_receipt_2() const { return ___receipt_2; }
	inline String_t** get_address_of_receipt_2() { return &___receipt_2; }
	inline void set_receipt_2(String_t* value)
	{
		___receipt_2 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_2), value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t1261164119, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T1261164119_H
#ifndef FAKEMICROSOFTEXTENSIONS_T4237934947_H
#define FAKEMICROSOFTEXTENSIONS_T4237934947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeMicrosoftExtensions
struct  FakeMicrosoftExtensions_t4237934947  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMICROSOFTEXTENSIONS_T4237934947_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T1153436473_H
#define U3CU3EC__DISPLAYCLASS13_0_T1153436473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t1153436473  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription> UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0::products
	List_1_t2186087874 * ___products_0;
	// UnityEngine.Purchasing.FakeStore UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0::<>4__this
	FakeStore_t3710170489 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_products_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t1153436473, ___products_0)); }
	inline List_1_t2186087874 * get_products_0() const { return ___products_0; }
	inline List_1_t2186087874 ** get_address_of_products_0() { return &___products_0; }
	inline void set_products_0(List_1_t2186087874 * value)
	{
		___products_0 = value;
		Il2CppCodeGenWriteBarrier((&___products_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t1153436473, ___U3CU3E4__this_1)); }
	inline FakeStore_t3710170489 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FakeStore_t3710170489 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FakeStore_t3710170489 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T1153436473_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T771099449_H
#define U3CU3EC__DISPLAYCLASS15_0_T771099449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t771099449  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.FakeStore/<>c__DisplayClass15_0::product
	ProductDefinition_t339727138 * ___product_0;
	// UnityEngine.Purchasing.FakeStore UnityEngine.Purchasing.FakeStore/<>c__DisplayClass15_0::<>4__this
	FakeStore_t3710170489 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_product_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t771099449, ___product_0)); }
	inline ProductDefinition_t339727138 * get_product_0() const { return ___product_0; }
	inline ProductDefinition_t339727138 ** get_address_of_product_0() { return &___product_0; }
	inline void set_product_0(ProductDefinition_t339727138 * value)
	{
		___product_0 = value;
		Il2CppCodeGenWriteBarrier((&___product_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t771099449, ___U3CU3E4__this_1)); }
	inline FakeStore_t3710170489 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FakeStore_t3710170489 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FakeStore_t3710170489 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T771099449_H
#ifndef FAKETIZENSTORECONFIGURATION_T714964994_H
#define FAKETIZENSTORECONFIGURATION_T714964994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeTizenStoreConfiguration
struct  FakeTizenStoreConfiguration_t714964994  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKETIZENSTORECONFIGURATION_T714964994_H
#ifndef FAKETRANSACTIONHISTORYEXTENSIONS_T1765525329_H
#define FAKETRANSACTIONHISTORYEXTENSIONS_T1765525329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeTransactionHistoryExtensions
struct  FakeTransactionHistoryExtensions_t1765525329  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKETRANSACTIONHISTORYEXTENSIONS_T1765525329_H
#ifndef FILEREFERENCE_T2849312398_H
#define FILEREFERENCE_T2849312398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FileReference
struct  FileReference_t2849312398  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.FileReference::m_FilePath
	String_t* ___m_FilePath_0;
	// UnityEngine.ILogger UnityEngine.Purchasing.FileReference::m_Logger
	RuntimeObject* ___m_Logger_1;

public:
	inline static int32_t get_offset_of_m_FilePath_0() { return static_cast<int32_t>(offsetof(FileReference_t2849312398, ___m_FilePath_0)); }
	inline String_t* get_m_FilePath_0() const { return ___m_FilePath_0; }
	inline String_t** get_address_of_m_FilePath_0() { return &___m_FilePath_0; }
	inline void set_m_FilePath_0(String_t* value)
	{
		___m_FilePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_FilePath_0), value);
	}

	inline static int32_t get_offset_of_m_Logger_1() { return static_cast<int32_t>(offsetof(FileReference_t2849312398, ___m_Logger_1)); }
	inline RuntimeObject* get_m_Logger_1() const { return ___m_Logger_1; }
	inline RuntimeObject** get_address_of_m_Logger_1() { return &___m_Logger_1; }
	inline void set_m_Logger_1(RuntimeObject* value)
	{
		___m_Logger_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEREFERENCE_T2849312398_H
#ifndef PRODUCTDEFINITIONEXTENSIONS_T3868030593_H
#define PRODUCTDEFINITIONEXTENSIONS_T3868030593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductDefinitionExtensions
struct  ProductDefinitionExtensions_t3868030593  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTDEFINITIONEXTENSIONS_T3868030593_H
#ifndef U3CU3EC_T126346862_H
#define U3CU3EC_T126346862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore/<>c
struct  U3CU3Ec_t126346862  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t126346862_StaticFields
{
public:
	// UnityEngine.Purchasing.UIFakeStore/<>c UnityEngine.Purchasing.UIFakeStore/<>c::<>9
	U3CU3Ec_t126346862 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String> UnityEngine.Purchasing.UIFakeStore/<>c::<>9__18_0
	Func_2_t1011448493 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t126346862_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t126346862 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t126346862 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t126346862 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t126346862_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t1011448493 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t1011448493 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t1011448493 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T126346862_H
#ifndef DIALOGREQUEST_T599015159_H
#define DIALOGREQUEST_T599015159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct  DialogRequest_t599015159  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UIFakeStore/DialogRequest::QueryText
	String_t* ___QueryText_0;
	// System.String UnityEngine.Purchasing.UIFakeStore/DialogRequest::OkayButtonText
	String_t* ___OkayButtonText_1;
	// System.String UnityEngine.Purchasing.UIFakeStore/DialogRequest::CancelButtonText
	String_t* ___CancelButtonText_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Purchasing.UIFakeStore/DialogRequest::Options
	List_1_t3319525431 * ___Options_3;
	// System.Action`2<System.Boolean,System.Int32> UnityEngine.Purchasing.UIFakeStore/DialogRequest::Callback
	Action_2_t2394327294 * ___Callback_4;

public:
	inline static int32_t get_offset_of_QueryText_0() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___QueryText_0)); }
	inline String_t* get_QueryText_0() const { return ___QueryText_0; }
	inline String_t** get_address_of_QueryText_0() { return &___QueryText_0; }
	inline void set_QueryText_0(String_t* value)
	{
		___QueryText_0 = value;
		Il2CppCodeGenWriteBarrier((&___QueryText_0), value);
	}

	inline static int32_t get_offset_of_OkayButtonText_1() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___OkayButtonText_1)); }
	inline String_t* get_OkayButtonText_1() const { return ___OkayButtonText_1; }
	inline String_t** get_address_of_OkayButtonText_1() { return &___OkayButtonText_1; }
	inline void set_OkayButtonText_1(String_t* value)
	{
		___OkayButtonText_1 = value;
		Il2CppCodeGenWriteBarrier((&___OkayButtonText_1), value);
	}

	inline static int32_t get_offset_of_CancelButtonText_2() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___CancelButtonText_2)); }
	inline String_t* get_CancelButtonText_2() const { return ___CancelButtonText_2; }
	inline String_t** get_address_of_CancelButtonText_2() { return &___CancelButtonText_2; }
	inline void set_CancelButtonText_2(String_t* value)
	{
		___CancelButtonText_2 = value;
		Il2CppCodeGenWriteBarrier((&___CancelButtonText_2), value);
	}

	inline static int32_t get_offset_of_Options_3() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___Options_3)); }
	inline List_1_t3319525431 * get_Options_3() const { return ___Options_3; }
	inline List_1_t3319525431 ** get_address_of_Options_3() { return &___Options_3; }
	inline void set_Options_3(List_1_t3319525431 * value)
	{
		___Options_3 = value;
		Il2CppCodeGenWriteBarrier((&___Options_3), value);
	}

	inline static int32_t get_offset_of_Callback_4() { return static_cast<int32_t>(offsetof(DialogRequest_t599015159, ___Callback_4)); }
	inline Action_2_t2394327294 * get_Callback_4() const { return ___Callback_4; }
	inline Action_2_t2394327294 ** get_address_of_Callback_4() { return &___Callback_4; }
	inline void set_Callback_4(Action_2_t2394327294 * value)
	{
		___Callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGREQUEST_T599015159_H
#ifndef UNIFIEDRECEIPT_T1348780434_H
#define UNIFIEDRECEIPT_T1348780434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnifiedReceipt
struct  UnifiedReceipt_t1348780434  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UnifiedReceipt::Payload
	String_t* ___Payload_0;

public:
	inline static int32_t get_offset_of_Payload_0() { return static_cast<int32_t>(offsetof(UnifiedReceipt_t1348780434, ___Payload_0)); }
	inline String_t* get_Payload_0() const { return ___Payload_0; }
	inline String_t** get_address_of_Payload_0() { return &___Payload_0; }
	inline void set_Payload_0(String_t* value)
	{
		___Payload_0 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFIEDRECEIPT_T1348780434_H
#ifndef U3CU3EC_T3746618910_H
#define U3CU3EC_T3746618910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.WinRTStore/<>c
struct  U3CU3Ec_t3746618910  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3746618910_StaticFields
{
public:
	// UnityEngine.Purchasing.WinRTStore/<>c UnityEngine.Purchasing.WinRTStore/<>c::<>9
	U3CU3Ec_t3746618910 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean> UnityEngine.Purchasing.WinRTStore/<>c::<>9__8_0
	Func_2_t3556253065 * ___U3CU3E9__8_0_1;
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription> UnityEngine.Purchasing.WinRTStore/<>c::<>9__8_1
	Func_2_t1244879711 * ___U3CU3E9__8_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3746618910_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3746618910 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3746618910 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3746618910 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3746618910_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t3556253065 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t3556253065 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t3556253065 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3746618910_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t1244879711 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t1244879711 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t1244879711 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3746618910_H
#ifndef DITHERING_T544635223_H
#define DITHERING_T544635223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Dithering
struct  Dithering_t544635223  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.Dithering::m_NoiseTextureIndex
	int32_t ___m_NoiseTextureIndex_0;

public:
	inline static int32_t get_offset_of_m_NoiseTextureIndex_0() { return static_cast<int32_t>(offsetof(Dithering_t544635223, ___m_NoiseTextureIndex_0)); }
	inline int32_t get_m_NoiseTextureIndex_0() const { return ___m_NoiseTextureIndex_0; }
	inline int32_t* get_address_of_m_NoiseTextureIndex_0() { return &___m_NoiseTextureIndex_0; }
	inline void set_m_NoiseTextureIndex_0(int32_t value)
	{
		___m_NoiseTextureIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERING_T544635223_H
#ifndef FASTAPPROXIMATEANTIALIASING_T3757489215_H
#define FASTAPPROXIMATEANTIALIASING_T3757489215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing
struct  FastApproximateAntialiasing_t3757489215  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing::fastMode
	bool ___fastMode_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing::keepAlpha
	bool ___keepAlpha_1;

public:
	inline static int32_t get_offset_of_fastMode_0() { return static_cast<int32_t>(offsetof(FastApproximateAntialiasing_t3757489215, ___fastMode_0)); }
	inline bool get_fastMode_0() const { return ___fastMode_0; }
	inline bool* get_address_of_fastMode_0() { return &___fastMode_0; }
	inline void set_fastMode_0(bool value)
	{
		___fastMode_0 = value;
	}

	inline static int32_t get_offset_of_keepAlpha_1() { return static_cast<int32_t>(offsetof(FastApproximateAntialiasing_t3757489215, ___keepAlpha_1)); }
	inline bool get_keepAlpha_1() const { return ___keepAlpha_1; }
	inline bool* get_address_of_keepAlpha_1() { return &___keepAlpha_1; }
	inline void set_keepAlpha_1(bool value)
	{
		___keepAlpha_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTAPPROXIMATEANTIALIASING_T3757489215_H
#ifndef FOG_T2420217198_H
#define FOG_T2420217198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Fog
struct  Fog_t2420217198  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.Fog::enabled
	bool ___enabled_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.Fog::excludeSkybox
	bool ___excludeSkybox_1;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(Fog_t2420217198, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_excludeSkybox_1() { return static_cast<int32_t>(offsetof(Fog_t2420217198, ___excludeSkybox_1)); }
	inline bool get_excludeSkybox_1() const { return ___excludeSkybox_1; }
	inline bool* get_address_of_excludeSkybox_1() { return &___excludeSkybox_1; }
	inline void set_excludeSkybox_1(bool value)
	{
		___excludeSkybox_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOG_T2420217198_H
#ifndef MONITOR_T1754509597_H
#define MONITOR_T1754509597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Monitor
struct  Monitor_t1754509597  : public RuntimeObject
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.Monitor::<output>k__BackingField
	RenderTexture_t2108887433 * ___U3CoutputU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.Monitor::requested
	bool ___requested_1;

public:
	inline static int32_t get_offset_of_U3CoutputU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Monitor_t1754509597, ___U3CoutputU3Ek__BackingField_0)); }
	inline RenderTexture_t2108887433 * get_U3CoutputU3Ek__BackingField_0() const { return ___U3CoutputU3Ek__BackingField_0; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CoutputU3Ek__BackingField_0() { return &___U3CoutputU3Ek__BackingField_0; }
	inline void set_U3CoutputU3Ek__BackingField_0(RenderTexture_t2108887433 * value)
	{
		___U3CoutputU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_requested_1() { return static_cast<int32_t>(offsetof(Monitor_t1754509597, ___requested_1)); }
	inline bool get_requested_1() const { return ___requested_1; }
	inline bool* get_address_of_requested_1() { return &___requested_1; }
	inline void set_requested_1(bool value)
	{
		___requested_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONITOR_T1754509597_H
#ifndef MULTISCALEVO_T1807722745_H
#define MULTISCALEVO_T1807722745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MultiScaleVO
struct  MultiScaleVO_t1807722745  : public RuntimeObject
{
public:
	// System.Single[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_SampleThickness
	SingleU5BU5D_t1444911251* ___m_SampleThickness_0;
	// System.Single[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_InvThicknessTable
	SingleU5BU5D_t1444911251* ___m_InvThicknessTable_1;
	// System.Single[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_SampleWeightTable
	SingleU5BU5D_t1444911251* ___m_SampleWeightTable_2;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Widths
	Int32U5BU5D_t385246372* ___m_Widths_3;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Heights
	Int32U5BU5D_t385246372* ___m_Heights_4;
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusion UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Settings
	AmbientOcclusion_t1140100160 * ___m_Settings_5;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_PropertySheet
	PropertySheet_t3821403501 * ___m_PropertySheet_6;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Resources
	PostProcessResources_t1163236733 * ___m_Resources_7;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_AmbientOnlyAO
	RenderTexture_t2108887433 * ___m_AmbientOnlyAO_8;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_9;

public:
	inline static int32_t get_offset_of_m_SampleThickness_0() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_SampleThickness_0)); }
	inline SingleU5BU5D_t1444911251* get_m_SampleThickness_0() const { return ___m_SampleThickness_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_SampleThickness_0() { return &___m_SampleThickness_0; }
	inline void set_m_SampleThickness_0(SingleU5BU5D_t1444911251* value)
	{
		___m_SampleThickness_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_SampleThickness_0), value);
	}

	inline static int32_t get_offset_of_m_InvThicknessTable_1() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_InvThicknessTable_1)); }
	inline SingleU5BU5D_t1444911251* get_m_InvThicknessTable_1() const { return ___m_InvThicknessTable_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_InvThicknessTable_1() { return &___m_InvThicknessTable_1; }
	inline void set_m_InvThicknessTable_1(SingleU5BU5D_t1444911251* value)
	{
		___m_InvThicknessTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvThicknessTable_1), value);
	}

	inline static int32_t get_offset_of_m_SampleWeightTable_2() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_SampleWeightTable_2)); }
	inline SingleU5BU5D_t1444911251* get_m_SampleWeightTable_2() const { return ___m_SampleWeightTable_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_SampleWeightTable_2() { return &___m_SampleWeightTable_2; }
	inline void set_m_SampleWeightTable_2(SingleU5BU5D_t1444911251* value)
	{
		___m_SampleWeightTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SampleWeightTable_2), value);
	}

	inline static int32_t get_offset_of_m_Widths_3() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_Widths_3)); }
	inline Int32U5BU5D_t385246372* get_m_Widths_3() const { return ___m_Widths_3; }
	inline Int32U5BU5D_t385246372** get_address_of_m_Widths_3() { return &___m_Widths_3; }
	inline void set_m_Widths_3(Int32U5BU5D_t385246372* value)
	{
		___m_Widths_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Widths_3), value);
	}

	inline static int32_t get_offset_of_m_Heights_4() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_Heights_4)); }
	inline Int32U5BU5D_t385246372* get_m_Heights_4() const { return ___m_Heights_4; }
	inline Int32U5BU5D_t385246372** get_address_of_m_Heights_4() { return &___m_Heights_4; }
	inline void set_m_Heights_4(Int32U5BU5D_t385246372* value)
	{
		___m_Heights_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Heights_4), value);
	}

	inline static int32_t get_offset_of_m_Settings_5() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_Settings_5)); }
	inline AmbientOcclusion_t1140100160 * get_m_Settings_5() const { return ___m_Settings_5; }
	inline AmbientOcclusion_t1140100160 ** get_address_of_m_Settings_5() { return &___m_Settings_5; }
	inline void set_m_Settings_5(AmbientOcclusion_t1140100160 * value)
	{
		___m_Settings_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_5), value);
	}

	inline static int32_t get_offset_of_m_PropertySheet_6() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_PropertySheet_6)); }
	inline PropertySheet_t3821403501 * get_m_PropertySheet_6() const { return ___m_PropertySheet_6; }
	inline PropertySheet_t3821403501 ** get_address_of_m_PropertySheet_6() { return &___m_PropertySheet_6; }
	inline void set_m_PropertySheet_6(PropertySheet_t3821403501 * value)
	{
		___m_PropertySheet_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheet_6), value);
	}

	inline static int32_t get_offset_of_m_Resources_7() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_Resources_7)); }
	inline PostProcessResources_t1163236733 * get_m_Resources_7() const { return ___m_Resources_7; }
	inline PostProcessResources_t1163236733 ** get_address_of_m_Resources_7() { return &___m_Resources_7; }
	inline void set_m_Resources_7(PostProcessResources_t1163236733 * value)
	{
		___m_Resources_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resources_7), value);
	}

	inline static int32_t get_offset_of_m_AmbientOnlyAO_8() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_AmbientOnlyAO_8)); }
	inline RenderTexture_t2108887433 * get_m_AmbientOnlyAO_8() const { return ___m_AmbientOnlyAO_8; }
	inline RenderTexture_t2108887433 ** get_address_of_m_AmbientOnlyAO_8() { return &___m_AmbientOnlyAO_8; }
	inline void set_m_AmbientOnlyAO_8(RenderTexture_t2108887433 * value)
	{
		___m_AmbientOnlyAO_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_AmbientOnlyAO_8), value);
	}

	inline static int32_t get_offset_of_m_MRT_9() { return static_cast<int32_t>(offsetof(MultiScaleVO_t1807722745, ___m_MRT_9)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_9() const { return ___m_MRT_9; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_9() { return &___m_MRT_9; }
	inline void set_m_MRT_9(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISCALEVO_T1807722745_H
#ifndef PARAMETEROVERRIDE_T3061054201_H
#define PARAMETEROVERRIDE_T3061054201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride
struct  ParameterOverride_t3061054201  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.ParameterOverride::overrideState
	bool ___overrideState_0;

public:
	inline static int32_t get_offset_of_overrideState_0() { return static_cast<int32_t>(offsetof(ParameterOverride_t3061054201, ___overrideState_0)); }
	inline bool get_overrideState_0() const { return ___overrideState_0; }
	inline bool* get_address_of_overrideState_0() { return &___overrideState_0; }
	inline void set_overrideState_0(bool value)
	{
		___overrideState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_T3061054201_H
#ifndef POSTPROCESSEFFECTRENDERER_T1060237_H
#define POSTPROCESSEFFECTRENDERER_T1060237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer
struct  PostProcessEffectRenderer_t1060237  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer::m_ResetHistory
	bool ___m_ResetHistory_0;

public:
	inline static int32_t get_offset_of_m_ResetHistory_0() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_t1060237, ___m_ResetHistory_0)); }
	inline bool get_m_ResetHistory_0() const { return ___m_ResetHistory_0; }
	inline bool* get_address_of_m_ResetHistory_0() { return &___m_ResetHistory_0; }
	inline void set_m_ResetHistory_0(bool value)
	{
		___m_ResetHistory_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_T1060237_H
#ifndef SCALABLEAO_T1980962979_H
#define SCALABLEAO_T1980962979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScalableAO
struct  ScalableAO_t1980962979  : public RuntimeObject
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ScalableAO::m_Result
	RenderTexture_t2108887433 * ___m_Result_0;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.ScalableAO::m_PropertySheet
	PropertySheet_t3821403501 * ___m_PropertySheet_1;
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusion UnityEngine.Rendering.PostProcessing.ScalableAO::m_Settings
	AmbientOcclusion_t1140100160 * ___m_Settings_2;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.PostProcessing.ScalableAO::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_3;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.ScalableAO::m_SampleCount
	Int32U5BU5D_t385246372* ___m_SampleCount_4;

public:
	inline static int32_t get_offset_of_m_Result_0() { return static_cast<int32_t>(offsetof(ScalableAO_t1980962979, ___m_Result_0)); }
	inline RenderTexture_t2108887433 * get_m_Result_0() const { return ___m_Result_0; }
	inline RenderTexture_t2108887433 ** get_address_of_m_Result_0() { return &___m_Result_0; }
	inline void set_m_Result_0(RenderTexture_t2108887433 * value)
	{
		___m_Result_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_0), value);
	}

	inline static int32_t get_offset_of_m_PropertySheet_1() { return static_cast<int32_t>(offsetof(ScalableAO_t1980962979, ___m_PropertySheet_1)); }
	inline PropertySheet_t3821403501 * get_m_PropertySheet_1() const { return ___m_PropertySheet_1; }
	inline PropertySheet_t3821403501 ** get_address_of_m_PropertySheet_1() { return &___m_PropertySheet_1; }
	inline void set_m_PropertySheet_1(PropertySheet_t3821403501 * value)
	{
		___m_PropertySheet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheet_1), value);
	}

	inline static int32_t get_offset_of_m_Settings_2() { return static_cast<int32_t>(offsetof(ScalableAO_t1980962979, ___m_Settings_2)); }
	inline AmbientOcclusion_t1140100160 * get_m_Settings_2() const { return ___m_Settings_2; }
	inline AmbientOcclusion_t1140100160 ** get_address_of_m_Settings_2() { return &___m_Settings_2; }
	inline void set_m_Settings_2(AmbientOcclusion_t1140100160 * value)
	{
		___m_Settings_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_2), value);
	}

	inline static int32_t get_offset_of_m_MRT_3() { return static_cast<int32_t>(offsetof(ScalableAO_t1980962979, ___m_MRT_3)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_3() const { return ___m_MRT_3; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_3() { return &___m_MRT_3; }
	inline void set_m_MRT_3(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_3), value);
	}

	inline static int32_t get_offset_of_m_SampleCount_4() { return static_cast<int32_t>(offsetof(ScalableAO_t1980962979, ___m_SampleCount_4)); }
	inline Int32U5BU5D_t385246372* get_m_SampleCount_4() const { return ___m_SampleCount_4; }
	inline Int32U5BU5D_t385246372** get_address_of_m_SampleCount_4() { return &___m_SampleCount_4; }
	inline void set_m_SampleCount_4(Int32U5BU5D_t385246372* value)
	{
		___m_SampleCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SampleCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALABLEAO_T1980962979_H
#ifndef __STATICARRAYINITTYPESIZEU3D368_T2501028641_H
#define __STATICARRAYINITTYPESIZEU3D368_T2501028641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=368
struct  __StaticArrayInitTypeSizeU3D368_t2501028641 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D368_t2501028641__padding[368];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D368_T2501028641_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef WINRTSTORE_T2015085940_H
#define WINRTSTORE_T2015085940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.WinRTStore
struct  WinRTStore_t2015085940  : public AbstractStore_t285429589
{
public:
	// UnityEngine.Purchasing.Default.IWindowsIAP UnityEngine.Purchasing.WinRTStore::win8
	RuntimeObject* ___win8_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.WinRTStore::callback
	RuntimeObject* ___callback_1;
	// Uniject.IUtil UnityEngine.Purchasing.WinRTStore::util
	RuntimeObject* ___util_2;
	// UnityEngine.ILogger UnityEngine.Purchasing.WinRTStore::logger
	RuntimeObject* ___logger_3;
	// System.Boolean UnityEngine.Purchasing.WinRTStore::m_CanReceivePurchases
	bool ___m_CanReceivePurchases_4;

public:
	inline static int32_t get_offset_of_win8_0() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___win8_0)); }
	inline RuntimeObject* get_win8_0() const { return ___win8_0; }
	inline RuntimeObject** get_address_of_win8_0() { return &___win8_0; }
	inline void set_win8_0(RuntimeObject* value)
	{
		___win8_0 = value;
		Il2CppCodeGenWriteBarrier((&___win8_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___callback_1)); }
	inline RuntimeObject* get_callback_1() const { return ___callback_1; }
	inline RuntimeObject** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(RuntimeObject* value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_util_2() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___util_2)); }
	inline RuntimeObject* get_util_2() const { return ___util_2; }
	inline RuntimeObject** get_address_of_util_2() { return &___util_2; }
	inline void set_util_2(RuntimeObject* value)
	{
		___util_2 = value;
		Il2CppCodeGenWriteBarrier((&___util_2), value);
	}

	inline static int32_t get_offset_of_logger_3() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___logger_3)); }
	inline RuntimeObject* get_logger_3() const { return ___logger_3; }
	inline RuntimeObject** get_address_of_logger_3() { return &___logger_3; }
	inline void set_logger_3(RuntimeObject* value)
	{
		___logger_3 = value;
		Il2CppCodeGenWriteBarrier((&___logger_3), value);
	}

	inline static int32_t get_offset_of_m_CanReceivePurchases_4() { return static_cast<int32_t>(offsetof(WinRTStore_t2015085940, ___m_CanReceivePurchases_4)); }
	inline bool get_m_CanReceivePurchases_4() const { return ___m_CanReceivePurchases_4; }
	inline bool* get_address_of_m_CanReceivePurchases_4() { return &___m_CanReceivePurchases_4; }
	inline void set_m_CanReceivePurchases_4(bool value)
	{
		___m_CanReceivePurchases_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINRTSTORE_T2015085940_H
#ifndef LEVEL_T3444658929_H
#define LEVEL_T3444658929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BloomRenderer/Level
struct  Level_t3444658929 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.BloomRenderer/Level::down
	int32_t ___down_0;
	// System.Int32 UnityEngine.Rendering.PostProcessing.BloomRenderer/Level::up
	int32_t ___up_1;

public:
	inline static int32_t get_offset_of_down_0() { return static_cast<int32_t>(offsetof(Level_t3444658929, ___down_0)); }
	inline int32_t get_down_0() const { return ___down_0; }
	inline int32_t* get_address_of_down_0() { return &___down_0; }
	inline void set_down_0(int32_t value)
	{
		___down_0 = value;
	}

	inline static int32_t get_offset_of_up_1() { return static_cast<int32_t>(offsetof(Level_t3444658929, ___up_1)); }
	inline int32_t get_up_1() const { return ___up_1; }
	inline int32_t* get_address_of_up_1() { return &___up_1; }
	inline void set_up_1(int32_t value)
	{
		___up_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T3444658929_H
#ifndef DISPLAYNAMEATTRIBUTE_T3139068674_H
#define DISPLAYNAMEATTRIBUTE_T3139068674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DisplayNameAttribute
struct  DisplayNameAttribute_t3139068674  : public Attribute_t861562559
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.DisplayNameAttribute::displayName
	String_t* ___displayName_0;

public:
	inline static int32_t get_offset_of_displayName_0() { return static_cast<int32_t>(offsetof(DisplayNameAttribute_t3139068674, ___displayName_0)); }
	inline String_t* get_displayName_0() const { return ___displayName_0; }
	inline String_t** get_address_of_displayName_0() { return &___displayName_0; }
	inline void set_displayName_0(String_t* value)
	{
		___displayName_0 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYNAMEATTRIBUTE_T3139068674_H
#ifndef LIGHTMETERMONITOR_T1816308400_H
#define LIGHTMETERMONITOR_T1816308400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LightMeterMonitor
struct  LightMeterMonitor_t1816308400  : public Monitor_t1754509597
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.LightMeterMonitor::width
	int32_t ___width_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.LightMeterMonitor::height
	int32_t ___height_3;
	// System.Boolean UnityEngine.Rendering.PostProcessing.LightMeterMonitor::showCurves
	bool ___showCurves_4;

public:
	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(LightMeterMonitor_t1816308400, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(LightMeterMonitor_t1816308400, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_showCurves_4() { return static_cast<int32_t>(offsetof(LightMeterMonitor_t1816308400, ___showCurves_4)); }
	inline bool get_showCurves_4() const { return ___showCurves_4; }
	inline bool* get_address_of_showCurves_4() { return &___showCurves_4; }
	inline void set_showCurves_4(bool value)
	{
		___showCurves_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTMETERMONITOR_T1816308400_H
#ifndef MINATTRIBUTE_T1996849270_H
#define MINATTRIBUTE_T1996849270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MinAttribute
struct  MinAttribute_t1996849270  : public Attribute_t861562559
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t1996849270, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T1996849270_H
#ifndef MINMAXATTRIBUTE_T2820105777_H
#define MINMAXATTRIBUTE_T2820105777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MinMaxAttribute
struct  MinMaxAttribute_t2820105777  : public Attribute_t861562559
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.MinMaxAttribute::min
	float ___min_0;
	// System.Single UnityEngine.Rendering.PostProcessing.MinMaxAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinMaxAttribute_t2820105777, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(MinMaxAttribute_t2820105777, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAXATTRIBUTE_T2820105777_H
#ifndef PARAMETEROVERRIDE_1_T3672619081_H
#define PARAMETEROVERRIDE_1_T3672619081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>
struct  ParameterOverride_1_t3672619081  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	float ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t3672619081, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T3672619081_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T3739647461_H
#define POSTPROCESSEFFECTRENDERER_1_T3739647461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.AmbientOcclusion>
struct  PostProcessEffectRenderer_1_t3739647461  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	AmbientOcclusion_t1140100160 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t3739647461, ___U3CsettingsU3Ek__BackingField_1)); }
	inline AmbientOcclusion_t1140100160 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline AmbientOcclusion_t1140100160 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(AmbientOcclusion_t1140100160 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T3739647461_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T775410174_H
#define POSTPROCESSEFFECTRENDERER_1_T775410174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.AutoExposure>
struct  PostProcessEffectRenderer_1_t775410174  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	AutoExposure_t2470830169 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t775410174, ___U3CsettingsU3Ek__BackingField_1)); }
	inline AutoExposure_t2470830169 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline AutoExposure_t2470830169 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(AutoExposure_t2470830169 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T775410174_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T1047298178_H
#define POSTPROCESSEFFECTRENDERER_1_T1047298178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.Bloom>
struct  PostProcessEffectRenderer_1_t1047298178  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	Bloom_t2742718173 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t1047298178, ___U3CsettingsU3Ek__BackingField_1)); }
	inline Bloom_t2742718173 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline Bloom_t2742718173 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(Bloom_t2742718173 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T1047298178_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T1139889269_H
#define POSTPROCESSEFFECTRENDERER_1_T1139889269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.ChromaticAberration>
struct  PostProcessEffectRenderer_1_t1139889269  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	ChromaticAberration_t2835309264 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t1139889269, ___U3CsettingsU3Ek__BackingField_1)); }
	inline ChromaticAberration_t2835309264 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline ChromaticAberration_t2835309264 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(ChromaticAberration_t2835309264 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T1139889269_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T261573835_H
#define POSTPROCESSEFFECTRENDERER_1_T261573835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.ColorGrading>
struct  PostProcessEffectRenderer_1_t261573835  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	ColorGrading_t1956993830 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t261573835, ___U3CsettingsU3Ek__BackingField_1)); }
	inline ColorGrading_t1956993830 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline ColorGrading_t1956993830 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(ColorGrading_t1956993830 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T261573835_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T3657259404_H
#define POSTPROCESSEFFECTRENDERER_1_T3657259404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.DepthOfField>
struct  PostProcessEffectRenderer_1_t3657259404  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	DepthOfField_t1057712103 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t3657259404, ___U3CsettingsU3Ek__BackingField_1)); }
	inline DepthOfField_t1057712103 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline DepthOfField_t1057712103 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(DepthOfField_t1057712103 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T3657259404_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T1486268606_H
#define POSTPROCESSEFFECTRENDERER_1_T1486268606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.Grain>
struct  PostProcessEffectRenderer_1_t1486268606  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	Grain_t3181688601 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t1486268606, ___U3CsettingsU3Ek__BackingField_1)); }
	inline Grain_t3181688601 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline Grain_t3181688601 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(Grain_t3181688601 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T1486268606_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T3308362066_H
#define POSTPROCESSEFFECTRENDERER_1_T3308362066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.LensDistortion>
struct  PostProcessEffectRenderer_1_t3308362066  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	LensDistortion_t708814765 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t3308362066, ___U3CsettingsU3Ek__BackingField_1)); }
	inline LensDistortion_t708814765 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline LensDistortion_t708814765 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(LensDistortion_t708814765 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T3308362066_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T1113746280_H
#define POSTPROCESSEFFECTRENDERER_1_T1113746280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.MotionBlur>
struct  PostProcessEffectRenderer_1_t1113746280  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	MotionBlur_t2809166275 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t1113746280, ___U3CsettingsU3Ek__BackingField_1)); }
	inline MotionBlur_t2809166275 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline MotionBlur_t2809166275 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(MotionBlur_t2809166275 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T1113746280_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T1421876342_H
#define POSTPROCESSEFFECTRENDERER_1_T1421876342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections>
struct  PostProcessEffectRenderer_1_t1421876342  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	ScreenSpaceReflections_t3117296337 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t1421876342, ___U3CsettingsU3Ek__BackingField_1)); }
	inline ScreenSpaceReflections_t3117296337 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline ScreenSpaceReflections_t3117296337 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(ScreenSpaceReflections_t3117296337 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T1421876342_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T388638640_H
#define POSTPROCESSEFFECTRENDERER_1_T388638640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.Vignette>
struct  PostProcessEffectRenderer_1_t388638640  : public PostProcessEffectRenderer_t1060237
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	Vignette_t2084058635 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t388638640, ___U3CsettingsU3Ek__BackingField_1)); }
	inline Vignette_t2084058635 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline Vignette_t2084058635 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(Vignette_t2084058635 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T388638640_H
#ifndef VECTORSCOPEMONITOR_T2083911122_H
#define VECTORSCOPEMONITOR_T2083911122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VectorscopeMonitor
struct  VectorscopeMonitor_t2083911122  : public Monitor_t1754509597
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.VectorscopeMonitor::size
	int32_t ___size_2;
	// System.Single UnityEngine.Rendering.PostProcessing.VectorscopeMonitor::exposure
	float ___exposure_3;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.VectorscopeMonitor::m_Data
	ComputeBuffer_t1033194329 * ___m_Data_4;

public:
	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(VectorscopeMonitor_t2083911122, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_exposure_3() { return static_cast<int32_t>(offsetof(VectorscopeMonitor_t2083911122, ___exposure_3)); }
	inline float get_exposure_3() const { return ___exposure_3; }
	inline float* get_address_of_exposure_3() { return &___exposure_3; }
	inline void set_exposure_3(float value)
	{
		___exposure_3 = value;
	}

	inline static int32_t get_offset_of_m_Data_4() { return static_cast<int32_t>(offsetof(VectorscopeMonitor_t2083911122, ___m_Data_4)); }
	inline ComputeBuffer_t1033194329 * get_m_Data_4() const { return ___m_Data_4; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_Data_4() { return &___m_Data_4; }
	inline void set_m_Data_4(ComputeBuffer_t1033194329 * value)
	{
		___m_Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORSCOPEMONITOR_T2083911122_H
#ifndef WAVEFORMMONITOR_T2029591948_H
#define WAVEFORMMONITOR_T2029591948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.WaveformMonitor
struct  WaveformMonitor_t2029591948  : public Monitor_t1754509597
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.WaveformMonitor::exposure
	float ___exposure_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.WaveformMonitor::height
	int32_t ___height_3;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.WaveformMonitor::m_Data
	ComputeBuffer_t1033194329 * ___m_Data_4;

public:
	inline static int32_t get_offset_of_exposure_2() { return static_cast<int32_t>(offsetof(WaveformMonitor_t2029591948, ___exposure_2)); }
	inline float get_exposure_2() const { return ___exposure_2; }
	inline float* get_address_of_exposure_2() { return &___exposure_2; }
	inline void set_exposure_2(float value)
	{
		___exposure_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(WaveformMonitor_t2029591948, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_m_Data_4() { return static_cast<int32_t>(offsetof(WaveformMonitor_t2029591948, ___m_Data_4)); }
	inline ComputeBuffer_t1033194329 * get_m_Data_4() const { return ___m_Data_4; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_Data_4() { return &___m_Data_4; }
	inline void set_m_Data_4(ComputeBuffer_t1033194329 * value)
	{
		___m_Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEFORMMONITOR_T2029591948_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255371  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=368 <PrivateImplementationDetails>::8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41
	__StaticArrayInitTypeSizeU3D368_t2501028641  ___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0;

public:
	inline static int32_t get_offset_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0)); }
	inline __StaticArrayInitTypeSizeU3D368_t2501028641  get_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0() const { return ___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0; }
	inline __StaticArrayInitTypeSizeU3D368_t2501028641 * get_address_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0() { return &___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0; }
	inline void set_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0(__StaticArrayInitTypeSizeU3D368_t2501028641  value)
	{
		___8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DIALOGTYPE_T918222323_H
#define DIALOGTYPE_T918222323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore/DialogType
struct  DialogType_t918222323 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStore/DialogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DialogType_t918222323, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGTYPE_T918222323_H
#ifndef FAKESTOREUIMODE_T680685637_H
#define FAKESTOREUIMODE_T680685637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStoreUIMode
struct  FakeStoreUIMode_t680685637 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStoreUIMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FakeStoreUIMode_t680685637, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESTOREUIMODE_T680685637_H
#ifndef STORESPECIFICPURCHASEERRORCODE_T2338830946_H
#define STORESPECIFICPURCHASEERRORCODE_T2338830946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode
struct  StoreSpecificPurchaseErrorCode_t2338830946 
{
public:
	// System.Int32 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StoreSpecificPurchaseErrorCode_t2338830946, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORESPECIFICPURCHASEERRORCODE_T2338830946_H
#ifndef AMBIENTOCCLUSIONMODE_T1066043822_H
#define AMBIENTOCCLUSIONMODE_T1066043822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionMode
struct  AmbientOcclusionMode_t1066043822 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.AmbientOcclusionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AmbientOcclusionMode_t1066043822, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODE_T1066043822_H
#ifndef AMBIENTOCCLUSIONQUALITY_T3249644899_H
#define AMBIENTOCCLUSIONQUALITY_T3249644899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQuality
struct  AmbientOcclusionQuality_t3249644899 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.AmbientOcclusionQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AmbientOcclusionQuality_t3249644899, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONQUALITY_T3249644899_H
#ifndef AMBIENTOCCLUSIONRENDERER_T1110100638_H
#define AMBIENTOCCLUSIONRENDERER_T1110100638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionRenderer
struct  AmbientOcclusionRenderer_t1110100638  : public PostProcessEffectRenderer_1_t3739647461
{
public:
	// UnityEngine.Rendering.PostProcessing.IAmbientOcclusionMethod[] UnityEngine.Rendering.PostProcessing.AmbientOcclusionRenderer::m_Methods
	IAmbientOcclusionMethodU5BU5D_t106627570* ___m_Methods_2;

public:
	inline static int32_t get_offset_of_m_Methods_2() { return static_cast<int32_t>(offsetof(AmbientOcclusionRenderer_t1110100638, ___m_Methods_2)); }
	inline IAmbientOcclusionMethodU5BU5D_t106627570* get_m_Methods_2() const { return ___m_Methods_2; }
	inline IAmbientOcclusionMethodU5BU5D_t106627570** get_address_of_m_Methods_2() { return &___m_Methods_2; }
	inline void set_m_Methods_2(IAmbientOcclusionMethodU5BU5D_t106627570* value)
	{
		___m_Methods_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Methods_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONRENDERER_T1110100638_H
#ifndef AUTOEXPOSURERENDERER_T90372877_H
#define AUTOEXPOSURERENDERER_T90372877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AutoExposureRenderer
struct  AutoExposureRenderer_t90372877  : public PostProcessEffectRenderer_1_t775410174
{
public:
	// UnityEngine.RenderTexture[][] UnityEngine.Rendering.PostProcessing.AutoExposureRenderer::m_AutoExposurePool
	RenderTextureU5BU5DU5BU5D_t847993469* ___m_AutoExposurePool_2;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.AutoExposureRenderer::m_AutoExposurePingPong
	Int32U5BU5D_t385246372* ___m_AutoExposurePingPong_3;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.AutoExposureRenderer::m_CurrentAutoExposure
	RenderTexture_t2108887433 * ___m_CurrentAutoExposure_4;

public:
	inline static int32_t get_offset_of_m_AutoExposurePool_2() { return static_cast<int32_t>(offsetof(AutoExposureRenderer_t90372877, ___m_AutoExposurePool_2)); }
	inline RenderTextureU5BU5DU5BU5D_t847993469* get_m_AutoExposurePool_2() const { return ___m_AutoExposurePool_2; }
	inline RenderTextureU5BU5DU5BU5D_t847993469** get_address_of_m_AutoExposurePool_2() { return &___m_AutoExposurePool_2; }
	inline void set_m_AutoExposurePool_2(RenderTextureU5BU5DU5BU5D_t847993469* value)
	{
		___m_AutoExposurePool_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePool_2), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePingPong_3() { return static_cast<int32_t>(offsetof(AutoExposureRenderer_t90372877, ___m_AutoExposurePingPong_3)); }
	inline Int32U5BU5D_t385246372* get_m_AutoExposurePingPong_3() const { return ___m_AutoExposurePingPong_3; }
	inline Int32U5BU5D_t385246372** get_address_of_m_AutoExposurePingPong_3() { return &___m_AutoExposurePingPong_3; }
	inline void set_m_AutoExposurePingPong_3(Int32U5BU5D_t385246372* value)
	{
		___m_AutoExposurePingPong_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePingPong_3), value);
	}

	inline static int32_t get_offset_of_m_CurrentAutoExposure_4() { return static_cast<int32_t>(offsetof(AutoExposureRenderer_t90372877, ___m_CurrentAutoExposure_4)); }
	inline RenderTexture_t2108887433 * get_m_CurrentAutoExposure_4() const { return ___m_CurrentAutoExposure_4; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CurrentAutoExposure_4() { return &___m_CurrentAutoExposure_4; }
	inline void set_m_CurrentAutoExposure_4(RenderTexture_t2108887433 * value)
	{
		___m_CurrentAutoExposure_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAutoExposure_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOEXPOSURERENDERER_T90372877_H
#ifndef BLOOMRENDERER_T4129140504_H
#define BLOOMRENDERER_T4129140504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BloomRenderer
struct  BloomRenderer_t4129140504  : public PostProcessEffectRenderer_1_t1047298178
{
public:
	// UnityEngine.Rendering.PostProcessing.BloomRenderer/Level[] UnityEngine.Rendering.PostProcessing.BloomRenderer::m_Pyramid
	LevelU5BU5D_t1986557484* ___m_Pyramid_2;

public:
	inline static int32_t get_offset_of_m_Pyramid_2() { return static_cast<int32_t>(offsetof(BloomRenderer_t4129140504, ___m_Pyramid_2)); }
	inline LevelU5BU5D_t1986557484* get_m_Pyramid_2() const { return ___m_Pyramid_2; }
	inline LevelU5BU5D_t1986557484** get_address_of_m_Pyramid_2() { return &___m_Pyramid_2; }
	inline void set_m_Pyramid_2(LevelU5BU5D_t1986557484* value)
	{
		___m_Pyramid_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pyramid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMRENDERER_T4129140504_H
#ifndef CHROMATICABERRATIONRENDERER_T2160044044_H
#define CHROMATICABERRATIONRENDERER_T2160044044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ChromaticAberrationRenderer
struct  ChromaticAberrationRenderer_t2160044044  : public PostProcessEffectRenderer_1_t1139889269
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.ChromaticAberrationRenderer::m_InternalSpectralLut
	Texture2D_t3840446185 * ___m_InternalSpectralLut_2;

public:
	inline static int32_t get_offset_of_m_InternalSpectralLut_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationRenderer_t2160044044, ___m_InternalSpectralLut_2)); }
	inline Texture2D_t3840446185 * get_m_InternalSpectralLut_2() const { return ___m_InternalSpectralLut_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_InternalSpectralLut_2() { return &___m_InternalSpectralLut_2; }
	inline void set_m_InternalSpectralLut_2(Texture2D_t3840446185 * value)
	{
		___m_InternalSpectralLut_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalSpectralLut_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONRENDERER_T2160044044_H
#ifndef COLORGRADINGRENDERER_T2114992433_H
#define COLORGRADINGRENDERER_T2114992433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorGradingRenderer
struct  ColorGradingRenderer_t2114992433  : public PostProcessEffectRenderer_1_t261573835
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_GradingCurves
	Texture2D_t3840446185 * ___m_GradingCurves_2;
	// UnityEngine.Color[] UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_Pixels
	ColorU5BU5D_t941916413* ___m_Pixels_3;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_InternalLdrLut
	RenderTexture_t2108887433 * ___m_InternalLdrLut_4;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_InternalLogLut
	RenderTexture_t2108887433 * ___m_InternalLogLut_5;
	// UnityEngine.Rendering.PostProcessing.HableCurve UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_HableCurve
	HableCurve_t1612137718 * ___m_HableCurve_6;

public:
	inline static int32_t get_offset_of_m_GradingCurves_2() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t2114992433, ___m_GradingCurves_2)); }
	inline Texture2D_t3840446185 * get_m_GradingCurves_2() const { return ___m_GradingCurves_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_GradingCurves_2() { return &___m_GradingCurves_2; }
	inline void set_m_GradingCurves_2(Texture2D_t3840446185 * value)
	{
		___m_GradingCurves_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradingCurves_2), value);
	}

	inline static int32_t get_offset_of_m_Pixels_3() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t2114992433, ___m_Pixels_3)); }
	inline ColorU5BU5D_t941916413* get_m_Pixels_3() const { return ___m_Pixels_3; }
	inline ColorU5BU5D_t941916413** get_address_of_m_Pixels_3() { return &___m_Pixels_3; }
	inline void set_m_Pixels_3(ColorU5BU5D_t941916413* value)
	{
		___m_Pixels_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pixels_3), value);
	}

	inline static int32_t get_offset_of_m_InternalLdrLut_4() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t2114992433, ___m_InternalLdrLut_4)); }
	inline RenderTexture_t2108887433 * get_m_InternalLdrLut_4() const { return ___m_InternalLdrLut_4; }
	inline RenderTexture_t2108887433 ** get_address_of_m_InternalLdrLut_4() { return &___m_InternalLdrLut_4; }
	inline void set_m_InternalLdrLut_4(RenderTexture_t2108887433 * value)
	{
		___m_InternalLdrLut_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLdrLut_4), value);
	}

	inline static int32_t get_offset_of_m_InternalLogLut_5() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t2114992433, ___m_InternalLogLut_5)); }
	inline RenderTexture_t2108887433 * get_m_InternalLogLut_5() const { return ___m_InternalLogLut_5; }
	inline RenderTexture_t2108887433 ** get_address_of_m_InternalLogLut_5() { return &___m_InternalLogLut_5; }
	inline void set_m_InternalLogLut_5(RenderTexture_t2108887433 * value)
	{
		___m_InternalLogLut_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLogLut_5), value);
	}

	inline static int32_t get_offset_of_m_HableCurve_6() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t2114992433, ___m_HableCurve_6)); }
	inline HableCurve_t1612137718 * get_m_HableCurve_6() const { return ___m_HableCurve_6; }
	inline HableCurve_t1612137718 ** get_address_of_m_HableCurve_6() { return &___m_HableCurve_6; }
	inline void set_m_HableCurve_6(HableCurve_t1612137718 * value)
	{
		___m_HableCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_HableCurve_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGRENDERER_T2114992433_H
#ifndef DEPTHOFFIELDRENDERER_T480065934_H
#define DEPTHOFFIELDRENDERER_T480065934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer
struct  DepthOfFieldRenderer_t480065934  : public PostProcessEffectRenderer_1_t3657259404
{
public:
	// UnityEngine.RenderTexture[][] UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer::m_CoCHistoryTextures
	RenderTextureU5BU5DU5BU5D_t847993469* ___m_CoCHistoryTextures_2;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer::m_HistoryPingPong
	Int32U5BU5D_t385246372* ___m_HistoryPingPong_3;

public:
	inline static int32_t get_offset_of_m_CoCHistoryTextures_2() { return static_cast<int32_t>(offsetof(DepthOfFieldRenderer_t480065934, ___m_CoCHistoryTextures_2)); }
	inline RenderTextureU5BU5DU5BU5D_t847993469* get_m_CoCHistoryTextures_2() const { return ___m_CoCHistoryTextures_2; }
	inline RenderTextureU5BU5DU5BU5D_t847993469** get_address_of_m_CoCHistoryTextures_2() { return &___m_CoCHistoryTextures_2; }
	inline void set_m_CoCHistoryTextures_2(RenderTextureU5BU5DU5BU5D_t847993469* value)
	{
		___m_CoCHistoryTextures_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoCHistoryTextures_2), value);
	}

	inline static int32_t get_offset_of_m_HistoryPingPong_3() { return static_cast<int32_t>(offsetof(DepthOfFieldRenderer_t480065934, ___m_HistoryPingPong_3)); }
	inline Int32U5BU5D_t385246372* get_m_HistoryPingPong_3() const { return ___m_HistoryPingPong_3; }
	inline Int32U5BU5D_t385246372** get_address_of_m_HistoryPingPong_3() { return &___m_HistoryPingPong_3; }
	inline void set_m_HistoryPingPong_3(Int32U5BU5D_t385246372* value)
	{
		___m_HistoryPingPong_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryPingPong_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDRENDERER_T480065934_H
#ifndef EYEADAPTATION_T3315401890_H
#define EYEADAPTATION_T3315401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.EyeAdaptation
struct  EyeAdaptation_t3315401890 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.EyeAdaptation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EyeAdaptation_t3315401890, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATION_T3315401890_H
#ifndef FLOATPARAMETER_T1840207740_H
#define FLOATPARAMETER_T1840207740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.FloatParameter
struct  FloatParameter_t1840207740  : public ParameterOverride_1_t3672619081
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARAMETER_T1840207740_H
#ifndef GRADINGMODE_T3858801194_H
#define GRADINGMODE_T3858801194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.GradingMode
struct  GradingMode_t3858801194 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.GradingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GradingMode_t3858801194, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADINGMODE_T3858801194_H
#ifndef GRAINRENDERER_T2133570186_H
#define GRAINRENDERER_T2133570186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.GrainRenderer
struct  GrainRenderer_t2133570186  : public PostProcessEffectRenderer_1_t1486268606
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.GrainRenderer::m_GrainLookupRT
	RenderTexture_t2108887433 * ___m_GrainLookupRT_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.GrainRenderer::m_SampleIndex
	int32_t ___m_SampleIndex_3;

public:
	inline static int32_t get_offset_of_m_GrainLookupRT_2() { return static_cast<int32_t>(offsetof(GrainRenderer_t2133570186, ___m_GrainLookupRT_2)); }
	inline RenderTexture_t2108887433 * get_m_GrainLookupRT_2() const { return ___m_GrainLookupRT_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_GrainLookupRT_2() { return &___m_GrainLookupRT_2; }
	inline void set_m_GrainLookupRT_2(RenderTexture_t2108887433 * value)
	{
		___m_GrainLookupRT_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrainLookupRT_2), value);
	}

	inline static int32_t get_offset_of_m_SampleIndex_3() { return static_cast<int32_t>(offsetof(GrainRenderer_t2133570186, ___m_SampleIndex_3)); }
	inline int32_t get_m_SampleIndex_3() const { return ___m_SampleIndex_3; }
	inline int32_t* get_address_of_m_SampleIndex_3() { return &___m_SampleIndex_3; }
	inline void set_m_SampleIndex_3(int32_t value)
	{
		___m_SampleIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINRENDERER_T2133570186_H
#ifndef CHANNEL_T1999646469_H
#define CHANNEL_T1999646469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HistogramMonitor/Channel
struct  Channel_t1999646469 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.HistogramMonitor/Channel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Channel_t1999646469, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNEL_T1999646469_H
#ifndef KERNELSIZE_T1641471414_H
#define KERNELSIZE_T1641471414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.KernelSize
struct  KernelSize_t1641471414 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.KernelSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KernelSize_t1641471414, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZE_T1641471414_H
#ifndef LENSDISTORTIONRENDERER_T259903296_H
#define LENSDISTORTIONRENDERER_T259903296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LensDistortionRenderer
struct  LensDistortionRenderer_t259903296  : public PostProcessEffectRenderer_1_t3308362066
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSDISTORTIONRENDERER_T259903296_H
#ifndef MONITORTYPE_T3583017366_H
#define MONITORTYPE_T3583017366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MonitorType
struct  MonitorType_t3583017366 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.MonitorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonitorType_t3583017366, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONITORTYPE_T3583017366_H
#ifndef MOTIONBLURRENDERER_T1665358776_H
#define MOTIONBLURRENDERER_T1665358776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MotionBlurRenderer
struct  MotionBlurRenderer_t1665358776  : public PostProcessEffectRenderer_1_t1113746280
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURRENDERER_T1665358776_H
#ifndef MIPLEVEL_T1715308661_H
#define MIPLEVEL_T1715308661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MultiScaleVO/MipLevel
struct  MipLevel_t1715308661 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.MultiScaleVO/MipLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MipLevel_t1715308661, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIPLEVEL_T1715308661_H
#ifndef POSTPROCESSEVENT_T3532433552_H
#define POSTPROCESSEVENT_T3532433552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEvent
struct  PostProcessEvent_t3532433552 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostProcessEvent_t3532433552, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEVENT_T3532433552_H
#ifndef SCREENSPACEREFLECTIONPRESET_T2401151656_H
#define SCREENSPACEREFLECTIONPRESET_T2401151656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPreset
struct  ScreenSpaceReflectionPreset_t2401151656 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionPreset_t2401151656, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONPRESET_T2401151656_H
#ifndef SCREENSPACEREFLECTIONRESOLUTION_T3090202209_H
#define SCREENSPACEREFLECTIONRESOLUTION_T3090202209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution
struct  ScreenSpaceReflectionResolution_t3090202209 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionResolution_t3090202209, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONRESOLUTION_T3090202209_H
#ifndef SCREENSPACEREFLECTIONSRENDERER_T661283308_H
#define SCREENSPACEREFLECTIONSRENDERER_T661283308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer
struct  ScreenSpaceReflectionsRenderer_t661283308  : public PostProcessEffectRenderer_1_t1421876342
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_Resolve
	RenderTexture_t2108887433 * ___m_Resolve_2;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_History
	RenderTexture_t2108887433 * ___m_History_3;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_MipIDs
	Int32U5BU5D_t385246372* ___m_MipIDs_4;
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset[] UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_Presets
	QualityPresetU5BU5D_t2343555814* ___m_Presets_5;

public:
	inline static int32_t get_offset_of_m_Resolve_2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_t661283308, ___m_Resolve_2)); }
	inline RenderTexture_t2108887433 * get_m_Resolve_2() const { return ___m_Resolve_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_Resolve_2() { return &___m_Resolve_2; }
	inline void set_m_Resolve_2(RenderTexture_t2108887433 * value)
	{
		___m_Resolve_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resolve_2), value);
	}

	inline static int32_t get_offset_of_m_History_3() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_t661283308, ___m_History_3)); }
	inline RenderTexture_t2108887433 * get_m_History_3() const { return ___m_History_3; }
	inline RenderTexture_t2108887433 ** get_address_of_m_History_3() { return &___m_History_3; }
	inline void set_m_History_3(RenderTexture_t2108887433 * value)
	{
		___m_History_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_History_3), value);
	}

	inline static int32_t get_offset_of_m_MipIDs_4() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_t661283308, ___m_MipIDs_4)); }
	inline Int32U5BU5D_t385246372* get_m_MipIDs_4() const { return ___m_MipIDs_4; }
	inline Int32U5BU5D_t385246372** get_address_of_m_MipIDs_4() { return &___m_MipIDs_4; }
	inline void set_m_MipIDs_4(Int32U5BU5D_t385246372* value)
	{
		___m_MipIDs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MipIDs_4), value);
	}

	inline static int32_t get_offset_of_m_Presets_5() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_t661283308, ___m_Presets_5)); }
	inline QualityPresetU5BU5D_t2343555814* get_m_Presets_5() const { return ___m_Presets_5; }
	inline QualityPresetU5BU5D_t2343555814** get_address_of_m_Presets_5() { return &___m_Presets_5; }
	inline void set_m_Presets_5(QualityPresetU5BU5D_t2343555814* value)
	{
		___m_Presets_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Presets_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONSRENDERER_T661283308_H
#ifndef QUALITY_T1883249404_H
#define QUALITY_T1883249404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing/Quality
struct  Quality_t1883249404 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing/Quality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Quality_t1883249404, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITY_T1883249404_H
#ifndef TEMPORALANTIALIASING_T1482226156_H
#define TEMPORALANTIALIASING_T1482226156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing
struct  TemporalAntialiasing_t1482226156  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::jitterSpread
	float ___jitterSpread_0;
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::sharpness
	float ___sharpness_1;
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::stationaryBlending
	float ___stationaryBlending_2;
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::motionBlending
	float ___motionBlending_3;
	// System.Func`3<UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Matrix4x4> UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::jitteredMatrixFunc
	Func_3_t2888966892 * ___jitteredMatrixFunc_4;
	// UnityEngine.Vector2 UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::<jitter>k__BackingField
	Vector2_t2156229523  ___U3CjitterU3Ek__BackingField_5;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_Mrt
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_Mrt_6;
	// System.Boolean UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_ResetHistory
	bool ___m_ResetHistory_7;
	// System.Int32 UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::<sampleIndex>k__BackingField
	int32_t ___U3CsampleIndexU3Ek__BackingField_9;
	// UnityEngine.RenderTexture[][] UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_HistoryTextures
	RenderTextureU5BU5DU5BU5D_t847993469* ___m_HistoryTextures_12;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_HistoryPingPong
	Int32U5BU5D_t385246372* ___m_HistoryPingPong_13;

public:
	inline static int32_t get_offset_of_jitterSpread_0() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___jitterSpread_0)); }
	inline float get_jitterSpread_0() const { return ___jitterSpread_0; }
	inline float* get_address_of_jitterSpread_0() { return &___jitterSpread_0; }
	inline void set_jitterSpread_0(float value)
	{
		___jitterSpread_0 = value;
	}

	inline static int32_t get_offset_of_sharpness_1() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___sharpness_1)); }
	inline float get_sharpness_1() const { return ___sharpness_1; }
	inline float* get_address_of_sharpness_1() { return &___sharpness_1; }
	inline void set_sharpness_1(float value)
	{
		___sharpness_1 = value;
	}

	inline static int32_t get_offset_of_stationaryBlending_2() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___stationaryBlending_2)); }
	inline float get_stationaryBlending_2() const { return ___stationaryBlending_2; }
	inline float* get_address_of_stationaryBlending_2() { return &___stationaryBlending_2; }
	inline void set_stationaryBlending_2(float value)
	{
		___stationaryBlending_2 = value;
	}

	inline static int32_t get_offset_of_motionBlending_3() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___motionBlending_3)); }
	inline float get_motionBlending_3() const { return ___motionBlending_3; }
	inline float* get_address_of_motionBlending_3() { return &___motionBlending_3; }
	inline void set_motionBlending_3(float value)
	{
		___motionBlending_3 = value;
	}

	inline static int32_t get_offset_of_jitteredMatrixFunc_4() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___jitteredMatrixFunc_4)); }
	inline Func_3_t2888966892 * get_jitteredMatrixFunc_4() const { return ___jitteredMatrixFunc_4; }
	inline Func_3_t2888966892 ** get_address_of_jitteredMatrixFunc_4() { return &___jitteredMatrixFunc_4; }
	inline void set_jitteredMatrixFunc_4(Func_3_t2888966892 * value)
	{
		___jitteredMatrixFunc_4 = value;
		Il2CppCodeGenWriteBarrier((&___jitteredMatrixFunc_4), value);
	}

	inline static int32_t get_offset_of_U3CjitterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___U3CjitterU3Ek__BackingField_5)); }
	inline Vector2_t2156229523  get_U3CjitterU3Ek__BackingField_5() const { return ___U3CjitterU3Ek__BackingField_5; }
	inline Vector2_t2156229523 * get_address_of_U3CjitterU3Ek__BackingField_5() { return &___U3CjitterU3Ek__BackingField_5; }
	inline void set_U3CjitterU3Ek__BackingField_5(Vector2_t2156229523  value)
	{
		___U3CjitterU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_m_Mrt_6() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___m_Mrt_6)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_Mrt_6() const { return ___m_Mrt_6; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_Mrt_6() { return &___m_Mrt_6; }
	inline void set_m_Mrt_6(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_Mrt_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mrt_6), value);
	}

	inline static int32_t get_offset_of_m_ResetHistory_7() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___m_ResetHistory_7)); }
	inline bool get_m_ResetHistory_7() const { return ___m_ResetHistory_7; }
	inline bool* get_address_of_m_ResetHistory_7() { return &___m_ResetHistory_7; }
	inline void set_m_ResetHistory_7(bool value)
	{
		___m_ResetHistory_7 = value;
	}

	inline static int32_t get_offset_of_U3CsampleIndexU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___U3CsampleIndexU3Ek__BackingField_9)); }
	inline int32_t get_U3CsampleIndexU3Ek__BackingField_9() const { return ___U3CsampleIndexU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CsampleIndexU3Ek__BackingField_9() { return &___U3CsampleIndexU3Ek__BackingField_9; }
	inline void set_U3CsampleIndexU3Ek__BackingField_9(int32_t value)
	{
		___U3CsampleIndexU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_m_HistoryTextures_12() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___m_HistoryTextures_12)); }
	inline RenderTextureU5BU5DU5BU5D_t847993469* get_m_HistoryTextures_12() const { return ___m_HistoryTextures_12; }
	inline RenderTextureU5BU5DU5BU5D_t847993469** get_address_of_m_HistoryTextures_12() { return &___m_HistoryTextures_12; }
	inline void set_m_HistoryTextures_12(RenderTextureU5BU5DU5BU5D_t847993469* value)
	{
		___m_HistoryTextures_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryTextures_12), value);
	}

	inline static int32_t get_offset_of_m_HistoryPingPong_13() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t1482226156, ___m_HistoryPingPong_13)); }
	inline Int32U5BU5D_t385246372* get_m_HistoryPingPong_13() const { return ___m_HistoryPingPong_13; }
	inline Int32U5BU5D_t385246372** get_address_of_m_HistoryPingPong_13() { return &___m_HistoryPingPong_13; }
	inline void set_m_HistoryPingPong_13(Int32U5BU5D_t385246372* value)
	{
		___m_HistoryPingPong_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryPingPong_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPORALANTIALIASING_T1482226156_H
#ifndef TONEMAPPER_T3044700181_H
#define TONEMAPPER_T3044700181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Tonemapper
struct  Tonemapper_t3044700181 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.Tonemapper::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tonemapper_t3044700181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_T3044700181_H
#ifndef MODE_T2795405020_H
#define MODE_T2795405020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TrackballAttribute/Mode
struct  Mode_t2795405020 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.TrackballAttribute/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t2795405020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2795405020_H
#ifndef VIGNETTEMODE_T1093529744_H
#define VIGNETTEMODE_T1093529744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VignetteMode
struct  VignetteMode_t1093529744 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.VignetteMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VignetteMode_t1093529744, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODE_T1093529744_H
#ifndef VIGNETTERENDERER_T4277974699_H
#define VIGNETTERENDERER_T4277974699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VignetteRenderer
struct  VignetteRenderer_t4277974699  : public PostProcessEffectRenderer_1_t388638640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTERENDERER_T4277974699_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef JSONSTORE_T1587640366_H
#define JSONSTORE_T1587640366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JSONStore
struct  JSONStore_t1587640366  : public AbstractStore_t285429589
{
public:
	// UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.JSONStore::m_managedStore
	StoreCatalogImpl_t2383312173 * ___m_managedStore_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.JSONStore::unity
	RuntimeObject* ___unity_1;
	// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.JSONStore::store
	RuntimeObject* ___store_2;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.JSONStore::m_storeCatalog
	List_1_t1811801880 * ___m_storeCatalog_3;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isManagedStoreEnabled
	bool ___isManagedStoreEnabled_4;
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.JSONStore::m_profileData
	ProfileData_t3386201266 * ___m_profileData_5;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isRefreshing
	bool ___isRefreshing_6;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isFirstTimeRetrievingProducts
	bool ___isFirstTimeRetrievingProducts_7;
	// System.Action UnityEngine.Purchasing.JSONStore::refreshCallback
	Action_t1264377477 * ___refreshCallback_8;
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.JSONStore::m_Module
	StandardPurchasingModule_t2580735509 * ___m_Module_9;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.JSONStore::m_BuilderProducts
	HashSet_1_t3199643908 * ___m_BuilderProducts_10;
	// UnityEngine.ILogger UnityEngine.Purchasing.JSONStore::m_Logger
	RuntimeObject* ___m_Logger_11;
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.JSONStore::m_EventQueue
	EventQueue_t2155959417 * ___m_EventQueue_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONStore::promoPayload
	Dictionary_2_t2865362463 * ___promoPayload_13;
	// System.Boolean UnityEngine.Purchasing.JSONStore::catalogDisabled
	bool ___catalogDisabled_14;
	// System.Boolean UnityEngine.Purchasing.JSONStore::testStore
	bool ___testStore_15;
	// System.String UnityEngine.Purchasing.JSONStore::iapBaseUrl
	String_t* ___iapBaseUrl_16;
	// System.String UnityEngine.Purchasing.JSONStore::eventBaseUrl
	String_t* ___eventBaseUrl_17;
	// UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONStore::lastPurchaseFailureDescription
	PurchaseFailureDescription_t437632294 * ___lastPurchaseFailureDescription_18;
	// UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::_lastPurchaseErrorCode
	int32_t ____lastPurchaseErrorCode_19;
	// System.String UnityEngine.Purchasing.JSONStore::kStoreSpecificErrorCodeKey
	String_t* ___kStoreSpecificErrorCodeKey_20;

public:
	inline static int32_t get_offset_of_m_managedStore_0() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_managedStore_0)); }
	inline StoreCatalogImpl_t2383312173 * get_m_managedStore_0() const { return ___m_managedStore_0; }
	inline StoreCatalogImpl_t2383312173 ** get_address_of_m_managedStore_0() { return &___m_managedStore_0; }
	inline void set_m_managedStore_0(StoreCatalogImpl_t2383312173 * value)
	{
		___m_managedStore_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_managedStore_0), value);
	}

	inline static int32_t get_offset_of_unity_1() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___unity_1)); }
	inline RuntimeObject* get_unity_1() const { return ___unity_1; }
	inline RuntimeObject** get_address_of_unity_1() { return &___unity_1; }
	inline void set_unity_1(RuntimeObject* value)
	{
		___unity_1 = value;
		Il2CppCodeGenWriteBarrier((&___unity_1), value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___store_2)); }
	inline RuntimeObject* get_store_2() const { return ___store_2; }
	inline RuntimeObject** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(RuntimeObject* value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier((&___store_2), value);
	}

	inline static int32_t get_offset_of_m_storeCatalog_3() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_storeCatalog_3)); }
	inline List_1_t1811801880 * get_m_storeCatalog_3() const { return ___m_storeCatalog_3; }
	inline List_1_t1811801880 ** get_address_of_m_storeCatalog_3() { return &___m_storeCatalog_3; }
	inline void set_m_storeCatalog_3(List_1_t1811801880 * value)
	{
		___m_storeCatalog_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_storeCatalog_3), value);
	}

	inline static int32_t get_offset_of_isManagedStoreEnabled_4() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___isManagedStoreEnabled_4)); }
	inline bool get_isManagedStoreEnabled_4() const { return ___isManagedStoreEnabled_4; }
	inline bool* get_address_of_isManagedStoreEnabled_4() { return &___isManagedStoreEnabled_4; }
	inline void set_isManagedStoreEnabled_4(bool value)
	{
		___isManagedStoreEnabled_4 = value;
	}

	inline static int32_t get_offset_of_m_profileData_5() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_profileData_5)); }
	inline ProfileData_t3386201266 * get_m_profileData_5() const { return ___m_profileData_5; }
	inline ProfileData_t3386201266 ** get_address_of_m_profileData_5() { return &___m_profileData_5; }
	inline void set_m_profileData_5(ProfileData_t3386201266 * value)
	{
		___m_profileData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_profileData_5), value);
	}

	inline static int32_t get_offset_of_isRefreshing_6() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___isRefreshing_6)); }
	inline bool get_isRefreshing_6() const { return ___isRefreshing_6; }
	inline bool* get_address_of_isRefreshing_6() { return &___isRefreshing_6; }
	inline void set_isRefreshing_6(bool value)
	{
		___isRefreshing_6 = value;
	}

	inline static int32_t get_offset_of_isFirstTimeRetrievingProducts_7() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___isFirstTimeRetrievingProducts_7)); }
	inline bool get_isFirstTimeRetrievingProducts_7() const { return ___isFirstTimeRetrievingProducts_7; }
	inline bool* get_address_of_isFirstTimeRetrievingProducts_7() { return &___isFirstTimeRetrievingProducts_7; }
	inline void set_isFirstTimeRetrievingProducts_7(bool value)
	{
		___isFirstTimeRetrievingProducts_7 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_8() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___refreshCallback_8)); }
	inline Action_t1264377477 * get_refreshCallback_8() const { return ___refreshCallback_8; }
	inline Action_t1264377477 ** get_address_of_refreshCallback_8() { return &___refreshCallback_8; }
	inline void set_refreshCallback_8(Action_t1264377477 * value)
	{
		___refreshCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___refreshCallback_8), value);
	}

	inline static int32_t get_offset_of_m_Module_9() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_Module_9)); }
	inline StandardPurchasingModule_t2580735509 * get_m_Module_9() const { return ___m_Module_9; }
	inline StandardPurchasingModule_t2580735509 ** get_address_of_m_Module_9() { return &___m_Module_9; }
	inline void set_m_Module_9(StandardPurchasingModule_t2580735509 * value)
	{
		___m_Module_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Module_9), value);
	}

	inline static int32_t get_offset_of_m_BuilderProducts_10() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_BuilderProducts_10)); }
	inline HashSet_1_t3199643908 * get_m_BuilderProducts_10() const { return ___m_BuilderProducts_10; }
	inline HashSet_1_t3199643908 ** get_address_of_m_BuilderProducts_10() { return &___m_BuilderProducts_10; }
	inline void set_m_BuilderProducts_10(HashSet_1_t3199643908 * value)
	{
		___m_BuilderProducts_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_BuilderProducts_10), value);
	}

	inline static int32_t get_offset_of_m_Logger_11() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_Logger_11)); }
	inline RuntimeObject* get_m_Logger_11() const { return ___m_Logger_11; }
	inline RuntimeObject** get_address_of_m_Logger_11() { return &___m_Logger_11; }
	inline void set_m_Logger_11(RuntimeObject* value)
	{
		___m_Logger_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_11), value);
	}

	inline static int32_t get_offset_of_m_EventQueue_12() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___m_EventQueue_12)); }
	inline EventQueue_t2155959417 * get_m_EventQueue_12() const { return ___m_EventQueue_12; }
	inline EventQueue_t2155959417 ** get_address_of_m_EventQueue_12() { return &___m_EventQueue_12; }
	inline void set_m_EventQueue_12(EventQueue_t2155959417 * value)
	{
		___m_EventQueue_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventQueue_12), value);
	}

	inline static int32_t get_offset_of_promoPayload_13() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___promoPayload_13)); }
	inline Dictionary_2_t2865362463 * get_promoPayload_13() const { return ___promoPayload_13; }
	inline Dictionary_2_t2865362463 ** get_address_of_promoPayload_13() { return &___promoPayload_13; }
	inline void set_promoPayload_13(Dictionary_2_t2865362463 * value)
	{
		___promoPayload_13 = value;
		Il2CppCodeGenWriteBarrier((&___promoPayload_13), value);
	}

	inline static int32_t get_offset_of_catalogDisabled_14() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___catalogDisabled_14)); }
	inline bool get_catalogDisabled_14() const { return ___catalogDisabled_14; }
	inline bool* get_address_of_catalogDisabled_14() { return &___catalogDisabled_14; }
	inline void set_catalogDisabled_14(bool value)
	{
		___catalogDisabled_14 = value;
	}

	inline static int32_t get_offset_of_testStore_15() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___testStore_15)); }
	inline bool get_testStore_15() const { return ___testStore_15; }
	inline bool* get_address_of_testStore_15() { return &___testStore_15; }
	inline void set_testStore_15(bool value)
	{
		___testStore_15 = value;
	}

	inline static int32_t get_offset_of_iapBaseUrl_16() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___iapBaseUrl_16)); }
	inline String_t* get_iapBaseUrl_16() const { return ___iapBaseUrl_16; }
	inline String_t** get_address_of_iapBaseUrl_16() { return &___iapBaseUrl_16; }
	inline void set_iapBaseUrl_16(String_t* value)
	{
		___iapBaseUrl_16 = value;
		Il2CppCodeGenWriteBarrier((&___iapBaseUrl_16), value);
	}

	inline static int32_t get_offset_of_eventBaseUrl_17() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___eventBaseUrl_17)); }
	inline String_t* get_eventBaseUrl_17() const { return ___eventBaseUrl_17; }
	inline String_t** get_address_of_eventBaseUrl_17() { return &___eventBaseUrl_17; }
	inline void set_eventBaseUrl_17(String_t* value)
	{
		___eventBaseUrl_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventBaseUrl_17), value);
	}

	inline static int32_t get_offset_of_lastPurchaseFailureDescription_18() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___lastPurchaseFailureDescription_18)); }
	inline PurchaseFailureDescription_t437632294 * get_lastPurchaseFailureDescription_18() const { return ___lastPurchaseFailureDescription_18; }
	inline PurchaseFailureDescription_t437632294 ** get_address_of_lastPurchaseFailureDescription_18() { return &___lastPurchaseFailureDescription_18; }
	inline void set_lastPurchaseFailureDescription_18(PurchaseFailureDescription_t437632294 * value)
	{
		___lastPurchaseFailureDescription_18 = value;
		Il2CppCodeGenWriteBarrier((&___lastPurchaseFailureDescription_18), value);
	}

	inline static int32_t get_offset_of__lastPurchaseErrorCode_19() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ____lastPurchaseErrorCode_19)); }
	inline int32_t get__lastPurchaseErrorCode_19() const { return ____lastPurchaseErrorCode_19; }
	inline int32_t* get_address_of__lastPurchaseErrorCode_19() { return &____lastPurchaseErrorCode_19; }
	inline void set__lastPurchaseErrorCode_19(int32_t value)
	{
		____lastPurchaseErrorCode_19 = value;
	}

	inline static int32_t get_offset_of_kStoreSpecificErrorCodeKey_20() { return static_cast<int32_t>(offsetof(JSONStore_t1587640366, ___kStoreSpecificErrorCodeKey_20)); }
	inline String_t* get_kStoreSpecificErrorCodeKey_20() const { return ___kStoreSpecificErrorCodeKey_20; }
	inline String_t** get_address_of_kStoreSpecificErrorCodeKey_20() { return &___kStoreSpecificErrorCodeKey_20; }
	inline void set_kStoreSpecificErrorCodeKey_20(String_t* value)
	{
		___kStoreSpecificErrorCodeKey_20 = value;
		Il2CppCodeGenWriteBarrier((&___kStoreSpecificErrorCodeKey_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTORE_T1587640366_H
#ifndef HISTOGRAMMONITOR_T3488597019_H
#define HISTOGRAMMONITOR_T3488597019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HistogramMonitor
struct  HistogramMonitor_t3488597019  : public Monitor_t1754509597
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.HistogramMonitor::width
	int32_t ___width_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.HistogramMonitor::height
	int32_t ___height_3;
	// UnityEngine.Rendering.PostProcessing.HistogramMonitor/Channel UnityEngine.Rendering.PostProcessing.HistogramMonitor::channel
	int32_t ___channel_4;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.HistogramMonitor::m_Data
	ComputeBuffer_t1033194329 * ___m_Data_5;

public:
	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(HistogramMonitor_t3488597019, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(HistogramMonitor_t3488597019, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_channel_4() { return static_cast<int32_t>(offsetof(HistogramMonitor_t3488597019, ___channel_4)); }
	inline int32_t get_channel_4() const { return ___channel_4; }
	inline int32_t* get_address_of_channel_4() { return &___channel_4; }
	inline void set_channel_4(int32_t value)
	{
		___channel_4 = value;
	}

	inline static int32_t get_offset_of_m_Data_5() { return static_cast<int32_t>(offsetof(HistogramMonitor_t3488597019, ___m_Data_5)); }
	inline ComputeBuffer_t1033194329 * get_m_Data_5() const { return ___m_Data_5; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_Data_5() { return &___m_Data_5; }
	inline void set_m_Data_5(ComputeBuffer_t1033194329 * value)
	{
		___m_Data_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HISTOGRAMMONITOR_T3488597019_H
#ifndef PARAMETEROVERRIDE_1_T3341396129_H
#define PARAMETEROVERRIDE_1_T3341396129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.AmbientOcclusionMode>
struct  ParameterOverride_1_t3341396129  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t3341396129, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T3341396129_H
#ifndef PARAMETEROVERRIDE_1_T1230029910_H
#define PARAMETEROVERRIDE_1_T1230029910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.AmbientOcclusionQuality>
struct  ParameterOverride_1_t1230029910  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1230029910, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1230029910_H
#ifndef PARAMETEROVERRIDE_1_T1295786901_H
#define PARAMETEROVERRIDE_1_T1295786901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.EyeAdaptation>
struct  ParameterOverride_1_t1295786901  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1295786901, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1295786901_H
#ifndef PARAMETEROVERRIDE_1_T1839186205_H
#define PARAMETEROVERRIDE_1_T1839186205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.GradingMode>
struct  ParameterOverride_1_t1839186205  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1839186205, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1839186205_H
#ifndef PARAMETEROVERRIDE_1_T3916823721_H
#define PARAMETEROVERRIDE_1_T3916823721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.KernelSize>
struct  ParameterOverride_1_t3916823721  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t3916823721, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T3916823721_H
#ifndef PARAMETEROVERRIDE_1_T381536667_H
#define PARAMETEROVERRIDE_1_T381536667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPreset>
struct  ParameterOverride_1_t381536667  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t381536667, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T381536667_H
#ifndef PARAMETEROVERRIDE_1_T1070587220_H
#define PARAMETEROVERRIDE_1_T1070587220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution>
struct  ParameterOverride_1_t1070587220  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1070587220, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1070587220_H
#ifndef PARAMETEROVERRIDE_1_T1025085192_H
#define PARAMETEROVERRIDE_1_T1025085192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.Tonemapper>
struct  ParameterOverride_1_t1025085192  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1025085192, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1025085192_H
#ifndef PARAMETEROVERRIDE_1_T3368882051_H
#define PARAMETEROVERRIDE_1_T3368882051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.VignetteMode>
struct  ParameterOverride_1_t3368882051  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t3368882051, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T3368882051_H
#ifndef POSTPROCESSATTRIBUTE_T2074534414_H
#define POSTPROCESSATTRIBUTE_T2074534414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessAttribute
struct  PostProcessAttribute_t2074534414  : public Attribute_t861562559
{
public:
	// System.Type UnityEngine.Rendering.PostProcessing.PostProcessAttribute::renderer
	Type_t * ___renderer_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessEvent UnityEngine.Rendering.PostProcessing.PostProcessAttribute::eventType
	int32_t ___eventType_1;
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessAttribute::menuItem
	String_t* ___menuItem_2;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessAttribute::allowInSceneView
	bool ___allowInSceneView_3;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessAttribute::builtinEffect
	bool ___builtinEffect_4;

public:
	inline static int32_t get_offset_of_renderer_0() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t2074534414, ___renderer_0)); }
	inline Type_t * get_renderer_0() const { return ___renderer_0; }
	inline Type_t ** get_address_of_renderer_0() { return &___renderer_0; }
	inline void set_renderer_0(Type_t * value)
	{
		___renderer_0 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_0), value);
	}

	inline static int32_t get_offset_of_eventType_1() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t2074534414, ___eventType_1)); }
	inline int32_t get_eventType_1() const { return ___eventType_1; }
	inline int32_t* get_address_of_eventType_1() { return &___eventType_1; }
	inline void set_eventType_1(int32_t value)
	{
		___eventType_1 = value;
	}

	inline static int32_t get_offset_of_menuItem_2() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t2074534414, ___menuItem_2)); }
	inline String_t* get_menuItem_2() const { return ___menuItem_2; }
	inline String_t** get_address_of_menuItem_2() { return &___menuItem_2; }
	inline void set_menuItem_2(String_t* value)
	{
		___menuItem_2 = value;
		Il2CppCodeGenWriteBarrier((&___menuItem_2), value);
	}

	inline static int32_t get_offset_of_allowInSceneView_3() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t2074534414, ___allowInSceneView_3)); }
	inline bool get_allowInSceneView_3() const { return ___allowInSceneView_3; }
	inline bool* get_address_of_allowInSceneView_3() { return &___allowInSceneView_3; }
	inline void set_allowInSceneView_3(bool value)
	{
		___allowInSceneView_3 = value;
	}

	inline static int32_t get_offset_of_builtinEffect_4() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t2074534414, ___builtinEffect_4)); }
	inline bool get_builtinEffect_4() const { return ___builtinEffect_4; }
	inline bool* get_address_of_builtinEffect_4() { return &___builtinEffect_4; }
	inline void set_builtinEffect_4(bool value)
	{
		___builtinEffect_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSATTRIBUTE_T2074534414_H
#ifndef QUALITYPRESET_T734522687_H
#define QUALITYPRESET_T734522687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset
struct  QualityPreset_t734522687  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset::maximumIterationCount
	int32_t ___maximumIterationCount_0;
	// System.Single UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset::thickness
	float ___thickness_1;
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset::downsampling
	int32_t ___downsampling_2;

public:
	inline static int32_t get_offset_of_maximumIterationCount_0() { return static_cast<int32_t>(offsetof(QualityPreset_t734522687, ___maximumIterationCount_0)); }
	inline int32_t get_maximumIterationCount_0() const { return ___maximumIterationCount_0; }
	inline int32_t* get_address_of_maximumIterationCount_0() { return &___maximumIterationCount_0; }
	inline void set_maximumIterationCount_0(int32_t value)
	{
		___maximumIterationCount_0 = value;
	}

	inline static int32_t get_offset_of_thickness_1() { return static_cast<int32_t>(offsetof(QualityPreset_t734522687, ___thickness_1)); }
	inline float get_thickness_1() const { return ___thickness_1; }
	inline float* get_address_of_thickness_1() { return &___thickness_1; }
	inline void set_thickness_1(float value)
	{
		___thickness_1 = value;
	}

	inline static int32_t get_offset_of_downsampling_2() { return static_cast<int32_t>(offsetof(QualityPreset_t734522687, ___downsampling_2)); }
	inline int32_t get_downsampling_2() const { return ___downsampling_2; }
	inline int32_t* get_address_of_downsampling_2() { return &___downsampling_2; }
	inline void set_downsampling_2(int32_t value)
	{
		___downsampling_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T734522687_H
#ifndef SUBPIXELMORPHOLOGICALANTIALIASING_T3102233738_H
#define SUBPIXELMORPHOLOGICALANTIALIASING_T3102233738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing
struct  SubpixelMorphologicalAntialiasing_t3102233738  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing/Quality UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing::quality
	int32_t ___quality_0;

public:
	inline static int32_t get_offset_of_quality_0() { return static_cast<int32_t>(offsetof(SubpixelMorphologicalAntialiasing_t3102233738, ___quality_0)); }
	inline int32_t get_quality_0() const { return ___quality_0; }
	inline int32_t* get_address_of_quality_0() { return &___quality_0; }
	inline void set_quality_0(int32_t value)
	{
		___quality_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBPIXELMORPHOLOGICALANTIALIASING_T3102233738_H
#ifndef TRACKBALLATTRIBUTE_T1878300430_H
#define TRACKBALLATTRIBUTE_T1878300430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TrackballAttribute
struct  TrackballAttribute_t1878300430  : public Attribute_t861562559
{
public:
	// UnityEngine.Rendering.PostProcessing.TrackballAttribute/Mode UnityEngine.Rendering.PostProcessing.TrackballAttribute::mode
	int32_t ___mode_0;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(TrackballAttribute_t1878300430, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLATTRIBUTE_T1878300430_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef FACEBOOKSTOREIMPL_T2480281949_H
#define FACEBOOKSTOREIMPL_T2480281949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FacebookStoreImpl
struct  FacebookStoreImpl_t2480281949  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.FacebookStoreImpl::m_Native
	RuntimeObject* ___m_Native_21;

public:
	inline static int32_t get_offset_of_m_Native_21() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t2480281949, ___m_Native_21)); }
	inline RuntimeObject* get_m_Native_21() const { return ___m_Native_21; }
	inline RuntimeObject** get_address_of_m_Native_21() { return &___m_Native_21; }
	inline void set_m_Native_21(RuntimeObject* value)
	{
		___m_Native_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Native_21), value);
	}
};

struct FacebookStoreImpl_t2480281949_StaticFields
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.FacebookStoreImpl::util
	RuntimeObject* ___util_22;
	// UnityEngine.Purchasing.FacebookStoreImpl UnityEngine.Purchasing.FacebookStoreImpl::instance
	FacebookStoreImpl_t2480281949 * ___instance_23;

public:
	inline static int32_t get_offset_of_util_22() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t2480281949_StaticFields, ___util_22)); }
	inline RuntimeObject* get_util_22() const { return ___util_22; }
	inline RuntimeObject** get_address_of_util_22() { return &___util_22; }
	inline void set_util_22(RuntimeObject* value)
	{
		___util_22 = value;
		Il2CppCodeGenWriteBarrier((&___util_22), value);
	}

	inline static int32_t get_offset_of_instance_23() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t2480281949_StaticFields, ___instance_23)); }
	inline FacebookStoreImpl_t2480281949 * get_instance_23() const { return ___instance_23; }
	inline FacebookStoreImpl_t2480281949 ** get_address_of_instance_23() { return &___instance_23; }
	inline void set_instance_23(FacebookStoreImpl_t2480281949 * value)
	{
		___instance_23 = value;
		Il2CppCodeGenWriteBarrier((&___instance_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSTOREIMPL_T2480281949_H
#ifndef FAKESTORE_T3710170489_H
#define FAKESTORE_T3710170489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore
struct  FakeStore_t3710170489  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.FakeStore::m_Biller
	RuntimeObject* ___m_Biller_21;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Purchasing.FakeStore::m_PurchasedProducts
	List_1_t3319525431 * ___m_PurchasedProducts_22;
	// System.Boolean UnityEngine.Purchasing.FakeStore::purchaseCalled
	bool ___purchaseCalled_23;
	// System.String UnityEngine.Purchasing.FakeStore::<unavailableProductId>k__BackingField
	String_t* ___U3CunavailableProductIdU3Ek__BackingField_24;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.FakeStore::UIMode
	int32_t ___UIMode_25;

public:
	inline static int32_t get_offset_of_m_Biller_21() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___m_Biller_21)); }
	inline RuntimeObject* get_m_Biller_21() const { return ___m_Biller_21; }
	inline RuntimeObject** get_address_of_m_Biller_21() { return &___m_Biller_21; }
	inline void set_m_Biller_21(RuntimeObject* value)
	{
		___m_Biller_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Biller_21), value);
	}

	inline static int32_t get_offset_of_m_PurchasedProducts_22() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___m_PurchasedProducts_22)); }
	inline List_1_t3319525431 * get_m_PurchasedProducts_22() const { return ___m_PurchasedProducts_22; }
	inline List_1_t3319525431 ** get_address_of_m_PurchasedProducts_22() { return &___m_PurchasedProducts_22; }
	inline void set_m_PurchasedProducts_22(List_1_t3319525431 * value)
	{
		___m_PurchasedProducts_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchasedProducts_22), value);
	}

	inline static int32_t get_offset_of_purchaseCalled_23() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___purchaseCalled_23)); }
	inline bool get_purchaseCalled_23() const { return ___purchaseCalled_23; }
	inline bool* get_address_of_purchaseCalled_23() { return &___purchaseCalled_23; }
	inline void set_purchaseCalled_23(bool value)
	{
		___purchaseCalled_23 = value;
	}

	inline static int32_t get_offset_of_U3CunavailableProductIdU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___U3CunavailableProductIdU3Ek__BackingField_24)); }
	inline String_t* get_U3CunavailableProductIdU3Ek__BackingField_24() const { return ___U3CunavailableProductIdU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CunavailableProductIdU3Ek__BackingField_24() { return &___U3CunavailableProductIdU3Ek__BackingField_24; }
	inline void set_U3CunavailableProductIdU3Ek__BackingField_24(String_t* value)
	{
		___U3CunavailableProductIdU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunavailableProductIdU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_UIMode_25() { return static_cast<int32_t>(offsetof(FakeStore_t3710170489, ___UIMode_25)); }
	inline int32_t get_UIMode_25() const { return ___UIMode_25; }
	inline int32_t* get_address_of_UIMode_25() { return &___UIMode_25; }
	inline void set_UIMode_25(int32_t value)
	{
		___UIMode_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESTORE_T3710170489_H
#ifndef TIZENSTOREIMPL_T2691530403_H
#define TIZENSTOREIMPL_T2691530403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TizenStoreImpl
struct  TizenStoreImpl_t2691530403  : public JSONStore_t1587640366
{
public:
	// UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.TizenStoreImpl::m_Native
	RuntimeObject* ___m_Native_22;

public:
	inline static int32_t get_offset_of_m_Native_22() { return static_cast<int32_t>(offsetof(TizenStoreImpl_t2691530403, ___m_Native_22)); }
	inline RuntimeObject* get_m_Native_22() const { return ___m_Native_22; }
	inline RuntimeObject** get_address_of_m_Native_22() { return &___m_Native_22; }
	inline void set_m_Native_22(RuntimeObject* value)
	{
		___m_Native_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Native_22), value);
	}
};

struct TizenStoreImpl_t2691530403_StaticFields
{
public:
	// UnityEngine.Purchasing.TizenStoreImpl UnityEngine.Purchasing.TizenStoreImpl::instance
	TizenStoreImpl_t2691530403 * ___instance_21;

public:
	inline static int32_t get_offset_of_instance_21() { return static_cast<int32_t>(offsetof(TizenStoreImpl_t2691530403_StaticFields, ___instance_21)); }
	inline TizenStoreImpl_t2691530403 * get_instance_21() const { return ___instance_21; }
	inline TizenStoreImpl_t2691530403 ** get_address_of_instance_21() { return &___instance_21; }
	inline void set_instance_21(TizenStoreImpl_t2691530403 * value)
	{
		___instance_21 = value;
		Il2CppCodeGenWriteBarrier((&___instance_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIZENSTOREIMPL_T2691530403_H
#ifndef AMBIENTOCCLUSIONMODEPARAMETER_T3592449485_H
#define AMBIENTOCCLUSIONMODEPARAMETER_T3592449485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionModeParameter
struct  AmbientOcclusionModeParameter_t3592449485  : public ParameterOverride_1_t3341396129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODEPARAMETER_T3592449485_H
#ifndef AMBIENTOCCLUSIONQUALITYPARAMETER_T3820917191_H
#define AMBIENTOCCLUSIONQUALITYPARAMETER_T3820917191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQualityParameter
struct  AmbientOcclusionQualityParameter_t3820917191  : public ParameterOverride_1_t1230029910
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONQUALITYPARAMETER_T3820917191_H
#ifndef EYEADAPTATIONPARAMETER_T2946234496_H
#define EYEADAPTATIONPARAMETER_T2946234496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.EyeAdaptationParameter
struct  EyeAdaptationParameter_t2946234496  : public ParameterOverride_1_t1295786901
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONPARAMETER_T2946234496_H
#ifndef GRADINGMODEPARAMETER_T3659529292_H
#define GRADINGMODEPARAMETER_T3659529292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.GradingModeParameter
struct  GradingModeParameter_t3659529292  : public ParameterOverride_1_t1839186205
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADINGMODEPARAMETER_T3659529292_H
#ifndef KERNELSIZEPARAMETER_T3218545409_H
#define KERNELSIZEPARAMETER_T3218545409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.KernelSizeParameter
struct  KernelSizeParameter_t3218545409  : public ParameterOverride_1_t3916823721
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZEPARAMETER_T3218545409_H
#ifndef POSTPROCESSEFFECTSETTINGS_T1672565614_H
#define POSTPROCESSEFFECTSETTINGS_T1672565614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct  PostProcessEffectSettings_t1672565614  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::active
	bool ___active_4;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::enabled
	BoolParameter_t2299103272 * ___enabled_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::parameters
	ReadOnlyCollection_1_t4273630488 * ___parameters_6;

public:
	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_enabled_5() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614, ___enabled_5)); }
	inline BoolParameter_t2299103272 * get_enabled_5() const { return ___enabled_5; }
	inline BoolParameter_t2299103272 ** get_address_of_enabled_5() { return &___enabled_5; }
	inline void set_enabled_5(BoolParameter_t2299103272 * value)
	{
		___enabled_5 = value;
		Il2CppCodeGenWriteBarrier((&___enabled_5), value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614, ___parameters_6)); }
	inline ReadOnlyCollection_1_t4273630488 * get_parameters_6() const { return ___parameters_6; }
	inline ReadOnlyCollection_1_t4273630488 ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(ReadOnlyCollection_1_t4273630488 * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_6), value);
	}
};

struct PostProcessEffectSettings_t1672565614_StaticFields
{
public:
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::<>f__am$cache0
	Func_2_t1761491126 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<System.Reflection.FieldInfo,System.Int32> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::<>f__am$cache1
	Func_2_t320181618 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1761491126 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1761491126 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1761491126 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t320181618 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t320181618 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t320181618 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTSETTINGS_T1672565614_H
#ifndef SCREENSPACEREFLECTIONPRESETPARAMETER_T2494457668_H
#define SCREENSPACEREFLECTIONPRESETPARAMETER_T2494457668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPresetParameter
struct  ScreenSpaceReflectionPresetParameter_t2494457668  : public ParameterOverride_1_t381536667
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONPRESETPARAMETER_T2494457668_H
#ifndef SCREENSPACEREFLECTIONRESOLUTIONPARAMETER_T1804578420_H
#define SCREENSPACEREFLECTIONRESOLUTIONPARAMETER_T1804578420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolutionParameter
struct  ScreenSpaceReflectionResolutionParameter_t1804578420  : public ParameterOverride_1_t1070587220
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONRESOLUTIONPARAMETER_T1804578420_H
#ifndef TONEMAPPERPARAMETER_T2646255172_H
#define TONEMAPPERPARAMETER_T2646255172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TonemapperParameter
struct  TonemapperParameter_t2646255172  : public ParameterOverride_1_t1025085192
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPERPARAMETER_T2646255172_H
#ifndef VIGNETTEMODEPARAMETER_T1229959487_H
#define VIGNETTEMODEPARAMETER_T1229959487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VignetteModeParameter
struct  VignetteModeParameter_t1229959487  : public ParameterOverride_1_t3368882051
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODEPARAMETER_T1229959487_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIFAKESTORE_T4139165440_H
#define UIFAKESTORE_T4139165440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore
struct  UIFakeStore_t4139165440  : public FakeStore_t3710170489
{
public:
	// UnityEngine.Purchasing.UIFakeStore/DialogRequest UnityEngine.Purchasing.UIFakeStore::m_CurrentDialog
	DialogRequest_t599015159 * ___m_CurrentDialog_26;
	// System.Int32 UnityEngine.Purchasing.UIFakeStore::m_LastSelectedDropdownIndex
	int32_t ___m_LastSelectedDropdownIndex_27;
	// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::UIFakeStoreCanvasPrefab
	GameObject_t1113636619 * ___UIFakeStoreCanvasPrefab_28;
	// UnityEngine.Canvas UnityEngine.Purchasing.UIFakeStore::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_29;
	// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::m_EventSystem
	GameObject_t1113636619 * ___m_EventSystem_30;
	// System.String UnityEngine.Purchasing.UIFakeStore::m_ParentGameObjectPath
	String_t* ___m_ParentGameObjectPath_31;

public:
	inline static int32_t get_offset_of_m_CurrentDialog_26() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_CurrentDialog_26)); }
	inline DialogRequest_t599015159 * get_m_CurrentDialog_26() const { return ___m_CurrentDialog_26; }
	inline DialogRequest_t599015159 ** get_address_of_m_CurrentDialog_26() { return &___m_CurrentDialog_26; }
	inline void set_m_CurrentDialog_26(DialogRequest_t599015159 * value)
	{
		___m_CurrentDialog_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentDialog_26), value);
	}

	inline static int32_t get_offset_of_m_LastSelectedDropdownIndex_27() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_LastSelectedDropdownIndex_27)); }
	inline int32_t get_m_LastSelectedDropdownIndex_27() const { return ___m_LastSelectedDropdownIndex_27; }
	inline int32_t* get_address_of_m_LastSelectedDropdownIndex_27() { return &___m_LastSelectedDropdownIndex_27; }
	inline void set_m_LastSelectedDropdownIndex_27(int32_t value)
	{
		___m_LastSelectedDropdownIndex_27 = value;
	}

	inline static int32_t get_offset_of_UIFakeStoreCanvasPrefab_28() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___UIFakeStoreCanvasPrefab_28)); }
	inline GameObject_t1113636619 * get_UIFakeStoreCanvasPrefab_28() const { return ___UIFakeStoreCanvasPrefab_28; }
	inline GameObject_t1113636619 ** get_address_of_UIFakeStoreCanvasPrefab_28() { return &___UIFakeStoreCanvasPrefab_28; }
	inline void set_UIFakeStoreCanvasPrefab_28(GameObject_t1113636619 * value)
	{
		___UIFakeStoreCanvasPrefab_28 = value;
		Il2CppCodeGenWriteBarrier((&___UIFakeStoreCanvasPrefab_28), value);
	}

	inline static int32_t get_offset_of_m_Canvas_29() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_Canvas_29)); }
	inline Canvas_t3310196443 * get_m_Canvas_29() const { return ___m_Canvas_29; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_29() { return &___m_Canvas_29; }
	inline void set_m_Canvas_29(Canvas_t3310196443 * value)
	{
		___m_Canvas_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_29), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_30() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_EventSystem_30)); }
	inline GameObject_t1113636619 * get_m_EventSystem_30() const { return ___m_EventSystem_30; }
	inline GameObject_t1113636619 ** get_address_of_m_EventSystem_30() { return &___m_EventSystem_30; }
	inline void set_m_EventSystem_30(GameObject_t1113636619 * value)
	{
		___m_EventSystem_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_30), value);
	}

	inline static int32_t get_offset_of_m_ParentGameObjectPath_31() { return static_cast<int32_t>(offsetof(UIFakeStore_t4139165440, ___m_ParentGameObjectPath_31)); }
	inline String_t* get_m_ParentGameObjectPath_31() const { return ___m_ParentGameObjectPath_31; }
	inline String_t** get_address_of_m_ParentGameObjectPath_31() { return &___m_ParentGameObjectPath_31; }
	inline void set_m_ParentGameObjectPath_31(String_t* value)
	{
		___m_ParentGameObjectPath_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentGameObjectPath_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFAKESTORE_T4139165440_H
#ifndef AMBIENTOCCLUSION_T1140100160_H
#define AMBIENTOCCLUSION_T1140100160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusion
struct  AmbientOcclusion_t1140100160  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusionModeParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::mode
	AmbientOcclusionModeParameter_t3592449485 * ___mode_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::intensity
	FloatParameter_t1840207740 * ___intensity_10;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::color
	ColorParameter_t2998827320 * ___color_11;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::ambientOnly
	BoolParameter_t2299103272 * ___ambientOnly_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::noiseFilterTolerance
	FloatParameter_t1840207740 * ___noiseFilterTolerance_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::blurTolerance
	FloatParameter_t1840207740 * ___blurTolerance_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::upsampleTolerance
	FloatParameter_t1840207740 * ___upsampleTolerance_15;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::thicknessModifier
	FloatParameter_t1840207740 * ___thicknessModifier_16;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::directLightingStrength
	FloatParameter_t1840207740 * ___directLightingStrength_17;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::radius
	FloatParameter_t1840207740 * ___radius_18;
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQualityParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::quality
	AmbientOcclusionQualityParameter_t3820917191 * ___quality_19;

public:
	inline static int32_t get_offset_of_mode_9() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___mode_9)); }
	inline AmbientOcclusionModeParameter_t3592449485 * get_mode_9() const { return ___mode_9; }
	inline AmbientOcclusionModeParameter_t3592449485 ** get_address_of_mode_9() { return &___mode_9; }
	inline void set_mode_9(AmbientOcclusionModeParameter_t3592449485 * value)
	{
		___mode_9 = value;
		Il2CppCodeGenWriteBarrier((&___mode_9), value);
	}

	inline static int32_t get_offset_of_intensity_10() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___intensity_10)); }
	inline FloatParameter_t1840207740 * get_intensity_10() const { return ___intensity_10; }
	inline FloatParameter_t1840207740 ** get_address_of_intensity_10() { return &___intensity_10; }
	inline void set_intensity_10(FloatParameter_t1840207740 * value)
	{
		___intensity_10 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_10), value);
	}

	inline static int32_t get_offset_of_color_11() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___color_11)); }
	inline ColorParameter_t2998827320 * get_color_11() const { return ___color_11; }
	inline ColorParameter_t2998827320 ** get_address_of_color_11() { return &___color_11; }
	inline void set_color_11(ColorParameter_t2998827320 * value)
	{
		___color_11 = value;
		Il2CppCodeGenWriteBarrier((&___color_11), value);
	}

	inline static int32_t get_offset_of_ambientOnly_12() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___ambientOnly_12)); }
	inline BoolParameter_t2299103272 * get_ambientOnly_12() const { return ___ambientOnly_12; }
	inline BoolParameter_t2299103272 ** get_address_of_ambientOnly_12() { return &___ambientOnly_12; }
	inline void set_ambientOnly_12(BoolParameter_t2299103272 * value)
	{
		___ambientOnly_12 = value;
		Il2CppCodeGenWriteBarrier((&___ambientOnly_12), value);
	}

	inline static int32_t get_offset_of_noiseFilterTolerance_13() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___noiseFilterTolerance_13)); }
	inline FloatParameter_t1840207740 * get_noiseFilterTolerance_13() const { return ___noiseFilterTolerance_13; }
	inline FloatParameter_t1840207740 ** get_address_of_noiseFilterTolerance_13() { return &___noiseFilterTolerance_13; }
	inline void set_noiseFilterTolerance_13(FloatParameter_t1840207740 * value)
	{
		___noiseFilterTolerance_13 = value;
		Il2CppCodeGenWriteBarrier((&___noiseFilterTolerance_13), value);
	}

	inline static int32_t get_offset_of_blurTolerance_14() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___blurTolerance_14)); }
	inline FloatParameter_t1840207740 * get_blurTolerance_14() const { return ___blurTolerance_14; }
	inline FloatParameter_t1840207740 ** get_address_of_blurTolerance_14() { return &___blurTolerance_14; }
	inline void set_blurTolerance_14(FloatParameter_t1840207740 * value)
	{
		___blurTolerance_14 = value;
		Il2CppCodeGenWriteBarrier((&___blurTolerance_14), value);
	}

	inline static int32_t get_offset_of_upsampleTolerance_15() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___upsampleTolerance_15)); }
	inline FloatParameter_t1840207740 * get_upsampleTolerance_15() const { return ___upsampleTolerance_15; }
	inline FloatParameter_t1840207740 ** get_address_of_upsampleTolerance_15() { return &___upsampleTolerance_15; }
	inline void set_upsampleTolerance_15(FloatParameter_t1840207740 * value)
	{
		___upsampleTolerance_15 = value;
		Il2CppCodeGenWriteBarrier((&___upsampleTolerance_15), value);
	}

	inline static int32_t get_offset_of_thicknessModifier_16() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___thicknessModifier_16)); }
	inline FloatParameter_t1840207740 * get_thicknessModifier_16() const { return ___thicknessModifier_16; }
	inline FloatParameter_t1840207740 ** get_address_of_thicknessModifier_16() { return &___thicknessModifier_16; }
	inline void set_thicknessModifier_16(FloatParameter_t1840207740 * value)
	{
		___thicknessModifier_16 = value;
		Il2CppCodeGenWriteBarrier((&___thicknessModifier_16), value);
	}

	inline static int32_t get_offset_of_directLightingStrength_17() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___directLightingStrength_17)); }
	inline FloatParameter_t1840207740 * get_directLightingStrength_17() const { return ___directLightingStrength_17; }
	inline FloatParameter_t1840207740 ** get_address_of_directLightingStrength_17() { return &___directLightingStrength_17; }
	inline void set_directLightingStrength_17(FloatParameter_t1840207740 * value)
	{
		___directLightingStrength_17 = value;
		Il2CppCodeGenWriteBarrier((&___directLightingStrength_17), value);
	}

	inline static int32_t get_offset_of_radius_18() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___radius_18)); }
	inline FloatParameter_t1840207740 * get_radius_18() const { return ___radius_18; }
	inline FloatParameter_t1840207740 ** get_address_of_radius_18() { return &___radius_18; }
	inline void set_radius_18(FloatParameter_t1840207740 * value)
	{
		___radius_18 = value;
		Il2CppCodeGenWriteBarrier((&___radius_18), value);
	}

	inline static int32_t get_offset_of_quality_19() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t1140100160, ___quality_19)); }
	inline AmbientOcclusionQualityParameter_t3820917191 * get_quality_19() const { return ___quality_19; }
	inline AmbientOcclusionQualityParameter_t3820917191 ** get_address_of_quality_19() { return &___quality_19; }
	inline void set_quality_19(AmbientOcclusionQualityParameter_t3820917191 * value)
	{
		___quality_19 = value;
		Il2CppCodeGenWriteBarrier((&___quality_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSION_T1140100160_H
#ifndef AUTOEXPOSURE_T2470830169_H
#define AUTOEXPOSURE_T2470830169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AutoExposure
struct  AutoExposure_t2470830169  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.Vector2Parameter UnityEngine.Rendering.PostProcessing.AutoExposure::filtering
	Vector2Parameter_t1794608574 * ___filtering_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::minLuminance
	FloatParameter_t1840207740 * ___minLuminance_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::maxLuminance
	FloatParameter_t1840207740 * ___maxLuminance_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::keyValue
	FloatParameter_t1840207740 * ___keyValue_12;
	// UnityEngine.Rendering.PostProcessing.EyeAdaptationParameter UnityEngine.Rendering.PostProcessing.AutoExposure::eyeAdaptation
	EyeAdaptationParameter_t2946234496 * ___eyeAdaptation_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::speedUp
	FloatParameter_t1840207740 * ___speedUp_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::speedDown
	FloatParameter_t1840207740 * ___speedDown_15;

public:
	inline static int32_t get_offset_of_filtering_9() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___filtering_9)); }
	inline Vector2Parameter_t1794608574 * get_filtering_9() const { return ___filtering_9; }
	inline Vector2Parameter_t1794608574 ** get_address_of_filtering_9() { return &___filtering_9; }
	inline void set_filtering_9(Vector2Parameter_t1794608574 * value)
	{
		___filtering_9 = value;
		Il2CppCodeGenWriteBarrier((&___filtering_9), value);
	}

	inline static int32_t get_offset_of_minLuminance_10() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___minLuminance_10)); }
	inline FloatParameter_t1840207740 * get_minLuminance_10() const { return ___minLuminance_10; }
	inline FloatParameter_t1840207740 ** get_address_of_minLuminance_10() { return &___minLuminance_10; }
	inline void set_minLuminance_10(FloatParameter_t1840207740 * value)
	{
		___minLuminance_10 = value;
		Il2CppCodeGenWriteBarrier((&___minLuminance_10), value);
	}

	inline static int32_t get_offset_of_maxLuminance_11() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___maxLuminance_11)); }
	inline FloatParameter_t1840207740 * get_maxLuminance_11() const { return ___maxLuminance_11; }
	inline FloatParameter_t1840207740 ** get_address_of_maxLuminance_11() { return &___maxLuminance_11; }
	inline void set_maxLuminance_11(FloatParameter_t1840207740 * value)
	{
		___maxLuminance_11 = value;
		Il2CppCodeGenWriteBarrier((&___maxLuminance_11), value);
	}

	inline static int32_t get_offset_of_keyValue_12() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___keyValue_12)); }
	inline FloatParameter_t1840207740 * get_keyValue_12() const { return ___keyValue_12; }
	inline FloatParameter_t1840207740 ** get_address_of_keyValue_12() { return &___keyValue_12; }
	inline void set_keyValue_12(FloatParameter_t1840207740 * value)
	{
		___keyValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___keyValue_12), value);
	}

	inline static int32_t get_offset_of_eyeAdaptation_13() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___eyeAdaptation_13)); }
	inline EyeAdaptationParameter_t2946234496 * get_eyeAdaptation_13() const { return ___eyeAdaptation_13; }
	inline EyeAdaptationParameter_t2946234496 ** get_address_of_eyeAdaptation_13() { return &___eyeAdaptation_13; }
	inline void set_eyeAdaptation_13(EyeAdaptationParameter_t2946234496 * value)
	{
		___eyeAdaptation_13 = value;
		Il2CppCodeGenWriteBarrier((&___eyeAdaptation_13), value);
	}

	inline static int32_t get_offset_of_speedUp_14() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___speedUp_14)); }
	inline FloatParameter_t1840207740 * get_speedUp_14() const { return ___speedUp_14; }
	inline FloatParameter_t1840207740 ** get_address_of_speedUp_14() { return &___speedUp_14; }
	inline void set_speedUp_14(FloatParameter_t1840207740 * value)
	{
		___speedUp_14 = value;
		Il2CppCodeGenWriteBarrier((&___speedUp_14), value);
	}

	inline static int32_t get_offset_of_speedDown_15() { return static_cast<int32_t>(offsetof(AutoExposure_t2470830169, ___speedDown_15)); }
	inline FloatParameter_t1840207740 * get_speedDown_15() const { return ___speedDown_15; }
	inline FloatParameter_t1840207740 ** get_address_of_speedDown_15() { return &___speedDown_15; }
	inline void set_speedDown_15(FloatParameter_t1840207740 * value)
	{
		___speedDown_15 = value;
		Il2CppCodeGenWriteBarrier((&___speedDown_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOEXPOSURE_T2470830169_H
#ifndef BLOOM_T2742718173_H
#define BLOOM_T2742718173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Bloom
struct  Bloom_t2742718173  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::intensity
	FloatParameter_t1840207740 * ___intensity_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::threshold
	FloatParameter_t1840207740 * ___threshold_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::softKnee
	FloatParameter_t1840207740 * ___softKnee_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::clamp
	FloatParameter_t1840207740 * ___clamp_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::diffusion
	FloatParameter_t1840207740 * ___diffusion_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::anamorphicRatio
	FloatParameter_t1840207740 * ___anamorphicRatio_14;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.Bloom::color
	ColorParameter_t2998827320 * ___color_15;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Bloom::fastMode
	BoolParameter_t2299103272 * ___fastMode_16;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.Bloom::dirtTexture
	TextureParameter_t4267400415 * ___dirtTexture_17;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::dirtIntensity
	FloatParameter_t1840207740 * ___dirtIntensity_18;

public:
	inline static int32_t get_offset_of_intensity_9() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___intensity_9)); }
	inline FloatParameter_t1840207740 * get_intensity_9() const { return ___intensity_9; }
	inline FloatParameter_t1840207740 ** get_address_of_intensity_9() { return &___intensity_9; }
	inline void set_intensity_9(FloatParameter_t1840207740 * value)
	{
		___intensity_9 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_9), value);
	}

	inline static int32_t get_offset_of_threshold_10() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___threshold_10)); }
	inline FloatParameter_t1840207740 * get_threshold_10() const { return ___threshold_10; }
	inline FloatParameter_t1840207740 ** get_address_of_threshold_10() { return &___threshold_10; }
	inline void set_threshold_10(FloatParameter_t1840207740 * value)
	{
		___threshold_10 = value;
		Il2CppCodeGenWriteBarrier((&___threshold_10), value);
	}

	inline static int32_t get_offset_of_softKnee_11() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___softKnee_11)); }
	inline FloatParameter_t1840207740 * get_softKnee_11() const { return ___softKnee_11; }
	inline FloatParameter_t1840207740 ** get_address_of_softKnee_11() { return &___softKnee_11; }
	inline void set_softKnee_11(FloatParameter_t1840207740 * value)
	{
		___softKnee_11 = value;
		Il2CppCodeGenWriteBarrier((&___softKnee_11), value);
	}

	inline static int32_t get_offset_of_clamp_12() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___clamp_12)); }
	inline FloatParameter_t1840207740 * get_clamp_12() const { return ___clamp_12; }
	inline FloatParameter_t1840207740 ** get_address_of_clamp_12() { return &___clamp_12; }
	inline void set_clamp_12(FloatParameter_t1840207740 * value)
	{
		___clamp_12 = value;
		Il2CppCodeGenWriteBarrier((&___clamp_12), value);
	}

	inline static int32_t get_offset_of_diffusion_13() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___diffusion_13)); }
	inline FloatParameter_t1840207740 * get_diffusion_13() const { return ___diffusion_13; }
	inline FloatParameter_t1840207740 ** get_address_of_diffusion_13() { return &___diffusion_13; }
	inline void set_diffusion_13(FloatParameter_t1840207740 * value)
	{
		___diffusion_13 = value;
		Il2CppCodeGenWriteBarrier((&___diffusion_13), value);
	}

	inline static int32_t get_offset_of_anamorphicRatio_14() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___anamorphicRatio_14)); }
	inline FloatParameter_t1840207740 * get_anamorphicRatio_14() const { return ___anamorphicRatio_14; }
	inline FloatParameter_t1840207740 ** get_address_of_anamorphicRatio_14() { return &___anamorphicRatio_14; }
	inline void set_anamorphicRatio_14(FloatParameter_t1840207740 * value)
	{
		___anamorphicRatio_14 = value;
		Il2CppCodeGenWriteBarrier((&___anamorphicRatio_14), value);
	}

	inline static int32_t get_offset_of_color_15() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___color_15)); }
	inline ColorParameter_t2998827320 * get_color_15() const { return ___color_15; }
	inline ColorParameter_t2998827320 ** get_address_of_color_15() { return &___color_15; }
	inline void set_color_15(ColorParameter_t2998827320 * value)
	{
		___color_15 = value;
		Il2CppCodeGenWriteBarrier((&___color_15), value);
	}

	inline static int32_t get_offset_of_fastMode_16() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___fastMode_16)); }
	inline BoolParameter_t2299103272 * get_fastMode_16() const { return ___fastMode_16; }
	inline BoolParameter_t2299103272 ** get_address_of_fastMode_16() { return &___fastMode_16; }
	inline void set_fastMode_16(BoolParameter_t2299103272 * value)
	{
		___fastMode_16 = value;
		Il2CppCodeGenWriteBarrier((&___fastMode_16), value);
	}

	inline static int32_t get_offset_of_dirtTexture_17() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___dirtTexture_17)); }
	inline TextureParameter_t4267400415 * get_dirtTexture_17() const { return ___dirtTexture_17; }
	inline TextureParameter_t4267400415 ** get_address_of_dirtTexture_17() { return &___dirtTexture_17; }
	inline void set_dirtTexture_17(TextureParameter_t4267400415 * value)
	{
		___dirtTexture_17 = value;
		Il2CppCodeGenWriteBarrier((&___dirtTexture_17), value);
	}

	inline static int32_t get_offset_of_dirtIntensity_18() { return static_cast<int32_t>(offsetof(Bloom_t2742718173, ___dirtIntensity_18)); }
	inline FloatParameter_t1840207740 * get_dirtIntensity_18() const { return ___dirtIntensity_18; }
	inline FloatParameter_t1840207740 ** get_address_of_dirtIntensity_18() { return &___dirtIntensity_18; }
	inline void set_dirtIntensity_18(FloatParameter_t1840207740 * value)
	{
		___dirtIntensity_18 = value;
		Il2CppCodeGenWriteBarrier((&___dirtIntensity_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOM_T2742718173_H
#ifndef CHROMATICABERRATION_T2835309264_H
#define CHROMATICABERRATION_T2835309264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ChromaticAberration
struct  ChromaticAberration_t2835309264  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::spectralLut
	TextureParameter_t4267400415 * ___spectralLut_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::intensity
	FloatParameter_t1840207740 * ___intensity_10;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::fastMode
	BoolParameter_t2299103272 * ___fastMode_11;

public:
	inline static int32_t get_offset_of_spectralLut_9() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2835309264, ___spectralLut_9)); }
	inline TextureParameter_t4267400415 * get_spectralLut_9() const { return ___spectralLut_9; }
	inline TextureParameter_t4267400415 ** get_address_of_spectralLut_9() { return &___spectralLut_9; }
	inline void set_spectralLut_9(TextureParameter_t4267400415 * value)
	{
		___spectralLut_9 = value;
		Il2CppCodeGenWriteBarrier((&___spectralLut_9), value);
	}

	inline static int32_t get_offset_of_intensity_10() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2835309264, ___intensity_10)); }
	inline FloatParameter_t1840207740 * get_intensity_10() const { return ___intensity_10; }
	inline FloatParameter_t1840207740 ** get_address_of_intensity_10() { return &___intensity_10; }
	inline void set_intensity_10(FloatParameter_t1840207740 * value)
	{
		___intensity_10 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_10), value);
	}

	inline static int32_t get_offset_of_fastMode_11() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2835309264, ___fastMode_11)); }
	inline BoolParameter_t2299103272 * get_fastMode_11() const { return ___fastMode_11; }
	inline BoolParameter_t2299103272 ** get_address_of_fastMode_11() { return &___fastMode_11; }
	inline void set_fastMode_11(BoolParameter_t2299103272 * value)
	{
		___fastMode_11 = value;
		Il2CppCodeGenWriteBarrier((&___fastMode_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATION_T2835309264_H
#ifndef COLORGRADING_T1956993830_H
#define COLORGRADING_T1956993830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorGrading
struct  ColorGrading_t1956993830  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.GradingModeParameter UnityEngine.Rendering.PostProcessing.ColorGrading::gradingMode
	GradingModeParameter_t3659529292 * ___gradingMode_9;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ColorGrading::externalLut
	TextureParameter_t4267400415 * ___externalLut_10;
	// UnityEngine.Rendering.PostProcessing.TonemapperParameter UnityEngine.Rendering.PostProcessing.ColorGrading::tonemapper
	TonemapperParameter_t2646255172 * ___tonemapper_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveToeStrength
	FloatParameter_t1840207740 * ___toneCurveToeStrength_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveToeLength
	FloatParameter_t1840207740 * ___toneCurveToeLength_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderStrength
	FloatParameter_t1840207740 * ___toneCurveShoulderStrength_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderLength
	FloatParameter_t1840207740 * ___toneCurveShoulderLength_15;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderAngle
	FloatParameter_t1840207740 * ___toneCurveShoulderAngle_16;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveGamma
	FloatParameter_t1840207740 * ___toneCurveGamma_17;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ColorGrading::ldrLut
	TextureParameter_t4267400415 * ___ldrLut_18;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::ldrLutContribution
	FloatParameter_t1840207740 * ___ldrLutContribution_19;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::temperature
	FloatParameter_t1840207740 * ___temperature_20;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::tint
	FloatParameter_t1840207740 * ___tint_21;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.ColorGrading::colorFilter
	ColorParameter_t2998827320 * ___colorFilter_22;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueShift
	FloatParameter_t1840207740 * ___hueShift_23;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::saturation
	FloatParameter_t1840207740 * ___saturation_24;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::brightness
	FloatParameter_t1840207740 * ___brightness_25;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::postExposure
	FloatParameter_t1840207740 * ___postExposure_26;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::contrast
	FloatParameter_t1840207740 * ___contrast_27;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutRedIn
	FloatParameter_t1840207740 * ___mixerRedOutRedIn_28;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutGreenIn
	FloatParameter_t1840207740 * ___mixerRedOutGreenIn_29;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutBlueIn
	FloatParameter_t1840207740 * ___mixerRedOutBlueIn_30;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutRedIn
	FloatParameter_t1840207740 * ___mixerGreenOutRedIn_31;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutGreenIn
	FloatParameter_t1840207740 * ___mixerGreenOutGreenIn_32;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutBlueIn
	FloatParameter_t1840207740 * ___mixerGreenOutBlueIn_33;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutRedIn
	FloatParameter_t1840207740 * ___mixerBlueOutRedIn_34;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutGreenIn
	FloatParameter_t1840207740 * ___mixerBlueOutGreenIn_35;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutBlueIn
	FloatParameter_t1840207740 * ___mixerBlueOutBlueIn_36;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::lift
	Vector4Parameter_t1505856958 * ___lift_37;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::gamma
	Vector4Parameter_t1505856958 * ___gamma_38;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::gain
	Vector4Parameter_t1505856958 * ___gain_39;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::masterCurve
	SplineParameter_t905443520 * ___masterCurve_40;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::redCurve
	SplineParameter_t905443520 * ___redCurve_41;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::greenCurve
	SplineParameter_t905443520 * ___greenCurve_42;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::blueCurve
	SplineParameter_t905443520 * ___blueCurve_43;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueVsHueCurve
	SplineParameter_t905443520 * ___hueVsHueCurve_44;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueVsSatCurve
	SplineParameter_t905443520 * ___hueVsSatCurve_45;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::satVsSatCurve
	SplineParameter_t905443520 * ___satVsSatCurve_46;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::lumVsSatCurve
	SplineParameter_t905443520 * ___lumVsSatCurve_47;

public:
	inline static int32_t get_offset_of_gradingMode_9() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___gradingMode_9)); }
	inline GradingModeParameter_t3659529292 * get_gradingMode_9() const { return ___gradingMode_9; }
	inline GradingModeParameter_t3659529292 ** get_address_of_gradingMode_9() { return &___gradingMode_9; }
	inline void set_gradingMode_9(GradingModeParameter_t3659529292 * value)
	{
		___gradingMode_9 = value;
		Il2CppCodeGenWriteBarrier((&___gradingMode_9), value);
	}

	inline static int32_t get_offset_of_externalLut_10() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___externalLut_10)); }
	inline TextureParameter_t4267400415 * get_externalLut_10() const { return ___externalLut_10; }
	inline TextureParameter_t4267400415 ** get_address_of_externalLut_10() { return &___externalLut_10; }
	inline void set_externalLut_10(TextureParameter_t4267400415 * value)
	{
		___externalLut_10 = value;
		Il2CppCodeGenWriteBarrier((&___externalLut_10), value);
	}

	inline static int32_t get_offset_of_tonemapper_11() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___tonemapper_11)); }
	inline TonemapperParameter_t2646255172 * get_tonemapper_11() const { return ___tonemapper_11; }
	inline TonemapperParameter_t2646255172 ** get_address_of_tonemapper_11() { return &___tonemapper_11; }
	inline void set_tonemapper_11(TonemapperParameter_t2646255172 * value)
	{
		___tonemapper_11 = value;
		Il2CppCodeGenWriteBarrier((&___tonemapper_11), value);
	}

	inline static int32_t get_offset_of_toneCurveToeStrength_12() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___toneCurveToeStrength_12)); }
	inline FloatParameter_t1840207740 * get_toneCurveToeStrength_12() const { return ___toneCurveToeStrength_12; }
	inline FloatParameter_t1840207740 ** get_address_of_toneCurveToeStrength_12() { return &___toneCurveToeStrength_12; }
	inline void set_toneCurveToeStrength_12(FloatParameter_t1840207740 * value)
	{
		___toneCurveToeStrength_12 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveToeStrength_12), value);
	}

	inline static int32_t get_offset_of_toneCurveToeLength_13() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___toneCurveToeLength_13)); }
	inline FloatParameter_t1840207740 * get_toneCurveToeLength_13() const { return ___toneCurveToeLength_13; }
	inline FloatParameter_t1840207740 ** get_address_of_toneCurveToeLength_13() { return &___toneCurveToeLength_13; }
	inline void set_toneCurveToeLength_13(FloatParameter_t1840207740 * value)
	{
		___toneCurveToeLength_13 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveToeLength_13), value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderStrength_14() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___toneCurveShoulderStrength_14)); }
	inline FloatParameter_t1840207740 * get_toneCurveShoulderStrength_14() const { return ___toneCurveShoulderStrength_14; }
	inline FloatParameter_t1840207740 ** get_address_of_toneCurveShoulderStrength_14() { return &___toneCurveShoulderStrength_14; }
	inline void set_toneCurveShoulderStrength_14(FloatParameter_t1840207740 * value)
	{
		___toneCurveShoulderStrength_14 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveShoulderStrength_14), value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderLength_15() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___toneCurveShoulderLength_15)); }
	inline FloatParameter_t1840207740 * get_toneCurveShoulderLength_15() const { return ___toneCurveShoulderLength_15; }
	inline FloatParameter_t1840207740 ** get_address_of_toneCurveShoulderLength_15() { return &___toneCurveShoulderLength_15; }
	inline void set_toneCurveShoulderLength_15(FloatParameter_t1840207740 * value)
	{
		___toneCurveShoulderLength_15 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveShoulderLength_15), value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderAngle_16() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___toneCurveShoulderAngle_16)); }
	inline FloatParameter_t1840207740 * get_toneCurveShoulderAngle_16() const { return ___toneCurveShoulderAngle_16; }
	inline FloatParameter_t1840207740 ** get_address_of_toneCurveShoulderAngle_16() { return &___toneCurveShoulderAngle_16; }
	inline void set_toneCurveShoulderAngle_16(FloatParameter_t1840207740 * value)
	{
		___toneCurveShoulderAngle_16 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveShoulderAngle_16), value);
	}

	inline static int32_t get_offset_of_toneCurveGamma_17() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___toneCurveGamma_17)); }
	inline FloatParameter_t1840207740 * get_toneCurveGamma_17() const { return ___toneCurveGamma_17; }
	inline FloatParameter_t1840207740 ** get_address_of_toneCurveGamma_17() { return &___toneCurveGamma_17; }
	inline void set_toneCurveGamma_17(FloatParameter_t1840207740 * value)
	{
		___toneCurveGamma_17 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveGamma_17), value);
	}

	inline static int32_t get_offset_of_ldrLut_18() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___ldrLut_18)); }
	inline TextureParameter_t4267400415 * get_ldrLut_18() const { return ___ldrLut_18; }
	inline TextureParameter_t4267400415 ** get_address_of_ldrLut_18() { return &___ldrLut_18; }
	inline void set_ldrLut_18(TextureParameter_t4267400415 * value)
	{
		___ldrLut_18 = value;
		Il2CppCodeGenWriteBarrier((&___ldrLut_18), value);
	}

	inline static int32_t get_offset_of_ldrLutContribution_19() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___ldrLutContribution_19)); }
	inline FloatParameter_t1840207740 * get_ldrLutContribution_19() const { return ___ldrLutContribution_19; }
	inline FloatParameter_t1840207740 ** get_address_of_ldrLutContribution_19() { return &___ldrLutContribution_19; }
	inline void set_ldrLutContribution_19(FloatParameter_t1840207740 * value)
	{
		___ldrLutContribution_19 = value;
		Il2CppCodeGenWriteBarrier((&___ldrLutContribution_19), value);
	}

	inline static int32_t get_offset_of_temperature_20() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___temperature_20)); }
	inline FloatParameter_t1840207740 * get_temperature_20() const { return ___temperature_20; }
	inline FloatParameter_t1840207740 ** get_address_of_temperature_20() { return &___temperature_20; }
	inline void set_temperature_20(FloatParameter_t1840207740 * value)
	{
		___temperature_20 = value;
		Il2CppCodeGenWriteBarrier((&___temperature_20), value);
	}

	inline static int32_t get_offset_of_tint_21() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___tint_21)); }
	inline FloatParameter_t1840207740 * get_tint_21() const { return ___tint_21; }
	inline FloatParameter_t1840207740 ** get_address_of_tint_21() { return &___tint_21; }
	inline void set_tint_21(FloatParameter_t1840207740 * value)
	{
		___tint_21 = value;
		Il2CppCodeGenWriteBarrier((&___tint_21), value);
	}

	inline static int32_t get_offset_of_colorFilter_22() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___colorFilter_22)); }
	inline ColorParameter_t2998827320 * get_colorFilter_22() const { return ___colorFilter_22; }
	inline ColorParameter_t2998827320 ** get_address_of_colorFilter_22() { return &___colorFilter_22; }
	inline void set_colorFilter_22(ColorParameter_t2998827320 * value)
	{
		___colorFilter_22 = value;
		Il2CppCodeGenWriteBarrier((&___colorFilter_22), value);
	}

	inline static int32_t get_offset_of_hueShift_23() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___hueShift_23)); }
	inline FloatParameter_t1840207740 * get_hueShift_23() const { return ___hueShift_23; }
	inline FloatParameter_t1840207740 ** get_address_of_hueShift_23() { return &___hueShift_23; }
	inline void set_hueShift_23(FloatParameter_t1840207740 * value)
	{
		___hueShift_23 = value;
		Il2CppCodeGenWriteBarrier((&___hueShift_23), value);
	}

	inline static int32_t get_offset_of_saturation_24() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___saturation_24)); }
	inline FloatParameter_t1840207740 * get_saturation_24() const { return ___saturation_24; }
	inline FloatParameter_t1840207740 ** get_address_of_saturation_24() { return &___saturation_24; }
	inline void set_saturation_24(FloatParameter_t1840207740 * value)
	{
		___saturation_24 = value;
		Il2CppCodeGenWriteBarrier((&___saturation_24), value);
	}

	inline static int32_t get_offset_of_brightness_25() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___brightness_25)); }
	inline FloatParameter_t1840207740 * get_brightness_25() const { return ___brightness_25; }
	inline FloatParameter_t1840207740 ** get_address_of_brightness_25() { return &___brightness_25; }
	inline void set_brightness_25(FloatParameter_t1840207740 * value)
	{
		___brightness_25 = value;
		Il2CppCodeGenWriteBarrier((&___brightness_25), value);
	}

	inline static int32_t get_offset_of_postExposure_26() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___postExposure_26)); }
	inline FloatParameter_t1840207740 * get_postExposure_26() const { return ___postExposure_26; }
	inline FloatParameter_t1840207740 ** get_address_of_postExposure_26() { return &___postExposure_26; }
	inline void set_postExposure_26(FloatParameter_t1840207740 * value)
	{
		___postExposure_26 = value;
		Il2CppCodeGenWriteBarrier((&___postExposure_26), value);
	}

	inline static int32_t get_offset_of_contrast_27() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___contrast_27)); }
	inline FloatParameter_t1840207740 * get_contrast_27() const { return ___contrast_27; }
	inline FloatParameter_t1840207740 ** get_address_of_contrast_27() { return &___contrast_27; }
	inline void set_contrast_27(FloatParameter_t1840207740 * value)
	{
		___contrast_27 = value;
		Il2CppCodeGenWriteBarrier((&___contrast_27), value);
	}

	inline static int32_t get_offset_of_mixerRedOutRedIn_28() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerRedOutRedIn_28)); }
	inline FloatParameter_t1840207740 * get_mixerRedOutRedIn_28() const { return ___mixerRedOutRedIn_28; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerRedOutRedIn_28() { return &___mixerRedOutRedIn_28; }
	inline void set_mixerRedOutRedIn_28(FloatParameter_t1840207740 * value)
	{
		___mixerRedOutRedIn_28 = value;
		Il2CppCodeGenWriteBarrier((&___mixerRedOutRedIn_28), value);
	}

	inline static int32_t get_offset_of_mixerRedOutGreenIn_29() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerRedOutGreenIn_29)); }
	inline FloatParameter_t1840207740 * get_mixerRedOutGreenIn_29() const { return ___mixerRedOutGreenIn_29; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerRedOutGreenIn_29() { return &___mixerRedOutGreenIn_29; }
	inline void set_mixerRedOutGreenIn_29(FloatParameter_t1840207740 * value)
	{
		___mixerRedOutGreenIn_29 = value;
		Il2CppCodeGenWriteBarrier((&___mixerRedOutGreenIn_29), value);
	}

	inline static int32_t get_offset_of_mixerRedOutBlueIn_30() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerRedOutBlueIn_30)); }
	inline FloatParameter_t1840207740 * get_mixerRedOutBlueIn_30() const { return ___mixerRedOutBlueIn_30; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerRedOutBlueIn_30() { return &___mixerRedOutBlueIn_30; }
	inline void set_mixerRedOutBlueIn_30(FloatParameter_t1840207740 * value)
	{
		___mixerRedOutBlueIn_30 = value;
		Il2CppCodeGenWriteBarrier((&___mixerRedOutBlueIn_30), value);
	}

	inline static int32_t get_offset_of_mixerGreenOutRedIn_31() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerGreenOutRedIn_31)); }
	inline FloatParameter_t1840207740 * get_mixerGreenOutRedIn_31() const { return ___mixerGreenOutRedIn_31; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerGreenOutRedIn_31() { return &___mixerGreenOutRedIn_31; }
	inline void set_mixerGreenOutRedIn_31(FloatParameter_t1840207740 * value)
	{
		___mixerGreenOutRedIn_31 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGreenOutRedIn_31), value);
	}

	inline static int32_t get_offset_of_mixerGreenOutGreenIn_32() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerGreenOutGreenIn_32)); }
	inline FloatParameter_t1840207740 * get_mixerGreenOutGreenIn_32() const { return ___mixerGreenOutGreenIn_32; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerGreenOutGreenIn_32() { return &___mixerGreenOutGreenIn_32; }
	inline void set_mixerGreenOutGreenIn_32(FloatParameter_t1840207740 * value)
	{
		___mixerGreenOutGreenIn_32 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGreenOutGreenIn_32), value);
	}

	inline static int32_t get_offset_of_mixerGreenOutBlueIn_33() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerGreenOutBlueIn_33)); }
	inline FloatParameter_t1840207740 * get_mixerGreenOutBlueIn_33() const { return ___mixerGreenOutBlueIn_33; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerGreenOutBlueIn_33() { return &___mixerGreenOutBlueIn_33; }
	inline void set_mixerGreenOutBlueIn_33(FloatParameter_t1840207740 * value)
	{
		___mixerGreenOutBlueIn_33 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGreenOutBlueIn_33), value);
	}

	inline static int32_t get_offset_of_mixerBlueOutRedIn_34() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerBlueOutRedIn_34)); }
	inline FloatParameter_t1840207740 * get_mixerBlueOutRedIn_34() const { return ___mixerBlueOutRedIn_34; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerBlueOutRedIn_34() { return &___mixerBlueOutRedIn_34; }
	inline void set_mixerBlueOutRedIn_34(FloatParameter_t1840207740 * value)
	{
		___mixerBlueOutRedIn_34 = value;
		Il2CppCodeGenWriteBarrier((&___mixerBlueOutRedIn_34), value);
	}

	inline static int32_t get_offset_of_mixerBlueOutGreenIn_35() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerBlueOutGreenIn_35)); }
	inline FloatParameter_t1840207740 * get_mixerBlueOutGreenIn_35() const { return ___mixerBlueOutGreenIn_35; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerBlueOutGreenIn_35() { return &___mixerBlueOutGreenIn_35; }
	inline void set_mixerBlueOutGreenIn_35(FloatParameter_t1840207740 * value)
	{
		___mixerBlueOutGreenIn_35 = value;
		Il2CppCodeGenWriteBarrier((&___mixerBlueOutGreenIn_35), value);
	}

	inline static int32_t get_offset_of_mixerBlueOutBlueIn_36() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___mixerBlueOutBlueIn_36)); }
	inline FloatParameter_t1840207740 * get_mixerBlueOutBlueIn_36() const { return ___mixerBlueOutBlueIn_36; }
	inline FloatParameter_t1840207740 ** get_address_of_mixerBlueOutBlueIn_36() { return &___mixerBlueOutBlueIn_36; }
	inline void set_mixerBlueOutBlueIn_36(FloatParameter_t1840207740 * value)
	{
		___mixerBlueOutBlueIn_36 = value;
		Il2CppCodeGenWriteBarrier((&___mixerBlueOutBlueIn_36), value);
	}

	inline static int32_t get_offset_of_lift_37() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___lift_37)); }
	inline Vector4Parameter_t1505856958 * get_lift_37() const { return ___lift_37; }
	inline Vector4Parameter_t1505856958 ** get_address_of_lift_37() { return &___lift_37; }
	inline void set_lift_37(Vector4Parameter_t1505856958 * value)
	{
		___lift_37 = value;
		Il2CppCodeGenWriteBarrier((&___lift_37), value);
	}

	inline static int32_t get_offset_of_gamma_38() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___gamma_38)); }
	inline Vector4Parameter_t1505856958 * get_gamma_38() const { return ___gamma_38; }
	inline Vector4Parameter_t1505856958 ** get_address_of_gamma_38() { return &___gamma_38; }
	inline void set_gamma_38(Vector4Parameter_t1505856958 * value)
	{
		___gamma_38 = value;
		Il2CppCodeGenWriteBarrier((&___gamma_38), value);
	}

	inline static int32_t get_offset_of_gain_39() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___gain_39)); }
	inline Vector4Parameter_t1505856958 * get_gain_39() const { return ___gain_39; }
	inline Vector4Parameter_t1505856958 ** get_address_of_gain_39() { return &___gain_39; }
	inline void set_gain_39(Vector4Parameter_t1505856958 * value)
	{
		___gain_39 = value;
		Il2CppCodeGenWriteBarrier((&___gain_39), value);
	}

	inline static int32_t get_offset_of_masterCurve_40() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___masterCurve_40)); }
	inline SplineParameter_t905443520 * get_masterCurve_40() const { return ___masterCurve_40; }
	inline SplineParameter_t905443520 ** get_address_of_masterCurve_40() { return &___masterCurve_40; }
	inline void set_masterCurve_40(SplineParameter_t905443520 * value)
	{
		___masterCurve_40 = value;
		Il2CppCodeGenWriteBarrier((&___masterCurve_40), value);
	}

	inline static int32_t get_offset_of_redCurve_41() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___redCurve_41)); }
	inline SplineParameter_t905443520 * get_redCurve_41() const { return ___redCurve_41; }
	inline SplineParameter_t905443520 ** get_address_of_redCurve_41() { return &___redCurve_41; }
	inline void set_redCurve_41(SplineParameter_t905443520 * value)
	{
		___redCurve_41 = value;
		Il2CppCodeGenWriteBarrier((&___redCurve_41), value);
	}

	inline static int32_t get_offset_of_greenCurve_42() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___greenCurve_42)); }
	inline SplineParameter_t905443520 * get_greenCurve_42() const { return ___greenCurve_42; }
	inline SplineParameter_t905443520 ** get_address_of_greenCurve_42() { return &___greenCurve_42; }
	inline void set_greenCurve_42(SplineParameter_t905443520 * value)
	{
		___greenCurve_42 = value;
		Il2CppCodeGenWriteBarrier((&___greenCurve_42), value);
	}

	inline static int32_t get_offset_of_blueCurve_43() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___blueCurve_43)); }
	inline SplineParameter_t905443520 * get_blueCurve_43() const { return ___blueCurve_43; }
	inline SplineParameter_t905443520 ** get_address_of_blueCurve_43() { return &___blueCurve_43; }
	inline void set_blueCurve_43(SplineParameter_t905443520 * value)
	{
		___blueCurve_43 = value;
		Il2CppCodeGenWriteBarrier((&___blueCurve_43), value);
	}

	inline static int32_t get_offset_of_hueVsHueCurve_44() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___hueVsHueCurve_44)); }
	inline SplineParameter_t905443520 * get_hueVsHueCurve_44() const { return ___hueVsHueCurve_44; }
	inline SplineParameter_t905443520 ** get_address_of_hueVsHueCurve_44() { return &___hueVsHueCurve_44; }
	inline void set_hueVsHueCurve_44(SplineParameter_t905443520 * value)
	{
		___hueVsHueCurve_44 = value;
		Il2CppCodeGenWriteBarrier((&___hueVsHueCurve_44), value);
	}

	inline static int32_t get_offset_of_hueVsSatCurve_45() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___hueVsSatCurve_45)); }
	inline SplineParameter_t905443520 * get_hueVsSatCurve_45() const { return ___hueVsSatCurve_45; }
	inline SplineParameter_t905443520 ** get_address_of_hueVsSatCurve_45() { return &___hueVsSatCurve_45; }
	inline void set_hueVsSatCurve_45(SplineParameter_t905443520 * value)
	{
		___hueVsSatCurve_45 = value;
		Il2CppCodeGenWriteBarrier((&___hueVsSatCurve_45), value);
	}

	inline static int32_t get_offset_of_satVsSatCurve_46() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___satVsSatCurve_46)); }
	inline SplineParameter_t905443520 * get_satVsSatCurve_46() const { return ___satVsSatCurve_46; }
	inline SplineParameter_t905443520 ** get_address_of_satVsSatCurve_46() { return &___satVsSatCurve_46; }
	inline void set_satVsSatCurve_46(SplineParameter_t905443520 * value)
	{
		___satVsSatCurve_46 = value;
		Il2CppCodeGenWriteBarrier((&___satVsSatCurve_46), value);
	}

	inline static int32_t get_offset_of_lumVsSatCurve_47() { return static_cast<int32_t>(offsetof(ColorGrading_t1956993830, ___lumVsSatCurve_47)); }
	inline SplineParameter_t905443520 * get_lumVsSatCurve_47() const { return ___lumVsSatCurve_47; }
	inline SplineParameter_t905443520 ** get_address_of_lumVsSatCurve_47() { return &___lumVsSatCurve_47; }
	inline void set_lumVsSatCurve_47(SplineParameter_t905443520 * value)
	{
		___lumVsSatCurve_47 = value;
		Il2CppCodeGenWriteBarrier((&___lumVsSatCurve_47), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADING_T1956993830_H
#ifndef DEPTHOFFIELD_T1057712103_H
#define DEPTHOFFIELD_T1057712103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DepthOfField
struct  DepthOfField_t1057712103  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::focusDistance
	FloatParameter_t1840207740 * ___focusDistance_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::aperture
	FloatParameter_t1840207740 * ___aperture_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::focalLength
	FloatParameter_t1840207740 * ___focalLength_11;
	// UnityEngine.Rendering.PostProcessing.KernelSizeParameter UnityEngine.Rendering.PostProcessing.DepthOfField::kernelSize
	KernelSizeParameter_t3218545409 * ___kernelSize_12;

public:
	inline static int32_t get_offset_of_focusDistance_9() { return static_cast<int32_t>(offsetof(DepthOfField_t1057712103, ___focusDistance_9)); }
	inline FloatParameter_t1840207740 * get_focusDistance_9() const { return ___focusDistance_9; }
	inline FloatParameter_t1840207740 ** get_address_of_focusDistance_9() { return &___focusDistance_9; }
	inline void set_focusDistance_9(FloatParameter_t1840207740 * value)
	{
		___focusDistance_9 = value;
		Il2CppCodeGenWriteBarrier((&___focusDistance_9), value);
	}

	inline static int32_t get_offset_of_aperture_10() { return static_cast<int32_t>(offsetof(DepthOfField_t1057712103, ___aperture_10)); }
	inline FloatParameter_t1840207740 * get_aperture_10() const { return ___aperture_10; }
	inline FloatParameter_t1840207740 ** get_address_of_aperture_10() { return &___aperture_10; }
	inline void set_aperture_10(FloatParameter_t1840207740 * value)
	{
		___aperture_10 = value;
		Il2CppCodeGenWriteBarrier((&___aperture_10), value);
	}

	inline static int32_t get_offset_of_focalLength_11() { return static_cast<int32_t>(offsetof(DepthOfField_t1057712103, ___focalLength_11)); }
	inline FloatParameter_t1840207740 * get_focalLength_11() const { return ___focalLength_11; }
	inline FloatParameter_t1840207740 ** get_address_of_focalLength_11() { return &___focalLength_11; }
	inline void set_focalLength_11(FloatParameter_t1840207740 * value)
	{
		___focalLength_11 = value;
		Il2CppCodeGenWriteBarrier((&___focalLength_11), value);
	}

	inline static int32_t get_offset_of_kernelSize_12() { return static_cast<int32_t>(offsetof(DepthOfField_t1057712103, ___kernelSize_12)); }
	inline KernelSizeParameter_t3218545409 * get_kernelSize_12() const { return ___kernelSize_12; }
	inline KernelSizeParameter_t3218545409 ** get_address_of_kernelSize_12() { return &___kernelSize_12; }
	inline void set_kernelSize_12(KernelSizeParameter_t3218545409 * value)
	{
		___kernelSize_12 = value;
		Il2CppCodeGenWriteBarrier((&___kernelSize_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELD_T1057712103_H
#ifndef GRAIN_T3181688601_H
#define GRAIN_T3181688601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Grain
struct  Grain_t3181688601  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Grain::colored
	BoolParameter_t2299103272 * ___colored_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Grain::intensity
	FloatParameter_t1840207740 * ___intensity_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Grain::size
	FloatParameter_t1840207740 * ___size_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Grain::lumContrib
	FloatParameter_t1840207740 * ___lumContrib_12;

public:
	inline static int32_t get_offset_of_colored_9() { return static_cast<int32_t>(offsetof(Grain_t3181688601, ___colored_9)); }
	inline BoolParameter_t2299103272 * get_colored_9() const { return ___colored_9; }
	inline BoolParameter_t2299103272 ** get_address_of_colored_9() { return &___colored_9; }
	inline void set_colored_9(BoolParameter_t2299103272 * value)
	{
		___colored_9 = value;
		Il2CppCodeGenWriteBarrier((&___colored_9), value);
	}

	inline static int32_t get_offset_of_intensity_10() { return static_cast<int32_t>(offsetof(Grain_t3181688601, ___intensity_10)); }
	inline FloatParameter_t1840207740 * get_intensity_10() const { return ___intensity_10; }
	inline FloatParameter_t1840207740 ** get_address_of_intensity_10() { return &___intensity_10; }
	inline void set_intensity_10(FloatParameter_t1840207740 * value)
	{
		___intensity_10 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_10), value);
	}

	inline static int32_t get_offset_of_size_11() { return static_cast<int32_t>(offsetof(Grain_t3181688601, ___size_11)); }
	inline FloatParameter_t1840207740 * get_size_11() const { return ___size_11; }
	inline FloatParameter_t1840207740 ** get_address_of_size_11() { return &___size_11; }
	inline void set_size_11(FloatParameter_t1840207740 * value)
	{
		___size_11 = value;
		Il2CppCodeGenWriteBarrier((&___size_11), value);
	}

	inline static int32_t get_offset_of_lumContrib_12() { return static_cast<int32_t>(offsetof(Grain_t3181688601, ___lumContrib_12)); }
	inline FloatParameter_t1840207740 * get_lumContrib_12() const { return ___lumContrib_12; }
	inline FloatParameter_t1840207740 ** get_address_of_lumContrib_12() { return &___lumContrib_12; }
	inline void set_lumContrib_12(FloatParameter_t1840207740 * value)
	{
		___lumContrib_12 = value;
		Il2CppCodeGenWriteBarrier((&___lumContrib_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAIN_T3181688601_H
#ifndef LENSDISTORTION_T708814765_H
#define LENSDISTORTION_T708814765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LensDistortion
struct  LensDistortion_t708814765  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensity
	FloatParameter_t1840207740 * ___intensity_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensityX
	FloatParameter_t1840207740 * ___intensityX_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensityY
	FloatParameter_t1840207740 * ___intensityY_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::centerX
	FloatParameter_t1840207740 * ___centerX_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::centerY
	FloatParameter_t1840207740 * ___centerY_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::scale
	FloatParameter_t1840207740 * ___scale_14;

public:
	inline static int32_t get_offset_of_intensity_9() { return static_cast<int32_t>(offsetof(LensDistortion_t708814765, ___intensity_9)); }
	inline FloatParameter_t1840207740 * get_intensity_9() const { return ___intensity_9; }
	inline FloatParameter_t1840207740 ** get_address_of_intensity_9() { return &___intensity_9; }
	inline void set_intensity_9(FloatParameter_t1840207740 * value)
	{
		___intensity_9 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_9), value);
	}

	inline static int32_t get_offset_of_intensityX_10() { return static_cast<int32_t>(offsetof(LensDistortion_t708814765, ___intensityX_10)); }
	inline FloatParameter_t1840207740 * get_intensityX_10() const { return ___intensityX_10; }
	inline FloatParameter_t1840207740 ** get_address_of_intensityX_10() { return &___intensityX_10; }
	inline void set_intensityX_10(FloatParameter_t1840207740 * value)
	{
		___intensityX_10 = value;
		Il2CppCodeGenWriteBarrier((&___intensityX_10), value);
	}

	inline static int32_t get_offset_of_intensityY_11() { return static_cast<int32_t>(offsetof(LensDistortion_t708814765, ___intensityY_11)); }
	inline FloatParameter_t1840207740 * get_intensityY_11() const { return ___intensityY_11; }
	inline FloatParameter_t1840207740 ** get_address_of_intensityY_11() { return &___intensityY_11; }
	inline void set_intensityY_11(FloatParameter_t1840207740 * value)
	{
		___intensityY_11 = value;
		Il2CppCodeGenWriteBarrier((&___intensityY_11), value);
	}

	inline static int32_t get_offset_of_centerX_12() { return static_cast<int32_t>(offsetof(LensDistortion_t708814765, ___centerX_12)); }
	inline FloatParameter_t1840207740 * get_centerX_12() const { return ___centerX_12; }
	inline FloatParameter_t1840207740 ** get_address_of_centerX_12() { return &___centerX_12; }
	inline void set_centerX_12(FloatParameter_t1840207740 * value)
	{
		___centerX_12 = value;
		Il2CppCodeGenWriteBarrier((&___centerX_12), value);
	}

	inline static int32_t get_offset_of_centerY_13() { return static_cast<int32_t>(offsetof(LensDistortion_t708814765, ___centerY_13)); }
	inline FloatParameter_t1840207740 * get_centerY_13() const { return ___centerY_13; }
	inline FloatParameter_t1840207740 ** get_address_of_centerY_13() { return &___centerY_13; }
	inline void set_centerY_13(FloatParameter_t1840207740 * value)
	{
		___centerY_13 = value;
		Il2CppCodeGenWriteBarrier((&___centerY_13), value);
	}

	inline static int32_t get_offset_of_scale_14() { return static_cast<int32_t>(offsetof(LensDistortion_t708814765, ___scale_14)); }
	inline FloatParameter_t1840207740 * get_scale_14() const { return ___scale_14; }
	inline FloatParameter_t1840207740 ** get_address_of_scale_14() { return &___scale_14; }
	inline void set_scale_14(FloatParameter_t1840207740 * value)
	{
		___scale_14 = value;
		Il2CppCodeGenWriteBarrier((&___scale_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSDISTORTION_T708814765_H
#ifndef MOTIONBLUR_T2809166275_H
#define MOTIONBLUR_T2809166275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MotionBlur
struct  MotionBlur_t2809166275  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.MotionBlur::shutterAngle
	FloatParameter_t1840207740 * ___shutterAngle_9;
	// UnityEngine.Rendering.PostProcessing.IntParameter UnityEngine.Rendering.PostProcessing.MotionBlur::sampleCount
	IntParameter_t773781776 * ___sampleCount_10;

public:
	inline static int32_t get_offset_of_shutterAngle_9() { return static_cast<int32_t>(offsetof(MotionBlur_t2809166275, ___shutterAngle_9)); }
	inline FloatParameter_t1840207740 * get_shutterAngle_9() const { return ___shutterAngle_9; }
	inline FloatParameter_t1840207740 ** get_address_of_shutterAngle_9() { return &___shutterAngle_9; }
	inline void set_shutterAngle_9(FloatParameter_t1840207740 * value)
	{
		___shutterAngle_9 = value;
		Il2CppCodeGenWriteBarrier((&___shutterAngle_9), value);
	}

	inline static int32_t get_offset_of_sampleCount_10() { return static_cast<int32_t>(offsetof(MotionBlur_t2809166275, ___sampleCount_10)); }
	inline IntParameter_t773781776 * get_sampleCount_10() const { return ___sampleCount_10; }
	inline IntParameter_t773781776 ** get_address_of_sampleCount_10() { return &___sampleCount_10; }
	inline void set_sampleCount_10(IntParameter_t773781776 * value)
	{
		___sampleCount_10 = value;
		Il2CppCodeGenWriteBarrier((&___sampleCount_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_T2809166275_H
#ifndef SCREENSPACEREFLECTIONS_T3117296337_H
#define SCREENSPACEREFLECTIONS_T3117296337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections
struct  ScreenSpaceReflections_t3117296337  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPresetParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::preset
	ScreenSpaceReflectionPresetParameter_t2494457668 * ___preset_9;
	// UnityEngine.Rendering.PostProcessing.IntParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::maximumIterationCount
	IntParameter_t773781776 * ___maximumIterationCount_10;
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolutionParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::resolution
	ScreenSpaceReflectionResolutionParameter_t1804578420 * ___resolution_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::thickness
	FloatParameter_t1840207740 * ___thickness_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::maximumMarchDistance
	FloatParameter_t1840207740 * ___maximumMarchDistance_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::distanceFade
	FloatParameter_t1840207740 * ___distanceFade_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::vignette
	FloatParameter_t1840207740 * ___vignette_15;

public:
	inline static int32_t get_offset_of_preset_9() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___preset_9)); }
	inline ScreenSpaceReflectionPresetParameter_t2494457668 * get_preset_9() const { return ___preset_9; }
	inline ScreenSpaceReflectionPresetParameter_t2494457668 ** get_address_of_preset_9() { return &___preset_9; }
	inline void set_preset_9(ScreenSpaceReflectionPresetParameter_t2494457668 * value)
	{
		___preset_9 = value;
		Il2CppCodeGenWriteBarrier((&___preset_9), value);
	}

	inline static int32_t get_offset_of_maximumIterationCount_10() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___maximumIterationCount_10)); }
	inline IntParameter_t773781776 * get_maximumIterationCount_10() const { return ___maximumIterationCount_10; }
	inline IntParameter_t773781776 ** get_address_of_maximumIterationCount_10() { return &___maximumIterationCount_10; }
	inline void set_maximumIterationCount_10(IntParameter_t773781776 * value)
	{
		___maximumIterationCount_10 = value;
		Il2CppCodeGenWriteBarrier((&___maximumIterationCount_10), value);
	}

	inline static int32_t get_offset_of_resolution_11() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___resolution_11)); }
	inline ScreenSpaceReflectionResolutionParameter_t1804578420 * get_resolution_11() const { return ___resolution_11; }
	inline ScreenSpaceReflectionResolutionParameter_t1804578420 ** get_address_of_resolution_11() { return &___resolution_11; }
	inline void set_resolution_11(ScreenSpaceReflectionResolutionParameter_t1804578420 * value)
	{
		___resolution_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolution_11), value);
	}

	inline static int32_t get_offset_of_thickness_12() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___thickness_12)); }
	inline FloatParameter_t1840207740 * get_thickness_12() const { return ___thickness_12; }
	inline FloatParameter_t1840207740 ** get_address_of_thickness_12() { return &___thickness_12; }
	inline void set_thickness_12(FloatParameter_t1840207740 * value)
	{
		___thickness_12 = value;
		Il2CppCodeGenWriteBarrier((&___thickness_12), value);
	}

	inline static int32_t get_offset_of_maximumMarchDistance_13() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___maximumMarchDistance_13)); }
	inline FloatParameter_t1840207740 * get_maximumMarchDistance_13() const { return ___maximumMarchDistance_13; }
	inline FloatParameter_t1840207740 ** get_address_of_maximumMarchDistance_13() { return &___maximumMarchDistance_13; }
	inline void set_maximumMarchDistance_13(FloatParameter_t1840207740 * value)
	{
		___maximumMarchDistance_13 = value;
		Il2CppCodeGenWriteBarrier((&___maximumMarchDistance_13), value);
	}

	inline static int32_t get_offset_of_distanceFade_14() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___distanceFade_14)); }
	inline FloatParameter_t1840207740 * get_distanceFade_14() const { return ___distanceFade_14; }
	inline FloatParameter_t1840207740 ** get_address_of_distanceFade_14() { return &___distanceFade_14; }
	inline void set_distanceFade_14(FloatParameter_t1840207740 * value)
	{
		___distanceFade_14 = value;
		Il2CppCodeGenWriteBarrier((&___distanceFade_14), value);
	}

	inline static int32_t get_offset_of_vignette_15() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_t3117296337, ___vignette_15)); }
	inline FloatParameter_t1840207740 * get_vignette_15() const { return ___vignette_15; }
	inline FloatParameter_t1840207740 ** get_address_of_vignette_15() { return &___vignette_15; }
	inline void set_vignette_15(FloatParameter_t1840207740 * value)
	{
		___vignette_15 = value;
		Il2CppCodeGenWriteBarrier((&___vignette_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONS_T3117296337_H
#ifndef VIGNETTE_T2084058635_H
#define VIGNETTE_T2084058635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Vignette
struct  Vignette_t2084058635  : public PostProcessEffectSettings_t1672565614
{
public:
	// UnityEngine.Rendering.PostProcessing.VignetteModeParameter UnityEngine.Rendering.PostProcessing.Vignette::mode
	VignetteModeParameter_t1229959487 * ___mode_9;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.Vignette::color
	ColorParameter_t2998827320 * ___color_10;
	// UnityEngine.Rendering.PostProcessing.Vector2Parameter UnityEngine.Rendering.PostProcessing.Vignette::center
	Vector2Parameter_t1794608574 * ___center_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::intensity
	FloatParameter_t1840207740 * ___intensity_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::smoothness
	FloatParameter_t1840207740 * ___smoothness_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::roundness
	FloatParameter_t1840207740 * ___roundness_14;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Vignette::rounded
	BoolParameter_t2299103272 * ___rounded_15;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.Vignette::mask
	TextureParameter_t4267400415 * ___mask_16;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::opacity
	FloatParameter_t1840207740 * ___opacity_17;

public:
	inline static int32_t get_offset_of_mode_9() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___mode_9)); }
	inline VignetteModeParameter_t1229959487 * get_mode_9() const { return ___mode_9; }
	inline VignetteModeParameter_t1229959487 ** get_address_of_mode_9() { return &___mode_9; }
	inline void set_mode_9(VignetteModeParameter_t1229959487 * value)
	{
		___mode_9 = value;
		Il2CppCodeGenWriteBarrier((&___mode_9), value);
	}

	inline static int32_t get_offset_of_color_10() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___color_10)); }
	inline ColorParameter_t2998827320 * get_color_10() const { return ___color_10; }
	inline ColorParameter_t2998827320 ** get_address_of_color_10() { return &___color_10; }
	inline void set_color_10(ColorParameter_t2998827320 * value)
	{
		___color_10 = value;
		Il2CppCodeGenWriteBarrier((&___color_10), value);
	}

	inline static int32_t get_offset_of_center_11() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___center_11)); }
	inline Vector2Parameter_t1794608574 * get_center_11() const { return ___center_11; }
	inline Vector2Parameter_t1794608574 ** get_address_of_center_11() { return &___center_11; }
	inline void set_center_11(Vector2Parameter_t1794608574 * value)
	{
		___center_11 = value;
		Il2CppCodeGenWriteBarrier((&___center_11), value);
	}

	inline static int32_t get_offset_of_intensity_12() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___intensity_12)); }
	inline FloatParameter_t1840207740 * get_intensity_12() const { return ___intensity_12; }
	inline FloatParameter_t1840207740 ** get_address_of_intensity_12() { return &___intensity_12; }
	inline void set_intensity_12(FloatParameter_t1840207740 * value)
	{
		___intensity_12 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_12), value);
	}

	inline static int32_t get_offset_of_smoothness_13() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___smoothness_13)); }
	inline FloatParameter_t1840207740 * get_smoothness_13() const { return ___smoothness_13; }
	inline FloatParameter_t1840207740 ** get_address_of_smoothness_13() { return &___smoothness_13; }
	inline void set_smoothness_13(FloatParameter_t1840207740 * value)
	{
		___smoothness_13 = value;
		Il2CppCodeGenWriteBarrier((&___smoothness_13), value);
	}

	inline static int32_t get_offset_of_roundness_14() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___roundness_14)); }
	inline FloatParameter_t1840207740 * get_roundness_14() const { return ___roundness_14; }
	inline FloatParameter_t1840207740 ** get_address_of_roundness_14() { return &___roundness_14; }
	inline void set_roundness_14(FloatParameter_t1840207740 * value)
	{
		___roundness_14 = value;
		Il2CppCodeGenWriteBarrier((&___roundness_14), value);
	}

	inline static int32_t get_offset_of_rounded_15() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___rounded_15)); }
	inline BoolParameter_t2299103272 * get_rounded_15() const { return ___rounded_15; }
	inline BoolParameter_t2299103272 ** get_address_of_rounded_15() { return &___rounded_15; }
	inline void set_rounded_15(BoolParameter_t2299103272 * value)
	{
		___rounded_15 = value;
		Il2CppCodeGenWriteBarrier((&___rounded_15), value);
	}

	inline static int32_t get_offset_of_mask_16() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___mask_16)); }
	inline TextureParameter_t4267400415 * get_mask_16() const { return ___mask_16; }
	inline TextureParameter_t4267400415 ** get_address_of_mask_16() { return &___mask_16; }
	inline void set_mask_16(TextureParameter_t4267400415 * value)
	{
		___mask_16 = value;
		Il2CppCodeGenWriteBarrier((&___mask_16), value);
	}

	inline static int32_t get_offset_of_opacity_17() { return static_cast<int32_t>(offsetof(Vignette_t2084058635, ___opacity_17)); }
	inline FloatParameter_t1840207740 * get_opacity_17() const { return ___opacity_17; }
	inline FloatParameter_t1840207740 ** get_address_of_opacity_17() { return &___opacity_17; }
	inline void set_opacity_17(FloatParameter_t1840207740 * value)
	{
		___opacity_17 = value;
		Il2CppCodeGenWriteBarrier((&___opacity_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTE_T2084058635_H
#ifndef UNITYUTIL_T103543446_H
#define UNITYUTIL_T103543446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.UnityUtil
struct  UnityUtil_t103543446  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> UnityEngine.Purchasing.Extension.UnityUtil::pauseListeners
	List_1_t1741830302 * ___pauseListeners_7;

public:
	inline static int32_t get_offset_of_pauseListeners_7() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446, ___pauseListeners_7)); }
	inline List_1_t1741830302 * get_pauseListeners_7() const { return ___pauseListeners_7; }
	inline List_1_t1741830302 ** get_address_of_pauseListeners_7() { return &___pauseListeners_7; }
	inline void set_pauseListeners_7(List_1_t1741830302 * value)
	{
		___pauseListeners_7 = value;
		Il2CppCodeGenWriteBarrier((&___pauseListeners_7), value);
	}
};

struct UnityUtil_t103543446_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Action> UnityEngine.Purchasing.Extension.UnityUtil::s_Callbacks
	List_1_t2736452219 * ___s_Callbacks_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.Purchasing.Extension.UnityUtil::s_CallbacksPending
	bool ___s_CallbacksPending_5;
	// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform> UnityEngine.Purchasing.Extension.UnityUtil::s_PcControlledPlatforms
	List_1_t1336965349 * ___s_PcControlledPlatforms_6;

public:
	inline static int32_t get_offset_of_s_Callbacks_4() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446_StaticFields, ___s_Callbacks_4)); }
	inline List_1_t2736452219 * get_s_Callbacks_4() const { return ___s_Callbacks_4; }
	inline List_1_t2736452219 ** get_address_of_s_Callbacks_4() { return &___s_Callbacks_4; }
	inline void set_s_Callbacks_4(List_1_t2736452219 * value)
	{
		___s_Callbacks_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Callbacks_4), value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_5() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446_StaticFields, ___s_CallbacksPending_5)); }
	inline bool get_s_CallbacksPending_5() const { return ___s_CallbacksPending_5; }
	inline bool* get_address_of_s_CallbacksPending_5() { return &___s_CallbacksPending_5; }
	inline void set_s_CallbacksPending_5(bool value)
	{
		___s_CallbacksPending_5 = value;
	}

	inline static int32_t get_offset_of_s_PcControlledPlatforms_6() { return static_cast<int32_t>(offsetof(UnityUtil_t103543446_StaticFields, ___s_PcControlledPlatforms_6)); }
	inline List_1_t1336965349 * get_s_PcControlledPlatforms_6() const { return ___s_PcControlledPlatforms_6; }
	inline List_1_t1336965349 ** get_address_of_s_PcControlledPlatforms_6() { return &___s_PcControlledPlatforms_6; }
	inline void set_s_PcControlledPlatforms_6(List_1_t1336965349 * value)
	{
		___s_PcControlledPlatforms_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_PcControlledPlatforms_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUTIL_T103543446_H
#ifndef LIFECYCLENOTIFIER_T3201457069_H
#define LIFECYCLENOTIFIER_T3201457069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier
struct  LifecycleNotifier_t3201457069  : public MonoBehaviour_t3962482529
{
public:
	// System.Action UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier::OnDestroyCallback
	Action_t1264377477 * ___OnDestroyCallback_4;

public:
	inline static int32_t get_offset_of_OnDestroyCallback_4() { return static_cast<int32_t>(offsetof(LifecycleNotifier_t3201457069, ___OnDestroyCallback_4)); }
	inline Action_t1264377477 * get_OnDestroyCallback_4() const { return ___OnDestroyCallback_4; }
	inline Action_t1264377477 ** get_address_of_OnDestroyCallback_4() { return &___OnDestroyCallback_4; }
	inline void set_OnDestroyCallback_4(Action_t1264377477 * value)
	{
		___OnDestroyCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyCallback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFECYCLENOTIFIER_T3201457069_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (FakeTransactionHistoryExtensions_t1765525329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (UnifiedReceipt_t1348780434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4102[1] = 
{
	UnifiedReceipt_t1348780434::get_offset_of_Payload_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (FakeMicrosoftExtensions_t4237934947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (WinRTStore_t2015085940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4106[5] = 
{
	WinRTStore_t2015085940::get_offset_of_win8_0(),
	WinRTStore_t2015085940::get_offset_of_callback_1(),
	WinRTStore_t2015085940::get_offset_of_util_2(),
	WinRTStore_t2015085940::get_offset_of_logger_3(),
	WinRTStore_t2015085940::get_offset_of_m_CanReceivePurchases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { sizeof (U3CU3Ec_t3746618910), -1, sizeof(U3CU3Ec_t3746618910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4107[3] = 
{
	U3CU3Ec_t3746618910_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3746618910_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t3746618910_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (TizenStoreImpl_t2691530403), -1, sizeof(TizenStoreImpl_t2691530403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4109[2] = 
{
	TizenStoreImpl_t2691530403_StaticFields::get_offset_of_instance_21(),
	TizenStoreImpl_t2691530403::get_offset_of_m_Native_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (FakeTizenStoreConfiguration_t714964994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { sizeof (FacebookStoreImpl_t2480281949), -1, sizeof(FacebookStoreImpl_t2480281949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4111[3] = 
{
	FacebookStoreImpl_t2480281949::get_offset_of_m_Native_21(),
	FacebookStoreImpl_t2480281949_StaticFields::get_offset_of_util_22(),
	FacebookStoreImpl_t2480281949_StaticFields::get_offset_of_instance_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (U3CU3Ec__DisplayClass6_0_t1261164119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4112[4] = 
{
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass6_0_t1261164119::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (FakeStoreUIMode_t680685637)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4113[4] = 
{
	FakeStoreUIMode_t680685637::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (FakeStore_t3710170489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4114[5] = 
{
	FakeStore_t3710170489::get_offset_of_m_Biller_21(),
	FakeStore_t3710170489::get_offset_of_m_PurchasedProducts_22(),
	FakeStore_t3710170489::get_offset_of_purchaseCalled_23(),
	FakeStore_t3710170489::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_24(),
	FakeStore_t3710170489::get_offset_of_UIMode_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { sizeof (DialogType_t918222323)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4115[3] = 
{
	DialogType_t918222323::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (U3CU3Ec__DisplayClass13_0_t1153436473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4116[2] = 
{
	U3CU3Ec__DisplayClass13_0_t1153436473::get_offset_of_products_0(),
	U3CU3Ec__DisplayClass13_0_t1153436473::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (U3CU3Ec__DisplayClass15_0_t771099449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4117[2] = 
{
	U3CU3Ec__DisplayClass15_0_t771099449::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass15_0_t771099449::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { sizeof (UIFakeStore_t4139165440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4119[6] = 
{
	UIFakeStore_t4139165440::get_offset_of_m_CurrentDialog_26(),
	UIFakeStore_t4139165440::get_offset_of_m_LastSelectedDropdownIndex_27(),
	UIFakeStore_t4139165440::get_offset_of_UIFakeStoreCanvasPrefab_28(),
	UIFakeStore_t4139165440::get_offset_of_m_Canvas_29(),
	UIFakeStore_t4139165440::get_offset_of_m_EventSystem_30(),
	UIFakeStore_t4139165440::get_offset_of_m_ParentGameObjectPath_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (DialogRequest_t599015159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4120[5] = 
{
	DialogRequest_t599015159::get_offset_of_QueryText_0(),
	DialogRequest_t599015159::get_offset_of_OkayButtonText_1(),
	DialogRequest_t599015159::get_offset_of_CancelButtonText_2(),
	DialogRequest_t599015159::get_offset_of_Options_3(),
	DialogRequest_t599015159::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { sizeof (LifecycleNotifier_t3201457069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4121[1] = 
{
	LifecycleNotifier_t3201457069::get_offset_of_OnDestroyCallback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4122[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (U3CU3Ec_t126346862), -1, sizeof(U3CU3Ec_t126346862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4123[2] = 
{
	U3CU3Ec_t126346862_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t126346862_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (FileReference_t2849312398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4124[2] = 
{
	FileReference_t2849312398::get_offset_of_m_FilePath_0(),
	FileReference_t2849312398::get_offset_of_m_Logger_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (ProductDefinitionExtensions_t3868030593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (UnityUtil_t103543446), -1, sizeof(UnityUtil_t103543446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4126[4] = 
{
	UnityUtil_t103543446_StaticFields::get_offset_of_s_Callbacks_4(),
	UnityUtil_t103543446_StaticFields::get_offset_of_s_CallbacksPending_5(),
	UnityUtil_t103543446_StaticFields::get_offset_of_s_PcControlledPlatforms_6(),
	UnityUtil_t103543446::get_offset_of_pauseListeners_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (U3CDelayedCoroutineU3Ed__49_t3667477287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4127[5] = 
{
	U3CDelayedCoroutineU3Ed__49_t3667477287::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedCoroutineU3Ed__49_t3667477287::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedCoroutineU3Ed__49_t3667477287::get_offset_of_coroutine_2(),
	U3CDelayedCoroutineU3Ed__49_t3667477287::get_offset_of_delay_3(),
	U3CDelayedCoroutineU3Ed__49_t3667477287::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255371), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4128[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (__StaticArrayInitTypeSizeU3D368_t2501028641)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D368_t2501028641 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { sizeof (U3CModuleU3E_t692745575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { sizeof (U3CModuleU3E_t692745576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { sizeof (DisplayNameAttribute_t3139068674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4132[1] = 
{
	DisplayNameAttribute_t3139068674::get_offset_of_displayName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { sizeof (MinAttribute_t1996849270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4133[1] = 
{
	MinAttribute_t1996849270::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { sizeof (MinMaxAttribute_t2820105777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4134[2] = 
{
	MinMaxAttribute_t2820105777::get_offset_of_min_0(),
	MinMaxAttribute_t2820105777::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (PostProcessAttribute_t2074534414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4135[5] = 
{
	PostProcessAttribute_t2074534414::get_offset_of_renderer_0(),
	PostProcessAttribute_t2074534414::get_offset_of_eventType_1(),
	PostProcessAttribute_t2074534414::get_offset_of_menuItem_2(),
	PostProcessAttribute_t2074534414::get_offset_of_allowInSceneView_3(),
	PostProcessAttribute_t2074534414::get_offset_of_builtinEffect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (TrackballAttribute_t1878300430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4136[1] = 
{
	TrackballAttribute_t1878300430::get_offset_of_mode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (Mode_t2795405020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4137[5] = 
{
	Mode_t2795405020::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (AmbientOcclusionMode_t1066043822)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4138[3] = 
{
	AmbientOcclusionMode_t1066043822::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (AmbientOcclusionQuality_t3249644899)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4139[6] = 
{
	AmbientOcclusionQuality_t3249644899::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (AmbientOcclusionModeParameter_t3592449485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (AmbientOcclusionQualityParameter_t3820917191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { sizeof (AmbientOcclusion_t1140100160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4142[11] = 
{
	AmbientOcclusion_t1140100160::get_offset_of_mode_9(),
	AmbientOcclusion_t1140100160::get_offset_of_intensity_10(),
	AmbientOcclusion_t1140100160::get_offset_of_color_11(),
	AmbientOcclusion_t1140100160::get_offset_of_ambientOnly_12(),
	AmbientOcclusion_t1140100160::get_offset_of_noiseFilterTolerance_13(),
	AmbientOcclusion_t1140100160::get_offset_of_blurTolerance_14(),
	AmbientOcclusion_t1140100160::get_offset_of_upsampleTolerance_15(),
	AmbientOcclusion_t1140100160::get_offset_of_thicknessModifier_16(),
	AmbientOcclusion_t1140100160::get_offset_of_directLightingStrength_17(),
	AmbientOcclusion_t1140100160::get_offset_of_radius_18(),
	AmbientOcclusion_t1140100160::get_offset_of_quality_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { sizeof (AmbientOcclusionRenderer_t1110100638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4144[1] = 
{
	AmbientOcclusionRenderer_t1110100638::get_offset_of_m_Methods_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { sizeof (EyeAdaptation_t3315401890)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4145[3] = 
{
	EyeAdaptation_t3315401890::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { sizeof (EyeAdaptationParameter_t2946234496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { sizeof (AutoExposure_t2470830169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4147[7] = 
{
	AutoExposure_t2470830169::get_offset_of_filtering_9(),
	AutoExposure_t2470830169::get_offset_of_minLuminance_10(),
	AutoExposure_t2470830169::get_offset_of_maxLuminance_11(),
	AutoExposure_t2470830169::get_offset_of_keyValue_12(),
	AutoExposure_t2470830169::get_offset_of_eyeAdaptation_13(),
	AutoExposure_t2470830169::get_offset_of_speedUp_14(),
	AutoExposure_t2470830169::get_offset_of_speedDown_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { sizeof (AutoExposureRenderer_t90372877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4148[3] = 
{
	AutoExposureRenderer_t90372877::get_offset_of_m_AutoExposurePool_2(),
	AutoExposureRenderer_t90372877::get_offset_of_m_AutoExposurePingPong_3(),
	AutoExposureRenderer_t90372877::get_offset_of_m_CurrentAutoExposure_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { sizeof (Bloom_t2742718173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4149[10] = 
{
	Bloom_t2742718173::get_offset_of_intensity_9(),
	Bloom_t2742718173::get_offset_of_threshold_10(),
	Bloom_t2742718173::get_offset_of_softKnee_11(),
	Bloom_t2742718173::get_offset_of_clamp_12(),
	Bloom_t2742718173::get_offset_of_diffusion_13(),
	Bloom_t2742718173::get_offset_of_anamorphicRatio_14(),
	Bloom_t2742718173::get_offset_of_color_15(),
	Bloom_t2742718173::get_offset_of_fastMode_16(),
	Bloom_t2742718173::get_offset_of_dirtTexture_17(),
	Bloom_t2742718173::get_offset_of_dirtIntensity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (BloomRenderer_t4129140504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4150[1] = 
{
	BloomRenderer_t4129140504::get_offset_of_m_Pyramid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (Level_t3444658929)+ sizeof (RuntimeObject), sizeof(Level_t3444658929 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4151[2] = 
{
	Level_t3444658929::get_offset_of_down_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Level_t3444658929::get_offset_of_up_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (ChromaticAberration_t2835309264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4152[3] = 
{
	ChromaticAberration_t2835309264::get_offset_of_spectralLut_9(),
	ChromaticAberration_t2835309264::get_offset_of_intensity_10(),
	ChromaticAberration_t2835309264::get_offset_of_fastMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (ChromaticAberrationRenderer_t2160044044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4153[1] = 
{
	ChromaticAberrationRenderer_t2160044044::get_offset_of_m_InternalSpectralLut_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (GradingMode_t3858801194)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4154[4] = 
{
	GradingMode_t3858801194::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { sizeof (Tonemapper_t3044700181)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4155[5] = 
{
	Tonemapper_t3044700181::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (GradingModeParameter_t3659529292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { sizeof (TonemapperParameter_t2646255172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (ColorGrading_t1956993830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4158[39] = 
{
	ColorGrading_t1956993830::get_offset_of_gradingMode_9(),
	ColorGrading_t1956993830::get_offset_of_externalLut_10(),
	ColorGrading_t1956993830::get_offset_of_tonemapper_11(),
	ColorGrading_t1956993830::get_offset_of_toneCurveToeStrength_12(),
	ColorGrading_t1956993830::get_offset_of_toneCurveToeLength_13(),
	ColorGrading_t1956993830::get_offset_of_toneCurveShoulderStrength_14(),
	ColorGrading_t1956993830::get_offset_of_toneCurveShoulderLength_15(),
	ColorGrading_t1956993830::get_offset_of_toneCurveShoulderAngle_16(),
	ColorGrading_t1956993830::get_offset_of_toneCurveGamma_17(),
	ColorGrading_t1956993830::get_offset_of_ldrLut_18(),
	ColorGrading_t1956993830::get_offset_of_ldrLutContribution_19(),
	ColorGrading_t1956993830::get_offset_of_temperature_20(),
	ColorGrading_t1956993830::get_offset_of_tint_21(),
	ColorGrading_t1956993830::get_offset_of_colorFilter_22(),
	ColorGrading_t1956993830::get_offset_of_hueShift_23(),
	ColorGrading_t1956993830::get_offset_of_saturation_24(),
	ColorGrading_t1956993830::get_offset_of_brightness_25(),
	ColorGrading_t1956993830::get_offset_of_postExposure_26(),
	ColorGrading_t1956993830::get_offset_of_contrast_27(),
	ColorGrading_t1956993830::get_offset_of_mixerRedOutRedIn_28(),
	ColorGrading_t1956993830::get_offset_of_mixerRedOutGreenIn_29(),
	ColorGrading_t1956993830::get_offset_of_mixerRedOutBlueIn_30(),
	ColorGrading_t1956993830::get_offset_of_mixerGreenOutRedIn_31(),
	ColorGrading_t1956993830::get_offset_of_mixerGreenOutGreenIn_32(),
	ColorGrading_t1956993830::get_offset_of_mixerGreenOutBlueIn_33(),
	ColorGrading_t1956993830::get_offset_of_mixerBlueOutRedIn_34(),
	ColorGrading_t1956993830::get_offset_of_mixerBlueOutGreenIn_35(),
	ColorGrading_t1956993830::get_offset_of_mixerBlueOutBlueIn_36(),
	ColorGrading_t1956993830::get_offset_of_lift_37(),
	ColorGrading_t1956993830::get_offset_of_gamma_38(),
	ColorGrading_t1956993830::get_offset_of_gain_39(),
	ColorGrading_t1956993830::get_offset_of_masterCurve_40(),
	ColorGrading_t1956993830::get_offset_of_redCurve_41(),
	ColorGrading_t1956993830::get_offset_of_greenCurve_42(),
	ColorGrading_t1956993830::get_offset_of_blueCurve_43(),
	ColorGrading_t1956993830::get_offset_of_hueVsHueCurve_44(),
	ColorGrading_t1956993830::get_offset_of_hueVsSatCurve_45(),
	ColorGrading_t1956993830::get_offset_of_satVsSatCurve_46(),
	ColorGrading_t1956993830::get_offset_of_lumVsSatCurve_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (ColorGradingRenderer_t2114992433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4159[5] = 
{
	ColorGradingRenderer_t2114992433::get_offset_of_m_GradingCurves_2(),
	ColorGradingRenderer_t2114992433::get_offset_of_m_Pixels_3(),
	ColorGradingRenderer_t2114992433::get_offset_of_m_InternalLdrLut_4(),
	ColorGradingRenderer_t2114992433::get_offset_of_m_InternalLogLut_5(),
	ColorGradingRenderer_t2114992433::get_offset_of_m_HableCurve_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (KernelSize_t1641471414)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4160[5] = 
{
	KernelSize_t1641471414::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (KernelSizeParameter_t3218545409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (DepthOfField_t1057712103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4162[4] = 
{
	DepthOfField_t1057712103::get_offset_of_focusDistance_9(),
	DepthOfField_t1057712103::get_offset_of_aperture_10(),
	DepthOfField_t1057712103::get_offset_of_focalLength_11(),
	DepthOfField_t1057712103::get_offset_of_kernelSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (DepthOfFieldRenderer_t480065934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4163[2] = 
{
	DepthOfFieldRenderer_t480065934::get_offset_of_m_CoCHistoryTextures_2(),
	DepthOfFieldRenderer_t480065934::get_offset_of_m_HistoryPingPong_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (Dithering_t544635223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4164[1] = 
{
	Dithering_t544635223::get_offset_of_m_NoiseTextureIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (FastApproximateAntialiasing_t3757489215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4165[2] = 
{
	FastApproximateAntialiasing_t3757489215::get_offset_of_fastMode_0(),
	FastApproximateAntialiasing_t3757489215::get_offset_of_keepAlpha_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { sizeof (Fog_t2420217198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4166[2] = 
{
	Fog_t2420217198::get_offset_of_enabled_0(),
	Fog_t2420217198::get_offset_of_excludeSkybox_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { sizeof (Grain_t3181688601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4167[4] = 
{
	Grain_t3181688601::get_offset_of_colored_9(),
	Grain_t3181688601::get_offset_of_intensity_10(),
	Grain_t3181688601::get_offset_of_size_11(),
	Grain_t3181688601::get_offset_of_lumContrib_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (GrainRenderer_t2133570186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4168[2] = 
{
	GrainRenderer_t2133570186::get_offset_of_m_GrainLookupRT_2(),
	GrainRenderer_t2133570186::get_offset_of_m_SampleIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (LensDistortion_t708814765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4169[6] = 
{
	LensDistortion_t708814765::get_offset_of_intensity_9(),
	LensDistortion_t708814765::get_offset_of_intensityX_10(),
	LensDistortion_t708814765::get_offset_of_intensityY_11(),
	LensDistortion_t708814765::get_offset_of_centerX_12(),
	LensDistortion_t708814765::get_offset_of_centerY_13(),
	LensDistortion_t708814765::get_offset_of_scale_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { sizeof (LensDistortionRenderer_t259903296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { sizeof (MotionBlur_t2809166275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4171[2] = 
{
	MotionBlur_t2809166275::get_offset_of_shutterAngle_9(),
	MotionBlur_t2809166275::get_offset_of_sampleCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { sizeof (MotionBlurRenderer_t1665358776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { sizeof (MultiScaleVO_t1807722745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4173[10] = 
{
	MultiScaleVO_t1807722745::get_offset_of_m_SampleThickness_0(),
	MultiScaleVO_t1807722745::get_offset_of_m_InvThicknessTable_1(),
	MultiScaleVO_t1807722745::get_offset_of_m_SampleWeightTable_2(),
	MultiScaleVO_t1807722745::get_offset_of_m_Widths_3(),
	MultiScaleVO_t1807722745::get_offset_of_m_Heights_4(),
	MultiScaleVO_t1807722745::get_offset_of_m_Settings_5(),
	MultiScaleVO_t1807722745::get_offset_of_m_PropertySheet_6(),
	MultiScaleVO_t1807722745::get_offset_of_m_Resources_7(),
	MultiScaleVO_t1807722745::get_offset_of_m_AmbientOnlyAO_8(),
	MultiScaleVO_t1807722745::get_offset_of_m_MRT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (MipLevel_t1715308661)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4174[8] = 
{
	MipLevel_t1715308661::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (ScalableAO_t1980962979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4175[5] = 
{
	ScalableAO_t1980962979::get_offset_of_m_Result_0(),
	ScalableAO_t1980962979::get_offset_of_m_PropertySheet_1(),
	ScalableAO_t1980962979::get_offset_of_m_Settings_2(),
	ScalableAO_t1980962979::get_offset_of_m_MRT_3(),
	ScalableAO_t1980962979::get_offset_of_m_SampleCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { sizeof (ScreenSpaceReflectionPreset_t2401151656)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4176[9] = 
{
	ScreenSpaceReflectionPreset_t2401151656::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { sizeof (ScreenSpaceReflectionResolution_t3090202209)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4177[4] = 
{
	ScreenSpaceReflectionResolution_t3090202209::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { sizeof (ScreenSpaceReflectionPresetParameter_t2494457668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (ScreenSpaceReflectionResolutionParameter_t1804578420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (ScreenSpaceReflections_t3117296337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4180[7] = 
{
	ScreenSpaceReflections_t3117296337::get_offset_of_preset_9(),
	ScreenSpaceReflections_t3117296337::get_offset_of_maximumIterationCount_10(),
	ScreenSpaceReflections_t3117296337::get_offset_of_resolution_11(),
	ScreenSpaceReflections_t3117296337::get_offset_of_thickness_12(),
	ScreenSpaceReflections_t3117296337::get_offset_of_maximumMarchDistance_13(),
	ScreenSpaceReflections_t3117296337::get_offset_of_distanceFade_14(),
	ScreenSpaceReflections_t3117296337::get_offset_of_vignette_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (ScreenSpaceReflectionsRenderer_t661283308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4181[4] = 
{
	ScreenSpaceReflectionsRenderer_t661283308::get_offset_of_m_Resolve_2(),
	ScreenSpaceReflectionsRenderer_t661283308::get_offset_of_m_History_3(),
	ScreenSpaceReflectionsRenderer_t661283308::get_offset_of_m_MipIDs_4(),
	ScreenSpaceReflectionsRenderer_t661283308::get_offset_of_m_Presets_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (QualityPreset_t734522687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4182[3] = 
{
	QualityPreset_t734522687::get_offset_of_maximumIterationCount_0(),
	QualityPreset_t734522687::get_offset_of_thickness_1(),
	QualityPreset_t734522687::get_offset_of_downsampling_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (SubpixelMorphologicalAntialiasing_t3102233738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4183[1] = 
{
	SubpixelMorphologicalAntialiasing_t3102233738::get_offset_of_quality_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (Quality_t1883249404)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4184[4] = 
{
	Quality_t1883249404::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (TemporalAntialiasing_t1482226156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4185[14] = 
{
	TemporalAntialiasing_t1482226156::get_offset_of_jitterSpread_0(),
	TemporalAntialiasing_t1482226156::get_offset_of_sharpness_1(),
	TemporalAntialiasing_t1482226156::get_offset_of_stationaryBlending_2(),
	TemporalAntialiasing_t1482226156::get_offset_of_motionBlending_3(),
	TemporalAntialiasing_t1482226156::get_offset_of_jitteredMatrixFunc_4(),
	TemporalAntialiasing_t1482226156::get_offset_of_U3CjitterU3Ek__BackingField_5(),
	TemporalAntialiasing_t1482226156::get_offset_of_m_Mrt_6(),
	TemporalAntialiasing_t1482226156::get_offset_of_m_ResetHistory_7(),
	0,
	TemporalAntialiasing_t1482226156::get_offset_of_U3CsampleIndexU3Ek__BackingField_9(),
	0,
	0,
	TemporalAntialiasing_t1482226156::get_offset_of_m_HistoryTextures_12(),
	TemporalAntialiasing_t1482226156::get_offset_of_m_HistoryPingPong_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (VignetteMode_t1093529744)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4186[3] = 
{
	VignetteMode_t1093529744::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (VignetteModeParameter_t1229959487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (Vignette_t2084058635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4188[9] = 
{
	Vignette_t2084058635::get_offset_of_mode_9(),
	Vignette_t2084058635::get_offset_of_color_10(),
	Vignette_t2084058635::get_offset_of_center_11(),
	Vignette_t2084058635::get_offset_of_intensity_12(),
	Vignette_t2084058635::get_offset_of_smoothness_13(),
	Vignette_t2084058635::get_offset_of_roundness_14(),
	Vignette_t2084058635::get_offset_of_rounded_15(),
	Vignette_t2084058635::get_offset_of_mask_16(),
	Vignette_t2084058635::get_offset_of_opacity_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (VignetteRenderer_t4277974699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { sizeof (HistogramMonitor_t3488597019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4190[7] = 
{
	HistogramMonitor_t3488597019::get_offset_of_width_2(),
	HistogramMonitor_t3488597019::get_offset_of_height_3(),
	HistogramMonitor_t3488597019::get_offset_of_channel_4(),
	HistogramMonitor_t3488597019::get_offset_of_m_Data_5(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (Channel_t1999646469)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4191[5] = 
{
	Channel_t1999646469::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (LightMeterMonitor_t1816308400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4192[3] = 
{
	LightMeterMonitor_t1816308400::get_offset_of_width_2(),
	LightMeterMonitor_t1816308400::get_offset_of_height_3(),
	LightMeterMonitor_t1816308400::get_offset_of_showCurves_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (MonitorType_t3583017366)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4193[5] = 
{
	MonitorType_t3583017366::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (Monitor_t1754509597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4194[2] = 
{
	Monitor_t1754509597::get_offset_of_U3CoutputU3Ek__BackingField_0(),
	Monitor_t1754509597::get_offset_of_requested_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195 = { sizeof (VectorscopeMonitor_t2083911122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4195[5] = 
{
	VectorscopeMonitor_t2083911122::get_offset_of_size_2(),
	VectorscopeMonitor_t2083911122::get_offset_of_exposure_3(),
	VectorscopeMonitor_t2083911122::get_offset_of_m_Data_4(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196 = { sizeof (WaveformMonitor_t2029591948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4196[6] = 
{
	WaveformMonitor_t2029591948::get_offset_of_exposure_2(),
	WaveformMonitor_t2029591948::get_offset_of_height_3(),
	WaveformMonitor_t2029591948::get_offset_of_m_Data_4(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197 = { sizeof (ParameterOverride_t3061054201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4197[1] = 
{
	ParameterOverride_t3061054201::get_offset_of_overrideState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4198[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199 = { sizeof (FloatParameter_t1840207740), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
