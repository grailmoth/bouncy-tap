﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t1264377477;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Action`1<System.UInt32>
struct Action_1_t2732529573;
// System.Action`1<UnityEngine.Color>
struct Action_1_t2728153919;
// System.Action`1<UnityEngine.Experimental.Rendering.DebugUI/Panel>
struct Action_1_t1007713453;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2328697118;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3894781059;
// System.Action`1<UnityEngine.Vector4>
struct Action_1_t3491496532;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.Boolean>,System.Boolean>
struct Action_2_t4273289399;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.Int32>,System.Int32>
struct Action_2_t319808071;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.Single>,System.Single>
struct Action_2_t4120761267;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.UInt32>,System.UInt32>
struct Action_2_t2314478627;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Color>,UnityEngine.Color>
struct Action_2_t481218683;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Vector2>,UnityEngine.Vector2>
struct Action_2_t597031455;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Vector3>,UnityEngine.Vector3>
struct Action_2_t2424680171;
// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Vector4>,UnityEngine.Vector4>
struct Action_2_t1236701319;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t3280968592;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume>>
struct Dictionary_2_t3343844990;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.RenderTextureFormat>
struct Dictionary_2_t4146031392;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Texture2D>
struct Dictionary_2_t2729159516;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessAttribute>
struct Dictionary_2_t223914182;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle>
struct Dictionary_2_t1614383438;
// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.MonitorType,UnityEngine.Rendering.PostProcessing.Monitor>
struct Dictionary_2_t679227455;
// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.PostProcessEvent,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef>>
struct Dictionary_2_t638195886;
// System.Collections.Generic.Dictionary`2<UnityEngine.Shader,UnityEngine.Rendering.PostProcessing.PropertySheet>
struct Dictionary_2_t77305525;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1463797649;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.String[]>
struct List_1_t2753864082;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t3245421752;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.DebugUI/Panel>
struct List_1_t2307320600;
// System.Collections.Generic.List`1<UnityEngine.KeyCode[]>
struct List_1_t3695308798;
// System.Collections.Generic.List`1<UnityEngine.RenderTexture>
struct List_1_t3580962175;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessBundle>
struct List_1_t642111116;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer>
struct List_1_t1473134979;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings>
struct List_1_t3144640356;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef>
struct List_1_t27135406;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume>
struct List_1_t160164363;
// System.Collections.Generic.List`1<UnityEngine.Rendering.RenderTargetIdentifier>
struct List_1_t3551259242;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Experimental.Rendering.DebugUI/Panel>
struct ReadOnlyCollection_1_t2047822145;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride>
struct ReadOnlyCollection_1_t4273630488;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// System.Func`1<System.Int32>
struct Func_1_t2380692400;
// System.Func`1<System.Object>
struct Func_1_t2509852811;
// System.Func`1<System.Single>
struct Func_1_t827013421;
// System.Func`1<System.UInt32>
struct Func_1_t1989808625;
// System.Func`1<UnityEngine.Color>
struct Func_1_t1985432971;
// System.Func`1<UnityEngine.Vector2>
struct Func_1_t1585976170;
// System.Func`1<UnityEngine.Vector3>
struct Func_1_t3152060111;
// System.Func`1<UnityEngine.Vector4>
struct Func_1_t2748775584;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle>,UnityEngine.Rendering.PostProcessing.PostProcessBundle>
struct Func_2_t569305667;
// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>>
struct Func_2_t779105388;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t1761491126;
// System.Func`2<System.Reflection.FieldInfo,System.Int32>
struct Func_2_t320181618;
// System.Func`2<System.String,UnityEngine.GUIContent>
struct Func_2_t855502256;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t561252955;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Predicate`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings>
struct Predicate_1_t2497859738;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1709987734;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// UnityEngine.ComputeShader
struct ComputeShader_t317220254;
// UnityEngine.Experimental.Rendering.DebugActionDesc[]
struct DebugActionDescU5BU5D_t255406021;
// UnityEngine.Experimental.Rendering.DebugActionState[]
struct DebugActionStateU5BU5D_t3603604718;
// UnityEngine.Experimental.Rendering.DebugUI/Button
struct Button_t565603744;
// UnityEngine.Experimental.Rendering.DebugUI/EnumField
struct EnumField_t3058254002;
// UnityEngine.Experimental.Rendering.DebugUI/IContainer
struct IContainer_t3624003324;
// UnityEngine.Experimental.Rendering.DebugUI/Panel
struct Panel_t835245858;
// UnityEngine.Experimental.Rendering.DebugUI/Widget
struct Widget_t1336755483;
// UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation[]
struct CopyOperationU5BU5D_t3246644051;
// UnityEngine.Experimental.Rendering.ObjectPool`1<UnityEngine.Rendering.CommandBuffer>
struct ObjectPool_1_t3743844062;
// UnityEngine.Experimental.Rendering.ObservableList`1<UnityEngine.Experimental.Rendering.DebugUI/Widget>
struct ObservableList_1_t535108294;
// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerCanvas
struct DebugUIHandlerCanvas_t1496321634;
// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerPersistentCanvas
struct DebugUIHandlerPersistentCanvas_t4045196280;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t2445521510;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Rendering.PostProcessing.AutoExposure
struct AutoExposure_t2470830169;
// UnityEngine.Rendering.PostProcessing.BoolParameter
struct BoolParameter_t2299103272;
// UnityEngine.Rendering.PostProcessing.Dithering
struct Dithering_t544635223;
// UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing
struct FastApproximateAntialiasing_t3757489215;
// UnityEngine.Rendering.PostProcessing.Fog
struct Fog_t2420217198;
// UnityEngine.Rendering.PostProcessing.HableCurve
struct HableCurve_t1612137718;
// UnityEngine.Rendering.PostProcessing.HableCurve/Segment[]
struct SegmentU5BU5D_t2749102818;
// UnityEngine.Rendering.PostProcessing.HableCurve/Uniforms
struct Uniforms_t165416594;
// UnityEngine.Rendering.PostProcessing.HistogramMonitor
struct HistogramMonitor_t3488597019;
// UnityEngine.Rendering.PostProcessing.LightMeterMonitor
struct LightMeterMonitor_t1816308400;
// UnityEngine.Rendering.PostProcessing.LogHistogram
struct LogHistogram_t1187052756;
// UnityEngine.Rendering.PostProcessing.PostProcessAttribute
struct PostProcessAttribute_t2074534414;
// UnityEngine.Rendering.PostProcessing.PostProcessBundle
struct PostProcessBundle_t3465003670;
// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer
struct PostProcessDebugLayer_t3290441360;
// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings
struct OverlaySettings_t1356898765;
// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer
struct PostProcessEffectRenderer_t1060237;
// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct PostProcessEffectSettings_t1672565614;
// UnityEngine.Rendering.PostProcessing.PostProcessLayer
struct PostProcessLayer_t4264744195;
// UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0
struct U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214;
// UnityEngine.Rendering.PostProcessing.PostProcessProfile
struct PostProcessProfile_t3936051061;
// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext
struct PostProcessRenderContext_t597611190;
// UnityEngine.Rendering.PostProcessing.PostProcessResources
struct PostProcessResources_t1163236733;
// UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders
struct ComputeShaders_t4172110136;
// UnityEngine.Rendering.PostProcessing.PostProcessResources/SMAALuts
struct SMAALuts_t184516107;
// UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders
struct Shaders_t2807171077;
// UnityEngine.Rendering.PostProcessing.PropertySheet
struct PropertySheet_t3821403501;
// UnityEngine.Rendering.PostProcessing.PropertySheetFactory
struct PropertySheetFactory_t1490101248;
// UnityEngine.Rendering.PostProcessing.Spline
struct Spline_t3835237600;
// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing
struct SubpixelMorphologicalAntialiasing_t3102233738;
// UnityEngine.Rendering.PostProcessing.TargetPool
struct TargetPool_t1535233241;
// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing
struct TemporalAntialiasing_t1482226156;
// UnityEngine.Rendering.PostProcessing.VectorscopeMonitor
struct VectorscopeMonitor_t2083911122;
// UnityEngine.Rendering.PostProcessing.WaveformMonitor
struct WaveformMonitor_t2029591948;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef U3CMODULEU3E_T692745577_H
#define U3CMODULEU3E_T692745577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745577 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745577_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef COMMANDBUFFERPOOL_T3037895833_H
#define COMMANDBUFFERPOOL_T3037895833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.CommandBufferPool
struct  CommandBufferPool_t3037895833  : public RuntimeObject
{
public:

public:
};

struct CommandBufferPool_t3037895833_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.ObjectPool`1<UnityEngine.Rendering.CommandBuffer> UnityEngine.Experimental.Rendering.CommandBufferPool::s_BufferPool
	ObjectPool_1_t3743844062 * ___s_BufferPool_0;

public:
	inline static int32_t get_offset_of_s_BufferPool_0() { return static_cast<int32_t>(offsetof(CommandBufferPool_t3037895833_StaticFields, ___s_BufferPool_0)); }
	inline ObjectPool_1_t3743844062 * get_s_BufferPool_0() const { return ___s_BufferPool_0; }
	inline ObjectPool_1_t3743844062 ** get_address_of_s_BufferPool_0() { return &___s_BufferPool_0; }
	inline void set_s_BufferPool_0(ObjectPool_1_t3743844062 * value)
	{
		___s_BufferPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_BufferPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFERPOOL_T3037895833_H
#ifndef DEBUGMANAGER_T4149441816_H
#define DEBUGMANAGER_T4149441816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugManager
struct  DebugManager_t4149441816  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.DebugActionDesc[] UnityEngine.Experimental.Rendering.DebugManager::m_DebugActions
	DebugActionDescU5BU5D_t255406021* ___m_DebugActions_9;
	// UnityEngine.Experimental.Rendering.DebugActionState[] UnityEngine.Experimental.Rendering.DebugManager::m_DebugActionStates
	DebugActionStateU5BU5D_t3603604718* ___m_DebugActionStates_10;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Experimental.Rendering.DebugUI/Panel> UnityEngine.Experimental.Rendering.DebugManager::m_ReadOnlyPanels
	ReadOnlyCollection_1_t2047822145 * ___m_ReadOnlyPanels_12;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.DebugUI/Panel> UnityEngine.Experimental.Rendering.DebugManager::m_Panels
	List_1_t2307320600 * ___m_Panels_13;
	// System.Action`1<System.Boolean> UnityEngine.Experimental.Rendering.DebugManager::onDisplayRuntimeUIChanged
	Action_1_t269755560 * ___onDisplayRuntimeUIChanged_14;
	// System.Action UnityEngine.Experimental.Rendering.DebugManager::onSetDirty
	Action_t1264377477 * ___onSetDirty_15;
	// System.Boolean UnityEngine.Experimental.Rendering.DebugManager::refreshEditorRequested
	bool ___refreshEditorRequested_16;
	// UnityEngine.GameObject UnityEngine.Experimental.Rendering.DebugManager::m_Root
	GameObject_t1113636619 * ___m_Root_17;
	// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerCanvas UnityEngine.Experimental.Rendering.DebugManager::m_RootUICanvas
	DebugUIHandlerCanvas_t1496321634 * ___m_RootUICanvas_18;
	// UnityEngine.GameObject UnityEngine.Experimental.Rendering.DebugManager::m_PersistentRoot
	GameObject_t1113636619 * ___m_PersistentRoot_19;
	// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerPersistentCanvas UnityEngine.Experimental.Rendering.DebugManager::m_RootUIPersistentCanvas
	DebugUIHandlerPersistentCanvas_t4045196280 * ___m_RootUIPersistentCanvas_20;

public:
	inline static int32_t get_offset_of_m_DebugActions_9() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_DebugActions_9)); }
	inline DebugActionDescU5BU5D_t255406021* get_m_DebugActions_9() const { return ___m_DebugActions_9; }
	inline DebugActionDescU5BU5D_t255406021** get_address_of_m_DebugActions_9() { return &___m_DebugActions_9; }
	inline void set_m_DebugActions_9(DebugActionDescU5BU5D_t255406021* value)
	{
		___m_DebugActions_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugActions_9), value);
	}

	inline static int32_t get_offset_of_m_DebugActionStates_10() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_DebugActionStates_10)); }
	inline DebugActionStateU5BU5D_t3603604718* get_m_DebugActionStates_10() const { return ___m_DebugActionStates_10; }
	inline DebugActionStateU5BU5D_t3603604718** get_address_of_m_DebugActionStates_10() { return &___m_DebugActionStates_10; }
	inline void set_m_DebugActionStates_10(DebugActionStateU5BU5D_t3603604718* value)
	{
		___m_DebugActionStates_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugActionStates_10), value);
	}

	inline static int32_t get_offset_of_m_ReadOnlyPanels_12() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_ReadOnlyPanels_12)); }
	inline ReadOnlyCollection_1_t2047822145 * get_m_ReadOnlyPanels_12() const { return ___m_ReadOnlyPanels_12; }
	inline ReadOnlyCollection_1_t2047822145 ** get_address_of_m_ReadOnlyPanels_12() { return &___m_ReadOnlyPanels_12; }
	inline void set_m_ReadOnlyPanels_12(ReadOnlyCollection_1_t2047822145 * value)
	{
		___m_ReadOnlyPanels_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReadOnlyPanels_12), value);
	}

	inline static int32_t get_offset_of_m_Panels_13() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_Panels_13)); }
	inline List_1_t2307320600 * get_m_Panels_13() const { return ___m_Panels_13; }
	inline List_1_t2307320600 ** get_address_of_m_Panels_13() { return &___m_Panels_13; }
	inline void set_m_Panels_13(List_1_t2307320600 * value)
	{
		___m_Panels_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Panels_13), value);
	}

	inline static int32_t get_offset_of_onDisplayRuntimeUIChanged_14() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___onDisplayRuntimeUIChanged_14)); }
	inline Action_1_t269755560 * get_onDisplayRuntimeUIChanged_14() const { return ___onDisplayRuntimeUIChanged_14; }
	inline Action_1_t269755560 ** get_address_of_onDisplayRuntimeUIChanged_14() { return &___onDisplayRuntimeUIChanged_14; }
	inline void set_onDisplayRuntimeUIChanged_14(Action_1_t269755560 * value)
	{
		___onDisplayRuntimeUIChanged_14 = value;
		Il2CppCodeGenWriteBarrier((&___onDisplayRuntimeUIChanged_14), value);
	}

	inline static int32_t get_offset_of_onSetDirty_15() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___onSetDirty_15)); }
	inline Action_t1264377477 * get_onSetDirty_15() const { return ___onSetDirty_15; }
	inline Action_t1264377477 ** get_address_of_onSetDirty_15() { return &___onSetDirty_15; }
	inline void set_onSetDirty_15(Action_t1264377477 * value)
	{
		___onSetDirty_15 = value;
		Il2CppCodeGenWriteBarrier((&___onSetDirty_15), value);
	}

	inline static int32_t get_offset_of_refreshEditorRequested_16() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___refreshEditorRequested_16)); }
	inline bool get_refreshEditorRequested_16() const { return ___refreshEditorRequested_16; }
	inline bool* get_address_of_refreshEditorRequested_16() { return &___refreshEditorRequested_16; }
	inline void set_refreshEditorRequested_16(bool value)
	{
		___refreshEditorRequested_16 = value;
	}

	inline static int32_t get_offset_of_m_Root_17() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_Root_17)); }
	inline GameObject_t1113636619 * get_m_Root_17() const { return ___m_Root_17; }
	inline GameObject_t1113636619 ** get_address_of_m_Root_17() { return &___m_Root_17; }
	inline void set_m_Root_17(GameObject_t1113636619 * value)
	{
		___m_Root_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Root_17), value);
	}

	inline static int32_t get_offset_of_m_RootUICanvas_18() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_RootUICanvas_18)); }
	inline DebugUIHandlerCanvas_t1496321634 * get_m_RootUICanvas_18() const { return ___m_RootUICanvas_18; }
	inline DebugUIHandlerCanvas_t1496321634 ** get_address_of_m_RootUICanvas_18() { return &___m_RootUICanvas_18; }
	inline void set_m_RootUICanvas_18(DebugUIHandlerCanvas_t1496321634 * value)
	{
		___m_RootUICanvas_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_RootUICanvas_18), value);
	}

	inline static int32_t get_offset_of_m_PersistentRoot_19() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_PersistentRoot_19)); }
	inline GameObject_t1113636619 * get_m_PersistentRoot_19() const { return ___m_PersistentRoot_19; }
	inline GameObject_t1113636619 ** get_address_of_m_PersistentRoot_19() { return &___m_PersistentRoot_19; }
	inline void set_m_PersistentRoot_19(GameObject_t1113636619 * value)
	{
		___m_PersistentRoot_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentRoot_19), value);
	}

	inline static int32_t get_offset_of_m_RootUIPersistentCanvas_20() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816, ___m_RootUIPersistentCanvas_20)); }
	inline DebugUIHandlerPersistentCanvas_t4045196280 * get_m_RootUIPersistentCanvas_20() const { return ___m_RootUIPersistentCanvas_20; }
	inline DebugUIHandlerPersistentCanvas_t4045196280 ** get_address_of_m_RootUIPersistentCanvas_20() { return &___m_RootUIPersistentCanvas_20; }
	inline void set_m_RootUIPersistentCanvas_20(DebugUIHandlerPersistentCanvas_t4045196280 * value)
	{
		___m_RootUIPersistentCanvas_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_RootUIPersistentCanvas_20), value);
	}
};

struct DebugManager_t4149441816_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.DebugManager UnityEngine.Experimental.Rendering.DebugManager::s_Instance
	DebugManager_t4149441816 * ___s_Instance_11;
	// System.Action`1<System.Boolean> UnityEngine.Experimental.Rendering.DebugManager::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_21;
	// System.Action UnityEngine.Experimental.Rendering.DebugManager::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_22;

public:
	inline static int32_t get_offset_of_s_Instance_11() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816_StaticFields, ___s_Instance_11)); }
	inline DebugManager_t4149441816 * get_s_Instance_11() const { return ___s_Instance_11; }
	inline DebugManager_t4149441816 ** get_address_of_s_Instance_11() { return &___s_Instance_11; }
	inline void set_s_Instance_11(DebugManager_t4149441816 * value)
	{
		___s_Instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_21() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816_StaticFields, ___U3CU3Ef__amU24cache0_21)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_21() const { return ___U3CU3Ef__amU24cache0_21; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_21() { return &___U3CU3Ef__amU24cache0_21; }
	inline void set_U3CU3Ef__amU24cache0_21(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_22() { return static_cast<int32_t>(offsetof(DebugManager_t4149441816_StaticFields, ___U3CU3Ef__amU24cache1_22)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_22() const { return ___U3CU3Ef__amU24cache1_22; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_22() { return &___U3CU3Ef__amU24cache1_22; }
	inline void set_U3CU3Ef__amU24cache1_22(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGMANAGER_T4149441816_H
#ifndef DEBUGUI_T4282187678_H
#define DEBUGUI_T4282187678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI
struct  DebugUI_t4282187678  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGUI_T4282187678_H
#ifndef GPUCOPY_T16803174_H
#define GPUCOPY_T16803174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.GPUCopy
struct  GPUCopy_t16803174  : public RuntimeObject
{
public:
	// UnityEngine.ComputeShader UnityEngine.Experimental.Rendering.GPUCopy::m_Shader
	ComputeShader_t317220254 * ___m_Shader_0;
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopy::k_SampleKernel_xyzw2x_8
	int32_t ___k_SampleKernel_xyzw2x_8_1;
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopy::k_SampleKernel_xyzw2x_1
	int32_t ___k_SampleKernel_xyzw2x_1_2;

public:
	inline static int32_t get_offset_of_m_Shader_0() { return static_cast<int32_t>(offsetof(GPUCopy_t16803174, ___m_Shader_0)); }
	inline ComputeShader_t317220254 * get_m_Shader_0() const { return ___m_Shader_0; }
	inline ComputeShader_t317220254 ** get_address_of_m_Shader_0() { return &___m_Shader_0; }
	inline void set_m_Shader_0(ComputeShader_t317220254 * value)
	{
		___m_Shader_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Shader_0), value);
	}

	inline static int32_t get_offset_of_k_SampleKernel_xyzw2x_8_1() { return static_cast<int32_t>(offsetof(GPUCopy_t16803174, ___k_SampleKernel_xyzw2x_8_1)); }
	inline int32_t get_k_SampleKernel_xyzw2x_8_1() const { return ___k_SampleKernel_xyzw2x_8_1; }
	inline int32_t* get_address_of_k_SampleKernel_xyzw2x_8_1() { return &___k_SampleKernel_xyzw2x_8_1; }
	inline void set_k_SampleKernel_xyzw2x_8_1(int32_t value)
	{
		___k_SampleKernel_xyzw2x_8_1 = value;
	}

	inline static int32_t get_offset_of_k_SampleKernel_xyzw2x_1_2() { return static_cast<int32_t>(offsetof(GPUCopy_t16803174, ___k_SampleKernel_xyzw2x_1_2)); }
	inline int32_t get_k_SampleKernel_xyzw2x_1_2() const { return ___k_SampleKernel_xyzw2x_1_2; }
	inline int32_t* get_address_of_k_SampleKernel_xyzw2x_1_2() { return &___k_SampleKernel_xyzw2x_1_2; }
	inline void set_k_SampleKernel_xyzw2x_1_2(int32_t value)
	{
		___k_SampleKernel_xyzw2x_1_2 = value;
	}
};

struct GPUCopy_t16803174_StaticFields
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopy::_RectOffset
	int32_t ____RectOffset_3;
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopy::_Result1
	int32_t ____Result1_4;
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopy::_Source4
	int32_t ____Source4_5;

public:
	inline static int32_t get_offset_of__RectOffset_3() { return static_cast<int32_t>(offsetof(GPUCopy_t16803174_StaticFields, ____RectOffset_3)); }
	inline int32_t get__RectOffset_3() const { return ____RectOffset_3; }
	inline int32_t* get_address_of__RectOffset_3() { return &____RectOffset_3; }
	inline void set__RectOffset_3(int32_t value)
	{
		____RectOffset_3 = value;
	}

	inline static int32_t get_offset_of__Result1_4() { return static_cast<int32_t>(offsetof(GPUCopy_t16803174_StaticFields, ____Result1_4)); }
	inline int32_t get__Result1_4() const { return ____Result1_4; }
	inline int32_t* get_address_of__Result1_4() { return &____Result1_4; }
	inline void set__Result1_4(int32_t value)
	{
		____Result1_4 = value;
	}

	inline static int32_t get_offset_of__Source4_5() { return static_cast<int32_t>(offsetof(GPUCopy_t16803174_StaticFields, ____Source4_5)); }
	inline int32_t get__Source4_5() const { return ____Source4_5; }
	inline int32_t* get_address_of__Source4_5() { return &____Source4_5; }
	inline void set__Source4_5(int32_t value)
	{
		____Source4_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUCOPY_T16803174_H
#ifndef MOUSEPOSITIONDEBUG_T947361257_H
#define MOUSEPOSITIONDEBUG_T947361257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.MousePositionDebug
struct  MousePositionDebug_t947361257  : public RuntimeObject
{
public:

public:
};

struct MousePositionDebug_t947361257_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.MousePositionDebug UnityEngine.Experimental.Rendering.MousePositionDebug::s_Instance
	MousePositionDebug_t947361257 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(MousePositionDebug_t947361257_StaticFields, ___s_Instance_0)); }
	inline MousePositionDebug_t947361257 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline MousePositionDebug_t947361257 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(MousePositionDebug_t947361257 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEPOSITIONDEBUG_T947361257_H
#ifndef TEXTUREPADDING_T452921104_H
#define TEXTUREPADDING_T452921104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.TexturePadding
struct  TexturePadding_t452921104  : public RuntimeObject
{
public:
	// UnityEngine.ComputeShader UnityEngine.Experimental.Rendering.TexturePadding::m_CS
	ComputeShader_t317220254 * ___m_CS_2;
	// System.Int32 UnityEngine.Experimental.Rendering.TexturePadding::m_KMainTopRight
	int32_t ___m_KMainTopRight_3;
	// System.Int32 UnityEngine.Experimental.Rendering.TexturePadding::m_KMainTop
	int32_t ___m_KMainTop_4;
	// System.Int32 UnityEngine.Experimental.Rendering.TexturePadding::m_KMainRight
	int32_t ___m_KMainRight_5;

public:
	inline static int32_t get_offset_of_m_CS_2() { return static_cast<int32_t>(offsetof(TexturePadding_t452921104, ___m_CS_2)); }
	inline ComputeShader_t317220254 * get_m_CS_2() const { return ___m_CS_2; }
	inline ComputeShader_t317220254 ** get_address_of_m_CS_2() { return &___m_CS_2; }
	inline void set_m_CS_2(ComputeShader_t317220254 * value)
	{
		___m_CS_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CS_2), value);
	}

	inline static int32_t get_offset_of_m_KMainTopRight_3() { return static_cast<int32_t>(offsetof(TexturePadding_t452921104, ___m_KMainTopRight_3)); }
	inline int32_t get_m_KMainTopRight_3() const { return ___m_KMainTopRight_3; }
	inline int32_t* get_address_of_m_KMainTopRight_3() { return &___m_KMainTopRight_3; }
	inline void set_m_KMainTopRight_3(int32_t value)
	{
		___m_KMainTopRight_3 = value;
	}

	inline static int32_t get_offset_of_m_KMainTop_4() { return static_cast<int32_t>(offsetof(TexturePadding_t452921104, ___m_KMainTop_4)); }
	inline int32_t get_m_KMainTop_4() const { return ___m_KMainTop_4; }
	inline int32_t* get_address_of_m_KMainTop_4() { return &___m_KMainTop_4; }
	inline void set_m_KMainTop_4(int32_t value)
	{
		___m_KMainTop_4 = value;
	}

	inline static int32_t get_offset_of_m_KMainRight_5() { return static_cast<int32_t>(offsetof(TexturePadding_t452921104, ___m_KMainRight_5)); }
	inline int32_t get_m_KMainRight_5() const { return ___m_KMainRight_5; }
	inline int32_t* get_address_of_m_KMainRight_5() { return &___m_KMainRight_5; }
	inline void set_m_KMainRight_5(int32_t value)
	{
		___m_KMainRight_5 = value;
	}
};

struct TexturePadding_t452921104_StaticFields
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.TexturePadding::_RectOffset
	int32_t ____RectOffset_0;
	// System.Int32 UnityEngine.Experimental.Rendering.TexturePadding::_InOutTexture
	int32_t ____InOutTexture_1;

public:
	inline static int32_t get_offset_of__RectOffset_0() { return static_cast<int32_t>(offsetof(TexturePadding_t452921104_StaticFields, ____RectOffset_0)); }
	inline int32_t get__RectOffset_0() const { return ____RectOffset_0; }
	inline int32_t* get_address_of__RectOffset_0() { return &____RectOffset_0; }
	inline void set__RectOffset_0(int32_t value)
	{
		____RectOffset_0 = value;
	}

	inline static int32_t get_offset_of__InOutTexture_1() { return static_cast<int32_t>(offsetof(TexturePadding_t452921104_StaticFields, ____InOutTexture_1)); }
	inline int32_t get__InOutTexture_1() const { return ____InOutTexture_1; }
	inline int32_t* get_address_of__InOutTexture_1() { return &____InOutTexture_1; }
	inline void set__InOutTexture_1(int32_t value)
	{
		____InOutTexture_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPADDING_T452921104_H
#ifndef COLORUTILITIES_T3120880352_H
#define COLORUTILITIES_T3120880352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorUtilities
struct  ColorUtilities_t3120880352  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTILITIES_T3120880352_H
#ifndef HABLECURVE_T1612137718_H
#define HABLECURVE_T1612137718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve
struct  HableCurve_t1612137718  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<whitePoint>k__BackingField
	float ___U3CwhitePointU3Ek__BackingField_0;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<inverseWhitePoint>k__BackingField
	float ___U3CinverseWhitePointU3Ek__BackingField_1;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<x0>k__BackingField
	float ___U3Cx0U3Ek__BackingField_2;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<x1>k__BackingField
	float ___U3Cx1U3Ek__BackingField_3;
	// UnityEngine.Rendering.PostProcessing.HableCurve/Segment[] UnityEngine.Rendering.PostProcessing.HableCurve::segments
	SegmentU5BU5D_t2749102818* ___segments_4;
	// UnityEngine.Rendering.PostProcessing.HableCurve/Uniforms UnityEngine.Rendering.PostProcessing.HableCurve::uniforms
	Uniforms_t165416594 * ___uniforms_5;

public:
	inline static int32_t get_offset_of_U3CwhitePointU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HableCurve_t1612137718, ___U3CwhitePointU3Ek__BackingField_0)); }
	inline float get_U3CwhitePointU3Ek__BackingField_0() const { return ___U3CwhitePointU3Ek__BackingField_0; }
	inline float* get_address_of_U3CwhitePointU3Ek__BackingField_0() { return &___U3CwhitePointU3Ek__BackingField_0; }
	inline void set_U3CwhitePointU3Ek__BackingField_0(float value)
	{
		___U3CwhitePointU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CinverseWhitePointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HableCurve_t1612137718, ___U3CinverseWhitePointU3Ek__BackingField_1)); }
	inline float get_U3CinverseWhitePointU3Ek__BackingField_1() const { return ___U3CinverseWhitePointU3Ek__BackingField_1; }
	inline float* get_address_of_U3CinverseWhitePointU3Ek__BackingField_1() { return &___U3CinverseWhitePointU3Ek__BackingField_1; }
	inline void set_U3CinverseWhitePointU3Ek__BackingField_1(float value)
	{
		___U3CinverseWhitePointU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cx0U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HableCurve_t1612137718, ___U3Cx0U3Ek__BackingField_2)); }
	inline float get_U3Cx0U3Ek__BackingField_2() const { return ___U3Cx0U3Ek__BackingField_2; }
	inline float* get_address_of_U3Cx0U3Ek__BackingField_2() { return &___U3Cx0U3Ek__BackingField_2; }
	inline void set_U3Cx0U3Ek__BackingField_2(float value)
	{
		___U3Cx0U3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cx1U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HableCurve_t1612137718, ___U3Cx1U3Ek__BackingField_3)); }
	inline float get_U3Cx1U3Ek__BackingField_3() const { return ___U3Cx1U3Ek__BackingField_3; }
	inline float* get_address_of_U3Cx1U3Ek__BackingField_3() { return &___U3Cx1U3Ek__BackingField_3; }
	inline void set_U3Cx1U3Ek__BackingField_3(float value)
	{
		___U3Cx1U3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_segments_4() { return static_cast<int32_t>(offsetof(HableCurve_t1612137718, ___segments_4)); }
	inline SegmentU5BU5D_t2749102818* get_segments_4() const { return ___segments_4; }
	inline SegmentU5BU5D_t2749102818** get_address_of_segments_4() { return &___segments_4; }
	inline void set_segments_4(SegmentU5BU5D_t2749102818* value)
	{
		___segments_4 = value;
		Il2CppCodeGenWriteBarrier((&___segments_4), value);
	}

	inline static int32_t get_offset_of_uniforms_5() { return static_cast<int32_t>(offsetof(HableCurve_t1612137718, ___uniforms_5)); }
	inline Uniforms_t165416594 * get_uniforms_5() const { return ___uniforms_5; }
	inline Uniforms_t165416594 ** get_address_of_uniforms_5() { return &___uniforms_5; }
	inline void set_uniforms_5(Uniforms_t165416594 * value)
	{
		___uniforms_5 = value;
		Il2CppCodeGenWriteBarrier((&___uniforms_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HABLECURVE_T1612137718_H
#ifndef SEGMENT_T754368499_H
#define SEGMENT_T754368499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve/Segment
struct  Segment_t754368499  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/Segment::offsetX
	float ___offsetX_0;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/Segment::offsetY
	float ___offsetY_1;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/Segment::scaleX
	float ___scaleX_2;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/Segment::scaleY
	float ___scaleY_3;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/Segment::lnA
	float ___lnA_4;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/Segment::B
	float ___B_5;

public:
	inline static int32_t get_offset_of_offsetX_0() { return static_cast<int32_t>(offsetof(Segment_t754368499, ___offsetX_0)); }
	inline float get_offsetX_0() const { return ___offsetX_0; }
	inline float* get_address_of_offsetX_0() { return &___offsetX_0; }
	inline void set_offsetX_0(float value)
	{
		___offsetX_0 = value;
	}

	inline static int32_t get_offset_of_offsetY_1() { return static_cast<int32_t>(offsetof(Segment_t754368499, ___offsetY_1)); }
	inline float get_offsetY_1() const { return ___offsetY_1; }
	inline float* get_address_of_offsetY_1() { return &___offsetY_1; }
	inline void set_offsetY_1(float value)
	{
		___offsetY_1 = value;
	}

	inline static int32_t get_offset_of_scaleX_2() { return static_cast<int32_t>(offsetof(Segment_t754368499, ___scaleX_2)); }
	inline float get_scaleX_2() const { return ___scaleX_2; }
	inline float* get_address_of_scaleX_2() { return &___scaleX_2; }
	inline void set_scaleX_2(float value)
	{
		___scaleX_2 = value;
	}

	inline static int32_t get_offset_of_scaleY_3() { return static_cast<int32_t>(offsetof(Segment_t754368499, ___scaleY_3)); }
	inline float get_scaleY_3() const { return ___scaleY_3; }
	inline float* get_address_of_scaleY_3() { return &___scaleY_3; }
	inline void set_scaleY_3(float value)
	{
		___scaleY_3 = value;
	}

	inline static int32_t get_offset_of_lnA_4() { return static_cast<int32_t>(offsetof(Segment_t754368499, ___lnA_4)); }
	inline float get_lnA_4() const { return ___lnA_4; }
	inline float* get_address_of_lnA_4() { return &___lnA_4; }
	inline void set_lnA_4(float value)
	{
		___lnA_4 = value;
	}

	inline static int32_t get_offset_of_B_5() { return static_cast<int32_t>(offsetof(Segment_t754368499, ___B_5)); }
	inline float get_B_5() const { return ___B_5; }
	inline float* get_address_of_B_5() { return &___B_5; }
	inline void set_B_5(float value)
	{
		___B_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENT_T754368499_H
#ifndef UNIFORMS_T165416594_H
#define UNIFORMS_T165416594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve/Uniforms
struct  Uniforms_t165416594  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.HableCurve UnityEngine.Rendering.PostProcessing.HableCurve/Uniforms::parent
	HableCurve_t1612137718 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Uniforms_t165416594, ___parent_0)); }
	inline HableCurve_t1612137718 * get_parent_0() const { return ___parent_0; }
	inline HableCurve_t1612137718 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(HableCurve_t1612137718 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T165416594_H
#ifndef HALTONSEQ_T947190270_H
#define HALTONSEQ_T947190270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HaltonSeq
struct  HaltonSeq_t947190270  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HALTONSEQ_T947190270_H
#ifndef LOGHISTOGRAM_T1187052756_H
#define LOGHISTOGRAM_T1187052756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LogHistogram
struct  LogHistogram_t1187052756  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.LogHistogram::m_ThreadX
	int32_t ___m_ThreadX_3;
	// System.Int32 UnityEngine.Rendering.PostProcessing.LogHistogram::m_ThreadY
	int32_t ___m_ThreadY_4;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.LogHistogram::<data>k__BackingField
	ComputeBuffer_t1033194329 * ___U3CdataU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_m_ThreadX_3() { return static_cast<int32_t>(offsetof(LogHistogram_t1187052756, ___m_ThreadX_3)); }
	inline int32_t get_m_ThreadX_3() const { return ___m_ThreadX_3; }
	inline int32_t* get_address_of_m_ThreadX_3() { return &___m_ThreadX_3; }
	inline void set_m_ThreadX_3(int32_t value)
	{
		___m_ThreadX_3 = value;
	}

	inline static int32_t get_offset_of_m_ThreadY_4() { return static_cast<int32_t>(offsetof(LogHistogram_t1187052756, ___m_ThreadY_4)); }
	inline int32_t get_m_ThreadY_4() const { return ___m_ThreadY_4; }
	inline int32_t* get_address_of_m_ThreadY_4() { return &___m_ThreadY_4; }
	inline void set_m_ThreadY_4(int32_t value)
	{
		___m_ThreadY_4 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LogHistogram_t1187052756, ___U3CdataU3Ek__BackingField_5)); }
	inline ComputeBuffer_t1033194329 * get_U3CdataU3Ek__BackingField_5() const { return ___U3CdataU3Ek__BackingField_5; }
	inline ComputeBuffer_t1033194329 ** get_address_of_U3CdataU3Ek__BackingField_5() { return &___U3CdataU3Ek__BackingField_5; }
	inline void set_U3CdataU3Ek__BackingField_5(ComputeBuffer_t1033194329 * value)
	{
		___U3CdataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGHISTOGRAM_T1187052756_H
#ifndef PARAMETEROVERRIDE_T3061054201_H
#define PARAMETEROVERRIDE_T3061054201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride
struct  ParameterOverride_t3061054201  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.ParameterOverride::overrideState
	bool ___overrideState_0;

public:
	inline static int32_t get_offset_of_overrideState_0() { return static_cast<int32_t>(offsetof(ParameterOverride_t3061054201, ___overrideState_0)); }
	inline bool get_overrideState_0() const { return ___overrideState_0; }
	inline bool* get_address_of_overrideState_0() { return &___overrideState_0; }
	inline void set_overrideState_0(bool value)
	{
		___overrideState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_T3061054201_H
#ifndef POSTPROCESSBUNDLE_T3465003670_H
#define POSTPROCESSBUNDLE_T3465003670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessBundle
struct  PostProcessBundle_t3465003670  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessAttribute UnityEngine.Rendering.PostProcessing.PostProcessBundle::<attribute>k__BackingField
	PostProcessAttribute_t2074534414 * ___U3CattributeU3Ek__BackingField_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings UnityEngine.Rendering.PostProcessing.PostProcessBundle::<settings>k__BackingField
	PostProcessEffectSettings_t1672565614 * ___U3CsettingsU3Ek__BackingField_1;
	// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer UnityEngine.Rendering.PostProcessing.PostProcessBundle::m_Renderer
	PostProcessEffectRenderer_t1060237 * ___m_Renderer_2;

public:
	inline static int32_t get_offset_of_U3CattributeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PostProcessBundle_t3465003670, ___U3CattributeU3Ek__BackingField_0)); }
	inline PostProcessAttribute_t2074534414 * get_U3CattributeU3Ek__BackingField_0() const { return ___U3CattributeU3Ek__BackingField_0; }
	inline PostProcessAttribute_t2074534414 ** get_address_of_U3CattributeU3Ek__BackingField_0() { return &___U3CattributeU3Ek__BackingField_0; }
	inline void set_U3CattributeU3Ek__BackingField_0(PostProcessAttribute_t2074534414 * value)
	{
		___U3CattributeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessBundle_t3465003670, ___U3CsettingsU3Ek__BackingField_1)); }
	inline PostProcessEffectSettings_t1672565614 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline PostProcessEffectSettings_t1672565614 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(PostProcessEffectSettings_t1672565614 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(PostProcessBundle_t3465003670, ___m_Renderer_2)); }
	inline PostProcessEffectRenderer_t1060237 * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline PostProcessEffectRenderer_t1060237 ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(PostProcessEffectRenderer_t1060237 * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSBUNDLE_T3465003670_H
#ifndef POSTPROCESSEFFECTRENDERER_T1060237_H
#define POSTPROCESSEFFECTRENDERER_T1060237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer
struct  PostProcessEffectRenderer_t1060237  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer::m_ResetHistory
	bool ___m_ResetHistory_0;

public:
	inline static int32_t get_offset_of_m_ResetHistory_0() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_t1060237, ___m_ResetHistory_0)); }
	inline bool get_m_ResetHistory_0() const { return ___m_ResetHistory_0; }
	inline bool* get_address_of_m_ResetHistory_0() { return &___m_ResetHistory_0; }
	inline void set_m_ResetHistory_0(bool value)
	{
		___m_ResetHistory_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_T1060237_H
#ifndef U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY1_T2108533815_H
#define U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY1_T2108533815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0/<UpdateBundleSortList>c__AnonStorey1
struct  U3CUpdateBundleSortListU3Ec__AnonStorey1_t2108533815  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0/<UpdateBundleSortList>c__AnonStorey1::searchStr
	String_t* ___searchStr_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0 UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0/<UpdateBundleSortList>c__AnonStorey1::<>f__ref$0
	U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_searchStr_0() { return static_cast<int32_t>(offsetof(U3CUpdateBundleSortListU3Ec__AnonStorey1_t2108533815, ___searchStr_0)); }
	inline String_t* get_searchStr_0() const { return ___searchStr_0; }
	inline String_t** get_address_of_searchStr_0() { return &___searchStr_0; }
	inline void set_searchStr_0(String_t* value)
	{
		___searchStr_0 = value;
		Il2CppCodeGenWriteBarrier((&___searchStr_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CUpdateBundleSortListU3Ec__AnonStorey1_t2108533815, ___U3CU3Ef__refU240_1)); }
	inline U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY1_T2108533815_H
#ifndef U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY2_T38712142_H
#define U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY2_T38712142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey2
struct  U3CUpdateBundleSortListU3Ec__AnonStorey2_t38712142  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey2::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(U3CUpdateBundleSortListU3Ec__AnonStorey2_t38712142, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY2_T38712142_H
#ifndef U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY3_T38646606_H
#define U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY3_T38646606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey3
struct  U3CUpdateBundleSortListU3Ec__AnonStorey3_t38646606  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey3::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(U3CUpdateBundleSortListU3Ec__AnonStorey3_t38646606, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY3_T38646606_H
#ifndef SERIALIZEDBUNDLEREF_T2850027960_H
#define SERIALIZEDBUNDLEREF_T2850027960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef
struct  SerializedBundleRef_t2850027960  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef::assemblyQualifiedName
	String_t* ___assemblyQualifiedName_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessBundle UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef::bundle
	PostProcessBundle_t3465003670 * ___bundle_1;

public:
	inline static int32_t get_offset_of_assemblyQualifiedName_0() { return static_cast<int32_t>(offsetof(SerializedBundleRef_t2850027960, ___assemblyQualifiedName_0)); }
	inline String_t* get_assemblyQualifiedName_0() const { return ___assemblyQualifiedName_0; }
	inline String_t** get_address_of_assemblyQualifiedName_0() { return &___assemblyQualifiedName_0; }
	inline void set_assemblyQualifiedName_0(String_t* value)
	{
		___assemblyQualifiedName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyQualifiedName_0), value);
	}

	inline static int32_t get_offset_of_bundle_1() { return static_cast<int32_t>(offsetof(SerializedBundleRef_t2850027960, ___bundle_1)); }
	inline PostProcessBundle_t3465003670 * get_bundle_1() const { return ___bundle_1; }
	inline PostProcessBundle_t3465003670 ** get_address_of_bundle_1() { return &___bundle_1; }
	inline void set_bundle_1(PostProcessBundle_t3465003670 * value)
	{
		___bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___bundle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDBUNDLEREF_T2850027960_H
#ifndef POSTPROCESSMANAGER_T1036946270_H
#define POSTPROCESSMANAGER_T1036946270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessManager
struct  PostProcessManager_t1036946270  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume>> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_SortedVolumes
	Dictionary_2_t3343844990 * ___m_SortedVolumes_1;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_Volumes
	List_1_t160164363 * ___m_Volumes_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_SortNeeded
	Dictionary_2_t3280968592 * ___m_SortNeeded_3;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_BaseSettings
	List_1_t3144640356 * ___m_BaseSettings_4;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_TempColliders
	List_1_t3245421752 * ___m_TempColliders_5;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessAttribute> UnityEngine.Rendering.PostProcessing.PostProcessManager::settingsTypes
	Dictionary_2_t223914182 * ___settingsTypes_6;

public:
	inline static int32_t get_offset_of_m_SortedVolumes_1() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270, ___m_SortedVolumes_1)); }
	inline Dictionary_2_t3343844990 * get_m_SortedVolumes_1() const { return ___m_SortedVolumes_1; }
	inline Dictionary_2_t3343844990 ** get_address_of_m_SortedVolumes_1() { return &___m_SortedVolumes_1; }
	inline void set_m_SortedVolumes_1(Dictionary_2_t3343844990 * value)
	{
		___m_SortedVolumes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortedVolumes_1), value);
	}

	inline static int32_t get_offset_of_m_Volumes_2() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270, ___m_Volumes_2)); }
	inline List_1_t160164363 * get_m_Volumes_2() const { return ___m_Volumes_2; }
	inline List_1_t160164363 ** get_address_of_m_Volumes_2() { return &___m_Volumes_2; }
	inline void set_m_Volumes_2(List_1_t160164363 * value)
	{
		___m_Volumes_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Volumes_2), value);
	}

	inline static int32_t get_offset_of_m_SortNeeded_3() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270, ___m_SortNeeded_3)); }
	inline Dictionary_2_t3280968592 * get_m_SortNeeded_3() const { return ___m_SortNeeded_3; }
	inline Dictionary_2_t3280968592 ** get_address_of_m_SortNeeded_3() { return &___m_SortNeeded_3; }
	inline void set_m_SortNeeded_3(Dictionary_2_t3280968592 * value)
	{
		___m_SortNeeded_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortNeeded_3), value);
	}

	inline static int32_t get_offset_of_m_BaseSettings_4() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270, ___m_BaseSettings_4)); }
	inline List_1_t3144640356 * get_m_BaseSettings_4() const { return ___m_BaseSettings_4; }
	inline List_1_t3144640356 ** get_address_of_m_BaseSettings_4() { return &___m_BaseSettings_4; }
	inline void set_m_BaseSettings_4(List_1_t3144640356 * value)
	{
		___m_BaseSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseSettings_4), value);
	}

	inline static int32_t get_offset_of_m_TempColliders_5() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270, ___m_TempColliders_5)); }
	inline List_1_t3245421752 * get_m_TempColliders_5() const { return ___m_TempColliders_5; }
	inline List_1_t3245421752 ** get_address_of_m_TempColliders_5() { return &___m_TempColliders_5; }
	inline void set_m_TempColliders_5(List_1_t3245421752 * value)
	{
		___m_TempColliders_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempColliders_5), value);
	}

	inline static int32_t get_offset_of_settingsTypes_6() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270, ___settingsTypes_6)); }
	inline Dictionary_2_t223914182 * get_settingsTypes_6() const { return ___settingsTypes_6; }
	inline Dictionary_2_t223914182 ** get_address_of_settingsTypes_6() { return &___settingsTypes_6; }
	inline void set_settingsTypes_6(Dictionary_2_t223914182 * value)
	{
		___settingsTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___settingsTypes_6), value);
	}
};

struct PostProcessManager_t1036946270_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessManager UnityEngine.Rendering.PostProcessing.PostProcessManager::s_Instance
	PostProcessManager_t1036946270 * ___s_Instance_0;
	// System.Func`2<System.Type,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessManager::<>f__am$cache0
	Func_2_t561252955 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270_StaticFields, ___s_Instance_0)); }
	inline PostProcessManager_t1036946270 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline PostProcessManager_t1036946270 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(PostProcessManager_t1036946270 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(PostProcessManager_t1036946270_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t561252955 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t561252955 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t561252955 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSMANAGER_T1036946270_H
#ifndef COMPUTESHADERS_T4172110136_H
#define COMPUTESHADERS_T4172110136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders
struct  ComputeShaders_t4172110136  : public RuntimeObject
{
public:
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::autoExposure
	ComputeShader_t317220254 * ___autoExposure_0;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::exposureHistogram
	ComputeShader_t317220254 * ___exposureHistogram_1;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::lut3DBaker
	ComputeShader_t317220254 * ___lut3DBaker_2;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::texture3dLerp
	ComputeShader_t317220254 * ___texture3dLerp_3;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::gammaHistogram
	ComputeShader_t317220254 * ___gammaHistogram_4;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::waveform
	ComputeShader_t317220254 * ___waveform_5;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::vectorscope
	ComputeShader_t317220254 * ___vectorscope_6;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::multiScaleAODownsample1
	ComputeShader_t317220254 * ___multiScaleAODownsample1_7;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::multiScaleAODownsample2
	ComputeShader_t317220254 * ___multiScaleAODownsample2_8;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::multiScaleAORender
	ComputeShader_t317220254 * ___multiScaleAORender_9;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::multiScaleAOUpsample
	ComputeShader_t317220254 * ___multiScaleAOUpsample_10;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders::gaussianDownsample
	ComputeShader_t317220254 * ___gaussianDownsample_11;

public:
	inline static int32_t get_offset_of_autoExposure_0() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___autoExposure_0)); }
	inline ComputeShader_t317220254 * get_autoExposure_0() const { return ___autoExposure_0; }
	inline ComputeShader_t317220254 ** get_address_of_autoExposure_0() { return &___autoExposure_0; }
	inline void set_autoExposure_0(ComputeShader_t317220254 * value)
	{
		___autoExposure_0 = value;
		Il2CppCodeGenWriteBarrier((&___autoExposure_0), value);
	}

	inline static int32_t get_offset_of_exposureHistogram_1() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___exposureHistogram_1)); }
	inline ComputeShader_t317220254 * get_exposureHistogram_1() const { return ___exposureHistogram_1; }
	inline ComputeShader_t317220254 ** get_address_of_exposureHistogram_1() { return &___exposureHistogram_1; }
	inline void set_exposureHistogram_1(ComputeShader_t317220254 * value)
	{
		___exposureHistogram_1 = value;
		Il2CppCodeGenWriteBarrier((&___exposureHistogram_1), value);
	}

	inline static int32_t get_offset_of_lut3DBaker_2() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___lut3DBaker_2)); }
	inline ComputeShader_t317220254 * get_lut3DBaker_2() const { return ___lut3DBaker_2; }
	inline ComputeShader_t317220254 ** get_address_of_lut3DBaker_2() { return &___lut3DBaker_2; }
	inline void set_lut3DBaker_2(ComputeShader_t317220254 * value)
	{
		___lut3DBaker_2 = value;
		Il2CppCodeGenWriteBarrier((&___lut3DBaker_2), value);
	}

	inline static int32_t get_offset_of_texture3dLerp_3() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___texture3dLerp_3)); }
	inline ComputeShader_t317220254 * get_texture3dLerp_3() const { return ___texture3dLerp_3; }
	inline ComputeShader_t317220254 ** get_address_of_texture3dLerp_3() { return &___texture3dLerp_3; }
	inline void set_texture3dLerp_3(ComputeShader_t317220254 * value)
	{
		___texture3dLerp_3 = value;
		Il2CppCodeGenWriteBarrier((&___texture3dLerp_3), value);
	}

	inline static int32_t get_offset_of_gammaHistogram_4() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___gammaHistogram_4)); }
	inline ComputeShader_t317220254 * get_gammaHistogram_4() const { return ___gammaHistogram_4; }
	inline ComputeShader_t317220254 ** get_address_of_gammaHistogram_4() { return &___gammaHistogram_4; }
	inline void set_gammaHistogram_4(ComputeShader_t317220254 * value)
	{
		___gammaHistogram_4 = value;
		Il2CppCodeGenWriteBarrier((&___gammaHistogram_4), value);
	}

	inline static int32_t get_offset_of_waveform_5() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___waveform_5)); }
	inline ComputeShader_t317220254 * get_waveform_5() const { return ___waveform_5; }
	inline ComputeShader_t317220254 ** get_address_of_waveform_5() { return &___waveform_5; }
	inline void set_waveform_5(ComputeShader_t317220254 * value)
	{
		___waveform_5 = value;
		Il2CppCodeGenWriteBarrier((&___waveform_5), value);
	}

	inline static int32_t get_offset_of_vectorscope_6() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___vectorscope_6)); }
	inline ComputeShader_t317220254 * get_vectorscope_6() const { return ___vectorscope_6; }
	inline ComputeShader_t317220254 ** get_address_of_vectorscope_6() { return &___vectorscope_6; }
	inline void set_vectorscope_6(ComputeShader_t317220254 * value)
	{
		___vectorscope_6 = value;
		Il2CppCodeGenWriteBarrier((&___vectorscope_6), value);
	}

	inline static int32_t get_offset_of_multiScaleAODownsample1_7() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___multiScaleAODownsample1_7)); }
	inline ComputeShader_t317220254 * get_multiScaleAODownsample1_7() const { return ___multiScaleAODownsample1_7; }
	inline ComputeShader_t317220254 ** get_address_of_multiScaleAODownsample1_7() { return &___multiScaleAODownsample1_7; }
	inline void set_multiScaleAODownsample1_7(ComputeShader_t317220254 * value)
	{
		___multiScaleAODownsample1_7 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAODownsample1_7), value);
	}

	inline static int32_t get_offset_of_multiScaleAODownsample2_8() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___multiScaleAODownsample2_8)); }
	inline ComputeShader_t317220254 * get_multiScaleAODownsample2_8() const { return ___multiScaleAODownsample2_8; }
	inline ComputeShader_t317220254 ** get_address_of_multiScaleAODownsample2_8() { return &___multiScaleAODownsample2_8; }
	inline void set_multiScaleAODownsample2_8(ComputeShader_t317220254 * value)
	{
		___multiScaleAODownsample2_8 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAODownsample2_8), value);
	}

	inline static int32_t get_offset_of_multiScaleAORender_9() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___multiScaleAORender_9)); }
	inline ComputeShader_t317220254 * get_multiScaleAORender_9() const { return ___multiScaleAORender_9; }
	inline ComputeShader_t317220254 ** get_address_of_multiScaleAORender_9() { return &___multiScaleAORender_9; }
	inline void set_multiScaleAORender_9(ComputeShader_t317220254 * value)
	{
		___multiScaleAORender_9 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAORender_9), value);
	}

	inline static int32_t get_offset_of_multiScaleAOUpsample_10() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___multiScaleAOUpsample_10)); }
	inline ComputeShader_t317220254 * get_multiScaleAOUpsample_10() const { return ___multiScaleAOUpsample_10; }
	inline ComputeShader_t317220254 ** get_address_of_multiScaleAOUpsample_10() { return &___multiScaleAOUpsample_10; }
	inline void set_multiScaleAOUpsample_10(ComputeShader_t317220254 * value)
	{
		___multiScaleAOUpsample_10 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAOUpsample_10), value);
	}

	inline static int32_t get_offset_of_gaussianDownsample_11() { return static_cast<int32_t>(offsetof(ComputeShaders_t4172110136, ___gaussianDownsample_11)); }
	inline ComputeShader_t317220254 * get_gaussianDownsample_11() const { return ___gaussianDownsample_11; }
	inline ComputeShader_t317220254 ** get_address_of_gaussianDownsample_11() { return &___gaussianDownsample_11; }
	inline void set_gaussianDownsample_11(ComputeShader_t317220254 * value)
	{
		___gaussianDownsample_11 = value;
		Il2CppCodeGenWriteBarrier((&___gaussianDownsample_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTESHADERS_T4172110136_H
#ifndef SMAALUTS_T184516107_H
#define SMAALUTS_T184516107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources/SMAALuts
struct  SMAALuts_t184516107  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.PostProcessResources/SMAALuts::area
	Texture2D_t3840446185 * ___area_0;
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.PostProcessResources/SMAALuts::search
	Texture2D_t3840446185 * ___search_1;

public:
	inline static int32_t get_offset_of_area_0() { return static_cast<int32_t>(offsetof(SMAALuts_t184516107, ___area_0)); }
	inline Texture2D_t3840446185 * get_area_0() const { return ___area_0; }
	inline Texture2D_t3840446185 ** get_address_of_area_0() { return &___area_0; }
	inline void set_area_0(Texture2D_t3840446185 * value)
	{
		___area_0 = value;
		Il2CppCodeGenWriteBarrier((&___area_0), value);
	}

	inline static int32_t get_offset_of_search_1() { return static_cast<int32_t>(offsetof(SMAALuts_t184516107, ___search_1)); }
	inline Texture2D_t3840446185 * get_search_1() const { return ___search_1; }
	inline Texture2D_t3840446185 ** get_address_of_search_1() { return &___search_1; }
	inline void set_search_1(Texture2D_t3840446185 * value)
	{
		___search_1 = value;
		Il2CppCodeGenWriteBarrier((&___search_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMAALUTS_T184516107_H
#ifndef SHADERS_T2807171077_H
#define SHADERS_T2807171077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders
struct  Shaders_t2807171077  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::bloom
	Shader_t4151988712 * ___bloom_0;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::copy
	Shader_t4151988712 * ___copy_1;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::copyStd
	Shader_t4151988712 * ___copyStd_2;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::discardAlpha
	Shader_t4151988712 * ___discardAlpha_3;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::depthOfField
	Shader_t4151988712 * ___depthOfField_4;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::finalPass
	Shader_t4151988712 * ___finalPass_5;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::grainBaker
	Shader_t4151988712 * ___grainBaker_6;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::motionBlur
	Shader_t4151988712 * ___motionBlur_7;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::temporalAntialiasing
	Shader_t4151988712 * ___temporalAntialiasing_8;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::subpixelMorphologicalAntialiasing
	Shader_t4151988712 * ___subpixelMorphologicalAntialiasing_9;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::texture2dLerp
	Shader_t4151988712 * ___texture2dLerp_10;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::uber
	Shader_t4151988712 * ___uber_11;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::lut2DBaker
	Shader_t4151988712 * ___lut2DBaker_12;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::lightMeter
	Shader_t4151988712 * ___lightMeter_13;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::gammaHistogram
	Shader_t4151988712 * ___gammaHistogram_14;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::waveform
	Shader_t4151988712 * ___waveform_15;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::vectorscope
	Shader_t4151988712 * ___vectorscope_16;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::debugOverlays
	Shader_t4151988712 * ___debugOverlays_17;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::deferredFog
	Shader_t4151988712 * ___deferredFog_18;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::scalableAO
	Shader_t4151988712 * ___scalableAO_19;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::multiScaleAO
	Shader_t4151988712 * ___multiScaleAO_20;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders::screenSpaceReflections
	Shader_t4151988712 * ___screenSpaceReflections_21;

public:
	inline static int32_t get_offset_of_bloom_0() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___bloom_0)); }
	inline Shader_t4151988712 * get_bloom_0() const { return ___bloom_0; }
	inline Shader_t4151988712 ** get_address_of_bloom_0() { return &___bloom_0; }
	inline void set_bloom_0(Shader_t4151988712 * value)
	{
		___bloom_0 = value;
		Il2CppCodeGenWriteBarrier((&___bloom_0), value);
	}

	inline static int32_t get_offset_of_copy_1() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___copy_1)); }
	inline Shader_t4151988712 * get_copy_1() const { return ___copy_1; }
	inline Shader_t4151988712 ** get_address_of_copy_1() { return &___copy_1; }
	inline void set_copy_1(Shader_t4151988712 * value)
	{
		___copy_1 = value;
		Il2CppCodeGenWriteBarrier((&___copy_1), value);
	}

	inline static int32_t get_offset_of_copyStd_2() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___copyStd_2)); }
	inline Shader_t4151988712 * get_copyStd_2() const { return ___copyStd_2; }
	inline Shader_t4151988712 ** get_address_of_copyStd_2() { return &___copyStd_2; }
	inline void set_copyStd_2(Shader_t4151988712 * value)
	{
		___copyStd_2 = value;
		Il2CppCodeGenWriteBarrier((&___copyStd_2), value);
	}

	inline static int32_t get_offset_of_discardAlpha_3() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___discardAlpha_3)); }
	inline Shader_t4151988712 * get_discardAlpha_3() const { return ___discardAlpha_3; }
	inline Shader_t4151988712 ** get_address_of_discardAlpha_3() { return &___discardAlpha_3; }
	inline void set_discardAlpha_3(Shader_t4151988712 * value)
	{
		___discardAlpha_3 = value;
		Il2CppCodeGenWriteBarrier((&___discardAlpha_3), value);
	}

	inline static int32_t get_offset_of_depthOfField_4() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___depthOfField_4)); }
	inline Shader_t4151988712 * get_depthOfField_4() const { return ___depthOfField_4; }
	inline Shader_t4151988712 ** get_address_of_depthOfField_4() { return &___depthOfField_4; }
	inline void set_depthOfField_4(Shader_t4151988712 * value)
	{
		___depthOfField_4 = value;
		Il2CppCodeGenWriteBarrier((&___depthOfField_4), value);
	}

	inline static int32_t get_offset_of_finalPass_5() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___finalPass_5)); }
	inline Shader_t4151988712 * get_finalPass_5() const { return ___finalPass_5; }
	inline Shader_t4151988712 ** get_address_of_finalPass_5() { return &___finalPass_5; }
	inline void set_finalPass_5(Shader_t4151988712 * value)
	{
		___finalPass_5 = value;
		Il2CppCodeGenWriteBarrier((&___finalPass_5), value);
	}

	inline static int32_t get_offset_of_grainBaker_6() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___grainBaker_6)); }
	inline Shader_t4151988712 * get_grainBaker_6() const { return ___grainBaker_6; }
	inline Shader_t4151988712 ** get_address_of_grainBaker_6() { return &___grainBaker_6; }
	inline void set_grainBaker_6(Shader_t4151988712 * value)
	{
		___grainBaker_6 = value;
		Il2CppCodeGenWriteBarrier((&___grainBaker_6), value);
	}

	inline static int32_t get_offset_of_motionBlur_7() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___motionBlur_7)); }
	inline Shader_t4151988712 * get_motionBlur_7() const { return ___motionBlur_7; }
	inline Shader_t4151988712 ** get_address_of_motionBlur_7() { return &___motionBlur_7; }
	inline void set_motionBlur_7(Shader_t4151988712 * value)
	{
		___motionBlur_7 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlur_7), value);
	}

	inline static int32_t get_offset_of_temporalAntialiasing_8() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___temporalAntialiasing_8)); }
	inline Shader_t4151988712 * get_temporalAntialiasing_8() const { return ___temporalAntialiasing_8; }
	inline Shader_t4151988712 ** get_address_of_temporalAntialiasing_8() { return &___temporalAntialiasing_8; }
	inline void set_temporalAntialiasing_8(Shader_t4151988712 * value)
	{
		___temporalAntialiasing_8 = value;
		Il2CppCodeGenWriteBarrier((&___temporalAntialiasing_8), value);
	}

	inline static int32_t get_offset_of_subpixelMorphologicalAntialiasing_9() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___subpixelMorphologicalAntialiasing_9)); }
	inline Shader_t4151988712 * get_subpixelMorphologicalAntialiasing_9() const { return ___subpixelMorphologicalAntialiasing_9; }
	inline Shader_t4151988712 ** get_address_of_subpixelMorphologicalAntialiasing_9() { return &___subpixelMorphologicalAntialiasing_9; }
	inline void set_subpixelMorphologicalAntialiasing_9(Shader_t4151988712 * value)
	{
		___subpixelMorphologicalAntialiasing_9 = value;
		Il2CppCodeGenWriteBarrier((&___subpixelMorphologicalAntialiasing_9), value);
	}

	inline static int32_t get_offset_of_texture2dLerp_10() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___texture2dLerp_10)); }
	inline Shader_t4151988712 * get_texture2dLerp_10() const { return ___texture2dLerp_10; }
	inline Shader_t4151988712 ** get_address_of_texture2dLerp_10() { return &___texture2dLerp_10; }
	inline void set_texture2dLerp_10(Shader_t4151988712 * value)
	{
		___texture2dLerp_10 = value;
		Il2CppCodeGenWriteBarrier((&___texture2dLerp_10), value);
	}

	inline static int32_t get_offset_of_uber_11() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___uber_11)); }
	inline Shader_t4151988712 * get_uber_11() const { return ___uber_11; }
	inline Shader_t4151988712 ** get_address_of_uber_11() { return &___uber_11; }
	inline void set_uber_11(Shader_t4151988712 * value)
	{
		___uber_11 = value;
		Il2CppCodeGenWriteBarrier((&___uber_11), value);
	}

	inline static int32_t get_offset_of_lut2DBaker_12() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___lut2DBaker_12)); }
	inline Shader_t4151988712 * get_lut2DBaker_12() const { return ___lut2DBaker_12; }
	inline Shader_t4151988712 ** get_address_of_lut2DBaker_12() { return &___lut2DBaker_12; }
	inline void set_lut2DBaker_12(Shader_t4151988712 * value)
	{
		___lut2DBaker_12 = value;
		Il2CppCodeGenWriteBarrier((&___lut2DBaker_12), value);
	}

	inline static int32_t get_offset_of_lightMeter_13() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___lightMeter_13)); }
	inline Shader_t4151988712 * get_lightMeter_13() const { return ___lightMeter_13; }
	inline Shader_t4151988712 ** get_address_of_lightMeter_13() { return &___lightMeter_13; }
	inline void set_lightMeter_13(Shader_t4151988712 * value)
	{
		___lightMeter_13 = value;
		Il2CppCodeGenWriteBarrier((&___lightMeter_13), value);
	}

	inline static int32_t get_offset_of_gammaHistogram_14() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___gammaHistogram_14)); }
	inline Shader_t4151988712 * get_gammaHistogram_14() const { return ___gammaHistogram_14; }
	inline Shader_t4151988712 ** get_address_of_gammaHistogram_14() { return &___gammaHistogram_14; }
	inline void set_gammaHistogram_14(Shader_t4151988712 * value)
	{
		___gammaHistogram_14 = value;
		Il2CppCodeGenWriteBarrier((&___gammaHistogram_14), value);
	}

	inline static int32_t get_offset_of_waveform_15() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___waveform_15)); }
	inline Shader_t4151988712 * get_waveform_15() const { return ___waveform_15; }
	inline Shader_t4151988712 ** get_address_of_waveform_15() { return &___waveform_15; }
	inline void set_waveform_15(Shader_t4151988712 * value)
	{
		___waveform_15 = value;
		Il2CppCodeGenWriteBarrier((&___waveform_15), value);
	}

	inline static int32_t get_offset_of_vectorscope_16() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___vectorscope_16)); }
	inline Shader_t4151988712 * get_vectorscope_16() const { return ___vectorscope_16; }
	inline Shader_t4151988712 ** get_address_of_vectorscope_16() { return &___vectorscope_16; }
	inline void set_vectorscope_16(Shader_t4151988712 * value)
	{
		___vectorscope_16 = value;
		Il2CppCodeGenWriteBarrier((&___vectorscope_16), value);
	}

	inline static int32_t get_offset_of_debugOverlays_17() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___debugOverlays_17)); }
	inline Shader_t4151988712 * get_debugOverlays_17() const { return ___debugOverlays_17; }
	inline Shader_t4151988712 ** get_address_of_debugOverlays_17() { return &___debugOverlays_17; }
	inline void set_debugOverlays_17(Shader_t4151988712 * value)
	{
		___debugOverlays_17 = value;
		Il2CppCodeGenWriteBarrier((&___debugOverlays_17), value);
	}

	inline static int32_t get_offset_of_deferredFog_18() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___deferredFog_18)); }
	inline Shader_t4151988712 * get_deferredFog_18() const { return ___deferredFog_18; }
	inline Shader_t4151988712 ** get_address_of_deferredFog_18() { return &___deferredFog_18; }
	inline void set_deferredFog_18(Shader_t4151988712 * value)
	{
		___deferredFog_18 = value;
		Il2CppCodeGenWriteBarrier((&___deferredFog_18), value);
	}

	inline static int32_t get_offset_of_scalableAO_19() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___scalableAO_19)); }
	inline Shader_t4151988712 * get_scalableAO_19() const { return ___scalableAO_19; }
	inline Shader_t4151988712 ** get_address_of_scalableAO_19() { return &___scalableAO_19; }
	inline void set_scalableAO_19(Shader_t4151988712 * value)
	{
		___scalableAO_19 = value;
		Il2CppCodeGenWriteBarrier((&___scalableAO_19), value);
	}

	inline static int32_t get_offset_of_multiScaleAO_20() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___multiScaleAO_20)); }
	inline Shader_t4151988712 * get_multiScaleAO_20() const { return ___multiScaleAO_20; }
	inline Shader_t4151988712 ** get_address_of_multiScaleAO_20() { return &___multiScaleAO_20; }
	inline void set_multiScaleAO_20(Shader_t4151988712 * value)
	{
		___multiScaleAO_20 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAO_20), value);
	}

	inline static int32_t get_offset_of_screenSpaceReflections_21() { return static_cast<int32_t>(offsetof(Shaders_t2807171077, ___screenSpaceReflections_21)); }
	inline Shader_t4151988712 * get_screenSpaceReflections_21() const { return ___screenSpaceReflections_21; }
	inline Shader_t4151988712 ** get_address_of_screenSpaceReflections_21() { return &___screenSpaceReflections_21; }
	inline void set_screenSpaceReflections_21(Shader_t4151988712 * value)
	{
		___screenSpaceReflections_21 = value;
		Il2CppCodeGenWriteBarrier((&___screenSpaceReflections_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERS_T2807171077_H
#ifndef PROPERTYSHEET_T3821403501_H
#define PROPERTYSHEET_T3821403501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PropertySheet
struct  PropertySheet_t3821403501  : public RuntimeObject
{
public:
	// UnityEngine.MaterialPropertyBlock UnityEngine.Rendering.PostProcessing.PropertySheet::<properties>k__BackingField
	MaterialPropertyBlock_t3213117958 * ___U3CpropertiesU3Ek__BackingField_0;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.PropertySheet::<material>k__BackingField
	Material_t340375123 * ___U3CmaterialU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CpropertiesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PropertySheet_t3821403501, ___U3CpropertiesU3Ek__BackingField_0)); }
	inline MaterialPropertyBlock_t3213117958 * get_U3CpropertiesU3Ek__BackingField_0() const { return ___U3CpropertiesU3Ek__BackingField_0; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_U3CpropertiesU3Ek__BackingField_0() { return &___U3CpropertiesU3Ek__BackingField_0; }
	inline void set_U3CpropertiesU3Ek__BackingField_0(MaterialPropertyBlock_t3213117958 * value)
	{
		___U3CpropertiesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpropertiesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmaterialU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PropertySheet_t3821403501, ___U3CmaterialU3Ek__BackingField_1)); }
	inline Material_t340375123 * get_U3CmaterialU3Ek__BackingField_1() const { return ___U3CmaterialU3Ek__BackingField_1; }
	inline Material_t340375123 ** get_address_of_U3CmaterialU3Ek__BackingField_1() { return &___U3CmaterialU3Ek__BackingField_1; }
	inline void set_U3CmaterialU3Ek__BackingField_1(Material_t340375123 * value)
	{
		___U3CmaterialU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmaterialU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYSHEET_T3821403501_H
#ifndef PROPERTYSHEETFACTORY_T1490101248_H
#define PROPERTYSHEETFACTORY_T1490101248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PropertySheetFactory
struct  PropertySheetFactory_t1490101248  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Shader,UnityEngine.Rendering.PostProcessing.PropertySheet> UnityEngine.Rendering.PostProcessing.PropertySheetFactory::m_Sheets
	Dictionary_2_t77305525 * ___m_Sheets_0;

public:
	inline static int32_t get_offset_of_m_Sheets_0() { return static_cast<int32_t>(offsetof(PropertySheetFactory_t1490101248, ___m_Sheets_0)); }
	inline Dictionary_2_t77305525 * get_m_Sheets_0() const { return ___m_Sheets_0; }
	inline Dictionary_2_t77305525 ** get_address_of_m_Sheets_0() { return &___m_Sheets_0; }
	inline void set_m_Sheets_0(Dictionary_2_t77305525 * value)
	{
		___m_Sheets_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sheets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYSHEETFACTORY_T1490101248_H
#ifndef RUNTIMEUTILITIES_T4060785994_H
#define RUNTIMEUTILITIES_T4060785994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.RuntimeUtilities
struct  RuntimeUtilities_t4060785994  : public RuntimeObject
{
public:

public:
};

struct RuntimeUtilities_t4060785994_StaticFields
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_WhiteTexture
	Texture2D_t3840446185 * ___m_WhiteTexture_0;
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_BlackTexture
	Texture2D_t3840446185 * ___m_BlackTexture_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Texture2D> UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_LutStrips
	Dictionary_2_t2729159516 * ___m_LutStrips_2;
	// UnityEngine.Mesh UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_FullscreenTriangle
	Mesh_t3648964284 * ___s_FullscreenTriangle_3;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyStdMaterial
	Material_t340375123 * ___s_CopyStdMaterial_4;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyMaterial
	Material_t340375123 * ___s_CopyMaterial_5;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopySheet
	PropertySheet_t3821403501 * ___s_CopySheet_6;
	// System.Collections.Generic.IEnumerable`1<System.Type> UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_AssemblyTypes
	RuntimeObject* ___m_AssemblyTypes_7;
	// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>> UnityEngine.Rendering.PostProcessing.RuntimeUtilities::<>f__am$cache0
	Func_2_t779105388 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_m_WhiteTexture_0() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___m_WhiteTexture_0)); }
	inline Texture2D_t3840446185 * get_m_WhiteTexture_0() const { return ___m_WhiteTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_m_WhiteTexture_0() { return &___m_WhiteTexture_0; }
	inline void set_m_WhiteTexture_0(Texture2D_t3840446185 * value)
	{
		___m_WhiteTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WhiteTexture_0), value);
	}

	inline static int32_t get_offset_of_m_BlackTexture_1() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___m_BlackTexture_1)); }
	inline Texture2D_t3840446185 * get_m_BlackTexture_1() const { return ___m_BlackTexture_1; }
	inline Texture2D_t3840446185 ** get_address_of_m_BlackTexture_1() { return &___m_BlackTexture_1; }
	inline void set_m_BlackTexture_1(Texture2D_t3840446185 * value)
	{
		___m_BlackTexture_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlackTexture_1), value);
	}

	inline static int32_t get_offset_of_m_LutStrips_2() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___m_LutStrips_2)); }
	inline Dictionary_2_t2729159516 * get_m_LutStrips_2() const { return ___m_LutStrips_2; }
	inline Dictionary_2_t2729159516 ** get_address_of_m_LutStrips_2() { return &___m_LutStrips_2; }
	inline void set_m_LutStrips_2(Dictionary_2_t2729159516 * value)
	{
		___m_LutStrips_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LutStrips_2), value);
	}

	inline static int32_t get_offset_of_s_FullscreenTriangle_3() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___s_FullscreenTriangle_3)); }
	inline Mesh_t3648964284 * get_s_FullscreenTriangle_3() const { return ___s_FullscreenTriangle_3; }
	inline Mesh_t3648964284 ** get_address_of_s_FullscreenTriangle_3() { return &___s_FullscreenTriangle_3; }
	inline void set_s_FullscreenTriangle_3(Mesh_t3648964284 * value)
	{
		___s_FullscreenTriangle_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_FullscreenTriangle_3), value);
	}

	inline static int32_t get_offset_of_s_CopyStdMaterial_4() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___s_CopyStdMaterial_4)); }
	inline Material_t340375123 * get_s_CopyStdMaterial_4() const { return ___s_CopyStdMaterial_4; }
	inline Material_t340375123 ** get_address_of_s_CopyStdMaterial_4() { return &___s_CopyStdMaterial_4; }
	inline void set_s_CopyStdMaterial_4(Material_t340375123 * value)
	{
		___s_CopyStdMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyStdMaterial_4), value);
	}

	inline static int32_t get_offset_of_s_CopyMaterial_5() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___s_CopyMaterial_5)); }
	inline Material_t340375123 * get_s_CopyMaterial_5() const { return ___s_CopyMaterial_5; }
	inline Material_t340375123 ** get_address_of_s_CopyMaterial_5() { return &___s_CopyMaterial_5; }
	inline void set_s_CopyMaterial_5(Material_t340375123 * value)
	{
		___s_CopyMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyMaterial_5), value);
	}

	inline static int32_t get_offset_of_s_CopySheet_6() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___s_CopySheet_6)); }
	inline PropertySheet_t3821403501 * get_s_CopySheet_6() const { return ___s_CopySheet_6; }
	inline PropertySheet_t3821403501 ** get_address_of_s_CopySheet_6() { return &___s_CopySheet_6; }
	inline void set_s_CopySheet_6(PropertySheet_t3821403501 * value)
	{
		___s_CopySheet_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopySheet_6), value);
	}

	inline static int32_t get_offset_of_m_AssemblyTypes_7() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___m_AssemblyTypes_7)); }
	inline RuntimeObject* get_m_AssemblyTypes_7() const { return ___m_AssemblyTypes_7; }
	inline RuntimeObject** get_address_of_m_AssemblyTypes_7() { return &___m_AssemblyTypes_7; }
	inline void set_m_AssemblyTypes_7(RuntimeObject* value)
	{
		___m_AssemblyTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssemblyTypes_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t4060785994_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Func_2_t779105388 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Func_2_t779105388 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Func_2_t779105388 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEUTILITIES_T4060785994_H
#ifndef SHADERIDS_T2844105293_H
#define SHADERIDS_T2844105293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ShaderIDs
struct  ShaderIDs_t2844105293  : public RuntimeObject
{
public:

public:
};

struct ShaderIDs_t2844105293_StaticFields
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MainTex
	int32_t ___MainTex_0;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Jitter
	int32_t ___Jitter_1;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Sharpness
	int32_t ___Sharpness_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::FinalBlendParameters
	int32_t ___FinalBlendParameters_3;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HistoryTex
	int32_t ___HistoryTex_4;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SMAA_Flip
	int32_t ___SMAA_Flip_5;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SMAA_Flop
	int32_t ___SMAA_Flop_6;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::AOParams
	int32_t ___AOParams_7;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::AOColor
	int32_t ___AOColor_8;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::OcclusionTexture1
	int32_t ___OcclusionTexture1_9;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::OcclusionTexture2
	int32_t ___OcclusionTexture2_10;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SAOcclusionTexture
	int32_t ___SAOcclusionTexture_11;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MSVOcclusionTexture
	int32_t ___MSVOcclusionTexture_12;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthCopy
	int32_t ___DepthCopy_13;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LinearDepth
	int32_t ___LinearDepth_14;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth1
	int32_t ___LowDepth1_15;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth2
	int32_t ___LowDepth2_16;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth3
	int32_t ___LowDepth3_17;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth4
	int32_t ___LowDepth4_18;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth1
	int32_t ___TiledDepth1_19;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth2
	int32_t ___TiledDepth2_20;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth3
	int32_t ___TiledDepth3_21;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth4
	int32_t ___TiledDepth4_22;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion1
	int32_t ___Occlusion1_23;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion2
	int32_t ___Occlusion2_24;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion3
	int32_t ___Occlusion3_25;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion4
	int32_t ___Occlusion4_26;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Combined1
	int32_t ___Combined1_27;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Combined2
	int32_t ___Combined2_28;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Combined3
	int32_t ___Combined3_29;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SSRResolveTemp
	int32_t ___SSRResolveTemp_30;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Noise
	int32_t ___Noise_31;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Test
	int32_t ___Test_32;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Resolve
	int32_t ___Resolve_33;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::History
	int32_t ___History_34;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ViewMatrix
	int32_t ___ViewMatrix_35;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::InverseViewMatrix
	int32_t ___InverseViewMatrix_36;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::InverseProjectionMatrix
	int32_t ___InverseProjectionMatrix_37;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ScreenSpaceProjectionMatrix
	int32_t ___ScreenSpaceProjectionMatrix_38;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Params2
	int32_t ___Params2_39;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::FogColor
	int32_t ___FogColor_40;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::FogParams
	int32_t ___FogParams_41;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::VelocityScale
	int32_t ___VelocityScale_42;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MaxBlurRadius
	int32_t ___MaxBlurRadius_43;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RcpMaxBlurRadius
	int32_t ___RcpMaxBlurRadius_44;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::VelocityTex
	int32_t ___VelocityTex_45;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Tile2RT
	int32_t ___Tile2RT_46;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Tile4RT
	int32_t ___Tile4RT_47;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Tile8RT
	int32_t ___Tile8RT_48;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TileMaxOffs
	int32_t ___TileMaxOffs_49;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TileMaxLoop
	int32_t ___TileMaxLoop_50;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TileVRT
	int32_t ___TileVRT_51;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::NeighborMaxTex
	int32_t ___NeighborMaxTex_52;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LoopCount
	int32_t ___LoopCount_53;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthOfFieldTemp
	int32_t ___DepthOfFieldTemp_54;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthOfFieldTex
	int32_t ___DepthOfFieldTex_55;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Distance
	int32_t ___Distance_56;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LensCoeff
	int32_t ___LensCoeff_57;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MaxCoC
	int32_t ___MaxCoC_58;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RcpMaxCoC
	int32_t ___RcpMaxCoC_59;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RcpAspect
	int32_t ___RcpAspect_60;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::CoCTex
	int32_t ___CoCTex_61;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TaaParams
	int32_t ___TaaParams_62;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::AutoExposureTex
	int32_t ___AutoExposureTex_63;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HistogramBuffer
	int32_t ___HistogramBuffer_64;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Params
	int32_t ___Params_65;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ScaleOffsetRes
	int32_t ___ScaleOffsetRes_66;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::BloomTex
	int32_t ___BloomTex_67;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SampleScale
	int32_t ___SampleScale_68;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Threshold
	int32_t ___Threshold_69;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ColorIntensity
	int32_t ___ColorIntensity_70;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_DirtTex
	int32_t ___Bloom_DirtTex_71;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_Settings
	int32_t ___Bloom_Settings_72;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_Color
	int32_t ___Bloom_Color_73;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_DirtTileOffset
	int32_t ___Bloom_DirtTileOffset_74;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChromaticAberration_Amount
	int32_t ___ChromaticAberration_Amount_75;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChromaticAberration_SpectralLut
	int32_t ___ChromaticAberration_SpectralLut_76;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Distortion_CenterScale
	int32_t ___Distortion_CenterScale_77;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Distortion_Amount
	int32_t ___Distortion_Amount_78;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut2D
	int32_t ___Lut2D_79;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut3D
	int32_t ___Lut3D_80;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut3D_Params
	int32_t ___Lut3D_Params_81;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut2D_Params
	int32_t ___Lut2D_Params_82;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::UserLut2D_Params
	int32_t ___UserLut2D_Params_83;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::PostExposure
	int32_t ___PostExposure_84;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ColorBalance
	int32_t ___ColorBalance_85;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ColorFilter
	int32_t ___ColorFilter_86;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HueSatCon
	int32_t ___HueSatCon_87;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Brightness
	int32_t ___Brightness_88;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChannelMixerRed
	int32_t ___ChannelMixerRed_89;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChannelMixerGreen
	int32_t ___ChannelMixerGreen_90;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChannelMixerBlue
	int32_t ___ChannelMixerBlue_91;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lift
	int32_t ___Lift_92;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::InvGamma
	int32_t ___InvGamma_93;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Gain
	int32_t ___Gain_94;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Curves
	int32_t ___Curves_95;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::CustomToneCurve
	int32_t ___CustomToneCurve_96;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ToeSegmentA
	int32_t ___ToeSegmentA_97;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ToeSegmentB
	int32_t ___ToeSegmentB_98;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MidSegmentA
	int32_t ___MidSegmentA_99;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MidSegmentB
	int32_t ___MidSegmentB_100;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ShoSegmentA
	int32_t ___ShoSegmentA_101;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ShoSegmentB
	int32_t ___ShoSegmentB_102;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Color
	int32_t ___Vignette_Color_103;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Center
	int32_t ___Vignette_Center_104;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Settings
	int32_t ___Vignette_Settings_105;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Mask
	int32_t ___Vignette_Mask_106;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Opacity
	int32_t ___Vignette_Opacity_107;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Mode
	int32_t ___Vignette_Mode_108;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Grain_Params1
	int32_t ___Grain_Params1_109;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Grain_Params2
	int32_t ___Grain_Params2_110;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::GrainTex
	int32_t ___GrainTex_111;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Phase
	int32_t ___Phase_112;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LumaInAlpha
	int32_t ___LumaInAlpha_113;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DitheringTex
	int32_t ___DitheringTex_114;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Dithering_Coords
	int32_t ___Dithering_Coords_115;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::From
	int32_t ___From_116;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::To
	int32_t ___To_117;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Interp
	int32_t ___Interp_118;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TargetColor
	int32_t ___TargetColor_119;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HalfResFinalCopy
	int32_t ___HalfResFinalCopy_120;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::WaveformSource
	int32_t ___WaveformSource_121;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::WaveformBuffer
	int32_t ___WaveformBuffer_122;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::VectorscopeBuffer
	int32_t ___VectorscopeBuffer_123;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RenderViewportScaleFactor
	int32_t ___RenderViewportScaleFactor_124;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::UVTransform
	int32_t ___UVTransform_125;

public:
	inline static int32_t get_offset_of_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___MainTex_0)); }
	inline int32_t get_MainTex_0() const { return ___MainTex_0; }
	inline int32_t* get_address_of_MainTex_0() { return &___MainTex_0; }
	inline void set_MainTex_0(int32_t value)
	{
		___MainTex_0 = value;
	}

	inline static int32_t get_offset_of_Jitter_1() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Jitter_1)); }
	inline int32_t get_Jitter_1() const { return ___Jitter_1; }
	inline int32_t* get_address_of_Jitter_1() { return &___Jitter_1; }
	inline void set_Jitter_1(int32_t value)
	{
		___Jitter_1 = value;
	}

	inline static int32_t get_offset_of_Sharpness_2() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Sharpness_2)); }
	inline int32_t get_Sharpness_2() const { return ___Sharpness_2; }
	inline int32_t* get_address_of_Sharpness_2() { return &___Sharpness_2; }
	inline void set_Sharpness_2(int32_t value)
	{
		___Sharpness_2 = value;
	}

	inline static int32_t get_offset_of_FinalBlendParameters_3() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___FinalBlendParameters_3)); }
	inline int32_t get_FinalBlendParameters_3() const { return ___FinalBlendParameters_3; }
	inline int32_t* get_address_of_FinalBlendParameters_3() { return &___FinalBlendParameters_3; }
	inline void set_FinalBlendParameters_3(int32_t value)
	{
		___FinalBlendParameters_3 = value;
	}

	inline static int32_t get_offset_of_HistoryTex_4() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___HistoryTex_4)); }
	inline int32_t get_HistoryTex_4() const { return ___HistoryTex_4; }
	inline int32_t* get_address_of_HistoryTex_4() { return &___HistoryTex_4; }
	inline void set_HistoryTex_4(int32_t value)
	{
		___HistoryTex_4 = value;
	}

	inline static int32_t get_offset_of_SMAA_Flip_5() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___SMAA_Flip_5)); }
	inline int32_t get_SMAA_Flip_5() const { return ___SMAA_Flip_5; }
	inline int32_t* get_address_of_SMAA_Flip_5() { return &___SMAA_Flip_5; }
	inline void set_SMAA_Flip_5(int32_t value)
	{
		___SMAA_Flip_5 = value;
	}

	inline static int32_t get_offset_of_SMAA_Flop_6() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___SMAA_Flop_6)); }
	inline int32_t get_SMAA_Flop_6() const { return ___SMAA_Flop_6; }
	inline int32_t* get_address_of_SMAA_Flop_6() { return &___SMAA_Flop_6; }
	inline void set_SMAA_Flop_6(int32_t value)
	{
		___SMAA_Flop_6 = value;
	}

	inline static int32_t get_offset_of_AOParams_7() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___AOParams_7)); }
	inline int32_t get_AOParams_7() const { return ___AOParams_7; }
	inline int32_t* get_address_of_AOParams_7() { return &___AOParams_7; }
	inline void set_AOParams_7(int32_t value)
	{
		___AOParams_7 = value;
	}

	inline static int32_t get_offset_of_AOColor_8() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___AOColor_8)); }
	inline int32_t get_AOColor_8() const { return ___AOColor_8; }
	inline int32_t* get_address_of_AOColor_8() { return &___AOColor_8; }
	inline void set_AOColor_8(int32_t value)
	{
		___AOColor_8 = value;
	}

	inline static int32_t get_offset_of_OcclusionTexture1_9() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___OcclusionTexture1_9)); }
	inline int32_t get_OcclusionTexture1_9() const { return ___OcclusionTexture1_9; }
	inline int32_t* get_address_of_OcclusionTexture1_9() { return &___OcclusionTexture1_9; }
	inline void set_OcclusionTexture1_9(int32_t value)
	{
		___OcclusionTexture1_9 = value;
	}

	inline static int32_t get_offset_of_OcclusionTexture2_10() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___OcclusionTexture2_10)); }
	inline int32_t get_OcclusionTexture2_10() const { return ___OcclusionTexture2_10; }
	inline int32_t* get_address_of_OcclusionTexture2_10() { return &___OcclusionTexture2_10; }
	inline void set_OcclusionTexture2_10(int32_t value)
	{
		___OcclusionTexture2_10 = value;
	}

	inline static int32_t get_offset_of_SAOcclusionTexture_11() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___SAOcclusionTexture_11)); }
	inline int32_t get_SAOcclusionTexture_11() const { return ___SAOcclusionTexture_11; }
	inline int32_t* get_address_of_SAOcclusionTexture_11() { return &___SAOcclusionTexture_11; }
	inline void set_SAOcclusionTexture_11(int32_t value)
	{
		___SAOcclusionTexture_11 = value;
	}

	inline static int32_t get_offset_of_MSVOcclusionTexture_12() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___MSVOcclusionTexture_12)); }
	inline int32_t get_MSVOcclusionTexture_12() const { return ___MSVOcclusionTexture_12; }
	inline int32_t* get_address_of_MSVOcclusionTexture_12() { return &___MSVOcclusionTexture_12; }
	inline void set_MSVOcclusionTexture_12(int32_t value)
	{
		___MSVOcclusionTexture_12 = value;
	}

	inline static int32_t get_offset_of_DepthCopy_13() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___DepthCopy_13)); }
	inline int32_t get_DepthCopy_13() const { return ___DepthCopy_13; }
	inline int32_t* get_address_of_DepthCopy_13() { return &___DepthCopy_13; }
	inline void set_DepthCopy_13(int32_t value)
	{
		___DepthCopy_13 = value;
	}

	inline static int32_t get_offset_of_LinearDepth_14() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LinearDepth_14)); }
	inline int32_t get_LinearDepth_14() const { return ___LinearDepth_14; }
	inline int32_t* get_address_of_LinearDepth_14() { return &___LinearDepth_14; }
	inline void set_LinearDepth_14(int32_t value)
	{
		___LinearDepth_14 = value;
	}

	inline static int32_t get_offset_of_LowDepth1_15() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LowDepth1_15)); }
	inline int32_t get_LowDepth1_15() const { return ___LowDepth1_15; }
	inline int32_t* get_address_of_LowDepth1_15() { return &___LowDepth1_15; }
	inline void set_LowDepth1_15(int32_t value)
	{
		___LowDepth1_15 = value;
	}

	inline static int32_t get_offset_of_LowDepth2_16() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LowDepth2_16)); }
	inline int32_t get_LowDepth2_16() const { return ___LowDepth2_16; }
	inline int32_t* get_address_of_LowDepth2_16() { return &___LowDepth2_16; }
	inline void set_LowDepth2_16(int32_t value)
	{
		___LowDepth2_16 = value;
	}

	inline static int32_t get_offset_of_LowDepth3_17() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LowDepth3_17)); }
	inline int32_t get_LowDepth3_17() const { return ___LowDepth3_17; }
	inline int32_t* get_address_of_LowDepth3_17() { return &___LowDepth3_17; }
	inline void set_LowDepth3_17(int32_t value)
	{
		___LowDepth3_17 = value;
	}

	inline static int32_t get_offset_of_LowDepth4_18() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LowDepth4_18)); }
	inline int32_t get_LowDepth4_18() const { return ___LowDepth4_18; }
	inline int32_t* get_address_of_LowDepth4_18() { return &___LowDepth4_18; }
	inline void set_LowDepth4_18(int32_t value)
	{
		___LowDepth4_18 = value;
	}

	inline static int32_t get_offset_of_TiledDepth1_19() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TiledDepth1_19)); }
	inline int32_t get_TiledDepth1_19() const { return ___TiledDepth1_19; }
	inline int32_t* get_address_of_TiledDepth1_19() { return &___TiledDepth1_19; }
	inline void set_TiledDepth1_19(int32_t value)
	{
		___TiledDepth1_19 = value;
	}

	inline static int32_t get_offset_of_TiledDepth2_20() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TiledDepth2_20)); }
	inline int32_t get_TiledDepth2_20() const { return ___TiledDepth2_20; }
	inline int32_t* get_address_of_TiledDepth2_20() { return &___TiledDepth2_20; }
	inline void set_TiledDepth2_20(int32_t value)
	{
		___TiledDepth2_20 = value;
	}

	inline static int32_t get_offset_of_TiledDepth3_21() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TiledDepth3_21)); }
	inline int32_t get_TiledDepth3_21() const { return ___TiledDepth3_21; }
	inline int32_t* get_address_of_TiledDepth3_21() { return &___TiledDepth3_21; }
	inline void set_TiledDepth3_21(int32_t value)
	{
		___TiledDepth3_21 = value;
	}

	inline static int32_t get_offset_of_TiledDepth4_22() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TiledDepth4_22)); }
	inline int32_t get_TiledDepth4_22() const { return ___TiledDepth4_22; }
	inline int32_t* get_address_of_TiledDepth4_22() { return &___TiledDepth4_22; }
	inline void set_TiledDepth4_22(int32_t value)
	{
		___TiledDepth4_22 = value;
	}

	inline static int32_t get_offset_of_Occlusion1_23() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Occlusion1_23)); }
	inline int32_t get_Occlusion1_23() const { return ___Occlusion1_23; }
	inline int32_t* get_address_of_Occlusion1_23() { return &___Occlusion1_23; }
	inline void set_Occlusion1_23(int32_t value)
	{
		___Occlusion1_23 = value;
	}

	inline static int32_t get_offset_of_Occlusion2_24() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Occlusion2_24)); }
	inline int32_t get_Occlusion2_24() const { return ___Occlusion2_24; }
	inline int32_t* get_address_of_Occlusion2_24() { return &___Occlusion2_24; }
	inline void set_Occlusion2_24(int32_t value)
	{
		___Occlusion2_24 = value;
	}

	inline static int32_t get_offset_of_Occlusion3_25() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Occlusion3_25)); }
	inline int32_t get_Occlusion3_25() const { return ___Occlusion3_25; }
	inline int32_t* get_address_of_Occlusion3_25() { return &___Occlusion3_25; }
	inline void set_Occlusion3_25(int32_t value)
	{
		___Occlusion3_25 = value;
	}

	inline static int32_t get_offset_of_Occlusion4_26() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Occlusion4_26)); }
	inline int32_t get_Occlusion4_26() const { return ___Occlusion4_26; }
	inline int32_t* get_address_of_Occlusion4_26() { return &___Occlusion4_26; }
	inline void set_Occlusion4_26(int32_t value)
	{
		___Occlusion4_26 = value;
	}

	inline static int32_t get_offset_of_Combined1_27() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Combined1_27)); }
	inline int32_t get_Combined1_27() const { return ___Combined1_27; }
	inline int32_t* get_address_of_Combined1_27() { return &___Combined1_27; }
	inline void set_Combined1_27(int32_t value)
	{
		___Combined1_27 = value;
	}

	inline static int32_t get_offset_of_Combined2_28() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Combined2_28)); }
	inline int32_t get_Combined2_28() const { return ___Combined2_28; }
	inline int32_t* get_address_of_Combined2_28() { return &___Combined2_28; }
	inline void set_Combined2_28(int32_t value)
	{
		___Combined2_28 = value;
	}

	inline static int32_t get_offset_of_Combined3_29() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Combined3_29)); }
	inline int32_t get_Combined3_29() const { return ___Combined3_29; }
	inline int32_t* get_address_of_Combined3_29() { return &___Combined3_29; }
	inline void set_Combined3_29(int32_t value)
	{
		___Combined3_29 = value;
	}

	inline static int32_t get_offset_of_SSRResolveTemp_30() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___SSRResolveTemp_30)); }
	inline int32_t get_SSRResolveTemp_30() const { return ___SSRResolveTemp_30; }
	inline int32_t* get_address_of_SSRResolveTemp_30() { return &___SSRResolveTemp_30; }
	inline void set_SSRResolveTemp_30(int32_t value)
	{
		___SSRResolveTemp_30 = value;
	}

	inline static int32_t get_offset_of_Noise_31() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Noise_31)); }
	inline int32_t get_Noise_31() const { return ___Noise_31; }
	inline int32_t* get_address_of_Noise_31() { return &___Noise_31; }
	inline void set_Noise_31(int32_t value)
	{
		___Noise_31 = value;
	}

	inline static int32_t get_offset_of_Test_32() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Test_32)); }
	inline int32_t get_Test_32() const { return ___Test_32; }
	inline int32_t* get_address_of_Test_32() { return &___Test_32; }
	inline void set_Test_32(int32_t value)
	{
		___Test_32 = value;
	}

	inline static int32_t get_offset_of_Resolve_33() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Resolve_33)); }
	inline int32_t get_Resolve_33() const { return ___Resolve_33; }
	inline int32_t* get_address_of_Resolve_33() { return &___Resolve_33; }
	inline void set_Resolve_33(int32_t value)
	{
		___Resolve_33 = value;
	}

	inline static int32_t get_offset_of_History_34() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___History_34)); }
	inline int32_t get_History_34() const { return ___History_34; }
	inline int32_t* get_address_of_History_34() { return &___History_34; }
	inline void set_History_34(int32_t value)
	{
		___History_34 = value;
	}

	inline static int32_t get_offset_of_ViewMatrix_35() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ViewMatrix_35)); }
	inline int32_t get_ViewMatrix_35() const { return ___ViewMatrix_35; }
	inline int32_t* get_address_of_ViewMatrix_35() { return &___ViewMatrix_35; }
	inline void set_ViewMatrix_35(int32_t value)
	{
		___ViewMatrix_35 = value;
	}

	inline static int32_t get_offset_of_InverseViewMatrix_36() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___InverseViewMatrix_36)); }
	inline int32_t get_InverseViewMatrix_36() const { return ___InverseViewMatrix_36; }
	inline int32_t* get_address_of_InverseViewMatrix_36() { return &___InverseViewMatrix_36; }
	inline void set_InverseViewMatrix_36(int32_t value)
	{
		___InverseViewMatrix_36 = value;
	}

	inline static int32_t get_offset_of_InverseProjectionMatrix_37() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___InverseProjectionMatrix_37)); }
	inline int32_t get_InverseProjectionMatrix_37() const { return ___InverseProjectionMatrix_37; }
	inline int32_t* get_address_of_InverseProjectionMatrix_37() { return &___InverseProjectionMatrix_37; }
	inline void set_InverseProjectionMatrix_37(int32_t value)
	{
		___InverseProjectionMatrix_37 = value;
	}

	inline static int32_t get_offset_of_ScreenSpaceProjectionMatrix_38() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ScreenSpaceProjectionMatrix_38)); }
	inline int32_t get_ScreenSpaceProjectionMatrix_38() const { return ___ScreenSpaceProjectionMatrix_38; }
	inline int32_t* get_address_of_ScreenSpaceProjectionMatrix_38() { return &___ScreenSpaceProjectionMatrix_38; }
	inline void set_ScreenSpaceProjectionMatrix_38(int32_t value)
	{
		___ScreenSpaceProjectionMatrix_38 = value;
	}

	inline static int32_t get_offset_of_Params2_39() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Params2_39)); }
	inline int32_t get_Params2_39() const { return ___Params2_39; }
	inline int32_t* get_address_of_Params2_39() { return &___Params2_39; }
	inline void set_Params2_39(int32_t value)
	{
		___Params2_39 = value;
	}

	inline static int32_t get_offset_of_FogColor_40() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___FogColor_40)); }
	inline int32_t get_FogColor_40() const { return ___FogColor_40; }
	inline int32_t* get_address_of_FogColor_40() { return &___FogColor_40; }
	inline void set_FogColor_40(int32_t value)
	{
		___FogColor_40 = value;
	}

	inline static int32_t get_offset_of_FogParams_41() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___FogParams_41)); }
	inline int32_t get_FogParams_41() const { return ___FogParams_41; }
	inline int32_t* get_address_of_FogParams_41() { return &___FogParams_41; }
	inline void set_FogParams_41(int32_t value)
	{
		___FogParams_41 = value;
	}

	inline static int32_t get_offset_of_VelocityScale_42() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___VelocityScale_42)); }
	inline int32_t get_VelocityScale_42() const { return ___VelocityScale_42; }
	inline int32_t* get_address_of_VelocityScale_42() { return &___VelocityScale_42; }
	inline void set_VelocityScale_42(int32_t value)
	{
		___VelocityScale_42 = value;
	}

	inline static int32_t get_offset_of_MaxBlurRadius_43() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___MaxBlurRadius_43)); }
	inline int32_t get_MaxBlurRadius_43() const { return ___MaxBlurRadius_43; }
	inline int32_t* get_address_of_MaxBlurRadius_43() { return &___MaxBlurRadius_43; }
	inline void set_MaxBlurRadius_43(int32_t value)
	{
		___MaxBlurRadius_43 = value;
	}

	inline static int32_t get_offset_of_RcpMaxBlurRadius_44() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___RcpMaxBlurRadius_44)); }
	inline int32_t get_RcpMaxBlurRadius_44() const { return ___RcpMaxBlurRadius_44; }
	inline int32_t* get_address_of_RcpMaxBlurRadius_44() { return &___RcpMaxBlurRadius_44; }
	inline void set_RcpMaxBlurRadius_44(int32_t value)
	{
		___RcpMaxBlurRadius_44 = value;
	}

	inline static int32_t get_offset_of_VelocityTex_45() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___VelocityTex_45)); }
	inline int32_t get_VelocityTex_45() const { return ___VelocityTex_45; }
	inline int32_t* get_address_of_VelocityTex_45() { return &___VelocityTex_45; }
	inline void set_VelocityTex_45(int32_t value)
	{
		___VelocityTex_45 = value;
	}

	inline static int32_t get_offset_of_Tile2RT_46() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Tile2RT_46)); }
	inline int32_t get_Tile2RT_46() const { return ___Tile2RT_46; }
	inline int32_t* get_address_of_Tile2RT_46() { return &___Tile2RT_46; }
	inline void set_Tile2RT_46(int32_t value)
	{
		___Tile2RT_46 = value;
	}

	inline static int32_t get_offset_of_Tile4RT_47() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Tile4RT_47)); }
	inline int32_t get_Tile4RT_47() const { return ___Tile4RT_47; }
	inline int32_t* get_address_of_Tile4RT_47() { return &___Tile4RT_47; }
	inline void set_Tile4RT_47(int32_t value)
	{
		___Tile4RT_47 = value;
	}

	inline static int32_t get_offset_of_Tile8RT_48() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Tile8RT_48)); }
	inline int32_t get_Tile8RT_48() const { return ___Tile8RT_48; }
	inline int32_t* get_address_of_Tile8RT_48() { return &___Tile8RT_48; }
	inline void set_Tile8RT_48(int32_t value)
	{
		___Tile8RT_48 = value;
	}

	inline static int32_t get_offset_of_TileMaxOffs_49() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TileMaxOffs_49)); }
	inline int32_t get_TileMaxOffs_49() const { return ___TileMaxOffs_49; }
	inline int32_t* get_address_of_TileMaxOffs_49() { return &___TileMaxOffs_49; }
	inline void set_TileMaxOffs_49(int32_t value)
	{
		___TileMaxOffs_49 = value;
	}

	inline static int32_t get_offset_of_TileMaxLoop_50() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TileMaxLoop_50)); }
	inline int32_t get_TileMaxLoop_50() const { return ___TileMaxLoop_50; }
	inline int32_t* get_address_of_TileMaxLoop_50() { return &___TileMaxLoop_50; }
	inline void set_TileMaxLoop_50(int32_t value)
	{
		___TileMaxLoop_50 = value;
	}

	inline static int32_t get_offset_of_TileVRT_51() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TileVRT_51)); }
	inline int32_t get_TileVRT_51() const { return ___TileVRT_51; }
	inline int32_t* get_address_of_TileVRT_51() { return &___TileVRT_51; }
	inline void set_TileVRT_51(int32_t value)
	{
		___TileVRT_51 = value;
	}

	inline static int32_t get_offset_of_NeighborMaxTex_52() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___NeighborMaxTex_52)); }
	inline int32_t get_NeighborMaxTex_52() const { return ___NeighborMaxTex_52; }
	inline int32_t* get_address_of_NeighborMaxTex_52() { return &___NeighborMaxTex_52; }
	inline void set_NeighborMaxTex_52(int32_t value)
	{
		___NeighborMaxTex_52 = value;
	}

	inline static int32_t get_offset_of_LoopCount_53() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LoopCount_53)); }
	inline int32_t get_LoopCount_53() const { return ___LoopCount_53; }
	inline int32_t* get_address_of_LoopCount_53() { return &___LoopCount_53; }
	inline void set_LoopCount_53(int32_t value)
	{
		___LoopCount_53 = value;
	}

	inline static int32_t get_offset_of_DepthOfFieldTemp_54() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___DepthOfFieldTemp_54)); }
	inline int32_t get_DepthOfFieldTemp_54() const { return ___DepthOfFieldTemp_54; }
	inline int32_t* get_address_of_DepthOfFieldTemp_54() { return &___DepthOfFieldTemp_54; }
	inline void set_DepthOfFieldTemp_54(int32_t value)
	{
		___DepthOfFieldTemp_54 = value;
	}

	inline static int32_t get_offset_of_DepthOfFieldTex_55() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___DepthOfFieldTex_55)); }
	inline int32_t get_DepthOfFieldTex_55() const { return ___DepthOfFieldTex_55; }
	inline int32_t* get_address_of_DepthOfFieldTex_55() { return &___DepthOfFieldTex_55; }
	inline void set_DepthOfFieldTex_55(int32_t value)
	{
		___DepthOfFieldTex_55 = value;
	}

	inline static int32_t get_offset_of_Distance_56() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Distance_56)); }
	inline int32_t get_Distance_56() const { return ___Distance_56; }
	inline int32_t* get_address_of_Distance_56() { return &___Distance_56; }
	inline void set_Distance_56(int32_t value)
	{
		___Distance_56 = value;
	}

	inline static int32_t get_offset_of_LensCoeff_57() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LensCoeff_57)); }
	inline int32_t get_LensCoeff_57() const { return ___LensCoeff_57; }
	inline int32_t* get_address_of_LensCoeff_57() { return &___LensCoeff_57; }
	inline void set_LensCoeff_57(int32_t value)
	{
		___LensCoeff_57 = value;
	}

	inline static int32_t get_offset_of_MaxCoC_58() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___MaxCoC_58)); }
	inline int32_t get_MaxCoC_58() const { return ___MaxCoC_58; }
	inline int32_t* get_address_of_MaxCoC_58() { return &___MaxCoC_58; }
	inline void set_MaxCoC_58(int32_t value)
	{
		___MaxCoC_58 = value;
	}

	inline static int32_t get_offset_of_RcpMaxCoC_59() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___RcpMaxCoC_59)); }
	inline int32_t get_RcpMaxCoC_59() const { return ___RcpMaxCoC_59; }
	inline int32_t* get_address_of_RcpMaxCoC_59() { return &___RcpMaxCoC_59; }
	inline void set_RcpMaxCoC_59(int32_t value)
	{
		___RcpMaxCoC_59 = value;
	}

	inline static int32_t get_offset_of_RcpAspect_60() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___RcpAspect_60)); }
	inline int32_t get_RcpAspect_60() const { return ___RcpAspect_60; }
	inline int32_t* get_address_of_RcpAspect_60() { return &___RcpAspect_60; }
	inline void set_RcpAspect_60(int32_t value)
	{
		___RcpAspect_60 = value;
	}

	inline static int32_t get_offset_of_CoCTex_61() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___CoCTex_61)); }
	inline int32_t get_CoCTex_61() const { return ___CoCTex_61; }
	inline int32_t* get_address_of_CoCTex_61() { return &___CoCTex_61; }
	inline void set_CoCTex_61(int32_t value)
	{
		___CoCTex_61 = value;
	}

	inline static int32_t get_offset_of_TaaParams_62() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TaaParams_62)); }
	inline int32_t get_TaaParams_62() const { return ___TaaParams_62; }
	inline int32_t* get_address_of_TaaParams_62() { return &___TaaParams_62; }
	inline void set_TaaParams_62(int32_t value)
	{
		___TaaParams_62 = value;
	}

	inline static int32_t get_offset_of_AutoExposureTex_63() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___AutoExposureTex_63)); }
	inline int32_t get_AutoExposureTex_63() const { return ___AutoExposureTex_63; }
	inline int32_t* get_address_of_AutoExposureTex_63() { return &___AutoExposureTex_63; }
	inline void set_AutoExposureTex_63(int32_t value)
	{
		___AutoExposureTex_63 = value;
	}

	inline static int32_t get_offset_of_HistogramBuffer_64() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___HistogramBuffer_64)); }
	inline int32_t get_HistogramBuffer_64() const { return ___HistogramBuffer_64; }
	inline int32_t* get_address_of_HistogramBuffer_64() { return &___HistogramBuffer_64; }
	inline void set_HistogramBuffer_64(int32_t value)
	{
		___HistogramBuffer_64 = value;
	}

	inline static int32_t get_offset_of_Params_65() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Params_65)); }
	inline int32_t get_Params_65() const { return ___Params_65; }
	inline int32_t* get_address_of_Params_65() { return &___Params_65; }
	inline void set_Params_65(int32_t value)
	{
		___Params_65 = value;
	}

	inline static int32_t get_offset_of_ScaleOffsetRes_66() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ScaleOffsetRes_66)); }
	inline int32_t get_ScaleOffsetRes_66() const { return ___ScaleOffsetRes_66; }
	inline int32_t* get_address_of_ScaleOffsetRes_66() { return &___ScaleOffsetRes_66; }
	inline void set_ScaleOffsetRes_66(int32_t value)
	{
		___ScaleOffsetRes_66 = value;
	}

	inline static int32_t get_offset_of_BloomTex_67() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___BloomTex_67)); }
	inline int32_t get_BloomTex_67() const { return ___BloomTex_67; }
	inline int32_t* get_address_of_BloomTex_67() { return &___BloomTex_67; }
	inline void set_BloomTex_67(int32_t value)
	{
		___BloomTex_67 = value;
	}

	inline static int32_t get_offset_of_SampleScale_68() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___SampleScale_68)); }
	inline int32_t get_SampleScale_68() const { return ___SampleScale_68; }
	inline int32_t* get_address_of_SampleScale_68() { return &___SampleScale_68; }
	inline void set_SampleScale_68(int32_t value)
	{
		___SampleScale_68 = value;
	}

	inline static int32_t get_offset_of_Threshold_69() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Threshold_69)); }
	inline int32_t get_Threshold_69() const { return ___Threshold_69; }
	inline int32_t* get_address_of_Threshold_69() { return &___Threshold_69; }
	inline void set_Threshold_69(int32_t value)
	{
		___Threshold_69 = value;
	}

	inline static int32_t get_offset_of_ColorIntensity_70() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ColorIntensity_70)); }
	inline int32_t get_ColorIntensity_70() const { return ___ColorIntensity_70; }
	inline int32_t* get_address_of_ColorIntensity_70() { return &___ColorIntensity_70; }
	inline void set_ColorIntensity_70(int32_t value)
	{
		___ColorIntensity_70 = value;
	}

	inline static int32_t get_offset_of_Bloom_DirtTex_71() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Bloom_DirtTex_71)); }
	inline int32_t get_Bloom_DirtTex_71() const { return ___Bloom_DirtTex_71; }
	inline int32_t* get_address_of_Bloom_DirtTex_71() { return &___Bloom_DirtTex_71; }
	inline void set_Bloom_DirtTex_71(int32_t value)
	{
		___Bloom_DirtTex_71 = value;
	}

	inline static int32_t get_offset_of_Bloom_Settings_72() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Bloom_Settings_72)); }
	inline int32_t get_Bloom_Settings_72() const { return ___Bloom_Settings_72; }
	inline int32_t* get_address_of_Bloom_Settings_72() { return &___Bloom_Settings_72; }
	inline void set_Bloom_Settings_72(int32_t value)
	{
		___Bloom_Settings_72 = value;
	}

	inline static int32_t get_offset_of_Bloom_Color_73() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Bloom_Color_73)); }
	inline int32_t get_Bloom_Color_73() const { return ___Bloom_Color_73; }
	inline int32_t* get_address_of_Bloom_Color_73() { return &___Bloom_Color_73; }
	inline void set_Bloom_Color_73(int32_t value)
	{
		___Bloom_Color_73 = value;
	}

	inline static int32_t get_offset_of_Bloom_DirtTileOffset_74() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Bloom_DirtTileOffset_74)); }
	inline int32_t get_Bloom_DirtTileOffset_74() const { return ___Bloom_DirtTileOffset_74; }
	inline int32_t* get_address_of_Bloom_DirtTileOffset_74() { return &___Bloom_DirtTileOffset_74; }
	inline void set_Bloom_DirtTileOffset_74(int32_t value)
	{
		___Bloom_DirtTileOffset_74 = value;
	}

	inline static int32_t get_offset_of_ChromaticAberration_Amount_75() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ChromaticAberration_Amount_75)); }
	inline int32_t get_ChromaticAberration_Amount_75() const { return ___ChromaticAberration_Amount_75; }
	inline int32_t* get_address_of_ChromaticAberration_Amount_75() { return &___ChromaticAberration_Amount_75; }
	inline void set_ChromaticAberration_Amount_75(int32_t value)
	{
		___ChromaticAberration_Amount_75 = value;
	}

	inline static int32_t get_offset_of_ChromaticAberration_SpectralLut_76() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ChromaticAberration_SpectralLut_76)); }
	inline int32_t get_ChromaticAberration_SpectralLut_76() const { return ___ChromaticAberration_SpectralLut_76; }
	inline int32_t* get_address_of_ChromaticAberration_SpectralLut_76() { return &___ChromaticAberration_SpectralLut_76; }
	inline void set_ChromaticAberration_SpectralLut_76(int32_t value)
	{
		___ChromaticAberration_SpectralLut_76 = value;
	}

	inline static int32_t get_offset_of_Distortion_CenterScale_77() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Distortion_CenterScale_77)); }
	inline int32_t get_Distortion_CenterScale_77() const { return ___Distortion_CenterScale_77; }
	inline int32_t* get_address_of_Distortion_CenterScale_77() { return &___Distortion_CenterScale_77; }
	inline void set_Distortion_CenterScale_77(int32_t value)
	{
		___Distortion_CenterScale_77 = value;
	}

	inline static int32_t get_offset_of_Distortion_Amount_78() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Distortion_Amount_78)); }
	inline int32_t get_Distortion_Amount_78() const { return ___Distortion_Amount_78; }
	inline int32_t* get_address_of_Distortion_Amount_78() { return &___Distortion_Amount_78; }
	inline void set_Distortion_Amount_78(int32_t value)
	{
		___Distortion_Amount_78 = value;
	}

	inline static int32_t get_offset_of_Lut2D_79() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Lut2D_79)); }
	inline int32_t get_Lut2D_79() const { return ___Lut2D_79; }
	inline int32_t* get_address_of_Lut2D_79() { return &___Lut2D_79; }
	inline void set_Lut2D_79(int32_t value)
	{
		___Lut2D_79 = value;
	}

	inline static int32_t get_offset_of_Lut3D_80() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Lut3D_80)); }
	inline int32_t get_Lut3D_80() const { return ___Lut3D_80; }
	inline int32_t* get_address_of_Lut3D_80() { return &___Lut3D_80; }
	inline void set_Lut3D_80(int32_t value)
	{
		___Lut3D_80 = value;
	}

	inline static int32_t get_offset_of_Lut3D_Params_81() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Lut3D_Params_81)); }
	inline int32_t get_Lut3D_Params_81() const { return ___Lut3D_Params_81; }
	inline int32_t* get_address_of_Lut3D_Params_81() { return &___Lut3D_Params_81; }
	inline void set_Lut3D_Params_81(int32_t value)
	{
		___Lut3D_Params_81 = value;
	}

	inline static int32_t get_offset_of_Lut2D_Params_82() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Lut2D_Params_82)); }
	inline int32_t get_Lut2D_Params_82() const { return ___Lut2D_Params_82; }
	inline int32_t* get_address_of_Lut2D_Params_82() { return &___Lut2D_Params_82; }
	inline void set_Lut2D_Params_82(int32_t value)
	{
		___Lut2D_Params_82 = value;
	}

	inline static int32_t get_offset_of_UserLut2D_Params_83() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___UserLut2D_Params_83)); }
	inline int32_t get_UserLut2D_Params_83() const { return ___UserLut2D_Params_83; }
	inline int32_t* get_address_of_UserLut2D_Params_83() { return &___UserLut2D_Params_83; }
	inline void set_UserLut2D_Params_83(int32_t value)
	{
		___UserLut2D_Params_83 = value;
	}

	inline static int32_t get_offset_of_PostExposure_84() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___PostExposure_84)); }
	inline int32_t get_PostExposure_84() const { return ___PostExposure_84; }
	inline int32_t* get_address_of_PostExposure_84() { return &___PostExposure_84; }
	inline void set_PostExposure_84(int32_t value)
	{
		___PostExposure_84 = value;
	}

	inline static int32_t get_offset_of_ColorBalance_85() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ColorBalance_85)); }
	inline int32_t get_ColorBalance_85() const { return ___ColorBalance_85; }
	inline int32_t* get_address_of_ColorBalance_85() { return &___ColorBalance_85; }
	inline void set_ColorBalance_85(int32_t value)
	{
		___ColorBalance_85 = value;
	}

	inline static int32_t get_offset_of_ColorFilter_86() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ColorFilter_86)); }
	inline int32_t get_ColorFilter_86() const { return ___ColorFilter_86; }
	inline int32_t* get_address_of_ColorFilter_86() { return &___ColorFilter_86; }
	inline void set_ColorFilter_86(int32_t value)
	{
		___ColorFilter_86 = value;
	}

	inline static int32_t get_offset_of_HueSatCon_87() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___HueSatCon_87)); }
	inline int32_t get_HueSatCon_87() const { return ___HueSatCon_87; }
	inline int32_t* get_address_of_HueSatCon_87() { return &___HueSatCon_87; }
	inline void set_HueSatCon_87(int32_t value)
	{
		___HueSatCon_87 = value;
	}

	inline static int32_t get_offset_of_Brightness_88() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Brightness_88)); }
	inline int32_t get_Brightness_88() const { return ___Brightness_88; }
	inline int32_t* get_address_of_Brightness_88() { return &___Brightness_88; }
	inline void set_Brightness_88(int32_t value)
	{
		___Brightness_88 = value;
	}

	inline static int32_t get_offset_of_ChannelMixerRed_89() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ChannelMixerRed_89)); }
	inline int32_t get_ChannelMixerRed_89() const { return ___ChannelMixerRed_89; }
	inline int32_t* get_address_of_ChannelMixerRed_89() { return &___ChannelMixerRed_89; }
	inline void set_ChannelMixerRed_89(int32_t value)
	{
		___ChannelMixerRed_89 = value;
	}

	inline static int32_t get_offset_of_ChannelMixerGreen_90() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ChannelMixerGreen_90)); }
	inline int32_t get_ChannelMixerGreen_90() const { return ___ChannelMixerGreen_90; }
	inline int32_t* get_address_of_ChannelMixerGreen_90() { return &___ChannelMixerGreen_90; }
	inline void set_ChannelMixerGreen_90(int32_t value)
	{
		___ChannelMixerGreen_90 = value;
	}

	inline static int32_t get_offset_of_ChannelMixerBlue_91() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ChannelMixerBlue_91)); }
	inline int32_t get_ChannelMixerBlue_91() const { return ___ChannelMixerBlue_91; }
	inline int32_t* get_address_of_ChannelMixerBlue_91() { return &___ChannelMixerBlue_91; }
	inline void set_ChannelMixerBlue_91(int32_t value)
	{
		___ChannelMixerBlue_91 = value;
	}

	inline static int32_t get_offset_of_Lift_92() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Lift_92)); }
	inline int32_t get_Lift_92() const { return ___Lift_92; }
	inline int32_t* get_address_of_Lift_92() { return &___Lift_92; }
	inline void set_Lift_92(int32_t value)
	{
		___Lift_92 = value;
	}

	inline static int32_t get_offset_of_InvGamma_93() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___InvGamma_93)); }
	inline int32_t get_InvGamma_93() const { return ___InvGamma_93; }
	inline int32_t* get_address_of_InvGamma_93() { return &___InvGamma_93; }
	inline void set_InvGamma_93(int32_t value)
	{
		___InvGamma_93 = value;
	}

	inline static int32_t get_offset_of_Gain_94() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Gain_94)); }
	inline int32_t get_Gain_94() const { return ___Gain_94; }
	inline int32_t* get_address_of_Gain_94() { return &___Gain_94; }
	inline void set_Gain_94(int32_t value)
	{
		___Gain_94 = value;
	}

	inline static int32_t get_offset_of_Curves_95() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Curves_95)); }
	inline int32_t get_Curves_95() const { return ___Curves_95; }
	inline int32_t* get_address_of_Curves_95() { return &___Curves_95; }
	inline void set_Curves_95(int32_t value)
	{
		___Curves_95 = value;
	}

	inline static int32_t get_offset_of_CustomToneCurve_96() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___CustomToneCurve_96)); }
	inline int32_t get_CustomToneCurve_96() const { return ___CustomToneCurve_96; }
	inline int32_t* get_address_of_CustomToneCurve_96() { return &___CustomToneCurve_96; }
	inline void set_CustomToneCurve_96(int32_t value)
	{
		___CustomToneCurve_96 = value;
	}

	inline static int32_t get_offset_of_ToeSegmentA_97() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ToeSegmentA_97)); }
	inline int32_t get_ToeSegmentA_97() const { return ___ToeSegmentA_97; }
	inline int32_t* get_address_of_ToeSegmentA_97() { return &___ToeSegmentA_97; }
	inline void set_ToeSegmentA_97(int32_t value)
	{
		___ToeSegmentA_97 = value;
	}

	inline static int32_t get_offset_of_ToeSegmentB_98() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ToeSegmentB_98)); }
	inline int32_t get_ToeSegmentB_98() const { return ___ToeSegmentB_98; }
	inline int32_t* get_address_of_ToeSegmentB_98() { return &___ToeSegmentB_98; }
	inline void set_ToeSegmentB_98(int32_t value)
	{
		___ToeSegmentB_98 = value;
	}

	inline static int32_t get_offset_of_MidSegmentA_99() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___MidSegmentA_99)); }
	inline int32_t get_MidSegmentA_99() const { return ___MidSegmentA_99; }
	inline int32_t* get_address_of_MidSegmentA_99() { return &___MidSegmentA_99; }
	inline void set_MidSegmentA_99(int32_t value)
	{
		___MidSegmentA_99 = value;
	}

	inline static int32_t get_offset_of_MidSegmentB_100() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___MidSegmentB_100)); }
	inline int32_t get_MidSegmentB_100() const { return ___MidSegmentB_100; }
	inline int32_t* get_address_of_MidSegmentB_100() { return &___MidSegmentB_100; }
	inline void set_MidSegmentB_100(int32_t value)
	{
		___MidSegmentB_100 = value;
	}

	inline static int32_t get_offset_of_ShoSegmentA_101() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ShoSegmentA_101)); }
	inline int32_t get_ShoSegmentA_101() const { return ___ShoSegmentA_101; }
	inline int32_t* get_address_of_ShoSegmentA_101() { return &___ShoSegmentA_101; }
	inline void set_ShoSegmentA_101(int32_t value)
	{
		___ShoSegmentA_101 = value;
	}

	inline static int32_t get_offset_of_ShoSegmentB_102() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___ShoSegmentB_102)); }
	inline int32_t get_ShoSegmentB_102() const { return ___ShoSegmentB_102; }
	inline int32_t* get_address_of_ShoSegmentB_102() { return &___ShoSegmentB_102; }
	inline void set_ShoSegmentB_102(int32_t value)
	{
		___ShoSegmentB_102 = value;
	}

	inline static int32_t get_offset_of_Vignette_Color_103() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Vignette_Color_103)); }
	inline int32_t get_Vignette_Color_103() const { return ___Vignette_Color_103; }
	inline int32_t* get_address_of_Vignette_Color_103() { return &___Vignette_Color_103; }
	inline void set_Vignette_Color_103(int32_t value)
	{
		___Vignette_Color_103 = value;
	}

	inline static int32_t get_offset_of_Vignette_Center_104() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Vignette_Center_104)); }
	inline int32_t get_Vignette_Center_104() const { return ___Vignette_Center_104; }
	inline int32_t* get_address_of_Vignette_Center_104() { return &___Vignette_Center_104; }
	inline void set_Vignette_Center_104(int32_t value)
	{
		___Vignette_Center_104 = value;
	}

	inline static int32_t get_offset_of_Vignette_Settings_105() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Vignette_Settings_105)); }
	inline int32_t get_Vignette_Settings_105() const { return ___Vignette_Settings_105; }
	inline int32_t* get_address_of_Vignette_Settings_105() { return &___Vignette_Settings_105; }
	inline void set_Vignette_Settings_105(int32_t value)
	{
		___Vignette_Settings_105 = value;
	}

	inline static int32_t get_offset_of_Vignette_Mask_106() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Vignette_Mask_106)); }
	inline int32_t get_Vignette_Mask_106() const { return ___Vignette_Mask_106; }
	inline int32_t* get_address_of_Vignette_Mask_106() { return &___Vignette_Mask_106; }
	inline void set_Vignette_Mask_106(int32_t value)
	{
		___Vignette_Mask_106 = value;
	}

	inline static int32_t get_offset_of_Vignette_Opacity_107() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Vignette_Opacity_107)); }
	inline int32_t get_Vignette_Opacity_107() const { return ___Vignette_Opacity_107; }
	inline int32_t* get_address_of_Vignette_Opacity_107() { return &___Vignette_Opacity_107; }
	inline void set_Vignette_Opacity_107(int32_t value)
	{
		___Vignette_Opacity_107 = value;
	}

	inline static int32_t get_offset_of_Vignette_Mode_108() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Vignette_Mode_108)); }
	inline int32_t get_Vignette_Mode_108() const { return ___Vignette_Mode_108; }
	inline int32_t* get_address_of_Vignette_Mode_108() { return &___Vignette_Mode_108; }
	inline void set_Vignette_Mode_108(int32_t value)
	{
		___Vignette_Mode_108 = value;
	}

	inline static int32_t get_offset_of_Grain_Params1_109() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Grain_Params1_109)); }
	inline int32_t get_Grain_Params1_109() const { return ___Grain_Params1_109; }
	inline int32_t* get_address_of_Grain_Params1_109() { return &___Grain_Params1_109; }
	inline void set_Grain_Params1_109(int32_t value)
	{
		___Grain_Params1_109 = value;
	}

	inline static int32_t get_offset_of_Grain_Params2_110() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Grain_Params2_110)); }
	inline int32_t get_Grain_Params2_110() const { return ___Grain_Params2_110; }
	inline int32_t* get_address_of_Grain_Params2_110() { return &___Grain_Params2_110; }
	inline void set_Grain_Params2_110(int32_t value)
	{
		___Grain_Params2_110 = value;
	}

	inline static int32_t get_offset_of_GrainTex_111() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___GrainTex_111)); }
	inline int32_t get_GrainTex_111() const { return ___GrainTex_111; }
	inline int32_t* get_address_of_GrainTex_111() { return &___GrainTex_111; }
	inline void set_GrainTex_111(int32_t value)
	{
		___GrainTex_111 = value;
	}

	inline static int32_t get_offset_of_Phase_112() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Phase_112)); }
	inline int32_t get_Phase_112() const { return ___Phase_112; }
	inline int32_t* get_address_of_Phase_112() { return &___Phase_112; }
	inline void set_Phase_112(int32_t value)
	{
		___Phase_112 = value;
	}

	inline static int32_t get_offset_of_LumaInAlpha_113() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___LumaInAlpha_113)); }
	inline int32_t get_LumaInAlpha_113() const { return ___LumaInAlpha_113; }
	inline int32_t* get_address_of_LumaInAlpha_113() { return &___LumaInAlpha_113; }
	inline void set_LumaInAlpha_113(int32_t value)
	{
		___LumaInAlpha_113 = value;
	}

	inline static int32_t get_offset_of_DitheringTex_114() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___DitheringTex_114)); }
	inline int32_t get_DitheringTex_114() const { return ___DitheringTex_114; }
	inline int32_t* get_address_of_DitheringTex_114() { return &___DitheringTex_114; }
	inline void set_DitheringTex_114(int32_t value)
	{
		___DitheringTex_114 = value;
	}

	inline static int32_t get_offset_of_Dithering_Coords_115() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Dithering_Coords_115)); }
	inline int32_t get_Dithering_Coords_115() const { return ___Dithering_Coords_115; }
	inline int32_t* get_address_of_Dithering_Coords_115() { return &___Dithering_Coords_115; }
	inline void set_Dithering_Coords_115(int32_t value)
	{
		___Dithering_Coords_115 = value;
	}

	inline static int32_t get_offset_of_From_116() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___From_116)); }
	inline int32_t get_From_116() const { return ___From_116; }
	inline int32_t* get_address_of_From_116() { return &___From_116; }
	inline void set_From_116(int32_t value)
	{
		___From_116 = value;
	}

	inline static int32_t get_offset_of_To_117() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___To_117)); }
	inline int32_t get_To_117() const { return ___To_117; }
	inline int32_t* get_address_of_To_117() { return &___To_117; }
	inline void set_To_117(int32_t value)
	{
		___To_117 = value;
	}

	inline static int32_t get_offset_of_Interp_118() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___Interp_118)); }
	inline int32_t get_Interp_118() const { return ___Interp_118; }
	inline int32_t* get_address_of_Interp_118() { return &___Interp_118; }
	inline void set_Interp_118(int32_t value)
	{
		___Interp_118 = value;
	}

	inline static int32_t get_offset_of_TargetColor_119() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___TargetColor_119)); }
	inline int32_t get_TargetColor_119() const { return ___TargetColor_119; }
	inline int32_t* get_address_of_TargetColor_119() { return &___TargetColor_119; }
	inline void set_TargetColor_119(int32_t value)
	{
		___TargetColor_119 = value;
	}

	inline static int32_t get_offset_of_HalfResFinalCopy_120() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___HalfResFinalCopy_120)); }
	inline int32_t get_HalfResFinalCopy_120() const { return ___HalfResFinalCopy_120; }
	inline int32_t* get_address_of_HalfResFinalCopy_120() { return &___HalfResFinalCopy_120; }
	inline void set_HalfResFinalCopy_120(int32_t value)
	{
		___HalfResFinalCopy_120 = value;
	}

	inline static int32_t get_offset_of_WaveformSource_121() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___WaveformSource_121)); }
	inline int32_t get_WaveformSource_121() const { return ___WaveformSource_121; }
	inline int32_t* get_address_of_WaveformSource_121() { return &___WaveformSource_121; }
	inline void set_WaveformSource_121(int32_t value)
	{
		___WaveformSource_121 = value;
	}

	inline static int32_t get_offset_of_WaveformBuffer_122() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___WaveformBuffer_122)); }
	inline int32_t get_WaveformBuffer_122() const { return ___WaveformBuffer_122; }
	inline int32_t* get_address_of_WaveformBuffer_122() { return &___WaveformBuffer_122; }
	inline void set_WaveformBuffer_122(int32_t value)
	{
		___WaveformBuffer_122 = value;
	}

	inline static int32_t get_offset_of_VectorscopeBuffer_123() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___VectorscopeBuffer_123)); }
	inline int32_t get_VectorscopeBuffer_123() const { return ___VectorscopeBuffer_123; }
	inline int32_t* get_address_of_VectorscopeBuffer_123() { return &___VectorscopeBuffer_123; }
	inline void set_VectorscopeBuffer_123(int32_t value)
	{
		___VectorscopeBuffer_123 = value;
	}

	inline static int32_t get_offset_of_RenderViewportScaleFactor_124() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___RenderViewportScaleFactor_124)); }
	inline int32_t get_RenderViewportScaleFactor_124() const { return ___RenderViewportScaleFactor_124; }
	inline int32_t* get_address_of_RenderViewportScaleFactor_124() { return &___RenderViewportScaleFactor_124; }
	inline void set_RenderViewportScaleFactor_124(int32_t value)
	{
		___RenderViewportScaleFactor_124 = value;
	}

	inline static int32_t get_offset_of_UVTransform_125() { return static_cast<int32_t>(offsetof(ShaderIDs_t2844105293_StaticFields, ___UVTransform_125)); }
	inline int32_t get_UVTransform_125() const { return ___UVTransform_125; }
	inline int32_t* get_address_of_UVTransform_125() { return &___UVTransform_125; }
	inline void set_UVTransform_125(int32_t value)
	{
		___UVTransform_125 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERIDS_T2844105293_H
#ifndef SPLINE_T3835237600_H
#define SPLINE_T3835237600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Spline
struct  Spline_t3835237600  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve UnityEngine.Rendering.PostProcessing.Spline::curve
	AnimationCurve_t3046754366 * ___curve_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.Spline::m_Loop
	bool ___m_Loop_1;
	// System.Single UnityEngine.Rendering.PostProcessing.Spline::m_ZeroValue
	float ___m_ZeroValue_2;
	// System.Single UnityEngine.Rendering.PostProcessing.Spline::m_Range
	float ___m_Range_3;
	// UnityEngine.AnimationCurve UnityEngine.Rendering.PostProcessing.Spline::m_InternalLoopingCurve
	AnimationCurve_t3046754366 * ___m_InternalLoopingCurve_4;
	// System.Int32 UnityEngine.Rendering.PostProcessing.Spline::frameCount
	int32_t ___frameCount_5;
	// System.Single[] UnityEngine.Rendering.PostProcessing.Spline::cachedData
	SingleU5BU5D_t1444911251* ___cachedData_6;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___curve_0)); }
	inline AnimationCurve_t3046754366 * get_curve_0() const { return ___curve_0; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(AnimationCurve_t3046754366 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_m_Loop_1() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___m_Loop_1)); }
	inline bool get_m_Loop_1() const { return ___m_Loop_1; }
	inline bool* get_address_of_m_Loop_1() { return &___m_Loop_1; }
	inline void set_m_Loop_1(bool value)
	{
		___m_Loop_1 = value;
	}

	inline static int32_t get_offset_of_m_ZeroValue_2() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___m_ZeroValue_2)); }
	inline float get_m_ZeroValue_2() const { return ___m_ZeroValue_2; }
	inline float* get_address_of_m_ZeroValue_2() { return &___m_ZeroValue_2; }
	inline void set_m_ZeroValue_2(float value)
	{
		___m_ZeroValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Range_3() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___m_Range_3)); }
	inline float get_m_Range_3() const { return ___m_Range_3; }
	inline float* get_address_of_m_Range_3() { return &___m_Range_3; }
	inline void set_m_Range_3(float value)
	{
		___m_Range_3 = value;
	}

	inline static int32_t get_offset_of_m_InternalLoopingCurve_4() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___m_InternalLoopingCurve_4)); }
	inline AnimationCurve_t3046754366 * get_m_InternalLoopingCurve_4() const { return ___m_InternalLoopingCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_InternalLoopingCurve_4() { return &___m_InternalLoopingCurve_4; }
	inline void set_m_InternalLoopingCurve_4(AnimationCurve_t3046754366 * value)
	{
		___m_InternalLoopingCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLoopingCurve_4), value);
	}

	inline static int32_t get_offset_of_frameCount_5() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___frameCount_5)); }
	inline int32_t get_frameCount_5() const { return ___frameCount_5; }
	inline int32_t* get_address_of_frameCount_5() { return &___frameCount_5; }
	inline void set_frameCount_5(int32_t value)
	{
		___frameCount_5 = value;
	}

	inline static int32_t get_offset_of_cachedData_6() { return static_cast<int32_t>(offsetof(Spline_t3835237600, ___cachedData_6)); }
	inline SingleU5BU5D_t1444911251* get_cachedData_6() const { return ___cachedData_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_cachedData_6() { return &___cachedData_6; }
	inline void set_cachedData_6(SingleU5BU5D_t1444911251* value)
	{
		___cachedData_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINE_T3835237600_H
#ifndef TARGETPOOL_T1535233241_H
#define TARGETPOOL_T1535233241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TargetPool
struct  TargetPool_t1535233241  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Rendering.PostProcessing.TargetPool::m_Pool
	List_1_t128053199 * ___m_Pool_0;
	// System.Int32 UnityEngine.Rendering.PostProcessing.TargetPool::m_Current
	int32_t ___m_Current_1;

public:
	inline static int32_t get_offset_of_m_Pool_0() { return static_cast<int32_t>(offsetof(TargetPool_t1535233241, ___m_Pool_0)); }
	inline List_1_t128053199 * get_m_Pool_0() const { return ___m_Pool_0; }
	inline List_1_t128053199 ** get_address_of_m_Pool_0() { return &___m_Pool_0; }
	inline void set_m_Pool_0(List_1_t128053199 * value)
	{
		___m_Pool_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pool_0), value);
	}

	inline static int32_t get_offset_of_m_Current_1() { return static_cast<int32_t>(offsetof(TargetPool_t1535233241, ___m_Current_1)); }
	inline int32_t get_m_Current_1() const { return ___m_Current_1; }
	inline int32_t* get_address_of_m_Current_1() { return &___m_Current_1; }
	inline void set_m_Current_1(int32_t value)
	{
		___m_Current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETPOOL_T1535233241_H
#ifndef TEXTUREFORMATUTILITIES_T2217912845_H
#define TEXTUREFORMATUTILITIES_T2217912845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureFormatUtilities
struct  TextureFormatUtilities_t2217912845  : public RuntimeObject
{
public:

public:
};

struct TextureFormatUtilities_t2217912845_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.RenderTextureFormat> UnityEngine.Rendering.PostProcessing.TextureFormatUtilities::s_FormatAliasMap
	Dictionary_2_t4146031392 * ___s_FormatAliasMap_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Rendering.PostProcessing.TextureFormatUtilities::s_SupportedRenderTextureFormats
	Dictionary_2_t3280968592 * ___s_SupportedRenderTextureFormats_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Rendering.PostProcessing.TextureFormatUtilities::s_SupportedTextureFormats
	Dictionary_2_t3280968592 * ___s_SupportedTextureFormats_2;

public:
	inline static int32_t get_offset_of_s_FormatAliasMap_0() { return static_cast<int32_t>(offsetof(TextureFormatUtilities_t2217912845_StaticFields, ___s_FormatAliasMap_0)); }
	inline Dictionary_2_t4146031392 * get_s_FormatAliasMap_0() const { return ___s_FormatAliasMap_0; }
	inline Dictionary_2_t4146031392 ** get_address_of_s_FormatAliasMap_0() { return &___s_FormatAliasMap_0; }
	inline void set_s_FormatAliasMap_0(Dictionary_2_t4146031392 * value)
	{
		___s_FormatAliasMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_FormatAliasMap_0), value);
	}

	inline static int32_t get_offset_of_s_SupportedRenderTextureFormats_1() { return static_cast<int32_t>(offsetof(TextureFormatUtilities_t2217912845_StaticFields, ___s_SupportedRenderTextureFormats_1)); }
	inline Dictionary_2_t3280968592 * get_s_SupportedRenderTextureFormats_1() const { return ___s_SupportedRenderTextureFormats_1; }
	inline Dictionary_2_t3280968592 ** get_address_of_s_SupportedRenderTextureFormats_1() { return &___s_SupportedRenderTextureFormats_1; }
	inline void set_s_SupportedRenderTextureFormats_1(Dictionary_2_t3280968592 * value)
	{
		___s_SupportedRenderTextureFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_SupportedRenderTextureFormats_1), value);
	}

	inline static int32_t get_offset_of_s_SupportedTextureFormats_2() { return static_cast<int32_t>(offsetof(TextureFormatUtilities_t2217912845_StaticFields, ___s_SupportedTextureFormats_2)); }
	inline Dictionary_2_t3280968592 * get_s_SupportedTextureFormats_2() const { return ___s_SupportedTextureFormats_2; }
	inline Dictionary_2_t3280968592 ** get_address_of_s_SupportedTextureFormats_2() { return &___s_SupportedTextureFormats_2; }
	inline void set_s_SupportedTextureFormats_2(Dictionary_2_t3280968592 * value)
	{
		___s_SupportedTextureFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_SupportedTextureFormats_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMATUTILITIES_T2217912845_H
#ifndef TEXTURELERPER_T1948079985_H
#define TEXTURELERPER_T1948079985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureLerper
struct  TextureLerper_t1948079985  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.TextureLerper::m_Command
	CommandBuffer_t2206337031 * ___m_Command_1;
	// UnityEngine.Rendering.PostProcessing.PropertySheetFactory UnityEngine.Rendering.PostProcessing.TextureLerper::m_PropertySheets
	PropertySheetFactory_t1490101248 * ___m_PropertySheets_2;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.TextureLerper::m_Resources
	PostProcessResources_t1163236733 * ___m_Resources_3;
	// System.Collections.Generic.List`1<UnityEngine.RenderTexture> UnityEngine.Rendering.PostProcessing.TextureLerper::m_Recycled
	List_1_t3580962175 * ___m_Recycled_4;
	// System.Collections.Generic.List`1<UnityEngine.RenderTexture> UnityEngine.Rendering.PostProcessing.TextureLerper::m_Actives
	List_1_t3580962175 * ___m_Actives_5;

public:
	inline static int32_t get_offset_of_m_Command_1() { return static_cast<int32_t>(offsetof(TextureLerper_t1948079985, ___m_Command_1)); }
	inline CommandBuffer_t2206337031 * get_m_Command_1() const { return ___m_Command_1; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_Command_1() { return &___m_Command_1; }
	inline void set_m_Command_1(CommandBuffer_t2206337031 * value)
	{
		___m_Command_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Command_1), value);
	}

	inline static int32_t get_offset_of_m_PropertySheets_2() { return static_cast<int32_t>(offsetof(TextureLerper_t1948079985, ___m_PropertySheets_2)); }
	inline PropertySheetFactory_t1490101248 * get_m_PropertySheets_2() const { return ___m_PropertySheets_2; }
	inline PropertySheetFactory_t1490101248 ** get_address_of_m_PropertySheets_2() { return &___m_PropertySheets_2; }
	inline void set_m_PropertySheets_2(PropertySheetFactory_t1490101248 * value)
	{
		___m_PropertySheets_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheets_2), value);
	}

	inline static int32_t get_offset_of_m_Resources_3() { return static_cast<int32_t>(offsetof(TextureLerper_t1948079985, ___m_Resources_3)); }
	inline PostProcessResources_t1163236733 * get_m_Resources_3() const { return ___m_Resources_3; }
	inline PostProcessResources_t1163236733 ** get_address_of_m_Resources_3() { return &___m_Resources_3; }
	inline void set_m_Resources_3(PostProcessResources_t1163236733 * value)
	{
		___m_Resources_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resources_3), value);
	}

	inline static int32_t get_offset_of_m_Recycled_4() { return static_cast<int32_t>(offsetof(TextureLerper_t1948079985, ___m_Recycled_4)); }
	inline List_1_t3580962175 * get_m_Recycled_4() const { return ___m_Recycled_4; }
	inline List_1_t3580962175 ** get_address_of_m_Recycled_4() { return &___m_Recycled_4; }
	inline void set_m_Recycled_4(List_1_t3580962175 * value)
	{
		___m_Recycled_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Recycled_4), value);
	}

	inline static int32_t get_offset_of_m_Actives_5() { return static_cast<int32_t>(offsetof(TextureLerper_t1948079985, ___m_Actives_5)); }
	inline List_1_t3580962175 * get_m_Actives_5() const { return ___m_Actives_5; }
	inline List_1_t3580962175 ** get_address_of_m_Actives_5() { return &___m_Actives_5; }
	inline void set_m_Actives_5(List_1_t3580962175 * value)
	{
		___m_Actives_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Actives_5), value);
	}
};

struct TextureLerper_t1948079985_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureLerper UnityEngine.Rendering.PostProcessing.TextureLerper::m_Instance
	TextureLerper_t1948079985 * ___m_Instance_0;

public:
	inline static int32_t get_offset_of_m_Instance_0() { return static_cast<int32_t>(offsetof(TextureLerper_t1948079985_StaticFields, ___m_Instance_0)); }
	inline TextureLerper_t1948079985 * get_m_Instance_0() const { return ___m_Instance_0; }
	inline TextureLerper_t1948079985 ** get_address_of_m_Instance_0() { return &___m_Instance_0; }
	inline void set_m_Instance_0(TextureLerper_t1948079985 * value)
	{
		___m_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELERPER_T1948079985_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef U24ARRAYTYPEU3D20_T1702832645_H
#define U24ARRAYTYPEU3D20_T1702832645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832645 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832645__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832645_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COPYOPERATION_T3389193622_H
#define COPYOPERATION_T3389193622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation
struct  CopyOperation_t3389193622 
{
public:
	// System.String UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation::subscript
	String_t* ___subscript_0;
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation::sourceChannel
	int32_t ___sourceChannel_1;
	// System.Int32 UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation::targetChannel
	int32_t ___targetChannel_2;

public:
	inline static int32_t get_offset_of_subscript_0() { return static_cast<int32_t>(offsetof(CopyOperation_t3389193622, ___subscript_0)); }
	inline String_t* get_subscript_0() const { return ___subscript_0; }
	inline String_t** get_address_of_subscript_0() { return &___subscript_0; }
	inline void set_subscript_0(String_t* value)
	{
		___subscript_0 = value;
		Il2CppCodeGenWriteBarrier((&___subscript_0), value);
	}

	inline static int32_t get_offset_of_sourceChannel_1() { return static_cast<int32_t>(offsetof(CopyOperation_t3389193622, ___sourceChannel_1)); }
	inline int32_t get_sourceChannel_1() const { return ___sourceChannel_1; }
	inline int32_t* get_address_of_sourceChannel_1() { return &___sourceChannel_1; }
	inline void set_sourceChannel_1(int32_t value)
	{
		___sourceChannel_1 = value;
	}

	inline static int32_t get_offset_of_targetChannel_2() { return static_cast<int32_t>(offsetof(CopyOperation_t3389193622, ___targetChannel_2)); }
	inline int32_t get_targetChannel_2() const { return ___targetChannel_2; }
	inline int32_t* get_address_of_targetChannel_2() { return &___targetChannel_2; }
	inline void set_targetChannel_2(int32_t value)
	{
		___targetChannel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation
struct CopyOperation_t3389193622_marshaled_pinvoke
{
	char* ___subscript_0;
	int32_t ___sourceChannel_1;
	int32_t ___targetChannel_2;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation
struct CopyOperation_t3389193622_marshaled_com
{
	Il2CppChar* ___subscript_0;
	int32_t ___sourceChannel_1;
	int32_t ___targetChannel_2;
};
#endif // COPYOPERATION_T3389193622_H
#ifndef RECTINT_T1577945902_H
#define RECTINT_T1577945902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RectInt
struct  RectInt_t1577945902 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.RectInt::x
	int32_t ___x_1;
	// System.Int32 UnityEngine.Experimental.Rendering.RectInt::y
	int32_t ___y_2;
	// System.Int32 UnityEngine.Experimental.Rendering.RectInt::width
	int32_t ___width_3;
	// System.Int32 UnityEngine.Experimental.Rendering.RectInt::height
	int32_t ___height_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(RectInt_t1577945902, ___x_1)); }
	inline int32_t get_x_1() const { return ___x_1; }
	inline int32_t* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(int32_t value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(RectInt_t1577945902, ___y_2)); }
	inline int32_t get_y_2() const { return ___y_2; }
	inline int32_t* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(int32_t value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(RectInt_t1577945902, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(RectInt_t1577945902, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}
};

struct RectInt_t1577945902_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.RectInt UnityEngine.Experimental.Rendering.RectInt::zero
	RectInt_t1577945902  ___zero_0;

public:
	inline static int32_t get_offset_of_zero_0() { return static_cast<int32_t>(offsetof(RectInt_t1577945902_StaticFields, ___zero_0)); }
	inline RectInt_t1577945902  get_zero_0() const { return ___zero_0; }
	inline RectInt_t1577945902 * get_address_of_zero_0() { return &___zero_0; }
	inline void set_zero_0(RectInt_t1577945902  value)
	{
		___zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTINT_T1577945902_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef DIRECTPARAMS_T291775049_H
#define DIRECTPARAMS_T291775049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams
struct  DirectParams_t291775049 
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::x0
	float ___x0_0;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::y0
	float ___y0_1;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::x1
	float ___x1_2;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::y1
	float ___y1_3;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::W
	float ___W_4;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::overshootX
	float ___overshootX_5;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::overshootY
	float ___overshootY_6;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve/DirectParams::gamma
	float ___gamma_7;

public:
	inline static int32_t get_offset_of_x0_0() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___x0_0)); }
	inline float get_x0_0() const { return ___x0_0; }
	inline float* get_address_of_x0_0() { return &___x0_0; }
	inline void set_x0_0(float value)
	{
		___x0_0 = value;
	}

	inline static int32_t get_offset_of_y0_1() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___y0_1)); }
	inline float get_y0_1() const { return ___y0_1; }
	inline float* get_address_of_y0_1() { return &___y0_1; }
	inline void set_y0_1(float value)
	{
		___y0_1 = value;
	}

	inline static int32_t get_offset_of_x1_2() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___x1_2)); }
	inline float get_x1_2() const { return ___x1_2; }
	inline float* get_address_of_x1_2() { return &___x1_2; }
	inline void set_x1_2(float value)
	{
		___x1_2 = value;
	}

	inline static int32_t get_offset_of_y1_3() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___y1_3)); }
	inline float get_y1_3() const { return ___y1_3; }
	inline float* get_address_of_y1_3() { return &___y1_3; }
	inline void set_y1_3(float value)
	{
		___y1_3 = value;
	}

	inline static int32_t get_offset_of_W_4() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___W_4)); }
	inline float get_W_4() const { return ___W_4; }
	inline float* get_address_of_W_4() { return &___W_4; }
	inline void set_W_4(float value)
	{
		___W_4 = value;
	}

	inline static int32_t get_offset_of_overshootX_5() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___overshootX_5)); }
	inline float get_overshootX_5() const { return ___overshootX_5; }
	inline float* get_address_of_overshootX_5() { return &___overshootX_5; }
	inline void set_overshootX_5(float value)
	{
		___overshootX_5 = value;
	}

	inline static int32_t get_offset_of_overshootY_6() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___overshootY_6)); }
	inline float get_overshootY_6() const { return ___overshootY_6; }
	inline float* get_address_of_overshootY_6() { return &___overshootY_6; }
	inline void set_overshootY_6(float value)
	{
		___overshootY_6 = value;
	}

	inline static int32_t get_offset_of_gamma_7() { return static_cast<int32_t>(offsetof(DirectParams_t291775049, ___gamma_7)); }
	inline float get_gamma_7() const { return ___gamma_7; }
	inline float* get_address_of_gamma_7() { return &___gamma_7; }
	inline void set_gamma_7(float value)
	{
		___gamma_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPARAMS_T291775049_H
#ifndef PARAMETEROVERRIDE_1_T2372640272_H
#define PARAMETEROVERRIDE_1_T2372640272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Boolean>
struct  ParameterOverride_1_t2372640272  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t2372640272, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T2372640272_H
#ifndef PARAMETEROVERRIDE_1_T931330764_H
#define PARAMETEROVERRIDE_1_T931330764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Int32>
struct  ParameterOverride_1_t931330764  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t931330764, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T931330764_H
#ifndef PARAMETEROVERRIDE_1_T1815622611_H
#define PARAMETEROVERRIDE_1_T1815622611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.Spline>
struct  ParameterOverride_1_t1815622611  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Spline_t3835237600 * ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1815622611, ___value_1)); }
	inline Spline_t3835237600 * get_value_1() const { return ___value_1; }
	inline Spline_t3835237600 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Spline_t3835237600 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1815622611_H
#ifndef PARAMETEROVERRIDE_1_T1642347714_H
#define PARAMETEROVERRIDE_1_T1642347714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Texture>
struct  ParameterOverride_1_t1642347714  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Texture_t3661962703 * ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1642347714, ___value_1)); }
	inline Texture_t3661962703 * get_value_1() const { return ___value_1; }
	inline Texture_t3661962703 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Texture_t3661962703 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1642347714_H
#ifndef POSTPROCESSEVENTCOMPARER_T1947197927_H
#define POSTPROCESSEVENTCOMPARER_T1947197927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEventComparer
struct  PostProcessEventComparer_t1947197927 
{
public:
	union
	{
		struct
		{
		};
		uint8_t PostProcessEventComparer_t1947197927__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEVENTCOMPARER_T1947197927_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255372_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255372  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-0ED907628EE272F93737B500A23D77C9B1C88368
	U24ArrayTypeU3D20_t1702832645  ___U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-727432515AE33ED62A216F2EBFF476490B631B0F
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0)); }
	inline U24ArrayTypeU3D20_t1702832645  get_U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0() const { return ___U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0; }
	inline U24ArrayTypeU3D20_t1702832645 * get_address_of_U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0() { return &___U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0; }
	inline void set_U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0(U24ArrayTypeU3D20_t1702832645  value)
	{
		___U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1() const { return ___U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1() { return &___U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1; }
	inline void set_U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255372_H
#ifndef CUBEMAPFACE_T1358225318_H
#define CUBEMAPFACE_T1358225318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t1358225318 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CubemapFace_t1358225318, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T1358225318_H
#ifndef DEBUGACTION_T229911009_H
#define DEBUGACTION_T229911009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugAction
struct  DebugAction_t229911009 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.DebugAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugAction_t229911009, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGACTION_T229911009_H
#ifndef DEBUGACTIONREPEATMODE_T2259669336_H
#define DEBUGACTIONREPEATMODE_T2259669336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugActionRepeatMode
struct  DebugActionRepeatMode_t2259669336 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.DebugActionRepeatMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugActionRepeatMode_t2259669336, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGACTIONREPEATMODE_T2259669336_H
#ifndef DEBUGACTIONKEYTYPE_T1119546432_H
#define DEBUGACTIONKEYTYPE_T1119546432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugActionState/DebugActionKeyType
struct  DebugActionKeyType_t1119546432 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.DebugActionState/DebugActionKeyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugActionKeyType_t1119546432, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGACTIONKEYTYPE_T1119546432_H
#ifndef FLAGS_T3313364473_H
#define FLAGS_T3313364473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Flags
struct  Flags_t3313364473 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t3313364473, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T3313364473_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RENDERTEXTURECREATIONFLAGS_T557679221_H
#define RENDERTEXTURECREATIONFLAGS_T557679221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureCreationFlags
struct  RenderTextureCreationFlags_t557679221 
{
public:
	// System.Int32 UnityEngine.RenderTextureCreationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureCreationFlags_t557679221, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURECREATIONFLAGS_T557679221_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef RENDERTEXTUREMEMORYLESS_T852891252_H
#define RENDERTEXTUREMEMORYLESS_T852891252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureMemoryless
struct  RenderTextureMemoryless_t852891252 
{
public:
	// System.Int32 UnityEngine.RenderTextureMemoryless::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureMemoryless_t852891252, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREMEMORYLESS_T852891252_H
#ifndef BUILTINRENDERTEXTURETYPE_T2399837169_H
#define BUILTINRENDERTEXTURETYPE_T2399837169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t2399837169 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t2399837169, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T2399837169_H
#ifndef BOOLPARAMETER_T2299103272_H
#define BOOLPARAMETER_T2299103272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BoolParameter
struct  BoolParameter_t2299103272  : public ParameterOverride_1_t2372640272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLPARAMETER_T2299103272_H
#ifndef COLORBLINDNESSTYPE_T2816108808_H
#define COLORBLINDNESSTYPE_T2816108808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorBlindnessType
struct  ColorBlindnessType_t2816108808 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ColorBlindnessType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorBlindnessType_t2816108808, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLINDNESSTYPE_T2816108808_H
#ifndef DEBUGOVERLAY_T1040601139_H
#define DEBUGOVERLAY_T1040601139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DebugOverlay
struct  DebugOverlay_t1040601139 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.DebugOverlay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugOverlay_t1040601139, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGOVERLAY_T1040601139_H
#ifndef INTPARAMETER_T773781776_H
#define INTPARAMETER_T773781776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.IntParameter
struct  IntParameter_t773781776  : public ParameterOverride_1_t931330764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPARAMETER_T773781776_H
#ifndef PARAMETEROVERRIDE_1_T536071335_H
#define PARAMETEROVERRIDE_1_T536071335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Color>
struct  ParameterOverride_1_t536071335  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Color_t2555686324  ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t536071335, ___value_1)); }
	inline Color_t2555686324  get_value_1() const { return ___value_1; }
	inline Color_t2555686324 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Color_t2555686324  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T536071335_H
#ifndef PARAMETEROVERRIDE_1_T136614534_H
#define PARAMETEROVERRIDE_1_T136614534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Vector2>
struct  ParameterOverride_1_t136614534  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Vector2_t2156229523  ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t136614534, ___value_1)); }
	inline Vector2_t2156229523  get_value_1() const { return ___value_1; }
	inline Vector2_t2156229523 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector2_t2156229523  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T136614534_H
#ifndef PARAMETEROVERRIDE_1_T1299413948_H
#define PARAMETEROVERRIDE_1_T1299413948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Vector4>
struct  ParameterOverride_1_t1299413948  : public ParameterOverride_t3061054201
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Vector4_t3319028937  ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t1299413948, ___value_1)); }
	inline Vector4_t3319028937  get_value_1() const { return ___value_1; }
	inline Vector4_t3319028937 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector4_t3319028937  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T1299413948_H
#ifndef POSTPROCESSEVENT_T3532433552_H
#define POSTPROCESSEVENT_T3532433552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEvent
struct  PostProcessEvent_t3532433552 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostProcessEvent_t3532433552, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEVENT_T3532433552_H
#ifndef ANTIALIASING_T455662751_H
#define ANTIALIASING_T455662751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer/Antialiasing
struct  Antialiasing_t455662751 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessLayer/Antialiasing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Antialiasing_t455662751, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASING_T455662751_H
#ifndef SPLINEPARAMETER_T905443520_H
#define SPLINEPARAMETER_T905443520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SplineParameter
struct  SplineParameter_t905443520  : public ParameterOverride_1_t1815622611
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPARAMETER_T905443520_H
#ifndef TEXTUREPARAMETERDEFAULT_T2577489536_H
#define TEXTUREPARAMETERDEFAULT_T2577489536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureParameterDefault
struct  TextureParameterDefault_t2577489536 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.TextureParameterDefault::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureParameterDefault_t2577489536, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPARAMETERDEFAULT_T2577489536_H
#ifndef SHADOWSAMPLINGMODE_T838715745_H
#define SHADOWSAMPLINGMODE_T838715745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ShadowSamplingMode
struct  ShadowSamplingMode_t838715745 
{
public:
	// System.Int32 UnityEngine.Rendering.ShadowSamplingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowSamplingMode_t838715745, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSAMPLINGMODE_T838715745_H
#ifndef TEXTUREDIMENSION_T3933106086_H
#define TEXTUREDIMENSION_T3933106086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.TextureDimension
struct  TextureDimension_t3933106086 
{
public:
	// System.Int32 UnityEngine.Rendering.TextureDimension::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureDimension_t3933106086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREDIMENSION_T3933106086_H
#ifndef VRTEXTUREUSAGE_T3142149582_H
#define VRTEXTUREUSAGE_T3142149582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VRTextureUsage
struct  VRTextureUsage_t3142149582 
{
public:
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRTextureUsage_t3142149582, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRTEXTUREUSAGE_T3142149582_H
#ifndef GAMEVIEWRENDERMODE_T3810192523_H
#define GAMEVIEWRENDERMODE_T3810192523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.GameViewRenderMode
struct  GameViewRenderMode_t3810192523 
{
public:
	// System.Int32 UnityEngine.XR.GameViewRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameViewRenderMode_t3810192523, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEVIEWRENDERMODE_T3810192523_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DEBUGACTIONDESC_T1210212492_H
#define DEBUGACTIONDESC_T1210212492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugActionDesc
struct  DebugActionDesc_t1210212492  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String[]> UnityEngine.Experimental.Rendering.DebugActionDesc::buttonTriggerList
	List_1_t2753864082 * ___buttonTriggerList_0;
	// System.String UnityEngine.Experimental.Rendering.DebugActionDesc::axisTrigger
	String_t* ___axisTrigger_1;
	// System.Collections.Generic.List`1<UnityEngine.KeyCode[]> UnityEngine.Experimental.Rendering.DebugActionDesc::keyTriggerList
	List_1_t3695308798 * ___keyTriggerList_2;
	// UnityEngine.Experimental.Rendering.DebugActionRepeatMode UnityEngine.Experimental.Rendering.DebugActionDesc::repeatMode
	int32_t ___repeatMode_3;
	// System.Single UnityEngine.Experimental.Rendering.DebugActionDesc::repeatDelay
	float ___repeatDelay_4;

public:
	inline static int32_t get_offset_of_buttonTriggerList_0() { return static_cast<int32_t>(offsetof(DebugActionDesc_t1210212492, ___buttonTriggerList_0)); }
	inline List_1_t2753864082 * get_buttonTriggerList_0() const { return ___buttonTriggerList_0; }
	inline List_1_t2753864082 ** get_address_of_buttonTriggerList_0() { return &___buttonTriggerList_0; }
	inline void set_buttonTriggerList_0(List_1_t2753864082 * value)
	{
		___buttonTriggerList_0 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTriggerList_0), value);
	}

	inline static int32_t get_offset_of_axisTrigger_1() { return static_cast<int32_t>(offsetof(DebugActionDesc_t1210212492, ___axisTrigger_1)); }
	inline String_t* get_axisTrigger_1() const { return ___axisTrigger_1; }
	inline String_t** get_address_of_axisTrigger_1() { return &___axisTrigger_1; }
	inline void set_axisTrigger_1(String_t* value)
	{
		___axisTrigger_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisTrigger_1), value);
	}

	inline static int32_t get_offset_of_keyTriggerList_2() { return static_cast<int32_t>(offsetof(DebugActionDesc_t1210212492, ___keyTriggerList_2)); }
	inline List_1_t3695308798 * get_keyTriggerList_2() const { return ___keyTriggerList_2; }
	inline List_1_t3695308798 ** get_address_of_keyTriggerList_2() { return &___keyTriggerList_2; }
	inline void set_keyTriggerList_2(List_1_t3695308798 * value)
	{
		___keyTriggerList_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyTriggerList_2), value);
	}

	inline static int32_t get_offset_of_repeatMode_3() { return static_cast<int32_t>(offsetof(DebugActionDesc_t1210212492, ___repeatMode_3)); }
	inline int32_t get_repeatMode_3() const { return ___repeatMode_3; }
	inline int32_t* get_address_of_repeatMode_3() { return &___repeatMode_3; }
	inline void set_repeatMode_3(int32_t value)
	{
		___repeatMode_3 = value;
	}

	inline static int32_t get_offset_of_repeatDelay_4() { return static_cast<int32_t>(offsetof(DebugActionDesc_t1210212492, ___repeatDelay_4)); }
	inline float get_repeatDelay_4() const { return ___repeatDelay_4; }
	inline float* get_address_of_repeatDelay_4() { return &___repeatDelay_4; }
	inline void set_repeatDelay_4(float value)
	{
		___repeatDelay_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGACTIONDESC_T1210212492_H
#ifndef DEBUGACTIONSTATE_T1357925847_H
#define DEBUGACTIONSTATE_T1357925847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugActionState
struct  DebugActionState_t1357925847  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.DebugActionState/DebugActionKeyType UnityEngine.Experimental.Rendering.DebugActionState::m_Type
	int32_t ___m_Type_0;
	// System.String[] UnityEngine.Experimental.Rendering.DebugActionState::m_PressedButtons
	StringU5BU5D_t1281789340* ___m_PressedButtons_1;
	// System.String UnityEngine.Experimental.Rendering.DebugActionState::m_PressedAxis
	String_t* ___m_PressedAxis_2;
	// UnityEngine.KeyCode[] UnityEngine.Experimental.Rendering.DebugActionState::m_PressedKeys
	KeyCodeU5BU5D_t2223234056* ___m_PressedKeys_3;
	// System.Boolean[] UnityEngine.Experimental.Rendering.DebugActionState::m_TriggerPressedUp
	BooleanU5BU5D_t2897418192* ___m_TriggerPressedUp_4;
	// System.Single UnityEngine.Experimental.Rendering.DebugActionState::m_Timer
	float ___m_Timer_5;
	// System.Boolean UnityEngine.Experimental.Rendering.DebugActionState::<runningAction>k__BackingField
	bool ___U3CrunningActionU3Ek__BackingField_6;
	// System.Single UnityEngine.Experimental.Rendering.DebugActionState::<actionState>k__BackingField
	float ___U3CactionStateU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_PressedButtons_1() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___m_PressedButtons_1)); }
	inline StringU5BU5D_t1281789340* get_m_PressedButtons_1() const { return ___m_PressedButtons_1; }
	inline StringU5BU5D_t1281789340** get_address_of_m_PressedButtons_1() { return &___m_PressedButtons_1; }
	inline void set_m_PressedButtons_1(StringU5BU5D_t1281789340* value)
	{
		___m_PressedButtons_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedButtons_1), value);
	}

	inline static int32_t get_offset_of_m_PressedAxis_2() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___m_PressedAxis_2)); }
	inline String_t* get_m_PressedAxis_2() const { return ___m_PressedAxis_2; }
	inline String_t** get_address_of_m_PressedAxis_2() { return &___m_PressedAxis_2; }
	inline void set_m_PressedAxis_2(String_t* value)
	{
		___m_PressedAxis_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedAxis_2), value);
	}

	inline static int32_t get_offset_of_m_PressedKeys_3() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___m_PressedKeys_3)); }
	inline KeyCodeU5BU5D_t2223234056* get_m_PressedKeys_3() const { return ___m_PressedKeys_3; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_m_PressedKeys_3() { return &___m_PressedKeys_3; }
	inline void set_m_PressedKeys_3(KeyCodeU5BU5D_t2223234056* value)
	{
		___m_PressedKeys_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedKeys_3), value);
	}

	inline static int32_t get_offset_of_m_TriggerPressedUp_4() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___m_TriggerPressedUp_4)); }
	inline BooleanU5BU5D_t2897418192* get_m_TriggerPressedUp_4() const { return ___m_TriggerPressedUp_4; }
	inline BooleanU5BU5D_t2897418192** get_address_of_m_TriggerPressedUp_4() { return &___m_TriggerPressedUp_4; }
	inline void set_m_TriggerPressedUp_4(BooleanU5BU5D_t2897418192* value)
	{
		___m_TriggerPressedUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerPressedUp_4), value);
	}

	inline static int32_t get_offset_of_m_Timer_5() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___m_Timer_5)); }
	inline float get_m_Timer_5() const { return ___m_Timer_5; }
	inline float* get_address_of_m_Timer_5() { return &___m_Timer_5; }
	inline void set_m_Timer_5(float value)
	{
		___m_Timer_5 = value;
	}

	inline static int32_t get_offset_of_U3CrunningActionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___U3CrunningActionU3Ek__BackingField_6)); }
	inline bool get_U3CrunningActionU3Ek__BackingField_6() const { return ___U3CrunningActionU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CrunningActionU3Ek__BackingField_6() { return &___U3CrunningActionU3Ek__BackingField_6; }
	inline void set_U3CrunningActionU3Ek__BackingField_6(bool value)
	{
		___U3CrunningActionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CactionStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DebugActionState_t1357925847, ___U3CactionStateU3Ek__BackingField_7)); }
	inline float get_U3CactionStateU3Ek__BackingField_7() const { return ___U3CactionStateU3Ek__BackingField_7; }
	inline float* get_address_of_U3CactionStateU3Ek__BackingField_7() { return &___U3CactionStateU3Ek__BackingField_7; }
	inline void set_U3CactionStateU3Ek__BackingField_7(float value)
	{
		___U3CactionStateU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGACTIONSTATE_T1357925847_H
#ifndef PANEL_T835245858_H
#define PANEL_T835245858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Panel
struct  Panel_t835245858  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.DebugUI/Flags UnityEngine.Experimental.Rendering.DebugUI/Panel::<flags>k__BackingField
	int32_t ___U3CflagsU3Ek__BackingField_0;
	// System.String UnityEngine.Experimental.Rendering.DebugUI/Panel::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_1;
	// UnityEngine.Experimental.Rendering.ObservableList`1<UnityEngine.Experimental.Rendering.DebugUI/Widget> UnityEngine.Experimental.Rendering.DebugUI/Panel::<children>k__BackingField
	ObservableList_1_t535108294 * ___U3CchildrenU3Ek__BackingField_2;
	// System.Action`1<UnityEngine.Experimental.Rendering.DebugUI/Panel> UnityEngine.Experimental.Rendering.DebugUI/Panel::onSetDirty
	Action_1_t1007713453 * ___onSetDirty_3;

public:
	inline static int32_t get_offset_of_U3CflagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Panel_t835245858, ___U3CflagsU3Ek__BackingField_0)); }
	inline int32_t get_U3CflagsU3Ek__BackingField_0() const { return ___U3CflagsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CflagsU3Ek__BackingField_0() { return &___U3CflagsU3Ek__BackingField_0; }
	inline void set_U3CflagsU3Ek__BackingField_0(int32_t value)
	{
		___U3CflagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Panel_t835245858, ___U3CdisplayNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_1() const { return ___U3CdisplayNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_1() { return &___U3CdisplayNameU3Ek__BackingField_1; }
	inline void set_U3CdisplayNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdisplayNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CchildrenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Panel_t835245858, ___U3CchildrenU3Ek__BackingField_2)); }
	inline ObservableList_1_t535108294 * get_U3CchildrenU3Ek__BackingField_2() const { return ___U3CchildrenU3Ek__BackingField_2; }
	inline ObservableList_1_t535108294 ** get_address_of_U3CchildrenU3Ek__BackingField_2() { return &___U3CchildrenU3Ek__BackingField_2; }
	inline void set_U3CchildrenU3Ek__BackingField_2(ObservableList_1_t535108294 * value)
	{
		___U3CchildrenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchildrenU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_onSetDirty_3() { return static_cast<int32_t>(offsetof(Panel_t835245858, ___onSetDirty_3)); }
	inline Action_1_t1007713453 * get_onSetDirty_3() const { return ___onSetDirty_3; }
	inline Action_1_t1007713453 ** get_address_of_onSetDirty_3() { return &___onSetDirty_3; }
	inline void set_onSetDirty_3(Action_1_t1007713453 * value)
	{
		___onSetDirty_3 = value;
		Il2CppCodeGenWriteBarrier((&___onSetDirty_3), value);
	}
};

struct Panel_t835245858_StaticFields
{
public:
	// System.Action`1<UnityEngine.Experimental.Rendering.DebugUI/Panel> UnityEngine.Experimental.Rendering.DebugUI/Panel::<>f__am$cache0
	Action_1_t1007713453 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(Panel_t835245858_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_1_t1007713453 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_1_t1007713453 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_1_t1007713453 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANEL_T835245858_H
#ifndef WIDGET_T1336755483_H
#define WIDGET_T1336755483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Widget
struct  Widget_t1336755483  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.DebugUI/Panel UnityEngine.Experimental.Rendering.DebugUI/Widget::m_Panel
	Panel_t835245858 * ___m_Panel_0;
	// UnityEngine.Experimental.Rendering.DebugUI/IContainer UnityEngine.Experimental.Rendering.DebugUI/Widget::m_Parent
	RuntimeObject* ___m_Parent_1;
	// UnityEngine.Experimental.Rendering.DebugUI/Flags UnityEngine.Experimental.Rendering.DebugUI/Widget::<flags>k__BackingField
	int32_t ___U3CflagsU3Ek__BackingField_2;
	// System.String UnityEngine.Experimental.Rendering.DebugUI/Widget::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_3;
	// System.String UnityEngine.Experimental.Rendering.DebugUI/Widget::<queryPath>k__BackingField
	String_t* ___U3CqueryPathU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_Panel_0() { return static_cast<int32_t>(offsetof(Widget_t1336755483, ___m_Panel_0)); }
	inline Panel_t835245858 * get_m_Panel_0() const { return ___m_Panel_0; }
	inline Panel_t835245858 ** get_address_of_m_Panel_0() { return &___m_Panel_0; }
	inline void set_m_Panel_0(Panel_t835245858 * value)
	{
		___m_Panel_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Panel_0), value);
	}

	inline static int32_t get_offset_of_m_Parent_1() { return static_cast<int32_t>(offsetof(Widget_t1336755483, ___m_Parent_1)); }
	inline RuntimeObject* get_m_Parent_1() const { return ___m_Parent_1; }
	inline RuntimeObject** get_address_of_m_Parent_1() { return &___m_Parent_1; }
	inline void set_m_Parent_1(RuntimeObject* value)
	{
		___m_Parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_1), value);
	}

	inline static int32_t get_offset_of_U3CflagsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Widget_t1336755483, ___U3CflagsU3Ek__BackingField_2)); }
	inline int32_t get_U3CflagsU3Ek__BackingField_2() const { return ___U3CflagsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CflagsU3Ek__BackingField_2() { return &___U3CflagsU3Ek__BackingField_2; }
	inline void set_U3CflagsU3Ek__BackingField_2(int32_t value)
	{
		___U3CflagsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Widget_t1336755483, ___U3CdisplayNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_3() const { return ___U3CdisplayNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_3() { return &___U3CdisplayNameU3Ek__BackingField_3; }
	inline void set_U3CdisplayNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdisplayNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CqueryPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Widget_t1336755483, ___U3CqueryPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CqueryPathU3Ek__BackingField_4() const { return ___U3CqueryPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CqueryPathU3Ek__BackingField_4() { return &___U3CqueryPathU3Ek__BackingField_4; }
	inline void set_U3CqueryPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CqueryPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CqueryPathU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIDGET_T1336755483_H
#ifndef XRGRAPHICSCONFIG_T170342676_H
#define XRGRAPHICSCONFIG_T170342676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.XRGraphicsConfig
struct  XRGraphicsConfig_t170342676  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Experimental.Rendering.XRGraphicsConfig::renderScale
	float ___renderScale_0;
	// System.Single UnityEngine.Experimental.Rendering.XRGraphicsConfig::viewportScale
	float ___viewportScale_1;
	// System.Boolean UnityEngine.Experimental.Rendering.XRGraphicsConfig::useOcclusionMesh
	bool ___useOcclusionMesh_2;
	// System.Single UnityEngine.Experimental.Rendering.XRGraphicsConfig::occlusionMaskScale
	float ___occlusionMaskScale_3;
	// System.Boolean UnityEngine.Experimental.Rendering.XRGraphicsConfig::showDeviceView
	bool ___showDeviceView_4;
	// UnityEngine.XR.GameViewRenderMode UnityEngine.Experimental.Rendering.XRGraphicsConfig::gameViewRenderMode
	int32_t ___gameViewRenderMode_5;

public:
	inline static int32_t get_offset_of_renderScale_0() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676, ___renderScale_0)); }
	inline float get_renderScale_0() const { return ___renderScale_0; }
	inline float* get_address_of_renderScale_0() { return &___renderScale_0; }
	inline void set_renderScale_0(float value)
	{
		___renderScale_0 = value;
	}

	inline static int32_t get_offset_of_viewportScale_1() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676, ___viewportScale_1)); }
	inline float get_viewportScale_1() const { return ___viewportScale_1; }
	inline float* get_address_of_viewportScale_1() { return &___viewportScale_1; }
	inline void set_viewportScale_1(float value)
	{
		___viewportScale_1 = value;
	}

	inline static int32_t get_offset_of_useOcclusionMesh_2() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676, ___useOcclusionMesh_2)); }
	inline bool get_useOcclusionMesh_2() const { return ___useOcclusionMesh_2; }
	inline bool* get_address_of_useOcclusionMesh_2() { return &___useOcclusionMesh_2; }
	inline void set_useOcclusionMesh_2(bool value)
	{
		___useOcclusionMesh_2 = value;
	}

	inline static int32_t get_offset_of_occlusionMaskScale_3() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676, ___occlusionMaskScale_3)); }
	inline float get_occlusionMaskScale_3() const { return ___occlusionMaskScale_3; }
	inline float* get_address_of_occlusionMaskScale_3() { return &___occlusionMaskScale_3; }
	inline void set_occlusionMaskScale_3(float value)
	{
		___occlusionMaskScale_3 = value;
	}

	inline static int32_t get_offset_of_showDeviceView_4() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676, ___showDeviceView_4)); }
	inline bool get_showDeviceView_4() const { return ___showDeviceView_4; }
	inline bool* get_address_of_showDeviceView_4() { return &___showDeviceView_4; }
	inline void set_showDeviceView_4(bool value)
	{
		___showDeviceView_4 = value;
	}

	inline static int32_t get_offset_of_gameViewRenderMode_5() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676, ___gameViewRenderMode_5)); }
	inline int32_t get_gameViewRenderMode_5() const { return ___gameViewRenderMode_5; }
	inline int32_t* get_address_of_gameViewRenderMode_5() { return &___gameViewRenderMode_5; }
	inline void set_gameViewRenderMode_5(int32_t value)
	{
		___gameViewRenderMode_5 = value;
	}
};

struct XRGraphicsConfig_t170342676_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.XRGraphicsConfig UnityEngine.Experimental.Rendering.XRGraphicsConfig::s_DefaultXRConfig
	XRGraphicsConfig_t170342676 * ___s_DefaultXRConfig_6;

public:
	inline static int32_t get_offset_of_s_DefaultXRConfig_6() { return static_cast<int32_t>(offsetof(XRGraphicsConfig_t170342676_StaticFields, ___s_DefaultXRConfig_6)); }
	inline XRGraphicsConfig_t170342676 * get_s_DefaultXRConfig_6() const { return ___s_DefaultXRConfig_6; }
	inline XRGraphicsConfig_t170342676 ** get_address_of_s_DefaultXRConfig_6() { return &___s_DefaultXRConfig_6; }
	inline void set_s_DefaultXRConfig_6(XRGraphicsConfig_t170342676 * value)
	{
		___s_DefaultXRConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultXRConfig_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRGRAPHICSCONFIG_T170342676_H
#ifndef RENDERTEXTUREDESCRIPTOR_T1974534975_H
#define RENDERTEXTUREDESCRIPTOR_T1974534975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureDescriptor
struct  RenderTextureDescriptor_t1974534975 
{
public:
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// UnityEngine.RenderTextureFormat UnityEngine.RenderTextureDescriptor::<colorFormat>k__BackingField
	int32_t ___U3CcolorFormatU3Ek__BackingField_4;
	// System.Int32 UnityEngine.RenderTextureDescriptor::_depthBufferBits
	int32_t ____depthBufferBits_5;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_7;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_8;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_9;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_10;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CwidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CheightU3Ek__BackingField_1)); }
	inline int32_t get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(int32_t value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmsaaSamplesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CmsaaSamplesU3Ek__BackingField_2)); }
	inline int32_t get_U3CmsaaSamplesU3Ek__BackingField_2() const { return ___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CmsaaSamplesU3Ek__BackingField_2() { return &___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline void set_U3CmsaaSamplesU3Ek__BackingField_2(int32_t value)
	{
		___U3CmsaaSamplesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeDepthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CvolumeDepthU3Ek__BackingField_3)); }
	inline int32_t get_U3CvolumeDepthU3Ek__BackingField_3() const { return ___U3CvolumeDepthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CvolumeDepthU3Ek__BackingField_3() { return &___U3CvolumeDepthU3Ek__BackingField_3; }
	inline void set_U3CvolumeDepthU3Ek__BackingField_3(int32_t value)
	{
		___U3CvolumeDepthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcolorFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CcolorFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CcolorFormatU3Ek__BackingField_4() const { return ___U3CcolorFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CcolorFormatU3Ek__BackingField_4() { return &___U3CcolorFormatU3Ek__BackingField_4; }
	inline void set_U3CcolorFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CcolorFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__depthBufferBits_5() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ____depthBufferBits_5)); }
	inline int32_t get__depthBufferBits_5() const { return ____depthBufferBits_5; }
	inline int32_t* get_address_of__depthBufferBits_5() { return &____depthBufferBits_5; }
	inline void set__depthBufferBits_5(int32_t value)
	{
		____depthBufferBits_5 = value;
	}

	inline static int32_t get_offset_of_U3CdimensionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CdimensionU3Ek__BackingField_7)); }
	inline int32_t get_U3CdimensionU3Ek__BackingField_7() const { return ___U3CdimensionU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CdimensionU3Ek__BackingField_7() { return &___U3CdimensionU3Ek__BackingField_7; }
	inline void set_U3CdimensionU3Ek__BackingField_7(int32_t value)
	{
		___U3CdimensionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CshadowSamplingModeU3Ek__BackingField_8)); }
	inline int32_t get_U3CshadowSamplingModeU3Ek__BackingField_8() const { return ___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return &___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline void set_U3CshadowSamplingModeU3Ek__BackingField_8(int32_t value)
	{
		___U3CshadowSamplingModeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CvrUsageU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CvrUsageU3Ek__BackingField_9)); }
	inline int32_t get_U3CvrUsageU3Ek__BackingField_9() const { return ___U3CvrUsageU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CvrUsageU3Ek__BackingField_9() { return &___U3CvrUsageU3Ek__BackingField_9; }
	inline void set_U3CvrUsageU3Ek__BackingField_9(int32_t value)
	{
		___U3CvrUsageU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of__flags_10() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ____flags_10)); }
	inline int32_t get__flags_10() const { return ____flags_10; }
	inline int32_t* get_address_of__flags_10() { return &____flags_10; }
	inline void set__flags_10(int32_t value)
	{
		____flags_10 = value;
	}

	inline static int32_t get_offset_of_U3CmemorylessU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CmemorylessU3Ek__BackingField_11)); }
	inline int32_t get_U3CmemorylessU3Ek__BackingField_11() const { return ___U3CmemorylessU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CmemorylessU3Ek__BackingField_11() { return &___U3CmemorylessU3Ek__BackingField_11; }
	inline void set_U3CmemorylessU3Ek__BackingField_11(int32_t value)
	{
		___U3CmemorylessU3Ek__BackingField_11 = value;
	}
};

struct RenderTextureDescriptor_t1974534975_StaticFields
{
public:
	// System.Int32[] UnityEngine.RenderTextureDescriptor::depthFormatBits
	Int32U5BU5D_t385246372* ___depthFormatBits_6;

public:
	inline static int32_t get_offset_of_depthFormatBits_6() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975_StaticFields, ___depthFormatBits_6)); }
	inline Int32U5BU5D_t385246372* get_depthFormatBits_6() const { return ___depthFormatBits_6; }
	inline Int32U5BU5D_t385246372** get_address_of_depthFormatBits_6() { return &___depthFormatBits_6; }
	inline void set_depthFormatBits_6(Int32U5BU5D_t385246372* value)
	{
		___depthFormatBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___depthFormatBits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREDESCRIPTOR_T1974534975_H
#ifndef COLORPARAMETER_T2998827320_H
#define COLORPARAMETER_T2998827320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorParameter
struct  ColorParameter_t2998827320  : public ParameterOverride_1_t536071335
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPARAMETER_T2998827320_H
#ifndef POSTPROCESSDEBUGLAYER_T3290441360_H
#define POSTPROCESSDEBUGLAYER_T3290441360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer
struct  PostProcessDebugLayer_t3290441360  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.LightMeterMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::lightMeter
	LightMeterMonitor_t1816308400 * ___lightMeter_0;
	// UnityEngine.Rendering.PostProcessing.HistogramMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::histogram
	HistogramMonitor_t3488597019 * ___histogram_1;
	// UnityEngine.Rendering.PostProcessing.WaveformMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::waveform
	WaveformMonitor_t2029591948 * ___waveform_2;
	// UnityEngine.Rendering.PostProcessing.VectorscopeMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::vectorscope
	VectorscopeMonitor_t2083911122 * ___vectorscope_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.MonitorType,UnityEngine.Rendering.PostProcessing.Monitor> UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::m_Monitors
	Dictionary_2_t679227455 * ___m_Monitors_4;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::frameWidth
	int32_t ___frameWidth_5;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::frameHeight
	int32_t ___frameHeight_6;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::<debugOverlayTarget>k__BackingField
	RenderTexture_t2108887433 * ___U3CdebugOverlayTargetU3Ek__BackingField_7;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::<debugOverlayActive>k__BackingField
	bool ___U3CdebugOverlayActiveU3Ek__BackingField_8;
	// UnityEngine.Rendering.PostProcessing.DebugOverlay UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::<debugOverlay>k__BackingField
	int32_t ___U3CdebugOverlayU3Ek__BackingField_9;
	// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::overlaySettings
	OverlaySettings_t1356898765 * ___overlaySettings_10;

public:
	inline static int32_t get_offset_of_lightMeter_0() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___lightMeter_0)); }
	inline LightMeterMonitor_t1816308400 * get_lightMeter_0() const { return ___lightMeter_0; }
	inline LightMeterMonitor_t1816308400 ** get_address_of_lightMeter_0() { return &___lightMeter_0; }
	inline void set_lightMeter_0(LightMeterMonitor_t1816308400 * value)
	{
		___lightMeter_0 = value;
		Il2CppCodeGenWriteBarrier((&___lightMeter_0), value);
	}

	inline static int32_t get_offset_of_histogram_1() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___histogram_1)); }
	inline HistogramMonitor_t3488597019 * get_histogram_1() const { return ___histogram_1; }
	inline HistogramMonitor_t3488597019 ** get_address_of_histogram_1() { return &___histogram_1; }
	inline void set_histogram_1(HistogramMonitor_t3488597019 * value)
	{
		___histogram_1 = value;
		Il2CppCodeGenWriteBarrier((&___histogram_1), value);
	}

	inline static int32_t get_offset_of_waveform_2() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___waveform_2)); }
	inline WaveformMonitor_t2029591948 * get_waveform_2() const { return ___waveform_2; }
	inline WaveformMonitor_t2029591948 ** get_address_of_waveform_2() { return &___waveform_2; }
	inline void set_waveform_2(WaveformMonitor_t2029591948 * value)
	{
		___waveform_2 = value;
		Il2CppCodeGenWriteBarrier((&___waveform_2), value);
	}

	inline static int32_t get_offset_of_vectorscope_3() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___vectorscope_3)); }
	inline VectorscopeMonitor_t2083911122 * get_vectorscope_3() const { return ___vectorscope_3; }
	inline VectorscopeMonitor_t2083911122 ** get_address_of_vectorscope_3() { return &___vectorscope_3; }
	inline void set_vectorscope_3(VectorscopeMonitor_t2083911122 * value)
	{
		___vectorscope_3 = value;
		Il2CppCodeGenWriteBarrier((&___vectorscope_3), value);
	}

	inline static int32_t get_offset_of_m_Monitors_4() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___m_Monitors_4)); }
	inline Dictionary_2_t679227455 * get_m_Monitors_4() const { return ___m_Monitors_4; }
	inline Dictionary_2_t679227455 ** get_address_of_m_Monitors_4() { return &___m_Monitors_4; }
	inline void set_m_Monitors_4(Dictionary_2_t679227455 * value)
	{
		___m_Monitors_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Monitors_4), value);
	}

	inline static int32_t get_offset_of_frameWidth_5() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___frameWidth_5)); }
	inline int32_t get_frameWidth_5() const { return ___frameWidth_5; }
	inline int32_t* get_address_of_frameWidth_5() { return &___frameWidth_5; }
	inline void set_frameWidth_5(int32_t value)
	{
		___frameWidth_5 = value;
	}

	inline static int32_t get_offset_of_frameHeight_6() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___frameHeight_6)); }
	inline int32_t get_frameHeight_6() const { return ___frameHeight_6; }
	inline int32_t* get_address_of_frameHeight_6() { return &___frameHeight_6; }
	inline void set_frameHeight_6(int32_t value)
	{
		___frameHeight_6 = value;
	}

	inline static int32_t get_offset_of_U3CdebugOverlayTargetU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___U3CdebugOverlayTargetU3Ek__BackingField_7)); }
	inline RenderTexture_t2108887433 * get_U3CdebugOverlayTargetU3Ek__BackingField_7() const { return ___U3CdebugOverlayTargetU3Ek__BackingField_7; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CdebugOverlayTargetU3Ek__BackingField_7() { return &___U3CdebugOverlayTargetU3Ek__BackingField_7; }
	inline void set_U3CdebugOverlayTargetU3Ek__BackingField_7(RenderTexture_t2108887433 * value)
	{
		___U3CdebugOverlayTargetU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdebugOverlayTargetU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdebugOverlayActiveU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___U3CdebugOverlayActiveU3Ek__BackingField_8)); }
	inline bool get_U3CdebugOverlayActiveU3Ek__BackingField_8() const { return ___U3CdebugOverlayActiveU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CdebugOverlayActiveU3Ek__BackingField_8() { return &___U3CdebugOverlayActiveU3Ek__BackingField_8; }
	inline void set_U3CdebugOverlayActiveU3Ek__BackingField_8(bool value)
	{
		___U3CdebugOverlayActiveU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CdebugOverlayU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___U3CdebugOverlayU3Ek__BackingField_9)); }
	inline int32_t get_U3CdebugOverlayU3Ek__BackingField_9() const { return ___U3CdebugOverlayU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CdebugOverlayU3Ek__BackingField_9() { return &___U3CdebugOverlayU3Ek__BackingField_9; }
	inline void set_U3CdebugOverlayU3Ek__BackingField_9(int32_t value)
	{
		___U3CdebugOverlayU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_overlaySettings_10() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t3290441360, ___overlaySettings_10)); }
	inline OverlaySettings_t1356898765 * get_overlaySettings_10() const { return ___overlaySettings_10; }
	inline OverlaySettings_t1356898765 ** get_address_of_overlaySettings_10() { return &___overlaySettings_10; }
	inline void set_overlaySettings_10(OverlaySettings_t1356898765 * value)
	{
		___overlaySettings_10 = value;
		Il2CppCodeGenWriteBarrier((&___overlaySettings_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSDEBUGLAYER_T3290441360_H
#ifndef OVERLAYSETTINGS_T1356898765_H
#define OVERLAYSETTINGS_T1356898765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings
struct  OverlaySettings_t1356898765  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings::linearDepth
	bool ___linearDepth_0;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings::motionColorIntensity
	float ___motionColorIntensity_1;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings::motionGridSize
	int32_t ___motionGridSize_2;
	// UnityEngine.Rendering.PostProcessing.ColorBlindnessType UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings::colorBlindnessType
	int32_t ___colorBlindnessType_3;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings::colorBlindnessStrength
	float ___colorBlindnessStrength_4;

public:
	inline static int32_t get_offset_of_linearDepth_0() { return static_cast<int32_t>(offsetof(OverlaySettings_t1356898765, ___linearDepth_0)); }
	inline bool get_linearDepth_0() const { return ___linearDepth_0; }
	inline bool* get_address_of_linearDepth_0() { return &___linearDepth_0; }
	inline void set_linearDepth_0(bool value)
	{
		___linearDepth_0 = value;
	}

	inline static int32_t get_offset_of_motionColorIntensity_1() { return static_cast<int32_t>(offsetof(OverlaySettings_t1356898765, ___motionColorIntensity_1)); }
	inline float get_motionColorIntensity_1() const { return ___motionColorIntensity_1; }
	inline float* get_address_of_motionColorIntensity_1() { return &___motionColorIntensity_1; }
	inline void set_motionColorIntensity_1(float value)
	{
		___motionColorIntensity_1 = value;
	}

	inline static int32_t get_offset_of_motionGridSize_2() { return static_cast<int32_t>(offsetof(OverlaySettings_t1356898765, ___motionGridSize_2)); }
	inline int32_t get_motionGridSize_2() const { return ___motionGridSize_2; }
	inline int32_t* get_address_of_motionGridSize_2() { return &___motionGridSize_2; }
	inline void set_motionGridSize_2(int32_t value)
	{
		___motionGridSize_2 = value;
	}

	inline static int32_t get_offset_of_colorBlindnessType_3() { return static_cast<int32_t>(offsetof(OverlaySettings_t1356898765, ___colorBlindnessType_3)); }
	inline int32_t get_colorBlindnessType_3() const { return ___colorBlindnessType_3; }
	inline int32_t* get_address_of_colorBlindnessType_3() { return &___colorBlindnessType_3; }
	inline void set_colorBlindnessType_3(int32_t value)
	{
		___colorBlindnessType_3 = value;
	}

	inline static int32_t get_offset_of_colorBlindnessStrength_4() { return static_cast<int32_t>(offsetof(OverlaySettings_t1356898765, ___colorBlindnessStrength_4)); }
	inline float get_colorBlindnessStrength_4() const { return ___colorBlindnessStrength_4; }
	inline float* get_address_of_colorBlindnessStrength_4() { return &___colorBlindnessStrength_4; }
	inline void set_colorBlindnessStrength_4(float value)
	{
		___colorBlindnessStrength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLAYSETTINGS_T1356898765_H
#ifndef U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY0_T38843214_H
#define U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY0_T38843214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0
struct  U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessEvent UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0::evt
	int32_t ___evt_0;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessBundle> UnityEngine.Rendering.PostProcessing.PostProcessLayer/<UpdateBundleSortList>c__AnonStorey0::effects
	List_1_t642111116 * ___effects_1;

public:
	inline static int32_t get_offset_of_evt_0() { return static_cast<int32_t>(offsetof(U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214, ___evt_0)); }
	inline int32_t get_evt_0() const { return ___evt_0; }
	inline int32_t* get_address_of_evt_0() { return &___evt_0; }
	inline void set_evt_0(int32_t value)
	{
		___evt_0 = value;
	}

	inline static int32_t get_offset_of_effects_1() { return static_cast<int32_t>(offsetof(U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214, ___effects_1)); }
	inline List_1_t642111116 * get_effects_1() const { return ___effects_1; }
	inline List_1_t642111116 ** get_address_of_effects_1() { return &___effects_1; }
	inline void set_effects_1(List_1_t642111116 * value)
	{
		___effects_1 = value;
		Il2CppCodeGenWriteBarrier((&___effects_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUNDLESORTLISTU3EC__ANONSTOREY0_T38843214_H
#ifndef TEXTUREPARAMETER_T4267400415_H
#define TEXTUREPARAMETER_T4267400415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureParameter
struct  TextureParameter_t4267400415  : public ParameterOverride_1_t1642347714
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureParameterDefault UnityEngine.Rendering.PostProcessing.TextureParameter::defaultState
	int32_t ___defaultState_2;

public:
	inline static int32_t get_offset_of_defaultState_2() { return static_cast<int32_t>(offsetof(TextureParameter_t4267400415, ___defaultState_2)); }
	inline int32_t get_defaultState_2() const { return ___defaultState_2; }
	inline int32_t* get_address_of_defaultState_2() { return &___defaultState_2; }
	inline void set_defaultState_2(int32_t value)
	{
		___defaultState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPARAMETER_T4267400415_H
#ifndef VECTOR2PARAMETER_T1794608574_H
#define VECTOR2PARAMETER_T1794608574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Vector2Parameter
struct  Vector2Parameter_t1794608574  : public ParameterOverride_1_t136614534
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PARAMETER_T1794608574_H
#ifndef VECTOR4PARAMETER_T1505856958_H
#define VECTOR4PARAMETER_T1505856958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Vector4Parameter
struct  Vector4Parameter_t1505856958  : public ParameterOverride_1_t1299413948
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4PARAMETER_T1505856958_H
#ifndef RENDERTARGETIDENTIFIER_T2079184500_H
#define RENDERTARGETIDENTIFIER_T2079184500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t2079184500 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T2079184500_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BUTTON_T565603744_H
#define BUTTON_T565603744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Button
struct  Button_t565603744  : public Widget_t1336755483
{
public:
	// System.Action UnityEngine.Experimental.Rendering.DebugUI/Button::<action>k__BackingField
	Action_t1264377477 * ___U3CactionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CactionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Button_t565603744, ___U3CactionU3Ek__BackingField_5)); }
	inline Action_t1264377477 * get_U3CactionU3Ek__BackingField_5() const { return ___U3CactionU3Ek__BackingField_5; }
	inline Action_t1264377477 ** get_address_of_U3CactionU3Ek__BackingField_5() { return &___U3CactionU3Ek__BackingField_5; }
	inline void set_U3CactionU3Ek__BackingField_5(Action_t1264377477 * value)
	{
		___U3CactionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CactionU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T565603744_H
#ifndef CONTAINER_T3218394953_H
#define CONTAINER_T3218394953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Container
struct  Container_t3218394953  : public Widget_t1336755483
{
public:
	// UnityEngine.Experimental.Rendering.ObservableList`1<UnityEngine.Experimental.Rendering.DebugUI/Widget> UnityEngine.Experimental.Rendering.DebugUI/Container::<children>k__BackingField
	ObservableList_1_t535108294 * ___U3CchildrenU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CchildrenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Container_t3218394953, ___U3CchildrenU3Ek__BackingField_5)); }
	inline ObservableList_1_t535108294 * get_U3CchildrenU3Ek__BackingField_5() const { return ___U3CchildrenU3Ek__BackingField_5; }
	inline ObservableList_1_t535108294 ** get_address_of_U3CchildrenU3Ek__BackingField_5() { return &___U3CchildrenU3Ek__BackingField_5; }
	inline void set_U3CchildrenU3Ek__BackingField_5(ObservableList_1_t535108294 * value)
	{
		___U3CchildrenU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchildrenU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINER_T3218394953_H
#ifndef FIELD_1_T245835276_H
#define FIELD_1_T245835276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.Boolean>
struct  Field_1_t245835276  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t3822001908 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t269755560 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t4273289399 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t245835276, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t3822001908 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t3822001908 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t3822001908 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t245835276, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t269755560 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t269755560 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t269755560 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t245835276, ___onValueChanged_7)); }
	inline Action_2_t4273289399 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t4273289399 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t4273289399 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T245835276_H
#ifndef FIELD_1_T3099493064_H
#define FIELD_1_T3099493064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.Int32>
struct  Field_1_t3099493064  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t2380692400 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t3123413348 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t319808071 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t3099493064, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t2380692400 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t2380692400 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t2380692400 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t3099493064, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t3123413348 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t3123413348 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t3123413348 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t3099493064, ___onValueChanged_7)); }
	inline Action_2_t319808071 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t319808071 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t319808071 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T3099493064_H
#ifndef FIELD_1_T1545814085_H
#define FIELD_1_T1545814085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.Single>
struct  Field_1_t1545814085  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t827013421 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t1569734369 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t4120761267 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t1545814085, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t827013421 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t827013421 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t827013421 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t1545814085, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t1569734369 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t1569734369 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t1569734369 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t1545814085, ___onValueChanged_7)); }
	inline Action_2_t4120761267 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t4120761267 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t4120761267 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T1545814085_H
#ifndef FIELD_1_T2708609289_H
#define FIELD_1_T2708609289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<System.UInt32>
struct  Field_1_t2708609289  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t1989808625 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t2732529573 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t2314478627 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t2708609289, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t1989808625 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t1989808625 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t1989808625 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t2708609289, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t2732529573 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t2732529573 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t2732529573 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t2708609289, ___onValueChanged_7)); }
	inline Action_2_t2314478627 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t2314478627 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t2314478627 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T2708609289_H
#ifndef FIELD_1_T2704233635_H
#define FIELD_1_T2704233635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Color>
struct  Field_1_t2704233635  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t1985432971 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t2728153919 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t481218683 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t2704233635, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t1985432971 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t1985432971 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t1985432971 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t2704233635, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t2728153919 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t2728153919 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t2728153919 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t2704233635, ___onValueChanged_7)); }
	inline Action_2_t481218683 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t481218683 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t481218683 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T2704233635_H
#ifndef FIELD_1_T2304776834_H
#define FIELD_1_T2304776834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Vector2>
struct  Field_1_t2304776834  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t1585976170 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t2328697118 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t597031455 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t2304776834, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t1585976170 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t1585976170 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t1585976170 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t2304776834, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t2328697118 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t2328697118 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t2328697118 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t2304776834, ___onValueChanged_7)); }
	inline Action_2_t597031455 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t597031455 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t597031455 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T2304776834_H
#ifndef FIELD_1_T3870860775_H
#define FIELD_1_T3870860775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Vector3>
struct  Field_1_t3870860775  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t3152060111 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t3894781059 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t2424680171 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t3870860775, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t3152060111 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t3152060111 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t3152060111 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t3870860775, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t3894781059 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t3894781059 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t3894781059 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t3870860775, ___onValueChanged_7)); }
	inline Action_2_t2424680171 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t2424680171 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t2424680171 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T3870860775_H
#ifndef FIELD_1_T3467576248_H
#define FIELD_1_T3467576248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Field`1<UnityEngine.Vector4>
struct  Field_1_t3467576248  : public Widget_t1336755483
{
public:
	// System.Func`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<getter>k__BackingField
	Func_1_t2748775584 * ___U3CgetterU3Ek__BackingField_5;
	// System.Action`1<T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::<setter>k__BackingField
	Action_1_t3491496532 * ___U3CsetterU3Ek__BackingField_6;
	// System.Action`2<UnityEngine.Experimental.Rendering.DebugUI/Field`1<T>,T> UnityEngine.Experimental.Rendering.DebugUI/Field`1::onValueChanged
	Action_2_t1236701319 * ___onValueChanged_7;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Field_1_t3467576248, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t2748775584 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t2748775584 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t2748775584 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsetterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Field_1_t3467576248, ___U3CsetterU3Ek__BackingField_6)); }
	inline Action_1_t3491496532 * get_U3CsetterU3Ek__BackingField_6() const { return ___U3CsetterU3Ek__BackingField_6; }
	inline Action_1_t3491496532 ** get_address_of_U3CsetterU3Ek__BackingField_6() { return &___U3CsetterU3Ek__BackingField_6; }
	inline void set_U3CsetterU3Ek__BackingField_6(Action_1_t3491496532 * value)
	{
		___U3CsetterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsetterU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_onValueChanged_7() { return static_cast<int32_t>(offsetof(Field_1_t3467576248, ___onValueChanged_7)); }
	inline Action_2_t1236701319 * get_onValueChanged_7() const { return ___onValueChanged_7; }
	inline Action_2_t1236701319 ** get_address_of_onValueChanged_7() { return &___onValueChanged_7; }
	inline void set_onValueChanged_7(Action_2_t1236701319 * value)
	{
		___onValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_1_T3467576248_H
#ifndef VALUE_T4182543730_H
#define VALUE_T4182543730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Value
struct  Value_t4182543730  : public Widget_t1336755483
{
public:
	// System.Func`1<System.Object> UnityEngine.Experimental.Rendering.DebugUI/Value::<getter>k__BackingField
	Func_1_t2509852811 * ___U3CgetterU3Ek__BackingField_5;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Value::refreshRate
	float ___refreshRate_6;

public:
	inline static int32_t get_offset_of_U3CgetterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Value_t4182543730, ___U3CgetterU3Ek__BackingField_5)); }
	inline Func_1_t2509852811 * get_U3CgetterU3Ek__BackingField_5() const { return ___U3CgetterU3Ek__BackingField_5; }
	inline Func_1_t2509852811 ** get_address_of_U3CgetterU3Ek__BackingField_5() { return &___U3CgetterU3Ek__BackingField_5; }
	inline void set_U3CgetterU3Ek__BackingField_5(Func_1_t2509852811 * value)
	{
		___U3CgetterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgetterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_refreshRate_6() { return static_cast<int32_t>(offsetof(Value_t4182543730, ___refreshRate_6)); }
	inline float get_refreshRate_6() const { return ___refreshRate_6; }
	inline float* get_address_of_refreshRate_6() { return &___refreshRate_6; }
	inline void set_refreshRate_6(float value)
	{
		___refreshRate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUE_T4182543730_H
#ifndef GPUCOPYASSET_T4186295563_H
#define GPUCOPYASSET_T4186295563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.GPUCopyAsset
struct  GPUCopyAsset_t4186295563  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation[] UnityEngine.Experimental.Rendering.GPUCopyAsset::m_CopyOperation
	CopyOperationU5BU5D_t3246644051* ___m_CopyOperation_6;

public:
	inline static int32_t get_offset_of_m_CopyOperation_6() { return static_cast<int32_t>(offsetof(GPUCopyAsset_t4186295563, ___m_CopyOperation_6)); }
	inline CopyOperationU5BU5D_t3246644051* get_m_CopyOperation_6() const { return ___m_CopyOperation_6; }
	inline CopyOperationU5BU5D_t3246644051** get_address_of_m_CopyOperation_6() { return &___m_CopyOperation_6; }
	inline void set_m_CopyOperation_6(CopyOperationU5BU5D_t3246644051* value)
	{
		___m_CopyOperation_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CopyOperation_6), value);
	}
};

struct GPUCopyAsset_t4186295563_StaticFields
{
public:
	// System.String[] UnityEngine.Experimental.Rendering.GPUCopyAsset::k_ChannelIDS
	StringU5BU5D_t1281789340* ___k_ChannelIDS_4;

public:
	inline static int32_t get_offset_of_k_ChannelIDS_4() { return static_cast<int32_t>(offsetof(GPUCopyAsset_t4186295563_StaticFields, ___k_ChannelIDS_4)); }
	inline StringU5BU5D_t1281789340* get_k_ChannelIDS_4() const { return ___k_ChannelIDS_4; }
	inline StringU5BU5D_t1281789340** get_address_of_k_ChannelIDS_4() { return &___k_ChannelIDS_4; }
	inline void set_k_ChannelIDS_4(StringU5BU5D_t1281789340* value)
	{
		___k_ChannelIDS_4 = value;
		Il2CppCodeGenWriteBarrier((&___k_ChannelIDS_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUCOPYASSET_T4186295563_H
#ifndef POSTPROCESSEFFECTSETTINGS_T1672565614_H
#define POSTPROCESSEFFECTSETTINGS_T1672565614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct  PostProcessEffectSettings_t1672565614  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::active
	bool ___active_4;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::enabled
	BoolParameter_t2299103272 * ___enabled_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::parameters
	ReadOnlyCollection_1_t4273630488 * ___parameters_6;

public:
	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_enabled_5() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614, ___enabled_5)); }
	inline BoolParameter_t2299103272 * get_enabled_5() const { return ___enabled_5; }
	inline BoolParameter_t2299103272 ** get_address_of_enabled_5() { return &___enabled_5; }
	inline void set_enabled_5(BoolParameter_t2299103272 * value)
	{
		___enabled_5 = value;
		Il2CppCodeGenWriteBarrier((&___enabled_5), value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614, ___parameters_6)); }
	inline ReadOnlyCollection_1_t4273630488 * get_parameters_6() const { return ___parameters_6; }
	inline ReadOnlyCollection_1_t4273630488 ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(ReadOnlyCollection_1_t4273630488 * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_6), value);
	}
};

struct PostProcessEffectSettings_t1672565614_StaticFields
{
public:
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::<>f__am$cache0
	Func_2_t1761491126 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<System.Reflection.FieldInfo,System.Int32> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::<>f__am$cache1
	Func_2_t320181618 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1761491126 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1761491126 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1761491126 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t1672565614_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t320181618 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t320181618 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t320181618 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTSETTINGS_T1672565614_H
#ifndef POSTPROCESSPROFILE_T3936051061_H
#define POSTPROCESSPROFILE_T3936051061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessProfile
struct  PostProcessProfile_t3936051061  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessProfile::settings
	List_1_t3144640356 * ___settings_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::isDirty
	bool ___isDirty_5;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(PostProcessProfile_t3936051061, ___settings_4)); }
	inline List_1_t3144640356 * get_settings_4() const { return ___settings_4; }
	inline List_1_t3144640356 ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(List_1_t3144640356 * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((&___settings_4), value);
	}

	inline static int32_t get_offset_of_isDirty_5() { return static_cast<int32_t>(offsetof(PostProcessProfile_t3936051061, ___isDirty_5)); }
	inline bool get_isDirty_5() const { return ___isDirty_5; }
	inline bool* get_address_of_isDirty_5() { return &___isDirty_5; }
	inline void set_isDirty_5(bool value)
	{
		___isDirty_5 = value;
	}
};

struct PostProcessProfile_t3936051061_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessProfile::<>f__am$cache0
	Predicate_1_t2497859738 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(PostProcessProfile_t3936051061_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Predicate_1_t2497859738 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Predicate_1_t2497859738 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Predicate_1_t2497859738 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSPROFILE_T3936051061_H
#ifndef POSTPROCESSRENDERCONTEXT_T597611190_H
#define POSTPROCESSRENDERCONTEXT_T597611190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext
struct  PostProcessRenderContext_t597611190  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::m_Camera
	Camera_t4157153871 * ___m_Camera_0;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<command>k__BackingField
	CommandBuffer_t2206337031 * ___U3CcommandU3Ek__BackingField_1;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<source>k__BackingField
	RenderTargetIdentifier_t2079184500  ___U3CsourceU3Ek__BackingField_2;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<destination>k__BackingField
	RenderTargetIdentifier_t2079184500  ___U3CdestinationU3Ek__BackingField_3;
	// UnityEngine.RenderTextureFormat UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<sourceFormat>k__BackingField
	int32_t ___U3CsourceFormatU3Ek__BackingField_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<flip>k__BackingField
	bool ___U3CflipU3Ek__BackingField_5;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<resources>k__BackingField
	PostProcessResources_t1163236733 * ___U3CresourcesU3Ek__BackingField_6;
	// UnityEngine.Rendering.PostProcessing.PropertySheetFactory UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<propertySheets>k__BackingField
	PropertySheetFactory_t1490101248 * ___U3CpropertySheetsU3Ek__BackingField_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<userData>k__BackingField
	Dictionary_2_t2865362463 * ___U3CuserDataU3Ek__BackingField_8;
	// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<debugLayer>k__BackingField
	PostProcessDebugLayer_t3290441360 * ___U3CdebugLayerU3Ek__BackingField_9;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_10;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_11;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<stereoActive>k__BackingField
	bool ___U3CstereoActiveU3Ek__BackingField_12;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<xrActiveEye>k__BackingField
	int32_t ___U3CxrActiveEyeU3Ek__BackingField_13;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<screenWidth>k__BackingField
	int32_t ___U3CscreenWidthU3Ek__BackingField_14;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<screenHeight>k__BackingField
	int32_t ___U3CscreenHeightU3Ek__BackingField_15;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<isSceneView>k__BackingField
	bool ___U3CisSceneViewU3Ek__BackingField_16;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer/Antialiasing UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<antialiasing>k__BackingField
	int32_t ___U3CantialiasingU3Ek__BackingField_17;
	// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<temporalAntialiasing>k__BackingField
	TemporalAntialiasing_t1482226156 * ___U3CtemporalAntialiasingU3Ek__BackingField_18;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::uberSheet
	PropertySheet_t3821403501 * ___uberSheet_19;
	// UnityEngine.Texture UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::autoExposureTexture
	Texture_t3661962703 * ___autoExposureTexture_20;
	// UnityEngine.Rendering.PostProcessing.LogHistogram UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::logHistogram
	LogHistogram_t1187052756 * ___logHistogram_21;
	// UnityEngine.Texture UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::logLut
	Texture_t3661962703 * ___logLut_22;
	// UnityEngine.Rendering.PostProcessing.AutoExposure UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::autoExposure
	AutoExposure_t2470830169 * ___autoExposure_23;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::bloomBufferNameID
	int32_t ___bloomBufferNameID_24;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::m_sourceDescriptor
	RenderTextureDescriptor_t1974534975  ___m_sourceDescriptor_25;

public:
	inline static int32_t get_offset_of_m_Camera_0() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___m_Camera_0)); }
	inline Camera_t4157153871 * get_m_Camera_0() const { return ___m_Camera_0; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_0() { return &___m_Camera_0; }
	inline void set_m_Camera_0(Camera_t4157153871 * value)
	{
		___m_Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_0), value);
	}

	inline static int32_t get_offset_of_U3CcommandU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CcommandU3Ek__BackingField_1)); }
	inline CommandBuffer_t2206337031 * get_U3CcommandU3Ek__BackingField_1() const { return ___U3CcommandU3Ek__BackingField_1; }
	inline CommandBuffer_t2206337031 ** get_address_of_U3CcommandU3Ek__BackingField_1() { return &___U3CcommandU3Ek__BackingField_1; }
	inline void set_U3CcommandU3Ek__BackingField_1(CommandBuffer_t2206337031 * value)
	{
		___U3CcommandU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcommandU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CsourceU3Ek__BackingField_2)); }
	inline RenderTargetIdentifier_t2079184500  get_U3CsourceU3Ek__BackingField_2() const { return ___U3CsourceU3Ek__BackingField_2; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_U3CsourceU3Ek__BackingField_2() { return &___U3CsourceU3Ek__BackingField_2; }
	inline void set_U3CsourceU3Ek__BackingField_2(RenderTargetIdentifier_t2079184500  value)
	{
		___U3CsourceU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdestinationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CdestinationU3Ek__BackingField_3)); }
	inline RenderTargetIdentifier_t2079184500  get_U3CdestinationU3Ek__BackingField_3() const { return ___U3CdestinationU3Ek__BackingField_3; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_U3CdestinationU3Ek__BackingField_3() { return &___U3CdestinationU3Ek__BackingField_3; }
	inline void set_U3CdestinationU3Ek__BackingField_3(RenderTargetIdentifier_t2079184500  value)
	{
		___U3CdestinationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CsourceFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CsourceFormatU3Ek__BackingField_4() const { return ___U3CsourceFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CsourceFormatU3Ek__BackingField_4() { return &___U3CsourceFormatU3Ek__BackingField_4; }
	inline void set_U3CsourceFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CsourceFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CflipU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CflipU3Ek__BackingField_5)); }
	inline bool get_U3CflipU3Ek__BackingField_5() const { return ___U3CflipU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CflipU3Ek__BackingField_5() { return &___U3CflipU3Ek__BackingField_5; }
	inline void set_U3CflipU3Ek__BackingField_5(bool value)
	{
		___U3CflipU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CresourcesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CresourcesU3Ek__BackingField_6)); }
	inline PostProcessResources_t1163236733 * get_U3CresourcesU3Ek__BackingField_6() const { return ___U3CresourcesU3Ek__BackingField_6; }
	inline PostProcessResources_t1163236733 ** get_address_of_U3CresourcesU3Ek__BackingField_6() { return &___U3CresourcesU3Ek__BackingField_6; }
	inline void set_U3CresourcesU3Ek__BackingField_6(PostProcessResources_t1163236733 * value)
	{
		___U3CresourcesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourcesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpropertySheetsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CpropertySheetsU3Ek__BackingField_7)); }
	inline PropertySheetFactory_t1490101248 * get_U3CpropertySheetsU3Ek__BackingField_7() const { return ___U3CpropertySheetsU3Ek__BackingField_7; }
	inline PropertySheetFactory_t1490101248 ** get_address_of_U3CpropertySheetsU3Ek__BackingField_7() { return &___U3CpropertySheetsU3Ek__BackingField_7; }
	inline void set_U3CpropertySheetsU3Ek__BackingField_7(PropertySheetFactory_t1490101248 * value)
	{
		___U3CpropertySheetsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpropertySheetsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CuserDataU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CuserDataU3Ek__BackingField_8)); }
	inline Dictionary_2_t2865362463 * get_U3CuserDataU3Ek__BackingField_8() const { return ___U3CuserDataU3Ek__BackingField_8; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CuserDataU3Ek__BackingField_8() { return &___U3CuserDataU3Ek__BackingField_8; }
	inline void set_U3CuserDataU3Ek__BackingField_8(Dictionary_2_t2865362463 * value)
	{
		___U3CuserDataU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserDataU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CdebugLayerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CdebugLayerU3Ek__BackingField_9)); }
	inline PostProcessDebugLayer_t3290441360 * get_U3CdebugLayerU3Ek__BackingField_9() const { return ___U3CdebugLayerU3Ek__BackingField_9; }
	inline PostProcessDebugLayer_t3290441360 ** get_address_of_U3CdebugLayerU3Ek__BackingField_9() { return &___U3CdebugLayerU3Ek__BackingField_9; }
	inline void set_U3CdebugLayerU3Ek__BackingField_9(PostProcessDebugLayer_t3290441360 * value)
	{
		___U3CdebugLayerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdebugLayerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CwidthU3Ek__BackingField_10)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_10() const { return ___U3CwidthU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_10() { return &___U3CwidthU3Ek__BackingField_10; }
	inline void set_U3CwidthU3Ek__BackingField_10(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CheightU3Ek__BackingField_11)); }
	inline int32_t get_U3CheightU3Ek__BackingField_11() const { return ___U3CheightU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_11() { return &___U3CheightU3Ek__BackingField_11; }
	inline void set_U3CheightU3Ek__BackingField_11(int32_t value)
	{
		___U3CheightU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CstereoActiveU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CstereoActiveU3Ek__BackingField_12)); }
	inline bool get_U3CstereoActiveU3Ek__BackingField_12() const { return ___U3CstereoActiveU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CstereoActiveU3Ek__BackingField_12() { return &___U3CstereoActiveU3Ek__BackingField_12; }
	inline void set_U3CstereoActiveU3Ek__BackingField_12(bool value)
	{
		___U3CstereoActiveU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CxrActiveEyeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CxrActiveEyeU3Ek__BackingField_13)); }
	inline int32_t get_U3CxrActiveEyeU3Ek__BackingField_13() const { return ___U3CxrActiveEyeU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CxrActiveEyeU3Ek__BackingField_13() { return &___U3CxrActiveEyeU3Ek__BackingField_13; }
	inline void set_U3CxrActiveEyeU3Ek__BackingField_13(int32_t value)
	{
		___U3CxrActiveEyeU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CscreenWidthU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CscreenWidthU3Ek__BackingField_14)); }
	inline int32_t get_U3CscreenWidthU3Ek__BackingField_14() const { return ___U3CscreenWidthU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CscreenWidthU3Ek__BackingField_14() { return &___U3CscreenWidthU3Ek__BackingField_14; }
	inline void set_U3CscreenWidthU3Ek__BackingField_14(int32_t value)
	{
		___U3CscreenWidthU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CscreenHeightU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CscreenHeightU3Ek__BackingField_15)); }
	inline int32_t get_U3CscreenHeightU3Ek__BackingField_15() const { return ___U3CscreenHeightU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CscreenHeightU3Ek__BackingField_15() { return &___U3CscreenHeightU3Ek__BackingField_15; }
	inline void set_U3CscreenHeightU3Ek__BackingField_15(int32_t value)
	{
		___U3CscreenHeightU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CisSceneViewU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CisSceneViewU3Ek__BackingField_16)); }
	inline bool get_U3CisSceneViewU3Ek__BackingField_16() const { return ___U3CisSceneViewU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisSceneViewU3Ek__BackingField_16() { return &___U3CisSceneViewU3Ek__BackingField_16; }
	inline void set_U3CisSceneViewU3Ek__BackingField_16(bool value)
	{
		___U3CisSceneViewU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CantialiasingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CantialiasingU3Ek__BackingField_17)); }
	inline int32_t get_U3CantialiasingU3Ek__BackingField_17() const { return ___U3CantialiasingU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CantialiasingU3Ek__BackingField_17() { return &___U3CantialiasingU3Ek__BackingField_17; }
	inline void set_U3CantialiasingU3Ek__BackingField_17(int32_t value)
	{
		___U3CantialiasingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CtemporalAntialiasingU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___U3CtemporalAntialiasingU3Ek__BackingField_18)); }
	inline TemporalAntialiasing_t1482226156 * get_U3CtemporalAntialiasingU3Ek__BackingField_18() const { return ___U3CtemporalAntialiasingU3Ek__BackingField_18; }
	inline TemporalAntialiasing_t1482226156 ** get_address_of_U3CtemporalAntialiasingU3Ek__BackingField_18() { return &___U3CtemporalAntialiasingU3Ek__BackingField_18; }
	inline void set_U3CtemporalAntialiasingU3Ek__BackingField_18(TemporalAntialiasing_t1482226156 * value)
	{
		___U3CtemporalAntialiasingU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtemporalAntialiasingU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_uberSheet_19() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___uberSheet_19)); }
	inline PropertySheet_t3821403501 * get_uberSheet_19() const { return ___uberSheet_19; }
	inline PropertySheet_t3821403501 ** get_address_of_uberSheet_19() { return &___uberSheet_19; }
	inline void set_uberSheet_19(PropertySheet_t3821403501 * value)
	{
		___uberSheet_19 = value;
		Il2CppCodeGenWriteBarrier((&___uberSheet_19), value);
	}

	inline static int32_t get_offset_of_autoExposureTexture_20() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___autoExposureTexture_20)); }
	inline Texture_t3661962703 * get_autoExposureTexture_20() const { return ___autoExposureTexture_20; }
	inline Texture_t3661962703 ** get_address_of_autoExposureTexture_20() { return &___autoExposureTexture_20; }
	inline void set_autoExposureTexture_20(Texture_t3661962703 * value)
	{
		___autoExposureTexture_20 = value;
		Il2CppCodeGenWriteBarrier((&___autoExposureTexture_20), value);
	}

	inline static int32_t get_offset_of_logHistogram_21() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___logHistogram_21)); }
	inline LogHistogram_t1187052756 * get_logHistogram_21() const { return ___logHistogram_21; }
	inline LogHistogram_t1187052756 ** get_address_of_logHistogram_21() { return &___logHistogram_21; }
	inline void set_logHistogram_21(LogHistogram_t1187052756 * value)
	{
		___logHistogram_21 = value;
		Il2CppCodeGenWriteBarrier((&___logHistogram_21), value);
	}

	inline static int32_t get_offset_of_logLut_22() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___logLut_22)); }
	inline Texture_t3661962703 * get_logLut_22() const { return ___logLut_22; }
	inline Texture_t3661962703 ** get_address_of_logLut_22() { return &___logLut_22; }
	inline void set_logLut_22(Texture_t3661962703 * value)
	{
		___logLut_22 = value;
		Il2CppCodeGenWriteBarrier((&___logLut_22), value);
	}

	inline static int32_t get_offset_of_autoExposure_23() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___autoExposure_23)); }
	inline AutoExposure_t2470830169 * get_autoExposure_23() const { return ___autoExposure_23; }
	inline AutoExposure_t2470830169 ** get_address_of_autoExposure_23() { return &___autoExposure_23; }
	inline void set_autoExposure_23(AutoExposure_t2470830169 * value)
	{
		___autoExposure_23 = value;
		Il2CppCodeGenWriteBarrier((&___autoExposure_23), value);
	}

	inline static int32_t get_offset_of_bloomBufferNameID_24() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___bloomBufferNameID_24)); }
	inline int32_t get_bloomBufferNameID_24() const { return ___bloomBufferNameID_24; }
	inline int32_t* get_address_of_bloomBufferNameID_24() { return &___bloomBufferNameID_24; }
	inline void set_bloomBufferNameID_24(int32_t value)
	{
		___bloomBufferNameID_24 = value;
	}

	inline static int32_t get_offset_of_m_sourceDescriptor_25() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_t597611190, ___m_sourceDescriptor_25)); }
	inline RenderTextureDescriptor_t1974534975  get_m_sourceDescriptor_25() const { return ___m_sourceDescriptor_25; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_m_sourceDescriptor_25() { return &___m_sourceDescriptor_25; }
	inline void set_m_sourceDescriptor_25(RenderTextureDescriptor_t1974534975  value)
	{
		___m_sourceDescriptor_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSRENDERCONTEXT_T597611190_H
#ifndef POSTPROCESSRESOURCES_T1163236733_H
#define POSTPROCESSRESOURCES_T1163236733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources
struct  PostProcessResources_t1163236733  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Texture2D[] UnityEngine.Rendering.PostProcessing.PostProcessResources::blueNoise64
	Texture2DU5BU5D_t149664596* ___blueNoise64_4;
	// UnityEngine.Texture2D[] UnityEngine.Rendering.PostProcessing.PostProcessResources::blueNoise256
	Texture2DU5BU5D_t149664596* ___blueNoise256_5;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources/SMAALuts UnityEngine.Rendering.PostProcessing.PostProcessResources::smaaLuts
	SMAALuts_t184516107 * ___smaaLuts_6;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders UnityEngine.Rendering.PostProcessing.PostProcessResources::shaders
	Shaders_t2807171077 * ___shaders_7;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders UnityEngine.Rendering.PostProcessing.PostProcessResources::computeShaders
	ComputeShaders_t4172110136 * ___computeShaders_8;

public:
	inline static int32_t get_offset_of_blueNoise64_4() { return static_cast<int32_t>(offsetof(PostProcessResources_t1163236733, ___blueNoise64_4)); }
	inline Texture2DU5BU5D_t149664596* get_blueNoise64_4() const { return ___blueNoise64_4; }
	inline Texture2DU5BU5D_t149664596** get_address_of_blueNoise64_4() { return &___blueNoise64_4; }
	inline void set_blueNoise64_4(Texture2DU5BU5D_t149664596* value)
	{
		___blueNoise64_4 = value;
		Il2CppCodeGenWriteBarrier((&___blueNoise64_4), value);
	}

	inline static int32_t get_offset_of_blueNoise256_5() { return static_cast<int32_t>(offsetof(PostProcessResources_t1163236733, ___blueNoise256_5)); }
	inline Texture2DU5BU5D_t149664596* get_blueNoise256_5() const { return ___blueNoise256_5; }
	inline Texture2DU5BU5D_t149664596** get_address_of_blueNoise256_5() { return &___blueNoise256_5; }
	inline void set_blueNoise256_5(Texture2DU5BU5D_t149664596* value)
	{
		___blueNoise256_5 = value;
		Il2CppCodeGenWriteBarrier((&___blueNoise256_5), value);
	}

	inline static int32_t get_offset_of_smaaLuts_6() { return static_cast<int32_t>(offsetof(PostProcessResources_t1163236733, ___smaaLuts_6)); }
	inline SMAALuts_t184516107 * get_smaaLuts_6() const { return ___smaaLuts_6; }
	inline SMAALuts_t184516107 ** get_address_of_smaaLuts_6() { return &___smaaLuts_6; }
	inline void set_smaaLuts_6(SMAALuts_t184516107 * value)
	{
		___smaaLuts_6 = value;
		Il2CppCodeGenWriteBarrier((&___smaaLuts_6), value);
	}

	inline static int32_t get_offset_of_shaders_7() { return static_cast<int32_t>(offsetof(PostProcessResources_t1163236733, ___shaders_7)); }
	inline Shaders_t2807171077 * get_shaders_7() const { return ___shaders_7; }
	inline Shaders_t2807171077 ** get_address_of_shaders_7() { return &___shaders_7; }
	inline void set_shaders_7(Shaders_t2807171077 * value)
	{
		___shaders_7 = value;
		Il2CppCodeGenWriteBarrier((&___shaders_7), value);
	}

	inline static int32_t get_offset_of_computeShaders_8() { return static_cast<int32_t>(offsetof(PostProcessResources_t1163236733, ___computeShaders_8)); }
	inline ComputeShaders_t4172110136 * get_computeShaders_8() const { return ___computeShaders_8; }
	inline ComputeShaders_t4172110136 ** get_address_of_computeShaders_8() { return &___computeShaders_8; }
	inline void set_computeShaders_8(ComputeShaders_t4172110136 * value)
	{
		___computeShaders_8 = value;
		Il2CppCodeGenWriteBarrier((&___computeShaders_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSRESOURCES_T1163236733_H
#ifndef BOOLFIELD_T1734172308_H
#define BOOLFIELD_T1734172308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/BoolField
struct  BoolField_t1734172308  : public Field_1_t245835276
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLFIELD_T1734172308_H
#ifndef COLORFIELD_T1371659825_H
#define COLORFIELD_T1371659825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/ColorField
struct  ColorField_t1371659825  : public Field_1_t2704233635
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.DebugUI/ColorField::hdr
	bool ___hdr_8;
	// System.Boolean UnityEngine.Experimental.Rendering.DebugUI/ColorField::showAlpha
	bool ___showAlpha_9;
	// System.Boolean UnityEngine.Experimental.Rendering.DebugUI/ColorField::showPicker
	bool ___showPicker_10;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/ColorField::incStep
	float ___incStep_11;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/ColorField::incStepMult
	float ___incStepMult_12;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/ColorField::decimals
	int32_t ___decimals_13;

public:
	inline static int32_t get_offset_of_hdr_8() { return static_cast<int32_t>(offsetof(ColorField_t1371659825, ___hdr_8)); }
	inline bool get_hdr_8() const { return ___hdr_8; }
	inline bool* get_address_of_hdr_8() { return &___hdr_8; }
	inline void set_hdr_8(bool value)
	{
		___hdr_8 = value;
	}

	inline static int32_t get_offset_of_showAlpha_9() { return static_cast<int32_t>(offsetof(ColorField_t1371659825, ___showAlpha_9)); }
	inline bool get_showAlpha_9() const { return ___showAlpha_9; }
	inline bool* get_address_of_showAlpha_9() { return &___showAlpha_9; }
	inline void set_showAlpha_9(bool value)
	{
		___showAlpha_9 = value;
	}

	inline static int32_t get_offset_of_showPicker_10() { return static_cast<int32_t>(offsetof(ColorField_t1371659825, ___showPicker_10)); }
	inline bool get_showPicker_10() const { return ___showPicker_10; }
	inline bool* get_address_of_showPicker_10() { return &___showPicker_10; }
	inline void set_showPicker_10(bool value)
	{
		___showPicker_10 = value;
	}

	inline static int32_t get_offset_of_incStep_11() { return static_cast<int32_t>(offsetof(ColorField_t1371659825, ___incStep_11)); }
	inline float get_incStep_11() const { return ___incStep_11; }
	inline float* get_address_of_incStep_11() { return &___incStep_11; }
	inline void set_incStep_11(float value)
	{
		___incStep_11 = value;
	}

	inline static int32_t get_offset_of_incStepMult_12() { return static_cast<int32_t>(offsetof(ColorField_t1371659825, ___incStepMult_12)); }
	inline float get_incStepMult_12() const { return ___incStepMult_12; }
	inline float* get_address_of_incStepMult_12() { return &___incStepMult_12; }
	inline void set_incStepMult_12(float value)
	{
		___incStepMult_12 = value;
	}

	inline static int32_t get_offset_of_decimals_13() { return static_cast<int32_t>(offsetof(ColorField_t1371659825, ___decimals_13)); }
	inline int32_t get_decimals_13() const { return ___decimals_13; }
	inline int32_t* get_address_of_decimals_13() { return &___decimals_13; }
	inline void set_decimals_13(int32_t value)
	{
		___decimals_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORFIELD_T1371659825_H
#ifndef ENUMFIELD_T3058254002_H
#define ENUMFIELD_T3058254002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/EnumField
struct  EnumField_t3058254002  : public Field_1_t3099493064
{
public:
	// UnityEngine.GUIContent[] UnityEngine.Experimental.Rendering.DebugUI/EnumField::enumNames
	GUIContentU5BU5D_t2445521510* ___enumNames_8;
	// System.Int32[] UnityEngine.Experimental.Rendering.DebugUI/EnumField::enumValues
	Int32U5BU5D_t385246372* ___enumValues_9;

public:
	inline static int32_t get_offset_of_enumNames_8() { return static_cast<int32_t>(offsetof(EnumField_t3058254002, ___enumNames_8)); }
	inline GUIContentU5BU5D_t2445521510* get_enumNames_8() const { return ___enumNames_8; }
	inline GUIContentU5BU5D_t2445521510** get_address_of_enumNames_8() { return &___enumNames_8; }
	inline void set_enumNames_8(GUIContentU5BU5D_t2445521510* value)
	{
		___enumNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___enumNames_8), value);
	}

	inline static int32_t get_offset_of_enumValues_9() { return static_cast<int32_t>(offsetof(EnumField_t3058254002, ___enumValues_9)); }
	inline Int32U5BU5D_t385246372* get_enumValues_9() const { return ___enumValues_9; }
	inline Int32U5BU5D_t385246372** get_address_of_enumValues_9() { return &___enumValues_9; }
	inline void set_enumValues_9(Int32U5BU5D_t385246372* value)
	{
		___enumValues_9 = value;
		Il2CppCodeGenWriteBarrier((&___enumValues_9), value);
	}
};

struct EnumField_t3058254002_StaticFields
{
public:
	// System.Func`2<System.String,UnityEngine.GUIContent> UnityEngine.Experimental.Rendering.DebugUI/EnumField::<>f__am$cache0
	Func_2_t855502256 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(EnumField_t3058254002_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Func_2_t855502256 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Func_2_t855502256 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Func_2_t855502256 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMFIELD_T3058254002_H
#ifndef FLOATFIELD_T2428081994_H
#define FLOATFIELD_T2428081994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/FloatField
struct  FloatField_t2428081994  : public Field_1_t1545814085
{
public:
	// System.Func`1<System.Single> UnityEngine.Experimental.Rendering.DebugUI/FloatField::min
	Func_1_t827013421 * ___min_8;
	// System.Func`1<System.Single> UnityEngine.Experimental.Rendering.DebugUI/FloatField::max
	Func_1_t827013421 * ___max_9;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/FloatField::incStep
	float ___incStep_10;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/FloatField::incStepMult
	float ___incStepMult_11;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/FloatField::decimals
	int32_t ___decimals_12;

public:
	inline static int32_t get_offset_of_min_8() { return static_cast<int32_t>(offsetof(FloatField_t2428081994, ___min_8)); }
	inline Func_1_t827013421 * get_min_8() const { return ___min_8; }
	inline Func_1_t827013421 ** get_address_of_min_8() { return &___min_8; }
	inline void set_min_8(Func_1_t827013421 * value)
	{
		___min_8 = value;
		Il2CppCodeGenWriteBarrier((&___min_8), value);
	}

	inline static int32_t get_offset_of_max_9() { return static_cast<int32_t>(offsetof(FloatField_t2428081994, ___max_9)); }
	inline Func_1_t827013421 * get_max_9() const { return ___max_9; }
	inline Func_1_t827013421 ** get_address_of_max_9() { return &___max_9; }
	inline void set_max_9(Func_1_t827013421 * value)
	{
		___max_9 = value;
		Il2CppCodeGenWriteBarrier((&___max_9), value);
	}

	inline static int32_t get_offset_of_incStep_10() { return static_cast<int32_t>(offsetof(FloatField_t2428081994, ___incStep_10)); }
	inline float get_incStep_10() const { return ___incStep_10; }
	inline float* get_address_of_incStep_10() { return &___incStep_10; }
	inline void set_incStep_10(float value)
	{
		___incStep_10 = value;
	}

	inline static int32_t get_offset_of_incStepMult_11() { return static_cast<int32_t>(offsetof(FloatField_t2428081994, ___incStepMult_11)); }
	inline float get_incStepMult_11() const { return ___incStepMult_11; }
	inline float* get_address_of_incStepMult_11() { return &___incStepMult_11; }
	inline void set_incStepMult_11(float value)
	{
		___incStepMult_11 = value;
	}

	inline static int32_t get_offset_of_decimals_12() { return static_cast<int32_t>(offsetof(FloatField_t2428081994, ___decimals_12)); }
	inline int32_t get_decimals_12() const { return ___decimals_12; }
	inline int32_t* get_address_of_decimals_12() { return &___decimals_12; }
	inline void set_decimals_12(int32_t value)
	{
		___decimals_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFIELD_T2428081994_H
#ifndef FOLDOUT_T3285563867_H
#define FOLDOUT_T3285563867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Foldout
struct  Foldout_t3285563867  : public Container_t3218394953
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.DebugUI/Foldout::opened
	bool ___opened_6;

public:
	inline static int32_t get_offset_of_opened_6() { return static_cast<int32_t>(offsetof(Foldout_t3285563867, ___opened_6)); }
	inline bool get_opened_6() const { return ___opened_6; }
	inline bool* get_address_of_opened_6() { return &___opened_6; }
	inline void set_opened_6(bool value)
	{
		___opened_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLDOUT_T3285563867_H
#ifndef HBOX_T382921510_H
#define HBOX_T382921510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/HBox
struct  HBox_t382921510  : public Container_t3218394953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HBOX_T382921510_H
#ifndef INTFIELD_T940763525_H
#define INTFIELD_T940763525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/IntField
struct  IntField_t940763525  : public Field_1_t3099493064
{
public:
	// System.Func`1<System.Int32> UnityEngine.Experimental.Rendering.DebugUI/IntField::min
	Func_1_t2380692400 * ___min_8;
	// System.Func`1<System.Int32> UnityEngine.Experimental.Rendering.DebugUI/IntField::max
	Func_1_t2380692400 * ___max_9;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/IntField::incStep
	int32_t ___incStep_10;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/IntField::intStepMult
	int32_t ___intStepMult_11;

public:
	inline static int32_t get_offset_of_min_8() { return static_cast<int32_t>(offsetof(IntField_t940763525, ___min_8)); }
	inline Func_1_t2380692400 * get_min_8() const { return ___min_8; }
	inline Func_1_t2380692400 ** get_address_of_min_8() { return &___min_8; }
	inline void set_min_8(Func_1_t2380692400 * value)
	{
		___min_8 = value;
		Il2CppCodeGenWriteBarrier((&___min_8), value);
	}

	inline static int32_t get_offset_of_max_9() { return static_cast<int32_t>(offsetof(IntField_t940763525, ___max_9)); }
	inline Func_1_t2380692400 * get_max_9() const { return ___max_9; }
	inline Func_1_t2380692400 ** get_address_of_max_9() { return &___max_9; }
	inline void set_max_9(Func_1_t2380692400 * value)
	{
		___max_9 = value;
		Il2CppCodeGenWriteBarrier((&___max_9), value);
	}

	inline static int32_t get_offset_of_incStep_10() { return static_cast<int32_t>(offsetof(IntField_t940763525, ___incStep_10)); }
	inline int32_t get_incStep_10() const { return ___incStep_10; }
	inline int32_t* get_address_of_incStep_10() { return &___incStep_10; }
	inline void set_incStep_10(int32_t value)
	{
		___incStep_10 = value;
	}

	inline static int32_t get_offset_of_intStepMult_11() { return static_cast<int32_t>(offsetof(IntField_t940763525, ___intStepMult_11)); }
	inline int32_t get_intStepMult_11() const { return ___intStepMult_11; }
	inline int32_t* get_address_of_intStepMult_11() { return &___intStepMult_11; }
	inline void set_intStepMult_11(int32_t value)
	{
		___intStepMult_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTFIELD_T940763525_H
#ifndef UINTFIELD_T197911686_H
#define UINTFIELD_T197911686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/UIntField
struct  UIntField_t197911686  : public Field_1_t2708609289
{
public:
	// System.Func`1<System.UInt32> UnityEngine.Experimental.Rendering.DebugUI/UIntField::min
	Func_1_t1989808625 * ___min_8;
	// System.Func`1<System.UInt32> UnityEngine.Experimental.Rendering.DebugUI/UIntField::max
	Func_1_t1989808625 * ___max_9;
	// System.UInt32 UnityEngine.Experimental.Rendering.DebugUI/UIntField::incStep
	uint32_t ___incStep_10;
	// System.UInt32 UnityEngine.Experimental.Rendering.DebugUI/UIntField::intStepMult
	uint32_t ___intStepMult_11;

public:
	inline static int32_t get_offset_of_min_8() { return static_cast<int32_t>(offsetof(UIntField_t197911686, ___min_8)); }
	inline Func_1_t1989808625 * get_min_8() const { return ___min_8; }
	inline Func_1_t1989808625 ** get_address_of_min_8() { return &___min_8; }
	inline void set_min_8(Func_1_t1989808625 * value)
	{
		___min_8 = value;
		Il2CppCodeGenWriteBarrier((&___min_8), value);
	}

	inline static int32_t get_offset_of_max_9() { return static_cast<int32_t>(offsetof(UIntField_t197911686, ___max_9)); }
	inline Func_1_t1989808625 * get_max_9() const { return ___max_9; }
	inline Func_1_t1989808625 ** get_address_of_max_9() { return &___max_9; }
	inline void set_max_9(Func_1_t1989808625 * value)
	{
		___max_9 = value;
		Il2CppCodeGenWriteBarrier((&___max_9), value);
	}

	inline static int32_t get_offset_of_incStep_10() { return static_cast<int32_t>(offsetof(UIntField_t197911686, ___incStep_10)); }
	inline uint32_t get_incStep_10() const { return ___incStep_10; }
	inline uint32_t* get_address_of_incStep_10() { return &___incStep_10; }
	inline void set_incStep_10(uint32_t value)
	{
		___incStep_10 = value;
	}

	inline static int32_t get_offset_of_intStepMult_11() { return static_cast<int32_t>(offsetof(UIntField_t197911686, ___intStepMult_11)); }
	inline uint32_t get_intStepMult_11() const { return ___intStepMult_11; }
	inline uint32_t* get_address_of_intStepMult_11() { return &___intStepMult_11; }
	inline void set_intStepMult_11(uint32_t value)
	{
		___intStepMult_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTFIELD_T197911686_H
#ifndef VBOX_T115141414_H
#define VBOX_T115141414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/VBox
struct  VBox_t115141414  : public Container_t3218394953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VBOX_T115141414_H
#ifndef VECTOR2FIELD_T3950563844_H
#define VECTOR2FIELD_T3950563844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Vector2Field
struct  Vector2Field_t3950563844  : public Field_1_t2304776834
{
public:
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Vector2Field::incStep
	float ___incStep_8;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Vector2Field::incStepMult
	float ___incStepMult_9;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/Vector2Field::decimals
	int32_t ___decimals_10;

public:
	inline static int32_t get_offset_of_incStep_8() { return static_cast<int32_t>(offsetof(Vector2Field_t3950563844, ___incStep_8)); }
	inline float get_incStep_8() const { return ___incStep_8; }
	inline float* get_address_of_incStep_8() { return &___incStep_8; }
	inline void set_incStep_8(float value)
	{
		___incStep_8 = value;
	}

	inline static int32_t get_offset_of_incStepMult_9() { return static_cast<int32_t>(offsetof(Vector2Field_t3950563844, ___incStepMult_9)); }
	inline float get_incStepMult_9() const { return ___incStepMult_9; }
	inline float* get_address_of_incStepMult_9() { return &___incStepMult_9; }
	inline void set_incStepMult_9(float value)
	{
		___incStepMult_9 = value;
	}

	inline static int32_t get_offset_of_decimals_10() { return static_cast<int32_t>(offsetof(Vector2Field_t3950563844, ___decimals_10)); }
	inline int32_t get_decimals_10() const { return ___decimals_10; }
	inline int32_t* get_address_of_decimals_10() { return &___decimals_10; }
	inline void set_decimals_10(int32_t value)
	{
		___decimals_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2FIELD_T3950563844_H
#ifndef VECTOR3FIELD_T3952595460_H
#define VECTOR3FIELD_T3952595460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Vector3Field
struct  Vector3Field_t3952595460  : public Field_1_t3870860775
{
public:
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Vector3Field::incStep
	float ___incStep_8;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Vector3Field::incStepMult
	float ___incStepMult_9;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/Vector3Field::decimals
	int32_t ___decimals_10;

public:
	inline static int32_t get_offset_of_incStep_8() { return static_cast<int32_t>(offsetof(Vector3Field_t3952595460, ___incStep_8)); }
	inline float get_incStep_8() const { return ___incStep_8; }
	inline float* get_address_of_incStep_8() { return &___incStep_8; }
	inline void set_incStep_8(float value)
	{
		___incStep_8 = value;
	}

	inline static int32_t get_offset_of_incStepMult_9() { return static_cast<int32_t>(offsetof(Vector3Field_t3952595460, ___incStepMult_9)); }
	inline float get_incStepMult_9() const { return ___incStepMult_9; }
	inline float* get_address_of_incStepMult_9() { return &___incStepMult_9; }
	inline void set_incStepMult_9(float value)
	{
		___incStepMult_9 = value;
	}

	inline static int32_t get_offset_of_decimals_10() { return static_cast<int32_t>(offsetof(Vector3Field_t3952595460, ___decimals_10)); }
	inline int32_t get_decimals_10() const { return ___decimals_10; }
	inline int32_t* get_address_of_decimals_10() { return &___decimals_10; }
	inline void set_decimals_10(int32_t value)
	{
		___decimals_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3FIELD_T3952595460_H
#ifndef VECTOR4FIELD_T3946500612_H
#define VECTOR4FIELD_T3946500612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUI/Vector4Field
struct  Vector4Field_t3946500612  : public Field_1_t3467576248
{
public:
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Vector4Field::incStep
	float ___incStep_8;
	// System.Single UnityEngine.Experimental.Rendering.DebugUI/Vector4Field::incStepMult
	float ___incStepMult_9;
	// System.Int32 UnityEngine.Experimental.Rendering.DebugUI/Vector4Field::decimals
	int32_t ___decimals_10;

public:
	inline static int32_t get_offset_of_incStep_8() { return static_cast<int32_t>(offsetof(Vector4Field_t3946500612, ___incStep_8)); }
	inline float get_incStep_8() const { return ___incStep_8; }
	inline float* get_address_of_incStep_8() { return &___incStep_8; }
	inline void set_incStep_8(float value)
	{
		___incStep_8 = value;
	}

	inline static int32_t get_offset_of_incStepMult_9() { return static_cast<int32_t>(offsetof(Vector4Field_t3946500612, ___incStepMult_9)); }
	inline float get_incStepMult_9() const { return ___incStepMult_9; }
	inline float* get_address_of_incStepMult_9() { return &___incStepMult_9; }
	inline void set_incStepMult_9(float value)
	{
		___incStepMult_9 = value;
	}

	inline static int32_t get_offset_of_decimals_10() { return static_cast<int32_t>(offsetof(Vector4Field_t3946500612, ___decimals_10)); }
	inline int32_t get_decimals_10() const { return ___decimals_10; }
	inline int32_t* get_address_of_decimals_10() { return &___decimals_10; }
	inline void set_decimals_10(int32_t value)
	{
		___decimals_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4FIELD_T3946500612_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CAMERASWITCHER_T1205312903_H
#define CAMERASWITCHER_T1205312903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.CameraSwitcher
struct  CameraSwitcher_t1205312903  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera[] UnityEngine.Experimental.Rendering.CameraSwitcher::m_Cameras
	CameraU5BU5D_t1709987734* ___m_Cameras_4;
	// System.Int32 UnityEngine.Experimental.Rendering.CameraSwitcher::m_CurrentCameraIndex
	int32_t ___m_CurrentCameraIndex_5;
	// UnityEngine.Camera UnityEngine.Experimental.Rendering.CameraSwitcher::m_OriginalCamera
	Camera_t4157153871 * ___m_OriginalCamera_6;
	// UnityEngine.Vector3 UnityEngine.Experimental.Rendering.CameraSwitcher::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// UnityEngine.Quaternion UnityEngine.Experimental.Rendering.CameraSwitcher::m_OriginalCameraRotation
	Quaternion_t2301928331  ___m_OriginalCameraRotation_8;
	// UnityEngine.Camera UnityEngine.Experimental.Rendering.CameraSwitcher::m_CurrentCamera
	Camera_t4157153871 * ___m_CurrentCamera_9;
	// UnityEngine.GUIContent[] UnityEngine.Experimental.Rendering.CameraSwitcher::m_CameraNames
	GUIContentU5BU5D_t2445521510* ___m_CameraNames_10;
	// System.Int32[] UnityEngine.Experimental.Rendering.CameraSwitcher::m_CameraIndices
	Int32U5BU5D_t385246372* ___m_CameraIndices_11;
	// UnityEngine.Experimental.Rendering.DebugUI/EnumField UnityEngine.Experimental.Rendering.CameraSwitcher::m_DebugEntry
	EnumField_t3058254002 * ___m_DebugEntry_12;

public:
	inline static int32_t get_offset_of_m_Cameras_4() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_Cameras_4)); }
	inline CameraU5BU5D_t1709987734* get_m_Cameras_4() const { return ___m_Cameras_4; }
	inline CameraU5BU5D_t1709987734** get_address_of_m_Cameras_4() { return &___m_Cameras_4; }
	inline void set_m_Cameras_4(CameraU5BU5D_t1709987734* value)
	{
		___m_Cameras_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cameras_4), value);
	}

	inline static int32_t get_offset_of_m_CurrentCameraIndex_5() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_CurrentCameraIndex_5)); }
	inline int32_t get_m_CurrentCameraIndex_5() const { return ___m_CurrentCameraIndex_5; }
	inline int32_t* get_address_of_m_CurrentCameraIndex_5() { return &___m_CurrentCameraIndex_5; }
	inline void set_m_CurrentCameraIndex_5(int32_t value)
	{
		___m_CurrentCameraIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCamera_6() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_OriginalCamera_6)); }
	inline Camera_t4157153871 * get_m_OriginalCamera_6() const { return ___m_OriginalCamera_6; }
	inline Camera_t4157153871 ** get_address_of_m_OriginalCamera_6() { return &___m_OriginalCamera_6; }
	inline void set_m_OriginalCamera_6(Camera_t4157153871 * value)
	{
		___m_OriginalCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalCamera_6), value);
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraRotation_8() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_OriginalCameraRotation_8)); }
	inline Quaternion_t2301928331  get_m_OriginalCameraRotation_8() const { return ___m_OriginalCameraRotation_8; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalCameraRotation_8() { return &___m_OriginalCameraRotation_8; }
	inline void set_m_OriginalCameraRotation_8(Quaternion_t2301928331  value)
	{
		___m_OriginalCameraRotation_8 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCamera_9() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_CurrentCamera_9)); }
	inline Camera_t4157153871 * get_m_CurrentCamera_9() const { return ___m_CurrentCamera_9; }
	inline Camera_t4157153871 ** get_address_of_m_CurrentCamera_9() { return &___m_CurrentCamera_9; }
	inline void set_m_CurrentCamera_9(Camera_t4157153871 * value)
	{
		___m_CurrentCamera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCamera_9), value);
	}

	inline static int32_t get_offset_of_m_CameraNames_10() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_CameraNames_10)); }
	inline GUIContentU5BU5D_t2445521510* get_m_CameraNames_10() const { return ___m_CameraNames_10; }
	inline GUIContentU5BU5D_t2445521510** get_address_of_m_CameraNames_10() { return &___m_CameraNames_10; }
	inline void set_m_CameraNames_10(GUIContentU5BU5D_t2445521510* value)
	{
		___m_CameraNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraNames_10), value);
	}

	inline static int32_t get_offset_of_m_CameraIndices_11() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_CameraIndices_11)); }
	inline Int32U5BU5D_t385246372* get_m_CameraIndices_11() const { return ___m_CameraIndices_11; }
	inline Int32U5BU5D_t385246372** get_address_of_m_CameraIndices_11() { return &___m_CameraIndices_11; }
	inline void set_m_CameraIndices_11(Int32U5BU5D_t385246372* value)
	{
		___m_CameraIndices_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraIndices_11), value);
	}

	inline static int32_t get_offset_of_m_DebugEntry_12() { return static_cast<int32_t>(offsetof(CameraSwitcher_t1205312903, ___m_DebugEntry_12)); }
	inline EnumField_t3058254002 * get_m_DebugEntry_12() const { return ___m_DebugEntry_12; }
	inline EnumField_t3058254002 ** get_address_of_m_DebugEntry_12() { return &___m_DebugEntry_12; }
	inline void set_m_DebugEntry_12(EnumField_t3058254002 * value)
	{
		___m_DebugEntry_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugEntry_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASWITCHER_T1205312903_H
#ifndef DEBUGUPDATER_T3339232225_H
#define DEBUGUPDATER_T3339232225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DebugUpdater
struct  DebugUpdater_t3339232225  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGUPDATER_T3339232225_H
#ifndef FREECAMERA_T2435895153_H
#define FREECAMERA_T2435895153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.FreeCamera
struct  FreeCamera_t2435895153  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.Experimental.Rendering.FreeCamera::m_LookSpeedController
	float ___m_LookSpeedController_4;
	// System.Single UnityEngine.Experimental.Rendering.FreeCamera::m_LookSpeedMouse
	float ___m_LookSpeedMouse_5;
	// System.Single UnityEngine.Experimental.Rendering.FreeCamera::m_MoveSpeed
	float ___m_MoveSpeed_6;
	// System.Single UnityEngine.Experimental.Rendering.FreeCamera::m_MoveSpeedIncrement
	float ___m_MoveSpeedIncrement_7;
	// System.Single UnityEngine.Experimental.Rendering.FreeCamera::m_Turbo
	float ___m_Turbo_8;

public:
	inline static int32_t get_offset_of_m_LookSpeedController_4() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153, ___m_LookSpeedController_4)); }
	inline float get_m_LookSpeedController_4() const { return ___m_LookSpeedController_4; }
	inline float* get_address_of_m_LookSpeedController_4() { return &___m_LookSpeedController_4; }
	inline void set_m_LookSpeedController_4(float value)
	{
		___m_LookSpeedController_4 = value;
	}

	inline static int32_t get_offset_of_m_LookSpeedMouse_5() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153, ___m_LookSpeedMouse_5)); }
	inline float get_m_LookSpeedMouse_5() const { return ___m_LookSpeedMouse_5; }
	inline float* get_address_of_m_LookSpeedMouse_5() { return &___m_LookSpeedMouse_5; }
	inline void set_m_LookSpeedMouse_5(float value)
	{
		___m_LookSpeedMouse_5 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeed_6() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153, ___m_MoveSpeed_6)); }
	inline float get_m_MoveSpeed_6() const { return ___m_MoveSpeed_6; }
	inline float* get_address_of_m_MoveSpeed_6() { return &___m_MoveSpeed_6; }
	inline void set_m_MoveSpeed_6(float value)
	{
		___m_MoveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedIncrement_7() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153, ___m_MoveSpeedIncrement_7)); }
	inline float get_m_MoveSpeedIncrement_7() const { return ___m_MoveSpeedIncrement_7; }
	inline float* get_address_of_m_MoveSpeedIncrement_7() { return &___m_MoveSpeedIncrement_7; }
	inline void set_m_MoveSpeedIncrement_7(float value)
	{
		___m_MoveSpeedIncrement_7 = value;
	}

	inline static int32_t get_offset_of_m_Turbo_8() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153, ___m_Turbo_8)); }
	inline float get_m_Turbo_8() const { return ___m_Turbo_8; }
	inline float* get_address_of_m_Turbo_8() { return &___m_Turbo_8; }
	inline void set_m_Turbo_8(float value)
	{
		___m_Turbo_8 = value;
	}
};

struct FreeCamera_t2435895153_StaticFields
{
public:
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kMouseX
	String_t* ___kMouseX_9;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kMouseY
	String_t* ___kMouseY_10;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kRightStickX
	String_t* ___kRightStickX_11;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kRightStickY
	String_t* ___kRightStickY_12;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kVertical
	String_t* ___kVertical_13;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kHorizontal
	String_t* ___kHorizontal_14;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kYAxis
	String_t* ___kYAxis_15;
	// System.String UnityEngine.Experimental.Rendering.FreeCamera::kSpeedAxis
	String_t* ___kSpeedAxis_16;

public:
	inline static int32_t get_offset_of_kMouseX_9() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kMouseX_9)); }
	inline String_t* get_kMouseX_9() const { return ___kMouseX_9; }
	inline String_t** get_address_of_kMouseX_9() { return &___kMouseX_9; }
	inline void set_kMouseX_9(String_t* value)
	{
		___kMouseX_9 = value;
		Il2CppCodeGenWriteBarrier((&___kMouseX_9), value);
	}

	inline static int32_t get_offset_of_kMouseY_10() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kMouseY_10)); }
	inline String_t* get_kMouseY_10() const { return ___kMouseY_10; }
	inline String_t** get_address_of_kMouseY_10() { return &___kMouseY_10; }
	inline void set_kMouseY_10(String_t* value)
	{
		___kMouseY_10 = value;
		Il2CppCodeGenWriteBarrier((&___kMouseY_10), value);
	}

	inline static int32_t get_offset_of_kRightStickX_11() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kRightStickX_11)); }
	inline String_t* get_kRightStickX_11() const { return ___kRightStickX_11; }
	inline String_t** get_address_of_kRightStickX_11() { return &___kRightStickX_11; }
	inline void set_kRightStickX_11(String_t* value)
	{
		___kRightStickX_11 = value;
		Il2CppCodeGenWriteBarrier((&___kRightStickX_11), value);
	}

	inline static int32_t get_offset_of_kRightStickY_12() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kRightStickY_12)); }
	inline String_t* get_kRightStickY_12() const { return ___kRightStickY_12; }
	inline String_t** get_address_of_kRightStickY_12() { return &___kRightStickY_12; }
	inline void set_kRightStickY_12(String_t* value)
	{
		___kRightStickY_12 = value;
		Il2CppCodeGenWriteBarrier((&___kRightStickY_12), value);
	}

	inline static int32_t get_offset_of_kVertical_13() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kVertical_13)); }
	inline String_t* get_kVertical_13() const { return ___kVertical_13; }
	inline String_t** get_address_of_kVertical_13() { return &___kVertical_13; }
	inline void set_kVertical_13(String_t* value)
	{
		___kVertical_13 = value;
		Il2CppCodeGenWriteBarrier((&___kVertical_13), value);
	}

	inline static int32_t get_offset_of_kHorizontal_14() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kHorizontal_14)); }
	inline String_t* get_kHorizontal_14() const { return ___kHorizontal_14; }
	inline String_t** get_address_of_kHorizontal_14() { return &___kHorizontal_14; }
	inline void set_kHorizontal_14(String_t* value)
	{
		___kHorizontal_14 = value;
		Il2CppCodeGenWriteBarrier((&___kHorizontal_14), value);
	}

	inline static int32_t get_offset_of_kYAxis_15() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kYAxis_15)); }
	inline String_t* get_kYAxis_15() const { return ___kYAxis_15; }
	inline String_t** get_address_of_kYAxis_15() { return &___kYAxis_15; }
	inline void set_kYAxis_15(String_t* value)
	{
		___kYAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___kYAxis_15), value);
	}

	inline static int32_t get_offset_of_kSpeedAxis_16() { return static_cast<int32_t>(offsetof(FreeCamera_t2435895153_StaticFields, ___kSpeedAxis_16)); }
	inline String_t* get_kSpeedAxis_16() const { return ___kSpeedAxis_16; }
	inline String_t** get_address_of_kSpeedAxis_16() { return &___kSpeedAxis_16; }
	inline void set_kSpeedAxis_16(String_t* value)
	{
		___kSpeedAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___kSpeedAxis_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREECAMERA_T2435895153_H
#ifndef DEBUGUIHANDLERWIDGET_T1600606064_H
#define DEBUGUIHANDLERWIDGET_T1600606064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget
struct  DebugUIHandlerWidget_t1600606064  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget::colorDefault
	Color_t2555686324  ___colorDefault_4;
	// UnityEngine.Color UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget::colorSelected
	Color_t2555686324  ___colorSelected_5;
	// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget::<parentUIHandler>k__BackingField
	DebugUIHandlerWidget_t1600606064 * ___U3CparentUIHandlerU3Ek__BackingField_6;
	// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget::<previousUIHandler>k__BackingField
	DebugUIHandlerWidget_t1600606064 * ___U3CpreviousUIHandlerU3Ek__BackingField_7;
	// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget::<nextUIHandler>k__BackingField
	DebugUIHandlerWidget_t1600606064 * ___U3CnextUIHandlerU3Ek__BackingField_8;
	// UnityEngine.Experimental.Rendering.DebugUI/Widget UnityEngine.Experimental.Rendering.UI.DebugUIHandlerWidget::m_Widget
	Widget_t1336755483 * ___m_Widget_9;

public:
	inline static int32_t get_offset_of_colorDefault_4() { return static_cast<int32_t>(offsetof(DebugUIHandlerWidget_t1600606064, ___colorDefault_4)); }
	inline Color_t2555686324  get_colorDefault_4() const { return ___colorDefault_4; }
	inline Color_t2555686324 * get_address_of_colorDefault_4() { return &___colorDefault_4; }
	inline void set_colorDefault_4(Color_t2555686324  value)
	{
		___colorDefault_4 = value;
	}

	inline static int32_t get_offset_of_colorSelected_5() { return static_cast<int32_t>(offsetof(DebugUIHandlerWidget_t1600606064, ___colorSelected_5)); }
	inline Color_t2555686324  get_colorSelected_5() const { return ___colorSelected_5; }
	inline Color_t2555686324 * get_address_of_colorSelected_5() { return &___colorSelected_5; }
	inline void set_colorSelected_5(Color_t2555686324  value)
	{
		___colorSelected_5 = value;
	}

	inline static int32_t get_offset_of_U3CparentUIHandlerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DebugUIHandlerWidget_t1600606064, ___U3CparentUIHandlerU3Ek__BackingField_6)); }
	inline DebugUIHandlerWidget_t1600606064 * get_U3CparentUIHandlerU3Ek__BackingField_6() const { return ___U3CparentUIHandlerU3Ek__BackingField_6; }
	inline DebugUIHandlerWidget_t1600606064 ** get_address_of_U3CparentUIHandlerU3Ek__BackingField_6() { return &___U3CparentUIHandlerU3Ek__BackingField_6; }
	inline void set_U3CparentUIHandlerU3Ek__BackingField_6(DebugUIHandlerWidget_t1600606064 * value)
	{
		___U3CparentUIHandlerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparentUIHandlerU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpreviousUIHandlerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DebugUIHandlerWidget_t1600606064, ___U3CpreviousUIHandlerU3Ek__BackingField_7)); }
	inline DebugUIHandlerWidget_t1600606064 * get_U3CpreviousUIHandlerU3Ek__BackingField_7() const { return ___U3CpreviousUIHandlerU3Ek__BackingField_7; }
	inline DebugUIHandlerWidget_t1600606064 ** get_address_of_U3CpreviousUIHandlerU3Ek__BackingField_7() { return &___U3CpreviousUIHandlerU3Ek__BackingField_7; }
	inline void set_U3CpreviousUIHandlerU3Ek__BackingField_7(DebugUIHandlerWidget_t1600606064 * value)
	{
		___U3CpreviousUIHandlerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpreviousUIHandlerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CnextUIHandlerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DebugUIHandlerWidget_t1600606064, ___U3CnextUIHandlerU3Ek__BackingField_8)); }
	inline DebugUIHandlerWidget_t1600606064 * get_U3CnextUIHandlerU3Ek__BackingField_8() const { return ___U3CnextUIHandlerU3Ek__BackingField_8; }
	inline DebugUIHandlerWidget_t1600606064 ** get_address_of_U3CnextUIHandlerU3Ek__BackingField_8() { return &___U3CnextUIHandlerU3Ek__BackingField_8; }
	inline void set_U3CnextUIHandlerU3Ek__BackingField_8(DebugUIHandlerWidget_t1600606064 * value)
	{
		___U3CnextUIHandlerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnextUIHandlerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_m_Widget_9() { return static_cast<int32_t>(offsetof(DebugUIHandlerWidget_t1600606064, ___m_Widget_9)); }
	inline Widget_t1336755483 * get_m_Widget_9() const { return ___m_Widget_9; }
	inline Widget_t1336755483 ** get_address_of_m_Widget_9() { return &___m_Widget_9; }
	inline void set_m_Widget_9(Widget_t1336755483 * value)
	{
		___m_Widget_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Widget_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGUIHANDLERWIDGET_T1600606064_H
#ifndef POSTPROCESSDEBUG_T1827270515_H
#define POSTPROCESSDEBUG_T1827270515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessDebug
struct  PostProcessDebug_t1827270515  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer UnityEngine.Rendering.PostProcessing.PostProcessDebug::postProcessLayer
	PostProcessLayer_t4264744195 * ___postProcessLayer_4;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer UnityEngine.Rendering.PostProcessing.PostProcessDebug::m_PreviousPostProcessLayer
	PostProcessLayer_t4264744195 * ___m_PreviousPostProcessLayer_5;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebug::lightMeter
	bool ___lightMeter_6;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebug::histogram
	bool ___histogram_7;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebug::waveform
	bool ___waveform_8;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebug::vectorscope
	bool ___vectorscope_9;
	// UnityEngine.Rendering.PostProcessing.DebugOverlay UnityEngine.Rendering.PostProcessing.PostProcessDebug::debugOverlay
	int32_t ___debugOverlay_10;
	// UnityEngine.Camera UnityEngine.Rendering.PostProcessing.PostProcessDebug::m_CurrentCamera
	Camera_t4157153871 * ___m_CurrentCamera_11;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessDebug::m_CmdAfterEverything
	CommandBuffer_t2206337031 * ___m_CmdAfterEverything_12;

public:
	inline static int32_t get_offset_of_postProcessLayer_4() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___postProcessLayer_4)); }
	inline PostProcessLayer_t4264744195 * get_postProcessLayer_4() const { return ___postProcessLayer_4; }
	inline PostProcessLayer_t4264744195 ** get_address_of_postProcessLayer_4() { return &___postProcessLayer_4; }
	inline void set_postProcessLayer_4(PostProcessLayer_t4264744195 * value)
	{
		___postProcessLayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___postProcessLayer_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousPostProcessLayer_5() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___m_PreviousPostProcessLayer_5)); }
	inline PostProcessLayer_t4264744195 * get_m_PreviousPostProcessLayer_5() const { return ___m_PreviousPostProcessLayer_5; }
	inline PostProcessLayer_t4264744195 ** get_address_of_m_PreviousPostProcessLayer_5() { return &___m_PreviousPostProcessLayer_5; }
	inline void set_m_PreviousPostProcessLayer_5(PostProcessLayer_t4264744195 * value)
	{
		___m_PreviousPostProcessLayer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousPostProcessLayer_5), value);
	}

	inline static int32_t get_offset_of_lightMeter_6() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___lightMeter_6)); }
	inline bool get_lightMeter_6() const { return ___lightMeter_6; }
	inline bool* get_address_of_lightMeter_6() { return &___lightMeter_6; }
	inline void set_lightMeter_6(bool value)
	{
		___lightMeter_6 = value;
	}

	inline static int32_t get_offset_of_histogram_7() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___histogram_7)); }
	inline bool get_histogram_7() const { return ___histogram_7; }
	inline bool* get_address_of_histogram_7() { return &___histogram_7; }
	inline void set_histogram_7(bool value)
	{
		___histogram_7 = value;
	}

	inline static int32_t get_offset_of_waveform_8() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___waveform_8)); }
	inline bool get_waveform_8() const { return ___waveform_8; }
	inline bool* get_address_of_waveform_8() { return &___waveform_8; }
	inline void set_waveform_8(bool value)
	{
		___waveform_8 = value;
	}

	inline static int32_t get_offset_of_vectorscope_9() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___vectorscope_9)); }
	inline bool get_vectorscope_9() const { return ___vectorscope_9; }
	inline bool* get_address_of_vectorscope_9() { return &___vectorscope_9; }
	inline void set_vectorscope_9(bool value)
	{
		___vectorscope_9 = value;
	}

	inline static int32_t get_offset_of_debugOverlay_10() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___debugOverlay_10)); }
	inline int32_t get_debugOverlay_10() const { return ___debugOverlay_10; }
	inline int32_t* get_address_of_debugOverlay_10() { return &___debugOverlay_10; }
	inline void set_debugOverlay_10(int32_t value)
	{
		___debugOverlay_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCamera_11() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___m_CurrentCamera_11)); }
	inline Camera_t4157153871 * get_m_CurrentCamera_11() const { return ___m_CurrentCamera_11; }
	inline Camera_t4157153871 ** get_address_of_m_CurrentCamera_11() { return &___m_CurrentCamera_11; }
	inline void set_m_CurrentCamera_11(Camera_t4157153871 * value)
	{
		___m_CurrentCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCamera_11), value);
	}

	inline static int32_t get_offset_of_m_CmdAfterEverything_12() { return static_cast<int32_t>(offsetof(PostProcessDebug_t1827270515, ___m_CmdAfterEverything_12)); }
	inline CommandBuffer_t2206337031 * get_m_CmdAfterEverything_12() const { return ___m_CmdAfterEverything_12; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_CmdAfterEverything_12() { return &___m_CmdAfterEverything_12; }
	inline void set_m_CmdAfterEverything_12(CommandBuffer_t2206337031 * value)
	{
		___m_CmdAfterEverything_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CmdAfterEverything_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSDEBUG_T1827270515_H
#ifndef POSTPROCESSLAYER_T4264744195_H
#define POSTPROCESSLAYER_T4264744195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer
struct  PostProcessLayer_t4264744195  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityEngine.Rendering.PostProcessing.PostProcessLayer::volumeTrigger
	Transform_t3600365921 * ___volumeTrigger_4;
	// UnityEngine.LayerMask UnityEngine.Rendering.PostProcessing.PostProcessLayer::volumeLayer
	LayerMask_t3493934918  ___volumeLayer_5;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::stopNaNPropagation
	bool ___stopNaNPropagation_6;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer/Antialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::antialiasingMode
	int32_t ___antialiasingMode_7;
	// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::temporalAntialiasing
	TemporalAntialiasing_t1482226156 * ___temporalAntialiasing_8;
	// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::subpixelMorphologicalAntialiasing
	SubpixelMorphologicalAntialiasing_t3102233738 * ___subpixelMorphologicalAntialiasing_9;
	// UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::fastApproximateAntialiasing
	FastApproximateAntialiasing_t3757489215 * ___fastApproximateAntialiasing_10;
	// UnityEngine.Rendering.PostProcessing.Fog UnityEngine.Rendering.PostProcessing.PostProcessLayer::fog
	Fog_t2420217198 * ___fog_11;
	// UnityEngine.Rendering.PostProcessing.Dithering UnityEngine.Rendering.PostProcessing.PostProcessLayer::dithering
	Dithering_t544635223 * ___dithering_12;
	// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer UnityEngine.Rendering.PostProcessing.PostProcessLayer::debugLayer
	PostProcessDebugLayer_t3290441360 * ___debugLayer_13;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Resources
	PostProcessResources_t1163236733 * ___m_Resources_14;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_ShowToolkit
	bool ___m_ShowToolkit_15;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_ShowCustomSorter
	bool ___m_ShowCustomSorter_16;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::breakBeforeColorGrading
	bool ___breakBeforeColorGrading_17;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_BeforeTransparentBundles
	List_1_t27135406 * ___m_BeforeTransparentBundles_18;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_BeforeStackBundles
	List_1_t27135406 * ___m_BeforeStackBundles_19;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_AfterStackBundles
	List_1_t27135406 * ___m_AfterStackBundles_20;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.PostProcessEvent,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef>> UnityEngine.Rendering.PostProcessing.PostProcessLayer::<sortedBundles>k__BackingField
	Dictionary_2_t638195886 * ___U3CsortedBundlesU3Ek__BackingField_21;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::<haveBundlesBeenInited>k__BackingField
	bool ___U3ChaveBundlesBeenInitedU3Ek__BackingField_22;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Bundles
	Dictionary_2_t1614383438 * ___m_Bundles_23;
	// UnityEngine.Rendering.PostProcessing.PropertySheetFactory UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_PropertySheetFactory
	PropertySheetFactory_t1490101248 * ___m_PropertySheetFactory_24;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBufferBeforeReflections
	CommandBuffer_t2206337031 * ___m_LegacyCmdBufferBeforeReflections_25;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBufferBeforeLighting
	CommandBuffer_t2206337031 * ___m_LegacyCmdBufferBeforeLighting_26;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBufferOpaque
	CommandBuffer_t2206337031 * ___m_LegacyCmdBufferOpaque_27;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBuffer
	CommandBuffer_t2206337031 * ___m_LegacyCmdBuffer_28;
	// UnityEngine.Camera UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Camera
	Camera_t4157153871 * ___m_Camera_29;
	// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_CurrentContext
	PostProcessRenderContext_t597611190 * ___m_CurrentContext_30;
	// UnityEngine.Rendering.PostProcessing.LogHistogram UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LogHistogram
	LogHistogram_t1187052756 * ___m_LogHistogram_31;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_SettingsUpdateNeeded
	bool ___m_SettingsUpdateNeeded_32;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_IsRenderingInSceneView
	bool ___m_IsRenderingInSceneView_33;
	// UnityEngine.Rendering.PostProcessing.TargetPool UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_TargetPool
	TargetPool_t1535233241 * ___m_TargetPool_34;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_NaNKilled
	bool ___m_NaNKilled_35;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_ActiveEffects
	List_1_t1473134979 * ___m_ActiveEffects_36;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.RenderTargetIdentifier> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Targets
	List_1_t3551259242 * ___m_Targets_37;

public:
	inline static int32_t get_offset_of_volumeTrigger_4() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___volumeTrigger_4)); }
	inline Transform_t3600365921 * get_volumeTrigger_4() const { return ___volumeTrigger_4; }
	inline Transform_t3600365921 ** get_address_of_volumeTrigger_4() { return &___volumeTrigger_4; }
	inline void set_volumeTrigger_4(Transform_t3600365921 * value)
	{
		___volumeTrigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___volumeTrigger_4), value);
	}

	inline static int32_t get_offset_of_volumeLayer_5() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___volumeLayer_5)); }
	inline LayerMask_t3493934918  get_volumeLayer_5() const { return ___volumeLayer_5; }
	inline LayerMask_t3493934918 * get_address_of_volumeLayer_5() { return &___volumeLayer_5; }
	inline void set_volumeLayer_5(LayerMask_t3493934918  value)
	{
		___volumeLayer_5 = value;
	}

	inline static int32_t get_offset_of_stopNaNPropagation_6() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___stopNaNPropagation_6)); }
	inline bool get_stopNaNPropagation_6() const { return ___stopNaNPropagation_6; }
	inline bool* get_address_of_stopNaNPropagation_6() { return &___stopNaNPropagation_6; }
	inline void set_stopNaNPropagation_6(bool value)
	{
		___stopNaNPropagation_6 = value;
	}

	inline static int32_t get_offset_of_antialiasingMode_7() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___antialiasingMode_7)); }
	inline int32_t get_antialiasingMode_7() const { return ___antialiasingMode_7; }
	inline int32_t* get_address_of_antialiasingMode_7() { return &___antialiasingMode_7; }
	inline void set_antialiasingMode_7(int32_t value)
	{
		___antialiasingMode_7 = value;
	}

	inline static int32_t get_offset_of_temporalAntialiasing_8() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___temporalAntialiasing_8)); }
	inline TemporalAntialiasing_t1482226156 * get_temporalAntialiasing_8() const { return ___temporalAntialiasing_8; }
	inline TemporalAntialiasing_t1482226156 ** get_address_of_temporalAntialiasing_8() { return &___temporalAntialiasing_8; }
	inline void set_temporalAntialiasing_8(TemporalAntialiasing_t1482226156 * value)
	{
		___temporalAntialiasing_8 = value;
		Il2CppCodeGenWriteBarrier((&___temporalAntialiasing_8), value);
	}

	inline static int32_t get_offset_of_subpixelMorphologicalAntialiasing_9() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___subpixelMorphologicalAntialiasing_9)); }
	inline SubpixelMorphologicalAntialiasing_t3102233738 * get_subpixelMorphologicalAntialiasing_9() const { return ___subpixelMorphologicalAntialiasing_9; }
	inline SubpixelMorphologicalAntialiasing_t3102233738 ** get_address_of_subpixelMorphologicalAntialiasing_9() { return &___subpixelMorphologicalAntialiasing_9; }
	inline void set_subpixelMorphologicalAntialiasing_9(SubpixelMorphologicalAntialiasing_t3102233738 * value)
	{
		___subpixelMorphologicalAntialiasing_9 = value;
		Il2CppCodeGenWriteBarrier((&___subpixelMorphologicalAntialiasing_9), value);
	}

	inline static int32_t get_offset_of_fastApproximateAntialiasing_10() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___fastApproximateAntialiasing_10)); }
	inline FastApproximateAntialiasing_t3757489215 * get_fastApproximateAntialiasing_10() const { return ___fastApproximateAntialiasing_10; }
	inline FastApproximateAntialiasing_t3757489215 ** get_address_of_fastApproximateAntialiasing_10() { return &___fastApproximateAntialiasing_10; }
	inline void set_fastApproximateAntialiasing_10(FastApproximateAntialiasing_t3757489215 * value)
	{
		___fastApproximateAntialiasing_10 = value;
		Il2CppCodeGenWriteBarrier((&___fastApproximateAntialiasing_10), value);
	}

	inline static int32_t get_offset_of_fog_11() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___fog_11)); }
	inline Fog_t2420217198 * get_fog_11() const { return ___fog_11; }
	inline Fog_t2420217198 ** get_address_of_fog_11() { return &___fog_11; }
	inline void set_fog_11(Fog_t2420217198 * value)
	{
		___fog_11 = value;
		Il2CppCodeGenWriteBarrier((&___fog_11), value);
	}

	inline static int32_t get_offset_of_dithering_12() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___dithering_12)); }
	inline Dithering_t544635223 * get_dithering_12() const { return ___dithering_12; }
	inline Dithering_t544635223 ** get_address_of_dithering_12() { return &___dithering_12; }
	inline void set_dithering_12(Dithering_t544635223 * value)
	{
		___dithering_12 = value;
		Il2CppCodeGenWriteBarrier((&___dithering_12), value);
	}

	inline static int32_t get_offset_of_debugLayer_13() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___debugLayer_13)); }
	inline PostProcessDebugLayer_t3290441360 * get_debugLayer_13() const { return ___debugLayer_13; }
	inline PostProcessDebugLayer_t3290441360 ** get_address_of_debugLayer_13() { return &___debugLayer_13; }
	inline void set_debugLayer_13(PostProcessDebugLayer_t3290441360 * value)
	{
		___debugLayer_13 = value;
		Il2CppCodeGenWriteBarrier((&___debugLayer_13), value);
	}

	inline static int32_t get_offset_of_m_Resources_14() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_Resources_14)); }
	inline PostProcessResources_t1163236733 * get_m_Resources_14() const { return ___m_Resources_14; }
	inline PostProcessResources_t1163236733 ** get_address_of_m_Resources_14() { return &___m_Resources_14; }
	inline void set_m_Resources_14(PostProcessResources_t1163236733 * value)
	{
		___m_Resources_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resources_14), value);
	}

	inline static int32_t get_offset_of_m_ShowToolkit_15() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_ShowToolkit_15)); }
	inline bool get_m_ShowToolkit_15() const { return ___m_ShowToolkit_15; }
	inline bool* get_address_of_m_ShowToolkit_15() { return &___m_ShowToolkit_15; }
	inline void set_m_ShowToolkit_15(bool value)
	{
		___m_ShowToolkit_15 = value;
	}

	inline static int32_t get_offset_of_m_ShowCustomSorter_16() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_ShowCustomSorter_16)); }
	inline bool get_m_ShowCustomSorter_16() const { return ___m_ShowCustomSorter_16; }
	inline bool* get_address_of_m_ShowCustomSorter_16() { return &___m_ShowCustomSorter_16; }
	inline void set_m_ShowCustomSorter_16(bool value)
	{
		___m_ShowCustomSorter_16 = value;
	}

	inline static int32_t get_offset_of_breakBeforeColorGrading_17() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___breakBeforeColorGrading_17)); }
	inline bool get_breakBeforeColorGrading_17() const { return ___breakBeforeColorGrading_17; }
	inline bool* get_address_of_breakBeforeColorGrading_17() { return &___breakBeforeColorGrading_17; }
	inline void set_breakBeforeColorGrading_17(bool value)
	{
		___breakBeforeColorGrading_17 = value;
	}

	inline static int32_t get_offset_of_m_BeforeTransparentBundles_18() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_BeforeTransparentBundles_18)); }
	inline List_1_t27135406 * get_m_BeforeTransparentBundles_18() const { return ___m_BeforeTransparentBundles_18; }
	inline List_1_t27135406 ** get_address_of_m_BeforeTransparentBundles_18() { return &___m_BeforeTransparentBundles_18; }
	inline void set_m_BeforeTransparentBundles_18(List_1_t27135406 * value)
	{
		___m_BeforeTransparentBundles_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_BeforeTransparentBundles_18), value);
	}

	inline static int32_t get_offset_of_m_BeforeStackBundles_19() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_BeforeStackBundles_19)); }
	inline List_1_t27135406 * get_m_BeforeStackBundles_19() const { return ___m_BeforeStackBundles_19; }
	inline List_1_t27135406 ** get_address_of_m_BeforeStackBundles_19() { return &___m_BeforeStackBundles_19; }
	inline void set_m_BeforeStackBundles_19(List_1_t27135406 * value)
	{
		___m_BeforeStackBundles_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_BeforeStackBundles_19), value);
	}

	inline static int32_t get_offset_of_m_AfterStackBundles_20() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_AfterStackBundles_20)); }
	inline List_1_t27135406 * get_m_AfterStackBundles_20() const { return ___m_AfterStackBundles_20; }
	inline List_1_t27135406 ** get_address_of_m_AfterStackBundles_20() { return &___m_AfterStackBundles_20; }
	inline void set_m_AfterStackBundles_20(List_1_t27135406 * value)
	{
		___m_AfterStackBundles_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_AfterStackBundles_20), value);
	}

	inline static int32_t get_offset_of_U3CsortedBundlesU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___U3CsortedBundlesU3Ek__BackingField_21)); }
	inline Dictionary_2_t638195886 * get_U3CsortedBundlesU3Ek__BackingField_21() const { return ___U3CsortedBundlesU3Ek__BackingField_21; }
	inline Dictionary_2_t638195886 ** get_address_of_U3CsortedBundlesU3Ek__BackingField_21() { return &___U3CsortedBundlesU3Ek__BackingField_21; }
	inline void set_U3CsortedBundlesU3Ek__BackingField_21(Dictionary_2_t638195886 * value)
	{
		___U3CsortedBundlesU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsortedBundlesU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3ChaveBundlesBeenInitedU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___U3ChaveBundlesBeenInitedU3Ek__BackingField_22)); }
	inline bool get_U3ChaveBundlesBeenInitedU3Ek__BackingField_22() const { return ___U3ChaveBundlesBeenInitedU3Ek__BackingField_22; }
	inline bool* get_address_of_U3ChaveBundlesBeenInitedU3Ek__BackingField_22() { return &___U3ChaveBundlesBeenInitedU3Ek__BackingField_22; }
	inline void set_U3ChaveBundlesBeenInitedU3Ek__BackingField_22(bool value)
	{
		___U3ChaveBundlesBeenInitedU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_m_Bundles_23() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_Bundles_23)); }
	inline Dictionary_2_t1614383438 * get_m_Bundles_23() const { return ___m_Bundles_23; }
	inline Dictionary_2_t1614383438 ** get_address_of_m_Bundles_23() { return &___m_Bundles_23; }
	inline void set_m_Bundles_23(Dictionary_2_t1614383438 * value)
	{
		___m_Bundles_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bundles_23), value);
	}

	inline static int32_t get_offset_of_m_PropertySheetFactory_24() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_PropertySheetFactory_24)); }
	inline PropertySheetFactory_t1490101248 * get_m_PropertySheetFactory_24() const { return ___m_PropertySheetFactory_24; }
	inline PropertySheetFactory_t1490101248 ** get_address_of_m_PropertySheetFactory_24() { return &___m_PropertySheetFactory_24; }
	inline void set_m_PropertySheetFactory_24(PropertySheetFactory_t1490101248 * value)
	{
		___m_PropertySheetFactory_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheetFactory_24), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBufferBeforeReflections_25() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_LegacyCmdBufferBeforeReflections_25)); }
	inline CommandBuffer_t2206337031 * get_m_LegacyCmdBufferBeforeReflections_25() const { return ___m_LegacyCmdBufferBeforeReflections_25; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_LegacyCmdBufferBeforeReflections_25() { return &___m_LegacyCmdBufferBeforeReflections_25; }
	inline void set_m_LegacyCmdBufferBeforeReflections_25(CommandBuffer_t2206337031 * value)
	{
		___m_LegacyCmdBufferBeforeReflections_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBufferBeforeReflections_25), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBufferBeforeLighting_26() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_LegacyCmdBufferBeforeLighting_26)); }
	inline CommandBuffer_t2206337031 * get_m_LegacyCmdBufferBeforeLighting_26() const { return ___m_LegacyCmdBufferBeforeLighting_26; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_LegacyCmdBufferBeforeLighting_26() { return &___m_LegacyCmdBufferBeforeLighting_26; }
	inline void set_m_LegacyCmdBufferBeforeLighting_26(CommandBuffer_t2206337031 * value)
	{
		___m_LegacyCmdBufferBeforeLighting_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBufferBeforeLighting_26), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBufferOpaque_27() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_LegacyCmdBufferOpaque_27)); }
	inline CommandBuffer_t2206337031 * get_m_LegacyCmdBufferOpaque_27() const { return ___m_LegacyCmdBufferOpaque_27; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_LegacyCmdBufferOpaque_27() { return &___m_LegacyCmdBufferOpaque_27; }
	inline void set_m_LegacyCmdBufferOpaque_27(CommandBuffer_t2206337031 * value)
	{
		___m_LegacyCmdBufferOpaque_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBufferOpaque_27), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBuffer_28() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_LegacyCmdBuffer_28)); }
	inline CommandBuffer_t2206337031 * get_m_LegacyCmdBuffer_28() const { return ___m_LegacyCmdBuffer_28; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_LegacyCmdBuffer_28() { return &___m_LegacyCmdBuffer_28; }
	inline void set_m_LegacyCmdBuffer_28(CommandBuffer_t2206337031 * value)
	{
		___m_LegacyCmdBuffer_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBuffer_28), value);
	}

	inline static int32_t get_offset_of_m_Camera_29() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_Camera_29)); }
	inline Camera_t4157153871 * get_m_Camera_29() const { return ___m_Camera_29; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_29() { return &___m_Camera_29; }
	inline void set_m_Camera_29(Camera_t4157153871 * value)
	{
		___m_Camera_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_29), value);
	}

	inline static int32_t get_offset_of_m_CurrentContext_30() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_CurrentContext_30)); }
	inline PostProcessRenderContext_t597611190 * get_m_CurrentContext_30() const { return ___m_CurrentContext_30; }
	inline PostProcessRenderContext_t597611190 ** get_address_of_m_CurrentContext_30() { return &___m_CurrentContext_30; }
	inline void set_m_CurrentContext_30(PostProcessRenderContext_t597611190 * value)
	{
		___m_CurrentContext_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentContext_30), value);
	}

	inline static int32_t get_offset_of_m_LogHistogram_31() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_LogHistogram_31)); }
	inline LogHistogram_t1187052756 * get_m_LogHistogram_31() const { return ___m_LogHistogram_31; }
	inline LogHistogram_t1187052756 ** get_address_of_m_LogHistogram_31() { return &___m_LogHistogram_31; }
	inline void set_m_LogHistogram_31(LogHistogram_t1187052756 * value)
	{
		___m_LogHistogram_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogHistogram_31), value);
	}

	inline static int32_t get_offset_of_m_SettingsUpdateNeeded_32() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_SettingsUpdateNeeded_32)); }
	inline bool get_m_SettingsUpdateNeeded_32() const { return ___m_SettingsUpdateNeeded_32; }
	inline bool* get_address_of_m_SettingsUpdateNeeded_32() { return &___m_SettingsUpdateNeeded_32; }
	inline void set_m_SettingsUpdateNeeded_32(bool value)
	{
		___m_SettingsUpdateNeeded_32 = value;
	}

	inline static int32_t get_offset_of_m_IsRenderingInSceneView_33() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_IsRenderingInSceneView_33)); }
	inline bool get_m_IsRenderingInSceneView_33() const { return ___m_IsRenderingInSceneView_33; }
	inline bool* get_address_of_m_IsRenderingInSceneView_33() { return &___m_IsRenderingInSceneView_33; }
	inline void set_m_IsRenderingInSceneView_33(bool value)
	{
		___m_IsRenderingInSceneView_33 = value;
	}

	inline static int32_t get_offset_of_m_TargetPool_34() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_TargetPool_34)); }
	inline TargetPool_t1535233241 * get_m_TargetPool_34() const { return ___m_TargetPool_34; }
	inline TargetPool_t1535233241 ** get_address_of_m_TargetPool_34() { return &___m_TargetPool_34; }
	inline void set_m_TargetPool_34(TargetPool_t1535233241 * value)
	{
		___m_TargetPool_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetPool_34), value);
	}

	inline static int32_t get_offset_of_m_NaNKilled_35() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_NaNKilled_35)); }
	inline bool get_m_NaNKilled_35() const { return ___m_NaNKilled_35; }
	inline bool* get_address_of_m_NaNKilled_35() { return &___m_NaNKilled_35; }
	inline void set_m_NaNKilled_35(bool value)
	{
		___m_NaNKilled_35 = value;
	}

	inline static int32_t get_offset_of_m_ActiveEffects_36() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_ActiveEffects_36)); }
	inline List_1_t1473134979 * get_m_ActiveEffects_36() const { return ___m_ActiveEffects_36; }
	inline List_1_t1473134979 ** get_address_of_m_ActiveEffects_36() { return &___m_ActiveEffects_36; }
	inline void set_m_ActiveEffects_36(List_1_t1473134979 * value)
	{
		___m_ActiveEffects_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveEffects_36), value);
	}

	inline static int32_t get_offset_of_m_Targets_37() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195, ___m_Targets_37)); }
	inline List_1_t3551259242 * get_m_Targets_37() const { return ___m_Targets_37; }
	inline List_1_t3551259242 ** get_address_of_m_Targets_37() { return &___m_Targets_37; }
	inline void set_m_Targets_37(List_1_t3551259242 * value)
	{
		___m_Targets_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_Targets_37), value);
	}
};

struct PostProcessLayer_t4264744195_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle>,UnityEngine.Rendering.PostProcessing.PostProcessBundle> UnityEngine.Rendering.PostProcessing.PostProcessLayer::<>f__am$cache0
	Func_2_t569305667 * ___U3CU3Ef__amU24cache0_38;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_38() { return static_cast<int32_t>(offsetof(PostProcessLayer_t4264744195_StaticFields, ___U3CU3Ef__amU24cache0_38)); }
	inline Func_2_t569305667 * get_U3CU3Ef__amU24cache0_38() const { return ___U3CU3Ef__amU24cache0_38; }
	inline Func_2_t569305667 ** get_address_of_U3CU3Ef__amU24cache0_38() { return &___U3CU3Ef__amU24cache0_38; }
	inline void set_U3CU3Ef__amU24cache0_38(Func_2_t569305667 * value)
	{
		___U3CU3Ef__amU24cache0_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSLAYER_T4264744195_H
#ifndef POSTPROCESSVOLUME_T2983056917_H
#define POSTPROCESSVOLUME_T2983056917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessVolume
struct  PostProcessVolume_t2983056917  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::sharedProfile
	PostProcessProfile_t3936051061 * ___sharedProfile_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessVolume::isGlobal
	bool ___isGlobal_5;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::blendDistance
	float ___blendDistance_6;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::weight
	float ___weight_7;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::priority
	float ___priority_8;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_PreviousLayer
	int32_t ___m_PreviousLayer_9;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_PreviousPriority
	float ___m_PreviousPriority_10;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_TempColliders
	List_1_t3245421752 * ___m_TempColliders_11;
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_InternalProfile
	PostProcessProfile_t3936051061 * ___m_InternalProfile_12;

public:
	inline static int32_t get_offset_of_sharedProfile_4() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___sharedProfile_4)); }
	inline PostProcessProfile_t3936051061 * get_sharedProfile_4() const { return ___sharedProfile_4; }
	inline PostProcessProfile_t3936051061 ** get_address_of_sharedProfile_4() { return &___sharedProfile_4; }
	inline void set_sharedProfile_4(PostProcessProfile_t3936051061 * value)
	{
		___sharedProfile_4 = value;
		Il2CppCodeGenWriteBarrier((&___sharedProfile_4), value);
	}

	inline static int32_t get_offset_of_isGlobal_5() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___isGlobal_5)); }
	inline bool get_isGlobal_5() const { return ___isGlobal_5; }
	inline bool* get_address_of_isGlobal_5() { return &___isGlobal_5; }
	inline void set_isGlobal_5(bool value)
	{
		___isGlobal_5 = value;
	}

	inline static int32_t get_offset_of_blendDistance_6() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___blendDistance_6)); }
	inline float get_blendDistance_6() const { return ___blendDistance_6; }
	inline float* get_address_of_blendDistance_6() { return &___blendDistance_6; }
	inline void set_blendDistance_6(float value)
	{
		___blendDistance_6 = value;
	}

	inline static int32_t get_offset_of_weight_7() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___weight_7)); }
	inline float get_weight_7() const { return ___weight_7; }
	inline float* get_address_of_weight_7() { return &___weight_7; }
	inline void set_weight_7(float value)
	{
		___weight_7 = value;
	}

	inline static int32_t get_offset_of_priority_8() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___priority_8)); }
	inline float get_priority_8() const { return ___priority_8; }
	inline float* get_address_of_priority_8() { return &___priority_8; }
	inline void set_priority_8(float value)
	{
		___priority_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousLayer_9() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___m_PreviousLayer_9)); }
	inline int32_t get_m_PreviousLayer_9() const { return ___m_PreviousLayer_9; }
	inline int32_t* get_address_of_m_PreviousLayer_9() { return &___m_PreviousLayer_9; }
	inline void set_m_PreviousLayer_9(int32_t value)
	{
		___m_PreviousLayer_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPriority_10() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___m_PreviousPriority_10)); }
	inline float get_m_PreviousPriority_10() const { return ___m_PreviousPriority_10; }
	inline float* get_address_of_m_PreviousPriority_10() { return &___m_PreviousPriority_10; }
	inline void set_m_PreviousPriority_10(float value)
	{
		___m_PreviousPriority_10 = value;
	}

	inline static int32_t get_offset_of_m_TempColliders_11() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___m_TempColliders_11)); }
	inline List_1_t3245421752 * get_m_TempColliders_11() const { return ___m_TempColliders_11; }
	inline List_1_t3245421752 ** get_address_of_m_TempColliders_11() { return &___m_TempColliders_11; }
	inline void set_m_TempColliders_11(List_1_t3245421752 * value)
	{
		___m_TempColliders_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempColliders_11), value);
	}

	inline static int32_t get_offset_of_m_InternalProfile_12() { return static_cast<int32_t>(offsetof(PostProcessVolume_t2983056917, ___m_InternalProfile_12)); }
	inline PostProcessProfile_t3936051061 * get_m_InternalProfile_12() const { return ___m_InternalProfile_12; }
	inline PostProcessProfile_t3936051061 ** get_address_of_m_InternalProfile_12() { return &___m_InternalProfile_12; }
	inline void set_m_InternalProfile_12(PostProcessProfile_t3936051061 * value)
	{
		___m_InternalProfile_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalProfile_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSVOLUME_T2983056917_H
#ifndef DEBUGUIHANDLERBUTTON_T2657197137_H
#define DEBUGUIHANDLERBUTTON_T2657197137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.UI.DebugUIHandlerButton
struct  DebugUIHandlerButton_t2657197137  : public DebugUIHandlerWidget_t1600606064
{
public:
	// UnityEngine.UI.Text UnityEngine.Experimental.Rendering.UI.DebugUIHandlerButton::nameLabel
	Text_t1901882714 * ___nameLabel_10;
	// UnityEngine.Experimental.Rendering.DebugUI/Button UnityEngine.Experimental.Rendering.UI.DebugUIHandlerButton::m_Field
	Button_t565603744 * ___m_Field_11;

public:
	inline static int32_t get_offset_of_nameLabel_10() { return static_cast<int32_t>(offsetof(DebugUIHandlerButton_t2657197137, ___nameLabel_10)); }
	inline Text_t1901882714 * get_nameLabel_10() const { return ___nameLabel_10; }
	inline Text_t1901882714 ** get_address_of_nameLabel_10() { return &___nameLabel_10; }
	inline void set_nameLabel_10(Text_t1901882714 * value)
	{
		___nameLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___nameLabel_10), value);
	}

	inline static int32_t get_offset_of_m_Field_11() { return static_cast<int32_t>(offsetof(DebugUIHandlerButton_t2657197137, ___m_Field_11)); }
	inline Button_t565603744 * get_m_Field_11() const { return ___m_Field_11; }
	inline Button_t565603744 ** get_address_of_m_Field_11() { return &___m_Field_11; }
	inline void set_m_Field_11(Button_t565603744 * value)
	{
		___m_Field_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Field_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGUIHANDLERBUTTON_T2657197137_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (IntParameter_t773781776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (BoolParameter_t2299103272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (ColorParameter_t2998827320), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (Vector2Parameter_t1794608574), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (Vector4Parameter_t1505856958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (SplineParameter_t905443520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (TextureParameterDefault_t2577489536)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4206[6] = 
{
	TextureParameterDefault_t2577489536::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (TextureParameter_t4267400415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4207[1] = 
{
	TextureParameter_t4267400415::get_offset_of_defaultState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (PostProcessBundle_t3465003670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4208[3] = 
{
	PostProcessBundle_t3465003670::get_offset_of_U3CattributeU3Ek__BackingField_0(),
	PostProcessBundle_t3465003670::get_offset_of_U3CsettingsU3Ek__BackingField_1(),
	PostProcessBundle_t3465003670::get_offset_of_m_Renderer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (PostProcessDebug_t1827270515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4209[9] = 
{
	PostProcessDebug_t1827270515::get_offset_of_postProcessLayer_4(),
	PostProcessDebug_t1827270515::get_offset_of_m_PreviousPostProcessLayer_5(),
	PostProcessDebug_t1827270515::get_offset_of_lightMeter_6(),
	PostProcessDebug_t1827270515::get_offset_of_histogram_7(),
	PostProcessDebug_t1827270515::get_offset_of_waveform_8(),
	PostProcessDebug_t1827270515::get_offset_of_vectorscope_9(),
	PostProcessDebug_t1827270515::get_offset_of_debugOverlay_10(),
	PostProcessDebug_t1827270515::get_offset_of_m_CurrentCamera_11(),
	PostProcessDebug_t1827270515::get_offset_of_m_CmdAfterEverything_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (DebugOverlay_t1040601139)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4210[12] = 
{
	DebugOverlay_t1040601139::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (ColorBlindnessType_t2816108808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4211[4] = 
{
	ColorBlindnessType_t2816108808::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (PostProcessDebugLayer_t3290441360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4212[11] = 
{
	PostProcessDebugLayer_t3290441360::get_offset_of_lightMeter_0(),
	PostProcessDebugLayer_t3290441360::get_offset_of_histogram_1(),
	PostProcessDebugLayer_t3290441360::get_offset_of_waveform_2(),
	PostProcessDebugLayer_t3290441360::get_offset_of_vectorscope_3(),
	PostProcessDebugLayer_t3290441360::get_offset_of_m_Monitors_4(),
	PostProcessDebugLayer_t3290441360::get_offset_of_frameWidth_5(),
	PostProcessDebugLayer_t3290441360::get_offset_of_frameHeight_6(),
	PostProcessDebugLayer_t3290441360::get_offset_of_U3CdebugOverlayTargetU3Ek__BackingField_7(),
	PostProcessDebugLayer_t3290441360::get_offset_of_U3CdebugOverlayActiveU3Ek__BackingField_8(),
	PostProcessDebugLayer_t3290441360::get_offset_of_U3CdebugOverlayU3Ek__BackingField_9(),
	PostProcessDebugLayer_t3290441360::get_offset_of_overlaySettings_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (OverlaySettings_t1356898765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4213[5] = 
{
	OverlaySettings_t1356898765::get_offset_of_linearDepth_0(),
	OverlaySettings_t1356898765::get_offset_of_motionColorIntensity_1(),
	OverlaySettings_t1356898765::get_offset_of_motionGridSize_2(),
	OverlaySettings_t1356898765::get_offset_of_colorBlindnessType_3(),
	OverlaySettings_t1356898765::get_offset_of_colorBlindnessStrength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (PostProcessEffectRenderer_t1060237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4214[1] = 
{
	PostProcessEffectRenderer_t1060237::get_offset_of_m_ResetHistory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4215[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (PostProcessEffectSettings_t1672565614), -1, sizeof(PostProcessEffectSettings_t1672565614_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4216[5] = 
{
	PostProcessEffectSettings_t1672565614::get_offset_of_active_4(),
	PostProcessEffectSettings_t1672565614::get_offset_of_enabled_5(),
	PostProcessEffectSettings_t1672565614::get_offset_of_parameters_6(),
	PostProcessEffectSettings_t1672565614_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	PostProcessEffectSettings_t1672565614_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (PostProcessEvent_t3532433552)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4217[4] = 
{
	PostProcessEvent_t3532433552::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (PostProcessEventComparer_t1947197927)+ sizeof (RuntimeObject), sizeof(PostProcessEventComparer_t1947197927 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (PostProcessLayer_t4264744195), -1, sizeof(PostProcessLayer_t4264744195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4219[35] = 
{
	PostProcessLayer_t4264744195::get_offset_of_volumeTrigger_4(),
	PostProcessLayer_t4264744195::get_offset_of_volumeLayer_5(),
	PostProcessLayer_t4264744195::get_offset_of_stopNaNPropagation_6(),
	PostProcessLayer_t4264744195::get_offset_of_antialiasingMode_7(),
	PostProcessLayer_t4264744195::get_offset_of_temporalAntialiasing_8(),
	PostProcessLayer_t4264744195::get_offset_of_subpixelMorphologicalAntialiasing_9(),
	PostProcessLayer_t4264744195::get_offset_of_fastApproximateAntialiasing_10(),
	PostProcessLayer_t4264744195::get_offset_of_fog_11(),
	PostProcessLayer_t4264744195::get_offset_of_dithering_12(),
	PostProcessLayer_t4264744195::get_offset_of_debugLayer_13(),
	PostProcessLayer_t4264744195::get_offset_of_m_Resources_14(),
	PostProcessLayer_t4264744195::get_offset_of_m_ShowToolkit_15(),
	PostProcessLayer_t4264744195::get_offset_of_m_ShowCustomSorter_16(),
	PostProcessLayer_t4264744195::get_offset_of_breakBeforeColorGrading_17(),
	PostProcessLayer_t4264744195::get_offset_of_m_BeforeTransparentBundles_18(),
	PostProcessLayer_t4264744195::get_offset_of_m_BeforeStackBundles_19(),
	PostProcessLayer_t4264744195::get_offset_of_m_AfterStackBundles_20(),
	PostProcessLayer_t4264744195::get_offset_of_U3CsortedBundlesU3Ek__BackingField_21(),
	PostProcessLayer_t4264744195::get_offset_of_U3ChaveBundlesBeenInitedU3Ek__BackingField_22(),
	PostProcessLayer_t4264744195::get_offset_of_m_Bundles_23(),
	PostProcessLayer_t4264744195::get_offset_of_m_PropertySheetFactory_24(),
	PostProcessLayer_t4264744195::get_offset_of_m_LegacyCmdBufferBeforeReflections_25(),
	PostProcessLayer_t4264744195::get_offset_of_m_LegacyCmdBufferBeforeLighting_26(),
	PostProcessLayer_t4264744195::get_offset_of_m_LegacyCmdBufferOpaque_27(),
	PostProcessLayer_t4264744195::get_offset_of_m_LegacyCmdBuffer_28(),
	PostProcessLayer_t4264744195::get_offset_of_m_Camera_29(),
	PostProcessLayer_t4264744195::get_offset_of_m_CurrentContext_30(),
	PostProcessLayer_t4264744195::get_offset_of_m_LogHistogram_31(),
	PostProcessLayer_t4264744195::get_offset_of_m_SettingsUpdateNeeded_32(),
	PostProcessLayer_t4264744195::get_offset_of_m_IsRenderingInSceneView_33(),
	PostProcessLayer_t4264744195::get_offset_of_m_TargetPool_34(),
	PostProcessLayer_t4264744195::get_offset_of_m_NaNKilled_35(),
	PostProcessLayer_t4264744195::get_offset_of_m_ActiveEffects_36(),
	PostProcessLayer_t4264744195::get_offset_of_m_Targets_37(),
	PostProcessLayer_t4264744195_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (Antialiasing_t455662751)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4220[5] = 
{
	Antialiasing_t455662751::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (SerializedBundleRef_t2850027960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4221[2] = 
{
	SerializedBundleRef_t2850027960::get_offset_of_assemblyQualifiedName_0(),
	SerializedBundleRef_t2850027960::get_offset_of_bundle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4222[2] = 
{
	U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214::get_offset_of_evt_0(),
	U3CUpdateBundleSortListU3Ec__AnonStorey0_t38843214::get_offset_of_effects_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (U3CUpdateBundleSortListU3Ec__AnonStorey1_t2108533815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4223[2] = 
{
	U3CUpdateBundleSortListU3Ec__AnonStorey1_t2108533815::get_offset_of_searchStr_0(),
	U3CUpdateBundleSortListU3Ec__AnonStorey1_t2108533815::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (U3CUpdateBundleSortListU3Ec__AnonStorey2_t38712142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[1] = 
{
	U3CUpdateBundleSortListU3Ec__AnonStorey2_t38712142::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (U3CUpdateBundleSortListU3Ec__AnonStorey3_t38646606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4225[1] = 
{
	U3CUpdateBundleSortListU3Ec__AnonStorey3_t38646606::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (PostProcessManager_t1036946270), -1, sizeof(PostProcessManager_t1036946270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4226[8] = 
{
	PostProcessManager_t1036946270_StaticFields::get_offset_of_s_Instance_0(),
	PostProcessManager_t1036946270::get_offset_of_m_SortedVolumes_1(),
	PostProcessManager_t1036946270::get_offset_of_m_Volumes_2(),
	PostProcessManager_t1036946270::get_offset_of_m_SortNeeded_3(),
	PostProcessManager_t1036946270::get_offset_of_m_BaseSettings_4(),
	PostProcessManager_t1036946270::get_offset_of_m_TempColliders_5(),
	PostProcessManager_t1036946270::get_offset_of_settingsTypes_6(),
	PostProcessManager_t1036946270_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (PostProcessProfile_t3936051061), -1, sizeof(PostProcessProfile_t3936051061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4227[3] = 
{
	PostProcessProfile_t3936051061::get_offset_of_settings_4(),
	PostProcessProfile_t3936051061::get_offset_of_isDirty_5(),
	PostProcessProfile_t3936051061_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (PostProcessRenderContext_t597611190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4228[26] = 
{
	PostProcessRenderContext_t597611190::get_offset_of_m_Camera_0(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CcommandU3Ek__BackingField_1(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CsourceU3Ek__BackingField_2(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CdestinationU3Ek__BackingField_3(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CsourceFormatU3Ek__BackingField_4(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CflipU3Ek__BackingField_5(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CresourcesU3Ek__BackingField_6(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CpropertySheetsU3Ek__BackingField_7(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CuserDataU3Ek__BackingField_8(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CdebugLayerU3Ek__BackingField_9(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CwidthU3Ek__BackingField_10(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CheightU3Ek__BackingField_11(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CstereoActiveU3Ek__BackingField_12(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CxrActiveEyeU3Ek__BackingField_13(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CscreenWidthU3Ek__BackingField_14(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CscreenHeightU3Ek__BackingField_15(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CisSceneViewU3Ek__BackingField_16(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CantialiasingU3Ek__BackingField_17(),
	PostProcessRenderContext_t597611190::get_offset_of_U3CtemporalAntialiasingU3Ek__BackingField_18(),
	PostProcessRenderContext_t597611190::get_offset_of_uberSheet_19(),
	PostProcessRenderContext_t597611190::get_offset_of_autoExposureTexture_20(),
	PostProcessRenderContext_t597611190::get_offset_of_logHistogram_21(),
	PostProcessRenderContext_t597611190::get_offset_of_logLut_22(),
	PostProcessRenderContext_t597611190::get_offset_of_autoExposure_23(),
	PostProcessRenderContext_t597611190::get_offset_of_bloomBufferNameID_24(),
	PostProcessRenderContext_t597611190::get_offset_of_m_sourceDescriptor_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (PostProcessResources_t1163236733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4229[5] = 
{
	PostProcessResources_t1163236733::get_offset_of_blueNoise64_4(),
	PostProcessResources_t1163236733::get_offset_of_blueNoise256_5(),
	PostProcessResources_t1163236733::get_offset_of_smaaLuts_6(),
	PostProcessResources_t1163236733::get_offset_of_shaders_7(),
	PostProcessResources_t1163236733::get_offset_of_computeShaders_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (Shaders_t2807171077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4230[22] = 
{
	Shaders_t2807171077::get_offset_of_bloom_0(),
	Shaders_t2807171077::get_offset_of_copy_1(),
	Shaders_t2807171077::get_offset_of_copyStd_2(),
	Shaders_t2807171077::get_offset_of_discardAlpha_3(),
	Shaders_t2807171077::get_offset_of_depthOfField_4(),
	Shaders_t2807171077::get_offset_of_finalPass_5(),
	Shaders_t2807171077::get_offset_of_grainBaker_6(),
	Shaders_t2807171077::get_offset_of_motionBlur_7(),
	Shaders_t2807171077::get_offset_of_temporalAntialiasing_8(),
	Shaders_t2807171077::get_offset_of_subpixelMorphologicalAntialiasing_9(),
	Shaders_t2807171077::get_offset_of_texture2dLerp_10(),
	Shaders_t2807171077::get_offset_of_uber_11(),
	Shaders_t2807171077::get_offset_of_lut2DBaker_12(),
	Shaders_t2807171077::get_offset_of_lightMeter_13(),
	Shaders_t2807171077::get_offset_of_gammaHistogram_14(),
	Shaders_t2807171077::get_offset_of_waveform_15(),
	Shaders_t2807171077::get_offset_of_vectorscope_16(),
	Shaders_t2807171077::get_offset_of_debugOverlays_17(),
	Shaders_t2807171077::get_offset_of_deferredFog_18(),
	Shaders_t2807171077::get_offset_of_scalableAO_19(),
	Shaders_t2807171077::get_offset_of_multiScaleAO_20(),
	Shaders_t2807171077::get_offset_of_screenSpaceReflections_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (ComputeShaders_t4172110136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4231[12] = 
{
	ComputeShaders_t4172110136::get_offset_of_autoExposure_0(),
	ComputeShaders_t4172110136::get_offset_of_exposureHistogram_1(),
	ComputeShaders_t4172110136::get_offset_of_lut3DBaker_2(),
	ComputeShaders_t4172110136::get_offset_of_texture3dLerp_3(),
	ComputeShaders_t4172110136::get_offset_of_gammaHistogram_4(),
	ComputeShaders_t4172110136::get_offset_of_waveform_5(),
	ComputeShaders_t4172110136::get_offset_of_vectorscope_6(),
	ComputeShaders_t4172110136::get_offset_of_multiScaleAODownsample1_7(),
	ComputeShaders_t4172110136::get_offset_of_multiScaleAODownsample2_8(),
	ComputeShaders_t4172110136::get_offset_of_multiScaleAORender_9(),
	ComputeShaders_t4172110136::get_offset_of_multiScaleAOUpsample_10(),
	ComputeShaders_t4172110136::get_offset_of_gaussianDownsample_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (SMAALuts_t184516107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4232[2] = 
{
	SMAALuts_t184516107::get_offset_of_area_0(),
	SMAALuts_t184516107::get_offset_of_search_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (PostProcessVolume_t2983056917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4233[9] = 
{
	PostProcessVolume_t2983056917::get_offset_of_sharedProfile_4(),
	PostProcessVolume_t2983056917::get_offset_of_isGlobal_5(),
	PostProcessVolume_t2983056917::get_offset_of_blendDistance_6(),
	PostProcessVolume_t2983056917::get_offset_of_weight_7(),
	PostProcessVolume_t2983056917::get_offset_of_priority_8(),
	PostProcessVolume_t2983056917::get_offset_of_m_PreviousLayer_9(),
	PostProcessVolume_t2983056917::get_offset_of_m_PreviousPriority_10(),
	PostProcessVolume_t2983056917::get_offset_of_m_TempColliders_11(),
	PostProcessVolume_t2983056917::get_offset_of_m_InternalProfile_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (ColorUtilities_t3120880352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (HableCurve_t1612137718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4235[6] = 
{
	HableCurve_t1612137718::get_offset_of_U3CwhitePointU3Ek__BackingField_0(),
	HableCurve_t1612137718::get_offset_of_U3CinverseWhitePointU3Ek__BackingField_1(),
	HableCurve_t1612137718::get_offset_of_U3Cx0U3Ek__BackingField_2(),
	HableCurve_t1612137718::get_offset_of_U3Cx1U3Ek__BackingField_3(),
	HableCurve_t1612137718::get_offset_of_segments_4(),
	HableCurve_t1612137718::get_offset_of_uniforms_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (Segment_t754368499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4236[6] = 
{
	Segment_t754368499::get_offset_of_offsetX_0(),
	Segment_t754368499::get_offset_of_offsetY_1(),
	Segment_t754368499::get_offset_of_scaleX_2(),
	Segment_t754368499::get_offset_of_scaleY_3(),
	Segment_t754368499::get_offset_of_lnA_4(),
	Segment_t754368499::get_offset_of_B_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (DirectParams_t291775049)+ sizeof (RuntimeObject), sizeof(DirectParams_t291775049 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4237[8] = 
{
	DirectParams_t291775049::get_offset_of_x0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_y0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_x1_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_y1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_W_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_overshootX_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_overshootY_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_t291775049::get_offset_of_gamma_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (Uniforms_t165416594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4238[1] = 
{
	Uniforms_t165416594::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (HaltonSeq_t947190270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (LogHistogram_t1187052756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4240[6] = 
{
	0,
	0,
	0,
	LogHistogram_t1187052756::get_offset_of_m_ThreadX_3(),
	LogHistogram_t1187052756::get_offset_of_m_ThreadY_4(),
	LogHistogram_t1187052756::get_offset_of_U3CdataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (PropertySheet_t3821403501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4241[2] = 
{
	PropertySheet_t3821403501::get_offset_of_U3CpropertiesU3Ek__BackingField_0(),
	PropertySheet_t3821403501::get_offset_of_U3CmaterialU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (PropertySheetFactory_t1490101248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4242[1] = 
{
	PropertySheetFactory_t1490101248::get_offset_of_m_Sheets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (RuntimeUtilities_t4060785994), -1, sizeof(RuntimeUtilities_t4060785994_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4243[9] = 
{
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_m_WhiteTexture_0(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_m_BlackTexture_1(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_m_LutStrips_2(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_s_FullscreenTriangle_3(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_s_CopyStdMaterial_4(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_s_CopyMaterial_5(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_s_CopySheet_6(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_m_AssemblyTypes_7(),
	RuntimeUtilities_t4060785994_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (ShaderIDs_t2844105293), -1, sizeof(ShaderIDs_t2844105293_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4244[126] = 
{
	ShaderIDs_t2844105293_StaticFields::get_offset_of_MainTex_0(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Jitter_1(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Sharpness_2(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_FinalBlendParameters_3(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_HistoryTex_4(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_SMAA_Flip_5(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_SMAA_Flop_6(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_AOParams_7(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_AOColor_8(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_OcclusionTexture1_9(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_OcclusionTexture2_10(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_SAOcclusionTexture_11(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_MSVOcclusionTexture_12(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_DepthCopy_13(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LinearDepth_14(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LowDepth1_15(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LowDepth2_16(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LowDepth3_17(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LowDepth4_18(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TiledDepth1_19(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TiledDepth2_20(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TiledDepth3_21(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TiledDepth4_22(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Occlusion1_23(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Occlusion2_24(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Occlusion3_25(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Occlusion4_26(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Combined1_27(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Combined2_28(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Combined3_29(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_SSRResolveTemp_30(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Noise_31(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Test_32(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Resolve_33(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_History_34(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ViewMatrix_35(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_InverseViewMatrix_36(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_InverseProjectionMatrix_37(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ScreenSpaceProjectionMatrix_38(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Params2_39(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_FogColor_40(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_FogParams_41(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_VelocityScale_42(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_MaxBlurRadius_43(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_RcpMaxBlurRadius_44(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_VelocityTex_45(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Tile2RT_46(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Tile4RT_47(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Tile8RT_48(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TileMaxOffs_49(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TileMaxLoop_50(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TileVRT_51(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_NeighborMaxTex_52(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LoopCount_53(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_DepthOfFieldTemp_54(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_DepthOfFieldTex_55(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Distance_56(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LensCoeff_57(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_MaxCoC_58(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_RcpMaxCoC_59(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_RcpAspect_60(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_CoCTex_61(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TaaParams_62(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_AutoExposureTex_63(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_HistogramBuffer_64(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Params_65(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ScaleOffsetRes_66(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_BloomTex_67(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_SampleScale_68(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Threshold_69(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ColorIntensity_70(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Bloom_DirtTex_71(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Bloom_Settings_72(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Bloom_Color_73(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Bloom_DirtTileOffset_74(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ChromaticAberration_Amount_75(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ChromaticAberration_SpectralLut_76(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Distortion_CenterScale_77(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Distortion_Amount_78(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Lut2D_79(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Lut3D_80(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Lut3D_Params_81(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Lut2D_Params_82(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_UserLut2D_Params_83(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_PostExposure_84(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ColorBalance_85(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ColorFilter_86(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_HueSatCon_87(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Brightness_88(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ChannelMixerRed_89(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ChannelMixerGreen_90(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ChannelMixerBlue_91(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Lift_92(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_InvGamma_93(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Gain_94(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Curves_95(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_CustomToneCurve_96(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ToeSegmentA_97(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ToeSegmentB_98(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_MidSegmentA_99(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_MidSegmentB_100(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ShoSegmentA_101(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_ShoSegmentB_102(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Vignette_Color_103(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Vignette_Center_104(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Vignette_Settings_105(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Vignette_Mask_106(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Vignette_Opacity_107(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Vignette_Mode_108(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Grain_Params1_109(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Grain_Params2_110(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_GrainTex_111(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Phase_112(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_LumaInAlpha_113(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_DitheringTex_114(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Dithering_Coords_115(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_From_116(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_To_117(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_Interp_118(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_TargetColor_119(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_HalfResFinalCopy_120(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_WaveformSource_121(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_WaveformBuffer_122(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_VectorscopeBuffer_123(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_RenderViewportScaleFactor_124(),
	ShaderIDs_t2844105293_StaticFields::get_offset_of_UVTransform_125(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (Spline_t3835237600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4245[7] = 
{
	Spline_t3835237600::get_offset_of_curve_0(),
	Spline_t3835237600::get_offset_of_m_Loop_1(),
	Spline_t3835237600::get_offset_of_m_ZeroValue_2(),
	Spline_t3835237600::get_offset_of_m_Range_3(),
	Spline_t3835237600::get_offset_of_m_InternalLoopingCurve_4(),
	Spline_t3835237600::get_offset_of_frameCount_5(),
	Spline_t3835237600::get_offset_of_cachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { sizeof (TargetPool_t1535233241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4246[2] = 
{
	TargetPool_t1535233241::get_offset_of_m_Pool_0(),
	TargetPool_t1535233241::get_offset_of_m_Current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (TextureFormatUtilities_t2217912845), -1, sizeof(TextureFormatUtilities_t2217912845_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4247[3] = 
{
	TextureFormatUtilities_t2217912845_StaticFields::get_offset_of_s_FormatAliasMap_0(),
	TextureFormatUtilities_t2217912845_StaticFields::get_offset_of_s_SupportedRenderTextureFormats_1(),
	TextureFormatUtilities_t2217912845_StaticFields::get_offset_of_s_SupportedTextureFormats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (TextureLerper_t1948079985), -1, sizeof(TextureLerper_t1948079985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4248[6] = 
{
	TextureLerper_t1948079985_StaticFields::get_offset_of_m_Instance_0(),
	TextureLerper_t1948079985::get_offset_of_m_Command_1(),
	TextureLerper_t1948079985::get_offset_of_m_PropertySheets_2(),
	TextureLerper_t1948079985::get_offset_of_m_Resources_3(),
	TextureLerper_t1948079985::get_offset_of_m_Recycled_4(),
	TextureLerper_t1948079985::get_offset_of_m_Actives_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255372), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4249[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24fieldU2D0ED907628EE272F93737B500A23D77C9B1C88368_0(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U24fieldU2D727432515AE33ED62A216F2EBFF476490B631B0F_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (U24ArrayTypeU3D20_t1702832645)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t1702832645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (U3CModuleU3E_t692745577), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (CameraSwitcher_t1205312903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4253[9] = 
{
	CameraSwitcher_t1205312903::get_offset_of_m_Cameras_4(),
	CameraSwitcher_t1205312903::get_offset_of_m_CurrentCameraIndex_5(),
	CameraSwitcher_t1205312903::get_offset_of_m_OriginalCamera_6(),
	CameraSwitcher_t1205312903::get_offset_of_m_OriginalCameraPosition_7(),
	CameraSwitcher_t1205312903::get_offset_of_m_OriginalCameraRotation_8(),
	CameraSwitcher_t1205312903::get_offset_of_m_CurrentCamera_9(),
	CameraSwitcher_t1205312903::get_offset_of_m_CameraNames_10(),
	CameraSwitcher_t1205312903::get_offset_of_m_CameraIndices_11(),
	CameraSwitcher_t1205312903::get_offset_of_m_DebugEntry_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (FreeCamera_t2435895153), -1, sizeof(FreeCamera_t2435895153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4254[13] = 
{
	FreeCamera_t2435895153::get_offset_of_m_LookSpeedController_4(),
	FreeCamera_t2435895153::get_offset_of_m_LookSpeedMouse_5(),
	FreeCamera_t2435895153::get_offset_of_m_MoveSpeed_6(),
	FreeCamera_t2435895153::get_offset_of_m_MoveSpeedIncrement_7(),
	FreeCamera_t2435895153::get_offset_of_m_Turbo_8(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kMouseX_9(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kMouseY_10(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kRightStickX_11(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kRightStickY_12(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kVertical_13(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kHorizontal_14(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kYAxis_15(),
	FreeCamera_t2435895153_StaticFields::get_offset_of_kSpeedAxis_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4255[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (CommandBufferPool_t3037895833), -1, sizeof(CommandBufferPool_t3037895833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4256[1] = 
{
	CommandBufferPool_t3037895833_StaticFields::get_offset_of_s_BufferPool_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4258[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4260[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (RectInt_t1577945902)+ sizeof (RuntimeObject), sizeof(RectInt_t1577945902 ), sizeof(RectInt_t1577945902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4261[5] = 
{
	RectInt_t1577945902_StaticFields::get_offset_of_zero_0(),
	RectInt_t1577945902::get_offset_of_x_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectInt_t1577945902::get_offset_of_y_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectInt_t1577945902::get_offset_of_width_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectInt_t1577945902::get_offset_of_height_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4262[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4263[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (XRGraphicsConfig_t170342676), -1, sizeof(XRGraphicsConfig_t170342676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4264[7] = 
{
	XRGraphicsConfig_t170342676::get_offset_of_renderScale_0(),
	XRGraphicsConfig_t170342676::get_offset_of_viewportScale_1(),
	XRGraphicsConfig_t170342676::get_offset_of_useOcclusionMesh_2(),
	XRGraphicsConfig_t170342676::get_offset_of_occlusionMaskScale_3(),
	XRGraphicsConfig_t170342676::get_offset_of_showDeviceView_4(),
	XRGraphicsConfig_t170342676::get_offset_of_gameViewRenderMode_5(),
	XRGraphicsConfig_t170342676_StaticFields::get_offset_of_s_DefaultXRConfig_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (GPUCopy_t16803174), -1, sizeof(GPUCopy_t16803174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4265[6] = 
{
	GPUCopy_t16803174::get_offset_of_m_Shader_0(),
	GPUCopy_t16803174::get_offset_of_k_SampleKernel_xyzw2x_8_1(),
	GPUCopy_t16803174::get_offset_of_k_SampleKernel_xyzw2x_1_2(),
	GPUCopy_t16803174_StaticFields::get_offset_of__RectOffset_3(),
	GPUCopy_t16803174_StaticFields::get_offset_of__Result1_4(),
	GPUCopy_t16803174_StaticFields::get_offset_of__Source4_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (GPUCopyAsset_t4186295563), -1, sizeof(GPUCopyAsset_t4186295563_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4266[3] = 
{
	GPUCopyAsset_t4186295563_StaticFields::get_offset_of_k_ChannelIDS_4(),
	0,
	GPUCopyAsset_t4186295563::get_offset_of_m_CopyOperation_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (CopyOperation_t3389193622)+ sizeof (RuntimeObject), sizeof(CopyOperation_t3389193622_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4267[3] = 
{
	CopyOperation_t3389193622::get_offset_of_subscript_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CopyOperation_t3389193622::get_offset_of_sourceChannel_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CopyOperation_t3389193622::get_offset_of_targetChannel_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (TexturePadding_t452921104), -1, sizeof(TexturePadding_t452921104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4268[6] = 
{
	TexturePadding_t452921104_StaticFields::get_offset_of__RectOffset_0(),
	TexturePadding_t452921104_StaticFields::get_offset_of__InOutTexture_1(),
	TexturePadding_t452921104::get_offset_of_m_CS_2(),
	TexturePadding_t452921104::get_offset_of_m_KMainTopRight_3(),
	TexturePadding_t452921104::get_offset_of_m_KMainTop_4(),
	TexturePadding_t452921104::get_offset_of_m_KMainRight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (DebugAction_t229911009)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4269[10] = 
{
	DebugAction_t229911009::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (DebugActionRepeatMode_t2259669336)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4270[3] = 
{
	DebugActionRepeatMode_t2259669336::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (DebugManager_t4149441816), -1, sizeof(DebugManager_t4149441816_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4271[23] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DebugManager_t4149441816::get_offset_of_m_DebugActions_9(),
	DebugManager_t4149441816::get_offset_of_m_DebugActionStates_10(),
	DebugManager_t4149441816_StaticFields::get_offset_of_s_Instance_11(),
	DebugManager_t4149441816::get_offset_of_m_ReadOnlyPanels_12(),
	DebugManager_t4149441816::get_offset_of_m_Panels_13(),
	DebugManager_t4149441816::get_offset_of_onDisplayRuntimeUIChanged_14(),
	DebugManager_t4149441816::get_offset_of_onSetDirty_15(),
	DebugManager_t4149441816::get_offset_of_refreshEditorRequested_16(),
	DebugManager_t4149441816::get_offset_of_m_Root_17(),
	DebugManager_t4149441816::get_offset_of_m_RootUICanvas_18(),
	DebugManager_t4149441816::get_offset_of_m_PersistentRoot_19(),
	DebugManager_t4149441816::get_offset_of_m_RootUIPersistentCanvas_20(),
	DebugManager_t4149441816_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_21(),
	DebugManager_t4149441816_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (DebugActionDesc_t1210212492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4272[5] = 
{
	DebugActionDesc_t1210212492::get_offset_of_buttonTriggerList_0(),
	DebugActionDesc_t1210212492::get_offset_of_axisTrigger_1(),
	DebugActionDesc_t1210212492::get_offset_of_keyTriggerList_2(),
	DebugActionDesc_t1210212492::get_offset_of_repeatMode_3(),
	DebugActionDesc_t1210212492::get_offset_of_repeatDelay_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (DebugActionState_t1357925847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4273[8] = 
{
	DebugActionState_t1357925847::get_offset_of_m_Type_0(),
	DebugActionState_t1357925847::get_offset_of_m_PressedButtons_1(),
	DebugActionState_t1357925847::get_offset_of_m_PressedAxis_2(),
	DebugActionState_t1357925847::get_offset_of_m_PressedKeys_3(),
	DebugActionState_t1357925847::get_offset_of_m_TriggerPressedUp_4(),
	DebugActionState_t1357925847::get_offset_of_m_Timer_5(),
	DebugActionState_t1357925847::get_offset_of_U3CrunningActionU3Ek__BackingField_6(),
	DebugActionState_t1357925847::get_offset_of_U3CactionStateU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (DebugActionKeyType_t1119546432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4274[4] = 
{
	DebugActionKeyType_t1119546432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (DebugUI_t4282187678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (Container_t3218394953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4276[1] = 
{
	Container_t3218394953::get_offset_of_U3CchildrenU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (Foldout_t3285563867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4277[1] = 
{
	Foldout_t3285563867::get_offset_of_opened_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (HBox_t382921510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (VBox_t115141414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (Flags_t3313364473)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4280[5] = 
{
	Flags_t3313364473::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (Widget_t1336755483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4281[5] = 
{
	Widget_t1336755483::get_offset_of_m_Panel_0(),
	Widget_t1336755483::get_offset_of_m_Parent_1(),
	Widget_t1336755483::get_offset_of_U3CflagsU3Ek__BackingField_2(),
	Widget_t1336755483::get_offset_of_U3CdisplayNameU3Ek__BackingField_3(),
	Widget_t1336755483::get_offset_of_U3CqueryPathU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (Button_t565603744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[1] = 
{
	Button_t565603744::get_offset_of_U3CactionU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (Value_t4182543730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4285[2] = 
{
	Value_t4182543730::get_offset_of_U3CgetterU3Ek__BackingField_5(),
	Value_t4182543730::get_offset_of_refreshRate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4286[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (BoolField_t1734172308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (IntField_t940763525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4288[4] = 
{
	IntField_t940763525::get_offset_of_min_8(),
	IntField_t940763525::get_offset_of_max_9(),
	IntField_t940763525::get_offset_of_incStep_10(),
	IntField_t940763525::get_offset_of_intStepMult_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (UIntField_t197911686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4289[4] = 
{
	UIntField_t197911686::get_offset_of_min_8(),
	UIntField_t197911686::get_offset_of_max_9(),
	UIntField_t197911686::get_offset_of_incStep_10(),
	UIntField_t197911686::get_offset_of_intStepMult_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (FloatField_t2428081994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4290[5] = 
{
	FloatField_t2428081994::get_offset_of_min_8(),
	FloatField_t2428081994::get_offset_of_max_9(),
	FloatField_t2428081994::get_offset_of_incStep_10(),
	FloatField_t2428081994::get_offset_of_incStepMult_11(),
	FloatField_t2428081994::get_offset_of_decimals_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (EnumField_t3058254002), -1, sizeof(EnumField_t3058254002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4291[3] = 
{
	EnumField_t3058254002::get_offset_of_enumNames_8(),
	EnumField_t3058254002::get_offset_of_enumValues_9(),
	EnumField_t3058254002_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (ColorField_t1371659825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4292[6] = 
{
	ColorField_t1371659825::get_offset_of_hdr_8(),
	ColorField_t1371659825::get_offset_of_showAlpha_9(),
	ColorField_t1371659825::get_offset_of_showPicker_10(),
	ColorField_t1371659825::get_offset_of_incStep_11(),
	ColorField_t1371659825::get_offset_of_incStepMult_12(),
	ColorField_t1371659825::get_offset_of_decimals_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (Vector2Field_t3950563844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4293[3] = 
{
	Vector2Field_t3950563844::get_offset_of_incStep_8(),
	Vector2Field_t3950563844::get_offset_of_incStepMult_9(),
	Vector2Field_t3950563844::get_offset_of_decimals_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (Vector3Field_t3952595460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4294[3] = 
{
	Vector3Field_t3952595460::get_offset_of_incStep_8(),
	Vector3Field_t3952595460::get_offset_of_incStepMult_9(),
	Vector3Field_t3952595460::get_offset_of_decimals_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { sizeof (Vector4Field_t3946500612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4295[3] = 
{
	Vector4Field_t3946500612::get_offset_of_incStep_8(),
	Vector4Field_t3946500612::get_offset_of_incStepMult_9(),
	Vector4Field_t3946500612::get_offset_of_decimals_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { sizeof (Panel_t835245858), -1, sizeof(Panel_t835245858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4296[5] = 
{
	Panel_t835245858::get_offset_of_U3CflagsU3Ek__BackingField_0(),
	Panel_t835245858::get_offset_of_U3CdisplayNameU3Ek__BackingField_1(),
	Panel_t835245858::get_offset_of_U3CchildrenU3Ek__BackingField_2(),
	Panel_t835245858::get_offset_of_onSetDirty_3(),
	Panel_t835245858_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (DebugUpdater_t3339232225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (MousePositionDebug_t947361257), -1, sizeof(MousePositionDebug_t947361257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4298[1] = 
{
	MousePositionDebug_t947361257_StaticFields::get_offset_of_s_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (DebugUIHandlerButton_t2657197137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4299[2] = 
{
	DebugUIHandlerButton_t2657197137::get_offset_of_nameLabel_10(),
	DebugUIHandlerButton_t2657197137::get_offset_of_m_Field_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
