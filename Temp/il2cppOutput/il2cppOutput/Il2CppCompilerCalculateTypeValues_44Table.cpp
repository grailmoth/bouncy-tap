﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t3243021218;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t3280968592;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.Volume>>
struct Dictionary_2_t4117966577;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Experimental.Rendering.VolumeComponent>
struct Dictionary_2_t3270125238;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>
struct Dictionary_2_t4225213415;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1329213854;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>
struct IDictionary_2_t624724933;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct IDictionary_2_t3096785446;
// System.Collections.Generic.IDictionary`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t4035537244;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1463797649;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t3245421752;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.Volume>
struct List_1_t934285950;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VolumeComponent>
struct List_1_t2297852916;
// System.Collections.Generic.Queue`1<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>
struct Queue_1_t382223268;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Experimental.Rendering.VolumeParameter>
struct ReadOnlyCollection_1_t3079943022;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>
struct EventHandler_1_t177306446;
// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>
struct EventHandler_1_t908338235;
// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>
struct EventHandler_1_t2768214265;
// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>
struct EventHandler_1_t2215985868;
// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>>
struct Func_2_t779105388;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t1761491126;
// System.Func`2<System.Reflection.FieldInfo,System.Int32>
struct Func_2_t320181618;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t561252955;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Net.WebRequest
struct WebRequest_t1939381076;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<UnityEngine.Experimental.Rendering.VolumeComponent>
struct Predicate_1_t1651072298;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0
struct U3CShowU3Ec__AnonStorey0_t3170924441;
// UnityEngine.Advertisements.Android.Platform
struct Platform_t1698302846;
// UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey4
struct U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180;
// UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey2
struct U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596;
// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_t363496179;
// UnityEngine.Advertisements.Editor.Configuration
struct Configuration_t1722493896;
// UnityEngine.Advertisements.Editor.Placeholder
struct Placeholder_t2906495853;
// UnityEngine.Advertisements.Editor.Platform
struct Platform_t2756657262;
// UnityEngine.Advertisements.IPlatform
struct IPlatform_t2298261414;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t990845000;
// UnityEngine.Advertisements.SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t1906253538;
// UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t3624702326;
// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3
struct U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544;
// UnityEngine.Advertisements.iOS.Platform/unityAdsDidError
struct unityAdsDidError_t1993223595;
// UnityEngine.Advertisements.iOS.Platform/unityAdsDidFinish
struct unityAdsDidFinish_t3747416149;
// UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart
struct unityAdsDidStart_t1058412932;
// UnityEngine.Advertisements.iOS.Platform/unityAdsReady
struct unityAdsReady_t96934738;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.Cubemap
struct Cubemap_t1972384409;
// UnityEngine.Experimental.Rendering.RenderPipelineAsset
struct RenderPipelineAsset_t533890058;
// UnityEngine.Experimental.Rendering.TextureCache/SSliceEntry[]
struct SSliceEntryU5BU5D_t2855060943;
// UnityEngine.Experimental.Rendering.VolumeProfile
struct VolumeProfile_t1487161726;
// UnityEngine.Experimental.Rendering.VolumeStack
struct VolumeStack_t1960641391;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t3656189108;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture3D
struct Texture3D_t1884131049;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;

struct Plane_t1000493321 ;
struct Vector3_t3722313464 ;



#ifndef U3CMODULEU3E_T692745578_H
#define U3CMODULEU3E_T692745578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745578 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745578_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t2843939325* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t2843939325* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t2843939325** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t2843939325* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSHOWU3EC__ANONSTOREY0_T3170924441_H
#define U3CSHOWU3EC__ANONSTOREY0_T3170924441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0
struct  U3CShowU3Ec__AnonStorey0_t3170924441  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0::showOptions
	ShowOptions_t990845000 * ___showOptions_0;

public:
	inline static int32_t get_offset_of_showOptions_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey0_t3170924441, ___showOptions_0)); }
	inline ShowOptions_t990845000 * get_showOptions_0() const { return ___showOptions_0; }
	inline ShowOptions_t990845000 ** get_address_of_showOptions_0() { return &___showOptions_0; }
	inline void set_showOptions_0(ShowOptions_t990845000 * value)
	{
		___showOptions_0 = value;
		Il2CppCodeGenWriteBarrier((&___showOptions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3EC__ANONSTOREY0_T3170924441_H
#ifndef U3CSHOWU3EC__ANONSTOREY1_T3170924442_H
#define U3CSHOWU3EC__ANONSTOREY1_T3170924442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey1
struct  U3CShowU3Ec__AnonStorey1_t3170924442  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey1::finishHandler
	EventHandler_1_t908338235 * ___finishHandler_0;
	// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0 UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey1::<>f__ref$0
	U3CShowU3Ec__AnonStorey0_t3170924441 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_finishHandler_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey1_t3170924442, ___finishHandler_0)); }
	inline EventHandler_1_t908338235 * get_finishHandler_0() const { return ___finishHandler_0; }
	inline EventHandler_1_t908338235 ** get_address_of_finishHandler_0() { return &___finishHandler_0; }
	inline void set_finishHandler_0(EventHandler_1_t908338235 * value)
	{
		___finishHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___finishHandler_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey1_t3170924442, ___U3CU3Ef__refU240_1)); }
	inline U3CShowU3Ec__AnonStorey0_t3170924441 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CShowU3Ec__AnonStorey0_t3170924441 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CShowU3Ec__AnonStorey0_t3170924441 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3EC__ANONSTOREY1_T3170924442_H
#ifndef U3CONUNITYADSERRORU3EC__ANONSTOREY4_T981031180_H
#define U3CONUNITYADSERRORU3EC__ANONSTOREY4_T981031180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey4
struct  U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs> UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey4::handler
	EventHandler_1_t177306446 * ___handler_0;
	// System.String UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey4::message
	String_t* ___message_1;
	// UnityEngine.Advertisements.Android.Platform UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey4::$this
	Platform_t1698302846 * ___U24this_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180, ___handler_0)); }
	inline EventHandler_1_t177306446 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t177306446 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t177306446 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180, ___U24this_2)); }
	inline Platform_t1698302846 * get_U24this_2() const { return ___U24this_2; }
	inline Platform_t1698302846 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Platform_t1698302846 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONUNITYADSERRORU3EC__ANONSTOREY4_T981031180_H
#ifndef U3CONUNITYADSERRORU3EC__ANONSTOREY5_T3709914535_H
#define U3CONUNITYADSERRORU3EC__ANONSTOREY5_T3709914535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey5
struct  U3ConUnityAdsErrorU3Ec__AnonStorey5_t3709914535  : public RuntimeObject
{
public:
	// System.Int64 UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey5::error
	int64_t ___error_0;
	// UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey4 UnityEngine.Advertisements.Android.Platform/<onUnityAdsError>c__AnonStorey5::<>f__ref$4
	U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(U3ConUnityAdsErrorU3Ec__AnonStorey5_t3709914535, ___error_0)); }
	inline int64_t get_error_0() const { return ___error_0; }
	inline int64_t* get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(int64_t value)
	{
		___error_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3ConUnityAdsErrorU3Ec__AnonStorey5_t3709914535, ___U3CU3Ef__refU244_1)); }
	inline U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU244_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONUNITYADSERRORU3EC__ANONSTOREY5_T3709914535_H
#ifndef U3CONUNITYADSFINISHU3EC__ANONSTOREY2_T696879596_H
#define U3CONUNITYADSFINISHU3EC__ANONSTOREY2_T696879596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey2
struct  U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey2::handler
	EventHandler_1_t908338235 * ___handler_0;
	// System.String UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey2::placementId
	String_t* ___placementId_1;
	// UnityEngine.Advertisements.Android.Platform UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey2::$this
	Platform_t1698302846 * ___U24this_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596, ___handler_0)); }
	inline EventHandler_1_t908338235 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t908338235 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t908338235 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596, ___U24this_2)); }
	inline Platform_t1698302846 * get_U24this_2() const { return ___U24this_2; }
	inline Platform_t1698302846 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Platform_t1698302846 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONUNITYADSFINISHU3EC__ANONSTOREY2_T696879596_H
#ifndef U3CONUNITYADSREADYU3EC__ANONSTOREY0_T2484343815_H
#define U3CONUNITYADSREADYU3EC__ANONSTOREY0_T2484343815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform/<onUnityAdsReady>c__AnonStorey0
struct  U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs> UnityEngine.Advertisements.Android.Platform/<onUnityAdsReady>c__AnonStorey0::handler
	EventHandler_1_t2768214265 * ___handler_0;
	// System.String UnityEngine.Advertisements.Android.Platform/<onUnityAdsReady>c__AnonStorey0::placementId
	String_t* ___placementId_1;
	// UnityEngine.Advertisements.Android.Platform UnityEngine.Advertisements.Android.Platform/<onUnityAdsReady>c__AnonStorey0::$this
	Platform_t1698302846 * ___U24this_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815, ___handler_0)); }
	inline EventHandler_1_t2768214265 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t2768214265 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t2768214265 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815, ___U24this_2)); }
	inline Platform_t1698302846 * get_U24this_2() const { return ___U24this_2; }
	inline Platform_t1698302846 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Platform_t1698302846 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONUNITYADSREADYU3EC__ANONSTOREY0_T2484343815_H
#ifndef U3CONUNITYADSSTARTU3EC__ANONSTOREY1_T2789353_H
#define U3CONUNITYADSSTARTU3EC__ANONSTOREY1_T2789353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform/<onUnityAdsStart>c__AnonStorey1
struct  U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.Android.Platform/<onUnityAdsStart>c__AnonStorey1::handler
	EventHandler_1_t2215985868 * ___handler_0;
	// System.String UnityEngine.Advertisements.Android.Platform/<onUnityAdsStart>c__AnonStorey1::placementId
	String_t* ___placementId_1;
	// UnityEngine.Advertisements.Android.Platform UnityEngine.Advertisements.Android.Platform/<onUnityAdsStart>c__AnonStorey1::$this
	Platform_t1698302846 * ___U24this_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353, ___handler_0)); }
	inline EventHandler_1_t2215985868 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t2215985868 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t2215985868 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353, ___U24this_2)); }
	inline Platform_t1698302846 * get_U24this_2() const { return ___U24this_2; }
	inline Platform_t1698302846 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Platform_t1698302846 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONUNITYADSSTARTU3EC__ANONSTOREY1_T2789353_H
#ifndef CONFIGURATION_T1722493896_H
#define CONFIGURATION_T1722493896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Configuration
struct  Configuration_t1722493896  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Advertisements.Editor.Configuration::<enabled>k__BackingField
	bool ___U3CenabledU3Ek__BackingField_0;
	// System.String UnityEngine.Advertisements.Editor.Configuration::<defaultPlacement>k__BackingField
	String_t* ___U3CdefaultPlacementU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Editor.Configuration::<placements>k__BackingField
	Dictionary_2_t4177511560 * ___U3CplacementsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CenabledU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Configuration_t1722493896, ___U3CenabledU3Ek__BackingField_0)); }
	inline bool get_U3CenabledU3Ek__BackingField_0() const { return ___U3CenabledU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CenabledU3Ek__BackingField_0() { return &___U3CenabledU3Ek__BackingField_0; }
	inline void set_U3CenabledU3Ek__BackingField_0(bool value)
	{
		___U3CenabledU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultPlacementU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Configuration_t1722493896, ___U3CdefaultPlacementU3Ek__BackingField_1)); }
	inline String_t* get_U3CdefaultPlacementU3Ek__BackingField_1() const { return ___U3CdefaultPlacementU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdefaultPlacementU3Ek__BackingField_1() { return &___U3CdefaultPlacementU3Ek__BackingField_1; }
	inline void set_U3CdefaultPlacementU3Ek__BackingField_1(String_t* value)
	{
		___U3CdefaultPlacementU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdefaultPlacementU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CplacementsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Configuration_t1722493896, ___U3CplacementsU3Ek__BackingField_2)); }
	inline Dictionary_2_t4177511560 * get_U3CplacementsU3Ek__BackingField_2() const { return ___U3CplacementsU3Ek__BackingField_2; }
	inline Dictionary_2_t4177511560 ** get_address_of_U3CplacementsU3Ek__BackingField_2() { return &___U3CplacementsU3Ek__BackingField_2; }
	inline void set_U3CplacementsU3Ek__BackingField_2(Dictionary_2_t4177511560 * value)
	{
		___U3CplacementsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATION_T1722493896_H
#ifndef PLATFORM_T2756657262_H
#define PLATFORM_T2756657262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Platform
struct  Platform_t2756657262  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Advertisements.Editor.Platform::m_DebugMode
	bool ___m_DebugMode_1;
	// UnityEngine.Advertisements.Editor.Configuration UnityEngine.Advertisements.Editor.Platform::m_Configuration
	Configuration_t1722493896 * ___m_Configuration_2;
	// UnityEngine.Advertisements.Editor.Placeholder UnityEngine.Advertisements.Editor.Platform::m_Placeholder
	Placeholder_t2906495853 * ___m_Placeholder_3;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.Editor.Platform::OnStart
	EventHandler_1_t2215985868 * ___OnStart_4;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Editor.Platform::OnFinish
	EventHandler_1_t908338235 * ___OnFinish_5;

public:
	inline static int32_t get_offset_of_m_DebugMode_1() { return static_cast<int32_t>(offsetof(Platform_t2756657262, ___m_DebugMode_1)); }
	inline bool get_m_DebugMode_1() const { return ___m_DebugMode_1; }
	inline bool* get_address_of_m_DebugMode_1() { return &___m_DebugMode_1; }
	inline void set_m_DebugMode_1(bool value)
	{
		___m_DebugMode_1 = value;
	}

	inline static int32_t get_offset_of_m_Configuration_2() { return static_cast<int32_t>(offsetof(Platform_t2756657262, ___m_Configuration_2)); }
	inline Configuration_t1722493896 * get_m_Configuration_2() const { return ___m_Configuration_2; }
	inline Configuration_t1722493896 ** get_address_of_m_Configuration_2() { return &___m_Configuration_2; }
	inline void set_m_Configuration_2(Configuration_t1722493896 * value)
	{
		___m_Configuration_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Configuration_2), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_3() { return static_cast<int32_t>(offsetof(Platform_t2756657262, ___m_Placeholder_3)); }
	inline Placeholder_t2906495853 * get_m_Placeholder_3() const { return ___m_Placeholder_3; }
	inline Placeholder_t2906495853 ** get_address_of_m_Placeholder_3() { return &___m_Placeholder_3; }
	inline void set_m_Placeholder_3(Placeholder_t2906495853 * value)
	{
		___m_Placeholder_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_3), value);
	}

	inline static int32_t get_offset_of_OnStart_4() { return static_cast<int32_t>(offsetof(Platform_t2756657262, ___OnStart_4)); }
	inline EventHandler_1_t2215985868 * get_OnStart_4() const { return ___OnStart_4; }
	inline EventHandler_1_t2215985868 ** get_address_of_OnStart_4() { return &___OnStart_4; }
	inline void set_OnStart_4(EventHandler_1_t2215985868 * value)
	{
		___OnStart_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_4), value);
	}

	inline static int32_t get_offset_of_OnFinish_5() { return static_cast<int32_t>(offsetof(Platform_t2756657262, ___OnFinish_5)); }
	inline EventHandler_1_t908338235 * get_OnFinish_5() const { return ___OnFinish_5; }
	inline EventHandler_1_t908338235 ** get_address_of_OnFinish_5() { return &___OnFinish_5; }
	inline void set_OnFinish_5(EventHandler_1_t908338235 * value)
	{
		___OnFinish_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_5), value);
	}
};

struct Platform_t2756657262_StaticFields
{
public:
	// System.String UnityEngine.Advertisements.Editor.Platform::s_BaseUrl
	String_t* ___s_BaseUrl_0;

public:
	inline static int32_t get_offset_of_s_BaseUrl_0() { return static_cast<int32_t>(offsetof(Platform_t2756657262_StaticFields, ___s_BaseUrl_0)); }
	inline String_t* get_s_BaseUrl_0() const { return ___s_BaseUrl_0; }
	inline String_t** get_address_of_s_BaseUrl_0() { return &___s_BaseUrl_0; }
	inline void set_s_BaseUrl_0(String_t* value)
	{
		___s_BaseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_BaseUrl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T2756657262_H
#ifndef U3CINITIALIZEU3EC__ANONSTOREY0_T2183256279_H
#define U3CINITIALIZEU3EC__ANONSTOREY0_T2183256279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0
struct  U3CInitializeU3Ec__AnonStorey0_t2183256279  : public RuntimeObject
{
public:
	// System.Net.WebRequest UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0::request
	WebRequest_t1939381076 * ___request_0;
	// System.String UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0::gameId
	String_t* ___gameId_1;
	// UnityEngine.Advertisements.Editor.Platform UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0::$this
	Platform_t2756657262 * ___U24this_2;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_t2183256279, ___request_0)); }
	inline WebRequest_t1939381076 * get_request_0() const { return ___request_0; }
	inline WebRequest_t1939381076 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(WebRequest_t1939381076 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_gameId_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_t2183256279, ___gameId_1)); }
	inline String_t* get_gameId_1() const { return ___gameId_1; }
	inline String_t** get_address_of_gameId_1() { return &___gameId_1; }
	inline void set_gameId_1(String_t* value)
	{
		___gameId_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameId_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_t2183256279, ___U24this_2)); }
	inline Platform_t2756657262 * get_U24this_2() const { return ___U24this_2; }
	inline Platform_t2756657262 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Platform_t2756657262 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3EC__ANONSTOREY0_T2183256279_H
#ifndef METADATA_T2274729001_H
#define METADATA_T2274729001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.MetaData
struct  MetaData_t2274729001  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::m_MetaData
	RuntimeObject* ___m_MetaData_0;
	// System.String UnityEngine.Advertisements.MetaData::<category>k__BackingField
	String_t* ___U3CcategoryU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_m_MetaData_0() { return static_cast<int32_t>(offsetof(MetaData_t2274729001, ___m_MetaData_0)); }
	inline RuntimeObject* get_m_MetaData_0() const { return ___m_MetaData_0; }
	inline RuntimeObject** get_address_of_m_MetaData_0() { return &___m_MetaData_0; }
	inline void set_m_MetaData_0(RuntimeObject* value)
	{
		___m_MetaData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MetaData_0), value);
	}

	inline static int32_t get_offset_of_U3CcategoryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetaData_t2274729001, ___U3CcategoryU3Ek__BackingField_1)); }
	inline String_t* get_U3CcategoryU3Ek__BackingField_1() const { return ___U3CcategoryU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcategoryU3Ek__BackingField_1() { return &___U3CcategoryU3Ek__BackingField_1; }
	inline void set_U3CcategoryU3Ek__BackingField_1(String_t* value)
	{
		___U3CcategoryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcategoryU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATA_T2274729001_H
#ifndef SHOWOPTIONS_T990845000_H
#define SHOWOPTIONS_T990845000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowOptions
struct  ShowOptions_t990845000  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::<resultCallback>k__BackingField
	Action_1_t3243021218 * ___U3CresultCallbackU3Ek__BackingField_0;
	// System.String UnityEngine.Advertisements.ShowOptions::<gamerSid>k__BackingField
	String_t* ___U3CgamerSidU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CresultCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowOptions_t990845000, ___U3CresultCallbackU3Ek__BackingField_0)); }
	inline Action_1_t3243021218 * get_U3CresultCallbackU3Ek__BackingField_0() const { return ___U3CresultCallbackU3Ek__BackingField_0; }
	inline Action_1_t3243021218 ** get_address_of_U3CresultCallbackU3Ek__BackingField_0() { return &___U3CresultCallbackU3Ek__BackingField_0; }
	inline void set_U3CresultCallbackU3Ek__BackingField_0(Action_1_t3243021218 * value)
	{
		___U3CresultCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CgamerSidU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowOptions_t990845000, ___U3CgamerSidU3Ek__BackingField_1)); }
	inline String_t* get_U3CgamerSidU3Ek__BackingField_1() const { return ___U3CgamerSidU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgamerSidU3Ek__BackingField_1() { return &___U3CgamerSidU3Ek__BackingField_1; }
	inline void set_U3CgamerSidU3Ek__BackingField_1(String_t* value)
	{
		___U3CgamerSidU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgamerSidU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWOPTIONS_T990845000_H
#ifndef JSONOBJECT_T1327569318_H
#define JSONOBJECT_T1327569318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.JsonObject
struct  JsonObject_t1327569318  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.SimpleJson.JsonObject::_members
	Dictionary_2_t2865362463 * ____members_0;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(JsonObject_t1327569318, ____members_0)); }
	inline Dictionary_2_t2865362463 * get__members_0() const { return ____members_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(Dictionary_2_t2865362463 * value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T1327569318_H
#ifndef POCOJSONSERIALIZERSTRATEGY_T3624702326_H
#define POCOJSONSERIALIZERSTRATEGY_T3624702326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy
struct  PocoJsonSerializerStrategy_t3624702326  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate> UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::ConstructorCache
	RuntimeObject* ___ConstructorCache_0;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/GetDelegate>> UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::GetCache
	RuntimeObject* ___GetCache_1;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/SetDelegate>>> UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::SetCache
	RuntimeObject* ___SetCache_2;

public:
	inline static int32_t get_offset_of_ConstructorCache_0() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3624702326, ___ConstructorCache_0)); }
	inline RuntimeObject* get_ConstructorCache_0() const { return ___ConstructorCache_0; }
	inline RuntimeObject** get_address_of_ConstructorCache_0() { return &___ConstructorCache_0; }
	inline void set_ConstructorCache_0(RuntimeObject* value)
	{
		___ConstructorCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorCache_0), value);
	}

	inline static int32_t get_offset_of_GetCache_1() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3624702326, ___GetCache_1)); }
	inline RuntimeObject* get_GetCache_1() const { return ___GetCache_1; }
	inline RuntimeObject** get_address_of_GetCache_1() { return &___GetCache_1; }
	inline void set_GetCache_1(RuntimeObject* value)
	{
		___GetCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___GetCache_1), value);
	}

	inline static int32_t get_offset_of_SetCache_2() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3624702326, ___SetCache_2)); }
	inline RuntimeObject* get_SetCache_2() const { return ___SetCache_2; }
	inline RuntimeObject** get_address_of_SetCache_2() { return &___SetCache_2; }
	inline void set_SetCache_2(RuntimeObject* value)
	{
		___SetCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___SetCache_2), value);
	}
};

struct PocoJsonSerializerStrategy_t3624702326_StaticFields
{
public:
	// System.Type[] UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Type[] UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::ArrayConstructorParameterTypes
	TypeU5BU5D_t3940880105* ___ArrayConstructorParameterTypes_4;
	// System.String[] UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::Iso8601Format
	StringU5BU5D_t1281789340* ___Iso8601Format_5;

public:
	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3624702326_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_ArrayConstructorParameterTypes_4() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3624702326_StaticFields, ___ArrayConstructorParameterTypes_4)); }
	inline TypeU5BU5D_t3940880105* get_ArrayConstructorParameterTypes_4() const { return ___ArrayConstructorParameterTypes_4; }
	inline TypeU5BU5D_t3940880105** get_address_of_ArrayConstructorParameterTypes_4() { return &___ArrayConstructorParameterTypes_4; }
	inline void set_ArrayConstructorParameterTypes_4(TypeU5BU5D_t3940880105* value)
	{
		___ArrayConstructorParameterTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayConstructorParameterTypes_4), value);
	}

	inline static int32_t get_offset_of_Iso8601Format_5() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3624702326_StaticFields, ___Iso8601Format_5)); }
	inline StringU5BU5D_t1281789340* get_Iso8601Format_5() const { return ___Iso8601Format_5; }
	inline StringU5BU5D_t1281789340** get_address_of_Iso8601Format_5() { return &___Iso8601Format_5; }
	inline void set_Iso8601Format_5(StringU5BU5D_t1281789340* value)
	{
		___Iso8601Format_5 = value;
		Il2CppCodeGenWriteBarrier((&___Iso8601Format_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POCOJSONSERIALIZERSTRATEGY_T3624702326_H
#ifndef SIMPLEJSON_T791838946_H
#define SIMPLEJSON_T791838946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.SimpleJson
struct  SimpleJson_t791838946  : public RuntimeObject
{
public:

public:
};

struct SimpleJson_t791838946_StaticFields
{
public:
	// UnityEngine.Advertisements.SimpleJson.IJsonSerializerStrategy UnityEngine.Advertisements.SimpleJson.SimpleJson::_currentJsonSerializerStrategy
	RuntimeObject* ____currentJsonSerializerStrategy_13;
	// UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy UnityEngine.Advertisements.SimpleJson.SimpleJson::_pocoJsonSerializerStrategy
	PocoJsonSerializerStrategy_t3624702326 * ____pocoJsonSerializerStrategy_14;

public:
	inline static int32_t get_offset_of__currentJsonSerializerStrategy_13() { return static_cast<int32_t>(offsetof(SimpleJson_t791838946_StaticFields, ____currentJsonSerializerStrategy_13)); }
	inline RuntimeObject* get__currentJsonSerializerStrategy_13() const { return ____currentJsonSerializerStrategy_13; }
	inline RuntimeObject** get_address_of__currentJsonSerializerStrategy_13() { return &____currentJsonSerializerStrategy_13; }
	inline void set__currentJsonSerializerStrategy_13(RuntimeObject* value)
	{
		____currentJsonSerializerStrategy_13 = value;
		Il2CppCodeGenWriteBarrier((&____currentJsonSerializerStrategy_13), value);
	}

	inline static int32_t get_offset_of__pocoJsonSerializerStrategy_14() { return static_cast<int32_t>(offsetof(SimpleJson_t791838946_StaticFields, ____pocoJsonSerializerStrategy_14)); }
	inline PocoJsonSerializerStrategy_t3624702326 * get__pocoJsonSerializerStrategy_14() const { return ____pocoJsonSerializerStrategy_14; }
	inline PocoJsonSerializerStrategy_t3624702326 ** get_address_of__pocoJsonSerializerStrategy_14() { return &____pocoJsonSerializerStrategy_14; }
	inline void set__pocoJsonSerializerStrategy_14(PocoJsonSerializerStrategy_t3624702326 * value)
	{
		____pocoJsonSerializerStrategy_14 = value;
		Il2CppCodeGenWriteBarrier((&____pocoJsonSerializerStrategy_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEJSON_T791838946_H
#ifndef PLATFORM_T1647901813_H
#define PLATFORM_T1647901813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform
struct  Platform_t1647901813  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs> UnityEngine.Advertisements.iOS.Platform::OnReady
	EventHandler_1_t2768214265 * ___OnReady_2;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.iOS.Platform::OnStart
	EventHandler_1_t2215985868 * ___OnStart_3;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.iOS.Platform::OnFinish
	EventHandler_1_t908338235 * ___OnFinish_4;
	// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs> UnityEngine.Advertisements.iOS.Platform::OnError
	EventHandler_1_t177306446 * ___OnError_5;

public:
	inline static int32_t get_offset_of_OnReady_2() { return static_cast<int32_t>(offsetof(Platform_t1647901813, ___OnReady_2)); }
	inline EventHandler_1_t2768214265 * get_OnReady_2() const { return ___OnReady_2; }
	inline EventHandler_1_t2768214265 ** get_address_of_OnReady_2() { return &___OnReady_2; }
	inline void set_OnReady_2(EventHandler_1_t2768214265 * value)
	{
		___OnReady_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnReady_2), value);
	}

	inline static int32_t get_offset_of_OnStart_3() { return static_cast<int32_t>(offsetof(Platform_t1647901813, ___OnStart_3)); }
	inline EventHandler_1_t2215985868 * get_OnStart_3() const { return ___OnStart_3; }
	inline EventHandler_1_t2215985868 ** get_address_of_OnStart_3() { return &___OnStart_3; }
	inline void set_OnStart_3(EventHandler_1_t2215985868 * value)
	{
		___OnStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_3), value);
	}

	inline static int32_t get_offset_of_OnFinish_4() { return static_cast<int32_t>(offsetof(Platform_t1647901813, ___OnFinish_4)); }
	inline EventHandler_1_t908338235 * get_OnFinish_4() const { return ___OnFinish_4; }
	inline EventHandler_1_t908338235 ** get_address_of_OnFinish_4() { return &___OnFinish_4; }
	inline void set_OnFinish_4(EventHandler_1_t908338235 * value)
	{
		___OnFinish_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_4), value);
	}

	inline static int32_t get_offset_of_OnError_5() { return static_cast<int32_t>(offsetof(Platform_t1647901813, ___OnError_5)); }
	inline EventHandler_1_t177306446 * get_OnError_5() const { return ___OnError_5; }
	inline EventHandler_1_t177306446 ** get_address_of_OnError_5() { return &___OnError_5; }
	inline void set_OnError_5(EventHandler_1_t177306446 * value)
	{
		___OnError_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_5), value);
	}
};

struct Platform_t1647901813_StaticFields
{
public:
	// UnityEngine.Advertisements.iOS.Platform UnityEngine.Advertisements.iOS.Platform::s_Instance
	Platform_t1647901813 * ___s_Instance_0;
	// UnityEngine.Advertisements.CallbackExecutor UnityEngine.Advertisements.iOS.Platform::s_CallbackExecutor
	CallbackExecutor_t363496179 * ___s_CallbackExecutor_1;
	// UnityEngine.Advertisements.iOS.Platform/unityAdsReady UnityEngine.Advertisements.iOS.Platform::<>f__mg$cache0
	unityAdsReady_t96934738 * ___U3CU3Ef__mgU24cache0_6;
	// UnityEngine.Advertisements.iOS.Platform/unityAdsDidError UnityEngine.Advertisements.iOS.Platform::<>f__mg$cache1
	unityAdsDidError_t1993223595 * ___U3CU3Ef__mgU24cache1_7;
	// UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart UnityEngine.Advertisements.iOS.Platform::<>f__mg$cache2
	unityAdsDidStart_t1058412932 * ___U3CU3Ef__mgU24cache2_8;
	// UnityEngine.Advertisements.iOS.Platform/unityAdsDidFinish UnityEngine.Advertisements.iOS.Platform::<>f__mg$cache3
	unityAdsDidFinish_t3747416149 * ___U3CU3Ef__mgU24cache3_9;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(Platform_t1647901813_StaticFields, ___s_Instance_0)); }
	inline Platform_t1647901813 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline Platform_t1647901813 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(Platform_t1647901813 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_s_CallbackExecutor_1() { return static_cast<int32_t>(offsetof(Platform_t1647901813_StaticFields, ___s_CallbackExecutor_1)); }
	inline CallbackExecutor_t363496179 * get_s_CallbackExecutor_1() const { return ___s_CallbackExecutor_1; }
	inline CallbackExecutor_t363496179 ** get_address_of_s_CallbackExecutor_1() { return &___s_CallbackExecutor_1; }
	inline void set_s_CallbackExecutor_1(CallbackExecutor_t363496179 * value)
	{
		___s_CallbackExecutor_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallbackExecutor_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(Platform_t1647901813_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline unityAdsReady_t96934738 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline unityAdsReady_t96934738 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(unityAdsReady_t96934738 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_7() { return static_cast<int32_t>(offsetof(Platform_t1647901813_StaticFields, ___U3CU3Ef__mgU24cache1_7)); }
	inline unityAdsDidError_t1993223595 * get_U3CU3Ef__mgU24cache1_7() const { return ___U3CU3Ef__mgU24cache1_7; }
	inline unityAdsDidError_t1993223595 ** get_address_of_U3CU3Ef__mgU24cache1_7() { return &___U3CU3Ef__mgU24cache1_7; }
	inline void set_U3CU3Ef__mgU24cache1_7(unityAdsDidError_t1993223595 * value)
	{
		___U3CU3Ef__mgU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_8() { return static_cast<int32_t>(offsetof(Platform_t1647901813_StaticFields, ___U3CU3Ef__mgU24cache2_8)); }
	inline unityAdsDidStart_t1058412932 * get_U3CU3Ef__mgU24cache2_8() const { return ___U3CU3Ef__mgU24cache2_8; }
	inline unityAdsDidStart_t1058412932 ** get_address_of_U3CU3Ef__mgU24cache2_8() { return &___U3CU3Ef__mgU24cache2_8; }
	inline void set_U3CU3Ef__mgU24cache2_8(unityAdsDidStart_t1058412932 * value)
	{
		___U3CU3Ef__mgU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_9() { return static_cast<int32_t>(offsetof(Platform_t1647901813_StaticFields, ___U3CU3Ef__mgU24cache3_9)); }
	inline unityAdsDidFinish_t3747416149 * get_U3CU3Ef__mgU24cache3_9() const { return ___U3CU3Ef__mgU24cache3_9; }
	inline unityAdsDidFinish_t3747416149 ** get_address_of_U3CU3Ef__mgU24cache3_9() { return &___U3CU3Ef__mgU24cache3_9; }
	inline void set_U3CU3Ef__mgU24cache3_9(unityAdsDidFinish_t3747416149 * value)
	{
		___U3CU3Ef__mgU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T1647901813_H
#ifndef U3CUNITYADSDIDERRORU3EC__ANONSTOREY1_T2659421617_H
#define U3CUNITYADSDIDERRORU3EC__ANONSTOREY1_T2659421617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1
struct  U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs> UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1::handler
	EventHandler_1_t177306446 * ___handler_0;
	// System.Int64 UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1::rawError
	int64_t ___rawError_1;
	// System.String UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1::message
	String_t* ___message_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617, ___handler_0)); }
	inline EventHandler_1_t177306446 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t177306446 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t177306446 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_rawError_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617, ___rawError_1)); }
	inline int64_t get_rawError_1() const { return ___rawError_1; }
	inline int64_t* get_address_of_rawError_1() { return &___rawError_1; }
	inline void set_rawError_1(int64_t value)
	{
		___rawError_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDERRORU3EC__ANONSTOREY1_T2659421617_H
#ifndef U3CUNITYADSDIDFINISHU3EC__ANONSTOREY3_T3126029544_H
#define U3CUNITYADSDIDFINISHU3EC__ANONSTOREY3_T3126029544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3
struct  U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3::handler
	EventHandler_1_t908338235 * ___handler_0;
	// System.String UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3::placementId
	String_t* ___placementId_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544, ___handler_0)); }
	inline EventHandler_1_t908338235 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t908338235 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t908338235 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDFINISHU3EC__ANONSTOREY3_T3126029544_H
#ifndef U3CUNITYADSDIDSTARTU3EC__ANONSTOREY2_T250005642_H
#define U3CUNITYADSDIDSTARTU3EC__ANONSTOREY2_T250005642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidStart>c__AnonStorey2
struct  U3CUnityAdsDidStartU3Ec__AnonStorey2_t250005642  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidStart>c__AnonStorey2::handler
	EventHandler_1_t2215985868 * ___handler_0;
	// System.String UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidStart>c__AnonStorey2::placementId
	String_t* ___placementId_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidStartU3Ec__AnonStorey2_t250005642, ___handler_0)); }
	inline EventHandler_1_t2215985868 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t2215985868 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t2215985868 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidStartU3Ec__AnonStorey2_t250005642, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDSTARTU3EC__ANONSTOREY2_T250005642_H
#ifndef U3CUNITYADSREADYU3EC__ANONSTOREY0_T1009704718_H
#define U3CUNITYADSREADYU3EC__ANONSTOREY0_T1009704718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsReady>c__AnonStorey0
struct  U3CUnityAdsReadyU3Ec__AnonStorey0_t1009704718  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs> UnityEngine.Advertisements.iOS.Platform/<UnityAdsReady>c__AnonStorey0::handler
	EventHandler_1_t2768214265 * ___handler_0;
	// System.String UnityEngine.Advertisements.iOS.Platform/<UnityAdsReady>c__AnonStorey0::placementId
	String_t* ___placementId_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsReadyU3Ec__AnonStorey0_t1009704718, ___handler_0)); }
	inline EventHandler_1_t2768214265 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t2768214265 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t2768214265 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsReadyU3Ec__AnonStorey0_t1009704718, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSREADYU3EC__ANONSTOREY0_T1009704718_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef COREUTILS_T3708687010_H
#define COREUTILS_T3708687010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.CoreUtils
struct  CoreUtils_t3708687010  : public RuntimeObject
{
public:

public:
};

struct CoreUtils_t3708687010_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.Experimental.Rendering.CoreUtils::lookAtList
	Vector3U5BU5D_t1718750761* ___lookAtList_0;
	// UnityEngine.Vector3[] UnityEngine.Experimental.Rendering.CoreUtils::upVectorList
	Vector3U5BU5D_t1718750761* ___upVectorList_1;
	// UnityEngine.Cubemap UnityEngine.Experimental.Rendering.CoreUtils::m_BlackCubeTexture
	Cubemap_t1972384409 * ___m_BlackCubeTexture_8;
	// UnityEngine.Cubemap UnityEngine.Experimental.Rendering.CoreUtils::m_MagentaCubeTexture
	Cubemap_t1972384409 * ___m_MagentaCubeTexture_9;
	// UnityEngine.Cubemap UnityEngine.Experimental.Rendering.CoreUtils::m_WhiteCubeTexture
	Cubemap_t1972384409 * ___m_WhiteCubeTexture_10;
	// UnityEngine.RenderTexture UnityEngine.Experimental.Rendering.CoreUtils::m_EmptyUAV
	RenderTexture_t2108887433 * ___m_EmptyUAV_11;
	// UnityEngine.Texture3D UnityEngine.Experimental.Rendering.CoreUtils::m_BlackVolumeTexture
	Texture3D_t1884131049 * ___m_BlackVolumeTexture_12;
	// System.Collections.Generic.IEnumerable`1<System.Type> UnityEngine.Experimental.Rendering.CoreUtils::m_AssemblyTypes
	RuntimeObject* ___m_AssemblyTypes_13;
	// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>> UnityEngine.Experimental.Rendering.CoreUtils::<>f__am$cache0
	Func_2_t779105388 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_lookAtList_0() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___lookAtList_0)); }
	inline Vector3U5BU5D_t1718750761* get_lookAtList_0() const { return ___lookAtList_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_lookAtList_0() { return &___lookAtList_0; }
	inline void set_lookAtList_0(Vector3U5BU5D_t1718750761* value)
	{
		___lookAtList_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtList_0), value);
	}

	inline static int32_t get_offset_of_upVectorList_1() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___upVectorList_1)); }
	inline Vector3U5BU5D_t1718750761* get_upVectorList_1() const { return ___upVectorList_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_upVectorList_1() { return &___upVectorList_1; }
	inline void set_upVectorList_1(Vector3U5BU5D_t1718750761* value)
	{
		___upVectorList_1 = value;
		Il2CppCodeGenWriteBarrier((&___upVectorList_1), value);
	}

	inline static int32_t get_offset_of_m_BlackCubeTexture_8() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___m_BlackCubeTexture_8)); }
	inline Cubemap_t1972384409 * get_m_BlackCubeTexture_8() const { return ___m_BlackCubeTexture_8; }
	inline Cubemap_t1972384409 ** get_address_of_m_BlackCubeTexture_8() { return &___m_BlackCubeTexture_8; }
	inline void set_m_BlackCubeTexture_8(Cubemap_t1972384409 * value)
	{
		___m_BlackCubeTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlackCubeTexture_8), value);
	}

	inline static int32_t get_offset_of_m_MagentaCubeTexture_9() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___m_MagentaCubeTexture_9)); }
	inline Cubemap_t1972384409 * get_m_MagentaCubeTexture_9() const { return ___m_MagentaCubeTexture_9; }
	inline Cubemap_t1972384409 ** get_address_of_m_MagentaCubeTexture_9() { return &___m_MagentaCubeTexture_9; }
	inline void set_m_MagentaCubeTexture_9(Cubemap_t1972384409 * value)
	{
		___m_MagentaCubeTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MagentaCubeTexture_9), value);
	}

	inline static int32_t get_offset_of_m_WhiteCubeTexture_10() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___m_WhiteCubeTexture_10)); }
	inline Cubemap_t1972384409 * get_m_WhiteCubeTexture_10() const { return ___m_WhiteCubeTexture_10; }
	inline Cubemap_t1972384409 ** get_address_of_m_WhiteCubeTexture_10() { return &___m_WhiteCubeTexture_10; }
	inline void set_m_WhiteCubeTexture_10(Cubemap_t1972384409 * value)
	{
		___m_WhiteCubeTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_WhiteCubeTexture_10), value);
	}

	inline static int32_t get_offset_of_m_EmptyUAV_11() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___m_EmptyUAV_11)); }
	inline RenderTexture_t2108887433 * get_m_EmptyUAV_11() const { return ___m_EmptyUAV_11; }
	inline RenderTexture_t2108887433 ** get_address_of_m_EmptyUAV_11() { return &___m_EmptyUAV_11; }
	inline void set_m_EmptyUAV_11(RenderTexture_t2108887433 * value)
	{
		___m_EmptyUAV_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_EmptyUAV_11), value);
	}

	inline static int32_t get_offset_of_m_BlackVolumeTexture_12() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___m_BlackVolumeTexture_12)); }
	inline Texture3D_t1884131049 * get_m_BlackVolumeTexture_12() const { return ___m_BlackVolumeTexture_12; }
	inline Texture3D_t1884131049 ** get_address_of_m_BlackVolumeTexture_12() { return &___m_BlackVolumeTexture_12; }
	inline void set_m_BlackVolumeTexture_12(Texture3D_t1884131049 * value)
	{
		___m_BlackVolumeTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlackVolumeTexture_12), value);
	}

	inline static int32_t get_offset_of_m_AssemblyTypes_13() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___m_AssemblyTypes_13)); }
	inline RuntimeObject* get_m_AssemblyTypes_13() const { return ___m_AssemblyTypes_13; }
	inline RuntimeObject** get_address_of_m_AssemblyTypes_13() { return &___m_AssemblyTypes_13; }
	inline void set_m_AssemblyTypes_13(RuntimeObject* value)
	{
		___m_AssemblyTypes_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssemblyTypes_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(CoreUtils_t3708687010_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Func_2_t779105388 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Func_2_t779105388 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Func_2_t779105388 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COREUTILS_T3708687010_H
#ifndef DELEGATEUTILITY_T69250001_H
#define DELEGATEUTILITY_T69250001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.DelegateUtility
struct  DelegateUtility_t69250001  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEUTILITY_T69250001_H
#ifndef TEXTURECACHE_T1429868522_H
#define TEXTURECACHE_T1429868522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.TextureCache
struct  TextureCache_t1429868522  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.TextureCache::m_NumMipLevels
	int32_t ___m_NumMipLevels_0;
	// System.String UnityEngine.Experimental.Rendering.TextureCache::m_CacheName
	String_t* ___m_CacheName_1;
	// System.Int32 UnityEngine.Experimental.Rendering.TextureCache::m_NumTextures
	int32_t ___m_NumTextures_2;
	// System.Int32[] UnityEngine.Experimental.Rendering.TextureCache::m_SortedIdxArray
	Int32U5BU5D_t385246372* ___m_SortedIdxArray_3;
	// UnityEngine.Experimental.Rendering.TextureCache/SSliceEntry[] UnityEngine.Experimental.Rendering.TextureCache::m_SliceArray
	SSliceEntryU5BU5D_t2855060943* ___m_SliceArray_4;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32> UnityEngine.Experimental.Rendering.TextureCache::m_LocatorInSliceArray
	Dictionary_2_t4225213415 * ___m_LocatorInSliceArray_5;

public:
	inline static int32_t get_offset_of_m_NumMipLevels_0() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522, ___m_NumMipLevels_0)); }
	inline int32_t get_m_NumMipLevels_0() const { return ___m_NumMipLevels_0; }
	inline int32_t* get_address_of_m_NumMipLevels_0() { return &___m_NumMipLevels_0; }
	inline void set_m_NumMipLevels_0(int32_t value)
	{
		___m_NumMipLevels_0 = value;
	}

	inline static int32_t get_offset_of_m_CacheName_1() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522, ___m_CacheName_1)); }
	inline String_t* get_m_CacheName_1() const { return ___m_CacheName_1; }
	inline String_t** get_address_of_m_CacheName_1() { return &___m_CacheName_1; }
	inline void set_m_CacheName_1(String_t* value)
	{
		___m_CacheName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheName_1), value);
	}

	inline static int32_t get_offset_of_m_NumTextures_2() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522, ___m_NumTextures_2)); }
	inline int32_t get_m_NumTextures_2() const { return ___m_NumTextures_2; }
	inline int32_t* get_address_of_m_NumTextures_2() { return &___m_NumTextures_2; }
	inline void set_m_NumTextures_2(int32_t value)
	{
		___m_NumTextures_2 = value;
	}

	inline static int32_t get_offset_of_m_SortedIdxArray_3() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522, ___m_SortedIdxArray_3)); }
	inline Int32U5BU5D_t385246372* get_m_SortedIdxArray_3() const { return ___m_SortedIdxArray_3; }
	inline Int32U5BU5D_t385246372** get_address_of_m_SortedIdxArray_3() { return &___m_SortedIdxArray_3; }
	inline void set_m_SortedIdxArray_3(Int32U5BU5D_t385246372* value)
	{
		___m_SortedIdxArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortedIdxArray_3), value);
	}

	inline static int32_t get_offset_of_m_SliceArray_4() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522, ___m_SliceArray_4)); }
	inline SSliceEntryU5BU5D_t2855060943* get_m_SliceArray_4() const { return ___m_SliceArray_4; }
	inline SSliceEntryU5BU5D_t2855060943** get_address_of_m_SliceArray_4() { return &___m_SliceArray_4; }
	inline void set_m_SliceArray_4(SSliceEntryU5BU5D_t2855060943* value)
	{
		___m_SliceArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SliceArray_4), value);
	}

	inline static int32_t get_offset_of_m_LocatorInSliceArray_5() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522, ___m_LocatorInSliceArray_5)); }
	inline Dictionary_2_t4225213415 * get_m_LocatorInSliceArray_5() const { return ___m_LocatorInSliceArray_5; }
	inline Dictionary_2_t4225213415 ** get_address_of_m_LocatorInSliceArray_5() { return &___m_LocatorInSliceArray_5; }
	inline void set_m_LocatorInSliceArray_5(Dictionary_2_t4225213415 * value)
	{
		___m_LocatorInSliceArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocatorInSliceArray_5), value);
	}
};

struct TextureCache_t1429868522_StaticFields
{
public:
	// System.UInt32 UnityEngine.Experimental.Rendering.TextureCache::g_MaxFrameCount
	uint32_t ___g_MaxFrameCount_6;
	// System.UInt32 UnityEngine.Experimental.Rendering.TextureCache::g_InvalidTexID
	uint32_t ___g_InvalidTexID_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Experimental.Rendering.TextureCache::s_TempIntList
	List_1_t128053199 * ___s_TempIntList_8;

public:
	inline static int32_t get_offset_of_g_MaxFrameCount_6() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522_StaticFields, ___g_MaxFrameCount_6)); }
	inline uint32_t get_g_MaxFrameCount_6() const { return ___g_MaxFrameCount_6; }
	inline uint32_t* get_address_of_g_MaxFrameCount_6() { return &___g_MaxFrameCount_6; }
	inline void set_g_MaxFrameCount_6(uint32_t value)
	{
		___g_MaxFrameCount_6 = value;
	}

	inline static int32_t get_offset_of_g_InvalidTexID_7() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522_StaticFields, ___g_InvalidTexID_7)); }
	inline uint32_t get_g_InvalidTexID_7() const { return ___g_InvalidTexID_7; }
	inline uint32_t* get_address_of_g_InvalidTexID_7() { return &___g_InvalidTexID_7; }
	inline void set_g_InvalidTexID_7(uint32_t value)
	{
		___g_InvalidTexID_7 = value;
	}

	inline static int32_t get_offset_of_s_TempIntList_8() { return static_cast<int32_t>(offsetof(TextureCache_t1429868522_StaticFields, ___s_TempIntList_8)); }
	inline List_1_t128053199 * get_s_TempIntList_8() const { return ___s_TempIntList_8; }
	inline List_1_t128053199 ** get_address_of_s_TempIntList_8() { return &___s_TempIntList_8; }
	inline void set_s_TempIntList_8(List_1_t128053199 * value)
	{
		___s_TempIntList_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_TempIntList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECACHE_T1429868522_H
#ifndef TEXTURESETTINGS_T2698640645_H
#define TEXTURESETTINGS_T2698640645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.TextureSettings
struct  TextureSettings_t2698640645  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.TextureSettings::spotCookieSize
	int32_t ___spotCookieSize_3;
	// System.Int32 UnityEngine.Experimental.Rendering.TextureSettings::pointCookieSize
	int32_t ___pointCookieSize_4;
	// System.Int32 UnityEngine.Experimental.Rendering.TextureSettings::reflectionCubemapSize
	int32_t ___reflectionCubemapSize_5;

public:
	inline static int32_t get_offset_of_spotCookieSize_3() { return static_cast<int32_t>(offsetof(TextureSettings_t2698640645, ___spotCookieSize_3)); }
	inline int32_t get_spotCookieSize_3() const { return ___spotCookieSize_3; }
	inline int32_t* get_address_of_spotCookieSize_3() { return &___spotCookieSize_3; }
	inline void set_spotCookieSize_3(int32_t value)
	{
		___spotCookieSize_3 = value;
	}

	inline static int32_t get_offset_of_pointCookieSize_4() { return static_cast<int32_t>(offsetof(TextureSettings_t2698640645, ___pointCookieSize_4)); }
	inline int32_t get_pointCookieSize_4() const { return ___pointCookieSize_4; }
	inline int32_t* get_address_of_pointCookieSize_4() { return &___pointCookieSize_4; }
	inline void set_pointCookieSize_4(int32_t value)
	{
		___pointCookieSize_4 = value;
	}

	inline static int32_t get_offset_of_reflectionCubemapSize_5() { return static_cast<int32_t>(offsetof(TextureSettings_t2698640645, ___reflectionCubemapSize_5)); }
	inline int32_t get_reflectionCubemapSize_5() const { return ___reflectionCubemapSize_5; }
	inline int32_t* get_address_of_reflectionCubemapSize_5() { return &___reflectionCubemapSize_5; }
	inline void set_reflectionCubemapSize_5(int32_t value)
	{
		___reflectionCubemapSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURESETTINGS_T2698640645_H
#ifndef TILELAYOUTUTILS_T1848109876_H
#define TILELAYOUTUTILS_T1848109876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.TileLayoutUtils
struct  TileLayoutUtils_t1848109876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILELAYOUTUTILS_T1848109876_H
#ifndef VOLUMEMANAGER_T1188818537_H
#define VOLUMEMANAGER_T1188818537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeManager
struct  VolumeManager_t1188818537  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.VolumeStack UnityEngine.Experimental.Rendering.VolumeManager::<stack>k__BackingField
	VolumeStack_t1960641391 * ___U3CstackU3Ek__BackingField_1;
	// System.Collections.Generic.IEnumerable`1<System.Type> UnityEngine.Experimental.Rendering.VolumeManager::<baseComponentTypes>k__BackingField
	RuntimeObject* ___U3CbaseComponentTypesU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.Volume>> UnityEngine.Experimental.Rendering.VolumeManager::m_SortedVolumes
	Dictionary_2_t4117966577 * ___m_SortedVolumes_4;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.Volume> UnityEngine.Experimental.Rendering.VolumeManager::m_Volumes
	List_1_t934285950 * ___m_Volumes_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Experimental.Rendering.VolumeManager::m_SortNeeded
	Dictionary_2_t3280968592 * ___m_SortNeeded_6;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VolumeComponent> UnityEngine.Experimental.Rendering.VolumeManager::m_ComponentsDefaultState
	List_1_t2297852916 * ___m_ComponentsDefaultState_7;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.Experimental.Rendering.VolumeManager::m_TempColliders
	List_1_t3245421752 * ___m_TempColliders_8;

public:
	inline static int32_t get_offset_of_U3CstackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___U3CstackU3Ek__BackingField_1)); }
	inline VolumeStack_t1960641391 * get_U3CstackU3Ek__BackingField_1() const { return ___U3CstackU3Ek__BackingField_1; }
	inline VolumeStack_t1960641391 ** get_address_of_U3CstackU3Ek__BackingField_1() { return &___U3CstackU3Ek__BackingField_1; }
	inline void set_U3CstackU3Ek__BackingField_1(VolumeStack_t1960641391 * value)
	{
		___U3CstackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CbaseComponentTypesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___U3CbaseComponentTypesU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CbaseComponentTypesU3Ek__BackingField_2() const { return ___U3CbaseComponentTypesU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CbaseComponentTypesU3Ek__BackingField_2() { return &___U3CbaseComponentTypesU3Ek__BackingField_2; }
	inline void set_U3CbaseComponentTypesU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CbaseComponentTypesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbaseComponentTypesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_SortedVolumes_4() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___m_SortedVolumes_4)); }
	inline Dictionary_2_t4117966577 * get_m_SortedVolumes_4() const { return ___m_SortedVolumes_4; }
	inline Dictionary_2_t4117966577 ** get_address_of_m_SortedVolumes_4() { return &___m_SortedVolumes_4; }
	inline void set_m_SortedVolumes_4(Dictionary_2_t4117966577 * value)
	{
		___m_SortedVolumes_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortedVolumes_4), value);
	}

	inline static int32_t get_offset_of_m_Volumes_5() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___m_Volumes_5)); }
	inline List_1_t934285950 * get_m_Volumes_5() const { return ___m_Volumes_5; }
	inline List_1_t934285950 ** get_address_of_m_Volumes_5() { return &___m_Volumes_5; }
	inline void set_m_Volumes_5(List_1_t934285950 * value)
	{
		___m_Volumes_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Volumes_5), value);
	}

	inline static int32_t get_offset_of_m_SortNeeded_6() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___m_SortNeeded_6)); }
	inline Dictionary_2_t3280968592 * get_m_SortNeeded_6() const { return ___m_SortNeeded_6; }
	inline Dictionary_2_t3280968592 ** get_address_of_m_SortNeeded_6() { return &___m_SortNeeded_6; }
	inline void set_m_SortNeeded_6(Dictionary_2_t3280968592 * value)
	{
		___m_SortNeeded_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortNeeded_6), value);
	}

	inline static int32_t get_offset_of_m_ComponentsDefaultState_7() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___m_ComponentsDefaultState_7)); }
	inline List_1_t2297852916 * get_m_ComponentsDefaultState_7() const { return ___m_ComponentsDefaultState_7; }
	inline List_1_t2297852916 ** get_address_of_m_ComponentsDefaultState_7() { return &___m_ComponentsDefaultState_7; }
	inline void set_m_ComponentsDefaultState_7(List_1_t2297852916 * value)
	{
		___m_ComponentsDefaultState_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsDefaultState_7), value);
	}

	inline static int32_t get_offset_of_m_TempColliders_8() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537, ___m_TempColliders_8)); }
	inline List_1_t3245421752 * get_m_TempColliders_8() const { return ___m_TempColliders_8; }
	inline List_1_t3245421752 ** get_address_of_m_TempColliders_8() { return &___m_TempColliders_8; }
	inline void set_m_TempColliders_8(List_1_t3245421752 * value)
	{
		___m_TempColliders_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempColliders_8), value);
	}
};

struct VolumeManager_t1188818537_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.VolumeManager UnityEngine.Experimental.Rendering.VolumeManager::s_Instance
	VolumeManager_t1188818537 * ___s_Instance_0;
	// System.Func`2<System.Type,System.Boolean> UnityEngine.Experimental.Rendering.VolumeManager::<>f__am$cache0
	Func_2_t561252955 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537_StaticFields, ___s_Instance_0)); }
	inline VolumeManager_t1188818537 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline VolumeManager_t1188818537 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(VolumeManager_t1188818537 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(VolumeManager_t1188818537_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Func_2_t561252955 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Func_2_t561252955 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Func_2_t561252955 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEMANAGER_T1188818537_H
#ifndef VOLUMEPARAMETER_T1867366735_H
#define VOLUMEPARAMETER_T1867366735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter
struct  VolumeParameter_t1867366735  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.VolumeParameter::m_OverrideState
	bool ___m_OverrideState_1;

public:
	inline static int32_t get_offset_of_m_OverrideState_1() { return static_cast<int32_t>(offsetof(VolumeParameter_t1867366735, ___m_OverrideState_1)); }
	inline bool get_m_OverrideState_1() const { return ___m_OverrideState_1; }
	inline bool* get_address_of_m_OverrideState_1() { return &___m_OverrideState_1; }
	inline void set_m_OverrideState_1(bool value)
	{
		___m_OverrideState_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_T1867366735_H
#ifndef VOLUMESTACK_T1960641391_H
#define VOLUMESTACK_T1960641391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeStack
struct  VolumeStack_t1960641391  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Experimental.Rendering.VolumeComponent> UnityEngine.Experimental.Rendering.VolumeStack::components
	Dictionary_2_t3270125238 * ___components_0;

public:
	inline static int32_t get_offset_of_components_0() { return static_cast<int32_t>(offsetof(VolumeStack_t1960641391, ___components_0)); }
	inline Dictionary_2_t3270125238 * get_components_0() const { return ___components_0; }
	inline Dictionary_2_t3270125238 ** get_address_of_components_0() { return &___components_0; }
	inline void set_components_0(Dictionary_2_t3270125238 * value)
	{
		___components_0 = value;
		Il2CppCodeGenWriteBarrier((&___components_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMESTACK_T1960641391_H
#ifndef U24ARRAYTYPEU3D12_T2488454198_H
#define U24ARRAYTYPEU3D12_T2488454198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454198 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454198__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454198_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef U24ARRAYTYPEU3D20_T1702832646_H
#define U24ARRAYTYPEU3D20_T1702832646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832646 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832646__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832646_H
#ifndef U24ARRAYTYPEU3D96_T2896897884_H
#define U24ARRAYTYPEU3D96_T2896897884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=96
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D96_t2896897884 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D96_t2896897884__padding[96];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D96_T2896897884_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PLATFORM_T1698302846_H
#define PLATFORM_T1698302846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform
struct  Platform_t1698302846  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Android.Platform::m_CurrentActivity
	AndroidJavaObject_t4131667876 * ___m_CurrentActivity_0;
	// UnityEngine.AndroidJavaClass UnityEngine.Advertisements.Android.Platform::m_UnityAds
	AndroidJavaClass_t32045322 * ___m_UnityAds_1;
	// UnityEngine.Advertisements.CallbackExecutor UnityEngine.Advertisements.Android.Platform::m_CallbackExecutor
	CallbackExecutor_t363496179 * ___m_CallbackExecutor_2;
	// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs> UnityEngine.Advertisements.Android.Platform::OnReady
	EventHandler_1_t2768214265 * ___OnReady_3;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.Android.Platform::OnStart
	EventHandler_1_t2215985868 * ___OnStart_4;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Android.Platform::OnFinish
	EventHandler_1_t908338235 * ___OnFinish_5;
	// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs> UnityEngine.Advertisements.Android.Platform::OnError
	EventHandler_1_t177306446 * ___OnError_6;

public:
	inline static int32_t get_offset_of_m_CurrentActivity_0() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___m_CurrentActivity_0)); }
	inline AndroidJavaObject_t4131667876 * get_m_CurrentActivity_0() const { return ___m_CurrentActivity_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_CurrentActivity_0() { return &___m_CurrentActivity_0; }
	inline void set_m_CurrentActivity_0(AndroidJavaObject_t4131667876 * value)
	{
		___m_CurrentActivity_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentActivity_0), value);
	}

	inline static int32_t get_offset_of_m_UnityAds_1() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___m_UnityAds_1)); }
	inline AndroidJavaClass_t32045322 * get_m_UnityAds_1() const { return ___m_UnityAds_1; }
	inline AndroidJavaClass_t32045322 ** get_address_of_m_UnityAds_1() { return &___m_UnityAds_1; }
	inline void set_m_UnityAds_1(AndroidJavaClass_t32045322 * value)
	{
		___m_UnityAds_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnityAds_1), value);
	}

	inline static int32_t get_offset_of_m_CallbackExecutor_2() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___m_CallbackExecutor_2)); }
	inline CallbackExecutor_t363496179 * get_m_CallbackExecutor_2() const { return ___m_CallbackExecutor_2; }
	inline CallbackExecutor_t363496179 ** get_address_of_m_CallbackExecutor_2() { return &___m_CallbackExecutor_2; }
	inline void set_m_CallbackExecutor_2(CallbackExecutor_t363496179 * value)
	{
		___m_CallbackExecutor_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CallbackExecutor_2), value);
	}

	inline static int32_t get_offset_of_OnReady_3() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___OnReady_3)); }
	inline EventHandler_1_t2768214265 * get_OnReady_3() const { return ___OnReady_3; }
	inline EventHandler_1_t2768214265 ** get_address_of_OnReady_3() { return &___OnReady_3; }
	inline void set_OnReady_3(EventHandler_1_t2768214265 * value)
	{
		___OnReady_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnReady_3), value);
	}

	inline static int32_t get_offset_of_OnStart_4() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___OnStart_4)); }
	inline EventHandler_1_t2215985868 * get_OnStart_4() const { return ___OnStart_4; }
	inline EventHandler_1_t2215985868 ** get_address_of_OnStart_4() { return &___OnStart_4; }
	inline void set_OnStart_4(EventHandler_1_t2215985868 * value)
	{
		___OnStart_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_4), value);
	}

	inline static int32_t get_offset_of_OnFinish_5() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___OnFinish_5)); }
	inline EventHandler_1_t908338235 * get_OnFinish_5() const { return ___OnFinish_5; }
	inline EventHandler_1_t908338235 ** get_address_of_OnFinish_5() { return &___OnFinish_5; }
	inline void set_OnFinish_5(EventHandler_1_t908338235 * value)
	{
		___OnFinish_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_5), value);
	}

	inline static int32_t get_offset_of_OnError_6() { return static_cast<int32_t>(offsetof(Platform_t1698302846, ___OnError_6)); }
	inline EventHandler_1_t177306446 * get_OnError_6() const { return ___OnError_6; }
	inline EventHandler_1_t177306446 ** get_address_of_OnError_6() { return &___OnError_6; }
	inline void set_OnError_6(EventHandler_1_t177306446 * value)
	{
		___OnError_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T1698302846_H
#ifndef ERROREVENTARGS_T2253147013_H
#define ERROREVENTARGS_T2253147013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ErrorEventArgs
struct  ErrorEventArgs_t2253147013  : public EventArgs_t3591816995
{
public:
	// System.Int64 UnityEngine.Advertisements.ErrorEventArgs::<error>k__BackingField
	int64_t ___U3CerrorU3Ek__BackingField_1;
	// System.String UnityEngine.Advertisements.ErrorEventArgs::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t2253147013, ___U3CerrorU3Ek__BackingField_1)); }
	inline int64_t get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(int64_t value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t2253147013, ___U3CmessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_2() const { return ___U3CmessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_2() { return &___U3CmessageU3Ek__BackingField_2; }
	inline void set_U3CmessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T2253147013_H
#ifndef READYEVENTARGS_T549087536_H
#define READYEVENTARGS_T549087536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ReadyEventArgs
struct  ReadyEventArgs_t549087536  : public EventArgs_t3591816995
{
public:
	// System.String UnityEngine.Advertisements.ReadyEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReadyEventArgs_t549087536, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READYEVENTARGS_T549087536_H
#ifndef JSONARRAY_T3985338818_H
#define JSONARRAY_T3985338818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.JsonArray
struct  JsonArray_t3985338818  : public List_1_t257213610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T3985338818_H
#ifndef STARTEVENTARGS_T4291826435_H
#define STARTEVENTARGS_T4291826435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.StartEventArgs
struct  StartEventArgs_t4291826435  : public EventArgs_t3591816995
{
public:
	// System.String UnityEngine.Advertisements.StartEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StartEventArgs_t4291826435, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTEVENTARGS_T4291826435_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef FRUSTUM_T2039891733_H
#define FRUSTUM_T2039891733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.Frustum
struct  Frustum_t2039891733 
{
public:
	// UnityEngine.Plane[] UnityEngine.Experimental.Rendering.Frustum::planes
	PlaneU5BU5D_t3656189108* ___planes_0;
	// UnityEngine.Vector3[] UnityEngine.Experimental.Rendering.Frustum::corners
	Vector3U5BU5D_t1718750761* ___corners_1;

public:
	inline static int32_t get_offset_of_planes_0() { return static_cast<int32_t>(offsetof(Frustum_t2039891733, ___planes_0)); }
	inline PlaneU5BU5D_t3656189108* get_planes_0() const { return ___planes_0; }
	inline PlaneU5BU5D_t3656189108** get_address_of_planes_0() { return &___planes_0; }
	inline void set_planes_0(PlaneU5BU5D_t3656189108* value)
	{
		___planes_0 = value;
		Il2CppCodeGenWriteBarrier((&___planes_0), value);
	}

	inline static int32_t get_offset_of_corners_1() { return static_cast<int32_t>(offsetof(Frustum_t2039891733, ___corners_1)); }
	inline Vector3U5BU5D_t1718750761* get_corners_1() const { return ___corners_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_corners_1() { return &___corners_1; }
	inline void set_corners_1(Vector3U5BU5D_t1718750761* value)
	{
		___corners_1 = value;
		Il2CppCodeGenWriteBarrier((&___corners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.Frustum
struct Frustum_t2039891733_marshaled_pinvoke
{
	Plane_t1000493321 * ___planes_0;
	Vector3_t3722313464 * ___corners_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.Frustum
struct Frustum_t2039891733_marshaled_com
{
	Plane_t1000493321 * ___planes_0;
	Vector3_t3722313464 * ___corners_1;
};
#endif // FRUSTUM_T2039891733_H
#ifndef SSLICEENTRY_T3166396362_H
#define SSLICEENTRY_T3166396362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.TextureCache/SSliceEntry
struct  SSliceEntry_t3166396362 
{
public:
	// System.UInt32 UnityEngine.Experimental.Rendering.TextureCache/SSliceEntry::texId
	uint32_t ___texId_0;
	// System.UInt32 UnityEngine.Experimental.Rendering.TextureCache/SSliceEntry::countLRU
	uint32_t ___countLRU_1;
	// System.UInt32 UnityEngine.Experimental.Rendering.TextureCache/SSliceEntry::sliceEntryHash
	uint32_t ___sliceEntryHash_2;

public:
	inline static int32_t get_offset_of_texId_0() { return static_cast<int32_t>(offsetof(SSliceEntry_t3166396362, ___texId_0)); }
	inline uint32_t get_texId_0() const { return ___texId_0; }
	inline uint32_t* get_address_of_texId_0() { return &___texId_0; }
	inline void set_texId_0(uint32_t value)
	{
		___texId_0 = value;
	}

	inline static int32_t get_offset_of_countLRU_1() { return static_cast<int32_t>(offsetof(SSliceEntry_t3166396362, ___countLRU_1)); }
	inline uint32_t get_countLRU_1() const { return ___countLRU_1; }
	inline uint32_t* get_address_of_countLRU_1() { return &___countLRU_1; }
	inline void set_countLRU_1(uint32_t value)
	{
		___countLRU_1 = value;
	}

	inline static int32_t get_offset_of_sliceEntryHash_2() { return static_cast<int32_t>(offsetof(SSliceEntry_t3166396362, ___sliceEntryHash_2)); }
	inline uint32_t get_sliceEntryHash_2() const { return ___sliceEntryHash_2; }
	inline uint32_t* get_address_of_sliceEntryHash_2() { return &___sliceEntryHash_2; }
	inline void set_sliceEntryHash_2(uint32_t value)
	{
		___sliceEntryHash_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLICEENTRY_T3166396362_H
#ifndef VOLUMECOMPONENTMENU_T1986383967_H
#define VOLUMECOMPONENTMENU_T1986383967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeComponentMenu
struct  VolumeComponentMenu_t1986383967  : public Attribute_t861562559
{
public:
	// System.String UnityEngine.Experimental.Rendering.VolumeComponentMenu::menu
	String_t* ___menu_0;

public:
	inline static int32_t get_offset_of_menu_0() { return static_cast<int32_t>(offsetof(VolumeComponentMenu_t1986383967, ___menu_0)); }
	inline String_t* get_menu_0() const { return ___menu_0; }
	inline String_t** get_address_of_menu_0() { return &___menu_0; }
	inline void set_menu_0(String_t* value)
	{
		___menu_0 = value;
		Il2CppCodeGenWriteBarrier((&___menu_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMECOMPONENTMENU_T1986383967_H
#ifndef VOLUMEPARAMETER_1_T2203730113_H
#define VOLUMEPARAMETER_1_T2203730113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<System.Boolean>
struct  VolumeParameter_1_t2203730113  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	bool ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t2203730113, ___m_Value_2)); }
	inline bool get_m_Value_2() const { return ___m_Value_2; }
	inline bool* get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(bool value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T2203730113_H
#ifndef VOLUMEPARAMETER_1_T762420605_H
#define VOLUMEPARAMETER_1_T762420605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<System.Int32>
struct  VolumeParameter_1_t762420605  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	int32_t ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t762420605, ___m_Value_2)); }
	inline int32_t get_m_Value_2() const { return ___m_Value_2; }
	inline int32_t* get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(int32_t value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T762420605_H
#ifndef VOLUMEPARAMETER_1_T3503708922_H
#define VOLUMEPARAMETER_1_T3503708922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<System.Single>
struct  VolumeParameter_1_t3503708922  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	float ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t3503708922, ___m_Value_2)); }
	inline float get_m_Value_2() const { return ___m_Value_2; }
	inline float* get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(float value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T3503708922_H
#ifndef VOLUMEPARAMETER_1_T4078826557_H
#define VOLUMEPARAMETER_1_T4078826557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.Cubemap>
struct  VolumeParameter_1_t4078826557  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	Cubemap_t1972384409 * ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t4078826557, ___m_Value_2)); }
	inline Cubemap_t1972384409 * get_m_Value_2() const { return ___m_Value_2; }
	inline Cubemap_t1972384409 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(Cubemap_t1972384409 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T4078826557_H
#ifndef VOLUMEPARAMETER_1_T4215329581_H
#define VOLUMEPARAMETER_1_T4215329581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.RenderTexture>
struct  VolumeParameter_1_t4215329581  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	RenderTexture_t2108887433 * ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t4215329581, ___m_Value_2)); }
	inline RenderTexture_t2108887433 * get_m_Value_2() const { return ___m_Value_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(RenderTexture_t2108887433 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T4215329581_H
#ifndef VOLUMEPARAMETER_1_T1473437555_H
#define VOLUMEPARAMETER_1_T1473437555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.Texture>
struct  VolumeParameter_1_t1473437555  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	Texture_t3661962703 * ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t1473437555, ___m_Value_2)); }
	inline Texture_t3661962703 * get_m_Value_2() const { return ___m_Value_2; }
	inline Texture_t3661962703 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(Texture_t3661962703 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T1473437555_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255373_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255373  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0
	U24ArrayTypeU3D12_t2488454198  ___U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-C38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1;
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-CDA46F1C24B92FA8375164475268E0375C6A403D
	U24ArrayTypeU3D20_t1702832646  ___U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2;
	// <PrivateImplementationDetails>/$ArrayType=96 <PrivateImplementationDetails>::$field-C5646B9DA42FF1A2BD05B36B7632EECB3A6D0472
	U24ArrayTypeU3D96_t2896897884  ___U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3;

public:
	inline static int32_t get_offset_of_U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields, ___U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0)); }
	inline U24ArrayTypeU3D12_t2488454198  get_U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0() const { return ___U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0; }
	inline U24ArrayTypeU3D12_t2488454198 * get_address_of_U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0() { return &___U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0; }
	inline void set_U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0(U24ArrayTypeU3D12_t2488454198  value)
	{
		___U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields, ___U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1() const { return ___U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1() { return &___U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1; }
	inline void set_U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields, ___U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2)); }
	inline U24ArrayTypeU3D20_t1702832646  get_U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2() const { return ___U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2; }
	inline U24ArrayTypeU3D20_t1702832646 * get_address_of_U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2() { return &___U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2; }
	inline void set_U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2(U24ArrayTypeU3D20_t1702832646  value)
	{
		___U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields, ___U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3)); }
	inline U24ArrayTypeU3D96_t2896897884  get_U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3() const { return ___U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3; }
	inline U24ArrayTypeU3D96_t2896897884 * get_address_of_U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3() { return &___U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3; }
	inline void set_U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3(U24ArrayTypeU3D96_t2896897884  value)
	{
		___U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255373_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef DEBUGLEVEL_T2669295423_H
#define DEBUGLEVEL_T2669295423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement/DebugLevel
struct  DebugLevel_t2669295423 
{
public:
	// System.Int32 UnityEngine.Advertisements.Advertisement/DebugLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLevel_t2669295423, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T2669295423_H
#ifndef DEBUGLEVELINTERNAL_T4213999277_H
#define DEBUGLEVELINTERNAL_T4213999277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement/DebugLevelInternal
struct  DebugLevelInternal_t4213999277 
{
public:
	// System.Int32 UnityEngine.Advertisements.Advertisement/DebugLevelInternal::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLevelInternal_t4213999277, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVELINTERNAL_T4213999277_H
#ifndef PLACEMENTSTATE_T4035359335_H
#define PLACEMENTSTATE_T4035359335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.PlacementState
struct  PlacementState_t4035359335 
{
public:
	// System.Int32 UnityEngine.Advertisements.PlacementState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlacementState_t4035359335, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTSTATE_T4035359335_H
#ifndef SHOWRESULT_T3070553623_H
#define SHOWRESULT_T3070553623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowResult
struct  ShowResult_t3070553623 
{
public:
	// System.Int32 UnityEngine.Advertisements.ShowResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShowResult_t3070553623, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWRESULT_T3070553623_H
#ifndef BOOLPARAMETER_T1742223839_H
#define BOOLPARAMETER_T1742223839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.BoolParameter
struct  BoolParameter_t1742223839  : public VolumeParameter_1_t2203730113
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLPARAMETER_T1742223839_H
#ifndef CLEARFLAG_T2207768290_H
#define CLEARFLAG_T2207768290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ClearFlag
struct  ClearFlag_t2207768290 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.ClearFlag::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClearFlag_t2207768290, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLEARFLAG_T2207768290_H
#ifndef CUBEMAPPARAMETER_T3121160090_H
#define CUBEMAPPARAMETER_T3121160090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.CubemapParameter
struct  CubemapParameter_t3121160090  : public VolumeParameter_1_t4078826557
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPPARAMETER_T3121160090_H
#ifndef FLOATPARAMETER_T2493370570_H
#define FLOATPARAMETER_T2493370570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.FloatParameter
struct  FloatParameter_t2493370570  : public VolumeParameter_1_t3503708922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARAMETER_T2493370570_H
#ifndef GEOMETRYUTILS_T1844999235_H
#define GEOMETRYUTILS_T1844999235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.GeometryUtils
struct  GeometryUtils_t1844999235  : public RuntimeObject
{
public:

public:
};

struct GeometryUtils_t1844999235_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Experimental.Rendering.GeometryUtils::FlipMatrixLHSRHS
	Matrix4x4_t1817901843  ___FlipMatrixLHSRHS_0;

public:
	inline static int32_t get_offset_of_FlipMatrixLHSRHS_0() { return static_cast<int32_t>(offsetof(GeometryUtils_t1844999235_StaticFields, ___FlipMatrixLHSRHS_0)); }
	inline Matrix4x4_t1817901843  get_FlipMatrixLHSRHS_0() const { return ___FlipMatrixLHSRHS_0; }
	inline Matrix4x4_t1817901843 * get_address_of_FlipMatrixLHSRHS_0() { return &___FlipMatrixLHSRHS_0; }
	inline void set_FlipMatrixLHSRHS_0(Matrix4x4_t1817901843  value)
	{
		___FlipMatrixLHSRHS_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOMETRYUTILS_T1844999235_H
#ifndef INTPARAMETER_T3390203283_H
#define INTPARAMETER_T3390203283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.IntParameter
struct  IntParameter_t3390203283  : public VolumeParameter_1_t762420605
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPARAMETER_T3390203283_H
#ifndef NOINTERPCLAMPEDFLOATPARAMETER_T1088071901_H
#define NOINTERPCLAMPEDFLOATPARAMETER_T1088071901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpClampedFloatParameter
struct  NoInterpClampedFloatParameter_t1088071901  : public VolumeParameter_1_t3503708922
{
public:
	// System.Single UnityEngine.Experimental.Rendering.NoInterpClampedFloatParameter::min
	float ___min_4;
	// System.Single UnityEngine.Experimental.Rendering.NoInterpClampedFloatParameter::max
	float ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(NoInterpClampedFloatParameter_t1088071901, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(NoInterpClampedFloatParameter_t1088071901, ___max_5)); }
	inline float get_max_5() const { return ___max_5; }
	inline float* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(float value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPCLAMPEDFLOATPARAMETER_T1088071901_H
#ifndef NOINTERPCLAMPEDINTPARAMETER_T3673768753_H
#define NOINTERPCLAMPEDINTPARAMETER_T3673768753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpClampedIntParameter
struct  NoInterpClampedIntParameter_t3673768753  : public VolumeParameter_1_t762420605
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.NoInterpClampedIntParameter::min
	int32_t ___min_4;
	// System.Int32 UnityEngine.Experimental.Rendering.NoInterpClampedIntParameter::max
	int32_t ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(NoInterpClampedIntParameter_t3673768753, ___min_4)); }
	inline int32_t get_min_4() const { return ___min_4; }
	inline int32_t* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(int32_t value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(NoInterpClampedIntParameter_t3673768753, ___max_5)); }
	inline int32_t get_max_5() const { return ___max_5; }
	inline int32_t* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(int32_t value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPCLAMPEDINTPARAMETER_T3673768753_H
#ifndef NOINTERPCUBEMAPPARAMETER_T2472825447_H
#define NOINTERPCUBEMAPPARAMETER_T2472825447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpCubemapParameter
struct  NoInterpCubemapParameter_t2472825447  : public VolumeParameter_1_t4078826557
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPCUBEMAPPARAMETER_T2472825447_H
#ifndef NOINTERPFLOATPARAMETER_T884567933_H
#define NOINTERPFLOATPARAMETER_T884567933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpFloatParameter
struct  NoInterpFloatParameter_t884567933  : public VolumeParameter_1_t3503708922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPFLOATPARAMETER_T884567933_H
#ifndef NOINTERPINTPARAMETER_T2802994991_H
#define NOINTERPINTPARAMETER_T2802994991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpIntParameter
struct  NoInterpIntParameter_t2802994991  : public VolumeParameter_1_t762420605
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPINTPARAMETER_T2802994991_H
#ifndef NOINTERPMAXFLOATPARAMETER_T2958801461_H
#define NOINTERPMAXFLOATPARAMETER_T2958801461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpMaxFloatParameter
struct  NoInterpMaxFloatParameter_t2958801461  : public VolumeParameter_1_t3503708922
{
public:
	// System.Single UnityEngine.Experimental.Rendering.NoInterpMaxFloatParameter::max
	float ___max_4;

public:
	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(NoInterpMaxFloatParameter_t2958801461, ___max_4)); }
	inline float get_max_4() const { return ___max_4; }
	inline float* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(float value)
	{
		___max_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPMAXFLOATPARAMETER_T2958801461_H
#ifndef NOINTERPMAXINTPARAMETER_T2970896447_H
#define NOINTERPMAXINTPARAMETER_T2970896447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpMaxIntParameter
struct  NoInterpMaxIntParameter_t2970896447  : public VolumeParameter_1_t762420605
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.NoInterpMaxIntParameter::max
	int32_t ___max_4;

public:
	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(NoInterpMaxIntParameter_t2970896447, ___max_4)); }
	inline int32_t get_max_4() const { return ___max_4; }
	inline int32_t* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(int32_t value)
	{
		___max_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPMAXINTPARAMETER_T2970896447_H
#ifndef NOINTERPMINFLOATPARAMETER_T1443953088_H
#define NOINTERPMINFLOATPARAMETER_T1443953088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpMinFloatParameter
struct  NoInterpMinFloatParameter_t1443953088  : public VolumeParameter_1_t3503708922
{
public:
	// System.Single UnityEngine.Experimental.Rendering.NoInterpMinFloatParameter::min
	float ___min_4;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(NoInterpMinFloatParameter_t1443953088, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPMINFLOATPARAMETER_T1443953088_H
#ifndef NOINTERPMININTPARAMETER_T3712115822_H
#define NOINTERPMININTPARAMETER_T3712115822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpMinIntParameter
struct  NoInterpMinIntParameter_t3712115822  : public VolumeParameter_1_t762420605
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.NoInterpMinIntParameter::min
	int32_t ___min_4;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(NoInterpMinIntParameter_t3712115822, ___min_4)); }
	inline int32_t get_min_4() const { return ___min_4; }
	inline int32_t* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(int32_t value)
	{
		___min_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPMININTPARAMETER_T3712115822_H
#ifndef NOINTERPRENDERTEXTUREPARAMETER_T1971206433_H
#define NOINTERPRENDERTEXTUREPARAMETER_T1971206433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpRenderTextureParameter
struct  NoInterpRenderTextureParameter_t1971206433  : public VolumeParameter_1_t4215329581
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPRENDERTEXTUREPARAMETER_T1971206433_H
#ifndef NOINTERPTEXTUREPARAMETER_T3361413952_H
#define NOINTERPTEXTUREPARAMETER_T3361413952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpTextureParameter
struct  NoInterpTextureParameter_t3361413952  : public VolumeParameter_1_t1473437555
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPTEXTUREPARAMETER_T3361413952_H
#ifndef ORIENTEDBBOX_T4009157433_H
#define ORIENTEDBBOX_T4009157433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.OrientedBBox
struct  OrientedBBox_t4009157433 
{
public:
	// UnityEngine.Vector3 UnityEngine.Experimental.Rendering.OrientedBBox::right
	Vector3_t3722313464  ___right_0;
	// System.Single UnityEngine.Experimental.Rendering.OrientedBBox::extentX
	float ___extentX_1;
	// UnityEngine.Vector3 UnityEngine.Experimental.Rendering.OrientedBBox::up
	Vector3_t3722313464  ___up_2;
	// System.Single UnityEngine.Experimental.Rendering.OrientedBBox::extentY
	float ___extentY_3;
	// UnityEngine.Vector3 UnityEngine.Experimental.Rendering.OrientedBBox::center
	Vector3_t3722313464  ___center_4;
	// System.Single UnityEngine.Experimental.Rendering.OrientedBBox::extentZ
	float ___extentZ_5;

public:
	inline static int32_t get_offset_of_right_0() { return static_cast<int32_t>(offsetof(OrientedBBox_t4009157433, ___right_0)); }
	inline Vector3_t3722313464  get_right_0() const { return ___right_0; }
	inline Vector3_t3722313464 * get_address_of_right_0() { return &___right_0; }
	inline void set_right_0(Vector3_t3722313464  value)
	{
		___right_0 = value;
	}

	inline static int32_t get_offset_of_extentX_1() { return static_cast<int32_t>(offsetof(OrientedBBox_t4009157433, ___extentX_1)); }
	inline float get_extentX_1() const { return ___extentX_1; }
	inline float* get_address_of_extentX_1() { return &___extentX_1; }
	inline void set_extentX_1(float value)
	{
		___extentX_1 = value;
	}

	inline static int32_t get_offset_of_up_2() { return static_cast<int32_t>(offsetof(OrientedBBox_t4009157433, ___up_2)); }
	inline Vector3_t3722313464  get_up_2() const { return ___up_2; }
	inline Vector3_t3722313464 * get_address_of_up_2() { return &___up_2; }
	inline void set_up_2(Vector3_t3722313464  value)
	{
		___up_2 = value;
	}

	inline static int32_t get_offset_of_extentY_3() { return static_cast<int32_t>(offsetof(OrientedBBox_t4009157433, ___extentY_3)); }
	inline float get_extentY_3() const { return ___extentY_3; }
	inline float* get_address_of_extentY_3() { return &___extentY_3; }
	inline void set_extentY_3(float value)
	{
		___extentY_3 = value;
	}

	inline static int32_t get_offset_of_center_4() { return static_cast<int32_t>(offsetof(OrientedBBox_t4009157433, ___center_4)); }
	inline Vector3_t3722313464  get_center_4() const { return ___center_4; }
	inline Vector3_t3722313464 * get_address_of_center_4() { return &___center_4; }
	inline void set_center_4(Vector3_t3722313464  value)
	{
		___center_4 = value;
	}

	inline static int32_t get_offset_of_extentZ_5() { return static_cast<int32_t>(offsetof(OrientedBBox_t4009157433, ___extentZ_5)); }
	inline float get_extentZ_5() const { return ___extentZ_5; }
	inline float* get_address_of_extentZ_5() { return &___extentZ_5; }
	inline void set_extentZ_5(float value)
	{
		___extentZ_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBBOX_T4009157433_H
#ifndef RENDERTEXTUREPARAMETER_T351824372_H
#define RENDERTEXTUREPARAMETER_T351824372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderTextureParameter
struct  RenderTextureParameter_t351824372  : public VolumeParameter_1_t4215329581
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREPARAMETER_T351824372_H
#ifndef TEXTUREPARAMETER_T4223009474_H
#define TEXTUREPARAMETER_T4223009474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.TextureParameter
struct  TextureParameter_t4223009474  : public VolumeParameter_1_t1473437555
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPARAMETER_T4223009474_H
#ifndef VOLUMEPARAMETER_1_T367161176_H
#define VOLUMEPARAMETER_1_T367161176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.Color>
struct  VolumeParameter_1_t367161176  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	Color_t2555686324  ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t367161176, ___m_Value_2)); }
	inline Color_t2555686324  get_m_Value_2() const { return ___m_Value_2; }
	inline Color_t2555686324 * get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(Color_t2555686324  value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T367161176_H
#ifndef VOLUMEPARAMETER_1_T4262671671_H
#define VOLUMEPARAMETER_1_T4262671671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.Vector2>
struct  VolumeParameter_1_t4262671671  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	Vector2_t2156229523  ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t4262671671, ___m_Value_2)); }
	inline Vector2_t2156229523  get_m_Value_2() const { return ___m_Value_2; }
	inline Vector2_t2156229523 * get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(Vector2_t2156229523  value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T4262671671_H
#ifndef VOLUMEPARAMETER_1_T1533788316_H
#define VOLUMEPARAMETER_1_T1533788316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.Vector3>
struct  VolumeParameter_1_t1533788316  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	Vector3_t3722313464  ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t1533788316, ___m_Value_2)); }
	inline Vector3_t3722313464  get_m_Value_2() const { return ___m_Value_2; }
	inline Vector3_t3722313464 * get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(Vector3_t3722313464  value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T1533788316_H
#ifndef VOLUMEPARAMETER_1_T1130503789_H
#define VOLUMEPARAMETER_1_T1130503789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeParameter`1<UnityEngine.Vector4>
struct  VolumeParameter_1_t1130503789  : public VolumeParameter_t1867366735
{
public:
	// T UnityEngine.Experimental.Rendering.VolumeParameter`1::m_Value
	Vector4_t3319028937  ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(VolumeParameter_1_t1130503789, ___m_Value_2)); }
	inline Vector4_t3319028937  get_m_Value_2() const { return ___m_Value_2; }
	inline Vector4_t3319028937 * get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(Vector4_t3319028937  value)
	{
		___m_Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPARAMETER_1_T1130503789_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ADVERTISEMENT_T842671397_H
#define ADVERTISEMENT_T842671397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement
struct  Advertisement_t842671397  : public RuntimeObject
{
public:

public:
};

struct Advertisement_t842671397_StaticFields
{
public:
	// System.Boolean UnityEngine.Advertisements.Advertisement::s_Initialized
	bool ___s_Initialized_0;
	// UnityEngine.Advertisements.IPlatform UnityEngine.Advertisements.Advertisement::s_Platform
	RuntimeObject* ___s_Platform_1;
	// System.Boolean UnityEngine.Advertisements.Advertisement::s_EditorSupportedPlatform
	bool ___s_EditorSupportedPlatform_2;
	// System.Boolean UnityEngine.Advertisements.Advertisement::s_Showing
	bool ___s_Showing_3;
	// UnityEngine.Advertisements.Advertisement/DebugLevelInternal UnityEngine.Advertisements.Advertisement::s_DebugLevel
	int32_t ___s_DebugLevel_4;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.Advertisement::<>f__am$cache0
	EventHandler_1_t2215985868 * ___U3CU3Ef__amU24cache0_5;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Advertisement::<>f__am$cache1
	EventHandler_1_t908338235 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_s_Initialized_0() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___s_Initialized_0)); }
	inline bool get_s_Initialized_0() const { return ___s_Initialized_0; }
	inline bool* get_address_of_s_Initialized_0() { return &___s_Initialized_0; }
	inline void set_s_Initialized_0(bool value)
	{
		___s_Initialized_0 = value;
	}

	inline static int32_t get_offset_of_s_Platform_1() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___s_Platform_1)); }
	inline RuntimeObject* get_s_Platform_1() const { return ___s_Platform_1; }
	inline RuntimeObject** get_address_of_s_Platform_1() { return &___s_Platform_1; }
	inline void set_s_Platform_1(RuntimeObject* value)
	{
		___s_Platform_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Platform_1), value);
	}

	inline static int32_t get_offset_of_s_EditorSupportedPlatform_2() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___s_EditorSupportedPlatform_2)); }
	inline bool get_s_EditorSupportedPlatform_2() const { return ___s_EditorSupportedPlatform_2; }
	inline bool* get_address_of_s_EditorSupportedPlatform_2() { return &___s_EditorSupportedPlatform_2; }
	inline void set_s_EditorSupportedPlatform_2(bool value)
	{
		___s_EditorSupportedPlatform_2 = value;
	}

	inline static int32_t get_offset_of_s_Showing_3() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___s_Showing_3)); }
	inline bool get_s_Showing_3() const { return ___s_Showing_3; }
	inline bool* get_address_of_s_Showing_3() { return &___s_Showing_3; }
	inline void set_s_Showing_3(bool value)
	{
		___s_Showing_3 = value;
	}

	inline static int32_t get_offset_of_s_DebugLevel_4() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___s_DebugLevel_4)); }
	inline int32_t get_s_DebugLevel_4() const { return ___s_DebugLevel_4; }
	inline int32_t* get_address_of_s_DebugLevel_4() { return &___s_DebugLevel_4; }
	inline void set_s_DebugLevel_4(int32_t value)
	{
		___s_DebugLevel_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline EventHandler_1_t2215985868 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline EventHandler_1_t2215985868 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(EventHandler_1_t2215985868 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(Advertisement_t842671397_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline EventHandler_1_t908338235 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline EventHandler_1_t908338235 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(EventHandler_1_t908338235 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVERTISEMENT_T842671397_H
#ifndef U3CONUNITYADSFINISHU3EC__ANONSTOREY3_T2653194732_H
#define U3CONUNITYADSFINISHU3EC__ANONSTOREY3_T2653194732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey3
struct  U3ConUnityAdsFinishU3Ec__AnonStorey3_t2653194732  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey3::showResult
	int32_t ___showResult_0;
	// UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey2 UnityEngine.Advertisements.Android.Platform/<onUnityAdsFinish>c__AnonStorey3::<>f__ref$2
	U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_showResult_0() { return static_cast<int32_t>(offsetof(U3ConUnityAdsFinishU3Ec__AnonStorey3_t2653194732, ___showResult_0)); }
	inline int32_t get_showResult_0() const { return ___showResult_0; }
	inline int32_t* get_address_of_showResult_0() { return &___showResult_0; }
	inline void set_showResult_0(int32_t value)
	{
		___showResult_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3ConUnityAdsFinishU3Ec__AnonStorey3_t2653194732, ___U3CU3Ef__refU242_1)); }
	inline U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONUNITYADSFINISHU3EC__ANONSTOREY3_T2653194732_H
#ifndef FINISHEVENTARGS_T2984178802_H
#define FINISHEVENTARGS_T2984178802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.FinishEventArgs
struct  FinishEventArgs_t2984178802  : public EventArgs_t3591816995
{
public:
	// System.String UnityEngine.Advertisements.FinishEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;
	// UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.FinishEventArgs::<showResult>k__BackingField
	int32_t ___U3CshowResultU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FinishEventArgs_t2984178802, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CshowResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FinishEventArgs_t2984178802, ___U3CshowResultU3Ek__BackingField_2)); }
	inline int32_t get_U3CshowResultU3Ek__BackingField_2() const { return ___U3CshowResultU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CshowResultU3Ek__BackingField_2() { return &___U3CshowResultU3Ek__BackingField_2; }
	inline void set_U3CshowResultU3Ek__BackingField_2(int32_t value)
	{
		___U3CshowResultU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINISHEVENTARGS_T2984178802_H
#ifndef U3CUNITYADSDIDFINISHU3EC__ANONSTOREY4_T3126029549_H
#define U3CUNITYADSDIDFINISHU3EC__ANONSTOREY4_T3126029549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey4
struct  U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3126029549  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey4::showResult
	int32_t ___showResult_0;
	// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3 UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey4::<>f__ref$3
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_showResult_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3126029549, ___showResult_0)); }
	inline int32_t get_showResult_0() const { return ___showResult_0; }
	inline int32_t* get_address_of_showResult_0() { return &___showResult_0; }
	inline void set_showResult_0(int32_t value)
	{
		___showResult_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3126029549, ___U3CU3Ef__refU243_1)); }
	inline U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDFINISHU3EC__ANONSTOREY4_T3126029549_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CLAMPEDFLOATPARAMETER_T3036544206_H
#define CLAMPEDFLOATPARAMETER_T3036544206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ClampedFloatParameter
struct  ClampedFloatParameter_t3036544206  : public FloatParameter_t2493370570
{
public:
	// System.Single UnityEngine.Experimental.Rendering.ClampedFloatParameter::min
	float ___min_4;
	// System.Single UnityEngine.Experimental.Rendering.ClampedFloatParameter::max
	float ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(ClampedFloatParameter_t3036544206, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(ClampedFloatParameter_t3036544206, ___max_5)); }
	inline float get_max_5() const { return ___max_5; }
	inline float* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(float value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAMPEDFLOATPARAMETER_T3036544206_H
#ifndef CLAMPEDINTPARAMETER_T2484235189_H
#define CLAMPEDINTPARAMETER_T2484235189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ClampedIntParameter
struct  ClampedIntParameter_t2484235189  : public IntParameter_t3390203283
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.ClampedIntParameter::min
	int32_t ___min_4;
	// System.Int32 UnityEngine.Experimental.Rendering.ClampedIntParameter::max
	int32_t ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(ClampedIntParameter_t2484235189, ___min_4)); }
	inline int32_t get_min_4() const { return ___min_4; }
	inline int32_t* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(int32_t value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(ClampedIntParameter_t2484235189, ___max_5)); }
	inline int32_t get_max_5() const { return ___max_5; }
	inline int32_t* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(int32_t value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAMPEDINTPARAMETER_T2484235189_H
#ifndef COLORPARAMETER_T1856347123_H
#define COLORPARAMETER_T1856347123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ColorParameter
struct  ColorParameter_t1856347123  : public VolumeParameter_1_t367161176
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.ColorParameter::hdr
	bool ___hdr_4;
	// System.Boolean UnityEngine.Experimental.Rendering.ColorParameter::showAlpha
	bool ___showAlpha_5;
	// System.Boolean UnityEngine.Experimental.Rendering.ColorParameter::showEyeDropper
	bool ___showEyeDropper_6;

public:
	inline static int32_t get_offset_of_hdr_4() { return static_cast<int32_t>(offsetof(ColorParameter_t1856347123, ___hdr_4)); }
	inline bool get_hdr_4() const { return ___hdr_4; }
	inline bool* get_address_of_hdr_4() { return &___hdr_4; }
	inline void set_hdr_4(bool value)
	{
		___hdr_4 = value;
	}

	inline static int32_t get_offset_of_showAlpha_5() { return static_cast<int32_t>(offsetof(ColorParameter_t1856347123, ___showAlpha_5)); }
	inline bool get_showAlpha_5() const { return ___showAlpha_5; }
	inline bool* get_address_of_showAlpha_5() { return &___showAlpha_5; }
	inline void set_showAlpha_5(bool value)
	{
		___showAlpha_5 = value;
	}

	inline static int32_t get_offset_of_showEyeDropper_6() { return static_cast<int32_t>(offsetof(ColorParameter_t1856347123, ___showEyeDropper_6)); }
	inline bool get_showEyeDropper_6() const { return ___showEyeDropper_6; }
	inline bool* get_address_of_showEyeDropper_6() { return &___showEyeDropper_6; }
	inline void set_showEyeDropper_6(bool value)
	{
		___showEyeDropper_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPARAMETER_T1856347123_H
#ifndef FLOATRANGEPARAMETER_T198971241_H
#define FLOATRANGEPARAMETER_T198971241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.FloatRangeParameter
struct  FloatRangeParameter_t198971241  : public VolumeParameter_1_t4262671671
{
public:
	// System.Single UnityEngine.Experimental.Rendering.FloatRangeParameter::min
	float ___min_4;
	// System.Single UnityEngine.Experimental.Rendering.FloatRangeParameter::max
	float ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(FloatRangeParameter_t198971241, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(FloatRangeParameter_t198971241, ___max_5)); }
	inline float get_max_5() const { return ___max_5; }
	inline float* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(float value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATRANGEPARAMETER_T198971241_H
#ifndef MAXFLOATPARAMETER_T3905301792_H
#define MAXFLOATPARAMETER_T3905301792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.MaxFloatParameter
struct  MaxFloatParameter_t3905301792  : public FloatParameter_t2493370570
{
public:
	// System.Single UnityEngine.Experimental.Rendering.MaxFloatParameter::max
	float ___max_4;

public:
	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(MaxFloatParameter_t3905301792, ___max_4)); }
	inline float get_max_4() const { return ___max_4; }
	inline float* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(float value)
	{
		___max_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXFLOATPARAMETER_T3905301792_H
#ifndef MAXINTPARAMETER_T954807667_H
#define MAXINTPARAMETER_T954807667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.MaxIntParameter
struct  MaxIntParameter_t954807667  : public IntParameter_t3390203283
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.MaxIntParameter::max
	int32_t ___max_4;

public:
	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(MaxIntParameter_t954807667, ___max_4)); }
	inline int32_t get_max_4() const { return ___max_4; }
	inline int32_t* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(int32_t value)
	{
		___max_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXINTPARAMETER_T954807667_H
#ifndef MINFLOATPARAMETER_T3988033055_H
#define MINFLOATPARAMETER_T3988033055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.MinFloatParameter
struct  MinFloatParameter_t3988033055  : public FloatParameter_t2493370570
{
public:
	// System.Single UnityEngine.Experimental.Rendering.MinFloatParameter::min
	float ___min_4;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(MinFloatParameter_t3988033055, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINFLOATPARAMETER_T3988033055_H
#ifndef MININTPARAMETER_T3115283297_H
#define MININTPARAMETER_T3115283297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.MinIntParameter
struct  MinIntParameter_t3115283297  : public IntParameter_t3390203283
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.MinIntParameter::min
	int32_t ___min_4;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(MinIntParameter_t3115283297, ___min_4)); }
	inline int32_t get_min_4() const { return ___min_4; }
	inline int32_t* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(int32_t value)
	{
		___min_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MININTPARAMETER_T3115283297_H
#ifndef NOINTERPCOLORPARAMETER_T3141422378_H
#define NOINTERPCOLORPARAMETER_T3141422378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpColorParameter
struct  NoInterpColorParameter_t3141422378  : public VolumeParameter_1_t367161176
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.NoInterpColorParameter::hdr
	bool ___hdr_4;
	// System.Boolean UnityEngine.Experimental.Rendering.NoInterpColorParameter::showAlpha
	bool ___showAlpha_5;
	// System.Boolean UnityEngine.Experimental.Rendering.NoInterpColorParameter::showEyeDropper
	bool ___showEyeDropper_6;

public:
	inline static int32_t get_offset_of_hdr_4() { return static_cast<int32_t>(offsetof(NoInterpColorParameter_t3141422378, ___hdr_4)); }
	inline bool get_hdr_4() const { return ___hdr_4; }
	inline bool* get_address_of_hdr_4() { return &___hdr_4; }
	inline void set_hdr_4(bool value)
	{
		___hdr_4 = value;
	}

	inline static int32_t get_offset_of_showAlpha_5() { return static_cast<int32_t>(offsetof(NoInterpColorParameter_t3141422378, ___showAlpha_5)); }
	inline bool get_showAlpha_5() const { return ___showAlpha_5; }
	inline bool* get_address_of_showAlpha_5() { return &___showAlpha_5; }
	inline void set_showAlpha_5(bool value)
	{
		___showAlpha_5 = value;
	}

	inline static int32_t get_offset_of_showEyeDropper_6() { return static_cast<int32_t>(offsetof(NoInterpColorParameter_t3141422378, ___showEyeDropper_6)); }
	inline bool get_showEyeDropper_6() const { return ___showEyeDropper_6; }
	inline bool* get_address_of_showEyeDropper_6() { return &___showEyeDropper_6; }
	inline void set_showEyeDropper_6(bool value)
	{
		___showEyeDropper_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPCOLORPARAMETER_T3141422378_H
#ifndef NOINTERPFLOATRANGEPARAMETER_T2225766086_H
#define NOINTERPFLOATRANGEPARAMETER_T2225766086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpFloatRangeParameter
struct  NoInterpFloatRangeParameter_t2225766086  : public VolumeParameter_1_t4262671671
{
public:
	// System.Single UnityEngine.Experimental.Rendering.NoInterpFloatRangeParameter::min
	float ___min_4;
	// System.Single UnityEngine.Experimental.Rendering.NoInterpFloatRangeParameter::max
	float ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(NoInterpFloatRangeParameter_t2225766086, ___min_4)); }
	inline float get_min_4() const { return ___min_4; }
	inline float* get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(float value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(NoInterpFloatRangeParameter_t2225766086, ___max_5)); }
	inline float get_max_5() const { return ___max_5; }
	inline float* get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(float value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPFLOATRANGEPARAMETER_T2225766086_H
#ifndef NOINTERPVECTOR2PARAMETER_T2694188167_H
#define NOINTERPVECTOR2PARAMETER_T2694188167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpVector2Parameter
struct  NoInterpVector2Parameter_t2694188167  : public VolumeParameter_1_t4262671671
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPVECTOR2PARAMETER_T2694188167_H
#ifndef NOINTERPVECTOR3PARAMETER_T2195262599_H
#define NOINTERPVECTOR3PARAMETER_T2195262599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpVector3Parameter
struct  NoInterpVector3Parameter_t2195262599  : public VolumeParameter_1_t1533788316
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPVECTOR3PARAMETER_T2195262599_H
#ifndef NOINTERPVECTOR4PARAMETER_T2560625799_H
#define NOINTERPVECTOR4PARAMETER_T2560625799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.NoInterpVector4Parameter
struct  NoInterpVector4Parameter_t2560625799  : public VolumeParameter_1_t1130503789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOINTERPVECTOR4PARAMETER_T2560625799_H
#ifndef VECTOR2PARAMETER_T2379770789_H
#define VECTOR2PARAMETER_T2379770789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.Vector2Parameter
struct  Vector2Parameter_t2379770789  : public VolumeParameter_1_t4262671671
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PARAMETER_T2379770789_H
#ifndef VECTOR3PARAMETER_T2442619813_H
#define VECTOR3PARAMETER_T2442619813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.Vector3Parameter
struct  Vector3Parameter_t2442619813  : public VolumeParameter_1_t1533788316
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3PARAMETER_T2442619813_H
#ifndef VECTOR4PARAMETER_T2245159845_H
#define VECTOR4PARAMETER_T2245159845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.Vector4Parameter
struct  Vector4Parameter_t2245159845  : public VolumeParameter_1_t1130503789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4PARAMETER_T2245159845_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef UNITYADSDIDERROR_T1993223595_H
#define UNITYADSDIDERROR_T1993223595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/unityAdsDidError
struct  unityAdsDidError_t1993223595  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSDIDERROR_T1993223595_H
#ifndef UNITYADSDIDFINISH_T3747416149_H
#define UNITYADSDIDFINISH_T3747416149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/unityAdsDidFinish
struct  unityAdsDidFinish_t3747416149  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSDIDFINISH_T3747416149_H
#ifndef UNITYADSDIDSTART_T1058412932_H
#define UNITYADSDIDSTART_T1058412932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart
struct  unityAdsDidStart_t1058412932  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSDIDSTART_T1058412932_H
#ifndef UNITYADSREADY_T96934738_H
#define UNITYADSREADY_T96934738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform/unityAdsReady
struct  unityAdsReady_t96934738  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSREADY_T96934738_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef VOLUMECOMPONENT_T825778174_H
#define VOLUMECOMPONENT_T825778174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeComponent
struct  VolumeComponent_t825778174  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.VolumeComponent::active
	bool ___active_4;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Experimental.Rendering.VolumeParameter> UnityEngine.Experimental.Rendering.VolumeComponent::<parameters>k__BackingField
	ReadOnlyCollection_1_t3079943022 * ___U3CparametersU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(VolumeComponent_t825778174, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VolumeComponent_t825778174, ___U3CparametersU3Ek__BackingField_5)); }
	inline ReadOnlyCollection_1_t3079943022 * get_U3CparametersU3Ek__BackingField_5() const { return ___U3CparametersU3Ek__BackingField_5; }
	inline ReadOnlyCollection_1_t3079943022 ** get_address_of_U3CparametersU3Ek__BackingField_5() { return &___U3CparametersU3Ek__BackingField_5; }
	inline void set_U3CparametersU3Ek__BackingField_5(ReadOnlyCollection_1_t3079943022 * value)
	{
		___U3CparametersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparametersU3Ek__BackingField_5), value);
	}
};

struct VolumeComponent_t825778174_StaticFields
{
public:
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> UnityEngine.Experimental.Rendering.VolumeComponent::<>f__am$cache0
	Func_2_t1761491126 * ___U3CU3Ef__amU24cache0_6;
	// System.Func`2<System.Reflection.FieldInfo,System.Int32> UnityEngine.Experimental.Rendering.VolumeComponent::<>f__am$cache1
	Func_2_t320181618 * ___U3CU3Ef__amU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(VolumeComponent_t825778174_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t1761491126 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t1761491126 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t1761491126 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(VolumeComponent_t825778174_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Func_2_t320181618 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Func_2_t320181618 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Func_2_t320181618 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMECOMPONENT_T825778174_H
#ifndef VOLUMEPROFILE_T1487161726_H
#define VOLUMEPROFILE_T1487161726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.VolumeProfile
struct  VolumeProfile_t1487161726  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VolumeComponent> UnityEngine.Experimental.Rendering.VolumeProfile::components
	List_1_t2297852916 * ___components_4;
	// System.Boolean UnityEngine.Experimental.Rendering.VolumeProfile::isDirty
	bool ___isDirty_5;

public:
	inline static int32_t get_offset_of_components_4() { return static_cast<int32_t>(offsetof(VolumeProfile_t1487161726, ___components_4)); }
	inline List_1_t2297852916 * get_components_4() const { return ___components_4; }
	inline List_1_t2297852916 ** get_address_of_components_4() { return &___components_4; }
	inline void set_components_4(List_1_t2297852916 * value)
	{
		___components_4 = value;
		Il2CppCodeGenWriteBarrier((&___components_4), value);
	}

	inline static int32_t get_offset_of_isDirty_5() { return static_cast<int32_t>(offsetof(VolumeProfile_t1487161726, ___isDirty_5)); }
	inline bool get_isDirty_5() const { return ___isDirty_5; }
	inline bool* get_address_of_isDirty_5() { return &___isDirty_5; }
	inline void set_isDirty_5(bool value)
	{
		___isDirty_5 = value;
	}
};

struct VolumeProfile_t1487161726_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.Experimental.Rendering.VolumeComponent> UnityEngine.Experimental.Rendering.VolumeProfile::<>f__am$cache0
	Predicate_1_t1651072298 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(VolumeProfile_t1487161726_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Predicate_1_t1651072298 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Predicate_1_t1651072298 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Predicate_1_t1651072298 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMEPROFILE_T1487161726_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SCENERENDERPIPELINE_T2037314482_H
#define SCENERENDERPIPELINE_T2037314482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneRenderPipeline
struct  SceneRenderPipeline_t2037314482  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Experimental.Rendering.RenderPipelineAsset SceneRenderPipeline::renderPipelineAsset
	RenderPipelineAsset_t533890058 * ___renderPipelineAsset_4;

public:
	inline static int32_t get_offset_of_renderPipelineAsset_4() { return static_cast<int32_t>(offsetof(SceneRenderPipeline_t2037314482, ___renderPipelineAsset_4)); }
	inline RenderPipelineAsset_t533890058 * get_renderPipelineAsset_4() const { return ___renderPipelineAsset_4; }
	inline RenderPipelineAsset_t533890058 ** get_address_of_renderPipelineAsset_4() { return &___renderPipelineAsset_4; }
	inline void set_renderPipelineAsset_4(RenderPipelineAsset_t533890058 * value)
	{
		___renderPipelineAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___renderPipelineAsset_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENERENDERPIPELINE_T2037314482_H
#ifndef CALLBACKEXECUTOR_T363496179_H
#define CALLBACKEXECUTOR_T363496179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.CallbackExecutor
struct  CallbackExecutor_t363496179  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Queue`1<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>> UnityEngine.Advertisements.CallbackExecutor::s_Queue
	Queue_1_t382223268 * ___s_Queue_4;

public:
	inline static int32_t get_offset_of_s_Queue_4() { return static_cast<int32_t>(offsetof(CallbackExecutor_t363496179, ___s_Queue_4)); }
	inline Queue_1_t382223268 * get_s_Queue_4() const { return ___s_Queue_4; }
	inline Queue_1_t382223268 ** get_address_of_s_Queue_4() { return &___s_Queue_4; }
	inline void set_s_Queue_4(Queue_1_t382223268 * value)
	{
		___s_Queue_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Queue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKEXECUTOR_T363496179_H
#ifndef PLACEHOLDER_T2906495853_H
#define PLACEHOLDER_T2906495853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Placeholder
struct  Placeholder_t2906495853  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D UnityEngine.Advertisements.Editor.Placeholder::m_LandscapeTexture
	Texture2D_t3840446185 * ___m_LandscapeTexture_4;
	// UnityEngine.Texture2D UnityEngine.Advertisements.Editor.Placeholder::m_PortraitTexture
	Texture2D_t3840446185 * ___m_PortraitTexture_5;
	// System.Boolean UnityEngine.Advertisements.Editor.Placeholder::m_Showing
	bool ___m_Showing_6;
	// System.String UnityEngine.Advertisements.Editor.Placeholder::m_PlacementId
	String_t* ___m_PlacementId_7;
	// System.Boolean UnityEngine.Advertisements.Editor.Placeholder::m_AllowSkip
	bool ___m_AllowSkip_8;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Editor.Placeholder::OnFinish
	EventHandler_1_t908338235 * ___OnFinish_9;

public:
	inline static int32_t get_offset_of_m_LandscapeTexture_4() { return static_cast<int32_t>(offsetof(Placeholder_t2906495853, ___m_LandscapeTexture_4)); }
	inline Texture2D_t3840446185 * get_m_LandscapeTexture_4() const { return ___m_LandscapeTexture_4; }
	inline Texture2D_t3840446185 ** get_address_of_m_LandscapeTexture_4() { return &___m_LandscapeTexture_4; }
	inline void set_m_LandscapeTexture_4(Texture2D_t3840446185 * value)
	{
		___m_LandscapeTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LandscapeTexture_4), value);
	}

	inline static int32_t get_offset_of_m_PortraitTexture_5() { return static_cast<int32_t>(offsetof(Placeholder_t2906495853, ___m_PortraitTexture_5)); }
	inline Texture2D_t3840446185 * get_m_PortraitTexture_5() const { return ___m_PortraitTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_m_PortraitTexture_5() { return &___m_PortraitTexture_5; }
	inline void set_m_PortraitTexture_5(Texture2D_t3840446185 * value)
	{
		___m_PortraitTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PortraitTexture_5), value);
	}

	inline static int32_t get_offset_of_m_Showing_6() { return static_cast<int32_t>(offsetof(Placeholder_t2906495853, ___m_Showing_6)); }
	inline bool get_m_Showing_6() const { return ___m_Showing_6; }
	inline bool* get_address_of_m_Showing_6() { return &___m_Showing_6; }
	inline void set_m_Showing_6(bool value)
	{
		___m_Showing_6 = value;
	}

	inline static int32_t get_offset_of_m_PlacementId_7() { return static_cast<int32_t>(offsetof(Placeholder_t2906495853, ___m_PlacementId_7)); }
	inline String_t* get_m_PlacementId_7() const { return ___m_PlacementId_7; }
	inline String_t** get_address_of_m_PlacementId_7() { return &___m_PlacementId_7; }
	inline void set_m_PlacementId_7(String_t* value)
	{
		___m_PlacementId_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacementId_7), value);
	}

	inline static int32_t get_offset_of_m_AllowSkip_8() { return static_cast<int32_t>(offsetof(Placeholder_t2906495853, ___m_AllowSkip_8)); }
	inline bool get_m_AllowSkip_8() const { return ___m_AllowSkip_8; }
	inline bool* get_address_of_m_AllowSkip_8() { return &___m_AllowSkip_8; }
	inline void set_m_AllowSkip_8(bool value)
	{
		___m_AllowSkip_8 = value;
	}

	inline static int32_t get_offset_of_OnFinish_9() { return static_cast<int32_t>(offsetof(Placeholder_t2906495853, ___OnFinish_9)); }
	inline EventHandler_1_t908338235 * get_OnFinish_9() const { return ___OnFinish_9; }
	inline EventHandler_1_t908338235 ** get_address_of_OnFinish_9() { return &___OnFinish_9; }
	inline void set_OnFinish_9(EventHandler_1_t908338235 * value)
	{
		___OnFinish_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEHOLDER_T2906495853_H
#ifndef VOLUME_T3757178504_H
#define VOLUME_T3757178504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.Volume
struct  Volume_t3757178504  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.Volume::isGlobal
	bool ___isGlobal_4;
	// System.Single UnityEngine.Experimental.Rendering.Volume::priority
	float ___priority_5;
	// System.Single UnityEngine.Experimental.Rendering.Volume::blendDistance
	float ___blendDistance_6;
	// System.Single UnityEngine.Experimental.Rendering.Volume::weight
	float ___weight_7;
	// UnityEngine.Experimental.Rendering.VolumeProfile UnityEngine.Experimental.Rendering.Volume::sharedProfile
	VolumeProfile_t1487161726 * ___sharedProfile_8;
	// System.Int32 UnityEngine.Experimental.Rendering.Volume::m_PreviousLayer
	int32_t ___m_PreviousLayer_9;
	// System.Single UnityEngine.Experimental.Rendering.Volume::m_PreviousPriority
	float ___m_PreviousPriority_10;
	// UnityEngine.Experimental.Rendering.VolumeProfile UnityEngine.Experimental.Rendering.Volume::m_InternalProfile
	VolumeProfile_t1487161726 * ___m_InternalProfile_11;

public:
	inline static int32_t get_offset_of_isGlobal_4() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___isGlobal_4)); }
	inline bool get_isGlobal_4() const { return ___isGlobal_4; }
	inline bool* get_address_of_isGlobal_4() { return &___isGlobal_4; }
	inline void set_isGlobal_4(bool value)
	{
		___isGlobal_4 = value;
	}

	inline static int32_t get_offset_of_priority_5() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___priority_5)); }
	inline float get_priority_5() const { return ___priority_5; }
	inline float* get_address_of_priority_5() { return &___priority_5; }
	inline void set_priority_5(float value)
	{
		___priority_5 = value;
	}

	inline static int32_t get_offset_of_blendDistance_6() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___blendDistance_6)); }
	inline float get_blendDistance_6() const { return ___blendDistance_6; }
	inline float* get_address_of_blendDistance_6() { return &___blendDistance_6; }
	inline void set_blendDistance_6(float value)
	{
		___blendDistance_6 = value;
	}

	inline static int32_t get_offset_of_weight_7() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___weight_7)); }
	inline float get_weight_7() const { return ___weight_7; }
	inline float* get_address_of_weight_7() { return &___weight_7; }
	inline void set_weight_7(float value)
	{
		___weight_7 = value;
	}

	inline static int32_t get_offset_of_sharedProfile_8() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___sharedProfile_8)); }
	inline VolumeProfile_t1487161726 * get_sharedProfile_8() const { return ___sharedProfile_8; }
	inline VolumeProfile_t1487161726 ** get_address_of_sharedProfile_8() { return &___sharedProfile_8; }
	inline void set_sharedProfile_8(VolumeProfile_t1487161726 * value)
	{
		___sharedProfile_8 = value;
		Il2CppCodeGenWriteBarrier((&___sharedProfile_8), value);
	}

	inline static int32_t get_offset_of_m_PreviousLayer_9() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___m_PreviousLayer_9)); }
	inline int32_t get_m_PreviousLayer_9() const { return ___m_PreviousLayer_9; }
	inline int32_t* get_address_of_m_PreviousLayer_9() { return &___m_PreviousLayer_9; }
	inline void set_m_PreviousLayer_9(int32_t value)
	{
		___m_PreviousLayer_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPriority_10() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___m_PreviousPriority_10)); }
	inline float get_m_PreviousPriority_10() const { return ___m_PreviousPriority_10; }
	inline float* get_address_of_m_PreviousPriority_10() { return &___m_PreviousPriority_10; }
	inline void set_m_PreviousPriority_10(float value)
	{
		___m_PreviousPriority_10 = value;
	}

	inline static int32_t get_offset_of_m_InternalProfile_11() { return static_cast<int32_t>(offsetof(Volume_t3757178504, ___m_InternalProfile_11)); }
	inline VolumeProfile_t1487161726 * get_m_InternalProfile_11() const { return ___m_InternalProfile_11; }
	inline VolumeProfile_t1487161726 ** get_address_of_m_InternalProfile_11() { return &___m_InternalProfile_11; }
	inline void set_m_InternalProfile_11(VolumeProfile_t1487161726 * value)
	{
		___m_InternalProfile_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalProfile_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUME_T3757178504_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (TextureCache_t1429868522), -1, sizeof(TextureCache_t1429868522_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4400[9] = 
{
	TextureCache_t1429868522::get_offset_of_m_NumMipLevels_0(),
	TextureCache_t1429868522::get_offset_of_m_CacheName_1(),
	TextureCache_t1429868522::get_offset_of_m_NumTextures_2(),
	TextureCache_t1429868522::get_offset_of_m_SortedIdxArray_3(),
	TextureCache_t1429868522::get_offset_of_m_SliceArray_4(),
	TextureCache_t1429868522::get_offset_of_m_LocatorInSliceArray_5(),
	TextureCache_t1429868522_StaticFields::get_offset_of_g_MaxFrameCount_6(),
	TextureCache_t1429868522_StaticFields::get_offset_of_g_InvalidTexID_7(),
	TextureCache_t1429868522_StaticFields::get_offset_of_s_TempIntList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (SSliceEntry_t3166396362)+ sizeof (RuntimeObject), sizeof(SSliceEntry_t3166396362 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4401[3] = 
{
	SSliceEntry_t3166396362::get_offset_of_texId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SSliceEntry_t3166396362::get_offset_of_countLRU_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SSliceEntry_t3166396362::get_offset_of_sliceEntryHash_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (TextureSettings_t2698640645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4402[6] = 
{
	0,
	0,
	0,
	TextureSettings_t2698640645::get_offset_of_spotCookieSize_3(),
	TextureSettings_t2698640645::get_offset_of_pointCookieSize_4(),
	TextureSettings_t2698640645::get_offset_of_reflectionCubemapSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (ClearFlag_t2207768290)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4403[5] = 
{
	ClearFlag_t2207768290::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (CoreUtils_t3708687010), -1, sizeof(CoreUtils_t3708687010_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4404[15] = 
{
	CoreUtils_t3708687010_StaticFields::get_offset_of_lookAtList_0(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_upVectorList_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	CoreUtils_t3708687010_StaticFields::get_offset_of_m_BlackCubeTexture_8(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_m_MagentaCubeTexture_9(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_m_WhiteCubeTexture_10(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_m_EmptyUAV_11(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_m_BlackVolumeTexture_12(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_m_AssemblyTypes_13(),
	CoreUtils_t3708687010_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (DelegateUtility_t69250001), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (Frustum_t2039891733)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[2] = 
{
	Frustum_t2039891733::get_offset_of_planes_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frustum_t2039891733::get_offset_of_corners_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (OrientedBBox_t4009157433)+ sizeof (RuntimeObject), sizeof(OrientedBBox_t4009157433 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4407[6] = 
{
	OrientedBBox_t4009157433::get_offset_of_right_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBBox_t4009157433::get_offset_of_extentX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBBox_t4009157433::get_offset_of_up_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBBox_t4009157433::get_offset_of_extentY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBBox_t4009157433::get_offset_of_center_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBBox_t4009157433::get_offset_of_extentZ_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (GeometryUtils_t1844999235), -1, sizeof(GeometryUtils_t1844999235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4408[1] = 
{
	GeometryUtils_t1844999235_StaticFields::get_offset_of_FlipMatrixLHSRHS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (SceneRenderPipeline_t2037314482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[1] = 
{
	SceneRenderPipeline_t2037314482::get_offset_of_renderPipelineAsset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (TileLayoutUtils_t1848109876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (Volume_t3757178504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4411[8] = 
{
	Volume_t3757178504::get_offset_of_isGlobal_4(),
	Volume_t3757178504::get_offset_of_priority_5(),
	Volume_t3757178504::get_offset_of_blendDistance_6(),
	Volume_t3757178504::get_offset_of_weight_7(),
	Volume_t3757178504::get_offset_of_sharedProfile_8(),
	Volume_t3757178504::get_offset_of_m_PreviousLayer_9(),
	Volume_t3757178504::get_offset_of_m_PreviousPriority_10(),
	Volume_t3757178504::get_offset_of_m_InternalProfile_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (VolumeComponentMenu_t1986383967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4412[1] = 
{
	VolumeComponentMenu_t1986383967::get_offset_of_menu_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (VolumeComponent_t825778174), -1, sizeof(VolumeComponent_t825778174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4413[4] = 
{
	VolumeComponent_t825778174::get_offset_of_active_4(),
	VolumeComponent_t825778174::get_offset_of_U3CparametersU3Ek__BackingField_5(),
	VolumeComponent_t825778174_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	VolumeComponent_t825778174_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (VolumeManager_t1188818537), -1, sizeof(VolumeManager_t1188818537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4414[10] = 
{
	VolumeManager_t1188818537_StaticFields::get_offset_of_s_Instance_0(),
	VolumeManager_t1188818537::get_offset_of_U3CstackU3Ek__BackingField_1(),
	VolumeManager_t1188818537::get_offset_of_U3CbaseComponentTypesU3Ek__BackingField_2(),
	0,
	VolumeManager_t1188818537::get_offset_of_m_SortedVolumes_4(),
	VolumeManager_t1188818537::get_offset_of_m_Volumes_5(),
	VolumeManager_t1188818537::get_offset_of_m_SortNeeded_6(),
	VolumeManager_t1188818537::get_offset_of_m_ComponentsDefaultState_7(),
	VolumeManager_t1188818537::get_offset_of_m_TempColliders_8(),
	VolumeManager_t1188818537_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (VolumeParameter_t1867366735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4415[2] = 
{
	0,
	VolumeParameter_t1867366735::get_offset_of_m_OverrideState_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4416[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (BoolParameter_t1742223839), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { sizeof (IntParameter_t3390203283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (NoInterpIntParameter_t2802994991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (MinIntParameter_t3115283297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4420[1] = 
{
	MinIntParameter_t3115283297::get_offset_of_min_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (NoInterpMinIntParameter_t3712115822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4421[1] = 
{
	NoInterpMinIntParameter_t3712115822::get_offset_of_min_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (MaxIntParameter_t954807667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4422[1] = 
{
	MaxIntParameter_t954807667::get_offset_of_max_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (NoInterpMaxIntParameter_t2970896447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4423[1] = 
{
	NoInterpMaxIntParameter_t2970896447::get_offset_of_max_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (ClampedIntParameter_t2484235189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4424[2] = 
{
	ClampedIntParameter_t2484235189::get_offset_of_min_4(),
	ClampedIntParameter_t2484235189::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (NoInterpClampedIntParameter_t3673768753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4425[2] = 
{
	NoInterpClampedIntParameter_t3673768753::get_offset_of_min_4(),
	NoInterpClampedIntParameter_t3673768753::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (FloatParameter_t2493370570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (NoInterpFloatParameter_t884567933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (MinFloatParameter_t3988033055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4428[1] = 
{
	MinFloatParameter_t3988033055::get_offset_of_min_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (NoInterpMinFloatParameter_t1443953088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4429[1] = 
{
	NoInterpMinFloatParameter_t1443953088::get_offset_of_min_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (MaxFloatParameter_t3905301792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4430[1] = 
{
	MaxFloatParameter_t3905301792::get_offset_of_max_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (NoInterpMaxFloatParameter_t2958801461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4431[1] = 
{
	NoInterpMaxFloatParameter_t2958801461::get_offset_of_max_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (ClampedFloatParameter_t3036544206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4432[2] = 
{
	ClampedFloatParameter_t3036544206::get_offset_of_min_4(),
	ClampedFloatParameter_t3036544206::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (NoInterpClampedFloatParameter_t1088071901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4433[2] = 
{
	NoInterpClampedFloatParameter_t1088071901::get_offset_of_min_4(),
	NoInterpClampedFloatParameter_t1088071901::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (FloatRangeParameter_t198971241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4434[2] = 
{
	FloatRangeParameter_t198971241::get_offset_of_min_4(),
	FloatRangeParameter_t198971241::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (NoInterpFloatRangeParameter_t2225766086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4435[2] = 
{
	NoInterpFloatRangeParameter_t2225766086::get_offset_of_min_4(),
	NoInterpFloatRangeParameter_t2225766086::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (ColorParameter_t1856347123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4436[3] = 
{
	ColorParameter_t1856347123::get_offset_of_hdr_4(),
	ColorParameter_t1856347123::get_offset_of_showAlpha_5(),
	ColorParameter_t1856347123::get_offset_of_showEyeDropper_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (NoInterpColorParameter_t3141422378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4437[3] = 
{
	NoInterpColorParameter_t3141422378::get_offset_of_hdr_4(),
	NoInterpColorParameter_t3141422378::get_offset_of_showAlpha_5(),
	NoInterpColorParameter_t3141422378::get_offset_of_showEyeDropper_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (Vector2Parameter_t2379770789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (NoInterpVector2Parameter_t2694188167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (Vector3Parameter_t2442619813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (NoInterpVector3Parameter_t2195262599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (Vector4Parameter_t2245159845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (NoInterpVector4Parameter_t2560625799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (TextureParameter_t4223009474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (NoInterpTextureParameter_t3361413952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (RenderTextureParameter_t351824372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (NoInterpRenderTextureParameter_t1971206433), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (CubemapParameter_t3121160090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (NoInterpCubemapParameter_t2472825447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4450[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (VolumeProfile_t1487161726), -1, sizeof(VolumeProfile_t1487161726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4451[3] = 
{
	VolumeProfile_t1487161726::get_offset_of_components_4(),
	VolumeProfile_t1487161726::get_offset_of_isDirty_5(),
	VolumeProfile_t1487161726_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (VolumeStack_t1960641391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4452[1] = 
{
	VolumeStack_t1960641391::get_offset_of_components_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255373), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4453[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_U24fieldU2D8035E08EB48AD6F05A66CB70BBA91EC34A10E7D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_U24fieldU2DC38C1932AFC0E9FC914191FA4EA84E4A6BF75EF9_1(),
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_U24fieldU2DCDA46F1C24B92FA8375164475268E0375C6A403D_2(),
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_U24fieldU2DC5646B9DA42FF1A2BD05B36B7632EECB3A6D0472_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (U24ArrayTypeU3D12_t2488454198)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454198 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (U24ArrayTypeU3D20_t1702832646)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t1702832646 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (U24ArrayTypeU3D96_t2896897884)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D96_t2896897884 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (U3CModuleU3E_t692745578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (Platform_t1698302846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4459[7] = 
{
	Platform_t1698302846::get_offset_of_m_CurrentActivity_0(),
	Platform_t1698302846::get_offset_of_m_UnityAds_1(),
	Platform_t1698302846::get_offset_of_m_CallbackExecutor_2(),
	Platform_t1698302846::get_offset_of_OnReady_3(),
	Platform_t1698302846::get_offset_of_OnStart_4(),
	Platform_t1698302846::get_offset_of_OnFinish_5(),
	Platform_t1698302846::get_offset_of_OnError_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4460[3] = 
{
	U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815::get_offset_of_handler_0(),
	U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815::get_offset_of_placementId_1(),
	U3ConUnityAdsReadyU3Ec__AnonStorey0_t2484343815::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4461[3] = 
{
	U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353::get_offset_of_handler_0(),
	U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353::get_offset_of_placementId_1(),
	U3ConUnityAdsStartU3Ec__AnonStorey1_t2789353::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4462[3] = 
{
	U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596::get_offset_of_handler_0(),
	U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596::get_offset_of_placementId_1(),
	U3ConUnityAdsFinishU3Ec__AnonStorey2_t696879596::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (U3ConUnityAdsFinishU3Ec__AnonStorey3_t2653194732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4463[2] = 
{
	U3ConUnityAdsFinishU3Ec__AnonStorey3_t2653194732::get_offset_of_showResult_0(),
	U3ConUnityAdsFinishU3Ec__AnonStorey3_t2653194732::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4464[3] = 
{
	U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180::get_offset_of_handler_0(),
	U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180::get_offset_of_message_1(),
	U3ConUnityAdsErrorU3Ec__AnonStorey4_t981031180::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (U3ConUnityAdsErrorU3Ec__AnonStorey5_t3709914535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4465[2] = 
{
	U3ConUnityAdsErrorU3Ec__AnonStorey5_t3709914535::get_offset_of_error_0(),
	U3ConUnityAdsErrorU3Ec__AnonStorey5_t3709914535::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (Placeholder_t2906495853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4466[6] = 
{
	Placeholder_t2906495853::get_offset_of_m_LandscapeTexture_4(),
	Placeholder_t2906495853::get_offset_of_m_PortraitTexture_5(),
	Placeholder_t2906495853::get_offset_of_m_Showing_6(),
	Placeholder_t2906495853::get_offset_of_m_PlacementId_7(),
	Placeholder_t2906495853::get_offset_of_m_AllowSkip_8(),
	Placeholder_t2906495853::get_offset_of_OnFinish_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { sizeof (Platform_t2756657262), -1, sizeof(Platform_t2756657262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4467[6] = 
{
	Platform_t2756657262_StaticFields::get_offset_of_s_BaseUrl_0(),
	Platform_t2756657262::get_offset_of_m_DebugMode_1(),
	Platform_t2756657262::get_offset_of_m_Configuration_2(),
	Platform_t2756657262::get_offset_of_m_Placeholder_3(),
	Platform_t2756657262::get_offset_of_OnStart_4(),
	Platform_t2756657262::get_offset_of_OnFinish_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (U3CInitializeU3Ec__AnonStorey0_t2183256279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4468[3] = 
{
	U3CInitializeU3Ec__AnonStorey0_t2183256279::get_offset_of_request_0(),
	U3CInitializeU3Ec__AnonStorey0_t2183256279::get_offset_of_gameId_1(),
	U3CInitializeU3Ec__AnonStorey0_t2183256279::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (Configuration_t1722493896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4469[3] = 
{
	Configuration_t1722493896::get_offset_of_U3CenabledU3Ek__BackingField_0(),
	Configuration_t1722493896::get_offset_of_U3CdefaultPlacementU3Ek__BackingField_1(),
	Configuration_t1722493896::get_offset_of_U3CplacementsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (Platform_t1647901813), -1, sizeof(Platform_t1647901813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4470[10] = 
{
	Platform_t1647901813_StaticFields::get_offset_of_s_Instance_0(),
	Platform_t1647901813_StaticFields::get_offset_of_s_CallbackExecutor_1(),
	Platform_t1647901813::get_offset_of_OnReady_2(),
	Platform_t1647901813::get_offset_of_OnStart_3(),
	Platform_t1647901813::get_offset_of_OnFinish_4(),
	Platform_t1647901813::get_offset_of_OnError_5(),
	Platform_t1647901813_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	Platform_t1647901813_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
	Platform_t1647901813_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_8(),
	Platform_t1647901813_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (unityAdsReady_t96934738), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (unityAdsDidError_t1993223595), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (unityAdsDidStart_t1058412932), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (unityAdsDidFinish_t3747416149), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { sizeof (U3CUnityAdsReadyU3Ec__AnonStorey0_t1009704718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4475[2] = 
{
	U3CUnityAdsReadyU3Ec__AnonStorey0_t1009704718::get_offset_of_handler_0(),
	U3CUnityAdsReadyU3Ec__AnonStorey0_t1009704718::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { sizeof (U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4476[3] = 
{
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617::get_offset_of_handler_0(),
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617::get_offset_of_rawError_1(),
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t2659421617::get_offset_of_message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (U3CUnityAdsDidStartU3Ec__AnonStorey2_t250005642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4477[2] = 
{
	U3CUnityAdsDidStartU3Ec__AnonStorey2_t250005642::get_offset_of_handler_0(),
	U3CUnityAdsDidStartU3Ec__AnonStorey2_t250005642::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4478[2] = 
{
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544::get_offset_of_handler_0(),
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3126029544::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3126029549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4479[2] = 
{
	U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3126029549::get_offset_of_showResult_0(),
	U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3126029549::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (Advertisement_t842671397), -1, sizeof(Advertisement_t842671397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4480[7] = 
{
	Advertisement_t842671397_StaticFields::get_offset_of_s_Initialized_0(),
	Advertisement_t842671397_StaticFields::get_offset_of_s_Platform_1(),
	Advertisement_t842671397_StaticFields::get_offset_of_s_EditorSupportedPlatform_2(),
	Advertisement_t842671397_StaticFields::get_offset_of_s_Showing_3(),
	Advertisement_t842671397_StaticFields::get_offset_of_s_DebugLevel_4(),
	Advertisement_t842671397_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	Advertisement_t842671397_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (DebugLevelInternal_t4213999277)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4481[6] = 
{
	DebugLevelInternal_t4213999277::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (DebugLevel_t2669295423)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4482[6] = 
{
	DebugLevel_t2669295423::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { sizeof (U3CShowU3Ec__AnonStorey0_t3170924441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4483[1] = 
{
	U3CShowU3Ec__AnonStorey0_t3170924441::get_offset_of_showOptions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (U3CShowU3Ec__AnonStorey1_t3170924442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4484[2] = 
{
	U3CShowU3Ec__AnonStorey1_t3170924442::get_offset_of_finishHandler_0(),
	U3CShowU3Ec__AnonStorey1_t3170924442::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (CallbackExecutor_t363496179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4485[1] = 
{
	CallbackExecutor_t363496179::get_offset_of_s_Queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (ReadyEventArgs_t549087536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4486[1] = 
{
	ReadyEventArgs_t549087536::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (StartEventArgs_t4291826435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4487[1] = 
{
	StartEventArgs_t4291826435::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (FinishEventArgs_t2984178802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4488[2] = 
{
	FinishEventArgs_t2984178802::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
	FinishEventArgs_t2984178802::get_offset_of_U3CshowResultU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (ErrorEventArgs_t2253147013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4489[2] = 
{
	ErrorEventArgs_t2253147013::get_offset_of_U3CerrorU3Ek__BackingField_1(),
	ErrorEventArgs_t2253147013::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (MetaData_t2274729001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4491[2] = 
{
	MetaData_t2274729001::get_offset_of_m_MetaData_0(),
	MetaData_t2274729001::get_offset_of_U3CcategoryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { sizeof (PlacementState_t4035359335)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4492[6] = 
{
	PlacementState_t4035359335::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (ShowOptions_t990845000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4493[2] = 
{
	ShowOptions_t990845000::get_offset_of_U3CresultCallbackU3Ek__BackingField_0(),
	ShowOptions_t990845000::get_offset_of_U3CgamerSidU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (ShowResult_t3070553623)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4494[4] = 
{
	ShowResult_t3070553623::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (JsonArray_t3985338818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (JsonObject_t1327569318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4496[1] = 
{
	JsonObject_t1327569318::get_offset_of__members_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { sizeof (SimpleJson_t791838946), -1, sizeof(SimpleJson_t791838946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4497[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SimpleJson_t791838946_StaticFields::get_offset_of__currentJsonSerializerStrategy_13(),
	SimpleJson_t791838946_StaticFields::get_offset_of__pocoJsonSerializerStrategy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (PocoJsonSerializerStrategy_t3624702326), -1, sizeof(PocoJsonSerializerStrategy_t3624702326_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4499[6] = 
{
	PocoJsonSerializerStrategy_t3624702326::get_offset_of_ConstructorCache_0(),
	PocoJsonSerializerStrategy_t3624702326::get_offset_of_GetCache_1(),
	PocoJsonSerializerStrategy_t3624702326::get_offset_of_SetCache_2(),
	PocoJsonSerializerStrategy_t3624702326_StaticFields::get_offset_of_EmptyTypes_3(),
	PocoJsonSerializerStrategy_t3624702326_StaticFields::get_offset_of_ArrayConstructorParameterTypes_4(),
	PocoJsonSerializerStrategy_t3624702326_StaticFields::get_offset_of_Iso8601Format_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
