﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// LightweightPipelineResources
struct LightweightPipelineResources_t2373355997;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t34654170;
// System.Action`1<UnityEngine.Camera[]>
struct Action_1_t1882455329;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.IRenderPipeline>
struct List_1_t2329883747;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.LightweightPipeline.ScriptableRenderPass>
struct List_1_t409017462;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.ShaderPassName>
struct List_1_t1566003363;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VisibleLight>
struct List_1_t2176140814;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VisibleReflectionProbe>
struct List_1_t2925566062;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>
struct EventHandler_1_t908338235;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t5769829;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t100236324;
// System.Void
struct Void_t1185182177;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// UnityEngine.Experimental.Rendering.LightweightPipeline.BeginXRRenderingPass
struct BeginXRRenderingPass_t4122675669;
// UnityEngine.Experimental.Rendering.LightweightPipeline.CameraComparer
struct CameraComparer_t3166570673;
// UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass
struct CopyColorPass_t3734904897;
// UnityEngine.Experimental.Rendering.LightweightPipeline.CopyDepthPass
struct CopyDepthPass_t304079192;
// UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass
struct CreateLightweightRenderTexturesPass_t2359216577;
// UnityEngine.Experimental.Rendering.LightweightPipeline.DepthOnlyPass
struct DepthOnlyPass_t4004473585;
// UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass
struct DirectionalShadowsPass_t4026755168;
// UnityEngine.Experimental.Rendering.LightweightPipeline.DrawSkyboxPass
struct DrawSkyboxPass_t705056231;
// UnityEngine.Experimental.Rendering.LightweightPipeline.EndXRRenderingPass
struct EndXRRenderingPass_t1600478297;
// UnityEngine.Experimental.Rendering.LightweightPipeline.FinalBlitPass
struct FinalBlitPass_t30679461;
// UnityEngine.Experimental.Rendering.LightweightPipeline.IRendererSetup
struct IRendererSetup_t1460800941;
// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer
struct LightweightForwardRenderer_t94815316;
// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset
struct LightweightPipelineAsset_t3453435338;
// UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass
struct LocalShadowsPass_t2703335903;
// UnityEngine.Experimental.Rendering.LightweightPipeline.OpaquePostProcessPass
struct OpaquePostProcessPass_t1918180670;
// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderOpaqueForwardPass
struct RenderOpaqueForwardPass_t31084166;
// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTransparentForwardPass
struct RenderTransparentForwardPass_t1241286203;
// UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass
struct ScreenSpaceShadowResolvePass_t442348744;
// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupForwardRenderingPass
struct SetupForwardRenderingPass_t3177828444;
// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass
struct SetupLightweightConstanstPass_t1972706453;
// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData[]
struct ShadowSliceDataU5BU5D_t1489940496;
// UnityEngine.Experimental.Rendering.LightweightPipeline.TransparentPostProcessPass
struct TransparentPostProcessPass_t4286252520;
// UnityEngine.Experimental.Rendering.XRGraphicsConfig
struct XRGraphicsConfig_t170342676;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t2302988098;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Rendering.PostProcessing.PostProcessLayer
struct PostProcessLayer_t4264744195;
// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext
struct PostProcessRenderContext_t597611190;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.WWW
struct WWW_t3688466362;




#ifndef U3CMODULEU3E_T692745580_H
#define U3CMODULEU3E_T692745580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745580 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745580_H
#ifndef U3CMODULEU3E_T692745582_H
#define U3CMODULEU3E_T692745582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745582 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745582_H
#ifndef U3CMODULEU3E_T692745581_H
#define U3CMODULEU3E_T692745581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745581 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745581_H
#ifndef U3CMODULEU3E_T692745579_H
#define U3CMODULEU3E_T692745579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745579 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745579_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef REFLECTIONUTILS_T1067364495_H
#define REFLECTIONUTILS_T1067364495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t1067364495  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t1067364495_StaticFields
{
public:
	// System.Object[] UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t2843939325* ___EmptyObjects_0;

public:
	inline static int32_t get_offset_of_EmptyObjects_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t1067364495_StaticFields, ___EmptyObjects_0)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyObjects_0() const { return ___EmptyObjects_0; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyObjects_0() { return &___EmptyObjects_0; }
	inline void set_EmptyObjects_0(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T1067364495_H
#ifndef U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T3903434793_H
#define U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T3903434793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey0
struct  U3CGetConstructorByReflectionU3Ec__AnonStorey0_t3903434793  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey0::constructorInfo
	ConstructorInfo_t5769829 * ___constructorInfo_0;

public:
	inline static int32_t get_offset_of_constructorInfo_0() { return static_cast<int32_t>(offsetof(U3CGetConstructorByReflectionU3Ec__AnonStorey0_t3903434793, ___constructorInfo_0)); }
	inline ConstructorInfo_t5769829 * get_constructorInfo_0() const { return ___constructorInfo_0; }
	inline ConstructorInfo_t5769829 ** get_address_of_constructorInfo_0() { return &___constructorInfo_0; }
	inline void set_constructorInfo_0(ConstructorInfo_t5769829 * value)
	{
		___constructorInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___constructorInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T3903434793_H
#ifndef U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T1858150574_H
#define U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T1858150574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey1
struct  U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t1858150574  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey1::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t1858150574, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T1858150574_H
#ifndef U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T1858150577_H
#define U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T1858150577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
struct  U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1858150577  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1858150577, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T1858150577_H
#ifndef U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_T651529835_H
#define U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_T651529835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey3
struct  U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t651529835  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey3::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t651529835, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_T651529835_H
#ifndef U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T651529832_H
#define U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T651529832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
struct  U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t651529832  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t651529832, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T651529832_H
#ifndef UNSUPPORTEDPLATFORM_T2036049172_H
#define UNSUPPORTEDPLATFORM_T2036049172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnsupportedPlatform
struct  UnsupportedPlatform_t2036049172  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.UnsupportedPlatform::OnFinish
	EventHandler_1_t908338235 * ___OnFinish_0;

public:
	inline static int32_t get_offset_of_OnFinish_0() { return static_cast<int32_t>(offsetof(UnsupportedPlatform_t2036049172, ___OnFinish_0)); }
	inline EventHandler_1_t908338235 * get_OnFinish_0() const { return ___OnFinish_0; }
	inline EventHandler_1_t908338235 ** get_address_of_OnFinish_0() { return &___OnFinish_0; }
	inline void set_OnFinish_0(EventHandler_1_t908338235 * value)
	{
		___OnFinish_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDPLATFORM_T2036049172_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef U3CFETCHPRIVACYURLCOROUTINEU3EC__ITERATOR1_T3219080383_H
#define U3CFETCHPRIVACYURLCOROUTINEU3EC__ITERATOR1_T3219080383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1
struct  U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::<postJson>__0
	String_t* ___U3CpostJsonU3E__0_0;
	// System.Byte[] UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::<bytes>__0
	ByteU5BU5D_t4116647657* ___U3CbytesU3E__0_1;
	// UnityEngine.WWW UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Action`1<System.String> UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::failure
	Action_1_t2019918284 * ___failure_3;
	// System.Action`1<System.String> UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::success
	Action_1_t2019918284 * ___success_4;
	// System.Object UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 UnityEngine.Analytics.DataPrivacy/<FetchPrivacyUrlCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CpostJsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U3CpostJsonU3E__0_0)); }
	inline String_t* get_U3CpostJsonU3E__0_0() const { return ___U3CpostJsonU3E__0_0; }
	inline String_t** get_address_of_U3CpostJsonU3E__0_0() { return &___U3CpostJsonU3E__0_0; }
	inline void set_U3CpostJsonU3E__0_0(String_t* value)
	{
		___U3CpostJsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostJsonU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U3CbytesU3E__0_1)); }
	inline ByteU5BU5D_t4116647657* get_U3CbytesU3E__0_1() const { return ___U3CbytesU3E__0_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbytesU3E__0_1() { return &___U3CbytesU3E__0_1; }
	inline void set_U3CbytesU3E__0_1(ByteU5BU5D_t4116647657* value)
	{
		___U3CbytesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbytesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_failure_3() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___failure_3)); }
	inline Action_1_t2019918284 * get_failure_3() const { return ___failure_3; }
	inline Action_1_t2019918284 ** get_address_of_failure_3() { return &___failure_3; }
	inline void set_failure_3(Action_1_t2019918284 * value)
	{
		___failure_3 = value;
		Il2CppCodeGenWriteBarrier((&___failure_3), value);
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___success_4)); }
	inline Action_1_t2019918284 * get_success_4() const { return ___success_4; }
	inline Action_1_t2019918284 ** get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(Action_1_t2019918284 * value)
	{
		___success_4 = value;
		Il2CppCodeGenWriteBarrier((&___success_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHPRIVACYURLCOROUTINEU3EC__ITERATOR1_T3219080383_H
#ifndef DATAPRIVACYUTILS_T3678541040_H
#define DATAPRIVACYUTILS_T3678541040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/DataPrivacyUtils
struct  DataPrivacyUtils_t3678541040  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPRIVACYUTILS_T3678541040_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef CAMERACOMPARER_T3166570673_H
#define CAMERACOMPARER_T3166570673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.CameraComparer
struct  CameraComparer_t3166570673  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACOMPARER_T3166570673_H
#ifndef DIRECTIONALSHADOWCONSTANTBUFFER_T4115518952_H
#define DIRECTIONALSHADOWCONSTANTBUFFER_T4115518952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer
struct  DirectionalShadowConstantBuffer_t4115518952  : public RuntimeObject
{
public:

public:
};

struct DirectionalShadowConstantBuffer_t4115518952_StaticFields
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_WorldToShadow
	int32_t ____WorldToShadow_0;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_ShadowData
	int32_t ____ShadowData_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_DirShadowSplitSpheres0
	int32_t ____DirShadowSplitSpheres0_2;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_DirShadowSplitSpheres1
	int32_t ____DirShadowSplitSpheres1_3;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_DirShadowSplitSpheres2
	int32_t ____DirShadowSplitSpheres2_4;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_DirShadowSplitSpheres3
	int32_t ____DirShadowSplitSpheres3_5;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_DirShadowSplitSphereRadii
	int32_t ____DirShadowSplitSphereRadii_6;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_ShadowOffset0
	int32_t ____ShadowOffset0_7;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_ShadowOffset1
	int32_t ____ShadowOffset1_8;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_ShadowOffset2
	int32_t ____ShadowOffset2_9;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_ShadowOffset3
	int32_t ____ShadowOffset3_10;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass/DirectionalShadowConstantBuffer::_ShadowmapSize
	int32_t ____ShadowmapSize_11;

public:
	inline static int32_t get_offset_of__WorldToShadow_0() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____WorldToShadow_0)); }
	inline int32_t get__WorldToShadow_0() const { return ____WorldToShadow_0; }
	inline int32_t* get_address_of__WorldToShadow_0() { return &____WorldToShadow_0; }
	inline void set__WorldToShadow_0(int32_t value)
	{
		____WorldToShadow_0 = value;
	}

	inline static int32_t get_offset_of__ShadowData_1() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____ShadowData_1)); }
	inline int32_t get__ShadowData_1() const { return ____ShadowData_1; }
	inline int32_t* get_address_of__ShadowData_1() { return &____ShadowData_1; }
	inline void set__ShadowData_1(int32_t value)
	{
		____ShadowData_1 = value;
	}

	inline static int32_t get_offset_of__DirShadowSplitSpheres0_2() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____DirShadowSplitSpheres0_2)); }
	inline int32_t get__DirShadowSplitSpheres0_2() const { return ____DirShadowSplitSpheres0_2; }
	inline int32_t* get_address_of__DirShadowSplitSpheres0_2() { return &____DirShadowSplitSpheres0_2; }
	inline void set__DirShadowSplitSpheres0_2(int32_t value)
	{
		____DirShadowSplitSpheres0_2 = value;
	}

	inline static int32_t get_offset_of__DirShadowSplitSpheres1_3() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____DirShadowSplitSpheres1_3)); }
	inline int32_t get__DirShadowSplitSpheres1_3() const { return ____DirShadowSplitSpheres1_3; }
	inline int32_t* get_address_of__DirShadowSplitSpheres1_3() { return &____DirShadowSplitSpheres1_3; }
	inline void set__DirShadowSplitSpheres1_3(int32_t value)
	{
		____DirShadowSplitSpheres1_3 = value;
	}

	inline static int32_t get_offset_of__DirShadowSplitSpheres2_4() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____DirShadowSplitSpheres2_4)); }
	inline int32_t get__DirShadowSplitSpheres2_4() const { return ____DirShadowSplitSpheres2_4; }
	inline int32_t* get_address_of__DirShadowSplitSpheres2_4() { return &____DirShadowSplitSpheres2_4; }
	inline void set__DirShadowSplitSpheres2_4(int32_t value)
	{
		____DirShadowSplitSpheres2_4 = value;
	}

	inline static int32_t get_offset_of__DirShadowSplitSpheres3_5() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____DirShadowSplitSpheres3_5)); }
	inline int32_t get__DirShadowSplitSpheres3_5() const { return ____DirShadowSplitSpheres3_5; }
	inline int32_t* get_address_of__DirShadowSplitSpheres3_5() { return &____DirShadowSplitSpheres3_5; }
	inline void set__DirShadowSplitSpheres3_5(int32_t value)
	{
		____DirShadowSplitSpheres3_5 = value;
	}

	inline static int32_t get_offset_of__DirShadowSplitSphereRadii_6() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____DirShadowSplitSphereRadii_6)); }
	inline int32_t get__DirShadowSplitSphereRadii_6() const { return ____DirShadowSplitSphereRadii_6; }
	inline int32_t* get_address_of__DirShadowSplitSphereRadii_6() { return &____DirShadowSplitSphereRadii_6; }
	inline void set__DirShadowSplitSphereRadii_6(int32_t value)
	{
		____DirShadowSplitSphereRadii_6 = value;
	}

	inline static int32_t get_offset_of__ShadowOffset0_7() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____ShadowOffset0_7)); }
	inline int32_t get__ShadowOffset0_7() const { return ____ShadowOffset0_7; }
	inline int32_t* get_address_of__ShadowOffset0_7() { return &____ShadowOffset0_7; }
	inline void set__ShadowOffset0_7(int32_t value)
	{
		____ShadowOffset0_7 = value;
	}

	inline static int32_t get_offset_of__ShadowOffset1_8() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____ShadowOffset1_8)); }
	inline int32_t get__ShadowOffset1_8() const { return ____ShadowOffset1_8; }
	inline int32_t* get_address_of__ShadowOffset1_8() { return &____ShadowOffset1_8; }
	inline void set__ShadowOffset1_8(int32_t value)
	{
		____ShadowOffset1_8 = value;
	}

	inline static int32_t get_offset_of__ShadowOffset2_9() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____ShadowOffset2_9)); }
	inline int32_t get__ShadowOffset2_9() const { return ____ShadowOffset2_9; }
	inline int32_t* get_address_of__ShadowOffset2_9() { return &____ShadowOffset2_9; }
	inline void set__ShadowOffset2_9(int32_t value)
	{
		____ShadowOffset2_9 = value;
	}

	inline static int32_t get_offset_of__ShadowOffset3_10() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____ShadowOffset3_10)); }
	inline int32_t get__ShadowOffset3_10() const { return ____ShadowOffset3_10; }
	inline int32_t* get_address_of__ShadowOffset3_10() { return &____ShadowOffset3_10; }
	inline void set__ShadowOffset3_10(int32_t value)
	{
		____ShadowOffset3_10 = value;
	}

	inline static int32_t get_offset_of__ShadowmapSize_11() { return static_cast<int32_t>(offsetof(DirectionalShadowConstantBuffer_t4115518952_StaticFields, ____ShadowmapSize_11)); }
	inline int32_t get__ShadowmapSize_11() const { return ____ShadowmapSize_11; }
	inline int32_t* get_address_of__ShadowmapSize_11() { return &____ShadowmapSize_11; }
	inline void set__ShadowmapSize_11(int32_t value)
	{
		____ShadowmapSize_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONALSHADOWCONSTANTBUFFER_T4115518952_H
#ifndef LIGHTWEIGHTKEYWORDSTRINGS_T3199804416_H
#define LIGHTWEIGHTKEYWORDSTRINGS_T3199804416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings
struct  LightweightKeywordStrings_t3199804416  : public RuntimeObject
{
public:

public:
};

struct LightweightKeywordStrings_t3199804416_StaticFields
{
public:
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::AdditionalLights
	String_t* ___AdditionalLights_0;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::VertexLights
	String_t* ___VertexLights_1;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::MixedLightingSubtractive
	String_t* ___MixedLightingSubtractive_2;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::MainLightCookie
	String_t* ___MainLightCookie_3;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::DirectionalShadows
	String_t* ___DirectionalShadows_4;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::LocalShadows
	String_t* ___LocalShadows_5;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::SoftShadows
	String_t* ___SoftShadows_6;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::CascadeShadows
	String_t* ___CascadeShadows_7;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::DepthNoMsaa
	String_t* ___DepthNoMsaa_8;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::DepthMsaa2
	String_t* ___DepthMsaa2_9;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightKeywordStrings::DepthMsaa4
	String_t* ___DepthMsaa4_10;

public:
	inline static int32_t get_offset_of_AdditionalLights_0() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___AdditionalLights_0)); }
	inline String_t* get_AdditionalLights_0() const { return ___AdditionalLights_0; }
	inline String_t** get_address_of_AdditionalLights_0() { return &___AdditionalLights_0; }
	inline void set_AdditionalLights_0(String_t* value)
	{
		___AdditionalLights_0 = value;
		Il2CppCodeGenWriteBarrier((&___AdditionalLights_0), value);
	}

	inline static int32_t get_offset_of_VertexLights_1() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___VertexLights_1)); }
	inline String_t* get_VertexLights_1() const { return ___VertexLights_1; }
	inline String_t** get_address_of_VertexLights_1() { return &___VertexLights_1; }
	inline void set_VertexLights_1(String_t* value)
	{
		___VertexLights_1 = value;
		Il2CppCodeGenWriteBarrier((&___VertexLights_1), value);
	}

	inline static int32_t get_offset_of_MixedLightingSubtractive_2() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___MixedLightingSubtractive_2)); }
	inline String_t* get_MixedLightingSubtractive_2() const { return ___MixedLightingSubtractive_2; }
	inline String_t** get_address_of_MixedLightingSubtractive_2() { return &___MixedLightingSubtractive_2; }
	inline void set_MixedLightingSubtractive_2(String_t* value)
	{
		___MixedLightingSubtractive_2 = value;
		Il2CppCodeGenWriteBarrier((&___MixedLightingSubtractive_2), value);
	}

	inline static int32_t get_offset_of_MainLightCookie_3() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___MainLightCookie_3)); }
	inline String_t* get_MainLightCookie_3() const { return ___MainLightCookie_3; }
	inline String_t** get_address_of_MainLightCookie_3() { return &___MainLightCookie_3; }
	inline void set_MainLightCookie_3(String_t* value)
	{
		___MainLightCookie_3 = value;
		Il2CppCodeGenWriteBarrier((&___MainLightCookie_3), value);
	}

	inline static int32_t get_offset_of_DirectionalShadows_4() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___DirectionalShadows_4)); }
	inline String_t* get_DirectionalShadows_4() const { return ___DirectionalShadows_4; }
	inline String_t** get_address_of_DirectionalShadows_4() { return &___DirectionalShadows_4; }
	inline void set_DirectionalShadows_4(String_t* value)
	{
		___DirectionalShadows_4 = value;
		Il2CppCodeGenWriteBarrier((&___DirectionalShadows_4), value);
	}

	inline static int32_t get_offset_of_LocalShadows_5() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___LocalShadows_5)); }
	inline String_t* get_LocalShadows_5() const { return ___LocalShadows_5; }
	inline String_t** get_address_of_LocalShadows_5() { return &___LocalShadows_5; }
	inline void set_LocalShadows_5(String_t* value)
	{
		___LocalShadows_5 = value;
		Il2CppCodeGenWriteBarrier((&___LocalShadows_5), value);
	}

	inline static int32_t get_offset_of_SoftShadows_6() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___SoftShadows_6)); }
	inline String_t* get_SoftShadows_6() const { return ___SoftShadows_6; }
	inline String_t** get_address_of_SoftShadows_6() { return &___SoftShadows_6; }
	inline void set_SoftShadows_6(String_t* value)
	{
		___SoftShadows_6 = value;
		Il2CppCodeGenWriteBarrier((&___SoftShadows_6), value);
	}

	inline static int32_t get_offset_of_CascadeShadows_7() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___CascadeShadows_7)); }
	inline String_t* get_CascadeShadows_7() const { return ___CascadeShadows_7; }
	inline String_t** get_address_of_CascadeShadows_7() { return &___CascadeShadows_7; }
	inline void set_CascadeShadows_7(String_t* value)
	{
		___CascadeShadows_7 = value;
		Il2CppCodeGenWriteBarrier((&___CascadeShadows_7), value);
	}

	inline static int32_t get_offset_of_DepthNoMsaa_8() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___DepthNoMsaa_8)); }
	inline String_t* get_DepthNoMsaa_8() const { return ___DepthNoMsaa_8; }
	inline String_t** get_address_of_DepthNoMsaa_8() { return &___DepthNoMsaa_8; }
	inline void set_DepthNoMsaa_8(String_t* value)
	{
		___DepthNoMsaa_8 = value;
		Il2CppCodeGenWriteBarrier((&___DepthNoMsaa_8), value);
	}

	inline static int32_t get_offset_of_DepthMsaa2_9() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___DepthMsaa2_9)); }
	inline String_t* get_DepthMsaa2_9() const { return ___DepthMsaa2_9; }
	inline String_t** get_address_of_DepthMsaa2_9() { return &___DepthMsaa2_9; }
	inline void set_DepthMsaa2_9(String_t* value)
	{
		___DepthMsaa2_9 = value;
		Il2CppCodeGenWriteBarrier((&___DepthMsaa2_9), value);
	}

	inline static int32_t get_offset_of_DepthMsaa4_10() { return static_cast<int32_t>(offsetof(LightweightKeywordStrings_t3199804416_StaticFields, ___DepthMsaa4_10)); }
	inline String_t* get_DepthMsaa4_10() const { return ___DepthMsaa4_10; }
	inline String_t** get_address_of_DepthMsaa4_10() { return &___DepthMsaa4_10; }
	inline void set_DepthMsaa4_10(String_t* value)
	{
		___DepthMsaa4_10 = value;
		Il2CppCodeGenWriteBarrier((&___DepthMsaa4_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTKEYWORDSTRINGS_T3199804416_H
#ifndef PERFRAMEBUFFER_T2481871112_H
#define PERFRAMEBUFFER_T2481871112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline/PerFrameBuffer
struct  PerFrameBuffer_t2481871112  : public RuntimeObject
{
public:

public:
};

struct PerFrameBuffer_t2481871112_StaticFields
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline/PerFrameBuffer::_GlossyEnvironmentColor
	int32_t ____GlossyEnvironmentColor_0;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline/PerFrameBuffer::_SubtractiveShadowColor
	int32_t ____SubtractiveShadowColor_1;

public:
	inline static int32_t get_offset_of__GlossyEnvironmentColor_0() { return static_cast<int32_t>(offsetof(PerFrameBuffer_t2481871112_StaticFields, ____GlossyEnvironmentColor_0)); }
	inline int32_t get__GlossyEnvironmentColor_0() const { return ____GlossyEnvironmentColor_0; }
	inline int32_t* get_address_of__GlossyEnvironmentColor_0() { return &____GlossyEnvironmentColor_0; }
	inline void set__GlossyEnvironmentColor_0(int32_t value)
	{
		____GlossyEnvironmentColor_0 = value;
	}

	inline static int32_t get_offset_of__SubtractiveShadowColor_1() { return static_cast<int32_t>(offsetof(PerFrameBuffer_t2481871112_StaticFields, ____SubtractiveShadowColor_1)); }
	inline int32_t get__SubtractiveShadowColor_1() const { return ____SubtractiveShadowColor_1; }
	inline int32_t* get_address_of__SubtractiveShadowColor_1() { return &____SubtractiveShadowColor_1; }
	inline void set__SubtractiveShadowColor_1(int32_t value)
	{
		____SubtractiveShadowColor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERFRAMEBUFFER_T2481871112_H
#ifndef LIGHTWEIGHTSHADERUTILS_T2293681725_H
#define LIGHTWEIGHTSHADERUTILS_T2293681725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightShaderUtils
struct  LightweightShaderUtils_t2293681725  : public RuntimeObject
{
public:

public:
};

struct LightweightShaderUtils_t2293681725_StaticFields
{
public:
	// System.String[] UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightShaderUtils::m_ShaderPaths
	StringU5BU5D_t1281789340* ___m_ShaderPaths_0;

public:
	inline static int32_t get_offset_of_m_ShaderPaths_0() { return static_cast<int32_t>(offsetof(LightweightShaderUtils_t2293681725_StaticFields, ___m_ShaderPaths_0)); }
	inline StringU5BU5D_t1281789340* get_m_ShaderPaths_0() const { return ___m_ShaderPaths_0; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ShaderPaths_0() { return &___m_ShaderPaths_0; }
	inline void set_m_ShaderPaths_0(StringU5BU5D_t1281789340* value)
	{
		___m_ShaderPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ShaderPaths_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTSHADERUTILS_T2293681725_H
#ifndef LIGHTWEIGHTSHADOWUTILS_T1883746463_H
#define LIGHTWEIGHTSHADOWUTILS_T1883746463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightShadowUtils
struct  LightweightShadowUtils_t1883746463  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTSHADOWUTILS_T1883746463_H
#ifndef LOCALSHADOWCONSTANTBUFFER_T1372550715_H
#define LOCALSHADOWCONSTANTBUFFER_T1372550715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer
struct  LocalShadowConstantBuffer_t1372550715  : public RuntimeObject
{
public:

public:
};

struct LocalShadowConstantBuffer_t1372550715_StaticFields
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalWorldToShadowAtlas
	int32_t ____LocalWorldToShadowAtlas_0;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalShadowStrength
	int32_t ____LocalShadowStrength_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalShadowOffset0
	int32_t ____LocalShadowOffset0_2;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalShadowOffset1
	int32_t ____LocalShadowOffset1_3;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalShadowOffset2
	int32_t ____LocalShadowOffset2_4;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalShadowOffset3
	int32_t ____LocalShadowOffset3_5;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass/LocalShadowConstantBuffer::_LocalShadowmapSize
	int32_t ____LocalShadowmapSize_6;

public:
	inline static int32_t get_offset_of__LocalWorldToShadowAtlas_0() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalWorldToShadowAtlas_0)); }
	inline int32_t get__LocalWorldToShadowAtlas_0() const { return ____LocalWorldToShadowAtlas_0; }
	inline int32_t* get_address_of__LocalWorldToShadowAtlas_0() { return &____LocalWorldToShadowAtlas_0; }
	inline void set__LocalWorldToShadowAtlas_0(int32_t value)
	{
		____LocalWorldToShadowAtlas_0 = value;
	}

	inline static int32_t get_offset_of__LocalShadowStrength_1() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalShadowStrength_1)); }
	inline int32_t get__LocalShadowStrength_1() const { return ____LocalShadowStrength_1; }
	inline int32_t* get_address_of__LocalShadowStrength_1() { return &____LocalShadowStrength_1; }
	inline void set__LocalShadowStrength_1(int32_t value)
	{
		____LocalShadowStrength_1 = value;
	}

	inline static int32_t get_offset_of__LocalShadowOffset0_2() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalShadowOffset0_2)); }
	inline int32_t get__LocalShadowOffset0_2() const { return ____LocalShadowOffset0_2; }
	inline int32_t* get_address_of__LocalShadowOffset0_2() { return &____LocalShadowOffset0_2; }
	inline void set__LocalShadowOffset0_2(int32_t value)
	{
		____LocalShadowOffset0_2 = value;
	}

	inline static int32_t get_offset_of__LocalShadowOffset1_3() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalShadowOffset1_3)); }
	inline int32_t get__LocalShadowOffset1_3() const { return ____LocalShadowOffset1_3; }
	inline int32_t* get_address_of__LocalShadowOffset1_3() { return &____LocalShadowOffset1_3; }
	inline void set__LocalShadowOffset1_3(int32_t value)
	{
		____LocalShadowOffset1_3 = value;
	}

	inline static int32_t get_offset_of__LocalShadowOffset2_4() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalShadowOffset2_4)); }
	inline int32_t get__LocalShadowOffset2_4() const { return ____LocalShadowOffset2_4; }
	inline int32_t* get_address_of__LocalShadowOffset2_4() { return &____LocalShadowOffset2_4; }
	inline void set__LocalShadowOffset2_4(int32_t value)
	{
		____LocalShadowOffset2_4 = value;
	}

	inline static int32_t get_offset_of__LocalShadowOffset3_5() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalShadowOffset3_5)); }
	inline int32_t get__LocalShadowOffset3_5() const { return ____LocalShadowOffset3_5; }
	inline int32_t* get_address_of__LocalShadowOffset3_5() { return &____LocalShadowOffset3_5; }
	inline void set__LocalShadowOffset3_5(int32_t value)
	{
		____LocalShadowOffset3_5 = value;
	}

	inline static int32_t get_offset_of__LocalShadowmapSize_6() { return static_cast<int32_t>(offsetof(LocalShadowConstantBuffer_t1372550715_StaticFields, ____LocalShadowmapSize_6)); }
	inline int32_t get__LocalShadowmapSize_6() const { return ____LocalShadowmapSize_6; }
	inline int32_t* get_address_of__LocalShadowmapSize_6() { return &____LocalShadowmapSize_6; }
	inline void set__LocalShadowmapSize_6(int32_t value)
	{
		____LocalShadowmapSize_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALSHADOWCONSTANTBUFFER_T1372550715_H
#ifndef SCRIPTABLERENDERPASS_T3231910016_H
#define SCRIPTABLERENDERPASS_T3231910016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ScriptableRenderPass
struct  ScriptableRenderPass_t3231910016  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer UnityEngine.Experimental.Rendering.LightweightPipeline.ScriptableRenderPass::<renderer>k__BackingField
	LightweightForwardRenderer_t94815316 * ___U3CrendererU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.ShaderPassName> UnityEngine.Experimental.Rendering.LightweightPipeline.ScriptableRenderPass::m_ShaderPassNames
	List_1_t1566003363 * ___m_ShaderPassNames_1;

public:
	inline static int32_t get_offset_of_U3CrendererU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScriptableRenderPass_t3231910016, ___U3CrendererU3Ek__BackingField_0)); }
	inline LightweightForwardRenderer_t94815316 * get_U3CrendererU3Ek__BackingField_0() const { return ___U3CrendererU3Ek__BackingField_0; }
	inline LightweightForwardRenderer_t94815316 ** get_address_of_U3CrendererU3Ek__BackingField_0() { return &___U3CrendererU3Ek__BackingField_0; }
	inline void set_U3CrendererU3Ek__BackingField_0(LightweightForwardRenderer_t94815316 * value)
	{
		___U3CrendererU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrendererU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_ShaderPassNames_1() { return static_cast<int32_t>(offsetof(ScriptableRenderPass_t3231910016, ___m_ShaderPassNames_1)); }
	inline List_1_t1566003363 * get_m_ShaderPassNames_1() const { return ___m_ShaderPassNames_1; }
	inline List_1_t1566003363 ** get_address_of_m_ShaderPassNames_1() { return &___m_ShaderPassNames_1; }
	inline void set_m_ShaderPassNames_1(List_1_t1566003363 * value)
	{
		___m_ShaderPassNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ShaderPassNames_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLERENDERPASS_T3231910016_H
#ifndef PERCAMERABUFFER_T439951145_H
#define PERCAMERABUFFER_T439951145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer
struct  PerCameraBuffer_t439951145  : public RuntimeObject
{
public:

public:
};

struct PerCameraBuffer_t439951145_StaticFields
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_MainLightPosition
	int32_t ____MainLightPosition_0;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_MainLightColor
	int32_t ____MainLightColor_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_MainLightCookie
	int32_t ____MainLightCookie_2;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_WorldToLight
	int32_t ____WorldToLight_3;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_AdditionalLightCount
	int32_t ____AdditionalLightCount_4;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_AdditionalLightPosition
	int32_t ____AdditionalLightPosition_5;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_AdditionalLightColor
	int32_t ____AdditionalLightColor_6;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_AdditionalLightDistanceAttenuation
	int32_t ____AdditionalLightDistanceAttenuation_7;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_AdditionalLightSpotDir
	int32_t ____AdditionalLightSpotDir_8;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_AdditionalLightSpotAttenuation
	int32_t ____AdditionalLightSpotAttenuation_9;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_LightIndexBuffer
	int32_t ____LightIndexBuffer_10;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass/PerCameraBuffer::_ScaledScreenParams
	int32_t ____ScaledScreenParams_11;

public:
	inline static int32_t get_offset_of__MainLightPosition_0() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____MainLightPosition_0)); }
	inline int32_t get__MainLightPosition_0() const { return ____MainLightPosition_0; }
	inline int32_t* get_address_of__MainLightPosition_0() { return &____MainLightPosition_0; }
	inline void set__MainLightPosition_0(int32_t value)
	{
		____MainLightPosition_0 = value;
	}

	inline static int32_t get_offset_of__MainLightColor_1() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____MainLightColor_1)); }
	inline int32_t get__MainLightColor_1() const { return ____MainLightColor_1; }
	inline int32_t* get_address_of__MainLightColor_1() { return &____MainLightColor_1; }
	inline void set__MainLightColor_1(int32_t value)
	{
		____MainLightColor_1 = value;
	}

	inline static int32_t get_offset_of__MainLightCookie_2() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____MainLightCookie_2)); }
	inline int32_t get__MainLightCookie_2() const { return ____MainLightCookie_2; }
	inline int32_t* get_address_of__MainLightCookie_2() { return &____MainLightCookie_2; }
	inline void set__MainLightCookie_2(int32_t value)
	{
		____MainLightCookie_2 = value;
	}

	inline static int32_t get_offset_of__WorldToLight_3() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____WorldToLight_3)); }
	inline int32_t get__WorldToLight_3() const { return ____WorldToLight_3; }
	inline int32_t* get_address_of__WorldToLight_3() { return &____WorldToLight_3; }
	inline void set__WorldToLight_3(int32_t value)
	{
		____WorldToLight_3 = value;
	}

	inline static int32_t get_offset_of__AdditionalLightCount_4() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____AdditionalLightCount_4)); }
	inline int32_t get__AdditionalLightCount_4() const { return ____AdditionalLightCount_4; }
	inline int32_t* get_address_of__AdditionalLightCount_4() { return &____AdditionalLightCount_4; }
	inline void set__AdditionalLightCount_4(int32_t value)
	{
		____AdditionalLightCount_4 = value;
	}

	inline static int32_t get_offset_of__AdditionalLightPosition_5() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____AdditionalLightPosition_5)); }
	inline int32_t get__AdditionalLightPosition_5() const { return ____AdditionalLightPosition_5; }
	inline int32_t* get_address_of__AdditionalLightPosition_5() { return &____AdditionalLightPosition_5; }
	inline void set__AdditionalLightPosition_5(int32_t value)
	{
		____AdditionalLightPosition_5 = value;
	}

	inline static int32_t get_offset_of__AdditionalLightColor_6() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____AdditionalLightColor_6)); }
	inline int32_t get__AdditionalLightColor_6() const { return ____AdditionalLightColor_6; }
	inline int32_t* get_address_of__AdditionalLightColor_6() { return &____AdditionalLightColor_6; }
	inline void set__AdditionalLightColor_6(int32_t value)
	{
		____AdditionalLightColor_6 = value;
	}

	inline static int32_t get_offset_of__AdditionalLightDistanceAttenuation_7() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____AdditionalLightDistanceAttenuation_7)); }
	inline int32_t get__AdditionalLightDistanceAttenuation_7() const { return ____AdditionalLightDistanceAttenuation_7; }
	inline int32_t* get_address_of__AdditionalLightDistanceAttenuation_7() { return &____AdditionalLightDistanceAttenuation_7; }
	inline void set__AdditionalLightDistanceAttenuation_7(int32_t value)
	{
		____AdditionalLightDistanceAttenuation_7 = value;
	}

	inline static int32_t get_offset_of__AdditionalLightSpotDir_8() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____AdditionalLightSpotDir_8)); }
	inline int32_t get__AdditionalLightSpotDir_8() const { return ____AdditionalLightSpotDir_8; }
	inline int32_t* get_address_of__AdditionalLightSpotDir_8() { return &____AdditionalLightSpotDir_8; }
	inline void set__AdditionalLightSpotDir_8(int32_t value)
	{
		____AdditionalLightSpotDir_8 = value;
	}

	inline static int32_t get_offset_of__AdditionalLightSpotAttenuation_9() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____AdditionalLightSpotAttenuation_9)); }
	inline int32_t get__AdditionalLightSpotAttenuation_9() const { return ____AdditionalLightSpotAttenuation_9; }
	inline int32_t* get_address_of__AdditionalLightSpotAttenuation_9() { return &____AdditionalLightSpotAttenuation_9; }
	inline void set__AdditionalLightSpotAttenuation_9(int32_t value)
	{
		____AdditionalLightSpotAttenuation_9 = value;
	}

	inline static int32_t get_offset_of__LightIndexBuffer_10() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____LightIndexBuffer_10)); }
	inline int32_t get__LightIndexBuffer_10() const { return ____LightIndexBuffer_10; }
	inline int32_t* get_address_of__LightIndexBuffer_10() { return &____LightIndexBuffer_10; }
	inline void set__LightIndexBuffer_10(int32_t value)
	{
		____LightIndexBuffer_10 = value;
	}

	inline static int32_t get_offset_of__ScaledScreenParams_11() { return static_cast<int32_t>(offsetof(PerCameraBuffer_t439951145_StaticFields, ____ScaledScreenParams_11)); }
	inline int32_t get__ScaledScreenParams_11() const { return ____ScaledScreenParams_11; }
	inline int32_t* get_address_of__ScaledScreenParams_11() { return &____ScaledScreenParams_11; }
	inline void set__ScaledScreenParams_11(int32_t value)
	{
		____ScaledScreenParams_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERCAMERABUFFER_T439951145_H
#ifndef RENDERPIPELINE_T957695896_H
#define RENDERPIPELINE_T957695896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipeline
struct  RenderPipeline_t957695896  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.RenderPipeline::<disposed>k__BackingField
	bool ___U3CdisposedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CdisposedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RenderPipeline_t957695896, ___U3CdisposedU3Ek__BackingField_2)); }
	inline bool get_U3CdisposedU3Ek__BackingField_2() const { return ___U3CdisposedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CdisposedU3Ek__BackingField_2() { return &___U3CdisposedU3Ek__BackingField_2; }
	inline void set_U3CdisposedU3Ek__BackingField_2(bool value)
	{
		___U3CdisposedU3Ek__BackingField_2 = value;
	}
};

struct RenderPipeline_t957695896_StaticFields
{
public:
	// System.Action`1<UnityEngine.Camera[]> UnityEngine.Experimental.Rendering.RenderPipeline::beginFrameRendering
	Action_1_t1882455329 * ___beginFrameRendering_0;
	// System.Action`1<UnityEngine.Camera> UnityEngine.Experimental.Rendering.RenderPipeline::beginCameraRendering
	Action_1_t34654170 * ___beginCameraRendering_1;

public:
	inline static int32_t get_offset_of_beginFrameRendering_0() { return static_cast<int32_t>(offsetof(RenderPipeline_t957695896_StaticFields, ___beginFrameRendering_0)); }
	inline Action_1_t1882455329 * get_beginFrameRendering_0() const { return ___beginFrameRendering_0; }
	inline Action_1_t1882455329 ** get_address_of_beginFrameRendering_0() { return &___beginFrameRendering_0; }
	inline void set_beginFrameRendering_0(Action_1_t1882455329 * value)
	{
		___beginFrameRendering_0 = value;
		Il2CppCodeGenWriteBarrier((&___beginFrameRendering_0), value);
	}

	inline static int32_t get_offset_of_beginCameraRendering_1() { return static_cast<int32_t>(offsetof(RenderPipeline_t957695896_StaticFields, ___beginCameraRendering_1)); }
	inline Action_1_t34654170 * get_beginCameraRendering_1() const { return ___beginCameraRendering_1; }
	inline Action_1_t34654170 ** get_address_of_beginCameraRendering_1() { return &___beginCameraRendering_1; }
	inline void set_beginCameraRendering_1(Action_1_t34654170 * value)
	{
		___beginCameraRendering_1 = value;
		Il2CppCodeGenWriteBarrier((&___beginCameraRendering_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINE_T957695896_H
#ifndef U24ARRAYTYPEU3D16_T3253128245_H
#define U24ARRAYTYPEU3D16_T3253128245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128245 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128245__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128245_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef OPTOUTSTATUS_T3541615854_H
#define OPTOUTSTATUS_T3541615854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct  OptOutStatus_t3541615854 
{
public:
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::optOut
	bool ___optOut_0;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::analyticsEnabled
	bool ___analyticsEnabled_1;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::deviceStatsEnabled
	bool ___deviceStatsEnabled_2;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::limitUserTracking
	bool ___limitUserTracking_3;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/OptOutStatus::performanceReportingEnabled
	bool ___performanceReportingEnabled_4;

public:
	inline static int32_t get_offset_of_optOut_0() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___optOut_0)); }
	inline bool get_optOut_0() const { return ___optOut_0; }
	inline bool* get_address_of_optOut_0() { return &___optOut_0; }
	inline void set_optOut_0(bool value)
	{
		___optOut_0 = value;
	}

	inline static int32_t get_offset_of_analyticsEnabled_1() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___analyticsEnabled_1)); }
	inline bool get_analyticsEnabled_1() const { return ___analyticsEnabled_1; }
	inline bool* get_address_of_analyticsEnabled_1() { return &___analyticsEnabled_1; }
	inline void set_analyticsEnabled_1(bool value)
	{
		___analyticsEnabled_1 = value;
	}

	inline static int32_t get_offset_of_deviceStatsEnabled_2() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___deviceStatsEnabled_2)); }
	inline bool get_deviceStatsEnabled_2() const { return ___deviceStatsEnabled_2; }
	inline bool* get_address_of_deviceStatsEnabled_2() { return &___deviceStatsEnabled_2; }
	inline void set_deviceStatsEnabled_2(bool value)
	{
		___deviceStatsEnabled_2 = value;
	}

	inline static int32_t get_offset_of_limitUserTracking_3() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___limitUserTracking_3)); }
	inline bool get_limitUserTracking_3() const { return ___limitUserTracking_3; }
	inline bool* get_address_of_limitUserTracking_3() { return &___limitUserTracking_3; }
	inline void set_limitUserTracking_3(bool value)
	{
		___limitUserTracking_3 = value;
	}

	inline static int32_t get_offset_of_performanceReportingEnabled_4() { return static_cast<int32_t>(offsetof(OptOutStatus_t3541615854, ___performanceReportingEnabled_4)); }
	inline bool get_performanceReportingEnabled_4() const { return ___performanceReportingEnabled_4; }
	inline bool* get_address_of_performanceReportingEnabled_4() { return &___performanceReportingEnabled_4; }
	inline void set_performanceReportingEnabled_4(bool value)
	{
		___performanceReportingEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct OptOutStatus_t3541615854_marshaled_pinvoke
{
	int32_t ___optOut_0;
	int32_t ___analyticsEnabled_1;
	int32_t ___deviceStatsEnabled_2;
	int32_t ___limitUserTracking_3;
	int32_t ___performanceReportingEnabled_4;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/OptOutStatus
struct OptOutStatus_t3541615854_marshaled_com
{
	int32_t ___optOut_0;
	int32_t ___analyticsEnabled_1;
	int32_t ___deviceStatsEnabled_2;
	int32_t ___limitUserTracking_3;
	int32_t ___performanceReportingEnabled_4;
};
#endif // OPTOUTSTATUS_T3541615854_H
#ifndef REQUESTDATA_T3101179086_H
#define REQUESTDATA_T3101179086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/RequestData
struct  RequestData_t3101179086 
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/RequestData::date
	String_t* ___date_0;

public:
	inline static int32_t get_offset_of_date_0() { return static_cast<int32_t>(offsetof(RequestData_t3101179086, ___date_0)); }
	inline String_t* get_date_0() const { return ___date_0; }
	inline String_t** get_address_of_date_0() { return &___date_0; }
	inline void set_date_0(String_t* value)
	{
		___date_0 = value;
		Il2CppCodeGenWriteBarrier((&___date_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/RequestData
struct RequestData_t3101179086_marshaled_pinvoke
{
	char* ___date_0;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/RequestData
struct RequestData_t3101179086_marshaled_com
{
	Il2CppChar* ___date_0;
};
#endif // REQUESTDATA_T3101179086_H
#ifndef TOKENDATA_T2077495713_H
#define TOKENDATA_T2077495713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/TokenData
struct  TokenData_t2077495713 
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/TokenData::url
	String_t* ___url_0;
	// System.String UnityEngine.Analytics.DataPrivacy/TokenData::token
	String_t* ___token_1;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(TokenData_t2077495713, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_token_1() { return static_cast<int32_t>(offsetof(TokenData_t2077495713, ___token_1)); }
	inline String_t* get_token_1() const { return ___token_1; }
	inline String_t** get_address_of_token_1() { return &___token_1; }
	inline void set_token_1(String_t* value)
	{
		___token_1 = value;
		Il2CppCodeGenWriteBarrier((&___token_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/TokenData
struct TokenData_t2077495713_marshaled_pinvoke
{
	char* ___url_0;
	char* ___token_1;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/TokenData
struct TokenData_t2077495713_marshaled_com
{
	Il2CppChar* ___url_0;
	Il2CppChar* ___token_1;
};
#endif // TOKENDATA_T2077495713_H
#ifndef USERPOSTDATA_T478425489_H
#define USERPOSTDATA_T478425489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/UserPostData
struct  UserPostData_t478425489 
{
public:
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::appid
	String_t* ___appid_0;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::userid
	String_t* ___userid_1;
	// System.Int64 UnityEngine.Analytics.DataPrivacy/UserPostData::sessionid
	int64_t ___sessionid_2;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::platform
	String_t* ___platform_3;
	// System.UInt32 UnityEngine.Analytics.DataPrivacy/UserPostData::platformid
	uint32_t ___platformid_4;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::sdk_ver
	String_t* ___sdk_ver_5;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/UserPostData::debug_device
	bool ___debug_device_6;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::deviceid
	String_t* ___deviceid_7;
	// System.String UnityEngine.Analytics.DataPrivacy/UserPostData::plugin_ver
	String_t* ___plugin_ver_8;

public:
	inline static int32_t get_offset_of_appid_0() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___appid_0)); }
	inline String_t* get_appid_0() const { return ___appid_0; }
	inline String_t** get_address_of_appid_0() { return &___appid_0; }
	inline void set_appid_0(String_t* value)
	{
		___appid_0 = value;
		Il2CppCodeGenWriteBarrier((&___appid_0), value);
	}

	inline static int32_t get_offset_of_userid_1() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___userid_1)); }
	inline String_t* get_userid_1() const { return ___userid_1; }
	inline String_t** get_address_of_userid_1() { return &___userid_1; }
	inline void set_userid_1(String_t* value)
	{
		___userid_1 = value;
		Il2CppCodeGenWriteBarrier((&___userid_1), value);
	}

	inline static int32_t get_offset_of_sessionid_2() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___sessionid_2)); }
	inline int64_t get_sessionid_2() const { return ___sessionid_2; }
	inline int64_t* get_address_of_sessionid_2() { return &___sessionid_2; }
	inline void set_sessionid_2(int64_t value)
	{
		___sessionid_2 = value;
	}

	inline static int32_t get_offset_of_platform_3() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___platform_3)); }
	inline String_t* get_platform_3() const { return ___platform_3; }
	inline String_t** get_address_of_platform_3() { return &___platform_3; }
	inline void set_platform_3(String_t* value)
	{
		___platform_3 = value;
		Il2CppCodeGenWriteBarrier((&___platform_3), value);
	}

	inline static int32_t get_offset_of_platformid_4() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___platformid_4)); }
	inline uint32_t get_platformid_4() const { return ___platformid_4; }
	inline uint32_t* get_address_of_platformid_4() { return &___platformid_4; }
	inline void set_platformid_4(uint32_t value)
	{
		___platformid_4 = value;
	}

	inline static int32_t get_offset_of_sdk_ver_5() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___sdk_ver_5)); }
	inline String_t* get_sdk_ver_5() const { return ___sdk_ver_5; }
	inline String_t** get_address_of_sdk_ver_5() { return &___sdk_ver_5; }
	inline void set_sdk_ver_5(String_t* value)
	{
		___sdk_ver_5 = value;
		Il2CppCodeGenWriteBarrier((&___sdk_ver_5), value);
	}

	inline static int32_t get_offset_of_debug_device_6() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___debug_device_6)); }
	inline bool get_debug_device_6() const { return ___debug_device_6; }
	inline bool* get_address_of_debug_device_6() { return &___debug_device_6; }
	inline void set_debug_device_6(bool value)
	{
		___debug_device_6 = value;
	}

	inline static int32_t get_offset_of_deviceid_7() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___deviceid_7)); }
	inline String_t* get_deviceid_7() const { return ___deviceid_7; }
	inline String_t** get_address_of_deviceid_7() { return &___deviceid_7; }
	inline void set_deviceid_7(String_t* value)
	{
		___deviceid_7 = value;
		Il2CppCodeGenWriteBarrier((&___deviceid_7), value);
	}

	inline static int32_t get_offset_of_plugin_ver_8() { return static_cast<int32_t>(offsetof(UserPostData_t478425489, ___plugin_ver_8)); }
	inline String_t* get_plugin_ver_8() const { return ___plugin_ver_8; }
	inline String_t** get_address_of_plugin_ver_8() { return &___plugin_ver_8; }
	inline void set_plugin_ver_8(String_t* value)
	{
		___plugin_ver_8 = value;
		Il2CppCodeGenWriteBarrier((&___plugin_ver_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/UserPostData
struct UserPostData_t478425489_marshaled_pinvoke
{
	char* ___appid_0;
	char* ___userid_1;
	int64_t ___sessionid_2;
	char* ___platform_3;
	uint32_t ___platformid_4;
	char* ___sdk_ver_5;
	int32_t ___debug_device_6;
	char* ___deviceid_7;
	char* ___plugin_ver_8;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/UserPostData
struct UserPostData_t478425489_marshaled_com
{
	Il2CppChar* ___appid_0;
	Il2CppChar* ___userid_1;
	int64_t ___sessionid_2;
	Il2CppChar* ___platform_3;
	uint32_t ___platformid_4;
	Il2CppChar* ___sdk_ver_5;
	int32_t ___debug_device_6;
	Il2CppChar* ___deviceid_7;
	Il2CppChar* ___plugin_ver_8;
};
#endif // USERPOSTDATA_T478425489_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BEGINXRRENDERINGPASS_T4122675669_H
#define BEGINXRRENDERINGPASS_T4122675669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.BeginXRRenderingPass
struct  BeginXRRenderingPass_t4122675669  : public ScriptableRenderPass_t3231910016
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEGINXRRENDERINGPASS_T4122675669_H
#ifndef DRAWSKYBOXPASS_T705056231_H
#define DRAWSKYBOXPASS_T705056231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.DrawSkyboxPass
struct  DrawSkyboxPass_t705056231  : public ScriptableRenderPass_t3231910016
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWSKYBOXPASS_T705056231_H
#ifndef ENDXRRENDERINGPASS_T1600478297_H
#define ENDXRRENDERINGPASS_T1600478297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.EndXRRenderingPass
struct  EndXRRenderingPass_t1600478297  : public ScriptableRenderPass_t3231910016
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDXRRENDERINGPASS_T1600478297_H
#ifndef LIGHTDATA_T718835846_H
#define LIGHTDATA_T718835846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightData
struct  LightData_t718835846 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightData::pixelAdditionalLightsCount
	int32_t ___pixelAdditionalLightsCount_0;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightData::totalAdditionalLightsCount
	int32_t ___totalAdditionalLightsCount_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightData::mainLightIndex
	int32_t ___mainLightIndex_2;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VisibleLight> UnityEngine.Experimental.Rendering.LightweightPipeline.LightData::visibleLights
	List_1_t2176140814 * ___visibleLights_3;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Experimental.Rendering.LightweightPipeline.LightData::visibleLocalLightIndices
	List_1_t128053199 * ___visibleLocalLightIndices_4;

public:
	inline static int32_t get_offset_of_pixelAdditionalLightsCount_0() { return static_cast<int32_t>(offsetof(LightData_t718835846, ___pixelAdditionalLightsCount_0)); }
	inline int32_t get_pixelAdditionalLightsCount_0() const { return ___pixelAdditionalLightsCount_0; }
	inline int32_t* get_address_of_pixelAdditionalLightsCount_0() { return &___pixelAdditionalLightsCount_0; }
	inline void set_pixelAdditionalLightsCount_0(int32_t value)
	{
		___pixelAdditionalLightsCount_0 = value;
	}

	inline static int32_t get_offset_of_totalAdditionalLightsCount_1() { return static_cast<int32_t>(offsetof(LightData_t718835846, ___totalAdditionalLightsCount_1)); }
	inline int32_t get_totalAdditionalLightsCount_1() const { return ___totalAdditionalLightsCount_1; }
	inline int32_t* get_address_of_totalAdditionalLightsCount_1() { return &___totalAdditionalLightsCount_1; }
	inline void set_totalAdditionalLightsCount_1(int32_t value)
	{
		___totalAdditionalLightsCount_1 = value;
	}

	inline static int32_t get_offset_of_mainLightIndex_2() { return static_cast<int32_t>(offsetof(LightData_t718835846, ___mainLightIndex_2)); }
	inline int32_t get_mainLightIndex_2() const { return ___mainLightIndex_2; }
	inline int32_t* get_address_of_mainLightIndex_2() { return &___mainLightIndex_2; }
	inline void set_mainLightIndex_2(int32_t value)
	{
		___mainLightIndex_2 = value;
	}

	inline static int32_t get_offset_of_visibleLights_3() { return static_cast<int32_t>(offsetof(LightData_t718835846, ___visibleLights_3)); }
	inline List_1_t2176140814 * get_visibleLights_3() const { return ___visibleLights_3; }
	inline List_1_t2176140814 ** get_address_of_visibleLights_3() { return &___visibleLights_3; }
	inline void set_visibleLights_3(List_1_t2176140814 * value)
	{
		___visibleLights_3 = value;
		Il2CppCodeGenWriteBarrier((&___visibleLights_3), value);
	}

	inline static int32_t get_offset_of_visibleLocalLightIndices_4() { return static_cast<int32_t>(offsetof(LightData_t718835846, ___visibleLocalLightIndices_4)); }
	inline List_1_t128053199 * get_visibleLocalLightIndices_4() const { return ___visibleLocalLightIndices_4; }
	inline List_1_t128053199 ** get_address_of_visibleLocalLightIndices_4() { return &___visibleLocalLightIndices_4; }
	inline void set_visibleLocalLightIndices_4(List_1_t128053199 * value)
	{
		___visibleLocalLightIndices_4 = value;
		Il2CppCodeGenWriteBarrier((&___visibleLocalLightIndices_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.LightData
struct LightData_t718835846_marshaled_pinvoke
{
	int32_t ___pixelAdditionalLightsCount_0;
	int32_t ___totalAdditionalLightsCount_1;
	int32_t ___mainLightIndex_2;
	List_1_t2176140814 * ___visibleLights_3;
	List_1_t128053199 * ___visibleLocalLightIndices_4;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.LightData
struct LightData_t718835846_marshaled_com
{
	int32_t ___pixelAdditionalLightsCount_0;
	int32_t ___totalAdditionalLightsCount_1;
	int32_t ___mainLightIndex_2;
	List_1_t2176140814 * ___visibleLights_3;
	List_1_t128053199 * ___visibleLocalLightIndices_4;
};
#endif // LIGHTDATA_T718835846_H
#ifndef RENDERTARGETHANDLE_T3987358924_H
#define RENDERTARGETHANDLE_T3987358924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle
struct  RenderTargetHandle_t3987358924 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenderTargetHandle_t3987358924, ___U3CidU3Ek__BackingField_0)); }
	inline int32_t get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(int32_t value)
	{
		___U3CidU3Ek__BackingField_0 = value;
	}
};

struct RenderTargetHandle_t3987358924_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle::CameraTarget
	RenderTargetHandle_t3987358924  ___CameraTarget_1;

public:
	inline static int32_t get_offset_of_CameraTarget_1() { return static_cast<int32_t>(offsetof(RenderTargetHandle_t3987358924_StaticFields, ___CameraTarget_1)); }
	inline RenderTargetHandle_t3987358924  get_CameraTarget_1() const { return ___CameraTarget_1; }
	inline RenderTargetHandle_t3987358924 * get_address_of_CameraTarget_1() { return &___CameraTarget_1; }
	inline void set_CameraTarget_1(RenderTargetHandle_t3987358924  value)
	{
		___CameraTarget_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETHANDLE_T3987358924_H
#ifndef SETUPFORWARDRENDERINGPASS_T3177828444_H
#define SETUPFORWARDRENDERINGPASS_T3177828444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupForwardRenderingPass
struct  SetupForwardRenderingPass_t3177828444  : public ScriptableRenderPass_t3231910016
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPFORWARDRENDERINGPASS_T3177828444_H
#ifndef RENDERQUEUERANGE_T2460280767_H
#define RENDERQUEUERANGE_T2460280767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderQueueRange
struct  RenderQueueRange_t2460280767 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.RenderQueueRange::min
	int32_t ___min_0;
	// System.Int32 UnityEngine.Experimental.Rendering.RenderQueueRange::max
	int32_t ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RenderQueueRange_t2460280767, ___min_0)); }
	inline int32_t get_min_0() const { return ___min_0; }
	inline int32_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int32_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RenderQueueRange_t2460280767, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERQUEUERANGE_T2460280767_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255374_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255374  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-898C2022A0C02FCE602BF05E1C09BD48301606E5
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-295DE5F80AAB8DD796EAD8330F4E26A70AC95009
	U24ArrayTypeU3D16_t3253128245  ___U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields, ___U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1)); }
	inline U24ArrayTypeU3D16_t3253128245  get_U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1() const { return ___U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1; }
	inline U24ArrayTypeU3D16_t3253128245 * get_address_of_U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1() { return &___U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1; }
	inline void set_U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1(U24ArrayTypeU3D16_t3253128245  value)
	{
		___U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255374_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef U3CFETCHOPTOUTSTATUSCOROUTINEU3EC__ITERATOR0_T2643221616_H
#define U3CFETCHOPTOUTSTATUSCOROUTINEU3EC__ITERATOR0_T2643221616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0
struct  U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<localOptOutStatus>__0
	OptOutStatus_t3541615854  ___U3ClocalOptOutStatusU3E__0_0;
	// UnityEngine.Analytics.DataPrivacy/UserPostData UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<userData>__0
	UserPostData_t478425489  ___U3CuserDataU3E__0_1;
	// System.String UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<query>__0
	String_t* ___U3CqueryU3E__0_2;
	// System.Uri UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<baseUri>__0
	Uri_t100236324 * ___U3CbaseUriU3E__0_3;
	// System.Uri UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<uri>__0
	Uri_t100236324 * ___U3CuriU3E__0_4;
	// UnityEngine.WWW UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_5;
	// System.Action`1<System.Boolean> UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::optOutAction
	Action_1_t269755560 * ___optOutAction_6;
	// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::<optOutStatus>__1
	OptOutStatus_t3541615854  ___U3CoptOutStatusU3E__1_7;
	// System.Object UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 UnityEngine.Analytics.DataPrivacy/<FetchOptOutStatusCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3ClocalOptOutStatusU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3ClocalOptOutStatusU3E__0_0)); }
	inline OptOutStatus_t3541615854  get_U3ClocalOptOutStatusU3E__0_0() const { return ___U3ClocalOptOutStatusU3E__0_0; }
	inline OptOutStatus_t3541615854 * get_address_of_U3ClocalOptOutStatusU3E__0_0() { return &___U3ClocalOptOutStatusU3E__0_0; }
	inline void set_U3ClocalOptOutStatusU3E__0_0(OptOutStatus_t3541615854  value)
	{
		___U3ClocalOptOutStatusU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CuserDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CuserDataU3E__0_1)); }
	inline UserPostData_t478425489  get_U3CuserDataU3E__0_1() const { return ___U3CuserDataU3E__0_1; }
	inline UserPostData_t478425489 * get_address_of_U3CuserDataU3E__0_1() { return &___U3CuserDataU3E__0_1; }
	inline void set_U3CuserDataU3E__0_1(UserPostData_t478425489  value)
	{
		___U3CuserDataU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CqueryU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CqueryU3E__0_2)); }
	inline String_t* get_U3CqueryU3E__0_2() const { return ___U3CqueryU3E__0_2; }
	inline String_t** get_address_of_U3CqueryU3E__0_2() { return &___U3CqueryU3E__0_2; }
	inline void set_U3CqueryU3E__0_2(String_t* value)
	{
		___U3CqueryU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CqueryU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CbaseUriU3E__0_3() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CbaseUriU3E__0_3)); }
	inline Uri_t100236324 * get_U3CbaseUriU3E__0_3() const { return ___U3CbaseUriU3E__0_3; }
	inline Uri_t100236324 ** get_address_of_U3CbaseUriU3E__0_3() { return &___U3CbaseUriU3E__0_3; }
	inline void set_U3CbaseUriU3E__0_3(Uri_t100236324 * value)
	{
		___U3CbaseUriU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbaseUriU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CuriU3E__0_4() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CuriU3E__0_4)); }
	inline Uri_t100236324 * get_U3CuriU3E__0_4() const { return ___U3CuriU3E__0_4; }
	inline Uri_t100236324 ** get_address_of_U3CuriU3E__0_4() { return &___U3CuriU3E__0_4; }
	inline void set_U3CuriU3E__0_4(Uri_t100236324 * value)
	{
		___U3CuriU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuriU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_5() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CwwwU3E__0_5)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_5() const { return ___U3CwwwU3E__0_5; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_5() { return &___U3CwwwU3E__0_5; }
	inline void set_U3CwwwU3E__0_5(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_5), value);
	}

	inline static int32_t get_offset_of_optOutAction_6() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___optOutAction_6)); }
	inline Action_1_t269755560 * get_optOutAction_6() const { return ___optOutAction_6; }
	inline Action_1_t269755560 ** get_address_of_optOutAction_6() { return &___optOutAction_6; }
	inline void set_optOutAction_6(Action_1_t269755560 * value)
	{
		___optOutAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___optOutAction_6), value);
	}

	inline static int32_t get_offset_of_U3CoptOutStatusU3E__1_7() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U3CoptOutStatusU3E__1_7)); }
	inline OptOutStatus_t3541615854  get_U3CoptOutStatusU3E__1_7() const { return ___U3CoptOutStatusU3E__1_7; }
	inline OptOutStatus_t3541615854 * get_address_of_U3CoptOutStatusU3E__1_7() { return &___U3CoptOutStatusU3E__1_7; }
	inline void set_U3CoptOutStatusU3E__1_7(OptOutStatus_t3541615854  value)
	{
		___U3CoptOutStatusU3E__1_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHOPTOUTSTATUSCOROUTINEU3EC__ITERATOR0_T2643221616_H
#ifndef OPTOUTRESPONSE_T2266495071_H
#define OPTOUTRESPONSE_T2266495071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy/OptOutResponse
struct  OptOutResponse_t2266495071 
{
public:
	// UnityEngine.Analytics.DataPrivacy/RequestData UnityEngine.Analytics.DataPrivacy/OptOutResponse::request
	RequestData_t3101179086  ___request_0;
	// UnityEngine.Analytics.DataPrivacy/OptOutStatus UnityEngine.Analytics.DataPrivacy/OptOutResponse::status
	OptOutStatus_t3541615854  ___status_1;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(OptOutResponse_t2266495071, ___request_0)); }
	inline RequestData_t3101179086  get_request_0() const { return ___request_0; }
	inline RequestData_t3101179086 * get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RequestData_t3101179086  value)
	{
		___request_0 = value;
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(OptOutResponse_t2266495071, ___status_1)); }
	inline OptOutStatus_t3541615854  get_status_1() const { return ___status_1; }
	inline OptOutStatus_t3541615854 * get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(OptOutStatus_t3541615854  value)
	{
		___status_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.DataPrivacy/OptOutResponse
struct OptOutResponse_t2266495071_marshaled_pinvoke
{
	RequestData_t3101179086_marshaled_pinvoke ___request_0;
	OptOutStatus_t3541615854_marshaled_pinvoke ___status_1;
};
// Native definition for COM marshalling of UnityEngine.Analytics.DataPrivacy/OptOutResponse
struct OptOutResponse_t2266495071_marshaled_com
{
	RequestData_t3101179086_marshaled_com ___request_0;
	OptOutStatus_t3541615854_marshaled_com ___status_1;
};
#endif // OPTOUTRESPONSE_T2266495071_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef CLEARFLAG_T2207768290_H
#define CLEARFLAG_T2207768290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ClearFlag
struct  ClearFlag_t2207768290 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.ClearFlag::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClearFlag_t2207768290, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLEARFLAG_T2207768290_H
#ifndef FILTERRENDERERSSETTINGS_T2384854789_H
#define FILTERRENDERERSSETTINGS_T2384854789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.FilterRenderersSettings
struct  FilterRenderersSettings_t2384854789 
{
public:
	// UnityEngine.Experimental.Rendering.RenderQueueRange UnityEngine.Experimental.Rendering.FilterRenderersSettings::m_RenderQueueRange
	RenderQueueRange_t2460280767  ___m_RenderQueueRange_0;
	// System.Int32 UnityEngine.Experimental.Rendering.FilterRenderersSettings::m_LayerMask
	int32_t ___m_LayerMask_1;
	// System.UInt32 UnityEngine.Experimental.Rendering.FilterRenderersSettings::m_RenderingLayerMask
	uint32_t ___m_RenderingLayerMask_2;

public:
	inline static int32_t get_offset_of_m_RenderQueueRange_0() { return static_cast<int32_t>(offsetof(FilterRenderersSettings_t2384854789, ___m_RenderQueueRange_0)); }
	inline RenderQueueRange_t2460280767  get_m_RenderQueueRange_0() const { return ___m_RenderQueueRange_0; }
	inline RenderQueueRange_t2460280767 * get_address_of_m_RenderQueueRange_0() { return &___m_RenderQueueRange_0; }
	inline void set_m_RenderQueueRange_0(RenderQueueRange_t2460280767  value)
	{
		___m_RenderQueueRange_0 = value;
	}

	inline static int32_t get_offset_of_m_LayerMask_1() { return static_cast<int32_t>(offsetof(FilterRenderersSettings_t2384854789, ___m_LayerMask_1)); }
	inline int32_t get_m_LayerMask_1() const { return ___m_LayerMask_1; }
	inline int32_t* get_address_of_m_LayerMask_1() { return &___m_LayerMask_1; }
	inline void set_m_LayerMask_1(int32_t value)
	{
		___m_LayerMask_1 = value;
	}

	inline static int32_t get_offset_of_m_RenderingLayerMask_2() { return static_cast<int32_t>(offsetof(FilterRenderersSettings_t2384854789, ___m_RenderingLayerMask_2)); }
	inline uint32_t get_m_RenderingLayerMask_2() const { return ___m_RenderingLayerMask_2; }
	inline uint32_t* get_address_of_m_RenderingLayerMask_2() { return &___m_RenderingLayerMask_2; }
	inline void set_m_RenderingLayerMask_2(uint32_t value)
	{
		___m_RenderingLayerMask_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERRENDERERSSETTINGS_T2384854789_H
#ifndef FILTERRESULTS_T2595222798_H
#define FILTERRESULTS_T2595222798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.FilterResults
struct  FilterResults_t2595222798 
{
public:
	// System.IntPtr UnityEngine.Experimental.Rendering.FilterResults::m_CullResults
	intptr_t ___m_CullResults_0;

public:
	inline static int32_t get_offset_of_m_CullResults_0() { return static_cast<int32_t>(offsetof(FilterResults_t2595222798, ___m_CullResults_0)); }
	inline intptr_t get_m_CullResults_0() const { return ___m_CullResults_0; }
	inline intptr_t* get_address_of_m_CullResults_0() { return &___m_CullResults_0; }
	inline void set_m_CullResults_0(intptr_t value)
	{
		___m_CullResults_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERRESULTS_T2595222798_H
#ifndef COPYCOLORPASS_T3734904897_H
#define COPYCOLORPASS_T3734904897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass
struct  CopyColorPass_t3734904897  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Material UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass::m_SamplingMaterial
	Material_t340375123 * ___m_SamplingMaterial_2;
	// System.Single[] UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass::m_OpaqueScalerValues
	SingleU5BU5D_t1444911251* ___m_OpaqueScalerValues_3;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass::m_SampleOffsetShaderHandle
	int32_t ___m_SampleOffsetShaderHandle_4;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass::<source>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CsourceU3Ek__BackingField_5;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass::<destination>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdestinationU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_SamplingMaterial_2() { return static_cast<int32_t>(offsetof(CopyColorPass_t3734904897, ___m_SamplingMaterial_2)); }
	inline Material_t340375123 * get_m_SamplingMaterial_2() const { return ___m_SamplingMaterial_2; }
	inline Material_t340375123 ** get_address_of_m_SamplingMaterial_2() { return &___m_SamplingMaterial_2; }
	inline void set_m_SamplingMaterial_2(Material_t340375123 * value)
	{
		___m_SamplingMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SamplingMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_OpaqueScalerValues_3() { return static_cast<int32_t>(offsetof(CopyColorPass_t3734904897, ___m_OpaqueScalerValues_3)); }
	inline SingleU5BU5D_t1444911251* get_m_OpaqueScalerValues_3() const { return ___m_OpaqueScalerValues_3; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_OpaqueScalerValues_3() { return &___m_OpaqueScalerValues_3; }
	inline void set_m_OpaqueScalerValues_3(SingleU5BU5D_t1444911251* value)
	{
		___m_OpaqueScalerValues_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpaqueScalerValues_3), value);
	}

	inline static int32_t get_offset_of_m_SampleOffsetShaderHandle_4() { return static_cast<int32_t>(offsetof(CopyColorPass_t3734904897, ___m_SampleOffsetShaderHandle_4)); }
	inline int32_t get_m_SampleOffsetShaderHandle_4() const { return ___m_SampleOffsetShaderHandle_4; }
	inline int32_t* get_address_of_m_SampleOffsetShaderHandle_4() { return &___m_SampleOffsetShaderHandle_4; }
	inline void set_m_SampleOffsetShaderHandle_4(int32_t value)
	{
		___m_SampleOffsetShaderHandle_4 = value;
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CopyColorPass_t3734904897, ___U3CsourceU3Ek__BackingField_5)); }
	inline RenderTargetHandle_t3987358924  get_U3CsourceU3Ek__BackingField_5() const { return ___U3CsourceU3Ek__BackingField_5; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CsourceU3Ek__BackingField_5() { return &___U3CsourceU3Ek__BackingField_5; }
	inline void set_U3CsourceU3Ek__BackingField_5(RenderTargetHandle_t3987358924  value)
	{
		___U3CsourceU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdestinationU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CopyColorPass_t3734904897, ___U3CdestinationU3Ek__BackingField_6)); }
	inline RenderTargetHandle_t3987358924  get_U3CdestinationU3Ek__BackingField_6() const { return ___U3CdestinationU3Ek__BackingField_6; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdestinationU3Ek__BackingField_6() { return &___U3CdestinationU3Ek__BackingField_6; }
	inline void set_U3CdestinationU3Ek__BackingField_6(RenderTargetHandle_t3987358924  value)
	{
		___U3CdestinationU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYCOLORPASS_T3734904897_H
#ifndef COPYDEPTHPASS_T304079192_H
#define COPYDEPTHPASS_T304079192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.CopyDepthPass
struct  CopyDepthPass_t304079192  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Material UnityEngine.Experimental.Rendering.LightweightPipeline.CopyDepthPass::m_DepthCopyMaterial
	Material_t340375123 * ___m_DepthCopyMaterial_2;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.CopyDepthPass::<source>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CsourceU3Ek__BackingField_3;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.CopyDepthPass::<destination>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdestinationU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_DepthCopyMaterial_2() { return static_cast<int32_t>(offsetof(CopyDepthPass_t304079192, ___m_DepthCopyMaterial_2)); }
	inline Material_t340375123 * get_m_DepthCopyMaterial_2() const { return ___m_DepthCopyMaterial_2; }
	inline Material_t340375123 ** get_address_of_m_DepthCopyMaterial_2() { return &___m_DepthCopyMaterial_2; }
	inline void set_m_DepthCopyMaterial_2(Material_t340375123 * value)
	{
		___m_DepthCopyMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthCopyMaterial_2), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CopyDepthPass_t304079192, ___U3CsourceU3Ek__BackingField_3)); }
	inline RenderTargetHandle_t3987358924  get_U3CsourceU3Ek__BackingField_3() const { return ___U3CsourceU3Ek__BackingField_3; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CsourceU3Ek__BackingField_3() { return &___U3CsourceU3Ek__BackingField_3; }
	inline void set_U3CsourceU3Ek__BackingField_3(RenderTargetHandle_t3987358924  value)
	{
		___U3CsourceU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdestinationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CopyDepthPass_t304079192, ___U3CdestinationU3Ek__BackingField_4)); }
	inline RenderTargetHandle_t3987358924  get_U3CdestinationU3Ek__BackingField_4() const { return ___U3CdestinationU3Ek__BackingField_4; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdestinationU3Ek__BackingField_4() { return &___U3CdestinationU3Ek__BackingField_4; }
	inline void set_U3CdestinationU3Ek__BackingField_4(RenderTargetHandle_t3987358924  value)
	{
		___U3CdestinationU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYDEPTHPASS_T304079192_H
#ifndef DEFAULTMATERIALTYPE_T2904751804_H
#define DEFAULTMATERIALTYPE_T2904751804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultMaterialType
struct  DefaultMaterialType_t2904751804 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultMaterialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultMaterialType_t2904751804, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMATERIALTYPE_T2904751804_H
#ifndef DEFAULTRENDERERSETUP_T3647655062_H
#define DEFAULTRENDERERSETUP_T3647655062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup
struct  DefaultRendererSetup_t3647655062  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.DepthOnlyPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_DepthOnlyPass
	DepthOnlyPass_t4004473585 * ___m_DepthOnlyPass_0;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_DirectionalShadowPass
	DirectionalShadowsPass_t4026755168 * ___m_DirectionalShadowPass_1;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_LocalShadowPass
	LocalShadowsPass_t2703335903 * ___m_LocalShadowPass_2;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupForwardRenderingPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_SetupForwardRenderingPass
	SetupForwardRenderingPass_t3177828444 * ___m_SetupForwardRenderingPass_3;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_ScreenSpaceShadowResovePass
	ScreenSpaceShadowResolvePass_t442348744 * ___m_ScreenSpaceShadowResovePass_4;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_CreateLightweightRenderTexturesPass
	CreateLightweightRenderTexturesPass_t2359216577 * ___m_CreateLightweightRenderTexturesPass_5;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.BeginXRRenderingPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_BeginXrRenderingPass
	BeginXRRenderingPass_t4122675669 * ___m_BeginXrRenderingPass_6;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_SetupLightweightConstants
	SetupLightweightConstanstPass_t1972706453 * ___m_SetupLightweightConstants_7;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderOpaqueForwardPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_RenderOpaqueForwardPass
	RenderOpaqueForwardPass_t31084166 * ___m_RenderOpaqueForwardPass_8;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.OpaquePostProcessPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_OpaquePostProcessPass
	OpaquePostProcessPass_t1918180670 * ___m_OpaquePostProcessPass_9;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.DrawSkyboxPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_DrawSkyboxPass
	DrawSkyboxPass_t705056231 * ___m_DrawSkyboxPass_10;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.CopyDepthPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_CopyDepthPass
	CopyDepthPass_t304079192 * ___m_CopyDepthPass_11;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.CopyColorPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_CopyColorPass
	CopyColorPass_t3734904897 * ___m_CopyColorPass_12;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTransparentForwardPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_RenderTransparentForwardPass
	RenderTransparentForwardPass_t1241286203 * ___m_RenderTransparentForwardPass_13;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.TransparentPostProcessPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_TransparentPostProcessPass
	TransparentPostProcessPass_t4286252520 * ___m_TransparentPostProcessPass_14;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.FinalBlitPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_FinalBlitPass
	FinalBlitPass_t30679461 * ___m_FinalBlitPass_15;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.EndXRRenderingPass UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_EndXrRenderingPass
	EndXRRenderingPass_t1600478297 * ___m_EndXrRenderingPass_16;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::Color
	RenderTargetHandle_t3987358924  ___Color_17;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::DepthAttachment
	RenderTargetHandle_t3987358924  ___DepthAttachment_18;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::DepthTexture
	RenderTargetHandle_t3987358924  ___DepthTexture_19;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::OpaqueColor
	RenderTargetHandle_t3987358924  ___OpaqueColor_20;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::DirectionalShadowmap
	RenderTargetHandle_t3987358924  ___DirectionalShadowmap_21;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::LocalShadowmap
	RenderTargetHandle_t3987358924  ___LocalShadowmap_22;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::ScreenSpaceShadowmap
	RenderTargetHandle_t3987358924  ___ScreenSpaceShadowmap_23;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.DefaultRendererSetup::m_Initialized
	bool ___m_Initialized_24;

public:
	inline static int32_t get_offset_of_m_DepthOnlyPass_0() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_DepthOnlyPass_0)); }
	inline DepthOnlyPass_t4004473585 * get_m_DepthOnlyPass_0() const { return ___m_DepthOnlyPass_0; }
	inline DepthOnlyPass_t4004473585 ** get_address_of_m_DepthOnlyPass_0() { return &___m_DepthOnlyPass_0; }
	inline void set_m_DepthOnlyPass_0(DepthOnlyPass_t4004473585 * value)
	{
		___m_DepthOnlyPass_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthOnlyPass_0), value);
	}

	inline static int32_t get_offset_of_m_DirectionalShadowPass_1() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_DirectionalShadowPass_1)); }
	inline DirectionalShadowsPass_t4026755168 * get_m_DirectionalShadowPass_1() const { return ___m_DirectionalShadowPass_1; }
	inline DirectionalShadowsPass_t4026755168 ** get_address_of_m_DirectionalShadowPass_1() { return &___m_DirectionalShadowPass_1; }
	inline void set_m_DirectionalShadowPass_1(DirectionalShadowsPass_t4026755168 * value)
	{
		___m_DirectionalShadowPass_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DirectionalShadowPass_1), value);
	}

	inline static int32_t get_offset_of_m_LocalShadowPass_2() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_LocalShadowPass_2)); }
	inline LocalShadowsPass_t2703335903 * get_m_LocalShadowPass_2() const { return ___m_LocalShadowPass_2; }
	inline LocalShadowsPass_t2703335903 ** get_address_of_m_LocalShadowPass_2() { return &___m_LocalShadowPass_2; }
	inline void set_m_LocalShadowPass_2(LocalShadowsPass_t2703335903 * value)
	{
		___m_LocalShadowPass_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalShadowPass_2), value);
	}

	inline static int32_t get_offset_of_m_SetupForwardRenderingPass_3() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_SetupForwardRenderingPass_3)); }
	inline SetupForwardRenderingPass_t3177828444 * get_m_SetupForwardRenderingPass_3() const { return ___m_SetupForwardRenderingPass_3; }
	inline SetupForwardRenderingPass_t3177828444 ** get_address_of_m_SetupForwardRenderingPass_3() { return &___m_SetupForwardRenderingPass_3; }
	inline void set_m_SetupForwardRenderingPass_3(SetupForwardRenderingPass_t3177828444 * value)
	{
		___m_SetupForwardRenderingPass_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SetupForwardRenderingPass_3), value);
	}

	inline static int32_t get_offset_of_m_ScreenSpaceShadowResovePass_4() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_ScreenSpaceShadowResovePass_4)); }
	inline ScreenSpaceShadowResolvePass_t442348744 * get_m_ScreenSpaceShadowResovePass_4() const { return ___m_ScreenSpaceShadowResovePass_4; }
	inline ScreenSpaceShadowResolvePass_t442348744 ** get_address_of_m_ScreenSpaceShadowResovePass_4() { return &___m_ScreenSpaceShadowResovePass_4; }
	inline void set_m_ScreenSpaceShadowResovePass_4(ScreenSpaceShadowResolvePass_t442348744 * value)
	{
		___m_ScreenSpaceShadowResovePass_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScreenSpaceShadowResovePass_4), value);
	}

	inline static int32_t get_offset_of_m_CreateLightweightRenderTexturesPass_5() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_CreateLightweightRenderTexturesPass_5)); }
	inline CreateLightweightRenderTexturesPass_t2359216577 * get_m_CreateLightweightRenderTexturesPass_5() const { return ___m_CreateLightweightRenderTexturesPass_5; }
	inline CreateLightweightRenderTexturesPass_t2359216577 ** get_address_of_m_CreateLightweightRenderTexturesPass_5() { return &___m_CreateLightweightRenderTexturesPass_5; }
	inline void set_m_CreateLightweightRenderTexturesPass_5(CreateLightweightRenderTexturesPass_t2359216577 * value)
	{
		___m_CreateLightweightRenderTexturesPass_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreateLightweightRenderTexturesPass_5), value);
	}

	inline static int32_t get_offset_of_m_BeginXrRenderingPass_6() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_BeginXrRenderingPass_6)); }
	inline BeginXRRenderingPass_t4122675669 * get_m_BeginXrRenderingPass_6() const { return ___m_BeginXrRenderingPass_6; }
	inline BeginXRRenderingPass_t4122675669 ** get_address_of_m_BeginXrRenderingPass_6() { return &___m_BeginXrRenderingPass_6; }
	inline void set_m_BeginXrRenderingPass_6(BeginXRRenderingPass_t4122675669 * value)
	{
		___m_BeginXrRenderingPass_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_BeginXrRenderingPass_6), value);
	}

	inline static int32_t get_offset_of_m_SetupLightweightConstants_7() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_SetupLightweightConstants_7)); }
	inline SetupLightweightConstanstPass_t1972706453 * get_m_SetupLightweightConstants_7() const { return ___m_SetupLightweightConstants_7; }
	inline SetupLightweightConstanstPass_t1972706453 ** get_address_of_m_SetupLightweightConstants_7() { return &___m_SetupLightweightConstants_7; }
	inline void set_m_SetupLightweightConstants_7(SetupLightweightConstanstPass_t1972706453 * value)
	{
		___m_SetupLightweightConstants_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SetupLightweightConstants_7), value);
	}

	inline static int32_t get_offset_of_m_RenderOpaqueForwardPass_8() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_RenderOpaqueForwardPass_8)); }
	inline RenderOpaqueForwardPass_t31084166 * get_m_RenderOpaqueForwardPass_8() const { return ___m_RenderOpaqueForwardPass_8; }
	inline RenderOpaqueForwardPass_t31084166 ** get_address_of_m_RenderOpaqueForwardPass_8() { return &___m_RenderOpaqueForwardPass_8; }
	inline void set_m_RenderOpaqueForwardPass_8(RenderOpaqueForwardPass_t31084166 * value)
	{
		___m_RenderOpaqueForwardPass_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RenderOpaqueForwardPass_8), value);
	}

	inline static int32_t get_offset_of_m_OpaquePostProcessPass_9() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_OpaquePostProcessPass_9)); }
	inline OpaquePostProcessPass_t1918180670 * get_m_OpaquePostProcessPass_9() const { return ___m_OpaquePostProcessPass_9; }
	inline OpaquePostProcessPass_t1918180670 ** get_address_of_m_OpaquePostProcessPass_9() { return &___m_OpaquePostProcessPass_9; }
	inline void set_m_OpaquePostProcessPass_9(OpaquePostProcessPass_t1918180670 * value)
	{
		___m_OpaquePostProcessPass_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpaquePostProcessPass_9), value);
	}

	inline static int32_t get_offset_of_m_DrawSkyboxPass_10() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_DrawSkyboxPass_10)); }
	inline DrawSkyboxPass_t705056231 * get_m_DrawSkyboxPass_10() const { return ___m_DrawSkyboxPass_10; }
	inline DrawSkyboxPass_t705056231 ** get_address_of_m_DrawSkyboxPass_10() { return &___m_DrawSkyboxPass_10; }
	inline void set_m_DrawSkyboxPass_10(DrawSkyboxPass_t705056231 * value)
	{
		___m_DrawSkyboxPass_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_DrawSkyboxPass_10), value);
	}

	inline static int32_t get_offset_of_m_CopyDepthPass_11() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_CopyDepthPass_11)); }
	inline CopyDepthPass_t304079192 * get_m_CopyDepthPass_11() const { return ___m_CopyDepthPass_11; }
	inline CopyDepthPass_t304079192 ** get_address_of_m_CopyDepthPass_11() { return &___m_CopyDepthPass_11; }
	inline void set_m_CopyDepthPass_11(CopyDepthPass_t304079192 * value)
	{
		___m_CopyDepthPass_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CopyDepthPass_11), value);
	}

	inline static int32_t get_offset_of_m_CopyColorPass_12() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_CopyColorPass_12)); }
	inline CopyColorPass_t3734904897 * get_m_CopyColorPass_12() const { return ___m_CopyColorPass_12; }
	inline CopyColorPass_t3734904897 ** get_address_of_m_CopyColorPass_12() { return &___m_CopyColorPass_12; }
	inline void set_m_CopyColorPass_12(CopyColorPass_t3734904897 * value)
	{
		___m_CopyColorPass_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CopyColorPass_12), value);
	}

	inline static int32_t get_offset_of_m_RenderTransparentForwardPass_13() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_RenderTransparentForwardPass_13)); }
	inline RenderTransparentForwardPass_t1241286203 * get_m_RenderTransparentForwardPass_13() const { return ___m_RenderTransparentForwardPass_13; }
	inline RenderTransparentForwardPass_t1241286203 ** get_address_of_m_RenderTransparentForwardPass_13() { return &___m_RenderTransparentForwardPass_13; }
	inline void set_m_RenderTransparentForwardPass_13(RenderTransparentForwardPass_t1241286203 * value)
	{
		___m_RenderTransparentForwardPass_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_RenderTransparentForwardPass_13), value);
	}

	inline static int32_t get_offset_of_m_TransparentPostProcessPass_14() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_TransparentPostProcessPass_14)); }
	inline TransparentPostProcessPass_t4286252520 * get_m_TransparentPostProcessPass_14() const { return ___m_TransparentPostProcessPass_14; }
	inline TransparentPostProcessPass_t4286252520 ** get_address_of_m_TransparentPostProcessPass_14() { return &___m_TransparentPostProcessPass_14; }
	inline void set_m_TransparentPostProcessPass_14(TransparentPostProcessPass_t4286252520 * value)
	{
		___m_TransparentPostProcessPass_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TransparentPostProcessPass_14), value);
	}

	inline static int32_t get_offset_of_m_FinalBlitPass_15() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_FinalBlitPass_15)); }
	inline FinalBlitPass_t30679461 * get_m_FinalBlitPass_15() const { return ___m_FinalBlitPass_15; }
	inline FinalBlitPass_t30679461 ** get_address_of_m_FinalBlitPass_15() { return &___m_FinalBlitPass_15; }
	inline void set_m_FinalBlitPass_15(FinalBlitPass_t30679461 * value)
	{
		___m_FinalBlitPass_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_FinalBlitPass_15), value);
	}

	inline static int32_t get_offset_of_m_EndXrRenderingPass_16() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_EndXrRenderingPass_16)); }
	inline EndXRRenderingPass_t1600478297 * get_m_EndXrRenderingPass_16() const { return ___m_EndXrRenderingPass_16; }
	inline EndXRRenderingPass_t1600478297 ** get_address_of_m_EndXrRenderingPass_16() { return &___m_EndXrRenderingPass_16; }
	inline void set_m_EndXrRenderingPass_16(EndXRRenderingPass_t1600478297 * value)
	{
		___m_EndXrRenderingPass_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_EndXrRenderingPass_16), value);
	}

	inline static int32_t get_offset_of_Color_17() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___Color_17)); }
	inline RenderTargetHandle_t3987358924  get_Color_17() const { return ___Color_17; }
	inline RenderTargetHandle_t3987358924 * get_address_of_Color_17() { return &___Color_17; }
	inline void set_Color_17(RenderTargetHandle_t3987358924  value)
	{
		___Color_17 = value;
	}

	inline static int32_t get_offset_of_DepthAttachment_18() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___DepthAttachment_18)); }
	inline RenderTargetHandle_t3987358924  get_DepthAttachment_18() const { return ___DepthAttachment_18; }
	inline RenderTargetHandle_t3987358924 * get_address_of_DepthAttachment_18() { return &___DepthAttachment_18; }
	inline void set_DepthAttachment_18(RenderTargetHandle_t3987358924  value)
	{
		___DepthAttachment_18 = value;
	}

	inline static int32_t get_offset_of_DepthTexture_19() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___DepthTexture_19)); }
	inline RenderTargetHandle_t3987358924  get_DepthTexture_19() const { return ___DepthTexture_19; }
	inline RenderTargetHandle_t3987358924 * get_address_of_DepthTexture_19() { return &___DepthTexture_19; }
	inline void set_DepthTexture_19(RenderTargetHandle_t3987358924  value)
	{
		___DepthTexture_19 = value;
	}

	inline static int32_t get_offset_of_OpaqueColor_20() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___OpaqueColor_20)); }
	inline RenderTargetHandle_t3987358924  get_OpaqueColor_20() const { return ___OpaqueColor_20; }
	inline RenderTargetHandle_t3987358924 * get_address_of_OpaqueColor_20() { return &___OpaqueColor_20; }
	inline void set_OpaqueColor_20(RenderTargetHandle_t3987358924  value)
	{
		___OpaqueColor_20 = value;
	}

	inline static int32_t get_offset_of_DirectionalShadowmap_21() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___DirectionalShadowmap_21)); }
	inline RenderTargetHandle_t3987358924  get_DirectionalShadowmap_21() const { return ___DirectionalShadowmap_21; }
	inline RenderTargetHandle_t3987358924 * get_address_of_DirectionalShadowmap_21() { return &___DirectionalShadowmap_21; }
	inline void set_DirectionalShadowmap_21(RenderTargetHandle_t3987358924  value)
	{
		___DirectionalShadowmap_21 = value;
	}

	inline static int32_t get_offset_of_LocalShadowmap_22() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___LocalShadowmap_22)); }
	inline RenderTargetHandle_t3987358924  get_LocalShadowmap_22() const { return ___LocalShadowmap_22; }
	inline RenderTargetHandle_t3987358924 * get_address_of_LocalShadowmap_22() { return &___LocalShadowmap_22; }
	inline void set_LocalShadowmap_22(RenderTargetHandle_t3987358924  value)
	{
		___LocalShadowmap_22 = value;
	}

	inline static int32_t get_offset_of_ScreenSpaceShadowmap_23() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___ScreenSpaceShadowmap_23)); }
	inline RenderTargetHandle_t3987358924  get_ScreenSpaceShadowmap_23() const { return ___ScreenSpaceShadowmap_23; }
	inline RenderTargetHandle_t3987358924 * get_address_of_ScreenSpaceShadowmap_23() { return &___ScreenSpaceShadowmap_23; }
	inline void set_ScreenSpaceShadowmap_23(RenderTargetHandle_t3987358924  value)
	{
		___ScreenSpaceShadowmap_23 = value;
	}

	inline static int32_t get_offset_of_m_Initialized_24() { return static_cast<int32_t>(offsetof(DefaultRendererSetup_t3647655062, ___m_Initialized_24)); }
	inline bool get_m_Initialized_24() const { return ___m_Initialized_24; }
	inline bool* get_address_of_m_Initialized_24() { return &___m_Initialized_24; }
	inline void set_m_Initialized_24(bool value)
	{
		___m_Initialized_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTRENDERERSETUP_T3647655062_H
#ifndef DOWNSAMPLING_T3130013916_H
#define DOWNSAMPLING_T3130013916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.Downsampling
struct  Downsampling_t3130013916 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.Downsampling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Downsampling_t3130013916, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNSAMPLING_T3130013916_H
#ifndef MSAAQUALITY_T3766701580_H
#define MSAAQUALITY_T3766701580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.MSAAQuality
struct  MSAAQuality_t3766701580 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.MSAAQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MSAAQuality_t3766701580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MSAAQUALITY_T3766701580_H
#ifndef MATERIALHANDLES_T3674233906_H
#define MATERIALHANDLES_T3674233906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.MaterialHandles
struct  MaterialHandles_t3674233906 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.MaterialHandles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialHandles_t3674233906, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALHANDLES_T3674233906_H
#ifndef MIXEDLIGHTINGSETUP_T2219919647_H
#define MIXEDLIGHTINGSETUP_T2219919647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.MixedLightingSetup
struct  MixedLightingSetup_t2219919647 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.MixedLightingSetup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MixedLightingSetup_t2219919647, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXEDLIGHTINGSETUP_T2219919647_H
#ifndef PIPELINECAPABILITIES_T2057244252_H
#define PIPELINECAPABILITIES_T2057244252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.PipelineCapabilities
struct  PipelineCapabilities_t2057244252 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.PipelineCapabilities::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PipelineCapabilities_t2057244252, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIPELINECAPABILITIES_T2057244252_H
#ifndef SAMPLECOUNT_T923198274_H
#define SAMPLECOUNT_T923198274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.SampleCount
struct  SampleCount_t923198274 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.SampleCount::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SampleCount_t923198274, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECOUNT_T923198274_H
#ifndef SCENEVIEWDEPTHCOPYPASS_T1517279723_H
#define SCENEVIEWDEPTHCOPYPASS_T1517279723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.SceneViewDepthCopyPass
struct  SceneViewDepthCopyPass_t1517279723  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.SceneViewDepthCopyPass::<source>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CsourceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SceneViewDepthCopyPass_t1517279723, ___U3CsourceU3Ek__BackingField_2)); }
	inline RenderTargetHandle_t3987358924  get_U3CsourceU3Ek__BackingField_2() const { return ___U3CsourceU3Ek__BackingField_2; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CsourceU3Ek__BackingField_2() { return &___U3CsourceU3Ek__BackingField_2; }
	inline void set_U3CsourceU3Ek__BackingField_2(RenderTargetHandle_t3987358924  value)
	{
		___U3CsourceU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEVIEWDEPTHCOPYPASS_T1517279723_H
#ifndef SHADERPATHID_T2494934061_H
#define SHADERPATHID_T2494934061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ShaderPathID
struct  ShaderPathID_t2494934061 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShaderPathID::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShaderPathID_t2494934061, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPATHID_T2494934061_H
#ifndef SHADOWCASCADES_T4003799638_H
#define SHADOWCASCADES_T4003799638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowCascades
struct  ShadowCascades_t4003799638 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowCascades::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowCascades_t4003799638, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWCASCADES_T4003799638_H
#ifndef SHADOWRESOLUTION_T17500100_H
#define SHADOWRESOLUTION_T17500100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowResolution
struct  ShadowResolution_t17500100 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowResolution_t17500100, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWRESOLUTION_T17500100_H
#ifndef SHADOWSLICEDATA_T800660445_H
#define SHADOWSLICEDATA_T800660445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData
struct  ShadowSliceData_t800660445 
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData::shadowTransform
	Matrix4x4_t1817901843  ___shadowTransform_0;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData::offsetX
	int32_t ___offsetX_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData::offsetY
	int32_t ___offsetY_2;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData::resolution
	int32_t ___resolution_3;

public:
	inline static int32_t get_offset_of_shadowTransform_0() { return static_cast<int32_t>(offsetof(ShadowSliceData_t800660445, ___shadowTransform_0)); }
	inline Matrix4x4_t1817901843  get_shadowTransform_0() const { return ___shadowTransform_0; }
	inline Matrix4x4_t1817901843 * get_address_of_shadowTransform_0() { return &___shadowTransform_0; }
	inline void set_shadowTransform_0(Matrix4x4_t1817901843  value)
	{
		___shadowTransform_0 = value;
	}

	inline static int32_t get_offset_of_offsetX_1() { return static_cast<int32_t>(offsetof(ShadowSliceData_t800660445, ___offsetX_1)); }
	inline int32_t get_offsetX_1() const { return ___offsetX_1; }
	inline int32_t* get_address_of_offsetX_1() { return &___offsetX_1; }
	inline void set_offsetX_1(int32_t value)
	{
		___offsetX_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(ShadowSliceData_t800660445, ___offsetY_2)); }
	inline int32_t get_offsetY_2() const { return ___offsetY_2; }
	inline int32_t* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(int32_t value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_resolution_3() { return static_cast<int32_t>(offsetof(ShadowSliceData_t800660445, ___resolution_3)); }
	inline int32_t get_resolution_3() const { return ___resolution_3; }
	inline int32_t* get_address_of_resolution_3() { return &___resolution_3; }
	inline void set_resolution_3(int32_t value)
	{
		___resolution_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSLICEDATA_T800660445_H
#ifndef SHADOWTYPE_T4250716912_H
#define SHADOWTYPE_T4250716912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowType
struct  ShadowType_t4250716912 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowType_t4250716912, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWTYPE_T4250716912_H
#ifndef RENDERERCONFIGURATION_T426365263_H
#define RENDERERCONFIGURATION_T426365263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RendererConfiguration
struct  RendererConfiguration_t426365263 
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.RendererConfiguration::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RendererConfiguration_t426365263, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERCONFIGURATION_T426365263_H
#ifndef LIGHTSHADOWS_T1089484200_H
#define LIGHTSHADOWS_T1089484200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LightShadows
struct  LightShadows_t1089484200 
{
public:
	// System.Int32 UnityEngine.LightShadows::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightShadows_t1089484200, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSHADOWS_T1089484200_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RENDERTEXTURECREATIONFLAGS_T557679221_H
#define RENDERTEXTURECREATIONFLAGS_T557679221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureCreationFlags
struct  RenderTextureCreationFlags_t557679221 
{
public:
	// System.Int32 UnityEngine.RenderTextureCreationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureCreationFlags_t557679221, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURECREATIONFLAGS_T557679221_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef RENDERTEXTUREMEMORYLESS_T852891252_H
#define RENDERTEXTUREMEMORYLESS_T852891252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureMemoryless
struct  RenderTextureMemoryless_t852891252 
{
public:
	// System.Int32 UnityEngine.RenderTextureMemoryless::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureMemoryless_t852891252, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREMEMORYLESS_T852891252_H
#ifndef SHADOWSAMPLINGMODE_T838715745_H
#define SHADOWSAMPLINGMODE_T838715745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ShadowSamplingMode
struct  ShadowSamplingMode_t838715745 
{
public:
	// System.Int32 UnityEngine.Rendering.ShadowSamplingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowSamplingMode_t838715745, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSAMPLINGMODE_T838715745_H
#ifndef TEXTUREDIMENSION_T3933106086_H
#define TEXTUREDIMENSION_T3933106086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.TextureDimension
struct  TextureDimension_t3933106086 
{
public:
	// System.Int32 UnityEngine.Rendering.TextureDimension::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureDimension_t3933106086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREDIMENSION_T3933106086_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef VRTEXTUREUSAGE_T3142149582_H
#define VRTEXTUREUSAGE_T3142149582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VRTextureUsage
struct  VRTextureUsage_t3142149582 
{
public:
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRTextureUsage_t3142149582, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRTEXTUREUSAGE_T3142149582_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CULLRESULTS_T76141518_H
#define CULLRESULTS_T76141518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.CullResults
struct  CullResults_t76141518 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VisibleLight> UnityEngine.Experimental.Rendering.CullResults::visibleLights
	List_1_t2176140814 * ___visibleLights_0;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VisibleLight> UnityEngine.Experimental.Rendering.CullResults::visibleOffscreenVertexLights
	List_1_t2176140814 * ___visibleOffscreenVertexLights_1;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.VisibleReflectionProbe> UnityEngine.Experimental.Rendering.CullResults::visibleReflectionProbes
	List_1_t2925566062 * ___visibleReflectionProbes_2;
	// UnityEngine.Experimental.Rendering.FilterResults UnityEngine.Experimental.Rendering.CullResults::visibleRenderers
	FilterResults_t2595222798  ___visibleRenderers_3;
	// System.IntPtr UnityEngine.Experimental.Rendering.CullResults::cullResults
	intptr_t ___cullResults_4;

public:
	inline static int32_t get_offset_of_visibleLights_0() { return static_cast<int32_t>(offsetof(CullResults_t76141518, ___visibleLights_0)); }
	inline List_1_t2176140814 * get_visibleLights_0() const { return ___visibleLights_0; }
	inline List_1_t2176140814 ** get_address_of_visibleLights_0() { return &___visibleLights_0; }
	inline void set_visibleLights_0(List_1_t2176140814 * value)
	{
		___visibleLights_0 = value;
		Il2CppCodeGenWriteBarrier((&___visibleLights_0), value);
	}

	inline static int32_t get_offset_of_visibleOffscreenVertexLights_1() { return static_cast<int32_t>(offsetof(CullResults_t76141518, ___visibleOffscreenVertexLights_1)); }
	inline List_1_t2176140814 * get_visibleOffscreenVertexLights_1() const { return ___visibleOffscreenVertexLights_1; }
	inline List_1_t2176140814 ** get_address_of_visibleOffscreenVertexLights_1() { return &___visibleOffscreenVertexLights_1; }
	inline void set_visibleOffscreenVertexLights_1(List_1_t2176140814 * value)
	{
		___visibleOffscreenVertexLights_1 = value;
		Il2CppCodeGenWriteBarrier((&___visibleOffscreenVertexLights_1), value);
	}

	inline static int32_t get_offset_of_visibleReflectionProbes_2() { return static_cast<int32_t>(offsetof(CullResults_t76141518, ___visibleReflectionProbes_2)); }
	inline List_1_t2925566062 * get_visibleReflectionProbes_2() const { return ___visibleReflectionProbes_2; }
	inline List_1_t2925566062 ** get_address_of_visibleReflectionProbes_2() { return &___visibleReflectionProbes_2; }
	inline void set_visibleReflectionProbes_2(List_1_t2925566062 * value)
	{
		___visibleReflectionProbes_2 = value;
		Il2CppCodeGenWriteBarrier((&___visibleReflectionProbes_2), value);
	}

	inline static int32_t get_offset_of_visibleRenderers_3() { return static_cast<int32_t>(offsetof(CullResults_t76141518, ___visibleRenderers_3)); }
	inline FilterResults_t2595222798  get_visibleRenderers_3() const { return ___visibleRenderers_3; }
	inline FilterResults_t2595222798 * get_address_of_visibleRenderers_3() { return &___visibleRenderers_3; }
	inline void set_visibleRenderers_3(FilterResults_t2595222798  value)
	{
		___visibleRenderers_3 = value;
	}

	inline static int32_t get_offset_of_cullResults_4() { return static_cast<int32_t>(offsetof(CullResults_t76141518, ___cullResults_4)); }
	inline intptr_t get_cullResults_4() const { return ___cullResults_4; }
	inline intptr_t* get_address_of_cullResults_4() { return &___cullResults_4; }
	inline void set_cullResults_4(intptr_t value)
	{
		___cullResults_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.CullResults
struct CullResults_t76141518_marshaled_pinvoke
{
	List_1_t2176140814 * ___visibleLights_0;
	List_1_t2176140814 * ___visibleOffscreenVertexLights_1;
	List_1_t2925566062 * ___visibleReflectionProbes_2;
	FilterResults_t2595222798  ___visibleRenderers_3;
	intptr_t ___cullResults_4;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.CullResults
struct CullResults_t76141518_marshaled_com
{
	List_1_t2176140814 * ___visibleLights_0;
	List_1_t2176140814 * ___visibleOffscreenVertexLights_1;
	List_1_t2925566062 * ___visibleReflectionProbes_2;
	FilterResults_t2595222798  ___visibleRenderers_3;
	intptr_t ___cullResults_4;
};
#endif // CULLRESULTS_T76141518_H
#ifndef CAMERADATA_T2373231130_H
#define CAMERADATA_T2373231130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData
struct  CameraData_t2373231130 
{
public:
	// UnityEngine.Camera UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::camera
	Camera_t4157153871 * ___camera_0;
	// System.Single UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::renderScale
	float ___renderScale_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::msaaSamples
	int32_t ___msaaSamples_2;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::isSceneViewCamera
	bool ___isSceneViewCamera_3;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::isDefaultViewport
	bool ___isDefaultViewport_4;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::isOffscreenRender
	bool ___isOffscreenRender_5;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::isHdrEnabled
	bool ___isHdrEnabled_6;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::requiresDepthTexture
	bool ___requiresDepthTexture_7;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::requiresSoftParticles
	bool ___requiresSoftParticles_8;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::requiresOpaqueTexture
	bool ___requiresOpaqueTexture_9;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.Downsampling UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::opaqueTextureDownsampling
	int32_t ___opaqueTextureDownsampling_10;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::isStereoEnabled
	bool ___isStereoEnabled_11;
	// System.Single UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::maxShadowDistance
	float ___maxShadowDistance_12;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::postProcessEnabled
	bool ___postProcessEnabled_13;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData::postProcessLayer
	PostProcessLayer_t4264744195 * ___postProcessLayer_14;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___camera_0)); }
	inline Camera_t4157153871 * get_camera_0() const { return ___camera_0; }
	inline Camera_t4157153871 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t4157153871 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___camera_0), value);
	}

	inline static int32_t get_offset_of_renderScale_1() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___renderScale_1)); }
	inline float get_renderScale_1() const { return ___renderScale_1; }
	inline float* get_address_of_renderScale_1() { return &___renderScale_1; }
	inline void set_renderScale_1(float value)
	{
		___renderScale_1 = value;
	}

	inline static int32_t get_offset_of_msaaSamples_2() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___msaaSamples_2)); }
	inline int32_t get_msaaSamples_2() const { return ___msaaSamples_2; }
	inline int32_t* get_address_of_msaaSamples_2() { return &___msaaSamples_2; }
	inline void set_msaaSamples_2(int32_t value)
	{
		___msaaSamples_2 = value;
	}

	inline static int32_t get_offset_of_isSceneViewCamera_3() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___isSceneViewCamera_3)); }
	inline bool get_isSceneViewCamera_3() const { return ___isSceneViewCamera_3; }
	inline bool* get_address_of_isSceneViewCamera_3() { return &___isSceneViewCamera_3; }
	inline void set_isSceneViewCamera_3(bool value)
	{
		___isSceneViewCamera_3 = value;
	}

	inline static int32_t get_offset_of_isDefaultViewport_4() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___isDefaultViewport_4)); }
	inline bool get_isDefaultViewport_4() const { return ___isDefaultViewport_4; }
	inline bool* get_address_of_isDefaultViewport_4() { return &___isDefaultViewport_4; }
	inline void set_isDefaultViewport_4(bool value)
	{
		___isDefaultViewport_4 = value;
	}

	inline static int32_t get_offset_of_isOffscreenRender_5() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___isOffscreenRender_5)); }
	inline bool get_isOffscreenRender_5() const { return ___isOffscreenRender_5; }
	inline bool* get_address_of_isOffscreenRender_5() { return &___isOffscreenRender_5; }
	inline void set_isOffscreenRender_5(bool value)
	{
		___isOffscreenRender_5 = value;
	}

	inline static int32_t get_offset_of_isHdrEnabled_6() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___isHdrEnabled_6)); }
	inline bool get_isHdrEnabled_6() const { return ___isHdrEnabled_6; }
	inline bool* get_address_of_isHdrEnabled_6() { return &___isHdrEnabled_6; }
	inline void set_isHdrEnabled_6(bool value)
	{
		___isHdrEnabled_6 = value;
	}

	inline static int32_t get_offset_of_requiresDepthTexture_7() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___requiresDepthTexture_7)); }
	inline bool get_requiresDepthTexture_7() const { return ___requiresDepthTexture_7; }
	inline bool* get_address_of_requiresDepthTexture_7() { return &___requiresDepthTexture_7; }
	inline void set_requiresDepthTexture_7(bool value)
	{
		___requiresDepthTexture_7 = value;
	}

	inline static int32_t get_offset_of_requiresSoftParticles_8() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___requiresSoftParticles_8)); }
	inline bool get_requiresSoftParticles_8() const { return ___requiresSoftParticles_8; }
	inline bool* get_address_of_requiresSoftParticles_8() { return &___requiresSoftParticles_8; }
	inline void set_requiresSoftParticles_8(bool value)
	{
		___requiresSoftParticles_8 = value;
	}

	inline static int32_t get_offset_of_requiresOpaqueTexture_9() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___requiresOpaqueTexture_9)); }
	inline bool get_requiresOpaqueTexture_9() const { return ___requiresOpaqueTexture_9; }
	inline bool* get_address_of_requiresOpaqueTexture_9() { return &___requiresOpaqueTexture_9; }
	inline void set_requiresOpaqueTexture_9(bool value)
	{
		___requiresOpaqueTexture_9 = value;
	}

	inline static int32_t get_offset_of_opaqueTextureDownsampling_10() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___opaqueTextureDownsampling_10)); }
	inline int32_t get_opaqueTextureDownsampling_10() const { return ___opaqueTextureDownsampling_10; }
	inline int32_t* get_address_of_opaqueTextureDownsampling_10() { return &___opaqueTextureDownsampling_10; }
	inline void set_opaqueTextureDownsampling_10(int32_t value)
	{
		___opaqueTextureDownsampling_10 = value;
	}

	inline static int32_t get_offset_of_isStereoEnabled_11() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___isStereoEnabled_11)); }
	inline bool get_isStereoEnabled_11() const { return ___isStereoEnabled_11; }
	inline bool* get_address_of_isStereoEnabled_11() { return &___isStereoEnabled_11; }
	inline void set_isStereoEnabled_11(bool value)
	{
		___isStereoEnabled_11 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_12() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___maxShadowDistance_12)); }
	inline float get_maxShadowDistance_12() const { return ___maxShadowDistance_12; }
	inline float* get_address_of_maxShadowDistance_12() { return &___maxShadowDistance_12; }
	inline void set_maxShadowDistance_12(float value)
	{
		___maxShadowDistance_12 = value;
	}

	inline static int32_t get_offset_of_postProcessEnabled_13() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___postProcessEnabled_13)); }
	inline bool get_postProcessEnabled_13() const { return ___postProcessEnabled_13; }
	inline bool* get_address_of_postProcessEnabled_13() { return &___postProcessEnabled_13; }
	inline void set_postProcessEnabled_13(bool value)
	{
		___postProcessEnabled_13 = value;
	}

	inline static int32_t get_offset_of_postProcessLayer_14() { return static_cast<int32_t>(offsetof(CameraData_t2373231130, ___postProcessLayer_14)); }
	inline PostProcessLayer_t4264744195 * get_postProcessLayer_14() const { return ___postProcessLayer_14; }
	inline PostProcessLayer_t4264744195 ** get_address_of_postProcessLayer_14() { return &___postProcessLayer_14; }
	inline void set_postProcessLayer_14(PostProcessLayer_t4264744195 * value)
	{
		___postProcessLayer_14 = value;
		Il2CppCodeGenWriteBarrier((&___postProcessLayer_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData
struct CameraData_t2373231130_marshaled_pinvoke
{
	Camera_t4157153871 * ___camera_0;
	float ___renderScale_1;
	int32_t ___msaaSamples_2;
	int32_t ___isSceneViewCamera_3;
	int32_t ___isDefaultViewport_4;
	int32_t ___isOffscreenRender_5;
	int32_t ___isHdrEnabled_6;
	int32_t ___requiresDepthTexture_7;
	int32_t ___requiresSoftParticles_8;
	int32_t ___requiresOpaqueTexture_9;
	int32_t ___opaqueTextureDownsampling_10;
	int32_t ___isStereoEnabled_11;
	float ___maxShadowDistance_12;
	int32_t ___postProcessEnabled_13;
	PostProcessLayer_t4264744195 * ___postProcessLayer_14;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData
struct CameraData_t2373231130_marshaled_com
{
	Camera_t4157153871 * ___camera_0;
	float ___renderScale_1;
	int32_t ___msaaSamples_2;
	int32_t ___isSceneViewCamera_3;
	int32_t ___isDefaultViewport_4;
	int32_t ___isOffscreenRender_5;
	int32_t ___isHdrEnabled_6;
	int32_t ___requiresDepthTexture_7;
	int32_t ___requiresSoftParticles_8;
	int32_t ___requiresOpaqueTexture_9;
	int32_t ___opaqueTextureDownsampling_10;
	int32_t ___isStereoEnabled_11;
	float ___maxShadowDistance_12;
	int32_t ___postProcessEnabled_13;
	PostProcessLayer_t4264744195 * ___postProcessLayer_14;
};
#endif // CAMERADATA_T2373231130_H
#ifndef DIRECTIONALSHADOWSPASS_T4026755168_H
#define DIRECTIONALSHADOWSPASS_T4026755168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass
struct  DirectionalShadowsPass_t4026755168  : public ScriptableRenderPass_t3231910016
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::m_ShadowCasterCascadesCount
	int32_t ___m_ShadowCasterCascadesCount_4;
	// UnityEngine.RenderTexture UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::m_DirectionalShadowmapTexture
	RenderTexture_t2108887433 * ___m_DirectionalShadowmapTexture_5;
	// UnityEngine.RenderTextureFormat UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::m_ShadowmapFormat
	int32_t ___m_ShadowmapFormat_6;
	// UnityEngine.Matrix4x4[] UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::m_DirectionalShadowMatrices
	Matrix4x4U5BU5D_t2302988098* ___m_DirectionalShadowMatrices_7;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData[] UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::m_CascadeSlices
	ShadowSliceDataU5BU5D_t1489940496* ___m_CascadeSlices_8;
	// UnityEngine.Vector4[] UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::m_CascadeSplitDistances
	Vector4U5BU5D_t934056436* ___m_CascadeSplitDistances_9;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DirectionalShadowsPass::<destination>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdestinationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_m_ShadowCasterCascadesCount_4() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___m_ShadowCasterCascadesCount_4)); }
	inline int32_t get_m_ShadowCasterCascadesCount_4() const { return ___m_ShadowCasterCascadesCount_4; }
	inline int32_t* get_address_of_m_ShadowCasterCascadesCount_4() { return &___m_ShadowCasterCascadesCount_4; }
	inline void set_m_ShadowCasterCascadesCount_4(int32_t value)
	{
		___m_ShadowCasterCascadesCount_4 = value;
	}

	inline static int32_t get_offset_of_m_DirectionalShadowmapTexture_5() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___m_DirectionalShadowmapTexture_5)); }
	inline RenderTexture_t2108887433 * get_m_DirectionalShadowmapTexture_5() const { return ___m_DirectionalShadowmapTexture_5; }
	inline RenderTexture_t2108887433 ** get_address_of_m_DirectionalShadowmapTexture_5() { return &___m_DirectionalShadowmapTexture_5; }
	inline void set_m_DirectionalShadowmapTexture_5(RenderTexture_t2108887433 * value)
	{
		___m_DirectionalShadowmapTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DirectionalShadowmapTexture_5), value);
	}

	inline static int32_t get_offset_of_m_ShadowmapFormat_6() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___m_ShadowmapFormat_6)); }
	inline int32_t get_m_ShadowmapFormat_6() const { return ___m_ShadowmapFormat_6; }
	inline int32_t* get_address_of_m_ShadowmapFormat_6() { return &___m_ShadowmapFormat_6; }
	inline void set_m_ShadowmapFormat_6(int32_t value)
	{
		___m_ShadowmapFormat_6 = value;
	}

	inline static int32_t get_offset_of_m_DirectionalShadowMatrices_7() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___m_DirectionalShadowMatrices_7)); }
	inline Matrix4x4U5BU5D_t2302988098* get_m_DirectionalShadowMatrices_7() const { return ___m_DirectionalShadowMatrices_7; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_m_DirectionalShadowMatrices_7() { return &___m_DirectionalShadowMatrices_7; }
	inline void set_m_DirectionalShadowMatrices_7(Matrix4x4U5BU5D_t2302988098* value)
	{
		___m_DirectionalShadowMatrices_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DirectionalShadowMatrices_7), value);
	}

	inline static int32_t get_offset_of_m_CascadeSlices_8() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___m_CascadeSlices_8)); }
	inline ShadowSliceDataU5BU5D_t1489940496* get_m_CascadeSlices_8() const { return ___m_CascadeSlices_8; }
	inline ShadowSliceDataU5BU5D_t1489940496** get_address_of_m_CascadeSlices_8() { return &___m_CascadeSlices_8; }
	inline void set_m_CascadeSlices_8(ShadowSliceDataU5BU5D_t1489940496* value)
	{
		___m_CascadeSlices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CascadeSlices_8), value);
	}

	inline static int32_t get_offset_of_m_CascadeSplitDistances_9() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___m_CascadeSplitDistances_9)); }
	inline Vector4U5BU5D_t934056436* get_m_CascadeSplitDistances_9() const { return ___m_CascadeSplitDistances_9; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_CascadeSplitDistances_9() { return &___m_CascadeSplitDistances_9; }
	inline void set_m_CascadeSplitDistances_9(Vector4U5BU5D_t934056436* value)
	{
		___m_CascadeSplitDistances_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CascadeSplitDistances_9), value);
	}

	inline static int32_t get_offset_of_U3CdestinationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DirectionalShadowsPass_t4026755168, ___U3CdestinationU3Ek__BackingField_11)); }
	inline RenderTargetHandle_t3987358924  get_U3CdestinationU3Ek__BackingField_11() const { return ___U3CdestinationU3Ek__BackingField_11; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdestinationU3Ek__BackingField_11() { return &___U3CdestinationU3Ek__BackingField_11; }
	inline void set_U3CdestinationU3Ek__BackingField_11(RenderTargetHandle_t3987358924  value)
	{
		___U3CdestinationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONALSHADOWSPASS_T4026755168_H
#ifndef LIGHTWEIGHTFORWARDRENDERER_T94815316_H
#define LIGHTWEIGHTFORWARDRENDERER_T94815316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer
struct  LightweightForwardRenderer_t94815316  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer::<postProcessRenderContext>k__BackingField
	PostProcessRenderContext_t597611190 * ___U3CpostProcessRenderContextU3Ek__BackingField_3;
	// UnityEngine.ComputeBuffer UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer::<perObjectLightIndices>k__BackingField
	ComputeBuffer_t1033194329 * ___U3CperObjectLightIndicesU3Ek__BackingField_4;
	// UnityEngine.Experimental.Rendering.FilterRenderersSettings UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer::<opaqueFilterSettings>k__BackingField
	FilterRenderersSettings_t2384854789  ___U3CopaqueFilterSettingsU3Ek__BackingField_5;
	// UnityEngine.Experimental.Rendering.FilterRenderersSettings UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer::<transparentFilterSettings>k__BackingField
	FilterRenderersSettings_t2384854789  ___U3CtransparentFilterSettingsU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.LightweightPipeline.ScriptableRenderPass> UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer::m_ActiveRenderPassQueue
	List_1_t409017462 * ___m_ActiveRenderPassQueue_7;
	// UnityEngine.Material[] UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer::m_Materials
	MaterialU5BU5D_t561872642* ___m_Materials_8;

public:
	inline static int32_t get_offset_of_U3CpostProcessRenderContextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LightweightForwardRenderer_t94815316, ___U3CpostProcessRenderContextU3Ek__BackingField_3)); }
	inline PostProcessRenderContext_t597611190 * get_U3CpostProcessRenderContextU3Ek__BackingField_3() const { return ___U3CpostProcessRenderContextU3Ek__BackingField_3; }
	inline PostProcessRenderContext_t597611190 ** get_address_of_U3CpostProcessRenderContextU3Ek__BackingField_3() { return &___U3CpostProcessRenderContextU3Ek__BackingField_3; }
	inline void set_U3CpostProcessRenderContextU3Ek__BackingField_3(PostProcessRenderContext_t597611190 * value)
	{
		___U3CpostProcessRenderContextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpostProcessRenderContextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CperObjectLightIndicesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LightweightForwardRenderer_t94815316, ___U3CperObjectLightIndicesU3Ek__BackingField_4)); }
	inline ComputeBuffer_t1033194329 * get_U3CperObjectLightIndicesU3Ek__BackingField_4() const { return ___U3CperObjectLightIndicesU3Ek__BackingField_4; }
	inline ComputeBuffer_t1033194329 ** get_address_of_U3CperObjectLightIndicesU3Ek__BackingField_4() { return &___U3CperObjectLightIndicesU3Ek__BackingField_4; }
	inline void set_U3CperObjectLightIndicesU3Ek__BackingField_4(ComputeBuffer_t1033194329 * value)
	{
		___U3CperObjectLightIndicesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CperObjectLightIndicesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CopaqueFilterSettingsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LightweightForwardRenderer_t94815316, ___U3CopaqueFilterSettingsU3Ek__BackingField_5)); }
	inline FilterRenderersSettings_t2384854789  get_U3CopaqueFilterSettingsU3Ek__BackingField_5() const { return ___U3CopaqueFilterSettingsU3Ek__BackingField_5; }
	inline FilterRenderersSettings_t2384854789 * get_address_of_U3CopaqueFilterSettingsU3Ek__BackingField_5() { return &___U3CopaqueFilterSettingsU3Ek__BackingField_5; }
	inline void set_U3CopaqueFilterSettingsU3Ek__BackingField_5(FilterRenderersSettings_t2384854789  value)
	{
		___U3CopaqueFilterSettingsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CtransparentFilterSettingsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LightweightForwardRenderer_t94815316, ___U3CtransparentFilterSettingsU3Ek__BackingField_6)); }
	inline FilterRenderersSettings_t2384854789  get_U3CtransparentFilterSettingsU3Ek__BackingField_6() const { return ___U3CtransparentFilterSettingsU3Ek__BackingField_6; }
	inline FilterRenderersSettings_t2384854789 * get_address_of_U3CtransparentFilterSettingsU3Ek__BackingField_6() { return &___U3CtransparentFilterSettingsU3Ek__BackingField_6; }
	inline void set_U3CtransparentFilterSettingsU3Ek__BackingField_6(FilterRenderersSettings_t2384854789  value)
	{
		___U3CtransparentFilterSettingsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_m_ActiveRenderPassQueue_7() { return static_cast<int32_t>(offsetof(LightweightForwardRenderer_t94815316, ___m_ActiveRenderPassQueue_7)); }
	inline List_1_t409017462 * get_m_ActiveRenderPassQueue_7() const { return ___m_ActiveRenderPassQueue_7; }
	inline List_1_t409017462 ** get_address_of_m_ActiveRenderPassQueue_7() { return &___m_ActiveRenderPassQueue_7; }
	inline void set_m_ActiveRenderPassQueue_7(List_1_t409017462 * value)
	{
		___m_ActiveRenderPassQueue_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveRenderPassQueue_7), value);
	}

	inline static int32_t get_offset_of_m_Materials_8() { return static_cast<int32_t>(offsetof(LightweightForwardRenderer_t94815316, ___m_Materials_8)); }
	inline MaterialU5BU5D_t561872642* get_m_Materials_8() const { return ___m_Materials_8; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_Materials_8() { return &___m_Materials_8; }
	inline void set_m_Materials_8(MaterialU5BU5D_t561872642* value)
	{
		___m_Materials_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Materials_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTFORWARDRENDERER_T94815316_H
#ifndef LOCALSHADOWSPASS_T2703335903_H
#define LOCALSHADOWSPASS_T2703335903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass
struct  LocalShadowsPass_t2703335903  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.RenderTexture UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass::m_LocalShadowmapTexture
	RenderTexture_t2108887433 * ___m_LocalShadowmapTexture_3;
	// UnityEngine.RenderTextureFormat UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass::m_LocalShadowmapFormat
	int32_t ___m_LocalShadowmapFormat_4;
	// UnityEngine.Matrix4x4[] UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass::m_LocalShadowMatrices
	Matrix4x4U5BU5D_t2302988098* ___m_LocalShadowMatrices_5;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowSliceData[] UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass::m_LocalLightSlices
	ShadowSliceDataU5BU5D_t1489940496* ___m_LocalLightSlices_6;
	// System.Single[] UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass::m_LocalShadowStrength
	SingleU5BU5D_t1444911251* ___m_LocalShadowStrength_7;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.LocalShadowsPass::<destination>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdestinationU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_m_LocalShadowmapTexture_3() { return static_cast<int32_t>(offsetof(LocalShadowsPass_t2703335903, ___m_LocalShadowmapTexture_3)); }
	inline RenderTexture_t2108887433 * get_m_LocalShadowmapTexture_3() const { return ___m_LocalShadowmapTexture_3; }
	inline RenderTexture_t2108887433 ** get_address_of_m_LocalShadowmapTexture_3() { return &___m_LocalShadowmapTexture_3; }
	inline void set_m_LocalShadowmapTexture_3(RenderTexture_t2108887433 * value)
	{
		___m_LocalShadowmapTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalShadowmapTexture_3), value);
	}

	inline static int32_t get_offset_of_m_LocalShadowmapFormat_4() { return static_cast<int32_t>(offsetof(LocalShadowsPass_t2703335903, ___m_LocalShadowmapFormat_4)); }
	inline int32_t get_m_LocalShadowmapFormat_4() const { return ___m_LocalShadowmapFormat_4; }
	inline int32_t* get_address_of_m_LocalShadowmapFormat_4() { return &___m_LocalShadowmapFormat_4; }
	inline void set_m_LocalShadowmapFormat_4(int32_t value)
	{
		___m_LocalShadowmapFormat_4 = value;
	}

	inline static int32_t get_offset_of_m_LocalShadowMatrices_5() { return static_cast<int32_t>(offsetof(LocalShadowsPass_t2703335903, ___m_LocalShadowMatrices_5)); }
	inline Matrix4x4U5BU5D_t2302988098* get_m_LocalShadowMatrices_5() const { return ___m_LocalShadowMatrices_5; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_m_LocalShadowMatrices_5() { return &___m_LocalShadowMatrices_5; }
	inline void set_m_LocalShadowMatrices_5(Matrix4x4U5BU5D_t2302988098* value)
	{
		___m_LocalShadowMatrices_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalShadowMatrices_5), value);
	}

	inline static int32_t get_offset_of_m_LocalLightSlices_6() { return static_cast<int32_t>(offsetof(LocalShadowsPass_t2703335903, ___m_LocalLightSlices_6)); }
	inline ShadowSliceDataU5BU5D_t1489940496* get_m_LocalLightSlices_6() const { return ___m_LocalLightSlices_6; }
	inline ShadowSliceDataU5BU5D_t1489940496** get_address_of_m_LocalLightSlices_6() { return &___m_LocalLightSlices_6; }
	inline void set_m_LocalLightSlices_6(ShadowSliceDataU5BU5D_t1489940496* value)
	{
		___m_LocalLightSlices_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalLightSlices_6), value);
	}

	inline static int32_t get_offset_of_m_LocalShadowStrength_7() { return static_cast<int32_t>(offsetof(LocalShadowsPass_t2703335903, ___m_LocalShadowStrength_7)); }
	inline SingleU5BU5D_t1444911251* get_m_LocalShadowStrength_7() const { return ___m_LocalShadowStrength_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_LocalShadowStrength_7() { return &___m_LocalShadowStrength_7; }
	inline void set_m_LocalShadowStrength_7(SingleU5BU5D_t1444911251* value)
	{
		___m_LocalShadowStrength_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalShadowStrength_7), value);
	}

	inline static int32_t get_offset_of_U3CdestinationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(LocalShadowsPass_t2703335903, ___U3CdestinationU3Ek__BackingField_9)); }
	inline RenderTargetHandle_t3987358924  get_U3CdestinationU3Ek__BackingField_9() const { return ___U3CdestinationU3Ek__BackingField_9; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdestinationU3Ek__BackingField_9() { return &___U3CdestinationU3Ek__BackingField_9; }
	inline void set_U3CdestinationU3Ek__BackingField_9(RenderTargetHandle_t3987358924  value)
	{
		___U3CdestinationU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALSHADOWSPASS_T2703335903_H
#ifndef SETUPLIGHTWEIGHTCONSTANSTPASS_T1972706453_H
#define SETUPLIGHTWEIGHTCONSTANSTPASS_T1972706453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass
struct  SetupLightweightConstanstPass_t1972706453  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.MixedLightingSetup UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::m_MixedLightingSetup
	int32_t ___m_MixedLightingSetup_2;
	// UnityEngine.Vector4 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::k_DefaultLightPosition
	Vector4_t3319028937  ___k_DefaultLightPosition_3;
	// UnityEngine.Vector4 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::k_DefaultLightColor
	Vector4_t3319028937  ___k_DefaultLightColor_4;
	// UnityEngine.Vector4 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::k_DefaultLightAttenuation
	Vector4_t3319028937  ___k_DefaultLightAttenuation_5;
	// UnityEngine.Vector4 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::k_DefaultLightSpotDirection
	Vector4_t3319028937  ___k_DefaultLightSpotDirection_6;
	// UnityEngine.Vector4 UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::k_DefaultLightSpotAttenuation
	Vector4_t3319028937  ___k_DefaultLightSpotAttenuation_7;
	// UnityEngine.Vector4[] UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::m_LightPositions
	Vector4U5BU5D_t934056436* ___m_LightPositions_8;
	// UnityEngine.Vector4[] UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::m_LightColors
	Vector4U5BU5D_t934056436* ___m_LightColors_9;
	// UnityEngine.Vector4[] UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::m_LightDistanceAttenuations
	Vector4U5BU5D_t934056436* ___m_LightDistanceAttenuations_10;
	// UnityEngine.Vector4[] UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::m_LightSpotDirections
	Vector4U5BU5D_t934056436* ___m_LightSpotDirections_11;
	// UnityEngine.Vector4[] UnityEngine.Experimental.Rendering.LightweightPipeline.SetupLightweightConstanstPass::m_LightSpotAttenuations
	Vector4U5BU5D_t934056436* ___m_LightSpotAttenuations_12;

public:
	inline static int32_t get_offset_of_m_MixedLightingSetup_2() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___m_MixedLightingSetup_2)); }
	inline int32_t get_m_MixedLightingSetup_2() const { return ___m_MixedLightingSetup_2; }
	inline int32_t* get_address_of_m_MixedLightingSetup_2() { return &___m_MixedLightingSetup_2; }
	inline void set_m_MixedLightingSetup_2(int32_t value)
	{
		___m_MixedLightingSetup_2 = value;
	}

	inline static int32_t get_offset_of_k_DefaultLightPosition_3() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___k_DefaultLightPosition_3)); }
	inline Vector4_t3319028937  get_k_DefaultLightPosition_3() const { return ___k_DefaultLightPosition_3; }
	inline Vector4_t3319028937 * get_address_of_k_DefaultLightPosition_3() { return &___k_DefaultLightPosition_3; }
	inline void set_k_DefaultLightPosition_3(Vector4_t3319028937  value)
	{
		___k_DefaultLightPosition_3 = value;
	}

	inline static int32_t get_offset_of_k_DefaultLightColor_4() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___k_DefaultLightColor_4)); }
	inline Vector4_t3319028937  get_k_DefaultLightColor_4() const { return ___k_DefaultLightColor_4; }
	inline Vector4_t3319028937 * get_address_of_k_DefaultLightColor_4() { return &___k_DefaultLightColor_4; }
	inline void set_k_DefaultLightColor_4(Vector4_t3319028937  value)
	{
		___k_DefaultLightColor_4 = value;
	}

	inline static int32_t get_offset_of_k_DefaultLightAttenuation_5() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___k_DefaultLightAttenuation_5)); }
	inline Vector4_t3319028937  get_k_DefaultLightAttenuation_5() const { return ___k_DefaultLightAttenuation_5; }
	inline Vector4_t3319028937 * get_address_of_k_DefaultLightAttenuation_5() { return &___k_DefaultLightAttenuation_5; }
	inline void set_k_DefaultLightAttenuation_5(Vector4_t3319028937  value)
	{
		___k_DefaultLightAttenuation_5 = value;
	}

	inline static int32_t get_offset_of_k_DefaultLightSpotDirection_6() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___k_DefaultLightSpotDirection_6)); }
	inline Vector4_t3319028937  get_k_DefaultLightSpotDirection_6() const { return ___k_DefaultLightSpotDirection_6; }
	inline Vector4_t3319028937 * get_address_of_k_DefaultLightSpotDirection_6() { return &___k_DefaultLightSpotDirection_6; }
	inline void set_k_DefaultLightSpotDirection_6(Vector4_t3319028937  value)
	{
		___k_DefaultLightSpotDirection_6 = value;
	}

	inline static int32_t get_offset_of_k_DefaultLightSpotAttenuation_7() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___k_DefaultLightSpotAttenuation_7)); }
	inline Vector4_t3319028937  get_k_DefaultLightSpotAttenuation_7() const { return ___k_DefaultLightSpotAttenuation_7; }
	inline Vector4_t3319028937 * get_address_of_k_DefaultLightSpotAttenuation_7() { return &___k_DefaultLightSpotAttenuation_7; }
	inline void set_k_DefaultLightSpotAttenuation_7(Vector4_t3319028937  value)
	{
		___k_DefaultLightSpotAttenuation_7 = value;
	}

	inline static int32_t get_offset_of_m_LightPositions_8() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___m_LightPositions_8)); }
	inline Vector4U5BU5D_t934056436* get_m_LightPositions_8() const { return ___m_LightPositions_8; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_LightPositions_8() { return &___m_LightPositions_8; }
	inline void set_m_LightPositions_8(Vector4U5BU5D_t934056436* value)
	{
		___m_LightPositions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightPositions_8), value);
	}

	inline static int32_t get_offset_of_m_LightColors_9() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___m_LightColors_9)); }
	inline Vector4U5BU5D_t934056436* get_m_LightColors_9() const { return ___m_LightColors_9; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_LightColors_9() { return &___m_LightColors_9; }
	inline void set_m_LightColors_9(Vector4U5BU5D_t934056436* value)
	{
		___m_LightColors_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightColors_9), value);
	}

	inline static int32_t get_offset_of_m_LightDistanceAttenuations_10() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___m_LightDistanceAttenuations_10)); }
	inline Vector4U5BU5D_t934056436* get_m_LightDistanceAttenuations_10() const { return ___m_LightDistanceAttenuations_10; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_LightDistanceAttenuations_10() { return &___m_LightDistanceAttenuations_10; }
	inline void set_m_LightDistanceAttenuations_10(Vector4U5BU5D_t934056436* value)
	{
		___m_LightDistanceAttenuations_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightDistanceAttenuations_10), value);
	}

	inline static int32_t get_offset_of_m_LightSpotDirections_11() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___m_LightSpotDirections_11)); }
	inline Vector4U5BU5D_t934056436* get_m_LightSpotDirections_11() const { return ___m_LightSpotDirections_11; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_LightSpotDirections_11() { return &___m_LightSpotDirections_11; }
	inline void set_m_LightSpotDirections_11(Vector4U5BU5D_t934056436* value)
	{
		___m_LightSpotDirections_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightSpotDirections_11), value);
	}

	inline static int32_t get_offset_of_m_LightSpotAttenuations_12() { return static_cast<int32_t>(offsetof(SetupLightweightConstanstPass_t1972706453, ___m_LightSpotAttenuations_12)); }
	inline Vector4U5BU5D_t934056436* get_m_LightSpotAttenuations_12() const { return ___m_LightSpotAttenuations_12; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_LightSpotAttenuations_12() { return &___m_LightSpotAttenuations_12; }
	inline void set_m_LightSpotAttenuations_12(Vector4U5BU5D_t934056436* value)
	{
		___m_LightSpotAttenuations_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightSpotAttenuations_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPLIGHTWEIGHTCONSTANSTPASS_T1972706453_H
#ifndef SHADOWDATA_T294369652_H
#define SHADOWDATA_T294369652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData
struct  ShadowData_t294369652 
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::renderDirectionalShadows
	bool ___renderDirectionalShadows_0;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::requiresScreenSpaceShadowResolve
	bool ___requiresScreenSpaceShadowResolve_1;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::directionalShadowAtlasWidth
	int32_t ___directionalShadowAtlasWidth_2;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::directionalShadowAtlasHeight
	int32_t ___directionalShadowAtlasHeight_3;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::directionalLightCascadeCount
	int32_t ___directionalLightCascadeCount_4;
	// UnityEngine.Vector3 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::directionalLightCascades
	Vector3_t3722313464  ___directionalLightCascades_5;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::renderLocalShadows
	bool ___renderLocalShadows_6;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::localShadowAtlasWidth
	int32_t ___localShadowAtlasWidth_7;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::localShadowAtlasHeight
	int32_t ___localShadowAtlasHeight_8;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::supportsSoftShadows
	bool ___supportsSoftShadows_9;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::bufferBitCount
	int32_t ___bufferBitCount_10;
	// UnityEngine.LightShadows UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::renderedDirectionalShadowQuality
	int32_t ___renderedDirectionalShadowQuality_11;
	// UnityEngine.LightShadows UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData::renderedLocalShadowQuality
	int32_t ___renderedLocalShadowQuality_12;

public:
	inline static int32_t get_offset_of_renderDirectionalShadows_0() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___renderDirectionalShadows_0)); }
	inline bool get_renderDirectionalShadows_0() const { return ___renderDirectionalShadows_0; }
	inline bool* get_address_of_renderDirectionalShadows_0() { return &___renderDirectionalShadows_0; }
	inline void set_renderDirectionalShadows_0(bool value)
	{
		___renderDirectionalShadows_0 = value;
	}

	inline static int32_t get_offset_of_requiresScreenSpaceShadowResolve_1() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___requiresScreenSpaceShadowResolve_1)); }
	inline bool get_requiresScreenSpaceShadowResolve_1() const { return ___requiresScreenSpaceShadowResolve_1; }
	inline bool* get_address_of_requiresScreenSpaceShadowResolve_1() { return &___requiresScreenSpaceShadowResolve_1; }
	inline void set_requiresScreenSpaceShadowResolve_1(bool value)
	{
		___requiresScreenSpaceShadowResolve_1 = value;
	}

	inline static int32_t get_offset_of_directionalShadowAtlasWidth_2() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___directionalShadowAtlasWidth_2)); }
	inline int32_t get_directionalShadowAtlasWidth_2() const { return ___directionalShadowAtlasWidth_2; }
	inline int32_t* get_address_of_directionalShadowAtlasWidth_2() { return &___directionalShadowAtlasWidth_2; }
	inline void set_directionalShadowAtlasWidth_2(int32_t value)
	{
		___directionalShadowAtlasWidth_2 = value;
	}

	inline static int32_t get_offset_of_directionalShadowAtlasHeight_3() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___directionalShadowAtlasHeight_3)); }
	inline int32_t get_directionalShadowAtlasHeight_3() const { return ___directionalShadowAtlasHeight_3; }
	inline int32_t* get_address_of_directionalShadowAtlasHeight_3() { return &___directionalShadowAtlasHeight_3; }
	inline void set_directionalShadowAtlasHeight_3(int32_t value)
	{
		___directionalShadowAtlasHeight_3 = value;
	}

	inline static int32_t get_offset_of_directionalLightCascadeCount_4() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___directionalLightCascadeCount_4)); }
	inline int32_t get_directionalLightCascadeCount_4() const { return ___directionalLightCascadeCount_4; }
	inline int32_t* get_address_of_directionalLightCascadeCount_4() { return &___directionalLightCascadeCount_4; }
	inline void set_directionalLightCascadeCount_4(int32_t value)
	{
		___directionalLightCascadeCount_4 = value;
	}

	inline static int32_t get_offset_of_directionalLightCascades_5() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___directionalLightCascades_5)); }
	inline Vector3_t3722313464  get_directionalLightCascades_5() const { return ___directionalLightCascades_5; }
	inline Vector3_t3722313464 * get_address_of_directionalLightCascades_5() { return &___directionalLightCascades_5; }
	inline void set_directionalLightCascades_5(Vector3_t3722313464  value)
	{
		___directionalLightCascades_5 = value;
	}

	inline static int32_t get_offset_of_renderLocalShadows_6() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___renderLocalShadows_6)); }
	inline bool get_renderLocalShadows_6() const { return ___renderLocalShadows_6; }
	inline bool* get_address_of_renderLocalShadows_6() { return &___renderLocalShadows_6; }
	inline void set_renderLocalShadows_6(bool value)
	{
		___renderLocalShadows_6 = value;
	}

	inline static int32_t get_offset_of_localShadowAtlasWidth_7() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___localShadowAtlasWidth_7)); }
	inline int32_t get_localShadowAtlasWidth_7() const { return ___localShadowAtlasWidth_7; }
	inline int32_t* get_address_of_localShadowAtlasWidth_7() { return &___localShadowAtlasWidth_7; }
	inline void set_localShadowAtlasWidth_7(int32_t value)
	{
		___localShadowAtlasWidth_7 = value;
	}

	inline static int32_t get_offset_of_localShadowAtlasHeight_8() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___localShadowAtlasHeight_8)); }
	inline int32_t get_localShadowAtlasHeight_8() const { return ___localShadowAtlasHeight_8; }
	inline int32_t* get_address_of_localShadowAtlasHeight_8() { return &___localShadowAtlasHeight_8; }
	inline void set_localShadowAtlasHeight_8(int32_t value)
	{
		___localShadowAtlasHeight_8 = value;
	}

	inline static int32_t get_offset_of_supportsSoftShadows_9() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___supportsSoftShadows_9)); }
	inline bool get_supportsSoftShadows_9() const { return ___supportsSoftShadows_9; }
	inline bool* get_address_of_supportsSoftShadows_9() { return &___supportsSoftShadows_9; }
	inline void set_supportsSoftShadows_9(bool value)
	{
		___supportsSoftShadows_9 = value;
	}

	inline static int32_t get_offset_of_bufferBitCount_10() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___bufferBitCount_10)); }
	inline int32_t get_bufferBitCount_10() const { return ___bufferBitCount_10; }
	inline int32_t* get_address_of_bufferBitCount_10() { return &___bufferBitCount_10; }
	inline void set_bufferBitCount_10(int32_t value)
	{
		___bufferBitCount_10 = value;
	}

	inline static int32_t get_offset_of_renderedDirectionalShadowQuality_11() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___renderedDirectionalShadowQuality_11)); }
	inline int32_t get_renderedDirectionalShadowQuality_11() const { return ___renderedDirectionalShadowQuality_11; }
	inline int32_t* get_address_of_renderedDirectionalShadowQuality_11() { return &___renderedDirectionalShadowQuality_11; }
	inline void set_renderedDirectionalShadowQuality_11(int32_t value)
	{
		___renderedDirectionalShadowQuality_11 = value;
	}

	inline static int32_t get_offset_of_renderedLocalShadowQuality_12() { return static_cast<int32_t>(offsetof(ShadowData_t294369652, ___renderedLocalShadowQuality_12)); }
	inline int32_t get_renderedLocalShadowQuality_12() const { return ___renderedLocalShadowQuality_12; }
	inline int32_t* get_address_of_renderedLocalShadowQuality_12() { return &___renderedLocalShadowQuality_12; }
	inline void set_renderedLocalShadowQuality_12(int32_t value)
	{
		___renderedLocalShadowQuality_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData
struct ShadowData_t294369652_marshaled_pinvoke
{
	int32_t ___renderDirectionalShadows_0;
	int32_t ___requiresScreenSpaceShadowResolve_1;
	int32_t ___directionalShadowAtlasWidth_2;
	int32_t ___directionalShadowAtlasHeight_3;
	int32_t ___directionalLightCascadeCount_4;
	Vector3_t3722313464  ___directionalLightCascades_5;
	int32_t ___renderLocalShadows_6;
	int32_t ___localShadowAtlasWidth_7;
	int32_t ___localShadowAtlasHeight_8;
	int32_t ___supportsSoftShadows_9;
	int32_t ___bufferBitCount_10;
	int32_t ___renderedDirectionalShadowQuality_11;
	int32_t ___renderedLocalShadowQuality_12;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData
struct ShadowData_t294369652_marshaled_com
{
	int32_t ___renderDirectionalShadows_0;
	int32_t ___requiresScreenSpaceShadowResolve_1;
	int32_t ___directionalShadowAtlasWidth_2;
	int32_t ___directionalShadowAtlasHeight_3;
	int32_t ___directionalLightCascadeCount_4;
	Vector3_t3722313464  ___directionalLightCascades_5;
	int32_t ___renderLocalShadows_6;
	int32_t ___localShadowAtlasWidth_7;
	int32_t ___localShadowAtlasHeight_8;
	int32_t ___supportsSoftShadows_9;
	int32_t ___bufferBitCount_10;
	int32_t ___renderedDirectionalShadowQuality_11;
	int32_t ___renderedLocalShadowQuality_12;
};
#endif // SHADOWDATA_T294369652_H
#ifndef RENDERTEXTUREDESCRIPTOR_T1974534975_H
#define RENDERTEXTUREDESCRIPTOR_T1974534975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureDescriptor
struct  RenderTextureDescriptor_t1974534975 
{
public:
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// UnityEngine.RenderTextureFormat UnityEngine.RenderTextureDescriptor::<colorFormat>k__BackingField
	int32_t ___U3CcolorFormatU3Ek__BackingField_4;
	// System.Int32 UnityEngine.RenderTextureDescriptor::_depthBufferBits
	int32_t ____depthBufferBits_5;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_7;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_8;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_9;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_10;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CwidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CheightU3Ek__BackingField_1)); }
	inline int32_t get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(int32_t value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmsaaSamplesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CmsaaSamplesU3Ek__BackingField_2)); }
	inline int32_t get_U3CmsaaSamplesU3Ek__BackingField_2() const { return ___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CmsaaSamplesU3Ek__BackingField_2() { return &___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline void set_U3CmsaaSamplesU3Ek__BackingField_2(int32_t value)
	{
		___U3CmsaaSamplesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeDepthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CvolumeDepthU3Ek__BackingField_3)); }
	inline int32_t get_U3CvolumeDepthU3Ek__BackingField_3() const { return ___U3CvolumeDepthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CvolumeDepthU3Ek__BackingField_3() { return &___U3CvolumeDepthU3Ek__BackingField_3; }
	inline void set_U3CvolumeDepthU3Ek__BackingField_3(int32_t value)
	{
		___U3CvolumeDepthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcolorFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CcolorFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CcolorFormatU3Ek__BackingField_4() const { return ___U3CcolorFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CcolorFormatU3Ek__BackingField_4() { return &___U3CcolorFormatU3Ek__BackingField_4; }
	inline void set_U3CcolorFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CcolorFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__depthBufferBits_5() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ____depthBufferBits_5)); }
	inline int32_t get__depthBufferBits_5() const { return ____depthBufferBits_5; }
	inline int32_t* get_address_of__depthBufferBits_5() { return &____depthBufferBits_5; }
	inline void set__depthBufferBits_5(int32_t value)
	{
		____depthBufferBits_5 = value;
	}

	inline static int32_t get_offset_of_U3CdimensionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CdimensionU3Ek__BackingField_7)); }
	inline int32_t get_U3CdimensionU3Ek__BackingField_7() const { return ___U3CdimensionU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CdimensionU3Ek__BackingField_7() { return &___U3CdimensionU3Ek__BackingField_7; }
	inline void set_U3CdimensionU3Ek__BackingField_7(int32_t value)
	{
		___U3CdimensionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CshadowSamplingModeU3Ek__BackingField_8)); }
	inline int32_t get_U3CshadowSamplingModeU3Ek__BackingField_8() const { return ___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return &___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline void set_U3CshadowSamplingModeU3Ek__BackingField_8(int32_t value)
	{
		___U3CshadowSamplingModeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CvrUsageU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CvrUsageU3Ek__BackingField_9)); }
	inline int32_t get_U3CvrUsageU3Ek__BackingField_9() const { return ___U3CvrUsageU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CvrUsageU3Ek__BackingField_9() { return &___U3CvrUsageU3Ek__BackingField_9; }
	inline void set_U3CvrUsageU3Ek__BackingField_9(int32_t value)
	{
		___U3CvrUsageU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of__flags_10() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ____flags_10)); }
	inline int32_t get__flags_10() const { return ____flags_10; }
	inline int32_t* get_address_of__flags_10() { return &____flags_10; }
	inline void set__flags_10(int32_t value)
	{
		____flags_10 = value;
	}

	inline static int32_t get_offset_of_U3CmemorylessU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975, ___U3CmemorylessU3Ek__BackingField_11)); }
	inline int32_t get_U3CmemorylessU3Ek__BackingField_11() const { return ___U3CmemorylessU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CmemorylessU3Ek__BackingField_11() { return &___U3CmemorylessU3Ek__BackingField_11; }
	inline void set_U3CmemorylessU3Ek__BackingField_11(int32_t value)
	{
		___U3CmemorylessU3Ek__BackingField_11 = value;
	}
};

struct RenderTextureDescriptor_t1974534975_StaticFields
{
public:
	// System.Int32[] UnityEngine.RenderTextureDescriptor::depthFormatBits
	Int32U5BU5D_t385246372* ___depthFormatBits_6;

public:
	inline static int32_t get_offset_of_depthFormatBits_6() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t1974534975_StaticFields, ___depthFormatBits_6)); }
	inline Int32U5BU5D_t385246372* get_depthFormatBits_6() const { return ___depthFormatBits_6; }
	inline Int32U5BU5D_t385246372** get_address_of_depthFormatBits_6() { return &___depthFormatBits_6; }
	inline void set_depthFormatBits_6(Int32U5BU5D_t385246372* value)
	{
		___depthFormatBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___depthFormatBits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREDESCRIPTOR_T1974534975_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef LIGHTWEIGHTPIPELINEEDITORRESOURCES_T3077566732_H
#define LIGHTWEIGHTPIPELINEEDITORRESOURCES_T3077566732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightweightPipelineEditorResources
struct  LightweightPipelineEditorResources_t3077566732  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Material LightweightPipelineEditorResources::DefaultMaterial
	Material_t340375123 * ___DefaultMaterial_4;
	// UnityEngine.Material LightweightPipelineEditorResources::DefaultParticleMaterial
	Material_t340375123 * ___DefaultParticleMaterial_5;
	// UnityEngine.Material LightweightPipelineEditorResources::DefaultTerrainMaterial
	Material_t340375123 * ___DefaultTerrainMaterial_6;

public:
	inline static int32_t get_offset_of_DefaultMaterial_4() { return static_cast<int32_t>(offsetof(LightweightPipelineEditorResources_t3077566732, ___DefaultMaterial_4)); }
	inline Material_t340375123 * get_DefaultMaterial_4() const { return ___DefaultMaterial_4; }
	inline Material_t340375123 ** get_address_of_DefaultMaterial_4() { return &___DefaultMaterial_4; }
	inline void set_DefaultMaterial_4(Material_t340375123 * value)
	{
		___DefaultMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaterial_4), value);
	}

	inline static int32_t get_offset_of_DefaultParticleMaterial_5() { return static_cast<int32_t>(offsetof(LightweightPipelineEditorResources_t3077566732, ___DefaultParticleMaterial_5)); }
	inline Material_t340375123 * get_DefaultParticleMaterial_5() const { return ___DefaultParticleMaterial_5; }
	inline Material_t340375123 ** get_address_of_DefaultParticleMaterial_5() { return &___DefaultParticleMaterial_5; }
	inline void set_DefaultParticleMaterial_5(Material_t340375123 * value)
	{
		___DefaultParticleMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultParticleMaterial_5), value);
	}

	inline static int32_t get_offset_of_DefaultTerrainMaterial_6() { return static_cast<int32_t>(offsetof(LightweightPipelineEditorResources_t3077566732, ___DefaultTerrainMaterial_6)); }
	inline Material_t340375123 * get_DefaultTerrainMaterial_6() const { return ___DefaultTerrainMaterial_6; }
	inline Material_t340375123 ** get_address_of_DefaultTerrainMaterial_6() { return &___DefaultTerrainMaterial_6; }
	inline void set_DefaultTerrainMaterial_6(Material_t340375123 * value)
	{
		___DefaultTerrainMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultTerrainMaterial_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTPIPELINEEDITORRESOURCES_T3077566732_H
#ifndef LIGHTWEIGHTPIPELINERESOURCES_T2373355997_H
#define LIGHTWEIGHTPIPELINERESOURCES_T2373355997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightweightPipelineResources
struct  LightweightPipelineResources_t2373355997  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Shader LightweightPipelineResources::BlitShader
	Shader_t4151988712 * ___BlitShader_4;
	// UnityEngine.Shader LightweightPipelineResources::CopyDepthShader
	Shader_t4151988712 * ___CopyDepthShader_5;
	// UnityEngine.Shader LightweightPipelineResources::ScreenSpaceShadowShader
	Shader_t4151988712 * ___ScreenSpaceShadowShader_6;
	// UnityEngine.Shader LightweightPipelineResources::SamplingShader
	Shader_t4151988712 * ___SamplingShader_7;

public:
	inline static int32_t get_offset_of_BlitShader_4() { return static_cast<int32_t>(offsetof(LightweightPipelineResources_t2373355997, ___BlitShader_4)); }
	inline Shader_t4151988712 * get_BlitShader_4() const { return ___BlitShader_4; }
	inline Shader_t4151988712 ** get_address_of_BlitShader_4() { return &___BlitShader_4; }
	inline void set_BlitShader_4(Shader_t4151988712 * value)
	{
		___BlitShader_4 = value;
		Il2CppCodeGenWriteBarrier((&___BlitShader_4), value);
	}

	inline static int32_t get_offset_of_CopyDepthShader_5() { return static_cast<int32_t>(offsetof(LightweightPipelineResources_t2373355997, ___CopyDepthShader_5)); }
	inline Shader_t4151988712 * get_CopyDepthShader_5() const { return ___CopyDepthShader_5; }
	inline Shader_t4151988712 ** get_address_of_CopyDepthShader_5() { return &___CopyDepthShader_5; }
	inline void set_CopyDepthShader_5(Shader_t4151988712 * value)
	{
		___CopyDepthShader_5 = value;
		Il2CppCodeGenWriteBarrier((&___CopyDepthShader_5), value);
	}

	inline static int32_t get_offset_of_ScreenSpaceShadowShader_6() { return static_cast<int32_t>(offsetof(LightweightPipelineResources_t2373355997, ___ScreenSpaceShadowShader_6)); }
	inline Shader_t4151988712 * get_ScreenSpaceShadowShader_6() const { return ___ScreenSpaceShadowShader_6; }
	inline Shader_t4151988712 ** get_address_of_ScreenSpaceShadowShader_6() { return &___ScreenSpaceShadowShader_6; }
	inline void set_ScreenSpaceShadowShader_6(Shader_t4151988712 * value)
	{
		___ScreenSpaceShadowShader_6 = value;
		Il2CppCodeGenWriteBarrier((&___ScreenSpaceShadowShader_6), value);
	}

	inline static int32_t get_offset_of_SamplingShader_7() { return static_cast<int32_t>(offsetof(LightweightPipelineResources_t2373355997, ___SamplingShader_7)); }
	inline Shader_t4151988712 * get_SamplingShader_7() const { return ___SamplingShader_7; }
	inline Shader_t4151988712 ** get_address_of_SamplingShader_7() { return &___SamplingShader_7; }
	inline void set_SamplingShader_7(Shader_t4151988712 * value)
	{
		___SamplingShader_7 = value;
		Il2CppCodeGenWriteBarrier((&___SamplingShader_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTPIPELINERESOURCES_T2373355997_H
#ifndef CONSTRUCTORDELEGATE_T3127338789_H
#define CONSTRUCTORDELEGATE_T3127338789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t3127338789  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORDELEGATE_T3127338789_H
#ifndef GETDELEGATE_T3939479301_H
#define GETDELEGATE_T3939479301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct  GetDelegate_t3939479301  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDELEGATE_T3939479301_H
#ifndef SETDELEGATE_T920366853_H
#define SETDELEGATE_T920366853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct  SetDelegate_t920366853  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETDELEGATE_T920366853_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CREATELIGHTWEIGHTRENDERTEXTURESPASS_T2359216577_H
#define CREATELIGHTWEIGHTRENDERTEXTURESPASS_T2359216577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass
struct  CreateLightweightRenderTexturesPass_t2359216577  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass::<colorAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CcolorAttachmentHandleU3Ek__BackingField_3;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass::<depthAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdepthAttachmentHandleU3Ek__BackingField_4;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_5;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.SampleCount UnityEngine.Experimental.Rendering.LightweightPipeline.CreateLightweightRenderTexturesPass::<samples>k__BackingField
	int32_t ___U3CsamplesU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CreateLightweightRenderTexturesPass_t2359216577, ___U3CcolorAttachmentHandleU3Ek__BackingField_3)); }
	inline RenderTargetHandle_t3987358924  get_U3CcolorAttachmentHandleU3Ek__BackingField_3() const { return ___U3CcolorAttachmentHandleU3Ek__BackingField_3; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CcolorAttachmentHandleU3Ek__BackingField_3() { return &___U3CcolorAttachmentHandleU3Ek__BackingField_3; }
	inline void set_U3CcolorAttachmentHandleU3Ek__BackingField_3(RenderTargetHandle_t3987358924  value)
	{
		___U3CcolorAttachmentHandleU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdepthAttachmentHandleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CreateLightweightRenderTexturesPass_t2359216577, ___U3CdepthAttachmentHandleU3Ek__BackingField_4)); }
	inline RenderTargetHandle_t3987358924  get_U3CdepthAttachmentHandleU3Ek__BackingField_4() const { return ___U3CdepthAttachmentHandleU3Ek__BackingField_4; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdepthAttachmentHandleU3Ek__BackingField_4() { return &___U3CdepthAttachmentHandleU3Ek__BackingField_4; }
	inline void set_U3CdepthAttachmentHandleU3Ek__BackingField_4(RenderTargetHandle_t3987358924  value)
	{
		___U3CdepthAttachmentHandleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CreateLightweightRenderTexturesPass_t2359216577, ___U3CdescriptorU3Ek__BackingField_5)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_5() const { return ___U3CdescriptorU3Ek__BackingField_5; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_5() { return &___U3CdescriptorU3Ek__BackingField_5; }
	inline void set_U3CdescriptorU3Ek__BackingField_5(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CsamplesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CreateLightweightRenderTexturesPass_t2359216577, ___U3CsamplesU3Ek__BackingField_6)); }
	inline int32_t get_U3CsamplesU3Ek__BackingField_6() const { return ___U3CsamplesU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CsamplesU3Ek__BackingField_6() { return &___U3CsamplesU3Ek__BackingField_6; }
	inline void set_U3CsamplesU3Ek__BackingField_6(int32_t value)
	{
		___U3CsamplesU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATELIGHTWEIGHTRENDERTEXTURESPASS_T2359216577_H
#ifndef DEPTHONLYPASS_T4004473585_H
#define DEPTHONLYPASS_T4004473585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.DepthOnlyPass
struct  DepthOnlyPass_t4004473585  : public ScriptableRenderPass_t3231910016
{
public:
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.DepthOnlyPass::kDepthBufferBits
	int32_t ___kDepthBufferBits_3;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.DepthOnlyPass::<depthAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdepthAttachmentHandleU3Ek__BackingField_4;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.DepthOnlyPass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_kDepthBufferBits_3() { return static_cast<int32_t>(offsetof(DepthOnlyPass_t4004473585, ___kDepthBufferBits_3)); }
	inline int32_t get_kDepthBufferBits_3() const { return ___kDepthBufferBits_3; }
	inline int32_t* get_address_of_kDepthBufferBits_3() { return &___kDepthBufferBits_3; }
	inline void set_kDepthBufferBits_3(int32_t value)
	{
		___kDepthBufferBits_3 = value;
	}

	inline static int32_t get_offset_of_U3CdepthAttachmentHandleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DepthOnlyPass_t4004473585, ___U3CdepthAttachmentHandleU3Ek__BackingField_4)); }
	inline RenderTargetHandle_t3987358924  get_U3CdepthAttachmentHandleU3Ek__BackingField_4() const { return ___U3CdepthAttachmentHandleU3Ek__BackingField_4; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdepthAttachmentHandleU3Ek__BackingField_4() { return &___U3CdepthAttachmentHandleU3Ek__BackingField_4; }
	inline void set_U3CdepthAttachmentHandleU3Ek__BackingField_4(RenderTargetHandle_t3987358924  value)
	{
		___U3CdepthAttachmentHandleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DepthOnlyPass_t4004473585, ___U3CdescriptorU3Ek__BackingField_5)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_5() const { return ___U3CdescriptorU3Ek__BackingField_5; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_5() { return &___U3CdescriptorU3Ek__BackingField_5; }
	inline void set_U3CdescriptorU3Ek__BackingField_5(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHONLYPASS_T4004473585_H
#ifndef FINALBLITPASS_T30679461_H
#define FINALBLITPASS_T30679461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.FinalBlitPass
struct  FinalBlitPass_t30679461  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Material UnityEngine.Experimental.Rendering.LightweightPipeline.FinalBlitPass::m_BlitMaterial
	Material_t340375123 * ___m_BlitMaterial_2;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.FinalBlitPass::<colorAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CcolorAttachmentHandleU3Ek__BackingField_3;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.FinalBlitPass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_BlitMaterial_2() { return static_cast<int32_t>(offsetof(FinalBlitPass_t30679461, ___m_BlitMaterial_2)); }
	inline Material_t340375123 * get_m_BlitMaterial_2() const { return ___m_BlitMaterial_2; }
	inline Material_t340375123 ** get_address_of_m_BlitMaterial_2() { return &___m_BlitMaterial_2; }
	inline void set_m_BlitMaterial_2(Material_t340375123 * value)
	{
		___m_BlitMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlitMaterial_2), value);
	}

	inline static int32_t get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FinalBlitPass_t30679461, ___U3CcolorAttachmentHandleU3Ek__BackingField_3)); }
	inline RenderTargetHandle_t3987358924  get_U3CcolorAttachmentHandleU3Ek__BackingField_3() const { return ___U3CcolorAttachmentHandleU3Ek__BackingField_3; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CcolorAttachmentHandleU3Ek__BackingField_3() { return &___U3CcolorAttachmentHandleU3Ek__BackingField_3; }
	inline void set_U3CcolorAttachmentHandleU3Ek__BackingField_3(RenderTargetHandle_t3987358924  value)
	{
		___U3CcolorAttachmentHandleU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FinalBlitPass_t30679461, ___U3CdescriptorU3Ek__BackingField_4)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_4() const { return ___U3CdescriptorU3Ek__BackingField_4; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_4() { return &___U3CdescriptorU3Ek__BackingField_4; }
	inline void set_U3CdescriptorU3Ek__BackingField_4(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINALBLITPASS_T30679461_H
#ifndef LIGHTWEIGHTFORWARDPASS_T835756159_H
#define LIGHTWEIGHTFORWARDPASS_T835756159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass
struct  LightweightForwardPass_t835756159  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::<colorAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CcolorAttachmentHandleU3Ek__BackingField_2;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::<depthAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CdepthAttachmentHandleU3Ek__BackingField_3;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_4;
	// UnityEngine.Experimental.Rendering.ClearFlag UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::<clearFlag>k__BackingField
	int32_t ___U3CclearFlagU3Ek__BackingField_5;
	// UnityEngine.Color UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::<clearColor>k__BackingField
	Color_t2555686324  ___U3CclearColorU3Ek__BackingField_6;
	// UnityEngine.Material UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::m_ErrorMaterial
	Material_t340375123 * ___m_ErrorMaterial_8;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.ShaderPassName> UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::m_LegacyShaderPassNames
	List_1_t1566003363 * ___m_LegacyShaderPassNames_9;
	// UnityEngine.Experimental.Rendering.RendererConfiguration UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::rendererConfiguration
	int32_t ___rendererConfiguration_10;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardPass::dynamicBatching
	bool ___dynamicBatching_11;

public:
	inline static int32_t get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___U3CcolorAttachmentHandleU3Ek__BackingField_2)); }
	inline RenderTargetHandle_t3987358924  get_U3CcolorAttachmentHandleU3Ek__BackingField_2() const { return ___U3CcolorAttachmentHandleU3Ek__BackingField_2; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CcolorAttachmentHandleU3Ek__BackingField_2() { return &___U3CcolorAttachmentHandleU3Ek__BackingField_2; }
	inline void set_U3CcolorAttachmentHandleU3Ek__BackingField_2(RenderTargetHandle_t3987358924  value)
	{
		___U3CcolorAttachmentHandleU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdepthAttachmentHandleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___U3CdepthAttachmentHandleU3Ek__BackingField_3)); }
	inline RenderTargetHandle_t3987358924  get_U3CdepthAttachmentHandleU3Ek__BackingField_3() const { return ___U3CdepthAttachmentHandleU3Ek__BackingField_3; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CdepthAttachmentHandleU3Ek__BackingField_3() { return &___U3CdepthAttachmentHandleU3Ek__BackingField_3; }
	inline void set_U3CdepthAttachmentHandleU3Ek__BackingField_3(RenderTargetHandle_t3987358924  value)
	{
		___U3CdepthAttachmentHandleU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___U3CdescriptorU3Ek__BackingField_4)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_4() const { return ___U3CdescriptorU3Ek__BackingField_4; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_4() { return &___U3CdescriptorU3Ek__BackingField_4; }
	inline void set_U3CdescriptorU3Ek__BackingField_4(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CclearFlagU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___U3CclearFlagU3Ek__BackingField_5)); }
	inline int32_t get_U3CclearFlagU3Ek__BackingField_5() const { return ___U3CclearFlagU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CclearFlagU3Ek__BackingField_5() { return &___U3CclearFlagU3Ek__BackingField_5; }
	inline void set_U3CclearFlagU3Ek__BackingField_5(int32_t value)
	{
		___U3CclearFlagU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CclearColorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___U3CclearColorU3Ek__BackingField_6)); }
	inline Color_t2555686324  get_U3CclearColorU3Ek__BackingField_6() const { return ___U3CclearColorU3Ek__BackingField_6; }
	inline Color_t2555686324 * get_address_of_U3CclearColorU3Ek__BackingField_6() { return &___U3CclearColorU3Ek__BackingField_6; }
	inline void set_U3CclearColorU3Ek__BackingField_6(Color_t2555686324  value)
	{
		___U3CclearColorU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_m_ErrorMaterial_8() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___m_ErrorMaterial_8)); }
	inline Material_t340375123 * get_m_ErrorMaterial_8() const { return ___m_ErrorMaterial_8; }
	inline Material_t340375123 ** get_address_of_m_ErrorMaterial_8() { return &___m_ErrorMaterial_8; }
	inline void set_m_ErrorMaterial_8(Material_t340375123 * value)
	{
		___m_ErrorMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ErrorMaterial_8), value);
	}

	inline static int32_t get_offset_of_m_LegacyShaderPassNames_9() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___m_LegacyShaderPassNames_9)); }
	inline List_1_t1566003363 * get_m_LegacyShaderPassNames_9() const { return ___m_LegacyShaderPassNames_9; }
	inline List_1_t1566003363 ** get_address_of_m_LegacyShaderPassNames_9() { return &___m_LegacyShaderPassNames_9; }
	inline void set_m_LegacyShaderPassNames_9(List_1_t1566003363 * value)
	{
		___m_LegacyShaderPassNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyShaderPassNames_9), value);
	}

	inline static int32_t get_offset_of_rendererConfiguration_10() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___rendererConfiguration_10)); }
	inline int32_t get_rendererConfiguration_10() const { return ___rendererConfiguration_10; }
	inline int32_t* get_address_of_rendererConfiguration_10() { return &___rendererConfiguration_10; }
	inline void set_rendererConfiguration_10(int32_t value)
	{
		___rendererConfiguration_10 = value;
	}

	inline static int32_t get_offset_of_dynamicBatching_11() { return static_cast<int32_t>(offsetof(LightweightForwardPass_t835756159, ___dynamicBatching_11)); }
	inline bool get_dynamicBatching_11() const { return ___dynamicBatching_11; }
	inline bool* get_address_of_dynamicBatching_11() { return &___dynamicBatching_11; }
	inline void set_dynamicBatching_11(bool value)
	{
		___dynamicBatching_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTFORWARDPASS_T835756159_H
#ifndef LIGHTWEIGHTPIPELINE_T2674386577_H
#define LIGHTWEIGHTPIPELINE_T2674386577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline
struct  LightweightPipeline_t2674386577  : public RenderPipeline_t957695896
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::<pipelineAsset>k__BackingField
	LightweightPipelineAsset_t3453435338 * ___U3CpipelineAssetU3Ek__BackingField_3;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.CameraComparer UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::m_CameraComparer
	CameraComparer_t3166570673 * ___m_CameraComparer_4;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightForwardRenderer UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::m_Renderer
	LightweightForwardRenderer_t94815316 * ___m_Renderer_5;
	// UnityEngine.Experimental.Rendering.CullResults UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::m_CullResults
	CullResults_t76141518  ___m_CullResults_6;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::m_LocalLightIndices
	List_1_t128053199 * ___m_LocalLightIndices_7;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::m_IsCameraRendering
	bool ___m_IsCameraRendering_8;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.IRendererSetup UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::m_DefaultRendererSetup
	RuntimeObject* ___m_DefaultRendererSetup_9;

public:
	inline static int32_t get_offset_of_U3CpipelineAssetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___U3CpipelineAssetU3Ek__BackingField_3)); }
	inline LightweightPipelineAsset_t3453435338 * get_U3CpipelineAssetU3Ek__BackingField_3() const { return ___U3CpipelineAssetU3Ek__BackingField_3; }
	inline LightweightPipelineAsset_t3453435338 ** get_address_of_U3CpipelineAssetU3Ek__BackingField_3() { return &___U3CpipelineAssetU3Ek__BackingField_3; }
	inline void set_U3CpipelineAssetU3Ek__BackingField_3(LightweightPipelineAsset_t3453435338 * value)
	{
		___U3CpipelineAssetU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpipelineAssetU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_m_CameraComparer_4() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___m_CameraComparer_4)); }
	inline CameraComparer_t3166570673 * get_m_CameraComparer_4() const { return ___m_CameraComparer_4; }
	inline CameraComparer_t3166570673 ** get_address_of_m_CameraComparer_4() { return &___m_CameraComparer_4; }
	inline void set_m_CameraComparer_4(CameraComparer_t3166570673 * value)
	{
		___m_CameraComparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraComparer_4), value);
	}

	inline static int32_t get_offset_of_m_Renderer_5() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___m_Renderer_5)); }
	inline LightweightForwardRenderer_t94815316 * get_m_Renderer_5() const { return ___m_Renderer_5; }
	inline LightweightForwardRenderer_t94815316 ** get_address_of_m_Renderer_5() { return &___m_Renderer_5; }
	inline void set_m_Renderer_5(LightweightForwardRenderer_t94815316 * value)
	{
		___m_Renderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_5), value);
	}

	inline static int32_t get_offset_of_m_CullResults_6() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___m_CullResults_6)); }
	inline CullResults_t76141518  get_m_CullResults_6() const { return ___m_CullResults_6; }
	inline CullResults_t76141518 * get_address_of_m_CullResults_6() { return &___m_CullResults_6; }
	inline void set_m_CullResults_6(CullResults_t76141518  value)
	{
		___m_CullResults_6 = value;
	}

	inline static int32_t get_offset_of_m_LocalLightIndices_7() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___m_LocalLightIndices_7)); }
	inline List_1_t128053199 * get_m_LocalLightIndices_7() const { return ___m_LocalLightIndices_7; }
	inline List_1_t128053199 ** get_address_of_m_LocalLightIndices_7() { return &___m_LocalLightIndices_7; }
	inline void set_m_LocalLightIndices_7(List_1_t128053199 * value)
	{
		___m_LocalLightIndices_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalLightIndices_7), value);
	}

	inline static int32_t get_offset_of_m_IsCameraRendering_8() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___m_IsCameraRendering_8)); }
	inline bool get_m_IsCameraRendering_8() const { return ___m_IsCameraRendering_8; }
	inline bool* get_address_of_m_IsCameraRendering_8() { return &___m_IsCameraRendering_8; }
	inline void set_m_IsCameraRendering_8(bool value)
	{
		___m_IsCameraRendering_8 = value;
	}

	inline static int32_t get_offset_of_m_DefaultRendererSetup_9() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577, ___m_DefaultRendererSetup_9)); }
	inline RuntimeObject* get_m_DefaultRendererSetup_9() const { return ___m_DefaultRendererSetup_9; }
	inline RuntimeObject** get_address_of_m_DefaultRendererSetup_9() { return &___m_DefaultRendererSetup_9; }
	inline void set_m_DefaultRendererSetup_9(RuntimeObject* value)
	{
		___m_DefaultRendererSetup_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultRendererSetup_9), value);
	}
};

struct LightweightPipeline_t2674386577_StaticFields
{
public:
	// UnityEngine.Mesh UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::s_FullscreenMesh
	Mesh_t3648964284 * ___s_FullscreenMesh_10;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.PipelineCapabilities UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipeline::s_PipelineCapabilities
	int32_t ___s_PipelineCapabilities_11;

public:
	inline static int32_t get_offset_of_s_FullscreenMesh_10() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577_StaticFields, ___s_FullscreenMesh_10)); }
	inline Mesh_t3648964284 * get_s_FullscreenMesh_10() const { return ___s_FullscreenMesh_10; }
	inline Mesh_t3648964284 ** get_address_of_s_FullscreenMesh_10() { return &___s_FullscreenMesh_10; }
	inline void set_s_FullscreenMesh_10(Mesh_t3648964284 * value)
	{
		___s_FullscreenMesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_FullscreenMesh_10), value);
	}

	inline static int32_t get_offset_of_s_PipelineCapabilities_11() { return static_cast<int32_t>(offsetof(LightweightPipeline_t2674386577_StaticFields, ___s_PipelineCapabilities_11)); }
	inline int32_t get_s_PipelineCapabilities_11() const { return ___s_PipelineCapabilities_11; }
	inline int32_t* get_address_of_s_PipelineCapabilities_11() { return &___s_PipelineCapabilities_11; }
	inline void set_s_PipelineCapabilities_11(int32_t value)
	{
		___s_PipelineCapabilities_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTPIPELINE_T2674386577_H
#ifndef OPAQUEPOSTPROCESSPASS_T1918180670_H
#define OPAQUEPOSTPROCESSPASS_T1918180670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.OpaquePostProcessPass
struct  OpaquePostProcessPass_t1918180670  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.OpaquePostProcessPass::<colorAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CcolorAttachmentHandleU3Ek__BackingField_2;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.OpaquePostProcessPass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OpaquePostProcessPass_t1918180670, ___U3CcolorAttachmentHandleU3Ek__BackingField_2)); }
	inline RenderTargetHandle_t3987358924  get_U3CcolorAttachmentHandleU3Ek__BackingField_2() const { return ___U3CcolorAttachmentHandleU3Ek__BackingField_2; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CcolorAttachmentHandleU3Ek__BackingField_2() { return &___U3CcolorAttachmentHandleU3Ek__BackingField_2; }
	inline void set_U3CcolorAttachmentHandleU3Ek__BackingField_2(RenderTargetHandle_t3987358924  value)
	{
		___U3CcolorAttachmentHandleU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(OpaquePostProcessPass_t1918180670, ___U3CdescriptorU3Ek__BackingField_3)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_3() const { return ___U3CdescriptorU3Ek__BackingField_3; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_3() { return &___U3CdescriptorU3Ek__BackingField_3; }
	inline void set_U3CdescriptorU3Ek__BackingField_3(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPAQUEPOSTPROCESSPASS_T1918180670_H
#ifndef RENDERINGDATA_T2420043686_H
#define RENDERINGDATA_T2420043686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData
struct  RenderingData_t2420043686 
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData::cameraData
	CameraData_t2373231130  ___cameraData_0;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.LightData UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData::lightData
	LightData_t718835846  ___lightData_1;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData::shadowData
	ShadowData_t294369652  ___shadowData_2;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData::supportsDynamicBatching
	bool ___supportsDynamicBatching_3;

public:
	inline static int32_t get_offset_of_cameraData_0() { return static_cast<int32_t>(offsetof(RenderingData_t2420043686, ___cameraData_0)); }
	inline CameraData_t2373231130  get_cameraData_0() const { return ___cameraData_0; }
	inline CameraData_t2373231130 * get_address_of_cameraData_0() { return &___cameraData_0; }
	inline void set_cameraData_0(CameraData_t2373231130  value)
	{
		___cameraData_0 = value;
	}

	inline static int32_t get_offset_of_lightData_1() { return static_cast<int32_t>(offsetof(RenderingData_t2420043686, ___lightData_1)); }
	inline LightData_t718835846  get_lightData_1() const { return ___lightData_1; }
	inline LightData_t718835846 * get_address_of_lightData_1() { return &___lightData_1; }
	inline void set_lightData_1(LightData_t718835846  value)
	{
		___lightData_1 = value;
	}

	inline static int32_t get_offset_of_shadowData_2() { return static_cast<int32_t>(offsetof(RenderingData_t2420043686, ___shadowData_2)); }
	inline ShadowData_t294369652  get_shadowData_2() const { return ___shadowData_2; }
	inline ShadowData_t294369652 * get_address_of_shadowData_2() { return &___shadowData_2; }
	inline void set_shadowData_2(ShadowData_t294369652  value)
	{
		___shadowData_2 = value;
	}

	inline static int32_t get_offset_of_supportsDynamicBatching_3() { return static_cast<int32_t>(offsetof(RenderingData_t2420043686, ___supportsDynamicBatching_3)); }
	inline bool get_supportsDynamicBatching_3() const { return ___supportsDynamicBatching_3; }
	inline bool* get_address_of_supportsDynamicBatching_3() { return &___supportsDynamicBatching_3; }
	inline void set_supportsDynamicBatching_3(bool value)
	{
		___supportsDynamicBatching_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData
struct RenderingData_t2420043686_marshaled_pinvoke
{
	CameraData_t2373231130_marshaled_pinvoke ___cameraData_0;
	LightData_t718835846_marshaled_pinvoke ___lightData_1;
	ShadowData_t294369652_marshaled_pinvoke ___shadowData_2;
	int32_t ___supportsDynamicBatching_3;
};
// Native definition for COM marshalling of UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData
struct RenderingData_t2420043686_marshaled_com
{
	CameraData_t2373231130_marshaled_com ___cameraData_0;
	LightData_t718835846_marshaled_com ___lightData_1;
	ShadowData_t294369652_marshaled_com ___shadowData_2;
	int32_t ___supportsDynamicBatching_3;
};
#endif // RENDERINGDATA_T2420043686_H
#ifndef SCREENSPACESHADOWRESOLVEPASS_T442348744_H
#define SCREENSPACESHADOWRESOLVEPASS_T442348744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass
struct  ScreenSpaceShadowResolvePass_t442348744  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.RenderTextureFormat UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass::m_ColorFormat
	int32_t ___m_ColorFormat_2;
	// UnityEngine.Material UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass::m_ScreenSpaceShadowsMaterial
	Material_t340375123 * ___m_ScreenSpaceShadowsMaterial_3;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass::<colorAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CcolorAttachmentHandleU3Ek__BackingField_4;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.ScreenSpaceShadowResolvePass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_m_ColorFormat_2() { return static_cast<int32_t>(offsetof(ScreenSpaceShadowResolvePass_t442348744, ___m_ColorFormat_2)); }
	inline int32_t get_m_ColorFormat_2() const { return ___m_ColorFormat_2; }
	inline int32_t* get_address_of_m_ColorFormat_2() { return &___m_ColorFormat_2; }
	inline void set_m_ColorFormat_2(int32_t value)
	{
		___m_ColorFormat_2 = value;
	}

	inline static int32_t get_offset_of_m_ScreenSpaceShadowsMaterial_3() { return static_cast<int32_t>(offsetof(ScreenSpaceShadowResolvePass_t442348744, ___m_ScreenSpaceShadowsMaterial_3)); }
	inline Material_t340375123 * get_m_ScreenSpaceShadowsMaterial_3() const { return ___m_ScreenSpaceShadowsMaterial_3; }
	inline Material_t340375123 ** get_address_of_m_ScreenSpaceShadowsMaterial_3() { return &___m_ScreenSpaceShadowsMaterial_3; }
	inline void set_m_ScreenSpaceShadowsMaterial_3(Material_t340375123 * value)
	{
		___m_ScreenSpaceShadowsMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScreenSpaceShadowsMaterial_3), value);
	}

	inline static int32_t get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ScreenSpaceShadowResolvePass_t442348744, ___U3CcolorAttachmentHandleU3Ek__BackingField_4)); }
	inline RenderTargetHandle_t3987358924  get_U3CcolorAttachmentHandleU3Ek__BackingField_4() const { return ___U3CcolorAttachmentHandleU3Ek__BackingField_4; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CcolorAttachmentHandleU3Ek__BackingField_4() { return &___U3CcolorAttachmentHandleU3Ek__BackingField_4; }
	inline void set_U3CcolorAttachmentHandleU3Ek__BackingField_4(RenderTargetHandle_t3987358924  value)
	{
		___U3CcolorAttachmentHandleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ScreenSpaceShadowResolvePass_t442348744, ___U3CdescriptorU3Ek__BackingField_5)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_5() const { return ___U3CdescriptorU3Ek__BackingField_5; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_5() { return &___U3CdescriptorU3Ek__BackingField_5; }
	inline void set_U3CdescriptorU3Ek__BackingField_5(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACESHADOWRESOLVEPASS_T442348744_H
#ifndef TRANSPARENTPOSTPROCESSPASS_T4286252520_H
#define TRANSPARENTPOSTPROCESSPASS_T4286252520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.TransparentPostProcessPass
struct  TransparentPostProcessPass_t4286252520  : public ScriptableRenderPass_t3231910016
{
public:
	// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTargetHandle UnityEngine.Experimental.Rendering.LightweightPipeline.TransparentPostProcessPass::<colorAttachmentHandle>k__BackingField
	RenderTargetHandle_t3987358924  ___U3CcolorAttachmentHandleU3Ek__BackingField_2;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Experimental.Rendering.LightweightPipeline.TransparentPostProcessPass::<descriptor>k__BackingField
	RenderTextureDescriptor_t1974534975  ___U3CdescriptorU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TransparentPostProcessPass_t4286252520, ___U3CcolorAttachmentHandleU3Ek__BackingField_2)); }
	inline RenderTargetHandle_t3987358924  get_U3CcolorAttachmentHandleU3Ek__BackingField_2() const { return ___U3CcolorAttachmentHandleU3Ek__BackingField_2; }
	inline RenderTargetHandle_t3987358924 * get_address_of_U3CcolorAttachmentHandleU3Ek__BackingField_2() { return &___U3CcolorAttachmentHandleU3Ek__BackingField_2; }
	inline void set_U3CcolorAttachmentHandleU3Ek__BackingField_2(RenderTargetHandle_t3987358924  value)
	{
		___U3CcolorAttachmentHandleU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TransparentPostProcessPass_t4286252520, ___U3CdescriptorU3Ek__BackingField_3)); }
	inline RenderTextureDescriptor_t1974534975  get_U3CdescriptorU3Ek__BackingField_3() const { return ___U3CdescriptorU3Ek__BackingField_3; }
	inline RenderTextureDescriptor_t1974534975 * get_address_of_U3CdescriptorU3Ek__BackingField_3() { return &___U3CdescriptorU3Ek__BackingField_3; }
	inline void set_U3CdescriptorU3Ek__BackingField_3(RenderTextureDescriptor_t1974534975  value)
	{
		___U3CdescriptorU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPARENTPOSTPROCESSPASS_T4286252520_H
#ifndef RENDERPIPELINEASSET_T533890058_H
#define RENDERPIPELINEASSET_T533890058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipelineAsset
struct  RenderPipelineAsset_t533890058  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.Rendering.IRenderPipeline> UnityEngine.Experimental.Rendering.RenderPipelineAsset::m_CreatedPipelines
	List_1_t2329883747 * ___m_CreatedPipelines_4;

public:
	inline static int32_t get_offset_of_m_CreatedPipelines_4() { return static_cast<int32_t>(offsetof(RenderPipelineAsset_t533890058, ___m_CreatedPipelines_4)); }
	inline List_1_t2329883747 * get_m_CreatedPipelines_4() const { return ___m_CreatedPipelines_4; }
	inline List_1_t2329883747 ** get_address_of_m_CreatedPipelines_4() { return &___m_CreatedPipelines_4; }
	inline void set_m_CreatedPipelines_4(List_1_t2329883747 * value)
	{
		___m_CreatedPipelines_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedPipelines_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINEASSET_T533890058_H
#ifndef LIGHTWEIGHTPIPELINEASSET_T3453435338_H
#define LIGHTWEIGHTPIPELINEASSET_T3453435338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset
struct  LightweightPipelineAsset_t3453435338  : public RenderPipelineAsset_t533890058
{
public:
	// UnityEngine.Shader UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_DefaultShader
	Shader_t4151988712 * ___m_DefaultShader_7;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::k_AssetVersion
	int32_t ___k_AssetVersion_8;
	// System.Int32 UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_MaxPixelLights
	int32_t ___m_MaxPixelLights_9;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_SupportsVertexLight
	bool ___m_SupportsVertexLight_10;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_RequireDepthTexture
	bool ___m_RequireDepthTexture_11;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_RequireSoftParticles
	bool ___m_RequireSoftParticles_12;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_RequireOpaqueTexture
	bool ___m_RequireOpaqueTexture_13;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.Downsampling UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_OpaqueDownsampling
	int32_t ___m_OpaqueDownsampling_14;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_SupportsHDR
	bool ___m_SupportsHDR_15;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.MSAAQuality UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_MSAA
	int32_t ___m_MSAA_16;
	// System.Single UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_RenderScale
	float ___m_RenderScale_17;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_SupportsDynamicBatching
	bool ___m_SupportsDynamicBatching_18;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_DirectionalShadowsSupported
	bool ___m_DirectionalShadowsSupported_19;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowResolution UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_ShadowAtlasResolution
	int32_t ___m_ShadowAtlasResolution_20;
	// System.Single UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_ShadowDistance
	float ___m_ShadowDistance_21;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowCascades UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_ShadowCascades
	int32_t ___m_ShadowCascades_22;
	// System.Single UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_Cascade2Split
	float ___m_Cascade2Split_23;
	// UnityEngine.Vector3 UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_Cascade4Split
	Vector3_t3722313464  ___m_Cascade4Split_24;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_LocalShadowsSupported
	bool ___m_LocalShadowsSupported_25;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowResolution UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_LocalShadowsAtlasResolution
	int32_t ___m_LocalShadowsAtlasResolution_26;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_SoftShadowsSupported
	bool ___m_SoftShadowsSupported_27;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_KeepAdditionalLightVariants
	bool ___m_KeepAdditionalLightVariants_28;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_KeepVertexLightVariants
	bool ___m_KeepVertexLightVariants_29;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_KeepDirectionalShadowVariants
	bool ___m_KeepDirectionalShadowVariants_30;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_KeepLocalShadowVariants
	bool ___m_KeepLocalShadowVariants_31;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_KeepSoftShadowVariants
	bool ___m_KeepSoftShadowVariants_32;
	// LightweightPipelineResources UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_ResourcesAsset
	LightweightPipelineResources_t2373355997 * ___m_ResourcesAsset_33;
	// UnityEngine.Experimental.Rendering.XRGraphicsConfig UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_SavedXRConfig
	XRGraphicsConfig_t170342676 * ___m_SavedXRConfig_34;
	// UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowType UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::m_ShadowType
	int32_t ___m_ShadowType_35;

public:
	inline static int32_t get_offset_of_m_DefaultShader_7() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_DefaultShader_7)); }
	inline Shader_t4151988712 * get_m_DefaultShader_7() const { return ___m_DefaultShader_7; }
	inline Shader_t4151988712 ** get_address_of_m_DefaultShader_7() { return &___m_DefaultShader_7; }
	inline void set_m_DefaultShader_7(Shader_t4151988712 * value)
	{
		___m_DefaultShader_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultShader_7), value);
	}

	inline static int32_t get_offset_of_k_AssetVersion_8() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___k_AssetVersion_8)); }
	inline int32_t get_k_AssetVersion_8() const { return ___k_AssetVersion_8; }
	inline int32_t* get_address_of_k_AssetVersion_8() { return &___k_AssetVersion_8; }
	inline void set_k_AssetVersion_8(int32_t value)
	{
		___k_AssetVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxPixelLights_9() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_MaxPixelLights_9)); }
	inline int32_t get_m_MaxPixelLights_9() const { return ___m_MaxPixelLights_9; }
	inline int32_t* get_address_of_m_MaxPixelLights_9() { return &___m_MaxPixelLights_9; }
	inline void set_m_MaxPixelLights_9(int32_t value)
	{
		___m_MaxPixelLights_9 = value;
	}

	inline static int32_t get_offset_of_m_SupportsVertexLight_10() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_SupportsVertexLight_10)); }
	inline bool get_m_SupportsVertexLight_10() const { return ___m_SupportsVertexLight_10; }
	inline bool* get_address_of_m_SupportsVertexLight_10() { return &___m_SupportsVertexLight_10; }
	inline void set_m_SupportsVertexLight_10(bool value)
	{
		___m_SupportsVertexLight_10 = value;
	}

	inline static int32_t get_offset_of_m_RequireDepthTexture_11() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_RequireDepthTexture_11)); }
	inline bool get_m_RequireDepthTexture_11() const { return ___m_RequireDepthTexture_11; }
	inline bool* get_address_of_m_RequireDepthTexture_11() { return &___m_RequireDepthTexture_11; }
	inline void set_m_RequireDepthTexture_11(bool value)
	{
		___m_RequireDepthTexture_11 = value;
	}

	inline static int32_t get_offset_of_m_RequireSoftParticles_12() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_RequireSoftParticles_12)); }
	inline bool get_m_RequireSoftParticles_12() const { return ___m_RequireSoftParticles_12; }
	inline bool* get_address_of_m_RequireSoftParticles_12() { return &___m_RequireSoftParticles_12; }
	inline void set_m_RequireSoftParticles_12(bool value)
	{
		___m_RequireSoftParticles_12 = value;
	}

	inline static int32_t get_offset_of_m_RequireOpaqueTexture_13() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_RequireOpaqueTexture_13)); }
	inline bool get_m_RequireOpaqueTexture_13() const { return ___m_RequireOpaqueTexture_13; }
	inline bool* get_address_of_m_RequireOpaqueTexture_13() { return &___m_RequireOpaqueTexture_13; }
	inline void set_m_RequireOpaqueTexture_13(bool value)
	{
		___m_RequireOpaqueTexture_13 = value;
	}

	inline static int32_t get_offset_of_m_OpaqueDownsampling_14() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_OpaqueDownsampling_14)); }
	inline int32_t get_m_OpaqueDownsampling_14() const { return ___m_OpaqueDownsampling_14; }
	inline int32_t* get_address_of_m_OpaqueDownsampling_14() { return &___m_OpaqueDownsampling_14; }
	inline void set_m_OpaqueDownsampling_14(int32_t value)
	{
		___m_OpaqueDownsampling_14 = value;
	}

	inline static int32_t get_offset_of_m_SupportsHDR_15() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_SupportsHDR_15)); }
	inline bool get_m_SupportsHDR_15() const { return ___m_SupportsHDR_15; }
	inline bool* get_address_of_m_SupportsHDR_15() { return &___m_SupportsHDR_15; }
	inline void set_m_SupportsHDR_15(bool value)
	{
		___m_SupportsHDR_15 = value;
	}

	inline static int32_t get_offset_of_m_MSAA_16() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_MSAA_16)); }
	inline int32_t get_m_MSAA_16() const { return ___m_MSAA_16; }
	inline int32_t* get_address_of_m_MSAA_16() { return &___m_MSAA_16; }
	inline void set_m_MSAA_16(int32_t value)
	{
		___m_MSAA_16 = value;
	}

	inline static int32_t get_offset_of_m_RenderScale_17() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_RenderScale_17)); }
	inline float get_m_RenderScale_17() const { return ___m_RenderScale_17; }
	inline float* get_address_of_m_RenderScale_17() { return &___m_RenderScale_17; }
	inline void set_m_RenderScale_17(float value)
	{
		___m_RenderScale_17 = value;
	}

	inline static int32_t get_offset_of_m_SupportsDynamicBatching_18() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_SupportsDynamicBatching_18)); }
	inline bool get_m_SupportsDynamicBatching_18() const { return ___m_SupportsDynamicBatching_18; }
	inline bool* get_address_of_m_SupportsDynamicBatching_18() { return &___m_SupportsDynamicBatching_18; }
	inline void set_m_SupportsDynamicBatching_18(bool value)
	{
		___m_SupportsDynamicBatching_18 = value;
	}

	inline static int32_t get_offset_of_m_DirectionalShadowsSupported_19() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_DirectionalShadowsSupported_19)); }
	inline bool get_m_DirectionalShadowsSupported_19() const { return ___m_DirectionalShadowsSupported_19; }
	inline bool* get_address_of_m_DirectionalShadowsSupported_19() { return &___m_DirectionalShadowsSupported_19; }
	inline void set_m_DirectionalShadowsSupported_19(bool value)
	{
		___m_DirectionalShadowsSupported_19 = value;
	}

	inline static int32_t get_offset_of_m_ShadowAtlasResolution_20() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_ShadowAtlasResolution_20)); }
	inline int32_t get_m_ShadowAtlasResolution_20() const { return ___m_ShadowAtlasResolution_20; }
	inline int32_t* get_address_of_m_ShadowAtlasResolution_20() { return &___m_ShadowAtlasResolution_20; }
	inline void set_m_ShadowAtlasResolution_20(int32_t value)
	{
		___m_ShadowAtlasResolution_20 = value;
	}

	inline static int32_t get_offset_of_m_ShadowDistance_21() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_ShadowDistance_21)); }
	inline float get_m_ShadowDistance_21() const { return ___m_ShadowDistance_21; }
	inline float* get_address_of_m_ShadowDistance_21() { return &___m_ShadowDistance_21; }
	inline void set_m_ShadowDistance_21(float value)
	{
		___m_ShadowDistance_21 = value;
	}

	inline static int32_t get_offset_of_m_ShadowCascades_22() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_ShadowCascades_22)); }
	inline int32_t get_m_ShadowCascades_22() const { return ___m_ShadowCascades_22; }
	inline int32_t* get_address_of_m_ShadowCascades_22() { return &___m_ShadowCascades_22; }
	inline void set_m_ShadowCascades_22(int32_t value)
	{
		___m_ShadowCascades_22 = value;
	}

	inline static int32_t get_offset_of_m_Cascade2Split_23() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_Cascade2Split_23)); }
	inline float get_m_Cascade2Split_23() const { return ___m_Cascade2Split_23; }
	inline float* get_address_of_m_Cascade2Split_23() { return &___m_Cascade2Split_23; }
	inline void set_m_Cascade2Split_23(float value)
	{
		___m_Cascade2Split_23 = value;
	}

	inline static int32_t get_offset_of_m_Cascade4Split_24() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_Cascade4Split_24)); }
	inline Vector3_t3722313464  get_m_Cascade4Split_24() const { return ___m_Cascade4Split_24; }
	inline Vector3_t3722313464 * get_address_of_m_Cascade4Split_24() { return &___m_Cascade4Split_24; }
	inline void set_m_Cascade4Split_24(Vector3_t3722313464  value)
	{
		___m_Cascade4Split_24 = value;
	}

	inline static int32_t get_offset_of_m_LocalShadowsSupported_25() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_LocalShadowsSupported_25)); }
	inline bool get_m_LocalShadowsSupported_25() const { return ___m_LocalShadowsSupported_25; }
	inline bool* get_address_of_m_LocalShadowsSupported_25() { return &___m_LocalShadowsSupported_25; }
	inline void set_m_LocalShadowsSupported_25(bool value)
	{
		___m_LocalShadowsSupported_25 = value;
	}

	inline static int32_t get_offset_of_m_LocalShadowsAtlasResolution_26() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_LocalShadowsAtlasResolution_26)); }
	inline int32_t get_m_LocalShadowsAtlasResolution_26() const { return ___m_LocalShadowsAtlasResolution_26; }
	inline int32_t* get_address_of_m_LocalShadowsAtlasResolution_26() { return &___m_LocalShadowsAtlasResolution_26; }
	inline void set_m_LocalShadowsAtlasResolution_26(int32_t value)
	{
		___m_LocalShadowsAtlasResolution_26 = value;
	}

	inline static int32_t get_offset_of_m_SoftShadowsSupported_27() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_SoftShadowsSupported_27)); }
	inline bool get_m_SoftShadowsSupported_27() const { return ___m_SoftShadowsSupported_27; }
	inline bool* get_address_of_m_SoftShadowsSupported_27() { return &___m_SoftShadowsSupported_27; }
	inline void set_m_SoftShadowsSupported_27(bool value)
	{
		___m_SoftShadowsSupported_27 = value;
	}

	inline static int32_t get_offset_of_m_KeepAdditionalLightVariants_28() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_KeepAdditionalLightVariants_28)); }
	inline bool get_m_KeepAdditionalLightVariants_28() const { return ___m_KeepAdditionalLightVariants_28; }
	inline bool* get_address_of_m_KeepAdditionalLightVariants_28() { return &___m_KeepAdditionalLightVariants_28; }
	inline void set_m_KeepAdditionalLightVariants_28(bool value)
	{
		___m_KeepAdditionalLightVariants_28 = value;
	}

	inline static int32_t get_offset_of_m_KeepVertexLightVariants_29() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_KeepVertexLightVariants_29)); }
	inline bool get_m_KeepVertexLightVariants_29() const { return ___m_KeepVertexLightVariants_29; }
	inline bool* get_address_of_m_KeepVertexLightVariants_29() { return &___m_KeepVertexLightVariants_29; }
	inline void set_m_KeepVertexLightVariants_29(bool value)
	{
		___m_KeepVertexLightVariants_29 = value;
	}

	inline static int32_t get_offset_of_m_KeepDirectionalShadowVariants_30() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_KeepDirectionalShadowVariants_30)); }
	inline bool get_m_KeepDirectionalShadowVariants_30() const { return ___m_KeepDirectionalShadowVariants_30; }
	inline bool* get_address_of_m_KeepDirectionalShadowVariants_30() { return &___m_KeepDirectionalShadowVariants_30; }
	inline void set_m_KeepDirectionalShadowVariants_30(bool value)
	{
		___m_KeepDirectionalShadowVariants_30 = value;
	}

	inline static int32_t get_offset_of_m_KeepLocalShadowVariants_31() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_KeepLocalShadowVariants_31)); }
	inline bool get_m_KeepLocalShadowVariants_31() const { return ___m_KeepLocalShadowVariants_31; }
	inline bool* get_address_of_m_KeepLocalShadowVariants_31() { return &___m_KeepLocalShadowVariants_31; }
	inline void set_m_KeepLocalShadowVariants_31(bool value)
	{
		___m_KeepLocalShadowVariants_31 = value;
	}

	inline static int32_t get_offset_of_m_KeepSoftShadowVariants_32() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_KeepSoftShadowVariants_32)); }
	inline bool get_m_KeepSoftShadowVariants_32() const { return ___m_KeepSoftShadowVariants_32; }
	inline bool* get_address_of_m_KeepSoftShadowVariants_32() { return &___m_KeepSoftShadowVariants_32; }
	inline void set_m_KeepSoftShadowVariants_32(bool value)
	{
		___m_KeepSoftShadowVariants_32 = value;
	}

	inline static int32_t get_offset_of_m_ResourcesAsset_33() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_ResourcesAsset_33)); }
	inline LightweightPipelineResources_t2373355997 * get_m_ResourcesAsset_33() const { return ___m_ResourcesAsset_33; }
	inline LightweightPipelineResources_t2373355997 ** get_address_of_m_ResourcesAsset_33() { return &___m_ResourcesAsset_33; }
	inline void set_m_ResourcesAsset_33(LightweightPipelineResources_t2373355997 * value)
	{
		___m_ResourcesAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_ResourcesAsset_33), value);
	}

	inline static int32_t get_offset_of_m_SavedXRConfig_34() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_SavedXRConfig_34)); }
	inline XRGraphicsConfig_t170342676 * get_m_SavedXRConfig_34() const { return ___m_SavedXRConfig_34; }
	inline XRGraphicsConfig_t170342676 ** get_address_of_m_SavedXRConfig_34() { return &___m_SavedXRConfig_34; }
	inline void set_m_SavedXRConfig_34(XRGraphicsConfig_t170342676 * value)
	{
		___m_SavedXRConfig_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_SavedXRConfig_34), value);
	}

	inline static int32_t get_offset_of_m_ShadowType_35() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338, ___m_ShadowType_35)); }
	inline int32_t get_m_ShadowType_35() const { return ___m_ShadowType_35; }
	inline int32_t* get_address_of_m_ShadowType_35() { return &___m_ShadowType_35; }
	inline void set_m_ShadowType_35(int32_t value)
	{
		___m_ShadowType_35 = value;
	}
};

struct LightweightPipelineAsset_t3453435338_StaticFields
{
public:
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::s_SearchPathProject
	String_t* ___s_SearchPathProject_5;
	// System.String UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightPipelineAsset::s_SearchPathPackage
	String_t* ___s_SearchPathPackage_6;

public:
	inline static int32_t get_offset_of_s_SearchPathProject_5() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338_StaticFields, ___s_SearchPathProject_5)); }
	inline String_t* get_s_SearchPathProject_5() const { return ___s_SearchPathProject_5; }
	inline String_t** get_address_of_s_SearchPathProject_5() { return &___s_SearchPathProject_5; }
	inline void set_s_SearchPathProject_5(String_t* value)
	{
		___s_SearchPathProject_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_SearchPathProject_5), value);
	}

	inline static int32_t get_offset_of_s_SearchPathPackage_6() { return static_cast<int32_t>(offsetof(LightweightPipelineAsset_t3453435338_StaticFields, ___s_SearchPathPackage_6)); }
	inline String_t* get_s_SearchPathPackage_6() const { return ___s_SearchPathPackage_6; }
	inline String_t** get_address_of_s_SearchPathPackage_6() { return &___s_SearchPathPackage_6; }
	inline void set_s_SearchPathPackage_6(String_t* value)
	{
		___s_SearchPathPackage_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_SearchPathPackage_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTPIPELINEASSET_T3453435338_H
#ifndef RENDEROPAQUEFORWARDPASS_T31084166_H
#define RENDEROPAQUEFORWARDPASS_T31084166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderOpaqueForwardPass
struct  RenderOpaqueForwardPass_t31084166  : public LightweightForwardPass_t835756159
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEROPAQUEFORWARDPASS_T31084166_H
#ifndef RENDERTRANSPARENTFORWARDPASS_T1241286203_H
#define RENDERTRANSPARENTFORWARDPASS_T1241286203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.RenderTransparentForwardPass
struct  RenderTransparentForwardPass_t1241286203  : public LightweightForwardPass_t835756159
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTRANSPARENTFORWARDPASS_T1241286203_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_4;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_5;

public:
	inline static int32_t get_offset_of_m_Trigger_4() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_4)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_4() const { return ___m_Trigger_4; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_4() { return &___m_Trigger_4; }
	inline void set_m_Trigger_4(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_4), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_5() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_5)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_5() const { return ___m_EventPayload_5; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_5() { return &___m_EventPayload_5; }
	inline void set_m_EventPayload_5(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef DATAPRIVACY_T670264006_H
#define DATAPRIVACY_T670264006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacy
struct  DataPrivacy_t670264006  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct DataPrivacy_t670264006_StaticFields
{
public:
	// UnityEngine.Analytics.DataPrivacy UnityEngine.Analytics.DataPrivacy::<instance>k__BackingField
	DataPrivacy_t670264006 * ___U3CinstanceU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DataPrivacy_t670264006_StaticFields, ___U3CinstanceU3Ek__BackingField_14)); }
	inline DataPrivacy_t670264006 * get_U3CinstanceU3Ek__BackingField_14() const { return ___U3CinstanceU3Ek__BackingField_14; }
	inline DataPrivacy_t670264006 ** get_address_of_U3CinstanceU3Ek__BackingField_14() { return &___U3CinstanceU3Ek__BackingField_14; }
	inline void set_U3CinstanceU3Ek__BackingField_14(DataPrivacy_t670264006 * value)
	{
		___U3CinstanceU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPRIVACY_T670264006_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef LIGHTWEIGHTADDITIONALCAMERADATA_T1117036424_H
#define LIGHTWEIGHTADDITIONALCAMERADATA_T1117036424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightAdditionalCameraData
struct  LightweightAdditionalCameraData_t1117036424  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightAdditionalCameraData::renderShadows
	bool ___renderShadows_4;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightAdditionalCameraData::requiresDepthTexture
	bool ___requiresDepthTexture_5;
	// System.Boolean UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightAdditionalCameraData::requiresColorTexture
	bool ___requiresColorTexture_6;
	// System.Single UnityEngine.Experimental.Rendering.LightweightPipeline.LightweightAdditionalCameraData::m_Version
	float ___m_Version_7;

public:
	inline static int32_t get_offset_of_renderShadows_4() { return static_cast<int32_t>(offsetof(LightweightAdditionalCameraData_t1117036424, ___renderShadows_4)); }
	inline bool get_renderShadows_4() const { return ___renderShadows_4; }
	inline bool* get_address_of_renderShadows_4() { return &___renderShadows_4; }
	inline void set_renderShadows_4(bool value)
	{
		___renderShadows_4 = value;
	}

	inline static int32_t get_offset_of_requiresDepthTexture_5() { return static_cast<int32_t>(offsetof(LightweightAdditionalCameraData_t1117036424, ___requiresDepthTexture_5)); }
	inline bool get_requiresDepthTexture_5() const { return ___requiresDepthTexture_5; }
	inline bool* get_address_of_requiresDepthTexture_5() { return &___requiresDepthTexture_5; }
	inline void set_requiresDepthTexture_5(bool value)
	{
		___requiresDepthTexture_5 = value;
	}

	inline static int32_t get_offset_of_requiresColorTexture_6() { return static_cast<int32_t>(offsetof(LightweightAdditionalCameraData_t1117036424, ___requiresColorTexture_6)); }
	inline bool get_requiresColorTexture_6() const { return ___requiresColorTexture_6; }
	inline bool* get_address_of_requiresColorTexture_6() { return &___requiresColorTexture_6; }
	inline void set_requiresColorTexture_6(bool value)
	{
		___requiresColorTexture_6 = value;
	}

	inline static int32_t get_offset_of_m_Version_7() { return static_cast<int32_t>(offsetof(LightweightAdditionalCameraData_t1117036424, ___m_Version_7)); }
	inline float get_m_Version_7() const { return ___m_Version_7; }
	inline float* get_address_of_m_Version_7() { return &___m_Version_7; }
	inline void set_m_Version_7(float value)
	{
		___m_Version_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTWEIGHTADDITIONALCAMERADATA_T1117036424_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef DATAPRIVACYBUTTON_T2259119369_H
#define DATAPRIVACYBUTTON_T2259119369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DataPrivacyButton
struct  DataPrivacyButton_t2259119369  : public Button_t4055032469
{
public:
	// System.Boolean UnityEngine.Analytics.DataPrivacyButton::urlOpened
	bool ___urlOpened_19;

public:
	inline static int32_t get_offset_of_urlOpened_19() { return static_cast<int32_t>(offsetof(DataPrivacyButton_t2259119369, ___urlOpened_19)); }
	inline bool get_urlOpened_19() const { return ___urlOpened_19; }
	inline bool* get_address_of_urlOpened_19() { return &___urlOpened_19; }
	inline void set_urlOpened_19(bool value)
	{
		___urlOpened_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAPRIVACYBUTTON_T2259119369_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (ReflectionUtils_t1067364495), -1, sizeof(ReflectionUtils_t1067364495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4500[1] = 
{
	ReflectionUtils_t1067364495_StaticFields::get_offset_of_EmptyObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (GetDelegate_t3939479301), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (SetDelegate_t920366853), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (ConstructorDelegate_t3127338789), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4505[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey0_t3903434793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4506[1] = 
{
	U3CGetConstructorByReflectionU3Ec__AnonStorey0_t3903434793::get_offset_of_constructorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t1858150574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4507[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t1858150574::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1858150577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4508[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1858150577::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t651529835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4509[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t651529835::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t651529832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4510[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t651529832::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (UnsupportedPlatform_t2036049172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4511[1] = 
{
	UnsupportedPlatform_t2036049172::get_offset_of_OnFinish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (U3CModuleU3E_t692745579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4513[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (U3CModuleU3E_t692745580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (DataPrivacy_t670264006), -1, sizeof(DataPrivacy_t670264006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4515[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DataPrivacy_t670264006_StaticFields::get_offset_of_U3CinstanceU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (UserPostData_t478425489)+ sizeof (RuntimeObject), sizeof(UserPostData_t478425489_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4516[9] = 
{
	UserPostData_t478425489::get_offset_of_appid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_userid_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_sessionid_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_platform_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_platformid_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_sdk_ver_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_debug_device_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_deviceid_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UserPostData_t478425489::get_offset_of_plugin_ver_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (OptOutStatus_t3541615854)+ sizeof (RuntimeObject), sizeof(OptOutStatus_t3541615854_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4517[5] = 
{
	OptOutStatus_t3541615854::get_offset_of_optOut_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptOutStatus_t3541615854::get_offset_of_analyticsEnabled_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptOutStatus_t3541615854::get_offset_of_deviceStatsEnabled_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptOutStatus_t3541615854::get_offset_of_limitUserTracking_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptOutStatus_t3541615854::get_offset_of_performanceReportingEnabled_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { sizeof (RequestData_t3101179086)+ sizeof (RuntimeObject), sizeof(RequestData_t3101179086_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4518[1] = 
{
	RequestData_t3101179086::get_offset_of_date_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (OptOutResponse_t2266495071)+ sizeof (RuntimeObject), sizeof(OptOutResponse_t2266495071_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4519[2] = 
{
	OptOutResponse_t2266495071::get_offset_of_request_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptOutResponse_t2266495071::get_offset_of_status_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { sizeof (TokenData_t2077495713)+ sizeof (RuntimeObject), sizeof(TokenData_t2077495713_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4520[2] = 
{
	TokenData_t2077495713::get_offset_of_url_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TokenData_t2077495713::get_offset_of_token_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (DataPrivacyUtils_t3678541040), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { sizeof (U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4522[11] = 
{
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3ClocalOptOutStatusU3E__0_0(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3CuserDataU3E__0_1(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3CqueryU3E__0_2(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3CbaseUriU3E__0_3(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3CuriU3E__0_4(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3CwwwU3E__0_5(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_optOutAction_6(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U3CoptOutStatusU3E__1_7(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U24current_8(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U24disposing_9(),
	U3CFetchOptOutStatusCoroutineU3Ec__Iterator0_t2643221616::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { sizeof (U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4523[8] = 
{
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_U3CpostJsonU3E__0_0(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_U3CbytesU3E__0_1(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_U3CwwwU3E__0_2(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_failure_3(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_success_4(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_U24current_5(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_U24disposing_6(),
	U3CFetchPrivacyUrlCoroutineU3Ec__Iterator1_t3219080383::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { sizeof (DataPrivacyButton_t2259119369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4524[1] = 
{
	DataPrivacyButton_t2259119369::get_offset_of_urlOpened_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { sizeof (U3CModuleU3E_t692745581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (ShadowCascades_t4003799638)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4526[4] = 
{
	ShadowCascades_t4003799638::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (ShadowType_t4250716912)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4527[4] = 
{
	ShadowType_t4250716912::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (ShadowResolution_t17500100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4528[6] = 
{
	ShadowResolution_t17500100::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (MSAAQuality_t3766701580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4529[5] = 
{
	MSAAQuality_t3766701580::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (Downsampling_t3130013916)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4530[5] = 
{
	Downsampling_t3130013916::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (DefaultMaterialType_t2904751804)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4531[5] = 
{
	DefaultMaterialType_t2904751804::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (LightweightPipelineAsset_t3453435338), -1, sizeof(LightweightPipelineAsset_t3453435338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4532[31] = 
{
	LightweightPipelineAsset_t3453435338_StaticFields::get_offset_of_s_SearchPathProject_5(),
	LightweightPipelineAsset_t3453435338_StaticFields::get_offset_of_s_SearchPathPackage_6(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_DefaultShader_7(),
	LightweightPipelineAsset_t3453435338::get_offset_of_k_AssetVersion_8(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_MaxPixelLights_9(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_SupportsVertexLight_10(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_RequireDepthTexture_11(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_RequireSoftParticles_12(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_RequireOpaqueTexture_13(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_OpaqueDownsampling_14(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_SupportsHDR_15(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_MSAA_16(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_RenderScale_17(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_SupportsDynamicBatching_18(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_DirectionalShadowsSupported_19(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_ShadowAtlasResolution_20(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_ShadowDistance_21(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_ShadowCascades_22(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_Cascade2Split_23(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_Cascade4Split_24(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_LocalShadowsSupported_25(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_LocalShadowsAtlasResolution_26(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_SoftShadowsSupported_27(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_KeepAdditionalLightVariants_28(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_KeepVertexLightVariants_29(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_KeepDirectionalShadowVariants_30(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_KeepLocalShadowVariants_31(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_KeepSoftShadowVariants_32(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_ResourcesAsset_33(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_SavedXRConfig_34(),
	LightweightPipelineAsset_t3453435338::get_offset_of_m_ShadowType_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (LightweightPipelineEditorResources_t3077566732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[3] = 
{
	LightweightPipelineEditorResources_t3077566732::get_offset_of_DefaultMaterial_4(),
	LightweightPipelineEditorResources_t3077566732::get_offset_of_DefaultParticleMaterial_5(),
	LightweightPipelineEditorResources_t3077566732::get_offset_of_DefaultTerrainMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (LightweightPipelineResources_t2373355997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4534[4] = 
{
	LightweightPipelineResources_t2373355997::get_offset_of_BlitShader_4(),
	LightweightPipelineResources_t2373355997::get_offset_of_CopyDepthShader_5(),
	LightweightPipelineResources_t2373355997::get_offset_of_ScreenSpaceShadowShader_6(),
	LightweightPipelineResources_t2373355997::get_offset_of_SamplingShader_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (DefaultRendererSetup_t3647655062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4535[25] = 
{
	DefaultRendererSetup_t3647655062::get_offset_of_m_DepthOnlyPass_0(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_DirectionalShadowPass_1(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_LocalShadowPass_2(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_SetupForwardRenderingPass_3(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_ScreenSpaceShadowResovePass_4(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_CreateLightweightRenderTexturesPass_5(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_BeginXrRenderingPass_6(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_SetupLightweightConstants_7(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_RenderOpaqueForwardPass_8(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_OpaquePostProcessPass_9(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_DrawSkyboxPass_10(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_CopyDepthPass_11(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_CopyColorPass_12(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_RenderTransparentForwardPass_13(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_TransparentPostProcessPass_14(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_FinalBlitPass_15(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_EndXrRenderingPass_16(),
	DefaultRendererSetup_t3647655062::get_offset_of_Color_17(),
	DefaultRendererSetup_t3647655062::get_offset_of_DepthAttachment_18(),
	DefaultRendererSetup_t3647655062::get_offset_of_DepthTexture_19(),
	DefaultRendererSetup_t3647655062::get_offset_of_OpaqueColor_20(),
	DefaultRendererSetup_t3647655062::get_offset_of_DirectionalShadowmap_21(),
	DefaultRendererSetup_t3647655062::get_offset_of_LocalShadowmap_22(),
	DefaultRendererSetup_t3647655062::get_offset_of_ScreenSpaceShadowmap_23(),
	DefaultRendererSetup_t3647655062::get_offset_of_m_Initialized_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (LightweightAdditionalCameraData_t1117036424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4537[4] = 
{
	LightweightAdditionalCameraData_t1117036424::get_offset_of_renderShadows_4(),
	LightweightAdditionalCameraData_t1117036424::get_offset_of_requiresDepthTexture_5(),
	LightweightAdditionalCameraData_t1117036424::get_offset_of_requiresColorTexture_6(),
	LightweightAdditionalCameraData_t1117036424::get_offset_of_m_Version_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (LightweightForwardRenderer_t94815316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4538[9] = 
{
	0,
	0,
	0,
	LightweightForwardRenderer_t94815316::get_offset_of_U3CpostProcessRenderContextU3Ek__BackingField_3(),
	LightweightForwardRenderer_t94815316::get_offset_of_U3CperObjectLightIndicesU3Ek__BackingField_4(),
	LightweightForwardRenderer_t94815316::get_offset_of_U3CopaqueFilterSettingsU3Ek__BackingField_5(),
	LightweightForwardRenderer_t94815316::get_offset_of_U3CtransparentFilterSettingsU3Ek__BackingField_6(),
	LightweightForwardRenderer_t94815316::get_offset_of_m_ActiveRenderPassQueue_7(),
	LightweightForwardRenderer_t94815316::get_offset_of_m_Materials_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (LightweightPipeline_t2674386577), -1, sizeof(LightweightPipeline_t2674386577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4539[9] = 
{
	LightweightPipeline_t2674386577::get_offset_of_U3CpipelineAssetU3Ek__BackingField_3(),
	LightweightPipeline_t2674386577::get_offset_of_m_CameraComparer_4(),
	LightweightPipeline_t2674386577::get_offset_of_m_Renderer_5(),
	LightweightPipeline_t2674386577::get_offset_of_m_CullResults_6(),
	LightweightPipeline_t2674386577::get_offset_of_m_LocalLightIndices_7(),
	LightweightPipeline_t2674386577::get_offset_of_m_IsCameraRendering_8(),
	LightweightPipeline_t2674386577::get_offset_of_m_DefaultRendererSetup_9(),
	LightweightPipeline_t2674386577_StaticFields::get_offset_of_s_FullscreenMesh_10(),
	LightweightPipeline_t2674386577_StaticFields::get_offset_of_s_PipelineCapabilities_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (PerFrameBuffer_t2481871112), -1, sizeof(PerFrameBuffer_t2481871112_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4540[2] = 
{
	PerFrameBuffer_t2481871112_StaticFields::get_offset_of__GlossyEnvironmentColor_0(),
	PerFrameBuffer_t2481871112_StaticFields::get_offset_of__SubtractiveShadowColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (PipelineCapabilities_t2057244252)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4541[6] = 
{
	PipelineCapabilities_t2057244252::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (MixedLightingSetup_t2219919647)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4542[4] = 
{
	MixedLightingSetup_t2219919647::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (RenderingData_t2420043686)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4543[4] = 
{
	RenderingData_t2420043686::get_offset_of_cameraData_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RenderingData_t2420043686::get_offset_of_lightData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RenderingData_t2420043686::get_offset_of_shadowData_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RenderingData_t2420043686::get_offset_of_supportsDynamicBatching_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (LightData_t718835846)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4544[5] = 
{
	LightData_t718835846::get_offset_of_pixelAdditionalLightsCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LightData_t718835846::get_offset_of_totalAdditionalLightsCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LightData_t718835846::get_offset_of_mainLightIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LightData_t718835846::get_offset_of_visibleLights_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LightData_t718835846::get_offset_of_visibleLocalLightIndices_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (CameraData_t2373231130)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4545[15] = 
{
	CameraData_t2373231130::get_offset_of_camera_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_renderScale_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_msaaSamples_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_isSceneViewCamera_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_isDefaultViewport_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_isOffscreenRender_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_isHdrEnabled_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_requiresDepthTexture_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_requiresSoftParticles_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_requiresOpaqueTexture_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_opaqueTextureDownsampling_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_isStereoEnabled_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_maxShadowDistance_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_postProcessEnabled_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraData_t2373231130::get_offset_of_postProcessLayer_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (ShadowData_t294369652)+ sizeof (RuntimeObject), sizeof(ShadowData_t294369652_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4546[13] = 
{
	ShadowData_t294369652::get_offset_of_renderDirectionalShadows_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_requiresScreenSpaceShadowResolve_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_directionalShadowAtlasWidth_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_directionalShadowAtlasHeight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_directionalLightCascadeCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_directionalLightCascades_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_renderLocalShadows_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_localShadowAtlasWidth_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_localShadowAtlasHeight_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_supportsSoftShadows_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_bufferBitCount_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_renderedDirectionalShadowQuality_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowData_t294369652::get_offset_of_renderedLocalShadowQuality_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (CameraComparer_t3166570673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (LightweightKeywordStrings_t3199804416), -1, sizeof(LightweightKeywordStrings_t3199804416_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4548[11] = 
{
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_AdditionalLights_0(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_VertexLights_1(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_MixedLightingSubtractive_2(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_MainLightCookie_3(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_DirectionalShadows_4(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_LocalShadows_5(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_SoftShadows_6(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_CascadeShadows_7(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_DepthNoMsaa_8(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_DepthMsaa2_9(),
	LightweightKeywordStrings_t3199804416_StaticFields::get_offset_of_DepthMsaa4_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (ShaderPathID_t2494934061)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4549[10] = 
{
	ShaderPathID_t2494934061::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (LightweightShaderUtils_t2293681725), -1, sizeof(LightweightShaderUtils_t2293681725_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4550[1] = 
{
	LightweightShaderUtils_t2293681725_StaticFields::get_offset_of_m_ShaderPaths_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (ShadowSliceData_t800660445)+ sizeof (RuntimeObject), sizeof(ShadowSliceData_t800660445 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4551[4] = 
{
	ShadowSliceData_t800660445::get_offset_of_shadowTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowSliceData_t800660445::get_offset_of_offsetX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowSliceData_t800660445::get_offset_of_offsetY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShadowSliceData_t800660445::get_offset_of_resolution_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (LightweightShadowUtils_t1883746463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (MaterialHandles_t3674233906)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4553[7] = 
{
	MaterialHandles_t3674233906::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (BeginXRRenderingPass_t4122675669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (CopyColorPass_t3734904897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4555[5] = 
{
	CopyColorPass_t3734904897::get_offset_of_m_SamplingMaterial_2(),
	CopyColorPass_t3734904897::get_offset_of_m_OpaqueScalerValues_3(),
	CopyColorPass_t3734904897::get_offset_of_m_SampleOffsetShaderHandle_4(),
	CopyColorPass_t3734904897::get_offset_of_U3CsourceU3Ek__BackingField_5(),
	CopyColorPass_t3734904897::get_offset_of_U3CdestinationU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (CopyDepthPass_t304079192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4556[3] = 
{
	CopyDepthPass_t304079192::get_offset_of_m_DepthCopyMaterial_2(),
	CopyDepthPass_t304079192::get_offset_of_U3CsourceU3Ek__BackingField_3(),
	CopyDepthPass_t304079192::get_offset_of_U3CdestinationU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (CreateLightweightRenderTexturesPass_t2359216577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4557[5] = 
{
	0,
	CreateLightweightRenderTexturesPass_t2359216577::get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_3(),
	CreateLightweightRenderTexturesPass_t2359216577::get_offset_of_U3CdepthAttachmentHandleU3Ek__BackingField_4(),
	CreateLightweightRenderTexturesPass_t2359216577::get_offset_of_U3CdescriptorU3Ek__BackingField_5(),
	CreateLightweightRenderTexturesPass_t2359216577::get_offset_of_U3CsamplesU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (DepthOnlyPass_t4004473585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4558[4] = 
{
	0,
	DepthOnlyPass_t4004473585::get_offset_of_kDepthBufferBits_3(),
	DepthOnlyPass_t4004473585::get_offset_of_U3CdepthAttachmentHandleU3Ek__BackingField_4(),
	DepthOnlyPass_t4004473585::get_offset_of_U3CdescriptorU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (DirectionalShadowsPass_t4026755168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4559[10] = 
{
	0,
	0,
	DirectionalShadowsPass_t4026755168::get_offset_of_m_ShadowCasterCascadesCount_4(),
	DirectionalShadowsPass_t4026755168::get_offset_of_m_DirectionalShadowmapTexture_5(),
	DirectionalShadowsPass_t4026755168::get_offset_of_m_ShadowmapFormat_6(),
	DirectionalShadowsPass_t4026755168::get_offset_of_m_DirectionalShadowMatrices_7(),
	DirectionalShadowsPass_t4026755168::get_offset_of_m_CascadeSlices_8(),
	DirectionalShadowsPass_t4026755168::get_offset_of_m_CascadeSplitDistances_9(),
	0,
	DirectionalShadowsPass_t4026755168::get_offset_of_U3CdestinationU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (DirectionalShadowConstantBuffer_t4115518952), -1, sizeof(DirectionalShadowConstantBuffer_t4115518952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4560[12] = 
{
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__WorldToShadow_0(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__ShadowData_1(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__DirShadowSplitSpheres0_2(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__DirShadowSplitSpheres1_3(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__DirShadowSplitSpheres2_4(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__DirShadowSplitSpheres3_5(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__DirShadowSplitSphereRadii_6(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__ShadowOffset0_7(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__ShadowOffset1_8(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__ShadowOffset2_9(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__ShadowOffset3_10(),
	DirectionalShadowConstantBuffer_t4115518952_StaticFields::get_offset_of__ShadowmapSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (DrawSkyboxPass_t705056231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (EndXRRenderingPass_t1600478297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (FinalBlitPass_t30679461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4563[3] = 
{
	FinalBlitPass_t30679461::get_offset_of_m_BlitMaterial_2(),
	FinalBlitPass_t30679461::get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_3(),
	FinalBlitPass_t30679461::get_offset_of_U3CdescriptorU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (LightweightForwardPass_t835756159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4564[10] = 
{
	LightweightForwardPass_t835756159::get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_2(),
	LightweightForwardPass_t835756159::get_offset_of_U3CdepthAttachmentHandleU3Ek__BackingField_3(),
	LightweightForwardPass_t835756159::get_offset_of_U3CdescriptorU3Ek__BackingField_4(),
	LightweightForwardPass_t835756159::get_offset_of_U3CclearFlagU3Ek__BackingField_5(),
	LightweightForwardPass_t835756159::get_offset_of_U3CclearColorU3Ek__BackingField_6(),
	0,
	LightweightForwardPass_t835756159::get_offset_of_m_ErrorMaterial_8(),
	LightweightForwardPass_t835756159::get_offset_of_m_LegacyShaderPassNames_9(),
	LightweightForwardPass_t835756159::get_offset_of_rendererConfiguration_10(),
	LightweightForwardPass_t835756159::get_offset_of_dynamicBatching_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (LocalShadowsPass_t2703335903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4565[8] = 
{
	0,
	LocalShadowsPass_t2703335903::get_offset_of_m_LocalShadowmapTexture_3(),
	LocalShadowsPass_t2703335903::get_offset_of_m_LocalShadowmapFormat_4(),
	LocalShadowsPass_t2703335903::get_offset_of_m_LocalShadowMatrices_5(),
	LocalShadowsPass_t2703335903::get_offset_of_m_LocalLightSlices_6(),
	LocalShadowsPass_t2703335903::get_offset_of_m_LocalShadowStrength_7(),
	0,
	LocalShadowsPass_t2703335903::get_offset_of_U3CdestinationU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (LocalShadowConstantBuffer_t1372550715), -1, sizeof(LocalShadowConstantBuffer_t1372550715_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4566[7] = 
{
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalWorldToShadowAtlas_0(),
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalShadowStrength_1(),
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalShadowOffset0_2(),
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalShadowOffset1_3(),
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalShadowOffset2_4(),
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalShadowOffset3_5(),
	LocalShadowConstantBuffer_t1372550715_StaticFields::get_offset_of__LocalShadowmapSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (OpaquePostProcessPass_t1918180670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4567[2] = 
{
	OpaquePostProcessPass_t1918180670::get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_2(),
	OpaquePostProcessPass_t1918180670::get_offset_of_U3CdescriptorU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (RenderOpaqueForwardPass_t31084166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4568[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (RenderTransparentForwardPass_t1241286203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4569[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (SceneViewDepthCopyPass_t1517279723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4570[1] = 
{
	SceneViewDepthCopyPass_t1517279723::get_offset_of_U3CsourceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (ScreenSpaceShadowResolvePass_t442348744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4571[4] = 
{
	ScreenSpaceShadowResolvePass_t442348744::get_offset_of_m_ColorFormat_2(),
	ScreenSpaceShadowResolvePass_t442348744::get_offset_of_m_ScreenSpaceShadowsMaterial_3(),
	ScreenSpaceShadowResolvePass_t442348744::get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_4(),
	ScreenSpaceShadowResolvePass_t442348744::get_offset_of_U3CdescriptorU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (ScriptableRenderPass_t3231910016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4572[2] = 
{
	ScriptableRenderPass_t3231910016::get_offset_of_U3CrendererU3Ek__BackingField_0(),
	ScriptableRenderPass_t3231910016::get_offset_of_m_ShaderPassNames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (SetupForwardRenderingPass_t3177828444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (SetupLightweightConstanstPass_t1972706453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4574[11] = 
{
	SetupLightweightConstanstPass_t1972706453::get_offset_of_m_MixedLightingSetup_2(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_k_DefaultLightPosition_3(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_k_DefaultLightColor_4(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_k_DefaultLightAttenuation_5(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_k_DefaultLightSpotDirection_6(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_k_DefaultLightSpotAttenuation_7(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_m_LightPositions_8(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_m_LightColors_9(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_m_LightDistanceAttenuations_10(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_m_LightSpotDirections_11(),
	SetupLightweightConstanstPass_t1972706453::get_offset_of_m_LightSpotAttenuations_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (PerCameraBuffer_t439951145), -1, sizeof(PerCameraBuffer_t439951145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4575[12] = 
{
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__MainLightPosition_0(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__MainLightColor_1(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__MainLightCookie_2(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__WorldToLight_3(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__AdditionalLightCount_4(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__AdditionalLightPosition_5(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__AdditionalLightColor_6(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__AdditionalLightDistanceAttenuation_7(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__AdditionalLightSpotDir_8(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__AdditionalLightSpotAttenuation_9(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__LightIndexBuffer_10(),
	PerCameraBuffer_t439951145_StaticFields::get_offset_of__ScaledScreenParams_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (TransparentPostProcessPass_t4286252520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4576[2] = 
{
	TransparentPostProcessPass_t4286252520::get_offset_of_U3CcolorAttachmentHandleU3Ek__BackingField_2(),
	TransparentPostProcessPass_t4286252520::get_offset_of_U3CdescriptorU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (RenderTargetHandle_t3987358924)+ sizeof (RuntimeObject), sizeof(RenderTargetHandle_t3987358924 ), sizeof(RenderTargetHandle_t3987358924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4577[2] = 
{
	RenderTargetHandle_t3987358924::get_offset_of_U3CidU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RenderTargetHandle_t3987358924_StaticFields::get_offset_of_CameraTarget_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (SampleCount_t923198274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4578[4] = 
{
	SampleCount_t923198274::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255374), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4579[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
	U3CPrivateImplementationDetailsU3E_t3057255374_StaticFields::get_offset_of_U24fieldU2D295DE5F80AAB8DD796EAD8330F4E26A70AC95009_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (U24ArrayTypeU3D16_t3253128245)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128245 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (U3CModuleU3E_t692745582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4583[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_4(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4584[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4585[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4586[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4587[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4588[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4589[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4590[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4591[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4592[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4593[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4594[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4595[8] = 
{
	Trigger_t4199345191::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4596[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4597[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4598[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4599[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
