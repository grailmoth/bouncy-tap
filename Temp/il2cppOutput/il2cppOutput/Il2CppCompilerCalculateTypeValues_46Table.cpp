﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AppStoresSupport.AppStoreSetting
struct AppStoreSetting_t1592337179;
// EasyMobile.Achievement
struct Achievement_t2708948008;
// EasyMobile.AdPlacement
struct AdPlacement_t3195089512;
// EasyMobile.AnimatedClip
struct AnimatedClip_t619307834;
// EasyMobile.ClipPlayerUI
struct ClipPlayerUI_t1249565783;
// EasyMobile.ConsentDialog
struct ConsentDialog_t3732976094;
// EasyMobile.ConsentDialog/CompletedHandler
struct CompletedHandler_t441206176;
// EasyMobile.ConsentDialog/ToggleStateUpdatedHandler
struct ToggleStateUpdatedHandler_t2741348581;
// EasyMobile.Demo.BannerSection
struct BannerSection_t2412952700;
// EasyMobile.Demo.BannerSection/CustomBannerUI
struct CustomBannerUI_t529944588;
// EasyMobile.Demo.BannerSection/DefaulBannerUI
struct DefaulBannerUI_t2752449824;
// EasyMobile.Demo.ColorHandler
struct ColorHandler_t4238062851;
// EasyMobile.Demo.DemoUtils
struct DemoUtils_t1360065201;
// EasyMobile.Demo.GameServicesDemo_SavedGames
struct GameServicesDemo_SavedGames_t941468990;
// EasyMobile.Demo.InAppPurchasingDemo
struct InAppPurchasingDemo_t2157663715;
// EasyMobile.Demo.InterstitialSection
struct InterstitialSection_t4034589797;
// EasyMobile.Demo.InterstitialSection/CustomInterstitialUI
struct CustomInterstitialUI_t2716953283;
// EasyMobile.Demo.InterstitialSection/DefaultInterstitialUI
struct DefaultInterstitialUI_t2954553843;
// EasyMobile.Demo.RewardedVideoSection
struct RewardedVideoSection_t2849218130;
// EasyMobile.Demo.RewardedVideoSection/CustomRewardedVideoUI
struct CustomRewardedVideoUI_t2933782405;
// EasyMobile.Demo.RewardedVideoSection/DefaultRewardedVideolUI
struct DefaultRewardedVideolUI_t2208290187;
// EasyMobile.Demo.ScrollableList
struct ScrollableList_t4043704206;
// EasyMobile.Demo.SharingDemo
struct SharingDemo_t2942947019;
// EasyMobile.IAPProduct
struct IAPProduct_t1805032951;
// EasyMobile.IAPProduct[]
struct IAPProductU5BU5D_t2701488206;
// EasyMobile.Leaderboard
struct Leaderboard_t4133458931;
// EasyMobile.Recorder
struct Recorder_t3009289404;
// EasyMobile.SavedGame
struct SavedGame_t947814848;
// EasyMobile.SavedGame[]
struct SavedGameU5BU5D_t399802177;
// IAPDemo
struct IAPDemo_t3681080565;
// IAPDemo/UnityChannelLoginHandler
struct UnityChannelLoginHandler_t2949829254;
// IAPDemo/UnityChannelPurchaseInfo
struct UnityChannelPurchaseInfo_t74063925;
// Sirenix.Serialization.Serializer`1<System.Int32>
struct Serializer_1_t332960279;
// System.Action
struct Action_t1264377477;
// System.Action`1<EasyMobile.ConsentDialog>
struct Action_1_t3905443689;
// System.Action`1<EasyMobile.Demo.ScrollableList>
struct Action_1_t4216171801;
// System.Action`1<EasyMobile.EEARegionStatus>
struct Action_1_t3300905132;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<UnityEngine.Color>
struct Action_1_t2728153919;
// System.Action`1<UnityEngine.Store.UserInfo>
struct Action_1_t3058893588;
// System.Action`3<EasyMobile.Demo.ScrollableList,System.String,System.String>
struct Action_3_t3311727287;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI>
struct Dictionary_2_t708210053;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<EasyMobile.BannerAdNetwork>
struct List_1_t1454037157;
// System.Collections.Generic.List`1<EasyMobile.BannerAdPosition>
struct List_1_t1658913812;
// System.Collections.Generic.List`1<EasyMobile.BannerAdSize>
struct List_1_t984944135;
// System.Collections.Generic.List`1<EasyMobile.IAPProduct>
struct List_1_t3277107693;
// System.Collections.Generic.List`1<EasyMobile.InterstitialAdNetwork>
struct List_1_t225486949;
// System.Collections.Generic.List`1<EasyMobile.RewardedAdNetwork>
struct List_1_t1544308239;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct List_1_t1741830302;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton>
struct List_1_t3820967359;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPListener>
struct List_1_t3473867730;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1618671084;
// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t3721407765;
// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t1729542224;
// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t1675809258;
// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t800864861;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t4146644616;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t3180538779;
// UnityEngine.Purchasing.IMicrosoftExtensions
struct IMicrosoftExtensions_t4020186927;
// UnityEngine.Purchasing.IMoolahExtension
struct IMoolahExtension_t955300474;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t2712620151;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t2579314702;
// UnityEngine.Purchasing.ITransactionHistoryExtensions
struct ITransactionHistoryExtensions_t1575111476;
// UnityEngine.Purchasing.IUnityChannelExtensions
struct IUnityChannelExtensions_t3299991497;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t3178009003;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;




#ifndef U3CMODULEU3E_T692745584_H
#define U3CMODULEU3E_T692745584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745584 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745584_H
#ifndef U3CMODULEU3E_T692745583_H
#define U3CMODULEU3E_T692745583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745583 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745583_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef APPSTORESETTING_T1592337179_H
#define APPSTORESETTING_T1592337179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSetting
struct  AppStoreSetting_t1592337179  : public RuntimeObject
{
public:
	// System.String AppStoresSupport.AppStoreSetting::AppID
	String_t* ___AppID_0;
	// System.String AppStoresSupport.AppStoreSetting::AppKey
	String_t* ___AppKey_1;
	// System.Boolean AppStoresSupport.AppStoreSetting::IsTestMode
	bool ___IsTestMode_2;

public:
	inline static int32_t get_offset_of_AppID_0() { return static_cast<int32_t>(offsetof(AppStoreSetting_t1592337179, ___AppID_0)); }
	inline String_t* get_AppID_0() const { return ___AppID_0; }
	inline String_t** get_address_of_AppID_0() { return &___AppID_0; }
	inline void set_AppID_0(String_t* value)
	{
		___AppID_0 = value;
		Il2CppCodeGenWriteBarrier((&___AppID_0), value);
	}

	inline static int32_t get_offset_of_AppKey_1() { return static_cast<int32_t>(offsetof(AppStoreSetting_t1592337179, ___AppKey_1)); }
	inline String_t* get_AppKey_1() const { return ___AppKey_1; }
	inline String_t** get_address_of_AppKey_1() { return &___AppKey_1; }
	inline void set_AppKey_1(String_t* value)
	{
		___AppKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___AppKey_1), value);
	}

	inline static int32_t get_offset_of_IsTestMode_2() { return static_cast<int32_t>(offsetof(AppStoreSetting_t1592337179, ___IsTestMode_2)); }
	inline bool get_IsTestMode_2() const { return ___IsTestMode_2; }
	inline bool* get_address_of_IsTestMode_2() { return &___IsTestMode_2; }
	inline void set_IsTestMode_2(bool value)
	{
		___IsTestMode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTING_T1592337179_H
#ifndef EM_GPGSIDS_T587911181_H
#define EM_GPGSIDS_T587911181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EM_GPGSIds
struct  EM_GPGSIds_t587911181  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EM_GPGSIDS_T587911181_H
#ifndef ADSSECTION_T4249879119_H
#define ADSSECTION_T4249879119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.AdsSection
struct  AdsSection_t4249879119  : public RuntimeObject
{
public:
	// UnityEngine.GameObject EasyMobile.Demo.AdsSection::defaultRoot
	GameObject_t1113636619 * ___defaultRoot_0;
	// UnityEngine.GameObject EasyMobile.Demo.AdsSection::customRoot
	GameObject_t1113636619 * ___customRoot_1;
	// UnityEngine.UI.Button EasyMobile.Demo.AdsSection::enableDefaultSectionButton
	Button_t4055032469 * ___enableDefaultSectionButton_2;
	// UnityEngine.UI.Button EasyMobile.Demo.AdsSection::enableCustomSectionButton
	Button_t4055032469 * ___enableCustomSectionButton_3;
	// System.Boolean EasyMobile.Demo.AdsSection::useDefaultUIAsDefault
	bool ___useDefaultUIAsDefault_4;
	// System.Boolean EasyMobile.Demo.AdsSection::<IsUsingDefaultSection>k__BackingField
	bool ___U3CIsUsingDefaultSectionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_defaultRoot_0() { return static_cast<int32_t>(offsetof(AdsSection_t4249879119, ___defaultRoot_0)); }
	inline GameObject_t1113636619 * get_defaultRoot_0() const { return ___defaultRoot_0; }
	inline GameObject_t1113636619 ** get_address_of_defaultRoot_0() { return &___defaultRoot_0; }
	inline void set_defaultRoot_0(GameObject_t1113636619 * value)
	{
		___defaultRoot_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultRoot_0), value);
	}

	inline static int32_t get_offset_of_customRoot_1() { return static_cast<int32_t>(offsetof(AdsSection_t4249879119, ___customRoot_1)); }
	inline GameObject_t1113636619 * get_customRoot_1() const { return ___customRoot_1; }
	inline GameObject_t1113636619 ** get_address_of_customRoot_1() { return &___customRoot_1; }
	inline void set_customRoot_1(GameObject_t1113636619 * value)
	{
		___customRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___customRoot_1), value);
	}

	inline static int32_t get_offset_of_enableDefaultSectionButton_2() { return static_cast<int32_t>(offsetof(AdsSection_t4249879119, ___enableDefaultSectionButton_2)); }
	inline Button_t4055032469 * get_enableDefaultSectionButton_2() const { return ___enableDefaultSectionButton_2; }
	inline Button_t4055032469 ** get_address_of_enableDefaultSectionButton_2() { return &___enableDefaultSectionButton_2; }
	inline void set_enableDefaultSectionButton_2(Button_t4055032469 * value)
	{
		___enableDefaultSectionButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___enableDefaultSectionButton_2), value);
	}

	inline static int32_t get_offset_of_enableCustomSectionButton_3() { return static_cast<int32_t>(offsetof(AdsSection_t4249879119, ___enableCustomSectionButton_3)); }
	inline Button_t4055032469 * get_enableCustomSectionButton_3() const { return ___enableCustomSectionButton_3; }
	inline Button_t4055032469 ** get_address_of_enableCustomSectionButton_3() { return &___enableCustomSectionButton_3; }
	inline void set_enableCustomSectionButton_3(Button_t4055032469 * value)
	{
		___enableCustomSectionButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___enableCustomSectionButton_3), value);
	}

	inline static int32_t get_offset_of_useDefaultUIAsDefault_4() { return static_cast<int32_t>(offsetof(AdsSection_t4249879119, ___useDefaultUIAsDefault_4)); }
	inline bool get_useDefaultUIAsDefault_4() const { return ___useDefaultUIAsDefault_4; }
	inline bool* get_address_of_useDefaultUIAsDefault_4() { return &___useDefaultUIAsDefault_4; }
	inline void set_useDefaultUIAsDefault_4(bool value)
	{
		___useDefaultUIAsDefault_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsUsingDefaultSectionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdsSection_t4249879119, ___U3CIsUsingDefaultSectionU3Ek__BackingField_5)); }
	inline bool get_U3CIsUsingDefaultSectionU3Ek__BackingField_5() const { return ___U3CIsUsingDefaultSectionU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsUsingDefaultSectionU3Ek__BackingField_5() { return &___U3CIsUsingDefaultSectionU3Ek__BackingField_5; }
	inline void set_U3CIsUsingDefaultSectionU3Ek__BackingField_5(bool value)
	{
		___U3CIsUsingDefaultSectionU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSSECTION_T4249879119_H
#ifndef U3CDELAYCOROUTINEU3EC__ITERATOR0_T2514348216_H
#define U3CDELAYCOROUTINEU3EC__ITERATOR0_T2514348216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.AdvertisingDemo/<DelayCoroutine>c__Iterator0
struct  U3CDelayCoroutineU3Ec__Iterator0_t2514348216  : public RuntimeObject
{
public:
	// System.Single EasyMobile.Demo.AdvertisingDemo/<DelayCoroutine>c__Iterator0::time
	float ___time_0;
	// System.Action EasyMobile.Demo.AdvertisingDemo/<DelayCoroutine>c__Iterator0::action
	Action_t1264377477 * ___action_1;
	// System.Object EasyMobile.Demo.AdvertisingDemo/<DelayCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EasyMobile.Demo.AdvertisingDemo/<DelayCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EasyMobile.Demo.AdvertisingDemo/<DelayCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CDelayCoroutineU3Ec__Iterator0_t2514348216, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CDelayCoroutineU3Ec__Iterator0_t2514348216, ___action_1)); }
	inline Action_t1264377477 * get_action_1() const { return ___action_1; }
	inline Action_t1264377477 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t1264377477 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayCoroutineU3Ec__Iterator0_t2514348216, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayCoroutineU3Ec__Iterator0_t2514348216, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayCoroutineU3Ec__Iterator0_t2514348216, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYCOROUTINEU3EC__ITERATOR0_T2514348216_H
#ifndef DEFAULBANNERUI_T2752449824_H
#define DEFAULBANNERUI_T2752449824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.BannerSection/DefaulBannerUI
struct  DefaulBannerUI_t2752449824  : public RuntimeObject
{
public:
	// UnityEngine.UI.Button EasyMobile.Demo.BannerSection/DefaulBannerUI::showBannerButton
	Button_t4055032469 * ___showBannerButton_0;
	// UnityEngine.UI.Button EasyMobile.Demo.BannerSection/DefaulBannerUI::hideBannerButton
	Button_t4055032469 * ___hideBannerButton_1;
	// UnityEngine.UI.Button EasyMobile.Demo.BannerSection/DefaulBannerUI::destroyBannerButton
	Button_t4055032469 * ___destroyBannerButton_2;
	// UnityEngine.UI.Dropdown EasyMobile.Demo.BannerSection/DefaulBannerUI::bannerPositionSelector
	Dropdown_t2274391225 * ___bannerPositionSelector_3;
	// UnityEngine.UI.Dropdown EasyMobile.Demo.BannerSection/DefaulBannerUI::bannerSizeSelector
	Dropdown_t2274391225 * ___bannerSizeSelector_4;
	// System.Collections.Generic.List`1<EasyMobile.BannerAdSize> EasyMobile.Demo.BannerSection/DefaulBannerUI::allBannerSizes
	List_1_t984944135 * ___allBannerSizes_5;
	// System.Collections.Generic.List`1<EasyMobile.BannerAdPosition> EasyMobile.Demo.BannerSection/DefaulBannerUI::allBannerPositions
	List_1_t1658913812 * ___allBannerPositions_6;

public:
	inline static int32_t get_offset_of_showBannerButton_0() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___showBannerButton_0)); }
	inline Button_t4055032469 * get_showBannerButton_0() const { return ___showBannerButton_0; }
	inline Button_t4055032469 ** get_address_of_showBannerButton_0() { return &___showBannerButton_0; }
	inline void set_showBannerButton_0(Button_t4055032469 * value)
	{
		___showBannerButton_0 = value;
		Il2CppCodeGenWriteBarrier((&___showBannerButton_0), value);
	}

	inline static int32_t get_offset_of_hideBannerButton_1() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___hideBannerButton_1)); }
	inline Button_t4055032469 * get_hideBannerButton_1() const { return ___hideBannerButton_1; }
	inline Button_t4055032469 ** get_address_of_hideBannerButton_1() { return &___hideBannerButton_1; }
	inline void set_hideBannerButton_1(Button_t4055032469 * value)
	{
		___hideBannerButton_1 = value;
		Il2CppCodeGenWriteBarrier((&___hideBannerButton_1), value);
	}

	inline static int32_t get_offset_of_destroyBannerButton_2() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___destroyBannerButton_2)); }
	inline Button_t4055032469 * get_destroyBannerButton_2() const { return ___destroyBannerButton_2; }
	inline Button_t4055032469 ** get_address_of_destroyBannerButton_2() { return &___destroyBannerButton_2; }
	inline void set_destroyBannerButton_2(Button_t4055032469 * value)
	{
		___destroyBannerButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___destroyBannerButton_2), value);
	}

	inline static int32_t get_offset_of_bannerPositionSelector_3() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___bannerPositionSelector_3)); }
	inline Dropdown_t2274391225 * get_bannerPositionSelector_3() const { return ___bannerPositionSelector_3; }
	inline Dropdown_t2274391225 ** get_address_of_bannerPositionSelector_3() { return &___bannerPositionSelector_3; }
	inline void set_bannerPositionSelector_3(Dropdown_t2274391225 * value)
	{
		___bannerPositionSelector_3 = value;
		Il2CppCodeGenWriteBarrier((&___bannerPositionSelector_3), value);
	}

	inline static int32_t get_offset_of_bannerSizeSelector_4() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___bannerSizeSelector_4)); }
	inline Dropdown_t2274391225 * get_bannerSizeSelector_4() const { return ___bannerSizeSelector_4; }
	inline Dropdown_t2274391225 ** get_address_of_bannerSizeSelector_4() { return &___bannerSizeSelector_4; }
	inline void set_bannerSizeSelector_4(Dropdown_t2274391225 * value)
	{
		___bannerSizeSelector_4 = value;
		Il2CppCodeGenWriteBarrier((&___bannerSizeSelector_4), value);
	}

	inline static int32_t get_offset_of_allBannerSizes_5() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___allBannerSizes_5)); }
	inline List_1_t984944135 * get_allBannerSizes_5() const { return ___allBannerSizes_5; }
	inline List_1_t984944135 ** get_address_of_allBannerSizes_5() { return &___allBannerSizes_5; }
	inline void set_allBannerSizes_5(List_1_t984944135 * value)
	{
		___allBannerSizes_5 = value;
		Il2CppCodeGenWriteBarrier((&___allBannerSizes_5), value);
	}

	inline static int32_t get_offset_of_allBannerPositions_6() { return static_cast<int32_t>(offsetof(DefaulBannerUI_t2752449824, ___allBannerPositions_6)); }
	inline List_1_t1658913812 * get_allBannerPositions_6() const { return ___allBannerPositions_6; }
	inline List_1_t1658913812 ** get_address_of_allBannerPositions_6() { return &___allBannerPositions_6; }
	inline void set_allBannerPositions_6(List_1_t1658913812 * value)
	{
		___allBannerPositions_6 = value;
		Il2CppCodeGenWriteBarrier((&___allBannerPositions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULBANNERUI_T2752449824_H
#ifndef U3CWRITESAVEDGAMEU3EC__ANONSTOREY0_T1973406844_H
#define U3CWRITESAVEDGAMEU3EC__ANONSTOREY0_T1973406844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.GameServicesDemo_SavedGames/<WriteSavedGame>c__AnonStorey0
struct  U3CWriteSavedGameU3Ec__AnonStorey0_t1973406844  : public RuntimeObject
{
public:
	// System.Byte[] EasyMobile.Demo.GameServicesDemo_SavedGames/<WriteSavedGame>c__AnonStorey0::data
	ByteU5BU5D_t4116647657* ___data_0;
	// EasyMobile.Demo.GameServicesDemo_SavedGames EasyMobile.Demo.GameServicesDemo_SavedGames/<WriteSavedGame>c__AnonStorey0::$this
	GameServicesDemo_SavedGames_t941468990 * ___U24this_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CWriteSavedGameU3Ec__AnonStorey0_t1973406844, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWriteSavedGameU3Ec__AnonStorey0_t1973406844, ___U24this_1)); }
	inline GameServicesDemo_SavedGames_t941468990 * get_U24this_1() const { return ___U24this_1; }
	inline GameServicesDemo_SavedGames_t941468990 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameServicesDemo_SavedGames_t941468990 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITESAVEDGAMEU3EC__ANONSTOREY0_T1973406844_H
#ifndef DEMOSAVEDGAMEDATA_T64080094_H
#define DEMOSAVEDGAMEDATA_T64080094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.GameServicesDemo_SavedGames/DemoSavedGameData
struct  DemoSavedGameData_t64080094  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.Demo.GameServicesDemo_SavedGames/DemoSavedGameData::demoInt
	int32_t ___demoInt_0;
	// System.Byte[] EasyMobile.Demo.GameServicesDemo_SavedGames/DemoSavedGameData::largeData
	ByteU5BU5D_t4116647657* ___largeData_1;

public:
	inline static int32_t get_offset_of_demoInt_0() { return static_cast<int32_t>(offsetof(DemoSavedGameData_t64080094, ___demoInt_0)); }
	inline int32_t get_demoInt_0() const { return ___demoInt_0; }
	inline int32_t* get_address_of_demoInt_0() { return &___demoInt_0; }
	inline void set_demoInt_0(int32_t value)
	{
		___demoInt_0 = value;
	}

	inline static int32_t get_offset_of_largeData_1() { return static_cast<int32_t>(offsetof(DemoSavedGameData_t64080094, ___largeData_1)); }
	inline ByteU5BU5D_t4116647657* get_largeData_1() const { return ___largeData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_largeData_1() { return &___largeData_1; }
	inline void set_largeData_1(ByteU5BU5D_t4116647657* value)
	{
		___largeData_1 = value;
		Il2CppCodeGenWriteBarrier((&___largeData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSAVEDGAMEDATA_T64080094_H
#ifndef U3CCRONRESTORECOMPLETEDU3EC__ITERATOR0_T2617974367_H
#define U3CCRONRESTORECOMPLETEDU3EC__ITERATOR0_T2617974367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.InAppPurchasingDemo/<CROnRestoreCompleted>c__Iterator0
struct  U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367  : public RuntimeObject
{
public:
	// System.Object EasyMobile.Demo.InAppPurchasingDemo/<CROnRestoreCompleted>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean EasyMobile.Demo.InAppPurchasingDemo/<CROnRestoreCompleted>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 EasyMobile.Demo.InAppPurchasingDemo/<CROnRestoreCompleted>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRONRESTORECOMPLETEDU3EC__ITERATOR0_T2617974367_H
#ifndef U3CCHECKOWNEDPRODUCTSU3EC__ITERATOR1_T1601353020_H
#define U3CCHECKOWNEDPRODUCTSU3EC__ITERATOR1_T1601353020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.InAppPurchasingDemo/<CheckOwnedProducts>c__Iterator1
struct  U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020  : public RuntimeObject
{
public:
	// EasyMobile.IAPProduct[] EasyMobile.Demo.InAppPurchasingDemo/<CheckOwnedProducts>c__Iterator1::<products>__0
	IAPProductU5BU5D_t2701488206* ___U3CproductsU3E__0_0;
	// EasyMobile.Demo.InAppPurchasingDemo EasyMobile.Demo.InAppPurchasingDemo/<CheckOwnedProducts>c__Iterator1::$this
	InAppPurchasingDemo_t2157663715 * ___U24this_1;
	// System.Object EasyMobile.Demo.InAppPurchasingDemo/<CheckOwnedProducts>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EasyMobile.Demo.InAppPurchasingDemo/<CheckOwnedProducts>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 EasyMobile.Demo.InAppPurchasingDemo/<CheckOwnedProducts>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CproductsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020, ___U3CproductsU3E__0_0)); }
	inline IAPProductU5BU5D_t2701488206* get_U3CproductsU3E__0_0() const { return ___U3CproductsU3E__0_0; }
	inline IAPProductU5BU5D_t2701488206** get_address_of_U3CproductsU3E__0_0() { return &___U3CproductsU3E__0_0; }
	inline void set_U3CproductsU3E__0_0(IAPProductU5BU5D_t2701488206* value)
	{
		___U3CproductsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020, ___U24this_1)); }
	inline InAppPurchasingDemo_t2157663715 * get_U24this_1() const { return ___U24this_1; }
	inline InAppPurchasingDemo_t2157663715 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(InAppPurchasingDemo_t2157663715 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKOWNEDPRODUCTSU3EC__ITERATOR1_T1601353020_H
#ifndef DEFAULTELEMENT_T1204156676_H
#define DEFAULTELEMENT_T1204156676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.LoadAndShowSection`2/DefaultElement<EasyMobile.Demo.InterstitialSection/DefaultInterstitialUI,EasyMobile.Demo.InterstitialSection/CustomInterstitialUI>
struct  DefaultElement_t1204156676  : public RuntimeObject
{
public:
	// UnityEngine.UI.Button EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::loadAdButton
	Button_t4055032469 * ___loadAdButton_0;
	// UnityEngine.UI.Button EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::showAdButton
	Button_t4055032469 * ___showAdButton_1;
	// UnityEngine.GameObject EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::isAdLoadedToggle
	GameObject_t1113636619 * ___isAdLoadedToggle_2;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::<DemoUtils>k__BackingField
	DemoUtils_t1360065201 * ___U3CDemoUtilsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_loadAdButton_0() { return static_cast<int32_t>(offsetof(DefaultElement_t1204156676, ___loadAdButton_0)); }
	inline Button_t4055032469 * get_loadAdButton_0() const { return ___loadAdButton_0; }
	inline Button_t4055032469 ** get_address_of_loadAdButton_0() { return &___loadAdButton_0; }
	inline void set_loadAdButton_0(Button_t4055032469 * value)
	{
		___loadAdButton_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadAdButton_0), value);
	}

	inline static int32_t get_offset_of_showAdButton_1() { return static_cast<int32_t>(offsetof(DefaultElement_t1204156676, ___showAdButton_1)); }
	inline Button_t4055032469 * get_showAdButton_1() const { return ___showAdButton_1; }
	inline Button_t4055032469 ** get_address_of_showAdButton_1() { return &___showAdButton_1; }
	inline void set_showAdButton_1(Button_t4055032469 * value)
	{
		___showAdButton_1 = value;
		Il2CppCodeGenWriteBarrier((&___showAdButton_1), value);
	}

	inline static int32_t get_offset_of_isAdLoadedToggle_2() { return static_cast<int32_t>(offsetof(DefaultElement_t1204156676, ___isAdLoadedToggle_2)); }
	inline GameObject_t1113636619 * get_isAdLoadedToggle_2() const { return ___isAdLoadedToggle_2; }
	inline GameObject_t1113636619 ** get_address_of_isAdLoadedToggle_2() { return &___isAdLoadedToggle_2; }
	inline void set_isAdLoadedToggle_2(GameObject_t1113636619 * value)
	{
		___isAdLoadedToggle_2 = value;
		Il2CppCodeGenWriteBarrier((&___isAdLoadedToggle_2), value);
	}

	inline static int32_t get_offset_of_U3CDemoUtilsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DefaultElement_t1204156676, ___U3CDemoUtilsU3Ek__BackingField_3)); }
	inline DemoUtils_t1360065201 * get_U3CDemoUtilsU3Ek__BackingField_3() const { return ___U3CDemoUtilsU3Ek__BackingField_3; }
	inline DemoUtils_t1360065201 ** get_address_of_U3CDemoUtilsU3Ek__BackingField_3() { return &___U3CDemoUtilsU3Ek__BackingField_3; }
	inline void set_U3CDemoUtilsU3Ek__BackingField_3(DemoUtils_t1360065201 * value)
	{
		___U3CDemoUtilsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDemoUtilsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTELEMENT_T1204156676_H
#ifndef DEFAULTELEMENT_T384712910_H
#define DEFAULTELEMENT_T384712910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.LoadAndShowSection`2/DefaultElement<EasyMobile.Demo.RewardedVideoSection/DefaultRewardedVideolUI,EasyMobile.Demo.RewardedVideoSection/CustomRewardedVideoUI>
struct  DefaultElement_t384712910  : public RuntimeObject
{
public:
	// UnityEngine.UI.Button EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::loadAdButton
	Button_t4055032469 * ___loadAdButton_0;
	// UnityEngine.UI.Button EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::showAdButton
	Button_t4055032469 * ___showAdButton_1;
	// UnityEngine.GameObject EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::isAdLoadedToggle
	GameObject_t1113636619 * ___isAdLoadedToggle_2;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.LoadAndShowSection`2/DefaultElement::<DemoUtils>k__BackingField
	DemoUtils_t1360065201 * ___U3CDemoUtilsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_loadAdButton_0() { return static_cast<int32_t>(offsetof(DefaultElement_t384712910, ___loadAdButton_0)); }
	inline Button_t4055032469 * get_loadAdButton_0() const { return ___loadAdButton_0; }
	inline Button_t4055032469 ** get_address_of_loadAdButton_0() { return &___loadAdButton_0; }
	inline void set_loadAdButton_0(Button_t4055032469 * value)
	{
		___loadAdButton_0 = value;
		Il2CppCodeGenWriteBarrier((&___loadAdButton_0), value);
	}

	inline static int32_t get_offset_of_showAdButton_1() { return static_cast<int32_t>(offsetof(DefaultElement_t384712910, ___showAdButton_1)); }
	inline Button_t4055032469 * get_showAdButton_1() const { return ___showAdButton_1; }
	inline Button_t4055032469 ** get_address_of_showAdButton_1() { return &___showAdButton_1; }
	inline void set_showAdButton_1(Button_t4055032469 * value)
	{
		___showAdButton_1 = value;
		Il2CppCodeGenWriteBarrier((&___showAdButton_1), value);
	}

	inline static int32_t get_offset_of_isAdLoadedToggle_2() { return static_cast<int32_t>(offsetof(DefaultElement_t384712910, ___isAdLoadedToggle_2)); }
	inline GameObject_t1113636619 * get_isAdLoadedToggle_2() const { return ___isAdLoadedToggle_2; }
	inline GameObject_t1113636619 ** get_address_of_isAdLoadedToggle_2() { return &___isAdLoadedToggle_2; }
	inline void set_isAdLoadedToggle_2(GameObject_t1113636619 * value)
	{
		___isAdLoadedToggle_2 = value;
		Il2CppCodeGenWriteBarrier((&___isAdLoadedToggle_2), value);
	}

	inline static int32_t get_offset_of_U3CDemoUtilsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DefaultElement_t384712910, ___U3CDemoUtilsU3Ek__BackingField_3)); }
	inline DemoUtils_t1360065201 * get_U3CDemoUtilsU3Ek__BackingField_3() const { return ___U3CDemoUtilsU3Ek__BackingField_3; }
	inline DemoUtils_t1360065201 ** get_address_of_U3CDemoUtilsU3Ek__BackingField_3() { return &___U3CDemoUtilsU3Ek__BackingField_3; }
	inline void set_U3CDemoUtilsU3Ek__BackingField_3(DemoUtils_t1360065201 * value)
	{
		___U3CDemoUtilsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDemoUtilsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTELEMENT_T384712910_H
#ifndef U3CCRWAITANDSHOWPOPUPU3EC__ITERATOR0_T2222973431_H
#define U3CCRWAITANDSHOWPOPUPU3EC__ITERATOR0_T2222973431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0
struct  U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::hasNewUpdate
	bool ___hasNewUpdate_0;
	// System.String EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::title
	String_t* ___title_1;
	// System.String EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::message
	String_t* ___message_2;
	// System.Object EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_hasNewUpdate_0() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431, ___hasNewUpdate_0)); }
	inline bool get_hasNewUpdate_0() const { return ___hasNewUpdate_0; }
	inline bool* get_address_of_hasNewUpdate_0() { return &___hasNewUpdate_0; }
	inline void set_hasNewUpdate_0(bool value)
	{
		___hasNewUpdate_0 = value;
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

struct U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431_StaticFields
{
public:
	// System.Action`1<System.Int32> EasyMobile.Demo.NotificationHandler/<CRWaitAndShowPopup>c__Iterator0::<>f__am$cache0
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRWAITANDSHOWPOPUPU3EC__ITERATOR0_T2222973431_H
#ifndef U3CFETCHUNITYANALYTICSOPTOUTURLU3EC__ANONSTOREY0_T1362407561_H
#define U3CFETCHUNITYANALYTICSOPTOUTURLU3EC__ANONSTOREY0_T1362407561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.PrivacyDemo/<FetchUnityAnalyticsOptOutURL>c__AnonStorey0
struct  U3CFetchUnityAnalyticsOptOutURLU3Ec__AnonStorey0_t1362407561  : public RuntimeObject
{
public:
	// System.Action`1<System.String> EasyMobile.Demo.PrivacyDemo/<FetchUnityAnalyticsOptOutURL>c__AnonStorey0::success
	Action_1_t2019918284 * ___success_0;
	// System.Action`1<System.String> EasyMobile.Demo.PrivacyDemo/<FetchUnityAnalyticsOptOutURL>c__AnonStorey0::failure
	Action_1_t2019918284 * ___failure_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CFetchUnityAnalyticsOptOutURLU3Ec__AnonStorey0_t1362407561, ___success_0)); }
	inline Action_1_t2019918284 * get_success_0() const { return ___success_0; }
	inline Action_1_t2019918284 ** get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(Action_1_t2019918284 * value)
	{
		___success_0 = value;
		Il2CppCodeGenWriteBarrier((&___success_0), value);
	}

	inline static int32_t get_offset_of_failure_1() { return static_cast<int32_t>(offsetof(U3CFetchUnityAnalyticsOptOutURLU3Ec__AnonStorey0_t1362407561, ___failure_1)); }
	inline Action_1_t2019918284 * get_failure_1() const { return ___failure_1; }
	inline Action_1_t2019918284 ** get_address_of_failure_1() { return &___failure_1; }
	inline void set_failure_1(Action_1_t2019918284 * value)
	{
		___failure_1 = value;
		Il2CppCodeGenWriteBarrier((&___failure_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHUNITYANALYTICSOPTOUTURLU3EC__ANONSTOREY0_T1362407561_H
#ifndef U3CADDITEMU3EC__ANONSTOREY0_T3352757045_H
#define U3CADDITEMU3EC__ANONSTOREY0_T3352757045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.ScrollableList/<AddItem>c__AnonStorey0
struct  U3CAddItemU3Ec__AnonStorey0_t3352757045  : public RuntimeObject
{
public:
	// System.String EasyMobile.Demo.ScrollableList/<AddItem>c__AnonStorey0::title
	String_t* ___title_0;
	// System.String EasyMobile.Demo.ScrollableList/<AddItem>c__AnonStorey0::subtitle
	String_t* ___subtitle_1;
	// EasyMobile.Demo.ScrollableList EasyMobile.Demo.ScrollableList/<AddItem>c__AnonStorey0::$this
	ScrollableList_t4043704206 * ___U24this_2;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(U3CAddItemU3Ec__AnonStorey0_t3352757045, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_subtitle_1() { return static_cast<int32_t>(offsetof(U3CAddItemU3Ec__AnonStorey0_t3352757045, ___subtitle_1)); }
	inline String_t* get_subtitle_1() const { return ___subtitle_1; }
	inline String_t** get_address_of_subtitle_1() { return &___subtitle_1; }
	inline void set_subtitle_1(String_t* value)
	{
		___subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___subtitle_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAddItemU3Ec__AnonStorey0_t3352757045, ___U24this_2)); }
	inline ScrollableList_t4043704206 * get_U24this_2() const { return ___U24this_2; }
	inline ScrollableList_t4043704206 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ScrollableList_t4043704206 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDITEMU3EC__ANONSTOREY0_T3352757045_H
#ifndef U3CCRONESTEPSHARINGU3EC__ITERATOR1_T921484503_H
#define U3CCRONESTEPSHARINGU3EC__ITERATOR1_T921484503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.SharingDemo/<CROneStepSharing>c__Iterator1
struct  U3CCROneStepSharingU3Ec__Iterator1_t921484503  : public RuntimeObject
{
public:
	// EasyMobile.Demo.SharingDemo EasyMobile.Demo.SharingDemo/<CROneStepSharing>c__Iterator1::$this
	SharingDemo_t2942947019 * ___U24this_0;
	// System.Object EasyMobile.Demo.SharingDemo/<CROneStepSharing>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean EasyMobile.Demo.SharingDemo/<CROneStepSharing>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 EasyMobile.Demo.SharingDemo/<CROneStepSharing>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCROneStepSharingU3Ec__Iterator1_t921484503, ___U24this_0)); }
	inline SharingDemo_t2942947019 * get_U24this_0() const { return ___U24this_0; }
	inline SharingDemo_t2942947019 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SharingDemo_t2942947019 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCROneStepSharingU3Ec__Iterator1_t921484503, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCROneStepSharingU3Ec__Iterator1_t921484503, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCROneStepSharingU3Ec__Iterator1_t921484503, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRONESTEPSHARINGU3EC__ITERATOR1_T921484503_H
#ifndef U3CCRSAVESCREENSHOTU3EC__ITERATOR0_T1145865530_H
#define U3CCRSAVESCREENSHOTU3EC__ITERATOR0_T1145865530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.SharingDemo/<CRSaveScreenshot>c__Iterator0
struct  U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530  : public RuntimeObject
{
public:
	// EasyMobile.Demo.SharingDemo EasyMobile.Demo.SharingDemo/<CRSaveScreenshot>c__Iterator0::$this
	SharingDemo_t2942947019 * ___U24this_0;
	// System.Object EasyMobile.Demo.SharingDemo/<CRSaveScreenshot>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean EasyMobile.Demo.SharingDemo/<CRSaveScreenshot>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 EasyMobile.Demo.SharingDemo/<CRSaveScreenshot>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530, ___U24this_0)); }
	inline SharingDemo_t2942947019 * get_U24this_0() const { return ___U24this_0; }
	inline SharingDemo_t2942947019 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SharingDemo_t2942947019 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRSAVESCREENSHOTU3EC__ITERATOR0_T1145865530_H
#ifndef EM_GAMESERVICESCONSTANTS_T963641404_H
#define EM_GAMESERVICESCONSTANTS_T963641404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EM_GameServicesConstants
struct  EM_GameServicesConstants_t963641404  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EM_GAMESERVICESCONSTANTS_T963641404_H
#ifndef EM_IAPCONSTANTS_T4099475916_H
#define EM_IAPCONSTANTS_T4099475916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EM_IAPConstants
struct  EM_IAPConstants_t4099475916  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EM_IAPCONSTANTS_T4099475916_H
#ifndef COLOREXT_T1658015523_H
#define COLOREXT_T1658015523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.ColorExt
struct  ColorExt_t1658015523  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOREXT_T1658015523_H
#ifndef DATETIMEEXT_T1870725150_H
#define DATETIMEEXT_T1870725150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.DateTimeExt
struct  DateTimeExt_t1870725150  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEEXT_T1870725150_H
#ifndef DUMMYAPPLIFECYCLEHANDLER_T4108139044_H
#define DUMMYAPPLIFECYCLEHANDLER_T4108139044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.DummyAppLifecycleHandler
struct  DummyAppLifecycleHandler_t4108139044  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYAPPLIFECYCLEHANDLER_T4108139044_H
#ifndef U3CAWAKEU3EC__ANONSTOREY0_T2364586269_H
#define U3CAWAKEU3EC__ANONSTOREY0_T2364586269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/<Awake>c__AnonStorey0
struct  U3CAwakeU3Ec__AnonStorey0_t2364586269  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ConfigurationBuilder IAPDemo/<Awake>c__AnonStorey0::builder
	ConfigurationBuilder_t1618671084 * ___builder_0;
	// System.Action IAPDemo/<Awake>c__AnonStorey0::initializeUnityIap
	Action_t1264377477 * ___initializeUnityIap_1;
	// IAPDemo IAPDemo/<Awake>c__AnonStorey0::$this
	IAPDemo_t3681080565 * ___U24this_2;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2364586269, ___builder_0)); }
	inline ConfigurationBuilder_t1618671084 * get_builder_0() const { return ___builder_0; }
	inline ConfigurationBuilder_t1618671084 ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(ConfigurationBuilder_t1618671084 * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}

	inline static int32_t get_offset_of_initializeUnityIap_1() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2364586269, ___initializeUnityIap_1)); }
	inline Action_t1264377477 * get_initializeUnityIap_1() const { return ___initializeUnityIap_1; }
	inline Action_t1264377477 ** get_address_of_initializeUnityIap_1() { return &___initializeUnityIap_1; }
	inline void set_initializeUnityIap_1(Action_t1264377477 * value)
	{
		___initializeUnityIap_1 = value;
		Il2CppCodeGenWriteBarrier((&___initializeUnityIap_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2364586269, ___U24this_2)); }
	inline IAPDemo_t3681080565 * get_U24this_2() const { return ___U24this_2; }
	inline IAPDemo_t3681080565 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(IAPDemo_t3681080565 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAWAKEU3EC__ANONSTOREY0_T2364586269_H
#ifndef U3CVALIDATEBUTTONCLICKU3EC__ANONSTOREY1_T541528072_H
#define U3CVALIDATEBUTTONCLICKU3EC__ANONSTOREY1_T541528072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/<ValidateButtonClick>c__AnonStorey1
struct  U3CValidateButtonClickU3Ec__AnonStorey1_t541528072  : public RuntimeObject
{
public:
	// System.String IAPDemo/<ValidateButtonClick>c__AnonStorey1::txId
	String_t* ___txId_0;

public:
	inline static int32_t get_offset_of_txId_0() { return static_cast<int32_t>(offsetof(U3CValidateButtonClickU3Ec__AnonStorey1_t541528072, ___txId_0)); }
	inline String_t* get_txId_0() const { return ___txId_0; }
	inline String_t** get_address_of_txId_0() { return &___txId_0; }
	inline void set_txId_0(String_t* value)
	{
		___txId_0 = value;
		Il2CppCodeGenWriteBarrier((&___txId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATEBUTTONCLICKU3EC__ANONSTOREY1_T541528072_H
#ifndef UNITYCHANNELLOGINHANDLER_T2949829254_H
#define UNITYCHANNELLOGINHANDLER_T2949829254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelLoginHandler
struct  UnityChannelLoginHandler_t2949829254  : public RuntimeObject
{
public:
	// System.Action IAPDemo/UnityChannelLoginHandler::initializeSucceededAction
	Action_t1264377477 * ___initializeSucceededAction_0;
	// System.Action`1<System.String> IAPDemo/UnityChannelLoginHandler::initializeFailedAction
	Action_1_t2019918284 * ___initializeFailedAction_1;
	// System.Action`1<UnityEngine.Store.UserInfo> IAPDemo/UnityChannelLoginHandler::loginSucceededAction
	Action_1_t3058893588 * ___loginSucceededAction_2;
	// System.Action`1<System.String> IAPDemo/UnityChannelLoginHandler::loginFailedAction
	Action_1_t2019918284 * ___loginFailedAction_3;

public:
	inline static int32_t get_offset_of_initializeSucceededAction_0() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___initializeSucceededAction_0)); }
	inline Action_t1264377477 * get_initializeSucceededAction_0() const { return ___initializeSucceededAction_0; }
	inline Action_t1264377477 ** get_address_of_initializeSucceededAction_0() { return &___initializeSucceededAction_0; }
	inline void set_initializeSucceededAction_0(Action_t1264377477 * value)
	{
		___initializeSucceededAction_0 = value;
		Il2CppCodeGenWriteBarrier((&___initializeSucceededAction_0), value);
	}

	inline static int32_t get_offset_of_initializeFailedAction_1() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___initializeFailedAction_1)); }
	inline Action_1_t2019918284 * get_initializeFailedAction_1() const { return ___initializeFailedAction_1; }
	inline Action_1_t2019918284 ** get_address_of_initializeFailedAction_1() { return &___initializeFailedAction_1; }
	inline void set_initializeFailedAction_1(Action_1_t2019918284 * value)
	{
		___initializeFailedAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___initializeFailedAction_1), value);
	}

	inline static int32_t get_offset_of_loginSucceededAction_2() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___loginSucceededAction_2)); }
	inline Action_1_t3058893588 * get_loginSucceededAction_2() const { return ___loginSucceededAction_2; }
	inline Action_1_t3058893588 ** get_address_of_loginSucceededAction_2() { return &___loginSucceededAction_2; }
	inline void set_loginSucceededAction_2(Action_1_t3058893588 * value)
	{
		___loginSucceededAction_2 = value;
		Il2CppCodeGenWriteBarrier((&___loginSucceededAction_2), value);
	}

	inline static int32_t get_offset_of_loginFailedAction_3() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2949829254, ___loginFailedAction_3)); }
	inline Action_1_t2019918284 * get_loginFailedAction_3() const { return ___loginFailedAction_3; }
	inline Action_1_t2019918284 ** get_address_of_loginFailedAction_3() { return &___loginFailedAction_3; }
	inline void set_loginFailedAction_3(Action_1_t2019918284 * value)
	{
		___loginFailedAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___loginFailedAction_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELLOGINHANDLER_T2949829254_H
#ifndef UNITYCHANNELPURCHASEERROR_T2306817818_H
#define UNITYCHANNELPURCHASEERROR_T2306817818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelPurchaseError
struct  UnityChannelPurchaseError_t2306817818  : public RuntimeObject
{
public:
	// System.String IAPDemo/UnityChannelPurchaseError::error
	String_t* ___error_0;
	// IAPDemo/UnityChannelPurchaseInfo IAPDemo/UnityChannelPurchaseError::purchaseInfo
	UnityChannelPurchaseInfo_t74063925 * ___purchaseInfo_1;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_t2306817818, ___error_0)); }
	inline String_t* get_error_0() const { return ___error_0; }
	inline String_t** get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(String_t* value)
	{
		___error_0 = value;
		Il2CppCodeGenWriteBarrier((&___error_0), value);
	}

	inline static int32_t get_offset_of_purchaseInfo_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_t2306817818, ___purchaseInfo_1)); }
	inline UnityChannelPurchaseInfo_t74063925 * get_purchaseInfo_1() const { return ___purchaseInfo_1; }
	inline UnityChannelPurchaseInfo_t74063925 ** get_address_of_purchaseInfo_1() { return &___purchaseInfo_1; }
	inline void set_purchaseInfo_1(UnityChannelPurchaseInfo_t74063925 * value)
	{
		___purchaseInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASEERROR_T2306817818_H
#ifndef UNITYCHANNELPURCHASEINFO_T74063925_H
#define UNITYCHANNELPURCHASEINFO_T74063925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelPurchaseInfo
struct  UnityChannelPurchaseInfo_t74063925  : public RuntimeObject
{
public:
	// System.String IAPDemo/UnityChannelPurchaseInfo::productCode
	String_t* ___productCode_0;
	// System.String IAPDemo/UnityChannelPurchaseInfo::gameOrderId
	String_t* ___gameOrderId_1;
	// System.String IAPDemo/UnityChannelPurchaseInfo::orderQueryToken
	String_t* ___orderQueryToken_2;

public:
	inline static int32_t get_offset_of_productCode_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t74063925, ___productCode_0)); }
	inline String_t* get_productCode_0() const { return ___productCode_0; }
	inline String_t** get_address_of_productCode_0() { return &___productCode_0; }
	inline void set_productCode_0(String_t* value)
	{
		___productCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___productCode_0), value);
	}

	inline static int32_t get_offset_of_gameOrderId_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t74063925, ___gameOrderId_1)); }
	inline String_t* get_gameOrderId_1() const { return ___gameOrderId_1; }
	inline String_t** get_address_of_gameOrderId_1() { return &___gameOrderId_1; }
	inline void set_gameOrderId_1(String_t* value)
	{
		___gameOrderId_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameOrderId_1), value);
	}

	inline static int32_t get_offset_of_orderQueryToken_2() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t74063925, ___orderQueryToken_2)); }
	inline String_t* get_orderQueryToken_2() const { return ___orderQueryToken_2; }
	inline String_t** get_address_of_orderQueryToken_2() { return &___orderQueryToken_2; }
	inline void set_orderQueryToken_2(String_t* value)
	{
		___orderQueryToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___orderQueryToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASEINFO_T74063925_H
#ifndef MINIMALBASEFORMATTER_1_T480015625_H
#define MINIMALBASEFORMATTER_1_T480015625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector2Int>
struct  MinimalBaseFormatter_1_t480015625  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t480015625_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t480015625_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T480015625_H
#ifndef MINIMALBASEFORMATTER_1_T2046099566_H
#define MINIMALBASEFORMATTER_1_T2046099566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.MinimalBaseFormatter`1<UnityEngine.Vector3Int>
struct  MinimalBaseFormatter_1_t2046099566  : public RuntimeObject
{
public:

public:
};

struct MinimalBaseFormatter_1_t2046099566_StaticFields
{
public:
	// System.Boolean Sirenix.Serialization.MinimalBaseFormatter`1::IsValueType
	bool ___IsValueType_0;

public:
	inline static int32_t get_offset_of_IsValueType_0() { return static_cast<int32_t>(offsetof(MinimalBaseFormatter_1_t2046099566_StaticFields, ___IsValueType_0)); }
	inline bool get_IsValueType_0() const { return ___IsValueType_0; }
	inline bool* get_address_of_IsValueType_0() { return &___IsValueType_0; }
	inline void set_IsValueType_0(bool value)
	{
		___IsValueType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMALBASEFORMATTER_1_T2046099566_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef CODELESSIAPSTORELISTENER_T3553337218_H
#define CODELESSIAPSTORELISTENER_T3553337218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CodelessIAPStoreListener
struct  CodelessIAPStoreListener_t3553337218  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton> UnityEngine.Purchasing.CodelessIAPStoreListener::activeButtons
	List_1_t3820967359 * ___activeButtons_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPListener> UnityEngine.Purchasing.CodelessIAPStoreListener::activeListeners
	List_1_t3473867730 * ___activeListeners_2;
	// UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.CodelessIAPStoreListener::controller
	RuntimeObject* ___controller_4;
	// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.CodelessIAPStoreListener::extensions
	RuntimeObject* ___extensions_5;
	// UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.CodelessIAPStoreListener::catalog
	ProductCatalog_t3178009003 * ___catalog_6;

public:
	inline static int32_t get_offset_of_activeButtons_1() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218, ___activeButtons_1)); }
	inline List_1_t3820967359 * get_activeButtons_1() const { return ___activeButtons_1; }
	inline List_1_t3820967359 ** get_address_of_activeButtons_1() { return &___activeButtons_1; }
	inline void set_activeButtons_1(List_1_t3820967359 * value)
	{
		___activeButtons_1 = value;
		Il2CppCodeGenWriteBarrier((&___activeButtons_1), value);
	}

	inline static int32_t get_offset_of_activeListeners_2() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218, ___activeListeners_2)); }
	inline List_1_t3473867730 * get_activeListeners_2() const { return ___activeListeners_2; }
	inline List_1_t3473867730 ** get_address_of_activeListeners_2() { return &___activeListeners_2; }
	inline void set_activeListeners_2(List_1_t3473867730 * value)
	{
		___activeListeners_2 = value;
		Il2CppCodeGenWriteBarrier((&___activeListeners_2), value);
	}

	inline static int32_t get_offset_of_controller_4() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218, ___controller_4)); }
	inline RuntimeObject* get_controller_4() const { return ___controller_4; }
	inline RuntimeObject** get_address_of_controller_4() { return &___controller_4; }
	inline void set_controller_4(RuntimeObject* value)
	{
		___controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___controller_4), value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218, ___extensions_5)); }
	inline RuntimeObject* get_extensions_5() const { return ___extensions_5; }
	inline RuntimeObject** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(RuntimeObject* value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_5), value);
	}

	inline static int32_t get_offset_of_catalog_6() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218, ___catalog_6)); }
	inline ProductCatalog_t3178009003 * get_catalog_6() const { return ___catalog_6; }
	inline ProductCatalog_t3178009003 ** get_address_of_catalog_6() { return &___catalog_6; }
	inline void set_catalog_6(ProductCatalog_t3178009003 * value)
	{
		___catalog_6 = value;
		Il2CppCodeGenWriteBarrier((&___catalog_6), value);
	}
};

struct CodelessIAPStoreListener_t3553337218_StaticFields
{
public:
	// UnityEngine.Purchasing.CodelessIAPStoreListener UnityEngine.Purchasing.CodelessIAPStoreListener::instance
	CodelessIAPStoreListener_t3553337218 * ___instance_0;
	// System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::unityPurchasingInitialized
	bool ___unityPurchasingInitialized_3;
	// System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::initializationComplete
	bool ___initializationComplete_7;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218_StaticFields, ___instance_0)); }
	inline CodelessIAPStoreListener_t3553337218 * get_instance_0() const { return ___instance_0; }
	inline CodelessIAPStoreListener_t3553337218 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(CodelessIAPStoreListener_t3553337218 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}

	inline static int32_t get_offset_of_unityPurchasingInitialized_3() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218_StaticFields, ___unityPurchasingInitialized_3)); }
	inline bool get_unityPurchasingInitialized_3() const { return ___unityPurchasingInitialized_3; }
	inline bool* get_address_of_unityPurchasingInitialized_3() { return &___unityPurchasingInitialized_3; }
	inline void set_unityPurchasingInitialized_3(bool value)
	{
		___unityPurchasingInitialized_3 = value;
	}

	inline static int32_t get_offset_of_initializationComplete_7() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t3553337218_StaticFields, ___initializationComplete_7)); }
	inline bool get_initializationComplete_7() const { return ___initializationComplete_7; }
	inline bool* get_address_of_initializationComplete_7() { return &___initializationComplete_7; }
	inline void set_initializationComplete_7(bool value)
	{
		___initializationComplete_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODELESSIAPSTORELISTENER_T3553337218_H
#ifndef IAPCONFIGURATIONHELPER_T2483224394_H
#define IAPCONFIGURATIONHELPER_T2483224394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPConfigurationHelper
struct  IAPConfigurationHelper_t2483224394  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPCONFIGURATIONHELPER_T2483224394_H
#ifndef APPLETANGLE_T2948648219_H
#define APPLETANGLE_T2948648219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleTangle
struct  AppleTangle_t2948648219  : public RuntimeObject
{
public:

public:
};

struct AppleTangle_t2948648219_StaticFields
{
public:
	// System.Byte[] UnityEngine.Purchasing.Security.AppleTangle::data
	ByteU5BU5D_t4116647657* ___data_0;
	// System.Int32[] UnityEngine.Purchasing.Security.AppleTangle::order
	Int32U5BU5D_t385246372* ___order_1;
	// System.Int32 UnityEngine.Purchasing.Security.AppleTangle::key
	int32_t ___key_2;
	// System.Boolean UnityEngine.Purchasing.Security.AppleTangle::IsPopulated
	bool ___IsPopulated_3;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(AppleTangle_t2948648219_StaticFields, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(AppleTangle_t2948648219_StaticFields, ___order_1)); }
	inline Int32U5BU5D_t385246372* get_order_1() const { return ___order_1; }
	inline Int32U5BU5D_t385246372** get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(Int32U5BU5D_t385246372* value)
	{
		___order_1 = value;
		Il2CppCodeGenWriteBarrier((&___order_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(AppleTangle_t2948648219_StaticFields, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_IsPopulated_3() { return static_cast<int32_t>(offsetof(AppleTangle_t2948648219_StaticFields, ___IsPopulated_3)); }
	inline bool get_IsPopulated_3() const { return ___IsPopulated_3; }
	inline bool* get_address_of_IsPopulated_3() { return &___IsPopulated_3; }
	inline void set_IsPopulated_3(bool value)
	{
		___IsPopulated_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLETANGLE_T2948648219_H
#ifndef GOOGLEPLAYTANGLE_T2803690650_H
#define GOOGLEPLAYTANGLE_T2803690650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GooglePlayTangle
struct  GooglePlayTangle_t2803690650  : public RuntimeObject
{
public:

public:
};

struct GooglePlayTangle_t2803690650_StaticFields
{
public:
	// System.Byte[] UnityEngine.Purchasing.Security.GooglePlayTangle::data
	ByteU5BU5D_t4116647657* ___data_0;
	// System.Int32[] UnityEngine.Purchasing.Security.GooglePlayTangle::order
	Int32U5BU5D_t385246372* ___order_1;
	// System.Int32 UnityEngine.Purchasing.Security.GooglePlayTangle::key
	int32_t ___key_2;
	// System.Boolean UnityEngine.Purchasing.Security.GooglePlayTangle::IsPopulated
	bool ___IsPopulated_3;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(GooglePlayTangle_t2803690650_StaticFields, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(GooglePlayTangle_t2803690650_StaticFields, ___order_1)); }
	inline Int32U5BU5D_t385246372* get_order_1() const { return ___order_1; }
	inline Int32U5BU5D_t385246372** get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(Int32U5BU5D_t385246372* value)
	{
		___order_1 = value;
		Il2CppCodeGenWriteBarrier((&___order_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(GooglePlayTangle_t2803690650_StaticFields, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_IsPopulated_3() { return static_cast<int32_t>(offsetof(GooglePlayTangle_t2803690650_StaticFields, ___IsPopulated_3)); }
	inline bool get_IsPopulated_3() const { return ___IsPopulated_3; }
	inline bool* get_address_of_IsPopulated_3() { return &___IsPopulated_3; }
	inline void set_IsPopulated_3(bool value)
	{
		___IsPopulated_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYTANGLE_T2803690650_H
#ifndef UNITYCHANNELTANGLE_T4110017680_H
#define UNITYCHANNELTANGLE_T4110017680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.UnityChannelTangle
struct  UnityChannelTangle_t4110017680  : public RuntimeObject
{
public:

public:
};

struct UnityChannelTangle_t4110017680_StaticFields
{
public:
	// System.Byte[] UnityEngine.Purchasing.Security.UnityChannelTangle::data
	ByteU5BU5D_t4116647657* ___data_0;
	// System.Int32[] UnityEngine.Purchasing.Security.UnityChannelTangle::order
	Int32U5BU5D_t385246372* ___order_1;
	// System.Int32 UnityEngine.Purchasing.Security.UnityChannelTangle::key
	int32_t ___key_2;
	// System.Boolean UnityEngine.Purchasing.Security.UnityChannelTangle::IsPopulated
	bool ___IsPopulated_3;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(UnityChannelTangle_t4110017680_StaticFields, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(UnityChannelTangle_t4110017680_StaticFields, ___order_1)); }
	inline Int32U5BU5D_t385246372* get_order_1() const { return ___order_1; }
	inline Int32U5BU5D_t385246372** get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(Int32U5BU5D_t385246372* value)
	{
		___order_1 = value;
		Il2CppCodeGenWriteBarrier((&___order_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(UnityChannelTangle_t4110017680_StaticFields, ___key_2)); }
	inline int32_t get_key_2() const { return ___key_2; }
	inline int32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int32_t value)
	{
		___key_2 = value;
	}

	inline static int32_t get_offset_of_IsPopulated_3() { return static_cast<int32_t>(offsetof(UnityChannelTangle_t4110017680_StaticFields, ___IsPopulated_3)); }
	inline bool get_IsPopulated_3() const { return ___IsPopulated_3; }
	inline bool* get_address_of_IsPopulated_3() { return &___IsPopulated_3; }
	inline void set_IsPopulated_3(bool value)
	{
		___IsPopulated_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELTANGLE_T4110017680_H
#ifndef U24ARRAYTYPEU3D244_T4214066944_H
#define U24ARRAYTYPEU3D244_T4214066944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=244
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D244_t4214066944 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D244_t4214066944__padding[244];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D244_T4214066944_H
#ifndef U24ARRAYTYPEU3D60_T4028431473_H
#define U24ARRAYTYPEU3D60_T4028431473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=60
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D60_t4028431473 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D60_t4028431473__padding[60];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D60_T4028431473_H
#ifndef BANNERSECTION_T2412952700_H
#define BANNERSECTION_T2412952700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.BannerSection
struct  BannerSection_t2412952700  : public AdsSection_t4249879119
{
public:
	// EasyMobile.Demo.BannerSection/DefaulBannerUI EasyMobile.Demo.BannerSection::defaultBannerUI
	DefaulBannerUI_t2752449824 * ___defaultBannerUI_6;
	// EasyMobile.Demo.BannerSection/CustomBannerUI EasyMobile.Demo.BannerSection::customBannerUI
	CustomBannerUI_t529944588 * ___customBannerUI_7;

public:
	inline static int32_t get_offset_of_defaultBannerUI_6() { return static_cast<int32_t>(offsetof(BannerSection_t2412952700, ___defaultBannerUI_6)); }
	inline DefaulBannerUI_t2752449824 * get_defaultBannerUI_6() const { return ___defaultBannerUI_6; }
	inline DefaulBannerUI_t2752449824 ** get_address_of_defaultBannerUI_6() { return &___defaultBannerUI_6; }
	inline void set_defaultBannerUI_6(DefaulBannerUI_t2752449824 * value)
	{
		___defaultBannerUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBannerUI_6), value);
	}

	inline static int32_t get_offset_of_customBannerUI_7() { return static_cast<int32_t>(offsetof(BannerSection_t2412952700, ___customBannerUI_7)); }
	inline CustomBannerUI_t529944588 * get_customBannerUI_7() const { return ___customBannerUI_7; }
	inline CustomBannerUI_t529944588 ** get_address_of_customBannerUI_7() { return &___customBannerUI_7; }
	inline void set_customBannerUI_7(CustomBannerUI_t529944588 * value)
	{
		___customBannerUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___customBannerUI_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERSECTION_T2412952700_H
#ifndef CUSTOMBANNERUI_T529944588_H
#define CUSTOMBANNERUI_T529944588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.BannerSection/CustomBannerUI
struct  CustomBannerUI_t529944588  : public DefaulBannerUI_t2752449824
{
public:
	// UnityEngine.UI.Dropdown EasyMobile.Demo.BannerSection/CustomBannerUI::networkSelector
	Dropdown_t2274391225 * ___networkSelector_7;
	// UnityEngine.UI.InputField EasyMobile.Demo.BannerSection/CustomBannerUI::customKeyInputField
	InputField_t3762917431 * ___customKeyInputField_8;
	// System.Collections.Generic.List`1<EasyMobile.BannerAdNetwork> EasyMobile.Demo.BannerSection/CustomBannerUI::allBannerNetworks
	List_1_t1454037157 * ___allBannerNetworks_9;

public:
	inline static int32_t get_offset_of_networkSelector_7() { return static_cast<int32_t>(offsetof(CustomBannerUI_t529944588, ___networkSelector_7)); }
	inline Dropdown_t2274391225 * get_networkSelector_7() const { return ___networkSelector_7; }
	inline Dropdown_t2274391225 ** get_address_of_networkSelector_7() { return &___networkSelector_7; }
	inline void set_networkSelector_7(Dropdown_t2274391225 * value)
	{
		___networkSelector_7 = value;
		Il2CppCodeGenWriteBarrier((&___networkSelector_7), value);
	}

	inline static int32_t get_offset_of_customKeyInputField_8() { return static_cast<int32_t>(offsetof(CustomBannerUI_t529944588, ___customKeyInputField_8)); }
	inline InputField_t3762917431 * get_customKeyInputField_8() const { return ___customKeyInputField_8; }
	inline InputField_t3762917431 ** get_address_of_customKeyInputField_8() { return &___customKeyInputField_8; }
	inline void set_customKeyInputField_8(InputField_t3762917431 * value)
	{
		___customKeyInputField_8 = value;
		Il2CppCodeGenWriteBarrier((&___customKeyInputField_8), value);
	}

	inline static int32_t get_offset_of_allBannerNetworks_9() { return static_cast<int32_t>(offsetof(CustomBannerUI_t529944588, ___allBannerNetworks_9)); }
	inline List_1_t1454037157 * get_allBannerNetworks_9() const { return ___allBannerNetworks_9; }
	inline List_1_t1454037157 ** get_address_of_allBannerNetworks_9() { return &___allBannerNetworks_9; }
	inline void set_allBannerNetworks_9(List_1_t1454037157 * value)
	{
		___allBannerNetworks_9 = value;
		Il2CppCodeGenWriteBarrier((&___allBannerNetworks_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMBANNERUI_T529944588_H
#ifndef DEFAULTINTERSTITIALUI_T2954553843_H
#define DEFAULTINTERSTITIALUI_T2954553843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.InterstitialSection/DefaultInterstitialUI
struct  DefaultInterstitialUI_t2954553843  : public DefaultElement_t1204156676
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINTERSTITIALUI_T2954553843_H
#ifndef CUSTOMELEMENT_T490039825_H
#define CUSTOMELEMENT_T490039825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.LoadAndShowSection`2/CustomElement<EasyMobile.Demo.InterstitialSection/DefaultInterstitialUI,EasyMobile.Demo.InterstitialSection/CustomInterstitialUI>
struct  CustomElement_t490039825  : public DefaultElement_t1204156676
{
public:
	// UnityEngine.UI.Dropdown EasyMobile.Demo.LoadAndShowSection`2/CustomElement::networkSelector
	Dropdown_t2274391225 * ___networkSelector_4;
	// UnityEngine.GameObject EasyMobile.Demo.LoadAndShowSection`2/CustomElement::inputFieldRoot
	GameObject_t1113636619 * ___inputFieldRoot_5;
	// UnityEngine.UI.InputField EasyMobile.Demo.LoadAndShowSection`2/CustomElement::customKeyInputField
	InputField_t3762917431 * ___customKeyInputField_6;
	// UnityEngine.UI.Toggle EasyMobile.Demo.LoadAndShowSection`2/CustomElement::enableCustomKey
	Toggle_t2735377061 * ___enableCustomKey_7;

public:
	inline static int32_t get_offset_of_networkSelector_4() { return static_cast<int32_t>(offsetof(CustomElement_t490039825, ___networkSelector_4)); }
	inline Dropdown_t2274391225 * get_networkSelector_4() const { return ___networkSelector_4; }
	inline Dropdown_t2274391225 ** get_address_of_networkSelector_4() { return &___networkSelector_4; }
	inline void set_networkSelector_4(Dropdown_t2274391225 * value)
	{
		___networkSelector_4 = value;
		Il2CppCodeGenWriteBarrier((&___networkSelector_4), value);
	}

	inline static int32_t get_offset_of_inputFieldRoot_5() { return static_cast<int32_t>(offsetof(CustomElement_t490039825, ___inputFieldRoot_5)); }
	inline GameObject_t1113636619 * get_inputFieldRoot_5() const { return ___inputFieldRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_inputFieldRoot_5() { return &___inputFieldRoot_5; }
	inline void set_inputFieldRoot_5(GameObject_t1113636619 * value)
	{
		___inputFieldRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldRoot_5), value);
	}

	inline static int32_t get_offset_of_customKeyInputField_6() { return static_cast<int32_t>(offsetof(CustomElement_t490039825, ___customKeyInputField_6)); }
	inline InputField_t3762917431 * get_customKeyInputField_6() const { return ___customKeyInputField_6; }
	inline InputField_t3762917431 ** get_address_of_customKeyInputField_6() { return &___customKeyInputField_6; }
	inline void set_customKeyInputField_6(InputField_t3762917431 * value)
	{
		___customKeyInputField_6 = value;
		Il2CppCodeGenWriteBarrier((&___customKeyInputField_6), value);
	}

	inline static int32_t get_offset_of_enableCustomKey_7() { return static_cast<int32_t>(offsetof(CustomElement_t490039825, ___enableCustomKey_7)); }
	inline Toggle_t2735377061 * get_enableCustomKey_7() const { return ___enableCustomKey_7; }
	inline Toggle_t2735377061 ** get_address_of_enableCustomKey_7() { return &___enableCustomKey_7; }
	inline void set_enableCustomKey_7(Toggle_t2735377061 * value)
	{
		___enableCustomKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___enableCustomKey_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMELEMENT_T490039825_H
#ifndef CUSTOMELEMENT_T3965563355_H
#define CUSTOMELEMENT_T3965563355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.LoadAndShowSection`2/CustomElement<EasyMobile.Demo.RewardedVideoSection/DefaultRewardedVideolUI,EasyMobile.Demo.RewardedVideoSection/CustomRewardedVideoUI>
struct  CustomElement_t3965563355  : public DefaultElement_t384712910
{
public:
	// UnityEngine.UI.Dropdown EasyMobile.Demo.LoadAndShowSection`2/CustomElement::networkSelector
	Dropdown_t2274391225 * ___networkSelector_4;
	// UnityEngine.GameObject EasyMobile.Demo.LoadAndShowSection`2/CustomElement::inputFieldRoot
	GameObject_t1113636619 * ___inputFieldRoot_5;
	// UnityEngine.UI.InputField EasyMobile.Demo.LoadAndShowSection`2/CustomElement::customKeyInputField
	InputField_t3762917431 * ___customKeyInputField_6;
	// UnityEngine.UI.Toggle EasyMobile.Demo.LoadAndShowSection`2/CustomElement::enableCustomKey
	Toggle_t2735377061 * ___enableCustomKey_7;

public:
	inline static int32_t get_offset_of_networkSelector_4() { return static_cast<int32_t>(offsetof(CustomElement_t3965563355, ___networkSelector_4)); }
	inline Dropdown_t2274391225 * get_networkSelector_4() const { return ___networkSelector_4; }
	inline Dropdown_t2274391225 ** get_address_of_networkSelector_4() { return &___networkSelector_4; }
	inline void set_networkSelector_4(Dropdown_t2274391225 * value)
	{
		___networkSelector_4 = value;
		Il2CppCodeGenWriteBarrier((&___networkSelector_4), value);
	}

	inline static int32_t get_offset_of_inputFieldRoot_5() { return static_cast<int32_t>(offsetof(CustomElement_t3965563355, ___inputFieldRoot_5)); }
	inline GameObject_t1113636619 * get_inputFieldRoot_5() const { return ___inputFieldRoot_5; }
	inline GameObject_t1113636619 ** get_address_of_inputFieldRoot_5() { return &___inputFieldRoot_5; }
	inline void set_inputFieldRoot_5(GameObject_t1113636619 * value)
	{
		___inputFieldRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldRoot_5), value);
	}

	inline static int32_t get_offset_of_customKeyInputField_6() { return static_cast<int32_t>(offsetof(CustomElement_t3965563355, ___customKeyInputField_6)); }
	inline InputField_t3762917431 * get_customKeyInputField_6() const { return ___customKeyInputField_6; }
	inline InputField_t3762917431 ** get_address_of_customKeyInputField_6() { return &___customKeyInputField_6; }
	inline void set_customKeyInputField_6(InputField_t3762917431 * value)
	{
		___customKeyInputField_6 = value;
		Il2CppCodeGenWriteBarrier((&___customKeyInputField_6), value);
	}

	inline static int32_t get_offset_of_enableCustomKey_7() { return static_cast<int32_t>(offsetof(CustomElement_t3965563355, ___enableCustomKey_7)); }
	inline Toggle_t2735377061 * get_enableCustomKey_7() const { return ___enableCustomKey_7; }
	inline Toggle_t2735377061 ** get_address_of_enableCustomKey_7() { return &___enableCustomKey_7; }
	inline void set_enableCustomKey_7(Toggle_t2735377061 * value)
	{
		___enableCustomKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___enableCustomKey_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMELEMENT_T3965563355_H
#ifndef LOADANDSHOWSECTION_2_T2421133264_H
#define LOADANDSHOWSECTION_2_T2421133264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.LoadAndShowSection`2<EasyMobile.Demo.InterstitialSection/DefaultInterstitialUI,EasyMobile.Demo.InterstitialSection/CustomInterstitialUI>
struct  LoadAndShowSection_2_t2421133264  : public AdsSection_t4249879119
{
public:
	// T EasyMobile.Demo.LoadAndShowSection`2::defaultElement
	DefaultInterstitialUI_t2954553843 * ___defaultElement_6;
	// U EasyMobile.Demo.LoadAndShowSection`2::customElement
	CustomInterstitialUI_t2716953283 * ___customElement_7;

public:
	inline static int32_t get_offset_of_defaultElement_6() { return static_cast<int32_t>(offsetof(LoadAndShowSection_2_t2421133264, ___defaultElement_6)); }
	inline DefaultInterstitialUI_t2954553843 * get_defaultElement_6() const { return ___defaultElement_6; }
	inline DefaultInterstitialUI_t2954553843 ** get_address_of_defaultElement_6() { return &___defaultElement_6; }
	inline void set_defaultElement_6(DefaultInterstitialUI_t2954553843 * value)
	{
		___defaultElement_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultElement_6), value);
	}

	inline static int32_t get_offset_of_customElement_7() { return static_cast<int32_t>(offsetof(LoadAndShowSection_2_t2421133264, ___customElement_7)); }
	inline CustomInterstitialUI_t2716953283 * get_customElement_7() const { return ___customElement_7; }
	inline CustomInterstitialUI_t2716953283 ** get_address_of_customElement_7() { return &___customElement_7; }
	inline void set_customElement_7(CustomInterstitialUI_t2716953283 * value)
	{
		___customElement_7 = value;
		Il2CppCodeGenWriteBarrier((&___customElement_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADANDSHOWSECTION_2_T2421133264_H
#ifndef LOADANDSHOWSECTION_2_T1601689498_H
#define LOADANDSHOWSECTION_2_T1601689498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.LoadAndShowSection`2<EasyMobile.Demo.RewardedVideoSection/DefaultRewardedVideolUI,EasyMobile.Demo.RewardedVideoSection/CustomRewardedVideoUI>
struct  LoadAndShowSection_2_t1601689498  : public AdsSection_t4249879119
{
public:
	// T EasyMobile.Demo.LoadAndShowSection`2::defaultElement
	DefaultRewardedVideolUI_t2208290187 * ___defaultElement_6;
	// U EasyMobile.Demo.LoadAndShowSection`2::customElement
	CustomRewardedVideoUI_t2933782405 * ___customElement_7;

public:
	inline static int32_t get_offset_of_defaultElement_6() { return static_cast<int32_t>(offsetof(LoadAndShowSection_2_t1601689498, ___defaultElement_6)); }
	inline DefaultRewardedVideolUI_t2208290187 * get_defaultElement_6() const { return ___defaultElement_6; }
	inline DefaultRewardedVideolUI_t2208290187 ** get_address_of_defaultElement_6() { return &___defaultElement_6; }
	inline void set_defaultElement_6(DefaultRewardedVideolUI_t2208290187 * value)
	{
		___defaultElement_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultElement_6), value);
	}

	inline static int32_t get_offset_of_customElement_7() { return static_cast<int32_t>(offsetof(LoadAndShowSection_2_t1601689498, ___customElement_7)); }
	inline CustomRewardedVideoUI_t2933782405 * get_customElement_7() const { return ___customElement_7; }
	inline CustomRewardedVideoUI_t2933782405 ** get_address_of_customElement_7() { return &___customElement_7; }
	inline void set_customElement_7(CustomRewardedVideoUI_t2933782405 * value)
	{
		___customElement_7 = value;
		Il2CppCodeGenWriteBarrier((&___customElement_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADANDSHOWSECTION_2_T1601689498_H
#ifndef DEFAULTREWARDEDVIDEOLUI_T2208290187_H
#define DEFAULTREWARDEDVIDEOLUI_T2208290187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.RewardedVideoSection/DefaultRewardedVideolUI
struct  DefaultRewardedVideolUI_t2208290187  : public DefaultElement_t384712910
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTREWARDEDVIDEOLUI_T2208290187_H
#ifndef VECTOR2INTFORMATTER_T3358610488_H
#define VECTOR2INTFORMATTER_T3358610488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector2IntFormatter
struct  Vector2IntFormatter_t3358610488  : public MinimalBaseFormatter_1_t480015625
{
public:

public:
};

struct Vector2IntFormatter_t3358610488_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Int32> Sirenix.Serialization.Vector2IntFormatter::Serializer
	Serializer_1_t332960279 * ___Serializer_1;

public:
	inline static int32_t get_offset_of_Serializer_1() { return static_cast<int32_t>(offsetof(Vector2IntFormatter_t3358610488_StaticFields, ___Serializer_1)); }
	inline Serializer_1_t332960279 * get_Serializer_1() const { return ___Serializer_1; }
	inline Serializer_1_t332960279 ** get_address_of_Serializer_1() { return &___Serializer_1; }
	inline void set_Serializer_1(Serializer_1_t332960279 * value)
	{
		___Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INTFORMATTER_T3358610488_H
#ifndef VECTOR3INTFORMATTER_T3358578805_H
#define VECTOR3INTFORMATTER_T3358578805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sirenix.Serialization.Vector3IntFormatter
struct  Vector3IntFormatter_t3358578805  : public MinimalBaseFormatter_1_t2046099566
{
public:

public:
};

struct Vector3IntFormatter_t3358578805_StaticFields
{
public:
	// Sirenix.Serialization.Serializer`1<System.Int32> Sirenix.Serialization.Vector3IntFormatter::Serializer
	Serializer_1_t332960279 * ___Serializer_1;

public:
	inline static int32_t get_offset_of_Serializer_1() { return static_cast<int32_t>(offsetof(Vector3IntFormatter_t3358578805_StaticFields, ___Serializer_1)); }
	inline Serializer_1_t332960279 * get_Serializer_1() const { return ___Serializer_1; }
	inline Serializer_1_t332960279 ** get_address_of_Serializer_1() { return &___Serializer_1; }
	inline void set_Serializer_1(Serializer_1_t332960279 * value)
	{
		___Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTFORMATTER_T3358578805_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_1_T4126069563_H
#define UNITYEVENT_1_T4126069563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Purchasing.Product>
struct  UnityEvent_1_t4126069563  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4126069563, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4126069563_H
#ifndef UNITYEVENT_2_T1877158062_H
#define UNITYEVENT_2_T1877158062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason>
struct  UnityEvent_2_t1877158062  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1877158062, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1877158062_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255375_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255375  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=244 <PrivateImplementationDetails>::$field-51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF
	U24ArrayTypeU3D244_t4214066944  ___U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0;
	// <PrivateImplementationDetails>/$ArrayType=60 <PrivateImplementationDetails>::$field-E769DE2910E6162FDCE1872402BC4A2AC2F7313B
	U24ArrayTypeU3D60_t4028431473  ___U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0)); }
	inline U24ArrayTypeU3D244_t4214066944  get_U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0() const { return ___U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0; }
	inline U24ArrayTypeU3D244_t4214066944 * get_address_of_U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0() { return &___U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0; }
	inline void set_U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0(U24ArrayTypeU3D244_t4214066944  value)
	{
		___U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields, ___U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1)); }
	inline U24ArrayTypeU3D60_t4028431473  get_U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1() const { return ___U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1; }
	inline U24ArrayTypeU3D60_t4028431473 * get_address_of_U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1() { return &___U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1; }
	inline void set_U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1(U24ArrayTypeU3D60_t4028431473  value)
	{
		___U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255375_H
#ifndef AUTOADLOADINGMODE_T854791933_H
#define AUTOADLOADINGMODE_T854791933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AutoAdLoadingMode
struct  AutoAdLoadingMode_t854791933 
{
public:
	// System.Int32 EasyMobile.AutoAdLoadingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoAdLoadingMode_t854791933, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOADLOADINGMODE_T854791933_H
#ifndef CONSENTSTATUS_T2718071093_H
#define CONSENTSTATUS_T2718071093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentStatus
struct  ConsentStatus_t2718071093 
{
public:
	// System.Int32 EasyMobile.ConsentStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConsentStatus_t2718071093, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTSTATUS_T2718071093_H
#ifndef U3CCRCHANGECOLORU3EC__ITERATOR0_T2584720601_H
#define U3CCRCHANGECOLORU3EC__ITERATOR0_T2584720601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0
struct  U3CCRChangeColorU3Ec__Iterator0_t2584720601  : public RuntimeObject
{
public:
	// UnityEngine.Color EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::<newColor>__1
	Color_t2555686324  ___U3CnewColorU3E__1_0;
	// System.Single EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::<elapsed>__2
	float ___U3CelapsedU3E__2_1;
	// System.Single EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::time
	float ___time_2;
	// UnityEngine.Color EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::<c>__3
	Color_t2555686324  ___U3CcU3E__3_3;
	// EasyMobile.Demo.ColorHandler EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::$this
	ColorHandler_t4238062851 * ___U24this_4;
	// System.Object EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 EasyMobile.Demo.ColorHandler/<CRChangeColor>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CnewColorU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U3CnewColorU3E__1_0)); }
	inline Color_t2555686324  get_U3CnewColorU3E__1_0() const { return ___U3CnewColorU3E__1_0; }
	inline Color_t2555686324 * get_address_of_U3CnewColorU3E__1_0() { return &___U3CnewColorU3E__1_0; }
	inline void set_U3CnewColorU3E__1_0(Color_t2555686324  value)
	{
		___U3CnewColorU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__2_1() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U3CelapsedU3E__2_1)); }
	inline float get_U3CelapsedU3E__2_1() const { return ___U3CelapsedU3E__2_1; }
	inline float* get_address_of_U3CelapsedU3E__2_1() { return &___U3CelapsedU3E__2_1; }
	inline void set_U3CelapsedU3E__2_1(float value)
	{
		___U3CelapsedU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_3() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U3CcU3E__3_3)); }
	inline Color_t2555686324  get_U3CcU3E__3_3() const { return ___U3CcU3E__3_3; }
	inline Color_t2555686324 * get_address_of_U3CcU3E__3_3() { return &___U3CcU3E__3_3; }
	inline void set_U3CcU3E__3_3(Color_t2555686324  value)
	{
		___U3CcU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U24this_4)); }
	inline ColorHandler_t4238062851 * get_U24this_4() const { return ___U24this_4; }
	inline ColorHandler_t4238062851 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ColorHandler_t4238062851 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCRChangeColorU3Ec__Iterator0_t2584720601, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRCHANGECOLORU3EC__ITERATOR0_T2584720601_H
#ifndef INTERSTITIALSECTION_T4034589797_H
#define INTERSTITIALSECTION_T4034589797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.InterstitialSection
struct  InterstitialSection_t4034589797  : public LoadAndShowSection_2_t2421133264
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALSECTION_T4034589797_H
#ifndef CUSTOMINTERSTITIALUI_T2716953283_H
#define CUSTOMINTERSTITIALUI_T2716953283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.InterstitialSection/CustomInterstitialUI
struct  CustomInterstitialUI_t2716953283  : public CustomElement_t490039825
{
public:
	// System.Collections.Generic.List`1<EasyMobile.InterstitialAdNetwork> EasyMobile.Demo.InterstitialSection/CustomInterstitialUI::allInterstitialNetworks
	List_1_t225486949 * ___allInterstitialNetworks_8;

public:
	inline static int32_t get_offset_of_allInterstitialNetworks_8() { return static_cast<int32_t>(offsetof(CustomInterstitialUI_t2716953283, ___allInterstitialNetworks_8)); }
	inline List_1_t225486949 * get_allInterstitialNetworks_8() const { return ___allInterstitialNetworks_8; }
	inline List_1_t225486949 ** get_address_of_allInterstitialNetworks_8() { return &___allInterstitialNetworks_8; }
	inline void set_allInterstitialNetworks_8(List_1_t225486949 * value)
	{
		___allInterstitialNetworks_8 = value;
		Il2CppCodeGenWriteBarrier((&___allInterstitialNetworks_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMINTERSTITIALUI_T2716953283_H
#ifndef REWARDEDVIDEOSECTION_T2849218130_H
#define REWARDEDVIDEOSECTION_T2849218130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.RewardedVideoSection
struct  RewardedVideoSection_t2849218130  : public LoadAndShowSection_2_t1601689498
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDVIDEOSECTION_T2849218130_H
#ifndef CUSTOMREWARDEDVIDEOUI_T2933782405_H
#define CUSTOMREWARDEDVIDEOUI_T2933782405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.RewardedVideoSection/CustomRewardedVideoUI
struct  CustomRewardedVideoUI_t2933782405  : public CustomElement_t3965563355
{
public:
	// System.Collections.Generic.List`1<EasyMobile.RewardedAdNetwork> EasyMobile.Demo.RewardedVideoSection/CustomRewardedVideoUI::allRewardedNetworks
	List_1_t1544308239 * ___allRewardedNetworks_8;

public:
	inline static int32_t get_offset_of_allRewardedNetworks_8() { return static_cast<int32_t>(offsetof(CustomRewardedVideoUI_t2933782405, ___allRewardedNetworks_8)); }
	inline List_1_t1544308239 * get_allRewardedNetworks_8() const { return ___allRewardedNetworks_8; }
	inline List_1_t1544308239 ** get_address_of_allRewardedNetworks_8() { return &___allRewardedNetworks_8; }
	inline void set_allRewardedNetworks_8(List_1_t1544308239 * value)
	{
		___allRewardedNetworks_8 = value;
		Il2CppCodeGenWriteBarrier((&___allRewardedNetworks_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMREWARDEDVIDEOUI_T2933782405_H
#ifndef ENUMFLAGSATTRIBUTE_T536974667_H
#define ENUMFLAGSATTRIBUTE_T536974667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.EnumFlagsAttribute
struct  EnumFlagsAttribute_t536974667  : public PropertyAttribute_t3677895545
{
public:
	// System.String EasyMobile.Internal.EnumFlagsAttribute::enumName
	String_t* ___enumName_0;

public:
	inline static int32_t get_offset_of_enumName_0() { return static_cast<int32_t>(offsetof(EnumFlagsAttribute_t536974667, ___enumName_0)); }
	inline String_t* get_enumName_0() const { return ___enumName_0; }
	inline String_t** get_address_of_enumName_0() { return &___enumName_0; }
	inline void set_enumName_0(String_t* value)
	{
		___enumName_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMFLAGSATTRIBUTE_T536974667_H
#ifndef RENAMEATTRIBUTE_T168776318_H
#define RENAMEATTRIBUTE_T168776318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.RenameAttribute
struct  RenameAttribute_t168776318  : public PropertyAttribute_t3677895545
{
public:
	// System.String EasyMobile.Internal.RenameAttribute::<NewName>k__BackingField
	String_t* ___U3CNewNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNewNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenameAttribute_t168776318, ___U3CNewNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNewNameU3Ek__BackingField_0() const { return ___U3CNewNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNewNameU3Ek__BackingField_0() { return &___U3CNewNameU3Ek__BackingField_0; }
	inline void set_U3CNewNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNewNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNewNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENAMEATTRIBUTE_T168776318_H
#ifndef INTERSTITIALADNETWORK_T3048379503_H
#define INTERSTITIALADNETWORK_T3048379503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InterstitialAdNetwork
struct  InterstitialAdNetwork_t3048379503 
{
public:
	// System.Int32 EasyMobile.InterstitialAdNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterstitialAdNetwork_t3048379503, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALADNETWORK_T3048379503_H
#ifndef NOTIFICATIONREPEAT_T3969194584_H
#define NOTIFICATIONREPEAT_T3969194584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationRepeat
struct  NotificationRepeat_t3969194584 
{
public:
	// System.Int32 EasyMobile.NotificationRepeat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotificationRepeat_t3969194584, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONREPEAT_T3969194584_H
#ifndef REWARDEDADNETWORK_T72233497_H
#define REWARDEDADNETWORK_T72233497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.RewardedAdNetwork
struct  RewardedAdNetwork_t72233497 
{
public:
	// System.Int32 EasyMobile.RewardedAdNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RewardedAdNetwork_t72233497, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDADNETWORK_T72233497_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef THREADPRIORITY_T1551740086_H
#define THREADPRIORITY_T1551740086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPriority
struct  ThreadPriority_t1551740086 
{
public:
	// System.Int32 System.Threading.ThreadPriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThreadPriority_t1551740086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_T1551740086_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef BUTTONTYPE_T908070482_H
#define BUTTONTYPE_T908070482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/ButtonType
struct  ButtonType_t908070482 
{
public:
	// System.Int32 UnityEngine.Purchasing.IAPButton/ButtonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonType_t908070482, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTYPE_T908070482_H
#ifndef ONPURCHASECOMPLETEDEVENT_T3721407765_H
#define ONPURCHASECOMPLETEDEVENT_T3721407765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct  OnPurchaseCompletedEvent_t3721407765  : public UnityEvent_1_t4126069563
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASECOMPLETEDEVENT_T3721407765_H
#ifndef ONPURCHASEFAILEDEVENT_T1729542224_H
#define ONPURCHASEFAILEDEVENT_T1729542224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct  OnPurchaseFailedEvent_t1729542224  : public UnityEvent_2_t1877158062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASEFAILEDEVENT_T1729542224_H
#ifndef ONPURCHASECOMPLETEDEVENT_T1675809258_H
#define ONPURCHASECOMPLETEDEVENT_T1675809258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct  OnPurchaseCompletedEvent_t1675809258  : public UnityEvent_1_t4126069563
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASECOMPLETEDEVENT_T1675809258_H
#ifndef ONPURCHASEFAILEDEVENT_T800864861_H
#define ONPURCHASEFAILEDEVENT_T800864861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct  OnPurchaseFailedEvent_t800864861  : public UnityEvent_2_t1877158062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASEFAILEDEVENT_T800864861_H
#ifndef U3CONINTERSTITIALADCOMPLETEDU3EC__ANONSTOREY1_T1182564281_H
#define U3CONINTERSTITIALADCOMPLETEDU3EC__ANONSTOREY1_T1182564281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.AdvertisingDemo/<OnInterstitialAdCompleted>c__AnonStorey1
struct  U3COnInterstitialAdCompletedU3Ec__AnonStorey1_t1182564281  : public RuntimeObject
{
public:
	// EasyMobile.InterstitialAdNetwork EasyMobile.Demo.AdvertisingDemo/<OnInterstitialAdCompleted>c__AnonStorey1::network
	int32_t ___network_0;
	// EasyMobile.AdPlacement EasyMobile.Demo.AdvertisingDemo/<OnInterstitialAdCompleted>c__AnonStorey1::placement
	AdPlacement_t3195089512 * ___placement_1;

public:
	inline static int32_t get_offset_of_network_0() { return static_cast<int32_t>(offsetof(U3COnInterstitialAdCompletedU3Ec__AnonStorey1_t1182564281, ___network_0)); }
	inline int32_t get_network_0() const { return ___network_0; }
	inline int32_t* get_address_of_network_0() { return &___network_0; }
	inline void set_network_0(int32_t value)
	{
		___network_0 = value;
	}

	inline static int32_t get_offset_of_placement_1() { return static_cast<int32_t>(offsetof(U3COnInterstitialAdCompletedU3Ec__AnonStorey1_t1182564281, ___placement_1)); }
	inline AdPlacement_t3195089512 * get_placement_1() const { return ___placement_1; }
	inline AdPlacement_t3195089512 ** get_address_of_placement_1() { return &___placement_1; }
	inline void set_placement_1(AdPlacement_t3195089512 * value)
	{
		___placement_1 = value;
		Il2CppCodeGenWriteBarrier((&___placement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONINTERSTITIALADCOMPLETEDU3EC__ANONSTOREY1_T1182564281_H
#ifndef U3CONREWARDEDADCOMPLETEDU3EC__ANONSTOREY2_T785433302_H
#define U3CONREWARDEDADCOMPLETEDU3EC__ANONSTOREY2_T785433302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.AdvertisingDemo/<OnRewardedAdCompleted>c__AnonStorey2
struct  U3COnRewardedAdCompletedU3Ec__AnonStorey2_t785433302  : public RuntimeObject
{
public:
	// EasyMobile.RewardedAdNetwork EasyMobile.Demo.AdvertisingDemo/<OnRewardedAdCompleted>c__AnonStorey2::network
	int32_t ___network_0;
	// EasyMobile.AdPlacement EasyMobile.Demo.AdvertisingDemo/<OnRewardedAdCompleted>c__AnonStorey2::placement
	AdPlacement_t3195089512 * ___placement_1;

public:
	inline static int32_t get_offset_of_network_0() { return static_cast<int32_t>(offsetof(U3COnRewardedAdCompletedU3Ec__AnonStorey2_t785433302, ___network_0)); }
	inline int32_t get_network_0() const { return ___network_0; }
	inline int32_t* get_address_of_network_0() { return &___network_0; }
	inline void set_network_0(int32_t value)
	{
		___network_0 = value;
	}

	inline static int32_t get_offset_of_placement_1() { return static_cast<int32_t>(offsetof(U3COnRewardedAdCompletedU3Ec__AnonStorey2_t785433302, ___placement_1)); }
	inline AdPlacement_t3195089512 * get_placement_1() const { return ___placement_1; }
	inline AdPlacement_t3195089512 ** get_address_of_placement_1() { return &___placement_1; }
	inline void set_placement_1(AdPlacement_t3195089512 * value)
	{
		___placement_1 = value;
		Il2CppCodeGenWriteBarrier((&___placement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREWARDEDADCOMPLETEDU3EC__ANONSTOREY2_T785433302_H
#ifndef U3CONREWARDEDADSKIPPEDU3EC__ANONSTOREY3_T625254138_H
#define U3CONREWARDEDADSKIPPEDU3EC__ANONSTOREY3_T625254138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.AdvertisingDemo/<OnRewardedAdSkipped>c__AnonStorey3
struct  U3COnRewardedAdSkippedU3Ec__AnonStorey3_t625254138  : public RuntimeObject
{
public:
	// EasyMobile.RewardedAdNetwork EasyMobile.Demo.AdvertisingDemo/<OnRewardedAdSkipped>c__AnonStorey3::network
	int32_t ___network_0;
	// EasyMobile.AdPlacement EasyMobile.Demo.AdvertisingDemo/<OnRewardedAdSkipped>c__AnonStorey3::placement
	AdPlacement_t3195089512 * ___placement_1;

public:
	inline static int32_t get_offset_of_network_0() { return static_cast<int32_t>(offsetof(U3COnRewardedAdSkippedU3Ec__AnonStorey3_t625254138, ___network_0)); }
	inline int32_t get_network_0() const { return ___network_0; }
	inline int32_t* get_address_of_network_0() { return &___network_0; }
	inline void set_network_0(int32_t value)
	{
		___network_0 = value;
	}

	inline static int32_t get_offset_of_placement_1() { return static_cast<int32_t>(offsetof(U3COnRewardedAdSkippedU3Ec__AnonStorey3_t625254138, ___placement_1)); }
	inline AdPlacement_t3195089512 * get_placement_1() const { return ___placement_1; }
	inline AdPlacement_t3195089512 ** get_address_of_placement_1() { return &___placement_1; }
	inline void set_placement_1(AdPlacement_t3195089512 * value)
	{
		___placement_1 = value;
		Il2CppCodeGenWriteBarrier((&___placement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREWARDEDADSKIPPEDU3EC__ANONSTOREY3_T625254138_H
#ifndef DEMOAPPCONSENT_T1008287276_H
#define DEMOAPPCONSENT_T1008287276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.DemoAppConsent
struct  DemoAppConsent_t1008287276  : public RuntimeObject
{
public:
	// EasyMobile.ConsentStatus EasyMobile.Demo.DemoAppConsent::advertisingConsent
	int32_t ___advertisingConsent_1;
	// EasyMobile.ConsentStatus EasyMobile.Demo.DemoAppConsent::notificationConsent
	int32_t ___notificationConsent_2;

public:
	inline static int32_t get_offset_of_advertisingConsent_1() { return static_cast<int32_t>(offsetof(DemoAppConsent_t1008287276, ___advertisingConsent_1)); }
	inline int32_t get_advertisingConsent_1() const { return ___advertisingConsent_1; }
	inline int32_t* get_address_of_advertisingConsent_1() { return &___advertisingConsent_1; }
	inline void set_advertisingConsent_1(int32_t value)
	{
		___advertisingConsent_1 = value;
	}

	inline static int32_t get_offset_of_notificationConsent_2() { return static_cast<int32_t>(offsetof(DemoAppConsent_t1008287276, ___notificationConsent_2)); }
	inline int32_t get_notificationConsent_2() const { return ___notificationConsent_2; }
	inline int32_t* get_address_of_notificationConsent_2() { return &___notificationConsent_2; }
	inline void set_notificationConsent_2(int32_t value)
	{
		___notificationConsent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOAPPCONSENT_T1008287276_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef APPSTORESETTINGS_T2325796953_H
#define APPSTORESETTINGS_T2325796953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSettings
struct  AppStoreSettings_t2325796953  : public ScriptableObject_t2528358522
{
public:
	// System.String AppStoresSupport.AppStoreSettings::UnityClientID
	String_t* ___UnityClientID_4;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientKey
	String_t* ___UnityClientKey_5;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientRSAPublicKey
	String_t* ___UnityClientRSAPublicKey_6;
	// AppStoresSupport.AppStoreSetting AppStoresSupport.AppStoreSettings::XiaomiAppStoreSetting
	AppStoreSetting_t1592337179 * ___XiaomiAppStoreSetting_7;

public:
	inline static int32_t get_offset_of_UnityClientID_4() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___UnityClientID_4)); }
	inline String_t* get_UnityClientID_4() const { return ___UnityClientID_4; }
	inline String_t** get_address_of_UnityClientID_4() { return &___UnityClientID_4; }
	inline void set_UnityClientID_4(String_t* value)
	{
		___UnityClientID_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientID_4), value);
	}

	inline static int32_t get_offset_of_UnityClientKey_5() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___UnityClientKey_5)); }
	inline String_t* get_UnityClientKey_5() const { return ___UnityClientKey_5; }
	inline String_t** get_address_of_UnityClientKey_5() { return &___UnityClientKey_5; }
	inline void set_UnityClientKey_5(String_t* value)
	{
		___UnityClientKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientKey_5), value);
	}

	inline static int32_t get_offset_of_UnityClientRSAPublicKey_6() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___UnityClientRSAPublicKey_6)); }
	inline String_t* get_UnityClientRSAPublicKey_6() const { return ___UnityClientRSAPublicKey_6; }
	inline String_t** get_address_of_UnityClientRSAPublicKey_6() { return &___UnityClientRSAPublicKey_6; }
	inline void set_UnityClientRSAPublicKey_6(String_t* value)
	{
		___UnityClientRSAPublicKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientRSAPublicKey_6), value);
	}

	inline static int32_t get_offset_of_XiaomiAppStoreSetting_7() { return static_cast<int32_t>(offsetof(AppStoreSettings_t2325796953, ___XiaomiAppStoreSetting_7)); }
	inline AppStoreSetting_t1592337179 * get_XiaomiAppStoreSetting_7() const { return ___XiaomiAppStoreSetting_7; }
	inline AppStoreSetting_t1592337179 ** get_address_of_XiaomiAppStoreSetting_7() { return &___XiaomiAppStoreSetting_7; }
	inline void set_XiaomiAppStoreSetting_7(AppStoreSetting_t1592337179 * value)
	{
		___XiaomiAppStoreSetting_7 = value;
		Il2CppCodeGenWriteBarrier((&___XiaomiAppStoreSetting_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTINGS_T2325796953_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ADVERTISINGDEMO_T3945797904_H
#define ADVERTISINGDEMO_T3945797904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.AdvertisingDemo
struct  AdvertisingDemo_t3945797904  : public MonoBehaviour_t3962482529
{
public:
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.AdvertisingDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_4;
	// UnityEngine.GameObject EasyMobile.Demo.AdvertisingDemo::curtain
	GameObject_t1113636619 * ___curtain_5;
	// UnityEngine.GameObject EasyMobile.Demo.AdvertisingDemo::isAdRemovedInfo
	GameObject_t1113636619 * ___isAdRemovedInfo_6;
	// UnityEngine.UI.Text EasyMobile.Demo.AdvertisingDemo::defaultBannerNetworkText
	Text_t1901882714 * ___defaultBannerNetworkText_7;
	// UnityEngine.UI.Text EasyMobile.Demo.AdvertisingDemo::defaultInterstitialNetworkText
	Text_t1901882714 * ___defaultInterstitialNetworkText_8;
	// UnityEngine.UI.Text EasyMobile.Demo.AdvertisingDemo::defaultRewardedNetworkText
	Text_t1901882714 * ___defaultRewardedNetworkText_9;
	// UnityEngine.UI.Dropdown EasyMobile.Demo.AdvertisingDemo::autoLoadModeDropdown
	Dropdown_t2274391225 * ___autoLoadModeDropdown_10;
	// EasyMobile.Demo.BannerSection EasyMobile.Demo.AdvertisingDemo::bannerUI
	BannerSection_t2412952700 * ___bannerUI_11;
	// EasyMobile.Demo.InterstitialSection EasyMobile.Demo.AdvertisingDemo::interstitialUI
	InterstitialSection_t4034589797 * ___interstitialUI_12;
	// EasyMobile.Demo.RewardedVideoSection EasyMobile.Demo.AdvertisingDemo::rewardedVideoUI
	RewardedVideoSection_t2849218130 * ___rewardedVideoUI_13;
	// EasyMobile.AutoAdLoadingMode EasyMobile.Demo.AdvertisingDemo::lastAutoLoadMode
	int32_t ___lastAutoLoadMode_14;

public:
	inline static int32_t get_offset_of_demoUtils_4() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___demoUtils_4)); }
	inline DemoUtils_t1360065201 * get_demoUtils_4() const { return ___demoUtils_4; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_4() { return &___demoUtils_4; }
	inline void set_demoUtils_4(DemoUtils_t1360065201 * value)
	{
		___demoUtils_4 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_4), value);
	}

	inline static int32_t get_offset_of_curtain_5() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___curtain_5)); }
	inline GameObject_t1113636619 * get_curtain_5() const { return ___curtain_5; }
	inline GameObject_t1113636619 ** get_address_of_curtain_5() { return &___curtain_5; }
	inline void set_curtain_5(GameObject_t1113636619 * value)
	{
		___curtain_5 = value;
		Il2CppCodeGenWriteBarrier((&___curtain_5), value);
	}

	inline static int32_t get_offset_of_isAdRemovedInfo_6() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___isAdRemovedInfo_6)); }
	inline GameObject_t1113636619 * get_isAdRemovedInfo_6() const { return ___isAdRemovedInfo_6; }
	inline GameObject_t1113636619 ** get_address_of_isAdRemovedInfo_6() { return &___isAdRemovedInfo_6; }
	inline void set_isAdRemovedInfo_6(GameObject_t1113636619 * value)
	{
		___isAdRemovedInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___isAdRemovedInfo_6), value);
	}

	inline static int32_t get_offset_of_defaultBannerNetworkText_7() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___defaultBannerNetworkText_7)); }
	inline Text_t1901882714 * get_defaultBannerNetworkText_7() const { return ___defaultBannerNetworkText_7; }
	inline Text_t1901882714 ** get_address_of_defaultBannerNetworkText_7() { return &___defaultBannerNetworkText_7; }
	inline void set_defaultBannerNetworkText_7(Text_t1901882714 * value)
	{
		___defaultBannerNetworkText_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBannerNetworkText_7), value);
	}

	inline static int32_t get_offset_of_defaultInterstitialNetworkText_8() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___defaultInterstitialNetworkText_8)); }
	inline Text_t1901882714 * get_defaultInterstitialNetworkText_8() const { return ___defaultInterstitialNetworkText_8; }
	inline Text_t1901882714 ** get_address_of_defaultInterstitialNetworkText_8() { return &___defaultInterstitialNetworkText_8; }
	inline void set_defaultInterstitialNetworkText_8(Text_t1901882714 * value)
	{
		___defaultInterstitialNetworkText_8 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInterstitialNetworkText_8), value);
	}

	inline static int32_t get_offset_of_defaultRewardedNetworkText_9() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___defaultRewardedNetworkText_9)); }
	inline Text_t1901882714 * get_defaultRewardedNetworkText_9() const { return ___defaultRewardedNetworkText_9; }
	inline Text_t1901882714 ** get_address_of_defaultRewardedNetworkText_9() { return &___defaultRewardedNetworkText_9; }
	inline void set_defaultRewardedNetworkText_9(Text_t1901882714 * value)
	{
		___defaultRewardedNetworkText_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultRewardedNetworkText_9), value);
	}

	inline static int32_t get_offset_of_autoLoadModeDropdown_10() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___autoLoadModeDropdown_10)); }
	inline Dropdown_t2274391225 * get_autoLoadModeDropdown_10() const { return ___autoLoadModeDropdown_10; }
	inline Dropdown_t2274391225 ** get_address_of_autoLoadModeDropdown_10() { return &___autoLoadModeDropdown_10; }
	inline void set_autoLoadModeDropdown_10(Dropdown_t2274391225 * value)
	{
		___autoLoadModeDropdown_10 = value;
		Il2CppCodeGenWriteBarrier((&___autoLoadModeDropdown_10), value);
	}

	inline static int32_t get_offset_of_bannerUI_11() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___bannerUI_11)); }
	inline BannerSection_t2412952700 * get_bannerUI_11() const { return ___bannerUI_11; }
	inline BannerSection_t2412952700 ** get_address_of_bannerUI_11() { return &___bannerUI_11; }
	inline void set_bannerUI_11(BannerSection_t2412952700 * value)
	{
		___bannerUI_11 = value;
		Il2CppCodeGenWriteBarrier((&___bannerUI_11), value);
	}

	inline static int32_t get_offset_of_interstitialUI_12() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___interstitialUI_12)); }
	inline InterstitialSection_t4034589797 * get_interstitialUI_12() const { return ___interstitialUI_12; }
	inline InterstitialSection_t4034589797 ** get_address_of_interstitialUI_12() { return &___interstitialUI_12; }
	inline void set_interstitialUI_12(InterstitialSection_t4034589797 * value)
	{
		___interstitialUI_12 = value;
		Il2CppCodeGenWriteBarrier((&___interstitialUI_12), value);
	}

	inline static int32_t get_offset_of_rewardedVideoUI_13() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___rewardedVideoUI_13)); }
	inline RewardedVideoSection_t2849218130 * get_rewardedVideoUI_13() const { return ___rewardedVideoUI_13; }
	inline RewardedVideoSection_t2849218130 ** get_address_of_rewardedVideoUI_13() { return &___rewardedVideoUI_13; }
	inline void set_rewardedVideoUI_13(RewardedVideoSection_t2849218130 * value)
	{
		___rewardedVideoUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___rewardedVideoUI_13), value);
	}

	inline static int32_t get_offset_of_lastAutoLoadMode_14() { return static_cast<int32_t>(offsetof(AdvertisingDemo_t3945797904, ___lastAutoLoadMode_14)); }
	inline int32_t get_lastAutoLoadMode_14() const { return ___lastAutoLoadMode_14; }
	inline int32_t* get_address_of_lastAutoLoadMode_14() { return &___lastAutoLoadMode_14; }
	inline void set_lastAutoLoadMode_14(int32_t value)
	{
		___lastAutoLoadMode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVERTISINGDEMO_T3945797904_H
#ifndef COLORCHOOSER_T386109851_H
#define COLORCHOOSER_T386109851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.ColorChooser
struct  ColorChooser_t386109851  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image EasyMobile.Demo.ColorChooser::imgComp
	Image_t2670269651 * ___imgComp_5;
	// UnityEngine.UI.Button EasyMobile.Demo.ColorChooser::btnComp
	Button_t4055032469 * ___btnComp_6;

public:
	inline static int32_t get_offset_of_imgComp_5() { return static_cast<int32_t>(offsetof(ColorChooser_t386109851, ___imgComp_5)); }
	inline Image_t2670269651 * get_imgComp_5() const { return ___imgComp_5; }
	inline Image_t2670269651 ** get_address_of_imgComp_5() { return &___imgComp_5; }
	inline void set_imgComp_5(Image_t2670269651 * value)
	{
		___imgComp_5 = value;
		Il2CppCodeGenWriteBarrier((&___imgComp_5), value);
	}

	inline static int32_t get_offset_of_btnComp_6() { return static_cast<int32_t>(offsetof(ColorChooser_t386109851, ___btnComp_6)); }
	inline Button_t4055032469 * get_btnComp_6() const { return ___btnComp_6; }
	inline Button_t4055032469 ** get_address_of_btnComp_6() { return &___btnComp_6; }
	inline void set_btnComp_6(Button_t4055032469 * value)
	{
		___btnComp_6 = value;
		Il2CppCodeGenWriteBarrier((&___btnComp_6), value);
	}
};

struct ColorChooser_t386109851_StaticFields
{
public:
	// System.Action`1<UnityEngine.Color> EasyMobile.Demo.ColorChooser::colorSelected
	Action_1_t2728153919 * ___colorSelected_4;

public:
	inline static int32_t get_offset_of_colorSelected_4() { return static_cast<int32_t>(offsetof(ColorChooser_t386109851_StaticFields, ___colorSelected_4)); }
	inline Action_1_t2728153919 * get_colorSelected_4() const { return ___colorSelected_4; }
	inline Action_1_t2728153919 ** get_address_of_colorSelected_4() { return &___colorSelected_4; }
	inline void set_colorSelected_4(Action_1_t2728153919 * value)
	{
		___colorSelected_4 = value;
		Il2CppCodeGenWriteBarrier((&___colorSelected_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHOOSER_T386109851_H
#ifndef COLORHANDLER_T4238062851_H
#define COLORHANDLER_T4238062851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.ColorHandler
struct  ColorHandler_t4238062851  : public MonoBehaviour_t3962482529
{
public:
	// System.Single EasyMobile.Demo.ColorHandler::lerpTime
	float ___lerpTime_4;
	// UnityEngine.Color[] EasyMobile.Demo.ColorHandler::colors
	ColorU5BU5D_t941916413* ___colors_5;
	// UnityEngine.UI.Image EasyMobile.Demo.ColorHandler::imgComp
	Image_t2670269651 * ___imgComp_6;
	// UnityEngine.Material EasyMobile.Demo.ColorHandler::material
	Material_t340375123 * ___material_7;
	// UnityEngine.Color EasyMobile.Demo.ColorHandler::currentColor
	Color_t2555686324  ___currentColor_8;

public:
	inline static int32_t get_offset_of_lerpTime_4() { return static_cast<int32_t>(offsetof(ColorHandler_t4238062851, ___lerpTime_4)); }
	inline float get_lerpTime_4() const { return ___lerpTime_4; }
	inline float* get_address_of_lerpTime_4() { return &___lerpTime_4; }
	inline void set_lerpTime_4(float value)
	{
		___lerpTime_4 = value;
	}

	inline static int32_t get_offset_of_colors_5() { return static_cast<int32_t>(offsetof(ColorHandler_t4238062851, ___colors_5)); }
	inline ColorU5BU5D_t941916413* get_colors_5() const { return ___colors_5; }
	inline ColorU5BU5D_t941916413** get_address_of_colors_5() { return &___colors_5; }
	inline void set_colors_5(ColorU5BU5D_t941916413* value)
	{
		___colors_5 = value;
		Il2CppCodeGenWriteBarrier((&___colors_5), value);
	}

	inline static int32_t get_offset_of_imgComp_6() { return static_cast<int32_t>(offsetof(ColorHandler_t4238062851, ___imgComp_6)); }
	inline Image_t2670269651 * get_imgComp_6() const { return ___imgComp_6; }
	inline Image_t2670269651 ** get_address_of_imgComp_6() { return &___imgComp_6; }
	inline void set_imgComp_6(Image_t2670269651 * value)
	{
		___imgComp_6 = value;
		Il2CppCodeGenWriteBarrier((&___imgComp_6), value);
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(ColorHandler_t4238062851, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_currentColor_8() { return static_cast<int32_t>(offsetof(ColorHandler_t4238062851, ___currentColor_8)); }
	inline Color_t2555686324  get_currentColor_8() const { return ___currentColor_8; }
	inline Color_t2555686324 * get_address_of_currentColor_8() { return &___currentColor_8; }
	inline void set_currentColor_8(Color_t2555686324  value)
	{
		___currentColor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORHANDLER_T4238062851_H
#ifndef DEMOHOMECONTROLLER_T3360241642_H
#define DEMOHOMECONTROLLER_T3360241642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.DemoHomeController
struct  DemoHomeController_t3360241642  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text EasyMobile.Demo.DemoHomeController::installationTime
	Text_t1901882714 * ___installationTime_4;

public:
	inline static int32_t get_offset_of_installationTime_4() { return static_cast<int32_t>(offsetof(DemoHomeController_t3360241642, ___installationTime_4)); }
	inline Text_t1901882714 * get_installationTime_4() const { return ___installationTime_4; }
	inline Text_t1901882714 ** get_address_of_installationTime_4() { return &___installationTime_4; }
	inline void set_installationTime_4(Text_t1901882714 * value)
	{
		___installationTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___installationTime_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOHOMECONTROLLER_T3360241642_H
#ifndef DEMOUTILS_T1360065201_H
#define DEMOUTILS_T1360065201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.DemoUtils
struct  DemoUtils_t1360065201  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite EasyMobile.Demo.DemoUtils::checkedSprite
	Sprite_t280657092 * ___checkedSprite_4;
	// UnityEngine.Sprite EasyMobile.Demo.DemoUtils::uncheckedSprite
	Sprite_t280657092 * ___uncheckedSprite_5;

public:
	inline static int32_t get_offset_of_checkedSprite_4() { return static_cast<int32_t>(offsetof(DemoUtils_t1360065201, ___checkedSprite_4)); }
	inline Sprite_t280657092 * get_checkedSprite_4() const { return ___checkedSprite_4; }
	inline Sprite_t280657092 ** get_address_of_checkedSprite_4() { return &___checkedSprite_4; }
	inline void set_checkedSprite_4(Sprite_t280657092 * value)
	{
		___checkedSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___checkedSprite_4), value);
	}

	inline static int32_t get_offset_of_uncheckedSprite_5() { return static_cast<int32_t>(offsetof(DemoUtils_t1360065201, ___uncheckedSprite_5)); }
	inline Sprite_t280657092 * get_uncheckedSprite_5() const { return ___uncheckedSprite_5; }
	inline Sprite_t280657092 ** get_address_of_uncheckedSprite_5() { return &___uncheckedSprite_5; }
	inline void set_uncheckedSprite_5(Sprite_t280657092 * value)
	{
		___uncheckedSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___uncheckedSprite_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOUTILS_T1360065201_H
#ifndef DIGITALCLOCK_T1260851316_H
#define DIGITALCLOCK_T1260851316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.DigitalClock
struct  DigitalClock_t1260851316  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text EasyMobile.Demo.DigitalClock::clockText
	Text_t1901882714 * ___clockText_4;

public:
	inline static int32_t get_offset_of_clockText_4() { return static_cast<int32_t>(offsetof(DigitalClock_t1260851316, ___clockText_4)); }
	inline Text_t1901882714 * get_clockText_4() const { return ___clockText_4; }
	inline Text_t1901882714 ** get_address_of_clockText_4() { return &___clockText_4; }
	inline void set_clockText_4(Text_t1901882714 * value)
	{
		___clockText_4 = value;
		Il2CppCodeGenWriteBarrier((&___clockText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALCLOCK_T1260851316_H
#ifndef GAMESERVICESDEMO_T3276791170_H
#define GAMESERVICESDEMO_T3276791170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.GameServicesDemo
struct  GameServicesDemo_t3276791170  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo::curtain
	GameObject_t1113636619 * ___curtain_4;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo::isAutoInitInfo
	GameObject_t1113636619 * ___isAutoInitInfo_5;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo::isInitializedInfo
	GameObject_t1113636619 * ___isInitializedInfo_6;
	// UnityEngine.UI.Text EasyMobile.Demo.GameServicesDemo::selectedAchievementInfo
	Text_t1901882714 * ___selectedAchievementInfo_7;
	// UnityEngine.UI.Text EasyMobile.Demo.GameServicesDemo::selectedLeaderboardInfo
	Text_t1901882714 * ___selectedLeaderboardInfo_8;
	// UnityEngine.UI.InputField EasyMobile.Demo.GameServicesDemo::scoreInput
	InputField_t3762917431 * ___scoreInput_9;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.GameServicesDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_10;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo::scrollableListPrefab
	GameObject_t1113636619 * ___scrollableListPrefab_11;
	// EasyMobile.Achievement EasyMobile.Demo.GameServicesDemo::selectedAchievement
	Achievement_t2708948008 * ___selectedAchievement_12;
	// EasyMobile.Leaderboard EasyMobile.Demo.GameServicesDemo::selectedLeaderboard
	Leaderboard_t4133458931 * ___selectedLeaderboard_13;
	// System.Boolean EasyMobile.Demo.GameServicesDemo::lastLoginState
	bool ___lastLoginState_14;

public:
	inline static int32_t get_offset_of_curtain_4() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___curtain_4)); }
	inline GameObject_t1113636619 * get_curtain_4() const { return ___curtain_4; }
	inline GameObject_t1113636619 ** get_address_of_curtain_4() { return &___curtain_4; }
	inline void set_curtain_4(GameObject_t1113636619 * value)
	{
		___curtain_4 = value;
		Il2CppCodeGenWriteBarrier((&___curtain_4), value);
	}

	inline static int32_t get_offset_of_isAutoInitInfo_5() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___isAutoInitInfo_5)); }
	inline GameObject_t1113636619 * get_isAutoInitInfo_5() const { return ___isAutoInitInfo_5; }
	inline GameObject_t1113636619 ** get_address_of_isAutoInitInfo_5() { return &___isAutoInitInfo_5; }
	inline void set_isAutoInitInfo_5(GameObject_t1113636619 * value)
	{
		___isAutoInitInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___isAutoInitInfo_5), value);
	}

	inline static int32_t get_offset_of_isInitializedInfo_6() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___isInitializedInfo_6)); }
	inline GameObject_t1113636619 * get_isInitializedInfo_6() const { return ___isInitializedInfo_6; }
	inline GameObject_t1113636619 ** get_address_of_isInitializedInfo_6() { return &___isInitializedInfo_6; }
	inline void set_isInitializedInfo_6(GameObject_t1113636619 * value)
	{
		___isInitializedInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___isInitializedInfo_6), value);
	}

	inline static int32_t get_offset_of_selectedAchievementInfo_7() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___selectedAchievementInfo_7)); }
	inline Text_t1901882714 * get_selectedAchievementInfo_7() const { return ___selectedAchievementInfo_7; }
	inline Text_t1901882714 ** get_address_of_selectedAchievementInfo_7() { return &___selectedAchievementInfo_7; }
	inline void set_selectedAchievementInfo_7(Text_t1901882714 * value)
	{
		___selectedAchievementInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectedAchievementInfo_7), value);
	}

	inline static int32_t get_offset_of_selectedLeaderboardInfo_8() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___selectedLeaderboardInfo_8)); }
	inline Text_t1901882714 * get_selectedLeaderboardInfo_8() const { return ___selectedLeaderboardInfo_8; }
	inline Text_t1901882714 ** get_address_of_selectedLeaderboardInfo_8() { return &___selectedLeaderboardInfo_8; }
	inline void set_selectedLeaderboardInfo_8(Text_t1901882714 * value)
	{
		___selectedLeaderboardInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___selectedLeaderboardInfo_8), value);
	}

	inline static int32_t get_offset_of_scoreInput_9() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___scoreInput_9)); }
	inline InputField_t3762917431 * get_scoreInput_9() const { return ___scoreInput_9; }
	inline InputField_t3762917431 ** get_address_of_scoreInput_9() { return &___scoreInput_9; }
	inline void set_scoreInput_9(InputField_t3762917431 * value)
	{
		___scoreInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___scoreInput_9), value);
	}

	inline static int32_t get_offset_of_demoUtils_10() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___demoUtils_10)); }
	inline DemoUtils_t1360065201 * get_demoUtils_10() const { return ___demoUtils_10; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_10() { return &___demoUtils_10; }
	inline void set_demoUtils_10(DemoUtils_t1360065201 * value)
	{
		___demoUtils_10 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_10), value);
	}

	inline static int32_t get_offset_of_scrollableListPrefab_11() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___scrollableListPrefab_11)); }
	inline GameObject_t1113636619 * get_scrollableListPrefab_11() const { return ___scrollableListPrefab_11; }
	inline GameObject_t1113636619 ** get_address_of_scrollableListPrefab_11() { return &___scrollableListPrefab_11; }
	inline void set_scrollableListPrefab_11(GameObject_t1113636619 * value)
	{
		___scrollableListPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___scrollableListPrefab_11), value);
	}

	inline static int32_t get_offset_of_selectedAchievement_12() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___selectedAchievement_12)); }
	inline Achievement_t2708948008 * get_selectedAchievement_12() const { return ___selectedAchievement_12; }
	inline Achievement_t2708948008 ** get_address_of_selectedAchievement_12() { return &___selectedAchievement_12; }
	inline void set_selectedAchievement_12(Achievement_t2708948008 * value)
	{
		___selectedAchievement_12 = value;
		Il2CppCodeGenWriteBarrier((&___selectedAchievement_12), value);
	}

	inline static int32_t get_offset_of_selectedLeaderboard_13() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___selectedLeaderboard_13)); }
	inline Leaderboard_t4133458931 * get_selectedLeaderboard_13() const { return ___selectedLeaderboard_13; }
	inline Leaderboard_t4133458931 ** get_address_of_selectedLeaderboard_13() { return &___selectedLeaderboard_13; }
	inline void set_selectedLeaderboard_13(Leaderboard_t4133458931 * value)
	{
		___selectedLeaderboard_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectedLeaderboard_13), value);
	}

	inline static int32_t get_offset_of_lastLoginState_14() { return static_cast<int32_t>(offsetof(GameServicesDemo_t3276791170, ___lastLoginState_14)); }
	inline bool get_lastLoginState_14() const { return ___lastLoginState_14; }
	inline bool* get_address_of_lastLoginState_14() { return &___lastLoginState_14; }
	inline void set_lastLoginState_14(bool value)
	{
		___lastLoginState_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVICESDEMO_T3276791170_H
#ifndef GAMESERVICESDEMO_SAVEDGAMES_T941468990_H
#define GAMESERVICESDEMO_SAVEDGAMES_T941468990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.GameServicesDemo_SavedGames
struct  GameServicesDemo_SavedGames_t941468990  : public MonoBehaviour_t3962482529
{
public:
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.GameServicesDemo_SavedGames::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_4;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo_SavedGames::scrollableListPrefab
	GameObject_t1113636619 * ___scrollableListPrefab_5;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo_SavedGames::isSavedGamesEnabledInfo
	GameObject_t1113636619 * ___isSavedGamesEnabledInfo_6;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo_SavedGames::isSavedGameSelectedInfo
	GameObject_t1113636619 * ___isSavedGameSelectedInfo_7;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo_SavedGames::isSavedGameOpenedInfo
	GameObject_t1113636619 * ___isSavedGameOpenedInfo_8;
	// UnityEngine.GameObject EasyMobile.Demo.GameServicesDemo_SavedGames::autoConflictResolutionInfo
	GameObject_t1113636619 * ___autoConflictResolutionInfo_9;
	// UnityEngine.UI.InputField EasyMobile.Demo.GameServicesDemo_SavedGames::savedGameNameInput
	InputField_t3762917431 * ___savedGameNameInput_10;
	// UnityEngine.UI.InputField EasyMobile.Demo.GameServicesDemo_SavedGames::savedGameDataInput
	InputField_t3762917431 * ___savedGameDataInput_11;
	// System.Boolean EasyMobile.Demo.GameServicesDemo_SavedGames::resolveConflictsManually
	bool ___resolveConflictsManually_12;
	// EasyMobile.SavedGame EasyMobile.Demo.GameServicesDemo_SavedGames::selectedSavedGame
	SavedGame_t947814848 * ___selectedSavedGame_14;
	// EasyMobile.SavedGame[] EasyMobile.Demo.GameServicesDemo_SavedGames::allSavedGames
	SavedGameU5BU5D_t399802177* ___allSavedGames_15;

public:
	inline static int32_t get_offset_of_demoUtils_4() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___demoUtils_4)); }
	inline DemoUtils_t1360065201 * get_demoUtils_4() const { return ___demoUtils_4; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_4() { return &___demoUtils_4; }
	inline void set_demoUtils_4(DemoUtils_t1360065201 * value)
	{
		___demoUtils_4 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_4), value);
	}

	inline static int32_t get_offset_of_scrollableListPrefab_5() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___scrollableListPrefab_5)); }
	inline GameObject_t1113636619 * get_scrollableListPrefab_5() const { return ___scrollableListPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_scrollableListPrefab_5() { return &___scrollableListPrefab_5; }
	inline void set_scrollableListPrefab_5(GameObject_t1113636619 * value)
	{
		___scrollableListPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___scrollableListPrefab_5), value);
	}

	inline static int32_t get_offset_of_isSavedGamesEnabledInfo_6() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___isSavedGamesEnabledInfo_6)); }
	inline GameObject_t1113636619 * get_isSavedGamesEnabledInfo_6() const { return ___isSavedGamesEnabledInfo_6; }
	inline GameObject_t1113636619 ** get_address_of_isSavedGamesEnabledInfo_6() { return &___isSavedGamesEnabledInfo_6; }
	inline void set_isSavedGamesEnabledInfo_6(GameObject_t1113636619 * value)
	{
		___isSavedGamesEnabledInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___isSavedGamesEnabledInfo_6), value);
	}

	inline static int32_t get_offset_of_isSavedGameSelectedInfo_7() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___isSavedGameSelectedInfo_7)); }
	inline GameObject_t1113636619 * get_isSavedGameSelectedInfo_7() const { return ___isSavedGameSelectedInfo_7; }
	inline GameObject_t1113636619 ** get_address_of_isSavedGameSelectedInfo_7() { return &___isSavedGameSelectedInfo_7; }
	inline void set_isSavedGameSelectedInfo_7(GameObject_t1113636619 * value)
	{
		___isSavedGameSelectedInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___isSavedGameSelectedInfo_7), value);
	}

	inline static int32_t get_offset_of_isSavedGameOpenedInfo_8() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___isSavedGameOpenedInfo_8)); }
	inline GameObject_t1113636619 * get_isSavedGameOpenedInfo_8() const { return ___isSavedGameOpenedInfo_8; }
	inline GameObject_t1113636619 ** get_address_of_isSavedGameOpenedInfo_8() { return &___isSavedGameOpenedInfo_8; }
	inline void set_isSavedGameOpenedInfo_8(GameObject_t1113636619 * value)
	{
		___isSavedGameOpenedInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___isSavedGameOpenedInfo_8), value);
	}

	inline static int32_t get_offset_of_autoConflictResolutionInfo_9() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___autoConflictResolutionInfo_9)); }
	inline GameObject_t1113636619 * get_autoConflictResolutionInfo_9() const { return ___autoConflictResolutionInfo_9; }
	inline GameObject_t1113636619 ** get_address_of_autoConflictResolutionInfo_9() { return &___autoConflictResolutionInfo_9; }
	inline void set_autoConflictResolutionInfo_9(GameObject_t1113636619 * value)
	{
		___autoConflictResolutionInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___autoConflictResolutionInfo_9), value);
	}

	inline static int32_t get_offset_of_savedGameNameInput_10() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___savedGameNameInput_10)); }
	inline InputField_t3762917431 * get_savedGameNameInput_10() const { return ___savedGameNameInput_10; }
	inline InputField_t3762917431 ** get_address_of_savedGameNameInput_10() { return &___savedGameNameInput_10; }
	inline void set_savedGameNameInput_10(InputField_t3762917431 * value)
	{
		___savedGameNameInput_10 = value;
		Il2CppCodeGenWriteBarrier((&___savedGameNameInput_10), value);
	}

	inline static int32_t get_offset_of_savedGameDataInput_11() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___savedGameDataInput_11)); }
	inline InputField_t3762917431 * get_savedGameDataInput_11() const { return ___savedGameDataInput_11; }
	inline InputField_t3762917431 ** get_address_of_savedGameDataInput_11() { return &___savedGameDataInput_11; }
	inline void set_savedGameDataInput_11(InputField_t3762917431 * value)
	{
		___savedGameDataInput_11 = value;
		Il2CppCodeGenWriteBarrier((&___savedGameDataInput_11), value);
	}

	inline static int32_t get_offset_of_resolveConflictsManually_12() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___resolveConflictsManually_12)); }
	inline bool get_resolveConflictsManually_12() const { return ___resolveConflictsManually_12; }
	inline bool* get_address_of_resolveConflictsManually_12() { return &___resolveConflictsManually_12; }
	inline void set_resolveConflictsManually_12(bool value)
	{
		___resolveConflictsManually_12 = value;
	}

	inline static int32_t get_offset_of_selectedSavedGame_14() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___selectedSavedGame_14)); }
	inline SavedGame_t947814848 * get_selectedSavedGame_14() const { return ___selectedSavedGame_14; }
	inline SavedGame_t947814848 ** get_address_of_selectedSavedGame_14() { return &___selectedSavedGame_14; }
	inline void set_selectedSavedGame_14(SavedGame_t947814848 * value)
	{
		___selectedSavedGame_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectedSavedGame_14), value);
	}

	inline static int32_t get_offset_of_allSavedGames_15() { return static_cast<int32_t>(offsetof(GameServicesDemo_SavedGames_t941468990, ___allSavedGames_15)); }
	inline SavedGameU5BU5D_t399802177* get_allSavedGames_15() const { return ___allSavedGames_15; }
	inline SavedGameU5BU5D_t399802177** get_address_of_allSavedGames_15() { return &___allSavedGames_15; }
	inline void set_allSavedGames_15(SavedGameU5BU5D_t399802177* value)
	{
		___allSavedGames_15 = value;
		Il2CppCodeGenWriteBarrier((&___allSavedGames_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVICESDEMO_SAVEDGAMES_T941468990_H
#ifndef GIFDEMO_T1006419778_H
#define GIFDEMO_T1006419778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.GifDemo
struct  GifDemo_t1006419778  : public MonoBehaviour_t3962482529
{
public:
	// EasyMobile.Recorder EasyMobile.Demo.GifDemo::recorder
	Recorder_t3009289404 * ___recorder_4;
	// System.String EasyMobile.Demo.GifDemo::gifFilename
	String_t* ___gifFilename_5;
	// System.Int32 EasyMobile.Demo.GifDemo::loop
	int32_t ___loop_6;
	// System.Int32 EasyMobile.Demo.GifDemo::quality
	int32_t ___quality_7;
	// System.Threading.ThreadPriority EasyMobile.Demo.GifDemo::exportThreadPriority
	int32_t ___exportThreadPriority_8;
	// System.String EasyMobile.Demo.GifDemo::giphyUsername
	String_t* ___giphyUsername_9;
	// System.String EasyMobile.Demo.GifDemo::giphyApiKey
	String_t* ___giphyApiKey_10;
	// UnityEngine.GameObject EasyMobile.Demo.GifDemo::recordingMark
	GameObject_t1113636619 * ___recordingMark_11;
	// UnityEngine.GameObject EasyMobile.Demo.GifDemo::startRecordingButton
	GameObject_t1113636619 * ___startRecordingButton_12;
	// UnityEngine.GameObject EasyMobile.Demo.GifDemo::stopRecordingButton
	GameObject_t1113636619 * ___stopRecordingButton_13;
	// UnityEngine.GameObject EasyMobile.Demo.GifDemo::playbackPanel
	GameObject_t1113636619 * ___playbackPanel_14;
	// EasyMobile.ClipPlayerUI EasyMobile.Demo.GifDemo::clipPlayer
	ClipPlayerUI_t1249565783 * ___clipPlayer_15;
	// UnityEngine.GameObject EasyMobile.Demo.GifDemo::giphyLogo
	GameObject_t1113636619 * ___giphyLogo_16;
	// UnityEngine.GameObject EasyMobile.Demo.GifDemo::activityProgress
	GameObject_t1113636619 * ___activityProgress_17;
	// UnityEngine.UI.Text EasyMobile.Demo.GifDemo::activityText
	Text_t1901882714 * ___activityText_18;
	// UnityEngine.UI.Text EasyMobile.Demo.GifDemo::progressText
	Text_t1901882714 * ___progressText_19;
	// EasyMobile.AnimatedClip EasyMobile.Demo.GifDemo::recordedClip
	AnimatedClip_t619307834 * ___recordedClip_20;
	// System.Boolean EasyMobile.Demo.GifDemo::isExportingGif
	bool ___isExportingGif_21;
	// System.Boolean EasyMobile.Demo.GifDemo::isUploadingGif
	bool ___isUploadingGif_22;
	// System.String EasyMobile.Demo.GifDemo::exportedGifPath
	String_t* ___exportedGifPath_23;
	// System.String EasyMobile.Demo.GifDemo::uploadedGifUrl
	String_t* ___uploadedGifUrl_24;

public:
	inline static int32_t get_offset_of_recorder_4() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___recorder_4)); }
	inline Recorder_t3009289404 * get_recorder_4() const { return ___recorder_4; }
	inline Recorder_t3009289404 ** get_address_of_recorder_4() { return &___recorder_4; }
	inline void set_recorder_4(Recorder_t3009289404 * value)
	{
		___recorder_4 = value;
		Il2CppCodeGenWriteBarrier((&___recorder_4), value);
	}

	inline static int32_t get_offset_of_gifFilename_5() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___gifFilename_5)); }
	inline String_t* get_gifFilename_5() const { return ___gifFilename_5; }
	inline String_t** get_address_of_gifFilename_5() { return &___gifFilename_5; }
	inline void set_gifFilename_5(String_t* value)
	{
		___gifFilename_5 = value;
		Il2CppCodeGenWriteBarrier((&___gifFilename_5), value);
	}

	inline static int32_t get_offset_of_loop_6() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___loop_6)); }
	inline int32_t get_loop_6() const { return ___loop_6; }
	inline int32_t* get_address_of_loop_6() { return &___loop_6; }
	inline void set_loop_6(int32_t value)
	{
		___loop_6 = value;
	}

	inline static int32_t get_offset_of_quality_7() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___quality_7)); }
	inline int32_t get_quality_7() const { return ___quality_7; }
	inline int32_t* get_address_of_quality_7() { return &___quality_7; }
	inline void set_quality_7(int32_t value)
	{
		___quality_7 = value;
	}

	inline static int32_t get_offset_of_exportThreadPriority_8() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___exportThreadPriority_8)); }
	inline int32_t get_exportThreadPriority_8() const { return ___exportThreadPriority_8; }
	inline int32_t* get_address_of_exportThreadPriority_8() { return &___exportThreadPriority_8; }
	inline void set_exportThreadPriority_8(int32_t value)
	{
		___exportThreadPriority_8 = value;
	}

	inline static int32_t get_offset_of_giphyUsername_9() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___giphyUsername_9)); }
	inline String_t* get_giphyUsername_9() const { return ___giphyUsername_9; }
	inline String_t** get_address_of_giphyUsername_9() { return &___giphyUsername_9; }
	inline void set_giphyUsername_9(String_t* value)
	{
		___giphyUsername_9 = value;
		Il2CppCodeGenWriteBarrier((&___giphyUsername_9), value);
	}

	inline static int32_t get_offset_of_giphyApiKey_10() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___giphyApiKey_10)); }
	inline String_t* get_giphyApiKey_10() const { return ___giphyApiKey_10; }
	inline String_t** get_address_of_giphyApiKey_10() { return &___giphyApiKey_10; }
	inline void set_giphyApiKey_10(String_t* value)
	{
		___giphyApiKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___giphyApiKey_10), value);
	}

	inline static int32_t get_offset_of_recordingMark_11() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___recordingMark_11)); }
	inline GameObject_t1113636619 * get_recordingMark_11() const { return ___recordingMark_11; }
	inline GameObject_t1113636619 ** get_address_of_recordingMark_11() { return &___recordingMark_11; }
	inline void set_recordingMark_11(GameObject_t1113636619 * value)
	{
		___recordingMark_11 = value;
		Il2CppCodeGenWriteBarrier((&___recordingMark_11), value);
	}

	inline static int32_t get_offset_of_startRecordingButton_12() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___startRecordingButton_12)); }
	inline GameObject_t1113636619 * get_startRecordingButton_12() const { return ___startRecordingButton_12; }
	inline GameObject_t1113636619 ** get_address_of_startRecordingButton_12() { return &___startRecordingButton_12; }
	inline void set_startRecordingButton_12(GameObject_t1113636619 * value)
	{
		___startRecordingButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___startRecordingButton_12), value);
	}

	inline static int32_t get_offset_of_stopRecordingButton_13() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___stopRecordingButton_13)); }
	inline GameObject_t1113636619 * get_stopRecordingButton_13() const { return ___stopRecordingButton_13; }
	inline GameObject_t1113636619 ** get_address_of_stopRecordingButton_13() { return &___stopRecordingButton_13; }
	inline void set_stopRecordingButton_13(GameObject_t1113636619 * value)
	{
		___stopRecordingButton_13 = value;
		Il2CppCodeGenWriteBarrier((&___stopRecordingButton_13), value);
	}

	inline static int32_t get_offset_of_playbackPanel_14() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___playbackPanel_14)); }
	inline GameObject_t1113636619 * get_playbackPanel_14() const { return ___playbackPanel_14; }
	inline GameObject_t1113636619 ** get_address_of_playbackPanel_14() { return &___playbackPanel_14; }
	inline void set_playbackPanel_14(GameObject_t1113636619 * value)
	{
		___playbackPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___playbackPanel_14), value);
	}

	inline static int32_t get_offset_of_clipPlayer_15() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___clipPlayer_15)); }
	inline ClipPlayerUI_t1249565783 * get_clipPlayer_15() const { return ___clipPlayer_15; }
	inline ClipPlayerUI_t1249565783 ** get_address_of_clipPlayer_15() { return &___clipPlayer_15; }
	inline void set_clipPlayer_15(ClipPlayerUI_t1249565783 * value)
	{
		___clipPlayer_15 = value;
		Il2CppCodeGenWriteBarrier((&___clipPlayer_15), value);
	}

	inline static int32_t get_offset_of_giphyLogo_16() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___giphyLogo_16)); }
	inline GameObject_t1113636619 * get_giphyLogo_16() const { return ___giphyLogo_16; }
	inline GameObject_t1113636619 ** get_address_of_giphyLogo_16() { return &___giphyLogo_16; }
	inline void set_giphyLogo_16(GameObject_t1113636619 * value)
	{
		___giphyLogo_16 = value;
		Il2CppCodeGenWriteBarrier((&___giphyLogo_16), value);
	}

	inline static int32_t get_offset_of_activityProgress_17() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___activityProgress_17)); }
	inline GameObject_t1113636619 * get_activityProgress_17() const { return ___activityProgress_17; }
	inline GameObject_t1113636619 ** get_address_of_activityProgress_17() { return &___activityProgress_17; }
	inline void set_activityProgress_17(GameObject_t1113636619 * value)
	{
		___activityProgress_17 = value;
		Il2CppCodeGenWriteBarrier((&___activityProgress_17), value);
	}

	inline static int32_t get_offset_of_activityText_18() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___activityText_18)); }
	inline Text_t1901882714 * get_activityText_18() const { return ___activityText_18; }
	inline Text_t1901882714 ** get_address_of_activityText_18() { return &___activityText_18; }
	inline void set_activityText_18(Text_t1901882714 * value)
	{
		___activityText_18 = value;
		Il2CppCodeGenWriteBarrier((&___activityText_18), value);
	}

	inline static int32_t get_offset_of_progressText_19() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___progressText_19)); }
	inline Text_t1901882714 * get_progressText_19() const { return ___progressText_19; }
	inline Text_t1901882714 ** get_address_of_progressText_19() { return &___progressText_19; }
	inline void set_progressText_19(Text_t1901882714 * value)
	{
		___progressText_19 = value;
		Il2CppCodeGenWriteBarrier((&___progressText_19), value);
	}

	inline static int32_t get_offset_of_recordedClip_20() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___recordedClip_20)); }
	inline AnimatedClip_t619307834 * get_recordedClip_20() const { return ___recordedClip_20; }
	inline AnimatedClip_t619307834 ** get_address_of_recordedClip_20() { return &___recordedClip_20; }
	inline void set_recordedClip_20(AnimatedClip_t619307834 * value)
	{
		___recordedClip_20 = value;
		Il2CppCodeGenWriteBarrier((&___recordedClip_20), value);
	}

	inline static int32_t get_offset_of_isExportingGif_21() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___isExportingGif_21)); }
	inline bool get_isExportingGif_21() const { return ___isExportingGif_21; }
	inline bool* get_address_of_isExportingGif_21() { return &___isExportingGif_21; }
	inline void set_isExportingGif_21(bool value)
	{
		___isExportingGif_21 = value;
	}

	inline static int32_t get_offset_of_isUploadingGif_22() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___isUploadingGif_22)); }
	inline bool get_isUploadingGif_22() const { return ___isUploadingGif_22; }
	inline bool* get_address_of_isUploadingGif_22() { return &___isUploadingGif_22; }
	inline void set_isUploadingGif_22(bool value)
	{
		___isUploadingGif_22 = value;
	}

	inline static int32_t get_offset_of_exportedGifPath_23() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___exportedGifPath_23)); }
	inline String_t* get_exportedGifPath_23() const { return ___exportedGifPath_23; }
	inline String_t** get_address_of_exportedGifPath_23() { return &___exportedGifPath_23; }
	inline void set_exportedGifPath_23(String_t* value)
	{
		___exportedGifPath_23 = value;
		Il2CppCodeGenWriteBarrier((&___exportedGifPath_23), value);
	}

	inline static int32_t get_offset_of_uploadedGifUrl_24() { return static_cast<int32_t>(offsetof(GifDemo_t1006419778, ___uploadedGifUrl_24)); }
	inline String_t* get_uploadedGifUrl_24() const { return ___uploadedGifUrl_24; }
	inline String_t** get_address_of_uploadedGifUrl_24() { return &___uploadedGifUrl_24; }
	inline void set_uploadedGifUrl_24(String_t* value)
	{
		___uploadedGifUrl_24 = value;
		Il2CppCodeGenWriteBarrier((&___uploadedGifUrl_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIFDEMO_T1006419778_H
#ifndef INAPPPURCHASINGDEMO_T2157663715_H
#define INAPPPURCHASINGDEMO_T2157663715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.InAppPurchasingDemo
struct  InAppPurchasingDemo_t2157663715  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean EasyMobile.Demo.InAppPurchasingDemo::logProductLocalizedData
	bool ___logProductLocalizedData_4;
	// UnityEngine.GameObject EasyMobile.Demo.InAppPurchasingDemo::curtain
	GameObject_t1113636619 * ___curtain_5;
	// UnityEngine.GameObject EasyMobile.Demo.InAppPurchasingDemo::scrollableListPrefab
	GameObject_t1113636619 * ___scrollableListPrefab_6;
	// UnityEngine.GameObject EasyMobile.Demo.InAppPurchasingDemo::isInitInfo
	GameObject_t1113636619 * ___isInitInfo_7;
	// UnityEngine.UI.Text EasyMobile.Demo.InAppPurchasingDemo::ownedProductsInfo
	Text_t1901882714 * ___ownedProductsInfo_8;
	// UnityEngine.UI.Text EasyMobile.Demo.InAppPurchasingDemo::selectedProductInfo
	Text_t1901882714 * ___selectedProductInfo_9;
	// UnityEngine.GameObject EasyMobile.Demo.InAppPurchasingDemo::receiptViewer
	GameObject_t1113636619 * ___receiptViewer_10;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.InAppPurchasingDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_11;
	// EasyMobile.IAPProduct EasyMobile.Demo.InAppPurchasingDemo::selectedProduct
	IAPProduct_t1805032951 * ___selectedProduct_12;
	// System.Collections.Generic.List`1<EasyMobile.IAPProduct> EasyMobile.Demo.InAppPurchasingDemo::ownedProducts
	List_1_t3277107693 * ___ownedProducts_13;

public:
	inline static int32_t get_offset_of_logProductLocalizedData_4() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___logProductLocalizedData_4)); }
	inline bool get_logProductLocalizedData_4() const { return ___logProductLocalizedData_4; }
	inline bool* get_address_of_logProductLocalizedData_4() { return &___logProductLocalizedData_4; }
	inline void set_logProductLocalizedData_4(bool value)
	{
		___logProductLocalizedData_4 = value;
	}

	inline static int32_t get_offset_of_curtain_5() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___curtain_5)); }
	inline GameObject_t1113636619 * get_curtain_5() const { return ___curtain_5; }
	inline GameObject_t1113636619 ** get_address_of_curtain_5() { return &___curtain_5; }
	inline void set_curtain_5(GameObject_t1113636619 * value)
	{
		___curtain_5 = value;
		Il2CppCodeGenWriteBarrier((&___curtain_5), value);
	}

	inline static int32_t get_offset_of_scrollableListPrefab_6() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___scrollableListPrefab_6)); }
	inline GameObject_t1113636619 * get_scrollableListPrefab_6() const { return ___scrollableListPrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_scrollableListPrefab_6() { return &___scrollableListPrefab_6; }
	inline void set_scrollableListPrefab_6(GameObject_t1113636619 * value)
	{
		___scrollableListPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___scrollableListPrefab_6), value);
	}

	inline static int32_t get_offset_of_isInitInfo_7() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___isInitInfo_7)); }
	inline GameObject_t1113636619 * get_isInitInfo_7() const { return ___isInitInfo_7; }
	inline GameObject_t1113636619 ** get_address_of_isInitInfo_7() { return &___isInitInfo_7; }
	inline void set_isInitInfo_7(GameObject_t1113636619 * value)
	{
		___isInitInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___isInitInfo_7), value);
	}

	inline static int32_t get_offset_of_ownedProductsInfo_8() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___ownedProductsInfo_8)); }
	inline Text_t1901882714 * get_ownedProductsInfo_8() const { return ___ownedProductsInfo_8; }
	inline Text_t1901882714 ** get_address_of_ownedProductsInfo_8() { return &___ownedProductsInfo_8; }
	inline void set_ownedProductsInfo_8(Text_t1901882714 * value)
	{
		___ownedProductsInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___ownedProductsInfo_8), value);
	}

	inline static int32_t get_offset_of_selectedProductInfo_9() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___selectedProductInfo_9)); }
	inline Text_t1901882714 * get_selectedProductInfo_9() const { return ___selectedProductInfo_9; }
	inline Text_t1901882714 ** get_address_of_selectedProductInfo_9() { return &___selectedProductInfo_9; }
	inline void set_selectedProductInfo_9(Text_t1901882714 * value)
	{
		___selectedProductInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___selectedProductInfo_9), value);
	}

	inline static int32_t get_offset_of_receiptViewer_10() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___receiptViewer_10)); }
	inline GameObject_t1113636619 * get_receiptViewer_10() const { return ___receiptViewer_10; }
	inline GameObject_t1113636619 ** get_address_of_receiptViewer_10() { return &___receiptViewer_10; }
	inline void set_receiptViewer_10(GameObject_t1113636619 * value)
	{
		___receiptViewer_10 = value;
		Il2CppCodeGenWriteBarrier((&___receiptViewer_10), value);
	}

	inline static int32_t get_offset_of_demoUtils_11() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___demoUtils_11)); }
	inline DemoUtils_t1360065201 * get_demoUtils_11() const { return ___demoUtils_11; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_11() { return &___demoUtils_11; }
	inline void set_demoUtils_11(DemoUtils_t1360065201 * value)
	{
		___demoUtils_11 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_11), value);
	}

	inline static int32_t get_offset_of_selectedProduct_12() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___selectedProduct_12)); }
	inline IAPProduct_t1805032951 * get_selectedProduct_12() const { return ___selectedProduct_12; }
	inline IAPProduct_t1805032951 ** get_address_of_selectedProduct_12() { return &___selectedProduct_12; }
	inline void set_selectedProduct_12(IAPProduct_t1805032951 * value)
	{
		___selectedProduct_12 = value;
		Il2CppCodeGenWriteBarrier((&___selectedProduct_12), value);
	}

	inline static int32_t get_offset_of_ownedProducts_13() { return static_cast<int32_t>(offsetof(InAppPurchasingDemo_t2157663715, ___ownedProducts_13)); }
	inline List_1_t3277107693 * get_ownedProducts_13() const { return ___ownedProducts_13; }
	inline List_1_t3277107693 ** get_address_of_ownedProducts_13() { return &___ownedProducts_13; }
	inline void set_ownedProducts_13(List_1_t3277107693 * value)
	{
		___ownedProducts_13 = value;
		Il2CppCodeGenWriteBarrier((&___ownedProducts_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INAPPPURCHASINGDEMO_T2157663715_H
#ifndef NATIVEUIDEMO_T2713937398_H
#define NATIVEUIDEMO_T2713937398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.NativeUIDemo
struct  NativeUIDemo_t2713937398  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject EasyMobile.Demo.NativeUIDemo::isFirstButtonBool
	GameObject_t1113636619 * ___isFirstButtonBool_4;
	// UnityEngine.GameObject EasyMobile.Demo.NativeUIDemo::isSecondButtonBool
	GameObject_t1113636619 * ___isSecondButtonBool_5;
	// UnityEngine.GameObject EasyMobile.Demo.NativeUIDemo::isThirdButtonBool
	GameObject_t1113636619 * ___isThirdButtonBool_6;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.NativeUIDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_7;

public:
	inline static int32_t get_offset_of_isFirstButtonBool_4() { return static_cast<int32_t>(offsetof(NativeUIDemo_t2713937398, ___isFirstButtonBool_4)); }
	inline GameObject_t1113636619 * get_isFirstButtonBool_4() const { return ___isFirstButtonBool_4; }
	inline GameObject_t1113636619 ** get_address_of_isFirstButtonBool_4() { return &___isFirstButtonBool_4; }
	inline void set_isFirstButtonBool_4(GameObject_t1113636619 * value)
	{
		___isFirstButtonBool_4 = value;
		Il2CppCodeGenWriteBarrier((&___isFirstButtonBool_4), value);
	}

	inline static int32_t get_offset_of_isSecondButtonBool_5() { return static_cast<int32_t>(offsetof(NativeUIDemo_t2713937398, ___isSecondButtonBool_5)); }
	inline GameObject_t1113636619 * get_isSecondButtonBool_5() const { return ___isSecondButtonBool_5; }
	inline GameObject_t1113636619 ** get_address_of_isSecondButtonBool_5() { return &___isSecondButtonBool_5; }
	inline void set_isSecondButtonBool_5(GameObject_t1113636619 * value)
	{
		___isSecondButtonBool_5 = value;
		Il2CppCodeGenWriteBarrier((&___isSecondButtonBool_5), value);
	}

	inline static int32_t get_offset_of_isThirdButtonBool_6() { return static_cast<int32_t>(offsetof(NativeUIDemo_t2713937398, ___isThirdButtonBool_6)); }
	inline GameObject_t1113636619 * get_isThirdButtonBool_6() const { return ___isThirdButtonBool_6; }
	inline GameObject_t1113636619 ** get_address_of_isThirdButtonBool_6() { return &___isThirdButtonBool_6; }
	inline void set_isThirdButtonBool_6(GameObject_t1113636619 * value)
	{
		___isThirdButtonBool_6 = value;
		Il2CppCodeGenWriteBarrier((&___isThirdButtonBool_6), value);
	}

	inline static int32_t get_offset_of_demoUtils_7() { return static_cast<int32_t>(offsetof(NativeUIDemo_t2713937398, ___demoUtils_7)); }
	inline DemoUtils_t1360065201 * get_demoUtils_7() const { return ___demoUtils_7; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_7() { return &___demoUtils_7; }
	inline void set_demoUtils_7(DemoUtils_t1360065201 * value)
	{
		___demoUtils_7 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEUIDEMO_T2713937398_H
#ifndef NOTIFICATIONHANDLER_T1136578581_H
#define NOTIFICATIONHANDLER_T1136578581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.NotificationHandler
struct  NotificationHandler_t1136578581  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct NotificationHandler_t1136578581_StaticFields
{
public:
	// EasyMobile.Demo.NotificationHandler EasyMobile.Demo.NotificationHandler::<Instance>k__BackingField
	NotificationHandler_t1136578581 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NotificationHandler_t1136578581_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline NotificationHandler_t1136578581 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline NotificationHandler_t1136578581 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(NotificationHandler_t1136578581 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONHANDLER_T1136578581_H
#ifndef NOTIFICATIONSDEMO_T701924017_H
#define NOTIFICATIONSDEMO_T701924017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.NotificationsDemo
struct  NotificationsDemo_t701924017  : public MonoBehaviour_t3962482529
{
public:
	// System.String EasyMobile.Demo.NotificationsDemo::title
	String_t* ___title_4;
	// System.String EasyMobile.Demo.NotificationsDemo::subtitle
	String_t* ___subtitle_5;
	// System.String EasyMobile.Demo.NotificationsDemo::message
	String_t* ___message_6;
	// System.String EasyMobile.Demo.NotificationsDemo::categoryId
	String_t* ___categoryId_7;
	// System.Boolean EasyMobile.Demo.NotificationsDemo::fakeNewUpdate
	bool ___fakeNewUpdate_8;
	// System.Int32 EasyMobile.Demo.NotificationsDemo::delayHours
	int32_t ___delayHours_9;
	// System.Int32 EasyMobile.Demo.NotificationsDemo::delayMinutes
	int32_t ___delayMinutes_10;
	// System.Int32 EasyMobile.Demo.NotificationsDemo::delaySeconds
	int32_t ___delaySeconds_11;
	// System.String EasyMobile.Demo.NotificationsDemo::repeatTitle
	String_t* ___repeatTitle_12;
	// System.String EasyMobile.Demo.NotificationsDemo::repeatSubtitle
	String_t* ___repeatSubtitle_13;
	// System.String EasyMobile.Demo.NotificationsDemo::repeatMessage
	String_t* ___repeatMessage_14;
	// System.String EasyMobile.Demo.NotificationsDemo::repeatCategoryId
	String_t* ___repeatCategoryId_15;
	// System.Int32 EasyMobile.Demo.NotificationsDemo::repeatDelayHours
	int32_t ___repeatDelayHours_16;
	// System.Int32 EasyMobile.Demo.NotificationsDemo::repeatDelayMinutes
	int32_t ___repeatDelayMinutes_17;
	// System.Int32 EasyMobile.Demo.NotificationsDemo::repeatDelaySeconds
	int32_t ___repeatDelaySeconds_18;
	// EasyMobile.NotificationRepeat EasyMobile.Demo.NotificationsDemo::repeatType
	int32_t ___repeatType_19;
	// UnityEngine.GameObject EasyMobile.Demo.NotificationsDemo::curtain
	GameObject_t1113636619 * ___curtain_20;
	// UnityEngine.GameObject EasyMobile.Demo.NotificationsDemo::pushNotifService
	GameObject_t1113636619 * ___pushNotifService_21;
	// UnityEngine.GameObject EasyMobile.Demo.NotificationsDemo::isAutoInitInfo
	GameObject_t1113636619 * ___isAutoInitInfo_22;
	// UnityEngine.GameObject EasyMobile.Demo.NotificationsDemo::isInitializedInfo
	GameObject_t1113636619 * ___isInitializedInfo_23;
	// UnityEngine.UI.Text EasyMobile.Demo.NotificationsDemo::pendingNotificationList
	Text_t1901882714 * ___pendingNotificationList_24;
	// UnityEngine.UI.InputField EasyMobile.Demo.NotificationsDemo::idInputField
	InputField_t3762917431 * ___idInputField_25;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.NotificationsDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_26;
	// System.String EasyMobile.Demo.NotificationsDemo::orgNotificationListText
	String_t* ___orgNotificationListText_27;

public:
	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___title_4)); }
	inline String_t* get_title_4() const { return ___title_4; }
	inline String_t** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(String_t* value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier((&___title_4), value);
	}

	inline static int32_t get_offset_of_subtitle_5() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___subtitle_5)); }
	inline String_t* get_subtitle_5() const { return ___subtitle_5; }
	inline String_t** get_address_of_subtitle_5() { return &___subtitle_5; }
	inline void set_subtitle_5(String_t* value)
	{
		___subtitle_5 = value;
		Il2CppCodeGenWriteBarrier((&___subtitle_5), value);
	}

	inline static int32_t get_offset_of_message_6() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___message_6)); }
	inline String_t* get_message_6() const { return ___message_6; }
	inline String_t** get_address_of_message_6() { return &___message_6; }
	inline void set_message_6(String_t* value)
	{
		___message_6 = value;
		Il2CppCodeGenWriteBarrier((&___message_6), value);
	}

	inline static int32_t get_offset_of_categoryId_7() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___categoryId_7)); }
	inline String_t* get_categoryId_7() const { return ___categoryId_7; }
	inline String_t** get_address_of_categoryId_7() { return &___categoryId_7; }
	inline void set_categoryId_7(String_t* value)
	{
		___categoryId_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryId_7), value);
	}

	inline static int32_t get_offset_of_fakeNewUpdate_8() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___fakeNewUpdate_8)); }
	inline bool get_fakeNewUpdate_8() const { return ___fakeNewUpdate_8; }
	inline bool* get_address_of_fakeNewUpdate_8() { return &___fakeNewUpdate_8; }
	inline void set_fakeNewUpdate_8(bool value)
	{
		___fakeNewUpdate_8 = value;
	}

	inline static int32_t get_offset_of_delayHours_9() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___delayHours_9)); }
	inline int32_t get_delayHours_9() const { return ___delayHours_9; }
	inline int32_t* get_address_of_delayHours_9() { return &___delayHours_9; }
	inline void set_delayHours_9(int32_t value)
	{
		___delayHours_9 = value;
	}

	inline static int32_t get_offset_of_delayMinutes_10() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___delayMinutes_10)); }
	inline int32_t get_delayMinutes_10() const { return ___delayMinutes_10; }
	inline int32_t* get_address_of_delayMinutes_10() { return &___delayMinutes_10; }
	inline void set_delayMinutes_10(int32_t value)
	{
		___delayMinutes_10 = value;
	}

	inline static int32_t get_offset_of_delaySeconds_11() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___delaySeconds_11)); }
	inline int32_t get_delaySeconds_11() const { return ___delaySeconds_11; }
	inline int32_t* get_address_of_delaySeconds_11() { return &___delaySeconds_11; }
	inline void set_delaySeconds_11(int32_t value)
	{
		___delaySeconds_11 = value;
	}

	inline static int32_t get_offset_of_repeatTitle_12() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatTitle_12)); }
	inline String_t* get_repeatTitle_12() const { return ___repeatTitle_12; }
	inline String_t** get_address_of_repeatTitle_12() { return &___repeatTitle_12; }
	inline void set_repeatTitle_12(String_t* value)
	{
		___repeatTitle_12 = value;
		Il2CppCodeGenWriteBarrier((&___repeatTitle_12), value);
	}

	inline static int32_t get_offset_of_repeatSubtitle_13() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatSubtitle_13)); }
	inline String_t* get_repeatSubtitle_13() const { return ___repeatSubtitle_13; }
	inline String_t** get_address_of_repeatSubtitle_13() { return &___repeatSubtitle_13; }
	inline void set_repeatSubtitle_13(String_t* value)
	{
		___repeatSubtitle_13 = value;
		Il2CppCodeGenWriteBarrier((&___repeatSubtitle_13), value);
	}

	inline static int32_t get_offset_of_repeatMessage_14() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatMessage_14)); }
	inline String_t* get_repeatMessage_14() const { return ___repeatMessage_14; }
	inline String_t** get_address_of_repeatMessage_14() { return &___repeatMessage_14; }
	inline void set_repeatMessage_14(String_t* value)
	{
		___repeatMessage_14 = value;
		Il2CppCodeGenWriteBarrier((&___repeatMessage_14), value);
	}

	inline static int32_t get_offset_of_repeatCategoryId_15() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatCategoryId_15)); }
	inline String_t* get_repeatCategoryId_15() const { return ___repeatCategoryId_15; }
	inline String_t** get_address_of_repeatCategoryId_15() { return &___repeatCategoryId_15; }
	inline void set_repeatCategoryId_15(String_t* value)
	{
		___repeatCategoryId_15 = value;
		Il2CppCodeGenWriteBarrier((&___repeatCategoryId_15), value);
	}

	inline static int32_t get_offset_of_repeatDelayHours_16() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatDelayHours_16)); }
	inline int32_t get_repeatDelayHours_16() const { return ___repeatDelayHours_16; }
	inline int32_t* get_address_of_repeatDelayHours_16() { return &___repeatDelayHours_16; }
	inline void set_repeatDelayHours_16(int32_t value)
	{
		___repeatDelayHours_16 = value;
	}

	inline static int32_t get_offset_of_repeatDelayMinutes_17() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatDelayMinutes_17)); }
	inline int32_t get_repeatDelayMinutes_17() const { return ___repeatDelayMinutes_17; }
	inline int32_t* get_address_of_repeatDelayMinutes_17() { return &___repeatDelayMinutes_17; }
	inline void set_repeatDelayMinutes_17(int32_t value)
	{
		___repeatDelayMinutes_17 = value;
	}

	inline static int32_t get_offset_of_repeatDelaySeconds_18() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatDelaySeconds_18)); }
	inline int32_t get_repeatDelaySeconds_18() const { return ___repeatDelaySeconds_18; }
	inline int32_t* get_address_of_repeatDelaySeconds_18() { return &___repeatDelaySeconds_18; }
	inline void set_repeatDelaySeconds_18(int32_t value)
	{
		___repeatDelaySeconds_18 = value;
	}

	inline static int32_t get_offset_of_repeatType_19() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___repeatType_19)); }
	inline int32_t get_repeatType_19() const { return ___repeatType_19; }
	inline int32_t* get_address_of_repeatType_19() { return &___repeatType_19; }
	inline void set_repeatType_19(int32_t value)
	{
		___repeatType_19 = value;
	}

	inline static int32_t get_offset_of_curtain_20() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___curtain_20)); }
	inline GameObject_t1113636619 * get_curtain_20() const { return ___curtain_20; }
	inline GameObject_t1113636619 ** get_address_of_curtain_20() { return &___curtain_20; }
	inline void set_curtain_20(GameObject_t1113636619 * value)
	{
		___curtain_20 = value;
		Il2CppCodeGenWriteBarrier((&___curtain_20), value);
	}

	inline static int32_t get_offset_of_pushNotifService_21() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___pushNotifService_21)); }
	inline GameObject_t1113636619 * get_pushNotifService_21() const { return ___pushNotifService_21; }
	inline GameObject_t1113636619 ** get_address_of_pushNotifService_21() { return &___pushNotifService_21; }
	inline void set_pushNotifService_21(GameObject_t1113636619 * value)
	{
		___pushNotifService_21 = value;
		Il2CppCodeGenWriteBarrier((&___pushNotifService_21), value);
	}

	inline static int32_t get_offset_of_isAutoInitInfo_22() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___isAutoInitInfo_22)); }
	inline GameObject_t1113636619 * get_isAutoInitInfo_22() const { return ___isAutoInitInfo_22; }
	inline GameObject_t1113636619 ** get_address_of_isAutoInitInfo_22() { return &___isAutoInitInfo_22; }
	inline void set_isAutoInitInfo_22(GameObject_t1113636619 * value)
	{
		___isAutoInitInfo_22 = value;
		Il2CppCodeGenWriteBarrier((&___isAutoInitInfo_22), value);
	}

	inline static int32_t get_offset_of_isInitializedInfo_23() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___isInitializedInfo_23)); }
	inline GameObject_t1113636619 * get_isInitializedInfo_23() const { return ___isInitializedInfo_23; }
	inline GameObject_t1113636619 ** get_address_of_isInitializedInfo_23() { return &___isInitializedInfo_23; }
	inline void set_isInitializedInfo_23(GameObject_t1113636619 * value)
	{
		___isInitializedInfo_23 = value;
		Il2CppCodeGenWriteBarrier((&___isInitializedInfo_23), value);
	}

	inline static int32_t get_offset_of_pendingNotificationList_24() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___pendingNotificationList_24)); }
	inline Text_t1901882714 * get_pendingNotificationList_24() const { return ___pendingNotificationList_24; }
	inline Text_t1901882714 ** get_address_of_pendingNotificationList_24() { return &___pendingNotificationList_24; }
	inline void set_pendingNotificationList_24(Text_t1901882714 * value)
	{
		___pendingNotificationList_24 = value;
		Il2CppCodeGenWriteBarrier((&___pendingNotificationList_24), value);
	}

	inline static int32_t get_offset_of_idInputField_25() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___idInputField_25)); }
	inline InputField_t3762917431 * get_idInputField_25() const { return ___idInputField_25; }
	inline InputField_t3762917431 ** get_address_of_idInputField_25() { return &___idInputField_25; }
	inline void set_idInputField_25(InputField_t3762917431 * value)
	{
		___idInputField_25 = value;
		Il2CppCodeGenWriteBarrier((&___idInputField_25), value);
	}

	inline static int32_t get_offset_of_demoUtils_26() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___demoUtils_26)); }
	inline DemoUtils_t1360065201 * get_demoUtils_26() const { return ___demoUtils_26; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_26() { return &___demoUtils_26; }
	inline void set_demoUtils_26(DemoUtils_t1360065201 * value)
	{
		___demoUtils_26 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_26), value);
	}

	inline static int32_t get_offset_of_orgNotificationListText_27() { return static_cast<int32_t>(offsetof(NotificationsDemo_t701924017, ___orgNotificationListText_27)); }
	inline String_t* get_orgNotificationListText_27() const { return ___orgNotificationListText_27; }
	inline String_t** get_address_of_orgNotificationListText_27() { return &___orgNotificationListText_27; }
	inline void set_orgNotificationListText_27(String_t* value)
	{
		___orgNotificationListText_27 = value;
		Il2CppCodeGenWriteBarrier((&___orgNotificationListText_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSDEMO_T701924017_H
#ifndef PRIVACYDEMO_T1050594026_H
#define PRIVACYDEMO_T1050594026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.PrivacyDemo
struct  PrivacyDemo_t1050594026  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean EasyMobile.Demo.PrivacyDemo::shouldRequestConsent
	bool ___shouldRequestConsent_39;
	// UnityEngine.GameObject EasyMobile.Demo.PrivacyDemo::isInEeaRegionDisplayer
	GameObject_t1113636619 * ___isInEeaRegionDisplayer_40;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.PrivacyDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_41;

public:
	inline static int32_t get_offset_of_shouldRequestConsent_39() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026, ___shouldRequestConsent_39)); }
	inline bool get_shouldRequestConsent_39() const { return ___shouldRequestConsent_39; }
	inline bool* get_address_of_shouldRequestConsent_39() { return &___shouldRequestConsent_39; }
	inline void set_shouldRequestConsent_39(bool value)
	{
		___shouldRequestConsent_39 = value;
	}

	inline static int32_t get_offset_of_isInEeaRegionDisplayer_40() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026, ___isInEeaRegionDisplayer_40)); }
	inline GameObject_t1113636619 * get_isInEeaRegionDisplayer_40() const { return ___isInEeaRegionDisplayer_40; }
	inline GameObject_t1113636619 ** get_address_of_isInEeaRegionDisplayer_40() { return &___isInEeaRegionDisplayer_40; }
	inline void set_isInEeaRegionDisplayer_40(GameObject_t1113636619 * value)
	{
		___isInEeaRegionDisplayer_40 = value;
		Il2CppCodeGenWriteBarrier((&___isInEeaRegionDisplayer_40), value);
	}

	inline static int32_t get_offset_of_demoUtils_41() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026, ___demoUtils_41)); }
	inline DemoUtils_t1360065201 * get_demoUtils_41() const { return ___demoUtils_41; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_41() { return &___demoUtils_41; }
	inline void set_demoUtils_41(DemoUtils_t1360065201 * value)
	{
		___demoUtils_41 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_41), value);
	}
};

struct PrivacyDemo_t1050594026_StaticFields
{
public:
	// System.String EasyMobile.Demo.PrivacyDemo::<UnityAnalyticsOptOutURL>k__BackingField
	String_t* ___U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9;
	// EasyMobile.ConsentDialog EasyMobile.Demo.PrivacyDemo::mPreviewConsentDialog
	ConsentDialog_t3732976094 * ___mPreviewConsentDialog_42;
	// EasyMobile.ConsentDialog EasyMobile.Demo.PrivacyDemo::mDemoConsentDialog
	ConsentDialog_t3732976094 * ___mDemoConsentDialog_43;
	// EasyMobile.ConsentDialog EasyMobile.Demo.PrivacyDemo::mDemoConsentDialogLocalized
	ConsentDialog_t3732976094 * ___mDemoConsentDialogLocalized_44;
	// System.Boolean EasyMobile.Demo.PrivacyDemo::mIsInEEARegion
	bool ___mIsInEEARegion_45;
	// System.Action`1<EasyMobile.EEARegionStatus> EasyMobile.Demo.PrivacyDemo::<>f__am$cache0
	Action_1_t3300905132 * ___U3CU3Ef__amU24cache0_46;
	// System.Action`1<EasyMobile.ConsentDialog> EasyMobile.Demo.PrivacyDemo::<>f__mg$cache0
	Action_1_t3905443689 * ___U3CU3Ef__mgU24cache0_47;
	// EasyMobile.ConsentDialog/CompletedHandler EasyMobile.Demo.PrivacyDemo::<>f__mg$cache1
	CompletedHandler_t441206176 * ___U3CU3Ef__mgU24cache1_48;
	// EasyMobile.ConsentDialog/ToggleStateUpdatedHandler EasyMobile.Demo.PrivacyDemo::<>f__mg$cache2
	ToggleStateUpdatedHandler_t2741348581 * ___U3CU3Ef__mgU24cache2_49;
	// System.Action`1<EasyMobile.ConsentDialog> EasyMobile.Demo.PrivacyDemo::<>f__mg$cache3
	Action_1_t3905443689 * ___U3CU3Ef__mgU24cache3_50;
	// EasyMobile.ConsentDialog/CompletedHandler EasyMobile.Demo.PrivacyDemo::<>f__mg$cache4
	CompletedHandler_t441206176 * ___U3CU3Ef__mgU24cache4_51;
	// EasyMobile.ConsentDialog/ToggleStateUpdatedHandler EasyMobile.Demo.PrivacyDemo::<>f__mg$cache5
	ToggleStateUpdatedHandler_t2741348581 * ___U3CU3Ef__mgU24cache5_52;

public:
	inline static int32_t get_offset_of_U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9)); }
	inline String_t* get_U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9() const { return ___U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9() { return &___U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9; }
	inline void set_U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9(String_t* value)
	{
		___U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_mPreviewConsentDialog_42() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___mPreviewConsentDialog_42)); }
	inline ConsentDialog_t3732976094 * get_mPreviewConsentDialog_42() const { return ___mPreviewConsentDialog_42; }
	inline ConsentDialog_t3732976094 ** get_address_of_mPreviewConsentDialog_42() { return &___mPreviewConsentDialog_42; }
	inline void set_mPreviewConsentDialog_42(ConsentDialog_t3732976094 * value)
	{
		___mPreviewConsentDialog_42 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewConsentDialog_42), value);
	}

	inline static int32_t get_offset_of_mDemoConsentDialog_43() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___mDemoConsentDialog_43)); }
	inline ConsentDialog_t3732976094 * get_mDemoConsentDialog_43() const { return ___mDemoConsentDialog_43; }
	inline ConsentDialog_t3732976094 ** get_address_of_mDemoConsentDialog_43() { return &___mDemoConsentDialog_43; }
	inline void set_mDemoConsentDialog_43(ConsentDialog_t3732976094 * value)
	{
		___mDemoConsentDialog_43 = value;
		Il2CppCodeGenWriteBarrier((&___mDemoConsentDialog_43), value);
	}

	inline static int32_t get_offset_of_mDemoConsentDialogLocalized_44() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___mDemoConsentDialogLocalized_44)); }
	inline ConsentDialog_t3732976094 * get_mDemoConsentDialogLocalized_44() const { return ___mDemoConsentDialogLocalized_44; }
	inline ConsentDialog_t3732976094 ** get_address_of_mDemoConsentDialogLocalized_44() { return &___mDemoConsentDialogLocalized_44; }
	inline void set_mDemoConsentDialogLocalized_44(ConsentDialog_t3732976094 * value)
	{
		___mDemoConsentDialogLocalized_44 = value;
		Il2CppCodeGenWriteBarrier((&___mDemoConsentDialogLocalized_44), value);
	}

	inline static int32_t get_offset_of_mIsInEEARegion_45() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___mIsInEEARegion_45)); }
	inline bool get_mIsInEEARegion_45() const { return ___mIsInEEARegion_45; }
	inline bool* get_address_of_mIsInEEARegion_45() { return &___mIsInEEARegion_45; }
	inline void set_mIsInEEARegion_45(bool value)
	{
		___mIsInEEARegion_45 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_46() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__amU24cache0_46)); }
	inline Action_1_t3300905132 * get_U3CU3Ef__amU24cache0_46() const { return ___U3CU3Ef__amU24cache0_46; }
	inline Action_1_t3300905132 ** get_address_of_U3CU3Ef__amU24cache0_46() { return &___U3CU3Ef__amU24cache0_46; }
	inline void set_U3CU3Ef__amU24cache0_46(Action_1_t3300905132 * value)
	{
		___U3CU3Ef__amU24cache0_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_46), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_47() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__mgU24cache0_47)); }
	inline Action_1_t3905443689 * get_U3CU3Ef__mgU24cache0_47() const { return ___U3CU3Ef__mgU24cache0_47; }
	inline Action_1_t3905443689 ** get_address_of_U3CU3Ef__mgU24cache0_47() { return &___U3CU3Ef__mgU24cache0_47; }
	inline void set_U3CU3Ef__mgU24cache0_47(Action_1_t3905443689 * value)
	{
		___U3CU3Ef__mgU24cache0_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_47), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_48() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__mgU24cache1_48)); }
	inline CompletedHandler_t441206176 * get_U3CU3Ef__mgU24cache1_48() const { return ___U3CU3Ef__mgU24cache1_48; }
	inline CompletedHandler_t441206176 ** get_address_of_U3CU3Ef__mgU24cache1_48() { return &___U3CU3Ef__mgU24cache1_48; }
	inline void set_U3CU3Ef__mgU24cache1_48(CompletedHandler_t441206176 * value)
	{
		___U3CU3Ef__mgU24cache1_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_48), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_49() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__mgU24cache2_49)); }
	inline ToggleStateUpdatedHandler_t2741348581 * get_U3CU3Ef__mgU24cache2_49() const { return ___U3CU3Ef__mgU24cache2_49; }
	inline ToggleStateUpdatedHandler_t2741348581 ** get_address_of_U3CU3Ef__mgU24cache2_49() { return &___U3CU3Ef__mgU24cache2_49; }
	inline void set_U3CU3Ef__mgU24cache2_49(ToggleStateUpdatedHandler_t2741348581 * value)
	{
		___U3CU3Ef__mgU24cache2_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_49), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_50() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__mgU24cache3_50)); }
	inline Action_1_t3905443689 * get_U3CU3Ef__mgU24cache3_50() const { return ___U3CU3Ef__mgU24cache3_50; }
	inline Action_1_t3905443689 ** get_address_of_U3CU3Ef__mgU24cache3_50() { return &___U3CU3Ef__mgU24cache3_50; }
	inline void set_U3CU3Ef__mgU24cache3_50(Action_1_t3905443689 * value)
	{
		___U3CU3Ef__mgU24cache3_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_50), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_51() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__mgU24cache4_51)); }
	inline CompletedHandler_t441206176 * get_U3CU3Ef__mgU24cache4_51() const { return ___U3CU3Ef__mgU24cache4_51; }
	inline CompletedHandler_t441206176 ** get_address_of_U3CU3Ef__mgU24cache4_51() { return &___U3CU3Ef__mgU24cache4_51; }
	inline void set_U3CU3Ef__mgU24cache4_51(CompletedHandler_t441206176 * value)
	{
		___U3CU3Ef__mgU24cache4_51 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_52() { return static_cast<int32_t>(offsetof(PrivacyDemo_t1050594026_StaticFields, ___U3CU3Ef__mgU24cache5_52)); }
	inline ToggleStateUpdatedHandler_t2741348581 * get_U3CU3Ef__mgU24cache5_52() const { return ___U3CU3Ef__mgU24cache5_52; }
	inline ToggleStateUpdatedHandler_t2741348581 ** get_address_of_U3CU3Ef__mgU24cache5_52() { return &___U3CU3Ef__mgU24cache5_52; }
	inline void set_U3CU3Ef__mgU24cache5_52(ToggleStateUpdatedHandler_t2741348581 * value)
	{
		___U3CU3Ef__mgU24cache5_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_52), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVACYDEMO_T1050594026_H
#ifndef SCROLLABLELIST_T4043704206_H
#define SCROLLABLELIST_T4043704206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.ScrollableList
struct  ScrollableList_t4043704206  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`3<EasyMobile.Demo.ScrollableList,System.String,System.String> EasyMobile.Demo.ScrollableList::ItemSelected
	Action_3_t3311727287 * ___ItemSelected_4;
	// System.Action`1<EasyMobile.Demo.ScrollableList> EasyMobile.Demo.ScrollableList::UIClosed
	Action_1_t4216171801 * ___UIClosed_5;
	// UnityEngine.Vector3 EasyMobile.Demo.ScrollableList::position
	Vector3_t3722313464  ___position_6;
	// System.Boolean EasyMobile.Demo.ScrollableList::horizontalScroll
	bool ___horizontalScroll_7;
	// System.Boolean EasyMobile.Demo.ScrollableList::verticalScroll
	bool ___verticalScroll_8;
	// System.Single EasyMobile.Demo.ScrollableList::width
	float ___width_9;
	// System.Single EasyMobile.Demo.ScrollableList::height
	float ___height_10;
	// System.Single EasyMobile.Demo.ScrollableList::itemHeight
	float ___itemHeight_11;
	// System.Single EasyMobile.Demo.ScrollableList::spacing
	float ___spacing_12;
	// System.Int32 EasyMobile.Demo.ScrollableList::paddingLeft
	int32_t ___paddingLeft_13;
	// System.Int32 EasyMobile.Demo.ScrollableList::paddingRight
	int32_t ___paddingRight_14;
	// System.Int32 EasyMobile.Demo.ScrollableList::paddingTop
	int32_t ___paddingTop_15;
	// System.Int32 EasyMobile.Demo.ScrollableList::paddingBottom
	int32_t ___paddingBottom_16;
	// UnityEngine.Color EasyMobile.Demo.ScrollableList::bodyColor
	Color_t2555686324  ___bodyColor_17;
	// UnityEngine.Color EasyMobile.Demo.ScrollableList::itemColor
	Color_t2555686324  ___itemColor_18;
	// UnityEngine.UI.Text EasyMobile.Demo.ScrollableList::title
	Text_t1901882714 * ___title_19;
	// UnityEngine.UI.ScrollRect EasyMobile.Demo.ScrollableList::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_20;
	// UnityEngine.Transform EasyMobile.Demo.ScrollableList::content
	Transform_t3600365921 * ___content_21;
	// UnityEngine.GameObject EasyMobile.Demo.ScrollableList::itemPrefab
	GameObject_t1113636619 * ___itemPrefab_22;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EasyMobile.Demo.ScrollableList::items
	Dictionary_2_t1632706988 * ___items_23;

public:
	inline static int32_t get_offset_of_ItemSelected_4() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___ItemSelected_4)); }
	inline Action_3_t3311727287 * get_ItemSelected_4() const { return ___ItemSelected_4; }
	inline Action_3_t3311727287 ** get_address_of_ItemSelected_4() { return &___ItemSelected_4; }
	inline void set_ItemSelected_4(Action_3_t3311727287 * value)
	{
		___ItemSelected_4 = value;
		Il2CppCodeGenWriteBarrier((&___ItemSelected_4), value);
	}

	inline static int32_t get_offset_of_UIClosed_5() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___UIClosed_5)); }
	inline Action_1_t4216171801 * get_UIClosed_5() const { return ___UIClosed_5; }
	inline Action_1_t4216171801 ** get_address_of_UIClosed_5() { return &___UIClosed_5; }
	inline void set_UIClosed_5(Action_1_t4216171801 * value)
	{
		___UIClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___UIClosed_5), value);
	}

	inline static int32_t get_offset_of_position_6() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___position_6)); }
	inline Vector3_t3722313464  get_position_6() const { return ___position_6; }
	inline Vector3_t3722313464 * get_address_of_position_6() { return &___position_6; }
	inline void set_position_6(Vector3_t3722313464  value)
	{
		___position_6 = value;
	}

	inline static int32_t get_offset_of_horizontalScroll_7() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___horizontalScroll_7)); }
	inline bool get_horizontalScroll_7() const { return ___horizontalScroll_7; }
	inline bool* get_address_of_horizontalScroll_7() { return &___horizontalScroll_7; }
	inline void set_horizontalScroll_7(bool value)
	{
		___horizontalScroll_7 = value;
	}

	inline static int32_t get_offset_of_verticalScroll_8() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___verticalScroll_8)); }
	inline bool get_verticalScroll_8() const { return ___verticalScroll_8; }
	inline bool* get_address_of_verticalScroll_8() { return &___verticalScroll_8; }
	inline void set_verticalScroll_8(bool value)
	{
		___verticalScroll_8 = value;
	}

	inline static int32_t get_offset_of_width_9() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___width_9)); }
	inline float get_width_9() const { return ___width_9; }
	inline float* get_address_of_width_9() { return &___width_9; }
	inline void set_width_9(float value)
	{
		___width_9 = value;
	}

	inline static int32_t get_offset_of_height_10() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___height_10)); }
	inline float get_height_10() const { return ___height_10; }
	inline float* get_address_of_height_10() { return &___height_10; }
	inline void set_height_10(float value)
	{
		___height_10 = value;
	}

	inline static int32_t get_offset_of_itemHeight_11() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___itemHeight_11)); }
	inline float get_itemHeight_11() const { return ___itemHeight_11; }
	inline float* get_address_of_itemHeight_11() { return &___itemHeight_11; }
	inline void set_itemHeight_11(float value)
	{
		___itemHeight_11 = value;
	}

	inline static int32_t get_offset_of_spacing_12() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___spacing_12)); }
	inline float get_spacing_12() const { return ___spacing_12; }
	inline float* get_address_of_spacing_12() { return &___spacing_12; }
	inline void set_spacing_12(float value)
	{
		___spacing_12 = value;
	}

	inline static int32_t get_offset_of_paddingLeft_13() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___paddingLeft_13)); }
	inline int32_t get_paddingLeft_13() const { return ___paddingLeft_13; }
	inline int32_t* get_address_of_paddingLeft_13() { return &___paddingLeft_13; }
	inline void set_paddingLeft_13(int32_t value)
	{
		___paddingLeft_13 = value;
	}

	inline static int32_t get_offset_of_paddingRight_14() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___paddingRight_14)); }
	inline int32_t get_paddingRight_14() const { return ___paddingRight_14; }
	inline int32_t* get_address_of_paddingRight_14() { return &___paddingRight_14; }
	inline void set_paddingRight_14(int32_t value)
	{
		___paddingRight_14 = value;
	}

	inline static int32_t get_offset_of_paddingTop_15() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___paddingTop_15)); }
	inline int32_t get_paddingTop_15() const { return ___paddingTop_15; }
	inline int32_t* get_address_of_paddingTop_15() { return &___paddingTop_15; }
	inline void set_paddingTop_15(int32_t value)
	{
		___paddingTop_15 = value;
	}

	inline static int32_t get_offset_of_paddingBottom_16() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___paddingBottom_16)); }
	inline int32_t get_paddingBottom_16() const { return ___paddingBottom_16; }
	inline int32_t* get_address_of_paddingBottom_16() { return &___paddingBottom_16; }
	inline void set_paddingBottom_16(int32_t value)
	{
		___paddingBottom_16 = value;
	}

	inline static int32_t get_offset_of_bodyColor_17() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___bodyColor_17)); }
	inline Color_t2555686324  get_bodyColor_17() const { return ___bodyColor_17; }
	inline Color_t2555686324 * get_address_of_bodyColor_17() { return &___bodyColor_17; }
	inline void set_bodyColor_17(Color_t2555686324  value)
	{
		___bodyColor_17 = value;
	}

	inline static int32_t get_offset_of_itemColor_18() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___itemColor_18)); }
	inline Color_t2555686324  get_itemColor_18() const { return ___itemColor_18; }
	inline Color_t2555686324 * get_address_of_itemColor_18() { return &___itemColor_18; }
	inline void set_itemColor_18(Color_t2555686324  value)
	{
		___itemColor_18 = value;
	}

	inline static int32_t get_offset_of_title_19() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___title_19)); }
	inline Text_t1901882714 * get_title_19() const { return ___title_19; }
	inline Text_t1901882714 ** get_address_of_title_19() { return &___title_19; }
	inline void set_title_19(Text_t1901882714 * value)
	{
		___title_19 = value;
		Il2CppCodeGenWriteBarrier((&___title_19), value);
	}

	inline static int32_t get_offset_of_scrollRect_20() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___scrollRect_20)); }
	inline ScrollRect_t4137855814 * get_scrollRect_20() const { return ___scrollRect_20; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_20() { return &___scrollRect_20; }
	inline void set_scrollRect_20(ScrollRect_t4137855814 * value)
	{
		___scrollRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_20), value);
	}

	inline static int32_t get_offset_of_content_21() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___content_21)); }
	inline Transform_t3600365921 * get_content_21() const { return ___content_21; }
	inline Transform_t3600365921 ** get_address_of_content_21() { return &___content_21; }
	inline void set_content_21(Transform_t3600365921 * value)
	{
		___content_21 = value;
		Il2CppCodeGenWriteBarrier((&___content_21), value);
	}

	inline static int32_t get_offset_of_itemPrefab_22() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___itemPrefab_22)); }
	inline GameObject_t1113636619 * get_itemPrefab_22() const { return ___itemPrefab_22; }
	inline GameObject_t1113636619 ** get_address_of_itemPrefab_22() { return &___itemPrefab_22; }
	inline void set_itemPrefab_22(GameObject_t1113636619 * value)
	{
		___itemPrefab_22 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrefab_22), value);
	}

	inline static int32_t get_offset_of_items_23() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206, ___items_23)); }
	inline Dictionary_2_t1632706988 * get_items_23() const { return ___items_23; }
	inline Dictionary_2_t1632706988 ** get_address_of_items_23() { return &___items_23; }
	inline void set_items_23(Dictionary_2_t1632706988 * value)
	{
		___items_23 = value;
		Il2CppCodeGenWriteBarrier((&___items_23), value);
	}
};

struct ScrollableList_t4043704206_StaticFields
{
public:
	// System.Action`3<EasyMobile.Demo.ScrollableList,System.String,System.String> EasyMobile.Demo.ScrollableList::<>f__am$cache0
	Action_3_t3311727287 * ___U3CU3Ef__amU24cache0_24;
	// System.Action`1<EasyMobile.Demo.ScrollableList> EasyMobile.Demo.ScrollableList::<>f__am$cache1
	Action_1_t4216171801 * ___U3CU3Ef__amU24cache1_25;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_24() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206_StaticFields, ___U3CU3Ef__amU24cache0_24)); }
	inline Action_3_t3311727287 * get_U3CU3Ef__amU24cache0_24() const { return ___U3CU3Ef__amU24cache0_24; }
	inline Action_3_t3311727287 ** get_address_of_U3CU3Ef__amU24cache0_24() { return &___U3CU3Ef__amU24cache0_24; }
	inline void set_U3CU3Ef__amU24cache0_24(Action_3_t3311727287 * value)
	{
		___U3CU3Ef__amU24cache0_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_25() { return static_cast<int32_t>(offsetof(ScrollableList_t4043704206_StaticFields, ___U3CU3Ef__amU24cache1_25)); }
	inline Action_1_t4216171801 * get_U3CU3Ef__amU24cache1_25() const { return ___U3CU3Ef__amU24cache1_25; }
	inline Action_1_t4216171801 ** get_address_of_U3CU3Ef__amU24cache1_25() { return &___U3CU3Ef__amU24cache1_25; }
	inline void set_U3CU3Ef__amU24cache1_25(Action_1_t4216171801 * value)
	{
		___U3CU3Ef__amU24cache1_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLABLELIST_T4043704206_H
#ifndef SCROLLABLELISTITEM_T3654577678_H
#define SCROLLABLELISTITEM_T3654577678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.ScrollableListItem
struct  ScrollableListItem_t3654577678  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text EasyMobile.Demo.ScrollableListItem::title
	Text_t1901882714 * ___title_4;
	// UnityEngine.UI.Text EasyMobile.Demo.ScrollableListItem::subtitle
	Text_t1901882714 * ___subtitle_5;
	// UnityEngine.UI.Button EasyMobile.Demo.ScrollableListItem::button
	Button_t4055032469 * ___button_6;

public:
	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(ScrollableListItem_t3654577678, ___title_4)); }
	inline Text_t1901882714 * get_title_4() const { return ___title_4; }
	inline Text_t1901882714 ** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(Text_t1901882714 * value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier((&___title_4), value);
	}

	inline static int32_t get_offset_of_subtitle_5() { return static_cast<int32_t>(offsetof(ScrollableListItem_t3654577678, ___subtitle_5)); }
	inline Text_t1901882714 * get_subtitle_5() const { return ___subtitle_5; }
	inline Text_t1901882714 ** get_address_of_subtitle_5() { return &___subtitle_5; }
	inline void set_subtitle_5(Text_t1901882714 * value)
	{
		___subtitle_5 = value;
		Il2CppCodeGenWriteBarrier((&___subtitle_5), value);
	}

	inline static int32_t get_offset_of_button_6() { return static_cast<int32_t>(offsetof(ScrollableListItem_t3654577678, ___button_6)); }
	inline Button_t4055032469 * get_button_6() const { return ___button_6; }
	inline Button_t4055032469 ** get_address_of_button_6() { return &___button_6; }
	inline void set_button_6(Button_t4055032469 * value)
	{
		___button_6 = value;
		Il2CppCodeGenWriteBarrier((&___button_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLABLELISTITEM_T3654577678_H
#ifndef SHARINGDEMO_T2942947019_H
#define SHARINGDEMO_T2942947019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.SharingDemo
struct  SharingDemo_t2942947019  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image EasyMobile.Demo.SharingDemo::clockRect
	Image_t2670269651 * ___clockRect_4;
	// UnityEngine.UI.Text EasyMobile.Demo.SharingDemo::clockText
	Text_t1901882714 * ___clockText_5;
	// System.String EasyMobile.Demo.SharingDemo::TwoStepScreenshotName
	String_t* ___TwoStepScreenshotName_6;
	// System.String EasyMobile.Demo.SharingDemo::OneStepScreenshotName
	String_t* ___OneStepScreenshotName_7;
	// System.String EasyMobile.Demo.SharingDemo::TwoStepScreenshotPath
	String_t* ___TwoStepScreenshotPath_8;
	// System.String EasyMobile.Demo.SharingDemo::sampleMessage
	String_t* ___sampleMessage_9;
	// System.String EasyMobile.Demo.SharingDemo::sampleText
	String_t* ___sampleText_10;
	// System.String EasyMobile.Demo.SharingDemo::sampleURL
	String_t* ___sampleURL_11;

public:
	inline static int32_t get_offset_of_clockRect_4() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___clockRect_4)); }
	inline Image_t2670269651 * get_clockRect_4() const { return ___clockRect_4; }
	inline Image_t2670269651 ** get_address_of_clockRect_4() { return &___clockRect_4; }
	inline void set_clockRect_4(Image_t2670269651 * value)
	{
		___clockRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___clockRect_4), value);
	}

	inline static int32_t get_offset_of_clockText_5() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___clockText_5)); }
	inline Text_t1901882714 * get_clockText_5() const { return ___clockText_5; }
	inline Text_t1901882714 ** get_address_of_clockText_5() { return &___clockText_5; }
	inline void set_clockText_5(Text_t1901882714 * value)
	{
		___clockText_5 = value;
		Il2CppCodeGenWriteBarrier((&___clockText_5), value);
	}

	inline static int32_t get_offset_of_TwoStepScreenshotName_6() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___TwoStepScreenshotName_6)); }
	inline String_t* get_TwoStepScreenshotName_6() const { return ___TwoStepScreenshotName_6; }
	inline String_t** get_address_of_TwoStepScreenshotName_6() { return &___TwoStepScreenshotName_6; }
	inline void set_TwoStepScreenshotName_6(String_t* value)
	{
		___TwoStepScreenshotName_6 = value;
		Il2CppCodeGenWriteBarrier((&___TwoStepScreenshotName_6), value);
	}

	inline static int32_t get_offset_of_OneStepScreenshotName_7() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___OneStepScreenshotName_7)); }
	inline String_t* get_OneStepScreenshotName_7() const { return ___OneStepScreenshotName_7; }
	inline String_t** get_address_of_OneStepScreenshotName_7() { return &___OneStepScreenshotName_7; }
	inline void set_OneStepScreenshotName_7(String_t* value)
	{
		___OneStepScreenshotName_7 = value;
		Il2CppCodeGenWriteBarrier((&___OneStepScreenshotName_7), value);
	}

	inline static int32_t get_offset_of_TwoStepScreenshotPath_8() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___TwoStepScreenshotPath_8)); }
	inline String_t* get_TwoStepScreenshotPath_8() const { return ___TwoStepScreenshotPath_8; }
	inline String_t** get_address_of_TwoStepScreenshotPath_8() { return &___TwoStepScreenshotPath_8; }
	inline void set_TwoStepScreenshotPath_8(String_t* value)
	{
		___TwoStepScreenshotPath_8 = value;
		Il2CppCodeGenWriteBarrier((&___TwoStepScreenshotPath_8), value);
	}

	inline static int32_t get_offset_of_sampleMessage_9() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___sampleMessage_9)); }
	inline String_t* get_sampleMessage_9() const { return ___sampleMessage_9; }
	inline String_t** get_address_of_sampleMessage_9() { return &___sampleMessage_9; }
	inline void set_sampleMessage_9(String_t* value)
	{
		___sampleMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___sampleMessage_9), value);
	}

	inline static int32_t get_offset_of_sampleText_10() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___sampleText_10)); }
	inline String_t* get_sampleText_10() const { return ___sampleText_10; }
	inline String_t** get_address_of_sampleText_10() { return &___sampleText_10; }
	inline void set_sampleText_10(String_t* value)
	{
		___sampleText_10 = value;
		Il2CppCodeGenWriteBarrier((&___sampleText_10), value);
	}

	inline static int32_t get_offset_of_sampleURL_11() { return static_cast<int32_t>(offsetof(SharingDemo_t2942947019, ___sampleURL_11)); }
	inline String_t* get_sampleURL_11() const { return ___sampleURL_11; }
	inline String_t** get_address_of_sampleURL_11() { return &___sampleURL_11; }
	inline void set_sampleURL_11(String_t* value)
	{
		___sampleURL_11 = value;
		Il2CppCodeGenWriteBarrier((&___sampleURL_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGDEMO_T2942947019_H
#ifndef SOUNDMANAGER_T1283146133_H
#define SOUNDMANAGER_T1283146133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.SoundManager
struct  SoundManager_t1283146133  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip EasyMobile.Demo.SoundManager::button
	AudioClip_t3680889665 * ___button_5;
	// UnityEngine.AudioSource EasyMobile.Demo.SoundManager::_audioSource
	AudioSource_t3935305588 * ____audioSource_6;

public:
	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(SoundManager_t1283146133, ___button_5)); }
	inline AudioClip_t3680889665 * get_button_5() const { return ___button_5; }
	inline AudioClip_t3680889665 ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(AudioClip_t3680889665 * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier((&___button_5), value);
	}

	inline static int32_t get_offset_of__audioSource_6() { return static_cast<int32_t>(offsetof(SoundManager_t1283146133, ____audioSource_6)); }
	inline AudioSource_t3935305588 * get__audioSource_6() const { return ____audioSource_6; }
	inline AudioSource_t3935305588 ** get_address_of__audioSource_6() { return &____audioSource_6; }
	inline void set__audioSource_6(AudioSource_t3935305588 * value)
	{
		____audioSource_6 = value;
		Il2CppCodeGenWriteBarrier((&____audioSource_6), value);
	}
};

struct SoundManager_t1283146133_StaticFields
{
public:
	// EasyMobile.Demo.SoundManager EasyMobile.Demo.SoundManager::<Instance>k__BackingField
	SoundManager_t1283146133 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SoundManager_t1283146133_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline SoundManager_t1283146133 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline SoundManager_t1283146133 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(SoundManager_t1283146133 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T1283146133_H
#ifndef UTILITIESDEMO_T4069723306_H
#define UTILITIESDEMO_T4069723306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Demo.UtilitiesDemo
struct  UtilitiesDemo_t4069723306  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject EasyMobile.Demo.UtilitiesDemo::ignoreConstraints
	GameObject_t1113636619 * ___ignoreConstraints_4;
	// UnityEngine.GameObject EasyMobile.Demo.UtilitiesDemo::isDisabled
	GameObject_t1113636619 * ___isDisabled_5;
	// UnityEngine.GameObject EasyMobile.Demo.UtilitiesDemo::annualRemainingRequests
	GameObject_t1113636619 * ___annualRemainingRequests_6;
	// UnityEngine.GameObject EasyMobile.Demo.UtilitiesDemo::delayAfterInstallRemainingTime
	GameObject_t1113636619 * ___delayAfterInstallRemainingTime_7;
	// UnityEngine.GameObject EasyMobile.Demo.UtilitiesDemo::coolingOffRemainingTime
	GameObject_t1113636619 * ___coolingOffRemainingTime_8;
	// EasyMobile.Demo.DemoUtils EasyMobile.Demo.UtilitiesDemo::demoUtils
	DemoUtils_t1360065201 * ___demoUtils_9;

public:
	inline static int32_t get_offset_of_ignoreConstraints_4() { return static_cast<int32_t>(offsetof(UtilitiesDemo_t4069723306, ___ignoreConstraints_4)); }
	inline GameObject_t1113636619 * get_ignoreConstraints_4() const { return ___ignoreConstraints_4; }
	inline GameObject_t1113636619 ** get_address_of_ignoreConstraints_4() { return &___ignoreConstraints_4; }
	inline void set_ignoreConstraints_4(GameObject_t1113636619 * value)
	{
		___ignoreConstraints_4 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreConstraints_4), value);
	}

	inline static int32_t get_offset_of_isDisabled_5() { return static_cast<int32_t>(offsetof(UtilitiesDemo_t4069723306, ___isDisabled_5)); }
	inline GameObject_t1113636619 * get_isDisabled_5() const { return ___isDisabled_5; }
	inline GameObject_t1113636619 ** get_address_of_isDisabled_5() { return &___isDisabled_5; }
	inline void set_isDisabled_5(GameObject_t1113636619 * value)
	{
		___isDisabled_5 = value;
		Il2CppCodeGenWriteBarrier((&___isDisabled_5), value);
	}

	inline static int32_t get_offset_of_annualRemainingRequests_6() { return static_cast<int32_t>(offsetof(UtilitiesDemo_t4069723306, ___annualRemainingRequests_6)); }
	inline GameObject_t1113636619 * get_annualRemainingRequests_6() const { return ___annualRemainingRequests_6; }
	inline GameObject_t1113636619 ** get_address_of_annualRemainingRequests_6() { return &___annualRemainingRequests_6; }
	inline void set_annualRemainingRequests_6(GameObject_t1113636619 * value)
	{
		___annualRemainingRequests_6 = value;
		Il2CppCodeGenWriteBarrier((&___annualRemainingRequests_6), value);
	}

	inline static int32_t get_offset_of_delayAfterInstallRemainingTime_7() { return static_cast<int32_t>(offsetof(UtilitiesDemo_t4069723306, ___delayAfterInstallRemainingTime_7)); }
	inline GameObject_t1113636619 * get_delayAfterInstallRemainingTime_7() const { return ___delayAfterInstallRemainingTime_7; }
	inline GameObject_t1113636619 ** get_address_of_delayAfterInstallRemainingTime_7() { return &___delayAfterInstallRemainingTime_7; }
	inline void set_delayAfterInstallRemainingTime_7(GameObject_t1113636619 * value)
	{
		___delayAfterInstallRemainingTime_7 = value;
		Il2CppCodeGenWriteBarrier((&___delayAfterInstallRemainingTime_7), value);
	}

	inline static int32_t get_offset_of_coolingOffRemainingTime_8() { return static_cast<int32_t>(offsetof(UtilitiesDemo_t4069723306, ___coolingOffRemainingTime_8)); }
	inline GameObject_t1113636619 * get_coolingOffRemainingTime_8() const { return ___coolingOffRemainingTime_8; }
	inline GameObject_t1113636619 ** get_address_of_coolingOffRemainingTime_8() { return &___coolingOffRemainingTime_8; }
	inline void set_coolingOffRemainingTime_8(GameObject_t1113636619 * value)
	{
		___coolingOffRemainingTime_8 = value;
		Il2CppCodeGenWriteBarrier((&___coolingOffRemainingTime_8), value);
	}

	inline static int32_t get_offset_of_demoUtils_9() { return static_cast<int32_t>(offsetof(UtilitiesDemo_t4069723306, ___demoUtils_9)); }
	inline DemoUtils_t1360065201 * get_demoUtils_9() const { return ___demoUtils_9; }
	inline DemoUtils_t1360065201 ** get_address_of_demoUtils_9() { return &___demoUtils_9; }
	inline void set_demoUtils_9(DemoUtils_t1360065201 * value)
	{
		___demoUtils_9 = value;
		Il2CppCodeGenWriteBarrier((&___demoUtils_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIESDEMO_T4069723306_H
#ifndef RUNTIMEHELPER_T2481760429_H
#define RUNTIMEHELPER_T2481760429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.RuntimeHelper
struct  RuntimeHelper_t2481760429  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.Action> EasyMobile.Internal.RuntimeHelper::localToMainThreadQueue
	List_1_t2736452219 * ___localToMainThreadQueue_6;

public:
	inline static int32_t get_offset_of_localToMainThreadQueue_6() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429, ___localToMainThreadQueue_6)); }
	inline List_1_t2736452219 * get_localToMainThreadQueue_6() const { return ___localToMainThreadQueue_6; }
	inline List_1_t2736452219 ** get_address_of_localToMainThreadQueue_6() { return &___localToMainThreadQueue_6; }
	inline void set_localToMainThreadQueue_6(List_1_t2736452219 * value)
	{
		___localToMainThreadQueue_6 = value;
		Il2CppCodeGenWriteBarrier((&___localToMainThreadQueue_6), value);
	}
};

struct RuntimeHelper_t2481760429_StaticFields
{
public:
	// EasyMobile.Internal.RuntimeHelper EasyMobile.Internal.RuntimeHelper::mInstance
	RuntimeHelper_t2481760429 * ___mInstance_4;
	// System.Collections.Generic.List`1<System.Action> EasyMobile.Internal.RuntimeHelper::mToMainThreadQueue
	List_1_t2736452219 * ___mToMainThreadQueue_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) EasyMobile.Internal.RuntimeHelper::mIsToMainThreadQueueEmpty
	bool ___mIsToMainThreadQueueEmpty_7;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> EasyMobile.Internal.RuntimeHelper::mPauseCallbackQueue
	List_1_t1741830302 * ___mPauseCallbackQueue_8;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> EasyMobile.Internal.RuntimeHelper::mFocusCallbackQueue
	List_1_t1741830302 * ___mFocusCallbackQueue_9;
	// System.Boolean EasyMobile.Internal.RuntimeHelper::mIsDummy
	bool ___mIsDummy_10;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429_StaticFields, ___mInstance_4)); }
	inline RuntimeHelper_t2481760429 * get_mInstance_4() const { return ___mInstance_4; }
	inline RuntimeHelper_t2481760429 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(RuntimeHelper_t2481760429 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_mToMainThreadQueue_5() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429_StaticFields, ___mToMainThreadQueue_5)); }
	inline List_1_t2736452219 * get_mToMainThreadQueue_5() const { return ___mToMainThreadQueue_5; }
	inline List_1_t2736452219 ** get_address_of_mToMainThreadQueue_5() { return &___mToMainThreadQueue_5; }
	inline void set_mToMainThreadQueue_5(List_1_t2736452219 * value)
	{
		___mToMainThreadQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___mToMainThreadQueue_5), value);
	}

	inline static int32_t get_offset_of_mIsToMainThreadQueueEmpty_7() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429_StaticFields, ___mIsToMainThreadQueueEmpty_7)); }
	inline bool get_mIsToMainThreadQueueEmpty_7() const { return ___mIsToMainThreadQueueEmpty_7; }
	inline bool* get_address_of_mIsToMainThreadQueueEmpty_7() { return &___mIsToMainThreadQueueEmpty_7; }
	inline void set_mIsToMainThreadQueueEmpty_7(bool value)
	{
		___mIsToMainThreadQueueEmpty_7 = value;
	}

	inline static int32_t get_offset_of_mPauseCallbackQueue_8() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429_StaticFields, ___mPauseCallbackQueue_8)); }
	inline List_1_t1741830302 * get_mPauseCallbackQueue_8() const { return ___mPauseCallbackQueue_8; }
	inline List_1_t1741830302 ** get_address_of_mPauseCallbackQueue_8() { return &___mPauseCallbackQueue_8; }
	inline void set_mPauseCallbackQueue_8(List_1_t1741830302 * value)
	{
		___mPauseCallbackQueue_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPauseCallbackQueue_8), value);
	}

	inline static int32_t get_offset_of_mFocusCallbackQueue_9() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429_StaticFields, ___mFocusCallbackQueue_9)); }
	inline List_1_t1741830302 * get_mFocusCallbackQueue_9() const { return ___mFocusCallbackQueue_9; }
	inline List_1_t1741830302 ** get_address_of_mFocusCallbackQueue_9() { return &___mFocusCallbackQueue_9; }
	inline void set_mFocusCallbackQueue_9(List_1_t1741830302 * value)
	{
		___mFocusCallbackQueue_9 = value;
		Il2CppCodeGenWriteBarrier((&___mFocusCallbackQueue_9), value);
	}

	inline static int32_t get_offset_of_mIsDummy_10() { return static_cast<int32_t>(offsetof(RuntimeHelper_t2481760429_StaticFields, ___mIsDummy_10)); }
	inline bool get_mIsDummy_10() const { return ___mIsDummy_10; }
	inline bool* get_address_of_mIsDummy_10() { return &___mIsDummy_10; }
	inline void set_mIsDummy_10(bool value)
	{
		___mIsDummy_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEHELPER_T2481760429_H
#ifndef IAPDEMO_T3681080565_H
#define IAPDEMO_T3681080565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo
struct  IAPDemo_t3681080565  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Purchasing.IStoreController IAPDemo::m_Controller
	RuntimeObject* ___m_Controller_4;
	// UnityEngine.Purchasing.IAppleExtensions IAPDemo::m_AppleExtensions
	RuntimeObject* ___m_AppleExtensions_5;
	// UnityEngine.Purchasing.IMoolahExtension IAPDemo::m_MoolahExtensions
	RuntimeObject* ___m_MoolahExtensions_6;
	// UnityEngine.Purchasing.ISamsungAppsExtensions IAPDemo::m_SamsungExtensions
	RuntimeObject* ___m_SamsungExtensions_7;
	// UnityEngine.Purchasing.IMicrosoftExtensions IAPDemo::m_MicrosoftExtensions
	RuntimeObject* ___m_MicrosoftExtensions_8;
	// UnityEngine.Purchasing.IUnityChannelExtensions IAPDemo::m_UnityChannelExtensions
	RuntimeObject* ___m_UnityChannelExtensions_9;
	// UnityEngine.Purchasing.ITransactionHistoryExtensions IAPDemo::m_TransactionHistoryExtensions
	RuntimeObject* ___m_TransactionHistoryExtensions_10;
	// System.Boolean IAPDemo::m_IsGooglePlayStoreSelected
	bool ___m_IsGooglePlayStoreSelected_11;
	// System.Boolean IAPDemo::m_IsSamsungAppsStoreSelected
	bool ___m_IsSamsungAppsStoreSelected_12;
	// System.Boolean IAPDemo::m_IsCloudMoolahStoreSelected
	bool ___m_IsCloudMoolahStoreSelected_13;
	// System.Boolean IAPDemo::m_IsUnityChannelSelected
	bool ___m_IsUnityChannelSelected_14;
	// System.String IAPDemo::m_LastTransactionID
	String_t* ___m_LastTransactionID_15;
	// System.Boolean IAPDemo::m_IsLoggedIn
	bool ___m_IsLoggedIn_16;
	// IAPDemo/UnityChannelLoginHandler IAPDemo::unityChannelLoginHandler
	UnityChannelLoginHandler_t2949829254 * ___unityChannelLoginHandler_17;
	// System.Boolean IAPDemo::m_FetchReceiptPayloadOnPurchase
	bool ___m_FetchReceiptPayloadOnPurchase_18;
	// System.Boolean IAPDemo::m_PurchaseInProgress
	bool ___m_PurchaseInProgress_19;
	// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI> IAPDemo::m_ProductUIs
	Dictionary_2_t708210053 * ___m_ProductUIs_20;
	// UnityEngine.GameObject IAPDemo::productUITemplate
	GameObject_t1113636619 * ___productUITemplate_21;
	// UnityEngine.RectTransform IAPDemo::contentRect
	RectTransform_t3704657025 * ___contentRect_22;
	// UnityEngine.UI.Button IAPDemo::restoreButton
	Button_t4055032469 * ___restoreButton_23;
	// UnityEngine.UI.Button IAPDemo::loginButton
	Button_t4055032469 * ___loginButton_24;
	// UnityEngine.UI.Button IAPDemo::validateButton
	Button_t4055032469 * ___validateButton_25;
	// UnityEngine.UI.Text IAPDemo::versionText
	Text_t1901882714 * ___versionText_26;

public:
	inline static int32_t get_offset_of_m_Controller_4() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_Controller_4)); }
	inline RuntimeObject* get_m_Controller_4() const { return ___m_Controller_4; }
	inline RuntimeObject** get_address_of_m_Controller_4() { return &___m_Controller_4; }
	inline void set_m_Controller_4(RuntimeObject* value)
	{
		___m_Controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_4), value);
	}

	inline static int32_t get_offset_of_m_AppleExtensions_5() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_AppleExtensions_5)); }
	inline RuntimeObject* get_m_AppleExtensions_5() const { return ___m_AppleExtensions_5; }
	inline RuntimeObject** get_address_of_m_AppleExtensions_5() { return &___m_AppleExtensions_5; }
	inline void set_m_AppleExtensions_5(RuntimeObject* value)
	{
		___m_AppleExtensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppleExtensions_5), value);
	}

	inline static int32_t get_offset_of_m_MoolahExtensions_6() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_MoolahExtensions_6)); }
	inline RuntimeObject* get_m_MoolahExtensions_6() const { return ___m_MoolahExtensions_6; }
	inline RuntimeObject** get_address_of_m_MoolahExtensions_6() { return &___m_MoolahExtensions_6; }
	inline void set_m_MoolahExtensions_6(RuntimeObject* value)
	{
		___m_MoolahExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoolahExtensions_6), value);
	}

	inline static int32_t get_offset_of_m_SamsungExtensions_7() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_SamsungExtensions_7)); }
	inline RuntimeObject* get_m_SamsungExtensions_7() const { return ___m_SamsungExtensions_7; }
	inline RuntimeObject** get_address_of_m_SamsungExtensions_7() { return &___m_SamsungExtensions_7; }
	inline void set_m_SamsungExtensions_7(RuntimeObject* value)
	{
		___m_SamsungExtensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SamsungExtensions_7), value);
	}

	inline static int32_t get_offset_of_m_MicrosoftExtensions_8() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_MicrosoftExtensions_8)); }
	inline RuntimeObject* get_m_MicrosoftExtensions_8() const { return ___m_MicrosoftExtensions_8; }
	inline RuntimeObject** get_address_of_m_MicrosoftExtensions_8() { return &___m_MicrosoftExtensions_8; }
	inline void set_m_MicrosoftExtensions_8(RuntimeObject* value)
	{
		___m_MicrosoftExtensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MicrosoftExtensions_8), value);
	}

	inline static int32_t get_offset_of_m_UnityChannelExtensions_9() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_UnityChannelExtensions_9)); }
	inline RuntimeObject* get_m_UnityChannelExtensions_9() const { return ___m_UnityChannelExtensions_9; }
	inline RuntimeObject** get_address_of_m_UnityChannelExtensions_9() { return &___m_UnityChannelExtensions_9; }
	inline void set_m_UnityChannelExtensions_9(RuntimeObject* value)
	{
		___m_UnityChannelExtensions_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnityChannelExtensions_9), value);
	}

	inline static int32_t get_offset_of_m_TransactionHistoryExtensions_10() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_TransactionHistoryExtensions_10)); }
	inline RuntimeObject* get_m_TransactionHistoryExtensions_10() const { return ___m_TransactionHistoryExtensions_10; }
	inline RuntimeObject** get_address_of_m_TransactionHistoryExtensions_10() { return &___m_TransactionHistoryExtensions_10; }
	inline void set_m_TransactionHistoryExtensions_10(RuntimeObject* value)
	{
		___m_TransactionHistoryExtensions_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TransactionHistoryExtensions_10), value);
	}

	inline static int32_t get_offset_of_m_IsGooglePlayStoreSelected_11() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsGooglePlayStoreSelected_11)); }
	inline bool get_m_IsGooglePlayStoreSelected_11() const { return ___m_IsGooglePlayStoreSelected_11; }
	inline bool* get_address_of_m_IsGooglePlayStoreSelected_11() { return &___m_IsGooglePlayStoreSelected_11; }
	inline void set_m_IsGooglePlayStoreSelected_11(bool value)
	{
		___m_IsGooglePlayStoreSelected_11 = value;
	}

	inline static int32_t get_offset_of_m_IsSamsungAppsStoreSelected_12() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsSamsungAppsStoreSelected_12)); }
	inline bool get_m_IsSamsungAppsStoreSelected_12() const { return ___m_IsSamsungAppsStoreSelected_12; }
	inline bool* get_address_of_m_IsSamsungAppsStoreSelected_12() { return &___m_IsSamsungAppsStoreSelected_12; }
	inline void set_m_IsSamsungAppsStoreSelected_12(bool value)
	{
		___m_IsSamsungAppsStoreSelected_12 = value;
	}

	inline static int32_t get_offset_of_m_IsCloudMoolahStoreSelected_13() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsCloudMoolahStoreSelected_13)); }
	inline bool get_m_IsCloudMoolahStoreSelected_13() const { return ___m_IsCloudMoolahStoreSelected_13; }
	inline bool* get_address_of_m_IsCloudMoolahStoreSelected_13() { return &___m_IsCloudMoolahStoreSelected_13; }
	inline void set_m_IsCloudMoolahStoreSelected_13(bool value)
	{
		___m_IsCloudMoolahStoreSelected_13 = value;
	}

	inline static int32_t get_offset_of_m_IsUnityChannelSelected_14() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsUnityChannelSelected_14)); }
	inline bool get_m_IsUnityChannelSelected_14() const { return ___m_IsUnityChannelSelected_14; }
	inline bool* get_address_of_m_IsUnityChannelSelected_14() { return &___m_IsUnityChannelSelected_14; }
	inline void set_m_IsUnityChannelSelected_14(bool value)
	{
		___m_IsUnityChannelSelected_14 = value;
	}

	inline static int32_t get_offset_of_m_LastTransactionID_15() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_LastTransactionID_15)); }
	inline String_t* get_m_LastTransactionID_15() const { return ___m_LastTransactionID_15; }
	inline String_t** get_address_of_m_LastTransactionID_15() { return &___m_LastTransactionID_15; }
	inline void set_m_LastTransactionID_15(String_t* value)
	{
		___m_LastTransactionID_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastTransactionID_15), value);
	}

	inline static int32_t get_offset_of_m_IsLoggedIn_16() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_IsLoggedIn_16)); }
	inline bool get_m_IsLoggedIn_16() const { return ___m_IsLoggedIn_16; }
	inline bool* get_address_of_m_IsLoggedIn_16() { return &___m_IsLoggedIn_16; }
	inline void set_m_IsLoggedIn_16(bool value)
	{
		___m_IsLoggedIn_16 = value;
	}

	inline static int32_t get_offset_of_unityChannelLoginHandler_17() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___unityChannelLoginHandler_17)); }
	inline UnityChannelLoginHandler_t2949829254 * get_unityChannelLoginHandler_17() const { return ___unityChannelLoginHandler_17; }
	inline UnityChannelLoginHandler_t2949829254 ** get_address_of_unityChannelLoginHandler_17() { return &___unityChannelLoginHandler_17; }
	inline void set_unityChannelLoginHandler_17(UnityChannelLoginHandler_t2949829254 * value)
	{
		___unityChannelLoginHandler_17 = value;
		Il2CppCodeGenWriteBarrier((&___unityChannelLoginHandler_17), value);
	}

	inline static int32_t get_offset_of_m_FetchReceiptPayloadOnPurchase_18() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_FetchReceiptPayloadOnPurchase_18)); }
	inline bool get_m_FetchReceiptPayloadOnPurchase_18() const { return ___m_FetchReceiptPayloadOnPurchase_18; }
	inline bool* get_address_of_m_FetchReceiptPayloadOnPurchase_18() { return &___m_FetchReceiptPayloadOnPurchase_18; }
	inline void set_m_FetchReceiptPayloadOnPurchase_18(bool value)
	{
		___m_FetchReceiptPayloadOnPurchase_18 = value;
	}

	inline static int32_t get_offset_of_m_PurchaseInProgress_19() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_PurchaseInProgress_19)); }
	inline bool get_m_PurchaseInProgress_19() const { return ___m_PurchaseInProgress_19; }
	inline bool* get_address_of_m_PurchaseInProgress_19() { return &___m_PurchaseInProgress_19; }
	inline void set_m_PurchaseInProgress_19(bool value)
	{
		___m_PurchaseInProgress_19 = value;
	}

	inline static int32_t get_offset_of_m_ProductUIs_20() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___m_ProductUIs_20)); }
	inline Dictionary_2_t708210053 * get_m_ProductUIs_20() const { return ___m_ProductUIs_20; }
	inline Dictionary_2_t708210053 ** get_address_of_m_ProductUIs_20() { return &___m_ProductUIs_20; }
	inline void set_m_ProductUIs_20(Dictionary_2_t708210053 * value)
	{
		___m_ProductUIs_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductUIs_20), value);
	}

	inline static int32_t get_offset_of_productUITemplate_21() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___productUITemplate_21)); }
	inline GameObject_t1113636619 * get_productUITemplate_21() const { return ___productUITemplate_21; }
	inline GameObject_t1113636619 ** get_address_of_productUITemplate_21() { return &___productUITemplate_21; }
	inline void set_productUITemplate_21(GameObject_t1113636619 * value)
	{
		___productUITemplate_21 = value;
		Il2CppCodeGenWriteBarrier((&___productUITemplate_21), value);
	}

	inline static int32_t get_offset_of_contentRect_22() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___contentRect_22)); }
	inline RectTransform_t3704657025 * get_contentRect_22() const { return ___contentRect_22; }
	inline RectTransform_t3704657025 ** get_address_of_contentRect_22() { return &___contentRect_22; }
	inline void set_contentRect_22(RectTransform_t3704657025 * value)
	{
		___contentRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___contentRect_22), value);
	}

	inline static int32_t get_offset_of_restoreButton_23() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___restoreButton_23)); }
	inline Button_t4055032469 * get_restoreButton_23() const { return ___restoreButton_23; }
	inline Button_t4055032469 ** get_address_of_restoreButton_23() { return &___restoreButton_23; }
	inline void set_restoreButton_23(Button_t4055032469 * value)
	{
		___restoreButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___restoreButton_23), value);
	}

	inline static int32_t get_offset_of_loginButton_24() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___loginButton_24)); }
	inline Button_t4055032469 * get_loginButton_24() const { return ___loginButton_24; }
	inline Button_t4055032469 ** get_address_of_loginButton_24() { return &___loginButton_24; }
	inline void set_loginButton_24(Button_t4055032469 * value)
	{
		___loginButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___loginButton_24), value);
	}

	inline static int32_t get_offset_of_validateButton_25() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___validateButton_25)); }
	inline Button_t4055032469 * get_validateButton_25() const { return ___validateButton_25; }
	inline Button_t4055032469 ** get_address_of_validateButton_25() { return &___validateButton_25; }
	inline void set_validateButton_25(Button_t4055032469 * value)
	{
		___validateButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___validateButton_25), value);
	}

	inline static int32_t get_offset_of_versionText_26() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565, ___versionText_26)); }
	inline Text_t1901882714 * get_versionText_26() const { return ___versionText_26; }
	inline Text_t1901882714 ** get_address_of_versionText_26() { return &___versionText_26; }
	inline void set_versionText_26(Text_t1901882714 * value)
	{
		___versionText_26 = value;
		Il2CppCodeGenWriteBarrier((&___versionText_26), value);
	}
};

struct IAPDemo_t3681080565_StaticFields
{
public:
	// System.Action`1<System.String> IAPDemo::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_27() { return static_cast<int32_t>(offsetof(IAPDemo_t3681080565_StaticFields, ___U3CU3Ef__amU24cache0_27)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_27() const { return ___U3CU3Ef__amU24cache0_27; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_27() { return &___U3CU3Ef__amU24cache0_27; }
	inline void set_U3CU3Ef__amU24cache0_27(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPDEMO_T3681080565_H
#ifndef IAPDEMOPRODUCTUI_T922953754_H
#define IAPDEMOPRODUCTUI_T922953754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemoProductUI
struct  IAPDemoProductUI_t922953754  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button IAPDemoProductUI::purchaseButton
	Button_t4055032469 * ___purchaseButton_4;
	// UnityEngine.UI.Button IAPDemoProductUI::receiptButton
	Button_t4055032469 * ___receiptButton_5;
	// UnityEngine.UI.Text IAPDemoProductUI::titleText
	Text_t1901882714 * ___titleText_6;
	// UnityEngine.UI.Text IAPDemoProductUI::descriptionText
	Text_t1901882714 * ___descriptionText_7;
	// UnityEngine.UI.Text IAPDemoProductUI::priceText
	Text_t1901882714 * ___priceText_8;
	// UnityEngine.UI.Text IAPDemoProductUI::statusText
	Text_t1901882714 * ___statusText_9;
	// System.String IAPDemoProductUI::m_ProductID
	String_t* ___m_ProductID_10;
	// System.Action`1<System.String> IAPDemoProductUI::m_PurchaseCallback
	Action_1_t2019918284 * ___m_PurchaseCallback_11;
	// System.String IAPDemoProductUI::m_Receipt
	String_t* ___m_Receipt_12;

public:
	inline static int32_t get_offset_of_purchaseButton_4() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___purchaseButton_4)); }
	inline Button_t4055032469 * get_purchaseButton_4() const { return ___purchaseButton_4; }
	inline Button_t4055032469 ** get_address_of_purchaseButton_4() { return &___purchaseButton_4; }
	inline void set_purchaseButton_4(Button_t4055032469 * value)
	{
		___purchaseButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseButton_4), value);
	}

	inline static int32_t get_offset_of_receiptButton_5() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___receiptButton_5)); }
	inline Button_t4055032469 * get_receiptButton_5() const { return ___receiptButton_5; }
	inline Button_t4055032469 ** get_address_of_receiptButton_5() { return &___receiptButton_5; }
	inline void set_receiptButton_5(Button_t4055032469 * value)
	{
		___receiptButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___receiptButton_5), value);
	}

	inline static int32_t get_offset_of_titleText_6() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___titleText_6)); }
	inline Text_t1901882714 * get_titleText_6() const { return ___titleText_6; }
	inline Text_t1901882714 ** get_address_of_titleText_6() { return &___titleText_6; }
	inline void set_titleText_6(Text_t1901882714 * value)
	{
		___titleText_6 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_6), value);
	}

	inline static int32_t get_offset_of_descriptionText_7() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___descriptionText_7)); }
	inline Text_t1901882714 * get_descriptionText_7() const { return ___descriptionText_7; }
	inline Text_t1901882714 ** get_address_of_descriptionText_7() { return &___descriptionText_7; }
	inline void set_descriptionText_7(Text_t1901882714 * value)
	{
		___descriptionText_7 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_7), value);
	}

	inline static int32_t get_offset_of_priceText_8() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___priceText_8)); }
	inline Text_t1901882714 * get_priceText_8() const { return ___priceText_8; }
	inline Text_t1901882714 ** get_address_of_priceText_8() { return &___priceText_8; }
	inline void set_priceText_8(Text_t1901882714 * value)
	{
		___priceText_8 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_8), value);
	}

	inline static int32_t get_offset_of_statusText_9() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___statusText_9)); }
	inline Text_t1901882714 * get_statusText_9() const { return ___statusText_9; }
	inline Text_t1901882714 ** get_address_of_statusText_9() { return &___statusText_9; }
	inline void set_statusText_9(Text_t1901882714 * value)
	{
		___statusText_9 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_9), value);
	}

	inline static int32_t get_offset_of_m_ProductID_10() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___m_ProductID_10)); }
	inline String_t* get_m_ProductID_10() const { return ___m_ProductID_10; }
	inline String_t** get_address_of_m_ProductID_10() { return &___m_ProductID_10; }
	inline void set_m_ProductID_10(String_t* value)
	{
		___m_ProductID_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductID_10), value);
	}

	inline static int32_t get_offset_of_m_PurchaseCallback_11() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___m_PurchaseCallback_11)); }
	inline Action_1_t2019918284 * get_m_PurchaseCallback_11() const { return ___m_PurchaseCallback_11; }
	inline Action_1_t2019918284 ** get_address_of_m_PurchaseCallback_11() { return &___m_PurchaseCallback_11; }
	inline void set_m_PurchaseCallback_11(Action_1_t2019918284 * value)
	{
		___m_PurchaseCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseCallback_11), value);
	}

	inline static int32_t get_offset_of_m_Receipt_12() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t922953754, ___m_Receipt_12)); }
	inline String_t* get_m_Receipt_12() const { return ___m_Receipt_12; }
	inline String_t** get_address_of_m_Receipt_12() { return &___m_Receipt_12; }
	inline void set_m_Receipt_12(String_t* value)
	{
		___m_Receipt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Receipt_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPDEMOPRODUCTUI_T922953754_H
#ifndef DEMOINVENTORY_T843047770_H
#define DEMOINVENTORY_T843047770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.DemoInventory
struct  DemoInventory_t843047770  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOINVENTORY_T843047770_H
#ifndef IAPBUTTON_T2348892617_H
#define IAPBUTTON_T2348892617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton
struct  IAPButton_t2348892617  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Purchasing.IAPButton::productId
	String_t* ___productId_4;
	// UnityEngine.Purchasing.IAPButton/ButtonType UnityEngine.Purchasing.IAPButton::buttonType
	int32_t ___buttonType_5;
	// System.Boolean UnityEngine.Purchasing.IAPButton::consumePurchase
	bool ___consumePurchase_6;
	// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPButton::onPurchaseComplete
	OnPurchaseCompletedEvent_t3721407765 * ___onPurchaseComplete_7;
	// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent UnityEngine.Purchasing.IAPButton::onPurchaseFailed
	OnPurchaseFailedEvent_t1729542224 * ___onPurchaseFailed_8;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::titleText
	Text_t1901882714 * ___titleText_9;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::descriptionText
	Text_t1901882714 * ___descriptionText_10;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::priceText
	Text_t1901882714 * ___priceText_11;

public:
	inline static int32_t get_offset_of_productId_4() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___productId_4)); }
	inline String_t* get_productId_4() const { return ___productId_4; }
	inline String_t** get_address_of_productId_4() { return &___productId_4; }
	inline void set_productId_4(String_t* value)
	{
		___productId_4 = value;
		Il2CppCodeGenWriteBarrier((&___productId_4), value);
	}

	inline static int32_t get_offset_of_buttonType_5() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___buttonType_5)); }
	inline int32_t get_buttonType_5() const { return ___buttonType_5; }
	inline int32_t* get_address_of_buttonType_5() { return &___buttonType_5; }
	inline void set_buttonType_5(int32_t value)
	{
		___buttonType_5 = value;
	}

	inline static int32_t get_offset_of_consumePurchase_6() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___consumePurchase_6)); }
	inline bool get_consumePurchase_6() const { return ___consumePurchase_6; }
	inline bool* get_address_of_consumePurchase_6() { return &___consumePurchase_6; }
	inline void set_consumePurchase_6(bool value)
	{
		___consumePurchase_6 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_7() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___onPurchaseComplete_7)); }
	inline OnPurchaseCompletedEvent_t3721407765 * get_onPurchaseComplete_7() const { return ___onPurchaseComplete_7; }
	inline OnPurchaseCompletedEvent_t3721407765 ** get_address_of_onPurchaseComplete_7() { return &___onPurchaseComplete_7; }
	inline void set_onPurchaseComplete_7(OnPurchaseCompletedEvent_t3721407765 * value)
	{
		___onPurchaseComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseComplete_7), value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_8() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___onPurchaseFailed_8)); }
	inline OnPurchaseFailedEvent_t1729542224 * get_onPurchaseFailed_8() const { return ___onPurchaseFailed_8; }
	inline OnPurchaseFailedEvent_t1729542224 ** get_address_of_onPurchaseFailed_8() { return &___onPurchaseFailed_8; }
	inline void set_onPurchaseFailed_8(OnPurchaseFailedEvent_t1729542224 * value)
	{
		___onPurchaseFailed_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseFailed_8), value);
	}

	inline static int32_t get_offset_of_titleText_9() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___titleText_9)); }
	inline Text_t1901882714 * get_titleText_9() const { return ___titleText_9; }
	inline Text_t1901882714 ** get_address_of_titleText_9() { return &___titleText_9; }
	inline void set_titleText_9(Text_t1901882714 * value)
	{
		___titleText_9 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_9), value);
	}

	inline static int32_t get_offset_of_descriptionText_10() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___descriptionText_10)); }
	inline Text_t1901882714 * get_descriptionText_10() const { return ___descriptionText_10; }
	inline Text_t1901882714 ** get_address_of_descriptionText_10() { return &___descriptionText_10; }
	inline void set_descriptionText_10(Text_t1901882714 * value)
	{
		___descriptionText_10 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_10), value);
	}

	inline static int32_t get_offset_of_priceText_11() { return static_cast<int32_t>(offsetof(IAPButton_t2348892617, ___priceText_11)); }
	inline Text_t1901882714 * get_priceText_11() const { return ___priceText_11; }
	inline Text_t1901882714 ** get_address_of_priceText_11() { return &___priceText_11; }
	inline void set_priceText_11(Text_t1901882714 * value)
	{
		___priceText_11 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPBUTTON_T2348892617_H
#ifndef IAPLISTENER_T2001792988_H
#define IAPLISTENER_T2001792988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener
struct  IAPListener_t2001792988  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.Purchasing.IAPListener::consumePurchase
	bool ___consumePurchase_4;
	// System.Boolean UnityEngine.Purchasing.IAPListener::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_5;
	// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPListener::onPurchaseComplete
	OnPurchaseCompletedEvent_t1675809258 * ___onPurchaseComplete_6;
	// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent UnityEngine.Purchasing.IAPListener::onPurchaseFailed
	OnPurchaseFailedEvent_t800864861 * ___onPurchaseFailed_7;

public:
	inline static int32_t get_offset_of_consumePurchase_4() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___consumePurchase_4)); }
	inline bool get_consumePurchase_4() const { return ___consumePurchase_4; }
	inline bool* get_address_of_consumePurchase_4() { return &___consumePurchase_4; }
	inline void set_consumePurchase_4(bool value)
	{
		___consumePurchase_4 = value;
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_5() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___dontDestroyOnLoad_5)); }
	inline bool get_dontDestroyOnLoad_5() const { return ___dontDestroyOnLoad_5; }
	inline bool* get_address_of_dontDestroyOnLoad_5() { return &___dontDestroyOnLoad_5; }
	inline void set_dontDestroyOnLoad_5(bool value)
	{
		___dontDestroyOnLoad_5 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_6() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___onPurchaseComplete_6)); }
	inline OnPurchaseCompletedEvent_t1675809258 * get_onPurchaseComplete_6() const { return ___onPurchaseComplete_6; }
	inline OnPurchaseCompletedEvent_t1675809258 ** get_address_of_onPurchaseComplete_6() { return &___onPurchaseComplete_6; }
	inline void set_onPurchaseComplete_6(OnPurchaseCompletedEvent_t1675809258 * value)
	{
		___onPurchaseComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseComplete_6), value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_7() { return static_cast<int32_t>(offsetof(IAPListener_t2001792988, ___onPurchaseFailed_7)); }
	inline OnPurchaseFailedEvent_t800864861 * get_onPurchaseFailed_7() const { return ___onPurchaseFailed_7; }
	inline OnPurchaseFailedEvent_t800864861 ** get_address_of_onPurchaseFailed_7() { return &___onPurchaseFailed_7; }
	inline void set_onPurchaseFailed_7(OnPurchaseFailedEvent_t800864861 * value)
	{
		___onPurchaseFailed_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseFailed_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPLISTENER_T2001792988_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4600[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4601[5] = 
{
	TriggerType_t105272677::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4602[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4603[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4607[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (U3CModuleU3E_t692745583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (Vector2IntFormatter_t3358610488), -1, sizeof(Vector2IntFormatter_t3358610488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4609[1] = 
{
	Vector2IntFormatter_t3358610488_StaticFields::get_offset_of_Serializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (Vector3IntFormatter_t3358578805), -1, sizeof(Vector3IntFormatter_t3358578805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4610[1] = 
{
	Vector3IntFormatter_t3358578805_StaticFields::get_offset_of_Serializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (AppStoreSetting_t1592337179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4611[3] = 
{
	AppStoreSetting_t1592337179::get_offset_of_AppID_0(),
	AppStoreSetting_t1592337179::get_offset_of_AppKey_1(),
	AppStoreSetting_t1592337179::get_offset_of_IsTestMode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (AppStoreSettings_t2325796953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4612[4] = 
{
	AppStoreSettings_t2325796953::get_offset_of_UnityClientID_4(),
	AppStoreSettings_t2325796953::get_offset_of_UnityClientKey_5(),
	AppStoreSettings_t2325796953::get_offset_of_UnityClientRSAPublicKey_6(),
	AppStoreSettings_t2325796953::get_offset_of_XiaomiAppStoreSetting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (AppleTangle_t2948648219), -1, sizeof(AppleTangle_t2948648219_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4613[4] = 
{
	AppleTangle_t2948648219_StaticFields::get_offset_of_data_0(),
	AppleTangle_t2948648219_StaticFields::get_offset_of_order_1(),
	AppleTangle_t2948648219_StaticFields::get_offset_of_key_2(),
	AppleTangle_t2948648219_StaticFields::get_offset_of_IsPopulated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (GooglePlayTangle_t2803690650), -1, sizeof(GooglePlayTangle_t2803690650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4614[4] = 
{
	GooglePlayTangle_t2803690650_StaticFields::get_offset_of_data_0(),
	GooglePlayTangle_t2803690650_StaticFields::get_offset_of_order_1(),
	GooglePlayTangle_t2803690650_StaticFields::get_offset_of_key_2(),
	GooglePlayTangle_t2803690650_StaticFields::get_offset_of_IsPopulated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (UnityChannelTangle_t4110017680), -1, sizeof(UnityChannelTangle_t4110017680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4615[4] = 
{
	UnityChannelTangle_t4110017680_StaticFields::get_offset_of_data_0(),
	UnityChannelTangle_t4110017680_StaticFields::get_offset_of_order_1(),
	UnityChannelTangle_t4110017680_StaticFields::get_offset_of_key_2(),
	UnityChannelTangle_t4110017680_StaticFields::get_offset_of_IsPopulated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (CodelessIAPStoreListener_t3553337218), -1, sizeof(CodelessIAPStoreListener_t3553337218_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4616[8] = 
{
	CodelessIAPStoreListener_t3553337218_StaticFields::get_offset_of_instance_0(),
	CodelessIAPStoreListener_t3553337218::get_offset_of_activeButtons_1(),
	CodelessIAPStoreListener_t3553337218::get_offset_of_activeListeners_2(),
	CodelessIAPStoreListener_t3553337218_StaticFields::get_offset_of_unityPurchasingInitialized_3(),
	CodelessIAPStoreListener_t3553337218::get_offset_of_controller_4(),
	CodelessIAPStoreListener_t3553337218::get_offset_of_extensions_5(),
	CodelessIAPStoreListener_t3553337218::get_offset_of_catalog_6(),
	CodelessIAPStoreListener_t3553337218_StaticFields::get_offset_of_initializationComplete_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (DemoInventory_t843047770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (IAPButton_t2348892617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4618[8] = 
{
	IAPButton_t2348892617::get_offset_of_productId_4(),
	IAPButton_t2348892617::get_offset_of_buttonType_5(),
	IAPButton_t2348892617::get_offset_of_consumePurchase_6(),
	IAPButton_t2348892617::get_offset_of_onPurchaseComplete_7(),
	IAPButton_t2348892617::get_offset_of_onPurchaseFailed_8(),
	IAPButton_t2348892617::get_offset_of_titleText_9(),
	IAPButton_t2348892617::get_offset_of_descriptionText_10(),
	IAPButton_t2348892617::get_offset_of_priceText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (ButtonType_t908070482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4619[3] = 
{
	ButtonType_t908070482::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (OnPurchaseCompletedEvent_t3721407765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (OnPurchaseFailedEvent_t1729542224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (IAPConfigurationHelper_t2483224394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (IAPDemo_t3681080565), -1, sizeof(IAPDemo_t3681080565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4623[24] = 
{
	IAPDemo_t3681080565::get_offset_of_m_Controller_4(),
	IAPDemo_t3681080565::get_offset_of_m_AppleExtensions_5(),
	IAPDemo_t3681080565::get_offset_of_m_MoolahExtensions_6(),
	IAPDemo_t3681080565::get_offset_of_m_SamsungExtensions_7(),
	IAPDemo_t3681080565::get_offset_of_m_MicrosoftExtensions_8(),
	IAPDemo_t3681080565::get_offset_of_m_UnityChannelExtensions_9(),
	IAPDemo_t3681080565::get_offset_of_m_TransactionHistoryExtensions_10(),
	IAPDemo_t3681080565::get_offset_of_m_IsGooglePlayStoreSelected_11(),
	IAPDemo_t3681080565::get_offset_of_m_IsSamsungAppsStoreSelected_12(),
	IAPDemo_t3681080565::get_offset_of_m_IsCloudMoolahStoreSelected_13(),
	IAPDemo_t3681080565::get_offset_of_m_IsUnityChannelSelected_14(),
	IAPDemo_t3681080565::get_offset_of_m_LastTransactionID_15(),
	IAPDemo_t3681080565::get_offset_of_m_IsLoggedIn_16(),
	IAPDemo_t3681080565::get_offset_of_unityChannelLoginHandler_17(),
	IAPDemo_t3681080565::get_offset_of_m_FetchReceiptPayloadOnPurchase_18(),
	IAPDemo_t3681080565::get_offset_of_m_PurchaseInProgress_19(),
	IAPDemo_t3681080565::get_offset_of_m_ProductUIs_20(),
	IAPDemo_t3681080565::get_offset_of_productUITemplate_21(),
	IAPDemo_t3681080565::get_offset_of_contentRect_22(),
	IAPDemo_t3681080565::get_offset_of_restoreButton_23(),
	IAPDemo_t3681080565::get_offset_of_loginButton_24(),
	IAPDemo_t3681080565::get_offset_of_validateButton_25(),
	IAPDemo_t3681080565::get_offset_of_versionText_26(),
	IAPDemo_t3681080565_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (UnityChannelPurchaseError_t2306817818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4624[2] = 
{
	UnityChannelPurchaseError_t2306817818::get_offset_of_error_0(),
	UnityChannelPurchaseError_t2306817818::get_offset_of_purchaseInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (UnityChannelPurchaseInfo_t74063925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4625[3] = 
{
	UnityChannelPurchaseInfo_t74063925::get_offset_of_productCode_0(),
	UnityChannelPurchaseInfo_t74063925::get_offset_of_gameOrderId_1(),
	UnityChannelPurchaseInfo_t74063925::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (UnityChannelLoginHandler_t2949829254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[4] = 
{
	UnityChannelLoginHandler_t2949829254::get_offset_of_initializeSucceededAction_0(),
	UnityChannelLoginHandler_t2949829254::get_offset_of_initializeFailedAction_1(),
	UnityChannelLoginHandler_t2949829254::get_offset_of_loginSucceededAction_2(),
	UnityChannelLoginHandler_t2949829254::get_offset_of_loginFailedAction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t2364586269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4627[3] = 
{
	U3CAwakeU3Ec__AnonStorey0_t2364586269::get_offset_of_builder_0(),
	U3CAwakeU3Ec__AnonStorey0_t2364586269::get_offset_of_initializeUnityIap_1(),
	U3CAwakeU3Ec__AnonStorey0_t2364586269::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (U3CValidateButtonClickU3Ec__AnonStorey1_t541528072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4628[1] = 
{
	U3CValidateButtonClickU3Ec__AnonStorey1_t541528072::get_offset_of_txId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (IAPDemoProductUI_t922953754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4629[9] = 
{
	IAPDemoProductUI_t922953754::get_offset_of_purchaseButton_4(),
	IAPDemoProductUI_t922953754::get_offset_of_receiptButton_5(),
	IAPDemoProductUI_t922953754::get_offset_of_titleText_6(),
	IAPDemoProductUI_t922953754::get_offset_of_descriptionText_7(),
	IAPDemoProductUI_t922953754::get_offset_of_priceText_8(),
	IAPDemoProductUI_t922953754::get_offset_of_statusText_9(),
	IAPDemoProductUI_t922953754::get_offset_of_m_ProductID_10(),
	IAPDemoProductUI_t922953754::get_offset_of_m_PurchaseCallback_11(),
	IAPDemoProductUI_t922953754::get_offset_of_m_Receipt_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (IAPListener_t2001792988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4630[4] = 
{
	IAPListener_t2001792988::get_offset_of_consumePurchase_4(),
	IAPListener_t2001792988::get_offset_of_dontDestroyOnLoad_5(),
	IAPListener_t2001792988::get_offset_of_onPurchaseComplete_6(),
	IAPListener_t2001792988::get_offset_of_onPurchaseFailed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (OnPurchaseCompletedEvent_t1675809258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (OnPurchaseFailedEvent_t800864861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255375), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4633[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U24fieldU2D51B34EEE9AD5E1B8585F43DADDF34A72E33E6CFF_0(),
	U3CPrivateImplementationDetailsU3E_t3057255375_StaticFields::get_offset_of_U24fieldU2DE769DE2910E6162FDCE1872402BC4A2AC2F7313B_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (U24ArrayTypeU3D244_t4214066944)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D244_t4214066944 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (U24ArrayTypeU3D60_t4028431473)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D60_t4028431473 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (U3CModuleU3E_t692745584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (AdsSection_t4249879119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4637[6] = 
{
	AdsSection_t4249879119::get_offset_of_defaultRoot_0(),
	AdsSection_t4249879119::get_offset_of_customRoot_1(),
	AdsSection_t4249879119::get_offset_of_enableDefaultSectionButton_2(),
	AdsSection_t4249879119::get_offset_of_enableCustomSectionButton_3(),
	AdsSection_t4249879119::get_offset_of_useDefaultUIAsDefault_4(),
	AdsSection_t4249879119::get_offset_of_U3CIsUsingDefaultSectionU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (AdvertisingDemo_t3945797904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4638[11] = 
{
	AdvertisingDemo_t3945797904::get_offset_of_demoUtils_4(),
	AdvertisingDemo_t3945797904::get_offset_of_curtain_5(),
	AdvertisingDemo_t3945797904::get_offset_of_isAdRemovedInfo_6(),
	AdvertisingDemo_t3945797904::get_offset_of_defaultBannerNetworkText_7(),
	AdvertisingDemo_t3945797904::get_offset_of_defaultInterstitialNetworkText_8(),
	AdvertisingDemo_t3945797904::get_offset_of_defaultRewardedNetworkText_9(),
	AdvertisingDemo_t3945797904::get_offset_of_autoLoadModeDropdown_10(),
	AdvertisingDemo_t3945797904::get_offset_of_bannerUI_11(),
	AdvertisingDemo_t3945797904::get_offset_of_interstitialUI_12(),
	AdvertisingDemo_t3945797904::get_offset_of_rewardedVideoUI_13(),
	AdvertisingDemo_t3945797904::get_offset_of_lastAutoLoadMode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (U3COnInterstitialAdCompletedU3Ec__AnonStorey1_t1182564281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[2] = 
{
	U3COnInterstitialAdCompletedU3Ec__AnonStorey1_t1182564281::get_offset_of_network_0(),
	U3COnInterstitialAdCompletedU3Ec__AnonStorey1_t1182564281::get_offset_of_placement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (U3COnRewardedAdCompletedU3Ec__AnonStorey2_t785433302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4640[2] = 
{
	U3COnRewardedAdCompletedU3Ec__AnonStorey2_t785433302::get_offset_of_network_0(),
	U3COnRewardedAdCompletedU3Ec__AnonStorey2_t785433302::get_offset_of_placement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (U3COnRewardedAdSkippedU3Ec__AnonStorey3_t625254138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4641[2] = 
{
	U3COnRewardedAdSkippedU3Ec__AnonStorey3_t625254138::get_offset_of_network_0(),
	U3COnRewardedAdSkippedU3Ec__AnonStorey3_t625254138::get_offset_of_placement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (U3CDelayCoroutineU3Ec__Iterator0_t2514348216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4642[5] = 
{
	U3CDelayCoroutineU3Ec__Iterator0_t2514348216::get_offset_of_time_0(),
	U3CDelayCoroutineU3Ec__Iterator0_t2514348216::get_offset_of_action_1(),
	U3CDelayCoroutineU3Ec__Iterator0_t2514348216::get_offset_of_U24current_2(),
	U3CDelayCoroutineU3Ec__Iterator0_t2514348216::get_offset_of_U24disposing_3(),
	U3CDelayCoroutineU3Ec__Iterator0_t2514348216::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (BannerSection_t2412952700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4643[2] = 
{
	BannerSection_t2412952700::get_offset_of_defaultBannerUI_6(),
	BannerSection_t2412952700::get_offset_of_customBannerUI_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (DefaulBannerUI_t2752449824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4644[7] = 
{
	DefaulBannerUI_t2752449824::get_offset_of_showBannerButton_0(),
	DefaulBannerUI_t2752449824::get_offset_of_hideBannerButton_1(),
	DefaulBannerUI_t2752449824::get_offset_of_destroyBannerButton_2(),
	DefaulBannerUI_t2752449824::get_offset_of_bannerPositionSelector_3(),
	DefaulBannerUI_t2752449824::get_offset_of_bannerSizeSelector_4(),
	DefaulBannerUI_t2752449824::get_offset_of_allBannerSizes_5(),
	DefaulBannerUI_t2752449824::get_offset_of_allBannerPositions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (CustomBannerUI_t529944588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4645[3] = 
{
	CustomBannerUI_t529944588::get_offset_of_networkSelector_7(),
	CustomBannerUI_t529944588::get_offset_of_customKeyInputField_8(),
	CustomBannerUI_t529944588::get_offset_of_allBannerNetworks_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (InterstitialSection_t4034589797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (DefaultInterstitialUI_t2954553843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (CustomInterstitialUI_t2716953283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4648[1] = 
{
	CustomInterstitialUI_t2716953283::get_offset_of_allInterstitialNetworks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4649[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4650[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (RewardedVideoSection_t2849218130), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (DefaultRewardedVideolUI_t2208290187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (CustomRewardedVideoUI_t2933782405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4654[1] = 
{
	CustomRewardedVideoUI_t2933782405::get_offset_of_allRewardedNetworks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (ColorChooser_t386109851), -1, sizeof(ColorChooser_t386109851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4655[3] = 
{
	ColorChooser_t386109851_StaticFields::get_offset_of_colorSelected_4(),
	ColorChooser_t386109851::get_offset_of_imgComp_5(),
	ColorChooser_t386109851::get_offset_of_btnComp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (ColorHandler_t4238062851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4656[5] = 
{
	ColorHandler_t4238062851::get_offset_of_lerpTime_4(),
	ColorHandler_t4238062851::get_offset_of_colors_5(),
	ColorHandler_t4238062851::get_offset_of_imgComp_6(),
	ColorHandler_t4238062851::get_offset_of_material_7(),
	ColorHandler_t4238062851::get_offset_of_currentColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (U3CCRChangeColorU3Ec__Iterator0_t2584720601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4657[8] = 
{
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U3CnewColorU3E__1_0(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U3CelapsedU3E__2_1(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_time_2(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U3CcU3E__3_3(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U24this_4(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U24current_5(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U24disposing_6(),
	U3CCRChangeColorU3Ec__Iterator0_t2584720601::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (DemoHomeController_t3360241642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4658[1] = 
{
	DemoHomeController_t3360241642::get_offset_of_installationTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (DemoUtils_t1360065201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4659[2] = 
{
	DemoUtils_t1360065201::get_offset_of_checkedSprite_4(),
	DemoUtils_t1360065201::get_offset_of_uncheckedSprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (DigitalClock_t1260851316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4660[1] = 
{
	DigitalClock_t1260851316::get_offset_of_clockText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (GameServicesDemo_t3276791170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4661[11] = 
{
	GameServicesDemo_t3276791170::get_offset_of_curtain_4(),
	GameServicesDemo_t3276791170::get_offset_of_isAutoInitInfo_5(),
	GameServicesDemo_t3276791170::get_offset_of_isInitializedInfo_6(),
	GameServicesDemo_t3276791170::get_offset_of_selectedAchievementInfo_7(),
	GameServicesDemo_t3276791170::get_offset_of_selectedLeaderboardInfo_8(),
	GameServicesDemo_t3276791170::get_offset_of_scoreInput_9(),
	GameServicesDemo_t3276791170::get_offset_of_demoUtils_10(),
	GameServicesDemo_t3276791170::get_offset_of_scrollableListPrefab_11(),
	GameServicesDemo_t3276791170::get_offset_of_selectedAchievement_12(),
	GameServicesDemo_t3276791170::get_offset_of_selectedLeaderboard_13(),
	GameServicesDemo_t3276791170::get_offset_of_lastLoginState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (GameServicesDemo_SavedGames_t941468990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4662[12] = 
{
	GameServicesDemo_SavedGames_t941468990::get_offset_of_demoUtils_4(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_scrollableListPrefab_5(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_isSavedGamesEnabledInfo_6(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_isSavedGameSelectedInfo_7(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_isSavedGameOpenedInfo_8(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_autoConflictResolutionInfo_9(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_savedGameNameInput_10(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_savedGameDataInput_11(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_resolveConflictsManually_12(),
	0,
	GameServicesDemo_SavedGames_t941468990::get_offset_of_selectedSavedGame_14(),
	GameServicesDemo_SavedGames_t941468990::get_offset_of_allSavedGames_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (DemoSavedGameData_t64080094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4663[2] = 
{
	DemoSavedGameData_t64080094::get_offset_of_demoInt_0(),
	DemoSavedGameData_t64080094::get_offset_of_largeData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (U3CWriteSavedGameU3Ec__AnonStorey0_t1973406844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4664[2] = 
{
	U3CWriteSavedGameU3Ec__AnonStorey0_t1973406844::get_offset_of_data_0(),
	U3CWriteSavedGameU3Ec__AnonStorey0_t1973406844::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (GifDemo_t1006419778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4665[21] = 
{
	GifDemo_t1006419778::get_offset_of_recorder_4(),
	GifDemo_t1006419778::get_offset_of_gifFilename_5(),
	GifDemo_t1006419778::get_offset_of_loop_6(),
	GifDemo_t1006419778::get_offset_of_quality_7(),
	GifDemo_t1006419778::get_offset_of_exportThreadPriority_8(),
	GifDemo_t1006419778::get_offset_of_giphyUsername_9(),
	GifDemo_t1006419778::get_offset_of_giphyApiKey_10(),
	GifDemo_t1006419778::get_offset_of_recordingMark_11(),
	GifDemo_t1006419778::get_offset_of_startRecordingButton_12(),
	GifDemo_t1006419778::get_offset_of_stopRecordingButton_13(),
	GifDemo_t1006419778::get_offset_of_playbackPanel_14(),
	GifDemo_t1006419778::get_offset_of_clipPlayer_15(),
	GifDemo_t1006419778::get_offset_of_giphyLogo_16(),
	GifDemo_t1006419778::get_offset_of_activityProgress_17(),
	GifDemo_t1006419778::get_offset_of_activityText_18(),
	GifDemo_t1006419778::get_offset_of_progressText_19(),
	GifDemo_t1006419778::get_offset_of_recordedClip_20(),
	GifDemo_t1006419778::get_offset_of_isExportingGif_21(),
	GifDemo_t1006419778::get_offset_of_isUploadingGif_22(),
	GifDemo_t1006419778::get_offset_of_exportedGifPath_23(),
	GifDemo_t1006419778::get_offset_of_uploadedGifUrl_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (InAppPurchasingDemo_t2157663715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4666[10] = 
{
	InAppPurchasingDemo_t2157663715::get_offset_of_logProductLocalizedData_4(),
	InAppPurchasingDemo_t2157663715::get_offset_of_curtain_5(),
	InAppPurchasingDemo_t2157663715::get_offset_of_scrollableListPrefab_6(),
	InAppPurchasingDemo_t2157663715::get_offset_of_isInitInfo_7(),
	InAppPurchasingDemo_t2157663715::get_offset_of_ownedProductsInfo_8(),
	InAppPurchasingDemo_t2157663715::get_offset_of_selectedProductInfo_9(),
	InAppPurchasingDemo_t2157663715::get_offset_of_receiptViewer_10(),
	InAppPurchasingDemo_t2157663715::get_offset_of_demoUtils_11(),
	InAppPurchasingDemo_t2157663715::get_offset_of_selectedProduct_12(),
	InAppPurchasingDemo_t2157663715::get_offset_of_ownedProducts_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4667[3] = 
{
	U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367::get_offset_of_U24current_0(),
	U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367::get_offset_of_U24disposing_1(),
	U3CCROnRestoreCompletedU3Ec__Iterator0_t2617974367::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4668[5] = 
{
	U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020::get_offset_of_U3CproductsU3E__0_0(),
	U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020::get_offset_of_U24this_1(),
	U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020::get_offset_of_U24current_2(),
	U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020::get_offset_of_U24disposing_3(),
	U3CCheckOwnedProductsU3Ec__Iterator1_t1601353020::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (NativeUIDemo_t2713937398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4669[4] = 
{
	NativeUIDemo_t2713937398::get_offset_of_isFirstButtonBool_4(),
	NativeUIDemo_t2713937398::get_offset_of_isSecondButtonBool_5(),
	NativeUIDemo_t2713937398::get_offset_of_isThirdButtonBool_6(),
	NativeUIDemo_t2713937398::get_offset_of_demoUtils_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (NotificationHandler_t1136578581), -1, sizeof(NotificationHandler_t1136578581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4670[1] = 
{
	NotificationHandler_t1136578581_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431), -1, sizeof(U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4671[7] = 
{
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431::get_offset_of_hasNewUpdate_0(),
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431::get_offset_of_title_1(),
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431::get_offset_of_message_2(),
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431::get_offset_of_U24current_3(),
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431::get_offset_of_U24disposing_4(),
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431::get_offset_of_U24PC_5(),
	U3CCRWaitAndShowPopupU3Ec__Iterator0_t2222973431_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (NotificationsDemo_t701924017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4672[24] = 
{
	NotificationsDemo_t701924017::get_offset_of_title_4(),
	NotificationsDemo_t701924017::get_offset_of_subtitle_5(),
	NotificationsDemo_t701924017::get_offset_of_message_6(),
	NotificationsDemo_t701924017::get_offset_of_categoryId_7(),
	NotificationsDemo_t701924017::get_offset_of_fakeNewUpdate_8(),
	NotificationsDemo_t701924017::get_offset_of_delayHours_9(),
	NotificationsDemo_t701924017::get_offset_of_delayMinutes_10(),
	NotificationsDemo_t701924017::get_offset_of_delaySeconds_11(),
	NotificationsDemo_t701924017::get_offset_of_repeatTitle_12(),
	NotificationsDemo_t701924017::get_offset_of_repeatSubtitle_13(),
	NotificationsDemo_t701924017::get_offset_of_repeatMessage_14(),
	NotificationsDemo_t701924017::get_offset_of_repeatCategoryId_15(),
	NotificationsDemo_t701924017::get_offset_of_repeatDelayHours_16(),
	NotificationsDemo_t701924017::get_offset_of_repeatDelayMinutes_17(),
	NotificationsDemo_t701924017::get_offset_of_repeatDelaySeconds_18(),
	NotificationsDemo_t701924017::get_offset_of_repeatType_19(),
	NotificationsDemo_t701924017::get_offset_of_curtain_20(),
	NotificationsDemo_t701924017::get_offset_of_pushNotifService_21(),
	NotificationsDemo_t701924017::get_offset_of_isAutoInitInfo_22(),
	NotificationsDemo_t701924017::get_offset_of_isInitializedInfo_23(),
	NotificationsDemo_t701924017::get_offset_of_pendingNotificationList_24(),
	NotificationsDemo_t701924017::get_offset_of_idInputField_25(),
	NotificationsDemo_t701924017::get_offset_of_demoUtils_26(),
	NotificationsDemo_t701924017::get_offset_of_orgNotificationListText_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (DemoAppConsent_t1008287276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4673[3] = 
{
	0,
	DemoAppConsent_t1008287276::get_offset_of_advertisingConsent_1(),
	DemoAppConsent_t1008287276::get_offset_of_notificationConsent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (PrivacyDemo_t1050594026), -1, sizeof(PrivacyDemo_t1050594026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4674[49] = 
{
	0,
	0,
	0,
	0,
	0,
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CUnityAnalyticsOptOutURLU3Ek__BackingField_9(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PrivacyDemo_t1050594026::get_offset_of_shouldRequestConsent_39(),
	PrivacyDemo_t1050594026::get_offset_of_isInEeaRegionDisplayer_40(),
	PrivacyDemo_t1050594026::get_offset_of_demoUtils_41(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_mPreviewConsentDialog_42(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_mDemoConsentDialog_43(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_mDemoConsentDialogLocalized_44(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_mIsInEEARegion_45(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_46(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_47(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_48(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_49(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_50(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_51(),
	PrivacyDemo_t1050594026_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (U3CFetchUnityAnalyticsOptOutURLU3Ec__AnonStorey0_t1362407561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4675[2] = 
{
	U3CFetchUnityAnalyticsOptOutURLU3Ec__AnonStorey0_t1362407561::get_offset_of_success_0(),
	U3CFetchUnityAnalyticsOptOutURLU3Ec__AnonStorey0_t1362407561::get_offset_of_failure_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (SharingDemo_t2942947019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4676[8] = 
{
	SharingDemo_t2942947019::get_offset_of_clockRect_4(),
	SharingDemo_t2942947019::get_offset_of_clockText_5(),
	SharingDemo_t2942947019::get_offset_of_TwoStepScreenshotName_6(),
	SharingDemo_t2942947019::get_offset_of_OneStepScreenshotName_7(),
	SharingDemo_t2942947019::get_offset_of_TwoStepScreenshotPath_8(),
	SharingDemo_t2942947019::get_offset_of_sampleMessage_9(),
	SharingDemo_t2942947019::get_offset_of_sampleText_10(),
	SharingDemo_t2942947019::get_offset_of_sampleURL_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4677[4] = 
{
	U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530::get_offset_of_U24this_0(),
	U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530::get_offset_of_U24current_1(),
	U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530::get_offset_of_U24disposing_2(),
	U3CCRSaveScreenshotU3Ec__Iterator0_t1145865530::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (U3CCROneStepSharingU3Ec__Iterator1_t921484503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4678[4] = 
{
	U3CCROneStepSharingU3Ec__Iterator1_t921484503::get_offset_of_U24this_0(),
	U3CCROneStepSharingU3Ec__Iterator1_t921484503::get_offset_of_U24current_1(),
	U3CCROneStepSharingU3Ec__Iterator1_t921484503::get_offset_of_U24disposing_2(),
	U3CCROneStepSharingU3Ec__Iterator1_t921484503::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { sizeof (SoundManager_t1283146133), -1, sizeof(SoundManager_t1283146133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4679[3] = 
{
	SoundManager_t1283146133_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	SoundManager_t1283146133::get_offset_of_button_5(),
	SoundManager_t1283146133::get_offset_of__audioSource_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { sizeof (UtilitiesDemo_t4069723306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4680[6] = 
{
	UtilitiesDemo_t4069723306::get_offset_of_ignoreConstraints_4(),
	UtilitiesDemo_t4069723306::get_offset_of_isDisabled_5(),
	UtilitiesDemo_t4069723306::get_offset_of_annualRemainingRequests_6(),
	UtilitiesDemo_t4069723306::get_offset_of_delayAfterInstallRemainingTime_7(),
	UtilitiesDemo_t4069723306::get_offset_of_coolingOffRemainingTime_8(),
	UtilitiesDemo_t4069723306::get_offset_of_demoUtils_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (ScrollableList_t4043704206), -1, sizeof(ScrollableList_t4043704206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4681[22] = 
{
	ScrollableList_t4043704206::get_offset_of_ItemSelected_4(),
	ScrollableList_t4043704206::get_offset_of_UIClosed_5(),
	ScrollableList_t4043704206::get_offset_of_position_6(),
	ScrollableList_t4043704206::get_offset_of_horizontalScroll_7(),
	ScrollableList_t4043704206::get_offset_of_verticalScroll_8(),
	ScrollableList_t4043704206::get_offset_of_width_9(),
	ScrollableList_t4043704206::get_offset_of_height_10(),
	ScrollableList_t4043704206::get_offset_of_itemHeight_11(),
	ScrollableList_t4043704206::get_offset_of_spacing_12(),
	ScrollableList_t4043704206::get_offset_of_paddingLeft_13(),
	ScrollableList_t4043704206::get_offset_of_paddingRight_14(),
	ScrollableList_t4043704206::get_offset_of_paddingTop_15(),
	ScrollableList_t4043704206::get_offset_of_paddingBottom_16(),
	ScrollableList_t4043704206::get_offset_of_bodyColor_17(),
	ScrollableList_t4043704206::get_offset_of_itemColor_18(),
	ScrollableList_t4043704206::get_offset_of_title_19(),
	ScrollableList_t4043704206::get_offset_of_scrollRect_20(),
	ScrollableList_t4043704206::get_offset_of_content_21(),
	ScrollableList_t4043704206::get_offset_of_itemPrefab_22(),
	ScrollableList_t4043704206::get_offset_of_items_23(),
	ScrollableList_t4043704206_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_24(),
	ScrollableList_t4043704206_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (U3CAddItemU3Ec__AnonStorey0_t3352757045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4682[3] = 
{
	U3CAddItemU3Ec__AnonStorey0_t3352757045::get_offset_of_title_0(),
	U3CAddItemU3Ec__AnonStorey0_t3352757045::get_offset_of_subtitle_1(),
	U3CAddItemU3Ec__AnonStorey0_t3352757045::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (ScrollableListItem_t3654577678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4683[3] = 
{
	ScrollableListItem_t3654577678::get_offset_of_title_4(),
	ScrollableListItem_t3654577678::get_offset_of_subtitle_5(),
	ScrollableListItem_t3654577678::get_offset_of_button_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (EM_GameServicesConstants_t963641404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4684[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (EM_GPGSIds_t587911181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4685[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (EM_IAPConstants_t4099475916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4686[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (DummyAppLifecycleHandler_t4108139044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (EnumFlagsAttribute_t536974667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4689[1] = 
{
	EnumFlagsAttribute_t536974667::get_offset_of_enumName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (RenameAttribute_t168776318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4690[1] = 
{
	RenameAttribute_t168776318::get_offset_of_U3CNewNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (ColorExt_t1658015523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (DateTimeExt_t1870725150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (RuntimeHelper_t2481760429), -1, sizeof(RuntimeHelper_t2481760429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4693[7] = 
{
	RuntimeHelper_t2481760429_StaticFields::get_offset_of_mInstance_4(),
	RuntimeHelper_t2481760429_StaticFields::get_offset_of_mToMainThreadQueue_5(),
	RuntimeHelper_t2481760429::get_offset_of_localToMainThreadQueue_6(),
	RuntimeHelper_t2481760429_StaticFields::get_offset_of_mIsToMainThreadQueueEmpty_7(),
	RuntimeHelper_t2481760429_StaticFields::get_offset_of_mPauseCallbackQueue_8(),
	RuntimeHelper_t2481760429_StaticFields::get_offset_of_mFocusCallbackQueue_9(),
	RuntimeHelper_t2481760429_StaticFields::get_offset_of_mIsDummy_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4694[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4695[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4696[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4697[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4698[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4699[4] = 
{
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
