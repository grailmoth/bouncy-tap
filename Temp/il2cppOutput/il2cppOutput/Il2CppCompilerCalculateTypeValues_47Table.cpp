﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EasyMobile.Achievement[]
struct AchievementU5BU5D_t2302189113;
// EasyMobile.AdClientImpl
struct AdClientImpl_t879350116;
// EasyMobile.AdColonyClientImpl
struct AdColonyClientImpl_t352798180;
// EasyMobile.AdColonySettings
struct AdColonySettings_t2994520974;
// EasyMobile.AdId
struct AdId_t1082999026;
// EasyMobile.AdId[]
struct AdIdU5BU5D_t2151920391;
// EasyMobile.AdMobClientImpl
struct AdMobClientImpl_t3823742600;
// EasyMobile.AdMobSettings
struct AdMobSettings_t1707484206;
// EasyMobile.AdMobSettings/AdMobTargetingSettings
struct AdMobTargetingSettings_t662407302;
// EasyMobile.AdPlacement
struct AdPlacement_t3195089512;
// EasyMobile.AdPlacement[]
struct AdPlacementU5BU5D_t642510585;
// EasyMobile.AdSettings
struct AdSettings_t94672728;
// EasyMobile.AudienceNetworkClientImpl
struct AudienceNetworkClientImpl_t266954160;
// EasyMobile.AudienceNetworkSettings
struct AudienceNetworkSettings_t1217453211;
// EasyMobile.ChartboostClientImpl
struct ChartboostClientImpl_t602894833;
// EasyMobile.ChartboostSettings
struct ChartboostSettings_t2523652890;
// EasyMobile.GameServicesSettings
struct GameServicesSettings_t1871988932;
// EasyMobile.HeyzapClientImpl
struct HeyzapClientImpl_t2848241831;
// EasyMobile.HeyzapSettings
struct HeyzapSettings_t3928682395;
// EasyMobile.IAPSettings
struct IAPSettings_t189197306;
// EasyMobile.ISavedGameClient
struct ISavedGameClient_t2919315494;
// EasyMobile.Internal.Dictionary_AdPlacement_AdId
struct Dictionary_AdPlacement_AdId_t1141413887;
// EasyMobile.Internal.IAppLifecycleHandler
struct IAppLifecycleHandler_t1207218692;
// EasyMobile.Internal.StringStringSerializableDictionary
struct StringStringSerializableDictionary_t4078874517;
// EasyMobile.IronSourceClientImpl
struct IronSourceClientImpl_t41919987;
// EasyMobile.IronSourceSettings
struct IronSourceSettings_t2849155294;
// EasyMobile.IronSourceSettings/SegmentSettings
struct SegmentSettings_t159419534;
// EasyMobile.Leaderboard[]
struct LeaderboardU5BU5D_t1320244962;
// EasyMobile.MoPubClientImpl
struct MoPubClientImpl_t784383640;
// EasyMobile.MoPubSettings
struct MoPubSettings_t3452462547;
// EasyMobile.MoPubSettings/MediationSetting[]
struct MediationSettingU5BU5D_t2663728768;
// EasyMobile.MoPubSettings/MoPubAdvancedBidder[]
struct MoPubAdvancedBidderU5BU5D_t2407845123;
// EasyMobile.MoPubSettings/MoPubInitNetwork[]
struct MoPubInitNetworkU5BU5D_t564488331;
// EasyMobile.NotificationsSettings
struct NotificationsSettings_t3829333496;
// EasyMobile.PrivacySettings
struct PrivacySettings_t2445251604;
// EasyMobile.RatingRequestSettings
struct RatingRequestSettings_t4213169708;
// EasyMobile.SavedGame
struct SavedGame_t947814848;
// EasyMobile.TapjoyClientImpl
struct TapjoyClientImpl_t2003437302;
// EasyMobile.TapjoySettings
struct TapjoySettings_t205532585;
// EasyMobile.UnityAdsClientImpl
struct UnityAdsClientImpl_t4105509132;
// EasyMobile.UnityAdsSettings
struct UnityAdsSettings_t2563580717;
// System.Action
struct Action_t1264377477;
// System.Action`1<EasyMobile.ConsentStatus>
struct Action_1_t2890538688;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t1082147328;
// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement>
struct Action_2_t3839421581;
// System.Action`2<EasyMobile.InterstitialAdNetwork,EasyMobile.AdPlacement>
struct Action_2_t2057961651;
// System.Action`2<EasyMobile.RewardedAdNetwork,EasyMobile.AdPlacement>
struct Action_2_t3281220897;
// System.Action`2<System.String,UnityEngine.SocialPlatforms.IScore>
struct Action_2_t387663310;
// System.Action`2<System.String,UnityEngine.SocialPlatforms.IScore[]>
struct Action_2_t2650591233;
// System.Action`2<UnityEngine.Advertisements.ShowResult,EasyMobile.AdPlacement>
struct Action_2_t2800288619;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Entry<EasyMobile.AdPlacement,EasyMobile.AdId>[]
struct EntryU5BU5D_t944158999;
// System.Collections.Generic.Dictionary`2/Entry<System.String,EasyMobile.AdId>[]
struct EntryU5BU5D_t1755157640;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t885026589;
// System.Collections.Generic.Dictionary`2/KeyCollection<EasyMobile.AdPlacement,EasyMobile.AdId>
struct KeyCollection_t418652041;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,EasyMobile.AdId>
struct KeyCollection_t1057930796;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t1822382459;
// System.Collections.Generic.Dictionary`2/ValueCollection<EasyMobile.AdPlacement,EasyMobile.AdId>
struct ValueCollection_t1945020888;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,EasyMobile.AdId>
struct ValueCollection_t2584299643;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t3348751306;
// System.Collections.Generic.Dictionary`2<EasyMobile.AdNetwork,System.Collections.Generic.List`1<EasyMobile.AdPlacement>>
struct Dictionary_2_t2490943318;
// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.AdPlacement>
struct Dictionary_2_t2980345811;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;
// System.Collections.Generic.IEqualityComparer`1<EasyMobile.AdPlacement>
struct IEqualityComparer_1_t1007454234;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.List`1<EasyMobile.GameServices/LoadScoreRequest>
struct List_1_t2814302364;
// System.Collections.Generic.List`1<EasyMobile.IAdClient>
struct List_1_t2429733395;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IDisposable
struct IDisposable_t3640265483;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t3934877283;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ADCLIENTIMPL_T879350116_H
#define ADCLIENTIMPL_T879350116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdClientImpl
struct  AdClientImpl_t879350116  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.AdClientImpl::mIsInitialized
	bool ___mIsInitialized_0;
	// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement> EasyMobile.AdClientImpl::InterstitialAdCompleted
	Action_2_t3839421581 * ___InterstitialAdCompleted_1;
	// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement> EasyMobile.AdClientImpl::RewardedAdSkipped
	Action_2_t3839421581 * ___RewardedAdSkipped_2;
	// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement> EasyMobile.AdClientImpl::RewardedAdCompleted
	Action_2_t3839421581 * ___RewardedAdCompleted_3;
	// System.Action`1<EasyMobile.ConsentStatus> EasyMobile.AdClientImpl::DataPrivacyConsentUpdated
	Action_1_t2890538688 * ___DataPrivacyConsentUpdated_4;

public:
	inline static int32_t get_offset_of_mIsInitialized_0() { return static_cast<int32_t>(offsetof(AdClientImpl_t879350116, ___mIsInitialized_0)); }
	inline bool get_mIsInitialized_0() const { return ___mIsInitialized_0; }
	inline bool* get_address_of_mIsInitialized_0() { return &___mIsInitialized_0; }
	inline void set_mIsInitialized_0(bool value)
	{
		___mIsInitialized_0 = value;
	}

	inline static int32_t get_offset_of_InterstitialAdCompleted_1() { return static_cast<int32_t>(offsetof(AdClientImpl_t879350116, ___InterstitialAdCompleted_1)); }
	inline Action_2_t3839421581 * get_InterstitialAdCompleted_1() const { return ___InterstitialAdCompleted_1; }
	inline Action_2_t3839421581 ** get_address_of_InterstitialAdCompleted_1() { return &___InterstitialAdCompleted_1; }
	inline void set_InterstitialAdCompleted_1(Action_2_t3839421581 * value)
	{
		___InterstitialAdCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___InterstitialAdCompleted_1), value);
	}

	inline static int32_t get_offset_of_RewardedAdSkipped_2() { return static_cast<int32_t>(offsetof(AdClientImpl_t879350116, ___RewardedAdSkipped_2)); }
	inline Action_2_t3839421581 * get_RewardedAdSkipped_2() const { return ___RewardedAdSkipped_2; }
	inline Action_2_t3839421581 ** get_address_of_RewardedAdSkipped_2() { return &___RewardedAdSkipped_2; }
	inline void set_RewardedAdSkipped_2(Action_2_t3839421581 * value)
	{
		___RewardedAdSkipped_2 = value;
		Il2CppCodeGenWriteBarrier((&___RewardedAdSkipped_2), value);
	}

	inline static int32_t get_offset_of_RewardedAdCompleted_3() { return static_cast<int32_t>(offsetof(AdClientImpl_t879350116, ___RewardedAdCompleted_3)); }
	inline Action_2_t3839421581 * get_RewardedAdCompleted_3() const { return ___RewardedAdCompleted_3; }
	inline Action_2_t3839421581 ** get_address_of_RewardedAdCompleted_3() { return &___RewardedAdCompleted_3; }
	inline void set_RewardedAdCompleted_3(Action_2_t3839421581 * value)
	{
		___RewardedAdCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___RewardedAdCompleted_3), value);
	}

	inline static int32_t get_offset_of_DataPrivacyConsentUpdated_4() { return static_cast<int32_t>(offsetof(AdClientImpl_t879350116, ___DataPrivacyConsentUpdated_4)); }
	inline Action_1_t2890538688 * get_DataPrivacyConsentUpdated_4() const { return ___DataPrivacyConsentUpdated_4; }
	inline Action_1_t2890538688 ** get_address_of_DataPrivacyConsentUpdated_4() { return &___DataPrivacyConsentUpdated_4; }
	inline void set_DataPrivacyConsentUpdated_4(Action_1_t2890538688 * value)
	{
		___DataPrivacyConsentUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___DataPrivacyConsentUpdated_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADCLIENTIMPL_T879350116_H
#ifndef ADLOCATION_T1108752805_H
#define ADLOCATION_T1108752805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdLocation
struct  AdLocation_t1108752805  : public RuntimeObject
{
public:
	// System.String EasyMobile.AdLocation::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

struct AdLocation_t1108752805_StaticFields
{
public:
	// System.Collections.Hashtable EasyMobile.AdLocation::map
	Hashtable_t1853889766 * ___map_1;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Default
	AdLocation_t1108752805 * ___Default_2;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Startup
	AdLocation_t1108752805 * ___Startup_3;
	// EasyMobile.AdLocation EasyMobile.AdLocation::HomeScreen
	AdLocation_t1108752805 * ___HomeScreen_4;
	// EasyMobile.AdLocation EasyMobile.AdLocation::MainMenu
	AdLocation_t1108752805 * ___MainMenu_5;
	// EasyMobile.AdLocation EasyMobile.AdLocation::GameScreen
	AdLocation_t1108752805 * ___GameScreen_6;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Achievements
	AdLocation_t1108752805 * ___Achievements_7;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Quests
	AdLocation_t1108752805 * ___Quests_8;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Pause
	AdLocation_t1108752805 * ___Pause_9;
	// EasyMobile.AdLocation EasyMobile.AdLocation::LevelStart
	AdLocation_t1108752805 * ___LevelStart_10;
	// EasyMobile.AdLocation EasyMobile.AdLocation::LevelComplete
	AdLocation_t1108752805 * ___LevelComplete_11;
	// EasyMobile.AdLocation EasyMobile.AdLocation::TurnComplete
	AdLocation_t1108752805 * ___TurnComplete_12;
	// EasyMobile.AdLocation EasyMobile.AdLocation::IAPStore
	AdLocation_t1108752805 * ___IAPStore_13;
	// EasyMobile.AdLocation EasyMobile.AdLocation::ItemStore
	AdLocation_t1108752805 * ___ItemStore_14;
	// EasyMobile.AdLocation EasyMobile.AdLocation::GameOver
	AdLocation_t1108752805 * ___GameOver_15;
	// EasyMobile.AdLocation EasyMobile.AdLocation::LeaderBoard
	AdLocation_t1108752805 * ___LeaderBoard_16;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Settings
	AdLocation_t1108752805 * ___Settings_17;
	// EasyMobile.AdLocation EasyMobile.AdLocation::Quit
	AdLocation_t1108752805 * ___Quit_18;

public:
	inline static int32_t get_offset_of_map_1() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___map_1)); }
	inline Hashtable_t1853889766 * get_map_1() const { return ___map_1; }
	inline Hashtable_t1853889766 ** get_address_of_map_1() { return &___map_1; }
	inline void set_map_1(Hashtable_t1853889766 * value)
	{
		___map_1 = value;
		Il2CppCodeGenWriteBarrier((&___map_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Default_2)); }
	inline AdLocation_t1108752805 * get_Default_2() const { return ___Default_2; }
	inline AdLocation_t1108752805 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(AdLocation_t1108752805 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}

	inline static int32_t get_offset_of_Startup_3() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Startup_3)); }
	inline AdLocation_t1108752805 * get_Startup_3() const { return ___Startup_3; }
	inline AdLocation_t1108752805 ** get_address_of_Startup_3() { return &___Startup_3; }
	inline void set_Startup_3(AdLocation_t1108752805 * value)
	{
		___Startup_3 = value;
		Il2CppCodeGenWriteBarrier((&___Startup_3), value);
	}

	inline static int32_t get_offset_of_HomeScreen_4() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___HomeScreen_4)); }
	inline AdLocation_t1108752805 * get_HomeScreen_4() const { return ___HomeScreen_4; }
	inline AdLocation_t1108752805 ** get_address_of_HomeScreen_4() { return &___HomeScreen_4; }
	inline void set_HomeScreen_4(AdLocation_t1108752805 * value)
	{
		___HomeScreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___HomeScreen_4), value);
	}

	inline static int32_t get_offset_of_MainMenu_5() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___MainMenu_5)); }
	inline AdLocation_t1108752805 * get_MainMenu_5() const { return ___MainMenu_5; }
	inline AdLocation_t1108752805 ** get_address_of_MainMenu_5() { return &___MainMenu_5; }
	inline void set_MainMenu_5(AdLocation_t1108752805 * value)
	{
		___MainMenu_5 = value;
		Il2CppCodeGenWriteBarrier((&___MainMenu_5), value);
	}

	inline static int32_t get_offset_of_GameScreen_6() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___GameScreen_6)); }
	inline AdLocation_t1108752805 * get_GameScreen_6() const { return ___GameScreen_6; }
	inline AdLocation_t1108752805 ** get_address_of_GameScreen_6() { return &___GameScreen_6; }
	inline void set_GameScreen_6(AdLocation_t1108752805 * value)
	{
		___GameScreen_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameScreen_6), value);
	}

	inline static int32_t get_offset_of_Achievements_7() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Achievements_7)); }
	inline AdLocation_t1108752805 * get_Achievements_7() const { return ___Achievements_7; }
	inline AdLocation_t1108752805 ** get_address_of_Achievements_7() { return &___Achievements_7; }
	inline void set_Achievements_7(AdLocation_t1108752805 * value)
	{
		___Achievements_7 = value;
		Il2CppCodeGenWriteBarrier((&___Achievements_7), value);
	}

	inline static int32_t get_offset_of_Quests_8() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Quests_8)); }
	inline AdLocation_t1108752805 * get_Quests_8() const { return ___Quests_8; }
	inline AdLocation_t1108752805 ** get_address_of_Quests_8() { return &___Quests_8; }
	inline void set_Quests_8(AdLocation_t1108752805 * value)
	{
		___Quests_8 = value;
		Il2CppCodeGenWriteBarrier((&___Quests_8), value);
	}

	inline static int32_t get_offset_of_Pause_9() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Pause_9)); }
	inline AdLocation_t1108752805 * get_Pause_9() const { return ___Pause_9; }
	inline AdLocation_t1108752805 ** get_address_of_Pause_9() { return &___Pause_9; }
	inline void set_Pause_9(AdLocation_t1108752805 * value)
	{
		___Pause_9 = value;
		Il2CppCodeGenWriteBarrier((&___Pause_9), value);
	}

	inline static int32_t get_offset_of_LevelStart_10() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___LevelStart_10)); }
	inline AdLocation_t1108752805 * get_LevelStart_10() const { return ___LevelStart_10; }
	inline AdLocation_t1108752805 ** get_address_of_LevelStart_10() { return &___LevelStart_10; }
	inline void set_LevelStart_10(AdLocation_t1108752805 * value)
	{
		___LevelStart_10 = value;
		Il2CppCodeGenWriteBarrier((&___LevelStart_10), value);
	}

	inline static int32_t get_offset_of_LevelComplete_11() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___LevelComplete_11)); }
	inline AdLocation_t1108752805 * get_LevelComplete_11() const { return ___LevelComplete_11; }
	inline AdLocation_t1108752805 ** get_address_of_LevelComplete_11() { return &___LevelComplete_11; }
	inline void set_LevelComplete_11(AdLocation_t1108752805 * value)
	{
		___LevelComplete_11 = value;
		Il2CppCodeGenWriteBarrier((&___LevelComplete_11), value);
	}

	inline static int32_t get_offset_of_TurnComplete_12() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___TurnComplete_12)); }
	inline AdLocation_t1108752805 * get_TurnComplete_12() const { return ___TurnComplete_12; }
	inline AdLocation_t1108752805 ** get_address_of_TurnComplete_12() { return &___TurnComplete_12; }
	inline void set_TurnComplete_12(AdLocation_t1108752805 * value)
	{
		___TurnComplete_12 = value;
		Il2CppCodeGenWriteBarrier((&___TurnComplete_12), value);
	}

	inline static int32_t get_offset_of_IAPStore_13() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___IAPStore_13)); }
	inline AdLocation_t1108752805 * get_IAPStore_13() const { return ___IAPStore_13; }
	inline AdLocation_t1108752805 ** get_address_of_IAPStore_13() { return &___IAPStore_13; }
	inline void set_IAPStore_13(AdLocation_t1108752805 * value)
	{
		___IAPStore_13 = value;
		Il2CppCodeGenWriteBarrier((&___IAPStore_13), value);
	}

	inline static int32_t get_offset_of_ItemStore_14() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___ItemStore_14)); }
	inline AdLocation_t1108752805 * get_ItemStore_14() const { return ___ItemStore_14; }
	inline AdLocation_t1108752805 ** get_address_of_ItemStore_14() { return &___ItemStore_14; }
	inline void set_ItemStore_14(AdLocation_t1108752805 * value)
	{
		___ItemStore_14 = value;
		Il2CppCodeGenWriteBarrier((&___ItemStore_14), value);
	}

	inline static int32_t get_offset_of_GameOver_15() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___GameOver_15)); }
	inline AdLocation_t1108752805 * get_GameOver_15() const { return ___GameOver_15; }
	inline AdLocation_t1108752805 ** get_address_of_GameOver_15() { return &___GameOver_15; }
	inline void set_GameOver_15(AdLocation_t1108752805 * value)
	{
		___GameOver_15 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_15), value);
	}

	inline static int32_t get_offset_of_LeaderBoard_16() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___LeaderBoard_16)); }
	inline AdLocation_t1108752805 * get_LeaderBoard_16() const { return ___LeaderBoard_16; }
	inline AdLocation_t1108752805 ** get_address_of_LeaderBoard_16() { return &___LeaderBoard_16; }
	inline void set_LeaderBoard_16(AdLocation_t1108752805 * value)
	{
		___LeaderBoard_16 = value;
		Il2CppCodeGenWriteBarrier((&___LeaderBoard_16), value);
	}

	inline static int32_t get_offset_of_Settings_17() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Settings_17)); }
	inline AdLocation_t1108752805 * get_Settings_17() const { return ___Settings_17; }
	inline AdLocation_t1108752805 ** get_address_of_Settings_17() { return &___Settings_17; }
	inline void set_Settings_17(AdLocation_t1108752805 * value)
	{
		___Settings_17 = value;
		Il2CppCodeGenWriteBarrier((&___Settings_17), value);
	}

	inline static int32_t get_offset_of_Quit_18() { return static_cast<int32_t>(offsetof(AdLocation_t1108752805_StaticFields, ___Quit_18)); }
	inline AdLocation_t1108752805 * get_Quit_18() const { return ___Quit_18; }
	inline AdLocation_t1108752805 ** get_address_of_Quit_18() { return &___Quit_18; }
	inline void set_Quit_18(AdLocation_t1108752805 * value)
	{
		___Quit_18 = value;
		Il2CppCodeGenWriteBarrier((&___Quit_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOCATION_T1108752805_H
#ifndef ADLOCATIONEXTENSION_T1550154976_H
#define ADLOCATIONEXTENSION_T1550154976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdLocationExtension
struct  AdLocationExtension_t1550154976  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOCATIONEXTENSION_T1550154976_H
#ifndef ADMOBSETTINGS_T1707484206_H
#define ADMOBSETTINGS_T1707484206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdMobSettings
struct  AdMobSettings_t1707484206  : public RuntimeObject
{
public:
	// EasyMobile.AdMobSettings/AdMobTargetingSettings EasyMobile.AdMobSettings::mTargetingSettings
	AdMobTargetingSettings_t662407302 * ___mTargetingSettings_0;
	// System.Boolean EasyMobile.AdMobSettings::mEnableTestMode
	bool ___mEnableTestMode_1;
	// System.String[] EasyMobile.AdMobSettings::mTestDeviceIds
	StringU5BU5D_t1281789340* ___mTestDeviceIds_2;
	// EasyMobile.AdId EasyMobile.AdMobSettings::mAppId
	AdId_t1082999026 * ___mAppId_3;
	// EasyMobile.AdId EasyMobile.AdMobSettings::mDefaultBannerAdId
	AdId_t1082999026 * ___mDefaultBannerAdId_4;
	// EasyMobile.AdId EasyMobile.AdMobSettings::mDefaultInterstitialAdId
	AdId_t1082999026 * ___mDefaultInterstitialAdId_5;
	// EasyMobile.AdId EasyMobile.AdMobSettings::mDefaultRewardedAdId
	AdId_t1082999026 * ___mDefaultRewardedAdId_6;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AdMobSettings::mCustomBannerAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomBannerAdIds_7;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AdMobSettings::mCustomInterstitialAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomInterstitialAdIds_8;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AdMobSettings::mCustomRewardedAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomRewardedAdIds_9;

public:
	inline static int32_t get_offset_of_mTargetingSettings_0() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mTargetingSettings_0)); }
	inline AdMobTargetingSettings_t662407302 * get_mTargetingSettings_0() const { return ___mTargetingSettings_0; }
	inline AdMobTargetingSettings_t662407302 ** get_address_of_mTargetingSettings_0() { return &___mTargetingSettings_0; }
	inline void set_mTargetingSettings_0(AdMobTargetingSettings_t662407302 * value)
	{
		___mTargetingSettings_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetingSettings_0), value);
	}

	inline static int32_t get_offset_of_mEnableTestMode_1() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mEnableTestMode_1)); }
	inline bool get_mEnableTestMode_1() const { return ___mEnableTestMode_1; }
	inline bool* get_address_of_mEnableTestMode_1() { return &___mEnableTestMode_1; }
	inline void set_mEnableTestMode_1(bool value)
	{
		___mEnableTestMode_1 = value;
	}

	inline static int32_t get_offset_of_mTestDeviceIds_2() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mTestDeviceIds_2)); }
	inline StringU5BU5D_t1281789340* get_mTestDeviceIds_2() const { return ___mTestDeviceIds_2; }
	inline StringU5BU5D_t1281789340** get_address_of_mTestDeviceIds_2() { return &___mTestDeviceIds_2; }
	inline void set_mTestDeviceIds_2(StringU5BU5D_t1281789340* value)
	{
		___mTestDeviceIds_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTestDeviceIds_2), value);
	}

	inline static int32_t get_offset_of_mAppId_3() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mAppId_3)); }
	inline AdId_t1082999026 * get_mAppId_3() const { return ___mAppId_3; }
	inline AdId_t1082999026 ** get_address_of_mAppId_3() { return &___mAppId_3; }
	inline void set_mAppId_3(AdId_t1082999026 * value)
	{
		___mAppId_3 = value;
		Il2CppCodeGenWriteBarrier((&___mAppId_3), value);
	}

	inline static int32_t get_offset_of_mDefaultBannerAdId_4() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mDefaultBannerAdId_4)); }
	inline AdId_t1082999026 * get_mDefaultBannerAdId_4() const { return ___mDefaultBannerAdId_4; }
	inline AdId_t1082999026 ** get_address_of_mDefaultBannerAdId_4() { return &___mDefaultBannerAdId_4; }
	inline void set_mDefaultBannerAdId_4(AdId_t1082999026 * value)
	{
		___mDefaultBannerAdId_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultBannerAdId_4), value);
	}

	inline static int32_t get_offset_of_mDefaultInterstitialAdId_5() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mDefaultInterstitialAdId_5)); }
	inline AdId_t1082999026 * get_mDefaultInterstitialAdId_5() const { return ___mDefaultInterstitialAdId_5; }
	inline AdId_t1082999026 ** get_address_of_mDefaultInterstitialAdId_5() { return &___mDefaultInterstitialAdId_5; }
	inline void set_mDefaultInterstitialAdId_5(AdId_t1082999026 * value)
	{
		___mDefaultInterstitialAdId_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultInterstitialAdId_5), value);
	}

	inline static int32_t get_offset_of_mDefaultRewardedAdId_6() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mDefaultRewardedAdId_6)); }
	inline AdId_t1082999026 * get_mDefaultRewardedAdId_6() const { return ___mDefaultRewardedAdId_6; }
	inline AdId_t1082999026 ** get_address_of_mDefaultRewardedAdId_6() { return &___mDefaultRewardedAdId_6; }
	inline void set_mDefaultRewardedAdId_6(AdId_t1082999026 * value)
	{
		___mDefaultRewardedAdId_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRewardedAdId_6), value);
	}

	inline static int32_t get_offset_of_mCustomBannerAdIds_7() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mCustomBannerAdIds_7)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomBannerAdIds_7() const { return ___mCustomBannerAdIds_7; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomBannerAdIds_7() { return &___mCustomBannerAdIds_7; }
	inline void set_mCustomBannerAdIds_7(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomBannerAdIds_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomBannerAdIds_7), value);
	}

	inline static int32_t get_offset_of_mCustomInterstitialAdIds_8() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mCustomInterstitialAdIds_8)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomInterstitialAdIds_8() const { return ___mCustomInterstitialAdIds_8; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomInterstitialAdIds_8() { return &___mCustomInterstitialAdIds_8; }
	inline void set_mCustomInterstitialAdIds_8(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomInterstitialAdIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialAdIds_8), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedAdIds_9() { return static_cast<int32_t>(offsetof(AdMobSettings_t1707484206, ___mCustomRewardedAdIds_9)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomRewardedAdIds_9() const { return ___mCustomRewardedAdIds_9; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomRewardedAdIds_9() { return &___mCustomRewardedAdIds_9; }
	inline void set_mCustomRewardedAdIds_9(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomRewardedAdIds_9 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedAdIds_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMOBSETTINGS_T1707484206_H
#ifndef U3CCRAUTOLOADALLADSU3EC__ITERATOR1_T1924352638_H
#define U3CCRAUTOLOADALLADSU3EC__ITERATOR1_T1924352638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1
struct  U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638  : public RuntimeObject
{
public:
	// System.Single EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1::delay
	float ___delay_0;
	// System.Collections.Generic.List`1<EasyMobile.IAdClient> EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1::<availableInterstitialNetworks>__0
	List_1_t2429733395 * ___U3CavailableInterstitialNetworksU3E__0_1;
	// System.Collections.Generic.List`1<EasyMobile.IAdClient> EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1::<availableRewardedNetworks>__0
	List_1_t2429733395 * ___U3CavailableRewardedNetworksU3E__0_2;
	// System.Object EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 EasyMobile.Advertising/<CRAutoLoadAllAds>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U3CavailableInterstitialNetworksU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638, ___U3CavailableInterstitialNetworksU3E__0_1)); }
	inline List_1_t2429733395 * get_U3CavailableInterstitialNetworksU3E__0_1() const { return ___U3CavailableInterstitialNetworksU3E__0_1; }
	inline List_1_t2429733395 ** get_address_of_U3CavailableInterstitialNetworksU3E__0_1() { return &___U3CavailableInterstitialNetworksU3E__0_1; }
	inline void set_U3CavailableInterstitialNetworksU3E__0_1(List_1_t2429733395 * value)
	{
		___U3CavailableInterstitialNetworksU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CavailableInterstitialNetworksU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CavailableRewardedNetworksU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638, ___U3CavailableRewardedNetworksU3E__0_2)); }
	inline List_1_t2429733395 * get_U3CavailableRewardedNetworksU3E__0_2() const { return ___U3CavailableRewardedNetworksU3E__0_2; }
	inline List_1_t2429733395 ** get_address_of_U3CavailableRewardedNetworksU3E__0_2() { return &___U3CavailableRewardedNetworksU3E__0_2; }
	inline void set_U3CavailableRewardedNetworksU3E__0_2(List_1_t2429733395 * value)
	{
		___U3CavailableRewardedNetworksU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CavailableRewardedNetworksU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRAUTOLOADALLADSU3EC__ITERATOR1_T1924352638_H
#ifndef U3CCRAUTOLOADDEFAULTADSU3EC__ITERATOR0_T2266990893_H
#define U3CCRAUTOLOADDEFAULTADSU3EC__ITERATOR0_T2266990893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0
struct  U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893  : public RuntimeObject
{
public:
	// System.Single EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0::delay
	float ___delay_0;
	// System.Collections.IEnumerator EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// System.IDisposable EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_2;
	// System.Object EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 EasyMobile.Advertising/<CRAutoLoadDefaultAds>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893, ___U24locvar1_2)); }
	inline RuntimeObject* get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline RuntimeObject** get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(RuntimeObject* value)
	{
		___U24locvar1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRAUTOLOADDEFAULTADSU3EC__ITERATOR0_T2266990893_H
#ifndef BANNERADSIZE_T3807836689_H
#define BANNERADSIZE_T3807836689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.BannerAdSize
struct  BannerAdSize_t3807836689  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.BannerAdSize::<IsSmartBanner>k__BackingField
	bool ___U3CIsSmartBannerU3Ek__BackingField_0;
	// System.Int32 EasyMobile.BannerAdSize::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_1;
	// System.Int32 EasyMobile.BannerAdSize::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsSmartBannerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689, ___U3CIsSmartBannerU3Ek__BackingField_0)); }
	inline bool get_U3CIsSmartBannerU3Ek__BackingField_0() const { return ___U3CIsSmartBannerU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsSmartBannerU3Ek__BackingField_0() { return &___U3CIsSmartBannerU3Ek__BackingField_0; }
	inline void set_U3CIsSmartBannerU3Ek__BackingField_0(bool value)
	{
		___U3CIsSmartBannerU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689, ___U3CWidthU3Ek__BackingField_1)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_1() const { return ___U3CWidthU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_1() { return &___U3CWidthU3Ek__BackingField_1; }
	inline void set_U3CWidthU3Ek__BackingField_1(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689, ___U3CHeightU3Ek__BackingField_2)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_2() const { return ___U3CHeightU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_2() { return &___U3CHeightU3Ek__BackingField_2; }
	inline void set_U3CHeightU3Ek__BackingField_2(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_2 = value;
	}
};

struct BannerAdSize_t3807836689_StaticFields
{
public:
	// EasyMobile.BannerAdSize EasyMobile.BannerAdSize::Banner
	BannerAdSize_t3807836689 * ___Banner_3;
	// EasyMobile.BannerAdSize EasyMobile.BannerAdSize::MediumRectangle
	BannerAdSize_t3807836689 * ___MediumRectangle_4;
	// EasyMobile.BannerAdSize EasyMobile.BannerAdSize::IABBanner
	BannerAdSize_t3807836689 * ___IABBanner_5;
	// EasyMobile.BannerAdSize EasyMobile.BannerAdSize::Leaderboard
	BannerAdSize_t3807836689 * ___Leaderboard_6;
	// EasyMobile.BannerAdSize EasyMobile.BannerAdSize::SmartBanner
	BannerAdSize_t3807836689 * ___SmartBanner_7;

public:
	inline static int32_t get_offset_of_Banner_3() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689_StaticFields, ___Banner_3)); }
	inline BannerAdSize_t3807836689 * get_Banner_3() const { return ___Banner_3; }
	inline BannerAdSize_t3807836689 ** get_address_of_Banner_3() { return &___Banner_3; }
	inline void set_Banner_3(BannerAdSize_t3807836689 * value)
	{
		___Banner_3 = value;
		Il2CppCodeGenWriteBarrier((&___Banner_3), value);
	}

	inline static int32_t get_offset_of_MediumRectangle_4() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689_StaticFields, ___MediumRectangle_4)); }
	inline BannerAdSize_t3807836689 * get_MediumRectangle_4() const { return ___MediumRectangle_4; }
	inline BannerAdSize_t3807836689 ** get_address_of_MediumRectangle_4() { return &___MediumRectangle_4; }
	inline void set_MediumRectangle_4(BannerAdSize_t3807836689 * value)
	{
		___MediumRectangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___MediumRectangle_4), value);
	}

	inline static int32_t get_offset_of_IABBanner_5() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689_StaticFields, ___IABBanner_5)); }
	inline BannerAdSize_t3807836689 * get_IABBanner_5() const { return ___IABBanner_5; }
	inline BannerAdSize_t3807836689 ** get_address_of_IABBanner_5() { return &___IABBanner_5; }
	inline void set_IABBanner_5(BannerAdSize_t3807836689 * value)
	{
		___IABBanner_5 = value;
		Il2CppCodeGenWriteBarrier((&___IABBanner_5), value);
	}

	inline static int32_t get_offset_of_Leaderboard_6() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689_StaticFields, ___Leaderboard_6)); }
	inline BannerAdSize_t3807836689 * get_Leaderboard_6() const { return ___Leaderboard_6; }
	inline BannerAdSize_t3807836689 ** get_address_of_Leaderboard_6() { return &___Leaderboard_6; }
	inline void set_Leaderboard_6(BannerAdSize_t3807836689 * value)
	{
		___Leaderboard_6 = value;
		Il2CppCodeGenWriteBarrier((&___Leaderboard_6), value);
	}

	inline static int32_t get_offset_of_SmartBanner_7() { return static_cast<int32_t>(offsetof(BannerAdSize_t3807836689_StaticFields, ___SmartBanner_7)); }
	inline BannerAdSize_t3807836689 * get_SmartBanner_7() const { return ___SmartBanner_7; }
	inline BannerAdSize_t3807836689 ** get_address_of_SmartBanner_7() { return &___SmartBanner_7; }
	inline void set_SmartBanner_7(BannerAdSize_t3807836689 * value)
	{
		___SmartBanner_7 = value;
		Il2CppCodeGenWriteBarrier((&___SmartBanner_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERADSIZE_T3807836689_H
#ifndef CHARTBOOSTSETTINGS_T2523652890_H
#define CHARTBOOSTSETTINGS_T2523652890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ChartboostSettings
struct  ChartboostSettings_t2523652890  : public RuntimeObject
{
public:
	// EasyMobile.AdPlacement[] EasyMobile.ChartboostSettings::mCustomInterstitialPlacements
	AdPlacementU5BU5D_t642510585* ___mCustomInterstitialPlacements_0;
	// EasyMobile.AdPlacement[] EasyMobile.ChartboostSettings::mCustomRewardedPlacements
	AdPlacementU5BU5D_t642510585* ___mCustomRewardedPlacements_1;

public:
	inline static int32_t get_offset_of_mCustomInterstitialPlacements_0() { return static_cast<int32_t>(offsetof(ChartboostSettings_t2523652890, ___mCustomInterstitialPlacements_0)); }
	inline AdPlacementU5BU5D_t642510585* get_mCustomInterstitialPlacements_0() const { return ___mCustomInterstitialPlacements_0; }
	inline AdPlacementU5BU5D_t642510585** get_address_of_mCustomInterstitialPlacements_0() { return &___mCustomInterstitialPlacements_0; }
	inline void set_mCustomInterstitialPlacements_0(AdPlacementU5BU5D_t642510585* value)
	{
		___mCustomInterstitialPlacements_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialPlacements_0), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedPlacements_1() { return static_cast<int32_t>(offsetof(ChartboostSettings_t2523652890, ___mCustomRewardedPlacements_1)); }
	inline AdPlacementU5BU5D_t642510585* get_mCustomRewardedPlacements_1() const { return ___mCustomRewardedPlacements_1; }
	inline AdPlacementU5BU5D_t642510585** get_address_of_mCustomRewardedPlacements_1() { return &___mCustomRewardedPlacements_1; }
	inline void set_mCustomRewardedPlacements_1(AdPlacementU5BU5D_t642510585* value)
	{
		___mCustomRewardedPlacements_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedPlacements_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARTBOOSTSETTINGS_T2523652890_H
#ifndef CONSENTMANAGER_T1411390104_H
#define CONSENTMANAGER_T1411390104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentManager
struct  ConsentManager_t1411390104  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentManager::mDataPrivacyConsentKey
	String_t* ___mDataPrivacyConsentKey_0;
	// System.Action`1<EasyMobile.ConsentStatus> EasyMobile.ConsentManager::DataPrivacyConsentUpdated
	Action_1_t2890538688 * ___DataPrivacyConsentUpdated_1;

public:
	inline static int32_t get_offset_of_mDataPrivacyConsentKey_0() { return static_cast<int32_t>(offsetof(ConsentManager_t1411390104, ___mDataPrivacyConsentKey_0)); }
	inline String_t* get_mDataPrivacyConsentKey_0() const { return ___mDataPrivacyConsentKey_0; }
	inline String_t** get_address_of_mDataPrivacyConsentKey_0() { return &___mDataPrivacyConsentKey_0; }
	inline void set_mDataPrivacyConsentKey_0(String_t* value)
	{
		___mDataPrivacyConsentKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDataPrivacyConsentKey_0), value);
	}

	inline static int32_t get_offset_of_DataPrivacyConsentUpdated_1() { return static_cast<int32_t>(offsetof(ConsentManager_t1411390104, ___DataPrivacyConsentUpdated_1)); }
	inline Action_1_t2890538688 * get_DataPrivacyConsentUpdated_1() const { return ___DataPrivacyConsentUpdated_1; }
	inline Action_1_t2890538688 ** get_address_of_DataPrivacyConsentUpdated_1() { return &___DataPrivacyConsentUpdated_1; }
	inline void set_DataPrivacyConsentUpdated_1(Action_1_t2890538688 * value)
	{
		___DataPrivacyConsentUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___DataPrivacyConsentUpdated_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTMANAGER_T1411390104_H
#ifndef CROSSPLATFORMID_T1390234332_H
#define CROSSPLATFORMID_T1390234332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.CrossPlatformId
struct  CrossPlatformId_t1390234332  : public RuntimeObject
{
public:
	// System.String EasyMobile.CrossPlatformId::mIosId
	String_t* ___mIosId_0;
	// System.String EasyMobile.CrossPlatformId::mAndroidId
	String_t* ___mAndroidId_1;

public:
	inline static int32_t get_offset_of_mIosId_0() { return static_cast<int32_t>(offsetof(CrossPlatformId_t1390234332, ___mIosId_0)); }
	inline String_t* get_mIosId_0() const { return ___mIosId_0; }
	inline String_t** get_address_of_mIosId_0() { return &___mIosId_0; }
	inline void set_mIosId_0(String_t* value)
	{
		___mIosId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mIosId_0), value);
	}

	inline static int32_t get_offset_of_mAndroidId_1() { return static_cast<int32_t>(offsetof(CrossPlatformId_t1390234332, ___mAndroidId_1)); }
	inline String_t* get_mAndroidId_1() const { return ___mAndroidId_1; }
	inline String_t** get_address_of_mAndroidId_1() { return &___mAndroidId_1; }
	inline void set_mAndroidId_1(String_t* value)
	{
		___mAndroidId_1 = value;
		Il2CppCodeGenWriteBarrier((&___mAndroidId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMID_T1390234332_H
#ifndef U3CCRAUTOINITU3EC__ITERATOR0_T2576002651_H
#define U3CCRAUTOINITU3EC__ITERATOR0_T2576002651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/<CRAutoInit>c__Iterator0
struct  U3CCRAutoInitU3Ec__Iterator0_t2576002651  : public RuntimeObject
{
public:
	// System.Single EasyMobile.GameServices/<CRAutoInit>c__Iterator0::delay
	float ___delay_0;
	// System.Object EasyMobile.GameServices/<CRAutoInit>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean EasyMobile.GameServices/<CRAutoInit>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 EasyMobile.GameServices/<CRAutoInit>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t2576002651, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t2576002651, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t2576002651, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t2576002651, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRAUTOINITU3EC__ITERATOR0_T2576002651_H
#ifndef U3CDOREPORTACHIEVEMENTPROGRESSU3EC__ANONSTOREY4_T2778844868_H
#define U3CDOREPORTACHIEVEMENTPROGRESSU3EC__ANONSTOREY4_T2778844868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/<DoReportAchievementProgress>c__AnonStorey4
struct  U3CDoReportAchievementProgressU3Ec__AnonStorey4_t2778844868  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> EasyMobile.GameServices/<DoReportAchievementProgress>c__AnonStorey4::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CDoReportAchievementProgressU3Ec__AnonStorey4_t2778844868, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOREPORTACHIEVEMENTPROGRESSU3EC__ANONSTOREY4_T2778844868_H
#ifndef U3CDOREPORTSCOREU3EC__ANONSTOREY3_T3438848096_H
#define U3CDOREPORTSCOREU3EC__ANONSTOREY3_T3438848096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/<DoReportScore>c__AnonStorey3
struct  U3CDoReportScoreU3Ec__AnonStorey3_t3438848096  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> EasyMobile.GameServices/<DoReportScore>c__AnonStorey3::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CDoReportScoreU3Ec__AnonStorey3_t3438848096, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOREPORTSCOREU3EC__ANONSTOREY3_T3438848096_H
#ifndef U3CLOADFRIENDSU3EC__ANONSTOREY1_T240145850_H
#define U3CLOADFRIENDSU3EC__ANONSTOREY1_T240145850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/<LoadFriends>c__AnonStorey1
struct  U3CLoadFriendsU3Ec__AnonStorey1_t240145850  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]> EasyMobile.GameServices/<LoadFriends>c__AnonStorey1::callback
	Action_1_t1082147328 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CLoadFriendsU3Ec__AnonStorey1_t240145850, ___callback_0)); }
	inline Action_1_t1082147328 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t1082147328 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t1082147328 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFRIENDSU3EC__ANONSTOREY1_T240145850_H
#ifndef U3CLOADLOCALUSERSCOREU3EC__ANONSTOREY2_T2535740323_H
#define U3CLOADLOCALUSERSCOREU3EC__ANONSTOREY2_T2535740323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/<LoadLocalUserScore>c__AnonStorey2
struct  U3CLoadLocalUserScoreU3Ec__AnonStorey2_t2535740323  : public RuntimeObject
{
public:
	// System.Action`2<System.String,UnityEngine.SocialPlatforms.IScore> EasyMobile.GameServices/<LoadLocalUserScore>c__AnonStorey2::callback
	Action_2_t387663310 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CLoadLocalUserScoreU3Ec__AnonStorey2_t2535740323, ___callback_0)); }
	inline Action_2_t387663310 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t387663310 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t387663310 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLOCALUSERSCOREU3EC__ANONSTOREY2_T2535740323_H
#ifndef GAMESERVICESITEM_T2394671562_H
#define GAMESERVICESITEM_T2394671562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServicesItem
struct  GameServicesItem_t2394671562  : public RuntimeObject
{
public:
	// System.String EasyMobile.GameServicesItem::_name
	String_t* ____name_0;
	// System.String EasyMobile.GameServicesItem::_iosId
	String_t* ____iosId_1;
	// System.String EasyMobile.GameServicesItem::_androidId
	String_t* ____androidId_2;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(GameServicesItem_t2394671562, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__iosId_1() { return static_cast<int32_t>(offsetof(GameServicesItem_t2394671562, ____iosId_1)); }
	inline String_t* get__iosId_1() const { return ____iosId_1; }
	inline String_t** get_address_of__iosId_1() { return &____iosId_1; }
	inline void set__iosId_1(String_t* value)
	{
		____iosId_1 = value;
		Il2CppCodeGenWriteBarrier((&____iosId_1), value);
	}

	inline static int32_t get_offset_of__androidId_2() { return static_cast<int32_t>(offsetof(GameServicesItem_t2394671562, ____androidId_2)); }
	inline String_t* get__androidId_2() const { return ____androidId_2; }
	inline String_t** get_address_of__androidId_2() { return &____androidId_2; }
	inline void set__androidId_2(String_t* value)
	{
		____androidId_2 = value;
		Il2CppCodeGenWriteBarrier((&____androidId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVICESITEM_T2394671562_H
#ifndef HEYZAPSETTINGS_T3928682395_H
#define HEYZAPSETTINGS_T3928682395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.HeyzapSettings
struct  HeyzapSettings_t3928682395  : public RuntimeObject
{
public:
	// System.String EasyMobile.HeyzapSettings::mPublisherId
	String_t* ___mPublisherId_0;
	// System.Boolean EasyMobile.HeyzapSettings::mShowTestSuite
	bool ___mShowTestSuite_1;
	// EasyMobile.AdPlacement[] EasyMobile.HeyzapSettings::mCustomInterstitialPlacements
	AdPlacementU5BU5D_t642510585* ___mCustomInterstitialPlacements_2;
	// EasyMobile.AdPlacement[] EasyMobile.HeyzapSettings::mCustomRewardedPlacements
	AdPlacementU5BU5D_t642510585* ___mCustomRewardedPlacements_3;

public:
	inline static int32_t get_offset_of_mPublisherId_0() { return static_cast<int32_t>(offsetof(HeyzapSettings_t3928682395, ___mPublisherId_0)); }
	inline String_t* get_mPublisherId_0() const { return ___mPublisherId_0; }
	inline String_t** get_address_of_mPublisherId_0() { return &___mPublisherId_0; }
	inline void set_mPublisherId_0(String_t* value)
	{
		___mPublisherId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPublisherId_0), value);
	}

	inline static int32_t get_offset_of_mShowTestSuite_1() { return static_cast<int32_t>(offsetof(HeyzapSettings_t3928682395, ___mShowTestSuite_1)); }
	inline bool get_mShowTestSuite_1() const { return ___mShowTestSuite_1; }
	inline bool* get_address_of_mShowTestSuite_1() { return &___mShowTestSuite_1; }
	inline void set_mShowTestSuite_1(bool value)
	{
		___mShowTestSuite_1 = value;
	}

	inline static int32_t get_offset_of_mCustomInterstitialPlacements_2() { return static_cast<int32_t>(offsetof(HeyzapSettings_t3928682395, ___mCustomInterstitialPlacements_2)); }
	inline AdPlacementU5BU5D_t642510585* get_mCustomInterstitialPlacements_2() const { return ___mCustomInterstitialPlacements_2; }
	inline AdPlacementU5BU5D_t642510585** get_address_of_mCustomInterstitialPlacements_2() { return &___mCustomInterstitialPlacements_2; }
	inline void set_mCustomInterstitialPlacements_2(AdPlacementU5BU5D_t642510585* value)
	{
		___mCustomInterstitialPlacements_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialPlacements_2), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedPlacements_3() { return static_cast<int32_t>(offsetof(HeyzapSettings_t3928682395, ___mCustomRewardedPlacements_3)); }
	inline AdPlacementU5BU5D_t642510585* get_mCustomRewardedPlacements_3() const { return ___mCustomRewardedPlacements_3; }
	inline AdPlacementU5BU5D_t642510585** get_address_of_mCustomRewardedPlacements_3() { return &___mCustomRewardedPlacements_3; }
	inline void set_mCustomRewardedPlacements_3(AdPlacementU5BU5D_t642510585* value)
	{
		___mCustomRewardedPlacements_3 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedPlacements_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEYZAPSETTINGS_T3928682395_H
#ifndef FILEUTIL_T2362248366_H
#define FILEUTIL_T2362248366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.FileUtil
struct  FileUtil_t2362248366  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTIL_T2362248366_H
#ifndef IOSSAVEDGAMENATIVE_T1863736551_H
#define IOSSAVEDGAMENATIVE_T1863736551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative
struct  iOSSavedGameNative_t1863736551  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSAVEDGAMENATIVE_T1863736551_H
#ifndef PINVOKECALLBACKUTIL_T3450891364_H
#define PINVOKECALLBACKUTIL_T3450891364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.PInvokeCallbackUtil
struct  PInvokeCallbackUtil_t3450891364  : public RuntimeObject
{
public:

public:
};

struct PInvokeCallbackUtil_t3450891364_StaticFields
{
public:
	// System.Boolean EasyMobile.Internal.PInvokeCallbackUtil::VERBOSE_DEBUG
	bool ___VERBOSE_DEBUG_0;

public:
	inline static int32_t get_offset_of_VERBOSE_DEBUG_0() { return static_cast<int32_t>(offsetof(PInvokeCallbackUtil_t3450891364_StaticFields, ___VERBOSE_DEBUG_0)); }
	inline bool get_VERBOSE_DEBUG_0() const { return ___VERBOSE_DEBUG_0; }
	inline bool* get_address_of_VERBOSE_DEBUG_0() { return &___VERBOSE_DEBUG_0; }
	inline void set_VERBOSE_DEBUG_0(bool value)
	{
		___VERBOSE_DEBUG_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINVOKECALLBACKUTIL_T3450891364_H
#ifndef PINVOKEUTIL_T2029783028_H
#define PINVOKEUTIL_T2029783028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.PInvokeUtil
struct  PInvokeUtil_t2029783028  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINVOKEUTIL_T2029783028_H
#ifndef REFLECTIONUTIL_T2959309983_H
#define REFLECTIONUTIL_T2959309983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.ReflectionUtil
struct  ReflectionUtil_t2959309983  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTIL_T2959309983_H
#ifndef SERIALIZABLEDICTIONARY_T2743353006_H
#define SERIALIZABLEDICTIONARY_T2743353006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionary
struct  SerializableDictionary_t2743353006  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_T2743353006_H
#ifndef STORAGEUTIL_T403816348_H
#define STORAGEUTIL_T403816348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.StorageUtil
struct  StorageUtil_t403816348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGEUTIL_T403816348_H
#ifndef IRONSOURCESETTINGS_T2849155294_H
#define IRONSOURCESETTINGS_T2849155294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IronSourceSettings
struct  IronSourceSettings_t2849155294  : public RuntimeObject
{
public:
	// EasyMobile.AdId EasyMobile.IronSourceSettings::mAppId
	AdId_t1082999026 * ___mAppId_0;
	// System.Boolean EasyMobile.IronSourceSettings::mUseAdvancedSetting
	bool ___mUseAdvancedSetting_1;
	// EasyMobile.IronSourceSettings/SegmentSettings EasyMobile.IronSourceSettings::mSegments
	SegmentSettings_t159419534 * ___mSegments_2;

public:
	inline static int32_t get_offset_of_mAppId_0() { return static_cast<int32_t>(offsetof(IronSourceSettings_t2849155294, ___mAppId_0)); }
	inline AdId_t1082999026 * get_mAppId_0() const { return ___mAppId_0; }
	inline AdId_t1082999026 ** get_address_of_mAppId_0() { return &___mAppId_0; }
	inline void set_mAppId_0(AdId_t1082999026 * value)
	{
		___mAppId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mAppId_0), value);
	}

	inline static int32_t get_offset_of_mUseAdvancedSetting_1() { return static_cast<int32_t>(offsetof(IronSourceSettings_t2849155294, ___mUseAdvancedSetting_1)); }
	inline bool get_mUseAdvancedSetting_1() const { return ___mUseAdvancedSetting_1; }
	inline bool* get_address_of_mUseAdvancedSetting_1() { return &___mUseAdvancedSetting_1; }
	inline void set_mUseAdvancedSetting_1(bool value)
	{
		___mUseAdvancedSetting_1 = value;
	}

	inline static int32_t get_offset_of_mSegments_2() { return static_cast<int32_t>(offsetof(IronSourceSettings_t2849155294, ___mSegments_2)); }
	inline SegmentSettings_t159419534 * get_mSegments_2() const { return ___mSegments_2; }
	inline SegmentSettings_t159419534 ** get_address_of_mSegments_2() { return &___mSegments_2; }
	inline void set_mSegments_2(SegmentSettings_t159419534 * value)
	{
		___mSegments_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSegments_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IRONSOURCESETTINGS_T2849155294_H
#ifndef SEGMENTSETTINGS_T159419534_H
#define SEGMENTSETTINGS_T159419534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IronSourceSettings/SegmentSettings
struct  SegmentSettings_t159419534  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.IronSourceSettings/SegmentSettings::age
	int32_t ___age_0;
	// System.String EasyMobile.IronSourceSettings/SegmentSettings::gender
	String_t* ___gender_1;
	// System.Int32 EasyMobile.IronSourceSettings/SegmentSettings::level
	int32_t ___level_2;
	// System.Boolean EasyMobile.IronSourceSettings/SegmentSettings::isPaying
	bool ___isPaying_3;
	// System.Int64 EasyMobile.IronSourceSettings/SegmentSettings::userCreationDate
	int64_t ___userCreationDate_4;
	// System.Double EasyMobile.IronSourceSettings/SegmentSettings::iapt
	double ___iapt_5;
	// System.String EasyMobile.IronSourceSettings/SegmentSettings::segmentName
	String_t* ___segmentName_6;
	// EasyMobile.Internal.StringStringSerializableDictionary EasyMobile.IronSourceSettings/SegmentSettings::customParams
	StringStringSerializableDictionary_t4078874517 * ___customParams_7;

public:
	inline static int32_t get_offset_of_age_0() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___age_0)); }
	inline int32_t get_age_0() const { return ___age_0; }
	inline int32_t* get_address_of_age_0() { return &___age_0; }
	inline void set_age_0(int32_t value)
	{
		___age_0 = value;
	}

	inline static int32_t get_offset_of_gender_1() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___gender_1)); }
	inline String_t* get_gender_1() const { return ___gender_1; }
	inline String_t** get_address_of_gender_1() { return &___gender_1; }
	inline void set_gender_1(String_t* value)
	{
		___gender_1 = value;
		Il2CppCodeGenWriteBarrier((&___gender_1), value);
	}

	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___level_2)); }
	inline int32_t get_level_2() const { return ___level_2; }
	inline int32_t* get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(int32_t value)
	{
		___level_2 = value;
	}

	inline static int32_t get_offset_of_isPaying_3() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___isPaying_3)); }
	inline bool get_isPaying_3() const { return ___isPaying_3; }
	inline bool* get_address_of_isPaying_3() { return &___isPaying_3; }
	inline void set_isPaying_3(bool value)
	{
		___isPaying_3 = value;
	}

	inline static int32_t get_offset_of_userCreationDate_4() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___userCreationDate_4)); }
	inline int64_t get_userCreationDate_4() const { return ___userCreationDate_4; }
	inline int64_t* get_address_of_userCreationDate_4() { return &___userCreationDate_4; }
	inline void set_userCreationDate_4(int64_t value)
	{
		___userCreationDate_4 = value;
	}

	inline static int32_t get_offset_of_iapt_5() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___iapt_5)); }
	inline double get_iapt_5() const { return ___iapt_5; }
	inline double* get_address_of_iapt_5() { return &___iapt_5; }
	inline void set_iapt_5(double value)
	{
		___iapt_5 = value;
	}

	inline static int32_t get_offset_of_segmentName_6() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___segmentName_6)); }
	inline String_t* get_segmentName_6() const { return ___segmentName_6; }
	inline String_t** get_address_of_segmentName_6() { return &___segmentName_6; }
	inline void set_segmentName_6(String_t* value)
	{
		___segmentName_6 = value;
		Il2CppCodeGenWriteBarrier((&___segmentName_6), value);
	}

	inline static int32_t get_offset_of_customParams_7() { return static_cast<int32_t>(offsetof(SegmentSettings_t159419534, ___customParams_7)); }
	inline StringStringSerializableDictionary_t4078874517 * get_customParams_7() const { return ___customParams_7; }
	inline StringStringSerializableDictionary_t4078874517 ** get_address_of_customParams_7() { return &___customParams_7; }
	inline void set_customParams_7(StringStringSerializableDictionary_t4078874517 * value)
	{
		___customParams_7 = value;
		Il2CppCodeGenWriteBarrier((&___customParams_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTSETTINGS_T159419534_H
#ifndef JSON_T2215844831_H
#define JSON_T2215844831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MiniJSON.Json
struct  Json_t2215844831  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T2215844831_H
#ifndef PARSER_T2497838200_H
#define PARSER_T2497838200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MiniJSON.Json/Parser
struct  Parser_t2497838200  : public RuntimeObject
{
public:
	// System.IO.StringReader EasyMobile.MiniJSON.Json/Parser::json
	StringReader_t3465604688 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_t2497838200, ___json_1)); }
	inline StringReader_t3465604688 * get_json_1() const { return ___json_1; }
	inline StringReader_t3465604688 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3465604688 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T2497838200_H
#ifndef SERIALIZER_T2860470690_H
#define SERIALIZER_T2860470690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MiniJSON.Json/Serializer
struct  Serializer_t2860470690  : public RuntimeObject
{
public:
	// System.Text.StringBuilder EasyMobile.MiniJSON.Json/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t2860470690, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T2860470690_H
#ifndef MOPUBSETTINGS_T3452462547_H
#define MOPUBSETTINGS_T3452462547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MoPubSettings
struct  MoPubSettings_t3452462547  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.MoPubSettings::mReportAppOpen
	bool ___mReportAppOpen_0;
	// System.String EasyMobile.MoPubSettings::mITuneAppID
	String_t* ___mITuneAppID_1;
	// System.Boolean EasyMobile.MoPubSettings::mEnableLocationPassing
	bool ___mEnableLocationPassing_2;
	// System.Boolean EasyMobile.MoPubSettings::mUseAdvancedSetting
	bool ___mUseAdvancedSetting_3;
	// EasyMobile.MoPubSettings/MediationSetting[] EasyMobile.MoPubSettings::mMediationSettings
	MediationSettingU5BU5D_t2663728768* ___mMediationSettings_4;
	// EasyMobile.MoPubSettings/MoPubInitNetwork[] EasyMobile.MoPubSettings::mInitNetworks
	MoPubInitNetworkU5BU5D_t564488331* ___mInitNetworks_5;
	// EasyMobile.MoPubSettings/MoPubAdvancedBidder[] EasyMobile.MoPubSettings::mAdvancedBidders
	MoPubAdvancedBidderU5BU5D_t2407845123* ___mAdvancedBidders_6;
	// System.Boolean EasyMobile.MoPubSettings::mAutoRequestConsent
	bool ___mAutoRequestConsent_7;
	// System.Boolean EasyMobile.MoPubSettings::mForceGdprApplicable
	bool ___mForceGdprApplicable_8;
	// EasyMobile.AdId EasyMobile.MoPubSettings::mDefaultBannerId
	AdId_t1082999026 * ___mDefaultBannerId_9;
	// EasyMobile.AdId EasyMobile.MoPubSettings::mDefaultInterstitialAdId
	AdId_t1082999026 * ___mDefaultInterstitialAdId_10;
	// EasyMobile.AdId EasyMobile.MoPubSettings::mDefaultRewardedAdId
	AdId_t1082999026 * ___mDefaultRewardedAdId_11;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.MoPubSettings::mCustomBannerIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomBannerIds_12;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.MoPubSettings::mCustomInterstitialAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomInterstitialAdIds_13;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.MoPubSettings::mCustomRewardedAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomRewardedAdIds_14;

public:
	inline static int32_t get_offset_of_mReportAppOpen_0() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mReportAppOpen_0)); }
	inline bool get_mReportAppOpen_0() const { return ___mReportAppOpen_0; }
	inline bool* get_address_of_mReportAppOpen_0() { return &___mReportAppOpen_0; }
	inline void set_mReportAppOpen_0(bool value)
	{
		___mReportAppOpen_0 = value;
	}

	inline static int32_t get_offset_of_mITuneAppID_1() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mITuneAppID_1)); }
	inline String_t* get_mITuneAppID_1() const { return ___mITuneAppID_1; }
	inline String_t** get_address_of_mITuneAppID_1() { return &___mITuneAppID_1; }
	inline void set_mITuneAppID_1(String_t* value)
	{
		___mITuneAppID_1 = value;
		Il2CppCodeGenWriteBarrier((&___mITuneAppID_1), value);
	}

	inline static int32_t get_offset_of_mEnableLocationPassing_2() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mEnableLocationPassing_2)); }
	inline bool get_mEnableLocationPassing_2() const { return ___mEnableLocationPassing_2; }
	inline bool* get_address_of_mEnableLocationPassing_2() { return &___mEnableLocationPassing_2; }
	inline void set_mEnableLocationPassing_2(bool value)
	{
		___mEnableLocationPassing_2 = value;
	}

	inline static int32_t get_offset_of_mUseAdvancedSetting_3() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mUseAdvancedSetting_3)); }
	inline bool get_mUseAdvancedSetting_3() const { return ___mUseAdvancedSetting_3; }
	inline bool* get_address_of_mUseAdvancedSetting_3() { return &___mUseAdvancedSetting_3; }
	inline void set_mUseAdvancedSetting_3(bool value)
	{
		___mUseAdvancedSetting_3 = value;
	}

	inline static int32_t get_offset_of_mMediationSettings_4() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mMediationSettings_4)); }
	inline MediationSettingU5BU5D_t2663728768* get_mMediationSettings_4() const { return ___mMediationSettings_4; }
	inline MediationSettingU5BU5D_t2663728768** get_address_of_mMediationSettings_4() { return &___mMediationSettings_4; }
	inline void set_mMediationSettings_4(MediationSettingU5BU5D_t2663728768* value)
	{
		___mMediationSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMediationSettings_4), value);
	}

	inline static int32_t get_offset_of_mInitNetworks_5() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mInitNetworks_5)); }
	inline MoPubInitNetworkU5BU5D_t564488331* get_mInitNetworks_5() const { return ___mInitNetworks_5; }
	inline MoPubInitNetworkU5BU5D_t564488331** get_address_of_mInitNetworks_5() { return &___mInitNetworks_5; }
	inline void set_mInitNetworks_5(MoPubInitNetworkU5BU5D_t564488331* value)
	{
		___mInitNetworks_5 = value;
		Il2CppCodeGenWriteBarrier((&___mInitNetworks_5), value);
	}

	inline static int32_t get_offset_of_mAdvancedBidders_6() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mAdvancedBidders_6)); }
	inline MoPubAdvancedBidderU5BU5D_t2407845123* get_mAdvancedBidders_6() const { return ___mAdvancedBidders_6; }
	inline MoPubAdvancedBidderU5BU5D_t2407845123** get_address_of_mAdvancedBidders_6() { return &___mAdvancedBidders_6; }
	inline void set_mAdvancedBidders_6(MoPubAdvancedBidderU5BU5D_t2407845123* value)
	{
		___mAdvancedBidders_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAdvancedBidders_6), value);
	}

	inline static int32_t get_offset_of_mAutoRequestConsent_7() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mAutoRequestConsent_7)); }
	inline bool get_mAutoRequestConsent_7() const { return ___mAutoRequestConsent_7; }
	inline bool* get_address_of_mAutoRequestConsent_7() { return &___mAutoRequestConsent_7; }
	inline void set_mAutoRequestConsent_7(bool value)
	{
		___mAutoRequestConsent_7 = value;
	}

	inline static int32_t get_offset_of_mForceGdprApplicable_8() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mForceGdprApplicable_8)); }
	inline bool get_mForceGdprApplicable_8() const { return ___mForceGdprApplicable_8; }
	inline bool* get_address_of_mForceGdprApplicable_8() { return &___mForceGdprApplicable_8; }
	inline void set_mForceGdprApplicable_8(bool value)
	{
		___mForceGdprApplicable_8 = value;
	}

	inline static int32_t get_offset_of_mDefaultBannerId_9() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mDefaultBannerId_9)); }
	inline AdId_t1082999026 * get_mDefaultBannerId_9() const { return ___mDefaultBannerId_9; }
	inline AdId_t1082999026 ** get_address_of_mDefaultBannerId_9() { return &___mDefaultBannerId_9; }
	inline void set_mDefaultBannerId_9(AdId_t1082999026 * value)
	{
		___mDefaultBannerId_9 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultBannerId_9), value);
	}

	inline static int32_t get_offset_of_mDefaultInterstitialAdId_10() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mDefaultInterstitialAdId_10)); }
	inline AdId_t1082999026 * get_mDefaultInterstitialAdId_10() const { return ___mDefaultInterstitialAdId_10; }
	inline AdId_t1082999026 ** get_address_of_mDefaultInterstitialAdId_10() { return &___mDefaultInterstitialAdId_10; }
	inline void set_mDefaultInterstitialAdId_10(AdId_t1082999026 * value)
	{
		___mDefaultInterstitialAdId_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultInterstitialAdId_10), value);
	}

	inline static int32_t get_offset_of_mDefaultRewardedAdId_11() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mDefaultRewardedAdId_11)); }
	inline AdId_t1082999026 * get_mDefaultRewardedAdId_11() const { return ___mDefaultRewardedAdId_11; }
	inline AdId_t1082999026 ** get_address_of_mDefaultRewardedAdId_11() { return &___mDefaultRewardedAdId_11; }
	inline void set_mDefaultRewardedAdId_11(AdId_t1082999026 * value)
	{
		___mDefaultRewardedAdId_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRewardedAdId_11), value);
	}

	inline static int32_t get_offset_of_mCustomBannerIds_12() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mCustomBannerIds_12)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomBannerIds_12() const { return ___mCustomBannerIds_12; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomBannerIds_12() { return &___mCustomBannerIds_12; }
	inline void set_mCustomBannerIds_12(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomBannerIds_12 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomBannerIds_12), value);
	}

	inline static int32_t get_offset_of_mCustomInterstitialAdIds_13() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mCustomInterstitialAdIds_13)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomInterstitialAdIds_13() const { return ___mCustomInterstitialAdIds_13; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomInterstitialAdIds_13() { return &___mCustomInterstitialAdIds_13; }
	inline void set_mCustomInterstitialAdIds_13(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomInterstitialAdIds_13 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialAdIds_13), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedAdIds_14() { return static_cast<int32_t>(offsetof(MoPubSettings_t3452462547, ___mCustomRewardedAdIds_14)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomRewardedAdIds_14() const { return ___mCustomRewardedAdIds_14; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomRewardedAdIds_14() { return &___mCustomRewardedAdIds_14; }
	inline void set_mCustomRewardedAdIds_14(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomRewardedAdIds_14 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedAdIds_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBSETTINGS_T3452462547_H
#ifndef MEDIATIONSETTING_T2014171693_H
#define MEDIATIONSETTING_T2014171693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MoPubSettings/MediationSetting
struct  MediationSetting_t2014171693  : public RuntimeObject
{
public:
	// System.String EasyMobile.MoPubSettings/MediationSetting::mAdVendor
	String_t* ___mAdVendor_0;
	// EasyMobile.Internal.StringStringSerializableDictionary EasyMobile.MoPubSettings/MediationSetting::mExtraOptions
	StringStringSerializableDictionary_t4078874517 * ___mExtraOptions_1;

public:
	inline static int32_t get_offset_of_mAdVendor_0() { return static_cast<int32_t>(offsetof(MediationSetting_t2014171693, ___mAdVendor_0)); }
	inline String_t* get_mAdVendor_0() const { return ___mAdVendor_0; }
	inline String_t** get_address_of_mAdVendor_0() { return &___mAdVendor_0; }
	inline void set_mAdVendor_0(String_t* value)
	{
		___mAdVendor_0 = value;
		Il2CppCodeGenWriteBarrier((&___mAdVendor_0), value);
	}

	inline static int32_t get_offset_of_mExtraOptions_1() { return static_cast<int32_t>(offsetof(MediationSetting_t2014171693, ___mExtraOptions_1)); }
	inline StringStringSerializableDictionary_t4078874517 * get_mExtraOptions_1() const { return ___mExtraOptions_1; }
	inline StringStringSerializableDictionary_t4078874517 ** get_address_of_mExtraOptions_1() { return &___mExtraOptions_1; }
	inline void set_mExtraOptions_1(StringStringSerializableDictionary_t4078874517 * value)
	{
		___mExtraOptions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mExtraOptions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATIONSETTING_T2014171693_H
#ifndef RUNTIMEMANAGER_T3497363304_H
#define RUNTIMEMANAGER_T3497363304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.RuntimeManager
struct  RuntimeManager_t3497363304  : public RuntimeObject
{
public:

public:
};

struct RuntimeManager_t3497363304_StaticFields
{
public:
	// System.Boolean EasyMobile.RuntimeManager::mIsInitialized
	bool ___mIsInitialized_1;

public:
	inline static int32_t get_offset_of_mIsInitialized_1() { return static_cast<int32_t>(offsetof(RuntimeManager_t3497363304_StaticFields, ___mIsInitialized_1)); }
	inline bool get_mIsInitialized_1() const { return ___mIsInitialized_1; }
	inline bool* get_address_of_mIsInitialized_1() { return &___mIsInitialized_1; }
	inline void set_mIsInitialized_1(bool value)
	{
		___mIsInitialized_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEMANAGER_T3497363304_H
#ifndef TAPJOYSETTINGS_T205532585_H
#define TAPJOYSETTINGS_T205532585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.TapjoySettings
struct  TapjoySettings_t205532585  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.TapjoySettings::mAutoReconnect
	bool ___mAutoReconnect_0;
	// System.Single EasyMobile.TapjoySettings::mAutoReconnectInterval
	float ___mAutoReconnectInterval_1;
	// EasyMobile.AdId EasyMobile.TapjoySettings::mDefaultInterstitialAdId
	AdId_t1082999026 * ___mDefaultInterstitialAdId_2;
	// EasyMobile.AdId EasyMobile.TapjoySettings::mDefaultRewardedAdId
	AdId_t1082999026 * ___mDefaultRewardedAdId_3;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.TapjoySettings::mCustomInterstitialAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomInterstitialAdIds_4;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.TapjoySettings::mCustomRewardedAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomRewardedAdIds_5;

public:
	inline static int32_t get_offset_of_mAutoReconnect_0() { return static_cast<int32_t>(offsetof(TapjoySettings_t205532585, ___mAutoReconnect_0)); }
	inline bool get_mAutoReconnect_0() const { return ___mAutoReconnect_0; }
	inline bool* get_address_of_mAutoReconnect_0() { return &___mAutoReconnect_0; }
	inline void set_mAutoReconnect_0(bool value)
	{
		___mAutoReconnect_0 = value;
	}

	inline static int32_t get_offset_of_mAutoReconnectInterval_1() { return static_cast<int32_t>(offsetof(TapjoySettings_t205532585, ___mAutoReconnectInterval_1)); }
	inline float get_mAutoReconnectInterval_1() const { return ___mAutoReconnectInterval_1; }
	inline float* get_address_of_mAutoReconnectInterval_1() { return &___mAutoReconnectInterval_1; }
	inline void set_mAutoReconnectInterval_1(float value)
	{
		___mAutoReconnectInterval_1 = value;
	}

	inline static int32_t get_offset_of_mDefaultInterstitialAdId_2() { return static_cast<int32_t>(offsetof(TapjoySettings_t205532585, ___mDefaultInterstitialAdId_2)); }
	inline AdId_t1082999026 * get_mDefaultInterstitialAdId_2() const { return ___mDefaultInterstitialAdId_2; }
	inline AdId_t1082999026 ** get_address_of_mDefaultInterstitialAdId_2() { return &___mDefaultInterstitialAdId_2; }
	inline void set_mDefaultInterstitialAdId_2(AdId_t1082999026 * value)
	{
		___mDefaultInterstitialAdId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultInterstitialAdId_2), value);
	}

	inline static int32_t get_offset_of_mDefaultRewardedAdId_3() { return static_cast<int32_t>(offsetof(TapjoySettings_t205532585, ___mDefaultRewardedAdId_3)); }
	inline AdId_t1082999026 * get_mDefaultRewardedAdId_3() const { return ___mDefaultRewardedAdId_3; }
	inline AdId_t1082999026 ** get_address_of_mDefaultRewardedAdId_3() { return &___mDefaultRewardedAdId_3; }
	inline void set_mDefaultRewardedAdId_3(AdId_t1082999026 * value)
	{
		___mDefaultRewardedAdId_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRewardedAdId_3), value);
	}

	inline static int32_t get_offset_of_mCustomInterstitialAdIds_4() { return static_cast<int32_t>(offsetof(TapjoySettings_t205532585, ___mCustomInterstitialAdIds_4)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomInterstitialAdIds_4() const { return ___mCustomInterstitialAdIds_4; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomInterstitialAdIds_4() { return &___mCustomInterstitialAdIds_4; }
	inline void set_mCustomInterstitialAdIds_4(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomInterstitialAdIds_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialAdIds_4), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedAdIds_5() { return static_cast<int32_t>(offsetof(TapjoySettings_t205532585, ___mCustomRewardedAdIds_5)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomRewardedAdIds_5() const { return ___mCustomRewardedAdIds_5; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomRewardedAdIds_5() { return &___mCustomRewardedAdIds_5; }
	inline void set_mCustomRewardedAdIds_5(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomRewardedAdIds_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedAdIds_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPJOYSETTINGS_T205532585_H
#ifndef U3CINTERNALSHOWINTERSTITIALADU3EC__ANONSTOREY0_T4024241046_H
#define U3CINTERNALSHOWINTERSTITIALADU3EC__ANONSTOREY0_T4024241046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.UnityAdsClientImpl/<InternalShowInterstitialAd>c__AnonStorey0
struct  U3CInternalShowInterstitialAdU3Ec__AnonStorey0_t4024241046  : public RuntimeObject
{
public:
	// EasyMobile.AdPlacement EasyMobile.UnityAdsClientImpl/<InternalShowInterstitialAd>c__AnonStorey0::placement
	AdPlacement_t3195089512 * ___placement_0;
	// EasyMobile.UnityAdsClientImpl EasyMobile.UnityAdsClientImpl/<InternalShowInterstitialAd>c__AnonStorey0::$this
	UnityAdsClientImpl_t4105509132 * ___U24this_1;

public:
	inline static int32_t get_offset_of_placement_0() { return static_cast<int32_t>(offsetof(U3CInternalShowInterstitialAdU3Ec__AnonStorey0_t4024241046, ___placement_0)); }
	inline AdPlacement_t3195089512 * get_placement_0() const { return ___placement_0; }
	inline AdPlacement_t3195089512 ** get_address_of_placement_0() { return &___placement_0; }
	inline void set_placement_0(AdPlacement_t3195089512 * value)
	{
		___placement_0 = value;
		Il2CppCodeGenWriteBarrier((&___placement_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInternalShowInterstitialAdU3Ec__AnonStorey0_t4024241046, ___U24this_1)); }
	inline UnityAdsClientImpl_t4105509132 * get_U24this_1() const { return ___U24this_1; }
	inline UnityAdsClientImpl_t4105509132 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UnityAdsClientImpl_t4105509132 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALSHOWINTERSTITIALADU3EC__ANONSTOREY0_T4024241046_H
#ifndef U3CINTERNALSHOWREWARDEDADU3EC__ANONSTOREY1_T1483428000_H
#define U3CINTERNALSHOWREWARDEDADU3EC__ANONSTOREY1_T1483428000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.UnityAdsClientImpl/<InternalShowRewardedAd>c__AnonStorey1
struct  U3CInternalShowRewardedAdU3Ec__AnonStorey1_t1483428000  : public RuntimeObject
{
public:
	// EasyMobile.AdPlacement EasyMobile.UnityAdsClientImpl/<InternalShowRewardedAd>c__AnonStorey1::placement
	AdPlacement_t3195089512 * ___placement_0;
	// EasyMobile.UnityAdsClientImpl EasyMobile.UnityAdsClientImpl/<InternalShowRewardedAd>c__AnonStorey1::$this
	UnityAdsClientImpl_t4105509132 * ___U24this_1;

public:
	inline static int32_t get_offset_of_placement_0() { return static_cast<int32_t>(offsetof(U3CInternalShowRewardedAdU3Ec__AnonStorey1_t1483428000, ___placement_0)); }
	inline AdPlacement_t3195089512 * get_placement_0() const { return ___placement_0; }
	inline AdPlacement_t3195089512 ** get_address_of_placement_0() { return &___placement_0; }
	inline void set_placement_0(AdPlacement_t3195089512 * value)
	{
		___placement_0 = value;
		Il2CppCodeGenWriteBarrier((&___placement_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInternalShowRewardedAdU3Ec__AnonStorey1_t1483428000, ___U24this_1)); }
	inline UnityAdsClientImpl_t4105509132 * get_U24this_1() const { return ___U24this_1; }
	inline UnityAdsClientImpl_t4105509132 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UnityAdsClientImpl_t4105509132 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALSHOWREWARDEDADU3EC__ANONSTOREY1_T1483428000_H
#ifndef UNITYADSSETTINGS_T2563580717_H
#define UNITYADSSETTINGS_T2563580717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.UnityAdsSettings
struct  UnityAdsSettings_t2563580717  : public RuntimeObject
{
public:
	// EasyMobile.AdId EasyMobile.UnityAdsSettings::mDefaultInterstitialAdId
	AdId_t1082999026 * ___mDefaultInterstitialAdId_2;
	// EasyMobile.AdId EasyMobile.UnityAdsSettings::mDefaultRewardedAdId
	AdId_t1082999026 * ___mDefaultRewardedAdId_3;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.UnityAdsSettings::mCustomInterstitialAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomInterstitialAdIds_4;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.UnityAdsSettings::mCustomRewardedAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomRewardedAdIds_5;

public:
	inline static int32_t get_offset_of_mDefaultInterstitialAdId_2() { return static_cast<int32_t>(offsetof(UnityAdsSettings_t2563580717, ___mDefaultInterstitialAdId_2)); }
	inline AdId_t1082999026 * get_mDefaultInterstitialAdId_2() const { return ___mDefaultInterstitialAdId_2; }
	inline AdId_t1082999026 ** get_address_of_mDefaultInterstitialAdId_2() { return &___mDefaultInterstitialAdId_2; }
	inline void set_mDefaultInterstitialAdId_2(AdId_t1082999026 * value)
	{
		___mDefaultInterstitialAdId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultInterstitialAdId_2), value);
	}

	inline static int32_t get_offset_of_mDefaultRewardedAdId_3() { return static_cast<int32_t>(offsetof(UnityAdsSettings_t2563580717, ___mDefaultRewardedAdId_3)); }
	inline AdId_t1082999026 * get_mDefaultRewardedAdId_3() const { return ___mDefaultRewardedAdId_3; }
	inline AdId_t1082999026 ** get_address_of_mDefaultRewardedAdId_3() { return &___mDefaultRewardedAdId_3; }
	inline void set_mDefaultRewardedAdId_3(AdId_t1082999026 * value)
	{
		___mDefaultRewardedAdId_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRewardedAdId_3), value);
	}

	inline static int32_t get_offset_of_mCustomInterstitialAdIds_4() { return static_cast<int32_t>(offsetof(UnityAdsSettings_t2563580717, ___mCustomInterstitialAdIds_4)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomInterstitialAdIds_4() const { return ___mCustomInterstitialAdIds_4; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomInterstitialAdIds_4() { return &___mCustomInterstitialAdIds_4; }
	inline void set_mCustomInterstitialAdIds_4(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomInterstitialAdIds_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialAdIds_4), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedAdIds_5() { return static_cast<int32_t>(offsetof(UnityAdsSettings_t2563580717, ___mCustomRewardedAdIds_5)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomRewardedAdIds_5() const { return ___mCustomRewardedAdIds_5; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomRewardedAdIds_5() { return &___mCustomRewardedAdIds_5; }
	inline void set_mCustomRewardedAdIds_5(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomRewardedAdIds_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedAdIds_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSSETTINGS_T2563580717_H
#ifndef DICTIONARY_2_T228976570_H
#define DICTIONARY_2_T228976570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<EasyMobile.AdPlacement,EasyMobile.AdId>
struct  Dictionary_2_t228976570  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t944158999* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t418652041 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t1945020888 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___entries_1)); }
	inline EntryU5BU5D_t944158999* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t944158999** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t944158999* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___keys_7)); }
	inline KeyCollection_t418652041 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t418652041 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t418652041 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ___values_8)); }
	inline ValueCollection_t1945020888 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t1945020888 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t1945020888 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t228976570, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T228976570_H
#ifndef DICTIONARY_2_T868255325_H
#define DICTIONARY_2_T868255325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.AdId>
struct  Dictionary_2_t868255325  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t1755157640* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t1057930796 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t2584299643 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___entries_1)); }
	inline EntryU5BU5D_t1755157640* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t1755157640** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t1755157640* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___keys_7)); }
	inline KeyCollection_t1057930796 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t1057930796 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t1057930796 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ___values_8)); }
	inline ValueCollection_t2584299643 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t2584299643 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t2584299643 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t868255325, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T868255325_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t885026589* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t1822382459 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t3348751306 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___entries_1)); }
	inline EntryU5BU5D_t885026589* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t885026589** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t885026589* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keys_7)); }
	inline KeyCollection_t1822382459 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t1822382459 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t1822382459 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___values_8)); }
	inline ValueCollection_t3348751306 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t3348751306 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t3348751306 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ACHIEVEMENT_T2708948008_H
#define ACHIEVEMENT_T2708948008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Achievement
struct  Achievement_t2708948008  : public GameServicesItem_t2394671562
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACHIEVEMENT_T2708948008_H
#ifndef ADCOLONYCLIENTIMPL_T352798180_H
#define ADCOLONYCLIENTIMPL_T352798180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdColonyClientImpl
struct  AdColonyClientImpl_t352798180  : public AdClientImpl_t879350116
{
public:

public:
};

struct AdColonyClientImpl_t352798180_StaticFields
{
public:
	// EasyMobile.AdColonyClientImpl EasyMobile.AdColonyClientImpl::sInstance
	AdColonyClientImpl_t352798180 * ___sInstance_7;

public:
	inline static int32_t get_offset_of_sInstance_7() { return static_cast<int32_t>(offsetof(AdColonyClientImpl_t352798180_StaticFields, ___sInstance_7)); }
	inline AdColonyClientImpl_t352798180 * get_sInstance_7() const { return ___sInstance_7; }
	inline AdColonyClientImpl_t352798180 ** get_address_of_sInstance_7() { return &___sInstance_7; }
	inline void set_sInstance_7(AdColonyClientImpl_t352798180 * value)
	{
		___sInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADCOLONYCLIENTIMPL_T352798180_H
#ifndef ADID_T1082999026_H
#define ADID_T1082999026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdId
struct  AdId_t1082999026  : public CrossPlatformId_t1390234332
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADID_T1082999026_H
#ifndef ADMOBCLIENTIMPL_T3823742600_H
#define ADMOBCLIENTIMPL_T3823742600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdMobClientImpl
struct  AdMobClientImpl_t3823742600  : public AdClientImpl_t879350116
{
public:

public:
};

struct AdMobClientImpl_t3823742600_StaticFields
{
public:
	// EasyMobile.AdMobClientImpl EasyMobile.AdMobClientImpl::sInstance
	AdMobClientImpl_t3823742600 * ___sInstance_6;

public:
	inline static int32_t get_offset_of_sInstance_6() { return static_cast<int32_t>(offsetof(AdMobClientImpl_t3823742600_StaticFields, ___sInstance_6)); }
	inline AdMobClientImpl_t3823742600 * get_sInstance_6() const { return ___sInstance_6; }
	inline AdMobClientImpl_t3823742600 ** get_address_of_sInstance_6() { return &___sInstance_6; }
	inline void set_sInstance_6(AdMobClientImpl_t3823742600 * value)
	{
		___sInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMOBCLIENTIMPL_T3823742600_H
#ifndef ADPLACEMENT_T3195089512_H
#define ADPLACEMENT_T3195089512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdPlacement
struct  AdPlacement_t3195089512  : public AdLocation_t1108752805
{
public:
	// System.String EasyMobile.AdPlacement::mName
	String_t* ___mName_20;

public:
	inline static int32_t get_offset_of_mName_20() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512, ___mName_20)); }
	inline String_t* get_mName_20() const { return ___mName_20; }
	inline String_t** get_address_of_mName_20() { return &___mName_20; }
	inline void set_mName_20(String_t* value)
	{
		___mName_20 = value;
		Il2CppCodeGenWriteBarrier((&___mName_20), value);
	}
};

struct AdPlacement_t3195089512_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.AdPlacement> EasyMobile.AdPlacement::sCustomPlacements
	Dictionary_2_t2980345811 * ___sCustomPlacements_19;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Default
	AdPlacement_t3195089512 * ___Default_21;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Startup
	AdPlacement_t3195089512 * ___Startup_22;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::HomeScreen
	AdPlacement_t3195089512 * ___HomeScreen_23;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::MainMenu
	AdPlacement_t3195089512 * ___MainMenu_24;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::GameScreen
	AdPlacement_t3195089512 * ___GameScreen_25;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Achievements
	AdPlacement_t3195089512 * ___Achievements_26;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::LevelStart
	AdPlacement_t3195089512 * ___LevelStart_27;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::LevelComplete
	AdPlacement_t3195089512 * ___LevelComplete_28;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::TurnComplete
	AdPlacement_t3195089512 * ___TurnComplete_29;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Quests
	AdPlacement_t3195089512 * ___Quests_30;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Pause
	AdPlacement_t3195089512 * ___Pause_31;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::IAPStore
	AdPlacement_t3195089512 * ___IAPStore_32;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::ItemStore
	AdPlacement_t3195089512 * ___ItemStore_33;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::GameOver
	AdPlacement_t3195089512 * ___GameOver_34;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Leaderboard
	AdPlacement_t3195089512 * ___Leaderboard_35;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Settings
	AdPlacement_t3195089512 * ___Settings_36;
	// EasyMobile.AdPlacement EasyMobile.AdPlacement::Quit
	AdPlacement_t3195089512 * ___Quit_37;

public:
	inline static int32_t get_offset_of_sCustomPlacements_19() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___sCustomPlacements_19)); }
	inline Dictionary_2_t2980345811 * get_sCustomPlacements_19() const { return ___sCustomPlacements_19; }
	inline Dictionary_2_t2980345811 ** get_address_of_sCustomPlacements_19() { return &___sCustomPlacements_19; }
	inline void set_sCustomPlacements_19(Dictionary_2_t2980345811 * value)
	{
		___sCustomPlacements_19 = value;
		Il2CppCodeGenWriteBarrier((&___sCustomPlacements_19), value);
	}

	inline static int32_t get_offset_of_Default_21() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Default_21)); }
	inline AdPlacement_t3195089512 * get_Default_21() const { return ___Default_21; }
	inline AdPlacement_t3195089512 ** get_address_of_Default_21() { return &___Default_21; }
	inline void set_Default_21(AdPlacement_t3195089512 * value)
	{
		___Default_21 = value;
		Il2CppCodeGenWriteBarrier((&___Default_21), value);
	}

	inline static int32_t get_offset_of_Startup_22() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Startup_22)); }
	inline AdPlacement_t3195089512 * get_Startup_22() const { return ___Startup_22; }
	inline AdPlacement_t3195089512 ** get_address_of_Startup_22() { return &___Startup_22; }
	inline void set_Startup_22(AdPlacement_t3195089512 * value)
	{
		___Startup_22 = value;
		Il2CppCodeGenWriteBarrier((&___Startup_22), value);
	}

	inline static int32_t get_offset_of_HomeScreen_23() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___HomeScreen_23)); }
	inline AdPlacement_t3195089512 * get_HomeScreen_23() const { return ___HomeScreen_23; }
	inline AdPlacement_t3195089512 ** get_address_of_HomeScreen_23() { return &___HomeScreen_23; }
	inline void set_HomeScreen_23(AdPlacement_t3195089512 * value)
	{
		___HomeScreen_23 = value;
		Il2CppCodeGenWriteBarrier((&___HomeScreen_23), value);
	}

	inline static int32_t get_offset_of_MainMenu_24() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___MainMenu_24)); }
	inline AdPlacement_t3195089512 * get_MainMenu_24() const { return ___MainMenu_24; }
	inline AdPlacement_t3195089512 ** get_address_of_MainMenu_24() { return &___MainMenu_24; }
	inline void set_MainMenu_24(AdPlacement_t3195089512 * value)
	{
		___MainMenu_24 = value;
		Il2CppCodeGenWriteBarrier((&___MainMenu_24), value);
	}

	inline static int32_t get_offset_of_GameScreen_25() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___GameScreen_25)); }
	inline AdPlacement_t3195089512 * get_GameScreen_25() const { return ___GameScreen_25; }
	inline AdPlacement_t3195089512 ** get_address_of_GameScreen_25() { return &___GameScreen_25; }
	inline void set_GameScreen_25(AdPlacement_t3195089512 * value)
	{
		___GameScreen_25 = value;
		Il2CppCodeGenWriteBarrier((&___GameScreen_25), value);
	}

	inline static int32_t get_offset_of_Achievements_26() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Achievements_26)); }
	inline AdPlacement_t3195089512 * get_Achievements_26() const { return ___Achievements_26; }
	inline AdPlacement_t3195089512 ** get_address_of_Achievements_26() { return &___Achievements_26; }
	inline void set_Achievements_26(AdPlacement_t3195089512 * value)
	{
		___Achievements_26 = value;
		Il2CppCodeGenWriteBarrier((&___Achievements_26), value);
	}

	inline static int32_t get_offset_of_LevelStart_27() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___LevelStart_27)); }
	inline AdPlacement_t3195089512 * get_LevelStart_27() const { return ___LevelStart_27; }
	inline AdPlacement_t3195089512 ** get_address_of_LevelStart_27() { return &___LevelStart_27; }
	inline void set_LevelStart_27(AdPlacement_t3195089512 * value)
	{
		___LevelStart_27 = value;
		Il2CppCodeGenWriteBarrier((&___LevelStart_27), value);
	}

	inline static int32_t get_offset_of_LevelComplete_28() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___LevelComplete_28)); }
	inline AdPlacement_t3195089512 * get_LevelComplete_28() const { return ___LevelComplete_28; }
	inline AdPlacement_t3195089512 ** get_address_of_LevelComplete_28() { return &___LevelComplete_28; }
	inline void set_LevelComplete_28(AdPlacement_t3195089512 * value)
	{
		___LevelComplete_28 = value;
		Il2CppCodeGenWriteBarrier((&___LevelComplete_28), value);
	}

	inline static int32_t get_offset_of_TurnComplete_29() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___TurnComplete_29)); }
	inline AdPlacement_t3195089512 * get_TurnComplete_29() const { return ___TurnComplete_29; }
	inline AdPlacement_t3195089512 ** get_address_of_TurnComplete_29() { return &___TurnComplete_29; }
	inline void set_TurnComplete_29(AdPlacement_t3195089512 * value)
	{
		___TurnComplete_29 = value;
		Il2CppCodeGenWriteBarrier((&___TurnComplete_29), value);
	}

	inline static int32_t get_offset_of_Quests_30() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Quests_30)); }
	inline AdPlacement_t3195089512 * get_Quests_30() const { return ___Quests_30; }
	inline AdPlacement_t3195089512 ** get_address_of_Quests_30() { return &___Quests_30; }
	inline void set_Quests_30(AdPlacement_t3195089512 * value)
	{
		___Quests_30 = value;
		Il2CppCodeGenWriteBarrier((&___Quests_30), value);
	}

	inline static int32_t get_offset_of_Pause_31() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Pause_31)); }
	inline AdPlacement_t3195089512 * get_Pause_31() const { return ___Pause_31; }
	inline AdPlacement_t3195089512 ** get_address_of_Pause_31() { return &___Pause_31; }
	inline void set_Pause_31(AdPlacement_t3195089512 * value)
	{
		___Pause_31 = value;
		Il2CppCodeGenWriteBarrier((&___Pause_31), value);
	}

	inline static int32_t get_offset_of_IAPStore_32() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___IAPStore_32)); }
	inline AdPlacement_t3195089512 * get_IAPStore_32() const { return ___IAPStore_32; }
	inline AdPlacement_t3195089512 ** get_address_of_IAPStore_32() { return &___IAPStore_32; }
	inline void set_IAPStore_32(AdPlacement_t3195089512 * value)
	{
		___IAPStore_32 = value;
		Il2CppCodeGenWriteBarrier((&___IAPStore_32), value);
	}

	inline static int32_t get_offset_of_ItemStore_33() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___ItemStore_33)); }
	inline AdPlacement_t3195089512 * get_ItemStore_33() const { return ___ItemStore_33; }
	inline AdPlacement_t3195089512 ** get_address_of_ItemStore_33() { return &___ItemStore_33; }
	inline void set_ItemStore_33(AdPlacement_t3195089512 * value)
	{
		___ItemStore_33 = value;
		Il2CppCodeGenWriteBarrier((&___ItemStore_33), value);
	}

	inline static int32_t get_offset_of_GameOver_34() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___GameOver_34)); }
	inline AdPlacement_t3195089512 * get_GameOver_34() const { return ___GameOver_34; }
	inline AdPlacement_t3195089512 ** get_address_of_GameOver_34() { return &___GameOver_34; }
	inline void set_GameOver_34(AdPlacement_t3195089512 * value)
	{
		___GameOver_34 = value;
		Il2CppCodeGenWriteBarrier((&___GameOver_34), value);
	}

	inline static int32_t get_offset_of_Leaderboard_35() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Leaderboard_35)); }
	inline AdPlacement_t3195089512 * get_Leaderboard_35() const { return ___Leaderboard_35; }
	inline AdPlacement_t3195089512 ** get_address_of_Leaderboard_35() { return &___Leaderboard_35; }
	inline void set_Leaderboard_35(AdPlacement_t3195089512 * value)
	{
		___Leaderboard_35 = value;
		Il2CppCodeGenWriteBarrier((&___Leaderboard_35), value);
	}

	inline static int32_t get_offset_of_Settings_36() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Settings_36)); }
	inline AdPlacement_t3195089512 * get_Settings_36() const { return ___Settings_36; }
	inline AdPlacement_t3195089512 ** get_address_of_Settings_36() { return &___Settings_36; }
	inline void set_Settings_36(AdPlacement_t3195089512 * value)
	{
		___Settings_36 = value;
		Il2CppCodeGenWriteBarrier((&___Settings_36), value);
	}

	inline static int32_t get_offset_of_Quit_37() { return static_cast<int32_t>(offsetof(AdPlacement_t3195089512_StaticFields, ___Quit_37)); }
	inline AdPlacement_t3195089512 * get_Quit_37() const { return ___Quit_37; }
	inline AdPlacement_t3195089512 ** get_address_of_Quit_37() { return &___Quit_37; }
	inline void set_Quit_37(AdPlacement_t3195089512 * value)
	{
		___Quit_37 = value;
		Il2CppCodeGenWriteBarrier((&___Quit_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADPLACEMENT_T3195089512_H
#ifndef ADVERTISINGCONSENTMANAGER_T2195563647_H
#define ADVERTISINGCONSENTMANAGER_T2195563647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdvertisingConsentManager
struct  AdvertisingConsentManager_t2195563647  : public ConsentManager_t1411390104
{
public:

public:
};

struct AdvertisingConsentManager_t2195563647_StaticFields
{
public:
	// EasyMobile.AdvertisingConsentManager EasyMobile.AdvertisingConsentManager::sInstance
	AdvertisingConsentManager_t2195563647 * ___sInstance_3;

public:
	inline static int32_t get_offset_of_sInstance_3() { return static_cast<int32_t>(offsetof(AdvertisingConsentManager_t2195563647_StaticFields, ___sInstance_3)); }
	inline AdvertisingConsentManager_t2195563647 * get_sInstance_3() const { return ___sInstance_3; }
	inline AdvertisingConsentManager_t2195563647 ** get_address_of_sInstance_3() { return &___sInstance_3; }
	inline void set_sInstance_3(AdvertisingConsentManager_t2195563647 * value)
	{
		___sInstance_3 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVERTISINGCONSENTMANAGER_T2195563647_H
#ifndef AUDIENCENETWORKCLIENTIMPL_T266954160_H
#define AUDIENCENETWORKCLIENTIMPL_T266954160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AudienceNetworkClientImpl
struct  AudienceNetworkClientImpl_t266954160  : public AdClientImpl_t879350116
{
public:

public:
};

struct AudienceNetworkClientImpl_t266954160_StaticFields
{
public:
	// EasyMobile.AudienceNetworkClientImpl EasyMobile.AudienceNetworkClientImpl::sInstance
	AudienceNetworkClientImpl_t266954160 * ___sInstance_7;

public:
	inline static int32_t get_offset_of_sInstance_7() { return static_cast<int32_t>(offsetof(AudienceNetworkClientImpl_t266954160_StaticFields, ___sInstance_7)); }
	inline AudienceNetworkClientImpl_t266954160 * get_sInstance_7() const { return ___sInstance_7; }
	inline AudienceNetworkClientImpl_t266954160 ** get_address_of_sInstance_7() { return &___sInstance_7; }
	inline void set_sInstance_7(AudienceNetworkClientImpl_t266954160 * value)
	{
		___sInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIENCENETWORKCLIENTIMPL_T266954160_H
#ifndef CHARTBOOSTCLIENTIMPL_T602894833_H
#define CHARTBOOSTCLIENTIMPL_T602894833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ChartboostClientImpl
struct  ChartboostClientImpl_t602894833  : public AdClientImpl_t879350116
{
public:

public:
};

struct ChartboostClientImpl_t602894833_StaticFields
{
public:
	// EasyMobile.ChartboostClientImpl EasyMobile.ChartboostClientImpl::sInstance
	ChartboostClientImpl_t602894833 * ___sInstance_7;

public:
	inline static int32_t get_offset_of_sInstance_7() { return static_cast<int32_t>(offsetof(ChartboostClientImpl_t602894833_StaticFields, ___sInstance_7)); }
	inline ChartboostClientImpl_t602894833 * get_sInstance_7() const { return ___sInstance_7; }
	inline ChartboostClientImpl_t602894833 ** get_address_of_sInstance_7() { return &___sInstance_7; }
	inline void set_sInstance_7(ChartboostClientImpl_t602894833 * value)
	{
		___sInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARTBOOSTCLIENTIMPL_T602894833_H
#ifndef HEYZAPCLIENTIMPL_T2848241831_H
#define HEYZAPCLIENTIMPL_T2848241831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.HeyzapClientImpl
struct  HeyzapClientImpl_t2848241831  : public AdClientImpl_t879350116
{
public:

public:
};

struct HeyzapClientImpl_t2848241831_StaticFields
{
public:
	// EasyMobile.HeyzapClientImpl EasyMobile.HeyzapClientImpl::sInstance
	HeyzapClientImpl_t2848241831 * ___sInstance_6;

public:
	inline static int32_t get_offset_of_sInstance_6() { return static_cast<int32_t>(offsetof(HeyzapClientImpl_t2848241831_StaticFields, ___sInstance_6)); }
	inline HeyzapClientImpl_t2848241831 * get_sInstance_6() const { return ___sInstance_6; }
	inline HeyzapClientImpl_t2848241831 ** get_address_of_sInstance_6() { return &___sInstance_6; }
	inline void set_sInstance_6(HeyzapClientImpl_t2848241831 * value)
	{
		___sInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEYZAPCLIENTIMPL_T2848241831_H
#ifndef SERIALIZABLEDICTIONARYBASE_3_T1757425711_H
#define SERIALIZABLEDICTIONARYBASE_3_T1757425711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionaryBase`3<EasyMobile.AdPlacement,EasyMobile.AdId,EasyMobile.AdId>
struct  SerializableDictionaryBase_3_t1757425711  : public Dictionary_2_t228976570
{
public:
	// T[] EasyMobile.Internal.SerializableDictionaryBase`3::keys
	AdPlacementU5BU5D_t642510585* ___keys_14;
	// V[] EasyMobile.Internal.SerializableDictionaryBase`3::values
	AdIdU5BU5D_t2151920391* ___values_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_3_t1757425711, ___keys_14)); }
	inline AdPlacementU5BU5D_t642510585* get_keys_14() const { return ___keys_14; }
	inline AdPlacementU5BU5D_t642510585** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(AdPlacementU5BU5D_t642510585* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_3_t1757425711, ___values_15)); }
	inline AdIdU5BU5D_t2151920391* get_values_15() const { return ___values_15; }
	inline AdIdU5BU5D_t2151920391** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(AdIdU5BU5D_t2151920391* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier((&___values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_3_T1757425711_H
#ifndef SERIALIZABLEDICTIONARYBASE_3_T2568424352_H
#define SERIALIZABLEDICTIONARYBASE_3_T2568424352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionaryBase`3<System.String,EasyMobile.AdId,EasyMobile.AdId>
struct  SerializableDictionaryBase_3_t2568424352  : public Dictionary_2_t868255325
{
public:
	// T[] EasyMobile.Internal.SerializableDictionaryBase`3::keys
	StringU5BU5D_t1281789340* ___keys_14;
	// V[] EasyMobile.Internal.SerializableDictionaryBase`3::values
	AdIdU5BU5D_t2151920391* ___values_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_3_t2568424352, ___keys_14)); }
	inline StringU5BU5D_t1281789340* get_keys_14() const { return ___keys_14; }
	inline StringU5BU5D_t1281789340** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(StringU5BU5D_t1281789340* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_3_t2568424352, ___values_15)); }
	inline AdIdU5BU5D_t2151920391* get_values_15() const { return ___values_15; }
	inline AdIdU5BU5D_t2151920391** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(AdIdU5BU5D_t2151920391* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier((&___values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_3_T2568424352_H
#ifndef SERIALIZABLEDICTIONARYBASE_3_T2462744964_H
#define SERIALIZABLEDICTIONARYBASE_3_T2462744964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionaryBase`3<System.String,System.String,System.String>
struct  SerializableDictionaryBase_3_t2462744964  : public Dictionary_2_t1632706988
{
public:
	// T[] EasyMobile.Internal.SerializableDictionaryBase`3::keys
	StringU5BU5D_t1281789340* ___keys_14;
	// V[] EasyMobile.Internal.SerializableDictionaryBase`3::values
	StringU5BU5D_t1281789340* ___values_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_3_t2462744964, ___keys_14)); }
	inline StringU5BU5D_t1281789340* get_keys_14() const { return ___keys_14; }
	inline StringU5BU5D_t1281789340** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(StringU5BU5D_t1281789340* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_3_t2462744964, ___values_15)); }
	inline StringU5BU5D_t1281789340* get_values_15() const { return ___values_15; }
	inline StringU5BU5D_t1281789340** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(StringU5BU5D_t1281789340* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier((&___values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_3_T2462744964_H
#ifndef IRONSOURCECLIENTIMPL_T41919987_H
#define IRONSOURCECLIENTIMPL_T41919987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IronSourceClientImpl
struct  IronSourceClientImpl_t41919987  : public AdClientImpl_t879350116
{
public:

public:
};

struct IronSourceClientImpl_t41919987_StaticFields
{
public:
	// EasyMobile.IronSourceClientImpl EasyMobile.IronSourceClientImpl::sInstance
	IronSourceClientImpl_t41919987 * ___sInstance_6;

public:
	inline static int32_t get_offset_of_sInstance_6() { return static_cast<int32_t>(offsetof(IronSourceClientImpl_t41919987_StaticFields, ___sInstance_6)); }
	inline IronSourceClientImpl_t41919987 * get_sInstance_6() const { return ___sInstance_6; }
	inline IronSourceClientImpl_t41919987 ** get_address_of_sInstance_6() { return &___sInstance_6; }
	inline void set_sInstance_6(IronSourceClientImpl_t41919987 * value)
	{
		___sInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IRONSOURCECLIENTIMPL_T41919987_H
#ifndef LEADERBOARD_T4133458931_H
#define LEADERBOARD_T4133458931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Leaderboard
struct  Leaderboard_t4133458931  : public GameServicesItem_t2394671562
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARD_T4133458931_H
#ifndef MOPUBCLIENTIMPL_T784383640_H
#define MOPUBCLIENTIMPL_T784383640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MoPubClientImpl
struct  MoPubClientImpl_t784383640  : public AdClientImpl_t879350116
{
public:

public:
};

struct MoPubClientImpl_t784383640_StaticFields
{
public:
	// EasyMobile.MoPubClientImpl EasyMobile.MoPubClientImpl::sInstance
	MoPubClientImpl_t784383640 * ___sInstance_6;

public:
	inline static int32_t get_offset_of_sInstance_6() { return static_cast<int32_t>(offsetof(MoPubClientImpl_t784383640_StaticFields, ___sInstance_6)); }
	inline MoPubClientImpl_t784383640 * get_sInstance_6() const { return ___sInstance_6; }
	inline MoPubClientImpl_t784383640 ** get_address_of_sInstance_6() { return &___sInstance_6; }
	inline void set_sInstance_6(MoPubClientImpl_t784383640 * value)
	{
		___sInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBCLIENTIMPL_T784383640_H
#ifndef NOOPCLIENTIMPL_T3976050009_H
#define NOOPCLIENTIMPL_T3976050009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NoOpClientImpl
struct  NoOpClientImpl_t3976050009  : public AdClientImpl_t879350116
{
public:

public:
};

struct NoOpClientImpl_t3976050009_StaticFields
{
public:
	// EasyMobile.NoOpClientImpl EasyMobile.NoOpClientImpl::sInstance
	NoOpClientImpl_t3976050009 * ___sInstance_5;

public:
	inline static int32_t get_offset_of_sInstance_5() { return static_cast<int32_t>(offsetof(NoOpClientImpl_t3976050009_StaticFields, ___sInstance_5)); }
	inline NoOpClientImpl_t3976050009 * get_sInstance_5() const { return ___sInstance_5; }
	inline NoOpClientImpl_t3976050009 ** get_address_of_sInstance_5() { return &___sInstance_5; }
	inline void set_sInstance_5(NoOpClientImpl_t3976050009 * value)
	{
		___sInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOOPCLIENTIMPL_T3976050009_H
#ifndef TAPJOYCLIENTIMPL_T2003437302_H
#define TAPJOYCLIENTIMPL_T2003437302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.TapjoyClientImpl
struct  TapjoyClientImpl_t2003437302  : public AdClientImpl_t879350116
{
public:

public:
};

struct TapjoyClientImpl_t2003437302_StaticFields
{
public:
	// EasyMobile.TapjoyClientImpl EasyMobile.TapjoyClientImpl::sInstance
	TapjoyClientImpl_t2003437302 * ___sInstance_7;

public:
	inline static int32_t get_offset_of_sInstance_7() { return static_cast<int32_t>(offsetof(TapjoyClientImpl_t2003437302_StaticFields, ___sInstance_7)); }
	inline TapjoyClientImpl_t2003437302 * get_sInstance_7() const { return ___sInstance_7; }
	inline TapjoyClientImpl_t2003437302 ** get_address_of_sInstance_7() { return &___sInstance_7; }
	inline void set_sInstance_7(TapjoyClientImpl_t2003437302 * value)
	{
		___sInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPJOYCLIENTIMPL_T2003437302_H
#ifndef UNITYADSCLIENTIMPL_T4105509132_H
#define UNITYADSCLIENTIMPL_T4105509132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.UnityAdsClientImpl
struct  UnityAdsClientImpl_t4105509132  : public AdClientImpl_t879350116
{
public:
	// EasyMobile.UnityAdsSettings EasyMobile.UnityAdsClientImpl::mAdSettings
	UnityAdsSettings_t2563580717 * ___mAdSettings_7;
	// System.Action`2<UnityEngine.Advertisements.ShowResult,EasyMobile.AdPlacement> EasyMobile.UnityAdsClientImpl::InterstitialAdCallback
	Action_2_t2800288619 * ___InterstitialAdCallback_8;
	// System.Action`2<UnityEngine.Advertisements.ShowResult,EasyMobile.AdPlacement> EasyMobile.UnityAdsClientImpl::RewardedAdCallback
	Action_2_t2800288619 * ___RewardedAdCallback_9;

public:
	inline static int32_t get_offset_of_mAdSettings_7() { return static_cast<int32_t>(offsetof(UnityAdsClientImpl_t4105509132, ___mAdSettings_7)); }
	inline UnityAdsSettings_t2563580717 * get_mAdSettings_7() const { return ___mAdSettings_7; }
	inline UnityAdsSettings_t2563580717 ** get_address_of_mAdSettings_7() { return &___mAdSettings_7; }
	inline void set_mAdSettings_7(UnityAdsSettings_t2563580717 * value)
	{
		___mAdSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAdSettings_7), value);
	}

	inline static int32_t get_offset_of_InterstitialAdCallback_8() { return static_cast<int32_t>(offsetof(UnityAdsClientImpl_t4105509132, ___InterstitialAdCallback_8)); }
	inline Action_2_t2800288619 * get_InterstitialAdCallback_8() const { return ___InterstitialAdCallback_8; }
	inline Action_2_t2800288619 ** get_address_of_InterstitialAdCallback_8() { return &___InterstitialAdCallback_8; }
	inline void set_InterstitialAdCallback_8(Action_2_t2800288619 * value)
	{
		___InterstitialAdCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___InterstitialAdCallback_8), value);
	}

	inline static int32_t get_offset_of_RewardedAdCallback_9() { return static_cast<int32_t>(offsetof(UnityAdsClientImpl_t4105509132, ___RewardedAdCallback_9)); }
	inline Action_2_t2800288619 * get_RewardedAdCallback_9() const { return ___RewardedAdCallback_9; }
	inline Action_2_t2800288619 ** get_address_of_RewardedAdCallback_9() { return &___RewardedAdCallback_9; }
	inline void set_RewardedAdCallback_9(Action_2_t2800288619 * value)
	{
		___RewardedAdCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___RewardedAdCallback_9), value);
	}
};

struct UnityAdsClientImpl_t4105509132_StaticFields
{
public:
	// EasyMobile.UnityAdsClientImpl EasyMobile.UnityAdsClientImpl::sInstance
	UnityAdsClientImpl_t4105509132 * ___sInstance_10;

public:
	inline static int32_t get_offset_of_sInstance_10() { return static_cast<int32_t>(offsetof(UnityAdsClientImpl_t4105509132_StaticFields, ___sInstance_10)); }
	inline UnityAdsClientImpl_t4105509132 * get_sInstance_10() const { return ___sInstance_10; }
	inline UnityAdsClientImpl_t4105509132 ** get_address_of_sInstance_10() { return &___sInstance_10; }
	inline void set_sInstance_10(UnityAdsClientImpl_t4105509132 * value)
	{
		___sInstance_10 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSCLIENTIMPL_T4105509132_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ADCHILDDIRECTEDTREATMENT_T4156566242_H
#define ADCHILDDIRECTEDTREATMENT_T4156566242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdChildDirectedTreatment
struct  AdChildDirectedTreatment_t4156566242 
{
public:
	// System.Int32 EasyMobile.AdChildDirectedTreatment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdChildDirectedTreatment_t4156566242, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADCHILDDIRECTEDTREATMENT_T4156566242_H
#ifndef ADNETWORK_T2557190136_H
#define ADNETWORK_T2557190136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdNetwork
struct  AdNetwork_t2557190136 
{
public:
	// System.Int32 EasyMobile.AdNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdNetwork_t2557190136, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADNETWORK_T2557190136_H
#ifndef ADORIENTATION_T2144699178_H
#define ADORIENTATION_T2144699178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdOrientation
struct  AdOrientation_t2144699178 
{
public:
	// System.Int32 EasyMobile.AdOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdOrientation_t2144699178, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADORIENTATION_T2144699178_H
#ifndef ADTYPE_T68004042_H
#define ADTYPE_T68004042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdType
struct  AdType_t68004042 
{
public:
	// System.Int32 EasyMobile.AdType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdType_t68004042, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADTYPE_T68004042_H
#ifndef FBAUDIENCEBANNERADSIZE_T1904814455_H
#define FBAUDIENCEBANNERADSIZE_T1904814455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AudienceNetworkSettings/FBAudienceBannerAdSize
struct  FBAudienceBannerAdSize_t1904814455 
{
public:
	// System.Int32 EasyMobile.AudienceNetworkSettings/FBAudienceBannerAdSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FBAudienceBannerAdSize_t1904814455, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBAUDIENCEBANNERADSIZE_T1904814455_H
#ifndef AUTOADLOADINGMODE_T854791933_H
#define AUTOADLOADINGMODE_T854791933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AutoAdLoadingMode
struct  AutoAdLoadingMode_t854791933 
{
public:
	// System.Int32 EasyMobile.AutoAdLoadingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoAdLoadingMode_t854791933, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOADLOADINGMODE_T854791933_H
#ifndef BANNERADNETWORK_T4276929711_H
#define BANNERADNETWORK_T4276929711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.BannerAdNetwork
struct  BannerAdNetwork_t4276929711 
{
public:
	// System.Int32 EasyMobile.BannerAdNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BannerAdNetwork_t4276929711, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERADNETWORK_T4276929711_H
#ifndef BANNERADPOSITION_T186839070_H
#define BANNERADPOSITION_T186839070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.BannerAdPosition
struct  BannerAdPosition_t186839070 
{
public:
	// System.Int32 EasyMobile.BannerAdPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BannerAdPosition_t186839070, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERADPOSITION_T186839070_H
#ifndef GPGSSAVEDGAMEDATASOURCE_T1302173665_H
#define GPGSSAVEDGAMEDATASOURCE_T1302173665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GPGSSavedGameDataSource
struct  GPGSSavedGameDataSource_t1302173665 
{
public:
	// System.Int32 EasyMobile.GPGSSavedGameDataSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GPGSSavedGameDataSource_t1302173665, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPGSSAVEDGAMEDATASOURCE_T1302173665_H
#ifndef GPGSGRAVITY_T3325317384_H
#define GPGSGRAVITY_T3325317384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServicesSettings/GpgsGravity
struct  GpgsGravity_t3325317384 
{
public:
	// System.Int32 EasyMobile.GameServicesSettings/GpgsGravity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GpgsGravity_t3325317384, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPGSGRAVITY_T3325317384_H
#ifndef TYPE_T3888639762_H
#define TYPE_T3888639762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.PInvokeCallbackUtil/Type
struct  Type_t3888639762 
{
public:
	// System.Int32 EasyMobile.Internal.PInvokeCallbackUtil/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t3888639762, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3888639762_H
#ifndef SERIALIZABLEDICTIONARY_2_T1077228432_H
#define SERIALIZABLEDICTIONARY_2_T1077228432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionary`2<EasyMobile.AdPlacement,EasyMobile.AdId>
struct  SerializableDictionary_2_t1077228432  : public SerializableDictionaryBase_3_t1757425711
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_2_T1077228432_H
#ifndef SERIALIZABLEDICTIONARY_2_T1716507187_H
#define SERIALIZABLEDICTIONARY_2_T1716507187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionary`2<System.String,EasyMobile.AdId>
struct  SerializableDictionary_2_t1716507187  : public SerializableDictionaryBase_3_t2568424352
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_2_T1716507187_H
#ifndef SERIALIZABLEDICTIONARY_2_T2480958850_H
#define SERIALIZABLEDICTIONARY_2_T2480958850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.SerializableDictionary`2<System.String,System.String>
struct  SerializableDictionary_2_t2480958850  : public SerializableDictionaryBase_3_t2462744964
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARY_2_T2480958850_H
#ifndef UTIL_T3803880283_H
#define UTIL_T3803880283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Util
struct  Util_t3803880283  : public RuntimeObject
{
public:

public:
};

struct Util_t3803880283_StaticFields
{
public:
	// System.DateTime EasyMobile.Internal.Util::UnixEpoch
	DateTime_t3738529785  ___UnixEpoch_0;

public:
	inline static int32_t get_offset_of_UnixEpoch_0() { return static_cast<int32_t>(offsetof(Util_t3803880283_StaticFields, ___UnixEpoch_0)); }
	inline DateTime_t3738529785  get_UnixEpoch_0() const { return ___UnixEpoch_0; }
	inline DateTime_t3738529785 * get_address_of_UnixEpoch_0() { return &___UnixEpoch_0; }
	inline void set_UnixEpoch_0(DateTime_t3738529785  value)
	{
		___UnixEpoch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTIL_T3803880283_H
#ifndef INTERSTITIALADNETWORK_T3048379503_H
#define INTERSTITIALADNETWORK_T3048379503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InterstitialAdNetwork
struct  InterstitialAdNetwork_t3048379503 
{
public:
	// System.Int32 EasyMobile.InterstitialAdNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterstitialAdNetwork_t3048379503, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALADNETWORK_T3048379503_H
#ifndef IRONSOURCEBANNERTYPE_T1343419624_H
#define IRONSOURCEBANNERTYPE_T1343419624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IronSourceSettings/IronSourceBannerType
struct  IronSourceBannerType_t1343419624 
{
public:
	// System.Int32 EasyMobile.IronSourceSettings/IronSourceBannerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IronSourceBannerType_t1343419624, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IRONSOURCEBANNERTYPE_T1343419624_H
#ifndef TOKEN_T1085193790_H
#define TOKEN_T1085193790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t1085193790 
{
public:
	// System.Int32 EasyMobile.MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t1085193790, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T1085193790_H
#ifndef MOPUBADVANCEDBIDDER_T3070790054_H
#define MOPUBADVANCEDBIDDER_T3070790054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MoPubSettings/MoPubAdvancedBidder
struct  MoPubAdvancedBidder_t3070790054 
{
public:
	// System.Int32 EasyMobile.MoPubSettings/MoPubAdvancedBidder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoPubAdvancedBidder_t3070790054, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBADVANCEDBIDDER_T3070790054_H
#ifndef MOPUBINITNETWORK_T4276534974_H
#define MOPUBINITNETWORK_T4276534974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.MoPubSettings/MoPubInitNetwork
struct  MoPubInitNetwork_t4276534974 
{
public:
	// System.Int32 EasyMobile.MoPubSettings/MoPubInitNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoPubInitNetwork_t4276534974, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBINITNETWORK_T4276534974_H
#ifndef REWARDEDADNETWORK_T72233497_H
#define REWARDEDADNETWORK_T72233497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.RewardedAdNetwork
struct  RewardedAdNetwork_t72233497 
{
public:
	// System.Int32 EasyMobile.RewardedAdNetwork::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RewardedAdNetwork_t72233497, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDADNETWORK_T72233497_H
#ifndef SAVEDGAMECONFLICTRESOLUTIONSTRATEGY_T482061790_H
#define SAVEDGAMECONFLICTRESOLUTIONSTRATEGY_T482061790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.SavedGameConflictResolutionStrategy
struct  SavedGameConflictResolutionStrategy_t482061790 
{
public:
	// System.Int32 EasyMobile.SavedGameConflictResolutionStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SavedGameConflictResolutionStrategy_t482061790, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDGAMECONFLICTRESOLUTIONSTRATEGY_T482061790_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TIMESCOPE_T539351503_H
#define TIMESCOPE_T539351503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.TimeScope
struct  TimeScope_t539351503 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.TimeScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimeScope_t539351503, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESCOPE_T539351503_H
#ifndef USERSCOPE_T604006431_H
#define USERSCOPE_T604006431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SocialPlatforms.UserScope
struct  UserScope_t604006431 
{
public:
	// System.Int32 UnityEngine.SocialPlatforms.UserScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserScope_t604006431, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSCOPE_T604006431_H
#ifndef ADCOLONYSETTINGS_T2994520974_H
#define ADCOLONYSETTINGS_T2994520974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdColonySettings
struct  AdColonySettings_t2994520974  : public RuntimeObject
{
public:
	// EasyMobile.AdId EasyMobile.AdColonySettings::mAppId
	AdId_t1082999026 * ___mAppId_0;
	// EasyMobile.AdOrientation EasyMobile.AdColonySettings::mOrientation
	int32_t ___mOrientation_1;
	// System.Boolean EasyMobile.AdColonySettings::mEnableRewardedAdPrePopup
	bool ___mEnableRewardedAdPrePopup_2;
	// System.Boolean EasyMobile.AdColonySettings::mEnableRewardedAdPostPopup
	bool ___mEnableRewardedAdPostPopup_3;
	// EasyMobile.AdId EasyMobile.AdColonySettings::mDefaultInterstitialAdId
	AdId_t1082999026 * ___mDefaultInterstitialAdId_4;
	// EasyMobile.AdId EasyMobile.AdColonySettings::mDefaultRewardedAdId
	AdId_t1082999026 * ___mDefaultRewardedAdId_5;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AdColonySettings::mCustomInterstitialAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomInterstitialAdIds_6;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AdColonySettings::mCustomRewardedAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomRewardedAdIds_7;

public:
	inline static int32_t get_offset_of_mAppId_0() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mAppId_0)); }
	inline AdId_t1082999026 * get_mAppId_0() const { return ___mAppId_0; }
	inline AdId_t1082999026 ** get_address_of_mAppId_0() { return &___mAppId_0; }
	inline void set_mAppId_0(AdId_t1082999026 * value)
	{
		___mAppId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mAppId_0), value);
	}

	inline static int32_t get_offset_of_mOrientation_1() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mOrientation_1)); }
	inline int32_t get_mOrientation_1() const { return ___mOrientation_1; }
	inline int32_t* get_address_of_mOrientation_1() { return &___mOrientation_1; }
	inline void set_mOrientation_1(int32_t value)
	{
		___mOrientation_1 = value;
	}

	inline static int32_t get_offset_of_mEnableRewardedAdPrePopup_2() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mEnableRewardedAdPrePopup_2)); }
	inline bool get_mEnableRewardedAdPrePopup_2() const { return ___mEnableRewardedAdPrePopup_2; }
	inline bool* get_address_of_mEnableRewardedAdPrePopup_2() { return &___mEnableRewardedAdPrePopup_2; }
	inline void set_mEnableRewardedAdPrePopup_2(bool value)
	{
		___mEnableRewardedAdPrePopup_2 = value;
	}

	inline static int32_t get_offset_of_mEnableRewardedAdPostPopup_3() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mEnableRewardedAdPostPopup_3)); }
	inline bool get_mEnableRewardedAdPostPopup_3() const { return ___mEnableRewardedAdPostPopup_3; }
	inline bool* get_address_of_mEnableRewardedAdPostPopup_3() { return &___mEnableRewardedAdPostPopup_3; }
	inline void set_mEnableRewardedAdPostPopup_3(bool value)
	{
		___mEnableRewardedAdPostPopup_3 = value;
	}

	inline static int32_t get_offset_of_mDefaultInterstitialAdId_4() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mDefaultInterstitialAdId_4)); }
	inline AdId_t1082999026 * get_mDefaultInterstitialAdId_4() const { return ___mDefaultInterstitialAdId_4; }
	inline AdId_t1082999026 ** get_address_of_mDefaultInterstitialAdId_4() { return &___mDefaultInterstitialAdId_4; }
	inline void set_mDefaultInterstitialAdId_4(AdId_t1082999026 * value)
	{
		___mDefaultInterstitialAdId_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultInterstitialAdId_4), value);
	}

	inline static int32_t get_offset_of_mDefaultRewardedAdId_5() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mDefaultRewardedAdId_5)); }
	inline AdId_t1082999026 * get_mDefaultRewardedAdId_5() const { return ___mDefaultRewardedAdId_5; }
	inline AdId_t1082999026 ** get_address_of_mDefaultRewardedAdId_5() { return &___mDefaultRewardedAdId_5; }
	inline void set_mDefaultRewardedAdId_5(AdId_t1082999026 * value)
	{
		___mDefaultRewardedAdId_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRewardedAdId_5), value);
	}

	inline static int32_t get_offset_of_mCustomInterstitialAdIds_6() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mCustomInterstitialAdIds_6)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomInterstitialAdIds_6() const { return ___mCustomInterstitialAdIds_6; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomInterstitialAdIds_6() { return &___mCustomInterstitialAdIds_6; }
	inline void set_mCustomInterstitialAdIds_6(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomInterstitialAdIds_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialAdIds_6), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedAdIds_7() { return static_cast<int32_t>(offsetof(AdColonySettings_t2994520974, ___mCustomRewardedAdIds_7)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomRewardedAdIds_7() const { return ___mCustomRewardedAdIds_7; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomRewardedAdIds_7() { return &___mCustomRewardedAdIds_7; }
	inline void set_mCustomRewardedAdIds_7(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomRewardedAdIds_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedAdIds_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADCOLONYSETTINGS_T2994520974_H
#ifndef ADMOBTARGETINGSETTINGS_T662407302_H
#define ADMOBTARGETINGSETTINGS_T662407302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdMobSettings/AdMobTargetingSettings
struct  AdMobTargetingSettings_t662407302  : public RuntimeObject
{
public:
	// EasyMobile.AdChildDirectedTreatment EasyMobile.AdMobSettings/AdMobTargetingSettings::mTagForChildDirectedTreatment
	int32_t ___mTagForChildDirectedTreatment_0;
	// EasyMobile.Internal.StringStringSerializableDictionary EasyMobile.AdMobSettings/AdMobTargetingSettings::mExtraOptions
	StringStringSerializableDictionary_t4078874517 * ___mExtraOptions_1;

public:
	inline static int32_t get_offset_of_mTagForChildDirectedTreatment_0() { return static_cast<int32_t>(offsetof(AdMobTargetingSettings_t662407302, ___mTagForChildDirectedTreatment_0)); }
	inline int32_t get_mTagForChildDirectedTreatment_0() const { return ___mTagForChildDirectedTreatment_0; }
	inline int32_t* get_address_of_mTagForChildDirectedTreatment_0() { return &___mTagForChildDirectedTreatment_0; }
	inline void set_mTagForChildDirectedTreatment_0(int32_t value)
	{
		___mTagForChildDirectedTreatment_0 = value;
	}

	inline static int32_t get_offset_of_mExtraOptions_1() { return static_cast<int32_t>(offsetof(AdMobTargetingSettings_t662407302, ___mExtraOptions_1)); }
	inline StringStringSerializableDictionary_t4078874517 * get_mExtraOptions_1() const { return ___mExtraOptions_1; }
	inline StringStringSerializableDictionary_t4078874517 ** get_address_of_mExtraOptions_1() { return &___mExtraOptions_1; }
	inline void set_mExtraOptions_1(StringStringSerializableDictionary_t4078874517 * value)
	{
		___mExtraOptions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mExtraOptions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMOBTARGETINGSETTINGS_T662407302_H
#ifndef DEFAULTADNETWORKS_T2678507152_H
#define DEFAULTADNETWORKS_T2678507152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdSettings/DefaultAdNetworks
struct  DefaultAdNetworks_t2678507152 
{
public:
	// EasyMobile.BannerAdNetwork EasyMobile.AdSettings/DefaultAdNetworks::bannerAdNetwork
	int32_t ___bannerAdNetwork_0;
	// EasyMobile.InterstitialAdNetwork EasyMobile.AdSettings/DefaultAdNetworks::interstitialAdNetwork
	int32_t ___interstitialAdNetwork_1;
	// EasyMobile.RewardedAdNetwork EasyMobile.AdSettings/DefaultAdNetworks::rewardedAdNetwork
	int32_t ___rewardedAdNetwork_2;

public:
	inline static int32_t get_offset_of_bannerAdNetwork_0() { return static_cast<int32_t>(offsetof(DefaultAdNetworks_t2678507152, ___bannerAdNetwork_0)); }
	inline int32_t get_bannerAdNetwork_0() const { return ___bannerAdNetwork_0; }
	inline int32_t* get_address_of_bannerAdNetwork_0() { return &___bannerAdNetwork_0; }
	inline void set_bannerAdNetwork_0(int32_t value)
	{
		___bannerAdNetwork_0 = value;
	}

	inline static int32_t get_offset_of_interstitialAdNetwork_1() { return static_cast<int32_t>(offsetof(DefaultAdNetworks_t2678507152, ___interstitialAdNetwork_1)); }
	inline int32_t get_interstitialAdNetwork_1() const { return ___interstitialAdNetwork_1; }
	inline int32_t* get_address_of_interstitialAdNetwork_1() { return &___interstitialAdNetwork_1; }
	inline void set_interstitialAdNetwork_1(int32_t value)
	{
		___interstitialAdNetwork_1 = value;
	}

	inline static int32_t get_offset_of_rewardedAdNetwork_2() { return static_cast<int32_t>(offsetof(DefaultAdNetworks_t2678507152, ___rewardedAdNetwork_2)); }
	inline int32_t get_rewardedAdNetwork_2() const { return ___rewardedAdNetwork_2; }
	inline int32_t* get_address_of_rewardedAdNetwork_2() { return &___rewardedAdNetwork_2; }
	inline void set_rewardedAdNetwork_2(int32_t value)
	{
		___rewardedAdNetwork_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTADNETWORKS_T2678507152_H
#ifndef AUDIENCENETWORKSETTINGS_T1217453211_H
#define AUDIENCENETWORKSETTINGS_T1217453211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AudienceNetworkSettings
struct  AudienceNetworkSettings_t1217453211  : public RuntimeObject
{
public:
	// EasyMobile.AudienceNetworkSettings/FBAudienceBannerAdSize EasyMobile.AudienceNetworkSettings::mBannerAdSize
	int32_t ___mBannerAdSize_0;
	// System.Boolean EasyMobile.AudienceNetworkSettings::mEnableTestMode
	bool ___mEnableTestMode_1;
	// System.String[] EasyMobile.AudienceNetworkSettings::mTestDevices
	StringU5BU5D_t1281789340* ___mTestDevices_2;
	// EasyMobile.AdId EasyMobile.AudienceNetworkSettings::mDefaultBannerId
	AdId_t1082999026 * ___mDefaultBannerId_3;
	// EasyMobile.AdId EasyMobile.AudienceNetworkSettings::mDefaultInterstitialAdId
	AdId_t1082999026 * ___mDefaultInterstitialAdId_4;
	// EasyMobile.AdId EasyMobile.AudienceNetworkSettings::mDefaultRewardedAdId
	AdId_t1082999026 * ___mDefaultRewardedAdId_5;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AudienceNetworkSettings::mCustomBannerIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomBannerIds_6;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AudienceNetworkSettings::mCustomInterstitialAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomInterstitialAdIds_7;
	// EasyMobile.Internal.Dictionary_AdPlacement_AdId EasyMobile.AudienceNetworkSettings::mCustomRewardedAdIds
	Dictionary_AdPlacement_AdId_t1141413887 * ___mCustomRewardedAdIds_8;

public:
	inline static int32_t get_offset_of_mBannerAdSize_0() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mBannerAdSize_0)); }
	inline int32_t get_mBannerAdSize_0() const { return ___mBannerAdSize_0; }
	inline int32_t* get_address_of_mBannerAdSize_0() { return &___mBannerAdSize_0; }
	inline void set_mBannerAdSize_0(int32_t value)
	{
		___mBannerAdSize_0 = value;
	}

	inline static int32_t get_offset_of_mEnableTestMode_1() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mEnableTestMode_1)); }
	inline bool get_mEnableTestMode_1() const { return ___mEnableTestMode_1; }
	inline bool* get_address_of_mEnableTestMode_1() { return &___mEnableTestMode_1; }
	inline void set_mEnableTestMode_1(bool value)
	{
		___mEnableTestMode_1 = value;
	}

	inline static int32_t get_offset_of_mTestDevices_2() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mTestDevices_2)); }
	inline StringU5BU5D_t1281789340* get_mTestDevices_2() const { return ___mTestDevices_2; }
	inline StringU5BU5D_t1281789340** get_address_of_mTestDevices_2() { return &___mTestDevices_2; }
	inline void set_mTestDevices_2(StringU5BU5D_t1281789340* value)
	{
		___mTestDevices_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTestDevices_2), value);
	}

	inline static int32_t get_offset_of_mDefaultBannerId_3() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mDefaultBannerId_3)); }
	inline AdId_t1082999026 * get_mDefaultBannerId_3() const { return ___mDefaultBannerId_3; }
	inline AdId_t1082999026 ** get_address_of_mDefaultBannerId_3() { return &___mDefaultBannerId_3; }
	inline void set_mDefaultBannerId_3(AdId_t1082999026 * value)
	{
		___mDefaultBannerId_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultBannerId_3), value);
	}

	inline static int32_t get_offset_of_mDefaultInterstitialAdId_4() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mDefaultInterstitialAdId_4)); }
	inline AdId_t1082999026 * get_mDefaultInterstitialAdId_4() const { return ___mDefaultInterstitialAdId_4; }
	inline AdId_t1082999026 ** get_address_of_mDefaultInterstitialAdId_4() { return &___mDefaultInterstitialAdId_4; }
	inline void set_mDefaultInterstitialAdId_4(AdId_t1082999026 * value)
	{
		___mDefaultInterstitialAdId_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultInterstitialAdId_4), value);
	}

	inline static int32_t get_offset_of_mDefaultRewardedAdId_5() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mDefaultRewardedAdId_5)); }
	inline AdId_t1082999026 * get_mDefaultRewardedAdId_5() const { return ___mDefaultRewardedAdId_5; }
	inline AdId_t1082999026 ** get_address_of_mDefaultRewardedAdId_5() { return &___mDefaultRewardedAdId_5; }
	inline void set_mDefaultRewardedAdId_5(AdId_t1082999026 * value)
	{
		___mDefaultRewardedAdId_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRewardedAdId_5), value);
	}

	inline static int32_t get_offset_of_mCustomBannerIds_6() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mCustomBannerIds_6)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomBannerIds_6() const { return ___mCustomBannerIds_6; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomBannerIds_6() { return &___mCustomBannerIds_6; }
	inline void set_mCustomBannerIds_6(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomBannerIds_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomBannerIds_6), value);
	}

	inline static int32_t get_offset_of_mCustomInterstitialAdIds_7() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mCustomInterstitialAdIds_7)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomInterstitialAdIds_7() const { return ___mCustomInterstitialAdIds_7; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomInterstitialAdIds_7() { return &___mCustomInterstitialAdIds_7; }
	inline void set_mCustomInterstitialAdIds_7(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomInterstitialAdIds_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomInterstitialAdIds_7), value);
	}

	inline static int32_t get_offset_of_mCustomRewardedAdIds_8() { return static_cast<int32_t>(offsetof(AudienceNetworkSettings_t1217453211, ___mCustomRewardedAdIds_8)); }
	inline Dictionary_AdPlacement_AdId_t1141413887 * get_mCustomRewardedAdIds_8() const { return ___mCustomRewardedAdIds_8; }
	inline Dictionary_AdPlacement_AdId_t1141413887 ** get_address_of_mCustomRewardedAdIds_8() { return &___mCustomRewardedAdIds_8; }
	inline void set_mCustomRewardedAdIds_8(Dictionary_AdPlacement_AdId_t1141413887 * value)
	{
		___mCustomRewardedAdIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomRewardedAdIds_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIENCENETWORKSETTINGS_T1217453211_H
#ifndef LOADSCOREREQUEST_T1342227622_H
#define LOADSCOREREQUEST_T1342227622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/LoadScoreRequest
struct  LoadScoreRequest_t1342227622 
{
public:
	// System.Boolean EasyMobile.GameServices/LoadScoreRequest::useLeaderboardDefault
	bool ___useLeaderboardDefault_0;
	// System.Boolean EasyMobile.GameServices/LoadScoreRequest::loadLocalUserScore
	bool ___loadLocalUserScore_1;
	// System.String EasyMobile.GameServices/LoadScoreRequest::leaderboardName
	String_t* ___leaderboardName_2;
	// System.String EasyMobile.GameServices/LoadScoreRequest::leaderboardId
	String_t* ___leaderboardId_3;
	// System.Int32 EasyMobile.GameServices/LoadScoreRequest::fromRank
	int32_t ___fromRank_4;
	// System.Int32 EasyMobile.GameServices/LoadScoreRequest::scoreCount
	int32_t ___scoreCount_5;
	// UnityEngine.SocialPlatforms.TimeScope EasyMobile.GameServices/LoadScoreRequest::timeScope
	int32_t ___timeScope_6;
	// UnityEngine.SocialPlatforms.UserScope EasyMobile.GameServices/LoadScoreRequest::userScope
	int32_t ___userScope_7;
	// System.Action`2<System.String,UnityEngine.SocialPlatforms.IScore[]> EasyMobile.GameServices/LoadScoreRequest::callback
	Action_2_t2650591233 * ___callback_8;

public:
	inline static int32_t get_offset_of_useLeaderboardDefault_0() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___useLeaderboardDefault_0)); }
	inline bool get_useLeaderboardDefault_0() const { return ___useLeaderboardDefault_0; }
	inline bool* get_address_of_useLeaderboardDefault_0() { return &___useLeaderboardDefault_0; }
	inline void set_useLeaderboardDefault_0(bool value)
	{
		___useLeaderboardDefault_0 = value;
	}

	inline static int32_t get_offset_of_loadLocalUserScore_1() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___loadLocalUserScore_1)); }
	inline bool get_loadLocalUserScore_1() const { return ___loadLocalUserScore_1; }
	inline bool* get_address_of_loadLocalUserScore_1() { return &___loadLocalUserScore_1; }
	inline void set_loadLocalUserScore_1(bool value)
	{
		___loadLocalUserScore_1 = value;
	}

	inline static int32_t get_offset_of_leaderboardName_2() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___leaderboardName_2)); }
	inline String_t* get_leaderboardName_2() const { return ___leaderboardName_2; }
	inline String_t** get_address_of_leaderboardName_2() { return &___leaderboardName_2; }
	inline void set_leaderboardName_2(String_t* value)
	{
		___leaderboardName_2 = value;
		Il2CppCodeGenWriteBarrier((&___leaderboardName_2), value);
	}

	inline static int32_t get_offset_of_leaderboardId_3() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___leaderboardId_3)); }
	inline String_t* get_leaderboardId_3() const { return ___leaderboardId_3; }
	inline String_t** get_address_of_leaderboardId_3() { return &___leaderboardId_3; }
	inline void set_leaderboardId_3(String_t* value)
	{
		___leaderboardId_3 = value;
		Il2CppCodeGenWriteBarrier((&___leaderboardId_3), value);
	}

	inline static int32_t get_offset_of_fromRank_4() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___fromRank_4)); }
	inline int32_t get_fromRank_4() const { return ___fromRank_4; }
	inline int32_t* get_address_of_fromRank_4() { return &___fromRank_4; }
	inline void set_fromRank_4(int32_t value)
	{
		___fromRank_4 = value;
	}

	inline static int32_t get_offset_of_scoreCount_5() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___scoreCount_5)); }
	inline int32_t get_scoreCount_5() const { return ___scoreCount_5; }
	inline int32_t* get_address_of_scoreCount_5() { return &___scoreCount_5; }
	inline void set_scoreCount_5(int32_t value)
	{
		___scoreCount_5 = value;
	}

	inline static int32_t get_offset_of_timeScope_6() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___timeScope_6)); }
	inline int32_t get_timeScope_6() const { return ___timeScope_6; }
	inline int32_t* get_address_of_timeScope_6() { return &___timeScope_6; }
	inline void set_timeScope_6(int32_t value)
	{
		___timeScope_6 = value;
	}

	inline static int32_t get_offset_of_userScope_7() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___userScope_7)); }
	inline int32_t get_userScope_7() const { return ___userScope_7; }
	inline int32_t* get_address_of_userScope_7() { return &___userScope_7; }
	inline void set_userScope_7(int32_t value)
	{
		___userScope_7 = value;
	}

	inline static int32_t get_offset_of_callback_8() { return static_cast<int32_t>(offsetof(LoadScoreRequest_t1342227622, ___callback_8)); }
	inline Action_2_t2650591233 * get_callback_8() const { return ___callback_8; }
	inline Action_2_t2650591233 ** get_address_of_callback_8() { return &___callback_8; }
	inline void set_callback_8(Action_2_t2650591233 * value)
	{
		___callback_8 = value;
		Il2CppCodeGenWriteBarrier((&___callback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.GameServices/LoadScoreRequest
struct LoadScoreRequest_t1342227622_marshaled_pinvoke
{
	int32_t ___useLeaderboardDefault_0;
	int32_t ___loadLocalUserScore_1;
	char* ___leaderboardName_2;
	char* ___leaderboardId_3;
	int32_t ___fromRank_4;
	int32_t ___scoreCount_5;
	int32_t ___timeScope_6;
	int32_t ___userScope_7;
	Il2CppMethodPointer ___callback_8;
};
// Native definition for COM marshalling of EasyMobile.GameServices/LoadScoreRequest
struct LoadScoreRequest_t1342227622_marshaled_com
{
	int32_t ___useLeaderboardDefault_0;
	int32_t ___loadLocalUserScore_1;
	Il2CppChar* ___leaderboardName_2;
	Il2CppChar* ___leaderboardId_3;
	int32_t ___fromRank_4;
	int32_t ___scoreCount_5;
	int32_t ___timeScope_6;
	int32_t ___userScope_7;
	Il2CppMethodPointer ___callback_8;
};
#endif // LOADSCOREREQUEST_T1342227622_H
#ifndef GAMESERVICESSETTINGS_T1871988932_H
#define GAMESERVICESSETTINGS_T1871988932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServicesSettings
struct  GameServicesSettings_t1871988932  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.GameServicesSettings::mAutoInit
	bool ___mAutoInit_0;
	// System.Single EasyMobile.GameServicesSettings::mAutoInitDelay
	float ___mAutoInitDelay_1;
	// System.Int32 EasyMobile.GameServicesSettings::mAndroidMaxLoginRequests
	int32_t ___mAndroidMaxLoginRequests_2;
	// System.Boolean EasyMobile.GameServicesSettings::mGpgsDebugLogEnabled
	bool ___mGpgsDebugLogEnabled_3;
	// EasyMobile.GameServicesSettings/GpgsGravity EasyMobile.GameServicesSettings::mGpgsPopupGravity
	int32_t ___mGpgsPopupGravity_4;
	// EasyMobile.Leaderboard[] EasyMobile.GameServicesSettings::mLeaderboards
	LeaderboardU5BU5D_t1320244962* ___mLeaderboards_5;
	// EasyMobile.Achievement[] EasyMobile.GameServicesSettings::mAchievements
	AchievementU5BU5D_t2302189113* ___mAchievements_6;
	// System.String EasyMobile.GameServicesSettings::mAndroidXmlResources
	String_t* ___mAndroidXmlResources_7;
	// System.Boolean EasyMobile.GameServicesSettings::mEnableSavedGames
	bool ___mEnableSavedGames_8;
	// EasyMobile.SavedGameConflictResolutionStrategy EasyMobile.GameServicesSettings::mAutoConflictResolutionStrategy
	int32_t ___mAutoConflictResolutionStrategy_9;
	// EasyMobile.GPGSSavedGameDataSource EasyMobile.GameServicesSettings::mGpgsDataSource
	int32_t ___mGpgsDataSource_10;

public:
	inline static int32_t get_offset_of_mAutoInit_0() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mAutoInit_0)); }
	inline bool get_mAutoInit_0() const { return ___mAutoInit_0; }
	inline bool* get_address_of_mAutoInit_0() { return &___mAutoInit_0; }
	inline void set_mAutoInit_0(bool value)
	{
		___mAutoInit_0 = value;
	}

	inline static int32_t get_offset_of_mAutoInitDelay_1() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mAutoInitDelay_1)); }
	inline float get_mAutoInitDelay_1() const { return ___mAutoInitDelay_1; }
	inline float* get_address_of_mAutoInitDelay_1() { return &___mAutoInitDelay_1; }
	inline void set_mAutoInitDelay_1(float value)
	{
		___mAutoInitDelay_1 = value;
	}

	inline static int32_t get_offset_of_mAndroidMaxLoginRequests_2() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mAndroidMaxLoginRequests_2)); }
	inline int32_t get_mAndroidMaxLoginRequests_2() const { return ___mAndroidMaxLoginRequests_2; }
	inline int32_t* get_address_of_mAndroidMaxLoginRequests_2() { return &___mAndroidMaxLoginRequests_2; }
	inline void set_mAndroidMaxLoginRequests_2(int32_t value)
	{
		___mAndroidMaxLoginRequests_2 = value;
	}

	inline static int32_t get_offset_of_mGpgsDebugLogEnabled_3() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mGpgsDebugLogEnabled_3)); }
	inline bool get_mGpgsDebugLogEnabled_3() const { return ___mGpgsDebugLogEnabled_3; }
	inline bool* get_address_of_mGpgsDebugLogEnabled_3() { return &___mGpgsDebugLogEnabled_3; }
	inline void set_mGpgsDebugLogEnabled_3(bool value)
	{
		___mGpgsDebugLogEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mGpgsPopupGravity_4() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mGpgsPopupGravity_4)); }
	inline int32_t get_mGpgsPopupGravity_4() const { return ___mGpgsPopupGravity_4; }
	inline int32_t* get_address_of_mGpgsPopupGravity_4() { return &___mGpgsPopupGravity_4; }
	inline void set_mGpgsPopupGravity_4(int32_t value)
	{
		___mGpgsPopupGravity_4 = value;
	}

	inline static int32_t get_offset_of_mLeaderboards_5() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mLeaderboards_5)); }
	inline LeaderboardU5BU5D_t1320244962* get_mLeaderboards_5() const { return ___mLeaderboards_5; }
	inline LeaderboardU5BU5D_t1320244962** get_address_of_mLeaderboards_5() { return &___mLeaderboards_5; }
	inline void set_mLeaderboards_5(LeaderboardU5BU5D_t1320244962* value)
	{
		___mLeaderboards_5 = value;
		Il2CppCodeGenWriteBarrier((&___mLeaderboards_5), value);
	}

	inline static int32_t get_offset_of_mAchievements_6() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mAchievements_6)); }
	inline AchievementU5BU5D_t2302189113* get_mAchievements_6() const { return ___mAchievements_6; }
	inline AchievementU5BU5D_t2302189113** get_address_of_mAchievements_6() { return &___mAchievements_6; }
	inline void set_mAchievements_6(AchievementU5BU5D_t2302189113* value)
	{
		___mAchievements_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAchievements_6), value);
	}

	inline static int32_t get_offset_of_mAndroidXmlResources_7() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mAndroidXmlResources_7)); }
	inline String_t* get_mAndroidXmlResources_7() const { return ___mAndroidXmlResources_7; }
	inline String_t** get_address_of_mAndroidXmlResources_7() { return &___mAndroidXmlResources_7; }
	inline void set_mAndroidXmlResources_7(String_t* value)
	{
		___mAndroidXmlResources_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAndroidXmlResources_7), value);
	}

	inline static int32_t get_offset_of_mEnableSavedGames_8() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mEnableSavedGames_8)); }
	inline bool get_mEnableSavedGames_8() const { return ___mEnableSavedGames_8; }
	inline bool* get_address_of_mEnableSavedGames_8() { return &___mEnableSavedGames_8; }
	inline void set_mEnableSavedGames_8(bool value)
	{
		___mEnableSavedGames_8 = value;
	}

	inline static int32_t get_offset_of_mAutoConflictResolutionStrategy_9() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mAutoConflictResolutionStrategy_9)); }
	inline int32_t get_mAutoConflictResolutionStrategy_9() const { return ___mAutoConflictResolutionStrategy_9; }
	inline int32_t* get_address_of_mAutoConflictResolutionStrategy_9() { return &___mAutoConflictResolutionStrategy_9; }
	inline void set_mAutoConflictResolutionStrategy_9(int32_t value)
	{
		___mAutoConflictResolutionStrategy_9 = value;
	}

	inline static int32_t get_offset_of_mGpgsDataSource_10() { return static_cast<int32_t>(offsetof(GameServicesSettings_t1871988932, ___mGpgsDataSource_10)); }
	inline int32_t get_mGpgsDataSource_10() const { return ___mGpgsDataSource_10; }
	inline int32_t* get_address_of_mGpgsDataSource_10() { return &___mGpgsDataSource_10; }
	inline void set_mGpgsDataSource_10(int32_t value)
	{
		___mGpgsDataSource_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVICESSETTINGS_T1871988932_H
#ifndef DICTIONARY_ADPLACEMENT_ADID_T1141413887_H
#define DICTIONARY_ADPLACEMENT_ADID_T1141413887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Dictionary_AdPlacement_AdId
struct  Dictionary_AdPlacement_AdId_t1141413887  : public SerializableDictionary_2_t1077228432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_ADPLACEMENT_ADID_T1141413887_H
#ifndef STRINGADIDSERIALIZABLEDICTIONARY_T3534368925_H
#define STRINGADIDSERIALIZABLEDICTIONARY_T3534368925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.StringAdIdSerializableDictionary
struct  StringAdIdSerializableDictionary_t3534368925  : public SerializableDictionary_2_t1716507187
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGADIDSERIALIZABLEDICTIONARY_T3534368925_H
#ifndef STRINGSTRINGSERIALIZABLEDICTIONARY_T4078874517_H
#define STRINGSTRINGSERIALIZABLEDICTIONARY_T4078874517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.StringStringSerializableDictionary
struct  StringStringSerializableDictionary_t4078874517  : public SerializableDictionary_2_t2480958850
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSTRINGSERIALIZABLEDICTIONARY_T4078874517_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ADSETTINGS_T94672728_H
#define ADSETTINGS_T94672728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AdSettings
struct  AdSettings_t94672728  : public RuntimeObject
{
public:
	// EasyMobile.AutoAdLoadingMode EasyMobile.AdSettings::mAutoLoadAdsMode
	int32_t ___mAutoLoadAdsMode_0;
	// System.Single EasyMobile.AdSettings::mAdCheckingInterval
	float ___mAdCheckingInterval_1;
	// System.Single EasyMobile.AdSettings::mAdLoadingInterval
	float ___mAdLoadingInterval_2;
	// EasyMobile.AdSettings/DefaultAdNetworks EasyMobile.AdSettings::mIosDefaultAdNetworks
	DefaultAdNetworks_t2678507152  ___mIosDefaultAdNetworks_3;
	// EasyMobile.AdSettings/DefaultAdNetworks EasyMobile.AdSettings::mAndroidDefaultAdNetworks
	DefaultAdNetworks_t2678507152  ___mAndroidDefaultAdNetworks_4;
	// EasyMobile.AdColonySettings EasyMobile.AdSettings::mAdColony
	AdColonySettings_t2994520974 * ___mAdColony_5;
	// EasyMobile.AdMobSettings EasyMobile.AdSettings::mAdMob
	AdMobSettings_t1707484206 * ___mAdMob_6;
	// EasyMobile.AudienceNetworkSettings EasyMobile.AdSettings::mFBAudience
	AudienceNetworkSettings_t1217453211 * ___mFBAudience_7;
	// EasyMobile.ChartboostSettings EasyMobile.AdSettings::mChartboost
	ChartboostSettings_t2523652890 * ___mChartboost_8;
	// EasyMobile.HeyzapSettings EasyMobile.AdSettings::mHeyzap
	HeyzapSettings_t3928682395 * ___mHeyzap_9;
	// EasyMobile.IronSourceSettings EasyMobile.AdSettings::mIronSource
	IronSourceSettings_t2849155294 * ___mIronSource_10;
	// EasyMobile.MoPubSettings EasyMobile.AdSettings::mMoPub
	MoPubSettings_t3452462547 * ___mMoPub_11;
	// EasyMobile.TapjoySettings EasyMobile.AdSettings::mTapjoy
	TapjoySettings_t205532585 * ___mTapjoy_12;
	// EasyMobile.UnityAdsSettings EasyMobile.AdSettings::mUnityAds
	UnityAdsSettings_t2563580717 * ___mUnityAds_13;

public:
	inline static int32_t get_offset_of_mAutoLoadAdsMode_0() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mAutoLoadAdsMode_0)); }
	inline int32_t get_mAutoLoadAdsMode_0() const { return ___mAutoLoadAdsMode_0; }
	inline int32_t* get_address_of_mAutoLoadAdsMode_0() { return &___mAutoLoadAdsMode_0; }
	inline void set_mAutoLoadAdsMode_0(int32_t value)
	{
		___mAutoLoadAdsMode_0 = value;
	}

	inline static int32_t get_offset_of_mAdCheckingInterval_1() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mAdCheckingInterval_1)); }
	inline float get_mAdCheckingInterval_1() const { return ___mAdCheckingInterval_1; }
	inline float* get_address_of_mAdCheckingInterval_1() { return &___mAdCheckingInterval_1; }
	inline void set_mAdCheckingInterval_1(float value)
	{
		___mAdCheckingInterval_1 = value;
	}

	inline static int32_t get_offset_of_mAdLoadingInterval_2() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mAdLoadingInterval_2)); }
	inline float get_mAdLoadingInterval_2() const { return ___mAdLoadingInterval_2; }
	inline float* get_address_of_mAdLoadingInterval_2() { return &___mAdLoadingInterval_2; }
	inline void set_mAdLoadingInterval_2(float value)
	{
		___mAdLoadingInterval_2 = value;
	}

	inline static int32_t get_offset_of_mIosDefaultAdNetworks_3() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mIosDefaultAdNetworks_3)); }
	inline DefaultAdNetworks_t2678507152  get_mIosDefaultAdNetworks_3() const { return ___mIosDefaultAdNetworks_3; }
	inline DefaultAdNetworks_t2678507152 * get_address_of_mIosDefaultAdNetworks_3() { return &___mIosDefaultAdNetworks_3; }
	inline void set_mIosDefaultAdNetworks_3(DefaultAdNetworks_t2678507152  value)
	{
		___mIosDefaultAdNetworks_3 = value;
	}

	inline static int32_t get_offset_of_mAndroidDefaultAdNetworks_4() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mAndroidDefaultAdNetworks_4)); }
	inline DefaultAdNetworks_t2678507152  get_mAndroidDefaultAdNetworks_4() const { return ___mAndroidDefaultAdNetworks_4; }
	inline DefaultAdNetworks_t2678507152 * get_address_of_mAndroidDefaultAdNetworks_4() { return &___mAndroidDefaultAdNetworks_4; }
	inline void set_mAndroidDefaultAdNetworks_4(DefaultAdNetworks_t2678507152  value)
	{
		___mAndroidDefaultAdNetworks_4 = value;
	}

	inline static int32_t get_offset_of_mAdColony_5() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mAdColony_5)); }
	inline AdColonySettings_t2994520974 * get_mAdColony_5() const { return ___mAdColony_5; }
	inline AdColonySettings_t2994520974 ** get_address_of_mAdColony_5() { return &___mAdColony_5; }
	inline void set_mAdColony_5(AdColonySettings_t2994520974 * value)
	{
		___mAdColony_5 = value;
		Il2CppCodeGenWriteBarrier((&___mAdColony_5), value);
	}

	inline static int32_t get_offset_of_mAdMob_6() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mAdMob_6)); }
	inline AdMobSettings_t1707484206 * get_mAdMob_6() const { return ___mAdMob_6; }
	inline AdMobSettings_t1707484206 ** get_address_of_mAdMob_6() { return &___mAdMob_6; }
	inline void set_mAdMob_6(AdMobSettings_t1707484206 * value)
	{
		___mAdMob_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAdMob_6), value);
	}

	inline static int32_t get_offset_of_mFBAudience_7() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mFBAudience_7)); }
	inline AudienceNetworkSettings_t1217453211 * get_mFBAudience_7() const { return ___mFBAudience_7; }
	inline AudienceNetworkSettings_t1217453211 ** get_address_of_mFBAudience_7() { return &___mFBAudience_7; }
	inline void set_mFBAudience_7(AudienceNetworkSettings_t1217453211 * value)
	{
		___mFBAudience_7 = value;
		Il2CppCodeGenWriteBarrier((&___mFBAudience_7), value);
	}

	inline static int32_t get_offset_of_mChartboost_8() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mChartboost_8)); }
	inline ChartboostSettings_t2523652890 * get_mChartboost_8() const { return ___mChartboost_8; }
	inline ChartboostSettings_t2523652890 ** get_address_of_mChartboost_8() { return &___mChartboost_8; }
	inline void set_mChartboost_8(ChartboostSettings_t2523652890 * value)
	{
		___mChartboost_8 = value;
		Il2CppCodeGenWriteBarrier((&___mChartboost_8), value);
	}

	inline static int32_t get_offset_of_mHeyzap_9() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mHeyzap_9)); }
	inline HeyzapSettings_t3928682395 * get_mHeyzap_9() const { return ___mHeyzap_9; }
	inline HeyzapSettings_t3928682395 ** get_address_of_mHeyzap_9() { return &___mHeyzap_9; }
	inline void set_mHeyzap_9(HeyzapSettings_t3928682395 * value)
	{
		___mHeyzap_9 = value;
		Il2CppCodeGenWriteBarrier((&___mHeyzap_9), value);
	}

	inline static int32_t get_offset_of_mIronSource_10() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mIronSource_10)); }
	inline IronSourceSettings_t2849155294 * get_mIronSource_10() const { return ___mIronSource_10; }
	inline IronSourceSettings_t2849155294 ** get_address_of_mIronSource_10() { return &___mIronSource_10; }
	inline void set_mIronSource_10(IronSourceSettings_t2849155294 * value)
	{
		___mIronSource_10 = value;
		Il2CppCodeGenWriteBarrier((&___mIronSource_10), value);
	}

	inline static int32_t get_offset_of_mMoPub_11() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mMoPub_11)); }
	inline MoPubSettings_t3452462547 * get_mMoPub_11() const { return ___mMoPub_11; }
	inline MoPubSettings_t3452462547 ** get_address_of_mMoPub_11() { return &___mMoPub_11; }
	inline void set_mMoPub_11(MoPubSettings_t3452462547 * value)
	{
		___mMoPub_11 = value;
		Il2CppCodeGenWriteBarrier((&___mMoPub_11), value);
	}

	inline static int32_t get_offset_of_mTapjoy_12() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mTapjoy_12)); }
	inline TapjoySettings_t205532585 * get_mTapjoy_12() const { return ___mTapjoy_12; }
	inline TapjoySettings_t205532585 ** get_address_of_mTapjoy_12() { return &___mTapjoy_12; }
	inline void set_mTapjoy_12(TapjoySettings_t205532585 * value)
	{
		___mTapjoy_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTapjoy_12), value);
	}

	inline static int32_t get_offset_of_mUnityAds_13() { return static_cast<int32_t>(offsetof(AdSettings_t94672728, ___mUnityAds_13)); }
	inline UnityAdsSettings_t2563580717 * get_mUnityAds_13() const { return ___mUnityAds_13; }
	inline UnityAdsSettings_t2563580717 ** get_address_of_mUnityAds_13() { return &___mUnityAds_13; }
	inline void set_mUnityAds_13(UnityAdsSettings_t2563580717 * value)
	{
		___mUnityAds_13 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityAds_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSETTINGS_T94672728_H
#ifndef EM_SETTINGS_T1671298574_H
#define EM_SETTINGS_T1671298574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EM_Settings
struct  EM_Settings_t1671298574  : public ScriptableObject_t2528358522
{
public:
	// EasyMobile.AdSettings EasyMobile.EM_Settings::mAdvertisingSettings
	AdSettings_t94672728 * ___mAdvertisingSettings_5;
	// EasyMobile.GameServicesSettings EasyMobile.EM_Settings::mGameServiceSettings
	GameServicesSettings_t1871988932 * ___mGameServiceSettings_6;
	// EasyMobile.IAPSettings EasyMobile.EM_Settings::mInAppPurchaseSettings
	IAPSettings_t189197306 * ___mInAppPurchaseSettings_7;
	// EasyMobile.NotificationsSettings EasyMobile.EM_Settings::mNotificationSettings
	NotificationsSettings_t3829333496 * ___mNotificationSettings_8;
	// EasyMobile.PrivacySettings EasyMobile.EM_Settings::mPrivacySettings
	PrivacySettings_t2445251604 * ___mPrivacySettings_9;
	// EasyMobile.RatingRequestSettings EasyMobile.EM_Settings::mRatingRequestSettings
	RatingRequestSettings_t4213169708 * ___mRatingRequestSettings_10;
	// System.Boolean EasyMobile.EM_Settings::mIsAdModuleEnable
	bool ___mIsAdModuleEnable_11;
	// System.Boolean EasyMobile.EM_Settings::mIsIAPModuleEnable
	bool ___mIsIAPModuleEnable_12;
	// System.Boolean EasyMobile.EM_Settings::mIsGameServiceModuleEnable
	bool ___mIsGameServiceModuleEnable_13;
	// System.Boolean EasyMobile.EM_Settings::mIsNotificationModuleEnable
	bool ___mIsNotificationModuleEnable_14;

public:
	inline static int32_t get_offset_of_mAdvertisingSettings_5() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mAdvertisingSettings_5)); }
	inline AdSettings_t94672728 * get_mAdvertisingSettings_5() const { return ___mAdvertisingSettings_5; }
	inline AdSettings_t94672728 ** get_address_of_mAdvertisingSettings_5() { return &___mAdvertisingSettings_5; }
	inline void set_mAdvertisingSettings_5(AdSettings_t94672728 * value)
	{
		___mAdvertisingSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___mAdvertisingSettings_5), value);
	}

	inline static int32_t get_offset_of_mGameServiceSettings_6() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mGameServiceSettings_6)); }
	inline GameServicesSettings_t1871988932 * get_mGameServiceSettings_6() const { return ___mGameServiceSettings_6; }
	inline GameServicesSettings_t1871988932 ** get_address_of_mGameServiceSettings_6() { return &___mGameServiceSettings_6; }
	inline void set_mGameServiceSettings_6(GameServicesSettings_t1871988932 * value)
	{
		___mGameServiceSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___mGameServiceSettings_6), value);
	}

	inline static int32_t get_offset_of_mInAppPurchaseSettings_7() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mInAppPurchaseSettings_7)); }
	inline IAPSettings_t189197306 * get_mInAppPurchaseSettings_7() const { return ___mInAppPurchaseSettings_7; }
	inline IAPSettings_t189197306 ** get_address_of_mInAppPurchaseSettings_7() { return &___mInAppPurchaseSettings_7; }
	inline void set_mInAppPurchaseSettings_7(IAPSettings_t189197306 * value)
	{
		___mInAppPurchaseSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInAppPurchaseSettings_7), value);
	}

	inline static int32_t get_offset_of_mNotificationSettings_8() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mNotificationSettings_8)); }
	inline NotificationsSettings_t3829333496 * get_mNotificationSettings_8() const { return ___mNotificationSettings_8; }
	inline NotificationsSettings_t3829333496 ** get_address_of_mNotificationSettings_8() { return &___mNotificationSettings_8; }
	inline void set_mNotificationSettings_8(NotificationsSettings_t3829333496 * value)
	{
		___mNotificationSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___mNotificationSettings_8), value);
	}

	inline static int32_t get_offset_of_mPrivacySettings_9() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mPrivacySettings_9)); }
	inline PrivacySettings_t2445251604 * get_mPrivacySettings_9() const { return ___mPrivacySettings_9; }
	inline PrivacySettings_t2445251604 ** get_address_of_mPrivacySettings_9() { return &___mPrivacySettings_9; }
	inline void set_mPrivacySettings_9(PrivacySettings_t2445251604 * value)
	{
		___mPrivacySettings_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPrivacySettings_9), value);
	}

	inline static int32_t get_offset_of_mRatingRequestSettings_10() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mRatingRequestSettings_10)); }
	inline RatingRequestSettings_t4213169708 * get_mRatingRequestSettings_10() const { return ___mRatingRequestSettings_10; }
	inline RatingRequestSettings_t4213169708 ** get_address_of_mRatingRequestSettings_10() { return &___mRatingRequestSettings_10; }
	inline void set_mRatingRequestSettings_10(RatingRequestSettings_t4213169708 * value)
	{
		___mRatingRequestSettings_10 = value;
		Il2CppCodeGenWriteBarrier((&___mRatingRequestSettings_10), value);
	}

	inline static int32_t get_offset_of_mIsAdModuleEnable_11() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mIsAdModuleEnable_11)); }
	inline bool get_mIsAdModuleEnable_11() const { return ___mIsAdModuleEnable_11; }
	inline bool* get_address_of_mIsAdModuleEnable_11() { return &___mIsAdModuleEnable_11; }
	inline void set_mIsAdModuleEnable_11(bool value)
	{
		___mIsAdModuleEnable_11 = value;
	}

	inline static int32_t get_offset_of_mIsIAPModuleEnable_12() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mIsIAPModuleEnable_12)); }
	inline bool get_mIsIAPModuleEnable_12() const { return ___mIsIAPModuleEnable_12; }
	inline bool* get_address_of_mIsIAPModuleEnable_12() { return &___mIsIAPModuleEnable_12; }
	inline void set_mIsIAPModuleEnable_12(bool value)
	{
		___mIsIAPModuleEnable_12 = value;
	}

	inline static int32_t get_offset_of_mIsGameServiceModuleEnable_13() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mIsGameServiceModuleEnable_13)); }
	inline bool get_mIsGameServiceModuleEnable_13() const { return ___mIsGameServiceModuleEnable_13; }
	inline bool* get_address_of_mIsGameServiceModuleEnable_13() { return &___mIsGameServiceModuleEnable_13; }
	inline void set_mIsGameServiceModuleEnable_13(bool value)
	{
		___mIsGameServiceModuleEnable_13 = value;
	}

	inline static int32_t get_offset_of_mIsNotificationModuleEnable_14() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574, ___mIsNotificationModuleEnable_14)); }
	inline bool get_mIsNotificationModuleEnable_14() const { return ___mIsNotificationModuleEnable_14; }
	inline bool* get_address_of_mIsNotificationModuleEnable_14() { return &___mIsNotificationModuleEnable_14; }
	inline void set_mIsNotificationModuleEnable_14(bool value)
	{
		___mIsNotificationModuleEnable_14 = value;
	}
};

struct EM_Settings_t1671298574_StaticFields
{
public:
	// EasyMobile.EM_Settings EasyMobile.EM_Settings::sInstance
	EM_Settings_t1671298574 * ___sInstance_4;

public:
	inline static int32_t get_offset_of_sInstance_4() { return static_cast<int32_t>(offsetof(EM_Settings_t1671298574_StaticFields, ___sInstance_4)); }
	inline EM_Settings_t1671298574 * get_sInstance_4() const { return ___sInstance_4; }
	inline EM_Settings_t1671298574 ** get_address_of_sInstance_4() { return &___sInstance_4; }
	inline void set_sInstance_4(EM_Settings_t1671298574 * value)
	{
		___sInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EM_SETTINGS_T1671298574_H
#ifndef U3CDONEXTLOADSCOREREQUESTU3EC__ANONSTOREY5_T2350042143_H
#define U3CDONEXTLOADSCOREREQUESTU3EC__ANONSTOREY5_T2350042143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices/<DoNextLoadScoreRequest>c__AnonStorey5
struct  U3CDoNextLoadScoreRequestU3Ec__AnonStorey5_t2350042143  : public RuntimeObject
{
public:
	// EasyMobile.GameServices/LoadScoreRequest EasyMobile.GameServices/<DoNextLoadScoreRequest>c__AnonStorey5::request
	LoadScoreRequest_t1342227622  ___request_0;
	// UnityEngine.SocialPlatforms.ILeaderboard EasyMobile.GameServices/<DoNextLoadScoreRequest>c__AnonStorey5::ldb
	RuntimeObject* ___ldb_1;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CDoNextLoadScoreRequestU3Ec__AnonStorey5_t2350042143, ___request_0)); }
	inline LoadScoreRequest_t1342227622  get_request_0() const { return ___request_0; }
	inline LoadScoreRequest_t1342227622 * get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(LoadScoreRequest_t1342227622  value)
	{
		___request_0 = value;
	}

	inline static int32_t get_offset_of_ldb_1() { return static_cast<int32_t>(offsetof(U3CDoNextLoadScoreRequestU3Ec__AnonStorey5_t2350042143, ___ldb_1)); }
	inline RuntimeObject* get_ldb_1() const { return ___ldb_1; }
	inline RuntimeObject** get_address_of_ldb_1() { return &___ldb_1; }
	inline void set_ldb_1(RuntimeObject* value)
	{
		___ldb_1 = value;
		Il2CppCodeGenWriteBarrier((&___ldb_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDONEXTLOADSCOREREQUESTU3EC__ANONSTOREY5_T2350042143_H
#ifndef FETCHSAVEDGAMESCALLBACK_T3515465237_H
#define FETCHSAVEDGAMESCALLBACK_T3515465237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/FetchSavedGamesCallback
struct  FetchSavedGamesCallback_t3515465237  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FETCHSAVEDGAMESCALLBACK_T3515465237_H
#ifndef LOADSAVEDGAMEDATACALLBACK_T1958165752_H
#define LOADSAVEDGAMEDATACALLBACK_T1958165752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/LoadSavedGameDataCallback
struct  LoadSavedGameDataCallback_t1958165752  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSAVEDGAMEDATACALLBACK_T1958165752_H
#ifndef OPENSAVEDGAMECALLBACK_T2349091444_H
#define OPENSAVEDGAMECALLBACK_T2349091444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/OpenSavedGameCallback
struct  OpenSavedGameCallback_t2349091444  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENSAVEDGAMECALLBACK_T2349091444_H
#ifndef SAVEGAMEDATACALLBACK_T3612996042_H
#define SAVEGAMEDATACALLBACK_T3612996042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/SaveGameDataCallback
struct  SaveGameDataCallback_t3612996042  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEGAMEDATACALLBACK_T3612996042_H
#ifndef SAVEDGAMECONFLICTRESOLVER_T2056225930_H
#define SAVEDGAMECONFLICTRESOLVER_T2056225930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.SavedGameConflictResolver
struct  SavedGameConflictResolver_t2056225930  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDGAMECONFLICTRESOLVER_T2056225930_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ADVERTISING_T2540168094_H
#define ADVERTISING_T2540168094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Advertising
struct  Advertising_t2540168094  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Advertising_t2540168094_StaticFields
{
public:
	// EasyMobile.Advertising EasyMobile.Advertising::<Instance>k__BackingField
	Advertising_t2540168094 * ___U3CInstanceU3Ek__BackingField_4;
	// EasyMobile.AdColonyClientImpl EasyMobile.Advertising::sAdColonyClient
	AdColonyClientImpl_t352798180 * ___sAdColonyClient_5;
	// EasyMobile.AdMobClientImpl EasyMobile.Advertising::sAdMobClient
	AdMobClientImpl_t3823742600 * ___sAdMobClient_6;
	// EasyMobile.ChartboostClientImpl EasyMobile.Advertising::sChartboostClient
	ChartboostClientImpl_t602894833 * ___sChartboostClient_7;
	// EasyMobile.AudienceNetworkClientImpl EasyMobile.Advertising::sAudienceNetworkClient
	AudienceNetworkClientImpl_t266954160 * ___sAudienceNetworkClient_8;
	// EasyMobile.HeyzapClientImpl EasyMobile.Advertising::sHeyzapClient
	HeyzapClientImpl_t2848241831 * ___sHeyzapClient_9;
	// EasyMobile.MoPubClientImpl EasyMobile.Advertising::sMoPubClient
	MoPubClientImpl_t784383640 * ___sMoPubClient_10;
	// EasyMobile.IronSourceClientImpl EasyMobile.Advertising::sIronSourceClient
	IronSourceClientImpl_t41919987 * ___sIronSourceClient_11;
	// EasyMobile.TapjoyClientImpl EasyMobile.Advertising::sTapjoyClient
	TapjoyClientImpl_t2003437302 * ___sTapjoyClient_12;
	// EasyMobile.UnityAdsClientImpl EasyMobile.Advertising::sUnityAdsClient
	UnityAdsClientImpl_t4105509132 * ___sUnityAdsClient_13;
	// EasyMobile.AdClientImpl EasyMobile.Advertising::sDefaultBannerAdClient
	AdClientImpl_t879350116 * ___sDefaultBannerAdClient_14;
	// EasyMobile.AdClientImpl EasyMobile.Advertising::sDefaultInterstitialAdClient
	AdClientImpl_t879350116 * ___sDefaultInterstitialAdClient_15;
	// EasyMobile.AdClientImpl EasyMobile.Advertising::sDefaultRewardedAdClient
	AdClientImpl_t879350116 * ___sDefaultRewardedAdClient_16;
	// System.Single EasyMobile.Advertising::DEFAULT_TIMESTAMP
	float ___DEFAULT_TIMESTAMP_20;
	// System.Collections.IEnumerator EasyMobile.Advertising::autoLoadAdsCoroutine
	RuntimeObject* ___autoLoadAdsCoroutine_21;
	// EasyMobile.AutoAdLoadingMode EasyMobile.Advertising::currentAutoLoadAdsMode
	int32_t ___currentAutoLoadAdsMode_22;
	// System.Single EasyMobile.Advertising::lastDefaultInterstitialAdLoadTimestamp
	float ___lastDefaultInterstitialAdLoadTimestamp_23;
	// System.Single EasyMobile.Advertising::lastDefaultRewardedAdLoadTimestamp
	float ___lastDefaultRewardedAdLoadTimestamp_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> EasyMobile.Advertising::lastCustomInterstitialAdsLoadTimestamp
	Dictionary_2_t1182523073 * ___lastCustomInterstitialAdsLoadTimestamp_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> EasyMobile.Advertising::lastCustomRewardedAdsLoadTimestamp
	Dictionary_2_t1182523073 * ___lastCustomRewardedAdsLoadTimestamp_26;
	// System.Boolean EasyMobile.Advertising::isUpdatingAutoLoadMode
	bool ___isUpdatingAutoLoadMode_27;
	// System.Collections.Generic.Dictionary`2<EasyMobile.AdNetwork,System.Collections.Generic.List`1<EasyMobile.AdPlacement>> EasyMobile.Advertising::activeBannerAds
	Dictionary_2_t2490943318 * ___activeBannerAds_28;
	// System.Action`2<EasyMobile.InterstitialAdNetwork,EasyMobile.AdPlacement> EasyMobile.Advertising::InterstitialAdCompleted
	Action_2_t2057961651 * ___InterstitialAdCompleted_29;
	// System.Action`2<EasyMobile.RewardedAdNetwork,EasyMobile.AdPlacement> EasyMobile.Advertising::RewardedAdSkipped
	Action_2_t3281220897 * ___RewardedAdSkipped_30;
	// System.Action`2<EasyMobile.RewardedAdNetwork,EasyMobile.AdPlacement> EasyMobile.Advertising::RewardedAdCompleted
	Action_2_t3281220897 * ___RewardedAdCompleted_31;
	// System.Action EasyMobile.Advertising::AdsRemoved
	Action_t1264377477 * ___AdsRemoved_32;
	// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement> EasyMobile.Advertising::<>f__mg$cache0
	Action_2_t3839421581 * ___U3CU3Ef__mgU24cache0_33;
	// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement> EasyMobile.Advertising::<>f__mg$cache1
	Action_2_t3839421581 * ___U3CU3Ef__mgU24cache1_34;
	// System.Action`2<EasyMobile.IAdClient,EasyMobile.AdPlacement> EasyMobile.Advertising::<>f__mg$cache2
	Action_2_t3839421581 * ___U3CU3Ef__mgU24cache2_35;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Advertising_t2540168094 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Advertising_t2540168094 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Advertising_t2540168094 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_sAdColonyClient_5() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sAdColonyClient_5)); }
	inline AdColonyClientImpl_t352798180 * get_sAdColonyClient_5() const { return ___sAdColonyClient_5; }
	inline AdColonyClientImpl_t352798180 ** get_address_of_sAdColonyClient_5() { return &___sAdColonyClient_5; }
	inline void set_sAdColonyClient_5(AdColonyClientImpl_t352798180 * value)
	{
		___sAdColonyClient_5 = value;
		Il2CppCodeGenWriteBarrier((&___sAdColonyClient_5), value);
	}

	inline static int32_t get_offset_of_sAdMobClient_6() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sAdMobClient_6)); }
	inline AdMobClientImpl_t3823742600 * get_sAdMobClient_6() const { return ___sAdMobClient_6; }
	inline AdMobClientImpl_t3823742600 ** get_address_of_sAdMobClient_6() { return &___sAdMobClient_6; }
	inline void set_sAdMobClient_6(AdMobClientImpl_t3823742600 * value)
	{
		___sAdMobClient_6 = value;
		Il2CppCodeGenWriteBarrier((&___sAdMobClient_6), value);
	}

	inline static int32_t get_offset_of_sChartboostClient_7() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sChartboostClient_7)); }
	inline ChartboostClientImpl_t602894833 * get_sChartboostClient_7() const { return ___sChartboostClient_7; }
	inline ChartboostClientImpl_t602894833 ** get_address_of_sChartboostClient_7() { return &___sChartboostClient_7; }
	inline void set_sChartboostClient_7(ChartboostClientImpl_t602894833 * value)
	{
		___sChartboostClient_7 = value;
		Il2CppCodeGenWriteBarrier((&___sChartboostClient_7), value);
	}

	inline static int32_t get_offset_of_sAudienceNetworkClient_8() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sAudienceNetworkClient_8)); }
	inline AudienceNetworkClientImpl_t266954160 * get_sAudienceNetworkClient_8() const { return ___sAudienceNetworkClient_8; }
	inline AudienceNetworkClientImpl_t266954160 ** get_address_of_sAudienceNetworkClient_8() { return &___sAudienceNetworkClient_8; }
	inline void set_sAudienceNetworkClient_8(AudienceNetworkClientImpl_t266954160 * value)
	{
		___sAudienceNetworkClient_8 = value;
		Il2CppCodeGenWriteBarrier((&___sAudienceNetworkClient_8), value);
	}

	inline static int32_t get_offset_of_sHeyzapClient_9() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sHeyzapClient_9)); }
	inline HeyzapClientImpl_t2848241831 * get_sHeyzapClient_9() const { return ___sHeyzapClient_9; }
	inline HeyzapClientImpl_t2848241831 ** get_address_of_sHeyzapClient_9() { return &___sHeyzapClient_9; }
	inline void set_sHeyzapClient_9(HeyzapClientImpl_t2848241831 * value)
	{
		___sHeyzapClient_9 = value;
		Il2CppCodeGenWriteBarrier((&___sHeyzapClient_9), value);
	}

	inline static int32_t get_offset_of_sMoPubClient_10() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sMoPubClient_10)); }
	inline MoPubClientImpl_t784383640 * get_sMoPubClient_10() const { return ___sMoPubClient_10; }
	inline MoPubClientImpl_t784383640 ** get_address_of_sMoPubClient_10() { return &___sMoPubClient_10; }
	inline void set_sMoPubClient_10(MoPubClientImpl_t784383640 * value)
	{
		___sMoPubClient_10 = value;
		Il2CppCodeGenWriteBarrier((&___sMoPubClient_10), value);
	}

	inline static int32_t get_offset_of_sIronSourceClient_11() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sIronSourceClient_11)); }
	inline IronSourceClientImpl_t41919987 * get_sIronSourceClient_11() const { return ___sIronSourceClient_11; }
	inline IronSourceClientImpl_t41919987 ** get_address_of_sIronSourceClient_11() { return &___sIronSourceClient_11; }
	inline void set_sIronSourceClient_11(IronSourceClientImpl_t41919987 * value)
	{
		___sIronSourceClient_11 = value;
		Il2CppCodeGenWriteBarrier((&___sIronSourceClient_11), value);
	}

	inline static int32_t get_offset_of_sTapjoyClient_12() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sTapjoyClient_12)); }
	inline TapjoyClientImpl_t2003437302 * get_sTapjoyClient_12() const { return ___sTapjoyClient_12; }
	inline TapjoyClientImpl_t2003437302 ** get_address_of_sTapjoyClient_12() { return &___sTapjoyClient_12; }
	inline void set_sTapjoyClient_12(TapjoyClientImpl_t2003437302 * value)
	{
		___sTapjoyClient_12 = value;
		Il2CppCodeGenWriteBarrier((&___sTapjoyClient_12), value);
	}

	inline static int32_t get_offset_of_sUnityAdsClient_13() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sUnityAdsClient_13)); }
	inline UnityAdsClientImpl_t4105509132 * get_sUnityAdsClient_13() const { return ___sUnityAdsClient_13; }
	inline UnityAdsClientImpl_t4105509132 ** get_address_of_sUnityAdsClient_13() { return &___sUnityAdsClient_13; }
	inline void set_sUnityAdsClient_13(UnityAdsClientImpl_t4105509132 * value)
	{
		___sUnityAdsClient_13 = value;
		Il2CppCodeGenWriteBarrier((&___sUnityAdsClient_13), value);
	}

	inline static int32_t get_offset_of_sDefaultBannerAdClient_14() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sDefaultBannerAdClient_14)); }
	inline AdClientImpl_t879350116 * get_sDefaultBannerAdClient_14() const { return ___sDefaultBannerAdClient_14; }
	inline AdClientImpl_t879350116 ** get_address_of_sDefaultBannerAdClient_14() { return &___sDefaultBannerAdClient_14; }
	inline void set_sDefaultBannerAdClient_14(AdClientImpl_t879350116 * value)
	{
		___sDefaultBannerAdClient_14 = value;
		Il2CppCodeGenWriteBarrier((&___sDefaultBannerAdClient_14), value);
	}

	inline static int32_t get_offset_of_sDefaultInterstitialAdClient_15() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sDefaultInterstitialAdClient_15)); }
	inline AdClientImpl_t879350116 * get_sDefaultInterstitialAdClient_15() const { return ___sDefaultInterstitialAdClient_15; }
	inline AdClientImpl_t879350116 ** get_address_of_sDefaultInterstitialAdClient_15() { return &___sDefaultInterstitialAdClient_15; }
	inline void set_sDefaultInterstitialAdClient_15(AdClientImpl_t879350116 * value)
	{
		___sDefaultInterstitialAdClient_15 = value;
		Il2CppCodeGenWriteBarrier((&___sDefaultInterstitialAdClient_15), value);
	}

	inline static int32_t get_offset_of_sDefaultRewardedAdClient_16() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___sDefaultRewardedAdClient_16)); }
	inline AdClientImpl_t879350116 * get_sDefaultRewardedAdClient_16() const { return ___sDefaultRewardedAdClient_16; }
	inline AdClientImpl_t879350116 ** get_address_of_sDefaultRewardedAdClient_16() { return &___sDefaultRewardedAdClient_16; }
	inline void set_sDefaultRewardedAdClient_16(AdClientImpl_t879350116 * value)
	{
		___sDefaultRewardedAdClient_16 = value;
		Il2CppCodeGenWriteBarrier((&___sDefaultRewardedAdClient_16), value);
	}

	inline static int32_t get_offset_of_DEFAULT_TIMESTAMP_20() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___DEFAULT_TIMESTAMP_20)); }
	inline float get_DEFAULT_TIMESTAMP_20() const { return ___DEFAULT_TIMESTAMP_20; }
	inline float* get_address_of_DEFAULT_TIMESTAMP_20() { return &___DEFAULT_TIMESTAMP_20; }
	inline void set_DEFAULT_TIMESTAMP_20(float value)
	{
		___DEFAULT_TIMESTAMP_20 = value;
	}

	inline static int32_t get_offset_of_autoLoadAdsCoroutine_21() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___autoLoadAdsCoroutine_21)); }
	inline RuntimeObject* get_autoLoadAdsCoroutine_21() const { return ___autoLoadAdsCoroutine_21; }
	inline RuntimeObject** get_address_of_autoLoadAdsCoroutine_21() { return &___autoLoadAdsCoroutine_21; }
	inline void set_autoLoadAdsCoroutine_21(RuntimeObject* value)
	{
		___autoLoadAdsCoroutine_21 = value;
		Il2CppCodeGenWriteBarrier((&___autoLoadAdsCoroutine_21), value);
	}

	inline static int32_t get_offset_of_currentAutoLoadAdsMode_22() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___currentAutoLoadAdsMode_22)); }
	inline int32_t get_currentAutoLoadAdsMode_22() const { return ___currentAutoLoadAdsMode_22; }
	inline int32_t* get_address_of_currentAutoLoadAdsMode_22() { return &___currentAutoLoadAdsMode_22; }
	inline void set_currentAutoLoadAdsMode_22(int32_t value)
	{
		___currentAutoLoadAdsMode_22 = value;
	}

	inline static int32_t get_offset_of_lastDefaultInterstitialAdLoadTimestamp_23() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___lastDefaultInterstitialAdLoadTimestamp_23)); }
	inline float get_lastDefaultInterstitialAdLoadTimestamp_23() const { return ___lastDefaultInterstitialAdLoadTimestamp_23; }
	inline float* get_address_of_lastDefaultInterstitialAdLoadTimestamp_23() { return &___lastDefaultInterstitialAdLoadTimestamp_23; }
	inline void set_lastDefaultInterstitialAdLoadTimestamp_23(float value)
	{
		___lastDefaultInterstitialAdLoadTimestamp_23 = value;
	}

	inline static int32_t get_offset_of_lastDefaultRewardedAdLoadTimestamp_24() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___lastDefaultRewardedAdLoadTimestamp_24)); }
	inline float get_lastDefaultRewardedAdLoadTimestamp_24() const { return ___lastDefaultRewardedAdLoadTimestamp_24; }
	inline float* get_address_of_lastDefaultRewardedAdLoadTimestamp_24() { return &___lastDefaultRewardedAdLoadTimestamp_24; }
	inline void set_lastDefaultRewardedAdLoadTimestamp_24(float value)
	{
		___lastDefaultRewardedAdLoadTimestamp_24 = value;
	}

	inline static int32_t get_offset_of_lastCustomInterstitialAdsLoadTimestamp_25() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___lastCustomInterstitialAdsLoadTimestamp_25)); }
	inline Dictionary_2_t1182523073 * get_lastCustomInterstitialAdsLoadTimestamp_25() const { return ___lastCustomInterstitialAdsLoadTimestamp_25; }
	inline Dictionary_2_t1182523073 ** get_address_of_lastCustomInterstitialAdsLoadTimestamp_25() { return &___lastCustomInterstitialAdsLoadTimestamp_25; }
	inline void set_lastCustomInterstitialAdsLoadTimestamp_25(Dictionary_2_t1182523073 * value)
	{
		___lastCustomInterstitialAdsLoadTimestamp_25 = value;
		Il2CppCodeGenWriteBarrier((&___lastCustomInterstitialAdsLoadTimestamp_25), value);
	}

	inline static int32_t get_offset_of_lastCustomRewardedAdsLoadTimestamp_26() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___lastCustomRewardedAdsLoadTimestamp_26)); }
	inline Dictionary_2_t1182523073 * get_lastCustomRewardedAdsLoadTimestamp_26() const { return ___lastCustomRewardedAdsLoadTimestamp_26; }
	inline Dictionary_2_t1182523073 ** get_address_of_lastCustomRewardedAdsLoadTimestamp_26() { return &___lastCustomRewardedAdsLoadTimestamp_26; }
	inline void set_lastCustomRewardedAdsLoadTimestamp_26(Dictionary_2_t1182523073 * value)
	{
		___lastCustomRewardedAdsLoadTimestamp_26 = value;
		Il2CppCodeGenWriteBarrier((&___lastCustomRewardedAdsLoadTimestamp_26), value);
	}

	inline static int32_t get_offset_of_isUpdatingAutoLoadMode_27() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___isUpdatingAutoLoadMode_27)); }
	inline bool get_isUpdatingAutoLoadMode_27() const { return ___isUpdatingAutoLoadMode_27; }
	inline bool* get_address_of_isUpdatingAutoLoadMode_27() { return &___isUpdatingAutoLoadMode_27; }
	inline void set_isUpdatingAutoLoadMode_27(bool value)
	{
		___isUpdatingAutoLoadMode_27 = value;
	}

	inline static int32_t get_offset_of_activeBannerAds_28() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___activeBannerAds_28)); }
	inline Dictionary_2_t2490943318 * get_activeBannerAds_28() const { return ___activeBannerAds_28; }
	inline Dictionary_2_t2490943318 ** get_address_of_activeBannerAds_28() { return &___activeBannerAds_28; }
	inline void set_activeBannerAds_28(Dictionary_2_t2490943318 * value)
	{
		___activeBannerAds_28 = value;
		Il2CppCodeGenWriteBarrier((&___activeBannerAds_28), value);
	}

	inline static int32_t get_offset_of_InterstitialAdCompleted_29() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___InterstitialAdCompleted_29)); }
	inline Action_2_t2057961651 * get_InterstitialAdCompleted_29() const { return ___InterstitialAdCompleted_29; }
	inline Action_2_t2057961651 ** get_address_of_InterstitialAdCompleted_29() { return &___InterstitialAdCompleted_29; }
	inline void set_InterstitialAdCompleted_29(Action_2_t2057961651 * value)
	{
		___InterstitialAdCompleted_29 = value;
		Il2CppCodeGenWriteBarrier((&___InterstitialAdCompleted_29), value);
	}

	inline static int32_t get_offset_of_RewardedAdSkipped_30() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___RewardedAdSkipped_30)); }
	inline Action_2_t3281220897 * get_RewardedAdSkipped_30() const { return ___RewardedAdSkipped_30; }
	inline Action_2_t3281220897 ** get_address_of_RewardedAdSkipped_30() { return &___RewardedAdSkipped_30; }
	inline void set_RewardedAdSkipped_30(Action_2_t3281220897 * value)
	{
		___RewardedAdSkipped_30 = value;
		Il2CppCodeGenWriteBarrier((&___RewardedAdSkipped_30), value);
	}

	inline static int32_t get_offset_of_RewardedAdCompleted_31() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___RewardedAdCompleted_31)); }
	inline Action_2_t3281220897 * get_RewardedAdCompleted_31() const { return ___RewardedAdCompleted_31; }
	inline Action_2_t3281220897 ** get_address_of_RewardedAdCompleted_31() { return &___RewardedAdCompleted_31; }
	inline void set_RewardedAdCompleted_31(Action_2_t3281220897 * value)
	{
		___RewardedAdCompleted_31 = value;
		Il2CppCodeGenWriteBarrier((&___RewardedAdCompleted_31), value);
	}

	inline static int32_t get_offset_of_AdsRemoved_32() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___AdsRemoved_32)); }
	inline Action_t1264377477 * get_AdsRemoved_32() const { return ___AdsRemoved_32; }
	inline Action_t1264377477 ** get_address_of_AdsRemoved_32() { return &___AdsRemoved_32; }
	inline void set_AdsRemoved_32(Action_t1264377477 * value)
	{
		___AdsRemoved_32 = value;
		Il2CppCodeGenWriteBarrier((&___AdsRemoved_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_33() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___U3CU3Ef__mgU24cache0_33)); }
	inline Action_2_t3839421581 * get_U3CU3Ef__mgU24cache0_33() const { return ___U3CU3Ef__mgU24cache0_33; }
	inline Action_2_t3839421581 ** get_address_of_U3CU3Ef__mgU24cache0_33() { return &___U3CU3Ef__mgU24cache0_33; }
	inline void set_U3CU3Ef__mgU24cache0_33(Action_2_t3839421581 * value)
	{
		___U3CU3Ef__mgU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_34() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___U3CU3Ef__mgU24cache1_34)); }
	inline Action_2_t3839421581 * get_U3CU3Ef__mgU24cache1_34() const { return ___U3CU3Ef__mgU24cache1_34; }
	inline Action_2_t3839421581 ** get_address_of_U3CU3Ef__mgU24cache1_34() { return &___U3CU3Ef__mgU24cache1_34; }
	inline void set_U3CU3Ef__mgU24cache1_34(Action_2_t3839421581 * value)
	{
		___U3CU3Ef__mgU24cache1_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_35() { return static_cast<int32_t>(offsetof(Advertising_t2540168094_StaticFields, ___U3CU3Ef__mgU24cache2_35)); }
	inline Action_2_t3839421581 * get_U3CU3Ef__mgU24cache2_35() const { return ___U3CU3Ef__mgU24cache2_35; }
	inline Action_2_t3839421581 ** get_address_of_U3CU3Ef__mgU24cache2_35() { return &___U3CU3Ef__mgU24cache2_35; }
	inline void set_U3CU3Ef__mgU24cache2_35(Action_2_t3839421581 * value)
	{
		___U3CU3Ef__mgU24cache2_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVERTISING_T2540168094_H
#ifndef APPLIFECYCLEMANAGER_T3829366876_H
#define APPLIFECYCLEMANAGER_T3829366876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AppLifecycleManager
struct  AppLifecycleManager_t3829366876  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct AppLifecycleManager_t3829366876_StaticFields
{
public:
	// EasyMobile.AppLifecycleManager EasyMobile.AppLifecycleManager::<Instance>k__BackingField
	AppLifecycleManager_t3829366876 * ___U3CInstanceU3Ek__BackingField_4;
	// EasyMobile.Internal.IAppLifecycleHandler EasyMobile.AppLifecycleManager::sAppLifecycleHandler
	RuntimeObject* ___sAppLifecycleHandler_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppLifecycleManager_t3829366876_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline AppLifecycleManager_t3829366876 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline AppLifecycleManager_t3829366876 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(AppLifecycleManager_t3829366876 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_sAppLifecycleHandler_5() { return static_cast<int32_t>(offsetof(AppLifecycleManager_t3829366876_StaticFields, ___sAppLifecycleHandler_5)); }
	inline RuntimeObject* get_sAppLifecycleHandler_5() const { return ___sAppLifecycleHandler_5; }
	inline RuntimeObject** get_address_of_sAppLifecycleHandler_5() { return &___sAppLifecycleHandler_5; }
	inline void set_sAppLifecycleHandler_5(RuntimeObject* value)
	{
		___sAppLifecycleHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___sAppLifecycleHandler_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLIFECYCLEMANAGER_T3829366876_H
#ifndef GAMESERVICES_T1437699199_H
#define GAMESERVICES_T1437699199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GameServices
struct  GameServices_t1437699199  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct GameServices_t1437699199_StaticFields
{
public:
	// EasyMobile.GameServices EasyMobile.GameServices::<Instance>k__BackingField
	GameServices_t1437699199 * ___U3CInstanceU3Ek__BackingField_4;
	// System.Action EasyMobile.GameServices::UserLoginSucceeded
	Action_t1264377477 * ___UserLoginSucceeded_5;
	// System.Action EasyMobile.GameServices::UserLoginFailed
	Action_t1264377477 * ___UserLoginFailed_6;
	// System.Boolean EasyMobile.GameServices::isLoadingScore
	bool ___isLoadingScore_7;
	// System.Collections.Generic.List`1<EasyMobile.GameServices/LoadScoreRequest> EasyMobile.GameServices::loadScoreRequests
	List_1_t2814302364 * ___loadScoreRequests_8;
	// EasyMobile.ISavedGameClient EasyMobile.GameServices::sSavedGameClient
	RuntimeObject* ___sSavedGameClient_9;
	// System.Action`1<System.Boolean> EasyMobile.GameServices::<>f__mg$cache0
	Action_1_t269755560 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline GameServices_t1437699199 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline GameServices_t1437699199 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(GameServices_t1437699199 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_UserLoginSucceeded_5() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___UserLoginSucceeded_5)); }
	inline Action_t1264377477 * get_UserLoginSucceeded_5() const { return ___UserLoginSucceeded_5; }
	inline Action_t1264377477 ** get_address_of_UserLoginSucceeded_5() { return &___UserLoginSucceeded_5; }
	inline void set_UserLoginSucceeded_5(Action_t1264377477 * value)
	{
		___UserLoginSucceeded_5 = value;
		Il2CppCodeGenWriteBarrier((&___UserLoginSucceeded_5), value);
	}

	inline static int32_t get_offset_of_UserLoginFailed_6() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___UserLoginFailed_6)); }
	inline Action_t1264377477 * get_UserLoginFailed_6() const { return ___UserLoginFailed_6; }
	inline Action_t1264377477 ** get_address_of_UserLoginFailed_6() { return &___UserLoginFailed_6; }
	inline void set_UserLoginFailed_6(Action_t1264377477 * value)
	{
		___UserLoginFailed_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserLoginFailed_6), value);
	}

	inline static int32_t get_offset_of_isLoadingScore_7() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___isLoadingScore_7)); }
	inline bool get_isLoadingScore_7() const { return ___isLoadingScore_7; }
	inline bool* get_address_of_isLoadingScore_7() { return &___isLoadingScore_7; }
	inline void set_isLoadingScore_7(bool value)
	{
		___isLoadingScore_7 = value;
	}

	inline static int32_t get_offset_of_loadScoreRequests_8() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___loadScoreRequests_8)); }
	inline List_1_t2814302364 * get_loadScoreRequests_8() const { return ___loadScoreRequests_8; }
	inline List_1_t2814302364 ** get_address_of_loadScoreRequests_8() { return &___loadScoreRequests_8; }
	inline void set_loadScoreRequests_8(List_1_t2814302364 * value)
	{
		___loadScoreRequests_8 = value;
		Il2CppCodeGenWriteBarrier((&___loadScoreRequests_8), value);
	}

	inline static int32_t get_offset_of_sSavedGameClient_9() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___sSavedGameClient_9)); }
	inline RuntimeObject* get_sSavedGameClient_9() const { return ___sSavedGameClient_9; }
	inline RuntimeObject** get_address_of_sSavedGameClient_9() { return &___sSavedGameClient_9; }
	inline void set_sSavedGameClient_9(RuntimeObject* value)
	{
		___sSavedGameClient_9 = value;
		Il2CppCodeGenWriteBarrier((&___sSavedGameClient_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(GameServices_t1437699199_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline Action_1_t269755560 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(Action_1_t269755560 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVICES_T1437699199_H
#ifndef IRONSOURCEAPPSTATEHANDLER_T237138469_H
#define IRONSOURCEAPPSTATEHANDLER_T237138469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.IronSourceAppStateHandler
struct  IronSourceAppStateHandler_t237138469  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IRONSOURCEAPPSTATEHANDLER_T237138469_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4700 = { sizeof (Json_t2215844831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4701 = { sizeof (Parser_t2497838200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4701[2] = 
{
	0,
	Parser_t2497838200::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4702 = { sizeof (TOKEN_t1085193790)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4702[13] = 
{
	TOKEN_t1085193790::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4703 = { sizeof (Serializer_t2860470690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4703[1] = 
{
	Serializer_t2860470690::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4704 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4705 = { sizeof (SerializableDictionary_t2743353006), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4706 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4706[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4707 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4708 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4708[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4709 = { sizeof (StringStringSerializableDictionary_t4078874517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4710 = { sizeof (StringAdIdSerializableDictionary_t3534368925), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4711 = { sizeof (Dictionary_AdPlacement_AdId_t1141413887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4712 = { sizeof (FileUtil_t2362248366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4713 = { sizeof (PInvokeCallbackUtil_t3450891364), -1, sizeof(PInvokeCallbackUtil_t3450891364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4713[1] = 
{
	PInvokeCallbackUtil_t3450891364_StaticFields::get_offset_of_VERBOSE_DEBUG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4714 = { sizeof (Type_t3888639762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4714[3] = 
{
	Type_t3888639762::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4715 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4715[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4716 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4716[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4717 = { sizeof (PInvokeUtil_t2029783028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4718 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4719 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4719[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4720 = { sizeof (ReflectionUtil_t2959309983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4721 = { sizeof (StorageUtil_t403816348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4722 = { sizeof (Util_t3803880283), -1, sizeof(Util_t3803880283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4722[1] = 
{
	Util_t3803880283_StaticFields::get_offset_of_UnixEpoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4723 = { sizeof (AppLifecycleManager_t3829366876), -1, sizeof(AppLifecycleManager_t3829366876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4723[2] = 
{
	AppLifecycleManager_t3829366876_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	AppLifecycleManager_t3829366876_StaticFields::get_offset_of_sAppLifecycleHandler_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4724 = { sizeof (CrossPlatformId_t1390234332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4724[2] = 
{
	CrossPlatformId_t1390234332::get_offset_of_mIosId_0(),
	CrossPlatformId_t1390234332::get_offset_of_mAndroidId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4725 = { sizeof (EM_Settings_t1671298574), -1, sizeof(EM_Settings_t1671298574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4725[11] = 
{
	EM_Settings_t1671298574_StaticFields::get_offset_of_sInstance_4(),
	EM_Settings_t1671298574::get_offset_of_mAdvertisingSettings_5(),
	EM_Settings_t1671298574::get_offset_of_mGameServiceSettings_6(),
	EM_Settings_t1671298574::get_offset_of_mInAppPurchaseSettings_7(),
	EM_Settings_t1671298574::get_offset_of_mNotificationSettings_8(),
	EM_Settings_t1671298574::get_offset_of_mPrivacySettings_9(),
	EM_Settings_t1671298574::get_offset_of_mRatingRequestSettings_10(),
	EM_Settings_t1671298574::get_offset_of_mIsAdModuleEnable_11(),
	EM_Settings_t1671298574::get_offset_of_mIsIAPModuleEnable_12(),
	EM_Settings_t1671298574::get_offset_of_mIsGameServiceModuleEnable_13(),
	EM_Settings_t1671298574::get_offset_of_mIsNotificationModuleEnable_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4726 = { sizeof (RuntimeManager_t3497363304), -1, sizeof(RuntimeManager_t3497363304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4726[2] = 
{
	0,
	RuntimeManager_t3497363304_StaticFields::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4727 = { sizeof (AdChildDirectedTreatment_t4156566242)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4727[4] = 
{
	AdChildDirectedTreatment_t4156566242::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4728 = { sizeof (AdId_t1082999026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4729 = { sizeof (AdLocation_t1108752805), -1, sizeof(AdLocation_t1108752805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4729[19] = 
{
	AdLocation_t1108752805::get_offset_of_name_0(),
	AdLocation_t1108752805_StaticFields::get_offset_of_map_1(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Default_2(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Startup_3(),
	AdLocation_t1108752805_StaticFields::get_offset_of_HomeScreen_4(),
	AdLocation_t1108752805_StaticFields::get_offset_of_MainMenu_5(),
	AdLocation_t1108752805_StaticFields::get_offset_of_GameScreen_6(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Achievements_7(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Quests_8(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Pause_9(),
	AdLocation_t1108752805_StaticFields::get_offset_of_LevelStart_10(),
	AdLocation_t1108752805_StaticFields::get_offset_of_LevelComplete_11(),
	AdLocation_t1108752805_StaticFields::get_offset_of_TurnComplete_12(),
	AdLocation_t1108752805_StaticFields::get_offset_of_IAPStore_13(),
	AdLocation_t1108752805_StaticFields::get_offset_of_ItemStore_14(),
	AdLocation_t1108752805_StaticFields::get_offset_of_GameOver_15(),
	AdLocation_t1108752805_StaticFields::get_offset_of_LeaderBoard_16(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Settings_17(),
	AdLocation_t1108752805_StaticFields::get_offset_of_Quit_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4730 = { sizeof (AdLocationExtension_t1550154976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4731 = { sizeof (AdNetwork_t2557190136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4731[11] = 
{
	AdNetwork_t2557190136::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4732 = { sizeof (BannerAdNetwork_t4276929711)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4732[7] = 
{
	BannerAdNetwork_t4276929711::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4733 = { sizeof (InterstitialAdNetwork_t3048379503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4733[11] = 
{
	InterstitialAdNetwork_t3048379503::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4734 = { sizeof (RewardedAdNetwork_t72233497)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4734[11] = 
{
	RewardedAdNetwork_t72233497::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4735 = { sizeof (AdOrientation_t2144699178)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4735[4] = 
{
	AdOrientation_t2144699178::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4736 = { sizeof (AdPlacement_t3195089512), -1, sizeof(AdPlacement_t3195089512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4736[19] = 
{
	AdPlacement_t3195089512_StaticFields::get_offset_of_sCustomPlacements_19(),
	AdPlacement_t3195089512::get_offset_of_mName_20(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Default_21(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Startup_22(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_HomeScreen_23(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_MainMenu_24(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_GameScreen_25(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Achievements_26(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_LevelStart_27(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_LevelComplete_28(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_TurnComplete_29(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Quests_30(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Pause_31(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_IAPStore_32(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_ItemStore_33(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_GameOver_34(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Leaderboard_35(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Settings_36(),
	AdPlacement_t3195089512_StaticFields::get_offset_of_Quit_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4737 = { sizeof (AdSettings_t94672728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4737[14] = 
{
	AdSettings_t94672728::get_offset_of_mAutoLoadAdsMode_0(),
	AdSettings_t94672728::get_offset_of_mAdCheckingInterval_1(),
	AdSettings_t94672728::get_offset_of_mAdLoadingInterval_2(),
	AdSettings_t94672728::get_offset_of_mIosDefaultAdNetworks_3(),
	AdSettings_t94672728::get_offset_of_mAndroidDefaultAdNetworks_4(),
	AdSettings_t94672728::get_offset_of_mAdColony_5(),
	AdSettings_t94672728::get_offset_of_mAdMob_6(),
	AdSettings_t94672728::get_offset_of_mFBAudience_7(),
	AdSettings_t94672728::get_offset_of_mChartboost_8(),
	AdSettings_t94672728::get_offset_of_mHeyzap_9(),
	AdSettings_t94672728::get_offset_of_mIronSource_10(),
	AdSettings_t94672728::get_offset_of_mMoPub_11(),
	AdSettings_t94672728::get_offset_of_mTapjoy_12(),
	AdSettings_t94672728::get_offset_of_mUnityAds_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4738 = { sizeof (DefaultAdNetworks_t2678507152)+ sizeof (RuntimeObject), sizeof(DefaultAdNetworks_t2678507152 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4738[3] = 
{
	DefaultAdNetworks_t2678507152::get_offset_of_bannerAdNetwork_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DefaultAdNetworks_t2678507152::get_offset_of_interstitialAdNetwork_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DefaultAdNetworks_t2678507152::get_offset_of_rewardedAdNetwork_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4739 = { sizeof (AdType_t68004042)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4739[4] = 
{
	AdType_t68004042::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4740 = { sizeof (Advertising_t2540168094), -1, sizeof(Advertising_t2540168094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4740[32] = 
{
	Advertising_t2540168094_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	Advertising_t2540168094_StaticFields::get_offset_of_sAdColonyClient_5(),
	Advertising_t2540168094_StaticFields::get_offset_of_sAdMobClient_6(),
	Advertising_t2540168094_StaticFields::get_offset_of_sChartboostClient_7(),
	Advertising_t2540168094_StaticFields::get_offset_of_sAudienceNetworkClient_8(),
	Advertising_t2540168094_StaticFields::get_offset_of_sHeyzapClient_9(),
	Advertising_t2540168094_StaticFields::get_offset_of_sMoPubClient_10(),
	Advertising_t2540168094_StaticFields::get_offset_of_sIronSourceClient_11(),
	Advertising_t2540168094_StaticFields::get_offset_of_sTapjoyClient_12(),
	Advertising_t2540168094_StaticFields::get_offset_of_sUnityAdsClient_13(),
	Advertising_t2540168094_StaticFields::get_offset_of_sDefaultBannerAdClient_14(),
	Advertising_t2540168094_StaticFields::get_offset_of_sDefaultInterstitialAdClient_15(),
	Advertising_t2540168094_StaticFields::get_offset_of_sDefaultRewardedAdClient_16(),
	0,
	0,
	0,
	Advertising_t2540168094_StaticFields::get_offset_of_DEFAULT_TIMESTAMP_20(),
	Advertising_t2540168094_StaticFields::get_offset_of_autoLoadAdsCoroutine_21(),
	Advertising_t2540168094_StaticFields::get_offset_of_currentAutoLoadAdsMode_22(),
	Advertising_t2540168094_StaticFields::get_offset_of_lastDefaultInterstitialAdLoadTimestamp_23(),
	Advertising_t2540168094_StaticFields::get_offset_of_lastDefaultRewardedAdLoadTimestamp_24(),
	Advertising_t2540168094_StaticFields::get_offset_of_lastCustomInterstitialAdsLoadTimestamp_25(),
	Advertising_t2540168094_StaticFields::get_offset_of_lastCustomRewardedAdsLoadTimestamp_26(),
	Advertising_t2540168094_StaticFields::get_offset_of_isUpdatingAutoLoadMode_27(),
	Advertising_t2540168094_StaticFields::get_offset_of_activeBannerAds_28(),
	Advertising_t2540168094_StaticFields::get_offset_of_InterstitialAdCompleted_29(),
	Advertising_t2540168094_StaticFields::get_offset_of_RewardedAdSkipped_30(),
	Advertising_t2540168094_StaticFields::get_offset_of_RewardedAdCompleted_31(),
	Advertising_t2540168094_StaticFields::get_offset_of_AdsRemoved_32(),
	Advertising_t2540168094_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
	Advertising_t2540168094_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_34(),
	Advertising_t2540168094_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4741 = { sizeof (U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4741[6] = 
{
	U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893::get_offset_of_delay_0(),
	U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893::get_offset_of_U24locvar0_1(),
	U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893::get_offset_of_U24locvar1_2(),
	U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893::get_offset_of_U24current_3(),
	U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893::get_offset_of_U24disposing_4(),
	U3CCRAutoLoadDefaultAdsU3Ec__Iterator0_t2266990893::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4742 = { sizeof (U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4742[6] = 
{
	U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638::get_offset_of_delay_0(),
	U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638::get_offset_of_U3CavailableInterstitialNetworksU3E__0_1(),
	U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638::get_offset_of_U3CavailableRewardedNetworksU3E__0_2(),
	U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638::get_offset_of_U24current_3(),
	U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638::get_offset_of_U24disposing_4(),
	U3CCRAutoLoadAllAdsU3Ec__Iterator1_t1924352638::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4743 = { sizeof (AdvertisingConsentManager_t2195563647), -1, sizeof(AdvertisingConsentManager_t2195563647_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4743[2] = 
{
	0,
	AdvertisingConsentManager_t2195563647_StaticFields::get_offset_of_sInstance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4744 = { sizeof (AutoAdLoadingMode_t854791933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4744[4] = 
{
	AutoAdLoadingMode_t854791933::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4745 = { sizeof (BannerAdPosition_t186839070)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4745[7] = 
{
	BannerAdPosition_t186839070::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4746 = { sizeof (BannerAdSize_t3807836689), -1, sizeof(BannerAdSize_t3807836689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4746[8] = 
{
	BannerAdSize_t3807836689::get_offset_of_U3CIsSmartBannerU3Ek__BackingField_0(),
	BannerAdSize_t3807836689::get_offset_of_U3CWidthU3Ek__BackingField_1(),
	BannerAdSize_t3807836689::get_offset_of_U3CHeightU3Ek__BackingField_2(),
	BannerAdSize_t3807836689_StaticFields::get_offset_of_Banner_3(),
	BannerAdSize_t3807836689_StaticFields::get_offset_of_MediumRectangle_4(),
	BannerAdSize_t3807836689_StaticFields::get_offset_of_IABBanner_5(),
	BannerAdSize_t3807836689_StaticFields::get_offset_of_Leaderboard_6(),
	BannerAdSize_t3807836689_StaticFields::get_offset_of_SmartBanner_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4747 = { sizeof (AdClientImpl_t879350116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4747[5] = 
{
	AdClientImpl_t879350116::get_offset_of_mIsInitialized_0(),
	AdClientImpl_t879350116::get_offset_of_InterstitialAdCompleted_1(),
	AdClientImpl_t879350116::get_offset_of_RewardedAdSkipped_2(),
	AdClientImpl_t879350116::get_offset_of_RewardedAdCompleted_3(),
	AdClientImpl_t879350116::get_offset_of_DataPrivacyConsentUpdated_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4748 = { sizeof (AdColonyClientImpl_t352798180), -1, sizeof(AdColonyClientImpl_t352798180_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4748[4] = 
{
	0,
	0,
	AdColonyClientImpl_t352798180_StaticFields::get_offset_of_sInstance_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4749 = { sizeof (AdMobClientImpl_t3823742600), -1, sizeof(AdMobClientImpl_t3823742600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4749[3] = 
{
	0,
	AdMobClientImpl_t3823742600_StaticFields::get_offset_of_sInstance_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4750 = { sizeof (AudienceNetworkClientImpl_t266954160), -1, sizeof(AudienceNetworkClientImpl_t266954160_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4750[4] = 
{
	0,
	0,
	AudienceNetworkClientImpl_t266954160_StaticFields::get_offset_of_sInstance_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4751 = { sizeof (ChartboostClientImpl_t602894833), -1, sizeof(ChartboostClientImpl_t602894833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4751[4] = 
{
	0,
	0,
	ChartboostClientImpl_t602894833_StaticFields::get_offset_of_sInstance_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4752 = { sizeof (HeyzapClientImpl_t2848241831), -1, sizeof(HeyzapClientImpl_t2848241831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4752[3] = 
{
	0,
	HeyzapClientImpl_t2848241831_StaticFields::get_offset_of_sInstance_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4754 = { sizeof (IronSourceClientImpl_t41919987), -1, sizeof(IronSourceClientImpl_t41919987_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4754[3] = 
{
	0,
	IronSourceClientImpl_t41919987_StaticFields::get_offset_of_sInstance_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4755 = { sizeof (IronSourceAppStateHandler_t237138469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4756 = { sizeof (MoPubClientImpl_t784383640), -1, sizeof(MoPubClientImpl_t784383640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4756[3] = 
{
	0,
	MoPubClientImpl_t784383640_StaticFields::get_offset_of_sInstance_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4757 = { sizeof (NoOpClientImpl_t3976050009), -1, sizeof(NoOpClientImpl_t3976050009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4757[1] = 
{
	NoOpClientImpl_t3976050009_StaticFields::get_offset_of_sInstance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4758 = { sizeof (TapjoyClientImpl_t2003437302), -1, sizeof(TapjoyClientImpl_t2003437302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4758[4] = 
{
	0,
	0,
	TapjoyClientImpl_t2003437302_StaticFields::get_offset_of_sInstance_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4759 = { sizeof (UnityAdsClientImpl_t4105509132), -1, sizeof(UnityAdsClientImpl_t4105509132_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4759[7] = 
{
	0,
	0,
	UnityAdsClientImpl_t4105509132::get_offset_of_mAdSettings_7(),
	UnityAdsClientImpl_t4105509132::get_offset_of_InterstitialAdCallback_8(),
	UnityAdsClientImpl_t4105509132::get_offset_of_RewardedAdCallback_9(),
	UnityAdsClientImpl_t4105509132_StaticFields::get_offset_of_sInstance_10(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4760 = { sizeof (U3CInternalShowInterstitialAdU3Ec__AnonStorey0_t4024241046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4760[2] = 
{
	U3CInternalShowInterstitialAdU3Ec__AnonStorey0_t4024241046::get_offset_of_placement_0(),
	U3CInternalShowInterstitialAdU3Ec__AnonStorey0_t4024241046::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4761 = { sizeof (U3CInternalShowRewardedAdU3Ec__AnonStorey1_t1483428000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4761[2] = 
{
	U3CInternalShowRewardedAdU3Ec__AnonStorey1_t1483428000::get_offset_of_placement_0(),
	U3CInternalShowRewardedAdU3Ec__AnonStorey1_t1483428000::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4762 = { sizeof (AdColonySettings_t2994520974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4762[8] = 
{
	AdColonySettings_t2994520974::get_offset_of_mAppId_0(),
	AdColonySettings_t2994520974::get_offset_of_mOrientation_1(),
	AdColonySettings_t2994520974::get_offset_of_mEnableRewardedAdPrePopup_2(),
	AdColonySettings_t2994520974::get_offset_of_mEnableRewardedAdPostPopup_3(),
	AdColonySettings_t2994520974::get_offset_of_mDefaultInterstitialAdId_4(),
	AdColonySettings_t2994520974::get_offset_of_mDefaultRewardedAdId_5(),
	AdColonySettings_t2994520974::get_offset_of_mCustomInterstitialAdIds_6(),
	AdColonySettings_t2994520974::get_offset_of_mCustomRewardedAdIds_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4763 = { sizeof (AdMobSettings_t1707484206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4763[10] = 
{
	AdMobSettings_t1707484206::get_offset_of_mTargetingSettings_0(),
	AdMobSettings_t1707484206::get_offset_of_mEnableTestMode_1(),
	AdMobSettings_t1707484206::get_offset_of_mTestDeviceIds_2(),
	AdMobSettings_t1707484206::get_offset_of_mAppId_3(),
	AdMobSettings_t1707484206::get_offset_of_mDefaultBannerAdId_4(),
	AdMobSettings_t1707484206::get_offset_of_mDefaultInterstitialAdId_5(),
	AdMobSettings_t1707484206::get_offset_of_mDefaultRewardedAdId_6(),
	AdMobSettings_t1707484206::get_offset_of_mCustomBannerAdIds_7(),
	AdMobSettings_t1707484206::get_offset_of_mCustomInterstitialAdIds_8(),
	AdMobSettings_t1707484206::get_offset_of_mCustomRewardedAdIds_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4764 = { sizeof (AdMobTargetingSettings_t662407302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4764[2] = 
{
	AdMobTargetingSettings_t662407302::get_offset_of_mTagForChildDirectedTreatment_0(),
	AdMobTargetingSettings_t662407302::get_offset_of_mExtraOptions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4765 = { sizeof (AudienceNetworkSettings_t1217453211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4765[9] = 
{
	AudienceNetworkSettings_t1217453211::get_offset_of_mBannerAdSize_0(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mEnableTestMode_1(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mTestDevices_2(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mDefaultBannerId_3(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mDefaultInterstitialAdId_4(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mDefaultRewardedAdId_5(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mCustomBannerIds_6(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mCustomInterstitialAdIds_7(),
	AudienceNetworkSettings_t1217453211::get_offset_of_mCustomRewardedAdIds_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4766 = { sizeof (FBAudienceBannerAdSize_t1904814455)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4766[4] = 
{
	FBAudienceBannerAdSize_t1904814455::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4767 = { sizeof (ChartboostSettings_t2523652890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4767[2] = 
{
	ChartboostSettings_t2523652890::get_offset_of_mCustomInterstitialPlacements_0(),
	ChartboostSettings_t2523652890::get_offset_of_mCustomRewardedPlacements_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4768 = { sizeof (HeyzapSettings_t3928682395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4768[4] = 
{
	HeyzapSettings_t3928682395::get_offset_of_mPublisherId_0(),
	HeyzapSettings_t3928682395::get_offset_of_mShowTestSuite_1(),
	HeyzapSettings_t3928682395::get_offset_of_mCustomInterstitialPlacements_2(),
	HeyzapSettings_t3928682395::get_offset_of_mCustomRewardedPlacements_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4769 = { sizeof (IronSourceSettings_t2849155294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4769[3] = 
{
	IronSourceSettings_t2849155294::get_offset_of_mAppId_0(),
	IronSourceSettings_t2849155294::get_offset_of_mUseAdvancedSetting_1(),
	IronSourceSettings_t2849155294::get_offset_of_mSegments_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4770 = { sizeof (SegmentSettings_t159419534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4770[8] = 
{
	SegmentSettings_t159419534::get_offset_of_age_0(),
	SegmentSettings_t159419534::get_offset_of_gender_1(),
	SegmentSettings_t159419534::get_offset_of_level_2(),
	SegmentSettings_t159419534::get_offset_of_isPaying_3(),
	SegmentSettings_t159419534::get_offset_of_userCreationDate_4(),
	SegmentSettings_t159419534::get_offset_of_iapt_5(),
	SegmentSettings_t159419534::get_offset_of_segmentName_6(),
	SegmentSettings_t159419534::get_offset_of_customParams_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4771 = { sizeof (IronSourceBannerType_t1343419624)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4771[5] = 
{
	IronSourceBannerType_t1343419624::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4772 = { sizeof (MoPubSettings_t3452462547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4772[15] = 
{
	MoPubSettings_t3452462547::get_offset_of_mReportAppOpen_0(),
	MoPubSettings_t3452462547::get_offset_of_mITuneAppID_1(),
	MoPubSettings_t3452462547::get_offset_of_mEnableLocationPassing_2(),
	MoPubSettings_t3452462547::get_offset_of_mUseAdvancedSetting_3(),
	MoPubSettings_t3452462547::get_offset_of_mMediationSettings_4(),
	MoPubSettings_t3452462547::get_offset_of_mInitNetworks_5(),
	MoPubSettings_t3452462547::get_offset_of_mAdvancedBidders_6(),
	MoPubSettings_t3452462547::get_offset_of_mAutoRequestConsent_7(),
	MoPubSettings_t3452462547::get_offset_of_mForceGdprApplicable_8(),
	MoPubSettings_t3452462547::get_offset_of_mDefaultBannerId_9(),
	MoPubSettings_t3452462547::get_offset_of_mDefaultInterstitialAdId_10(),
	MoPubSettings_t3452462547::get_offset_of_mDefaultRewardedAdId_11(),
	MoPubSettings_t3452462547::get_offset_of_mCustomBannerIds_12(),
	MoPubSettings_t3452462547::get_offset_of_mCustomInterstitialAdIds_13(),
	MoPubSettings_t3452462547::get_offset_of_mCustomRewardedAdIds_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4773 = { sizeof (MoPubInitNetwork_t4276534974)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4773[8] = 
{
	MoPubInitNetwork_t4276534974::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4774 = { sizeof (MoPubAdvancedBidder_t3070790054)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4774[3] = 
{
	MoPubAdvancedBidder_t3070790054::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4775 = { sizeof (MediationSetting_t2014171693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4775[2] = 
{
	MediationSetting_t2014171693::get_offset_of_mAdVendor_0(),
	MediationSetting_t2014171693::get_offset_of_mExtraOptions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4776 = { sizeof (TapjoySettings_t205532585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4776[6] = 
{
	TapjoySettings_t205532585::get_offset_of_mAutoReconnect_0(),
	TapjoySettings_t205532585::get_offset_of_mAutoReconnectInterval_1(),
	TapjoySettings_t205532585::get_offset_of_mDefaultInterstitialAdId_2(),
	TapjoySettings_t205532585::get_offset_of_mDefaultRewardedAdId_3(),
	TapjoySettings_t205532585::get_offset_of_mCustomInterstitialAdIds_4(),
	TapjoySettings_t205532585::get_offset_of_mCustomRewardedAdIds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4777 = { sizeof (UnityAdsSettings_t2563580717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4777[6] = 
{
	0,
	0,
	UnityAdsSettings_t2563580717::get_offset_of_mDefaultInterstitialAdId_2(),
	UnityAdsSettings_t2563580717::get_offset_of_mDefaultRewardedAdId_3(),
	UnityAdsSettings_t2563580717::get_offset_of_mCustomInterstitialAdIds_4(),
	UnityAdsSettings_t2563580717::get_offset_of_mCustomRewardedAdIds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4778 = { sizeof (GameServices_t1437699199), -1, sizeof(GameServices_t1437699199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4778[7] = 
{
	GameServices_t1437699199_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	GameServices_t1437699199_StaticFields::get_offset_of_UserLoginSucceeded_5(),
	GameServices_t1437699199_StaticFields::get_offset_of_UserLoginFailed_6(),
	GameServices_t1437699199_StaticFields::get_offset_of_isLoadingScore_7(),
	GameServices_t1437699199_StaticFields::get_offset_of_loadScoreRequests_8(),
	GameServices_t1437699199_StaticFields::get_offset_of_sSavedGameClient_9(),
	GameServices_t1437699199_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4779 = { sizeof (LoadScoreRequest_t1342227622)+ sizeof (RuntimeObject), sizeof(LoadScoreRequest_t1342227622_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4779[9] = 
{
	LoadScoreRequest_t1342227622::get_offset_of_useLeaderboardDefault_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_loadLocalUserScore_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_leaderboardName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_leaderboardId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_fromRank_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_scoreCount_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_timeScope_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_userScope_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LoadScoreRequest_t1342227622::get_offset_of_callback_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4780 = { sizeof (U3CCRAutoInitU3Ec__Iterator0_t2576002651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4780[4] = 
{
	U3CCRAutoInitU3Ec__Iterator0_t2576002651::get_offset_of_delay_0(),
	U3CCRAutoInitU3Ec__Iterator0_t2576002651::get_offset_of_U24current_1(),
	U3CCRAutoInitU3Ec__Iterator0_t2576002651::get_offset_of_U24disposing_2(),
	U3CCRAutoInitU3Ec__Iterator0_t2576002651::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4781 = { sizeof (U3CLoadFriendsU3Ec__AnonStorey1_t240145850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4781[1] = 
{
	U3CLoadFriendsU3Ec__AnonStorey1_t240145850::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4782 = { sizeof (U3CLoadLocalUserScoreU3Ec__AnonStorey2_t2535740323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4782[1] = 
{
	U3CLoadLocalUserScoreU3Ec__AnonStorey2_t2535740323::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4783 = { sizeof (U3CDoReportScoreU3Ec__AnonStorey3_t3438848096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4783[1] = 
{
	U3CDoReportScoreU3Ec__AnonStorey3_t3438848096::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4784 = { sizeof (U3CDoReportAchievementProgressU3Ec__AnonStorey4_t2778844868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4784[1] = 
{
	U3CDoReportAchievementProgressU3Ec__AnonStorey4_t2778844868::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4785 = { sizeof (U3CDoNextLoadScoreRequestU3Ec__AnonStorey5_t2350042143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4785[2] = 
{
	U3CDoNextLoadScoreRequestU3Ec__AnonStorey5_t2350042143::get_offset_of_request_0(),
	U3CDoNextLoadScoreRequestU3Ec__AnonStorey5_t2350042143::get_offset_of_ldb_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4786 = { sizeof (GameServicesItem_t2394671562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4786[3] = 
{
	GameServicesItem_t2394671562::get_offset_of__name_0(),
	GameServicesItem_t2394671562::get_offset_of__iosId_1(),
	GameServicesItem_t2394671562::get_offset_of__androidId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4787 = { sizeof (Leaderboard_t4133458931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4788 = { sizeof (Achievement_t2708948008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4789 = { sizeof (GameServicesSettings_t1871988932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4789[11] = 
{
	GameServicesSettings_t1871988932::get_offset_of_mAutoInit_0(),
	GameServicesSettings_t1871988932::get_offset_of_mAutoInitDelay_1(),
	GameServicesSettings_t1871988932::get_offset_of_mAndroidMaxLoginRequests_2(),
	GameServicesSettings_t1871988932::get_offset_of_mGpgsDebugLogEnabled_3(),
	GameServicesSettings_t1871988932::get_offset_of_mGpgsPopupGravity_4(),
	GameServicesSettings_t1871988932::get_offset_of_mLeaderboards_5(),
	GameServicesSettings_t1871988932::get_offset_of_mAchievements_6(),
	GameServicesSettings_t1871988932::get_offset_of_mAndroidXmlResources_7(),
	GameServicesSettings_t1871988932::get_offset_of_mEnableSavedGames_8(),
	GameServicesSettings_t1871988932::get_offset_of_mAutoConflictResolutionStrategy_9(),
	GameServicesSettings_t1871988932::get_offset_of_mGpgsDataSource_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4790 = { sizeof (GpgsGravity_t3325317384)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4790[6] = 
{
	GpgsGravity_t3325317384::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4791 = { sizeof (GPGSSavedGameDataSource_t1302173665)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4791[3] = 
{
	GPGSSavedGameDataSource_t1302173665::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4792 = { sizeof (SavedGameConflictResolutionStrategy_t482061790)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4792[3] = 
{
	SavedGameConflictResolutionStrategy_t482061790::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4793 = { sizeof (SavedGameConflictResolver_t2056225930), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4794 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4795 = { sizeof (iOSSavedGameNative_t1863736551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4796 = { sizeof (OpenSavedGameCallback_t2349091444), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4797 = { sizeof (SaveGameDataCallback_t3612996042), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4798 = { sizeof (LoadSavedGameDataCallback_t1958165752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4799 = { sizeof (FetchSavedGamesCallback_t3515465237), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
