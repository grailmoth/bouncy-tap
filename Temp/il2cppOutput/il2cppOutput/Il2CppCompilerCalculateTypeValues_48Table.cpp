﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EasyMobile.AnimatedClip
struct AnimatedClip_t619307834;
// EasyMobile.ClipPlayer
struct ClipPlayer_t2965428560;
// EasyMobile.ClipPlayerUI
struct ClipPlayerUI_t1249565783;
// EasyMobile.FirebaseNotification
struct FirebaseNotification_t2232694309;
// EasyMobile.GifExportTask
struct GifExportTask_t1106658426;
// EasyMobile.Giphy/UploadSuccessResponse/UploadSuccessData
struct UploadSuccessData_t1863379328;
// EasyMobile.IAPProduct/StoreSpecificId[]
struct StoreSpecificIdU5BU5D_t3380943197;
// EasyMobile.IAPProduct[]
struct IAPProductU5BU5D_t2701488206;
// EasyMobile.InAppPurchasing/StoreListener
struct StoreListener_t4250367273;
// EasyMobile.Internal.GameServices.IIOSConflictResolver
struct IIOSConflictResolver_t144447553;
// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame
struct iOSGKSavedGame_t2037893409;
// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[]
struct iOSGKSavedGameU5BU5D_t3170496828;
// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/DeleteSavedGameCallback
struct DeleteSavedGameCallback_t2032035250;
// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/FetchSavedGamesCallback
struct FetchSavedGamesCallback_t3515465237;
// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/LoadSavedGameDataCallback
struct LoadSavedGameDataCallback_t1958165752;
// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/OpenSavedGameCallback
struct OpenSavedGameCallback_t2349091444;
// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/ResolveConflictingSavedGamesCallback
struct ResolveConflictingSavedGamesCallback_t4045204290;
// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/SaveGameDataCallback
struct SaveGameDataCallback_t3612996042;
// EasyMobile.Internal.GameServices.iOSConflictCallback
struct iOSConflictCallback_t756019127;
// EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver
struct NativeConflictResolver_t651818787;
// EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportCompletedDelegate
struct GifExportCompletedDelegate_t4082577070;
// EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportProgressDelegate
struct GifExportProgressDelegate_t324897231;
// EasyMobile.Internal.Notifications.NativeNotificationHandler
struct NativeNotificationHandler_t1892928298;
// EasyMobile.Internal.Notifications.iOS.iOSNotificationAction[]
struct iOSNotificationActionU5BU5D_t3075117727;
// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetNotificationResponseCallback
struct GetNotificationResponseCallback_t3346532913;
// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetPendingNotificationRequestsCallback
struct GetPendingNotificationRequestsCallback_t3407937767;
// EasyMobile.Internal.Notifications.iOSNotificationListener
struct iOSNotificationListener_t2070489240;
// EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0
struct U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955;
// EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1
struct U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746;
// EasyMobile.NotificationRequest
struct NotificationRequest_t4022128807;
// EasyMobile.SavedGame
struct SavedGame_t947814848;
// EasyMobile.SavedGameConflictResolver
struct SavedGameConflictResolver_t2056225930;
// Moments.Encoder.GifEncoder
struct GifEncoder_t60958606;
// Moments.Encoder.GifFrame
struct GifFrame_t2916230237;
// Moments.ReflectionUtils`1<EasyMobile.Recorder>
struct ReflectionUtils_1_t676350948;
// System.Action
struct Action_t1264377477;
// System.Action`1<EasyMobile.IAPProduct>
struct Action_1_t1977500546;
// System.Action`1<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[]>
struct Action_1_t3342964423;
// System.Action`1<EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest[]>
struct Action_1_t3976455385;
// System.Action`1<EasyMobile.Internal.Notifications.iOS.iOSNotificationResponse>
struct Action_1_t3697448605;
// System.Action`1<EasyMobile.LocalNotification>
struct Action_1_t516386935;
// System.Action`1<EasyMobile.NotificationRequest[]>
struct Action_1_t2750272105;
// System.Action`1<EasyMobile.RemoteNotification>
struct Action_1_t1859537056;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<UnityEngine.Purchasing.Product>
struct Action_1_t3416877654;
// System.Action`2<EasyMobile.AnimatedClip,System.Single>
struct Action_2_t1508656458;
// System.Action`2<EasyMobile.AnimatedClip,System.String>
struct Action_2_t1958840373;
// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String>
struct Action_2_t1563910866;
// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[],System.String>
struct Action_2_t2572077035;
// System.Action`2<EasyMobile.SavedGame,System.String>
struct Action_2_t3088183511;
// System.Action`2<EasyMobile.SavedGame[],System.String>
struct Action_2_t3708252210;
// System.Action`2<System.Byte[],System.Byte[]>
struct Action_2_t1120524178;
// System.Action`2<System.Byte[],System.String>
struct Action_2_t3146294506;
// System.Action`2<System.Int32,System.Single>
struct Action_2_t2623443791;
// System.Action`2<System.Int32,System.String>
struct Action_2_t3073627706;
// System.Action`3<EasyMobile.SavedGame,System.Byte[],System.String>
struct Action_3_t201929169;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,EasyMobile.GifExportTask>
struct Dictionary_2_t4290339053;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Runtime.InteropServices.GCHandle[]>
struct Dictionary_2_t3219349245;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t96558379;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.List`1<Moments.Encoder.GifFrame>
struct List_1_t93337683;
// System.Collections.Generic.Queue`1<UnityEngine.RenderTexture>
struct Queue_1_t1955146927;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Comparison`1<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame>
struct Comparison_1_t1812824588;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.DeleteSavedGameResponse>
struct Func_2_t2153995311;
// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.FetchSavedGamesResponse>
struct Func_2_t4170619710;
// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.LoadSavedGameDataResponse>
struct Func_2_t546531261;
// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.OpenSavedGameResponse>
struct Func_2_t1496992990;
// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.ResolveConflictResponse>
struct Func_2_t2259940743;
// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.SaveGameDataResponse>
struct Func_2_t128877021;
// System.Func`2<System.IntPtr,EasyMobile.Internal.Notifications.iOS.GetPendingNotificationRequestsResponse>
struct Func_2_t2389551456;
// System.Func`2<System.IntPtr,EasyMobile.Internal.Notifications.iOS.iOSNotificationResponse>
struct Func_2_t4061431951;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.FileStream
struct FileStream_t4292183065;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3365920845;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Threading.Thread
struct Thread_t2300836069;
// System.Uri
struct Uri_t100236324;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Color32[][]
struct Color32U5BU5DU5BU5D_t107669032;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1618671084;
// UnityEngine.Purchasing.IAmazonExtensions
struct IAmazonExtensions_t2856685217;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t4146644616;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t3180538779;
// UnityEngine.Purchasing.IGooglePlayStoreExtensions
struct IGooglePlayStoreExtensions_t1267379539;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t2712620151;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t2579314702;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;

struct iOSNotificationAction_t1537218362_marshaled_com;
struct iOSNotificationAction_t1537218362_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ANIMATEDCLIP_T619307834_H
#define ANIMATEDCLIP_T619307834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.AnimatedClip
struct  AnimatedClip_t619307834  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.AnimatedClip::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_0;
	// System.Int32 EasyMobile.AnimatedClip::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_1;
	// System.Int32 EasyMobile.AnimatedClip::<FramePerSecond>k__BackingField
	int32_t ___U3CFramePerSecondU3Ek__BackingField_2;
	// System.Single EasyMobile.AnimatedClip::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_3;
	// UnityEngine.RenderTexture[] EasyMobile.AnimatedClip::<Frames>k__BackingField
	RenderTextureU5BU5D_t4111643188* ___U3CFramesU3Ek__BackingField_4;
	// System.Boolean EasyMobile.AnimatedClip::isDisposed
	bool ___isDisposed_5;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnimatedClip_t619307834, ___U3CWidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_0() const { return ___U3CWidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_0() { return &___U3CWidthU3Ek__BackingField_0; }
	inline void set_U3CWidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnimatedClip_t619307834, ___U3CHeightU3Ek__BackingField_1)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_1() const { return ___U3CHeightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_1() { return &___U3CHeightU3Ek__BackingField_1; }
	inline void set_U3CHeightU3Ek__BackingField_1(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFramePerSecondU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnimatedClip_t619307834, ___U3CFramePerSecondU3Ek__BackingField_2)); }
	inline int32_t get_U3CFramePerSecondU3Ek__BackingField_2() const { return ___U3CFramePerSecondU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CFramePerSecondU3Ek__BackingField_2() { return &___U3CFramePerSecondU3Ek__BackingField_2; }
	inline void set_U3CFramePerSecondU3Ek__BackingField_2(int32_t value)
	{
		___U3CFramePerSecondU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AnimatedClip_t619307834, ___U3CLengthU3Ek__BackingField_3)); }
	inline float get_U3CLengthU3Ek__BackingField_3() const { return ___U3CLengthU3Ek__BackingField_3; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_3() { return &___U3CLengthU3Ek__BackingField_3; }
	inline void set_U3CLengthU3Ek__BackingField_3(float value)
	{
		___U3CLengthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CFramesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AnimatedClip_t619307834, ___U3CFramesU3Ek__BackingField_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_U3CFramesU3Ek__BackingField_4() const { return ___U3CFramesU3Ek__BackingField_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_U3CFramesU3Ek__BackingField_4() { return &___U3CFramesU3Ek__BackingField_4; }
	inline void set_U3CFramesU3Ek__BackingField_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___U3CFramesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFramesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_isDisposed_5() { return static_cast<int32_t>(offsetof(AnimatedClip_t619307834, ___isDisposed_5)); }
	inline bool get_isDisposed_5() const { return ___isDisposed_5; }
	inline bool* get_address_of_isDisposed_5() { return &___isDisposed_5; }
	inline void set_isDisposed_5(bool value)
	{
		___isDisposed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDCLIP_T619307834_H
#ifndef U3CCRPLAYU3EC__ITERATOR0_T643632238_H
#define U3CCRPLAYU3EC__ITERATOR0_T643632238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ClipPlayer/<CRPlay>c__Iterator0
struct  U3CCRPlayU3Ec__Iterator0_t643632238  : public RuntimeObject
{
public:
	// EasyMobile.AnimatedClip EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::clip
	AnimatedClip_t619307834 * ___clip_0;
	// System.Single EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::<timePerFrame>__0
	float ___U3CtimePerFrameU3E__0_1;
	// System.Boolean EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::<hasDelayed>__0
	bool ___U3ChasDelayedU3E__0_2;
	// System.Int32 EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_3;
	// System.Single EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::startDelay
	float ___startDelay_4;
	// System.Boolean EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::loop
	bool ___loop_5;
	// EasyMobile.ClipPlayer EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::$this
	ClipPlayer_t2965428560 * ___U24this_6;
	// System.Object EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 EasyMobile.ClipPlayer/<CRPlay>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___clip_0)); }
	inline AnimatedClip_t619307834 * get_clip_0() const { return ___clip_0; }
	inline AnimatedClip_t619307834 ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AnimatedClip_t619307834 * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier((&___clip_0), value);
	}

	inline static int32_t get_offset_of_U3CtimePerFrameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U3CtimePerFrameU3E__0_1)); }
	inline float get_U3CtimePerFrameU3E__0_1() const { return ___U3CtimePerFrameU3E__0_1; }
	inline float* get_address_of_U3CtimePerFrameU3E__0_1() { return &___U3CtimePerFrameU3E__0_1; }
	inline void set_U3CtimePerFrameU3E__0_1(float value)
	{
		___U3CtimePerFrameU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3ChasDelayedU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U3ChasDelayedU3E__0_2)); }
	inline bool get_U3ChasDelayedU3E__0_2() const { return ___U3ChasDelayedU3E__0_2; }
	inline bool* get_address_of_U3ChasDelayedU3E__0_2() { return &___U3ChasDelayedU3E__0_2; }
	inline void set_U3ChasDelayedU3E__0_2(bool value)
	{
		___U3ChasDelayedU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U3CiU3E__1_3)); }
	inline int32_t get_U3CiU3E__1_3() const { return ___U3CiU3E__1_3; }
	inline int32_t* get_address_of_U3CiU3E__1_3() { return &___U3CiU3E__1_3; }
	inline void set_U3CiU3E__1_3(int32_t value)
	{
		___U3CiU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_startDelay_4() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___startDelay_4)); }
	inline float get_startDelay_4() const { return ___startDelay_4; }
	inline float* get_address_of_startDelay_4() { return &___startDelay_4; }
	inline void set_startDelay_4(float value)
	{
		___startDelay_4 = value;
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U24this_6)); }
	inline ClipPlayer_t2965428560 * get_U24this_6() const { return ___U24this_6; }
	inline ClipPlayer_t2965428560 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ClipPlayer_t2965428560 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t643632238, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRPLAYU3EC__ITERATOR0_T643632238_H
#ifndef U3CCRPLAYU3EC__ITERATOR0_T1429219391_H
#define U3CCRPLAYU3EC__ITERATOR0_T1429219391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0
struct  U3CCRPlayU3Ec__Iterator0_t1429219391  : public RuntimeObject
{
public:
	// EasyMobile.AnimatedClip EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::clip
	AnimatedClip_t619307834 * ___clip_0;
	// System.Single EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::<timePerFrame>__0
	float ___U3CtimePerFrameU3E__0_1;
	// System.Boolean EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::<hasDelayed>__0
	bool ___U3ChasDelayedU3E__0_2;
	// System.Int32 EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_3;
	// System.Single EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::startDelay
	float ___startDelay_4;
	// System.Boolean EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::loop
	bool ___loop_5;
	// EasyMobile.ClipPlayerUI EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::$this
	ClipPlayerUI_t1249565783 * ___U24this_6;
	// System.Object EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 EasyMobile.ClipPlayerUI/<CRPlay>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___clip_0)); }
	inline AnimatedClip_t619307834 * get_clip_0() const { return ___clip_0; }
	inline AnimatedClip_t619307834 ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AnimatedClip_t619307834 * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier((&___clip_0), value);
	}

	inline static int32_t get_offset_of_U3CtimePerFrameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U3CtimePerFrameU3E__0_1)); }
	inline float get_U3CtimePerFrameU3E__0_1() const { return ___U3CtimePerFrameU3E__0_1; }
	inline float* get_address_of_U3CtimePerFrameU3E__0_1() { return &___U3CtimePerFrameU3E__0_1; }
	inline void set_U3CtimePerFrameU3E__0_1(float value)
	{
		___U3CtimePerFrameU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3ChasDelayedU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U3ChasDelayedU3E__0_2)); }
	inline bool get_U3ChasDelayedU3E__0_2() const { return ___U3ChasDelayedU3E__0_2; }
	inline bool* get_address_of_U3ChasDelayedU3E__0_2() { return &___U3ChasDelayedU3E__0_2; }
	inline void set_U3ChasDelayedU3E__0_2(bool value)
	{
		___U3ChasDelayedU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_3() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U3CiU3E__1_3)); }
	inline int32_t get_U3CiU3E__1_3() const { return ___U3CiU3E__1_3; }
	inline int32_t* get_address_of_U3CiU3E__1_3() { return &___U3CiU3E__1_3; }
	inline void set_U3CiU3E__1_3(int32_t value)
	{
		___U3CiU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_startDelay_4() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___startDelay_4)); }
	inline float get_startDelay_4() const { return ___startDelay_4; }
	inline float* get_address_of_startDelay_4() { return &___startDelay_4; }
	inline void set_startDelay_4(float value)
	{
		___startDelay_4 = value;
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U24this_6)); }
	inline ClipPlayerUI_t1249565783 * get_U24this_6() const { return ___U24this_6; }
	inline ClipPlayerUI_t1249565783 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ClipPlayerUI_t1249565783 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CCRPlayU3Ec__Iterator0_t1429219391, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRPLAYU3EC__ITERATOR0_T1429219391_H
#ifndef FIREBASENOTIFICATION_T2232694309_H
#define FIREBASENOTIFICATION_T2232694309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.FirebaseNotification
struct  FirebaseNotification_t2232694309  : public RuntimeObject
{
public:
	// System.String EasyMobile.FirebaseNotification::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_0;
	// System.String EasyMobile.FirebaseNotification::<Body>k__BackingField
	String_t* ___U3CBodyU3Ek__BackingField_1;
	// System.String EasyMobile.FirebaseNotification::<Icon>k__BackingField
	String_t* ___U3CIconU3Ek__BackingField_2;
	// System.String EasyMobile.FirebaseNotification::<Sound>k__BackingField
	String_t* ___U3CSoundU3Ek__BackingField_3;
	// System.String EasyMobile.FirebaseNotification::<Badge>k__BackingField
	String_t* ___U3CBadgeU3Ek__BackingField_4;
	// System.String EasyMobile.FirebaseNotification::<Tag>k__BackingField
	String_t* ___U3CTagU3Ek__BackingField_5;
	// System.String EasyMobile.FirebaseNotification::<Color>k__BackingField
	String_t* ___U3CColorU3Ek__BackingField_6;
	// System.String EasyMobile.FirebaseNotification::<ClickAction>k__BackingField
	String_t* ___U3CClickActionU3Ek__BackingField_7;
	// System.String EasyMobile.FirebaseNotification::<BodyLocalizationKey>k__BackingField
	String_t* ___U3CBodyLocalizationKeyU3Ek__BackingField_8;
	// System.Collections.Generic.IEnumerable`1<System.String> EasyMobile.FirebaseNotification::<BodyLocalizationArgs>k__BackingField
	RuntimeObject* ___U3CBodyLocalizationArgsU3Ek__BackingField_9;
	// System.String EasyMobile.FirebaseNotification::<TitleLocalizationKey>k__BackingField
	String_t* ___U3CTitleLocalizationKeyU3Ek__BackingField_10;
	// System.Collections.Generic.IEnumerable`1<System.String> EasyMobile.FirebaseNotification::<TitleLocalizationArgs>k__BackingField
	RuntimeObject* ___U3CTitleLocalizationArgsU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CTitleU3Ek__BackingField_0)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_0() const { return ___U3CTitleU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_0() { return &___U3CTitleU3Ek__BackingField_0; }
	inline void set_U3CTitleU3Ek__BackingField_0(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CBodyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CBodyU3Ek__BackingField_1)); }
	inline String_t* get_U3CBodyU3Ek__BackingField_1() const { return ___U3CBodyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CBodyU3Ek__BackingField_1() { return &___U3CBodyU3Ek__BackingField_1; }
	inline void set_U3CBodyU3Ek__BackingField_1(String_t* value)
	{
		___U3CBodyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBodyU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIconU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CIconU3Ek__BackingField_2)); }
	inline String_t* get_U3CIconU3Ek__BackingField_2() const { return ___U3CIconU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CIconU3Ek__BackingField_2() { return &___U3CIconU3Ek__BackingField_2; }
	inline void set_U3CIconU3Ek__BackingField_2(String_t* value)
	{
		___U3CIconU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIconU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CSoundU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CSoundU3Ek__BackingField_3)); }
	inline String_t* get_U3CSoundU3Ek__BackingField_3() const { return ___U3CSoundU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CSoundU3Ek__BackingField_3() { return &___U3CSoundU3Ek__BackingField_3; }
	inline void set_U3CSoundU3Ek__BackingField_3(String_t* value)
	{
		___U3CSoundU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSoundU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CBadgeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CBadgeU3Ek__BackingField_4)); }
	inline String_t* get_U3CBadgeU3Ek__BackingField_4() const { return ___U3CBadgeU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CBadgeU3Ek__BackingField_4() { return &___U3CBadgeU3Ek__BackingField_4; }
	inline void set_U3CBadgeU3Ek__BackingField_4(String_t* value)
	{
		___U3CBadgeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBadgeU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CTagU3Ek__BackingField_5)); }
	inline String_t* get_U3CTagU3Ek__BackingField_5() const { return ___U3CTagU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTagU3Ek__BackingField_5() { return &___U3CTagU3Ek__BackingField_5; }
	inline void set_U3CTagU3Ek__BackingField_5(String_t* value)
	{
		___U3CTagU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CColorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CColorU3Ek__BackingField_6)); }
	inline String_t* get_U3CColorU3Ek__BackingField_6() const { return ___U3CColorU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CColorU3Ek__BackingField_6() { return &___U3CColorU3Ek__BackingField_6; }
	inline void set_U3CColorU3Ek__BackingField_6(String_t* value)
	{
		___U3CColorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColorU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CClickActionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CClickActionU3Ek__BackingField_7)); }
	inline String_t* get_U3CClickActionU3Ek__BackingField_7() const { return ___U3CClickActionU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CClickActionU3Ek__BackingField_7() { return &___U3CClickActionU3Ek__BackingField_7; }
	inline void set_U3CClickActionU3Ek__BackingField_7(String_t* value)
	{
		___U3CClickActionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClickActionU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CBodyLocalizationKeyU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CBodyLocalizationKeyU3Ek__BackingField_8)); }
	inline String_t* get_U3CBodyLocalizationKeyU3Ek__BackingField_8() const { return ___U3CBodyLocalizationKeyU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CBodyLocalizationKeyU3Ek__BackingField_8() { return &___U3CBodyLocalizationKeyU3Ek__BackingField_8; }
	inline void set_U3CBodyLocalizationKeyU3Ek__BackingField_8(String_t* value)
	{
		___U3CBodyLocalizationKeyU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBodyLocalizationKeyU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CBodyLocalizationArgsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CBodyLocalizationArgsU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CBodyLocalizationArgsU3Ek__BackingField_9() const { return ___U3CBodyLocalizationArgsU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CBodyLocalizationArgsU3Ek__BackingField_9() { return &___U3CBodyLocalizationArgsU3Ek__BackingField_9; }
	inline void set_U3CBodyLocalizationArgsU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CBodyLocalizationArgsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBodyLocalizationArgsU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CTitleLocalizationKeyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CTitleLocalizationKeyU3Ek__BackingField_10)); }
	inline String_t* get_U3CTitleLocalizationKeyU3Ek__BackingField_10() const { return ___U3CTitleLocalizationKeyU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CTitleLocalizationKeyU3Ek__BackingField_10() { return &___U3CTitleLocalizationKeyU3Ek__BackingField_10; }
	inline void set_U3CTitleLocalizationKeyU3Ek__BackingField_10(String_t* value)
	{
		___U3CTitleLocalizationKeyU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleLocalizationKeyU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTitleLocalizationArgsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FirebaseNotification_t2232694309, ___U3CTitleLocalizationArgsU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CTitleLocalizationArgsU3Ek__BackingField_11() const { return ___U3CTitleLocalizationArgsU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CTitleLocalizationArgsU3Ek__BackingField_11() { return &___U3CTitleLocalizationArgsU3Ek__BackingField_11; }
	inline void set_U3CTitleLocalizationArgsU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CTitleLocalizationArgsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleLocalizationArgsU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASENOTIFICATION_T2232694309_H
#ifndef U3CCRUPLOADU3EC__ITERATOR0_T1799629917_H
#define U3CCRUPLOADU3EC__ITERATOR0_T1799629917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Giphy/<CRUpload>c__Iterator0
struct  U3CCRUploadU3Ec__Iterator0_t1799629917  : public RuntimeObject
{
public:
	// System.String EasyMobile.Giphy/<CRUpload>c__Iterator0::uploadPath
	String_t* ___uploadPath_0;
	// UnityEngine.WWWForm EasyMobile.Giphy/<CRUpload>c__Iterator0::form
	WWWForm_t4064702195 * ___form_1;
	// UnityEngine.WWW EasyMobile.Giphy/<CRUpload>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_2;
	// System.Action`1<System.Single> EasyMobile.Giphy/<CRUpload>c__Iterator0::uploadProgressCB
	Action_1_t1569734369 * ___uploadProgressCB_3;
	// System.Action`1<System.String> EasyMobile.Giphy/<CRUpload>c__Iterator0::uploadCompletedCB
	Action_1_t2019918284 * ___uploadCompletedCB_4;
	// System.Action`1<System.String> EasyMobile.Giphy/<CRUpload>c__Iterator0::uploadFailedCB
	Action_1_t2019918284 * ___uploadFailedCB_5;
	// System.Object EasyMobile.Giphy/<CRUpload>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean EasyMobile.Giphy/<CRUpload>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 EasyMobile.Giphy/<CRUpload>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_uploadPath_0() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___uploadPath_0)); }
	inline String_t* get_uploadPath_0() const { return ___uploadPath_0; }
	inline String_t** get_address_of_uploadPath_0() { return &___uploadPath_0; }
	inline void set_uploadPath_0(String_t* value)
	{
		___uploadPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___uploadPath_0), value);
	}

	inline static int32_t get_offset_of_form_1() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___form_1)); }
	inline WWWForm_t4064702195 * get_form_1() const { return ___form_1; }
	inline WWWForm_t4064702195 ** get_address_of_form_1() { return &___form_1; }
	inline void set_form_1(WWWForm_t4064702195 * value)
	{
		___form_1 = value;
		Il2CppCodeGenWriteBarrier((&___form_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___U3CwwwU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_uploadProgressCB_3() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___uploadProgressCB_3)); }
	inline Action_1_t1569734369 * get_uploadProgressCB_3() const { return ___uploadProgressCB_3; }
	inline Action_1_t1569734369 ** get_address_of_uploadProgressCB_3() { return &___uploadProgressCB_3; }
	inline void set_uploadProgressCB_3(Action_1_t1569734369 * value)
	{
		___uploadProgressCB_3 = value;
		Il2CppCodeGenWriteBarrier((&___uploadProgressCB_3), value);
	}

	inline static int32_t get_offset_of_uploadCompletedCB_4() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___uploadCompletedCB_4)); }
	inline Action_1_t2019918284 * get_uploadCompletedCB_4() const { return ___uploadCompletedCB_4; }
	inline Action_1_t2019918284 ** get_address_of_uploadCompletedCB_4() { return &___uploadCompletedCB_4; }
	inline void set_uploadCompletedCB_4(Action_1_t2019918284 * value)
	{
		___uploadCompletedCB_4 = value;
		Il2CppCodeGenWriteBarrier((&___uploadCompletedCB_4), value);
	}

	inline static int32_t get_offset_of_uploadFailedCB_5() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___uploadFailedCB_5)); }
	inline Action_1_t2019918284 * get_uploadFailedCB_5() const { return ___uploadFailedCB_5; }
	inline Action_1_t2019918284 ** get_address_of_uploadFailedCB_5() { return &___uploadFailedCB_5; }
	inline void set_uploadFailedCB_5(Action_1_t2019918284 * value)
	{
		___uploadFailedCB_5 = value;
		Il2CppCodeGenWriteBarrier((&___uploadFailedCB_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CCRUploadU3Ec__Iterator0_t1799629917, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRUPLOADU3EC__ITERATOR0_T1799629917_H
#ifndef UPLOADSUCCESSRESPONSE_T411033403_H
#define UPLOADSUCCESSRESPONSE_T411033403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Giphy/UploadSuccessResponse
struct  UploadSuccessResponse_t411033403  : public RuntimeObject
{
public:
	// EasyMobile.Giphy/UploadSuccessResponse/UploadSuccessData EasyMobile.Giphy/UploadSuccessResponse::data
	UploadSuccessData_t1863379328 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(UploadSuccessResponse_t411033403, ___data_0)); }
	inline UploadSuccessData_t1863379328 * get_data_0() const { return ___data_0; }
	inline UploadSuccessData_t1863379328 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(UploadSuccessData_t1863379328 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSUCCESSRESPONSE_T411033403_H
#ifndef UPLOADSUCCESSDATA_T1863379328_H
#define UPLOADSUCCESSDATA_T1863379328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Giphy/UploadSuccessResponse/UploadSuccessData
struct  UploadSuccessData_t1863379328  : public RuntimeObject
{
public:
	// System.String EasyMobile.Giphy/UploadSuccessResponse/UploadSuccessData::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UploadSuccessData_t1863379328, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSUCCESSDATA_T1863379328_H
#ifndef U3CREFRESHAPPLEAPPRECEIPTU3EC__ANONSTOREY0_T4269051416_H
#define U3CREFRESHAPPLEAPPRECEIPTU3EC__ANONSTOREY0_T4269051416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InAppPurchasing/<RefreshAppleAppReceipt>c__AnonStorey0
struct  U3CRefreshAppleAppReceiptU3Ec__AnonStorey0_t4269051416  : public RuntimeObject
{
public:
	// System.Action`1<System.String> EasyMobile.InAppPurchasing/<RefreshAppleAppReceipt>c__AnonStorey0::successCallback
	Action_1_t2019918284 * ___successCallback_0;
	// System.Action EasyMobile.InAppPurchasing/<RefreshAppleAppReceipt>c__AnonStorey0::errorCallback
	Action_t1264377477 * ___errorCallback_1;

public:
	inline static int32_t get_offset_of_successCallback_0() { return static_cast<int32_t>(offsetof(U3CRefreshAppleAppReceiptU3Ec__AnonStorey0_t4269051416, ___successCallback_0)); }
	inline Action_1_t2019918284 * get_successCallback_0() const { return ___successCallback_0; }
	inline Action_1_t2019918284 ** get_address_of_successCallback_0() { return &___successCallback_0; }
	inline void set_successCallback_0(Action_1_t2019918284 * value)
	{
		___successCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___successCallback_0), value);
	}

	inline static int32_t get_offset_of_errorCallback_1() { return static_cast<int32_t>(offsetof(U3CRefreshAppleAppReceiptU3Ec__AnonStorey0_t4269051416, ___errorCallback_1)); }
	inline Action_t1264377477 * get_errorCallback_1() const { return ___errorCallback_1; }
	inline Action_t1264377477 ** get_address_of_errorCallback_1() { return &___errorCallback_1; }
	inline void set_errorCallback_1(Action_t1264377477 * value)
	{
		___errorCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREFRESHAPPLEAPPRECEIPTU3EC__ANONSTOREY0_T4269051416_H
#ifndef STORELISTENER_T4250367273_H
#define STORELISTENER_T4250367273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InAppPurchasing/StoreListener
struct  StoreListener_t4250367273  : public RuntimeObject
{
public:

public:
};

struct StoreListener_t4250367273_StaticFields
{
public:
	// System.Action`1<UnityEngine.Purchasing.Product> EasyMobile.InAppPurchasing/StoreListener::<>f__mg$cache0
	Action_1_t3416877654 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(StoreListener_t4250367273_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline Action_1_t3416877654 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline Action_1_t3416877654 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(Action_1_t3416877654 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELISTENER_T4250367273_H
#ifndef UNSUPPORTEDSAVEDGAMECLIENT_T1182319790_H
#define UNSUPPORTEDSAVEDGAMECLIENT_T1182319790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.UnsupportedSavedGameClient
struct  UnsupportedSavedGameClient_t1182319790  : public RuntimeObject
{
public:
	// System.String EasyMobile.Internal.GameServices.UnsupportedSavedGameClient::mMessage
	String_t* ___mMessage_1;

public:
	inline static int32_t get_offset_of_mMessage_1() { return static_cast<int32_t>(offsetof(UnsupportedSavedGameClient_t1182319790, ___mMessage_1)); }
	inline String_t* get_mMessage_1() const { return ___mMessage_1; }
	inline String_t** get_address_of_mMessage_1() { return &___mMessage_1; }
	inline void set_mMessage_1(String_t* value)
	{
		___mMessage_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMessage_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDSAVEDGAMECLIENT_T1182319790_H
#ifndef IOSSAVEDGAMECLIENT_T251267421_H
#define IOSSAVEDGAMECLIENT_T251267421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient
struct  iOSSavedGameClient_t251267421  : public RuntimeObject
{
public:

public:
};

struct iOSSavedGameClient_t251267421_StaticFields
{
public:
	// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.OpenSavedGameResponse> EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache0
	Func_2_t1496992990 * ___U3CU3Ef__mgU24cache0_0;
	// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/OpenSavedGameCallback EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache1
	OpenSavedGameCallback_t2349091444 * ___U3CU3Ef__mgU24cache1_1;
	// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.SaveGameDataResponse> EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache2
	Func_2_t128877021 * ___U3CU3Ef__mgU24cache2_2;
	// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/SaveGameDataCallback EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache3
	SaveGameDataCallback_t3612996042 * ___U3CU3Ef__mgU24cache3_3;
	// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.LoadSavedGameDataResponse> EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache4
	Func_2_t546531261 * ___U3CU3Ef__mgU24cache4_4;
	// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/LoadSavedGameDataCallback EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache5
	LoadSavedGameDataCallback_t1958165752 * ___U3CU3Ef__mgU24cache5_5;
	// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.FetchSavedGamesResponse> EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache6
	Func_2_t4170619710 * ___U3CU3Ef__mgU24cache6_6;
	// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/FetchSavedGamesCallback EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache7
	FetchSavedGamesCallback_t3515465237 * ___U3CU3Ef__mgU24cache7_7;
	// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.ResolveConflictResponse> EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache8
	Func_2_t2259940743 * ___U3CU3Ef__mgU24cache8_8;
	// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/ResolveConflictingSavedGamesCallback EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cache9
	ResolveConflictingSavedGamesCallback_t4045204290 * ___U3CU3Ef__mgU24cache9_9;
	// System.Func`2<System.IntPtr,EasyMobile.Internal.GameServices.iOS.DeleteSavedGameResponse> EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cacheA
	Func_2_t2153995311 * ___U3CU3Ef__mgU24cacheA_10;
	// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/DeleteSavedGameCallback EasyMobile.Internal.GameServices.iOSSavedGameClient::<>f__mg$cacheB
	DeleteSavedGameCallback_t2032035250 * ___U3CU3Ef__mgU24cacheB_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline Func_2_t1496992990 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline Func_2_t1496992990 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(Func_2_t1496992990 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_1() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache1_1)); }
	inline OpenSavedGameCallback_t2349091444 * get_U3CU3Ef__mgU24cache1_1() const { return ___U3CU3Ef__mgU24cache1_1; }
	inline OpenSavedGameCallback_t2349091444 ** get_address_of_U3CU3Ef__mgU24cache1_1() { return &___U3CU3Ef__mgU24cache1_1; }
	inline void set_U3CU3Ef__mgU24cache1_1(OpenSavedGameCallback_t2349091444 * value)
	{
		___U3CU3Ef__mgU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_2() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache2_2)); }
	inline Func_2_t128877021 * get_U3CU3Ef__mgU24cache2_2() const { return ___U3CU3Ef__mgU24cache2_2; }
	inline Func_2_t128877021 ** get_address_of_U3CU3Ef__mgU24cache2_2() { return &___U3CU3Ef__mgU24cache2_2; }
	inline void set_U3CU3Ef__mgU24cache2_2(Func_2_t128877021 * value)
	{
		___U3CU3Ef__mgU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_3() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache3_3)); }
	inline SaveGameDataCallback_t3612996042 * get_U3CU3Ef__mgU24cache3_3() const { return ___U3CU3Ef__mgU24cache3_3; }
	inline SaveGameDataCallback_t3612996042 ** get_address_of_U3CU3Ef__mgU24cache3_3() { return &___U3CU3Ef__mgU24cache3_3; }
	inline void set_U3CU3Ef__mgU24cache3_3(SaveGameDataCallback_t3612996042 * value)
	{
		___U3CU3Ef__mgU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_4() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache4_4)); }
	inline Func_2_t546531261 * get_U3CU3Ef__mgU24cache4_4() const { return ___U3CU3Ef__mgU24cache4_4; }
	inline Func_2_t546531261 ** get_address_of_U3CU3Ef__mgU24cache4_4() { return &___U3CU3Ef__mgU24cache4_4; }
	inline void set_U3CU3Ef__mgU24cache4_4(Func_2_t546531261 * value)
	{
		___U3CU3Ef__mgU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_5() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache5_5)); }
	inline LoadSavedGameDataCallback_t1958165752 * get_U3CU3Ef__mgU24cache5_5() const { return ___U3CU3Ef__mgU24cache5_5; }
	inline LoadSavedGameDataCallback_t1958165752 ** get_address_of_U3CU3Ef__mgU24cache5_5() { return &___U3CU3Ef__mgU24cache5_5; }
	inline void set_U3CU3Ef__mgU24cache5_5(LoadSavedGameDataCallback_t1958165752 * value)
	{
		___U3CU3Ef__mgU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_6() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache6_6)); }
	inline Func_2_t4170619710 * get_U3CU3Ef__mgU24cache6_6() const { return ___U3CU3Ef__mgU24cache6_6; }
	inline Func_2_t4170619710 ** get_address_of_U3CU3Ef__mgU24cache6_6() { return &___U3CU3Ef__mgU24cache6_6; }
	inline void set_U3CU3Ef__mgU24cache6_6(Func_2_t4170619710 * value)
	{
		___U3CU3Ef__mgU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_7() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache7_7)); }
	inline FetchSavedGamesCallback_t3515465237 * get_U3CU3Ef__mgU24cache7_7() const { return ___U3CU3Ef__mgU24cache7_7; }
	inline FetchSavedGamesCallback_t3515465237 ** get_address_of_U3CU3Ef__mgU24cache7_7() { return &___U3CU3Ef__mgU24cache7_7; }
	inline void set_U3CU3Ef__mgU24cache7_7(FetchSavedGamesCallback_t3515465237 * value)
	{
		___U3CU3Ef__mgU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_8() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache8_8)); }
	inline Func_2_t2259940743 * get_U3CU3Ef__mgU24cache8_8() const { return ___U3CU3Ef__mgU24cache8_8; }
	inline Func_2_t2259940743 ** get_address_of_U3CU3Ef__mgU24cache8_8() { return &___U3CU3Ef__mgU24cache8_8; }
	inline void set_U3CU3Ef__mgU24cache8_8(Func_2_t2259940743 * value)
	{
		___U3CU3Ef__mgU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_9() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cache9_9)); }
	inline ResolveConflictingSavedGamesCallback_t4045204290 * get_U3CU3Ef__mgU24cache9_9() const { return ___U3CU3Ef__mgU24cache9_9; }
	inline ResolveConflictingSavedGamesCallback_t4045204290 ** get_address_of_U3CU3Ef__mgU24cache9_9() { return &___U3CU3Ef__mgU24cache9_9; }
	inline void set_U3CU3Ef__mgU24cache9_9(ResolveConflictingSavedGamesCallback_t4045204290 * value)
	{
		___U3CU3Ef__mgU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_10() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cacheA_10)); }
	inline Func_2_t2153995311 * get_U3CU3Ef__mgU24cacheA_10() const { return ___U3CU3Ef__mgU24cacheA_10; }
	inline Func_2_t2153995311 ** get_address_of_U3CU3Ef__mgU24cacheA_10() { return &___U3CU3Ef__mgU24cacheA_10; }
	inline void set_U3CU3Ef__mgU24cacheA_10(Func_2_t2153995311 * value)
	{
		___U3CU3Ef__mgU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_11() { return static_cast<int32_t>(offsetof(iOSSavedGameClient_t251267421_StaticFields, ___U3CU3Ef__mgU24cacheB_11)); }
	inline DeleteSavedGameCallback_t2032035250 * get_U3CU3Ef__mgU24cacheB_11() const { return ___U3CU3Ef__mgU24cacheB_11; }
	inline DeleteSavedGameCallback_t2032035250 ** get_address_of_U3CU3Ef__mgU24cacheB_11() { return &___U3CU3Ef__mgU24cacheB_11; }
	inline void set_U3CU3Ef__mgU24cacheB_11(DeleteSavedGameCallback_t2032035250 * value)
	{
		___U3CU3Ef__mgU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSAVEDGAMECLIENT_T251267421_H
#ifndef U3CCONFLICTRESOLVINGLOOPU3EC__ANONSTOREY9_T1617656396_H
#define U3CCONFLICTRESOLVINGLOOPU3EC__ANONSTOREY9_T1617656396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9
struct  U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9::prefetchData
	bool ___prefetchData_0;
	// EasyMobile.Internal.GameServices.iOSConflictCallback EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9::conflictCallback
	iOSConflictCallback_t756019127 * ___conflictCallback_1;
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9::completedCallback
	Action_2_t1563910866 * ___completedCallback_2;
	// EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9::resolver
	NativeConflictResolver_t651818787 * ___resolver_3;
	// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9::baseGame
	iOSGKSavedGame_t2037893409 * ___baseGame_4;
	// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame EasyMobile.Internal.GameServices.iOSSavedGameClient/<ConflictResolvingLoop>c__AnonStorey9::remoteGame
	iOSGKSavedGame_t2037893409 * ___remoteGame_5;

public:
	inline static int32_t get_offset_of_prefetchData_0() { return static_cast<int32_t>(offsetof(U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396, ___prefetchData_0)); }
	inline bool get_prefetchData_0() const { return ___prefetchData_0; }
	inline bool* get_address_of_prefetchData_0() { return &___prefetchData_0; }
	inline void set_prefetchData_0(bool value)
	{
		___prefetchData_0 = value;
	}

	inline static int32_t get_offset_of_conflictCallback_1() { return static_cast<int32_t>(offsetof(U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396, ___conflictCallback_1)); }
	inline iOSConflictCallback_t756019127 * get_conflictCallback_1() const { return ___conflictCallback_1; }
	inline iOSConflictCallback_t756019127 ** get_address_of_conflictCallback_1() { return &___conflictCallback_1; }
	inline void set_conflictCallback_1(iOSConflictCallback_t756019127 * value)
	{
		___conflictCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___conflictCallback_1), value);
	}

	inline static int32_t get_offset_of_completedCallback_2() { return static_cast<int32_t>(offsetof(U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396, ___completedCallback_2)); }
	inline Action_2_t1563910866 * get_completedCallback_2() const { return ___completedCallback_2; }
	inline Action_2_t1563910866 ** get_address_of_completedCallback_2() { return &___completedCallback_2; }
	inline void set_completedCallback_2(Action_2_t1563910866 * value)
	{
		___completedCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___completedCallback_2), value);
	}

	inline static int32_t get_offset_of_resolver_3() { return static_cast<int32_t>(offsetof(U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396, ___resolver_3)); }
	inline NativeConflictResolver_t651818787 * get_resolver_3() const { return ___resolver_3; }
	inline NativeConflictResolver_t651818787 ** get_address_of_resolver_3() { return &___resolver_3; }
	inline void set_resolver_3(NativeConflictResolver_t651818787 * value)
	{
		___resolver_3 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_3), value);
	}

	inline static int32_t get_offset_of_baseGame_4() { return static_cast<int32_t>(offsetof(U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396, ___baseGame_4)); }
	inline iOSGKSavedGame_t2037893409 * get_baseGame_4() const { return ___baseGame_4; }
	inline iOSGKSavedGame_t2037893409 ** get_address_of_baseGame_4() { return &___baseGame_4; }
	inline void set_baseGame_4(iOSGKSavedGame_t2037893409 * value)
	{
		___baseGame_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseGame_4), value);
	}

	inline static int32_t get_offset_of_remoteGame_5() { return static_cast<int32_t>(offsetof(U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396, ___remoteGame_5)); }
	inline iOSGKSavedGame_t2037893409 * get_remoteGame_5() const { return ___remoteGame_5; }
	inline iOSGKSavedGame_t2037893409 ** get_address_of_remoteGame_5() { return &___remoteGame_5; }
	inline void set_remoteGame_5(iOSGKSavedGame_t2037893409 * value)
	{
		___remoteGame_5 = value;
		Il2CppCodeGenWriteBarrier((&___remoteGame_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONFLICTRESOLVINGLOOPU3EC__ANONSTOREY9_T1617656396_H
#ifndef U3CDELETESAVEDGAMEU3EC__ANONSTOREY5_T4129970464_H
#define U3CDELETESAVEDGAMEU3EC__ANONSTOREY5_T4129970464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<DeleteSavedGame>c__AnonStorey5
struct  U3CDeleteSavedGameU3Ec__AnonStorey5_t4129970464  : public RuntimeObject
{
public:
	// System.String EasyMobile.Internal.GameServices.iOSSavedGameClient/<DeleteSavedGame>c__AnonStorey5::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CDeleteSavedGameU3Ec__AnonStorey5_t4129970464, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETESAVEDGAMEU3EC__ANONSTOREY5_T4129970464_H
#ifndef U3CFETCHALLSAVEDGAMESU3EC__ANONSTOREY4_T3113088512_H
#define U3CFETCHALLSAVEDGAMESU3EC__ANONSTOREY4_T3113088512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<FetchAllSavedGames>c__AnonStorey4
struct  U3CFetchAllSavedGamesU3Ec__AnonStorey4_t3113088512  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.SavedGame[],System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<FetchAllSavedGames>c__AnonStorey4::callback
	Action_2_t3708252210 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CFetchAllSavedGamesU3Ec__AnonStorey4_t3113088512, ___callback_0)); }
	inline Action_2_t3708252210 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3708252210 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3708252210 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHALLSAVEDGAMESU3EC__ANONSTOREY4_T3113088512_H
#ifndef U3CINTERNALDELETESAVEDGAMEU3EC__ANONSTOREYE_T3357283351_H
#define U3CINTERNALDELETESAVEDGAMEU3EC__ANONSTOREYE_T3357283351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalDeleteSavedGame>c__AnonStoreyE
struct  U3CInternalDeleteSavedGameU3Ec__AnonStoreyE_t3357283351  : public RuntimeObject
{
public:
	// System.Action`1<System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalDeleteSavedGame>c__AnonStoreyE::callback
	Action_1_t2019918284 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalDeleteSavedGameU3Ec__AnonStoreyE_t3357283351, ___callback_0)); }
	inline Action_1_t2019918284 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t2019918284 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t2019918284 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALDELETESAVEDGAMEU3EC__ANONSTOREYE_T3357283351_H
#ifndef U3CINTERNALFETCHSAVEDGAMESU3EC__ANONSTOREYC_T1277696669_H
#define U3CINTERNALFETCHSAVEDGAMESU3EC__ANONSTOREYC_T1277696669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalFetchSavedGames>c__AnonStoreyC
struct  U3CInternalFetchSavedGamesU3Ec__AnonStoreyC_t1277696669  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[],System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalFetchSavedGames>c__AnonStoreyC::callback
	Action_2_t2572077035 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalFetchSavedGamesU3Ec__AnonStoreyC_t1277696669, ___callback_0)); }
	inline Action_2_t2572077035 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t2572077035 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t2572077035 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALFETCHSAVEDGAMESU3EC__ANONSTOREYC_T1277696669_H
#ifndef U3CINTERNALLOADSAVEDGAMEDATAU3EC__ANONSTOREYB_T233618823_H
#define U3CINTERNALLOADSAVEDGAMEDATAU3EC__ANONSTOREYB_T233618823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalLoadSavedGameData>c__AnonStoreyB
struct  U3CInternalLoadSavedGameDataU3Ec__AnonStoreyB_t233618823  : public RuntimeObject
{
public:
	// System.Action`2<System.Byte[],System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalLoadSavedGameData>c__AnonStoreyB::callback
	Action_2_t3146294506 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalLoadSavedGameDataU3Ec__AnonStoreyB_t233618823, ___callback_0)); }
	inline Action_2_t3146294506 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3146294506 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3146294506 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALLOADSAVEDGAMEDATAU3EC__ANONSTOREYB_T233618823_H
#ifndef U3CINTERNALOPENSAVEDGAMEU3EC__ANONSTOREY8_T3579769808_H
#define U3CINTERNALOPENSAVEDGAMEU3EC__ANONSTOREY8_T3579769808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenSavedGame>c__AnonStorey8
struct  U3CInternalOpenSavedGameU3Ec__AnonStorey8_t3579769808  : public RuntimeObject
{
public:
	// System.Action`1<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[]> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenSavedGame>c__AnonStorey8::callback
	Action_1_t3342964423 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalOpenSavedGameU3Ec__AnonStorey8_t3579769808, ___callback_0)); }
	inline Action_1_t3342964423 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3342964423 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3342964423 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALOPENSAVEDGAMEU3EC__ANONSTOREY8_T3579769808_H
#ifndef U3CINTERNALOPENWITHMANUALCONFLICTRESOLUTIONU3EC__ANONSTOREY7_T2168430634_H
#define U3CINTERNALOPENWITHMANUALCONFLICTRESOLUTIONU3EC__ANONSTOREY7_T2168430634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithManualConflictResolution>c__AnonStorey7
struct  U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithManualConflictResolution>c__AnonStorey7::completedCallback
	Action_2_t1563910866 * ___completedCallback_0;
	// System.Boolean EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithManualConflictResolution>c__AnonStorey7::prefetchDataOnConflict
	bool ___prefetchDataOnConflict_1;
	// EasyMobile.Internal.GameServices.iOSConflictCallback EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithManualConflictResolution>c__AnonStorey7::conflictCallback
	iOSConflictCallback_t756019127 * ___conflictCallback_2;

public:
	inline static int32_t get_offset_of_completedCallback_0() { return static_cast<int32_t>(offsetof(U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634, ___completedCallback_0)); }
	inline Action_2_t1563910866 * get_completedCallback_0() const { return ___completedCallback_0; }
	inline Action_2_t1563910866 ** get_address_of_completedCallback_0() { return &___completedCallback_0; }
	inline void set_completedCallback_0(Action_2_t1563910866 * value)
	{
		___completedCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___completedCallback_0), value);
	}

	inline static int32_t get_offset_of_prefetchDataOnConflict_1() { return static_cast<int32_t>(offsetof(U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634, ___prefetchDataOnConflict_1)); }
	inline bool get_prefetchDataOnConflict_1() const { return ___prefetchDataOnConflict_1; }
	inline bool* get_address_of_prefetchDataOnConflict_1() { return &___prefetchDataOnConflict_1; }
	inline void set_prefetchDataOnConflict_1(bool value)
	{
		___prefetchDataOnConflict_1 = value;
	}

	inline static int32_t get_offset_of_conflictCallback_2() { return static_cast<int32_t>(offsetof(U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634, ___conflictCallback_2)); }
	inline iOSConflictCallback_t756019127 * get_conflictCallback_2() const { return ___conflictCallback_2; }
	inline iOSConflictCallback_t756019127 ** get_address_of_conflictCallback_2() { return &___conflictCallback_2; }
	inline void set_conflictCallback_2(iOSConflictCallback_t756019127 * value)
	{
		___conflictCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___conflictCallback_2), value);
	}
};

struct U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634_StaticFields
{
public:
	// System.Comparison`1<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithManualConflictResolution>c__AnonStorey7::<>f__am$cache0
	Comparison_1_t1812824588 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Comparison_1_t1812824588 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Comparison_1_t1812824588 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Comparison_1_t1812824588 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALOPENWITHMANUALCONFLICTRESOLUTIONU3EC__ANONSTOREY7_T2168430634_H
#ifndef U3CINTERNALRESOLVECONFLICTINGSAVEDGAMESU3EC__ANONSTOREYD_T990894612_H
#define U3CINTERNALRESOLVECONFLICTINGSAVEDGAMESU3EC__ANONSTOREYD_T990894612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalResolveConflictingSavedGames>c__AnonStoreyD
struct  U3CInternalResolveConflictingSavedGamesU3Ec__AnonStoreyD_t990894612  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[],System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalResolveConflictingSavedGames>c__AnonStoreyD::callback
	Action_2_t2572077035 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalResolveConflictingSavedGamesU3Ec__AnonStoreyD_t990894612, ___callback_0)); }
	inline Action_2_t2572077035 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t2572077035 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t2572077035 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALRESOLVECONFLICTINGSAVEDGAMESU3EC__ANONSTOREYD_T990894612_H
#ifndef U3CINTERNALSAVEGAMEDATAU3EC__ANONSTOREYA_T1786357441_H
#define U3CINTERNALSAVEGAMEDATAU3EC__ANONSTOREYA_T1786357441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalSaveGameData>c__AnonStoreyA
struct  U3CInternalSaveGameDataU3Ec__AnonStoreyA_t1786357441  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalSaveGameData>c__AnonStoreyA::callback
	Action_2_t1563910866 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalSaveGameDataU3Ec__AnonStoreyA_t1786357441, ___callback_0)); }
	inline Action_2_t1563910866 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t1563910866 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t1563910866 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALSAVEGAMEDATAU3EC__ANONSTOREYA_T1786357441_H
#ifndef U3COPENWITHAUTOMATICCONFLICTRESOLUTIONU3EC__ANONSTOREY0_T3585423550_H
#define U3COPENWITHAUTOMATICCONFLICTRESOLUTIONU3EC__ANONSTOREY0_T3585423550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey0
struct  U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey0_t3585423550  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.SavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey0::callback
	Action_2_t3088183511 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey0_t3585423550, ___callback_0)); }
	inline Action_2_t3088183511 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3088183511 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3088183511 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENWITHAUTOMATICCONFLICTRESOLUTIONU3EC__ANONSTOREY0_T3585423550_H
#ifndef U3COPENWITHMANUALCONFLICTRESOLUTIONU3EC__ANONSTOREY1_T1856166065_H
#define U3COPENWITHMANUALCONFLICTRESOLUTIONU3EC__ANONSTOREY1_T1856166065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<OpenWithManualConflictResolution>c__AnonStorey1
struct  U3COpenWithManualConflictResolutionU3Ec__AnonStorey1_t1856166065  : public RuntimeObject
{
public:
	// EasyMobile.SavedGameConflictResolver EasyMobile.Internal.GameServices.iOSSavedGameClient/<OpenWithManualConflictResolution>c__AnonStorey1::resolverFunction
	SavedGameConflictResolver_t2056225930 * ___resolverFunction_0;
	// System.Action`2<EasyMobile.SavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<OpenWithManualConflictResolution>c__AnonStorey1::completedCallback
	Action_2_t3088183511 * ___completedCallback_1;

public:
	inline static int32_t get_offset_of_resolverFunction_0() { return static_cast<int32_t>(offsetof(U3COpenWithManualConflictResolutionU3Ec__AnonStorey1_t1856166065, ___resolverFunction_0)); }
	inline SavedGameConflictResolver_t2056225930 * get_resolverFunction_0() const { return ___resolverFunction_0; }
	inline SavedGameConflictResolver_t2056225930 ** get_address_of_resolverFunction_0() { return &___resolverFunction_0; }
	inline void set_resolverFunction_0(SavedGameConflictResolver_t2056225930 * value)
	{
		___resolverFunction_0 = value;
		Il2CppCodeGenWriteBarrier((&___resolverFunction_0), value);
	}

	inline static int32_t get_offset_of_completedCallback_1() { return static_cast<int32_t>(offsetof(U3COpenWithManualConflictResolutionU3Ec__AnonStorey1_t1856166065, ___completedCallback_1)); }
	inline Action_2_t3088183511 * get_completedCallback_1() const { return ___completedCallback_1; }
	inline Action_2_t3088183511 ** get_address_of_completedCallback_1() { return &___completedCallback_1; }
	inline void set_completedCallback_1(Action_2_t3088183511 * value)
	{
		___completedCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___completedCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENWITHMANUALCONFLICTRESOLUTIONU3EC__ANONSTOREY1_T1856166065_H
#ifndef U3CREADSAVEDGAMEDATAU3EC__ANONSTOREY2_T89225513_H
#define U3CREADSAVEDGAMEDATAU3EC__ANONSTOREY2_T89225513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<ReadSavedGameData>c__AnonStorey2
struct  U3CReadSavedGameDataU3Ec__AnonStorey2_t89225513  : public RuntimeObject
{
public:
	// System.Action`3<EasyMobile.SavedGame,System.Byte[],System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<ReadSavedGameData>c__AnonStorey2::callback
	Action_3_t201929169 * ___callback_0;
	// EasyMobile.SavedGame EasyMobile.Internal.GameServices.iOSSavedGameClient/<ReadSavedGameData>c__AnonStorey2::savedGame
	SavedGame_t947814848 * ___savedGame_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CReadSavedGameDataU3Ec__AnonStorey2_t89225513, ___callback_0)); }
	inline Action_3_t201929169 * get_callback_0() const { return ___callback_0; }
	inline Action_3_t201929169 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_3_t201929169 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_savedGame_1() { return static_cast<int32_t>(offsetof(U3CReadSavedGameDataU3Ec__AnonStorey2_t89225513, ___savedGame_1)); }
	inline SavedGame_t947814848 * get_savedGame_1() const { return ___savedGame_1; }
	inline SavedGame_t947814848 ** get_address_of_savedGame_1() { return &___savedGame_1; }
	inline void set_savedGame_1(SavedGame_t947814848 * value)
	{
		___savedGame_1 = value;
		Il2CppCodeGenWriteBarrier((&___savedGame_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADSAVEDGAMEDATAU3EC__ANONSTOREY2_T89225513_H
#ifndef U3CWRITESAVEDGAMEDATAU3EC__ANONSTOREY3_T254795096_H
#define U3CWRITESAVEDGAMEDATAU3EC__ANONSTOREY3_T254795096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<WriteSavedGameData>c__AnonStorey3
struct  U3CWriteSavedGameDataU3Ec__AnonStorey3_t254795096  : public RuntimeObject
{
public:
	// System.Action`2<EasyMobile.SavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<WriteSavedGameData>c__AnonStorey3::callback
	Action_2_t3088183511 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CWriteSavedGameDataU3Ec__AnonStorey3_t254795096, ___callback_0)); }
	inline Action_2_t3088183511 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t3088183511 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t3088183511 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITESAVEDGAMEDATAU3EC__ANONSTOREY3_T254795096_H
#ifndef NATIVECONFLICTRESOLVER_T651818787_H
#define NATIVECONFLICTRESOLVER_T651818787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver
struct  NativeConflictResolver_t651818787  : public RuntimeObject
{
public:
	// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver::_base
	iOSGKSavedGame_t2037893409 * ____base_0;
	// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver::_remote
	iOSGKSavedGame_t2037893409 * ____remote_1;
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver::_completeCallback
	Action_2_t1563910866 * ____completeCallback_2;
	// System.Action`1<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[]> EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver::_repeatResolveConflicts
	Action_1_t3342964423 * ____repeatResolveConflicts_3;

public:
	inline static int32_t get_offset_of__base_0() { return static_cast<int32_t>(offsetof(NativeConflictResolver_t651818787, ____base_0)); }
	inline iOSGKSavedGame_t2037893409 * get__base_0() const { return ____base_0; }
	inline iOSGKSavedGame_t2037893409 ** get_address_of__base_0() { return &____base_0; }
	inline void set__base_0(iOSGKSavedGame_t2037893409 * value)
	{
		____base_0 = value;
		Il2CppCodeGenWriteBarrier((&____base_0), value);
	}

	inline static int32_t get_offset_of__remote_1() { return static_cast<int32_t>(offsetof(NativeConflictResolver_t651818787, ____remote_1)); }
	inline iOSGKSavedGame_t2037893409 * get__remote_1() const { return ____remote_1; }
	inline iOSGKSavedGame_t2037893409 ** get_address_of__remote_1() { return &____remote_1; }
	inline void set__remote_1(iOSGKSavedGame_t2037893409 * value)
	{
		____remote_1 = value;
		Il2CppCodeGenWriteBarrier((&____remote_1), value);
	}

	inline static int32_t get_offset_of__completeCallback_2() { return static_cast<int32_t>(offsetof(NativeConflictResolver_t651818787, ____completeCallback_2)); }
	inline Action_2_t1563910866 * get__completeCallback_2() const { return ____completeCallback_2; }
	inline Action_2_t1563910866 ** get_address_of__completeCallback_2() { return &____completeCallback_2; }
	inline void set__completeCallback_2(Action_2_t1563910866 * value)
	{
		____completeCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&____completeCallback_2), value);
	}

	inline static int32_t get_offset_of__repeatResolveConflicts_3() { return static_cast<int32_t>(offsetof(NativeConflictResolver_t651818787, ____repeatResolveConflicts_3)); }
	inline Action_1_t3342964423 * get__repeatResolveConflicts_3() const { return ____repeatResolveConflicts_3; }
	inline Action_1_t3342964423 ** get_address_of__repeatResolveConflicts_3() { return &____repeatResolveConflicts_3; }
	inline void set__repeatResolveConflicts_3(Action_1_t3342964423 * value)
	{
		____repeatResolveConflicts_3 = value;
		Il2CppCodeGenWriteBarrier((&____repeatResolveConflicts_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONFLICTRESOLVER_T651818787_H
#ifndef U3CCHOOSEGKSAVEDGAMEU3EC__ANONSTOREY0_T404845350_H
#define U3CCHOOSEGKSAVEDGAMEU3EC__ANONSTOREY0_T404845350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver/<ChooseGKSavedGame>c__AnonStorey0
struct  U3CChooseGKSavedGameU3Ec__AnonStorey0_t404845350  : public RuntimeObject
{
public:
	// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame[] EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver/<ChooseGKSavedGame>c__AnonStorey0::conflictingGames
	iOSGKSavedGameU5BU5D_t3170496828* ___conflictingGames_0;
	// EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver EasyMobile.Internal.GameServices.iOSSavedGameClient/NativeConflictResolver/<ChooseGKSavedGame>c__AnonStorey0::$this
	NativeConflictResolver_t651818787 * ___U24this_1;

public:
	inline static int32_t get_offset_of_conflictingGames_0() { return static_cast<int32_t>(offsetof(U3CChooseGKSavedGameU3Ec__AnonStorey0_t404845350, ___conflictingGames_0)); }
	inline iOSGKSavedGameU5BU5D_t3170496828* get_conflictingGames_0() const { return ___conflictingGames_0; }
	inline iOSGKSavedGameU5BU5D_t3170496828** get_address_of_conflictingGames_0() { return &___conflictingGames_0; }
	inline void set_conflictingGames_0(iOSGKSavedGameU5BU5D_t3170496828* value)
	{
		___conflictingGames_0 = value;
		Il2CppCodeGenWriteBarrier((&___conflictingGames_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CChooseGKSavedGameU3Ec__AnonStorey0_t404845350, ___U24this_1)); }
	inline NativeConflictResolver_t651818787 * get_U24this_1() const { return ___U24this_1; }
	inline NativeConflictResolver_t651818787 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(NativeConflictResolver_t651818787 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHOOSEGKSAVEDGAMEU3EC__ANONSTOREY0_T404845350_H
#ifndef PREFETCHER_T1369232627_H
#define PREFETCHER_T1369232627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher
struct  Prefetcher_t1369232627  : public RuntimeObject
{
public:
	// System.Object EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_lock
	RuntimeObject * ____lock_0;
	// System.Boolean EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_baseDataFetched
	bool ____baseDataFetched_1;
	// System.Byte[] EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_baseData
	ByteU5BU5D_t4116647657* ____baseData_2;
	// System.Boolean EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_remoteDataFetched
	bool ____remoteDataFetched_3;
	// System.Byte[] EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_remoteData
	ByteU5BU5D_t4116647657* ____remoteData_4;
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_completedCallback
	Action_2_t1563910866 * ____completedCallback_5;
	// System.Action`2<System.Byte[],System.Byte[]> EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::_dataFetchedCallback
	Action_2_t1120524178 * ____dataFetchedCallback_6;

public:
	inline static int32_t get_offset_of__lock_0() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____lock_0)); }
	inline RuntimeObject * get__lock_0() const { return ____lock_0; }
	inline RuntimeObject ** get_address_of__lock_0() { return &____lock_0; }
	inline void set__lock_0(RuntimeObject * value)
	{
		____lock_0 = value;
		Il2CppCodeGenWriteBarrier((&____lock_0), value);
	}

	inline static int32_t get_offset_of__baseDataFetched_1() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____baseDataFetched_1)); }
	inline bool get__baseDataFetched_1() const { return ____baseDataFetched_1; }
	inline bool* get_address_of__baseDataFetched_1() { return &____baseDataFetched_1; }
	inline void set__baseDataFetched_1(bool value)
	{
		____baseDataFetched_1 = value;
	}

	inline static int32_t get_offset_of__baseData_2() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____baseData_2)); }
	inline ByteU5BU5D_t4116647657* get__baseData_2() const { return ____baseData_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__baseData_2() { return &____baseData_2; }
	inline void set__baseData_2(ByteU5BU5D_t4116647657* value)
	{
		____baseData_2 = value;
		Il2CppCodeGenWriteBarrier((&____baseData_2), value);
	}

	inline static int32_t get_offset_of__remoteDataFetched_3() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____remoteDataFetched_3)); }
	inline bool get__remoteDataFetched_3() const { return ____remoteDataFetched_3; }
	inline bool* get_address_of__remoteDataFetched_3() { return &____remoteDataFetched_3; }
	inline void set__remoteDataFetched_3(bool value)
	{
		____remoteDataFetched_3 = value;
	}

	inline static int32_t get_offset_of__remoteData_4() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____remoteData_4)); }
	inline ByteU5BU5D_t4116647657* get__remoteData_4() const { return ____remoteData_4; }
	inline ByteU5BU5D_t4116647657** get_address_of__remoteData_4() { return &____remoteData_4; }
	inline void set__remoteData_4(ByteU5BU5D_t4116647657* value)
	{
		____remoteData_4 = value;
		Il2CppCodeGenWriteBarrier((&____remoteData_4), value);
	}

	inline static int32_t get_offset_of__completedCallback_5() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____completedCallback_5)); }
	inline Action_2_t1563910866 * get__completedCallback_5() const { return ____completedCallback_5; }
	inline Action_2_t1563910866 ** get_address_of__completedCallback_5() { return &____completedCallback_5; }
	inline void set__completedCallback_5(Action_2_t1563910866 * value)
	{
		____completedCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&____completedCallback_5), value);
	}

	inline static int32_t get_offset_of__dataFetchedCallback_6() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627, ____dataFetchedCallback_6)); }
	inline Action_2_t1120524178 * get__dataFetchedCallback_6() const { return ____dataFetchedCallback_6; }
	inline Action_2_t1120524178 ** get_address_of__dataFetchedCallback_6() { return &____dataFetchedCallback_6; }
	inline void set__dataFetchedCallback_6(Action_2_t1120524178 * value)
	{
		____dataFetchedCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&____dataFetchedCallback_6), value);
	}
};

struct Prefetcher_t1369232627_StaticFields
{
public:
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::<>f__am$cache0
	Action_2_t1563910866 * ___U3CU3Ef__amU24cache0_7;
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/Prefetcher::<>f__am$cache1
	Action_2_t1563910866 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_2_t1563910866 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_2_t1563910866 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_2_t1563910866 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(Prefetcher_t1369232627_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Action_2_t1563910866 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Action_2_t1563910866 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Action_2_t1563910866 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFETCHER_T1369232627_H
#ifndef IOSNATIVEGIF_T1153823649_H
#define IOSNATIVEGIF_T1153823649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Gif.iOS.iOSNativeGif
struct  iOSNativeGif_t1153823649  : public RuntimeObject
{
public:

public:
};

struct iOSNativeGif_t1153823649_StaticFields
{
public:
	// System.Action`2<System.Int32,System.Single> EasyMobile.Internal.Gif.iOS.iOSNativeGif::GifExportProgress
	Action_2_t2623443791 * ___GifExportProgress_0;
	// System.Action`2<System.Int32,System.String> EasyMobile.Internal.Gif.iOS.iOSNativeGif::GifExportCompleted
	Action_2_t3073627706 * ___GifExportCompleted_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Runtime.InteropServices.GCHandle[]> EasyMobile.Internal.Gif.iOS.iOSNativeGif::gcHandles
	Dictionary_2_t3219349245 * ___gcHandles_2;
	// EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportProgressDelegate EasyMobile.Internal.Gif.iOS.iOSNativeGif::<>f__mg$cache0
	GifExportProgressDelegate_t324897231 * ___U3CU3Ef__mgU24cache0_3;
	// EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportCompletedDelegate EasyMobile.Internal.Gif.iOS.iOSNativeGif::<>f__mg$cache1
	GifExportCompletedDelegate_t4082577070 * ___U3CU3Ef__mgU24cache1_4;

public:
	inline static int32_t get_offset_of_GifExportProgress_0() { return static_cast<int32_t>(offsetof(iOSNativeGif_t1153823649_StaticFields, ___GifExportProgress_0)); }
	inline Action_2_t2623443791 * get_GifExportProgress_0() const { return ___GifExportProgress_0; }
	inline Action_2_t2623443791 ** get_address_of_GifExportProgress_0() { return &___GifExportProgress_0; }
	inline void set_GifExportProgress_0(Action_2_t2623443791 * value)
	{
		___GifExportProgress_0 = value;
		Il2CppCodeGenWriteBarrier((&___GifExportProgress_0), value);
	}

	inline static int32_t get_offset_of_GifExportCompleted_1() { return static_cast<int32_t>(offsetof(iOSNativeGif_t1153823649_StaticFields, ___GifExportCompleted_1)); }
	inline Action_2_t3073627706 * get_GifExportCompleted_1() const { return ___GifExportCompleted_1; }
	inline Action_2_t3073627706 ** get_address_of_GifExportCompleted_1() { return &___GifExportCompleted_1; }
	inline void set_GifExportCompleted_1(Action_2_t3073627706 * value)
	{
		___GifExportCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___GifExportCompleted_1), value);
	}

	inline static int32_t get_offset_of_gcHandles_2() { return static_cast<int32_t>(offsetof(iOSNativeGif_t1153823649_StaticFields, ___gcHandles_2)); }
	inline Dictionary_2_t3219349245 * get_gcHandles_2() const { return ___gcHandles_2; }
	inline Dictionary_2_t3219349245 ** get_address_of_gcHandles_2() { return &___gcHandles_2; }
	inline void set_gcHandles_2(Dictionary_2_t3219349245 * value)
	{
		___gcHandles_2 = value;
		Il2CppCodeGenWriteBarrier((&___gcHandles_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(iOSNativeGif_t1153823649_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline GifExportProgressDelegate_t324897231 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline GifExportProgressDelegate_t324897231 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(GifExportProgressDelegate_t324897231 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_4() { return static_cast<int32_t>(offsetof(iOSNativeGif_t1153823649_StaticFields, ___U3CU3Ef__mgU24cache1_4)); }
	inline GifExportCompletedDelegate_t4082577070 * get_U3CU3Ef__mgU24cache1_4() const { return ___U3CU3Ef__mgU24cache1_4; }
	inline GifExportCompletedDelegate_t4082577070 ** get_address_of_U3CU3Ef__mgU24cache1_4() { return &___U3CU3Ef__mgU24cache1_4; }
	inline void set_U3CU3Ef__mgU24cache1_4(GifExportCompletedDelegate_t4082577070 * value)
	{
		___U3CU3Ef__mgU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEGIF_T1153823649_H
#ifndef IOSNATIVEALERT_T3657863450_H
#define IOSNATIVEALERT_T3657863450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.NativeAPIs.iOS.iOSNativeAlert
struct  iOSNativeAlert_t3657863450  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEALERT_T3657863450_H
#ifndef UNSUPPORTEDLOCALNOTIFICATIONCLIENT_T431853024_H
#define UNSUPPORTEDLOCALNOTIFICATIONCLIENT_T431853024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.UnsupportedLocalNotificationClient
struct  UnsupportedLocalNotificationClient_t431853024  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDLOCALNOTIFICATIONCLIENT_T431853024_H
#ifndef UNSUPPORTEDNOTIFICATIONLISTENER_T2573704219_H
#define UNSUPPORTEDNOTIFICATIONLISTENER_T2573704219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.UnsupportedNotificationListener
struct  UnsupportedNotificationListener_t2573704219  : public RuntimeObject
{
public:
	// System.Action`1<EasyMobile.LocalNotification> EasyMobile.Internal.Notifications.UnsupportedNotificationListener::LocalNotificationOpened
	Action_1_t516386935 * ___LocalNotificationOpened_1;
	// System.Action`1<EasyMobile.RemoteNotification> EasyMobile.Internal.Notifications.UnsupportedNotificationListener::RemoteNotificationOpened
	Action_1_t1859537056 * ___RemoteNotificationOpened_2;

public:
	inline static int32_t get_offset_of_LocalNotificationOpened_1() { return static_cast<int32_t>(offsetof(UnsupportedNotificationListener_t2573704219, ___LocalNotificationOpened_1)); }
	inline Action_1_t516386935 * get_LocalNotificationOpened_1() const { return ___LocalNotificationOpened_1; }
	inline Action_1_t516386935 ** get_address_of_LocalNotificationOpened_1() { return &___LocalNotificationOpened_1; }
	inline void set_LocalNotificationOpened_1(Action_1_t516386935 * value)
	{
		___LocalNotificationOpened_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalNotificationOpened_1), value);
	}

	inline static int32_t get_offset_of_RemoteNotificationOpened_2() { return static_cast<int32_t>(offsetof(UnsupportedNotificationListener_t2573704219, ___RemoteNotificationOpened_2)); }
	inline Action_1_t1859537056 * get_RemoteNotificationOpened_2() const { return ___RemoteNotificationOpened_2; }
	inline Action_1_t1859537056 ** get_address_of_RemoteNotificationOpened_2() { return &___RemoteNotificationOpened_2; }
	inline void set_RemoteNotificationOpened_2(Action_1_t1859537056 * value)
	{
		___RemoteNotificationOpened_2 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteNotificationOpened_2), value);
	}
};

struct UnsupportedNotificationListener_t2573704219_StaticFields
{
public:
	// EasyMobile.Internal.Notifications.UnsupportedNotificationListener EasyMobile.Internal.Notifications.UnsupportedNotificationListener::sInstance
	UnsupportedNotificationListener_t2573704219 * ___sInstance_0;
	// EasyMobile.Internal.Notifications.NativeNotificationHandler EasyMobile.Internal.Notifications.UnsupportedNotificationListener::<>f__am$cache0
	NativeNotificationHandler_t1892928298 * ___U3CU3Ef__amU24cache0_3;
	// EasyMobile.Internal.Notifications.NativeNotificationHandler EasyMobile.Internal.Notifications.UnsupportedNotificationListener::<>f__am$cache1
	NativeNotificationHandler_t1892928298 * ___U3CU3Ef__amU24cache1_4;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(UnsupportedNotificationListener_t2573704219_StaticFields, ___sInstance_0)); }
	inline UnsupportedNotificationListener_t2573704219 * get_sInstance_0() const { return ___sInstance_0; }
	inline UnsupportedNotificationListener_t2573704219 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(UnsupportedNotificationListener_t2573704219 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(UnsupportedNotificationListener_t2573704219_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline NativeNotificationHandler_t1892928298 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline NativeNotificationHandler_t1892928298 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(NativeNotificationHandler_t1892928298 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(UnsupportedNotificationListener_t2573704219_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline NativeNotificationHandler_t1892928298 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline NativeNotificationHandler_t1892928298 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(NativeNotificationHandler_t1892928298 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDNOTIFICATIONLISTENER_T2573704219_H
#ifndef IOSNOTIFICATIONHELPER_T1663845977_H
#define IOSNOTIFICATIONHELPER_T1663845977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationHelper
struct  iOSNotificationHelper_t1663845977  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNOTIFICATIONHELPER_T1663845977_H
#ifndef IOSNOTIFICATIONNATIVE_T1286273913_H
#define IOSNOTIFICATIONNATIVE_T1286273913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative
struct  iOSNotificationNative_t1286273913  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNOTIFICATIONNATIVE_T1286273913_H
#ifndef IOSLOCALNOTIFICATIONCLIENT_T527495909_H
#define IOSLOCALNOTIFICATIONCLIENT_T527495909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSLocalNotificationClient
struct  iOSLocalNotificationClient_t527495909  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Internal.Notifications.iOSLocalNotificationClient::mIsInitialized
	bool ___mIsInitialized_0;

public:
	inline static int32_t get_offset_of_mIsInitialized_0() { return static_cast<int32_t>(offsetof(iOSLocalNotificationClient_t527495909, ___mIsInitialized_0)); }
	inline bool get_mIsInitialized_0() const { return ___mIsInitialized_0; }
	inline bool* get_address_of_mIsInitialized_0() { return &___mIsInitialized_0; }
	inline void set_mIsInitialized_0(bool value)
	{
		___mIsInitialized_0 = value;
	}
};

struct iOSLocalNotificationClient_t527495909_StaticFields
{
public:
	// System.Func`2<System.IntPtr,EasyMobile.Internal.Notifications.iOS.GetPendingNotificationRequestsResponse> EasyMobile.Internal.Notifications.iOSLocalNotificationClient::<>f__mg$cache0
	Func_2_t2389551456 * ___U3CU3Ef__mgU24cache0_1;
	// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetPendingNotificationRequestsCallback EasyMobile.Internal.Notifications.iOSLocalNotificationClient::<>f__mg$cache1
	GetPendingNotificationRequestsCallback_t3407937767 * ___U3CU3Ef__mgU24cache1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(iOSLocalNotificationClient_t527495909_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Func_2_t2389551456 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Func_2_t2389551456 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Func_2_t2389551456 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_2() { return static_cast<int32_t>(offsetof(iOSLocalNotificationClient_t527495909_StaticFields, ___U3CU3Ef__mgU24cache1_2)); }
	inline GetPendingNotificationRequestsCallback_t3407937767 * get_U3CU3Ef__mgU24cache1_2() const { return ___U3CU3Ef__mgU24cache1_2; }
	inline GetPendingNotificationRequestsCallback_t3407937767 ** get_address_of_U3CU3Ef__mgU24cache1_2() { return &___U3CU3Ef__mgU24cache1_2; }
	inline void set_U3CU3Ef__mgU24cache1_2(GetPendingNotificationRequestsCallback_t3407937767 * value)
	{
		___U3CU3Ef__mgU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSLOCALNOTIFICATIONCLIENT_T527495909_H
#ifndef U3CGETPENDINGLOCALNOTIFICATIONSU3EC__ANONSTOREY0_T1018253411_H
#define U3CGETPENDINGLOCALNOTIFICATIONSU3EC__ANONSTOREY0_T1018253411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSLocalNotificationClient/<GetPendingLocalNotifications>c__AnonStorey0
struct  U3CGetPendingLocalNotificationsU3Ec__AnonStorey0_t1018253411  : public RuntimeObject
{
public:
	// System.Action`1<EasyMobile.NotificationRequest[]> EasyMobile.Internal.Notifications.iOSLocalNotificationClient/<GetPendingLocalNotifications>c__AnonStorey0::callback
	Action_1_t2750272105 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CGetPendingLocalNotificationsU3Ec__AnonStorey0_t1018253411, ___callback_0)); }
	inline Action_1_t2750272105 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t2750272105 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t2750272105 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPENDINGLOCALNOTIFICATIONSU3EC__ANONSTOREY0_T1018253411_H
#ifndef U3CINTERNALGETPENDINGNOTIFICATIONREQUESTSU3EC__ANONSTOREY1_T978461598_H
#define U3CINTERNALGETPENDINGNOTIFICATIONREQUESTSU3EC__ANONSTOREY1_T978461598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSLocalNotificationClient/<InternalGetPendingNotificationRequests>c__AnonStorey1
struct  U3CInternalGetPendingNotificationRequestsU3Ec__AnonStorey1_t978461598  : public RuntimeObject
{
public:
	// System.Action`1<EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest[]> EasyMobile.Internal.Notifications.iOSLocalNotificationClient/<InternalGetPendingNotificationRequests>c__AnonStorey1::callback
	Action_1_t3976455385 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalGetPendingNotificationRequestsU3Ec__AnonStorey1_t978461598, ___callback_0)); }
	inline Action_1_t3976455385 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3976455385 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3976455385 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALGETPENDINGNOTIFICATIONREQUESTSU3EC__ANONSTOREY1_T978461598_H
#ifndef U3CCRRAISELOCALNOTIFICATIONEVENTU3EC__ITERATOR0_T3068192955_H
#define U3CCRRAISELOCALNOTIFICATIONEVENTU3EC__ITERATOR0_T3068192955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0
struct  U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955  : public RuntimeObject
{
public:
	// EasyMobile.NotificationRequest EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::request
	NotificationRequest_t4022128807 * ___request_0;
	// System.String EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::actionId
	String_t* ___actionId_1;
	// System.Boolean EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::isForeground
	bool ___isForeground_2;
	// EasyMobile.Internal.Notifications.iOSNotificationListener EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::$this
	iOSNotificationListener_t2070489240 * ___U24this_3;
	// System.Object EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::$PC
	int32_t ___U24PC_6;
	// EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1 EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0::$locvar0
	U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746 * ___U24locvar0_7;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___request_0)); }
	inline NotificationRequest_t4022128807 * get_request_0() const { return ___request_0; }
	inline NotificationRequest_t4022128807 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(NotificationRequest_t4022128807 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_actionId_1() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___actionId_1)); }
	inline String_t* get_actionId_1() const { return ___actionId_1; }
	inline String_t** get_address_of_actionId_1() { return &___actionId_1; }
	inline void set_actionId_1(String_t* value)
	{
		___actionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___actionId_1), value);
	}

	inline static int32_t get_offset_of_isForeground_2() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___isForeground_2)); }
	inline bool get_isForeground_2() const { return ___isForeground_2; }
	inline bool* get_address_of_isForeground_2() { return &___isForeground_2; }
	inline void set_isForeground_2(bool value)
	{
		___isForeground_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___U24this_3)); }
	inline iOSNotificationListener_t2070489240 * get_U24this_3() const { return ___U24this_3; }
	inline iOSNotificationListener_t2070489240 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(iOSNotificationListener_t2070489240 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_7() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955, ___U24locvar0_7)); }
	inline U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746 * get_U24locvar0_7() const { return ___U24locvar0_7; }
	inline U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746 ** get_address_of_U24locvar0_7() { return &___U24locvar0_7; }
	inline void set_U24locvar0_7(U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746 * value)
	{
		___U24locvar0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRRAISELOCALNOTIFICATIONEVENTU3EC__ITERATOR0_T3068192955_H
#ifndef U3CCRRAISELOCALNOTIFICATIONEVENTU3EC__ANONSTOREY1_T148810746_H
#define U3CCRRAISELOCALNOTIFICATIONEVENTU3EC__ANONSTOREY1_T148810746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1
struct  U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746  : public RuntimeObject
{
public:
	// EasyMobile.NotificationRequest EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1::request
	NotificationRequest_t4022128807 * ___request_0;
	// System.String EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1::actionId
	String_t* ___actionId_1;
	// System.Boolean EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1::isForeground
	bool ___isForeground_2;
	// EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0 EasyMobile.Internal.Notifications.iOSNotificationListener/<CRRaiseLocalNotificationEvent>c__Iterator0/<CRRaiseLocalNotificationEvent>c__AnonStorey1::<>f__ref$0
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955 * ___U3CU3Ef__refU240_3;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746, ___request_0)); }
	inline NotificationRequest_t4022128807 * get_request_0() const { return ___request_0; }
	inline NotificationRequest_t4022128807 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(NotificationRequest_t4022128807 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_actionId_1() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746, ___actionId_1)); }
	inline String_t* get_actionId_1() const { return ___actionId_1; }
	inline String_t** get_address_of_actionId_1() { return &___actionId_1; }
	inline void set_actionId_1(String_t* value)
	{
		___actionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___actionId_1), value);
	}

	inline static int32_t get_offset_of_isForeground_2() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746, ___isForeground_2)); }
	inline bool get_isForeground_2() const { return ___isForeground_2; }
	inline bool* get_address_of_isForeground_2() { return &___isForeground_2; }
	inline void set_isForeground_2(bool value)
	{
		___isForeground_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_3() { return static_cast<int32_t>(offsetof(U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746, ___U3CU3Ef__refU240_3)); }
	inline U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955 * get_U3CU3Ef__refU240_3() const { return ___U3CU3Ef__refU240_3; }
	inline U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955 ** get_address_of_U3CU3Ef__refU240_3() { return &___U3CU3Ef__refU240_3; }
	inline void set_U3CU3Ef__refU240_3(U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955 * value)
	{
		___U3CU3Ef__refU240_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRRAISELOCALNOTIFICATIONEVENTU3EC__ANONSTOREY1_T148810746_H
#ifndef U3CINTERNALGETNOTIFICATIONRESPONSEU3EC__ANONSTOREY3_T1240372025_H
#define U3CINTERNALGETNOTIFICATIONRESPONSEU3EC__ANONSTOREY3_T1240372025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSNotificationListener/<InternalGetNotificationResponse>c__AnonStorey3
struct  U3CInternalGetNotificationResponseU3Ec__AnonStorey3_t1240372025  : public RuntimeObject
{
public:
	// System.Action`1<EasyMobile.Internal.Notifications.iOS.iOSNotificationResponse> EasyMobile.Internal.Notifications.iOSNotificationListener/<InternalGetNotificationResponse>c__AnonStorey3::callback
	Action_1_t3697448605 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CInternalGetNotificationResponseU3Ec__AnonStorey3_t1240372025, ___callback_0)); }
	inline Action_1_t3697448605 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3697448605 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3697448605 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALGETNOTIFICATIONRESPONSEU3EC__ANONSTOREY3_T1240372025_H
#ifndef U3CINTERNALONNATIVENOTIFICATIONEVENTU3EC__ANONSTOREY2_T558446483_H
#define U3CINTERNALONNATIVENOTIFICATIONEVENTU3EC__ANONSTOREY2_T558446483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSNotificationListener/<InternalOnNativeNotificationEvent>c__AnonStorey2
struct  U3CInternalOnNativeNotificationEventU3Ec__AnonStorey2_t558446483  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Internal.Notifications.iOSNotificationListener/<InternalOnNativeNotificationEvent>c__AnonStorey2::isForeground
	bool ___isForeground_0;
	// EasyMobile.Internal.Notifications.iOSNotificationListener EasyMobile.Internal.Notifications.iOSNotificationListener/<InternalOnNativeNotificationEvent>c__AnonStorey2::$this
	iOSNotificationListener_t2070489240 * ___U24this_1;

public:
	inline static int32_t get_offset_of_isForeground_0() { return static_cast<int32_t>(offsetof(U3CInternalOnNativeNotificationEventU3Ec__AnonStorey2_t558446483, ___isForeground_0)); }
	inline bool get_isForeground_0() const { return ___isForeground_0; }
	inline bool* get_address_of_isForeground_0() { return &___isForeground_0; }
	inline void set_isForeground_0(bool value)
	{
		___isForeground_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInternalOnNativeNotificationEventU3Ec__AnonStorey2_t558446483, ___U24this_1)); }
	inline iOSNotificationListener_t2070489240 * get_U24this_1() const { return ___U24this_1; }
	inline iOSNotificationListener_t2070489240 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(iOSNotificationListener_t2070489240 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALONNATIVENOTIFICATIONEVENTU3EC__ANONSTOREY2_T558446483_H
#ifndef NATIVEUI_T364718726_H
#define NATIVEUI_T364718726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NativeUI
struct  NativeUI_t364718726  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEUI_T364718726_H
#ifndef SAVEDGAME_T947814848_H
#define SAVEDGAME_T947814848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.SavedGame
struct  SavedGame_t947814848  : public RuntimeObject
{
public:
	// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame EasyMobile.SavedGame::<GKSavedGame>k__BackingField
	iOSGKSavedGame_t2037893409 * ___U3CGKSavedGameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CGKSavedGameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SavedGame_t947814848, ___U3CGKSavedGameU3Ek__BackingField_0)); }
	inline iOSGKSavedGame_t2037893409 * get_U3CGKSavedGameU3Ek__BackingField_0() const { return ___U3CGKSavedGameU3Ek__BackingField_0; }
	inline iOSGKSavedGame_t2037893409 ** get_address_of_U3CGKSavedGameU3Ek__BackingField_0() { return &___U3CGKSavedGameU3Ek__BackingField_0; }
	inline void set_U3CGKSavedGameU3Ek__BackingField_0(iOSGKSavedGame_t2037893409 * value)
	{
		___U3CGKSavedGameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGKSavedGameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDGAME_T947814848_H
#ifndef GIFENCODER_T60958606_H
#define GIFENCODER_T60958606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moments.Encoder.GifEncoder
struct  GifEncoder_t60958606  : public RuntimeObject
{
public:
	// System.Int32 Moments.Encoder.GifEncoder::m_Width
	int32_t ___m_Width_0;
	// System.Int32 Moments.Encoder.GifEncoder::m_Height
	int32_t ___m_Height_1;
	// System.Int32 Moments.Encoder.GifEncoder::m_Repeat
	int32_t ___m_Repeat_2;
	// System.Int32 Moments.Encoder.GifEncoder::m_FrameDelay
	int32_t ___m_FrameDelay_3;
	// System.Boolean Moments.Encoder.GifEncoder::m_HasStarted
	bool ___m_HasStarted_4;
	// System.IO.FileStream Moments.Encoder.GifEncoder::m_FileStream
	FileStream_t4292183065 * ___m_FileStream_5;
	// Moments.Encoder.GifFrame Moments.Encoder.GifEncoder::m_CurrentFrame
	GifFrame_t2916230237 * ___m_CurrentFrame_6;
	// System.Byte[] Moments.Encoder.GifEncoder::m_Pixels
	ByteU5BU5D_t4116647657* ___m_Pixels_7;
	// System.Byte[] Moments.Encoder.GifEncoder::m_IndexedPixels
	ByteU5BU5D_t4116647657* ___m_IndexedPixels_8;
	// System.Int32 Moments.Encoder.GifEncoder::m_ColorDepth
	int32_t ___m_ColorDepth_9;
	// System.Byte[] Moments.Encoder.GifEncoder::m_ColorTab
	ByteU5BU5D_t4116647657* ___m_ColorTab_10;
	// System.Boolean[] Moments.Encoder.GifEncoder::m_UsedEntry
	BooleanU5BU5D_t2897418192* ___m_UsedEntry_11;
	// System.Int32 Moments.Encoder.GifEncoder::m_PaletteSize
	int32_t ___m_PaletteSize_12;
	// System.Int32 Moments.Encoder.GifEncoder::m_DisposalCode
	int32_t ___m_DisposalCode_13;
	// System.Boolean Moments.Encoder.GifEncoder::m_ShouldCloseStream
	bool ___m_ShouldCloseStream_14;
	// System.Boolean Moments.Encoder.GifEncoder::m_IsFirstFrame
	bool ___m_IsFirstFrame_15;
	// System.Boolean Moments.Encoder.GifEncoder::m_IsSizeSet
	bool ___m_IsSizeSet_16;
	// System.Int32 Moments.Encoder.GifEncoder::m_SampleInterval
	int32_t ___m_SampleInterval_17;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_Repeat_2() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_Repeat_2)); }
	inline int32_t get_m_Repeat_2() const { return ___m_Repeat_2; }
	inline int32_t* get_address_of_m_Repeat_2() { return &___m_Repeat_2; }
	inline void set_m_Repeat_2(int32_t value)
	{
		___m_Repeat_2 = value;
	}

	inline static int32_t get_offset_of_m_FrameDelay_3() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_FrameDelay_3)); }
	inline int32_t get_m_FrameDelay_3() const { return ___m_FrameDelay_3; }
	inline int32_t* get_address_of_m_FrameDelay_3() { return &___m_FrameDelay_3; }
	inline void set_m_FrameDelay_3(int32_t value)
	{
		___m_FrameDelay_3 = value;
	}

	inline static int32_t get_offset_of_m_HasStarted_4() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_HasStarted_4)); }
	inline bool get_m_HasStarted_4() const { return ___m_HasStarted_4; }
	inline bool* get_address_of_m_HasStarted_4() { return &___m_HasStarted_4; }
	inline void set_m_HasStarted_4(bool value)
	{
		___m_HasStarted_4 = value;
	}

	inline static int32_t get_offset_of_m_FileStream_5() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_FileStream_5)); }
	inline FileStream_t4292183065 * get_m_FileStream_5() const { return ___m_FileStream_5; }
	inline FileStream_t4292183065 ** get_address_of_m_FileStream_5() { return &___m_FileStream_5; }
	inline void set_m_FileStream_5(FileStream_t4292183065 * value)
	{
		___m_FileStream_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FileStream_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentFrame_6() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_CurrentFrame_6)); }
	inline GifFrame_t2916230237 * get_m_CurrentFrame_6() const { return ___m_CurrentFrame_6; }
	inline GifFrame_t2916230237 ** get_address_of_m_CurrentFrame_6() { return &___m_CurrentFrame_6; }
	inline void set_m_CurrentFrame_6(GifFrame_t2916230237 * value)
	{
		___m_CurrentFrame_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentFrame_6), value);
	}

	inline static int32_t get_offset_of_m_Pixels_7() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_Pixels_7)); }
	inline ByteU5BU5D_t4116647657* get_m_Pixels_7() const { return ___m_Pixels_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_Pixels_7() { return &___m_Pixels_7; }
	inline void set_m_Pixels_7(ByteU5BU5D_t4116647657* value)
	{
		___m_Pixels_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pixels_7), value);
	}

	inline static int32_t get_offset_of_m_IndexedPixels_8() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_IndexedPixels_8)); }
	inline ByteU5BU5D_t4116647657* get_m_IndexedPixels_8() const { return ___m_IndexedPixels_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_IndexedPixels_8() { return &___m_IndexedPixels_8; }
	inline void set_m_IndexedPixels_8(ByteU5BU5D_t4116647657* value)
	{
		___m_IndexedPixels_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_IndexedPixels_8), value);
	}

	inline static int32_t get_offset_of_m_ColorDepth_9() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_ColorDepth_9)); }
	inline int32_t get_m_ColorDepth_9() const { return ___m_ColorDepth_9; }
	inline int32_t* get_address_of_m_ColorDepth_9() { return &___m_ColorDepth_9; }
	inline void set_m_ColorDepth_9(int32_t value)
	{
		___m_ColorDepth_9 = value;
	}

	inline static int32_t get_offset_of_m_ColorTab_10() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_ColorTab_10)); }
	inline ByteU5BU5D_t4116647657* get_m_ColorTab_10() const { return ___m_ColorTab_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_ColorTab_10() { return &___m_ColorTab_10; }
	inline void set_m_ColorTab_10(ByteU5BU5D_t4116647657* value)
	{
		___m_ColorTab_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTab_10), value);
	}

	inline static int32_t get_offset_of_m_UsedEntry_11() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_UsedEntry_11)); }
	inline BooleanU5BU5D_t2897418192* get_m_UsedEntry_11() const { return ___m_UsedEntry_11; }
	inline BooleanU5BU5D_t2897418192** get_address_of_m_UsedEntry_11() { return &___m_UsedEntry_11; }
	inline void set_m_UsedEntry_11(BooleanU5BU5D_t2897418192* value)
	{
		___m_UsedEntry_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_UsedEntry_11), value);
	}

	inline static int32_t get_offset_of_m_PaletteSize_12() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_PaletteSize_12)); }
	inline int32_t get_m_PaletteSize_12() const { return ___m_PaletteSize_12; }
	inline int32_t* get_address_of_m_PaletteSize_12() { return &___m_PaletteSize_12; }
	inline void set_m_PaletteSize_12(int32_t value)
	{
		___m_PaletteSize_12 = value;
	}

	inline static int32_t get_offset_of_m_DisposalCode_13() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_DisposalCode_13)); }
	inline int32_t get_m_DisposalCode_13() const { return ___m_DisposalCode_13; }
	inline int32_t* get_address_of_m_DisposalCode_13() { return &___m_DisposalCode_13; }
	inline void set_m_DisposalCode_13(int32_t value)
	{
		___m_DisposalCode_13 = value;
	}

	inline static int32_t get_offset_of_m_ShouldCloseStream_14() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_ShouldCloseStream_14)); }
	inline bool get_m_ShouldCloseStream_14() const { return ___m_ShouldCloseStream_14; }
	inline bool* get_address_of_m_ShouldCloseStream_14() { return &___m_ShouldCloseStream_14; }
	inline void set_m_ShouldCloseStream_14(bool value)
	{
		___m_ShouldCloseStream_14 = value;
	}

	inline static int32_t get_offset_of_m_IsFirstFrame_15() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_IsFirstFrame_15)); }
	inline bool get_m_IsFirstFrame_15() const { return ___m_IsFirstFrame_15; }
	inline bool* get_address_of_m_IsFirstFrame_15() { return &___m_IsFirstFrame_15; }
	inline void set_m_IsFirstFrame_15(bool value)
	{
		___m_IsFirstFrame_15 = value;
	}

	inline static int32_t get_offset_of_m_IsSizeSet_16() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_IsSizeSet_16)); }
	inline bool get_m_IsSizeSet_16() const { return ___m_IsSizeSet_16; }
	inline bool* get_address_of_m_IsSizeSet_16() { return &___m_IsSizeSet_16; }
	inline void set_m_IsSizeSet_16(bool value)
	{
		___m_IsSizeSet_16 = value;
	}

	inline static int32_t get_offset_of_m_SampleInterval_17() { return static_cast<int32_t>(offsetof(GifEncoder_t60958606, ___m_SampleInterval_17)); }
	inline int32_t get_m_SampleInterval_17() const { return ___m_SampleInterval_17; }
	inline int32_t* get_address_of_m_SampleInterval_17() { return &___m_SampleInterval_17; }
	inline void set_m_SampleInterval_17(int32_t value)
	{
		___m_SampleInterval_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIFENCODER_T60958606_H
#ifndef GIFFRAME_T2916230237_H
#define GIFFRAME_T2916230237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moments.Encoder.GifFrame
struct  GifFrame_t2916230237  : public RuntimeObject
{
public:
	// System.Int32 Moments.Encoder.GifFrame::Width
	int32_t ___Width_0;
	// System.Int32 Moments.Encoder.GifFrame::Height
	int32_t ___Height_1;
	// UnityEngine.Color32[] Moments.Encoder.GifFrame::Data
	Color32U5BU5D_t3850468773* ___Data_2;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(GifFrame_t2916230237, ___Width_0)); }
	inline int32_t get_Width_0() const { return ___Width_0; }
	inline int32_t* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(int32_t value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Height_1() { return static_cast<int32_t>(offsetof(GifFrame_t2916230237, ___Height_1)); }
	inline int32_t get_Height_1() const { return ___Height_1; }
	inline int32_t* get_address_of_Height_1() { return &___Height_1; }
	inline void set_Height_1(int32_t value)
	{
		___Height_1 = value;
	}

	inline static int32_t get_offset_of_Data_2() { return static_cast<int32_t>(offsetof(GifFrame_t2916230237, ___Data_2)); }
	inline Color32U5BU5D_t3850468773* get_Data_2() const { return ___Data_2; }
	inline Color32U5BU5D_t3850468773** get_address_of_Data_2() { return &___Data_2; }
	inline void set_Data_2(Color32U5BU5D_t3850468773* value)
	{
		___Data_2 = value;
		Il2CppCodeGenWriteBarrier((&___Data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIFFRAME_T2916230237_H
#ifndef LZWENCODER_T293885915_H
#define LZWENCODER_T293885915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moments.Encoder.LzwEncoder
struct  LzwEncoder_t293885915  : public RuntimeObject
{
public:
	// System.Byte[] Moments.Encoder.LzwEncoder::pixAry
	ByteU5BU5D_t4116647657* ___pixAry_1;
	// System.Int32 Moments.Encoder.LzwEncoder::initCodeSize
	int32_t ___initCodeSize_2;
	// System.Int32 Moments.Encoder.LzwEncoder::curPixel
	int32_t ___curPixel_3;
	// System.Int32 Moments.Encoder.LzwEncoder::n_bits
	int32_t ___n_bits_6;
	// System.Int32 Moments.Encoder.LzwEncoder::maxbits
	int32_t ___maxbits_7;
	// System.Int32 Moments.Encoder.LzwEncoder::maxcode
	int32_t ___maxcode_8;
	// System.Int32 Moments.Encoder.LzwEncoder::maxmaxcode
	int32_t ___maxmaxcode_9;
	// System.Int32[] Moments.Encoder.LzwEncoder::htab
	Int32U5BU5D_t385246372* ___htab_10;
	// System.Int32[] Moments.Encoder.LzwEncoder::codetab
	Int32U5BU5D_t385246372* ___codetab_11;
	// System.Int32 Moments.Encoder.LzwEncoder::hsize
	int32_t ___hsize_12;
	// System.Int32 Moments.Encoder.LzwEncoder::free_ent
	int32_t ___free_ent_13;
	// System.Boolean Moments.Encoder.LzwEncoder::clear_flg
	bool ___clear_flg_14;
	// System.Int32 Moments.Encoder.LzwEncoder::g_init_bits
	int32_t ___g_init_bits_15;
	// System.Int32 Moments.Encoder.LzwEncoder::ClearCode
	int32_t ___ClearCode_16;
	// System.Int32 Moments.Encoder.LzwEncoder::EOFCode
	int32_t ___EOFCode_17;
	// System.Int32 Moments.Encoder.LzwEncoder::cur_accum
	int32_t ___cur_accum_18;
	// System.Int32 Moments.Encoder.LzwEncoder::cur_bits
	int32_t ___cur_bits_19;
	// System.Int32[] Moments.Encoder.LzwEncoder::masks
	Int32U5BU5D_t385246372* ___masks_20;
	// System.Int32 Moments.Encoder.LzwEncoder::a_count
	int32_t ___a_count_21;
	// System.Byte[] Moments.Encoder.LzwEncoder::accum
	ByteU5BU5D_t4116647657* ___accum_22;

public:
	inline static int32_t get_offset_of_pixAry_1() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___pixAry_1)); }
	inline ByteU5BU5D_t4116647657* get_pixAry_1() const { return ___pixAry_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_pixAry_1() { return &___pixAry_1; }
	inline void set_pixAry_1(ByteU5BU5D_t4116647657* value)
	{
		___pixAry_1 = value;
		Il2CppCodeGenWriteBarrier((&___pixAry_1), value);
	}

	inline static int32_t get_offset_of_initCodeSize_2() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___initCodeSize_2)); }
	inline int32_t get_initCodeSize_2() const { return ___initCodeSize_2; }
	inline int32_t* get_address_of_initCodeSize_2() { return &___initCodeSize_2; }
	inline void set_initCodeSize_2(int32_t value)
	{
		___initCodeSize_2 = value;
	}

	inline static int32_t get_offset_of_curPixel_3() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___curPixel_3)); }
	inline int32_t get_curPixel_3() const { return ___curPixel_3; }
	inline int32_t* get_address_of_curPixel_3() { return &___curPixel_3; }
	inline void set_curPixel_3(int32_t value)
	{
		___curPixel_3 = value;
	}

	inline static int32_t get_offset_of_n_bits_6() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___n_bits_6)); }
	inline int32_t get_n_bits_6() const { return ___n_bits_6; }
	inline int32_t* get_address_of_n_bits_6() { return &___n_bits_6; }
	inline void set_n_bits_6(int32_t value)
	{
		___n_bits_6 = value;
	}

	inline static int32_t get_offset_of_maxbits_7() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___maxbits_7)); }
	inline int32_t get_maxbits_7() const { return ___maxbits_7; }
	inline int32_t* get_address_of_maxbits_7() { return &___maxbits_7; }
	inline void set_maxbits_7(int32_t value)
	{
		___maxbits_7 = value;
	}

	inline static int32_t get_offset_of_maxcode_8() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___maxcode_8)); }
	inline int32_t get_maxcode_8() const { return ___maxcode_8; }
	inline int32_t* get_address_of_maxcode_8() { return &___maxcode_8; }
	inline void set_maxcode_8(int32_t value)
	{
		___maxcode_8 = value;
	}

	inline static int32_t get_offset_of_maxmaxcode_9() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___maxmaxcode_9)); }
	inline int32_t get_maxmaxcode_9() const { return ___maxmaxcode_9; }
	inline int32_t* get_address_of_maxmaxcode_9() { return &___maxmaxcode_9; }
	inline void set_maxmaxcode_9(int32_t value)
	{
		___maxmaxcode_9 = value;
	}

	inline static int32_t get_offset_of_htab_10() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___htab_10)); }
	inline Int32U5BU5D_t385246372* get_htab_10() const { return ___htab_10; }
	inline Int32U5BU5D_t385246372** get_address_of_htab_10() { return &___htab_10; }
	inline void set_htab_10(Int32U5BU5D_t385246372* value)
	{
		___htab_10 = value;
		Il2CppCodeGenWriteBarrier((&___htab_10), value);
	}

	inline static int32_t get_offset_of_codetab_11() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___codetab_11)); }
	inline Int32U5BU5D_t385246372* get_codetab_11() const { return ___codetab_11; }
	inline Int32U5BU5D_t385246372** get_address_of_codetab_11() { return &___codetab_11; }
	inline void set_codetab_11(Int32U5BU5D_t385246372* value)
	{
		___codetab_11 = value;
		Il2CppCodeGenWriteBarrier((&___codetab_11), value);
	}

	inline static int32_t get_offset_of_hsize_12() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___hsize_12)); }
	inline int32_t get_hsize_12() const { return ___hsize_12; }
	inline int32_t* get_address_of_hsize_12() { return &___hsize_12; }
	inline void set_hsize_12(int32_t value)
	{
		___hsize_12 = value;
	}

	inline static int32_t get_offset_of_free_ent_13() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___free_ent_13)); }
	inline int32_t get_free_ent_13() const { return ___free_ent_13; }
	inline int32_t* get_address_of_free_ent_13() { return &___free_ent_13; }
	inline void set_free_ent_13(int32_t value)
	{
		___free_ent_13 = value;
	}

	inline static int32_t get_offset_of_clear_flg_14() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___clear_flg_14)); }
	inline bool get_clear_flg_14() const { return ___clear_flg_14; }
	inline bool* get_address_of_clear_flg_14() { return &___clear_flg_14; }
	inline void set_clear_flg_14(bool value)
	{
		___clear_flg_14 = value;
	}

	inline static int32_t get_offset_of_g_init_bits_15() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___g_init_bits_15)); }
	inline int32_t get_g_init_bits_15() const { return ___g_init_bits_15; }
	inline int32_t* get_address_of_g_init_bits_15() { return &___g_init_bits_15; }
	inline void set_g_init_bits_15(int32_t value)
	{
		___g_init_bits_15 = value;
	}

	inline static int32_t get_offset_of_ClearCode_16() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___ClearCode_16)); }
	inline int32_t get_ClearCode_16() const { return ___ClearCode_16; }
	inline int32_t* get_address_of_ClearCode_16() { return &___ClearCode_16; }
	inline void set_ClearCode_16(int32_t value)
	{
		___ClearCode_16 = value;
	}

	inline static int32_t get_offset_of_EOFCode_17() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___EOFCode_17)); }
	inline int32_t get_EOFCode_17() const { return ___EOFCode_17; }
	inline int32_t* get_address_of_EOFCode_17() { return &___EOFCode_17; }
	inline void set_EOFCode_17(int32_t value)
	{
		___EOFCode_17 = value;
	}

	inline static int32_t get_offset_of_cur_accum_18() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___cur_accum_18)); }
	inline int32_t get_cur_accum_18() const { return ___cur_accum_18; }
	inline int32_t* get_address_of_cur_accum_18() { return &___cur_accum_18; }
	inline void set_cur_accum_18(int32_t value)
	{
		___cur_accum_18 = value;
	}

	inline static int32_t get_offset_of_cur_bits_19() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___cur_bits_19)); }
	inline int32_t get_cur_bits_19() const { return ___cur_bits_19; }
	inline int32_t* get_address_of_cur_bits_19() { return &___cur_bits_19; }
	inline void set_cur_bits_19(int32_t value)
	{
		___cur_bits_19 = value;
	}

	inline static int32_t get_offset_of_masks_20() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___masks_20)); }
	inline Int32U5BU5D_t385246372* get_masks_20() const { return ___masks_20; }
	inline Int32U5BU5D_t385246372** get_address_of_masks_20() { return &___masks_20; }
	inline void set_masks_20(Int32U5BU5D_t385246372* value)
	{
		___masks_20 = value;
		Il2CppCodeGenWriteBarrier((&___masks_20), value);
	}

	inline static int32_t get_offset_of_a_count_21() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___a_count_21)); }
	inline int32_t get_a_count_21() const { return ___a_count_21; }
	inline int32_t* get_address_of_a_count_21() { return &___a_count_21; }
	inline void set_a_count_21(int32_t value)
	{
		___a_count_21 = value;
	}

	inline static int32_t get_offset_of_accum_22() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915, ___accum_22)); }
	inline ByteU5BU5D_t4116647657* get_accum_22() const { return ___accum_22; }
	inline ByteU5BU5D_t4116647657** get_address_of_accum_22() { return &___accum_22; }
	inline void set_accum_22(ByteU5BU5D_t4116647657* value)
	{
		___accum_22 = value;
		Il2CppCodeGenWriteBarrier((&___accum_22), value);
	}
};

struct LzwEncoder_t293885915_StaticFields
{
public:
	// System.Int32 Moments.Encoder.LzwEncoder::EOF
	int32_t ___EOF_0;
	// System.Int32 Moments.Encoder.LzwEncoder::BITS
	int32_t ___BITS_4;
	// System.Int32 Moments.Encoder.LzwEncoder::HSIZE
	int32_t ___HSIZE_5;

public:
	inline static int32_t get_offset_of_EOF_0() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915_StaticFields, ___EOF_0)); }
	inline int32_t get_EOF_0() const { return ___EOF_0; }
	inline int32_t* get_address_of_EOF_0() { return &___EOF_0; }
	inline void set_EOF_0(int32_t value)
	{
		___EOF_0 = value;
	}

	inline static int32_t get_offset_of_BITS_4() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915_StaticFields, ___BITS_4)); }
	inline int32_t get_BITS_4() const { return ___BITS_4; }
	inline int32_t* get_address_of_BITS_4() { return &___BITS_4; }
	inline void set_BITS_4(int32_t value)
	{
		___BITS_4 = value;
	}

	inline static int32_t get_offset_of_HSIZE_5() { return static_cast<int32_t>(offsetof(LzwEncoder_t293885915_StaticFields, ___HSIZE_5)); }
	inline int32_t get_HSIZE_5() const { return ___HSIZE_5; }
	inline int32_t* get_address_of_HSIZE_5() { return &___HSIZE_5; }
	inline void set_HSIZE_5(int32_t value)
	{
		___HSIZE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LZWENCODER_T293885915_H
#ifndef NEUQUANT_T3852273683_H
#define NEUQUANT_T3852273683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moments.Encoder.NeuQuant
struct  NeuQuant_t3852273683  : public RuntimeObject
{
public:
	// System.Int32 Moments.Encoder.NeuQuant::alphadec
	int32_t ___alphadec_23;
	// System.Byte[] Moments.Encoder.NeuQuant::thepicture
	ByteU5BU5D_t4116647657* ___thepicture_28;
	// System.Int32 Moments.Encoder.NeuQuant::lengthcount
	int32_t ___lengthcount_29;
	// System.Int32 Moments.Encoder.NeuQuant::samplefac
	int32_t ___samplefac_30;
	// System.Int32[][] Moments.Encoder.NeuQuant::network
	Int32U5BU5DU5BU5D_t3365920845* ___network_31;
	// System.Int32[] Moments.Encoder.NeuQuant::netindex
	Int32U5BU5D_t385246372* ___netindex_32;
	// System.Int32[] Moments.Encoder.NeuQuant::bias
	Int32U5BU5D_t385246372* ___bias_33;
	// System.Int32[] Moments.Encoder.NeuQuant::freq
	Int32U5BU5D_t385246372* ___freq_34;
	// System.Int32[] Moments.Encoder.NeuQuant::radpower
	Int32U5BU5D_t385246372* ___radpower_35;

public:
	inline static int32_t get_offset_of_alphadec_23() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___alphadec_23)); }
	inline int32_t get_alphadec_23() const { return ___alphadec_23; }
	inline int32_t* get_address_of_alphadec_23() { return &___alphadec_23; }
	inline void set_alphadec_23(int32_t value)
	{
		___alphadec_23 = value;
	}

	inline static int32_t get_offset_of_thepicture_28() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___thepicture_28)); }
	inline ByteU5BU5D_t4116647657* get_thepicture_28() const { return ___thepicture_28; }
	inline ByteU5BU5D_t4116647657** get_address_of_thepicture_28() { return &___thepicture_28; }
	inline void set_thepicture_28(ByteU5BU5D_t4116647657* value)
	{
		___thepicture_28 = value;
		Il2CppCodeGenWriteBarrier((&___thepicture_28), value);
	}

	inline static int32_t get_offset_of_lengthcount_29() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___lengthcount_29)); }
	inline int32_t get_lengthcount_29() const { return ___lengthcount_29; }
	inline int32_t* get_address_of_lengthcount_29() { return &___lengthcount_29; }
	inline void set_lengthcount_29(int32_t value)
	{
		___lengthcount_29 = value;
	}

	inline static int32_t get_offset_of_samplefac_30() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___samplefac_30)); }
	inline int32_t get_samplefac_30() const { return ___samplefac_30; }
	inline int32_t* get_address_of_samplefac_30() { return &___samplefac_30; }
	inline void set_samplefac_30(int32_t value)
	{
		___samplefac_30 = value;
	}

	inline static int32_t get_offset_of_network_31() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___network_31)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get_network_31() const { return ___network_31; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of_network_31() { return &___network_31; }
	inline void set_network_31(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		___network_31 = value;
		Il2CppCodeGenWriteBarrier((&___network_31), value);
	}

	inline static int32_t get_offset_of_netindex_32() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___netindex_32)); }
	inline Int32U5BU5D_t385246372* get_netindex_32() const { return ___netindex_32; }
	inline Int32U5BU5D_t385246372** get_address_of_netindex_32() { return &___netindex_32; }
	inline void set_netindex_32(Int32U5BU5D_t385246372* value)
	{
		___netindex_32 = value;
		Il2CppCodeGenWriteBarrier((&___netindex_32), value);
	}

	inline static int32_t get_offset_of_bias_33() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___bias_33)); }
	inline Int32U5BU5D_t385246372* get_bias_33() const { return ___bias_33; }
	inline Int32U5BU5D_t385246372** get_address_of_bias_33() { return &___bias_33; }
	inline void set_bias_33(Int32U5BU5D_t385246372* value)
	{
		___bias_33 = value;
		Il2CppCodeGenWriteBarrier((&___bias_33), value);
	}

	inline static int32_t get_offset_of_freq_34() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___freq_34)); }
	inline Int32U5BU5D_t385246372* get_freq_34() const { return ___freq_34; }
	inline Int32U5BU5D_t385246372** get_address_of_freq_34() { return &___freq_34; }
	inline void set_freq_34(Int32U5BU5D_t385246372* value)
	{
		___freq_34 = value;
		Il2CppCodeGenWriteBarrier((&___freq_34), value);
	}

	inline static int32_t get_offset_of_radpower_35() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683, ___radpower_35)); }
	inline Int32U5BU5D_t385246372* get_radpower_35() const { return ___radpower_35; }
	inline Int32U5BU5D_t385246372** get_address_of_radpower_35() { return &___radpower_35; }
	inline void set_radpower_35(Int32U5BU5D_t385246372* value)
	{
		___radpower_35 = value;
		Il2CppCodeGenWriteBarrier((&___radpower_35), value);
	}
};

struct NeuQuant_t3852273683_StaticFields
{
public:
	// System.Int32 Moments.Encoder.NeuQuant::netsize
	int32_t ___netsize_0;
	// System.Int32 Moments.Encoder.NeuQuant::prime1
	int32_t ___prime1_1;
	// System.Int32 Moments.Encoder.NeuQuant::prime2
	int32_t ___prime2_2;
	// System.Int32 Moments.Encoder.NeuQuant::prime3
	int32_t ___prime3_3;
	// System.Int32 Moments.Encoder.NeuQuant::prime4
	int32_t ___prime4_4;
	// System.Int32 Moments.Encoder.NeuQuant::minpicturebytes
	int32_t ___minpicturebytes_5;
	// System.Int32 Moments.Encoder.NeuQuant::maxnetpos
	int32_t ___maxnetpos_6;
	// System.Int32 Moments.Encoder.NeuQuant::netbiasshift
	int32_t ___netbiasshift_7;
	// System.Int32 Moments.Encoder.NeuQuant::ncycles
	int32_t ___ncycles_8;
	// System.Int32 Moments.Encoder.NeuQuant::intbiasshift
	int32_t ___intbiasshift_9;
	// System.Int32 Moments.Encoder.NeuQuant::intbias
	int32_t ___intbias_10;
	// System.Int32 Moments.Encoder.NeuQuant::gammashift
	int32_t ___gammashift_11;
	// System.Int32 Moments.Encoder.NeuQuant::gamma
	int32_t ___gamma_12;
	// System.Int32 Moments.Encoder.NeuQuant::betashift
	int32_t ___betashift_13;
	// System.Int32 Moments.Encoder.NeuQuant::beta
	int32_t ___beta_14;
	// System.Int32 Moments.Encoder.NeuQuant::betagamma
	int32_t ___betagamma_15;
	// System.Int32 Moments.Encoder.NeuQuant::initrad
	int32_t ___initrad_16;
	// System.Int32 Moments.Encoder.NeuQuant::radiusbiasshift
	int32_t ___radiusbiasshift_17;
	// System.Int32 Moments.Encoder.NeuQuant::radiusbias
	int32_t ___radiusbias_18;
	// System.Int32 Moments.Encoder.NeuQuant::initradius
	int32_t ___initradius_19;
	// System.Int32 Moments.Encoder.NeuQuant::radiusdec
	int32_t ___radiusdec_20;
	// System.Int32 Moments.Encoder.NeuQuant::alphabiasshift
	int32_t ___alphabiasshift_21;
	// System.Int32 Moments.Encoder.NeuQuant::initalpha
	int32_t ___initalpha_22;
	// System.Int32 Moments.Encoder.NeuQuant::radbiasshift
	int32_t ___radbiasshift_24;
	// System.Int32 Moments.Encoder.NeuQuant::radbias
	int32_t ___radbias_25;
	// System.Int32 Moments.Encoder.NeuQuant::alpharadbshift
	int32_t ___alpharadbshift_26;
	// System.Int32 Moments.Encoder.NeuQuant::alpharadbias
	int32_t ___alpharadbias_27;

public:
	inline static int32_t get_offset_of_netsize_0() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___netsize_0)); }
	inline int32_t get_netsize_0() const { return ___netsize_0; }
	inline int32_t* get_address_of_netsize_0() { return &___netsize_0; }
	inline void set_netsize_0(int32_t value)
	{
		___netsize_0 = value;
	}

	inline static int32_t get_offset_of_prime1_1() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___prime1_1)); }
	inline int32_t get_prime1_1() const { return ___prime1_1; }
	inline int32_t* get_address_of_prime1_1() { return &___prime1_1; }
	inline void set_prime1_1(int32_t value)
	{
		___prime1_1 = value;
	}

	inline static int32_t get_offset_of_prime2_2() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___prime2_2)); }
	inline int32_t get_prime2_2() const { return ___prime2_2; }
	inline int32_t* get_address_of_prime2_2() { return &___prime2_2; }
	inline void set_prime2_2(int32_t value)
	{
		___prime2_2 = value;
	}

	inline static int32_t get_offset_of_prime3_3() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___prime3_3)); }
	inline int32_t get_prime3_3() const { return ___prime3_3; }
	inline int32_t* get_address_of_prime3_3() { return &___prime3_3; }
	inline void set_prime3_3(int32_t value)
	{
		___prime3_3 = value;
	}

	inline static int32_t get_offset_of_prime4_4() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___prime4_4)); }
	inline int32_t get_prime4_4() const { return ___prime4_4; }
	inline int32_t* get_address_of_prime4_4() { return &___prime4_4; }
	inline void set_prime4_4(int32_t value)
	{
		___prime4_4 = value;
	}

	inline static int32_t get_offset_of_minpicturebytes_5() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___minpicturebytes_5)); }
	inline int32_t get_minpicturebytes_5() const { return ___minpicturebytes_5; }
	inline int32_t* get_address_of_minpicturebytes_5() { return &___minpicturebytes_5; }
	inline void set_minpicturebytes_5(int32_t value)
	{
		___minpicturebytes_5 = value;
	}

	inline static int32_t get_offset_of_maxnetpos_6() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___maxnetpos_6)); }
	inline int32_t get_maxnetpos_6() const { return ___maxnetpos_6; }
	inline int32_t* get_address_of_maxnetpos_6() { return &___maxnetpos_6; }
	inline void set_maxnetpos_6(int32_t value)
	{
		___maxnetpos_6 = value;
	}

	inline static int32_t get_offset_of_netbiasshift_7() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___netbiasshift_7)); }
	inline int32_t get_netbiasshift_7() const { return ___netbiasshift_7; }
	inline int32_t* get_address_of_netbiasshift_7() { return &___netbiasshift_7; }
	inline void set_netbiasshift_7(int32_t value)
	{
		___netbiasshift_7 = value;
	}

	inline static int32_t get_offset_of_ncycles_8() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___ncycles_8)); }
	inline int32_t get_ncycles_8() const { return ___ncycles_8; }
	inline int32_t* get_address_of_ncycles_8() { return &___ncycles_8; }
	inline void set_ncycles_8(int32_t value)
	{
		___ncycles_8 = value;
	}

	inline static int32_t get_offset_of_intbiasshift_9() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___intbiasshift_9)); }
	inline int32_t get_intbiasshift_9() const { return ___intbiasshift_9; }
	inline int32_t* get_address_of_intbiasshift_9() { return &___intbiasshift_9; }
	inline void set_intbiasshift_9(int32_t value)
	{
		___intbiasshift_9 = value;
	}

	inline static int32_t get_offset_of_intbias_10() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___intbias_10)); }
	inline int32_t get_intbias_10() const { return ___intbias_10; }
	inline int32_t* get_address_of_intbias_10() { return &___intbias_10; }
	inline void set_intbias_10(int32_t value)
	{
		___intbias_10 = value;
	}

	inline static int32_t get_offset_of_gammashift_11() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___gammashift_11)); }
	inline int32_t get_gammashift_11() const { return ___gammashift_11; }
	inline int32_t* get_address_of_gammashift_11() { return &___gammashift_11; }
	inline void set_gammashift_11(int32_t value)
	{
		___gammashift_11 = value;
	}

	inline static int32_t get_offset_of_gamma_12() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___gamma_12)); }
	inline int32_t get_gamma_12() const { return ___gamma_12; }
	inline int32_t* get_address_of_gamma_12() { return &___gamma_12; }
	inline void set_gamma_12(int32_t value)
	{
		___gamma_12 = value;
	}

	inline static int32_t get_offset_of_betashift_13() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___betashift_13)); }
	inline int32_t get_betashift_13() const { return ___betashift_13; }
	inline int32_t* get_address_of_betashift_13() { return &___betashift_13; }
	inline void set_betashift_13(int32_t value)
	{
		___betashift_13 = value;
	}

	inline static int32_t get_offset_of_beta_14() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___beta_14)); }
	inline int32_t get_beta_14() const { return ___beta_14; }
	inline int32_t* get_address_of_beta_14() { return &___beta_14; }
	inline void set_beta_14(int32_t value)
	{
		___beta_14 = value;
	}

	inline static int32_t get_offset_of_betagamma_15() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___betagamma_15)); }
	inline int32_t get_betagamma_15() const { return ___betagamma_15; }
	inline int32_t* get_address_of_betagamma_15() { return &___betagamma_15; }
	inline void set_betagamma_15(int32_t value)
	{
		___betagamma_15 = value;
	}

	inline static int32_t get_offset_of_initrad_16() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___initrad_16)); }
	inline int32_t get_initrad_16() const { return ___initrad_16; }
	inline int32_t* get_address_of_initrad_16() { return &___initrad_16; }
	inline void set_initrad_16(int32_t value)
	{
		___initrad_16 = value;
	}

	inline static int32_t get_offset_of_radiusbiasshift_17() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___radiusbiasshift_17)); }
	inline int32_t get_radiusbiasshift_17() const { return ___radiusbiasshift_17; }
	inline int32_t* get_address_of_radiusbiasshift_17() { return &___radiusbiasshift_17; }
	inline void set_radiusbiasshift_17(int32_t value)
	{
		___radiusbiasshift_17 = value;
	}

	inline static int32_t get_offset_of_radiusbias_18() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___radiusbias_18)); }
	inline int32_t get_radiusbias_18() const { return ___radiusbias_18; }
	inline int32_t* get_address_of_radiusbias_18() { return &___radiusbias_18; }
	inline void set_radiusbias_18(int32_t value)
	{
		___radiusbias_18 = value;
	}

	inline static int32_t get_offset_of_initradius_19() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___initradius_19)); }
	inline int32_t get_initradius_19() const { return ___initradius_19; }
	inline int32_t* get_address_of_initradius_19() { return &___initradius_19; }
	inline void set_initradius_19(int32_t value)
	{
		___initradius_19 = value;
	}

	inline static int32_t get_offset_of_radiusdec_20() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___radiusdec_20)); }
	inline int32_t get_radiusdec_20() const { return ___radiusdec_20; }
	inline int32_t* get_address_of_radiusdec_20() { return &___radiusdec_20; }
	inline void set_radiusdec_20(int32_t value)
	{
		___radiusdec_20 = value;
	}

	inline static int32_t get_offset_of_alphabiasshift_21() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___alphabiasshift_21)); }
	inline int32_t get_alphabiasshift_21() const { return ___alphabiasshift_21; }
	inline int32_t* get_address_of_alphabiasshift_21() { return &___alphabiasshift_21; }
	inline void set_alphabiasshift_21(int32_t value)
	{
		___alphabiasshift_21 = value;
	}

	inline static int32_t get_offset_of_initalpha_22() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___initalpha_22)); }
	inline int32_t get_initalpha_22() const { return ___initalpha_22; }
	inline int32_t* get_address_of_initalpha_22() { return &___initalpha_22; }
	inline void set_initalpha_22(int32_t value)
	{
		___initalpha_22 = value;
	}

	inline static int32_t get_offset_of_radbiasshift_24() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___radbiasshift_24)); }
	inline int32_t get_radbiasshift_24() const { return ___radbiasshift_24; }
	inline int32_t* get_address_of_radbiasshift_24() { return &___radbiasshift_24; }
	inline void set_radbiasshift_24(int32_t value)
	{
		___radbiasshift_24 = value;
	}

	inline static int32_t get_offset_of_radbias_25() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___radbias_25)); }
	inline int32_t get_radbias_25() const { return ___radbias_25; }
	inline int32_t* get_address_of_radbias_25() { return &___radbias_25; }
	inline void set_radbias_25(int32_t value)
	{
		___radbias_25 = value;
	}

	inline static int32_t get_offset_of_alpharadbshift_26() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___alpharadbshift_26)); }
	inline int32_t get_alpharadbshift_26() const { return ___alpharadbshift_26; }
	inline int32_t* get_address_of_alpharadbshift_26() { return &___alpharadbshift_26; }
	inline void set_alpharadbshift_26(int32_t value)
	{
		___alpharadbshift_26 = value;
	}

	inline static int32_t get_offset_of_alpharadbias_27() { return static_cast<int32_t>(offsetof(NeuQuant_t3852273683_StaticFields, ___alpharadbias_27)); }
	inline int32_t get_alpharadbias_27() const { return ___alpharadbias_27; }
	inline int32_t* get_address_of_alpharadbias_27() { return &___alpharadbias_27; }
	inline void set_alpharadbias_27(int32_t value)
	{
		___alpharadbias_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEUQUANT_T3852273683_H
#ifndef WORKER_T3267557706_H
#define WORKER_T3267557706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moments.Worker
struct  Worker_t3267557706  : public RuntimeObject
{
public:
	// System.Threading.Thread Moments.Worker::m_Thread
	Thread_t2300836069 * ___m_Thread_0;
	// System.Int32 Moments.Worker::m_Id
	int32_t ___m_Id_1;
	// System.Collections.Generic.List`1<Moments.Encoder.GifFrame> Moments.Worker::m_Frames
	List_1_t93337683 * ___m_Frames_2;
	// Moments.Encoder.GifEncoder Moments.Worker::m_Encoder
	GifEncoder_t60958606 * ___m_Encoder_3;
	// System.String Moments.Worker::m_FilePath
	String_t* ___m_FilePath_4;
	// System.Action`2<System.Int32,System.String> Moments.Worker::m_OnFileSaved
	Action_2_t3073627706 * ___m_OnFileSaved_5;
	// System.Action`2<System.Int32,System.Single> Moments.Worker::m_OnFileSaveProgress
	Action_2_t2623443791 * ___m_OnFileSaveProgress_6;

public:
	inline static int32_t get_offset_of_m_Thread_0() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_Thread_0)); }
	inline Thread_t2300836069 * get_m_Thread_0() const { return ___m_Thread_0; }
	inline Thread_t2300836069 ** get_address_of_m_Thread_0() { return &___m_Thread_0; }
	inline void set_m_Thread_0(Thread_t2300836069 * value)
	{
		___m_Thread_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Thread_0), value);
	}

	inline static int32_t get_offset_of_m_Id_1() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_Id_1)); }
	inline int32_t get_m_Id_1() const { return ___m_Id_1; }
	inline int32_t* get_address_of_m_Id_1() { return &___m_Id_1; }
	inline void set_m_Id_1(int32_t value)
	{
		___m_Id_1 = value;
	}

	inline static int32_t get_offset_of_m_Frames_2() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_Frames_2)); }
	inline List_1_t93337683 * get_m_Frames_2() const { return ___m_Frames_2; }
	inline List_1_t93337683 ** get_address_of_m_Frames_2() { return &___m_Frames_2; }
	inline void set_m_Frames_2(List_1_t93337683 * value)
	{
		___m_Frames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Frames_2), value);
	}

	inline static int32_t get_offset_of_m_Encoder_3() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_Encoder_3)); }
	inline GifEncoder_t60958606 * get_m_Encoder_3() const { return ___m_Encoder_3; }
	inline GifEncoder_t60958606 ** get_address_of_m_Encoder_3() { return &___m_Encoder_3; }
	inline void set_m_Encoder_3(GifEncoder_t60958606 * value)
	{
		___m_Encoder_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Encoder_3), value);
	}

	inline static int32_t get_offset_of_m_FilePath_4() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_FilePath_4)); }
	inline String_t* get_m_FilePath_4() const { return ___m_FilePath_4; }
	inline String_t** get_address_of_m_FilePath_4() { return &___m_FilePath_4; }
	inline void set_m_FilePath_4(String_t* value)
	{
		___m_FilePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_FilePath_4), value);
	}

	inline static int32_t get_offset_of_m_OnFileSaved_5() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_OnFileSaved_5)); }
	inline Action_2_t3073627706 * get_m_OnFileSaved_5() const { return ___m_OnFileSaved_5; }
	inline Action_2_t3073627706 ** get_address_of_m_OnFileSaved_5() { return &___m_OnFileSaved_5; }
	inline void set_m_OnFileSaved_5(Action_2_t3073627706 * value)
	{
		___m_OnFileSaved_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFileSaved_5), value);
	}

	inline static int32_t get_offset_of_m_OnFileSaveProgress_6() { return static_cast<int32_t>(offsetof(Worker_t3267557706, ___m_OnFileSaveProgress_6)); }
	inline Action_2_t2623443791 * get_m_OnFileSaveProgress_6() const { return ___m_OnFileSaveProgress_6; }
	inline Action_2_t2623443791 ** get_address_of_m_OnFileSaveProgress_6() { return &___m_OnFileSaveProgress_6; }
	inline void set_m_OnFileSaveProgress_6(Action_2_t2623443791 * value)
	{
		___m_OnFileSaveProgress_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFileSaveProgress_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKER_T3267557706_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GIPHYUPLOADPARAMS_T2791732282_H
#define GIPHYUPLOADPARAMS_T2791732282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GiphyUploadParams
struct  GiphyUploadParams_t2791732282 
{
public:
	// System.String EasyMobile.GiphyUploadParams::localImagePath
	String_t* ___localImagePath_0;
	// System.String EasyMobile.GiphyUploadParams::sourceImageUrl
	String_t* ___sourceImageUrl_1;
	// System.String EasyMobile.GiphyUploadParams::tags
	String_t* ___tags_2;
	// System.String EasyMobile.GiphyUploadParams::sourcePostUrl
	String_t* ___sourcePostUrl_3;
	// System.Boolean EasyMobile.GiphyUploadParams::isHidden
	bool ___isHidden_4;

public:
	inline static int32_t get_offset_of_localImagePath_0() { return static_cast<int32_t>(offsetof(GiphyUploadParams_t2791732282, ___localImagePath_0)); }
	inline String_t* get_localImagePath_0() const { return ___localImagePath_0; }
	inline String_t** get_address_of_localImagePath_0() { return &___localImagePath_0; }
	inline void set_localImagePath_0(String_t* value)
	{
		___localImagePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___localImagePath_0), value);
	}

	inline static int32_t get_offset_of_sourceImageUrl_1() { return static_cast<int32_t>(offsetof(GiphyUploadParams_t2791732282, ___sourceImageUrl_1)); }
	inline String_t* get_sourceImageUrl_1() const { return ___sourceImageUrl_1; }
	inline String_t** get_address_of_sourceImageUrl_1() { return &___sourceImageUrl_1; }
	inline void set_sourceImageUrl_1(String_t* value)
	{
		___sourceImageUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceImageUrl_1), value);
	}

	inline static int32_t get_offset_of_tags_2() { return static_cast<int32_t>(offsetof(GiphyUploadParams_t2791732282, ___tags_2)); }
	inline String_t* get_tags_2() const { return ___tags_2; }
	inline String_t** get_address_of_tags_2() { return &___tags_2; }
	inline void set_tags_2(String_t* value)
	{
		___tags_2 = value;
		Il2CppCodeGenWriteBarrier((&___tags_2), value);
	}

	inline static int32_t get_offset_of_sourcePostUrl_3() { return static_cast<int32_t>(offsetof(GiphyUploadParams_t2791732282, ___sourcePostUrl_3)); }
	inline String_t* get_sourcePostUrl_3() const { return ___sourcePostUrl_3; }
	inline String_t** get_address_of_sourcePostUrl_3() { return &___sourcePostUrl_3; }
	inline void set_sourcePostUrl_3(String_t* value)
	{
		___sourcePostUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourcePostUrl_3), value);
	}

	inline static int32_t get_offset_of_isHidden_4() { return static_cast<int32_t>(offsetof(GiphyUploadParams_t2791732282, ___isHidden_4)); }
	inline bool get_isHidden_4() const { return ___isHidden_4; }
	inline bool* get_address_of_isHidden_4() { return &___isHidden_4; }
	inline void set_isHidden_4(bool value)
	{
		___isHidden_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.GiphyUploadParams
struct GiphyUploadParams_t2791732282_marshaled_pinvoke
{
	char* ___localImagePath_0;
	char* ___sourceImageUrl_1;
	char* ___tags_2;
	char* ___sourcePostUrl_3;
	int32_t ___isHidden_4;
};
// Native definition for COM marshalling of EasyMobile.GiphyUploadParams
struct GiphyUploadParams_t2791732282_marshaled_com
{
	Il2CppChar* ___localImagePath_0;
	Il2CppChar* ___sourceImageUrl_1;
	Il2CppChar* ___tags_2;
	Il2CppChar* ___sourcePostUrl_3;
	int32_t ___isHidden_4;
};
#endif // GIPHYUPLOADPARAMS_T2791732282_H
#ifndef IOSDATECOMPONENTS_T1856826077_H
#define IOSDATECOMPONENTS_T1856826077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSDateComponents
struct  iOSDateComponents_t1856826077 
{
public:
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSDateComponents::year
	int32_t ___year_0;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSDateComponents::month
	int32_t ___month_1;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSDateComponents::day
	int32_t ___day_2;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSDateComponents::hour
	int32_t ___hour_3;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSDateComponents::minute
	int32_t ___minute_4;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSDateComponents::second
	int32_t ___second_5;

public:
	inline static int32_t get_offset_of_year_0() { return static_cast<int32_t>(offsetof(iOSDateComponents_t1856826077, ___year_0)); }
	inline int32_t get_year_0() const { return ___year_0; }
	inline int32_t* get_address_of_year_0() { return &___year_0; }
	inline void set_year_0(int32_t value)
	{
		___year_0 = value;
	}

	inline static int32_t get_offset_of_month_1() { return static_cast<int32_t>(offsetof(iOSDateComponents_t1856826077, ___month_1)); }
	inline int32_t get_month_1() const { return ___month_1; }
	inline int32_t* get_address_of_month_1() { return &___month_1; }
	inline void set_month_1(int32_t value)
	{
		___month_1 = value;
	}

	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(iOSDateComponents_t1856826077, ___day_2)); }
	inline int32_t get_day_2() const { return ___day_2; }
	inline int32_t* get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(int32_t value)
	{
		___day_2 = value;
	}

	inline static int32_t get_offset_of_hour_3() { return static_cast<int32_t>(offsetof(iOSDateComponents_t1856826077, ___hour_3)); }
	inline int32_t get_hour_3() const { return ___hour_3; }
	inline int32_t* get_address_of_hour_3() { return &___hour_3; }
	inline void set_hour_3(int32_t value)
	{
		___hour_3 = value;
	}

	inline static int32_t get_offset_of_minute_4() { return static_cast<int32_t>(offsetof(iOSDateComponents_t1856826077, ___minute_4)); }
	inline int32_t get_minute_4() const { return ___minute_4; }
	inline int32_t* get_address_of_minute_4() { return &___minute_4; }
	inline void set_minute_4(int32_t value)
	{
		___minute_4 = value;
	}

	inline static int32_t get_offset_of_second_5() { return static_cast<int32_t>(offsetof(iOSDateComponents_t1856826077, ___second_5)); }
	inline int32_t get_second_5() const { return ___second_5; }
	inline int32_t* get_address_of_second_5() { return &___second_5; }
	inline void set_second_5(int32_t value)
	{
		___second_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSDATECOMPONENTS_T1856826077_H
#ifndef IOSNOTIFICATIONACTION_T1537218362_H
#define IOSNOTIFICATIONACTION_T1537218362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationAction
struct  iOSNotificationAction_t1537218362 
{
public:
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationAction::id
	String_t* ___id_0;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationAction::title
	String_t* ___title_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(iOSNotificationAction_t1537218362, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(iOSNotificationAction_t1537218362, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationAction
struct iOSNotificationAction_t1537218362_marshaled_pinvoke
{
	char* ___id_0;
	char* ___title_1;
};
// Native definition for COM marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationAction
struct iOSNotificationAction_t1537218362_marshaled_com
{
	Il2CppChar* ___id_0;
	Il2CppChar* ___title_1;
};
#endif // IOSNOTIFICATIONACTION_T1537218362_H
#ifndef IOSNOTIFICATIONAUTHOPTIONS_T3273998710_H
#define IOSNOTIFICATIONAUTHOPTIONS_T3273998710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationAuthOptions
struct  iOSNotificationAuthOptions_t3273998710 
{
public:
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSNotificationAuthOptions::isAlertAllowed
	int32_t ___isAlertAllowed_0;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSNotificationAuthOptions::isBadgeAllowed
	int32_t ___isBadgeAllowed_1;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSNotificationAuthOptions::isSoundAllowed
	int32_t ___isSoundAllowed_2;

public:
	inline static int32_t get_offset_of_isAlertAllowed_0() { return static_cast<int32_t>(offsetof(iOSNotificationAuthOptions_t3273998710, ___isAlertAllowed_0)); }
	inline int32_t get_isAlertAllowed_0() const { return ___isAlertAllowed_0; }
	inline int32_t* get_address_of_isAlertAllowed_0() { return &___isAlertAllowed_0; }
	inline void set_isAlertAllowed_0(int32_t value)
	{
		___isAlertAllowed_0 = value;
	}

	inline static int32_t get_offset_of_isBadgeAllowed_1() { return static_cast<int32_t>(offsetof(iOSNotificationAuthOptions_t3273998710, ___isBadgeAllowed_1)); }
	inline int32_t get_isBadgeAllowed_1() const { return ___isBadgeAllowed_1; }
	inline int32_t* get_address_of_isBadgeAllowed_1() { return &___isBadgeAllowed_1; }
	inline void set_isBadgeAllowed_1(int32_t value)
	{
		___isBadgeAllowed_1 = value;
	}

	inline static int32_t get_offset_of_isSoundAllowed_2() { return static_cast<int32_t>(offsetof(iOSNotificationAuthOptions_t3273998710, ___isSoundAllowed_2)); }
	inline int32_t get_isSoundAllowed_2() const { return ___isSoundAllowed_2; }
	inline int32_t* get_address_of_isSoundAllowed_2() { return &___isSoundAllowed_2; }
	inline void set_isSoundAllowed_2(int32_t value)
	{
		___isSoundAllowed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNOTIFICATIONAUTHOPTIONS_T3273998710_H
#ifndef IOSNOTIFICATIONCATEGORY_T4187379170_H
#define IOSNOTIFICATIONCATEGORY_T4187379170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationCategory
struct  iOSNotificationCategory_t4187379170 
{
public:
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationCategory::id
	String_t* ___id_0;
	// EasyMobile.Internal.Notifications.iOS.iOSNotificationAction[] EasyMobile.Internal.Notifications.iOS.iOSNotificationCategory::actions
	iOSNotificationActionU5BU5D_t3075117727* ___actions_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(iOSNotificationCategory_t4187379170, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_actions_1() { return static_cast<int32_t>(offsetof(iOSNotificationCategory_t4187379170, ___actions_1)); }
	inline iOSNotificationActionU5BU5D_t3075117727* get_actions_1() const { return ___actions_1; }
	inline iOSNotificationActionU5BU5D_t3075117727** get_address_of_actions_1() { return &___actions_1; }
	inline void set_actions_1(iOSNotificationActionU5BU5D_t3075117727* value)
	{
		___actions_1 = value;
		Il2CppCodeGenWriteBarrier((&___actions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationCategory
struct iOSNotificationCategory_t4187379170_marshaled_pinvoke
{
	char* ___id_0;
	iOSNotificationAction_t1537218362_marshaled_pinvoke* ___actions_1;
};
// Native definition for COM marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationCategory
struct iOSNotificationCategory_t4187379170_marshaled_com
{
	Il2CppChar* ___id_0;
	iOSNotificationAction_t1537218362_marshaled_com* ___actions_1;
};
#endif // IOSNOTIFICATIONCATEGORY_T4187379170_H
#ifndef IOSNOTIFICATIONCONTENT_T1040386259_H
#define IOSNOTIFICATIONCONTENT_T1040386259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationContent
struct  iOSNotificationContent_t1040386259 
{
public:
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::title
	String_t* ___title_0;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::subtitle
	String_t* ___subtitle_1;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::body
	String_t* ___body_2;
	// System.Int32 EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::badge
	int32_t ___badge_3;
	// System.Boolean EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::enableSound
	bool ___enableSound_4;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::soundName
	String_t* ___soundName_5;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::userInfoJson
	String_t* ___userInfoJson_6;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationContent::categoryId
	String_t* ___categoryId_7;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_subtitle_1() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___subtitle_1)); }
	inline String_t* get_subtitle_1() const { return ___subtitle_1; }
	inline String_t** get_address_of_subtitle_1() { return &___subtitle_1; }
	inline void set_subtitle_1(String_t* value)
	{
		___subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___subtitle_1), value);
	}

	inline static int32_t get_offset_of_body_2() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___body_2)); }
	inline String_t* get_body_2() const { return ___body_2; }
	inline String_t** get_address_of_body_2() { return &___body_2; }
	inline void set_body_2(String_t* value)
	{
		___body_2 = value;
		Il2CppCodeGenWriteBarrier((&___body_2), value);
	}

	inline static int32_t get_offset_of_badge_3() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___badge_3)); }
	inline int32_t get_badge_3() const { return ___badge_3; }
	inline int32_t* get_address_of_badge_3() { return &___badge_3; }
	inline void set_badge_3(int32_t value)
	{
		___badge_3 = value;
	}

	inline static int32_t get_offset_of_enableSound_4() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___enableSound_4)); }
	inline bool get_enableSound_4() const { return ___enableSound_4; }
	inline bool* get_address_of_enableSound_4() { return &___enableSound_4; }
	inline void set_enableSound_4(bool value)
	{
		___enableSound_4 = value;
	}

	inline static int32_t get_offset_of_soundName_5() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___soundName_5)); }
	inline String_t* get_soundName_5() const { return ___soundName_5; }
	inline String_t** get_address_of_soundName_5() { return &___soundName_5; }
	inline void set_soundName_5(String_t* value)
	{
		___soundName_5 = value;
		Il2CppCodeGenWriteBarrier((&___soundName_5), value);
	}

	inline static int32_t get_offset_of_userInfoJson_6() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___userInfoJson_6)); }
	inline String_t* get_userInfoJson_6() const { return ___userInfoJson_6; }
	inline String_t** get_address_of_userInfoJson_6() { return &___userInfoJson_6; }
	inline void set_userInfoJson_6(String_t* value)
	{
		___userInfoJson_6 = value;
		Il2CppCodeGenWriteBarrier((&___userInfoJson_6), value);
	}

	inline static int32_t get_offset_of_categoryId_7() { return static_cast<int32_t>(offsetof(iOSNotificationContent_t1040386259, ___categoryId_7)); }
	inline String_t* get_categoryId_7() const { return ___categoryId_7; }
	inline String_t** get_address_of_categoryId_7() { return &___categoryId_7; }
	inline void set_categoryId_7(String_t* value)
	{
		___categoryId_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryId_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationContent
struct iOSNotificationContent_t1040386259_marshaled_pinvoke
{
	char* ___title_0;
	char* ___subtitle_1;
	char* ___body_2;
	int32_t ___badge_3;
	int32_t ___enableSound_4;
	char* ___soundName_5;
	char* ___userInfoJson_6;
	char* ___categoryId_7;
};
// Native definition for COM marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationContent
struct iOSNotificationContent_t1040386259_marshaled_com
{
	Il2CppChar* ___title_0;
	Il2CppChar* ___subtitle_1;
	Il2CppChar* ___body_2;
	int32_t ___badge_3;
	int32_t ___enableSound_4;
	Il2CppChar* ___soundName_5;
	Il2CppChar* ___userInfoJson_6;
	Il2CppChar* ___categoryId_7;
};
#endif // IOSNOTIFICATIONCONTENT_T1040386259_H
#ifndef IOSNOTIFICATIONLISTENERINFO_T996772639_H
#define IOSNOTIFICATIONLISTENERINFO_T996772639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo
struct  iOSNotificationListenerInfo_t996772639 
{
public:
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo::name
	String_t* ___name_0;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo::backgroundNotificationMethod
	String_t* ___backgroundNotificationMethod_1;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo::foregroundNotificationMethod
	String_t* ___foregroundNotificationMethod_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(iOSNotificationListenerInfo_t996772639, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_backgroundNotificationMethod_1() { return static_cast<int32_t>(offsetof(iOSNotificationListenerInfo_t996772639, ___backgroundNotificationMethod_1)); }
	inline String_t* get_backgroundNotificationMethod_1() const { return ___backgroundNotificationMethod_1; }
	inline String_t** get_address_of_backgroundNotificationMethod_1() { return &___backgroundNotificationMethod_1; }
	inline void set_backgroundNotificationMethod_1(String_t* value)
	{
		___backgroundNotificationMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundNotificationMethod_1), value);
	}

	inline static int32_t get_offset_of_foregroundNotificationMethod_2() { return static_cast<int32_t>(offsetof(iOSNotificationListenerInfo_t996772639, ___foregroundNotificationMethod_2)); }
	inline String_t* get_foregroundNotificationMethod_2() const { return ___foregroundNotificationMethod_2; }
	inline String_t** get_address_of_foregroundNotificationMethod_2() { return &___foregroundNotificationMethod_2; }
	inline void set_foregroundNotificationMethod_2(String_t* value)
	{
		___foregroundNotificationMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&___foregroundNotificationMethod_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo
struct iOSNotificationListenerInfo_t996772639_marshaled_pinvoke
{
	char* ___name_0;
	char* ___backgroundNotificationMethod_1;
	char* ___foregroundNotificationMethod_2;
};
// Native definition for COM marshalling of EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo
struct iOSNotificationListenerInfo_t996772639_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___backgroundNotificationMethod_1;
	Il2CppChar* ___foregroundNotificationMethod_2;
};
#endif // IOSNOTIFICATIONLISTENERINFO_T996772639_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef CLIPPLAYERSCALEMODE_T2357387684_H
#define CLIPPLAYERSCALEMODE_T2357387684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ClipPlayerScaleMode
struct  ClipPlayerScaleMode_t2357387684 
{
public:
	// System.Int32 EasyMobile.ClipPlayerScaleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClipPlayerScaleMode_t2357387684, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPLAYERSCALEMODE_T2357387684_H
#ifndef IAPANDROIDSTORE_T3652621280_H
#define IAPANDROIDSTORE_T3652621280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IAPAndroidStore
struct  IAPAndroidStore_t3652621280 
{
public:
	// System.Int32 EasyMobile.IAPAndroidStore::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IAPAndroidStore_t3652621280, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPANDROIDSTORE_T3652621280_H
#ifndef IAPPRODUCTTYPE_T4238212781_H
#define IAPPRODUCTTYPE_T4238212781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IAPProductType
struct  IAPProductType_t4238212781 
{
public:
	// System.Int32 EasyMobile.IAPProductType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IAPProductType_t4238212781, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPPRODUCTTYPE_T4238212781_H
#ifndef IAPSTORE_T973483246_H
#define IAPSTORE_T973483246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IAPStore
struct  IAPStore_t973483246 
{
public:
	// System.Int32 EasyMobile.IAPStore::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IAPStore_t973483246, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPSTORE_T973483246_H
#ifndef IOSCONFLICTRESOLUTIONSTRATEGY_T1704602082_H
#define IOSCONFLICTRESOLUTIONSTRATEGY_T1704602082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSConflictResolutionStrategy
struct  iOSConflictResolutionStrategy_t1704602082 
{
public:
	// System.Int32 EasyMobile.Internal.GameServices.iOSConflictResolutionStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(iOSConflictResolutionStrategy_t1704602082, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCONFLICTRESOLUTIONSTRATEGY_T1704602082_H
#ifndef RECORDERSTATE_T1736470218_H
#define RECORDERSTATE_T1736470218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Recorder/RecorderState
struct  RecorderState_t1736470218 
{
public:
	// System.Int32 EasyMobile.Recorder/RecorderState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecorderState_t1736470218, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDERSTATE_T1736470218_H
#ifndef MINATTRIBUTE_T1751344115_H
#define MINATTRIBUTE_T1751344115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moments.MinAttribute
struct  MinAttribute_t1751344115  : public PropertyAttribute_t3677895545
{
public:
	// System.Single Moments.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t1751344115, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T1751344115_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef NULLABLE_1_T2762948341_H
#define NULLABLE_1_T2762948341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<EasyMobile.Internal.Notifications.iOS.iOSNotificationContent>
struct  Nullable_1_t2762948341 
{
public:
	// T System.Nullable`1::value
	iOSNotificationContent_t1040386259  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2762948341, ___value_0)); }
	inline iOSNotificationContent_t1040386259  get_value_0() const { return ___value_0; }
	inline iOSNotificationContent_t1040386259 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(iOSNotificationContent_t1040386259  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2762948341, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2762948341_H
#ifndef HANDLEREF_T3745784362_H
#define HANDLEREF_T3745784362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784362 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::m_wrapper
	RuntimeObject * ___m_wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::m_handle
	intptr_t ___m_handle_1;

public:
	inline static int32_t get_offset_of_m_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___m_wrapper_0)); }
	inline RuntimeObject * get_m_wrapper_0() const { return ___m_wrapper_0; }
	inline RuntimeObject ** get_address_of_m_wrapper_0() { return &___m_wrapper_0; }
	inline void set_m_wrapper_0(RuntimeObject * value)
	{
		___m_wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_wrapper_0), value);
	}

	inline static int32_t get_offset_of_m_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___m_handle_1)); }
	inline intptr_t get_m_handle_1() const { return ___m_handle_1; }
	inline intptr_t* get_address_of_m_handle_1() { return &___m_handle_1; }
	inline void set_m_handle_1(intptr_t value)
	{
		___m_handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEREF_T3745784362_H
#ifndef THREADPRIORITY_T1551740086_H
#define THREADPRIORITY_T1551740086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadPriority
struct  ThreadPriority_t1551740086 
{
public:
	// System.Int32 System.Threading.ThreadPriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ThreadPriority_t1551740086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_T1551740086_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef FIREBASEMESSAGE_T1021966236_H
#define FIREBASEMESSAGE_T1021966236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.FirebaseMessage
struct  FirebaseMessage_t1021966236  : public RuntimeObject
{
public:
	// System.String EasyMobile.FirebaseMessage::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_0;
	// System.String EasyMobile.FirebaseMessage::<Priority>k__BackingField
	String_t* ___U3CPriorityU3Ek__BackingField_1;
	// System.String EasyMobile.FirebaseMessage::<MessageType>k__BackingField
	String_t* ___U3CMessageTypeU3Ek__BackingField_2;
	// System.String EasyMobile.FirebaseMessage::<MessageId>k__BackingField
	String_t* ___U3CMessageIdU3Ek__BackingField_3;
	// System.String EasyMobile.FirebaseMessage::<RawData>k__BackingField
	String_t* ___U3CRawDataU3Ek__BackingField_4;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> EasyMobile.FirebaseMessage::<Data>k__BackingField
	RuntimeObject* ___U3CDataU3Ek__BackingField_5;
	// System.String EasyMobile.FirebaseMessage::<CollapseKey>k__BackingField
	String_t* ___U3CCollapseKeyU3Ek__BackingField_6;
	// System.String EasyMobile.FirebaseMessage::<To>k__BackingField
	String_t* ___U3CToU3Ek__BackingField_7;
	// System.String EasyMobile.FirebaseMessage::<From>k__BackingField
	String_t* ___U3CFromU3Ek__BackingField_8;
	// System.Uri EasyMobile.FirebaseMessage::<Link>k__BackingField
	Uri_t100236324 * ___U3CLinkU3Ek__BackingField_9;
	// EasyMobile.FirebaseNotification EasyMobile.FirebaseMessage::<Notification>k__BackingField
	FirebaseNotification_t2232694309 * ___U3CNotificationU3Ek__BackingField_10;
	// System.TimeSpan EasyMobile.FirebaseMessage::<TimeToLive>k__BackingField
	TimeSpan_t881159249  ___U3CTimeToLiveU3Ek__BackingField_11;
	// System.String EasyMobile.FirebaseMessage::<ErrorDescription>k__BackingField
	String_t* ___U3CErrorDescriptionU3Ek__BackingField_12;
	// System.Boolean EasyMobile.FirebaseMessage::<NotificationOpened>k__BackingField
	bool ___U3CNotificationOpenedU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CErrorU3Ek__BackingField_0)); }
	inline String_t* get_U3CErrorU3Ek__BackingField_0() const { return ___U3CErrorU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CErrorU3Ek__BackingField_0() { return &___U3CErrorU3Ek__BackingField_0; }
	inline void set_U3CErrorU3Ek__BackingField_0(String_t* value)
	{
		___U3CErrorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CPriorityU3Ek__BackingField_1)); }
	inline String_t* get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(String_t* value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPriorityU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMessageTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CMessageTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageTypeU3Ek__BackingField_2() const { return ___U3CMessageTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageTypeU3Ek__BackingField_2() { return &___U3CMessageTypeU3Ek__BackingField_2; }
	inline void set_U3CMessageTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageTypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMessageIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CMessageIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CMessageIdU3Ek__BackingField_3() const { return ___U3CMessageIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CMessageIdU3Ek__BackingField_3() { return &___U3CMessageIdU3Ek__BackingField_3; }
	inline void set_U3CMessageIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CMessageIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageIdU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CRawDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CRawDataU3Ek__BackingField_4)); }
	inline String_t* get_U3CRawDataU3Ek__BackingField_4() const { return ___U3CRawDataU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CRawDataU3Ek__BackingField_4() { return &___U3CRawDataU3Ek__BackingField_4; }
	inline void set_U3CRawDataU3Ek__BackingField_4(String_t* value)
	{
		___U3CRawDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CDataU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CDataU3Ek__BackingField_5() const { return ___U3CDataU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CDataU3Ek__BackingField_5() { return &___U3CDataU3Ek__BackingField_5; }
	inline void set_U3CDataU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCollapseKeyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CCollapseKeyU3Ek__BackingField_6)); }
	inline String_t* get_U3CCollapseKeyU3Ek__BackingField_6() const { return ___U3CCollapseKeyU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CCollapseKeyU3Ek__BackingField_6() { return &___U3CCollapseKeyU3Ek__BackingField_6; }
	inline void set_U3CCollapseKeyU3Ek__BackingField_6(String_t* value)
	{
		___U3CCollapseKeyU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollapseKeyU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CToU3Ek__BackingField_7)); }
	inline String_t* get_U3CToU3Ek__BackingField_7() const { return ___U3CToU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CToU3Ek__BackingField_7() { return &___U3CToU3Ek__BackingField_7; }
	inline void set_U3CToU3Ek__BackingField_7(String_t* value)
	{
		___U3CToU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CFromU3Ek__BackingField_8)); }
	inline String_t* get_U3CFromU3Ek__BackingField_8() const { return ___U3CFromU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CFromU3Ek__BackingField_8() { return &___U3CFromU3Ek__BackingField_8; }
	inline void set_U3CFromU3Ek__BackingField_8(String_t* value)
	{
		___U3CFromU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFromU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CLinkU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CLinkU3Ek__BackingField_9)); }
	inline Uri_t100236324 * get_U3CLinkU3Ek__BackingField_9() const { return ___U3CLinkU3Ek__BackingField_9; }
	inline Uri_t100236324 ** get_address_of_U3CLinkU3Ek__BackingField_9() { return &___U3CLinkU3Ek__BackingField_9; }
	inline void set_U3CLinkU3Ek__BackingField_9(Uri_t100236324 * value)
	{
		___U3CLinkU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLinkU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CNotificationU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CNotificationU3Ek__BackingField_10)); }
	inline FirebaseNotification_t2232694309 * get_U3CNotificationU3Ek__BackingField_10() const { return ___U3CNotificationU3Ek__BackingField_10; }
	inline FirebaseNotification_t2232694309 ** get_address_of_U3CNotificationU3Ek__BackingField_10() { return &___U3CNotificationU3Ek__BackingField_10; }
	inline void set_U3CNotificationU3Ek__BackingField_10(FirebaseNotification_t2232694309 * value)
	{
		___U3CNotificationU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNotificationU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTimeToLiveU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CTimeToLiveU3Ek__BackingField_11)); }
	inline TimeSpan_t881159249  get_U3CTimeToLiveU3Ek__BackingField_11() const { return ___U3CTimeToLiveU3Ek__BackingField_11; }
	inline TimeSpan_t881159249 * get_address_of_U3CTimeToLiveU3Ek__BackingField_11() { return &___U3CTimeToLiveU3Ek__BackingField_11; }
	inline void set_U3CTimeToLiveU3Ek__BackingField_11(TimeSpan_t881159249  value)
	{
		___U3CTimeToLiveU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CErrorDescriptionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CErrorDescriptionU3Ek__BackingField_12)); }
	inline String_t* get_U3CErrorDescriptionU3Ek__BackingField_12() const { return ___U3CErrorDescriptionU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CErrorDescriptionU3Ek__BackingField_12() { return &___U3CErrorDescriptionU3Ek__BackingField_12; }
	inline void set_U3CErrorDescriptionU3Ek__BackingField_12(String_t* value)
	{
		___U3CErrorDescriptionU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorDescriptionU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CNotificationOpenedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FirebaseMessage_t1021966236, ___U3CNotificationOpenedU3Ek__BackingField_13)); }
	inline bool get_U3CNotificationOpenedU3Ek__BackingField_13() const { return ___U3CNotificationOpenedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CNotificationOpenedU3Ek__BackingField_13() { return &___U3CNotificationOpenedU3Ek__BackingField_13; }
	inline void set_U3CNotificationOpenedU3Ek__BackingField_13(bool value)
	{
		___U3CNotificationOpenedU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGE_T1021966236_H
#ifndef U3CCREXPORTGIFU3EC__ITERATOR0_T662514209_H
#define U3CCREXPORTGIFU3EC__ITERATOR0_T662514209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Gif/<CRExportGif>c__Iterator0
struct  U3CCRExportGifU3Ec__Iterator0_t662514209  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.Gif/<CRExportGif>c__Iterator0::loop
	int32_t ___loop_0;
	// System.Int32 EasyMobile.Gif/<CRExportGif>c__Iterator0::quality
	int32_t ___quality_1;
	// System.Int32 EasyMobile.Gif/<CRExportGif>c__Iterator0::<sampleFac>__0
	int32_t ___U3CsampleFacU3E__0_2;
	// System.String EasyMobile.Gif/<CRExportGif>c__Iterator0::<folder>__0
	String_t* ___U3CfolderU3E__0_3;
	// System.String EasyMobile.Gif/<CRExportGif>c__Iterator0::filename
	String_t* ___filename_4;
	// System.String EasyMobile.Gif/<CRExportGif>c__Iterator0::<filepath>__0
	String_t* ___U3CfilepathU3E__0_5;
	// EasyMobile.GifExportTask EasyMobile.Gif/<CRExportGif>c__Iterator0::<exportTask>__0
	GifExportTask_t1106658426 * ___U3CexportTaskU3E__0_6;
	// EasyMobile.AnimatedClip EasyMobile.Gif/<CRExportGif>c__Iterator0::clip
	AnimatedClip_t619307834 * ___clip_7;
	// System.Action`2<EasyMobile.AnimatedClip,System.Single> EasyMobile.Gif/<CRExportGif>c__Iterator0::exportProgressCallback
	Action_2_t1508656458 * ___exportProgressCallback_8;
	// System.Action`2<EasyMobile.AnimatedClip,System.String> EasyMobile.Gif/<CRExportGif>c__Iterator0::exportCompletedCallback
	Action_2_t1958840373 * ___exportCompletedCallback_9;
	// System.Threading.ThreadPriority EasyMobile.Gif/<CRExportGif>c__Iterator0::threadPriority
	int32_t ___threadPriority_10;
	// UnityEngine.Texture2D EasyMobile.Gif/<CRExportGif>c__Iterator0::<temp>__0
	Texture2D_t3840446185 * ___U3CtempU3E__0_11;
	// System.Int32 EasyMobile.Gif/<CRExportGif>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_12;
	// UnityEngine.RenderTexture EasyMobile.Gif/<CRExportGif>c__Iterator0::<source>__2
	RenderTexture_t2108887433 * ___U3CsourceU3E__2_13;
	// System.Single EasyMobile.Gif/<CRExportGif>c__Iterator0::<progress>__2
	float ___U3CprogressU3E__2_14;
	// System.Object EasyMobile.Gif/<CRExportGif>c__Iterator0::$current
	RuntimeObject * ___U24current_15;
	// System.Boolean EasyMobile.Gif/<CRExportGif>c__Iterator0::$disposing
	bool ___U24disposing_16;
	// System.Int32 EasyMobile.Gif/<CRExportGif>c__Iterator0::$PC
	int32_t ___U24PC_17;

public:
	inline static int32_t get_offset_of_loop_0() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___loop_0)); }
	inline int32_t get_loop_0() const { return ___loop_0; }
	inline int32_t* get_address_of_loop_0() { return &___loop_0; }
	inline void set_loop_0(int32_t value)
	{
		___loop_0 = value;
	}

	inline static int32_t get_offset_of_quality_1() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___quality_1)); }
	inline int32_t get_quality_1() const { return ___quality_1; }
	inline int32_t* get_address_of_quality_1() { return &___quality_1; }
	inline void set_quality_1(int32_t value)
	{
		___quality_1 = value;
	}

	inline static int32_t get_offset_of_U3CsampleFacU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CsampleFacU3E__0_2)); }
	inline int32_t get_U3CsampleFacU3E__0_2() const { return ___U3CsampleFacU3E__0_2; }
	inline int32_t* get_address_of_U3CsampleFacU3E__0_2() { return &___U3CsampleFacU3E__0_2; }
	inline void set_U3CsampleFacU3E__0_2(int32_t value)
	{
		___U3CsampleFacU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CfolderU3E__0_3() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CfolderU3E__0_3)); }
	inline String_t* get_U3CfolderU3E__0_3() const { return ___U3CfolderU3E__0_3; }
	inline String_t** get_address_of_U3CfolderU3E__0_3() { return &___U3CfolderU3E__0_3; }
	inline void set_U3CfolderU3E__0_3(String_t* value)
	{
		___U3CfolderU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfolderU3E__0_3), value);
	}

	inline static int32_t get_offset_of_filename_4() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___filename_4)); }
	inline String_t* get_filename_4() const { return ___filename_4; }
	inline String_t** get_address_of_filename_4() { return &___filename_4; }
	inline void set_filename_4(String_t* value)
	{
		___filename_4 = value;
		Il2CppCodeGenWriteBarrier((&___filename_4), value);
	}

	inline static int32_t get_offset_of_U3CfilepathU3E__0_5() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CfilepathU3E__0_5)); }
	inline String_t* get_U3CfilepathU3E__0_5() const { return ___U3CfilepathU3E__0_5; }
	inline String_t** get_address_of_U3CfilepathU3E__0_5() { return &___U3CfilepathU3E__0_5; }
	inline void set_U3CfilepathU3E__0_5(String_t* value)
	{
		___U3CfilepathU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfilepathU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CexportTaskU3E__0_6() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CexportTaskU3E__0_6)); }
	inline GifExportTask_t1106658426 * get_U3CexportTaskU3E__0_6() const { return ___U3CexportTaskU3E__0_6; }
	inline GifExportTask_t1106658426 ** get_address_of_U3CexportTaskU3E__0_6() { return &___U3CexportTaskU3E__0_6; }
	inline void set_U3CexportTaskU3E__0_6(GifExportTask_t1106658426 * value)
	{
		___U3CexportTaskU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexportTaskU3E__0_6), value);
	}

	inline static int32_t get_offset_of_clip_7() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___clip_7)); }
	inline AnimatedClip_t619307834 * get_clip_7() const { return ___clip_7; }
	inline AnimatedClip_t619307834 ** get_address_of_clip_7() { return &___clip_7; }
	inline void set_clip_7(AnimatedClip_t619307834 * value)
	{
		___clip_7 = value;
		Il2CppCodeGenWriteBarrier((&___clip_7), value);
	}

	inline static int32_t get_offset_of_exportProgressCallback_8() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___exportProgressCallback_8)); }
	inline Action_2_t1508656458 * get_exportProgressCallback_8() const { return ___exportProgressCallback_8; }
	inline Action_2_t1508656458 ** get_address_of_exportProgressCallback_8() { return &___exportProgressCallback_8; }
	inline void set_exportProgressCallback_8(Action_2_t1508656458 * value)
	{
		___exportProgressCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___exportProgressCallback_8), value);
	}

	inline static int32_t get_offset_of_exportCompletedCallback_9() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___exportCompletedCallback_9)); }
	inline Action_2_t1958840373 * get_exportCompletedCallback_9() const { return ___exportCompletedCallback_9; }
	inline Action_2_t1958840373 ** get_address_of_exportCompletedCallback_9() { return &___exportCompletedCallback_9; }
	inline void set_exportCompletedCallback_9(Action_2_t1958840373 * value)
	{
		___exportCompletedCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___exportCompletedCallback_9), value);
	}

	inline static int32_t get_offset_of_threadPriority_10() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___threadPriority_10)); }
	inline int32_t get_threadPriority_10() const { return ___threadPriority_10; }
	inline int32_t* get_address_of_threadPriority_10() { return &___threadPriority_10; }
	inline void set_threadPriority_10(int32_t value)
	{
		___threadPriority_10 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__0_11() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CtempU3E__0_11)); }
	inline Texture2D_t3840446185 * get_U3CtempU3E__0_11() const { return ___U3CtempU3E__0_11; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtempU3E__0_11() { return &___U3CtempU3E__0_11; }
	inline void set_U3CtempU3E__0_11(Texture2D_t3840446185 * value)
	{
		___U3CtempU3E__0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempU3E__0_11), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_12() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CiU3E__1_12)); }
	inline int32_t get_U3CiU3E__1_12() const { return ___U3CiU3E__1_12; }
	inline int32_t* get_address_of_U3CiU3E__1_12() { return &___U3CiU3E__1_12; }
	inline void set_U3CiU3E__1_12(int32_t value)
	{
		___U3CiU3E__1_12 = value;
	}

	inline static int32_t get_offset_of_U3CsourceU3E__2_13() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CsourceU3E__2_13)); }
	inline RenderTexture_t2108887433 * get_U3CsourceU3E__2_13() const { return ___U3CsourceU3E__2_13; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CsourceU3E__2_13() { return &___U3CsourceU3E__2_13; }
	inline void set_U3CsourceU3E__2_13(RenderTexture_t2108887433 * value)
	{
		___U3CsourceU3E__2_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3E__2_13), value);
	}

	inline static int32_t get_offset_of_U3CprogressU3E__2_14() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U3CprogressU3E__2_14)); }
	inline float get_U3CprogressU3E__2_14() const { return ___U3CprogressU3E__2_14; }
	inline float* get_address_of_U3CprogressU3E__2_14() { return &___U3CprogressU3E__2_14; }
	inline void set_U3CprogressU3E__2_14(float value)
	{
		___U3CprogressU3E__2_14 = value;
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U24current_15)); }
	inline RuntimeObject * get_U24current_15() const { return ___U24current_15; }
	inline RuntimeObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(RuntimeObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_15), value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CCRExportGifU3Ec__Iterator0_t662514209, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREXPORTGIFU3EC__ITERATOR0_T662514209_H
#ifndef GIFEXPORTTASK_T1106658426_H
#define GIFEXPORTTASK_T1106658426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GifExportTask
struct  GifExportTask_t1106658426  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.GifExportTask::taskId
	int32_t ___taskId_0;
	// EasyMobile.AnimatedClip EasyMobile.GifExportTask::clip
	AnimatedClip_t619307834 * ___clip_1;
	// UnityEngine.Color32[][] EasyMobile.GifExportTask::imageData
	Color32U5BU5DU5BU5D_t107669032* ___imageData_2;
	// System.String EasyMobile.GifExportTask::filepath
	String_t* ___filepath_3;
	// System.Int32 EasyMobile.GifExportTask::loop
	int32_t ___loop_4;
	// System.Int32 EasyMobile.GifExportTask::sampleFac
	int32_t ___sampleFac_5;
	// System.Boolean EasyMobile.GifExportTask::isExporting
	bool ___isExporting_6;
	// System.Boolean EasyMobile.GifExportTask::isDone
	bool ___isDone_7;
	// System.Single EasyMobile.GifExportTask::progress
	float ___progress_8;
	// System.Action`2<EasyMobile.AnimatedClip,System.Single> EasyMobile.GifExportTask::exportProgressCallback
	Action_2_t1508656458 * ___exportProgressCallback_9;
	// System.Action`2<EasyMobile.AnimatedClip,System.String> EasyMobile.GifExportTask::exportCompletedCallback
	Action_2_t1958840373 * ___exportCompletedCallback_10;
	// System.Threading.ThreadPriority EasyMobile.GifExportTask::workerPriority
	int32_t ___workerPriority_11;

public:
	inline static int32_t get_offset_of_taskId_0() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___taskId_0)); }
	inline int32_t get_taskId_0() const { return ___taskId_0; }
	inline int32_t* get_address_of_taskId_0() { return &___taskId_0; }
	inline void set_taskId_0(int32_t value)
	{
		___taskId_0 = value;
	}

	inline static int32_t get_offset_of_clip_1() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___clip_1)); }
	inline AnimatedClip_t619307834 * get_clip_1() const { return ___clip_1; }
	inline AnimatedClip_t619307834 ** get_address_of_clip_1() { return &___clip_1; }
	inline void set_clip_1(AnimatedClip_t619307834 * value)
	{
		___clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___clip_1), value);
	}

	inline static int32_t get_offset_of_imageData_2() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___imageData_2)); }
	inline Color32U5BU5DU5BU5D_t107669032* get_imageData_2() const { return ___imageData_2; }
	inline Color32U5BU5DU5BU5D_t107669032** get_address_of_imageData_2() { return &___imageData_2; }
	inline void set_imageData_2(Color32U5BU5DU5BU5D_t107669032* value)
	{
		___imageData_2 = value;
		Il2CppCodeGenWriteBarrier((&___imageData_2), value);
	}

	inline static int32_t get_offset_of_filepath_3() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___filepath_3)); }
	inline String_t* get_filepath_3() const { return ___filepath_3; }
	inline String_t** get_address_of_filepath_3() { return &___filepath_3; }
	inline void set_filepath_3(String_t* value)
	{
		___filepath_3 = value;
		Il2CppCodeGenWriteBarrier((&___filepath_3), value);
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___loop_4)); }
	inline int32_t get_loop_4() const { return ___loop_4; }
	inline int32_t* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(int32_t value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_sampleFac_5() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___sampleFac_5)); }
	inline int32_t get_sampleFac_5() const { return ___sampleFac_5; }
	inline int32_t* get_address_of_sampleFac_5() { return &___sampleFac_5; }
	inline void set_sampleFac_5(int32_t value)
	{
		___sampleFac_5 = value;
	}

	inline static int32_t get_offset_of_isExporting_6() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___isExporting_6)); }
	inline bool get_isExporting_6() const { return ___isExporting_6; }
	inline bool* get_address_of_isExporting_6() { return &___isExporting_6; }
	inline void set_isExporting_6(bool value)
	{
		___isExporting_6 = value;
	}

	inline static int32_t get_offset_of_isDone_7() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___isDone_7)); }
	inline bool get_isDone_7() const { return ___isDone_7; }
	inline bool* get_address_of_isDone_7() { return &___isDone_7; }
	inline void set_isDone_7(bool value)
	{
		___isDone_7 = value;
	}

	inline static int32_t get_offset_of_progress_8() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___progress_8)); }
	inline float get_progress_8() const { return ___progress_8; }
	inline float* get_address_of_progress_8() { return &___progress_8; }
	inline void set_progress_8(float value)
	{
		___progress_8 = value;
	}

	inline static int32_t get_offset_of_exportProgressCallback_9() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___exportProgressCallback_9)); }
	inline Action_2_t1508656458 * get_exportProgressCallback_9() const { return ___exportProgressCallback_9; }
	inline Action_2_t1508656458 ** get_address_of_exportProgressCallback_9() { return &___exportProgressCallback_9; }
	inline void set_exportProgressCallback_9(Action_2_t1508656458 * value)
	{
		___exportProgressCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___exportProgressCallback_9), value);
	}

	inline static int32_t get_offset_of_exportCompletedCallback_10() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___exportCompletedCallback_10)); }
	inline Action_2_t1958840373 * get_exportCompletedCallback_10() const { return ___exportCompletedCallback_10; }
	inline Action_2_t1958840373 ** get_address_of_exportCompletedCallback_10() { return &___exportCompletedCallback_10; }
	inline void set_exportCompletedCallback_10(Action_2_t1958840373 * value)
	{
		___exportCompletedCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___exportCompletedCallback_10), value);
	}

	inline static int32_t get_offset_of_workerPriority_11() { return static_cast<int32_t>(offsetof(GifExportTask_t1106658426, ___workerPriority_11)); }
	inline int32_t get_workerPriority_11() const { return ___workerPriority_11; }
	inline int32_t* get_address_of_workerPriority_11() { return &___workerPriority_11; }
	inline void set_workerPriority_11(int32_t value)
	{
		___workerPriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIFEXPORTTASK_T1106658426_H
#ifndef IAPPRODUCT_T1805032951_H
#define IAPPRODUCT_T1805032951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IAPProduct
struct  IAPProduct_t1805032951  : public RuntimeObject
{
public:
	// System.String EasyMobile.IAPProduct::_name
	String_t* ____name_0;
	// EasyMobile.IAPProductType EasyMobile.IAPProduct::_type
	int32_t ____type_1;
	// System.String EasyMobile.IAPProduct::_id
	String_t* ____id_2;
	// System.String EasyMobile.IAPProduct::_price
	String_t* ____price_3;
	// System.String EasyMobile.IAPProduct::_description
	String_t* ____description_4;
	// EasyMobile.IAPProduct/StoreSpecificId[] EasyMobile.IAPProduct::_storeSpecificIds
	StoreSpecificIdU5BU5D_t3380943197* ____storeSpecificIds_5;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(IAPProduct_t1805032951, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(IAPProduct_t1805032951, ____type_1)); }
	inline int32_t get__type_1() const { return ____type_1; }
	inline int32_t* get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(int32_t value)
	{
		____type_1 = value;
	}

	inline static int32_t get_offset_of__id_2() { return static_cast<int32_t>(offsetof(IAPProduct_t1805032951, ____id_2)); }
	inline String_t* get__id_2() const { return ____id_2; }
	inline String_t** get_address_of__id_2() { return &____id_2; }
	inline void set__id_2(String_t* value)
	{
		____id_2 = value;
		Il2CppCodeGenWriteBarrier((&____id_2), value);
	}

	inline static int32_t get_offset_of__price_3() { return static_cast<int32_t>(offsetof(IAPProduct_t1805032951, ____price_3)); }
	inline String_t* get__price_3() const { return ____price_3; }
	inline String_t** get_address_of__price_3() { return &____price_3; }
	inline void set__price_3(String_t* value)
	{
		____price_3 = value;
		Il2CppCodeGenWriteBarrier((&____price_3), value);
	}

	inline static int32_t get_offset_of__description_4() { return static_cast<int32_t>(offsetof(IAPProduct_t1805032951, ____description_4)); }
	inline String_t* get__description_4() const { return ____description_4; }
	inline String_t** get_address_of__description_4() { return &____description_4; }
	inline void set__description_4(String_t* value)
	{
		____description_4 = value;
		Il2CppCodeGenWriteBarrier((&____description_4), value);
	}

	inline static int32_t get_offset_of__storeSpecificIds_5() { return static_cast<int32_t>(offsetof(IAPProduct_t1805032951, ____storeSpecificIds_5)); }
	inline StoreSpecificIdU5BU5D_t3380943197* get__storeSpecificIds_5() const { return ____storeSpecificIds_5; }
	inline StoreSpecificIdU5BU5D_t3380943197** get_address_of__storeSpecificIds_5() { return &____storeSpecificIds_5; }
	inline void set__storeSpecificIds_5(StoreSpecificIdU5BU5D_t3380943197* value)
	{
		____storeSpecificIds_5 = value;
		Il2CppCodeGenWriteBarrier((&____storeSpecificIds_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPPRODUCT_T1805032951_H
#ifndef STORESPECIFICID_T955348180_H
#define STORESPECIFICID_T955348180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IAPProduct/StoreSpecificId
struct  StoreSpecificId_t955348180  : public RuntimeObject
{
public:
	// EasyMobile.IAPStore EasyMobile.IAPProduct/StoreSpecificId::store
	int32_t ___store_0;
	// System.String EasyMobile.IAPProduct/StoreSpecificId::id
	String_t* ___id_1;

public:
	inline static int32_t get_offset_of_store_0() { return static_cast<int32_t>(offsetof(StoreSpecificId_t955348180, ___store_0)); }
	inline int32_t get_store_0() const { return ___store_0; }
	inline int32_t* get_address_of_store_0() { return &___store_0; }
	inline void set_store_0(int32_t value)
	{
		___store_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(StoreSpecificId_t955348180, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORESPECIFICID_T955348180_H
#ifndef IAPSETTINGS_T189197306_H
#define IAPSETTINGS_T189197306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.IAPSettings
struct  IAPSettings_t189197306  : public RuntimeObject
{
public:
	// EasyMobile.IAPAndroidStore EasyMobile.IAPSettings::mTargetAndroidStore
	int32_t ___mTargetAndroidStore_0;
	// System.Boolean EasyMobile.IAPSettings::mSimulateAppleAskToBuy
	bool ___mSimulateAppleAskToBuy_1;
	// System.Boolean EasyMobile.IAPSettings::mInterceptApplePromotionalPurchases
	bool ___mInterceptApplePromotionalPurchases_2;
	// System.Boolean EasyMobile.IAPSettings::mEnableAmazonSandboxTesting
	bool ___mEnableAmazonSandboxTesting_3;
	// System.Boolean EasyMobile.IAPSettings::mValidateAppleReceipt
	bool ___mValidateAppleReceipt_4;
	// System.Boolean EasyMobile.IAPSettings::mValidateGooglePlayReceipt
	bool ___mValidateGooglePlayReceipt_5;
	// EasyMobile.IAPProduct[] EasyMobile.IAPSettings::mProducts
	IAPProductU5BU5D_t2701488206* ___mProducts_6;

public:
	inline static int32_t get_offset_of_mTargetAndroidStore_0() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mTargetAndroidStore_0)); }
	inline int32_t get_mTargetAndroidStore_0() const { return ___mTargetAndroidStore_0; }
	inline int32_t* get_address_of_mTargetAndroidStore_0() { return &___mTargetAndroidStore_0; }
	inline void set_mTargetAndroidStore_0(int32_t value)
	{
		___mTargetAndroidStore_0 = value;
	}

	inline static int32_t get_offset_of_mSimulateAppleAskToBuy_1() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mSimulateAppleAskToBuy_1)); }
	inline bool get_mSimulateAppleAskToBuy_1() const { return ___mSimulateAppleAskToBuy_1; }
	inline bool* get_address_of_mSimulateAppleAskToBuy_1() { return &___mSimulateAppleAskToBuy_1; }
	inline void set_mSimulateAppleAskToBuy_1(bool value)
	{
		___mSimulateAppleAskToBuy_1 = value;
	}

	inline static int32_t get_offset_of_mInterceptApplePromotionalPurchases_2() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mInterceptApplePromotionalPurchases_2)); }
	inline bool get_mInterceptApplePromotionalPurchases_2() const { return ___mInterceptApplePromotionalPurchases_2; }
	inline bool* get_address_of_mInterceptApplePromotionalPurchases_2() { return &___mInterceptApplePromotionalPurchases_2; }
	inline void set_mInterceptApplePromotionalPurchases_2(bool value)
	{
		___mInterceptApplePromotionalPurchases_2 = value;
	}

	inline static int32_t get_offset_of_mEnableAmazonSandboxTesting_3() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mEnableAmazonSandboxTesting_3)); }
	inline bool get_mEnableAmazonSandboxTesting_3() const { return ___mEnableAmazonSandboxTesting_3; }
	inline bool* get_address_of_mEnableAmazonSandboxTesting_3() { return &___mEnableAmazonSandboxTesting_3; }
	inline void set_mEnableAmazonSandboxTesting_3(bool value)
	{
		___mEnableAmazonSandboxTesting_3 = value;
	}

	inline static int32_t get_offset_of_mValidateAppleReceipt_4() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mValidateAppleReceipt_4)); }
	inline bool get_mValidateAppleReceipt_4() const { return ___mValidateAppleReceipt_4; }
	inline bool* get_address_of_mValidateAppleReceipt_4() { return &___mValidateAppleReceipt_4; }
	inline void set_mValidateAppleReceipt_4(bool value)
	{
		___mValidateAppleReceipt_4 = value;
	}

	inline static int32_t get_offset_of_mValidateGooglePlayReceipt_5() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mValidateGooglePlayReceipt_5)); }
	inline bool get_mValidateGooglePlayReceipt_5() const { return ___mValidateGooglePlayReceipt_5; }
	inline bool* get_address_of_mValidateGooglePlayReceipt_5() { return &___mValidateGooglePlayReceipt_5; }
	inline void set_mValidateGooglePlayReceipt_5(bool value)
	{
		___mValidateGooglePlayReceipt_5 = value;
	}

	inline static int32_t get_offset_of_mProducts_6() { return static_cast<int32_t>(offsetof(IAPSettings_t189197306, ___mProducts_6)); }
	inline IAPProductU5BU5D_t2701488206* get_mProducts_6() const { return ___mProducts_6; }
	inline IAPProductU5BU5D_t2701488206** get_address_of_mProducts_6() { return &___mProducts_6; }
	inline void set_mProducts_6(IAPProductU5BU5D_t2701488206* value)
	{
		___mProducts_6 = value;
		Il2CppCodeGenWriteBarrier((&___mProducts_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPSETTINGS_T189197306_H
#ifndef U3CINTERNALOPENWITHAUTOMATICCONFLICTRESOLUTIONU3EC__ANONSTOREY6_T440150218_H
#define U3CINTERNALOPENWITHAUTOMATICCONFLICTRESOLUTIONU3EC__ANONSTOREY6_T440150218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithAutomaticConflictResolution>c__AnonStorey6
struct  U3CInternalOpenWithAutomaticConflictResolutionU3Ec__AnonStorey6_t440150218  : public RuntimeObject
{
public:
	// EasyMobile.Internal.GameServices.iOSConflictResolutionStrategy EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithAutomaticConflictResolution>c__AnonStorey6::resolutionStrategy
	int32_t ___resolutionStrategy_0;
	// System.Action`2<EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame,System.String> EasyMobile.Internal.GameServices.iOSSavedGameClient/<InternalOpenWithAutomaticConflictResolution>c__AnonStorey6::callback
	Action_2_t1563910866 * ___callback_1;

public:
	inline static int32_t get_offset_of_resolutionStrategy_0() { return static_cast<int32_t>(offsetof(U3CInternalOpenWithAutomaticConflictResolutionU3Ec__AnonStorey6_t440150218, ___resolutionStrategy_0)); }
	inline int32_t get_resolutionStrategy_0() const { return ___resolutionStrategy_0; }
	inline int32_t* get_address_of_resolutionStrategy_0() { return &___resolutionStrategy_0; }
	inline void set_resolutionStrategy_0(int32_t value)
	{
		___resolutionStrategy_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CInternalOpenWithAutomaticConflictResolutionU3Ec__AnonStorey6_t440150218, ___callback_1)); }
	inline Action_2_t1563910866 * get_callback_1() const { return ___callback_1; }
	inline Action_2_t1563910866 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_2_t1563910866 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALOPENWITHAUTOMATICCONFLICTRESOLUTIONU3EC__ANONSTOREY6_T440150218_H
#ifndef INTEROPOBJECT_T35158505_H
#define INTEROPOBJECT_T35158505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InteropObject
struct  InteropObject_t35158505  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef EasyMobile.InteropObject::mSelfPointer
	HandleRef_t3745784362  ___mSelfPointer_0;

public:
	inline static int32_t get_offset_of_mSelfPointer_0() { return static_cast<int32_t>(offsetof(InteropObject_t35158505, ___mSelfPointer_0)); }
	inline HandleRef_t3745784362  get_mSelfPointer_0() const { return ___mSelfPointer_0; }
	inline HandleRef_t3745784362 * get_address_of_mSelfPointer_0() { return &___mSelfPointer_0; }
	inline void set_mSelfPointer_0(HandleRef_t3745784362  value)
	{
		___mSelfPointer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEROPOBJECT_T35158505_H
#ifndef SAVEDGAMEINFOUPDATE_T1021132738_H
#define SAVEDGAMEINFOUPDATE_T1021132738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.SavedGameInfoUpdate
struct  SavedGameInfoUpdate_t1021132738 
{
public:
	// System.Boolean EasyMobile.SavedGameInfoUpdate::_descriptionUpdated
	bool ____descriptionUpdated_0;
	// System.String EasyMobile.SavedGameInfoUpdate::_newDescription
	String_t* ____newDescription_1;
	// System.Boolean EasyMobile.SavedGameInfoUpdate::_coverImageUpdated
	bool ____coverImageUpdated_2;
	// System.Byte[] EasyMobile.SavedGameInfoUpdate::_newPngCoverImage
	ByteU5BU5D_t4116647657* ____newPngCoverImage_3;
	// System.Boolean EasyMobile.SavedGameInfoUpdate::_playedTimeUpdated
	bool ____playedTimeUpdated_4;
	// System.TimeSpan EasyMobile.SavedGameInfoUpdate::_newPlayedTime
	TimeSpan_t881159249  ____newPlayedTime_5;

public:
	inline static int32_t get_offset_of__descriptionUpdated_0() { return static_cast<int32_t>(offsetof(SavedGameInfoUpdate_t1021132738, ____descriptionUpdated_0)); }
	inline bool get__descriptionUpdated_0() const { return ____descriptionUpdated_0; }
	inline bool* get_address_of__descriptionUpdated_0() { return &____descriptionUpdated_0; }
	inline void set__descriptionUpdated_0(bool value)
	{
		____descriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of__newDescription_1() { return static_cast<int32_t>(offsetof(SavedGameInfoUpdate_t1021132738, ____newDescription_1)); }
	inline String_t* get__newDescription_1() const { return ____newDescription_1; }
	inline String_t** get_address_of__newDescription_1() { return &____newDescription_1; }
	inline void set__newDescription_1(String_t* value)
	{
		____newDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&____newDescription_1), value);
	}

	inline static int32_t get_offset_of__coverImageUpdated_2() { return static_cast<int32_t>(offsetof(SavedGameInfoUpdate_t1021132738, ____coverImageUpdated_2)); }
	inline bool get__coverImageUpdated_2() const { return ____coverImageUpdated_2; }
	inline bool* get_address_of__coverImageUpdated_2() { return &____coverImageUpdated_2; }
	inline void set__coverImageUpdated_2(bool value)
	{
		____coverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of__newPngCoverImage_3() { return static_cast<int32_t>(offsetof(SavedGameInfoUpdate_t1021132738, ____newPngCoverImage_3)); }
	inline ByteU5BU5D_t4116647657* get__newPngCoverImage_3() const { return ____newPngCoverImage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__newPngCoverImage_3() { return &____newPngCoverImage_3; }
	inline void set__newPngCoverImage_3(ByteU5BU5D_t4116647657* value)
	{
		____newPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((&____newPngCoverImage_3), value);
	}

	inline static int32_t get_offset_of__playedTimeUpdated_4() { return static_cast<int32_t>(offsetof(SavedGameInfoUpdate_t1021132738, ____playedTimeUpdated_4)); }
	inline bool get__playedTimeUpdated_4() const { return ____playedTimeUpdated_4; }
	inline bool* get_address_of__playedTimeUpdated_4() { return &____playedTimeUpdated_4; }
	inline void set__playedTimeUpdated_4(bool value)
	{
		____playedTimeUpdated_4 = value;
	}

	inline static int32_t get_offset_of__newPlayedTime_5() { return static_cast<int32_t>(offsetof(SavedGameInfoUpdate_t1021132738, ____newPlayedTime_5)); }
	inline TimeSpan_t881159249  get__newPlayedTime_5() const { return ____newPlayedTime_5; }
	inline TimeSpan_t881159249 * get_address_of__newPlayedTime_5() { return &____newPlayedTime_5; }
	inline void set__newPlayedTime_5(TimeSpan_t881159249  value)
	{
		____newPlayedTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.SavedGameInfoUpdate
struct SavedGameInfoUpdate_t1021132738_marshaled_pinvoke
{
	int32_t ____descriptionUpdated_0;
	char* ____newDescription_1;
	int32_t ____coverImageUpdated_2;
	uint8_t* ____newPngCoverImage_3;
	int32_t ____playedTimeUpdated_4;
	TimeSpan_t881159249  ____newPlayedTime_5;
};
// Native definition for COM marshalling of EasyMobile.SavedGameInfoUpdate
struct SavedGameInfoUpdate_t1021132738_marshaled_com
{
	int32_t ____descriptionUpdated_0;
	Il2CppChar* ____newDescription_1;
	int32_t ____coverImageUpdated_2;
	uint8_t* ____newPngCoverImage_3;
	int32_t ____playedTimeUpdated_4;
	TimeSpan_t881159249  ____newPlayedTime_5;
};
#endif // SAVEDGAMEINFOUPDATE_T1021132738_H
#ifndef BUILDER_T2048762101_H
#define BUILDER_T2048762101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.SavedGameInfoUpdate/Builder
struct  Builder_t2048762101 
{
public:
	// System.Boolean EasyMobile.SavedGameInfoUpdate/Builder::_descriptionUpdated
	bool ____descriptionUpdated_0;
	// System.String EasyMobile.SavedGameInfoUpdate/Builder::_newDescription
	String_t* ____newDescription_1;
	// System.Boolean EasyMobile.SavedGameInfoUpdate/Builder::_coverImageUpdated
	bool ____coverImageUpdated_2;
	// System.Byte[] EasyMobile.SavedGameInfoUpdate/Builder::_newPngCoverImage
	ByteU5BU5D_t4116647657* ____newPngCoverImage_3;
	// System.Boolean EasyMobile.SavedGameInfoUpdate/Builder::_playedTimeUpdated
	bool ____playedTimeUpdated_4;
	// System.TimeSpan EasyMobile.SavedGameInfoUpdate/Builder::_newPlayedTime
	TimeSpan_t881159249  ____newPlayedTime_5;

public:
	inline static int32_t get_offset_of__descriptionUpdated_0() { return static_cast<int32_t>(offsetof(Builder_t2048762101, ____descriptionUpdated_0)); }
	inline bool get__descriptionUpdated_0() const { return ____descriptionUpdated_0; }
	inline bool* get_address_of__descriptionUpdated_0() { return &____descriptionUpdated_0; }
	inline void set__descriptionUpdated_0(bool value)
	{
		____descriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of__newDescription_1() { return static_cast<int32_t>(offsetof(Builder_t2048762101, ____newDescription_1)); }
	inline String_t* get__newDescription_1() const { return ____newDescription_1; }
	inline String_t** get_address_of__newDescription_1() { return &____newDescription_1; }
	inline void set__newDescription_1(String_t* value)
	{
		____newDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&____newDescription_1), value);
	}

	inline static int32_t get_offset_of__coverImageUpdated_2() { return static_cast<int32_t>(offsetof(Builder_t2048762101, ____coverImageUpdated_2)); }
	inline bool get__coverImageUpdated_2() const { return ____coverImageUpdated_2; }
	inline bool* get_address_of__coverImageUpdated_2() { return &____coverImageUpdated_2; }
	inline void set__coverImageUpdated_2(bool value)
	{
		____coverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of__newPngCoverImage_3() { return static_cast<int32_t>(offsetof(Builder_t2048762101, ____newPngCoverImage_3)); }
	inline ByteU5BU5D_t4116647657* get__newPngCoverImage_3() const { return ____newPngCoverImage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__newPngCoverImage_3() { return &____newPngCoverImage_3; }
	inline void set__newPngCoverImage_3(ByteU5BU5D_t4116647657* value)
	{
		____newPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((&____newPngCoverImage_3), value);
	}

	inline static int32_t get_offset_of__playedTimeUpdated_4() { return static_cast<int32_t>(offsetof(Builder_t2048762101, ____playedTimeUpdated_4)); }
	inline bool get__playedTimeUpdated_4() const { return ____playedTimeUpdated_4; }
	inline bool* get_address_of__playedTimeUpdated_4() { return &____playedTimeUpdated_4; }
	inline void set__playedTimeUpdated_4(bool value)
	{
		____playedTimeUpdated_4 = value;
	}

	inline static int32_t get_offset_of__newPlayedTime_5() { return static_cast<int32_t>(offsetof(Builder_t2048762101, ____newPlayedTime_5)); }
	inline TimeSpan_t881159249  get__newPlayedTime_5() const { return ____newPlayedTime_5; }
	inline TimeSpan_t881159249 * get_address_of__newPlayedTime_5() { return &____newPlayedTime_5; }
	inline void set__newPlayedTime_5(TimeSpan_t881159249  value)
	{
		____newPlayedTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.SavedGameInfoUpdate/Builder
struct Builder_t2048762101_marshaled_pinvoke
{
	int32_t ____descriptionUpdated_0;
	char* ____newDescription_1;
	int32_t ____coverImageUpdated_2;
	uint8_t* ____newPngCoverImage_3;
	int32_t ____playedTimeUpdated_4;
	TimeSpan_t881159249  ____newPlayedTime_5;
};
// Native definition for COM marshalling of EasyMobile.SavedGameInfoUpdate/Builder
struct Builder_t2048762101_marshaled_com
{
	int32_t ____descriptionUpdated_0;
	Il2CppChar* ____newDescription_1;
	int32_t ____coverImageUpdated_2;
	uint8_t* ____newPngCoverImage_3;
	int32_t ____playedTimeUpdated_4;
	TimeSpan_t881159249  ____newPlayedTime_5;
};
#endif // BUILDER_T2048762101_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DELETESAVEDGAMERESPONSE_T1617544370_H
#define DELETESAVEDGAMERESPONSE_T1617544370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.DeleteSavedGameResponse
struct  DeleteSavedGameResponse_t1617544370  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETESAVEDGAMERESPONSE_T1617544370_H
#ifndef FETCHSAVEDGAMESRESPONSE_T3634168769_H
#define FETCHSAVEDGAMESRESPONSE_T3634168769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.FetchSavedGamesResponse
struct  FetchSavedGamesResponse_t3634168769  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FETCHSAVEDGAMESRESPONSE_T3634168769_H
#ifndef LOADSAVEDGAMEDATARESPONSE_T10080320_H
#define LOADSAVEDGAMEDATARESPONSE_T10080320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.LoadSavedGameDataResponse
struct  LoadSavedGameDataResponse_t10080320  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSAVEDGAMEDATARESPONSE_T10080320_H
#ifndef OPENSAVEDGAMERESPONSE_T960542049_H
#define OPENSAVEDGAMERESPONSE_T960542049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.OpenSavedGameResponse
struct  OpenSavedGameResponse_t960542049  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENSAVEDGAMERESPONSE_T960542049_H
#ifndef RESOLVECONFLICTRESPONSE_T1723489802_H
#define RESOLVECONFLICTRESPONSE_T1723489802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.ResolveConflictResponse
struct  ResolveConflictResponse_t1723489802  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVECONFLICTRESPONSE_T1723489802_H
#ifndef SAVEGAMEDATARESPONSE_T3887393376_H
#define SAVEGAMEDATARESPONSE_T3887393376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.SaveGameDataResponse
struct  SaveGameDataResponse_t3887393376  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEGAMEDATARESPONSE_T3887393376_H
#ifndef IOSGKSAVEDGAME_T2037893409_H
#define IOSGKSAVEDGAME_T2037893409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSGKSavedGame
struct  iOSGKSavedGame_t2037893409  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSGKSAVEDGAME_T2037893409_H
#ifndef DELETESAVEDGAMECALLBACK_T2032035250_H
#define DELETESAVEDGAMECALLBACK_T2032035250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/DeleteSavedGameCallback
struct  DeleteSavedGameCallback_t2032035250  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETESAVEDGAMECALLBACK_T2032035250_H
#ifndef RESOLVECONFLICTINGSAVEDGAMESCALLBACK_T4045204290_H
#define RESOLVECONFLICTINGSAVEDGAMESCALLBACK_T4045204290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/ResolveConflictingSavedGamesCallback
struct  ResolveConflictingSavedGamesCallback_t4045204290  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVECONFLICTINGSAVEDGAMESCALLBACK_T4045204290_H
#ifndef IOSCONFLICTCALLBACK_T756019127_H
#define IOSCONFLICTCALLBACK_T756019127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.GameServices.iOSConflictCallback
struct  iOSConflictCallback_t756019127  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCONFLICTCALLBACK_T756019127_H
#ifndef GIFEXPORTCOMPLETEDDELEGATE_T4082577070_H
#define GIFEXPORTCOMPLETEDDELEGATE_T4082577070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportCompletedDelegate
struct  GifExportCompletedDelegate_t4082577070  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIFEXPORTCOMPLETEDDELEGATE_T4082577070_H
#ifndef GIFEXPORTPROGRESSDELEGATE_T324897231_H
#define GIFEXPORTPROGRESSDELEGATE_T324897231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportProgressDelegate
struct  GifExportProgressDelegate_t324897231  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIFEXPORTPROGRESSDELEGATE_T324897231_H
#ifndef NATIVENOTIFICATIONHANDLER_T1892928298_H
#define NATIVENOTIFICATIONHANDLER_T1892928298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.NativeNotificationHandler
struct  NativeNotificationHandler_t1892928298  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVENOTIFICATIONHANDLER_T1892928298_H
#ifndef GETPENDINGNOTIFICATIONREQUESTSRESPONSE_T1853100515_H
#define GETPENDINGNOTIFICATIONREQUESTSRESPONSE_T1853100515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.GetPendingNotificationRequestsResponse
struct  GetPendingNotificationRequestsResponse_t1853100515  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPENDINGNOTIFICATIONREQUESTSRESPONSE_T1853100515_H
#ifndef IOSNOTIFICATIONREQUEST_T2359474935_H
#define IOSNOTIFICATIONREQUEST_T2359474935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest
struct  iOSNotificationRequest_t2359474935  : public InteropObject_t35158505
{
public:
	// System.Nullable`1<EasyMobile.Internal.Notifications.iOS.iOSNotificationContent> EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mContent
	Nullable_1_t2762948341  ___mContent_1;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mId
	String_t* ___mId_2;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mTitle
	String_t* ___mTitle_3;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mSubtitle
	String_t* ___mSubtitle_4;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mBody
	String_t* ___mBody_5;
	// System.Nullable`1<System.Int32> EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mBadge
	Nullable_1_t378540539  ___mBadge_6;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mUserInfoJson
	String_t* ___mUserInfoJson_7;
	// System.String EasyMobile.Internal.Notifications.iOS.iOSNotificationRequest::mCategoryId
	String_t* ___mCategoryId_8;

public:
	inline static int32_t get_offset_of_mContent_1() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mContent_1)); }
	inline Nullable_1_t2762948341  get_mContent_1() const { return ___mContent_1; }
	inline Nullable_1_t2762948341 * get_address_of_mContent_1() { return &___mContent_1; }
	inline void set_mContent_1(Nullable_1_t2762948341  value)
	{
		___mContent_1 = value;
	}

	inline static int32_t get_offset_of_mId_2() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mId_2)); }
	inline String_t* get_mId_2() const { return ___mId_2; }
	inline String_t** get_address_of_mId_2() { return &___mId_2; }
	inline void set_mId_2(String_t* value)
	{
		___mId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mId_2), value);
	}

	inline static int32_t get_offset_of_mTitle_3() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mTitle_3)); }
	inline String_t* get_mTitle_3() const { return ___mTitle_3; }
	inline String_t** get_address_of_mTitle_3() { return &___mTitle_3; }
	inline void set_mTitle_3(String_t* value)
	{
		___mTitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTitle_3), value);
	}

	inline static int32_t get_offset_of_mSubtitle_4() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mSubtitle_4)); }
	inline String_t* get_mSubtitle_4() const { return ___mSubtitle_4; }
	inline String_t** get_address_of_mSubtitle_4() { return &___mSubtitle_4; }
	inline void set_mSubtitle_4(String_t* value)
	{
		___mSubtitle_4 = value;
		Il2CppCodeGenWriteBarrier((&___mSubtitle_4), value);
	}

	inline static int32_t get_offset_of_mBody_5() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mBody_5)); }
	inline String_t* get_mBody_5() const { return ___mBody_5; }
	inline String_t** get_address_of_mBody_5() { return &___mBody_5; }
	inline void set_mBody_5(String_t* value)
	{
		___mBody_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBody_5), value);
	}

	inline static int32_t get_offset_of_mBadge_6() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mBadge_6)); }
	inline Nullable_1_t378540539  get_mBadge_6() const { return ___mBadge_6; }
	inline Nullable_1_t378540539 * get_address_of_mBadge_6() { return &___mBadge_6; }
	inline void set_mBadge_6(Nullable_1_t378540539  value)
	{
		___mBadge_6 = value;
	}

	inline static int32_t get_offset_of_mUserInfoJson_7() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mUserInfoJson_7)); }
	inline String_t* get_mUserInfoJson_7() const { return ___mUserInfoJson_7; }
	inline String_t** get_address_of_mUserInfoJson_7() { return &___mUserInfoJson_7; }
	inline void set_mUserInfoJson_7(String_t* value)
	{
		___mUserInfoJson_7 = value;
		Il2CppCodeGenWriteBarrier((&___mUserInfoJson_7), value);
	}

	inline static int32_t get_offset_of_mCategoryId_8() { return static_cast<int32_t>(offsetof(iOSNotificationRequest_t2359474935, ___mCategoryId_8)); }
	inline String_t* get_mCategoryId_8() const { return ___mCategoryId_8; }
	inline String_t** get_address_of_mCategoryId_8() { return &___mCategoryId_8; }
	inline void set_mCategoryId_8(String_t* value)
	{
		___mCategoryId_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCategoryId_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNOTIFICATIONREQUEST_T2359474935_H
#ifndef IOSNOTIFICATIONRESPONSE_T3524981010_H
#define IOSNOTIFICATIONRESPONSE_T3524981010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationResponse
struct  iOSNotificationResponse_t3524981010  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNOTIFICATIONRESPONSE_T3524981010_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CLIPPLAYER_T2965428560_H
#define CLIPPLAYER_T2965428560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ClipPlayer
struct  ClipPlayer_t2965428560  : public MonoBehaviour_t3962482529
{
public:
	// EasyMobile.ClipPlayerScaleMode EasyMobile.ClipPlayer::_scaleMode
	int32_t ____scaleMode_4;
	// UnityEngine.Material EasyMobile.ClipPlayer::mat
	Material_t340375123 * ___mat_5;
	// System.Collections.IEnumerator EasyMobile.ClipPlayer::playCoroutine
	RuntimeObject* ___playCoroutine_6;
	// System.Boolean EasyMobile.ClipPlayer::isPaused
	bool ___isPaused_7;

public:
	inline static int32_t get_offset_of__scaleMode_4() { return static_cast<int32_t>(offsetof(ClipPlayer_t2965428560, ____scaleMode_4)); }
	inline int32_t get__scaleMode_4() const { return ____scaleMode_4; }
	inline int32_t* get_address_of__scaleMode_4() { return &____scaleMode_4; }
	inline void set__scaleMode_4(int32_t value)
	{
		____scaleMode_4 = value;
	}

	inline static int32_t get_offset_of_mat_5() { return static_cast<int32_t>(offsetof(ClipPlayer_t2965428560, ___mat_5)); }
	inline Material_t340375123 * get_mat_5() const { return ___mat_5; }
	inline Material_t340375123 ** get_address_of_mat_5() { return &___mat_5; }
	inline void set_mat_5(Material_t340375123 * value)
	{
		___mat_5 = value;
		Il2CppCodeGenWriteBarrier((&___mat_5), value);
	}

	inline static int32_t get_offset_of_playCoroutine_6() { return static_cast<int32_t>(offsetof(ClipPlayer_t2965428560, ___playCoroutine_6)); }
	inline RuntimeObject* get_playCoroutine_6() const { return ___playCoroutine_6; }
	inline RuntimeObject** get_address_of_playCoroutine_6() { return &___playCoroutine_6; }
	inline void set_playCoroutine_6(RuntimeObject* value)
	{
		___playCoroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___playCoroutine_6), value);
	}

	inline static int32_t get_offset_of_isPaused_7() { return static_cast<int32_t>(offsetof(ClipPlayer_t2965428560, ___isPaused_7)); }
	inline bool get_isPaused_7() const { return ___isPaused_7; }
	inline bool* get_address_of_isPaused_7() { return &___isPaused_7; }
	inline void set_isPaused_7(bool value)
	{
		___isPaused_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPLAYER_T2965428560_H
#ifndef CLIPPLAYERUI_T1249565783_H
#define CLIPPLAYERUI_T1249565783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ClipPlayerUI
struct  ClipPlayerUI_t1249565783  : public MonoBehaviour_t3962482529
{
public:
	// EasyMobile.ClipPlayerScaleMode EasyMobile.ClipPlayerUI::_scaleMode
	int32_t ____scaleMode_4;
	// UnityEngine.UI.RawImage EasyMobile.ClipPlayerUI::rawImage
	RawImage_t3182918964 * ___rawImage_5;
	// UnityEngine.RectTransform EasyMobile.ClipPlayerUI::rt
	RectTransform_t3704657025 * ___rt_6;
	// System.Collections.IEnumerator EasyMobile.ClipPlayerUI::playCoroutine
	RuntimeObject* ___playCoroutine_7;
	// System.Boolean EasyMobile.ClipPlayerUI::isPaused
	bool ___isPaused_8;

public:
	inline static int32_t get_offset_of__scaleMode_4() { return static_cast<int32_t>(offsetof(ClipPlayerUI_t1249565783, ____scaleMode_4)); }
	inline int32_t get__scaleMode_4() const { return ____scaleMode_4; }
	inline int32_t* get_address_of__scaleMode_4() { return &____scaleMode_4; }
	inline void set__scaleMode_4(int32_t value)
	{
		____scaleMode_4 = value;
	}

	inline static int32_t get_offset_of_rawImage_5() { return static_cast<int32_t>(offsetof(ClipPlayerUI_t1249565783, ___rawImage_5)); }
	inline RawImage_t3182918964 * get_rawImage_5() const { return ___rawImage_5; }
	inline RawImage_t3182918964 ** get_address_of_rawImage_5() { return &___rawImage_5; }
	inline void set_rawImage_5(RawImage_t3182918964 * value)
	{
		___rawImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___rawImage_5), value);
	}

	inline static int32_t get_offset_of_rt_6() { return static_cast<int32_t>(offsetof(ClipPlayerUI_t1249565783, ___rt_6)); }
	inline RectTransform_t3704657025 * get_rt_6() const { return ___rt_6; }
	inline RectTransform_t3704657025 ** get_address_of_rt_6() { return &___rt_6; }
	inline void set_rt_6(RectTransform_t3704657025 * value)
	{
		___rt_6 = value;
		Il2CppCodeGenWriteBarrier((&___rt_6), value);
	}

	inline static int32_t get_offset_of_playCoroutine_7() { return static_cast<int32_t>(offsetof(ClipPlayerUI_t1249565783, ___playCoroutine_7)); }
	inline RuntimeObject* get_playCoroutine_7() const { return ___playCoroutine_7; }
	inline RuntimeObject** get_address_of_playCoroutine_7() { return &___playCoroutine_7; }
	inline void set_playCoroutine_7(RuntimeObject* value)
	{
		___playCoroutine_7 = value;
		Il2CppCodeGenWriteBarrier((&___playCoroutine_7), value);
	}

	inline static int32_t get_offset_of_isPaused_8() { return static_cast<int32_t>(offsetof(ClipPlayerUI_t1249565783, ___isPaused_8)); }
	inline bool get_isPaused_8() const { return ___isPaused_8; }
	inline bool* get_address_of_isPaused_8() { return &___isPaused_8; }
	inline void set_isPaused_8(bool value)
	{
		___isPaused_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPLAYERUI_T1249565783_H
#ifndef GIF_T1867699976_H
#define GIF_T1867699976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Gif
struct  Gif_t1867699976  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Gif_t1867699976_StaticFields
{
public:
	// EasyMobile.Gif EasyMobile.Gif::_instance
	Gif_t1867699976 * ____instance_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,EasyMobile.GifExportTask> EasyMobile.Gif::gifExportTasks
	Dictionary_2_t4290339053 * ___gifExportTasks_5;
	// System.Int32 EasyMobile.Gif::curExportId
	int32_t ___curExportId_6;
	// System.Action`2<System.Int32,System.Single> EasyMobile.Gif::<>f__mg$cache0
	Action_2_t2623443791 * ___U3CU3Ef__mgU24cache0_7;
	// System.Action`2<System.Int32,System.String> EasyMobile.Gif::<>f__mg$cache1
	Action_2_t3073627706 * ___U3CU3Ef__mgU24cache1_8;
	// System.Action`2<System.Int32,System.Single> EasyMobile.Gif::<>f__mg$cache2
	Action_2_t2623443791 * ___U3CU3Ef__mgU24cache2_9;
	// System.Action`2<System.Int32,System.String> EasyMobile.Gif::<>f__mg$cache3
	Action_2_t3073627706 * ___U3CU3Ef__mgU24cache3_10;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ____instance_4)); }
	inline Gif_t1867699976 * get__instance_4() const { return ____instance_4; }
	inline Gif_t1867699976 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Gif_t1867699976 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of_gifExportTasks_5() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ___gifExportTasks_5)); }
	inline Dictionary_2_t4290339053 * get_gifExportTasks_5() const { return ___gifExportTasks_5; }
	inline Dictionary_2_t4290339053 ** get_address_of_gifExportTasks_5() { return &___gifExportTasks_5; }
	inline void set_gifExportTasks_5(Dictionary_2_t4290339053 * value)
	{
		___gifExportTasks_5 = value;
		Il2CppCodeGenWriteBarrier((&___gifExportTasks_5), value);
	}

	inline static int32_t get_offset_of_curExportId_6() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ___curExportId_6)); }
	inline int32_t get_curExportId_6() const { return ___curExportId_6; }
	inline int32_t* get_address_of_curExportId_6() { return &___curExportId_6; }
	inline void set_curExportId_6(int32_t value)
	{
		___curExportId_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Action_2_t2623443791 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Action_2_t2623443791 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Action_2_t2623443791 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline Action_2_t3073627706 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline Action_2_t3073627706 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(Action_2_t3073627706 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline Action_2_t2623443791 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline Action_2_t2623443791 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(Action_2_t2623443791 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_10() { return static_cast<int32_t>(offsetof(Gif_t1867699976_StaticFields, ___U3CU3Ef__mgU24cache3_10)); }
	inline Action_2_t3073627706 * get_U3CU3Ef__mgU24cache3_10() const { return ___U3CU3Ef__mgU24cache3_10; }
	inline Action_2_t3073627706 ** get_address_of_U3CU3Ef__mgU24cache3_10() { return &___U3CU3Ef__mgU24cache3_10; }
	inline void set_U3CU3Ef__mgU24cache3_10(Action_2_t3073627706 * value)
	{
		___U3CU3Ef__mgU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIF_T1867699976_H
#ifndef GIPHY_T3365606249_H
#define GIPHY_T3365606249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Giphy
struct  Giphy_t3365606249  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Giphy_t3365606249_StaticFields
{
public:
	// EasyMobile.Giphy EasyMobile.Giphy::_instance
	Giphy_t3365606249 * ____instance_4;
	// System.Int32 EasyMobile.Giphy::_apiUseCount
	int32_t ____apiUseCount_8;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Giphy_t3365606249_StaticFields, ____instance_4)); }
	inline Giphy_t3365606249 * get__instance_4() const { return ____instance_4; }
	inline Giphy_t3365606249 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Giphy_t3365606249 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__apiUseCount_8() { return static_cast<int32_t>(offsetof(Giphy_t3365606249_StaticFields, ____apiUseCount_8)); }
	inline int32_t get__apiUseCount_8() const { return ____apiUseCount_8; }
	inline int32_t* get_address_of__apiUseCount_8() { return &____apiUseCount_8; }
	inline void set__apiUseCount_8(int32_t value)
	{
		____apiUseCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIPHY_T3365606249_H
#ifndef INAPPPURCHASING_T3058570446_H
#define INAPPPURCHASING_T3058570446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InAppPurchasing
struct  InAppPurchasing_t3058570446  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InAppPurchasing_t3058570446_StaticFields
{
public:
	// EasyMobile.InAppPurchasing EasyMobile.InAppPurchasing::<Instance>k__BackingField
	InAppPurchasing_t3058570446 * ___U3CInstanceU3Ek__BackingField_4;
	// System.Action EasyMobile.InAppPurchasing::InitializeSucceeded
	Action_t1264377477 * ___InitializeSucceeded_5;
	// System.Action EasyMobile.InAppPurchasing::InitializeFailed
	Action_t1264377477 * ___InitializeFailed_6;
	// System.Action`1<EasyMobile.IAPProduct> EasyMobile.InAppPurchasing::PurchaseCompleted
	Action_1_t1977500546 * ___PurchaseCompleted_7;
	// System.Action`1<EasyMobile.IAPProduct> EasyMobile.InAppPurchasing::PurchaseFailed
	Action_1_t1977500546 * ___PurchaseFailed_8;
	// System.Action`1<EasyMobile.IAPProduct> EasyMobile.InAppPurchasing::PurchaseDeferred
	Action_1_t1977500546 * ___PurchaseDeferred_9;
	// System.Action`1<EasyMobile.IAPProduct> EasyMobile.InAppPurchasing::PromotionalPurchaseIntercepted
	Action_1_t1977500546 * ___PromotionalPurchaseIntercepted_10;
	// System.Action EasyMobile.InAppPurchasing::RestoreCompleted
	Action_t1264377477 * ___RestoreCompleted_11;
	// System.Action EasyMobile.InAppPurchasing::RestoreFailed
	Action_t1264377477 * ___RestoreFailed_12;
	// UnityEngine.Purchasing.ConfigurationBuilder EasyMobile.InAppPurchasing::sBuilder
	ConfigurationBuilder_t1618671084 * ___sBuilder_13;
	// UnityEngine.Purchasing.IStoreController EasyMobile.InAppPurchasing::sStoreController
	RuntimeObject* ___sStoreController_14;
	// UnityEngine.Purchasing.IExtensionProvider EasyMobile.InAppPurchasing::sStoreExtensionProvider
	RuntimeObject* ___sStoreExtensionProvider_15;
	// UnityEngine.Purchasing.IAppleExtensions EasyMobile.InAppPurchasing::sAppleExtensions
	RuntimeObject* ___sAppleExtensions_16;
	// UnityEngine.Purchasing.IGooglePlayStoreExtensions EasyMobile.InAppPurchasing::sGooglePlayStoreExtensions
	RuntimeObject* ___sGooglePlayStoreExtensions_17;
	// UnityEngine.Purchasing.IAmazonExtensions EasyMobile.InAppPurchasing::sAmazonExtensions
	RuntimeObject* ___sAmazonExtensions_18;
	// UnityEngine.Purchasing.ISamsungAppsExtensions EasyMobile.InAppPurchasing::sSamsungAppsExtensions
	RuntimeObject* ___sSamsungAppsExtensions_19;
	// EasyMobile.InAppPurchasing/StoreListener EasyMobile.InAppPurchasing::sStoreListener
	StoreListener_t4250367273 * ___sStoreListener_20;
	// System.Action`1<UnityEngine.Purchasing.Product> EasyMobile.InAppPurchasing::<>f__mg$cache0
	Action_1_t3416877654 * ___U3CU3Ef__mgU24cache0_21;
	// System.Action`1<System.Boolean> EasyMobile.InAppPurchasing::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline InAppPurchasing_t3058570446 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline InAppPurchasing_t3058570446 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(InAppPurchasing_t3058570446 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_InitializeSucceeded_5() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___InitializeSucceeded_5)); }
	inline Action_t1264377477 * get_InitializeSucceeded_5() const { return ___InitializeSucceeded_5; }
	inline Action_t1264377477 ** get_address_of_InitializeSucceeded_5() { return &___InitializeSucceeded_5; }
	inline void set_InitializeSucceeded_5(Action_t1264377477 * value)
	{
		___InitializeSucceeded_5 = value;
		Il2CppCodeGenWriteBarrier((&___InitializeSucceeded_5), value);
	}

	inline static int32_t get_offset_of_InitializeFailed_6() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___InitializeFailed_6)); }
	inline Action_t1264377477 * get_InitializeFailed_6() const { return ___InitializeFailed_6; }
	inline Action_t1264377477 ** get_address_of_InitializeFailed_6() { return &___InitializeFailed_6; }
	inline void set_InitializeFailed_6(Action_t1264377477 * value)
	{
		___InitializeFailed_6 = value;
		Il2CppCodeGenWriteBarrier((&___InitializeFailed_6), value);
	}

	inline static int32_t get_offset_of_PurchaseCompleted_7() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___PurchaseCompleted_7)); }
	inline Action_1_t1977500546 * get_PurchaseCompleted_7() const { return ___PurchaseCompleted_7; }
	inline Action_1_t1977500546 ** get_address_of_PurchaseCompleted_7() { return &___PurchaseCompleted_7; }
	inline void set_PurchaseCompleted_7(Action_1_t1977500546 * value)
	{
		___PurchaseCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___PurchaseCompleted_7), value);
	}

	inline static int32_t get_offset_of_PurchaseFailed_8() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___PurchaseFailed_8)); }
	inline Action_1_t1977500546 * get_PurchaseFailed_8() const { return ___PurchaseFailed_8; }
	inline Action_1_t1977500546 ** get_address_of_PurchaseFailed_8() { return &___PurchaseFailed_8; }
	inline void set_PurchaseFailed_8(Action_1_t1977500546 * value)
	{
		___PurchaseFailed_8 = value;
		Il2CppCodeGenWriteBarrier((&___PurchaseFailed_8), value);
	}

	inline static int32_t get_offset_of_PurchaseDeferred_9() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___PurchaseDeferred_9)); }
	inline Action_1_t1977500546 * get_PurchaseDeferred_9() const { return ___PurchaseDeferred_9; }
	inline Action_1_t1977500546 ** get_address_of_PurchaseDeferred_9() { return &___PurchaseDeferred_9; }
	inline void set_PurchaseDeferred_9(Action_1_t1977500546 * value)
	{
		___PurchaseDeferred_9 = value;
		Il2CppCodeGenWriteBarrier((&___PurchaseDeferred_9), value);
	}

	inline static int32_t get_offset_of_PromotionalPurchaseIntercepted_10() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___PromotionalPurchaseIntercepted_10)); }
	inline Action_1_t1977500546 * get_PromotionalPurchaseIntercepted_10() const { return ___PromotionalPurchaseIntercepted_10; }
	inline Action_1_t1977500546 ** get_address_of_PromotionalPurchaseIntercepted_10() { return &___PromotionalPurchaseIntercepted_10; }
	inline void set_PromotionalPurchaseIntercepted_10(Action_1_t1977500546 * value)
	{
		___PromotionalPurchaseIntercepted_10 = value;
		Il2CppCodeGenWriteBarrier((&___PromotionalPurchaseIntercepted_10), value);
	}

	inline static int32_t get_offset_of_RestoreCompleted_11() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___RestoreCompleted_11)); }
	inline Action_t1264377477 * get_RestoreCompleted_11() const { return ___RestoreCompleted_11; }
	inline Action_t1264377477 ** get_address_of_RestoreCompleted_11() { return &___RestoreCompleted_11; }
	inline void set_RestoreCompleted_11(Action_t1264377477 * value)
	{
		___RestoreCompleted_11 = value;
		Il2CppCodeGenWriteBarrier((&___RestoreCompleted_11), value);
	}

	inline static int32_t get_offset_of_RestoreFailed_12() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___RestoreFailed_12)); }
	inline Action_t1264377477 * get_RestoreFailed_12() const { return ___RestoreFailed_12; }
	inline Action_t1264377477 ** get_address_of_RestoreFailed_12() { return &___RestoreFailed_12; }
	inline void set_RestoreFailed_12(Action_t1264377477 * value)
	{
		___RestoreFailed_12 = value;
		Il2CppCodeGenWriteBarrier((&___RestoreFailed_12), value);
	}

	inline static int32_t get_offset_of_sBuilder_13() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sBuilder_13)); }
	inline ConfigurationBuilder_t1618671084 * get_sBuilder_13() const { return ___sBuilder_13; }
	inline ConfigurationBuilder_t1618671084 ** get_address_of_sBuilder_13() { return &___sBuilder_13; }
	inline void set_sBuilder_13(ConfigurationBuilder_t1618671084 * value)
	{
		___sBuilder_13 = value;
		Il2CppCodeGenWriteBarrier((&___sBuilder_13), value);
	}

	inline static int32_t get_offset_of_sStoreController_14() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sStoreController_14)); }
	inline RuntimeObject* get_sStoreController_14() const { return ___sStoreController_14; }
	inline RuntimeObject** get_address_of_sStoreController_14() { return &___sStoreController_14; }
	inline void set_sStoreController_14(RuntimeObject* value)
	{
		___sStoreController_14 = value;
		Il2CppCodeGenWriteBarrier((&___sStoreController_14), value);
	}

	inline static int32_t get_offset_of_sStoreExtensionProvider_15() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sStoreExtensionProvider_15)); }
	inline RuntimeObject* get_sStoreExtensionProvider_15() const { return ___sStoreExtensionProvider_15; }
	inline RuntimeObject** get_address_of_sStoreExtensionProvider_15() { return &___sStoreExtensionProvider_15; }
	inline void set_sStoreExtensionProvider_15(RuntimeObject* value)
	{
		___sStoreExtensionProvider_15 = value;
		Il2CppCodeGenWriteBarrier((&___sStoreExtensionProvider_15), value);
	}

	inline static int32_t get_offset_of_sAppleExtensions_16() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sAppleExtensions_16)); }
	inline RuntimeObject* get_sAppleExtensions_16() const { return ___sAppleExtensions_16; }
	inline RuntimeObject** get_address_of_sAppleExtensions_16() { return &___sAppleExtensions_16; }
	inline void set_sAppleExtensions_16(RuntimeObject* value)
	{
		___sAppleExtensions_16 = value;
		Il2CppCodeGenWriteBarrier((&___sAppleExtensions_16), value);
	}

	inline static int32_t get_offset_of_sGooglePlayStoreExtensions_17() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sGooglePlayStoreExtensions_17)); }
	inline RuntimeObject* get_sGooglePlayStoreExtensions_17() const { return ___sGooglePlayStoreExtensions_17; }
	inline RuntimeObject** get_address_of_sGooglePlayStoreExtensions_17() { return &___sGooglePlayStoreExtensions_17; }
	inline void set_sGooglePlayStoreExtensions_17(RuntimeObject* value)
	{
		___sGooglePlayStoreExtensions_17 = value;
		Il2CppCodeGenWriteBarrier((&___sGooglePlayStoreExtensions_17), value);
	}

	inline static int32_t get_offset_of_sAmazonExtensions_18() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sAmazonExtensions_18)); }
	inline RuntimeObject* get_sAmazonExtensions_18() const { return ___sAmazonExtensions_18; }
	inline RuntimeObject** get_address_of_sAmazonExtensions_18() { return &___sAmazonExtensions_18; }
	inline void set_sAmazonExtensions_18(RuntimeObject* value)
	{
		___sAmazonExtensions_18 = value;
		Il2CppCodeGenWriteBarrier((&___sAmazonExtensions_18), value);
	}

	inline static int32_t get_offset_of_sSamsungAppsExtensions_19() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sSamsungAppsExtensions_19)); }
	inline RuntimeObject* get_sSamsungAppsExtensions_19() const { return ___sSamsungAppsExtensions_19; }
	inline RuntimeObject** get_address_of_sSamsungAppsExtensions_19() { return &___sSamsungAppsExtensions_19; }
	inline void set_sSamsungAppsExtensions_19(RuntimeObject* value)
	{
		___sSamsungAppsExtensions_19 = value;
		Il2CppCodeGenWriteBarrier((&___sSamsungAppsExtensions_19), value);
	}

	inline static int32_t get_offset_of_sStoreListener_20() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___sStoreListener_20)); }
	inline StoreListener_t4250367273 * get_sStoreListener_20() const { return ___sStoreListener_20; }
	inline StoreListener_t4250367273 ** get_address_of_sStoreListener_20() { return &___sStoreListener_20; }
	inline void set_sStoreListener_20(StoreListener_t4250367273 * value)
	{
		___sStoreListener_20 = value;
		Il2CppCodeGenWriteBarrier((&___sStoreListener_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_21() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___U3CU3Ef__mgU24cache0_21)); }
	inline Action_1_t3416877654 * get_U3CU3Ef__mgU24cache0_21() const { return ___U3CU3Ef__mgU24cache0_21; }
	inline Action_1_t3416877654 ** get_address_of_U3CU3Ef__mgU24cache0_21() { return &___U3CU3Ef__mgU24cache0_21; }
	inline void set_U3CU3Ef__mgU24cache0_21(Action_1_t3416877654 * value)
	{
		___U3CU3Ef__mgU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(InAppPurchasing_t3058570446_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INAPPPURCHASING_T3058570446_H
#ifndef IOSNOTIFICATIONLISTENER_T2070489240_H
#define IOSNOTIFICATIONLISTENER_T2070489240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOSNotificationListener
struct  iOSNotificationListener_t2070489240  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<EasyMobile.LocalNotification> EasyMobile.Internal.Notifications.iOSNotificationListener::LocalNotificationOpened
	Action_1_t516386935 * ___LocalNotificationOpened_6;
	// System.Action`1<EasyMobile.RemoteNotification> EasyMobile.Internal.Notifications.iOSNotificationListener::RemoteNotificationOpened
	Action_1_t1859537056 * ___RemoteNotificationOpened_7;

public:
	inline static int32_t get_offset_of_LocalNotificationOpened_6() { return static_cast<int32_t>(offsetof(iOSNotificationListener_t2070489240, ___LocalNotificationOpened_6)); }
	inline Action_1_t516386935 * get_LocalNotificationOpened_6() const { return ___LocalNotificationOpened_6; }
	inline Action_1_t516386935 ** get_address_of_LocalNotificationOpened_6() { return &___LocalNotificationOpened_6; }
	inline void set_LocalNotificationOpened_6(Action_1_t516386935 * value)
	{
		___LocalNotificationOpened_6 = value;
		Il2CppCodeGenWriteBarrier((&___LocalNotificationOpened_6), value);
	}

	inline static int32_t get_offset_of_RemoteNotificationOpened_7() { return static_cast<int32_t>(offsetof(iOSNotificationListener_t2070489240, ___RemoteNotificationOpened_7)); }
	inline Action_1_t1859537056 * get_RemoteNotificationOpened_7() const { return ___RemoteNotificationOpened_7; }
	inline Action_1_t1859537056 ** get_address_of_RemoteNotificationOpened_7() { return &___RemoteNotificationOpened_7; }
	inline void set_RemoteNotificationOpened_7(Action_1_t1859537056 * value)
	{
		___RemoteNotificationOpened_7 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteNotificationOpened_7), value);
	}
};

struct iOSNotificationListener_t2070489240_StaticFields
{
public:
	// EasyMobile.Internal.Notifications.iOSNotificationListener EasyMobile.Internal.Notifications.iOSNotificationListener::sInstance
	iOSNotificationListener_t2070489240 * ___sInstance_5;
	// System.Func`2<System.IntPtr,EasyMobile.Internal.Notifications.iOS.iOSNotificationResponse> EasyMobile.Internal.Notifications.iOSNotificationListener::<>f__mg$cache0
	Func_2_t4061431951 * ___U3CU3Ef__mgU24cache0_8;
	// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetNotificationResponseCallback EasyMobile.Internal.Notifications.iOSNotificationListener::<>f__mg$cache1
	GetNotificationResponseCallback_t3346532913 * ___U3CU3Ef__mgU24cache1_9;

public:
	inline static int32_t get_offset_of_sInstance_5() { return static_cast<int32_t>(offsetof(iOSNotificationListener_t2070489240_StaticFields, ___sInstance_5)); }
	inline iOSNotificationListener_t2070489240 * get_sInstance_5() const { return ___sInstance_5; }
	inline iOSNotificationListener_t2070489240 ** get_address_of_sInstance_5() { return &___sInstance_5; }
	inline void set_sInstance_5(iOSNotificationListener_t2070489240 * value)
	{
		___sInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(iOSNotificationListener_t2070489240_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline Func_2_t4061431951 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline Func_2_t4061431951 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(Func_2_t4061431951 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_9() { return static_cast<int32_t>(offsetof(iOSNotificationListener_t2070489240_StaticFields, ___U3CU3Ef__mgU24cache1_9)); }
	inline GetNotificationResponseCallback_t3346532913 * get_U3CU3Ef__mgU24cache1_9() const { return ___U3CU3Ef__mgU24cache1_9; }
	inline GetNotificationResponseCallback_t3346532913 ** get_address_of_U3CU3Ef__mgU24cache1_9() { return &___U3CU3Ef__mgU24cache1_9; }
	inline void set_U3CU3Ef__mgU24cache1_9(GetNotificationResponseCallback_t3346532913 * value)
	{
		___U3CU3Ef__mgU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNOTIFICATIONLISTENER_T2070489240_H
#ifndef ALERTPOPUP_T1689515052_H
#define ALERTPOPUP_T1689515052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NativeUI/AlertPopup
struct  AlertPopup_t1689515052  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<System.Int32> EasyMobile.NativeUI/AlertPopup::OnComplete
	Action_1_t3123413348 * ___OnComplete_5;

public:
	inline static int32_t get_offset_of_OnComplete_5() { return static_cast<int32_t>(offsetof(AlertPopup_t1689515052, ___OnComplete_5)); }
	inline Action_1_t3123413348 * get_OnComplete_5() const { return ___OnComplete_5; }
	inline Action_1_t3123413348 ** get_address_of_OnComplete_5() { return &___OnComplete_5; }
	inline void set_OnComplete_5(Action_1_t3123413348 * value)
	{
		___OnComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_5), value);
	}
};

struct AlertPopup_t1689515052_StaticFields
{
public:
	// EasyMobile.NativeUI/AlertPopup EasyMobile.NativeUI/AlertPopup::<Instance>k__BackingField
	AlertPopup_t1689515052 * ___U3CInstanceU3Ek__BackingField_4;
	// System.String EasyMobile.NativeUI/AlertPopup::ALERT_GAMEOBJECT
	String_t* ___ALERT_GAMEOBJECT_6;
	// System.Action`1<System.Int32> EasyMobile.NativeUI/AlertPopup::<>f__am$cache0
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AlertPopup_t1689515052_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline AlertPopup_t1689515052 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline AlertPopup_t1689515052 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(AlertPopup_t1689515052 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_ALERT_GAMEOBJECT_6() { return static_cast<int32_t>(offsetof(AlertPopup_t1689515052_StaticFields, ___ALERT_GAMEOBJECT_6)); }
	inline String_t* get_ALERT_GAMEOBJECT_6() const { return ___ALERT_GAMEOBJECT_6; }
	inline String_t** get_address_of_ALERT_GAMEOBJECT_6() { return &___ALERT_GAMEOBJECT_6; }
	inline void set_ALERT_GAMEOBJECT_6(String_t* value)
	{
		___ALERT_GAMEOBJECT_6 = value;
		Il2CppCodeGenWriteBarrier((&___ALERT_GAMEOBJECT_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(AlertPopup_t1689515052_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTPOPUP_T1689515052_H
#ifndef RECORDER_T3009289404_H
#define RECORDER_T3009289404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Recorder
struct  Recorder_t3009289404  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean EasyMobile.Recorder::_autoHeight
	bool ____autoHeight_4;
	// System.Int32 EasyMobile.Recorder::_width
	int32_t ____width_5;
	// System.Int32 EasyMobile.Recorder::_height
	int32_t ____height_6;
	// System.Int32 EasyMobile.Recorder::_framePerSecond
	int32_t ____framePerSecond_7;
	// System.Single EasyMobile.Recorder::_length
	float ____length_8;
	// EasyMobile.Recorder/RecorderState EasyMobile.Recorder::_state
	int32_t ____state_9;
	// UnityEngine.Camera EasyMobile.Recorder::_targetCamera
	Camera_t4157153871 * ____targetCamera_10;
	// System.Int32 EasyMobile.Recorder::maxFrameCount
	int32_t ___maxFrameCount_11;
	// System.Single EasyMobile.Recorder::pastTime
	float ___pastTime_12;
	// System.Single EasyMobile.Recorder::timePerFrame
	float ___timePerFrame_13;
	// System.Collections.Generic.Queue`1<UnityEngine.RenderTexture> EasyMobile.Recorder::recordedFrames
	Queue_1_t1955146927 * ___recordedFrames_14;
	// Moments.ReflectionUtils`1<EasyMobile.Recorder> EasyMobile.Recorder::reflectionUtils
	ReflectionUtils_1_t676350948 * ___reflectionUtils_15;

public:
	inline static int32_t get_offset_of__autoHeight_4() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____autoHeight_4)); }
	inline bool get__autoHeight_4() const { return ____autoHeight_4; }
	inline bool* get_address_of__autoHeight_4() { return &____autoHeight_4; }
	inline void set__autoHeight_4(bool value)
	{
		____autoHeight_4 = value;
	}

	inline static int32_t get_offset_of__width_5() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____width_5)); }
	inline int32_t get__width_5() const { return ____width_5; }
	inline int32_t* get_address_of__width_5() { return &____width_5; }
	inline void set__width_5(int32_t value)
	{
		____width_5 = value;
	}

	inline static int32_t get_offset_of__height_6() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____height_6)); }
	inline int32_t get__height_6() const { return ____height_6; }
	inline int32_t* get_address_of__height_6() { return &____height_6; }
	inline void set__height_6(int32_t value)
	{
		____height_6 = value;
	}

	inline static int32_t get_offset_of__framePerSecond_7() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____framePerSecond_7)); }
	inline int32_t get__framePerSecond_7() const { return ____framePerSecond_7; }
	inline int32_t* get_address_of__framePerSecond_7() { return &____framePerSecond_7; }
	inline void set__framePerSecond_7(int32_t value)
	{
		____framePerSecond_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____length_8)); }
	inline float get__length_8() const { return ____length_8; }
	inline float* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(float value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____state_9)); }
	inline int32_t get__state_9() const { return ____state_9; }
	inline int32_t* get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(int32_t value)
	{
		____state_9 = value;
	}

	inline static int32_t get_offset_of__targetCamera_10() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ____targetCamera_10)); }
	inline Camera_t4157153871 * get__targetCamera_10() const { return ____targetCamera_10; }
	inline Camera_t4157153871 ** get_address_of__targetCamera_10() { return &____targetCamera_10; }
	inline void set__targetCamera_10(Camera_t4157153871 * value)
	{
		____targetCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&____targetCamera_10), value);
	}

	inline static int32_t get_offset_of_maxFrameCount_11() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ___maxFrameCount_11)); }
	inline int32_t get_maxFrameCount_11() const { return ___maxFrameCount_11; }
	inline int32_t* get_address_of_maxFrameCount_11() { return &___maxFrameCount_11; }
	inline void set_maxFrameCount_11(int32_t value)
	{
		___maxFrameCount_11 = value;
	}

	inline static int32_t get_offset_of_pastTime_12() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ___pastTime_12)); }
	inline float get_pastTime_12() const { return ___pastTime_12; }
	inline float* get_address_of_pastTime_12() { return &___pastTime_12; }
	inline void set_pastTime_12(float value)
	{
		___pastTime_12 = value;
	}

	inline static int32_t get_offset_of_timePerFrame_13() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ___timePerFrame_13)); }
	inline float get_timePerFrame_13() const { return ___timePerFrame_13; }
	inline float* get_address_of_timePerFrame_13() { return &___timePerFrame_13; }
	inline void set_timePerFrame_13(float value)
	{
		___timePerFrame_13 = value;
	}

	inline static int32_t get_offset_of_recordedFrames_14() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ___recordedFrames_14)); }
	inline Queue_1_t1955146927 * get_recordedFrames_14() const { return ___recordedFrames_14; }
	inline Queue_1_t1955146927 ** get_address_of_recordedFrames_14() { return &___recordedFrames_14; }
	inline void set_recordedFrames_14(Queue_1_t1955146927 * value)
	{
		___recordedFrames_14 = value;
		Il2CppCodeGenWriteBarrier((&___recordedFrames_14), value);
	}

	inline static int32_t get_offset_of_reflectionUtils_15() { return static_cast<int32_t>(offsetof(Recorder_t3009289404, ___reflectionUtils_15)); }
	inline ReflectionUtils_1_t676350948 * get_reflectionUtils_15() const { return ___reflectionUtils_15; }
	inline ReflectionUtils_1_t676350948 ** get_address_of_reflectionUtils_15() { return &___reflectionUtils_15; }
	inline void set_reflectionUtils_15(ReflectionUtils_1_t676350948 * value)
	{
		___reflectionUtils_15 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionUtils_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDER_T3009289404_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (ResolveConflictingSavedGamesCallback_t4045204290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (DeleteSavedGameCallback_t2032035250), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (iOSGKSavedGame_t2037893409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (OpenSavedGameResponse_t960542049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (SaveGameDataResponse_t3887393376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (LoadSavedGameDataResponse_t10080320), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (FetchSavedGamesResponse_t3634168769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (ResolveConflictResponse_t1723489802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (DeleteSavedGameResponse_t1617544370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (iOSConflictResolutionStrategy_t1704602082)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4809[3] = 
{
	iOSConflictResolutionStrategy_t1704602082::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (iOSConflictCallback_t756019127), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (iOSSavedGameClient_t251267421), -1, sizeof(iOSSavedGameClient_t251267421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4812[12] = 
{
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_1(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_2(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_3(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_4(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_5(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_6(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_7(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_8(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_9(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_10(),
	iOSSavedGameClient_t251267421_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (NativeConflictResolver_t651818787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4813[4] = 
{
	NativeConflictResolver_t651818787::get_offset_of__base_0(),
	NativeConflictResolver_t651818787::get_offset_of__remote_1(),
	NativeConflictResolver_t651818787::get_offset_of__completeCallback_2(),
	NativeConflictResolver_t651818787::get_offset_of__repeatResolveConflicts_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (U3CChooseGKSavedGameU3Ec__AnonStorey0_t404845350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4814[2] = 
{
	U3CChooseGKSavedGameU3Ec__AnonStorey0_t404845350::get_offset_of_conflictingGames_0(),
	U3CChooseGKSavedGameU3Ec__AnonStorey0_t404845350::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (Prefetcher_t1369232627), -1, sizeof(Prefetcher_t1369232627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4815[9] = 
{
	Prefetcher_t1369232627::get_offset_of__lock_0(),
	Prefetcher_t1369232627::get_offset_of__baseDataFetched_1(),
	Prefetcher_t1369232627::get_offset_of__baseData_2(),
	Prefetcher_t1369232627::get_offset_of__remoteDataFetched_3(),
	Prefetcher_t1369232627::get_offset_of__remoteData_4(),
	Prefetcher_t1369232627::get_offset_of__completedCallback_5(),
	Prefetcher_t1369232627::get_offset_of__dataFetchedCallback_6(),
	Prefetcher_t1369232627_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	Prefetcher_t1369232627_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey0_t3585423550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4816[1] = 
{
	U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey0_t3585423550::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (U3COpenWithManualConflictResolutionU3Ec__AnonStorey1_t1856166065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4817[2] = 
{
	U3COpenWithManualConflictResolutionU3Ec__AnonStorey1_t1856166065::get_offset_of_resolverFunction_0(),
	U3COpenWithManualConflictResolutionU3Ec__AnonStorey1_t1856166065::get_offset_of_completedCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (U3CReadSavedGameDataU3Ec__AnonStorey2_t89225513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4818[2] = 
{
	U3CReadSavedGameDataU3Ec__AnonStorey2_t89225513::get_offset_of_callback_0(),
	U3CReadSavedGameDataU3Ec__AnonStorey2_t89225513::get_offset_of_savedGame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (U3CWriteSavedGameDataU3Ec__AnonStorey3_t254795096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4819[1] = 
{
	U3CWriteSavedGameDataU3Ec__AnonStorey3_t254795096::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (U3CFetchAllSavedGamesU3Ec__AnonStorey4_t3113088512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4820[1] = 
{
	U3CFetchAllSavedGamesU3Ec__AnonStorey4_t3113088512::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (U3CDeleteSavedGameU3Ec__AnonStorey5_t4129970464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4821[1] = 
{
	U3CDeleteSavedGameU3Ec__AnonStorey5_t4129970464::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (U3CInternalOpenWithAutomaticConflictResolutionU3Ec__AnonStorey6_t440150218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4822[2] = 
{
	U3CInternalOpenWithAutomaticConflictResolutionU3Ec__AnonStorey6_t440150218::get_offset_of_resolutionStrategy_0(),
	U3CInternalOpenWithAutomaticConflictResolutionU3Ec__AnonStorey6_t440150218::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634), -1, sizeof(U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4823[4] = 
{
	U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634::get_offset_of_completedCallback_0(),
	U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634::get_offset_of_prefetchDataOnConflict_1(),
	U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634::get_offset_of_conflictCallback_2(),
	U3CInternalOpenWithManualConflictResolutionU3Ec__AnonStorey7_t2168430634_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (U3CInternalOpenSavedGameU3Ec__AnonStorey8_t3579769808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4824[1] = 
{
	U3CInternalOpenSavedGameU3Ec__AnonStorey8_t3579769808::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4825[6] = 
{
	U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396::get_offset_of_prefetchData_0(),
	U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396::get_offset_of_conflictCallback_1(),
	U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396::get_offset_of_completedCallback_2(),
	U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396::get_offset_of_resolver_3(),
	U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396::get_offset_of_baseGame_4(),
	U3CConflictResolvingLoopU3Ec__AnonStorey9_t1617656396::get_offset_of_remoteGame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (U3CInternalSaveGameDataU3Ec__AnonStoreyA_t1786357441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4826[1] = 
{
	U3CInternalSaveGameDataU3Ec__AnonStoreyA_t1786357441::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (U3CInternalLoadSavedGameDataU3Ec__AnonStoreyB_t233618823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4827[1] = 
{
	U3CInternalLoadSavedGameDataU3Ec__AnonStoreyB_t233618823::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (U3CInternalFetchSavedGamesU3Ec__AnonStoreyC_t1277696669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4828[1] = 
{
	U3CInternalFetchSavedGamesU3Ec__AnonStoreyC_t1277696669::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (U3CInternalResolveConflictingSavedGamesU3Ec__AnonStoreyD_t990894612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4829[1] = 
{
	U3CInternalResolveConflictingSavedGamesU3Ec__AnonStoreyD_t990894612::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (U3CInternalDeleteSavedGameU3Ec__AnonStoreyE_t3357283351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4830[1] = 
{
	U3CInternalDeleteSavedGameU3Ec__AnonStoreyE_t3357283351::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (UnsupportedSavedGameClient_t1182319790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4831[2] = 
{
	0,
	UnsupportedSavedGameClient_t1182319790::get_offset_of_mMessage_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (SavedGame_t947814848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4832[1] = 
{
	SavedGame_t947814848::get_offset_of_U3CGKSavedGameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (SavedGameInfoUpdate_t1021132738)+ sizeof (RuntimeObject), sizeof(SavedGameInfoUpdate_t1021132738_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4833[6] = 
{
	SavedGameInfoUpdate_t1021132738::get_offset_of__descriptionUpdated_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameInfoUpdate_t1021132738::get_offset_of__newDescription_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameInfoUpdate_t1021132738::get_offset_of__coverImageUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameInfoUpdate_t1021132738::get_offset_of__newPngCoverImage_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameInfoUpdate_t1021132738::get_offset_of__playedTimeUpdated_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameInfoUpdate_t1021132738::get_offset_of__newPlayedTime_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (Builder_t2048762101)+ sizeof (RuntimeObject), sizeof(Builder_t2048762101_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4834[6] = 
{
	Builder_t2048762101::get_offset_of__descriptionUpdated_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t2048762101::get_offset_of__newDescription_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t2048762101::get_offset_of__coverImageUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t2048762101::get_offset_of__newPngCoverImage_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t2048762101::get_offset_of__playedTimeUpdated_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t2048762101::get_offset_of__newPlayedTime_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (AnimatedClip_t619307834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4835[6] = 
{
	AnimatedClip_t619307834::get_offset_of_U3CWidthU3Ek__BackingField_0(),
	AnimatedClip_t619307834::get_offset_of_U3CHeightU3Ek__BackingField_1(),
	AnimatedClip_t619307834::get_offset_of_U3CFramePerSecondU3Ek__BackingField_2(),
	AnimatedClip_t619307834::get_offset_of_U3CLengthU3Ek__BackingField_3(),
	AnimatedClip_t619307834::get_offset_of_U3CFramesU3Ek__BackingField_4(),
	AnimatedClip_t619307834::get_offset_of_isDisposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (ClipPlayer_t2965428560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4836[4] = 
{
	ClipPlayer_t2965428560::get_offset_of__scaleMode_4(),
	ClipPlayer_t2965428560::get_offset_of_mat_5(),
	ClipPlayer_t2965428560::get_offset_of_playCoroutine_6(),
	ClipPlayer_t2965428560::get_offset_of_isPaused_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (U3CCRPlayU3Ec__Iterator0_t643632238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4837[10] = 
{
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_clip_0(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U3CtimePerFrameU3E__0_1(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U3ChasDelayedU3E__0_2(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U3CiU3E__1_3(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_startDelay_4(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_loop_5(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U24this_6(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U24current_7(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U24disposing_8(),
	U3CCRPlayU3Ec__Iterator0_t643632238::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (ClipPlayerScaleMode_t2357387684)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4838[4] = 
{
	ClipPlayerScaleMode_t2357387684::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (ClipPlayerUI_t1249565783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4839[5] = 
{
	ClipPlayerUI_t1249565783::get_offset_of__scaleMode_4(),
	ClipPlayerUI_t1249565783::get_offset_of_rawImage_5(),
	ClipPlayerUI_t1249565783::get_offset_of_rt_6(),
	ClipPlayerUI_t1249565783::get_offset_of_playCoroutine_7(),
	ClipPlayerUI_t1249565783::get_offset_of_isPaused_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (U3CCRPlayU3Ec__Iterator0_t1429219391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4840[10] = 
{
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_clip_0(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U3CtimePerFrameU3E__0_1(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U3ChasDelayedU3E__0_2(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U3CiU3E__1_3(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_startDelay_4(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_loop_5(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U24this_6(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U24current_7(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U24disposing_8(),
	U3CCRPlayU3Ec__Iterator0_t1429219391::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (Gif_t1867699976), -1, sizeof(Gif_t1867699976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4841[7] = 
{
	Gif_t1867699976_StaticFields::get_offset_of__instance_4(),
	Gif_t1867699976_StaticFields::get_offset_of_gifExportTasks_5(),
	Gif_t1867699976_StaticFields::get_offset_of_curExportId_6(),
	Gif_t1867699976_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	Gif_t1867699976_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	Gif_t1867699976_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	Gif_t1867699976_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (U3CCRExportGifU3Ec__Iterator0_t662514209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4842[18] = 
{
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_loop_0(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_quality_1(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CsampleFacU3E__0_2(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CfolderU3E__0_3(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_filename_4(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CfilepathU3E__0_5(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CexportTaskU3E__0_6(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_clip_7(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_exportProgressCallback_8(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_exportCompletedCallback_9(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_threadPriority_10(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CtempU3E__0_11(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CiU3E__1_12(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CsourceU3E__2_13(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U3CprogressU3E__2_14(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U24current_15(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U24disposing_16(),
	U3CCRExportGifU3Ec__Iterator0_t662514209::get_offset_of_U24PC_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (GifExportTask_t1106658426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4843[12] = 
{
	GifExportTask_t1106658426::get_offset_of_taskId_0(),
	GifExportTask_t1106658426::get_offset_of_clip_1(),
	GifExportTask_t1106658426::get_offset_of_imageData_2(),
	GifExportTask_t1106658426::get_offset_of_filepath_3(),
	GifExportTask_t1106658426::get_offset_of_loop_4(),
	GifExportTask_t1106658426::get_offset_of_sampleFac_5(),
	GifExportTask_t1106658426::get_offset_of_isExporting_6(),
	GifExportTask_t1106658426::get_offset_of_isDone_7(),
	GifExportTask_t1106658426::get_offset_of_progress_8(),
	GifExportTask_t1106658426::get_offset_of_exportProgressCallback_9(),
	GifExportTask_t1106658426::get_offset_of_exportCompletedCallback_10(),
	GifExportTask_t1106658426::get_offset_of_workerPriority_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (Giphy_t3365606249), -1, sizeof(Giphy_t3365606249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4844[5] = 
{
	Giphy_t3365606249_StaticFields::get_offset_of__instance_4(),
	0,
	0,
	0,
	Giphy_t3365606249_StaticFields::get_offset_of__apiUseCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (UploadSuccessResponse_t411033403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4845[1] = 
{
	UploadSuccessResponse_t411033403::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (UploadSuccessData_t1863379328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4846[1] = 
{
	UploadSuccessData_t1863379328::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (U3CCRUploadU3Ec__Iterator0_t1799629917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4847[9] = 
{
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_uploadPath_0(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_form_1(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_U3CwwwU3E__0_2(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_uploadProgressCB_3(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_uploadCompletedCB_4(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_uploadFailedCB_5(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_U24current_6(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_U24disposing_7(),
	U3CCRUploadU3Ec__Iterator0_t1799629917::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (GiphyUploadParams_t2791732282)+ sizeof (RuntimeObject), sizeof(GiphyUploadParams_t2791732282_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4848[5] = 
{
	GiphyUploadParams_t2791732282::get_offset_of_localImagePath_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GiphyUploadParams_t2791732282::get_offset_of_sourceImageUrl_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GiphyUploadParams_t2791732282::get_offset_of_tags_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GiphyUploadParams_t2791732282::get_offset_of_sourcePostUrl_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GiphyUploadParams_t2791732282::get_offset_of_isHidden_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (GifEncoder_t60958606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4850[18] = 
{
	GifEncoder_t60958606::get_offset_of_m_Width_0(),
	GifEncoder_t60958606::get_offset_of_m_Height_1(),
	GifEncoder_t60958606::get_offset_of_m_Repeat_2(),
	GifEncoder_t60958606::get_offset_of_m_FrameDelay_3(),
	GifEncoder_t60958606::get_offset_of_m_HasStarted_4(),
	GifEncoder_t60958606::get_offset_of_m_FileStream_5(),
	GifEncoder_t60958606::get_offset_of_m_CurrentFrame_6(),
	GifEncoder_t60958606::get_offset_of_m_Pixels_7(),
	GifEncoder_t60958606::get_offset_of_m_IndexedPixels_8(),
	GifEncoder_t60958606::get_offset_of_m_ColorDepth_9(),
	GifEncoder_t60958606::get_offset_of_m_ColorTab_10(),
	GifEncoder_t60958606::get_offset_of_m_UsedEntry_11(),
	GifEncoder_t60958606::get_offset_of_m_PaletteSize_12(),
	GifEncoder_t60958606::get_offset_of_m_DisposalCode_13(),
	GifEncoder_t60958606::get_offset_of_m_ShouldCloseStream_14(),
	GifEncoder_t60958606::get_offset_of_m_IsFirstFrame_15(),
	GifEncoder_t60958606::get_offset_of_m_IsSizeSet_16(),
	GifEncoder_t60958606::get_offset_of_m_SampleInterval_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (GifFrame_t2916230237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4851[3] = 
{
	GifFrame_t2916230237::get_offset_of_Width_0(),
	GifFrame_t2916230237::get_offset_of_Height_1(),
	GifFrame_t2916230237::get_offset_of_Data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (LzwEncoder_t293885915), -1, sizeof(LzwEncoder_t293885915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4852[23] = 
{
	LzwEncoder_t293885915_StaticFields::get_offset_of_EOF_0(),
	LzwEncoder_t293885915::get_offset_of_pixAry_1(),
	LzwEncoder_t293885915::get_offset_of_initCodeSize_2(),
	LzwEncoder_t293885915::get_offset_of_curPixel_3(),
	LzwEncoder_t293885915_StaticFields::get_offset_of_BITS_4(),
	LzwEncoder_t293885915_StaticFields::get_offset_of_HSIZE_5(),
	LzwEncoder_t293885915::get_offset_of_n_bits_6(),
	LzwEncoder_t293885915::get_offset_of_maxbits_7(),
	LzwEncoder_t293885915::get_offset_of_maxcode_8(),
	LzwEncoder_t293885915::get_offset_of_maxmaxcode_9(),
	LzwEncoder_t293885915::get_offset_of_htab_10(),
	LzwEncoder_t293885915::get_offset_of_codetab_11(),
	LzwEncoder_t293885915::get_offset_of_hsize_12(),
	LzwEncoder_t293885915::get_offset_of_free_ent_13(),
	LzwEncoder_t293885915::get_offset_of_clear_flg_14(),
	LzwEncoder_t293885915::get_offset_of_g_init_bits_15(),
	LzwEncoder_t293885915::get_offset_of_ClearCode_16(),
	LzwEncoder_t293885915::get_offset_of_EOFCode_17(),
	LzwEncoder_t293885915::get_offset_of_cur_accum_18(),
	LzwEncoder_t293885915::get_offset_of_cur_bits_19(),
	LzwEncoder_t293885915::get_offset_of_masks_20(),
	LzwEncoder_t293885915::get_offset_of_a_count_21(),
	LzwEncoder_t293885915::get_offset_of_accum_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (NeuQuant_t3852273683), -1, sizeof(NeuQuant_t3852273683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4853[36] = 
{
	NeuQuant_t3852273683_StaticFields::get_offset_of_netsize_0(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_prime1_1(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_prime2_2(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_prime3_3(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_prime4_4(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_minpicturebytes_5(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_maxnetpos_6(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_netbiasshift_7(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_ncycles_8(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_intbiasshift_9(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_intbias_10(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_gammashift_11(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_gamma_12(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_betashift_13(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_beta_14(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_betagamma_15(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_initrad_16(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_radiusbiasshift_17(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_radiusbias_18(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_initradius_19(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_radiusdec_20(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_alphabiasshift_21(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_initalpha_22(),
	NeuQuant_t3852273683::get_offset_of_alphadec_23(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_radbiasshift_24(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_radbias_25(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_alpharadbshift_26(),
	NeuQuant_t3852273683_StaticFields::get_offset_of_alpharadbias_27(),
	NeuQuant_t3852273683::get_offset_of_thepicture_28(),
	NeuQuant_t3852273683::get_offset_of_lengthcount_29(),
	NeuQuant_t3852273683::get_offset_of_samplefac_30(),
	NeuQuant_t3852273683::get_offset_of_network_31(),
	NeuQuant_t3852273683::get_offset_of_netindex_32(),
	NeuQuant_t3852273683::get_offset_of_bias_33(),
	NeuQuant_t3852273683::get_offset_of_freq_34(),
	NeuQuant_t3852273683::get_offset_of_radpower_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (MinAttribute_t1751344115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4854[1] = 
{
	MinAttribute_t1751344115::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4855[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (Worker_t3267557706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4856[7] = 
{
	Worker_t3267557706::get_offset_of_m_Thread_0(),
	Worker_t3267557706::get_offset_of_m_Id_1(),
	Worker_t3267557706::get_offset_of_m_Frames_2(),
	Worker_t3267557706::get_offset_of_m_Encoder_3(),
	Worker_t3267557706::get_offset_of_m_FilePath_4(),
	Worker_t3267557706::get_offset_of_m_OnFileSaved_5(),
	Worker_t3267557706::get_offset_of_m_OnFileSaveProgress_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (iOSNativeGif_t1153823649), -1, sizeof(iOSNativeGif_t1153823649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4857[5] = 
{
	iOSNativeGif_t1153823649_StaticFields::get_offset_of_GifExportProgress_0(),
	iOSNativeGif_t1153823649_StaticFields::get_offset_of_GifExportCompleted_1(),
	iOSNativeGif_t1153823649_StaticFields::get_offset_of_gcHandles_2(),
	iOSNativeGif_t1153823649_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	iOSNativeGif_t1153823649_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (GifExportProgressDelegate_t324897231), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (GifExportCompletedDelegate_t4082577070), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (Recorder_t3009289404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4860[12] = 
{
	Recorder_t3009289404::get_offset_of__autoHeight_4(),
	Recorder_t3009289404::get_offset_of__width_5(),
	Recorder_t3009289404::get_offset_of__height_6(),
	Recorder_t3009289404::get_offset_of__framePerSecond_7(),
	Recorder_t3009289404::get_offset_of__length_8(),
	Recorder_t3009289404::get_offset_of__state_9(),
	Recorder_t3009289404::get_offset_of__targetCamera_10(),
	Recorder_t3009289404::get_offset_of_maxFrameCount_11(),
	Recorder_t3009289404::get_offset_of_pastTime_12(),
	Recorder_t3009289404::get_offset_of_timePerFrame_13(),
	Recorder_t3009289404::get_offset_of_recordedFrames_14(),
	Recorder_t3009289404::get_offset_of_reflectionUtils_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (RecorderState_t1736470218)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4861[3] = 
{
	RecorderState_t1736470218::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (IAPProductType_t4238212781)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4862[4] = 
{
	IAPProductType_t4238212781::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (IAPProduct_t1805032951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4863[6] = 
{
	IAPProduct_t1805032951::get_offset_of__name_0(),
	IAPProduct_t1805032951::get_offset_of__type_1(),
	IAPProduct_t1805032951::get_offset_of__id_2(),
	IAPProduct_t1805032951::get_offset_of__price_3(),
	IAPProduct_t1805032951::get_offset_of__description_4(),
	IAPProduct_t1805032951::get_offset_of__storeSpecificIds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (StoreSpecificId_t955348180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4864[2] = 
{
	StoreSpecificId_t955348180::get_offset_of_store_0(),
	StoreSpecificId_t955348180::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (IAPSettings_t189197306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4865[7] = 
{
	IAPSettings_t189197306::get_offset_of_mTargetAndroidStore_0(),
	IAPSettings_t189197306::get_offset_of_mSimulateAppleAskToBuy_1(),
	IAPSettings_t189197306::get_offset_of_mInterceptApplePromotionalPurchases_2(),
	IAPSettings_t189197306::get_offset_of_mEnableAmazonSandboxTesting_3(),
	IAPSettings_t189197306::get_offset_of_mValidateAppleReceipt_4(),
	IAPSettings_t189197306::get_offset_of_mValidateGooglePlayReceipt_5(),
	IAPSettings_t189197306::get_offset_of_mProducts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (IAPStore_t973483246)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4866[11] = 
{
	IAPStore_t973483246::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (IAPAndroidStore_t3652621280)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4867[7] = 
{
	IAPAndroidStore_t3652621280::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { sizeof (InAppPurchasing_t3058570446), -1, sizeof(InAppPurchasing_t3058570446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4868[19] = 
{
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_InitializeSucceeded_5(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_InitializeFailed_6(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_PurchaseCompleted_7(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_PurchaseFailed_8(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_PurchaseDeferred_9(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_PromotionalPurchaseIntercepted_10(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_RestoreCompleted_11(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_RestoreFailed_12(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sBuilder_13(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sStoreController_14(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sStoreExtensionProvider_15(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sAppleExtensions_16(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sGooglePlayStoreExtensions_17(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sAmazonExtensions_18(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sSamsungAppsExtensions_19(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_sStoreListener_20(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
	InAppPurchasing_t3058570446_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (StoreListener_t4250367273), -1, sizeof(StoreListener_t4250367273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4869[1] = 
{
	StoreListener_t4250367273_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (U3CRefreshAppleAppReceiptU3Ec__AnonStorey0_t4269051416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4870[2] = 
{
	U3CRefreshAppleAppReceiptU3Ec__AnonStorey0_t4269051416::get_offset_of_successCallback_0(),
	U3CRefreshAppleAppReceiptU3Ec__AnonStorey0_t4269051416::get_offset_of_errorCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (NativeUI_t364718726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (AlertPopup_t1689515052), -1, sizeof(AlertPopup_t1689515052_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4872[4] = 
{
	AlertPopup_t1689515052_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	AlertPopup_t1689515052::get_offset_of_OnComplete_5(),
	AlertPopup_t1689515052_StaticFields::get_offset_of_ALERT_GAMEOBJECT_6(),
	AlertPopup_t1689515052_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (iOSNativeAlert_t3657863450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (FirebaseMessage_t1021966236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4874[14] = 
{
	FirebaseMessage_t1021966236::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	FirebaseMessage_t1021966236::get_offset_of_U3CPriorityU3Ek__BackingField_1(),
	FirebaseMessage_t1021966236::get_offset_of_U3CMessageTypeU3Ek__BackingField_2(),
	FirebaseMessage_t1021966236::get_offset_of_U3CMessageIdU3Ek__BackingField_3(),
	FirebaseMessage_t1021966236::get_offset_of_U3CRawDataU3Ek__BackingField_4(),
	FirebaseMessage_t1021966236::get_offset_of_U3CDataU3Ek__BackingField_5(),
	FirebaseMessage_t1021966236::get_offset_of_U3CCollapseKeyU3Ek__BackingField_6(),
	FirebaseMessage_t1021966236::get_offset_of_U3CToU3Ek__BackingField_7(),
	FirebaseMessage_t1021966236::get_offset_of_U3CFromU3Ek__BackingField_8(),
	FirebaseMessage_t1021966236::get_offset_of_U3CLinkU3Ek__BackingField_9(),
	FirebaseMessage_t1021966236::get_offset_of_U3CNotificationU3Ek__BackingField_10(),
	FirebaseMessage_t1021966236::get_offset_of_U3CTimeToLiveU3Ek__BackingField_11(),
	FirebaseMessage_t1021966236::get_offset_of_U3CErrorDescriptionU3Ek__BackingField_12(),
	FirebaseMessage_t1021966236::get_offset_of_U3CNotificationOpenedU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (FirebaseNotification_t2232694309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4875[12] = 
{
	FirebaseNotification_t2232694309::get_offset_of_U3CTitleU3Ek__BackingField_0(),
	FirebaseNotification_t2232694309::get_offset_of_U3CBodyU3Ek__BackingField_1(),
	FirebaseNotification_t2232694309::get_offset_of_U3CIconU3Ek__BackingField_2(),
	FirebaseNotification_t2232694309::get_offset_of_U3CSoundU3Ek__BackingField_3(),
	FirebaseNotification_t2232694309::get_offset_of_U3CBadgeU3Ek__BackingField_4(),
	FirebaseNotification_t2232694309::get_offset_of_U3CTagU3Ek__BackingField_5(),
	FirebaseNotification_t2232694309::get_offset_of_U3CColorU3Ek__BackingField_6(),
	FirebaseNotification_t2232694309::get_offset_of_U3CClickActionU3Ek__BackingField_7(),
	FirebaseNotification_t2232694309::get_offset_of_U3CBodyLocalizationKeyU3Ek__BackingField_8(),
	FirebaseNotification_t2232694309::get_offset_of_U3CBodyLocalizationArgsU3Ek__BackingField_9(),
	FirebaseNotification_t2232694309::get_offset_of_U3CTitleLocalizationKeyU3Ek__BackingField_10(),
	FirebaseNotification_t2232694309::get_offset_of_U3CTitleLocalizationArgsU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (NativeNotificationHandler_t1892928298), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { sizeof (iOSNotificationListener_t2070489240), -1, sizeof(iOSNotificationListener_t2070489240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4878[6] = 
{
	0,
	iOSNotificationListener_t2070489240_StaticFields::get_offset_of_sInstance_5(),
	iOSNotificationListener_t2070489240::get_offset_of_LocalNotificationOpened_6(),
	iOSNotificationListener_t2070489240::get_offset_of_RemoteNotificationOpened_7(),
	iOSNotificationListener_t2070489240_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
	iOSNotificationListener_t2070489240_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { sizeof (U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4879[8] = 
{
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_request_0(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_actionId_1(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_isForeground_2(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_U24this_3(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_U24current_4(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_U24disposing_5(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_U24PC_6(),
	U3CCRRaiseLocalNotificationEventU3Ec__Iterator0_t3068192955::get_offset_of_U24locvar0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { sizeof (U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4880[4] = 
{
	U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746::get_offset_of_request_0(),
	U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746::get_offset_of_actionId_1(),
	U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746::get_offset_of_isForeground_2(),
	U3CCRRaiseLocalNotificationEventU3Ec__AnonStorey1_t148810746::get_offset_of_U3CU3Ef__refU240_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { sizeof (U3CInternalOnNativeNotificationEventU3Ec__AnonStorey2_t558446483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4881[2] = 
{
	U3CInternalOnNativeNotificationEventU3Ec__AnonStorey2_t558446483::get_offset_of_isForeground_0(),
	U3CInternalOnNativeNotificationEventU3Ec__AnonStorey2_t558446483::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (U3CInternalGetNotificationResponseU3Ec__AnonStorey3_t1240372025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4882[1] = 
{
	U3CInternalGetNotificationResponseU3Ec__AnonStorey3_t1240372025::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (UnsupportedNotificationListener_t2573704219), -1, sizeof(UnsupportedNotificationListener_t2573704219_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4883[5] = 
{
	UnsupportedNotificationListener_t2573704219_StaticFields::get_offset_of_sInstance_0(),
	UnsupportedNotificationListener_t2573704219::get_offset_of_LocalNotificationOpened_1(),
	UnsupportedNotificationListener_t2573704219::get_offset_of_RemoteNotificationOpened_2(),
	UnsupportedNotificationListener_t2573704219_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
	UnsupportedNotificationListener_t2573704219_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (iOSLocalNotificationClient_t527495909), -1, sizeof(iOSLocalNotificationClient_t527495909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4885[3] = 
{
	iOSLocalNotificationClient_t527495909::get_offset_of_mIsInitialized_0(),
	iOSLocalNotificationClient_t527495909_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	iOSLocalNotificationClient_t527495909_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (U3CGetPendingLocalNotificationsU3Ec__AnonStorey0_t1018253411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4886[1] = 
{
	U3CGetPendingLocalNotificationsU3Ec__AnonStorey0_t1018253411::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (U3CInternalGetPendingNotificationRequestsU3Ec__AnonStorey1_t978461598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4887[1] = 
{
	U3CInternalGetPendingNotificationRequestsU3Ec__AnonStorey1_t978461598::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (UnsupportedLocalNotificationClient_t431853024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4888[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (iOSNotificationListenerInfo_t996772639)+ sizeof (RuntimeObject), sizeof(iOSNotificationListenerInfo_t996772639_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4889[3] = 
{
	iOSNotificationListenerInfo_t996772639::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationListenerInfo_t996772639::get_offset_of_backgroundNotificationMethod_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationListenerInfo_t996772639::get_offset_of_foregroundNotificationMethod_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (iOSNotificationAuthOptions_t3273998710)+ sizeof (RuntimeObject), sizeof(iOSNotificationAuthOptions_t3273998710 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4890[3] = 
{
	iOSNotificationAuthOptions_t3273998710::get_offset_of_isAlertAllowed_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationAuthOptions_t3273998710::get_offset_of_isBadgeAllowed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationAuthOptions_t3273998710::get_offset_of_isSoundAllowed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (iOSNotificationContent_t1040386259)+ sizeof (RuntimeObject), sizeof(iOSNotificationContent_t1040386259_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4891[8] = 
{
	iOSNotificationContent_t1040386259::get_offset_of_title_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_subtitle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_body_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_badge_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_enableSound_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_soundName_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_userInfoJson_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationContent_t1040386259::get_offset_of_categoryId_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (iOSDateComponents_t1856826077)+ sizeof (RuntimeObject), sizeof(iOSDateComponents_t1856826077 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4892[6] = 
{
	iOSDateComponents_t1856826077::get_offset_of_year_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSDateComponents_t1856826077::get_offset_of_month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSDateComponents_t1856826077::get_offset_of_day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSDateComponents_t1856826077::get_offset_of_hour_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSDateComponents_t1856826077::get_offset_of_minute_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSDateComponents_t1856826077::get_offset_of_second_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (iOSNotificationAction_t1537218362)+ sizeof (RuntimeObject), sizeof(iOSNotificationAction_t1537218362_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4893[2] = 
{
	iOSNotificationAction_t1537218362::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationAction_t1537218362::get_offset_of_title_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (iOSNotificationCategory_t4187379170)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4894[2] = 
{
	iOSNotificationCategory_t4187379170::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSNotificationCategory_t4187379170::get_offset_of_actions_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (iOSNotificationHelper_t1663845977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4895[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (iOSNotificationRequest_t2359474935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4896[8] = 
{
	iOSNotificationRequest_t2359474935::get_offset_of_mContent_1(),
	iOSNotificationRequest_t2359474935::get_offset_of_mId_2(),
	iOSNotificationRequest_t2359474935::get_offset_of_mTitle_3(),
	iOSNotificationRequest_t2359474935::get_offset_of_mSubtitle_4(),
	iOSNotificationRequest_t2359474935::get_offset_of_mBody_5(),
	iOSNotificationRequest_t2359474935::get_offset_of_mBadge_6(),
	iOSNotificationRequest_t2359474935::get_offset_of_mUserInfoJson_7(),
	iOSNotificationRequest_t2359474935::get_offset_of_mCategoryId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (iOSNotificationResponse_t3524981010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (GetPendingNotificationRequestsResponse_t1853100515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (iOSNotificationNative_t1286273913), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
