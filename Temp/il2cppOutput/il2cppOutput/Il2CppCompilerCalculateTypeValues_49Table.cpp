﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DigitalRubyShared.FingersDPadScript
struct FingersDPadScript_t3801874975;
// DigitalRubyShared.LongPressGestureRecognizer
struct LongPressGestureRecognizer_t3980777482;
// DigitalRubyShared.PanGestureRecognizer
struct PanGestureRecognizer_t195762396;
// DigitalRubyShared.RotateGestureRecognizer
struct RotateGestureRecognizer_t4100246528;
// DigitalRubyShared.ScaleGestureRecognizer
struct ScaleGestureRecognizer_t1137887245;
// DigitalRubyShared.SwipeGestureRecognizer
struct SwipeGestureRecognizer_t2328511861;
// DigitalRubyShared.TapGestureRecognizer
struct TapGestureRecognizer_t3178883670;
// EasyMobile.ConsentDialog
struct ConsentDialog_t3732976094;
// EasyMobile.ConsentDialog/Button
struct Button_t2810410457;
// EasyMobile.ConsentDialog/Button[]
struct ButtonU5BU5D_t1341334180;
// EasyMobile.ConsentDialog/CompletedHandler
struct CompletedHandler_t441206176;
// EasyMobile.ConsentDialog/CompletedResults
struct CompletedResults_t1454177566;
// EasyMobile.ConsentDialog/Toggle
struct Toggle_t3590481327;
// EasyMobile.ConsentDialog/ToggleStateUpdatedHandler
struct ToggleStateUpdatedHandler_t2741348581;
// EasyMobile.ConsentDialog/Toggle[]
struct ToggleU5BU5D_t155957942;
// EasyMobile.ConsentDialogComposerSettings
struct ConsentDialogComposerSettings_t4154825527;
// EasyMobile.FirebaseMessage
struct FirebaseMessage_t1021966236;
// EasyMobile.Internal.Notifications.ILocalNotificationClient
struct ILocalNotificationClient_t3747904824;
// EasyMobile.Internal.Notifications.INotificationListener
struct INotificationListener_t3990812337;
// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte>
struct NativeToManagedArray_1_t3233103070;
// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator
struct BaseNativeEEARegionValidator_t3155147623;
// EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI
struct EditorConsentDialogButtonUI_t376789464;
// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText
struct EditorConsentDialogClickableText_t861962490;
// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName[]
struct IconNameU5BU5D_t1392287153;
// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch
struct EditorConsentDialogToggleSwitch_t2938536456;
// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/ToggleEvent
struct ToggleEvent_t1368036467;
// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI
struct EditorConsentDialogToggleUI_t3945542931;
// EasyMobile.Internal.Privacy.EditorConsentDialogUI
struct EditorConsentDialogUI_t244447250;
// EasyMobile.Internal.Privacy.IPlatformConsentDialog
struct IPlatformConsentDialog_t1124028880;
// EasyMobile.Internal.Privacy.IPlatformEEARegionValidator
struct IPlatformEEARegionValidator_t2623307517;
// EasyMobile.Internal.Privacy.NativeConsentDialogListener
struct NativeConsentDialogListener_t2145450263;
// EasyMobile.NotificationCategory
struct NotificationCategory_t2144930983;
// EasyMobile.NotificationCategory/ActionButton[]
struct ActionButtonU5BU5D_t1842622499;
// EasyMobile.NotificationCategoryGroup[]
struct NotificationCategoryGroupU5BU5D_t1432226907;
// EasyMobile.NotificationCategory[]
struct NotificationCategoryU5BU5D_t1472561374;
// EasyMobile.NotificationContent
struct NotificationContent_t758602733;
// EasyMobile.OneSignalNotificationPayload
struct OneSignalNotificationPayload_t1758448350;
// EasyMobile.RatingDialogContent
struct RatingDialogContent_t752797626;
// EasyMobile.iOS.UIKit.UIImage
struct UIImage_t4033140513;
// System.Action
struct Action_t1264377477;
// System.Action`1<EasyMobile.ConsentDialog>
struct Action_1_t3905443689;
// System.Action`1<EasyMobile.ConsentStatus>
struct Action_1_t2890538688;
// System.Action`1<EasyMobile.EEARegionStatus>
struct Action_1_t3300905132;
// System.Action`1<EasyMobile.Internal.Privacy.IPlatformConsentDialog>
struct Action_1_t1296496475;
// System.Action`1<EasyMobile.LocalNotification>
struct Action_1_t516386935;
// System.Action`1<EasyMobile.RemoteNotification>
struct Action_1_t1859537056;
// System.Action`1<EasyMobile.StoreReview/UserAction>
struct Action_1_t1180320417;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`2<System.String,System.Boolean>
struct Action_2_t2220007950;
// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>>
struct Action_2_t2005264249;
// System.Action`3<EasyMobile.Internal.Privacy.IPlatformConsentDialog,System.String,System.Boolean>
struct Action_3_t4238996613;
// System.Action`3<EasyMobile.Internal.Privacy.IPlatformConsentDialog,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>>
struct Action_3_t4024252912;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI>
struct Dictionary_2_t162045763;
// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI>
struct Dictionary_2_t3730799230;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite>
struct Dictionary_2_t65913391;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.IEnumerator`1<EasyMobile.EEARegionValidationMethods>
struct IEnumerator_1_t2909570567;
// System.Collections.Generic.List`1<EasyMobile.EEARegionValidationMethods>
struct List_1_t3949074841;
// System.Collections.Generic.List`1<EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo>
struct List_1_t4187619232;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t3832554601;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.Stack`1<EasyMobile.EEARegionStatus>
struct Stack_1_t3971826992;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Func`2<EasyMobile.ConsentDialog/Button,System.String>
struct Func_2_t4006837050;
// System.Func`2<EasyMobile.ConsentDialog/Toggle,System.String>
struct Func_2_t2821460812;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>,System.Int32>
struct Func_2_t564255688;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>,System.String>
struct Func_2_t3755727920;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI>,System.Boolean>
struct Func_2_t254368474;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI>,System.String>
struct Func_2_t2004531198;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Boolean>
struct Func_2_t2813788053;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String>
struct Func_2_t268983481;
// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>>
struct Func_2_t1671534078;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.RegularExpressions.Match
struct Match_t3408321083;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1395363720;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t3046220461;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CONSENTDIALOG_T3732976094_H
#define CONSENTDIALOG_T3732976094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog
struct  ConsentDialog_t3732976094  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog::mContent
	String_t* ___mContent_6;
	// System.String EasyMobile.ConsentDialog::mTitle
	String_t* ___mTitle_7;
	// EasyMobile.ConsentDialog/Toggle[] EasyMobile.ConsentDialog::mToggles
	ToggleU5BU5D_t155957942* ___mToggles_8;
	// EasyMobile.ConsentDialog/Button[] EasyMobile.ConsentDialog::mActionButtons
	ButtonU5BU5D_t1341334180* ___mActionButtons_9;
	// EasyMobile.ConsentDialog/ToggleStateUpdatedHandler EasyMobile.ConsentDialog::ToggleStateUpdated
	ToggleStateUpdatedHandler_t2741348581 * ___ToggleStateUpdated_10;
	// EasyMobile.ConsentDialog/CompletedHandler EasyMobile.ConsentDialog::Completed
	CompletedHandler_t441206176 * ___Completed_11;
	// System.Action`1<EasyMobile.ConsentDialog> EasyMobile.ConsentDialog::Dismissed
	Action_1_t3905443689 * ___Dismissed_12;

public:
	inline static int32_t get_offset_of_mContent_6() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___mContent_6)); }
	inline String_t* get_mContent_6() const { return ___mContent_6; }
	inline String_t** get_address_of_mContent_6() { return &___mContent_6; }
	inline void set_mContent_6(String_t* value)
	{
		___mContent_6 = value;
		Il2CppCodeGenWriteBarrier((&___mContent_6), value);
	}

	inline static int32_t get_offset_of_mTitle_7() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___mTitle_7)); }
	inline String_t* get_mTitle_7() const { return ___mTitle_7; }
	inline String_t** get_address_of_mTitle_7() { return &___mTitle_7; }
	inline void set_mTitle_7(String_t* value)
	{
		___mTitle_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTitle_7), value);
	}

	inline static int32_t get_offset_of_mToggles_8() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___mToggles_8)); }
	inline ToggleU5BU5D_t155957942* get_mToggles_8() const { return ___mToggles_8; }
	inline ToggleU5BU5D_t155957942** get_address_of_mToggles_8() { return &___mToggles_8; }
	inline void set_mToggles_8(ToggleU5BU5D_t155957942* value)
	{
		___mToggles_8 = value;
		Il2CppCodeGenWriteBarrier((&___mToggles_8), value);
	}

	inline static int32_t get_offset_of_mActionButtons_9() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___mActionButtons_9)); }
	inline ButtonU5BU5D_t1341334180* get_mActionButtons_9() const { return ___mActionButtons_9; }
	inline ButtonU5BU5D_t1341334180** get_address_of_mActionButtons_9() { return &___mActionButtons_9; }
	inline void set_mActionButtons_9(ButtonU5BU5D_t1341334180* value)
	{
		___mActionButtons_9 = value;
		Il2CppCodeGenWriteBarrier((&___mActionButtons_9), value);
	}

	inline static int32_t get_offset_of_ToggleStateUpdated_10() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___ToggleStateUpdated_10)); }
	inline ToggleStateUpdatedHandler_t2741348581 * get_ToggleStateUpdated_10() const { return ___ToggleStateUpdated_10; }
	inline ToggleStateUpdatedHandler_t2741348581 ** get_address_of_ToggleStateUpdated_10() { return &___ToggleStateUpdated_10; }
	inline void set_ToggleStateUpdated_10(ToggleStateUpdatedHandler_t2741348581 * value)
	{
		___ToggleStateUpdated_10 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleStateUpdated_10), value);
	}

	inline static int32_t get_offset_of_Completed_11() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___Completed_11)); }
	inline CompletedHandler_t441206176 * get_Completed_11() const { return ___Completed_11; }
	inline CompletedHandler_t441206176 ** get_address_of_Completed_11() { return &___Completed_11; }
	inline void set_Completed_11(CompletedHandler_t441206176 * value)
	{
		___Completed_11 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_11), value);
	}

	inline static int32_t get_offset_of_Dismissed_12() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094, ___Dismissed_12)); }
	inline Action_1_t3905443689 * get_Dismissed_12() const { return ___Dismissed_12; }
	inline Action_1_t3905443689 ** get_address_of_Dismissed_12() { return &___Dismissed_12; }
	inline void set_Dismissed_12(Action_1_t3905443689 * value)
	{
		___Dismissed_12 = value;
		Il2CppCodeGenWriteBarrier((&___Dismissed_12), value);
	}
};

struct ConsentDialog_t3732976094_StaticFields
{
public:
	// EasyMobile.ConsentDialog EasyMobile.ConsentDialog::<ActiveDialog>k__BackingField
	ConsentDialog_t3732976094 * ___U3CActiveDialogU3Ek__BackingField_5;
	// EasyMobile.Internal.Privacy.IPlatformConsentDialog EasyMobile.ConsentDialog::sPlatformDialog
	RuntimeObject* ___sPlatformDialog_13;
	// System.Func`2<EasyMobile.ConsentDialog/Toggle,System.String> EasyMobile.ConsentDialog::<>f__am$cache0
	Func_2_t2821460812 * ___U3CU3Ef__amU24cache0_14;
	// System.Func`2<EasyMobile.ConsentDialog/Button,System.String> EasyMobile.ConsentDialog::<>f__am$cache1
	Func_2_t4006837050 * ___U3CU3Ef__amU24cache1_15;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>,System.Int32> EasyMobile.ConsentDialog::<>f__am$cache2
	Func_2_t564255688 * ___U3CU3Ef__amU24cache2_16;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>,System.String> EasyMobile.ConsentDialog::<>f__am$cache3
	Func_2_t3755727920 * ___U3CU3Ef__amU24cache3_17;

public:
	inline static int32_t get_offset_of_U3CActiveDialogU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094_StaticFields, ___U3CActiveDialogU3Ek__BackingField_5)); }
	inline ConsentDialog_t3732976094 * get_U3CActiveDialogU3Ek__BackingField_5() const { return ___U3CActiveDialogU3Ek__BackingField_5; }
	inline ConsentDialog_t3732976094 ** get_address_of_U3CActiveDialogU3Ek__BackingField_5() { return &___U3CActiveDialogU3Ek__BackingField_5; }
	inline void set_U3CActiveDialogU3Ek__BackingField_5(ConsentDialog_t3732976094 * value)
	{
		___U3CActiveDialogU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActiveDialogU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_sPlatformDialog_13() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094_StaticFields, ___sPlatformDialog_13)); }
	inline RuntimeObject* get_sPlatformDialog_13() const { return ___sPlatformDialog_13; }
	inline RuntimeObject** get_address_of_sPlatformDialog_13() { return &___sPlatformDialog_13; }
	inline void set_sPlatformDialog_13(RuntimeObject* value)
	{
		___sPlatformDialog_13 = value;
		Il2CppCodeGenWriteBarrier((&___sPlatformDialog_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Func_2_t2821460812 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Func_2_t2821460812 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Func_2_t2821460812 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_15() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094_StaticFields, ___U3CU3Ef__amU24cache1_15)); }
	inline Func_2_t4006837050 * get_U3CU3Ef__amU24cache1_15() const { return ___U3CU3Ef__amU24cache1_15; }
	inline Func_2_t4006837050 ** get_address_of_U3CU3Ef__amU24cache1_15() { return &___U3CU3Ef__amU24cache1_15; }
	inline void set_U3CU3Ef__amU24cache1_15(Func_2_t4006837050 * value)
	{
		___U3CU3Ef__amU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_16() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094_StaticFields, ___U3CU3Ef__amU24cache2_16)); }
	inline Func_2_t564255688 * get_U3CU3Ef__amU24cache2_16() const { return ___U3CU3Ef__amU24cache2_16; }
	inline Func_2_t564255688 ** get_address_of_U3CU3Ef__amU24cache2_16() { return &___U3CU3Ef__amU24cache2_16; }
	inline void set_U3CU3Ef__amU24cache2_16(Func_2_t564255688 * value)
	{
		___U3CU3Ef__amU24cache2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_17() { return static_cast<int32_t>(offsetof(ConsentDialog_t3732976094_StaticFields, ___U3CU3Ef__amU24cache3_17)); }
	inline Func_2_t3755727920 * get_U3CU3Ef__amU24cache3_17() const { return ___U3CU3Ef__amU24cache3_17; }
	inline Func_2_t3755727920 ** get_address_of_U3CU3Ef__amU24cache3_17() { return &___U3CU3Ef__amU24cache3_17; }
	inline void set_U3CU3Ef__amU24cache3_17(Func_2_t3755727920 * value)
	{
		___U3CU3Ef__amU24cache3_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTDIALOG_T3732976094_H
#ifndef U3CFINDBUTTONWITHIDU3EC__ANONSTOREY4_T647197783_H
#define U3CFINDBUTTONWITHIDU3EC__ANONSTOREY4_T647197783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/<FindButtonWithId>c__AnonStorey4
struct  U3CFindButtonWithIdU3Ec__AnonStorey4_t647197783  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/<FindButtonWithId>c__AnonStorey4::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CFindButtonWithIdU3Ec__AnonStorey4_t647197783, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDBUTTONWITHIDU3EC__ANONSTOREY4_T647197783_H
#ifndef U3CFINDTOGGLEWITHIDU3EC__ANONSTOREY3_T227446378_H
#define U3CFINDTOGGLEWITHIDU3EC__ANONSTOREY3_T227446378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/<FindToggleWithId>c__AnonStorey3
struct  U3CFindToggleWithIdU3Ec__AnonStorey3_t227446378  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/<FindToggleWithId>c__AnonStorey3::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CFindToggleWithIdU3Ec__AnonStorey3_t227446378, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDTOGGLEWITHIDU3EC__ANONSTOREY3_T227446378_H
#ifndef U3CGETALLPATTERNSINCONTENTU3EC__ANONSTOREY5_T307009447_H
#define U3CGETALLPATTERNSINCONTENTU3EC__ANONSTOREY5_T307009447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/<GetAllPatternsInContent>c__AnonStorey5
struct  U3CGetAllPatternsInContentU3Ec__AnonStorey5_t307009447  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.String> EasyMobile.ConsentDialog/<GetAllPatternsInContent>c__AnonStorey5::allPatterns
	HashSet_1_t412400163 * ___allPatterns_0;

public:
	inline static int32_t get_offset_of_allPatterns_0() { return static_cast<int32_t>(offsetof(U3CGetAllPatternsInContentU3Ec__AnonStorey5_t307009447, ___allPatterns_0)); }
	inline HashSet_1_t412400163 * get_allPatterns_0() const { return ___allPatterns_0; }
	inline HashSet_1_t412400163 ** get_address_of_allPatterns_0() { return &___allPatterns_0; }
	inline void set_allPatterns_0(HashSet_1_t412400163 * value)
	{
		___allPatterns_0 = value;
		Il2CppCodeGenWriteBarrier((&___allPatterns_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLPATTERNSINCONTENTU3EC__ANONSTOREY5_T307009447_H
#ifndef U3CGETALLURLSINCONTENTU3EC__ITERATOR0_T1692377037_H
#define U3CGETALLURLSINCONTENTU3EC__ITERATOR0_T1692377037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0
struct  U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::<match>__0
	Regex_t3657309853 * ___U3CmatchU3E__0_0;
	// System.Text.RegularExpressions.MatchCollection EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::<matches>__0
	MatchCollection_t1395363720 * ___U3CmatchesU3E__0_1;
	// System.Collections.IEnumerator EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_2;
	// System.Text.RegularExpressions.Match EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::<url>__1
	Match_t3408321083 * ___U3CurlU3E__1_3;
	// System.IDisposable EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_4;
	// EasyMobile.ConsentDialog EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::$this
	ConsentDialog_t3732976094 * ___U24this_5;
	// System.String EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::$current
	String_t* ___U24current_6;
	// System.Boolean EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 EasyMobile.ConsentDialog/<GetAllUrlsInContent>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CmatchU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U3CmatchU3E__0_0)); }
	inline Regex_t3657309853 * get_U3CmatchU3E__0_0() const { return ___U3CmatchU3E__0_0; }
	inline Regex_t3657309853 ** get_address_of_U3CmatchU3E__0_0() { return &___U3CmatchU3E__0_0; }
	inline void set_U3CmatchU3E__0_0(Regex_t3657309853 * value)
	{
		___U3CmatchU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatchU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U3CmatchesU3E__0_1)); }
	inline MatchCollection_t1395363720 * get_U3CmatchesU3E__0_1() const { return ___U3CmatchesU3E__0_1; }
	inline MatchCollection_t1395363720 ** get_address_of_U3CmatchesU3E__0_1() { return &___U3CmatchesU3E__0_1; }
	inline void set_U3CmatchesU3E__0_1(MatchCollection_t1395363720 * value)
	{
		___U3CmatchesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatchesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U24locvar0_2)); }
	inline RuntimeObject* get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline RuntimeObject** get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(RuntimeObject* value)
	{
		___U24locvar0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_2), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U3CurlU3E__1_3)); }
	inline Match_t3408321083 * get_U3CurlU3E__1_3() const { return ___U3CurlU3E__1_3; }
	inline Match_t3408321083 ** get_address_of_U3CurlU3E__1_3() { return &___U3CurlU3E__1_3; }
	inline void set_U3CurlU3E__1_3(Match_t3408321083 * value)
	{
		___U3CurlU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24locvar1_4() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U24locvar1_4)); }
	inline RuntimeObject* get_U24locvar1_4() const { return ___U24locvar1_4; }
	inline RuntimeObject** get_address_of_U24locvar1_4() { return &___U24locvar1_4; }
	inline void set_U24locvar1_4(RuntimeObject* value)
	{
		___U24locvar1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U24this_5)); }
	inline ConsentDialog_t3732976094 * get_U24this_5() const { return ___U24this_5; }
	inline ConsentDialog_t3732976094 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ConsentDialog_t3732976094 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U24current_6)); }
	inline String_t* get_U24current_6() const { return ___U24current_6; }
	inline String_t** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(String_t* value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLURLSINCONTENTU3EC__ITERATOR0_T1692377037_H
#ifndef U3CREMOVEBUTTONU3EC__ANONSTOREY2_T1939157825_H
#define U3CREMOVEBUTTONU3EC__ANONSTOREY2_T1939157825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/<RemoveButton>c__AnonStorey2
struct  U3CRemoveButtonU3Ec__AnonStorey2_t1939157825  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/<RemoveButton>c__AnonStorey2::buttonId
	String_t* ___buttonId_0;

public:
	inline static int32_t get_offset_of_buttonId_0() { return static_cast<int32_t>(offsetof(U3CRemoveButtonU3Ec__AnonStorey2_t1939157825, ___buttonId_0)); }
	inline String_t* get_buttonId_0() const { return ___buttonId_0; }
	inline String_t** get_address_of_buttonId_0() { return &___buttonId_0; }
	inline void set_buttonId_0(String_t* value)
	{
		___buttonId_0 = value;
		Il2CppCodeGenWriteBarrier((&___buttonId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEBUTTONU3EC__ANONSTOREY2_T1939157825_H
#ifndef U3CREMOVETOGGLEU3EC__ANONSTOREY1_T2440481277_H
#define U3CREMOVETOGGLEU3EC__ANONSTOREY1_T2440481277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/<RemoveToggle>c__AnonStorey1
struct  U3CRemoveToggleU3Ec__AnonStorey1_t2440481277  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/<RemoveToggle>c__AnonStorey1::toggleId
	String_t* ___toggleId_0;

public:
	inline static int32_t get_offset_of_toggleId_0() { return static_cast<int32_t>(offsetof(U3CRemoveToggleU3Ec__AnonStorey1_t2440481277, ___toggleId_0)); }
	inline String_t* get_toggleId_0() const { return ___toggleId_0; }
	inline String_t** get_address_of_toggleId_0() { return &___toggleId_0; }
	inline void set_toggleId_0(String_t* value)
	{
		___toggleId_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggleId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVETOGGLEU3EC__ANONSTOREY1_T2440481277_H
#ifndef COMPLETEDRESULTS_T1454177566_H
#define COMPLETEDRESULTS_T1454177566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/CompletedResults
struct  CompletedResults_t1454177566  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/CompletedResults::buttonId
	String_t* ___buttonId_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> EasyMobile.ConsentDialog/CompletedResults::toggleValues
	Dictionary_2_t4177511560 * ___toggleValues_1;

public:
	inline static int32_t get_offset_of_buttonId_0() { return static_cast<int32_t>(offsetof(CompletedResults_t1454177566, ___buttonId_0)); }
	inline String_t* get_buttonId_0() const { return ___buttonId_0; }
	inline String_t** get_address_of_buttonId_0() { return &___buttonId_0; }
	inline void set_buttonId_0(String_t* value)
	{
		___buttonId_0 = value;
		Il2CppCodeGenWriteBarrier((&___buttonId_0), value);
	}

	inline static int32_t get_offset_of_toggleValues_1() { return static_cast<int32_t>(offsetof(CompletedResults_t1454177566, ___toggleValues_1)); }
	inline Dictionary_2_t4177511560 * get_toggleValues_1() const { return ___toggleValues_1; }
	inline Dictionary_2_t4177511560 ** get_address_of_toggleValues_1() { return &___toggleValues_1; }
	inline void set_toggleValues_1(Dictionary_2_t4177511560 * value)
	{
		___toggleValues_1 = value;
		Il2CppCodeGenWriteBarrier((&___toggleValues_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETEDRESULTS_T1454177566_H
#ifndef TOGGLE_T3590481327_H
#define TOGGLE_T3590481327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/Toggle
struct  Toggle_t3590481327  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/Toggle::id
	String_t* ___id_0;
	// System.String EasyMobile.ConsentDialog/Toggle::title
	String_t* ___title_1;
	// System.String EasyMobile.ConsentDialog/Toggle::onDescription
	String_t* ___onDescription_2;
	// System.String EasyMobile.ConsentDialog/Toggle::offDescription
	String_t* ___offDescription_3;
	// System.Boolean EasyMobile.ConsentDialog/Toggle::isOn
	bool ___isOn_4;
	// System.Boolean EasyMobile.ConsentDialog/Toggle::interactable
	bool ___interactable_5;
	// System.Boolean EasyMobile.ConsentDialog/Toggle::shouldToggleDescription
	bool ___shouldToggleDescription_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_onDescription_2() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___onDescription_2)); }
	inline String_t* get_onDescription_2() const { return ___onDescription_2; }
	inline String_t** get_address_of_onDescription_2() { return &___onDescription_2; }
	inline void set_onDescription_2(String_t* value)
	{
		___onDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___onDescription_2), value);
	}

	inline static int32_t get_offset_of_offDescription_3() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___offDescription_3)); }
	inline String_t* get_offDescription_3() const { return ___offDescription_3; }
	inline String_t** get_address_of_offDescription_3() { return &___offDescription_3; }
	inline void set_offDescription_3(String_t* value)
	{
		___offDescription_3 = value;
		Il2CppCodeGenWriteBarrier((&___offDescription_3), value);
	}

	inline static int32_t get_offset_of_isOn_4() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___isOn_4)); }
	inline bool get_isOn_4() const { return ___isOn_4; }
	inline bool* get_address_of_isOn_4() { return &___isOn_4; }
	inline void set_isOn_4(bool value)
	{
		___isOn_4 = value;
	}

	inline static int32_t get_offset_of_interactable_5() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___interactable_5)); }
	inline bool get_interactable_5() const { return ___interactable_5; }
	inline bool* get_address_of_interactable_5() { return &___interactable_5; }
	inline void set_interactable_5(bool value)
	{
		___interactable_5 = value;
	}

	inline static int32_t get_offset_of_shouldToggleDescription_6() { return static_cast<int32_t>(offsetof(Toggle_t3590481327, ___shouldToggleDescription_6)); }
	inline bool get_shouldToggleDescription_6() const { return ___shouldToggleDescription_6; }
	inline bool* get_address_of_shouldToggleDescription_6() { return &___shouldToggleDescription_6; }
	inline void set_shouldToggleDescription_6(bool value)
	{
		___shouldToggleDescription_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T3590481327_H
#ifndef CONSENTDIALOGCOMPOSERSETTINGS_T4154825527_H
#define CONSENTDIALOGCOMPOSERSETTINGS_T4154825527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialogComposerSettings
struct  ConsentDialogComposerSettings_t4154825527  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.ConsentDialogComposerSettings::mToggleSelectedIndex
	int32_t ___mToggleSelectedIndex_0;
	// System.Int32 EasyMobile.ConsentDialogComposerSettings::mButtonSelectedIndex
	int32_t ___mButtonSelectedIndex_1;
	// System.Boolean EasyMobile.ConsentDialogComposerSettings::mEnableCopyPasteMode
	bool ___mEnableCopyPasteMode_2;

public:
	inline static int32_t get_offset_of_mToggleSelectedIndex_0() { return static_cast<int32_t>(offsetof(ConsentDialogComposerSettings_t4154825527, ___mToggleSelectedIndex_0)); }
	inline int32_t get_mToggleSelectedIndex_0() const { return ___mToggleSelectedIndex_0; }
	inline int32_t* get_address_of_mToggleSelectedIndex_0() { return &___mToggleSelectedIndex_0; }
	inline void set_mToggleSelectedIndex_0(int32_t value)
	{
		___mToggleSelectedIndex_0 = value;
	}

	inline static int32_t get_offset_of_mButtonSelectedIndex_1() { return static_cast<int32_t>(offsetof(ConsentDialogComposerSettings_t4154825527, ___mButtonSelectedIndex_1)); }
	inline int32_t get_mButtonSelectedIndex_1() const { return ___mButtonSelectedIndex_1; }
	inline int32_t* get_address_of_mButtonSelectedIndex_1() { return &___mButtonSelectedIndex_1; }
	inline void set_mButtonSelectedIndex_1(int32_t value)
	{
		___mButtonSelectedIndex_1 = value;
	}

	inline static int32_t get_offset_of_mEnableCopyPasteMode_2() { return static_cast<int32_t>(offsetof(ConsentDialogComposerSettings_t4154825527, ___mEnableCopyPasteMode_2)); }
	inline bool get_mEnableCopyPasteMode_2() const { return ___mEnableCopyPasteMode_2; }
	inline bool* get_address_of_mEnableCopyPasteMode_2() { return &___mEnableCopyPasteMode_2; }
	inline void set_mEnableCopyPasteMode_2(bool value)
	{
		___mEnableCopyPasteMode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTDIALOGCOMPOSERSETTINGS_T4154825527_H
#ifndef CONSENTMANAGER_T1411390104_H
#define CONSENTMANAGER_T1411390104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentManager
struct  ConsentManager_t1411390104  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentManager::mDataPrivacyConsentKey
	String_t* ___mDataPrivacyConsentKey_0;
	// System.Action`1<EasyMobile.ConsentStatus> EasyMobile.ConsentManager::DataPrivacyConsentUpdated
	Action_1_t2890538688 * ___DataPrivacyConsentUpdated_1;

public:
	inline static int32_t get_offset_of_mDataPrivacyConsentKey_0() { return static_cast<int32_t>(offsetof(ConsentManager_t1411390104, ___mDataPrivacyConsentKey_0)); }
	inline String_t* get_mDataPrivacyConsentKey_0() const { return ___mDataPrivacyConsentKey_0; }
	inline String_t** get_address_of_mDataPrivacyConsentKey_0() { return &___mDataPrivacyConsentKey_0; }
	inline void set_mDataPrivacyConsentKey_0(String_t* value)
	{
		___mDataPrivacyConsentKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDataPrivacyConsentKey_0), value);
	}

	inline static int32_t get_offset_of_DataPrivacyConsentUpdated_1() { return static_cast<int32_t>(offsetof(ConsentManager_t1411390104, ___DataPrivacyConsentUpdated_1)); }
	inline Action_1_t2890538688 * get_DataPrivacyConsentUpdated_1() const { return ___DataPrivacyConsentUpdated_1; }
	inline Action_1_t2890538688 ** get_address_of_DataPrivacyConsentUpdated_1() { return &___DataPrivacyConsentUpdated_1; }
	inline void set_DataPrivacyConsentUpdated_1(Action_1_t2890538688 * value)
	{
		___DataPrivacyConsentUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___DataPrivacyConsentUpdated_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTMANAGER_T1411390104_H
#ifndef CONSENTSTORAGE_T2037750554_H
#define CONSENTSTORAGE_T2037750554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentStorage
struct  ConsentStorage_t2037750554  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTSTORAGE_T2037750554_H
#ifndef EEACOUNTRIESEXTENSION_T1734210167_H
#define EEACOUNTRIESEXTENSION_T1734210167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EEACountriesExtension
struct  EEACountriesExtension_t1734210167  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEACOUNTRIESEXTENSION_T1734210167_H
#ifndef EEAREGIONSTATUSEXTENSION_T1071987055_H
#define EEAREGIONSTATUSEXTENSION_T1071987055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EEARegionStatusExtension
struct  EEARegionStatusExtension_t1071987055  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEAREGIONSTATUSEXTENSION_T1071987055_H
#ifndef EEAREGIONVALIDATOR_T2555212889_H
#define EEAREGIONVALIDATOR_T2555212889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EEARegionValidator
struct  EEARegionValidator_t2555212889  : public RuntimeObject
{
public:

public:
};

struct EEARegionValidator_t2555212889_StaticFields
{
public:
	// System.Collections.Generic.List`1<EasyMobile.EEARegionValidationMethods> EasyMobile.EEARegionValidator::DefaultMethods
	List_1_t3949074841 * ___DefaultMethods_0;
	// EasyMobile.Internal.Privacy.IPlatformEEARegionValidator EasyMobile.EEARegionValidator::validator
	RuntimeObject* ___validator_1;

public:
	inline static int32_t get_offset_of_DefaultMethods_0() { return static_cast<int32_t>(offsetof(EEARegionValidator_t2555212889_StaticFields, ___DefaultMethods_0)); }
	inline List_1_t3949074841 * get_DefaultMethods_0() const { return ___DefaultMethods_0; }
	inline List_1_t3949074841 ** get_address_of_DefaultMethods_0() { return &___DefaultMethods_0; }
	inline void set_DefaultMethods_0(List_1_t3949074841 * value)
	{
		___DefaultMethods_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMethods_0), value);
	}

	inline static int32_t get_offset_of_validator_1() { return static_cast<int32_t>(offsetof(EEARegionValidator_t2555212889_StaticFields, ___validator_1)); }
	inline RuntimeObject* get_validator_1() const { return ___validator_1; }
	inline RuntimeObject** get_address_of_validator_1() { return &___validator_1; }
	inline void set_validator_1(RuntimeObject* value)
	{
		___validator_1 = value;
		Il2CppCodeGenWriteBarrier((&___validator_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEAREGIONVALIDATOR_T2555212889_H
#ifndef BASENATIVEEEAREGIONVALIDATOR_T3155147623_H
#define BASENATIVEEEAREGIONVALIDATOR_T3155147623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator
struct  BaseNativeEEARegionValidator_t3155147623  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator::isValidateCoroutineRunning
	bool ___isValidateCoroutineRunning_0;

public:
	inline static int32_t get_offset_of_isValidateCoroutineRunning_0() { return static_cast<int32_t>(offsetof(BaseNativeEEARegionValidator_t3155147623, ___isValidateCoroutineRunning_0)); }
	inline bool get_isValidateCoroutineRunning_0() const { return ___isValidateCoroutineRunning_0; }
	inline bool* get_address_of_isValidateCoroutineRunning_0() { return &___isValidateCoroutineRunning_0; }
	inline void set_isValidateCoroutineRunning_0(bool value)
	{
		___isValidateCoroutineRunning_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASENATIVEEEAREGIONVALIDATOR_T3155147623_H
#ifndef GOOGLESERVICERESPONSE_T406184433_H
#define GOOGLESERVICERESPONSE_T406184433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/GoogleServiceResponse
struct  GoogleServiceResponse_t406184433  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/GoogleServiceResponse::is_request_in_eea_or_unknown
	bool ___is_request_in_eea_or_unknown_0;

public:
	inline static int32_t get_offset_of_is_request_in_eea_or_unknown_0() { return static_cast<int32_t>(offsetof(GoogleServiceResponse_t406184433, ___is_request_in_eea_or_unknown_0)); }
	inline bool get_is_request_in_eea_or_unknown_0() const { return ___is_request_in_eea_or_unknown_0; }
	inline bool* get_address_of_is_request_in_eea_or_unknown_0() { return &___is_request_in_eea_or_unknown_0; }
	inline void set_is_request_in_eea_or_unknown_0(bool value)
	{
		___is_request_in_eea_or_unknown_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLESERVICERESPONSE_T406184433_H
#ifndef CONSENTDIALOGCONTENTSERIALIZER_T3709129555_H
#define CONSENTDIALOGCONTENTSERIALIZER_T3709129555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.ConsentDialogContentSerializer
struct  ConsentDialogContentSerializer_t3709129555  : public RuntimeObject
{
public:
	// System.String EasyMobile.Internal.Privacy.ConsentDialogContentSerializer::<SerializedContent>k__BackingField
	String_t* ___U3CSerializedContentU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CSerializedContentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConsentDialogContentSerializer_t3709129555, ___U3CSerializedContentU3Ek__BackingField_0)); }
	inline String_t* get_U3CSerializedContentU3Ek__BackingField_0() const { return ___U3CSerializedContentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSerializedContentU3Ek__BackingField_0() { return &___U3CSerializedContentU3Ek__BackingField_0; }
	inline void set_U3CSerializedContentU3Ek__BackingField_0(String_t* value)
	{
		___U3CSerializedContentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSerializedContentU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTDIALOGCONTENTSERIALIZER_T3709129555_H
#ifndef SPLITCONTENT_T2522047456_H
#define SPLITCONTENT_T2522047456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.ConsentDialogContentSerializer/SplitContent
struct  SplitContent_t2522047456  : public RuntimeObject
{
public:
	// System.String EasyMobile.Internal.Privacy.ConsentDialogContentSerializer/SplitContent::type
	String_t* ___type_3;
	// System.String EasyMobile.Internal.Privacy.ConsentDialogContentSerializer/SplitContent::content
	String_t* ___content_4;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(SplitContent_t2522047456, ___type_3)); }
	inline String_t* get_type_3() const { return ___type_3; }
	inline String_t** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(String_t* value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(SplitContent_t2522047456, ___content_4)); }
	inline String_t* get_content_4() const { return ___content_4; }
	inline String_t** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(String_t* value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier((&___content_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLITCONTENT_T2522047456_H
#ifndef HREFINFO_T2715544490_H
#define HREFINFO_T2715544490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo
struct  HrefInfo_t2715544490  : public RuntimeObject
{
public:
	// System.Int32 EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo::startIndex
	int32_t ___startIndex_0;
	// System.Int32 EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo::endIndex
	int32_t ___endIndex_1;
	// System.String EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo::name
	String_t* ___name_2;
	// System.Collections.Generic.List`1<UnityEngine.Rect> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo::boxes
	List_1_t3832554601 * ___boxes_3;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(HrefInfo_t2715544490, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_endIndex_1() { return static_cast<int32_t>(offsetof(HrefInfo_t2715544490, ___endIndex_1)); }
	inline int32_t get_endIndex_1() const { return ___endIndex_1; }
	inline int32_t* get_address_of_endIndex_1() { return &___endIndex_1; }
	inline void set_endIndex_1(int32_t value)
	{
		___endIndex_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(HrefInfo_t2715544490, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_boxes_3() { return static_cast<int32_t>(offsetof(HrefInfo_t2715544490, ___boxes_3)); }
	inline List_1_t3832554601 * get_boxes_3() const { return ___boxes_3; }
	inline List_1_t3832554601 ** get_address_of_boxes_3() { return &___boxes_3; }
	inline void set_boxes_3(List_1_t3832554601 * value)
	{
		___boxes_3 = value;
		Il2CppCodeGenWriteBarrier((&___boxes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HREFINFO_T2715544490_H
#ifndef U3CANIMATEDESCRIPTIONCOROUTINEU3EC__ITERATOR0_T3726424355_H
#define U3CANIMATEDESCRIPTIONCOROUTINEU3EC__ITERATOR0_T3726424355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0
struct  U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355  : public RuntimeObject
{
public:
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::<currentLerpValue>__0
	float ___U3CcurrentLerpValueU3E__0_0;
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::<startHeight>__0
	float ___U3CstartHeightU3E__0_1;
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::targetHeight
	float ___targetHeight_2;
	// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::$this
	EditorConsentDialogToggleUI_t3945542931 * ___U24this_3;
	// System.Object EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<AnimateDescriptionCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcurrentLerpValueU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___U3CcurrentLerpValueU3E__0_0)); }
	inline float get_U3CcurrentLerpValueU3E__0_0() const { return ___U3CcurrentLerpValueU3E__0_0; }
	inline float* get_address_of_U3CcurrentLerpValueU3E__0_0() { return &___U3CcurrentLerpValueU3E__0_0; }
	inline void set_U3CcurrentLerpValueU3E__0_0(float value)
	{
		___U3CcurrentLerpValueU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartHeightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___U3CstartHeightU3E__0_1)); }
	inline float get_U3CstartHeightU3E__0_1() const { return ___U3CstartHeightU3E__0_1; }
	inline float* get_address_of_U3CstartHeightU3E__0_1() { return &___U3CstartHeightU3E__0_1; }
	inline void set_U3CstartHeightU3E__0_1(float value)
	{
		___U3CstartHeightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_targetHeight_2() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___targetHeight_2)); }
	inline float get_targetHeight_2() const { return ___targetHeight_2; }
	inline float* get_address_of_targetHeight_2() { return &___targetHeight_2; }
	inline void set_targetHeight_2(float value)
	{
		___targetHeight_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___U24this_3)); }
	inline EditorConsentDialogToggleUI_t3945542931 * get_U24this_3() const { return ___U24this_3; }
	inline EditorConsentDialogToggleUI_t3945542931 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(EditorConsentDialogToggleUI_t3945542931 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEDESCRIPTIONCOROUTINEU3EC__ITERATOR0_T3726424355_H
#ifndef U3CSETUPTOGGLEU3EC__ANONSTOREY1_T3174135061_H
#define U3CSETUPTOGGLEU3EC__ANONSTOREY1_T3174135061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<SetupToggle>c__AnonStorey1
struct  U3CSetupToggleU3Ec__AnonStorey1_t3174135061  : public RuntimeObject
{
public:
	// EasyMobile.ConsentDialog/Toggle EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<SetupToggle>c__AnonStorey1::toggleData
	Toggle_t3590481327 * ___toggleData_0;
	// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI/<SetupToggle>c__AnonStorey1::$this
	EditorConsentDialogToggleUI_t3945542931 * ___U24this_1;

public:
	inline static int32_t get_offset_of_toggleData_0() { return static_cast<int32_t>(offsetof(U3CSetupToggleU3Ec__AnonStorey1_t3174135061, ___toggleData_0)); }
	inline Toggle_t3590481327 * get_toggleData_0() const { return ___toggleData_0; }
	inline Toggle_t3590481327 ** get_address_of_toggleData_0() { return &___toggleData_0; }
	inline void set_toggleData_0(Toggle_t3590481327 * value)
	{
		___toggleData_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggleData_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSetupToggleU3Ec__AnonStorey1_t3174135061, ___U24this_1)); }
	inline EditorConsentDialogToggleUI_t3945542931 * get_U24this_1() const { return ___U24this_1; }
	inline EditorConsentDialogToggleUI_t3945542931 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EditorConsentDialogToggleUI_t3945542931 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETUPTOGGLEU3EC__ANONSTOREY1_T3174135061_H
#ifndef U3CADDBUTTONU3EC__ANONSTOREY0_T1451979152_H
#define U3CADDBUTTONU3EC__ANONSTOREY0_T1451979152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogUI/<AddButton>c__AnonStorey0
struct  U3CAddButtonU3Ec__AnonStorey0_t1451979152  : public RuntimeObject
{
public:
	// EasyMobile.ConsentDialog/Button EasyMobile.Internal.Privacy.EditorConsentDialogUI/<AddButton>c__AnonStorey0::buttonData
	Button_t2810410457 * ___buttonData_0;
	// EasyMobile.Internal.Privacy.EditorConsentDialogUI EasyMobile.Internal.Privacy.EditorConsentDialogUI/<AddButton>c__AnonStorey0::$this
	EditorConsentDialogUI_t244447250 * ___U24this_1;

public:
	inline static int32_t get_offset_of_buttonData_0() { return static_cast<int32_t>(offsetof(U3CAddButtonU3Ec__AnonStorey0_t1451979152, ___buttonData_0)); }
	inline Button_t2810410457 * get_buttonData_0() const { return ___buttonData_0; }
	inline Button_t2810410457 ** get_address_of_buttonData_0() { return &___buttonData_0; }
	inline void set_buttonData_0(Button_t2810410457 * value)
	{
		___buttonData_0 = value;
		Il2CppCodeGenWriteBarrier((&___buttonData_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAddButtonU3Ec__AnonStorey0_t1451979152, ___U24this_1)); }
	inline EditorConsentDialogUI_t244447250 * get_U24this_1() const { return ___U24this_1; }
	inline EditorConsentDialogUI_t244447250 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EditorConsentDialogUI_t244447250 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDBUTTONU3EC__ANONSTOREY0_T1451979152_H
#ifndef UNSUPPORTEDCONSENTDIALOG_T239542232_H
#define UNSUPPORTEDCONSENTDIALOG_T239542232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.UnsupportedConsentDialog
struct  UnsupportedConsentDialog_t239542232  : public RuntimeObject
{
public:
	// System.Action`3<EasyMobile.Internal.Privacy.IPlatformConsentDialog,System.String,System.Boolean> EasyMobile.Internal.Privacy.UnsupportedConsentDialog::ToggleStateUpdated
	Action_3_t4238996613 * ___ToggleStateUpdated_1;
	// System.Action`3<EasyMobile.Internal.Privacy.IPlatformConsentDialog,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>> EasyMobile.Internal.Privacy.UnsupportedConsentDialog::Completed
	Action_3_t4024252912 * ___Completed_2;
	// System.Action`1<EasyMobile.Internal.Privacy.IPlatformConsentDialog> EasyMobile.Internal.Privacy.UnsupportedConsentDialog::Dismissed
	Action_1_t1296496475 * ___Dismissed_3;

public:
	inline static int32_t get_offset_of_ToggleStateUpdated_1() { return static_cast<int32_t>(offsetof(UnsupportedConsentDialog_t239542232, ___ToggleStateUpdated_1)); }
	inline Action_3_t4238996613 * get_ToggleStateUpdated_1() const { return ___ToggleStateUpdated_1; }
	inline Action_3_t4238996613 ** get_address_of_ToggleStateUpdated_1() { return &___ToggleStateUpdated_1; }
	inline void set_ToggleStateUpdated_1(Action_3_t4238996613 * value)
	{
		___ToggleStateUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleStateUpdated_1), value);
	}

	inline static int32_t get_offset_of_Completed_2() { return static_cast<int32_t>(offsetof(UnsupportedConsentDialog_t239542232, ___Completed_2)); }
	inline Action_3_t4024252912 * get_Completed_2() const { return ___Completed_2; }
	inline Action_3_t4024252912 ** get_address_of_Completed_2() { return &___Completed_2; }
	inline void set_Completed_2(Action_3_t4024252912 * value)
	{
		___Completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_2), value);
	}

	inline static int32_t get_offset_of_Dismissed_3() { return static_cast<int32_t>(offsetof(UnsupportedConsentDialog_t239542232, ___Dismissed_3)); }
	inline Action_1_t1296496475 * get_Dismissed_3() const { return ___Dismissed_3; }
	inline Action_1_t1296496475 ** get_address_of_Dismissed_3() { return &___Dismissed_3; }
	inline void set_Dismissed_3(Action_1_t1296496475 * value)
	{
		___Dismissed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Dismissed_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDCONSENTDIALOG_T239542232_H
#ifndef UNSUPPORTEDEEAREGIONVALIDATOR_T2520111492_H
#define UNSUPPORTEDEEAREGIONVALIDATOR_T2520111492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.UnsupportedEEARegionValidator
struct  UnsupportedEEARegionValidator_t2520111492  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDEEAREGIONVALIDATOR_T2520111492_H
#ifndef IOSCONSENTDIALOG_T3621930855_H
#define IOSCONSENTDIALOG_T3621930855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.iOSConsentDialog
struct  iOSConsentDialog_t3621930855  : public RuntimeObject
{
public:
	// EasyMobile.Internal.Privacy.NativeConsentDialogListener EasyMobile.Internal.Privacy.iOSConsentDialog::mListener
	NativeConsentDialogListener_t2145450263 * ___mListener_3;
	// System.Action`3<EasyMobile.Internal.Privacy.IPlatformConsentDialog,System.String,System.Boolean> EasyMobile.Internal.Privacy.iOSConsentDialog::ToggleStateUpdated
	Action_3_t4238996613 * ___ToggleStateUpdated_4;
	// System.Action`3<EasyMobile.Internal.Privacy.IPlatformConsentDialog,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>> EasyMobile.Internal.Privacy.iOSConsentDialog::Completed
	Action_3_t4024252912 * ___Completed_5;
	// System.Action`1<EasyMobile.Internal.Privacy.IPlatformConsentDialog> EasyMobile.Internal.Privacy.iOSConsentDialog::Dismissed
	Action_1_t1296496475 * ___Dismissed_6;

public:
	inline static int32_t get_offset_of_mListener_3() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855, ___mListener_3)); }
	inline NativeConsentDialogListener_t2145450263 * get_mListener_3() const { return ___mListener_3; }
	inline NativeConsentDialogListener_t2145450263 ** get_address_of_mListener_3() { return &___mListener_3; }
	inline void set_mListener_3(NativeConsentDialogListener_t2145450263 * value)
	{
		___mListener_3 = value;
		Il2CppCodeGenWriteBarrier((&___mListener_3), value);
	}

	inline static int32_t get_offset_of_ToggleStateUpdated_4() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855, ___ToggleStateUpdated_4)); }
	inline Action_3_t4238996613 * get_ToggleStateUpdated_4() const { return ___ToggleStateUpdated_4; }
	inline Action_3_t4238996613 ** get_address_of_ToggleStateUpdated_4() { return &___ToggleStateUpdated_4; }
	inline void set_ToggleStateUpdated_4(Action_3_t4238996613 * value)
	{
		___ToggleStateUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleStateUpdated_4), value);
	}

	inline static int32_t get_offset_of_Completed_5() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855, ___Completed_5)); }
	inline Action_3_t4024252912 * get_Completed_5() const { return ___Completed_5; }
	inline Action_3_t4024252912 ** get_address_of_Completed_5() { return &___Completed_5; }
	inline void set_Completed_5(Action_3_t4024252912 * value)
	{
		___Completed_5 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_5), value);
	}

	inline static int32_t get_offset_of_Dismissed_6() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855, ___Dismissed_6)); }
	inline Action_1_t1296496475 * get_Dismissed_6() const { return ___Dismissed_6; }
	inline Action_1_t1296496475 ** get_address_of_Dismissed_6() { return &___Dismissed_6; }
	inline void set_Dismissed_6(Action_1_t1296496475 * value)
	{
		___Dismissed_6 = value;
		Il2CppCodeGenWriteBarrier((&___Dismissed_6), value);
	}
};

struct iOSConsentDialog_t3621930855_StaticFields
{
public:
	// EasyMobile.Internal.Privacy.iOSConsentDialog EasyMobile.Internal.Privacy.iOSConsentDialog::sInstance
	iOSConsentDialog_t3621930855 * ___sInstance_2;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> EasyMobile.Internal.Privacy.iOSConsentDialog::<>f__am$cache0
	Func_2_t268983481 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Boolean> EasyMobile.Internal.Privacy.iOSConsentDialog::<>f__am$cache1
	Func_2_t2813788053 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_sInstance_2() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855_StaticFields, ___sInstance_2)); }
	inline iOSConsentDialog_t3621930855 * get_sInstance_2() const { return ___sInstance_2; }
	inline iOSConsentDialog_t3621930855 ** get_address_of_sInstance_2() { return &___sInstance_2; }
	inline void set_sInstance_2(iOSConsentDialog_t3621930855 * value)
	{
		___sInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t268983481 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t268983481 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t268983481 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(iOSConsentDialog_t3621930855_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t2813788053 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t2813788053 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t2813788053 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCONSENTDIALOG_T3621930855_H
#ifndef IOSNATIVESHARE_T3881593496_H
#define IOSNATIVESHARE_T3881593496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Sharing.iOS.iOSNativeShare
struct  iOSNativeShare_t3881593496  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVESHARE_T3881593496_H
#ifndef IOSNATIVEUTILITY_T1924238516_H
#define IOSNATIVEUTILITY_T1924238516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.StoreReview.iOS.iOSNativeUtility
struct  iOSNativeUtility_t1924238516  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSNATIVEUTILITY_T1924238516_H
#ifndef NOTIFICATION_T414530818_H
#define NOTIFICATION_T414530818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Notification
struct  Notification_t414530818  : public RuntimeObject
{
public:
	// System.String EasyMobile.Notification::id
	String_t* ___id_0;
	// System.String EasyMobile.Notification::actionId
	String_t* ___actionId_1;
	// EasyMobile.NotificationContent EasyMobile.Notification::content
	NotificationContent_t758602733 * ___content_2;
	// System.Boolean EasyMobile.Notification::isAppInForeground
	bool ___isAppInForeground_3;
	// System.Boolean EasyMobile.Notification::isOpened
	bool ___isOpened_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Notification_t414530818, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_actionId_1() { return static_cast<int32_t>(offsetof(Notification_t414530818, ___actionId_1)); }
	inline String_t* get_actionId_1() const { return ___actionId_1; }
	inline String_t** get_address_of_actionId_1() { return &___actionId_1; }
	inline void set_actionId_1(String_t* value)
	{
		___actionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___actionId_1), value);
	}

	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(Notification_t414530818, ___content_2)); }
	inline NotificationContent_t758602733 * get_content_2() const { return ___content_2; }
	inline NotificationContent_t758602733 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(NotificationContent_t758602733 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}

	inline static int32_t get_offset_of_isAppInForeground_3() { return static_cast<int32_t>(offsetof(Notification_t414530818, ___isAppInForeground_3)); }
	inline bool get_isAppInForeground_3() const { return ___isAppInForeground_3; }
	inline bool* get_address_of_isAppInForeground_3() { return &___isAppInForeground_3; }
	inline void set_isAppInForeground_3(bool value)
	{
		___isAppInForeground_3 = value;
	}

	inline static int32_t get_offset_of_isOpened_4() { return static_cast<int32_t>(offsetof(Notification_t414530818, ___isOpened_4)); }
	inline bool get_isOpened_4() const { return ___isOpened_4; }
	inline bool* get_address_of_isOpened_4() { return &___isOpened_4; }
	inline void set_isOpened_4(bool value)
	{
		___isOpened_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATION_T414530818_H
#ifndef NOTIFICATIONCATEGORYGROUP_T219032366_H
#define NOTIFICATIONCATEGORYGROUP_T219032366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategoryGroup
struct  NotificationCategoryGroup_t219032366  : public RuntimeObject
{
public:
	// System.String EasyMobile.NotificationCategoryGroup::id
	String_t* ___id_0;
	// System.String EasyMobile.NotificationCategoryGroup::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(NotificationCategoryGroup_t219032366, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(NotificationCategoryGroup_t219032366, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONCATEGORYGROUP_T219032366_H
#ifndef NOTIFICATIONCONTENT_T758602733_H
#define NOTIFICATIONCONTENT_T758602733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationContent
struct  NotificationContent_t758602733  : public RuntimeObject
{
public:
	// System.String EasyMobile.NotificationContent::title
	String_t* ___title_2;
	// System.String EasyMobile.NotificationContent::subtitle
	String_t* ___subtitle_3;
	// System.String EasyMobile.NotificationContent::body
	String_t* ___body_4;
	// System.Int32 EasyMobile.NotificationContent::badge
	int32_t ___badge_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> EasyMobile.NotificationContent::userInfo
	Dictionary_2_t2865362463 * ___userInfo_6;
	// System.String EasyMobile.NotificationContent::categoryId
	String_t* ___categoryId_7;
	// System.String EasyMobile.NotificationContent::smallIcon
	String_t* ___smallIcon_8;
	// System.String EasyMobile.NotificationContent::largeIcon
	String_t* ___largeIcon_9;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_subtitle_3() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___subtitle_3)); }
	inline String_t* get_subtitle_3() const { return ___subtitle_3; }
	inline String_t** get_address_of_subtitle_3() { return &___subtitle_3; }
	inline void set_subtitle_3(String_t* value)
	{
		___subtitle_3 = value;
		Il2CppCodeGenWriteBarrier((&___subtitle_3), value);
	}

	inline static int32_t get_offset_of_body_4() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___body_4)); }
	inline String_t* get_body_4() const { return ___body_4; }
	inline String_t** get_address_of_body_4() { return &___body_4; }
	inline void set_body_4(String_t* value)
	{
		___body_4 = value;
		Il2CppCodeGenWriteBarrier((&___body_4), value);
	}

	inline static int32_t get_offset_of_badge_5() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___badge_5)); }
	inline int32_t get_badge_5() const { return ___badge_5; }
	inline int32_t* get_address_of_badge_5() { return &___badge_5; }
	inline void set_badge_5(int32_t value)
	{
		___badge_5 = value;
	}

	inline static int32_t get_offset_of_userInfo_6() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___userInfo_6)); }
	inline Dictionary_2_t2865362463 * get_userInfo_6() const { return ___userInfo_6; }
	inline Dictionary_2_t2865362463 ** get_address_of_userInfo_6() { return &___userInfo_6; }
	inline void set_userInfo_6(Dictionary_2_t2865362463 * value)
	{
		___userInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___userInfo_6), value);
	}

	inline static int32_t get_offset_of_categoryId_7() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___categoryId_7)); }
	inline String_t* get_categoryId_7() const { return ___categoryId_7; }
	inline String_t** get_address_of_categoryId_7() { return &___categoryId_7; }
	inline void set_categoryId_7(String_t* value)
	{
		___categoryId_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryId_7), value);
	}

	inline static int32_t get_offset_of_smallIcon_8() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___smallIcon_8)); }
	inline String_t* get_smallIcon_8() const { return ___smallIcon_8; }
	inline String_t** get_address_of_smallIcon_8() { return &___smallIcon_8; }
	inline void set_smallIcon_8(String_t* value)
	{
		___smallIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&___smallIcon_8), value);
	}

	inline static int32_t get_offset_of_largeIcon_9() { return static_cast<int32_t>(offsetof(NotificationContent_t758602733, ___largeIcon_9)); }
	inline String_t* get_largeIcon_9() const { return ___largeIcon_9; }
	inline String_t** get_address_of_largeIcon_9() { return &___largeIcon_9; }
	inline void set_largeIcon_9(String_t* value)
	{
		___largeIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&___largeIcon_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONCONTENT_T758602733_H
#ifndef NOTIFICATIONREPEATEXTENSION_T3960328516_H
#define NOTIFICATIONREPEATEXTENSION_T3960328516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationRepeatExtension
struct  NotificationRepeatExtension_t3960328516  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONREPEATEXTENSION_T3960328516_H
#ifndef U3CCRAUTOINITU3EC__ITERATOR0_T481153097_H
#define U3CCRAUTOINITU3EC__ITERATOR0_T481153097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Notifications/<CRAutoInit>c__Iterator0
struct  U3CCRAutoInitU3Ec__Iterator0_t481153097  : public RuntimeObject
{
public:
	// System.Single EasyMobile.Notifications/<CRAutoInit>c__Iterator0::delay
	float ___delay_0;
	// System.Object EasyMobile.Notifications/<CRAutoInit>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean EasyMobile.Notifications/<CRAutoInit>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 EasyMobile.Notifications/<CRAutoInit>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t481153097, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t481153097, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t481153097, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCRAutoInitU3Ec__Iterator0_t481153097, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCRAUTOINITU3EC__ITERATOR0_T481153097_H
#ifndef ONESIGNALNOTIFICATIONPAYLOAD_T1758448350_H
#define ONESIGNALNOTIFICATIONPAYLOAD_T1758448350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.OneSignalNotificationPayload
struct  OneSignalNotificationPayload_t1758448350  : public RuntimeObject
{
public:
	// System.String EasyMobile.OneSignalNotificationPayload::notificationID
	String_t* ___notificationID_0;
	// System.String EasyMobile.OneSignalNotificationPayload::sound
	String_t* ___sound_1;
	// System.String EasyMobile.OneSignalNotificationPayload::title
	String_t* ___title_2;
	// System.String EasyMobile.OneSignalNotificationPayload::body
	String_t* ___body_3;
	// System.String EasyMobile.OneSignalNotificationPayload::subtitle
	String_t* ___subtitle_4;
	// System.String EasyMobile.OneSignalNotificationPayload::launchURL
	String_t* ___launchURL_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> EasyMobile.OneSignalNotificationPayload::additionalData
	Dictionary_2_t2865362463 * ___additionalData_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> EasyMobile.OneSignalNotificationPayload::actionButtons
	Dictionary_2_t2865362463 * ___actionButtons_7;
	// System.Boolean EasyMobile.OneSignalNotificationPayload::contentAvailable
	bool ___contentAvailable_8;
	// System.Int32 EasyMobile.OneSignalNotificationPayload::badge
	int32_t ___badge_9;
	// System.String EasyMobile.OneSignalNotificationPayload::smallIcon
	String_t* ___smallIcon_10;
	// System.String EasyMobile.OneSignalNotificationPayload::largeIcon
	String_t* ___largeIcon_11;
	// System.String EasyMobile.OneSignalNotificationPayload::bigPicture
	String_t* ___bigPicture_12;
	// System.String EasyMobile.OneSignalNotificationPayload::smallIconAccentColor
	String_t* ___smallIconAccentColor_13;
	// System.String EasyMobile.OneSignalNotificationPayload::ledColor
	String_t* ___ledColor_14;
	// System.Int32 EasyMobile.OneSignalNotificationPayload::lockScreenVisibility
	int32_t ___lockScreenVisibility_15;
	// System.String EasyMobile.OneSignalNotificationPayload::groupKey
	String_t* ___groupKey_16;
	// System.String EasyMobile.OneSignalNotificationPayload::groupMessage
	String_t* ___groupMessage_17;
	// System.String EasyMobile.OneSignalNotificationPayload::fromProjectNumber
	String_t* ___fromProjectNumber_18;

public:
	inline static int32_t get_offset_of_notificationID_0() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___notificationID_0)); }
	inline String_t* get_notificationID_0() const { return ___notificationID_0; }
	inline String_t** get_address_of_notificationID_0() { return &___notificationID_0; }
	inline void set_notificationID_0(String_t* value)
	{
		___notificationID_0 = value;
		Il2CppCodeGenWriteBarrier((&___notificationID_0), value);
	}

	inline static int32_t get_offset_of_sound_1() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___sound_1)); }
	inline String_t* get_sound_1() const { return ___sound_1; }
	inline String_t** get_address_of_sound_1() { return &___sound_1; }
	inline void set_sound_1(String_t* value)
	{
		___sound_1 = value;
		Il2CppCodeGenWriteBarrier((&___sound_1), value);
	}

	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___body_3)); }
	inline String_t* get_body_3() const { return ___body_3; }
	inline String_t** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(String_t* value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier((&___body_3), value);
	}

	inline static int32_t get_offset_of_subtitle_4() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___subtitle_4)); }
	inline String_t* get_subtitle_4() const { return ___subtitle_4; }
	inline String_t** get_address_of_subtitle_4() { return &___subtitle_4; }
	inline void set_subtitle_4(String_t* value)
	{
		___subtitle_4 = value;
		Il2CppCodeGenWriteBarrier((&___subtitle_4), value);
	}

	inline static int32_t get_offset_of_launchURL_5() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___launchURL_5)); }
	inline String_t* get_launchURL_5() const { return ___launchURL_5; }
	inline String_t** get_address_of_launchURL_5() { return &___launchURL_5; }
	inline void set_launchURL_5(String_t* value)
	{
		___launchURL_5 = value;
		Il2CppCodeGenWriteBarrier((&___launchURL_5), value);
	}

	inline static int32_t get_offset_of_additionalData_6() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___additionalData_6)); }
	inline Dictionary_2_t2865362463 * get_additionalData_6() const { return ___additionalData_6; }
	inline Dictionary_2_t2865362463 ** get_address_of_additionalData_6() { return &___additionalData_6; }
	inline void set_additionalData_6(Dictionary_2_t2865362463 * value)
	{
		___additionalData_6 = value;
		Il2CppCodeGenWriteBarrier((&___additionalData_6), value);
	}

	inline static int32_t get_offset_of_actionButtons_7() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___actionButtons_7)); }
	inline Dictionary_2_t2865362463 * get_actionButtons_7() const { return ___actionButtons_7; }
	inline Dictionary_2_t2865362463 ** get_address_of_actionButtons_7() { return &___actionButtons_7; }
	inline void set_actionButtons_7(Dictionary_2_t2865362463 * value)
	{
		___actionButtons_7 = value;
		Il2CppCodeGenWriteBarrier((&___actionButtons_7), value);
	}

	inline static int32_t get_offset_of_contentAvailable_8() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___contentAvailable_8)); }
	inline bool get_contentAvailable_8() const { return ___contentAvailable_8; }
	inline bool* get_address_of_contentAvailable_8() { return &___contentAvailable_8; }
	inline void set_contentAvailable_8(bool value)
	{
		___contentAvailable_8 = value;
	}

	inline static int32_t get_offset_of_badge_9() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___badge_9)); }
	inline int32_t get_badge_9() const { return ___badge_9; }
	inline int32_t* get_address_of_badge_9() { return &___badge_9; }
	inline void set_badge_9(int32_t value)
	{
		___badge_9 = value;
	}

	inline static int32_t get_offset_of_smallIcon_10() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___smallIcon_10)); }
	inline String_t* get_smallIcon_10() const { return ___smallIcon_10; }
	inline String_t** get_address_of_smallIcon_10() { return &___smallIcon_10; }
	inline void set_smallIcon_10(String_t* value)
	{
		___smallIcon_10 = value;
		Il2CppCodeGenWriteBarrier((&___smallIcon_10), value);
	}

	inline static int32_t get_offset_of_largeIcon_11() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___largeIcon_11)); }
	inline String_t* get_largeIcon_11() const { return ___largeIcon_11; }
	inline String_t** get_address_of_largeIcon_11() { return &___largeIcon_11; }
	inline void set_largeIcon_11(String_t* value)
	{
		___largeIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&___largeIcon_11), value);
	}

	inline static int32_t get_offset_of_bigPicture_12() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___bigPicture_12)); }
	inline String_t* get_bigPicture_12() const { return ___bigPicture_12; }
	inline String_t** get_address_of_bigPicture_12() { return &___bigPicture_12; }
	inline void set_bigPicture_12(String_t* value)
	{
		___bigPicture_12 = value;
		Il2CppCodeGenWriteBarrier((&___bigPicture_12), value);
	}

	inline static int32_t get_offset_of_smallIconAccentColor_13() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___smallIconAccentColor_13)); }
	inline String_t* get_smallIconAccentColor_13() const { return ___smallIconAccentColor_13; }
	inline String_t** get_address_of_smallIconAccentColor_13() { return &___smallIconAccentColor_13; }
	inline void set_smallIconAccentColor_13(String_t* value)
	{
		___smallIconAccentColor_13 = value;
		Il2CppCodeGenWriteBarrier((&___smallIconAccentColor_13), value);
	}

	inline static int32_t get_offset_of_ledColor_14() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___ledColor_14)); }
	inline String_t* get_ledColor_14() const { return ___ledColor_14; }
	inline String_t** get_address_of_ledColor_14() { return &___ledColor_14; }
	inline void set_ledColor_14(String_t* value)
	{
		___ledColor_14 = value;
		Il2CppCodeGenWriteBarrier((&___ledColor_14), value);
	}

	inline static int32_t get_offset_of_lockScreenVisibility_15() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___lockScreenVisibility_15)); }
	inline int32_t get_lockScreenVisibility_15() const { return ___lockScreenVisibility_15; }
	inline int32_t* get_address_of_lockScreenVisibility_15() { return &___lockScreenVisibility_15; }
	inline void set_lockScreenVisibility_15(int32_t value)
	{
		___lockScreenVisibility_15 = value;
	}

	inline static int32_t get_offset_of_groupKey_16() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___groupKey_16)); }
	inline String_t* get_groupKey_16() const { return ___groupKey_16; }
	inline String_t** get_address_of_groupKey_16() { return &___groupKey_16; }
	inline void set_groupKey_16(String_t* value)
	{
		___groupKey_16 = value;
		Il2CppCodeGenWriteBarrier((&___groupKey_16), value);
	}

	inline static int32_t get_offset_of_groupMessage_17() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___groupMessage_17)); }
	inline String_t* get_groupMessage_17() const { return ___groupMessage_17; }
	inline String_t** get_address_of_groupMessage_17() { return &___groupMessage_17; }
	inline void set_groupMessage_17(String_t* value)
	{
		___groupMessage_17 = value;
		Il2CppCodeGenWriteBarrier((&___groupMessage_17), value);
	}

	inline static int32_t get_offset_of_fromProjectNumber_18() { return static_cast<int32_t>(offsetof(OneSignalNotificationPayload_t1758448350, ___fromProjectNumber_18)); }
	inline String_t* get_fromProjectNumber_18() const { return ___fromProjectNumber_18; }
	inline String_t** get_address_of_fromProjectNumber_18() { return &___fromProjectNumber_18; }
	inline void set_fromProjectNumber_18(String_t* value)
	{
		___fromProjectNumber_18 = value;
		Il2CppCodeGenWriteBarrier((&___fromProjectNumber_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONESIGNALNOTIFICATIONPAYLOAD_T1758448350_H
#ifndef PRIVACY_T1244755513_H
#define PRIVACY_T1244755513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Privacy
struct  Privacy_t1244755513  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVACY_T1244755513_H
#ifndef PRIVACYSETTINGS_T2445251604_H
#define PRIVACYSETTINGS_T2445251604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.PrivacySettings
struct  PrivacySettings_t2445251604  : public RuntimeObject
{
public:
	// EasyMobile.ConsentDialog EasyMobile.PrivacySettings::mDefaultConsentDialog
	ConsentDialog_t3732976094 * ___mDefaultConsentDialog_0;
	// EasyMobile.ConsentDialogComposerSettings EasyMobile.PrivacySettings::mConsentDialogComposerSettings
	ConsentDialogComposerSettings_t4154825527 * ___mConsentDialogComposerSettings_1;

public:
	inline static int32_t get_offset_of_mDefaultConsentDialog_0() { return static_cast<int32_t>(offsetof(PrivacySettings_t2445251604, ___mDefaultConsentDialog_0)); }
	inline ConsentDialog_t3732976094 * get_mDefaultConsentDialog_0() const { return ___mDefaultConsentDialog_0; }
	inline ConsentDialog_t3732976094 ** get_address_of_mDefaultConsentDialog_0() { return &___mDefaultConsentDialog_0; }
	inline void set_mDefaultConsentDialog_0(ConsentDialog_t3732976094 * value)
	{
		___mDefaultConsentDialog_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultConsentDialog_0), value);
	}

	inline static int32_t get_offset_of_mConsentDialogComposerSettings_1() { return static_cast<int32_t>(offsetof(PrivacySettings_t2445251604, ___mConsentDialogComposerSettings_1)); }
	inline ConsentDialogComposerSettings_t4154825527 * get_mConsentDialogComposerSettings_1() const { return ___mConsentDialogComposerSettings_1; }
	inline ConsentDialogComposerSettings_t4154825527 ** get_address_of_mConsentDialogComposerSettings_1() { return &___mConsentDialogComposerSettings_1; }
	inline void set_mConsentDialogComposerSettings_1(ConsentDialogComposerSettings_t4154825527 * value)
	{
		___mConsentDialogComposerSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&___mConsentDialogComposerSettings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVACYSETTINGS_T2445251604_H
#ifndef RATINGDIALOGCONTENT_T752797626_H
#define RATINGDIALOGCONTENT_T752797626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.RatingDialogContent
struct  RatingDialogContent_t752797626  : public RuntimeObject
{
public:
	// System.String EasyMobile.RatingDialogContent::mTitle
	String_t* ___mTitle_2;
	// System.String EasyMobile.RatingDialogContent::mMessage
	String_t* ___mMessage_3;
	// System.String EasyMobile.RatingDialogContent::mLowRatingMessage
	String_t* ___mLowRatingMessage_4;
	// System.String EasyMobile.RatingDialogContent::mHighRatingMessage
	String_t* ___mHighRatingMessage_5;
	// System.String EasyMobile.RatingDialogContent::mPostponeButtonText
	String_t* ___mPostponeButtonText_6;
	// System.String EasyMobile.RatingDialogContent::mRefuseButtonText
	String_t* ___mRefuseButtonText_7;
	// System.String EasyMobile.RatingDialogContent::mRateButtonText
	String_t* ___mRateButtonText_8;
	// System.String EasyMobile.RatingDialogContent::mCancelButtonText
	String_t* ___mCancelButtonText_9;
	// System.String EasyMobile.RatingDialogContent::mFeedbackButtonText
	String_t* ___mFeedbackButtonText_10;

public:
	inline static int32_t get_offset_of_mTitle_2() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mTitle_2)); }
	inline String_t* get_mTitle_2() const { return ___mTitle_2; }
	inline String_t** get_address_of_mTitle_2() { return &___mTitle_2; }
	inline void set_mTitle_2(String_t* value)
	{
		___mTitle_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTitle_2), value);
	}

	inline static int32_t get_offset_of_mMessage_3() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mMessage_3)); }
	inline String_t* get_mMessage_3() const { return ___mMessage_3; }
	inline String_t** get_address_of_mMessage_3() { return &___mMessage_3; }
	inline void set_mMessage_3(String_t* value)
	{
		___mMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mMessage_3), value);
	}

	inline static int32_t get_offset_of_mLowRatingMessage_4() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mLowRatingMessage_4)); }
	inline String_t* get_mLowRatingMessage_4() const { return ___mLowRatingMessage_4; }
	inline String_t** get_address_of_mLowRatingMessage_4() { return &___mLowRatingMessage_4; }
	inline void set_mLowRatingMessage_4(String_t* value)
	{
		___mLowRatingMessage_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLowRatingMessage_4), value);
	}

	inline static int32_t get_offset_of_mHighRatingMessage_5() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mHighRatingMessage_5)); }
	inline String_t* get_mHighRatingMessage_5() const { return ___mHighRatingMessage_5; }
	inline String_t** get_address_of_mHighRatingMessage_5() { return &___mHighRatingMessage_5; }
	inline void set_mHighRatingMessage_5(String_t* value)
	{
		___mHighRatingMessage_5 = value;
		Il2CppCodeGenWriteBarrier((&___mHighRatingMessage_5), value);
	}

	inline static int32_t get_offset_of_mPostponeButtonText_6() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mPostponeButtonText_6)); }
	inline String_t* get_mPostponeButtonText_6() const { return ___mPostponeButtonText_6; }
	inline String_t** get_address_of_mPostponeButtonText_6() { return &___mPostponeButtonText_6; }
	inline void set_mPostponeButtonText_6(String_t* value)
	{
		___mPostponeButtonText_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPostponeButtonText_6), value);
	}

	inline static int32_t get_offset_of_mRefuseButtonText_7() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mRefuseButtonText_7)); }
	inline String_t* get_mRefuseButtonText_7() const { return ___mRefuseButtonText_7; }
	inline String_t** get_address_of_mRefuseButtonText_7() { return &___mRefuseButtonText_7; }
	inline void set_mRefuseButtonText_7(String_t* value)
	{
		___mRefuseButtonText_7 = value;
		Il2CppCodeGenWriteBarrier((&___mRefuseButtonText_7), value);
	}

	inline static int32_t get_offset_of_mRateButtonText_8() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mRateButtonText_8)); }
	inline String_t* get_mRateButtonText_8() const { return ___mRateButtonText_8; }
	inline String_t** get_address_of_mRateButtonText_8() { return &___mRateButtonText_8; }
	inline void set_mRateButtonText_8(String_t* value)
	{
		___mRateButtonText_8 = value;
		Il2CppCodeGenWriteBarrier((&___mRateButtonText_8), value);
	}

	inline static int32_t get_offset_of_mCancelButtonText_9() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mCancelButtonText_9)); }
	inline String_t* get_mCancelButtonText_9() const { return ___mCancelButtonText_9; }
	inline String_t** get_address_of_mCancelButtonText_9() { return &___mCancelButtonText_9; }
	inline void set_mCancelButtonText_9(String_t* value)
	{
		___mCancelButtonText_9 = value;
		Il2CppCodeGenWriteBarrier((&___mCancelButtonText_9), value);
	}

	inline static int32_t get_offset_of_mFeedbackButtonText_10() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626, ___mFeedbackButtonText_10)); }
	inline String_t* get_mFeedbackButtonText_10() const { return ___mFeedbackButtonText_10; }
	inline String_t** get_address_of_mFeedbackButtonText_10() { return &___mFeedbackButtonText_10; }
	inline void set_mFeedbackButtonText_10(String_t* value)
	{
		___mFeedbackButtonText_10 = value;
		Il2CppCodeGenWriteBarrier((&___mFeedbackButtonText_10), value);
	}
};

struct RatingDialogContent_t752797626_StaticFields
{
public:
	// EasyMobile.RatingDialogContent EasyMobile.RatingDialogContent::Default
	RatingDialogContent_t752797626 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(RatingDialogContent_t752797626_StaticFields, ___Default_1)); }
	inline RatingDialogContent_t752797626 * get_Default_1() const { return ___Default_1; }
	inline RatingDialogContent_t752797626 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(RatingDialogContent_t752797626 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RATINGDIALOGCONTENT_T752797626_H
#ifndef RATINGREQUESTSETTINGS_T4213169708_H
#define RATINGREQUESTSETTINGS_T4213169708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.RatingRequestSettings
struct  RatingRequestSettings_t4213169708  : public RuntimeObject
{
public:
	// EasyMobile.RatingDialogContent EasyMobile.RatingRequestSettings::mDefaultRatingDialogContent
	RatingDialogContent_t752797626 * ___mDefaultRatingDialogContent_0;
	// System.UInt32 EasyMobile.RatingRequestSettings::mMinimumAcceptedStars
	uint32_t ___mMinimumAcceptedStars_1;
	// System.String EasyMobile.RatingRequestSettings::mSupportEmail
	String_t* ___mSupportEmail_2;
	// System.String EasyMobile.RatingRequestSettings::mIosAppId
	String_t* ___mIosAppId_3;
	// System.UInt32 EasyMobile.RatingRequestSettings::mAnnualCap
	uint32_t ___mAnnualCap_4;
	// System.UInt32 EasyMobile.RatingRequestSettings::mDelayAfterInstallation
	uint32_t ___mDelayAfterInstallation_5;
	// System.UInt32 EasyMobile.RatingRequestSettings::mCoolingOffPeriod
	uint32_t ___mCoolingOffPeriod_6;
	// System.Boolean EasyMobile.RatingRequestSettings::mIgnoreContraintsInDevelopment
	bool ___mIgnoreContraintsInDevelopment_7;

public:
	inline static int32_t get_offset_of_mDefaultRatingDialogContent_0() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mDefaultRatingDialogContent_0)); }
	inline RatingDialogContent_t752797626 * get_mDefaultRatingDialogContent_0() const { return ___mDefaultRatingDialogContent_0; }
	inline RatingDialogContent_t752797626 ** get_address_of_mDefaultRatingDialogContent_0() { return &___mDefaultRatingDialogContent_0; }
	inline void set_mDefaultRatingDialogContent_0(RatingDialogContent_t752797626 * value)
	{
		___mDefaultRatingDialogContent_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultRatingDialogContent_0), value);
	}

	inline static int32_t get_offset_of_mMinimumAcceptedStars_1() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mMinimumAcceptedStars_1)); }
	inline uint32_t get_mMinimumAcceptedStars_1() const { return ___mMinimumAcceptedStars_1; }
	inline uint32_t* get_address_of_mMinimumAcceptedStars_1() { return &___mMinimumAcceptedStars_1; }
	inline void set_mMinimumAcceptedStars_1(uint32_t value)
	{
		___mMinimumAcceptedStars_1 = value;
	}

	inline static int32_t get_offset_of_mSupportEmail_2() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mSupportEmail_2)); }
	inline String_t* get_mSupportEmail_2() const { return ___mSupportEmail_2; }
	inline String_t** get_address_of_mSupportEmail_2() { return &___mSupportEmail_2; }
	inline void set_mSupportEmail_2(String_t* value)
	{
		___mSupportEmail_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportEmail_2), value);
	}

	inline static int32_t get_offset_of_mIosAppId_3() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mIosAppId_3)); }
	inline String_t* get_mIosAppId_3() const { return ___mIosAppId_3; }
	inline String_t** get_address_of_mIosAppId_3() { return &___mIosAppId_3; }
	inline void set_mIosAppId_3(String_t* value)
	{
		___mIosAppId_3 = value;
		Il2CppCodeGenWriteBarrier((&___mIosAppId_3), value);
	}

	inline static int32_t get_offset_of_mAnnualCap_4() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mAnnualCap_4)); }
	inline uint32_t get_mAnnualCap_4() const { return ___mAnnualCap_4; }
	inline uint32_t* get_address_of_mAnnualCap_4() { return &___mAnnualCap_4; }
	inline void set_mAnnualCap_4(uint32_t value)
	{
		___mAnnualCap_4 = value;
	}

	inline static int32_t get_offset_of_mDelayAfterInstallation_5() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mDelayAfterInstallation_5)); }
	inline uint32_t get_mDelayAfterInstallation_5() const { return ___mDelayAfterInstallation_5; }
	inline uint32_t* get_address_of_mDelayAfterInstallation_5() { return &___mDelayAfterInstallation_5; }
	inline void set_mDelayAfterInstallation_5(uint32_t value)
	{
		___mDelayAfterInstallation_5 = value;
	}

	inline static int32_t get_offset_of_mCoolingOffPeriod_6() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mCoolingOffPeriod_6)); }
	inline uint32_t get_mCoolingOffPeriod_6() const { return ___mCoolingOffPeriod_6; }
	inline uint32_t* get_address_of_mCoolingOffPeriod_6() { return &___mCoolingOffPeriod_6; }
	inline void set_mCoolingOffPeriod_6(uint32_t value)
	{
		___mCoolingOffPeriod_6 = value;
	}

	inline static int32_t get_offset_of_mIgnoreContraintsInDevelopment_7() { return static_cast<int32_t>(offsetof(RatingRequestSettings_t4213169708, ___mIgnoreContraintsInDevelopment_7)); }
	inline bool get_mIgnoreContraintsInDevelopment_7() const { return ___mIgnoreContraintsInDevelopment_7; }
	inline bool* get_address_of_mIgnoreContraintsInDevelopment_7() { return &___mIgnoreContraintsInDevelopment_7; }
	inline void set_mIgnoreContraintsInDevelopment_7(bool value)
	{
		___mIgnoreContraintsInDevelopment_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RATINGREQUESTSETTINGS_T4213169708_H
#ifndef SHARING_T4167477152_H
#define SHARING_T4167477152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Sharing
struct  Sharing_t4167477152  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARING_T4167477152_H
#ifndef NSLOCALE_T2849095482_H
#define NSLOCALE_T2849095482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.Foundation.NSLocale
struct  NSLocale_t2849095482  : public RuntimeObject
{
public:

public:
};

struct NSLocale_t2849095482_StaticFields
{
public:
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache0
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache0_0;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache1
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache1_1;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache2
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache2_2;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache3
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache3_3;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache4
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache4_4;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache5
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache5_5;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSLocale::<>f__mg$cache6
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache6_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_1() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache1_1)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache1_1() const { return ___U3CU3Ef__mgU24cache1_1; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache1_1() { return &___U3CU3Ef__mgU24cache1_1; }
	inline void set_U3CU3Ef__mgU24cache1_1(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_2() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache2_2)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache2_2() const { return ___U3CU3Ef__mgU24cache2_2; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache2_2() { return &___U3CU3Ef__mgU24cache2_2; }
	inline void set_U3CU3Ef__mgU24cache2_2(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_3() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache3_3)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache3_3() const { return ___U3CU3Ef__mgU24cache3_3; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache3_3() { return &___U3CU3Ef__mgU24cache3_3; }
	inline void set_U3CU3Ef__mgU24cache3_3(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_4() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache4_4)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache4_4() const { return ___U3CU3Ef__mgU24cache4_4; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache4_4() { return &___U3CU3Ef__mgU24cache4_4; }
	inline void set_U3CU3Ef__mgU24cache4_4(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_5() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache5_5)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache5_5() const { return ___U3CU3Ef__mgU24cache5_5; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache5_5() { return &___U3CU3Ef__mgU24cache5_5; }
	inline void set_U3CU3Ef__mgU24cache5_5(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_6() { return static_cast<int32_t>(offsetof(NSLocale_t2849095482_StaticFields, ___U3CU3Ef__mgU24cache6_6)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache6_6() const { return ___U3CU3Ef__mgU24cache6_6; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache6_6() { return &___U3CU3Ef__mgU24cache6_6; }
	inline void set_U3CU3Ef__mgU24cache6_6(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSLOCALE_T2849095482_H
#ifndef NSTIMEZONE_T3876610886_H
#define NSTIMEZONE_T3876610886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.Foundation.NSTimeZone
struct  NSTimeZone_t3876610886  : public RuntimeObject
{
public:

public:
};

struct NSTimeZone_t3876610886_StaticFields
{
public:
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSTimeZone::<>f__mg$cache0
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache0_0;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Foundation.NSTimeZone::<>f__mg$cache1
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(NSTimeZone_t3876610886_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_1() { return static_cast<int32_t>(offsetof(NSTimeZone_t3876610886_StaticFields, ___U3CU3Ef__mgU24cache1_1)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache1_1() const { return ___U3CU3Ef__mgU24cache1_1; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache1_1() { return &___U3CU3Ef__mgU24cache1_1; }
	inline void set_U3CU3Ef__mgU24cache1_1(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSTIMEZONE_T3876610886_H
#ifndef CTCARRIER_T4250217597_H
#define CTCARRIER_T4250217597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.Telephony.CTCarrier
struct  CTCarrier_t4250217597  : public RuntimeObject
{
public:

public:
};

struct CTCarrier_t4250217597_StaticFields
{
public:
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Telephony.CTCarrier::<>f__mg$cache0
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache0_0;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Telephony.CTCarrier::<>f__mg$cache1
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache1_1;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Telephony.CTCarrier::<>f__mg$cache2
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache2_2;
	// EasyMobile.Internal.PInvokeUtil/NativeToManagedArray`1<System.Byte> EasyMobile.iOS.Telephony.CTCarrier::<>f__mg$cache3
	NativeToManagedArray_1_t3233103070 * ___U3CU3Ef__mgU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(CTCarrier_t4250217597_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_1() { return static_cast<int32_t>(offsetof(CTCarrier_t4250217597_StaticFields, ___U3CU3Ef__mgU24cache1_1)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache1_1() const { return ___U3CU3Ef__mgU24cache1_1; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache1_1() { return &___U3CU3Ef__mgU24cache1_1; }
	inline void set_U3CU3Ef__mgU24cache1_1(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_2() { return static_cast<int32_t>(offsetof(CTCarrier_t4250217597_StaticFields, ___U3CU3Ef__mgU24cache2_2)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache2_2() const { return ___U3CU3Ef__mgU24cache2_2; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache2_2() { return &___U3CU3Ef__mgU24cache2_2; }
	inline void set_U3CU3Ef__mgU24cache2_2(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_3() { return static_cast<int32_t>(offsetof(CTCarrier_t4250217597_StaticFields, ___U3CU3Ef__mgU24cache3_3)); }
	inline NativeToManagedArray_1_t3233103070 * get_U3CU3Ef__mgU24cache3_3() const { return ___U3CU3Ef__mgU24cache3_3; }
	inline NativeToManagedArray_1_t3233103070 ** get_address_of_U3CU3Ef__mgU24cache3_3() { return &___U3CU3Ef__mgU24cache3_3; }
	inline void set_U3CU3Ef__mgU24cache3_3(NativeToManagedArray_1_t3233103070 * value)
	{
		___U3CU3Ef__mgU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTCARRIER_T4250217597_H
#ifndef UIAPPLICATION_T1418071823_H
#define UIAPPLICATION_T1418071823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.UIKit.UIApplication
struct  UIApplication_t1418071823  : public RuntimeObject
{
public:

public:
};

struct UIApplication_t1418071823_StaticFields
{
public:
	// EasyMobile.iOS.UIKit.UIApplication EasyMobile.iOS.UIKit.UIApplication::sSharedApplication
	UIApplication_t1418071823 * ___sSharedApplication_0;

public:
	inline static int32_t get_offset_of_sSharedApplication_0() { return static_cast<int32_t>(offsetof(UIApplication_t1418071823_StaticFields, ___sSharedApplication_0)); }
	inline UIApplication_t1418071823 * get_sSharedApplication_0() const { return ___sSharedApplication_0; }
	inline UIApplication_t1418071823 ** get_address_of_sSharedApplication_0() { return &___sSharedApplication_0; }
	inline void set_sSharedApplication_0(UIApplication_t1418071823 * value)
	{
		___sSharedApplication_0 = value;
		Il2CppCodeGenWriteBarrier((&___sSharedApplication_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIAPPLICATION_T1418071823_H
#ifndef U3CGETJPEGDATAU3EC__ANONSTOREY0_T3081864437_H
#define U3CGETJPEGDATAU3EC__ANONSTOREY0_T3081864437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.UIKit.UIImage/<GetJPEGData>c__AnonStorey0
struct  U3CGetJPEGDataU3Ec__AnonStorey0_t3081864437  : public RuntimeObject
{
public:
	// System.Single EasyMobile.iOS.UIKit.UIImage/<GetJPEGData>c__AnonStorey0::compressionQuality
	float ___compressionQuality_0;
	// EasyMobile.iOS.UIKit.UIImage EasyMobile.iOS.UIKit.UIImage/<GetJPEGData>c__AnonStorey0::$this
	UIImage_t4033140513 * ___U24this_1;

public:
	inline static int32_t get_offset_of_compressionQuality_0() { return static_cast<int32_t>(offsetof(U3CGetJPEGDataU3Ec__AnonStorey0_t3081864437, ___compressionQuality_0)); }
	inline float get_compressionQuality_0() const { return ___compressionQuality_0; }
	inline float* get_address_of_compressionQuality_0() { return &___compressionQuality_0; }
	inline void set_compressionQuality_0(float value)
	{
		___compressionQuality_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetJPEGDataU3Ec__AnonStorey0_t3081864437, ___U24this_1)); }
	inline UIImage_t4033140513 * get_U24this_1() const { return ___U24this_1; }
	inline UIImage_t4033140513 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UIImage_t4033140513 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETJPEGDATAU3EC__ANONSTOREY0_T3081864437_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef GLOBALCONSENTMANAGER_T3680228011_H
#define GLOBALCONSENTMANAGER_T3680228011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.GlobalConsentManager
struct  GlobalConsentManager_t3680228011  : public ConsentManager_t1411390104
{
public:

public:
};

struct GlobalConsentManager_t3680228011_StaticFields
{
public:
	// EasyMobile.GlobalConsentManager EasyMobile.GlobalConsentManager::sInstance
	GlobalConsentManager_t3680228011 * ___sInstance_3;

public:
	inline static int32_t get_offset_of_sInstance_3() { return static_cast<int32_t>(offsetof(GlobalConsentManager_t3680228011_StaticFields, ___sInstance_3)); }
	inline GlobalConsentManager_t3680228011 * get_sInstance_3() const { return ___sInstance_3; }
	inline GlobalConsentManager_t3680228011 ** get_address_of_sInstance_3() { return &___sInstance_3; }
	inline void set_sInstance_3(GlobalConsentManager_t3680228011 * value)
	{
		___sInstance_3 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALCONSENTMANAGER_T3680228011_H
#ifndef ICONNAME_T2443857936_H
#define ICONNAME_T2443857936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName
struct  IconName_t2443857936 
{
public:
	// System.String EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName::name
	String_t* ___name_0;
	// UnityEngine.Sprite EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName::sprite
	Sprite_t280657092 * ___sprite_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IconName_t2443857936, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sprite_1() { return static_cast<int32_t>(offsetof(IconName_t2443857936, ___sprite_1)); }
	inline Sprite_t280657092 * get_sprite_1() const { return ___sprite_1; }
	inline Sprite_t280657092 ** get_address_of_sprite_1() { return &___sprite_1; }
	inline void set_sprite_1(Sprite_t280657092 * value)
	{
		___sprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName
struct IconName_t2443857936_marshaled_pinvoke
{
	char* ___name_0;
	Sprite_t280657092 * ___sprite_1;
};
// Native definition for COM marshalling of EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName
struct IconName_t2443857936_marshaled_com
{
	Il2CppChar* ___name_0;
	Sprite_t280657092 * ___sprite_1;
};
#endif // ICONNAME_T2443857936_H
#ifndef IOSCONSENTDIALOGLISTENERINFO_T124266453_H
#define IOSCONSENTDIALOGLISTENERINFO_T124266453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo
struct  iOSConsentDialogListenerInfo_t124266453 
{
public:
	// System.String EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo::name
	String_t* ___name_0;
	// System.String EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo::onToggleBecameOnHandler
	String_t* ___onToggleBecameOnHandler_1;
	// System.String EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo::onToggleBecameOffHandler
	String_t* ___onToggleBecameOffHandler_2;
	// System.String EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo::onDialogCompletedHandler
	String_t* ___onDialogCompletedHandler_3;
	// System.String EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo::onDialogDismissedHandler
	String_t* ___onDialogDismissedHandler_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(iOSConsentDialogListenerInfo_t124266453, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_onToggleBecameOnHandler_1() { return static_cast<int32_t>(offsetof(iOSConsentDialogListenerInfo_t124266453, ___onToggleBecameOnHandler_1)); }
	inline String_t* get_onToggleBecameOnHandler_1() const { return ___onToggleBecameOnHandler_1; }
	inline String_t** get_address_of_onToggleBecameOnHandler_1() { return &___onToggleBecameOnHandler_1; }
	inline void set_onToggleBecameOnHandler_1(String_t* value)
	{
		___onToggleBecameOnHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___onToggleBecameOnHandler_1), value);
	}

	inline static int32_t get_offset_of_onToggleBecameOffHandler_2() { return static_cast<int32_t>(offsetof(iOSConsentDialogListenerInfo_t124266453, ___onToggleBecameOffHandler_2)); }
	inline String_t* get_onToggleBecameOffHandler_2() const { return ___onToggleBecameOffHandler_2; }
	inline String_t** get_address_of_onToggleBecameOffHandler_2() { return &___onToggleBecameOffHandler_2; }
	inline void set_onToggleBecameOffHandler_2(String_t* value)
	{
		___onToggleBecameOffHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___onToggleBecameOffHandler_2), value);
	}

	inline static int32_t get_offset_of_onDialogCompletedHandler_3() { return static_cast<int32_t>(offsetof(iOSConsentDialogListenerInfo_t124266453, ___onDialogCompletedHandler_3)); }
	inline String_t* get_onDialogCompletedHandler_3() const { return ___onDialogCompletedHandler_3; }
	inline String_t** get_address_of_onDialogCompletedHandler_3() { return &___onDialogCompletedHandler_3; }
	inline void set_onDialogCompletedHandler_3(String_t* value)
	{
		___onDialogCompletedHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDialogCompletedHandler_3), value);
	}

	inline static int32_t get_offset_of_onDialogDismissedHandler_4() { return static_cast<int32_t>(offsetof(iOSConsentDialogListenerInfo_t124266453, ___onDialogDismissedHandler_4)); }
	inline String_t* get_onDialogDismissedHandler_4() const { return ___onDialogDismissedHandler_4; }
	inline String_t** get_address_of_onDialogDismissedHandler_4() { return &___onDialogDismissedHandler_4; }
	inline void set_onDialogDismissedHandler_4(String_t* value)
	{
		___onDialogDismissedHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___onDialogDismissedHandler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo
struct iOSConsentDialogListenerInfo_t124266453_marshaled_pinvoke
{
	char* ___name_0;
	char* ___onToggleBecameOnHandler_1;
	char* ___onToggleBecameOffHandler_2;
	char* ___onDialogCompletedHandler_3;
	char* ___onDialogDismissedHandler_4;
};
// Native definition for COM marshalling of EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo
struct iOSConsentDialogListenerInfo_t124266453_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___onToggleBecameOnHandler_1;
	Il2CppChar* ___onToggleBecameOffHandler_2;
	Il2CppChar* ___onDialogCompletedHandler_3;
	Il2CppChar* ___onDialogDismissedHandler_4;
};
#endif // IOSCONSENTDIALOGLISTENERINFO_T124266453_H
#ifndef IOSEEAREGIONVALIDATOR_T2872212933_H
#define IOSEEAREGIONVALIDATOR_T2872212933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.iOSEEARegionValidator
struct  iOSEEARegionValidator_t2872212933  : public BaseNativeEEARegionValidator_t3155147623
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSEEAREGIONVALIDATOR_T2872212933_H
#ifndef SHAREDATA_T888004449_H
#define SHAREDATA_T888004449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData
struct  ShareData_t888004449 
{
public:
	// System.String EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData::text
	String_t* ___text_0;
	// System.String EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData::url
	String_t* ___url_1;
	// System.String EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData::image
	String_t* ___image_2;
	// System.String EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData::subject
	String_t* ___subject_3;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(ShareData_t888004449, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(ShareData_t888004449, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(ShareData_t888004449, ___image_2)); }
	inline String_t* get_image_2() const { return ___image_2; }
	inline String_t** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(String_t* value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier((&___image_2), value);
	}

	inline static int32_t get_offset_of_subject_3() { return static_cast<int32_t>(offsetof(ShareData_t888004449, ___subject_3)); }
	inline String_t* get_subject_3() const { return ___subject_3; }
	inline String_t** get_address_of_subject_3() { return &___subject_3; }
	inline void set_subject_3(String_t* value)
	{
		___subject_3 = value;
		Il2CppCodeGenWriteBarrier((&___subject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData
struct ShareData_t888004449_marshaled_pinvoke
{
	char* ___text_0;
	char* ___url_1;
	char* ___image_2;
	char* ___subject_3;
};
// Native definition for COM marshalling of EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData
struct ShareData_t888004449_marshaled_com
{
	Il2CppChar* ___text_0;
	Il2CppChar* ___url_1;
	Il2CppChar* ___image_2;
	Il2CppChar* ___subject_3;
};
#endif // SHAREDATA_T888004449_H
#ifndef LOCALNOTIFICATION_T343919340_H
#define LOCALNOTIFICATION_T343919340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.LocalNotification
struct  LocalNotification_t343919340  : public Notification_t414530818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALNOTIFICATION_T343919340_H
#ifndef ACTIONBUTTON_T554814726_H
#define ACTIONBUTTON_T554814726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory/ActionButton
struct  ActionButton_t554814726 
{
public:
	// System.String EasyMobile.NotificationCategory/ActionButton::id
	String_t* ___id_0;
	// System.String EasyMobile.NotificationCategory/ActionButton::title
	String_t* ___title_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ActionButton_t554814726, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(ActionButton_t554814726, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EasyMobile.NotificationCategory/ActionButton
struct ActionButton_t554814726_marshaled_pinvoke
{
	char* ___id_0;
	char* ___title_1;
};
// Native definition for COM marshalling of EasyMobile.NotificationCategory/ActionButton
struct ActionButton_t554814726_marshaled_com
{
	Il2CppChar* ___id_0;
	Il2CppChar* ___title_1;
};
#endif // ACTIONBUTTON_T554814726_H
#ifndef NOTIFICATIONSCONSENTMANAGER_T864684174_H
#define NOTIFICATIONSCONSENTMANAGER_T864684174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationsConsentManager
struct  NotificationsConsentManager_t864684174  : public ConsentManager_t1411390104
{
public:

public:
};

struct NotificationsConsentManager_t864684174_StaticFields
{
public:
	// EasyMobile.NotificationsConsentManager EasyMobile.NotificationsConsentManager::sInstance
	NotificationsConsentManager_t864684174 * ___sInstance_3;

public:
	inline static int32_t get_offset_of_sInstance_3() { return static_cast<int32_t>(offsetof(NotificationsConsentManager_t864684174_StaticFields, ___sInstance_3)); }
	inline NotificationsConsentManager_t864684174 * get_sInstance_3() const { return ___sInstance_3; }
	inline NotificationsConsentManager_t864684174 ** get_address_of_sInstance_3() { return &___sInstance_3; }
	inline void set_sInstance_3(NotificationsConsentManager_t864684174 * value)
	{
		___sInstance_3 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSCONSENTMANAGER_T864684174_H
#ifndef REMOTENOTIFICATION_T1687069461_H
#define REMOTENOTIFICATION_T1687069461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.RemoteNotification
struct  RemoteNotification_t1687069461  : public Notification_t414530818
{
public:
	// EasyMobile.OneSignalNotificationPayload EasyMobile.RemoteNotification::oneSignalPayload
	OneSignalNotificationPayload_t1758448350 * ___oneSignalPayload_5;
	// EasyMobile.FirebaseMessage EasyMobile.RemoteNotification::firebasePayload
	FirebaseMessage_t1021966236 * ___firebasePayload_6;

public:
	inline static int32_t get_offset_of_oneSignalPayload_5() { return static_cast<int32_t>(offsetof(RemoteNotification_t1687069461, ___oneSignalPayload_5)); }
	inline OneSignalNotificationPayload_t1758448350 * get_oneSignalPayload_5() const { return ___oneSignalPayload_5; }
	inline OneSignalNotificationPayload_t1758448350 ** get_address_of_oneSignalPayload_5() { return &___oneSignalPayload_5; }
	inline void set_oneSignalPayload_5(OneSignalNotificationPayload_t1758448350 * value)
	{
		___oneSignalPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___oneSignalPayload_5), value);
	}

	inline static int32_t get_offset_of_firebasePayload_6() { return static_cast<int32_t>(offsetof(RemoteNotification_t1687069461, ___firebasePayload_6)); }
	inline FirebaseMessage_t1021966236 * get_firebasePayload_6() const { return ___firebasePayload_6; }
	inline FirebaseMessage_t1021966236 ** get_address_of_firebasePayload_6() { return &___firebasePayload_6; }
	inline void set_firebasePayload_6(FirebaseMessage_t1021966236 * value)
	{
		___firebasePayload_6 = value;
		Il2CppCodeGenWriteBarrier((&___firebasePayload_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTENOTIFICATION_T1687069461_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef BUTTON_T2810410457_H
#define BUTTON_T2810410457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/Button
struct  Button_t2810410457  : public RuntimeObject
{
public:
	// System.String EasyMobile.ConsentDialog/Button::id
	String_t* ___id_0;
	// System.String EasyMobile.ConsentDialog/Button::title
	String_t* ___title_1;
	// System.Boolean EasyMobile.ConsentDialog/Button::interactable
	bool ___interactable_2;
	// UnityEngine.Color EasyMobile.ConsentDialog/Button::titleColor
	Color_t2555686324  ___titleColor_3;
	// UnityEngine.Color EasyMobile.ConsentDialog/Button::backgroundColor
	Color_t2555686324  ___backgroundColor_4;
	// UnityEngine.Color EasyMobile.ConsentDialog/Button::uninteractableTitleColor
	Color_t2555686324  ___uninteractableTitleColor_5;
	// UnityEngine.Color EasyMobile.ConsentDialog/Button::uninteractableBackgroundColor
	Color_t2555686324  ___uninteractableBackgroundColor_6;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_interactable_2() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___interactable_2)); }
	inline bool get_interactable_2() const { return ___interactable_2; }
	inline bool* get_address_of_interactable_2() { return &___interactable_2; }
	inline void set_interactable_2(bool value)
	{
		___interactable_2 = value;
	}

	inline static int32_t get_offset_of_titleColor_3() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___titleColor_3)); }
	inline Color_t2555686324  get_titleColor_3() const { return ___titleColor_3; }
	inline Color_t2555686324 * get_address_of_titleColor_3() { return &___titleColor_3; }
	inline void set_titleColor_3(Color_t2555686324  value)
	{
		___titleColor_3 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_4() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___backgroundColor_4)); }
	inline Color_t2555686324  get_backgroundColor_4() const { return ___backgroundColor_4; }
	inline Color_t2555686324 * get_address_of_backgroundColor_4() { return &___backgroundColor_4; }
	inline void set_backgroundColor_4(Color_t2555686324  value)
	{
		___backgroundColor_4 = value;
	}

	inline static int32_t get_offset_of_uninteractableTitleColor_5() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___uninteractableTitleColor_5)); }
	inline Color_t2555686324  get_uninteractableTitleColor_5() const { return ___uninteractableTitleColor_5; }
	inline Color_t2555686324 * get_address_of_uninteractableTitleColor_5() { return &___uninteractableTitleColor_5; }
	inline void set_uninteractableTitleColor_5(Color_t2555686324  value)
	{
		___uninteractableTitleColor_5 = value;
	}

	inline static int32_t get_offset_of_uninteractableBackgroundColor_6() { return static_cast<int32_t>(offsetof(Button_t2810410457, ___uninteractableBackgroundColor_6)); }
	inline Color_t2555686324  get_uninteractableBackgroundColor_6() const { return ___uninteractableBackgroundColor_6; }
	inline Color_t2555686324 * get_address_of_uninteractableBackgroundColor_6() { return &___uninteractableBackgroundColor_6; }
	inline void set_uninteractableBackgroundColor_6(Color_t2555686324  value)
	{
		___uninteractableBackgroundColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T2810410457_H
#ifndef CONSENTSTATUS_T2718071093_H
#define CONSENTSTATUS_T2718071093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentStatus
struct  ConsentStatus_t2718071093 
{
public:
	// System.Int32 EasyMobile.ConsentStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConsentStatus_t2718071093, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENTSTATUS_T2718071093_H
#ifndef EEACOUNTRIES_T2360821445_H
#define EEACOUNTRIES_T2360821445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EEACountries
struct  EEACountries_t2360821445 
{
public:
	// System.Int32 EasyMobile.EEACountries::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EEACountries_t2360821445, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEACOUNTRIES_T2360821445_H
#ifndef EEAREGIONSTATUS_T3128437537_H
#define EEAREGIONSTATUS_T3128437537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EEARegionStatus
struct  EEARegionStatus_t3128437537 
{
public:
	// System.Int32 EasyMobile.EEARegionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EEARegionStatus_t3128437537, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEAREGIONSTATUS_T3128437537_H
#ifndef EEAREGIONVALIDATIONMETHODS_T2477000099_H
#define EEAREGIONVALIDATIONMETHODS_T2477000099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.EEARegionValidationMethods
struct  EEARegionValidationMethods_t2477000099 
{
public:
	// System.Int32 EasyMobile.EEARegionValidationMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EEARegionValidationMethods_t2477000099, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEAREGIONVALIDATIONMETHODS_T2477000099_H
#ifndef U3CSWITCHCOROUTINEU3EC__ITERATOR0_T1662270879_H
#define U3CSWITCHCOROUTINEU3EC__ITERATOR0_T1662270879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0
struct  U3CSwitchCoroutineU3Ec__Iterator0_t1662270879  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::isOn
	bool ___isOn_0;
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::<currentLerpValue>__0
	float ___U3CcurrentLerpValueU3E__0_1;
	// UnityEngine.Vector2 EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::<startPosition>__0
	Vector2_t2156229523  ___U3CstartPositionU3E__0_2;
	// UnityEngine.Vector2 EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::targetPosition
	Vector2_t2156229523  ___targetPosition_3;
	// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::$this
	EditorConsentDialogToggleSwitch_t2938536456 * ___U24this_4;
	// System.Object EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/<SwitchCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_isOn_0() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___isOn_0)); }
	inline bool get_isOn_0() const { return ___isOn_0; }
	inline bool* get_address_of_isOn_0() { return &___isOn_0; }
	inline void set_isOn_0(bool value)
	{
		___isOn_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentLerpValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___U3CcurrentLerpValueU3E__0_1)); }
	inline float get_U3CcurrentLerpValueU3E__0_1() const { return ___U3CcurrentLerpValueU3E__0_1; }
	inline float* get_address_of_U3CcurrentLerpValueU3E__0_1() { return &___U3CcurrentLerpValueU3E__0_1; }
	inline void set_U3CcurrentLerpValueU3E__0_1(float value)
	{
		___U3CcurrentLerpValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___U3CstartPositionU3E__0_2)); }
	inline Vector2_t2156229523  get_U3CstartPositionU3E__0_2() const { return ___U3CstartPositionU3E__0_2; }
	inline Vector2_t2156229523 * get_address_of_U3CstartPositionU3E__0_2() { return &___U3CstartPositionU3E__0_2; }
	inline void set_U3CstartPositionU3E__0_2(Vector2_t2156229523  value)
	{
		___U3CstartPositionU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_targetPosition_3() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___targetPosition_3)); }
	inline Vector2_t2156229523  get_targetPosition_3() const { return ___targetPosition_3; }
	inline Vector2_t2156229523 * get_address_of_targetPosition_3() { return &___targetPosition_3; }
	inline void set_targetPosition_3(Vector2_t2156229523  value)
	{
		___targetPosition_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___U24this_4)); }
	inline EditorConsentDialogToggleSwitch_t2938536456 * get_U24this_4() const { return ___U24this_4; }
	inline EditorConsentDialogToggleSwitch_t2938536456 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(EditorConsentDialogToggleSwitch_t2938536456 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSwitchCoroutineU3Ec__Iterator0_t1662270879, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSWITCHCOROUTINEU3EC__ITERATOR0_T1662270879_H
#ifndef TOGGLEEVENT_T1368036467_H
#define TOGGLEEVENT_T1368036467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/ToggleEvent
struct  ToggleEvent_t1368036467  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEEVENT_T1368036467_H
#ifndef NOTIFICATIONAUTHOPTIONS_T1370813110_H
#define NOTIFICATIONAUTHOPTIONS_T1370813110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationAuthOptions
struct  NotificationAuthOptions_t1370813110 
{
public:
	// System.Int32 EasyMobile.NotificationAuthOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotificationAuthOptions_t1370813110, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONAUTHOPTIONS_T1370813110_H
#ifndef IMPORTANCE_T3090563125_H
#define IMPORTANCE_T3090563125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory/Importance
struct  Importance_t3090563125 
{
public:
	// System.Int32 EasyMobile.NotificationCategory/Importance::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Importance_t3090563125, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTANCE_T3090563125_H
#ifndef LIGHTOPTIONS_T1731901357_H
#define LIGHTOPTIONS_T1731901357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory/LightOptions
struct  LightOptions_t1731901357 
{
public:
	// System.Int32 EasyMobile.NotificationCategory/LightOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightOptions_t1731901357, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTOPTIONS_T1731901357_H
#ifndef LOCKSCREENVISIBILITYOPTIONS_T1989431188_H
#define LOCKSCREENVISIBILITYOPTIONS_T1989431188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory/LockScreenVisibilityOptions
struct  LockScreenVisibilityOptions_t1989431188 
{
public:
	// System.Int32 EasyMobile.NotificationCategory/LockScreenVisibilityOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LockScreenVisibilityOptions_t1989431188, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCKSCREENVISIBILITYOPTIONS_T1989431188_H
#ifndef SOUNDOPTIONS_T3551592480_H
#define SOUNDOPTIONS_T3551592480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory/SoundOptions
struct  SoundOptions_t3551592480 
{
public:
	// System.Int32 EasyMobile.NotificationCategory/SoundOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SoundOptions_t3551592480, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDOPTIONS_T3551592480_H
#ifndef VIBRATIONOPTIONS_T3667218198_H
#define VIBRATIONOPTIONS_T3667218198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory/VibrationOptions
struct  VibrationOptions_t3667218198 
{
public:
	// System.Int32 EasyMobile.NotificationCategory/VibrationOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VibrationOptions_t3667218198, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIBRATIONOPTIONS_T3667218198_H
#ifndef NOTIFICATIONREPEAT_T3969194584_H
#define NOTIFICATIONREPEAT_T3969194584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationRepeat
struct  NotificationRepeat_t3969194584 
{
public:
	// System.Int32 EasyMobile.NotificationRepeat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotificationRepeat_t3969194584, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONREPEAT_T3969194584_H
#ifndef PUSHNOTIFICATIONPROVIDER_T1742081748_H
#define PUSHNOTIFICATIONPROVIDER_T1742081748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.PushNotificationProvider
struct  PushNotificationProvider_t1742081748 
{
public:
	// System.Int32 EasyMobile.PushNotificationProvider::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PushNotificationProvider_t1742081748, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUSHNOTIFICATIONPROVIDER_T1742081748_H
#ifndef USERACTION_T1007852822_H
#define USERACTION_T1007852822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.StoreReview/UserAction
struct  UserAction_t1007852822 
{
public:
	// System.Int32 EasyMobile.StoreReview/UserAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserAction_t1007852822, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERACTION_T1007852822_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef HANDLEREF_T3745784362_H
#define HANDLEREF_T3745784362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t3745784362 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::m_wrapper
	RuntimeObject * ___m_wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::m_handle
	intptr_t ___m_handle_1;

public:
	inline static int32_t get_offset_of_m_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___m_wrapper_0)); }
	inline RuntimeObject * get_m_wrapper_0() const { return ___m_wrapper_0; }
	inline RuntimeObject ** get_address_of_m_wrapper_0() { return &___m_wrapper_0; }
	inline void set_m_wrapper_0(RuntimeObject * value)
	{
		___m_wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_wrapper_0), value);
	}

	inline static int32_t get_offset_of_m_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t3745784362, ___m_handle_1)); }
	inline intptr_t get_m_handle_1() const { return ___m_handle_1; }
	inline intptr_t* get_address_of_m_handle_1() { return &___m_handle_1; }
	inline void set_m_handle_1(intptr_t value)
	{
		___m_handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEREF_T3745784362_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef U3CVALIDATEEEAREGIONSTATUSCOROUTINEU3EC__ITERATOR0_T1025791164_H
#define U3CVALIDATEEEAREGIONSTATUSCOROUTINEU3EC__ITERATOR0_T1025791164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0
struct  U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<EasyMobile.EEARegionValidationMethods> EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::methods
	List_1_t3949074841 * ___methods_0;
	// System.Action`1<EasyMobile.EEARegionStatus> EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::callback
	Action_1_t3300905132 * ___callback_1;
	// System.Collections.Generic.Stack`1<EasyMobile.EEARegionStatus> EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::<results>__0
	Stack_1_t3971826992 * ___U3CresultsU3E__0_2;
	// System.Collections.Generic.IEnumerator`1<EasyMobile.EEARegionValidationMethods> EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_3;
	// EasyMobile.EEARegionValidationMethods EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::<method>__1
	int32_t ___U3CmethodU3E__1_4;
	// EasyMobile.EEARegionStatus EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::<result>__0
	int32_t ___U3CresultU3E__0_5;
	// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::$this
	BaseNativeEEARegionValidator_t3155147623 * ___U24this_6;
	// System.Object EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateEEARegionStatusCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_methods_0() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___methods_0)); }
	inline List_1_t3949074841 * get_methods_0() const { return ___methods_0; }
	inline List_1_t3949074841 ** get_address_of_methods_0() { return &___methods_0; }
	inline void set_methods_0(List_1_t3949074841 * value)
	{
		___methods_0 = value;
		Il2CppCodeGenWriteBarrier((&___methods_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___callback_1)); }
	inline Action_1_t3300905132 * get_callback_1() const { return ___callback_1; }
	inline Action_1_t3300905132 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_1_t3300905132 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_U3CresultsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U3CresultsU3E__0_2)); }
	inline Stack_1_t3971826992 * get_U3CresultsU3E__0_2() const { return ___U3CresultsU3E__0_2; }
	inline Stack_1_t3971826992 ** get_address_of_U3CresultsU3E__0_2() { return &___U3CresultsU3E__0_2; }
	inline void set_U3CresultsU3E__0_2(Stack_1_t3971826992 * value)
	{
		___U3CresultsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U24locvar0_3)); }
	inline RuntimeObject* get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline RuntimeObject** get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(RuntimeObject* value)
	{
		___U24locvar0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_3), value);
	}

	inline static int32_t get_offset_of_U3CmethodU3E__1_4() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U3CmethodU3E__1_4)); }
	inline int32_t get_U3CmethodU3E__1_4() const { return ___U3CmethodU3E__1_4; }
	inline int32_t* get_address_of_U3CmethodU3E__1_4() { return &___U3CmethodU3E__1_4; }
	inline void set_U3CmethodU3E__1_4(int32_t value)
	{
		___U3CmethodU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CresultU3E__0_5() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U3CresultU3E__0_5)); }
	inline int32_t get_U3CresultU3E__0_5() const { return ___U3CresultU3E__0_5; }
	inline int32_t* get_address_of_U3CresultU3E__0_5() { return &___U3CresultU3E__0_5; }
	inline void set_U3CresultU3E__0_5(int32_t value)
	{
		___U3CresultU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U24this_6)); }
	inline BaseNativeEEARegionValidator_t3155147623 * get_U24this_6() const { return ___U24this_6; }
	inline BaseNativeEEARegionValidator_t3155147623 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(BaseNativeEEARegionValidator_t3155147623 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATEEEAREGIONSTATUSCOROUTINEU3EC__ITERATOR0_T1025791164_H
#ifndef U3CVALIDATEVIAGOOGLESERVICECOROUTINEU3EC__ITERATOR1_T3967541878_H
#define U3CVALIDATEVIAGOOGLESERVICECOROUTINEU3EC__ITERATOR1_T3967541878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1
struct  U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_0;
	// EasyMobile.EEARegionStatus EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::<status>__0
	int32_t ___U3CstatusU3E__0_1;
	// System.Boolean EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::<errorFlag>__0
	bool ___U3CerrorFlagU3E__0_2;
	// System.Collections.Generic.Stack`1<EasyMobile.EEARegionStatus> EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::resultsStack
	Stack_1_t3971826992 * ___resultsStack_3;
	// EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::$this
	BaseNativeEEARegionValidator_t3155147623 * ___U24this_4;
	// System.Object EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 EasyMobile.Internal.Privacy.BaseNativeEEARegionValidator/<ValidateViaGoogleServiceCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U3CwwwU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3E__0_1() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U3CstatusU3E__0_1)); }
	inline int32_t get_U3CstatusU3E__0_1() const { return ___U3CstatusU3E__0_1; }
	inline int32_t* get_address_of_U3CstatusU3E__0_1() { return &___U3CstatusU3E__0_1; }
	inline void set_U3CstatusU3E__0_1(int32_t value)
	{
		___U3CstatusU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CerrorFlagU3E__0_2() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U3CerrorFlagU3E__0_2)); }
	inline bool get_U3CerrorFlagU3E__0_2() const { return ___U3CerrorFlagU3E__0_2; }
	inline bool* get_address_of_U3CerrorFlagU3E__0_2() { return &___U3CerrorFlagU3E__0_2; }
	inline void set_U3CerrorFlagU3E__0_2(bool value)
	{
		___U3CerrorFlagU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_resultsStack_3() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___resultsStack_3)); }
	inline Stack_1_t3971826992 * get_resultsStack_3() const { return ___resultsStack_3; }
	inline Stack_1_t3971826992 ** get_address_of_resultsStack_3() { return &___resultsStack_3; }
	inline void set_resultsStack_3(Stack_1_t3971826992 * value)
	{
		___resultsStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___resultsStack_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U24this_4)); }
	inline BaseNativeEEARegionValidator_t3155147623 * get_U24this_4() const { return ___U24this_4; }
	inline BaseNativeEEARegionValidator_t3155147623 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(BaseNativeEEARegionValidator_t3155147623 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATEVIAGOOGLESERVICECOROUTINEU3EC__ITERATOR1_T3967541878_H
#ifndef INTEROPOBJECT_T35158505_H
#define INTEROPOBJECT_T35158505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.InteropObject
struct  InteropObject_t35158505  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef EasyMobile.InteropObject::mSelfPointer
	HandleRef_t3745784362  ___mSelfPointer_0;

public:
	inline static int32_t get_offset_of_mSelfPointer_0() { return static_cast<int32_t>(offsetof(InteropObject_t35158505, ___mSelfPointer_0)); }
	inline HandleRef_t3745784362  get_mSelfPointer_0() const { return ___mSelfPointer_0; }
	inline HandleRef_t3745784362 * get_address_of_mSelfPointer_0() { return &___mSelfPointer_0; }
	inline void set_mSelfPointer_0(HandleRef_t3745784362  value)
	{
		___mSelfPointer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEROPOBJECT_T35158505_H
#ifndef NOTIFICATIONCATEGORY_T2144930983_H
#define NOTIFICATIONCATEGORY_T2144930983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationCategory
struct  NotificationCategory_t2144930983  : public RuntimeObject
{
public:
	// System.String EasyMobile.NotificationCategory::id
	String_t* ___id_0;
	// System.String EasyMobile.NotificationCategory::groupId
	String_t* ___groupId_1;
	// System.String EasyMobile.NotificationCategory::name
	String_t* ___name_2;
	// System.String EasyMobile.NotificationCategory::description
	String_t* ___description_3;
	// EasyMobile.NotificationCategory/Importance EasyMobile.NotificationCategory::importance
	int32_t ___importance_4;
	// System.Boolean EasyMobile.NotificationCategory::enableBadge
	bool ___enableBadge_5;
	// EasyMobile.NotificationCategory/LightOptions EasyMobile.NotificationCategory::lights
	int32_t ___lights_6;
	// UnityEngine.Color EasyMobile.NotificationCategory::lightColor
	Color_t2555686324  ___lightColor_7;
	// EasyMobile.NotificationCategory/VibrationOptions EasyMobile.NotificationCategory::vibration
	int32_t ___vibration_8;
	// System.Int32[] EasyMobile.NotificationCategory::vibrationPattern
	Int32U5BU5D_t385246372* ___vibrationPattern_9;
	// EasyMobile.NotificationCategory/LockScreenVisibilityOptions EasyMobile.NotificationCategory::lockScreenVisibility
	int32_t ___lockScreenVisibility_10;
	// EasyMobile.NotificationCategory/SoundOptions EasyMobile.NotificationCategory::sound
	int32_t ___sound_11;
	// System.String EasyMobile.NotificationCategory::soundName
	String_t* ___soundName_12;
	// EasyMobile.NotificationCategory/ActionButton[] EasyMobile.NotificationCategory::actionButtons
	ActionButtonU5BU5D_t1842622499* ___actionButtons_13;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_groupId_1() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___groupId_1)); }
	inline String_t* get_groupId_1() const { return ___groupId_1; }
	inline String_t** get_address_of_groupId_1() { return &___groupId_1; }
	inline void set_groupId_1(String_t* value)
	{
		___groupId_1 = value;
		Il2CppCodeGenWriteBarrier((&___groupId_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_description_3() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___description_3)); }
	inline String_t* get_description_3() const { return ___description_3; }
	inline String_t** get_address_of_description_3() { return &___description_3; }
	inline void set_description_3(String_t* value)
	{
		___description_3 = value;
		Il2CppCodeGenWriteBarrier((&___description_3), value);
	}

	inline static int32_t get_offset_of_importance_4() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___importance_4)); }
	inline int32_t get_importance_4() const { return ___importance_4; }
	inline int32_t* get_address_of_importance_4() { return &___importance_4; }
	inline void set_importance_4(int32_t value)
	{
		___importance_4 = value;
	}

	inline static int32_t get_offset_of_enableBadge_5() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___enableBadge_5)); }
	inline bool get_enableBadge_5() const { return ___enableBadge_5; }
	inline bool* get_address_of_enableBadge_5() { return &___enableBadge_5; }
	inline void set_enableBadge_5(bool value)
	{
		___enableBadge_5 = value;
	}

	inline static int32_t get_offset_of_lights_6() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___lights_6)); }
	inline int32_t get_lights_6() const { return ___lights_6; }
	inline int32_t* get_address_of_lights_6() { return &___lights_6; }
	inline void set_lights_6(int32_t value)
	{
		___lights_6 = value;
	}

	inline static int32_t get_offset_of_lightColor_7() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___lightColor_7)); }
	inline Color_t2555686324  get_lightColor_7() const { return ___lightColor_7; }
	inline Color_t2555686324 * get_address_of_lightColor_7() { return &___lightColor_7; }
	inline void set_lightColor_7(Color_t2555686324  value)
	{
		___lightColor_7 = value;
	}

	inline static int32_t get_offset_of_vibration_8() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___vibration_8)); }
	inline int32_t get_vibration_8() const { return ___vibration_8; }
	inline int32_t* get_address_of_vibration_8() { return &___vibration_8; }
	inline void set_vibration_8(int32_t value)
	{
		___vibration_8 = value;
	}

	inline static int32_t get_offset_of_vibrationPattern_9() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___vibrationPattern_9)); }
	inline Int32U5BU5D_t385246372* get_vibrationPattern_9() const { return ___vibrationPattern_9; }
	inline Int32U5BU5D_t385246372** get_address_of_vibrationPattern_9() { return &___vibrationPattern_9; }
	inline void set_vibrationPattern_9(Int32U5BU5D_t385246372* value)
	{
		___vibrationPattern_9 = value;
		Il2CppCodeGenWriteBarrier((&___vibrationPattern_9), value);
	}

	inline static int32_t get_offset_of_lockScreenVisibility_10() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___lockScreenVisibility_10)); }
	inline int32_t get_lockScreenVisibility_10() const { return ___lockScreenVisibility_10; }
	inline int32_t* get_address_of_lockScreenVisibility_10() { return &___lockScreenVisibility_10; }
	inline void set_lockScreenVisibility_10(int32_t value)
	{
		___lockScreenVisibility_10 = value;
	}

	inline static int32_t get_offset_of_sound_11() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___sound_11)); }
	inline int32_t get_sound_11() const { return ___sound_11; }
	inline int32_t* get_address_of_sound_11() { return &___sound_11; }
	inline void set_sound_11(int32_t value)
	{
		___sound_11 = value;
	}

	inline static int32_t get_offset_of_soundName_12() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___soundName_12)); }
	inline String_t* get_soundName_12() const { return ___soundName_12; }
	inline String_t** get_address_of_soundName_12() { return &___soundName_12; }
	inline void set_soundName_12(String_t* value)
	{
		___soundName_12 = value;
		Il2CppCodeGenWriteBarrier((&___soundName_12), value);
	}

	inline static int32_t get_offset_of_actionButtons_13() { return static_cast<int32_t>(offsetof(NotificationCategory_t2144930983, ___actionButtons_13)); }
	inline ActionButtonU5BU5D_t1842622499* get_actionButtons_13() const { return ___actionButtons_13; }
	inline ActionButtonU5BU5D_t1842622499** get_address_of_actionButtons_13() { return &___actionButtons_13; }
	inline void set_actionButtons_13(ActionButtonU5BU5D_t1842622499* value)
	{
		___actionButtons_13 = value;
		Il2CppCodeGenWriteBarrier((&___actionButtons_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONCATEGORY_T2144930983_H
#ifndef NOTIFICATIONREQUEST_T4022128807_H
#define NOTIFICATIONREQUEST_T4022128807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationRequest
struct  NotificationRequest_t4022128807  : public RuntimeObject
{
public:
	// System.String EasyMobile.NotificationRequest::id
	String_t* ___id_0;
	// EasyMobile.NotificationContent EasyMobile.NotificationRequest::content
	NotificationContent_t758602733 * ___content_1;
	// System.DateTime EasyMobile.NotificationRequest::nextTriggerDate
	DateTime_t3738529785  ___nextTriggerDate_2;
	// EasyMobile.NotificationRepeat EasyMobile.NotificationRequest::repeat
	int32_t ___repeat_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(NotificationRequest_t4022128807, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(NotificationRequest_t4022128807, ___content_1)); }
	inline NotificationContent_t758602733 * get_content_1() const { return ___content_1; }
	inline NotificationContent_t758602733 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(NotificationContent_t758602733 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_nextTriggerDate_2() { return static_cast<int32_t>(offsetof(NotificationRequest_t4022128807, ___nextTriggerDate_2)); }
	inline DateTime_t3738529785  get_nextTriggerDate_2() const { return ___nextTriggerDate_2; }
	inline DateTime_t3738529785 * get_address_of_nextTriggerDate_2() { return &___nextTriggerDate_2; }
	inline void set_nextTriggerDate_2(DateTime_t3738529785  value)
	{
		___nextTriggerDate_2 = value;
	}

	inline static int32_t get_offset_of_repeat_3() { return static_cast<int32_t>(offsetof(NotificationRequest_t4022128807, ___repeat_3)); }
	inline int32_t get_repeat_3() const { return ___repeat_3; }
	inline int32_t* get_address_of_repeat_3() { return &___repeat_3; }
	inline void set_repeat_3(int32_t value)
	{
		___repeat_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONREQUEST_T4022128807_H
#ifndef NOTIFICATIONSSETTINGS_T3829333496_H
#define NOTIFICATIONSSETTINGS_T3829333496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.NotificationsSettings
struct  NotificationsSettings_t3829333496  : public RuntimeObject
{
public:
	// System.Boolean EasyMobile.NotificationsSettings::mAutoInit
	bool ___mAutoInit_2;
	// System.Single EasyMobile.NotificationsSettings::mAutoInitDelay
	float ___mAutoInitDelay_3;
	// EasyMobile.NotificationAuthOptions EasyMobile.NotificationsSettings::mIosAuthOptions
	int32_t ___mIosAuthOptions_4;
	// EasyMobile.PushNotificationProvider EasyMobile.NotificationsSettings::mPushNotificationService
	int32_t ___mPushNotificationService_5;
	// System.String EasyMobile.NotificationsSettings::mOneSignalAppId
	String_t* ___mOneSignalAppId_6;
	// System.String[] EasyMobile.NotificationsSettings::mFirebaseTopics
	StringU5BU5D_t1281789340* ___mFirebaseTopics_7;
	// EasyMobile.NotificationCategoryGroup[] EasyMobile.NotificationsSettings::mCategoryGroups
	NotificationCategoryGroupU5BU5D_t1432226907* ___mCategoryGroups_8;
	// EasyMobile.NotificationCategory EasyMobile.NotificationsSettings::mDefaultCategory
	NotificationCategory_t2144930983 * ___mDefaultCategory_9;
	// EasyMobile.NotificationCategory[] EasyMobile.NotificationsSettings::mUserCategories
	NotificationCategoryU5BU5D_t1472561374* ___mUserCategories_10;

public:
	inline static int32_t get_offset_of_mAutoInit_2() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mAutoInit_2)); }
	inline bool get_mAutoInit_2() const { return ___mAutoInit_2; }
	inline bool* get_address_of_mAutoInit_2() { return &___mAutoInit_2; }
	inline void set_mAutoInit_2(bool value)
	{
		___mAutoInit_2 = value;
	}

	inline static int32_t get_offset_of_mAutoInitDelay_3() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mAutoInitDelay_3)); }
	inline float get_mAutoInitDelay_3() const { return ___mAutoInitDelay_3; }
	inline float* get_address_of_mAutoInitDelay_3() { return &___mAutoInitDelay_3; }
	inline void set_mAutoInitDelay_3(float value)
	{
		___mAutoInitDelay_3 = value;
	}

	inline static int32_t get_offset_of_mIosAuthOptions_4() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mIosAuthOptions_4)); }
	inline int32_t get_mIosAuthOptions_4() const { return ___mIosAuthOptions_4; }
	inline int32_t* get_address_of_mIosAuthOptions_4() { return &___mIosAuthOptions_4; }
	inline void set_mIosAuthOptions_4(int32_t value)
	{
		___mIosAuthOptions_4 = value;
	}

	inline static int32_t get_offset_of_mPushNotificationService_5() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mPushNotificationService_5)); }
	inline int32_t get_mPushNotificationService_5() const { return ___mPushNotificationService_5; }
	inline int32_t* get_address_of_mPushNotificationService_5() { return &___mPushNotificationService_5; }
	inline void set_mPushNotificationService_5(int32_t value)
	{
		___mPushNotificationService_5 = value;
	}

	inline static int32_t get_offset_of_mOneSignalAppId_6() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mOneSignalAppId_6)); }
	inline String_t* get_mOneSignalAppId_6() const { return ___mOneSignalAppId_6; }
	inline String_t** get_address_of_mOneSignalAppId_6() { return &___mOneSignalAppId_6; }
	inline void set_mOneSignalAppId_6(String_t* value)
	{
		___mOneSignalAppId_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOneSignalAppId_6), value);
	}

	inline static int32_t get_offset_of_mFirebaseTopics_7() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mFirebaseTopics_7)); }
	inline StringU5BU5D_t1281789340* get_mFirebaseTopics_7() const { return ___mFirebaseTopics_7; }
	inline StringU5BU5D_t1281789340** get_address_of_mFirebaseTopics_7() { return &___mFirebaseTopics_7; }
	inline void set_mFirebaseTopics_7(StringU5BU5D_t1281789340* value)
	{
		___mFirebaseTopics_7 = value;
		Il2CppCodeGenWriteBarrier((&___mFirebaseTopics_7), value);
	}

	inline static int32_t get_offset_of_mCategoryGroups_8() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mCategoryGroups_8)); }
	inline NotificationCategoryGroupU5BU5D_t1432226907* get_mCategoryGroups_8() const { return ___mCategoryGroups_8; }
	inline NotificationCategoryGroupU5BU5D_t1432226907** get_address_of_mCategoryGroups_8() { return &___mCategoryGroups_8; }
	inline void set_mCategoryGroups_8(NotificationCategoryGroupU5BU5D_t1432226907* value)
	{
		___mCategoryGroups_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCategoryGroups_8), value);
	}

	inline static int32_t get_offset_of_mDefaultCategory_9() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mDefaultCategory_9)); }
	inline NotificationCategory_t2144930983 * get_mDefaultCategory_9() const { return ___mDefaultCategory_9; }
	inline NotificationCategory_t2144930983 ** get_address_of_mDefaultCategory_9() { return &___mDefaultCategory_9; }
	inline void set_mDefaultCategory_9(NotificationCategory_t2144930983 * value)
	{
		___mDefaultCategory_9 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultCategory_9), value);
	}

	inline static int32_t get_offset_of_mUserCategories_10() { return static_cast<int32_t>(offsetof(NotificationsSettings_t3829333496, ___mUserCategories_10)); }
	inline NotificationCategoryU5BU5D_t1472561374* get_mUserCategories_10() const { return ___mUserCategories_10; }
	inline NotificationCategoryU5BU5D_t1472561374** get_address_of_mUserCategories_10() { return &___mUserCategories_10; }
	inline void set_mUserCategories_10(NotificationCategoryU5BU5D_t1472561374* value)
	{
		___mUserCategories_10 = value;
		Il2CppCodeGenWriteBarrier((&___mUserCategories_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSSETTINGS_T3829333496_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef COMPLETEDHANDLER_T441206176_H
#define COMPLETEDHANDLER_T441206176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/CompletedHandler
struct  CompletedHandler_t441206176  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETEDHANDLER_T441206176_H
#ifndef TOGGLESTATEUPDATEDHANDLER_T2741348581_H
#define TOGGLESTATEUPDATEDHANDLER_T2741348581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.ConsentDialog/ToggleStateUpdatedHandler
struct  ToggleStateUpdatedHandler_t2741348581  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLESTATEUPDATEDHANDLER_T2741348581_H
#ifndef GETNOTIFICATIONRESPONSECALLBACK_T3346532913_H
#define GETNOTIFICATIONRESPONSECALLBACK_T3346532913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetNotificationResponseCallback
struct  GetNotificationResponseCallback_t3346532913  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNOTIFICATIONRESPONSECALLBACK_T3346532913_H
#ifndef GETPENDINGNOTIFICATIONREQUESTSCALLBACK_T3407937767_H
#define GETPENDINGNOTIFICATIONREQUESTSCALLBACK_T3407937767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetPendingNotificationRequestsCallback
struct  GetPendingNotificationRequestsCallback_t3407937767  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPENDINGNOTIFICATIONREQUESTSCALLBACK_T3407937767_H
#ifndef IOSINTEROPOBJECT_T3130024260_H
#define IOSINTEROPOBJECT_T3130024260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.iOSInteropObject
struct  iOSInteropObject_t3130024260  : public InteropObject_t35158505
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSINTEROPOBJECT_T3130024260_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef NSERROR_T3543635482_H
#define NSERROR_T3543635482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.Foundation.NSError
struct  NSError_t3543635482  : public iOSInteropObject_t3130024260
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSERROR_T3543635482_H
#ifndef UIIMAGE_T4033140513_H
#define UIIMAGE_T4033140513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.iOS.UIKit.UIImage
struct  UIImage_t4033140513  : public iOSInteropObject_t3130024260
{
public:
	// System.Nullable`1<System.Single> EasyMobile.iOS.UIKit.UIImage::mScale
	Nullable_1_t3119828856  ___mScale_1;
	// System.Nullable`1<System.Single> EasyMobile.iOS.UIKit.UIImage::mWidth
	Nullable_1_t3119828856  ___mWidth_2;
	// System.Nullable`1<System.Single> EasyMobile.iOS.UIKit.UIImage::mHeight
	Nullable_1_t3119828856  ___mHeight_3;

public:
	inline static int32_t get_offset_of_mScale_1() { return static_cast<int32_t>(offsetof(UIImage_t4033140513, ___mScale_1)); }
	inline Nullable_1_t3119828856  get_mScale_1() const { return ___mScale_1; }
	inline Nullable_1_t3119828856 * get_address_of_mScale_1() { return &___mScale_1; }
	inline void set_mScale_1(Nullable_1_t3119828856  value)
	{
		___mScale_1 = value;
	}

	inline static int32_t get_offset_of_mWidth_2() { return static_cast<int32_t>(offsetof(UIImage_t4033140513, ___mWidth_2)); }
	inline Nullable_1_t3119828856  get_mWidth_2() const { return ___mWidth_2; }
	inline Nullable_1_t3119828856 * get_address_of_mWidth_2() { return &___mWidth_2; }
	inline void set_mWidth_2(Nullable_1_t3119828856  value)
	{
		___mWidth_2 = value;
	}

	inline static int32_t get_offset_of_mHeight_3() { return static_cast<int32_t>(offsetof(UIImage_t4033140513, ___mHeight_3)); }
	inline Nullable_1_t3119828856  get_mHeight_3() const { return ___mHeight_3; }
	inline Nullable_1_t3119828856 * get_address_of_mHeight_3() { return &___mHeight_3; }
	inline void set_mHeight_3(Nullable_1_t3119828856  value)
	{
		___mHeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGE_T4033140513_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEMOASTEROIDSCRIPT_T1162572965_H
#define DEMOASTEROIDSCRIPT_T1162572965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoAsteroidScript
struct  DemoAsteroidScript_t1162572965  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOASTEROIDSCRIPT_T1162572965_H
#ifndef DEMOSCRIPT_T2141982657_H
#define DEMOSCRIPT_T2141982657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScript
struct  DemoScript_t2141982657  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::Earth
	GameObject_t1113636619 * ___Earth_4;
	// UnityEngine.UI.Text DigitalRubyShared.DemoScript::dpiLabel
	Text_t1901882714 * ___dpiLabel_5;
	// UnityEngine.UI.Text DigitalRubyShared.DemoScript::bottomLabel
	Text_t1901882714 * ___bottomLabel_6;
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::AsteroidPrefab
	GameObject_t1113636619 * ___AsteroidPrefab_7;
	// UnityEngine.Material DigitalRubyShared.DemoScript::LineMaterial
	Material_t340375123 * ___LineMaterial_8;
	// UnityEngine.Sprite[] DigitalRubyShared.DemoScript::asteroids
	SpriteU5BU5D_t2581906349* ___asteroids_9;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScript::tapGesture
	TapGestureRecognizer_t3178883670 * ___tapGesture_10;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScript::doubleTapGesture
	TapGestureRecognizer_t3178883670 * ___doubleTapGesture_11;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScript::tripleTapGesture
	TapGestureRecognizer_t3178883670 * ___tripleTapGesture_12;
	// DigitalRubyShared.SwipeGestureRecognizer DigitalRubyShared.DemoScript::swipeGesture
	SwipeGestureRecognizer_t2328511861 * ___swipeGesture_13;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.DemoScript::panGesture
	PanGestureRecognizer_t195762396 * ___panGesture_14;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.DemoScript::scaleGesture
	ScaleGestureRecognizer_t1137887245 * ___scaleGesture_15;
	// DigitalRubyShared.RotateGestureRecognizer DigitalRubyShared.DemoScript::rotateGesture
	RotateGestureRecognizer_t4100246528 * ___rotateGesture_16;
	// DigitalRubyShared.LongPressGestureRecognizer DigitalRubyShared.DemoScript::longPressGesture
	LongPressGestureRecognizer_t3980777482 * ___longPressGesture_17;
	// System.Single DigitalRubyShared.DemoScript::nextAsteroid
	float ___nextAsteroid_18;
	// UnityEngine.GameObject DigitalRubyShared.DemoScript::draggingAsteroid
	GameObject_t1113636619 * ___draggingAsteroid_19;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DigitalRubyShared.DemoScript::swipeLines
	List_1_t899420910 * ___swipeLines_20;

public:
	inline static int32_t get_offset_of_Earth_4() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___Earth_4)); }
	inline GameObject_t1113636619 * get_Earth_4() const { return ___Earth_4; }
	inline GameObject_t1113636619 ** get_address_of_Earth_4() { return &___Earth_4; }
	inline void set_Earth_4(GameObject_t1113636619 * value)
	{
		___Earth_4 = value;
		Il2CppCodeGenWriteBarrier((&___Earth_4), value);
	}

	inline static int32_t get_offset_of_dpiLabel_5() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___dpiLabel_5)); }
	inline Text_t1901882714 * get_dpiLabel_5() const { return ___dpiLabel_5; }
	inline Text_t1901882714 ** get_address_of_dpiLabel_5() { return &___dpiLabel_5; }
	inline void set_dpiLabel_5(Text_t1901882714 * value)
	{
		___dpiLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___dpiLabel_5), value);
	}

	inline static int32_t get_offset_of_bottomLabel_6() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___bottomLabel_6)); }
	inline Text_t1901882714 * get_bottomLabel_6() const { return ___bottomLabel_6; }
	inline Text_t1901882714 ** get_address_of_bottomLabel_6() { return &___bottomLabel_6; }
	inline void set_bottomLabel_6(Text_t1901882714 * value)
	{
		___bottomLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___bottomLabel_6), value);
	}

	inline static int32_t get_offset_of_AsteroidPrefab_7() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___AsteroidPrefab_7)); }
	inline GameObject_t1113636619 * get_AsteroidPrefab_7() const { return ___AsteroidPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_AsteroidPrefab_7() { return &___AsteroidPrefab_7; }
	inline void set_AsteroidPrefab_7(GameObject_t1113636619 * value)
	{
		___AsteroidPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___AsteroidPrefab_7), value);
	}

	inline static int32_t get_offset_of_LineMaterial_8() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___LineMaterial_8)); }
	inline Material_t340375123 * get_LineMaterial_8() const { return ___LineMaterial_8; }
	inline Material_t340375123 ** get_address_of_LineMaterial_8() { return &___LineMaterial_8; }
	inline void set_LineMaterial_8(Material_t340375123 * value)
	{
		___LineMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_8), value);
	}

	inline static int32_t get_offset_of_asteroids_9() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___asteroids_9)); }
	inline SpriteU5BU5D_t2581906349* get_asteroids_9() const { return ___asteroids_9; }
	inline SpriteU5BU5D_t2581906349** get_address_of_asteroids_9() { return &___asteroids_9; }
	inline void set_asteroids_9(SpriteU5BU5D_t2581906349* value)
	{
		___asteroids_9 = value;
		Il2CppCodeGenWriteBarrier((&___asteroids_9), value);
	}

	inline static int32_t get_offset_of_tapGesture_10() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___tapGesture_10)); }
	inline TapGestureRecognizer_t3178883670 * get_tapGesture_10() const { return ___tapGesture_10; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tapGesture_10() { return &___tapGesture_10; }
	inline void set_tapGesture_10(TapGestureRecognizer_t3178883670 * value)
	{
		___tapGesture_10 = value;
		Il2CppCodeGenWriteBarrier((&___tapGesture_10), value);
	}

	inline static int32_t get_offset_of_doubleTapGesture_11() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___doubleTapGesture_11)); }
	inline TapGestureRecognizer_t3178883670 * get_doubleTapGesture_11() const { return ___doubleTapGesture_11; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_doubleTapGesture_11() { return &___doubleTapGesture_11; }
	inline void set_doubleTapGesture_11(TapGestureRecognizer_t3178883670 * value)
	{
		___doubleTapGesture_11 = value;
		Il2CppCodeGenWriteBarrier((&___doubleTapGesture_11), value);
	}

	inline static int32_t get_offset_of_tripleTapGesture_12() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___tripleTapGesture_12)); }
	inline TapGestureRecognizer_t3178883670 * get_tripleTapGesture_12() const { return ___tripleTapGesture_12; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tripleTapGesture_12() { return &___tripleTapGesture_12; }
	inline void set_tripleTapGesture_12(TapGestureRecognizer_t3178883670 * value)
	{
		___tripleTapGesture_12 = value;
		Il2CppCodeGenWriteBarrier((&___tripleTapGesture_12), value);
	}

	inline static int32_t get_offset_of_swipeGesture_13() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___swipeGesture_13)); }
	inline SwipeGestureRecognizer_t2328511861 * get_swipeGesture_13() const { return ___swipeGesture_13; }
	inline SwipeGestureRecognizer_t2328511861 ** get_address_of_swipeGesture_13() { return &___swipeGesture_13; }
	inline void set_swipeGesture_13(SwipeGestureRecognizer_t2328511861 * value)
	{
		___swipeGesture_13 = value;
		Il2CppCodeGenWriteBarrier((&___swipeGesture_13), value);
	}

	inline static int32_t get_offset_of_panGesture_14() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___panGesture_14)); }
	inline PanGestureRecognizer_t195762396 * get_panGesture_14() const { return ___panGesture_14; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_panGesture_14() { return &___panGesture_14; }
	inline void set_panGesture_14(PanGestureRecognizer_t195762396 * value)
	{
		___panGesture_14 = value;
		Il2CppCodeGenWriteBarrier((&___panGesture_14), value);
	}

	inline static int32_t get_offset_of_scaleGesture_15() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___scaleGesture_15)); }
	inline ScaleGestureRecognizer_t1137887245 * get_scaleGesture_15() const { return ___scaleGesture_15; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_scaleGesture_15() { return &___scaleGesture_15; }
	inline void set_scaleGesture_15(ScaleGestureRecognizer_t1137887245 * value)
	{
		___scaleGesture_15 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_15), value);
	}

	inline static int32_t get_offset_of_rotateGesture_16() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___rotateGesture_16)); }
	inline RotateGestureRecognizer_t4100246528 * get_rotateGesture_16() const { return ___rotateGesture_16; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_rotateGesture_16() { return &___rotateGesture_16; }
	inline void set_rotateGesture_16(RotateGestureRecognizer_t4100246528 * value)
	{
		___rotateGesture_16 = value;
		Il2CppCodeGenWriteBarrier((&___rotateGesture_16), value);
	}

	inline static int32_t get_offset_of_longPressGesture_17() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___longPressGesture_17)); }
	inline LongPressGestureRecognizer_t3980777482 * get_longPressGesture_17() const { return ___longPressGesture_17; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_longPressGesture_17() { return &___longPressGesture_17; }
	inline void set_longPressGesture_17(LongPressGestureRecognizer_t3980777482 * value)
	{
		___longPressGesture_17 = value;
		Il2CppCodeGenWriteBarrier((&___longPressGesture_17), value);
	}

	inline static int32_t get_offset_of_nextAsteroid_18() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___nextAsteroid_18)); }
	inline float get_nextAsteroid_18() const { return ___nextAsteroid_18; }
	inline float* get_address_of_nextAsteroid_18() { return &___nextAsteroid_18; }
	inline void set_nextAsteroid_18(float value)
	{
		___nextAsteroid_18 = value;
	}

	inline static int32_t get_offset_of_draggingAsteroid_19() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___draggingAsteroid_19)); }
	inline GameObject_t1113636619 * get_draggingAsteroid_19() const { return ___draggingAsteroid_19; }
	inline GameObject_t1113636619 ** get_address_of_draggingAsteroid_19() { return &___draggingAsteroid_19; }
	inline void set_draggingAsteroid_19(GameObject_t1113636619 * value)
	{
		___draggingAsteroid_19 = value;
		Il2CppCodeGenWriteBarrier((&___draggingAsteroid_19), value);
	}

	inline static int32_t get_offset_of_swipeLines_20() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657, ___swipeLines_20)); }
	inline List_1_t899420910 * get_swipeLines_20() const { return ___swipeLines_20; }
	inline List_1_t899420910 ** get_address_of_swipeLines_20() { return &___swipeLines_20; }
	inline void set_swipeLines_20(List_1_t899420910 * value)
	{
		___swipeLines_20 = value;
		Il2CppCodeGenWriteBarrier((&___swipeLines_20), value);
	}
};

struct DemoScript_t2141982657_StaticFields
{
public:
	// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>> DigitalRubyShared.DemoScript::<>f__mg$cache0
	Func_2_t1671534078 * ___U3CU3Ef__mgU24cache0_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_21() { return static_cast<int32_t>(offsetof(DemoScript_t2141982657_StaticFields, ___U3CU3Ef__mgU24cache0_21)); }
	inline Func_2_t1671534078 * get_U3CU3Ef__mgU24cache0_21() const { return ___U3CU3Ef__mgU24cache0_21; }
	inline Func_2_t1671534078 ** get_address_of_U3CU3Ef__mgU24cache0_21() { return &___U3CU3Ef__mgU24cache0_21; }
	inline void set_U3CU3Ef__mgU24cache0_21(Func_2_t1671534078 * value)
	{
		___U3CU3Ef__mgU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT_T2141982657_H
#ifndef DEMOSCRIPT3DORBIT_T3440477113_H
#define DEMOSCRIPT3DORBIT_T3440477113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScript3DOrbit
struct  DemoScript3DOrbit_t3440477113  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT3DORBIT_T3440477113_H
#ifndef DEMOSCRIPTCOMPONENT_T1393959397_H
#define DEMOSCRIPTCOMPONENT_T1393959397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptComponent
struct  DemoScriptComponent_t1393959397  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DigitalRubyShared.DemoScriptComponent::scale
	float ___scale_4;
	// System.Single DigitalRubyShared.DemoScriptComponent::oneTouchScale
	float ___oneTouchScale_5;

public:
	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(DemoScriptComponent_t1393959397, ___scale_4)); }
	inline float get_scale_4() const { return ___scale_4; }
	inline float* get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(float value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_oneTouchScale_5() { return static_cast<int32_t>(offsetof(DemoScriptComponent_t1393959397, ___oneTouchScale_5)); }
	inline float get_oneTouchScale_5() const { return ___oneTouchScale_5; }
	inline float* get_address_of_oneTouchScale_5() { return &___oneTouchScale_5; }
	inline void set_oneTouchScale_5(float value)
	{
		___oneTouchScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTCOMPONENT_T1393959397_H
#ifndef DEMOSCRIPTDPAD_T2734679325_H
#define DEMOSCRIPTDPAD_T2734679325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptDPad
struct  DemoScriptDPad_t2734679325  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersDPadScript DigitalRubyShared.DemoScriptDPad::DPadScript
	FingersDPadScript_t3801874975 * ___DPadScript_4;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptDPad::Mover
	GameObject_t1113636619 * ___Mover_5;
	// System.Single DigitalRubyShared.DemoScriptDPad::Speed
	float ___Speed_6;
	// System.Boolean DigitalRubyShared.DemoScriptDPad::MoveDPadToGestureStartLocation
	bool ___MoveDPadToGestureStartLocation_7;
	// UnityEngine.Vector3 DigitalRubyShared.DemoScriptDPad::startPos
	Vector3_t3722313464  ___startPos_8;

public:
	inline static int32_t get_offset_of_DPadScript_4() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___DPadScript_4)); }
	inline FingersDPadScript_t3801874975 * get_DPadScript_4() const { return ___DPadScript_4; }
	inline FingersDPadScript_t3801874975 ** get_address_of_DPadScript_4() { return &___DPadScript_4; }
	inline void set_DPadScript_4(FingersDPadScript_t3801874975 * value)
	{
		___DPadScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___DPadScript_4), value);
	}

	inline static int32_t get_offset_of_Mover_5() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___Mover_5)); }
	inline GameObject_t1113636619 * get_Mover_5() const { return ___Mover_5; }
	inline GameObject_t1113636619 ** get_address_of_Mover_5() { return &___Mover_5; }
	inline void set_Mover_5(GameObject_t1113636619 * value)
	{
		___Mover_5 = value;
		Il2CppCodeGenWriteBarrier((&___Mover_5), value);
	}

	inline static int32_t get_offset_of_Speed_6() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___Speed_6)); }
	inline float get_Speed_6() const { return ___Speed_6; }
	inline float* get_address_of_Speed_6() { return &___Speed_6; }
	inline void set_Speed_6(float value)
	{
		___Speed_6 = value;
	}

	inline static int32_t get_offset_of_MoveDPadToGestureStartLocation_7() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___MoveDPadToGestureStartLocation_7)); }
	inline bool get_MoveDPadToGestureStartLocation_7() const { return ___MoveDPadToGestureStartLocation_7; }
	inline bool* get_address_of_MoveDPadToGestureStartLocation_7() { return &___MoveDPadToGestureStartLocation_7; }
	inline void set_MoveDPadToGestureStartLocation_7(bool value)
	{
		___MoveDPadToGestureStartLocation_7 = value;
	}

	inline static int32_t get_offset_of_startPos_8() { return static_cast<int32_t>(offsetof(DemoScriptDPad_t2734679325, ___startPos_8)); }
	inline Vector3_t3722313464  get_startPos_8() const { return ___startPos_8; }
	inline Vector3_t3722313464 * get_address_of_startPos_8() { return &___startPos_8; }
	inline void set_startPos_8(Vector3_t3722313464  value)
	{
		___startPos_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTDPAD_T2734679325_H
#ifndef DEMOSCRIPTDRAGSWIPE_T2328868075_H
#define DEMOSCRIPTDRAGSWIPE_T2328868075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptDragSwipe
struct  DemoScriptDragSwipe_t2328868075  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DigitalRubyShared.DemoScriptDragSwipe::SwipeAwaySpeed
	float ___SwipeAwaySpeed_4;
	// System.Single DigitalRubyShared.DemoScriptDragSwipe::SwipeVelocityDampening
	float ___SwipeVelocityDampening_5;
	// DigitalRubyShared.LongPressGestureRecognizer DigitalRubyShared.DemoScriptDragSwipe::longPress
	LongPressGestureRecognizer_t3980777482 * ___longPress_6;
	// UnityEngine.Transform DigitalRubyShared.DemoScriptDragSwipe::draggingCard
	Transform_t3600365921 * ___draggingCard_7;
	// UnityEngine.Vector3 DigitalRubyShared.DemoScriptDragSwipe::dragOffset
	Vector3_t3722313464  ___dragOffset_8;
	// System.Collections.Generic.List`1<UnityEngine.Transform> DigitalRubyShared.DemoScriptDragSwipe::swipedCards
	List_1_t777473367 * ___swipedCards_9;

public:
	inline static int32_t get_offset_of_SwipeAwaySpeed_4() { return static_cast<int32_t>(offsetof(DemoScriptDragSwipe_t2328868075, ___SwipeAwaySpeed_4)); }
	inline float get_SwipeAwaySpeed_4() const { return ___SwipeAwaySpeed_4; }
	inline float* get_address_of_SwipeAwaySpeed_4() { return &___SwipeAwaySpeed_4; }
	inline void set_SwipeAwaySpeed_4(float value)
	{
		___SwipeAwaySpeed_4 = value;
	}

	inline static int32_t get_offset_of_SwipeVelocityDampening_5() { return static_cast<int32_t>(offsetof(DemoScriptDragSwipe_t2328868075, ___SwipeVelocityDampening_5)); }
	inline float get_SwipeVelocityDampening_5() const { return ___SwipeVelocityDampening_5; }
	inline float* get_address_of_SwipeVelocityDampening_5() { return &___SwipeVelocityDampening_5; }
	inline void set_SwipeVelocityDampening_5(float value)
	{
		___SwipeVelocityDampening_5 = value;
	}

	inline static int32_t get_offset_of_longPress_6() { return static_cast<int32_t>(offsetof(DemoScriptDragSwipe_t2328868075, ___longPress_6)); }
	inline LongPressGestureRecognizer_t3980777482 * get_longPress_6() const { return ___longPress_6; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_longPress_6() { return &___longPress_6; }
	inline void set_longPress_6(LongPressGestureRecognizer_t3980777482 * value)
	{
		___longPress_6 = value;
		Il2CppCodeGenWriteBarrier((&___longPress_6), value);
	}

	inline static int32_t get_offset_of_draggingCard_7() { return static_cast<int32_t>(offsetof(DemoScriptDragSwipe_t2328868075, ___draggingCard_7)); }
	inline Transform_t3600365921 * get_draggingCard_7() const { return ___draggingCard_7; }
	inline Transform_t3600365921 ** get_address_of_draggingCard_7() { return &___draggingCard_7; }
	inline void set_draggingCard_7(Transform_t3600365921 * value)
	{
		___draggingCard_7 = value;
		Il2CppCodeGenWriteBarrier((&___draggingCard_7), value);
	}

	inline static int32_t get_offset_of_dragOffset_8() { return static_cast<int32_t>(offsetof(DemoScriptDragSwipe_t2328868075, ___dragOffset_8)); }
	inline Vector3_t3722313464  get_dragOffset_8() const { return ___dragOffset_8; }
	inline Vector3_t3722313464 * get_address_of_dragOffset_8() { return &___dragOffset_8; }
	inline void set_dragOffset_8(Vector3_t3722313464  value)
	{
		___dragOffset_8 = value;
	}

	inline static int32_t get_offset_of_swipedCards_9() { return static_cast<int32_t>(offsetof(DemoScriptDragSwipe_t2328868075, ___swipedCards_9)); }
	inline List_1_t777473367 * get_swipedCards_9() const { return ___swipedCards_9; }
	inline List_1_t777473367 ** get_address_of_swipedCards_9() { return &___swipedCards_9; }
	inline void set_swipedCards_9(List_1_t777473367 * value)
	{
		___swipedCards_9 = value;
		Il2CppCodeGenWriteBarrier((&___swipedCards_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTDRAGSWIPE_T2328868075_H
#ifndef DEMOSCRIPTDYNAMICGRID_T430679104_H
#define DEMOSCRIPTDYNAMICGRID_T430679104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptDynamicGrid
struct  DemoScriptDynamicGrid_t430679104  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 DigitalRubyShared.DemoScriptDynamicGrid::Rows
	int32_t ___Rows_4;
	// System.Int32 DigitalRubyShared.DemoScriptDynamicGrid::Columns
	int32_t ___Columns_5;
	// UnityEngine.UI.GridLayoutGroup DigitalRubyShared.DemoScriptDynamicGrid::grid
	GridLayoutGroup_t3046220461 * ___grid_6;
	// UnityEngine.RectTransform DigitalRubyShared.DemoScriptDynamicGrid::rectTransform
	RectTransform_t3704657025 * ___rectTransform_7;

public:
	inline static int32_t get_offset_of_Rows_4() { return static_cast<int32_t>(offsetof(DemoScriptDynamicGrid_t430679104, ___Rows_4)); }
	inline int32_t get_Rows_4() const { return ___Rows_4; }
	inline int32_t* get_address_of_Rows_4() { return &___Rows_4; }
	inline void set_Rows_4(int32_t value)
	{
		___Rows_4 = value;
	}

	inline static int32_t get_offset_of_Columns_5() { return static_cast<int32_t>(offsetof(DemoScriptDynamicGrid_t430679104, ___Columns_5)); }
	inline int32_t get_Columns_5() const { return ___Columns_5; }
	inline int32_t* get_address_of_Columns_5() { return &___Columns_5; }
	inline void set_Columns_5(int32_t value)
	{
		___Columns_5 = value;
	}

	inline static int32_t get_offset_of_grid_6() { return static_cast<int32_t>(offsetof(DemoScriptDynamicGrid_t430679104, ___grid_6)); }
	inline GridLayoutGroup_t3046220461 * get_grid_6() const { return ___grid_6; }
	inline GridLayoutGroup_t3046220461 ** get_address_of_grid_6() { return &___grid_6; }
	inline void set_grid_6(GridLayoutGroup_t3046220461 * value)
	{
		___grid_6 = value;
		Il2CppCodeGenWriteBarrier((&___grid_6), value);
	}

	inline static int32_t get_offset_of_rectTransform_7() { return static_cast<int32_t>(offsetof(DemoScriptDynamicGrid_t430679104, ___rectTransform_7)); }
	inline RectTransform_t3704657025 * get_rectTransform_7() const { return ___rectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_7() { return &___rectTransform_7; }
	inline void set_rectTransform_7(RectTransform_t3704657025 * value)
	{
		___rectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTDYNAMICGRID_T430679104_H
#ifndef EDITORCONSENTDIALOGBUTTONUI_T376789464_H
#define EDITORCONSENTDIALOGBUTTONUI_T376789464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI
struct  EditorConsentDialogButtonUI_t376789464  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI::button
	Button_t4055032469 * ___button_4;
	// UnityEngine.UI.Text EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI::text
	Text_t1901882714 * ___text_5;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(EditorConsentDialogButtonUI_t376789464, ___button_4)); }
	inline Button_t4055032469 * get_button_4() const { return ___button_4; }
	inline Button_t4055032469 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_t4055032469 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((&___button_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(EditorConsentDialogButtonUI_t376789464, ___text_5)); }
	inline Text_t1901882714 * get_text_5() const { return ___text_5; }
	inline Text_t1901882714 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t1901882714 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORCONSENTDIALOGBUTTONUI_T376789464_H
#ifndef EDITORCONSENTDIALOGTOGGLEUI_T3945542931_H
#define EDITORCONSENTDIALOGTOGGLEUI_T3945542931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI
struct  EditorConsentDialogToggleUI_t3945542931  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::expandButton
	Button_t4055032469 * ___expandButton_4;
	// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::toggle
	EditorConsentDialogToggleSwitch_t2938536456 * ___toggle_5;
	// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::descriptionText
	EditorConsentDialogClickableText_t861962490 * ___descriptionText_6;
	// UnityEngine.UI.Text EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::title
	Text_t1901882714 * ___title_7;
	// UnityEngine.UI.Image EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::expandArrow
	Image_t2670269651 * ___expandArrow_8;
	// UnityEngine.Sprite EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::collapseIcon
	Sprite_t280657092 * ___collapseIcon_9;
	// UnityEngine.Sprite EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::expandIcon
	Sprite_t280657092 * ___expandIcon_10;
	// UnityEngine.UI.LayoutElement EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::descriptionLayout
	LayoutElement_t1785403678 * ___descriptionLayout_11;
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::expandAnimationDuration
	float ___expandAnimationDuration_12;
	// System.Action`2<System.String,System.Boolean> EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::OnToggleStateUpdated
	Action_2_t2220007950 * ___OnToggleStateUpdated_13;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::<IsDescriptionExpanded>k__BackingField
	bool ___U3CIsDescriptionExpandedU3Ek__BackingField_14;
	// UnityEngine.Coroutine EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::expandCoroutine
	Coroutine_t3829159415 * ___expandCoroutine_15;

public:
	inline static int32_t get_offset_of_expandButton_4() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___expandButton_4)); }
	inline Button_t4055032469 * get_expandButton_4() const { return ___expandButton_4; }
	inline Button_t4055032469 ** get_address_of_expandButton_4() { return &___expandButton_4; }
	inline void set_expandButton_4(Button_t4055032469 * value)
	{
		___expandButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___expandButton_4), value);
	}

	inline static int32_t get_offset_of_toggle_5() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___toggle_5)); }
	inline EditorConsentDialogToggleSwitch_t2938536456 * get_toggle_5() const { return ___toggle_5; }
	inline EditorConsentDialogToggleSwitch_t2938536456 ** get_address_of_toggle_5() { return &___toggle_5; }
	inline void set_toggle_5(EditorConsentDialogToggleSwitch_t2938536456 * value)
	{
		___toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_5), value);
	}

	inline static int32_t get_offset_of_descriptionText_6() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___descriptionText_6)); }
	inline EditorConsentDialogClickableText_t861962490 * get_descriptionText_6() const { return ___descriptionText_6; }
	inline EditorConsentDialogClickableText_t861962490 ** get_address_of_descriptionText_6() { return &___descriptionText_6; }
	inline void set_descriptionText_6(EditorConsentDialogClickableText_t861962490 * value)
	{
		___descriptionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_6), value);
	}

	inline static int32_t get_offset_of_title_7() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___title_7)); }
	inline Text_t1901882714 * get_title_7() const { return ___title_7; }
	inline Text_t1901882714 ** get_address_of_title_7() { return &___title_7; }
	inline void set_title_7(Text_t1901882714 * value)
	{
		___title_7 = value;
		Il2CppCodeGenWriteBarrier((&___title_7), value);
	}

	inline static int32_t get_offset_of_expandArrow_8() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___expandArrow_8)); }
	inline Image_t2670269651 * get_expandArrow_8() const { return ___expandArrow_8; }
	inline Image_t2670269651 ** get_address_of_expandArrow_8() { return &___expandArrow_8; }
	inline void set_expandArrow_8(Image_t2670269651 * value)
	{
		___expandArrow_8 = value;
		Il2CppCodeGenWriteBarrier((&___expandArrow_8), value);
	}

	inline static int32_t get_offset_of_collapseIcon_9() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___collapseIcon_9)); }
	inline Sprite_t280657092 * get_collapseIcon_9() const { return ___collapseIcon_9; }
	inline Sprite_t280657092 ** get_address_of_collapseIcon_9() { return &___collapseIcon_9; }
	inline void set_collapseIcon_9(Sprite_t280657092 * value)
	{
		___collapseIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&___collapseIcon_9), value);
	}

	inline static int32_t get_offset_of_expandIcon_10() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___expandIcon_10)); }
	inline Sprite_t280657092 * get_expandIcon_10() const { return ___expandIcon_10; }
	inline Sprite_t280657092 ** get_address_of_expandIcon_10() { return &___expandIcon_10; }
	inline void set_expandIcon_10(Sprite_t280657092 * value)
	{
		___expandIcon_10 = value;
		Il2CppCodeGenWriteBarrier((&___expandIcon_10), value);
	}

	inline static int32_t get_offset_of_descriptionLayout_11() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___descriptionLayout_11)); }
	inline LayoutElement_t1785403678 * get_descriptionLayout_11() const { return ___descriptionLayout_11; }
	inline LayoutElement_t1785403678 ** get_address_of_descriptionLayout_11() { return &___descriptionLayout_11; }
	inline void set_descriptionLayout_11(LayoutElement_t1785403678 * value)
	{
		___descriptionLayout_11 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionLayout_11), value);
	}

	inline static int32_t get_offset_of_expandAnimationDuration_12() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___expandAnimationDuration_12)); }
	inline float get_expandAnimationDuration_12() const { return ___expandAnimationDuration_12; }
	inline float* get_address_of_expandAnimationDuration_12() { return &___expandAnimationDuration_12; }
	inline void set_expandAnimationDuration_12(float value)
	{
		___expandAnimationDuration_12 = value;
	}

	inline static int32_t get_offset_of_OnToggleStateUpdated_13() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___OnToggleStateUpdated_13)); }
	inline Action_2_t2220007950 * get_OnToggleStateUpdated_13() const { return ___OnToggleStateUpdated_13; }
	inline Action_2_t2220007950 ** get_address_of_OnToggleStateUpdated_13() { return &___OnToggleStateUpdated_13; }
	inline void set_OnToggleStateUpdated_13(Action_2_t2220007950 * value)
	{
		___OnToggleStateUpdated_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleStateUpdated_13), value);
	}

	inline static int32_t get_offset_of_U3CIsDescriptionExpandedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___U3CIsDescriptionExpandedU3Ek__BackingField_14)); }
	inline bool get_U3CIsDescriptionExpandedU3Ek__BackingField_14() const { return ___U3CIsDescriptionExpandedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsDescriptionExpandedU3Ek__BackingField_14() { return &___U3CIsDescriptionExpandedU3Ek__BackingField_14; }
	inline void set_U3CIsDescriptionExpandedU3Ek__BackingField_14(bool value)
	{
		___U3CIsDescriptionExpandedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_expandCoroutine_15() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931, ___expandCoroutine_15)); }
	inline Coroutine_t3829159415 * get_expandCoroutine_15() const { return ___expandCoroutine_15; }
	inline Coroutine_t3829159415 ** get_address_of_expandCoroutine_15() { return &___expandCoroutine_15; }
	inline void set_expandCoroutine_15(Coroutine_t3829159415 * value)
	{
		___expandCoroutine_15 = value;
		Il2CppCodeGenWriteBarrier((&___expandCoroutine_15), value);
	}
};

struct EditorConsentDialogToggleUI_t3945542931_StaticFields
{
public:
	// System.Action`1<System.String> EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_16() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleUI_t3945542931_StaticFields, ___U3CU3Ef__amU24cache0_16)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_16() const { return ___U3CU3Ef__amU24cache0_16; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_16() { return &___U3CU3Ef__amU24cache0_16; }
	inline void set_U3CU3Ef__amU24cache0_16(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORCONSENTDIALOGTOGGLEUI_T3945542931_H
#ifndef EDITORCONSENTDIALOGUI_T244447250_H
#define EDITORCONSENTDIALOGUI_T244447250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogUI
struct  EditorConsentDialogUI_t244447250  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform EasyMobile.Internal.Privacy.EditorConsentDialogUI::rootTransform
	RectTransform_t3704657025 * ___rootTransform_4;
	// UnityEngine.RectTransform EasyMobile.Internal.Privacy.EditorConsentDialogUI::contentParent
	RectTransform_t3704657025 * ___contentParent_5;
	// UnityEngine.UI.Text EasyMobile.Internal.Privacy.EditorConsentDialogUI::titleText
	Text_t1901882714 * ___titleText_6;
	// UnityEngine.UI.Button EasyMobile.Internal.Privacy.EditorConsentDialogUI::backButton
	Button_t4055032469 * ___backButton_7;
	// EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI EasyMobile.Internal.Privacy.EditorConsentDialogUI::togglePrefab
	EditorConsentDialogToggleUI_t3945542931 * ___togglePrefab_8;
	// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText EasyMobile.Internal.Privacy.EditorConsentDialogUI::plainTextPrefab
	EditorConsentDialogClickableText_t861962490 * ___plainTextPrefab_9;
	// EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI EasyMobile.Internal.Privacy.EditorConsentDialogUI::buttonPrefab
	EditorConsentDialogButtonUI_t376789464 * ___buttonPrefab_10;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>> EasyMobile.Internal.Privacy.EditorConsentDialogUI::OnCompleteted
	Action_2_t2005264249 * ___OnCompleteted_11;
	// System.Action EasyMobile.Internal.Privacy.EditorConsentDialogUI::OnDismissed
	Action_t1264377477 * ___OnDismissed_12;
	// System.Action`2<System.String,System.Boolean> EasyMobile.Internal.Privacy.EditorConsentDialogUI::OnToggleStateUpdated
	Action_2_t2220007950 * ___OnToggleStateUpdated_13;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogUI::<IsShowing>k__BackingField
	bool ___U3CIsShowingU3Ek__BackingField_14;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogUI::<IsDismissible>k__BackingField
	bool ___U3CIsDismissibleU3Ek__BackingField_15;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogUI::<IsConstructed>k__BackingField
	bool ___U3CIsConstructedU3Ek__BackingField_16;
	// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogButtonUI> EasyMobile.Internal.Privacy.EditorConsentDialogUI::createdButtons
	Dictionary_2_t162045763 * ___createdButtons_17;
	// System.Collections.Generic.Dictionary`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI> EasyMobile.Internal.Privacy.EditorConsentDialogUI::createdToggles
	Dictionary_2_t3730799230 * ___createdToggles_18;

public:
	inline static int32_t get_offset_of_rootTransform_4() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___rootTransform_4)); }
	inline RectTransform_t3704657025 * get_rootTransform_4() const { return ___rootTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_rootTransform_4() { return &___rootTransform_4; }
	inline void set_rootTransform_4(RectTransform_t3704657025 * value)
	{
		___rootTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___rootTransform_4), value);
	}

	inline static int32_t get_offset_of_contentParent_5() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___contentParent_5)); }
	inline RectTransform_t3704657025 * get_contentParent_5() const { return ___contentParent_5; }
	inline RectTransform_t3704657025 ** get_address_of_contentParent_5() { return &___contentParent_5; }
	inline void set_contentParent_5(RectTransform_t3704657025 * value)
	{
		___contentParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___contentParent_5), value);
	}

	inline static int32_t get_offset_of_titleText_6() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___titleText_6)); }
	inline Text_t1901882714 * get_titleText_6() const { return ___titleText_6; }
	inline Text_t1901882714 ** get_address_of_titleText_6() { return &___titleText_6; }
	inline void set_titleText_6(Text_t1901882714 * value)
	{
		___titleText_6 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_6), value);
	}

	inline static int32_t get_offset_of_backButton_7() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___backButton_7)); }
	inline Button_t4055032469 * get_backButton_7() const { return ___backButton_7; }
	inline Button_t4055032469 ** get_address_of_backButton_7() { return &___backButton_7; }
	inline void set_backButton_7(Button_t4055032469 * value)
	{
		___backButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___backButton_7), value);
	}

	inline static int32_t get_offset_of_togglePrefab_8() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___togglePrefab_8)); }
	inline EditorConsentDialogToggleUI_t3945542931 * get_togglePrefab_8() const { return ___togglePrefab_8; }
	inline EditorConsentDialogToggleUI_t3945542931 ** get_address_of_togglePrefab_8() { return &___togglePrefab_8; }
	inline void set_togglePrefab_8(EditorConsentDialogToggleUI_t3945542931 * value)
	{
		___togglePrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___togglePrefab_8), value);
	}

	inline static int32_t get_offset_of_plainTextPrefab_9() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___plainTextPrefab_9)); }
	inline EditorConsentDialogClickableText_t861962490 * get_plainTextPrefab_9() const { return ___plainTextPrefab_9; }
	inline EditorConsentDialogClickableText_t861962490 ** get_address_of_plainTextPrefab_9() { return &___plainTextPrefab_9; }
	inline void set_plainTextPrefab_9(EditorConsentDialogClickableText_t861962490 * value)
	{
		___plainTextPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___plainTextPrefab_9), value);
	}

	inline static int32_t get_offset_of_buttonPrefab_10() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___buttonPrefab_10)); }
	inline EditorConsentDialogButtonUI_t376789464 * get_buttonPrefab_10() const { return ___buttonPrefab_10; }
	inline EditorConsentDialogButtonUI_t376789464 ** get_address_of_buttonPrefab_10() { return &___buttonPrefab_10; }
	inline void set_buttonPrefab_10(EditorConsentDialogButtonUI_t376789464 * value)
	{
		___buttonPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPrefab_10), value);
	}

	inline static int32_t get_offset_of_OnCompleteted_11() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___OnCompleteted_11)); }
	inline Action_2_t2005264249 * get_OnCompleteted_11() const { return ___OnCompleteted_11; }
	inline Action_2_t2005264249 ** get_address_of_OnCompleteted_11() { return &___OnCompleteted_11; }
	inline void set_OnCompleteted_11(Action_2_t2005264249 * value)
	{
		___OnCompleteted_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnCompleteted_11), value);
	}

	inline static int32_t get_offset_of_OnDismissed_12() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___OnDismissed_12)); }
	inline Action_t1264377477 * get_OnDismissed_12() const { return ___OnDismissed_12; }
	inline Action_t1264377477 ** get_address_of_OnDismissed_12() { return &___OnDismissed_12; }
	inline void set_OnDismissed_12(Action_t1264377477 * value)
	{
		___OnDismissed_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnDismissed_12), value);
	}

	inline static int32_t get_offset_of_OnToggleStateUpdated_13() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___OnToggleStateUpdated_13)); }
	inline Action_2_t2220007950 * get_OnToggleStateUpdated_13() const { return ___OnToggleStateUpdated_13; }
	inline Action_2_t2220007950 ** get_address_of_OnToggleStateUpdated_13() { return &___OnToggleStateUpdated_13; }
	inline void set_OnToggleStateUpdated_13(Action_2_t2220007950 * value)
	{
		___OnToggleStateUpdated_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleStateUpdated_13), value);
	}

	inline static int32_t get_offset_of_U3CIsShowingU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___U3CIsShowingU3Ek__BackingField_14)); }
	inline bool get_U3CIsShowingU3Ek__BackingField_14() const { return ___U3CIsShowingU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsShowingU3Ek__BackingField_14() { return &___U3CIsShowingU3Ek__BackingField_14; }
	inline void set_U3CIsShowingU3Ek__BackingField_14(bool value)
	{
		___U3CIsShowingU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CIsDismissibleU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___U3CIsDismissibleU3Ek__BackingField_15)); }
	inline bool get_U3CIsDismissibleU3Ek__BackingField_15() const { return ___U3CIsDismissibleU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIsDismissibleU3Ek__BackingField_15() { return &___U3CIsDismissibleU3Ek__BackingField_15; }
	inline void set_U3CIsDismissibleU3Ek__BackingField_15(bool value)
	{
		___U3CIsDismissibleU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CIsConstructedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___U3CIsConstructedU3Ek__BackingField_16)); }
	inline bool get_U3CIsConstructedU3Ek__BackingField_16() const { return ___U3CIsConstructedU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsConstructedU3Ek__BackingField_16() { return &___U3CIsConstructedU3Ek__BackingField_16; }
	inline void set_U3CIsConstructedU3Ek__BackingField_16(bool value)
	{
		___U3CIsConstructedU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_createdButtons_17() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___createdButtons_17)); }
	inline Dictionary_2_t162045763 * get_createdButtons_17() const { return ___createdButtons_17; }
	inline Dictionary_2_t162045763 ** get_address_of_createdButtons_17() { return &___createdButtons_17; }
	inline void set_createdButtons_17(Dictionary_2_t162045763 * value)
	{
		___createdButtons_17 = value;
		Il2CppCodeGenWriteBarrier((&___createdButtons_17), value);
	}

	inline static int32_t get_offset_of_createdToggles_18() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250, ___createdToggles_18)); }
	inline Dictionary_2_t3730799230 * get_createdToggles_18() const { return ___createdToggles_18; }
	inline Dictionary_2_t3730799230 ** get_address_of_createdToggles_18() { return &___createdToggles_18; }
	inline void set_createdToggles_18(Dictionary_2_t3730799230 * value)
	{
		___createdToggles_18 = value;
		Il2CppCodeGenWriteBarrier((&___createdToggles_18), value);
	}
};

struct EditorConsentDialogUI_t244447250_StaticFields
{
public:
	// System.Action`1<System.String> EasyMobile.Internal.Privacy.EditorConsentDialogUI::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_19;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI>,System.String> EasyMobile.Internal.Privacy.EditorConsentDialogUI::<>f__am$cache1
	Func_2_t2004531198 * ___U3CU3Ef__amU24cache1_20;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,EasyMobile.Internal.Privacy.EditorConsentDialogToggleUI>,System.Boolean> EasyMobile.Internal.Privacy.EditorConsentDialogUI::<>f__am$cache2
	Func_2_t254368474 * ___U3CU3Ef__amU24cache2_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_19() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250_StaticFields, ___U3CU3Ef__amU24cache0_19)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_19() const { return ___U3CU3Ef__amU24cache0_19; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_19() { return &___U3CU3Ef__amU24cache0_19; }
	inline void set_U3CU3Ef__amU24cache0_19(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_20() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250_StaticFields, ___U3CU3Ef__amU24cache1_20)); }
	inline Func_2_t2004531198 * get_U3CU3Ef__amU24cache1_20() const { return ___U3CU3Ef__amU24cache1_20; }
	inline Func_2_t2004531198 ** get_address_of_U3CU3Ef__amU24cache1_20() { return &___U3CU3Ef__amU24cache1_20; }
	inline void set_U3CU3Ef__amU24cache1_20(Func_2_t2004531198 * value)
	{
		___U3CU3Ef__amU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_21() { return static_cast<int32_t>(offsetof(EditorConsentDialogUI_t244447250_StaticFields, ___U3CU3Ef__amU24cache2_21)); }
	inline Func_2_t254368474 * get_U3CU3Ef__amU24cache2_21() const { return ___U3CU3Ef__amU24cache2_21; }
	inline Func_2_t254368474 ** get_address_of_U3CU3Ef__amU24cache2_21() { return &___U3CU3Ef__amU24cache2_21; }
	inline void set_U3CU3Ef__amU24cache2_21(Func_2_t254368474 * value)
	{
		___U3CU3Ef__amU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORCONSENTDIALOGUI_T244447250_H
#ifndef NATIVECONSENTDIALOGLISTENER_T2145450263_H
#define NATIVECONSENTDIALOGLISTENER_T2145450263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.NativeConsentDialogListener
struct  NativeConsentDialogListener_t2145450263  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`2<System.String,System.Boolean> EasyMobile.Internal.Privacy.NativeConsentDialogListener::ToggleStateUpdated
	Action_2_t2220007950 * ___ToggleStateUpdated_6;
	// System.Action`1<System.String> EasyMobile.Internal.Privacy.NativeConsentDialogListener::DialogCompleted
	Action_1_t2019918284 * ___DialogCompleted_7;
	// System.Action EasyMobile.Internal.Privacy.NativeConsentDialogListener::DialogDismissed
	Action_t1264377477 * ___DialogDismissed_8;

public:
	inline static int32_t get_offset_of_ToggleStateUpdated_6() { return static_cast<int32_t>(offsetof(NativeConsentDialogListener_t2145450263, ___ToggleStateUpdated_6)); }
	inline Action_2_t2220007950 * get_ToggleStateUpdated_6() const { return ___ToggleStateUpdated_6; }
	inline Action_2_t2220007950 ** get_address_of_ToggleStateUpdated_6() { return &___ToggleStateUpdated_6; }
	inline void set_ToggleStateUpdated_6(Action_2_t2220007950 * value)
	{
		___ToggleStateUpdated_6 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleStateUpdated_6), value);
	}

	inline static int32_t get_offset_of_DialogCompleted_7() { return static_cast<int32_t>(offsetof(NativeConsentDialogListener_t2145450263, ___DialogCompleted_7)); }
	inline Action_1_t2019918284 * get_DialogCompleted_7() const { return ___DialogCompleted_7; }
	inline Action_1_t2019918284 ** get_address_of_DialogCompleted_7() { return &___DialogCompleted_7; }
	inline void set_DialogCompleted_7(Action_1_t2019918284 * value)
	{
		___DialogCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___DialogCompleted_7), value);
	}

	inline static int32_t get_offset_of_DialogDismissed_8() { return static_cast<int32_t>(offsetof(NativeConsentDialogListener_t2145450263, ___DialogDismissed_8)); }
	inline Action_t1264377477 * get_DialogDismissed_8() const { return ___DialogDismissed_8; }
	inline Action_t1264377477 ** get_address_of_DialogDismissed_8() { return &___DialogDismissed_8; }
	inline void set_DialogDismissed_8(Action_t1264377477 * value)
	{
		___DialogDismissed_8 = value;
		Il2CppCodeGenWriteBarrier((&___DialogDismissed_8), value);
	}
};

struct NativeConsentDialogListener_t2145450263_StaticFields
{
public:
	// EasyMobile.Internal.Privacy.NativeConsentDialogListener EasyMobile.Internal.Privacy.NativeConsentDialogListener::sInstance
	NativeConsentDialogListener_t2145450263 * ___sInstance_5;

public:
	inline static int32_t get_offset_of_sInstance_5() { return static_cast<int32_t>(offsetof(NativeConsentDialogListener_t2145450263_StaticFields, ___sInstance_5)); }
	inline NativeConsentDialogListener_t2145450263 * get_sInstance_5() const { return ___sInstance_5; }
	inline NativeConsentDialogListener_t2145450263 ** get_address_of_sInstance_5() { return &___sInstance_5; }
	inline void set_sInstance_5(NativeConsentDialogListener_t2145450263 * value)
	{
		___sInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONSENTDIALOGLISTENER_T2145450263_H
#ifndef NOTIFICATIONS_T1249131778_H
#define NOTIFICATIONS_T1249131778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Notifications
struct  Notifications_t1249131778  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Notifications_t1249131778_StaticFields
{
public:
	// EasyMobile.Notifications EasyMobile.Notifications::<Instance>k__BackingField
	Notifications_t1249131778 * ___U3CInstanceU3Ek__BackingField_4;
	// System.Action`1<System.String> EasyMobile.Notifications::PushTokenReceived
	Action_1_t2019918284 * ___PushTokenReceived_7;
	// System.Action`1<EasyMobile.RemoteNotification> EasyMobile.Notifications::RemoteNotificationOpened
	Action_1_t1859537056 * ___RemoteNotificationOpened_8;
	// System.Action`1<EasyMobile.LocalNotification> EasyMobile.Notifications::LocalNotificationOpened
	Action_1_t516386935 * ___LocalNotificationOpened_9;
	// System.String EasyMobile.Notifications::<PushToken>k__BackingField
	String_t* ___U3CPushTokenU3Ek__BackingField_10;
	// EasyMobile.Internal.Notifications.ILocalNotificationClient EasyMobile.Notifications::sLocalNotificationClient
	RuntimeObject* ___sLocalNotificationClient_11;
	// EasyMobile.Internal.Notifications.INotificationListener EasyMobile.Notifications::sListener
	RuntimeObject* ___sListener_12;
	// System.Boolean EasyMobile.Notifications::sIsInitialized
	bool ___sIsInitialized_13;
	// System.Action`1<EasyMobile.LocalNotification> EasyMobile.Notifications::<>f__mg$cache0
	Action_1_t516386935 * ___U3CU3Ef__mgU24cache0_14;
	// System.Action`1<EasyMobile.RemoteNotification> EasyMobile.Notifications::<>f__mg$cache1
	Action_1_t1859537056 * ___U3CU3Ef__mgU24cache1_15;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Notifications_t1249131778 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Notifications_t1249131778 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Notifications_t1249131778 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_PushTokenReceived_7() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___PushTokenReceived_7)); }
	inline Action_1_t2019918284 * get_PushTokenReceived_7() const { return ___PushTokenReceived_7; }
	inline Action_1_t2019918284 ** get_address_of_PushTokenReceived_7() { return &___PushTokenReceived_7; }
	inline void set_PushTokenReceived_7(Action_1_t2019918284 * value)
	{
		___PushTokenReceived_7 = value;
		Il2CppCodeGenWriteBarrier((&___PushTokenReceived_7), value);
	}

	inline static int32_t get_offset_of_RemoteNotificationOpened_8() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___RemoteNotificationOpened_8)); }
	inline Action_1_t1859537056 * get_RemoteNotificationOpened_8() const { return ___RemoteNotificationOpened_8; }
	inline Action_1_t1859537056 ** get_address_of_RemoteNotificationOpened_8() { return &___RemoteNotificationOpened_8; }
	inline void set_RemoteNotificationOpened_8(Action_1_t1859537056 * value)
	{
		___RemoteNotificationOpened_8 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteNotificationOpened_8), value);
	}

	inline static int32_t get_offset_of_LocalNotificationOpened_9() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___LocalNotificationOpened_9)); }
	inline Action_1_t516386935 * get_LocalNotificationOpened_9() const { return ___LocalNotificationOpened_9; }
	inline Action_1_t516386935 ** get_address_of_LocalNotificationOpened_9() { return &___LocalNotificationOpened_9; }
	inline void set_LocalNotificationOpened_9(Action_1_t516386935 * value)
	{
		___LocalNotificationOpened_9 = value;
		Il2CppCodeGenWriteBarrier((&___LocalNotificationOpened_9), value);
	}

	inline static int32_t get_offset_of_U3CPushTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___U3CPushTokenU3Ek__BackingField_10)); }
	inline String_t* get_U3CPushTokenU3Ek__BackingField_10() const { return ___U3CPushTokenU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CPushTokenU3Ek__BackingField_10() { return &___U3CPushTokenU3Ek__BackingField_10; }
	inline void set_U3CPushTokenU3Ek__BackingField_10(String_t* value)
	{
		___U3CPushTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPushTokenU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_sLocalNotificationClient_11() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___sLocalNotificationClient_11)); }
	inline RuntimeObject* get_sLocalNotificationClient_11() const { return ___sLocalNotificationClient_11; }
	inline RuntimeObject** get_address_of_sLocalNotificationClient_11() { return &___sLocalNotificationClient_11; }
	inline void set_sLocalNotificationClient_11(RuntimeObject* value)
	{
		___sLocalNotificationClient_11 = value;
		Il2CppCodeGenWriteBarrier((&___sLocalNotificationClient_11), value);
	}

	inline static int32_t get_offset_of_sListener_12() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___sListener_12)); }
	inline RuntimeObject* get_sListener_12() const { return ___sListener_12; }
	inline RuntimeObject** get_address_of_sListener_12() { return &___sListener_12; }
	inline void set_sListener_12(RuntimeObject* value)
	{
		___sListener_12 = value;
		Il2CppCodeGenWriteBarrier((&___sListener_12), value);
	}

	inline static int32_t get_offset_of_sIsInitialized_13() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___sIsInitialized_13)); }
	inline bool get_sIsInitialized_13() const { return ___sIsInitialized_13; }
	inline bool* get_address_of_sIsInitialized_13() { return &___sIsInitialized_13; }
	inline void set_sIsInitialized_13(bool value)
	{
		___sIsInitialized_13 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_14() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___U3CU3Ef__mgU24cache0_14)); }
	inline Action_1_t516386935 * get_U3CU3Ef__mgU24cache0_14() const { return ___U3CU3Ef__mgU24cache0_14; }
	inline Action_1_t516386935 ** get_address_of_U3CU3Ef__mgU24cache0_14() { return &___U3CU3Ef__mgU24cache0_14; }
	inline void set_U3CU3Ef__mgU24cache0_14(Action_1_t516386935 * value)
	{
		___U3CU3Ef__mgU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_15() { return static_cast<int32_t>(offsetof(Notifications_t1249131778_StaticFields, ___U3CU3Ef__mgU24cache1_15)); }
	inline Action_1_t1859537056 * get_U3CU3Ef__mgU24cache1_15() const { return ___U3CU3Ef__mgU24cache1_15; }
	inline Action_1_t1859537056 ** get_address_of_U3CU3Ef__mgU24cache1_15() { return &___U3CU3Ef__mgU24cache1_15; }
	inline void set_U3CU3Ef__mgU24cache1_15(Action_1_t1859537056 * value)
	{
		___U3CU3Ef__mgU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONS_T1249131778_H
#ifndef STOREREVIEW_T4084477087_H
#define STOREREVIEW_T4084477087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.StoreReview
struct  StoreReview_t4084477087  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct StoreReview_t4084477087_StaticFields
{
public:
	// System.Action`1<EasyMobile.StoreReview/UserAction> EasyMobile.StoreReview::customBehaviour
	Action_1_t1180320417 * ___customBehaviour_10;
	// System.Action`1<System.Int32> EasyMobile.StoreReview::<>f__mg$cache0
	Action_1_t3123413348 * ___U3CU3Ef__mgU24cache0_11;

public:
	inline static int32_t get_offset_of_customBehaviour_10() { return static_cast<int32_t>(offsetof(StoreReview_t4084477087_StaticFields, ___customBehaviour_10)); }
	inline Action_1_t1180320417 * get_customBehaviour_10() const { return ___customBehaviour_10; }
	inline Action_1_t1180320417 ** get_address_of_customBehaviour_10() { return &___customBehaviour_10; }
	inline void set_customBehaviour_10(Action_1_t1180320417 * value)
	{
		___customBehaviour_10 = value;
		Il2CppCodeGenWriteBarrier((&___customBehaviour_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_11() { return static_cast<int32_t>(offsetof(StoreReview_t4084477087_StaticFields, ___U3CU3Ef__mgU24cache0_11)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__mgU24cache0_11() const { return ___U3CU3Ef__mgU24cache0_11; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__mgU24cache0_11() { return &___U3CU3Ef__mgU24cache0_11; }
	inline void set_U3CU3Ef__mgU24cache0_11(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__mgU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOREREVIEW_T4084477087_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef EDITORCONSENTDIALOGTOGGLESWITCH_T2938536456_H
#define EDITORCONSENTDIALOGTOGGLESWITCH_T2938536456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch
struct  EditorConsentDialogToggleSwitch_t2938536456  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Image EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::switchObject
	Image_t2670269651 * ___switchObject_18;
	// UnityEngine.UI.Image EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::background
	Image_t2670269651 * ___background_19;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::m_IsOn
	bool ___m_IsOn_20;
	// UnityEngine.Vector2 EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::isOnPosition
	Vector2_t2156229523  ___isOnPosition_21;
	// UnityEngine.Vector2 EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::isOffPosition
	Vector2_t2156229523  ___isOffPosition_22;
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::animationDuration
	float ___animationDuration_23;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::toggleColor
	bool ___toggleColor_24;
	// UnityEngine.Color EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::switchOnColor
	Color_t2555686324  ___switchOnColor_25;
	// UnityEngine.Color EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::switchOffColor
	Color_t2555686324  ___switchOffColor_26;
	// UnityEngine.Color EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::backgroundOnColor
	Color_t2555686324  ___backgroundOnColor_27;
	// UnityEngine.Color EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::backgroundOffColor
	Color_t2555686324  ___backgroundOffColor_28;
	// EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch/ToggleEvent EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::onValueChanged
	ToggleEvent_t1368036467 * ___onValueChanged_29;
	// UnityEngine.Coroutine EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::switchCoroutine
	Coroutine_t3829159415 * ___switchCoroutine_30;
	// UnityEngine.RectTransform EasyMobile.Internal.Privacy.EditorConsentDialogToggleSwitch::switchTransform
	RectTransform_t3704657025 * ___switchTransform_31;

public:
	inline static int32_t get_offset_of_switchObject_18() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___switchObject_18)); }
	inline Image_t2670269651 * get_switchObject_18() const { return ___switchObject_18; }
	inline Image_t2670269651 ** get_address_of_switchObject_18() { return &___switchObject_18; }
	inline void set_switchObject_18(Image_t2670269651 * value)
	{
		___switchObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___switchObject_18), value);
	}

	inline static int32_t get_offset_of_background_19() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___background_19)); }
	inline Image_t2670269651 * get_background_19() const { return ___background_19; }
	inline Image_t2670269651 ** get_address_of_background_19() { return &___background_19; }
	inline void set_background_19(Image_t2670269651 * value)
	{
		___background_19 = value;
		Il2CppCodeGenWriteBarrier((&___background_19), value);
	}

	inline static int32_t get_offset_of_m_IsOn_20() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___m_IsOn_20)); }
	inline bool get_m_IsOn_20() const { return ___m_IsOn_20; }
	inline bool* get_address_of_m_IsOn_20() { return &___m_IsOn_20; }
	inline void set_m_IsOn_20(bool value)
	{
		___m_IsOn_20 = value;
	}

	inline static int32_t get_offset_of_isOnPosition_21() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___isOnPosition_21)); }
	inline Vector2_t2156229523  get_isOnPosition_21() const { return ___isOnPosition_21; }
	inline Vector2_t2156229523 * get_address_of_isOnPosition_21() { return &___isOnPosition_21; }
	inline void set_isOnPosition_21(Vector2_t2156229523  value)
	{
		___isOnPosition_21 = value;
	}

	inline static int32_t get_offset_of_isOffPosition_22() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___isOffPosition_22)); }
	inline Vector2_t2156229523  get_isOffPosition_22() const { return ___isOffPosition_22; }
	inline Vector2_t2156229523 * get_address_of_isOffPosition_22() { return &___isOffPosition_22; }
	inline void set_isOffPosition_22(Vector2_t2156229523  value)
	{
		___isOffPosition_22 = value;
	}

	inline static int32_t get_offset_of_animationDuration_23() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___animationDuration_23)); }
	inline float get_animationDuration_23() const { return ___animationDuration_23; }
	inline float* get_address_of_animationDuration_23() { return &___animationDuration_23; }
	inline void set_animationDuration_23(float value)
	{
		___animationDuration_23 = value;
	}

	inline static int32_t get_offset_of_toggleColor_24() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___toggleColor_24)); }
	inline bool get_toggleColor_24() const { return ___toggleColor_24; }
	inline bool* get_address_of_toggleColor_24() { return &___toggleColor_24; }
	inline void set_toggleColor_24(bool value)
	{
		___toggleColor_24 = value;
	}

	inline static int32_t get_offset_of_switchOnColor_25() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___switchOnColor_25)); }
	inline Color_t2555686324  get_switchOnColor_25() const { return ___switchOnColor_25; }
	inline Color_t2555686324 * get_address_of_switchOnColor_25() { return &___switchOnColor_25; }
	inline void set_switchOnColor_25(Color_t2555686324  value)
	{
		___switchOnColor_25 = value;
	}

	inline static int32_t get_offset_of_switchOffColor_26() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___switchOffColor_26)); }
	inline Color_t2555686324  get_switchOffColor_26() const { return ___switchOffColor_26; }
	inline Color_t2555686324 * get_address_of_switchOffColor_26() { return &___switchOffColor_26; }
	inline void set_switchOffColor_26(Color_t2555686324  value)
	{
		___switchOffColor_26 = value;
	}

	inline static int32_t get_offset_of_backgroundOnColor_27() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___backgroundOnColor_27)); }
	inline Color_t2555686324  get_backgroundOnColor_27() const { return ___backgroundOnColor_27; }
	inline Color_t2555686324 * get_address_of_backgroundOnColor_27() { return &___backgroundOnColor_27; }
	inline void set_backgroundOnColor_27(Color_t2555686324  value)
	{
		___backgroundOnColor_27 = value;
	}

	inline static int32_t get_offset_of_backgroundOffColor_28() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___backgroundOffColor_28)); }
	inline Color_t2555686324  get_backgroundOffColor_28() const { return ___backgroundOffColor_28; }
	inline Color_t2555686324 * get_address_of_backgroundOffColor_28() { return &___backgroundOffColor_28; }
	inline void set_backgroundOffColor_28(Color_t2555686324  value)
	{
		___backgroundOffColor_28 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_29() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___onValueChanged_29)); }
	inline ToggleEvent_t1368036467 * get_onValueChanged_29() const { return ___onValueChanged_29; }
	inline ToggleEvent_t1368036467 ** get_address_of_onValueChanged_29() { return &___onValueChanged_29; }
	inline void set_onValueChanged_29(ToggleEvent_t1368036467 * value)
	{
		___onValueChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_29), value);
	}

	inline static int32_t get_offset_of_switchCoroutine_30() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___switchCoroutine_30)); }
	inline Coroutine_t3829159415 * get_switchCoroutine_30() const { return ___switchCoroutine_30; }
	inline Coroutine_t3829159415 ** get_address_of_switchCoroutine_30() { return &___switchCoroutine_30; }
	inline void set_switchCoroutine_30(Coroutine_t3829159415 * value)
	{
		___switchCoroutine_30 = value;
		Il2CppCodeGenWriteBarrier((&___switchCoroutine_30), value);
	}

	inline static int32_t get_offset_of_switchTransform_31() { return static_cast<int32_t>(offsetof(EditorConsentDialogToggleSwitch_t2938536456, ___switchTransform_31)); }
	inline RectTransform_t3704657025 * get_switchTransform_31() const { return ___switchTransform_31; }
	inline RectTransform_t3704657025 ** get_address_of_switchTransform_31() { return &___switchTransform_31; }
	inline void set_switchTransform_31(RectTransform_t3704657025 * value)
	{
		___switchTransform_31 = value;
		Il2CppCodeGenWriteBarrier((&___switchTransform_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORCONSENTDIALOGTOGGLESWITCH_T2938536456_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_30)); }
	inline FontData_t746620069 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t746620069 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t746620069 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_32)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_34)); }
	inline Material_t340375123 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_t340375123 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef EDITORCONSENTDIALOGCLICKABLETEXT_T861962490_H
#define EDITORCONSENTDIALOGCLICKABLETEXT_T861962490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText
struct  EditorConsentDialogClickableText_t861962490  : public Text_t1901882714
{
public:
	// System.String EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::HyperlinkColor
	String_t* ___HyperlinkColor_37;
	// System.Action`1<System.String> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::OnHyperlinkClicked
	Action_1_t2019918284 * ___OnHyperlinkClicked_38;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::imagesPool
	List_1_t4142344393 * ___imagesPool_39;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::culledImagesPool
	List_1_t2585711361 * ___culledImagesPool_40;
	// System.Collections.Generic.List`1<System.Int32> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::imagesVertexIndex
	List_1_t128053199 * ___imagesVertexIndex_41;
	// System.Collections.Generic.List`1<EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/HrefInfo> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::hrefInfos
	List_1_t4187619232 * ___hrefInfos_45;
	// System.String EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::fixedString
	String_t* ___fixedString_46;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::clearImages
	bool ___clearImages_47;
	// System.String EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::outputText
	String_t* ___outputText_48;
	// EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName[] EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::inspectorIconList
	IconNameU5BU5D_t1392287153* ___inspectorIconList_49;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::iconList
	Dictionary_2_t65913391 * ___iconList_50;
	// System.Single EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::imageScalingFactor
	float ___imageScalingFactor_51;
	// UnityEngine.Vector2 EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::imageOffset
	Vector2_t2156229523  ___imageOffset_52;
	// UnityEngine.UI.Button EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::button
	Button_t4055032469 * ___button_53;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::positions
	List_1_t3628304265 * ___positions_54;
	// System.String EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::previousText
	String_t* ___previousText_55;
	// System.Boolean EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::isCreatingHrefInfos
	bool ___isCreatingHrefInfos_56;

public:
	inline static int32_t get_offset_of_HyperlinkColor_37() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___HyperlinkColor_37)); }
	inline String_t* get_HyperlinkColor_37() const { return ___HyperlinkColor_37; }
	inline String_t** get_address_of_HyperlinkColor_37() { return &___HyperlinkColor_37; }
	inline void set_HyperlinkColor_37(String_t* value)
	{
		___HyperlinkColor_37 = value;
		Il2CppCodeGenWriteBarrier((&___HyperlinkColor_37), value);
	}

	inline static int32_t get_offset_of_OnHyperlinkClicked_38() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___OnHyperlinkClicked_38)); }
	inline Action_1_t2019918284 * get_OnHyperlinkClicked_38() const { return ___OnHyperlinkClicked_38; }
	inline Action_1_t2019918284 ** get_address_of_OnHyperlinkClicked_38() { return &___OnHyperlinkClicked_38; }
	inline void set_OnHyperlinkClicked_38(Action_1_t2019918284 * value)
	{
		___OnHyperlinkClicked_38 = value;
		Il2CppCodeGenWriteBarrier((&___OnHyperlinkClicked_38), value);
	}

	inline static int32_t get_offset_of_imagesPool_39() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___imagesPool_39)); }
	inline List_1_t4142344393 * get_imagesPool_39() const { return ___imagesPool_39; }
	inline List_1_t4142344393 ** get_address_of_imagesPool_39() { return &___imagesPool_39; }
	inline void set_imagesPool_39(List_1_t4142344393 * value)
	{
		___imagesPool_39 = value;
		Il2CppCodeGenWriteBarrier((&___imagesPool_39), value);
	}

	inline static int32_t get_offset_of_culledImagesPool_40() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___culledImagesPool_40)); }
	inline List_1_t2585711361 * get_culledImagesPool_40() const { return ___culledImagesPool_40; }
	inline List_1_t2585711361 ** get_address_of_culledImagesPool_40() { return &___culledImagesPool_40; }
	inline void set_culledImagesPool_40(List_1_t2585711361 * value)
	{
		___culledImagesPool_40 = value;
		Il2CppCodeGenWriteBarrier((&___culledImagesPool_40), value);
	}

	inline static int32_t get_offset_of_imagesVertexIndex_41() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___imagesVertexIndex_41)); }
	inline List_1_t128053199 * get_imagesVertexIndex_41() const { return ___imagesVertexIndex_41; }
	inline List_1_t128053199 ** get_address_of_imagesVertexIndex_41() { return &___imagesVertexIndex_41; }
	inline void set_imagesVertexIndex_41(List_1_t128053199 * value)
	{
		___imagesVertexIndex_41 = value;
		Il2CppCodeGenWriteBarrier((&___imagesVertexIndex_41), value);
	}

	inline static int32_t get_offset_of_hrefInfos_45() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___hrefInfos_45)); }
	inline List_1_t4187619232 * get_hrefInfos_45() const { return ___hrefInfos_45; }
	inline List_1_t4187619232 ** get_address_of_hrefInfos_45() { return &___hrefInfos_45; }
	inline void set_hrefInfos_45(List_1_t4187619232 * value)
	{
		___hrefInfos_45 = value;
		Il2CppCodeGenWriteBarrier((&___hrefInfos_45), value);
	}

	inline static int32_t get_offset_of_fixedString_46() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___fixedString_46)); }
	inline String_t* get_fixedString_46() const { return ___fixedString_46; }
	inline String_t** get_address_of_fixedString_46() { return &___fixedString_46; }
	inline void set_fixedString_46(String_t* value)
	{
		___fixedString_46 = value;
		Il2CppCodeGenWriteBarrier((&___fixedString_46), value);
	}

	inline static int32_t get_offset_of_clearImages_47() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___clearImages_47)); }
	inline bool get_clearImages_47() const { return ___clearImages_47; }
	inline bool* get_address_of_clearImages_47() { return &___clearImages_47; }
	inline void set_clearImages_47(bool value)
	{
		___clearImages_47 = value;
	}

	inline static int32_t get_offset_of_outputText_48() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___outputText_48)); }
	inline String_t* get_outputText_48() const { return ___outputText_48; }
	inline String_t** get_address_of_outputText_48() { return &___outputText_48; }
	inline void set_outputText_48(String_t* value)
	{
		___outputText_48 = value;
		Il2CppCodeGenWriteBarrier((&___outputText_48), value);
	}

	inline static int32_t get_offset_of_inspectorIconList_49() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___inspectorIconList_49)); }
	inline IconNameU5BU5D_t1392287153* get_inspectorIconList_49() const { return ___inspectorIconList_49; }
	inline IconNameU5BU5D_t1392287153** get_address_of_inspectorIconList_49() { return &___inspectorIconList_49; }
	inline void set_inspectorIconList_49(IconNameU5BU5D_t1392287153* value)
	{
		___inspectorIconList_49 = value;
		Il2CppCodeGenWriteBarrier((&___inspectorIconList_49), value);
	}

	inline static int32_t get_offset_of_iconList_50() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___iconList_50)); }
	inline Dictionary_2_t65913391 * get_iconList_50() const { return ___iconList_50; }
	inline Dictionary_2_t65913391 ** get_address_of_iconList_50() { return &___iconList_50; }
	inline void set_iconList_50(Dictionary_2_t65913391 * value)
	{
		___iconList_50 = value;
		Il2CppCodeGenWriteBarrier((&___iconList_50), value);
	}

	inline static int32_t get_offset_of_imageScalingFactor_51() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___imageScalingFactor_51)); }
	inline float get_imageScalingFactor_51() const { return ___imageScalingFactor_51; }
	inline float* get_address_of_imageScalingFactor_51() { return &___imageScalingFactor_51; }
	inline void set_imageScalingFactor_51(float value)
	{
		___imageScalingFactor_51 = value;
	}

	inline static int32_t get_offset_of_imageOffset_52() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___imageOffset_52)); }
	inline Vector2_t2156229523  get_imageOffset_52() const { return ___imageOffset_52; }
	inline Vector2_t2156229523 * get_address_of_imageOffset_52() { return &___imageOffset_52; }
	inline void set_imageOffset_52(Vector2_t2156229523  value)
	{
		___imageOffset_52 = value;
	}

	inline static int32_t get_offset_of_button_53() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___button_53)); }
	inline Button_t4055032469 * get_button_53() const { return ___button_53; }
	inline Button_t4055032469 ** get_address_of_button_53() { return &___button_53; }
	inline void set_button_53(Button_t4055032469 * value)
	{
		___button_53 = value;
		Il2CppCodeGenWriteBarrier((&___button_53), value);
	}

	inline static int32_t get_offset_of_positions_54() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___positions_54)); }
	inline List_1_t3628304265 * get_positions_54() const { return ___positions_54; }
	inline List_1_t3628304265 ** get_address_of_positions_54() { return &___positions_54; }
	inline void set_positions_54(List_1_t3628304265 * value)
	{
		___positions_54 = value;
		Il2CppCodeGenWriteBarrier((&___positions_54), value);
	}

	inline static int32_t get_offset_of_previousText_55() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___previousText_55)); }
	inline String_t* get_previousText_55() const { return ___previousText_55; }
	inline String_t** get_address_of_previousText_55() { return &___previousText_55; }
	inline void set_previousText_55(String_t* value)
	{
		___previousText_55 = value;
		Il2CppCodeGenWriteBarrier((&___previousText_55), value);
	}

	inline static int32_t get_offset_of_isCreatingHrefInfos_56() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490, ___isCreatingHrefInfos_56)); }
	inline bool get_isCreatingHrefInfos_56() const { return ___isCreatingHrefInfos_56; }
	inline bool* get_address_of_isCreatingHrefInfos_56() { return &___isCreatingHrefInfos_56; }
	inline void set_isCreatingHrefInfos_56(bool value)
	{
		___isCreatingHrefInfos_56 = value;
	}
};

struct EditorConsentDialogClickableText_t861962490_StaticFields
{
public:
	// System.Text.StringBuilder EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::textBuilder
	StringBuilder_t * ___textBuilder_42;
	// System.Text.RegularExpressions.Regex EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::hrefRegex
	Regex_t3657309853 * ___hrefRegex_43;
	// System.Text.RegularExpressions.Regex EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::regex
	Regex_t3657309853 * ___regex_44;
	// System.Predicate`1<UnityEngine.UI.Image> EasyMobile.Internal.Privacy.EditorConsentDialogClickableText::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_57;

public:
	inline static int32_t get_offset_of_textBuilder_42() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490_StaticFields, ___textBuilder_42)); }
	inline StringBuilder_t * get_textBuilder_42() const { return ___textBuilder_42; }
	inline StringBuilder_t ** get_address_of_textBuilder_42() { return &___textBuilder_42; }
	inline void set_textBuilder_42(StringBuilder_t * value)
	{
		___textBuilder_42 = value;
		Il2CppCodeGenWriteBarrier((&___textBuilder_42), value);
	}

	inline static int32_t get_offset_of_hrefRegex_43() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490_StaticFields, ___hrefRegex_43)); }
	inline Regex_t3657309853 * get_hrefRegex_43() const { return ___hrefRegex_43; }
	inline Regex_t3657309853 ** get_address_of_hrefRegex_43() { return &___hrefRegex_43; }
	inline void set_hrefRegex_43(Regex_t3657309853 * value)
	{
		___hrefRegex_43 = value;
		Il2CppCodeGenWriteBarrier((&___hrefRegex_43), value);
	}

	inline static int32_t get_offset_of_regex_44() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490_StaticFields, ___regex_44)); }
	inline Regex_t3657309853 * get_regex_44() const { return ___regex_44; }
	inline Regex_t3657309853 ** get_address_of_regex_44() { return &___regex_44; }
	inline void set_regex_44(Regex_t3657309853 * value)
	{
		___regex_44 = value;
		Il2CppCodeGenWriteBarrier((&___regex_44), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_57() { return static_cast<int32_t>(offsetof(EditorConsentDialogClickableText_t861962490_StaticFields, ___U3CU3Ef__amU24cache0_57)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_57() const { return ___U3CU3Ef__amU24cache0_57; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_57() { return &___U3CU3Ef__amU24cache0_57; }
	inline void set_U3CU3Ef__amU24cache0_57(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_57), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORCONSENTDIALOGCLICKABLETEXT_T861962490_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (GetPendingNotificationRequestsCallback_t3407937767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (GetNotificationResponseCallback_t3346532913), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (Notification_t414530818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4902[5] = 
{
	Notification_t414530818::get_offset_of_id_0(),
	Notification_t414530818::get_offset_of_actionId_1(),
	Notification_t414530818::get_offset_of_content_2(),
	Notification_t414530818::get_offset_of_isAppInForeground_3(),
	Notification_t414530818::get_offset_of_isOpened_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (LocalNotification_t343919340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (RemoteNotification_t1687069461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4904[2] = 
{
	RemoteNotification_t1687069461::get_offset_of_oneSignalPayload_5(),
	RemoteNotification_t1687069461::get_offset_of_firebasePayload_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (NotificationCategory_t2144930983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4905[14] = 
{
	NotificationCategory_t2144930983::get_offset_of_id_0(),
	NotificationCategory_t2144930983::get_offset_of_groupId_1(),
	NotificationCategory_t2144930983::get_offset_of_name_2(),
	NotificationCategory_t2144930983::get_offset_of_description_3(),
	NotificationCategory_t2144930983::get_offset_of_importance_4(),
	NotificationCategory_t2144930983::get_offset_of_enableBadge_5(),
	NotificationCategory_t2144930983::get_offset_of_lights_6(),
	NotificationCategory_t2144930983::get_offset_of_lightColor_7(),
	NotificationCategory_t2144930983::get_offset_of_vibration_8(),
	NotificationCategory_t2144930983::get_offset_of_vibrationPattern_9(),
	NotificationCategory_t2144930983::get_offset_of_lockScreenVisibility_10(),
	NotificationCategory_t2144930983::get_offset_of_sound_11(),
	NotificationCategory_t2144930983::get_offset_of_soundName_12(),
	NotificationCategory_t2144930983::get_offset_of_actionButtons_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (Importance_t3090563125)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4906[7] = 
{
	Importance_t3090563125::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { sizeof (LightOptions_t1731901357)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4907[4] = 
{
	LightOptions_t1731901357::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { sizeof (SoundOptions_t3551592480)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4908[4] = 
{
	SoundOptions_t3551592480::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { sizeof (VibrationOptions_t3667218198)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4909[4] = 
{
	VibrationOptions_t3667218198::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { sizeof (LockScreenVisibilityOptions_t1989431188)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4910[4] = 
{
	LockScreenVisibilityOptions_t1989431188::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { sizeof (ActionButton_t554814726)+ sizeof (RuntimeObject), sizeof(ActionButton_t554814726_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4911[2] = 
{
	ActionButton_t554814726::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionButton_t554814726::get_offset_of_title_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { sizeof (NotificationCategoryGroup_t219032366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4912[2] = 
{
	NotificationCategoryGroup_t219032366::get_offset_of_id_0(),
	NotificationCategoryGroup_t219032366::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { sizeof (NotificationContent_t758602733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4913[10] = 
{
	0,
	0,
	NotificationContent_t758602733::get_offset_of_title_2(),
	NotificationContent_t758602733::get_offset_of_subtitle_3(),
	NotificationContent_t758602733::get_offset_of_body_4(),
	NotificationContent_t758602733::get_offset_of_badge_5(),
	NotificationContent_t758602733::get_offset_of_userInfo_6(),
	NotificationContent_t758602733::get_offset_of_categoryId_7(),
	NotificationContent_t758602733::get_offset_of_smallIcon_8(),
	NotificationContent_t758602733::get_offset_of_largeIcon_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { sizeof (NotificationRepeat_t3969194584)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4914[6] = 
{
	NotificationRepeat_t3969194584::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { sizeof (NotificationRepeatExtension_t3960328516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { sizeof (NotificationRequest_t4022128807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4916[4] = 
{
	NotificationRequest_t4022128807::get_offset_of_id_0(),
	NotificationRequest_t4022128807::get_offset_of_content_1(),
	NotificationRequest_t4022128807::get_offset_of_nextTriggerDate_2(),
	NotificationRequest_t4022128807::get_offset_of_repeat_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { sizeof (Notifications_t1249131778), -1, sizeof(Notifications_t1249131778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4917[12] = 
{
	Notifications_t1249131778_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	0,
	0,
	Notifications_t1249131778_StaticFields::get_offset_of_PushTokenReceived_7(),
	Notifications_t1249131778_StaticFields::get_offset_of_RemoteNotificationOpened_8(),
	Notifications_t1249131778_StaticFields::get_offset_of_LocalNotificationOpened_9(),
	Notifications_t1249131778_StaticFields::get_offset_of_U3CPushTokenU3Ek__BackingField_10(),
	Notifications_t1249131778_StaticFields::get_offset_of_sLocalNotificationClient_11(),
	Notifications_t1249131778_StaticFields::get_offset_of_sListener_12(),
	Notifications_t1249131778_StaticFields::get_offset_of_sIsInitialized_13(),
	Notifications_t1249131778_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
	Notifications_t1249131778_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { sizeof (U3CCRAutoInitU3Ec__Iterator0_t481153097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4918[4] = 
{
	U3CCRAutoInitU3Ec__Iterator0_t481153097::get_offset_of_delay_0(),
	U3CCRAutoInitU3Ec__Iterator0_t481153097::get_offset_of_U24current_1(),
	U3CCRAutoInitU3Ec__Iterator0_t481153097::get_offset_of_U24disposing_2(),
	U3CCRAutoInitU3Ec__Iterator0_t481153097::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { sizeof (NotificationsConsentManager_t864684174), -1, sizeof(NotificationsConsentManager_t864684174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4919[2] = 
{
	0,
	NotificationsConsentManager_t864684174_StaticFields::get_offset_of_sInstance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { sizeof (NotificationsSettings_t3829333496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4920[11] = 
{
	0,
	0,
	NotificationsSettings_t3829333496::get_offset_of_mAutoInit_2(),
	NotificationsSettings_t3829333496::get_offset_of_mAutoInitDelay_3(),
	NotificationsSettings_t3829333496::get_offset_of_mIosAuthOptions_4(),
	NotificationsSettings_t3829333496::get_offset_of_mPushNotificationService_5(),
	NotificationsSettings_t3829333496::get_offset_of_mOneSignalAppId_6(),
	NotificationsSettings_t3829333496::get_offset_of_mFirebaseTopics_7(),
	NotificationsSettings_t3829333496::get_offset_of_mCategoryGroups_8(),
	NotificationsSettings_t3829333496::get_offset_of_mDefaultCategory_9(),
	NotificationsSettings_t3829333496::get_offset_of_mUserCategories_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { sizeof (PushNotificationProvider_t1742081748)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4921[4] = 
{
	PushNotificationProvider_t1742081748::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { sizeof (NotificationAuthOptions_t1370813110)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4922[4] = 
{
	NotificationAuthOptions_t1370813110::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { sizeof (OneSignalNotificationPayload_t1758448350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4923[19] = 
{
	OneSignalNotificationPayload_t1758448350::get_offset_of_notificationID_0(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_sound_1(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_title_2(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_body_3(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_subtitle_4(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_launchURL_5(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_additionalData_6(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_actionButtons_7(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_contentAvailable_8(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_badge_9(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_smallIcon_10(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_largeIcon_11(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_bigPicture_12(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_smallIconAccentColor_13(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_ledColor_14(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_lockScreenVisibility_15(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_groupKey_16(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_groupMessage_17(),
	OneSignalNotificationPayload_t1758448350::get_offset_of_fromProjectNumber_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { sizeof (ConsentDialog_t3732976094), -1, sizeof(ConsentDialog_t3732976094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4924[18] = 
{
	0,
	0,
	0,
	0,
	0,
	ConsentDialog_t3732976094_StaticFields::get_offset_of_U3CActiveDialogU3Ek__BackingField_5(),
	ConsentDialog_t3732976094::get_offset_of_mContent_6(),
	ConsentDialog_t3732976094::get_offset_of_mTitle_7(),
	ConsentDialog_t3732976094::get_offset_of_mToggles_8(),
	ConsentDialog_t3732976094::get_offset_of_mActionButtons_9(),
	ConsentDialog_t3732976094::get_offset_of_ToggleStateUpdated_10(),
	ConsentDialog_t3732976094::get_offset_of_Completed_11(),
	ConsentDialog_t3732976094::get_offset_of_Dismissed_12(),
	ConsentDialog_t3732976094_StaticFields::get_offset_of_sPlatformDialog_13(),
	ConsentDialog_t3732976094_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
	ConsentDialog_t3732976094_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_15(),
	ConsentDialog_t3732976094_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_16(),
	ConsentDialog_t3732976094_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { sizeof (Toggle_t3590481327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4925[7] = 
{
	Toggle_t3590481327::get_offset_of_id_0(),
	Toggle_t3590481327::get_offset_of_title_1(),
	Toggle_t3590481327::get_offset_of_onDescription_2(),
	Toggle_t3590481327::get_offset_of_offDescription_3(),
	Toggle_t3590481327::get_offset_of_isOn_4(),
	Toggle_t3590481327::get_offset_of_interactable_5(),
	Toggle_t3590481327::get_offset_of_shouldToggleDescription_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (Button_t2810410457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4926[7] = 
{
	Button_t2810410457::get_offset_of_id_0(),
	Button_t2810410457::get_offset_of_title_1(),
	Button_t2810410457::get_offset_of_interactable_2(),
	Button_t2810410457::get_offset_of_titleColor_3(),
	Button_t2810410457::get_offset_of_backgroundColor_4(),
	Button_t2810410457::get_offset_of_uninteractableTitleColor_5(),
	Button_t2810410457::get_offset_of_uninteractableBackgroundColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (CompletedResults_t1454177566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4927[2] = 
{
	CompletedResults_t1454177566::get_offset_of_buttonId_0(),
	CompletedResults_t1454177566::get_offset_of_toggleValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (CompletedHandler_t441206176), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (ToggleStateUpdatedHandler_t2741348581), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (U3CRemoveToggleU3Ec__AnonStorey1_t2440481277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4930[1] = 
{
	U3CRemoveToggleU3Ec__AnonStorey1_t2440481277::get_offset_of_toggleId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (U3CRemoveButtonU3Ec__AnonStorey2_t1939157825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4931[1] = 
{
	U3CRemoveButtonU3Ec__AnonStorey2_t1939157825::get_offset_of_buttonId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (U3CFindToggleWithIdU3Ec__AnonStorey3_t227446378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4932[1] = 
{
	U3CFindToggleWithIdU3Ec__AnonStorey3_t227446378::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { sizeof (U3CFindButtonWithIdU3Ec__AnonStorey4_t647197783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4933[1] = 
{
	U3CFindButtonWithIdU3Ec__AnonStorey4_t647197783::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { sizeof (U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4934[9] = 
{
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U3CmatchU3E__0_0(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U3CmatchesU3E__0_1(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U24locvar0_2(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U3CurlU3E__1_3(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U24locvar1_4(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U24this_5(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U24current_6(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U24disposing_7(),
	U3CGetAllUrlsInContentU3Ec__Iterator0_t1692377037::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (U3CGetAllPatternsInContentU3Ec__AnonStorey5_t307009447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4935[1] = 
{
	U3CGetAllPatternsInContentU3Ec__AnonStorey5_t307009447::get_offset_of_allPatterns_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (ConsentManager_t1411390104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4936[2] = 
{
	ConsentManager_t1411390104::get_offset_of_mDataPrivacyConsentKey_0(),
	ConsentManager_t1411390104::get_offset_of_DataPrivacyConsentUpdated_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (ConsentStatus_t2718071093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4937[4] = 
{
	ConsentStatus_t2718071093::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (ConsentStorage_t2037750554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4938[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (EEACountries_t2360821445)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4939[46] = 
{
	EEACountries_t2360821445::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { sizeof (EEACountriesExtension_t1734210167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (EEARegionStatus_t3128437537)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4941[4] = 
{
	EEARegionStatus_t3128437537::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (EEARegionStatusExtension_t1071987055), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { sizeof (EEARegionValidationMethods_t2477000099)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4943[5] = 
{
	EEARegionValidationMethods_t2477000099::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (EEARegionValidator_t2555212889), -1, sizeof(EEARegionValidator_t2555212889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4944[2] = 
{
	EEARegionValidator_t2555212889_StaticFields::get_offset_of_DefaultMethods_0(),
	EEARegionValidator_t2555212889_StaticFields::get_offset_of_validator_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (BaseNativeEEARegionValidator_t3155147623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4945[1] = 
{
	BaseNativeEEARegionValidator_t3155147623::get_offset_of_isValidateCoroutineRunning_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (GoogleServiceResponse_t406184433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4946[1] = 
{
	GoogleServiceResponse_t406184433::get_offset_of_is_request_in_eea_or_unknown_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4947[10] = 
{
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_methods_0(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_callback_1(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U3CresultsU3E__0_2(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U24locvar0_3(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U3CmethodU3E__1_4(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U3CresultU3E__0_5(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U24this_6(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U24current_7(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U24disposing_8(),
	U3CValidateEEARegionStatusCoroutineU3Ec__Iterator0_t1025791164::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4948[8] = 
{
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U3CwwwU3E__0_0(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U3CstatusU3E__0_1(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U3CerrorFlagU3E__0_2(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_resultsStack_3(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U24this_4(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U24current_5(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U24disposing_6(),
	U3CValidateViaGoogleServiceCoroutineU3Ec__Iterator1_t3967541878::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (iOSEEARegionValidator_t2872212933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (UnsupportedEEARegionValidator_t2520111492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4951[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (GlobalConsentManager_t3680228011), -1, sizeof(GlobalConsentManager_t3680228011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4952[2] = 
{
	0,
	GlobalConsentManager_t3680228011_StaticFields::get_offset_of_sInstance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (ConsentDialogContentSerializer_t3709129555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4954[1] = 
{
	ConsentDialogContentSerializer_t3709129555::get_offset_of_U3CSerializedContentU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (SplitContent_t2522047456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4955[5] = 
{
	0,
	0,
	0,
	SplitContent_t2522047456::get_offset_of_type_3(),
	SplitContent_t2522047456::get_offset_of_content_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (EditorConsentDialogButtonUI_t376789464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4956[2] = 
{
	EditorConsentDialogButtonUI_t376789464::get_offset_of_button_4(),
	EditorConsentDialogButtonUI_t376789464::get_offset_of_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (EditorConsentDialogClickableText_t861962490), -1, sizeof(EditorConsentDialogClickableText_t861962490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4957[21] = 
{
	EditorConsentDialogClickableText_t861962490::get_offset_of_HyperlinkColor_37(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_OnHyperlinkClicked_38(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_imagesPool_39(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_culledImagesPool_40(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_imagesVertexIndex_41(),
	EditorConsentDialogClickableText_t861962490_StaticFields::get_offset_of_textBuilder_42(),
	EditorConsentDialogClickableText_t861962490_StaticFields::get_offset_of_hrefRegex_43(),
	EditorConsentDialogClickableText_t861962490_StaticFields::get_offset_of_regex_44(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_hrefInfos_45(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_fixedString_46(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_clearImages_47(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_outputText_48(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_inspectorIconList_49(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_iconList_50(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_imageScalingFactor_51(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_imageOffset_52(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_button_53(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_positions_54(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_previousText_55(),
	EditorConsentDialogClickableText_t861962490::get_offset_of_isCreatingHrefInfos_56(),
	EditorConsentDialogClickableText_t861962490_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (IconName_t2443857936)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4958[2] = 
{
	IconName_t2443857936::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IconName_t2443857936::get_offset_of_sprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (HrefInfo_t2715544490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4959[4] = 
{
	HrefInfo_t2715544490::get_offset_of_startIndex_0(),
	HrefInfo_t2715544490::get_offset_of_endIndex_1(),
	HrefInfo_t2715544490::get_offset_of_name_2(),
	HrefInfo_t2715544490::get_offset_of_boxes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { sizeof (EditorConsentDialogToggleSwitch_t2938536456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4960[14] = 
{
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_switchObject_18(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_background_19(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_m_IsOn_20(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_isOnPosition_21(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_isOffPosition_22(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_animationDuration_23(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_toggleColor_24(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_switchOnColor_25(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_switchOffColor_26(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_backgroundOnColor_27(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_backgroundOffColor_28(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_onValueChanged_29(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_switchCoroutine_30(),
	EditorConsentDialogToggleSwitch_t2938536456::get_offset_of_switchTransform_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { sizeof (ToggleEvent_t1368036467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { sizeof (U3CSwitchCoroutineU3Ec__Iterator0_t1662270879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4962[8] = 
{
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_isOn_0(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_U3CcurrentLerpValueU3E__0_1(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_U3CstartPositionU3E__0_2(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_targetPosition_3(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_U24this_4(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_U24current_5(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_U24disposing_6(),
	U3CSwitchCoroutineU3Ec__Iterator0_t1662270879::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { sizeof (EditorConsentDialogToggleUI_t3945542931), -1, sizeof(EditorConsentDialogToggleUI_t3945542931_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4963[13] = 
{
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_expandButton_4(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_toggle_5(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_descriptionText_6(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_title_7(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_expandArrow_8(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_collapseIcon_9(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_expandIcon_10(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_descriptionLayout_11(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_expandAnimationDuration_12(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_OnToggleStateUpdated_13(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_U3CIsDescriptionExpandedU3Ek__BackingField_14(),
	EditorConsentDialogToggleUI_t3945542931::get_offset_of_expandCoroutine_15(),
	EditorConsentDialogToggleUI_t3945542931_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (U3CSetupToggleU3Ec__AnonStorey1_t3174135061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4964[2] = 
{
	U3CSetupToggleU3Ec__AnonStorey1_t3174135061::get_offset_of_toggleData_0(),
	U3CSetupToggleU3Ec__AnonStorey1_t3174135061::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4965[7] = 
{
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_U3CcurrentLerpValueU3E__0_0(),
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_U3CstartHeightU3E__0_1(),
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_targetHeight_2(),
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_U24this_3(),
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_U24current_4(),
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_U24disposing_5(),
	U3CAnimateDescriptionCoroutineU3Ec__Iterator0_t3726424355::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (EditorConsentDialogUI_t244447250), -1, sizeof(EditorConsentDialogUI_t244447250_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4966[18] = 
{
	EditorConsentDialogUI_t244447250::get_offset_of_rootTransform_4(),
	EditorConsentDialogUI_t244447250::get_offset_of_contentParent_5(),
	EditorConsentDialogUI_t244447250::get_offset_of_titleText_6(),
	EditorConsentDialogUI_t244447250::get_offset_of_backButton_7(),
	EditorConsentDialogUI_t244447250::get_offset_of_togglePrefab_8(),
	EditorConsentDialogUI_t244447250::get_offset_of_plainTextPrefab_9(),
	EditorConsentDialogUI_t244447250::get_offset_of_buttonPrefab_10(),
	EditorConsentDialogUI_t244447250::get_offset_of_OnCompleteted_11(),
	EditorConsentDialogUI_t244447250::get_offset_of_OnDismissed_12(),
	EditorConsentDialogUI_t244447250::get_offset_of_OnToggleStateUpdated_13(),
	EditorConsentDialogUI_t244447250::get_offset_of_U3CIsShowingU3Ek__BackingField_14(),
	EditorConsentDialogUI_t244447250::get_offset_of_U3CIsDismissibleU3Ek__BackingField_15(),
	EditorConsentDialogUI_t244447250::get_offset_of_U3CIsConstructedU3Ek__BackingField_16(),
	EditorConsentDialogUI_t244447250::get_offset_of_createdButtons_17(),
	EditorConsentDialogUI_t244447250::get_offset_of_createdToggles_18(),
	EditorConsentDialogUI_t244447250_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_19(),
	EditorConsentDialogUI_t244447250_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_20(),
	EditorConsentDialogUI_t244447250_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (U3CAddButtonU3Ec__AnonStorey0_t1451979152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4967[2] = 
{
	U3CAddButtonU3Ec__AnonStorey0_t1451979152::get_offset_of_buttonData_0(),
	U3CAddButtonU3Ec__AnonStorey0_t1451979152::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (NativeConsentDialogListener_t2145450263), -1, sizeof(NativeConsentDialogListener_t2145450263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4968[5] = 
{
	0,
	NativeConsentDialogListener_t2145450263_StaticFields::get_offset_of_sInstance_5(),
	NativeConsentDialogListener_t2145450263::get_offset_of_ToggleStateUpdated_6(),
	NativeConsentDialogListener_t2145450263::get_offset_of_DialogCompleted_7(),
	NativeConsentDialogListener_t2145450263::get_offset_of_DialogDismissed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (iOSConsentDialog_t3621930855), -1, sizeof(iOSConsentDialog_t3621930855_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4969[9] = 
{
	0,
	0,
	iOSConsentDialog_t3621930855_StaticFields::get_offset_of_sInstance_2(),
	iOSConsentDialog_t3621930855::get_offset_of_mListener_3(),
	iOSConsentDialog_t3621930855::get_offset_of_ToggleStateUpdated_4(),
	iOSConsentDialog_t3621930855::get_offset_of_Completed_5(),
	iOSConsentDialog_t3621930855::get_offset_of_Dismissed_6(),
	iOSConsentDialog_t3621930855_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	iOSConsentDialog_t3621930855_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (iOSConsentDialogListenerInfo_t124266453)+ sizeof (RuntimeObject), sizeof(iOSConsentDialogListenerInfo_t124266453_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4970[5] = 
{
	iOSConsentDialogListenerInfo_t124266453::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSConsentDialogListenerInfo_t124266453::get_offset_of_onToggleBecameOnHandler_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSConsentDialogListenerInfo_t124266453::get_offset_of_onToggleBecameOffHandler_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSConsentDialogListenerInfo_t124266453::get_offset_of_onDialogCompletedHandler_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	iOSConsentDialogListenerInfo_t124266453::get_offset_of_onDialogDismissedHandler_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { sizeof (UnsupportedConsentDialog_t239542232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4972[4] = 
{
	0,
	UnsupportedConsentDialog_t239542232::get_offset_of_ToggleStateUpdated_1(),
	UnsupportedConsentDialog_t239542232::get_offset_of_Completed_2(),
	UnsupportedConsentDialog_t239542232::get_offset_of_Dismissed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (Privacy_t1244755513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { sizeof (PrivacySettings_t2445251604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4974[2] = 
{
	PrivacySettings_t2445251604::get_offset_of_mDefaultConsentDialog_0(),
	PrivacySettings_t2445251604::get_offset_of_mConsentDialogComposerSettings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { sizeof (ConsentDialogComposerSettings_t4154825527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4975[3] = 
{
	ConsentDialogComposerSettings_t4154825527::get_offset_of_mToggleSelectedIndex_0(),
	ConsentDialogComposerSettings_t4154825527::get_offset_of_mButtonSelectedIndex_1(),
	ConsentDialogComposerSettings_t4154825527::get_offset_of_mEnableCopyPasteMode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { sizeof (iOSNativeShare_t3881593496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { sizeof (ShareData_t888004449)+ sizeof (RuntimeObject), sizeof(ShareData_t888004449_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4977[4] = 
{
	ShareData_t888004449::get_offset_of_text_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShareData_t888004449::get_offset_of_url_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShareData_t888004449::get_offset_of_image_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShareData_t888004449::get_offset_of_subject_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (Sharing_t4167477152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (iOSNativeUtility_t1924238516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (RatingDialogContent_t752797626), -1, sizeof(RatingDialogContent_t752797626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4980[11] = 
{
	0,
	RatingDialogContent_t752797626_StaticFields::get_offset_of_Default_1(),
	RatingDialogContent_t752797626::get_offset_of_mTitle_2(),
	RatingDialogContent_t752797626::get_offset_of_mMessage_3(),
	RatingDialogContent_t752797626::get_offset_of_mLowRatingMessage_4(),
	RatingDialogContent_t752797626::get_offset_of_mHighRatingMessage_5(),
	RatingDialogContent_t752797626::get_offset_of_mPostponeButtonText_6(),
	RatingDialogContent_t752797626::get_offset_of_mRefuseButtonText_7(),
	RatingDialogContent_t752797626::get_offset_of_mRateButtonText_8(),
	RatingDialogContent_t752797626::get_offset_of_mCancelButtonText_9(),
	RatingDialogContent_t752797626::get_offset_of_mFeedbackButtonText_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { sizeof (RatingRequestSettings_t4213169708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4981[8] = 
{
	RatingRequestSettings_t4213169708::get_offset_of_mDefaultRatingDialogContent_0(),
	RatingRequestSettings_t4213169708::get_offset_of_mMinimumAcceptedStars_1(),
	RatingRequestSettings_t4213169708::get_offset_of_mSupportEmail_2(),
	RatingRequestSettings_t4213169708::get_offset_of_mIosAppId_3(),
	RatingRequestSettings_t4213169708::get_offset_of_mAnnualCap_4(),
	RatingRequestSettings_t4213169708::get_offset_of_mDelayAfterInstallation_5(),
	RatingRequestSettings_t4213169708::get_offset_of_mCoolingOffPeriod_6(),
	RatingRequestSettings_t4213169708::get_offset_of_mIgnoreContraintsInDevelopment_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { sizeof (StoreReview_t4084477087), -1, sizeof(StoreReview_t4084477087_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4982[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	StoreReview_t4084477087_StaticFields::get_offset_of_customBehaviour_10(),
	StoreReview_t4084477087_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (UserAction_t1007852822)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4983[5] = 
{
	UserAction_t1007852822::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (InteropObject_t35158505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4984[1] = 
{
	InteropObject_t35158505::get_offset_of_mSelfPointer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (CTCarrier_t4250217597), -1, sizeof(CTCarrier_t4250217597_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4985[4] = 
{
	CTCarrier_t4250217597_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
	CTCarrier_t4250217597_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_1(),
	CTCarrier_t4250217597_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_2(),
	CTCarrier_t4250217597_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (NSError_t3543635482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { sizeof (NSLocale_t2849095482), -1, sizeof(NSLocale_t2849095482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4987[7] = 
{
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_1(),
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_2(),
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_3(),
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_4(),
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_5(),
	NSLocale_t2849095482_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { sizeof (NSTimeZone_t3876610886), -1, sizeof(NSTimeZone_t3876610886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4988[2] = 
{
	NSTimeZone_t3876610886_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
	NSTimeZone_t3876610886_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (iOSInteropObject_t3130024260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (UIApplication_t1418071823), -1, sizeof(UIApplication_t1418071823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4990[1] = 
{
	UIApplication_t1418071823_StaticFields::get_offset_of_sSharedApplication_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (UIImage_t4033140513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4991[3] = 
{
	UIImage_t4033140513::get_offset_of_mScale_1(),
	UIImage_t4033140513::get_offset_of_mWidth_2(),
	UIImage_t4033140513::get_offset_of_mHeight_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (U3CGetJPEGDataU3Ec__AnonStorey0_t3081864437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4992[2] = 
{
	U3CGetJPEGDataU3Ec__AnonStorey0_t3081864437::get_offset_of_compressionQuality_0(),
	U3CGetJPEGDataU3Ec__AnonStorey0_t3081864437::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (DemoAsteroidScript_t1162572965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (DemoScript_t2141982657), -1, sizeof(DemoScript_t2141982657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4994[18] = 
{
	DemoScript_t2141982657::get_offset_of_Earth_4(),
	DemoScript_t2141982657::get_offset_of_dpiLabel_5(),
	DemoScript_t2141982657::get_offset_of_bottomLabel_6(),
	DemoScript_t2141982657::get_offset_of_AsteroidPrefab_7(),
	DemoScript_t2141982657::get_offset_of_LineMaterial_8(),
	DemoScript_t2141982657::get_offset_of_asteroids_9(),
	DemoScript_t2141982657::get_offset_of_tapGesture_10(),
	DemoScript_t2141982657::get_offset_of_doubleTapGesture_11(),
	DemoScript_t2141982657::get_offset_of_tripleTapGesture_12(),
	DemoScript_t2141982657::get_offset_of_swipeGesture_13(),
	DemoScript_t2141982657::get_offset_of_panGesture_14(),
	DemoScript_t2141982657::get_offset_of_scaleGesture_15(),
	DemoScript_t2141982657::get_offset_of_rotateGesture_16(),
	DemoScript_t2141982657::get_offset_of_longPressGesture_17(),
	DemoScript_t2141982657::get_offset_of_nextAsteroid_18(),
	DemoScript_t2141982657::get_offset_of_draggingAsteroid_19(),
	DemoScript_t2141982657::get_offset_of_swipeLines_20(),
	DemoScript_t2141982657_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (DemoScript3DOrbit_t3440477113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (DemoScriptComponent_t1393959397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4996[2] = 
{
	DemoScriptComponent_t1393959397::get_offset_of_scale_4(),
	DemoScriptComponent_t1393959397::get_offset_of_oneTouchScale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (DemoScriptDPad_t2734679325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4997[5] = 
{
	DemoScriptDPad_t2734679325::get_offset_of_DPadScript_4(),
	DemoScriptDPad_t2734679325::get_offset_of_Mover_5(),
	DemoScriptDPad_t2734679325::get_offset_of_Speed_6(),
	DemoScriptDPad_t2734679325::get_offset_of_MoveDPadToGestureStartLocation_7(),
	DemoScriptDPad_t2734679325::get_offset_of_startPos_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (DemoScriptDragSwipe_t2328868075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4998[6] = 
{
	DemoScriptDragSwipe_t2328868075::get_offset_of_SwipeAwaySpeed_4(),
	DemoScriptDragSwipe_t2328868075::get_offset_of_SwipeVelocityDampening_5(),
	DemoScriptDragSwipe_t2328868075::get_offset_of_longPress_6(),
	DemoScriptDragSwipe_t2328868075::get_offset_of_draggingCard_7(),
	DemoScriptDragSwipe_t2328868075::get_offset_of_dragOffset_8(),
	DemoScriptDragSwipe_t2328868075::get_offset_of_swipedCards_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (DemoScriptDynamicGrid_t430679104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4999[4] = 
{
	DemoScriptDynamicGrid_t430679104::get_offset_of_Rows_4(),
	DemoScriptDynamicGrid_t430679104::get_offset_of_Columns_5(),
	DemoScriptDynamicGrid_t430679104::get_offset_of_grid_6(),
	DemoScriptDynamicGrid_t430679104::get_offset_of_rectTransform_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
