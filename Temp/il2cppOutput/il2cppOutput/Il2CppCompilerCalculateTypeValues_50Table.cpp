﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DigitalRubyShared.FingersImageGestureHelperComponentScript
struct FingersImageGestureHelperComponentScript_t4247928971;
// DigitalRubyShared.FingersJoystickScript
struct FingersJoystickScript_t2468414040;
// DigitalRubyShared.FingersScript
struct FingersScript_t1857011421;
// DigitalRubyShared.FingersZoomPanCameraComponentScript
struct FingersZoomPanCameraComponentScript_t2768970116;
// DigitalRubyShared.GestureRecognizer
struct GestureRecognizer_t3684029681;
// DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate
struct CallbackMainThreadDelegate_t469493312;
// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent
struct GestureRecognizerComponentStateUpdatedEvent_t1428864973;
// DigitalRubyShared.GestureRecognizerStateUpdatedDelegate
struct GestureRecognizerStateUpdatedDelegate_t837748355;
// DigitalRubyShared.GestureRecognizerUpdated
struct GestureRecognizerUpdated_t601711085;
// DigitalRubyShared.GestureVelocityTracker
struct GestureVelocityTracker_t806949657;
// DigitalRubyShared.ImageGestureImage
struct ImageGestureImage_t2885596271;
// DigitalRubyShared.ImageGestureRecognizer
struct ImageGestureRecognizer_t4233185475;
// DigitalRubyShared.LongPressGestureRecognizer
struct LongPressGestureRecognizer_t3980777482;
// DigitalRubyShared.OneTouchRotateGestureRecognizer
struct OneTouchRotateGestureRecognizer_t3272893959;
// DigitalRubyShared.OneTouchScaleGestureRecognizer
struct OneTouchScaleGestureRecognizer_t1313669683;
// DigitalRubyShared.PanGestureRecognizer
struct PanGestureRecognizer_t195762396;
// DigitalRubyShared.RotateGestureRecognizer
struct RotateGestureRecognizer_t4100246528;
// DigitalRubyShared.ScaleGestureRecognizer
struct ScaleGestureRecognizer_t1137887245;
// DigitalRubyShared.SwipeGestureRecognizer
struct SwipeGestureRecognizer_t2328511861;
// DigitalRubyShared.TapGestureRecognizer
struct TapGestureRecognizer_t3178883670;
// DigitalRubyShared.TapGestureRecognizer[]
struct TapGestureRecognizerU5BU5D_t2106712979;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t3064354032;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t1932989778;
// System.Action
struct Action_t1264377477;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t2609895709;
// System.Action`2<DigitalRubyShared.FingersJoystickScript,UnityEngine.Vector2>
struct Action_2_t1565375025;
// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.PanGestureRecognizer>
struct Action_3_t66419768;
// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.TapGestureRecognizer>
struct Action_3_t3049541042;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String>
struct Dictionary_2_t3194170630;
// System.Collections.Generic.Dictionary`2<System.Int32,DigitalRubyShared.FingersMultiDragComponentScript/DragState>
struct Dictionary_2_t2023732987;
// System.Collections.Generic.Dictionary`2<System.Int32,DigitalRubyShared.FingersScript/ShownTouch>
struct Dictionary_2_t629320345;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t1474424692;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector2>
struct Dictionary_2_t1044942854;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,DigitalRubyShared.FingersMultiDragComponentScript/DragState>
struct Dictionary_2_t672118465;
// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer>
struct HashSet_1_t2248979155;
// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureTouch>
struct HashSet_1_t557351607;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t1515895227;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Collections.Generic.ICollection`1<DigitalRubyShared.GestureTouch>
struct ICollection_1_t525587071;
// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer>
struct List_1_t861137127;
// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase>
struct List_1_t1747559692;
// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch>
struct List_1_t3464476875;
// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureImage>
struct List_1_t62703717;
// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point>
struct List_1_t3883198886;
// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry>
struct List_1_t4185350021;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Single>>
struct List_1_t920424597;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point>>
struct List_1_t1060306332;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct List_1_t805411711;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.Queue`1<DigitalRubyShared.GestureVelocityTracker/VelocityHistory>
struct Queue_1_t1111824545;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DigitalRubyShared.GestureTouch>
struct ReadOnlyCollection_1_t3204978420;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Func`2<UnityEngine.EventSystems.RaycastResult,System.String>
struct Func_2_t3794335314;
// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>>
struct Func_2_t1671534078;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.UInt64[]
struct UInt64U5BU5D_t1659327989;
// System.Void
struct Void_t1185182177;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.LineRenderer[]
struct LineRendererU5BU5D_t976293515;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.PlatformEffector2D
struct PlatformEffector2D_t1249518933;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DEVICEINFO_T3428345325_H
#define DEVICEINFO_T3428345325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DeviceInfo
struct  DeviceInfo_t3428345325  : public RuntimeObject
{
public:

public:
};

struct DeviceInfo_t3428345325_StaticFields
{
public:
	// System.Single DigitalRubyShared.DeviceInfo::pixelsPerInch
	float ___pixelsPerInch_0;
	// System.Single DigitalRubyShared.DeviceInfo::unitMultiplier
	float ___unitMultiplier_1;
	// System.Single DigitalRubyShared.DeviceInfo::oneOverUnitMultiplier
	float ___oneOverUnitMultiplier_2;

public:
	inline static int32_t get_offset_of_pixelsPerInch_0() { return static_cast<int32_t>(offsetof(DeviceInfo_t3428345325_StaticFields, ___pixelsPerInch_0)); }
	inline float get_pixelsPerInch_0() const { return ___pixelsPerInch_0; }
	inline float* get_address_of_pixelsPerInch_0() { return &___pixelsPerInch_0; }
	inline void set_pixelsPerInch_0(float value)
	{
		___pixelsPerInch_0 = value;
	}

	inline static int32_t get_offset_of_unitMultiplier_1() { return static_cast<int32_t>(offsetof(DeviceInfo_t3428345325_StaticFields, ___unitMultiplier_1)); }
	inline float get_unitMultiplier_1() const { return ___unitMultiplier_1; }
	inline float* get_address_of_unitMultiplier_1() { return &___unitMultiplier_1; }
	inline void set_unitMultiplier_1(float value)
	{
		___unitMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_oneOverUnitMultiplier_2() { return static_cast<int32_t>(offsetof(DeviceInfo_t3428345325_StaticFields, ___oneOverUnitMultiplier_2)); }
	inline float get_oneOverUnitMultiplier_2() const { return ___oneOverUnitMultiplier_2; }
	inline float* get_address_of_oneOverUnitMultiplier_2() { return &___oneOverUnitMultiplier_2; }
	inline void set_oneOverUnitMultiplier_2(float value)
	{
		___oneOverUnitMultiplier_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEINFO_T3428345325_H
#ifndef U3CSTOPFALLTHROUGHU3EC__ITERATOR0_T1183277613_H
#define U3CSTOPFALLTHROUGHU3EC__ITERATOR0_T1183277613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersPlatformMoveJumpComponentScript/<StopFallThrough>c__Iterator0
struct  U3CStopFallThroughU3Ec__Iterator0_t1183277613  : public RuntimeObject
{
public:
	// UnityEngine.PlatformEffector2D DigitalRubyShared.FingersPlatformMoveJumpComponentScript/<StopFallThrough>c__Iterator0::effector
	PlatformEffector2D_t1249518933 * ___effector_0;
	// System.Object DigitalRubyShared.FingersPlatformMoveJumpComponentScript/<StopFallThrough>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean DigitalRubyShared.FingersPlatformMoveJumpComponentScript/<StopFallThrough>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 DigitalRubyShared.FingersPlatformMoveJumpComponentScript/<StopFallThrough>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_effector_0() { return static_cast<int32_t>(offsetof(U3CStopFallThroughU3Ec__Iterator0_t1183277613, ___effector_0)); }
	inline PlatformEffector2D_t1249518933 * get_effector_0() const { return ___effector_0; }
	inline PlatformEffector2D_t1249518933 ** get_address_of_effector_0() { return &___effector_0; }
	inline void set_effector_0(PlatformEffector2D_t1249518933 * value)
	{
		___effector_0 = value;
		Il2CppCodeGenWriteBarrier((&___effector_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStopFallThroughU3Ec__Iterator0_t1183277613, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStopFallThroughU3Ec__Iterator0_t1183277613, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStopFallThroughU3Ec__Iterator0_t1183277613, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPFALLTHROUGHU3EC__ITERATOR0_T1183277613_H
#ifndef U3CMAINTHREADCALLBACKU3EC__ITERATOR0_T2673350864_H
#define U3CMAINTHREADCALLBACKU3EC__ITERATOR0_T2673350864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0
struct  U3CMainThreadCallbackU3Ec__Iterator0_t2673350864  : public RuntimeObject
{
public:
	// System.Action DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::action
	Action_t1264377477 * ___action_0;
	// System.Diagnostics.Stopwatch DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::<timer>__1
	Stopwatch_t305734070 * ___U3CtimerU3E__1_1;
	// System.Single DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::delay
	float ___delay_2;
	// System.Object DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 DigitalRubyShared.FingersScript/<MainThreadCallback>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___action_0)); }
	inline Action_t1264377477 * get_action_0() const { return ___action_0; }
	inline Action_t1264377477 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t1264377477 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U3CtimerU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U3CtimerU3E__1_1)); }
	inline Stopwatch_t305734070 * get_U3CtimerU3E__1_1() const { return ___U3CtimerU3E__1_1; }
	inline Stopwatch_t305734070 ** get_address_of_U3CtimerU3E__1_1() { return &___U3CtimerU3E__1_1; }
	inline void set_U3CtimerU3E__1_1(Stopwatch_t305734070 * value)
	{
		___U3CtimerU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtimerU3E__1_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CMainThreadCallbackU3Ec__Iterator0_t2673350864, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAINTHREADCALLBACKU3EC__ITERATOR0_T2673350864_H
#ifndef GESTURELOGGER_T3995736004_H
#define GESTURELOGGER_T3995736004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureLogger
struct  GestureLogger_t3995736004  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURELOGGER_T3995736004_H
#ifndef GESTUREVELOCITYTRACKER_T806949657_H
#define GESTUREVELOCITYTRACKER_T806949657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureVelocityTracker
struct  GestureVelocityTracker_t806949657  : public RuntimeObject
{
public:
	// System.Collections.Generic.Queue`1<DigitalRubyShared.GestureVelocityTracker/VelocityHistory> DigitalRubyShared.GestureVelocityTracker::history
	Queue_1_t1111824545 * ___history_1;
	// System.Diagnostics.Stopwatch DigitalRubyShared.GestureVelocityTracker::timer
	Stopwatch_t305734070 * ___timer_2;
	// System.Single DigitalRubyShared.GestureVelocityTracker::previousX
	float ___previousX_3;
	// System.Single DigitalRubyShared.GestureVelocityTracker::previousY
	float ___previousY_4;
	// System.Single DigitalRubyShared.GestureVelocityTracker::<VelocityX>k__BackingField
	float ___U3CVelocityXU3Ek__BackingField_5;
	// System.Single DigitalRubyShared.GestureVelocityTracker::<VelocityY>k__BackingField
	float ___U3CVelocityYU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_history_1() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___history_1)); }
	inline Queue_1_t1111824545 * get_history_1() const { return ___history_1; }
	inline Queue_1_t1111824545 ** get_address_of_history_1() { return &___history_1; }
	inline void set_history_1(Queue_1_t1111824545 * value)
	{
		___history_1 = value;
		Il2CppCodeGenWriteBarrier((&___history_1), value);
	}

	inline static int32_t get_offset_of_timer_2() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___timer_2)); }
	inline Stopwatch_t305734070 * get_timer_2() const { return ___timer_2; }
	inline Stopwatch_t305734070 ** get_address_of_timer_2() { return &___timer_2; }
	inline void set_timer_2(Stopwatch_t305734070 * value)
	{
		___timer_2 = value;
		Il2CppCodeGenWriteBarrier((&___timer_2), value);
	}

	inline static int32_t get_offset_of_previousX_3() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___previousX_3)); }
	inline float get_previousX_3() const { return ___previousX_3; }
	inline float* get_address_of_previousX_3() { return &___previousX_3; }
	inline void set_previousX_3(float value)
	{
		___previousX_3 = value;
	}

	inline static int32_t get_offset_of_previousY_4() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___previousY_4)); }
	inline float get_previousY_4() const { return ___previousY_4; }
	inline float* get_address_of_previousY_4() { return &___previousY_4; }
	inline void set_previousY_4(float value)
	{
		___previousY_4 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityXU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___U3CVelocityXU3Ek__BackingField_5)); }
	inline float get_U3CVelocityXU3Ek__BackingField_5() const { return ___U3CVelocityXU3Ek__BackingField_5; }
	inline float* get_address_of_U3CVelocityXU3Ek__BackingField_5() { return &___U3CVelocityXU3Ek__BackingField_5; }
	inline void set_U3CVelocityXU3Ek__BackingField_5(float value)
	{
		___U3CVelocityXU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityYU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GestureVelocityTracker_t806949657, ___U3CVelocityYU3Ek__BackingField_6)); }
	inline float get_U3CVelocityYU3Ek__BackingField_6() const { return ___U3CVelocityYU3Ek__BackingField_6; }
	inline float* get_address_of_U3CVelocityYU3Ek__BackingField_6() { return &___U3CVelocityYU3Ek__BackingField_6; }
	inline void set_U3CVelocityYU3Ek__BackingField_6(float value)
	{
		___U3CVelocityYU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREVELOCITYTRACKER_T806949657_H
#ifndef IMAGEGESTUREIMAGE_T2885596271_H
#define IMAGEGESTUREIMAGE_T2885596271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureImage
struct  ImageGestureImage_t2885596271  : public RuntimeObject
{
public:
	// System.Int32 DigitalRubyShared.ImageGestureImage::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_8;
	// System.Int32 DigitalRubyShared.ImageGestureImage::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_9;
	// System.Int32 DigitalRubyShared.ImageGestureImage::<Size>k__BackingField
	int32_t ___U3CSizeU3Ek__BackingField_10;
	// System.UInt64[] DigitalRubyShared.ImageGestureImage::<Rows>k__BackingField
	UInt64U5BU5D_t1659327989* ___U3CRowsU3Ek__BackingField_11;
	// System.Byte[] DigitalRubyShared.ImageGestureImage::<Pixels>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CPixelsU3Ek__BackingField_12;
	// System.Single DigitalRubyShared.ImageGestureImage::<SimilarityPadding>k__BackingField
	float ___U3CSimilarityPaddingU3Ek__BackingField_13;
	// System.Single DigitalRubyShared.ImageGestureImage::<Score>k__BackingField
	float ___U3CScoreU3Ek__BackingField_14;
	// System.String DigitalRubyShared.ImageGestureImage::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CWidthU3Ek__BackingField_8)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_8() const { return ___U3CWidthU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_8() { return &___U3CWidthU3Ek__BackingField_8; }
	inline void set_U3CWidthU3Ek__BackingField_8(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CHeightU3Ek__BackingField_9)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_9() const { return ___U3CHeightU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_9() { return &___U3CHeightU3Ek__BackingField_9; }
	inline void set_U3CHeightU3Ek__BackingField_9(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CSizeU3Ek__BackingField_10)); }
	inline int32_t get_U3CSizeU3Ek__BackingField_10() const { return ___U3CSizeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CSizeU3Ek__BackingField_10() { return &___U3CSizeU3Ek__BackingField_10; }
	inline void set_U3CSizeU3Ek__BackingField_10(int32_t value)
	{
		___U3CSizeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRowsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CRowsU3Ek__BackingField_11)); }
	inline UInt64U5BU5D_t1659327989* get_U3CRowsU3Ek__BackingField_11() const { return ___U3CRowsU3Ek__BackingField_11; }
	inline UInt64U5BU5D_t1659327989** get_address_of_U3CRowsU3Ek__BackingField_11() { return &___U3CRowsU3Ek__BackingField_11; }
	inline void set_U3CRowsU3Ek__BackingField_11(UInt64U5BU5D_t1659327989* value)
	{
		___U3CRowsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowsU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CPixelsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CPixelsU3Ek__BackingField_12)); }
	inline ByteU5BU5D_t4116647657* get_U3CPixelsU3Ek__BackingField_12() const { return ___U3CPixelsU3Ek__BackingField_12; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CPixelsU3Ek__BackingField_12() { return &___U3CPixelsU3Ek__BackingField_12; }
	inline void set_U3CPixelsU3Ek__BackingField_12(ByteU5BU5D_t4116647657* value)
	{
		___U3CPixelsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPixelsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CSimilarityPaddingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CSimilarityPaddingU3Ek__BackingField_13)); }
	inline float get_U3CSimilarityPaddingU3Ek__BackingField_13() const { return ___U3CSimilarityPaddingU3Ek__BackingField_13; }
	inline float* get_address_of_U3CSimilarityPaddingU3Ek__BackingField_13() { return &___U3CSimilarityPaddingU3Ek__BackingField_13; }
	inline void set_U3CSimilarityPaddingU3Ek__BackingField_13(float value)
	{
		___U3CSimilarityPaddingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CScoreU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CScoreU3Ek__BackingField_14)); }
	inline float get_U3CScoreU3Ek__BackingField_14() const { return ___U3CScoreU3Ek__BackingField_14; }
	inline float* get_address_of_U3CScoreU3Ek__BackingField_14() { return &___U3CScoreU3Ek__BackingField_14; }
	inline void set_U3CScoreU3Ek__BackingField_14(float value)
	{
		___U3CScoreU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ImageGestureImage_t2885596271, ___U3CNameU3Ek__BackingField_15)); }
	inline String_t* get_U3CNameU3Ek__BackingField_15() const { return ___U3CNameU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_15() { return &___U3CNameU3Ek__BackingField_15; }
	inline void set_U3CNameU3Ek__BackingField_15(String_t* value)
	{
		___U3CNameU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEGESTUREIMAGE_T2885596271_H
#ifndef COMMONTYPESUTIL_T3521372089_H
#define COMMONTYPESUTIL_T3521372089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.CommonTypesUtil
struct  CommonTypesUtil_t3521372089  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONTYPESUTIL_T3521372089_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef SHOWNTOUCH_T1740607014_H
#define SHOWNTOUCH_T1740607014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/ShownTouch
struct  ShownTouch_t1740607014 
{
public:
	// UnityEngine.GameObject DigitalRubyShared.FingersScript/ShownTouch::<GameObject>k__BackingField
	GameObject_t1113636619 * ___U3CGameObjectU3Ek__BackingField_0;
	// System.Single DigitalRubyShared.FingersScript/ShownTouch::<Timestamp>k__BackingField
	float ___U3CTimestampU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShownTouch_t1740607014, ___U3CGameObjectU3Ek__BackingField_0)); }
	inline GameObject_t1113636619 * get_U3CGameObjectU3Ek__BackingField_0() const { return ___U3CGameObjectU3Ek__BackingField_0; }
	inline GameObject_t1113636619 ** get_address_of_U3CGameObjectU3Ek__BackingField_0() { return &___U3CGameObjectU3Ek__BackingField_0; }
	inline void set_U3CGameObjectU3Ek__BackingField_0(GameObject_t1113636619 * value)
	{
		___U3CGameObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShownTouch_t1740607014, ___U3CTimestampU3Ek__BackingField_1)); }
	inline float get_U3CTimestampU3Ek__BackingField_1() const { return ___U3CTimestampU3Ek__BackingField_1; }
	inline float* get_address_of_U3CTimestampU3Ek__BackingField_1() { return &___U3CTimestampU3Ek__BackingField_1; }
	inline void set_U3CTimestampU3Ek__BackingField_1(float value)
	{
		___U3CTimestampU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DigitalRubyShared.FingersScript/ShownTouch
struct ShownTouch_t1740607014_marshaled_pinvoke
{
	GameObject_t1113636619 * ___U3CGameObjectU3Ek__BackingField_0;
	float ___U3CTimestampU3Ek__BackingField_1;
};
// Native definition for COM marshalling of DigitalRubyShared.FingersScript/ShownTouch
struct ShownTouch_t1740607014_marshaled_com
{
	GameObject_t1113636619 * ___U3CGameObjectU3Ek__BackingField_0;
	float ___U3CTimestampU3Ek__BackingField_1;
};
#endif // SHOWNTOUCH_T1740607014_H
#ifndef VELOCITYHISTORY_T1265565051_H
#define VELOCITYHISTORY_T1265565051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureVelocityTracker/VelocityHistory
struct  VelocityHistory_t1265565051 
{
public:
	// System.Single DigitalRubyShared.GestureVelocityTracker/VelocityHistory::VelocityX
	float ___VelocityX_0;
	// System.Single DigitalRubyShared.GestureVelocityTracker/VelocityHistory::VelocityY
	float ___VelocityY_1;
	// System.Single DigitalRubyShared.GestureVelocityTracker/VelocityHistory::Seconds
	float ___Seconds_2;

public:
	inline static int32_t get_offset_of_VelocityX_0() { return static_cast<int32_t>(offsetof(VelocityHistory_t1265565051, ___VelocityX_0)); }
	inline float get_VelocityX_0() const { return ___VelocityX_0; }
	inline float* get_address_of_VelocityX_0() { return &___VelocityX_0; }
	inline void set_VelocityX_0(float value)
	{
		___VelocityX_0 = value;
	}

	inline static int32_t get_offset_of_VelocityY_1() { return static_cast<int32_t>(offsetof(VelocityHistory_t1265565051, ___VelocityY_1)); }
	inline float get_VelocityY_1() const { return ___VelocityY_1; }
	inline float* get_address_of_VelocityY_1() { return &___VelocityY_1; }
	inline void set_VelocityY_1(float value)
	{
		___VelocityY_1 = value;
	}

	inline static int32_t get_offset_of_Seconds_2() { return static_cast<int32_t>(offsetof(VelocityHistory_t1265565051, ___Seconds_2)); }
	inline float get_Seconds_2() const { return ___Seconds_2; }
	inline float* get_address_of_Seconds_2() { return &___Seconds_2; }
	inline void set_Seconds_2(float value)
	{
		___Seconds_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VELOCITYHISTORY_T1265565051_H
#ifndef POINT_T2411124144_H
#define POINT_T2411124144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureRecognizer/Point
struct  Point_t2411124144 
{
public:
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer/Point::X
	int32_t ___X_0;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer/Point::Y
	int32_t ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(Point_t2411124144, ___X_0)); }
	inline int32_t get_X_0() const { return ___X_0; }
	inline int32_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int32_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(Point_t2411124144, ___Y_1)); }
	inline int32_t get_Y_1() const { return ___Y_1; }
	inline int32_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int32_t value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T2411124144_H
#ifndef IMAGEGESTURERECOGNIZERCOMPONENTSCRIPTIMAGEENTRY_T2713275279_H
#define IMAGEGESTURERECOGNIZERCOMPONENTSCRIPTIMAGEENTRY_T2713275279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry
struct  ImageGestureRecognizerComponentScriptImageEntry_t2713275279 
{
public:
	// System.String DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry::Key
	String_t* ___Key_0;
	// System.Single DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry::ScorePadding
	float ___ScorePadding_1;
	// System.String DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry::Images
	String_t* ___Images_2;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScriptImageEntry_t2713275279, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_ScorePadding_1() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScriptImageEntry_t2713275279, ___ScorePadding_1)); }
	inline float get_ScorePadding_1() const { return ___ScorePadding_1; }
	inline float* get_address_of_ScorePadding_1() { return &___ScorePadding_1; }
	inline void set_ScorePadding_1(float value)
	{
		___ScorePadding_1 = value;
	}

	inline static int32_t get_offset_of_Images_2() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScriptImageEntry_t2713275279, ___Images_2)); }
	inline String_t* get_Images_2() const { return ___Images_2; }
	inline String_t** get_address_of_Images_2() { return &___Images_2; }
	inline void set_Images_2(String_t* value)
	{
		___Images_2 = value;
		Il2CppCodeGenWriteBarrier((&___Images_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry
struct ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshaled_pinvoke
{
	char* ___Key_0;
	float ___ScorePadding_1;
	char* ___Images_2;
};
// Native definition for COM marshalling of DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry
struct ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshaled_com
{
	Il2CppChar* ___Key_0;
	float ___ScorePadding_1;
	Il2CppChar* ___Images_2;
};
#endif // IMAGEGESTURERECOGNIZERCOMPONENTSCRIPTIMAGEENTRY_T2713275279_H
#ifndef ENDPOINTDETAILS_T3891698496_H
#define ENDPOINTDETAILS_T3891698496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct  EndpointDetails_t3891698496 
{
public:
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mEndpointId
	String_t* ___mEndpointId_0;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mName
	String_t* ___mName_1;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mServiceId
	String_t* ___mServiceId_2;

public:
	inline static int32_t get_offset_of_mEndpointId_0() { return static_cast<int32_t>(offsetof(EndpointDetails_t3891698496, ___mEndpointId_0)); }
	inline String_t* get_mEndpointId_0() const { return ___mEndpointId_0; }
	inline String_t** get_address_of_mEndpointId_0() { return &___mEndpointId_0; }
	inline void set_mEndpointId_0(String_t* value)
	{
		___mEndpointId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mEndpointId_0), value);
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(EndpointDetails_t3891698496, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}

	inline static int32_t get_offset_of_mServiceId_2() { return static_cast<int32_t>(offsetof(EndpointDetails_t3891698496, ___mServiceId_2)); }
	inline String_t* get_mServiceId_2() const { return ___mServiceId_2; }
	inline String_t** get_address_of_mServiceId_2() { return &___mServiceId_2; }
	inline void set_mServiceId_2(String_t* value)
	{
		___mServiceId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mServiceId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_t3891698496_marshaled_pinvoke
{
	char* ___mEndpointId_0;
	char* ___mName_1;
	char* ___mServiceId_2;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_t3891698496_marshaled_com
{
	Il2CppChar* ___mEndpointId_0;
	Il2CppChar* ___mName_1;
	Il2CppChar* ___mServiceId_2;
};
#endif // ENDPOINTDETAILS_T3891698496_H
#ifndef NEARBYCONNECTIONCONFIGURATION_T2019425596_H
#define NEARBYCONNECTIONCONFIGURATION_T2019425596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct  NearbyConnectionConfiguration_t2019425596 
{
public:
	// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::mInitializationCallback
	Action_1_t2609895709 * ___mInitializationCallback_2;
	// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::mLocalClientId
	int64_t ___mLocalClientId_3;

public:
	inline static int32_t get_offset_of_mInitializationCallback_2() { return static_cast<int32_t>(offsetof(NearbyConnectionConfiguration_t2019425596, ___mInitializationCallback_2)); }
	inline Action_1_t2609895709 * get_mInitializationCallback_2() const { return ___mInitializationCallback_2; }
	inline Action_1_t2609895709 ** get_address_of_mInitializationCallback_2() { return &___mInitializationCallback_2; }
	inline void set_mInitializationCallback_2(Action_1_t2609895709 * value)
	{
		___mInitializationCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInitializationCallback_2), value);
	}

	inline static int32_t get_offset_of_mLocalClientId_3() { return static_cast<int32_t>(offsetof(NearbyConnectionConfiguration_t2019425596, ___mLocalClientId_3)); }
	inline int64_t get_mLocalClientId_3() const { return ___mLocalClientId_3; }
	inline int64_t* get_address_of_mLocalClientId_3() { return &___mLocalClientId_3; }
	inline void set_mLocalClientId_3(int64_t value)
	{
		___mLocalClientId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t2019425596_marshaled_pinvoke
{
	Il2CppMethodPointer ___mInitializationCallback_2;
	int64_t ___mLocalClientId_3;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t2019425596_marshaled_com
{
	Il2CppMethodPointer ___mInitializationCallback_2;
	int64_t ___mLocalClientId_3;
};
#endif // NEARBYCONNECTIONCONFIGURATION_T2019425596_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_1_T270721889_H
#define UNITYEVENT_1_T270721889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<DigitalRubyShared.GestureRecognizer>
struct  UnityEvent_1_t270721889  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t270721889, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T270721889_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef FINGERSDPADITEM_T4100703432_H
#define FINGERSDPADITEM_T4100703432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersDPadItem
struct  FingersDPadItem_t4100703432 
{
public:
	// System.Int32 DigitalRubyShared.FingersDPadItem::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FingersDPadItem_t4100703432, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSDPADITEM_T4100703432_H
#ifndef DRAGSTATE_T3135019656_H
#define DRAGSTATE_T3135019656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersMultiDragComponentScript/DragState
struct  DragState_t3135019656  : public RuntimeObject
{
public:
	// UnityEngine.GameObject DigitalRubyShared.FingersMultiDragComponentScript/DragState::GameObject
	GameObject_t1113636619 * ___GameObject_0;
	// UnityEngine.Vector2 DigitalRubyShared.FingersMultiDragComponentScript/DragState::Offset
	Vector2_t2156229523  ___Offset_1;

public:
	inline static int32_t get_offset_of_GameObject_0() { return static_cast<int32_t>(offsetof(DragState_t3135019656, ___GameObject_0)); }
	inline GameObject_t1113636619 * get_GameObject_0() const { return ___GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_GameObject_0() { return &___GameObject_0; }
	inline void set_GameObject_0(GameObject_t1113636619 * value)
	{
		___GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(DragState_t3135019656, ___Offset_1)); }
	inline Vector2_t2156229523  get_Offset_1() const { return ___Offset_1; }
	inline Vector2_t2156229523 * get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(Vector2_t2156229523  value)
	{
		___Offset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGSTATE_T3135019656_H
#ifndef PANORBITMOVEMENTTYPE_T3736241033_H
#define PANORBITMOVEMENTTYPE_T3736241033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersPanOrbitComponentScript/PanOrbitMovementType
struct  PanOrbitMovementType_t3736241033 
{
public:
	// System.Int32 DigitalRubyShared.FingersPanOrbitComponentScript/PanOrbitMovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PanOrbitMovementType_t3736241033, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANORBITMOVEMENTTYPE_T3736241033_H
#ifndef CAPTURERESULT_T3073219507_H
#define CAPTURERESULT_T3073219507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/CaptureResult
struct  CaptureResult_t3073219507 
{
public:
	// System.Int32 DigitalRubyShared.FingersScript/CaptureResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CaptureResult_t3073219507, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURERESULT_T3073219507_H
#ifndef GESTURELEVELUNLOADOPTION_T11098870_H
#define GESTURELEVELUNLOADOPTION_T11098870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/GestureLevelUnloadOption
struct  GestureLevelUnloadOption_t11098870 
{
public:
	// System.Int32 DigitalRubyShared.FingersScript/GestureLevelUnloadOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureLevelUnloadOption_t11098870, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURELEVELUNLOADOPTION_T11098870_H
#ifndef U3CANIMATIONCOROUTINEU3EC__ITERATOR0_T3218050250_H
#define U3CANIMATIONCOROUTINEU3EC__ITERATOR0_T3218050250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0
struct  U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0::<start>__0
	Vector3_t3722313464  ___U3CstartU3E__0_0;
	// System.Single DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0::<accumTime>__1
	float ___U3CaccumTimeU3E__1_1;
	// DigitalRubyShared.FingersZoomPanCameraComponentScript DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0::$this
	FingersZoomPanCameraComponentScript_t2768970116 * ___U24this_2;
	// System.Object DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 DigitalRubyShared.FingersZoomPanCameraComponentScript/<AnimationCoRoutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CstartU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250, ___U3CstartU3E__0_0)); }
	inline Vector3_t3722313464  get_U3CstartU3E__0_0() const { return ___U3CstartU3E__0_0; }
	inline Vector3_t3722313464 * get_address_of_U3CstartU3E__0_0() { return &___U3CstartU3E__0_0; }
	inline void set_U3CstartU3E__0_0(Vector3_t3722313464  value)
	{
		___U3CstartU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CaccumTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250, ___U3CaccumTimeU3E__1_1)); }
	inline float get_U3CaccumTimeU3E__1_1() const { return ___U3CaccumTimeU3E__1_1; }
	inline float* get_address_of_U3CaccumTimeU3E__1_1() { return &___U3CaccumTimeU3E__1_1; }
	inline void set_U3CaccumTimeU3E__1_1(float value)
	{
		___U3CaccumTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250, ___U24this_2)); }
	inline FingersZoomPanCameraComponentScript_t2768970116 * get_U24this_2() const { return ___U24this_2; }
	inline FingersZoomPanCameraComponentScript_t2768970116 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(FingersZoomPanCameraComponentScript_t2768970116 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATIONCOROUTINEU3EC__ITERATOR0_T3218050250_H
#ifndef GESTUREOBJECTMODE_T1423348675_H
#define GESTUREOBJECTMODE_T1423348675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScriptBase/GestureObjectMode
struct  GestureObjectMode_t1423348675 
{
public:
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScriptBase/GestureObjectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureObjectMode_t1423348675, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREOBJECTMODE_T1423348675_H
#ifndef GESTURERECOGNIZERCOMPONENTSTATEUPDATEDEVENT_T1428864973_H
#define GESTURERECOGNIZERCOMPONENTSTATEUPDATEDEVENT_T1428864973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent
struct  GestureRecognizerComponentStateUpdatedEvent_t1428864973  : public UnityEvent_1_t270721889
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSTATEUPDATEDEVENT_T1428864973_H
#ifndef GESTURERECOGNIZERSTATE_T650110565_H
#define GESTURERECOGNIZERSTATE_T650110565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerState
struct  GestureRecognizerState_t650110565 
{
public:
	// System.Int32 DigitalRubyShared.GestureRecognizerState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GestureRecognizerState_t650110565, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERSTATE_T650110565_H
#ifndef SWIPEGESTURERECOGNIZERDIRECTION_T3225897881_H
#define SWIPEGESTURERECOGNIZERDIRECTION_T3225897881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.SwipeGestureRecognizerDirection
struct  SwipeGestureRecognizerDirection_t3225897881 
{
public:
	// System.Int32 DigitalRubyShared.SwipeGestureRecognizerDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerDirection_t3225897881, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTURERECOGNIZERDIRECTION_T3225897881_H
#ifndef SWIPEGESTURERECOGNIZERENDMODE_T3668288691_H
#define SWIPEGESTURERECOGNIZERENDMODE_T3668288691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.SwipeGestureRecognizerEndMode
struct  SwipeGestureRecognizerEndMode_t3668288691 
{
public:
	// System.Int32 DigitalRubyShared.SwipeGestureRecognizerEndMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerEndMode_t3668288691, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTURERECOGNIZERENDMODE_T3668288691_H
#ifndef TOUCHPHASE_T3693698977_H
#define TOUCHPHASE_T3693698977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.TouchPhase
struct  TouchPhase_t3693698977 
{
public:
	// System.Int32 DigitalRubyShared.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_t3693698977, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T3693698977_H
#ifndef DATASOURCE_T833220627_H
#define DATASOURCE_T833220627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.DataSource
struct  DataSource_t833220627 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.DataSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataSource_t833220627, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASOURCE_T833220627_H
#ifndef EVENTVISIBILITY_T3702936362_H
#define EVENTVISIBILITY_T3702936362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Events.EventVisibility
struct  EventVisibility_t3702936362 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Events.EventVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventVisibility_t3702936362, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTVISIBILITY_T3702936362_H
#ifndef GRAVITY_T1500868723_H
#define GRAVITY_T1500868723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Gravity
struct  Gravity_t1500868723 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Gravity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Gravity_t1500868723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAVITY_T1500868723_H
#ifndef LEADERBOARDCOLLECTION_T3003544407_H
#define LEADERBOARDCOLLECTION_T3003544407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.LeaderboardCollection
struct  LeaderboardCollection_t3003544407 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardCollection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeaderboardCollection_t3003544407, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDCOLLECTION_T3003544407_H
#ifndef LEADERBOARDSTART_T3259090716_H
#define LEADERBOARDSTART_T3259090716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.LeaderboardStart
struct  LeaderboardStart_t3259090716 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardStart::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeaderboardStart_t3259090716, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDSTART_T3259090716_H
#ifndef LEADERBOARDTIMESPAN_T1503936786_H
#define LEADERBOARDTIMESPAN_T1503936786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.LeaderboardTimeSpan
struct  LeaderboardTimeSpan_t1503936786 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardTimeSpan::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeaderboardTimeSpan_t1503936786, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDTIMESPAN_T1503936786_H
#ifndef CONNECTIONREQUEST_T684574500_H
#define CONNECTIONREQUEST_T684574500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct  ConnectionRequest_t684574500 
{
public:
	// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mRemoteEndpoint
	EndpointDetails_t3891698496  ___mRemoteEndpoint_0;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mPayload
	ByteU5BU5D_t4116647657* ___mPayload_1;

public:
	inline static int32_t get_offset_of_mRemoteEndpoint_0() { return static_cast<int32_t>(offsetof(ConnectionRequest_t684574500, ___mRemoteEndpoint_0)); }
	inline EndpointDetails_t3891698496  get_mRemoteEndpoint_0() const { return ___mRemoteEndpoint_0; }
	inline EndpointDetails_t3891698496 * get_address_of_mRemoteEndpoint_0() { return &___mRemoteEndpoint_0; }
	inline void set_mRemoteEndpoint_0(EndpointDetails_t3891698496  value)
	{
		___mRemoteEndpoint_0 = value;
	}

	inline static int32_t get_offset_of_mPayload_1() { return static_cast<int32_t>(offsetof(ConnectionRequest_t684574500, ___mPayload_1)); }
	inline ByteU5BU5D_t4116647657* get_mPayload_1() const { return ___mPayload_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_mPayload_1() { return &___mPayload_1; }
	inline void set_mPayload_1(ByteU5BU5D_t4116647657* value)
	{
		___mPayload_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPayload_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_t684574500_marshaled_pinvoke
{
	EndpointDetails_t3891698496_marshaled_pinvoke ___mRemoteEndpoint_0;
	uint8_t* ___mPayload_1;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_t684574500_marshaled_com
{
	EndpointDetails_t3891698496_marshaled_com ___mRemoteEndpoint_0;
	uint8_t* ___mPayload_1;
};
#endif // CONNECTIONREQUEST_T684574500_H
#ifndef STATUS_T465938950_H
#define STATUS_T465938950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
struct  Status_t465938950 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t465938950, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T465938950_H
#ifndef INITIALIZATIONSTATUS_T2437428114_H
#define INITIALIZATIONSTATUS_T2437428114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.InitializationStatus
struct  InitializationStatus_t2437428114 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Nearby.InitializationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializationStatus_t2437428114, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZATIONSTATUS_T2437428114_H
#ifndef RESPONSESTATUS_T4073530167_H
#define RESPONSESTATUS_T4073530167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.ResponseStatus
struct  ResponseStatus_t4073530167 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.ResponseStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResponseStatus_t4073530167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSESTATUS_T4073530167_H
#ifndef CONFLICTRESOLUTIONSTRATEGY_T4255039905_H
#define CONFLICTRESOLUTIONSTRATEGY_T4255039905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
struct  ConflictResolutionStrategy_t4255039905 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConflictResolutionStrategy_t4255039905, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTRESOLUTIONSTRATEGY_T4255039905_H
#ifndef SAVEDGAMEREQUESTSTATUS_T3745141777_H
#define SAVEDGAMEREQUESTSTATUS_T3745141777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
struct  SavedGameRequestStatus_t3745141777 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SavedGameRequestStatus_t3745141777, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDGAMEREQUESTSTATUS_T3745141777_H
#ifndef SELECTUISTATUS_T1293076877_H
#define SELECTUISTATUS_T1293076877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
struct  SelectUIStatus_t1293076877 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.SelectUIStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectUIStatus_t1293076877, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTUISTATUS_T1293076877_H
#ifndef UISTATUS_T582920877_H
#define UISTATUS_T582920877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.UIStatus
struct  UIStatus_t582920877 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.UIStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIStatus_t582920877, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTATUS_T582920877_H
#ifndef VIDEOCAPTUREMODE_T1984088482_H
#define VIDEOCAPTUREMODE_T1984088482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoCaptureMode
struct  VideoCaptureMode_t1984088482 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoCaptureMode_t1984088482, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTUREMODE_T1984088482_H
#ifndef VIDEOCAPTUREOVERLAYSTATE_T4111180056_H
#define VIDEOCAPTUREOVERLAYSTATE_T4111180056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoCaptureOverlayState
struct  VideoCaptureOverlayState_t4111180056 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureOverlayState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoCaptureOverlayState_t4111180056, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTUREOVERLAYSTATE_T4111180056_H
#ifndef VIDEOQUALITYLEVEL_T4283418091_H
#define VIDEOQUALITYLEVEL_T4283418091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoQualityLevel
struct  VideoQualityLevel_t4283418091 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoQualityLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoQualityLevel_t4283418091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOQUALITYLEVEL_T4283418091_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CPOPULATEGAMEOBJECTSFORTOUCHU3EC__ANONSTOREY1_T2702075252_H
#define U3CPOPULATEGAMEOBJECTSFORTOUCHU3EC__ANONSTOREY1_T2702075252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript/<PopulateGameObjectsForTouch>c__AnonStorey1
struct  U3CPopulateGameObjectsForTouchU3Ec__AnonStorey1_t2702075252  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.RaycastResult DigitalRubyShared.FingersScript/<PopulateGameObjectsForTouch>c__AnonStorey1::result
	RaycastResult_t3360306849  ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CPopulateGameObjectsForTouchU3Ec__AnonStorey1_t2702075252, ___result_0)); }
	inline RaycastResult_t3360306849  get_result_0() const { return ___result_0; }
	inline RaycastResult_t3360306849 * get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(RaycastResult_t3360306849  value)
	{
		___result_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOPULATEGAMEOBJECTSFORTOUCHU3EC__ANONSTOREY1_T2702075252_H
#ifndef GESTURERECOGNIZER_T3684029681_H
#define GESTURERECOGNIZER_T3684029681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizer
struct  GestureRecognizer_t3684029681  : public RuntimeObject
{
public:
	// DigitalRubyShared.GestureRecognizerState DigitalRubyShared.GestureRecognizer::state
	int32_t ___state_1;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.GestureRecognizer::currentTrackedTouches
	List_1_t3464476875 * ___currentTrackedTouches_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.GestureRecognizer::currentTrackedTouchesReadOnly
	ReadOnlyCollection_1_t3204978420 * ___currentTrackedTouchesReadOnly_3;
	// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::requireGestureRecognizersToFail
	HashSet_1_t2248979155 * ___requireGestureRecognizersToFail_4;
	// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::failGestures
	HashSet_1_t2248979155 * ___failGestures_5;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::simultaneousGestures
	List_1_t861137127 * ___simultaneousGestures_6;
	// DigitalRubyShared.GestureVelocityTracker DigitalRubyShared.GestureRecognizer::velocityTracker
	GestureVelocityTracker_t806949657 * ___velocityTracker_7;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Single>> DigitalRubyShared.GestureRecognizer::touchStartLocations
	List_1_t920424597 * ___touchStartLocations_8;
	// System.Collections.Generic.HashSet`1<System.Int32> DigitalRubyShared.GestureRecognizer::ignoreTouchIds
	HashSet_1_t1515895227 * ___ignoreTouchIds_9;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.GestureRecognizer::tempTouches
	List_1_t3464476875 * ___tempTouches_10;
	// System.Int32 DigitalRubyShared.GestureRecognizer::minimumNumberOfTouchesToTrack
	int32_t ___minimumNumberOfTouchesToTrack_11;
	// System.Int32 DigitalRubyShared.GestureRecognizer::maximumNumberOfTouchesToTrack
	int32_t ___maximumNumberOfTouchesToTrack_12;
	// System.Boolean DigitalRubyShared.GestureRecognizer::justFailed
	bool ___justFailed_13;
	// System.Boolean DigitalRubyShared.GestureRecognizer::justEnded
	bool ___justEnded_14;
	// System.Boolean DigitalRubyShared.GestureRecognizer::isRestarting
	bool ___isRestarting_15;
	// System.Int32 DigitalRubyShared.GestureRecognizer::lastTrackTouchCount
	int32_t ___lastTrackTouchCount_16;
	// System.Single DigitalRubyShared.GestureRecognizer::<PrevFocusX>k__BackingField
	float ___U3CPrevFocusXU3Ek__BackingField_17;
	// System.Single DigitalRubyShared.GestureRecognizer::<PrevFocusY>k__BackingField
	float ___U3CPrevFocusYU3Ek__BackingField_18;
	// DigitalRubyShared.GestureRecognizerUpdated DigitalRubyShared.GestureRecognizer::Updated
	GestureRecognizerUpdated_t601711085 * ___Updated_20;
	// DigitalRubyShared.GestureRecognizerStateUpdatedDelegate DigitalRubyShared.GestureRecognizer::StateUpdated
	GestureRecognizerStateUpdatedDelegate_t837748355 * ___StateUpdated_21;
	// System.Single DigitalRubyShared.GestureRecognizer::<FocusX>k__BackingField
	float ___U3CFocusXU3Ek__BackingField_22;
	// System.Single DigitalRubyShared.GestureRecognizer::<FocusY>k__BackingField
	float ___U3CFocusYU3Ek__BackingField_23;
	// System.Single DigitalRubyShared.GestureRecognizer::<StartFocusX>k__BackingField
	float ___U3CStartFocusXU3Ek__BackingField_24;
	// System.Single DigitalRubyShared.GestureRecognizer::<StartFocusY>k__BackingField
	float ___U3CStartFocusYU3Ek__BackingField_25;
	// System.Single DigitalRubyShared.GestureRecognizer::<DeltaX>k__BackingField
	float ___U3CDeltaXU3Ek__BackingField_26;
	// System.Single DigitalRubyShared.GestureRecognizer::<DeltaY>k__BackingField
	float ___U3CDeltaYU3Ek__BackingField_27;
	// System.Single DigitalRubyShared.GestureRecognizer::<DistanceX>k__BackingField
	float ___U3CDistanceXU3Ek__BackingField_28;
	// System.Single DigitalRubyShared.GestureRecognizer::<DistanceY>k__BackingField
	float ___U3CDistanceYU3Ek__BackingField_29;
	// System.Single DigitalRubyShared.GestureRecognizer::<Pressure>k__BackingField
	float ___U3CPressureU3Ek__BackingField_30;
	// System.Object DigitalRubyShared.GestureRecognizer::<PlatformSpecificView>k__BackingField
	RuntimeObject * ___U3CPlatformSpecificViewU3Ek__BackingField_31;
	// System.Single DigitalRubyShared.GestureRecognizer::<PlatformSpecificViewScale>k__BackingField
	float ___U3CPlatformSpecificViewScaleU3Ek__BackingField_32;
	// System.Boolean DigitalRubyShared.GestureRecognizer::<ClearTrackedTouchesOnEndOrFail>k__BackingField
	bool ___U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33;
	// System.Boolean DigitalRubyShared.GestureRecognizer::<AllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferent>k__BackingField
	bool ___U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34;
	// System.Boolean DigitalRubyShared.GestureRecognizer::<ReceivedAdditionalTouches>k__BackingField
	bool ___U3CReceivedAdditionalTouchesU3Ek__BackingField_35;

public:
	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_currentTrackedTouches_2() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___currentTrackedTouches_2)); }
	inline List_1_t3464476875 * get_currentTrackedTouches_2() const { return ___currentTrackedTouches_2; }
	inline List_1_t3464476875 ** get_address_of_currentTrackedTouches_2() { return &___currentTrackedTouches_2; }
	inline void set_currentTrackedTouches_2(List_1_t3464476875 * value)
	{
		___currentTrackedTouches_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentTrackedTouches_2), value);
	}

	inline static int32_t get_offset_of_currentTrackedTouchesReadOnly_3() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___currentTrackedTouchesReadOnly_3)); }
	inline ReadOnlyCollection_1_t3204978420 * get_currentTrackedTouchesReadOnly_3() const { return ___currentTrackedTouchesReadOnly_3; }
	inline ReadOnlyCollection_1_t3204978420 ** get_address_of_currentTrackedTouchesReadOnly_3() { return &___currentTrackedTouchesReadOnly_3; }
	inline void set_currentTrackedTouchesReadOnly_3(ReadOnlyCollection_1_t3204978420 * value)
	{
		___currentTrackedTouchesReadOnly_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentTrackedTouchesReadOnly_3), value);
	}

	inline static int32_t get_offset_of_requireGestureRecognizersToFail_4() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___requireGestureRecognizersToFail_4)); }
	inline HashSet_1_t2248979155 * get_requireGestureRecognizersToFail_4() const { return ___requireGestureRecognizersToFail_4; }
	inline HashSet_1_t2248979155 ** get_address_of_requireGestureRecognizersToFail_4() { return &___requireGestureRecognizersToFail_4; }
	inline void set_requireGestureRecognizersToFail_4(HashSet_1_t2248979155 * value)
	{
		___requireGestureRecognizersToFail_4 = value;
		Il2CppCodeGenWriteBarrier((&___requireGestureRecognizersToFail_4), value);
	}

	inline static int32_t get_offset_of_failGestures_5() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___failGestures_5)); }
	inline HashSet_1_t2248979155 * get_failGestures_5() const { return ___failGestures_5; }
	inline HashSet_1_t2248979155 ** get_address_of_failGestures_5() { return &___failGestures_5; }
	inline void set_failGestures_5(HashSet_1_t2248979155 * value)
	{
		___failGestures_5 = value;
		Il2CppCodeGenWriteBarrier((&___failGestures_5), value);
	}

	inline static int32_t get_offset_of_simultaneousGestures_6() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___simultaneousGestures_6)); }
	inline List_1_t861137127 * get_simultaneousGestures_6() const { return ___simultaneousGestures_6; }
	inline List_1_t861137127 ** get_address_of_simultaneousGestures_6() { return &___simultaneousGestures_6; }
	inline void set_simultaneousGestures_6(List_1_t861137127 * value)
	{
		___simultaneousGestures_6 = value;
		Il2CppCodeGenWriteBarrier((&___simultaneousGestures_6), value);
	}

	inline static int32_t get_offset_of_velocityTracker_7() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___velocityTracker_7)); }
	inline GestureVelocityTracker_t806949657 * get_velocityTracker_7() const { return ___velocityTracker_7; }
	inline GestureVelocityTracker_t806949657 ** get_address_of_velocityTracker_7() { return &___velocityTracker_7; }
	inline void set_velocityTracker_7(GestureVelocityTracker_t806949657 * value)
	{
		___velocityTracker_7 = value;
		Il2CppCodeGenWriteBarrier((&___velocityTracker_7), value);
	}

	inline static int32_t get_offset_of_touchStartLocations_8() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___touchStartLocations_8)); }
	inline List_1_t920424597 * get_touchStartLocations_8() const { return ___touchStartLocations_8; }
	inline List_1_t920424597 ** get_address_of_touchStartLocations_8() { return &___touchStartLocations_8; }
	inline void set_touchStartLocations_8(List_1_t920424597 * value)
	{
		___touchStartLocations_8 = value;
		Il2CppCodeGenWriteBarrier((&___touchStartLocations_8), value);
	}

	inline static int32_t get_offset_of_ignoreTouchIds_9() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___ignoreTouchIds_9)); }
	inline HashSet_1_t1515895227 * get_ignoreTouchIds_9() const { return ___ignoreTouchIds_9; }
	inline HashSet_1_t1515895227 ** get_address_of_ignoreTouchIds_9() { return &___ignoreTouchIds_9; }
	inline void set_ignoreTouchIds_9(HashSet_1_t1515895227 * value)
	{
		___ignoreTouchIds_9 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreTouchIds_9), value);
	}

	inline static int32_t get_offset_of_tempTouches_10() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___tempTouches_10)); }
	inline List_1_t3464476875 * get_tempTouches_10() const { return ___tempTouches_10; }
	inline List_1_t3464476875 ** get_address_of_tempTouches_10() { return &___tempTouches_10; }
	inline void set_tempTouches_10(List_1_t3464476875 * value)
	{
		___tempTouches_10 = value;
		Il2CppCodeGenWriteBarrier((&___tempTouches_10), value);
	}

	inline static int32_t get_offset_of_minimumNumberOfTouchesToTrack_11() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___minimumNumberOfTouchesToTrack_11)); }
	inline int32_t get_minimumNumberOfTouchesToTrack_11() const { return ___minimumNumberOfTouchesToTrack_11; }
	inline int32_t* get_address_of_minimumNumberOfTouchesToTrack_11() { return &___minimumNumberOfTouchesToTrack_11; }
	inline void set_minimumNumberOfTouchesToTrack_11(int32_t value)
	{
		___minimumNumberOfTouchesToTrack_11 = value;
	}

	inline static int32_t get_offset_of_maximumNumberOfTouchesToTrack_12() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___maximumNumberOfTouchesToTrack_12)); }
	inline int32_t get_maximumNumberOfTouchesToTrack_12() const { return ___maximumNumberOfTouchesToTrack_12; }
	inline int32_t* get_address_of_maximumNumberOfTouchesToTrack_12() { return &___maximumNumberOfTouchesToTrack_12; }
	inline void set_maximumNumberOfTouchesToTrack_12(int32_t value)
	{
		___maximumNumberOfTouchesToTrack_12 = value;
	}

	inline static int32_t get_offset_of_justFailed_13() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___justFailed_13)); }
	inline bool get_justFailed_13() const { return ___justFailed_13; }
	inline bool* get_address_of_justFailed_13() { return &___justFailed_13; }
	inline void set_justFailed_13(bool value)
	{
		___justFailed_13 = value;
	}

	inline static int32_t get_offset_of_justEnded_14() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___justEnded_14)); }
	inline bool get_justEnded_14() const { return ___justEnded_14; }
	inline bool* get_address_of_justEnded_14() { return &___justEnded_14; }
	inline void set_justEnded_14(bool value)
	{
		___justEnded_14 = value;
	}

	inline static int32_t get_offset_of_isRestarting_15() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___isRestarting_15)); }
	inline bool get_isRestarting_15() const { return ___isRestarting_15; }
	inline bool* get_address_of_isRestarting_15() { return &___isRestarting_15; }
	inline void set_isRestarting_15(bool value)
	{
		___isRestarting_15 = value;
	}

	inline static int32_t get_offset_of_lastTrackTouchCount_16() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___lastTrackTouchCount_16)); }
	inline int32_t get_lastTrackTouchCount_16() const { return ___lastTrackTouchCount_16; }
	inline int32_t* get_address_of_lastTrackTouchCount_16() { return &___lastTrackTouchCount_16; }
	inline void set_lastTrackTouchCount_16(int32_t value)
	{
		___lastTrackTouchCount_16 = value;
	}

	inline static int32_t get_offset_of_U3CPrevFocusXU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPrevFocusXU3Ek__BackingField_17)); }
	inline float get_U3CPrevFocusXU3Ek__BackingField_17() const { return ___U3CPrevFocusXU3Ek__BackingField_17; }
	inline float* get_address_of_U3CPrevFocusXU3Ek__BackingField_17() { return &___U3CPrevFocusXU3Ek__BackingField_17; }
	inline void set_U3CPrevFocusXU3Ek__BackingField_17(float value)
	{
		___U3CPrevFocusXU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CPrevFocusYU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPrevFocusYU3Ek__BackingField_18)); }
	inline float get_U3CPrevFocusYU3Ek__BackingField_18() const { return ___U3CPrevFocusYU3Ek__BackingField_18; }
	inline float* get_address_of_U3CPrevFocusYU3Ek__BackingField_18() { return &___U3CPrevFocusYU3Ek__BackingField_18; }
	inline void set_U3CPrevFocusYU3Ek__BackingField_18(float value)
	{
		___U3CPrevFocusYU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_Updated_20() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___Updated_20)); }
	inline GestureRecognizerUpdated_t601711085 * get_Updated_20() const { return ___Updated_20; }
	inline GestureRecognizerUpdated_t601711085 ** get_address_of_Updated_20() { return &___Updated_20; }
	inline void set_Updated_20(GestureRecognizerUpdated_t601711085 * value)
	{
		___Updated_20 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_20), value);
	}

	inline static int32_t get_offset_of_StateUpdated_21() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___StateUpdated_21)); }
	inline GestureRecognizerStateUpdatedDelegate_t837748355 * get_StateUpdated_21() const { return ___StateUpdated_21; }
	inline GestureRecognizerStateUpdatedDelegate_t837748355 ** get_address_of_StateUpdated_21() { return &___StateUpdated_21; }
	inline void set_StateUpdated_21(GestureRecognizerStateUpdatedDelegate_t837748355 * value)
	{
		___StateUpdated_21 = value;
		Il2CppCodeGenWriteBarrier((&___StateUpdated_21), value);
	}

	inline static int32_t get_offset_of_U3CFocusXU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CFocusXU3Ek__BackingField_22)); }
	inline float get_U3CFocusXU3Ek__BackingField_22() const { return ___U3CFocusXU3Ek__BackingField_22; }
	inline float* get_address_of_U3CFocusXU3Ek__BackingField_22() { return &___U3CFocusXU3Ek__BackingField_22; }
	inline void set_U3CFocusXU3Ek__BackingField_22(float value)
	{
		___U3CFocusXU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CFocusYU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CFocusYU3Ek__BackingField_23)); }
	inline float get_U3CFocusYU3Ek__BackingField_23() const { return ___U3CFocusYU3Ek__BackingField_23; }
	inline float* get_address_of_U3CFocusYU3Ek__BackingField_23() { return &___U3CFocusYU3Ek__BackingField_23; }
	inline void set_U3CFocusYU3Ek__BackingField_23(float value)
	{
		___U3CFocusYU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CStartFocusXU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CStartFocusXU3Ek__BackingField_24)); }
	inline float get_U3CStartFocusXU3Ek__BackingField_24() const { return ___U3CStartFocusXU3Ek__BackingField_24; }
	inline float* get_address_of_U3CStartFocusXU3Ek__BackingField_24() { return &___U3CStartFocusXU3Ek__BackingField_24; }
	inline void set_U3CStartFocusXU3Ek__BackingField_24(float value)
	{
		___U3CStartFocusXU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CStartFocusYU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CStartFocusYU3Ek__BackingField_25)); }
	inline float get_U3CStartFocusYU3Ek__BackingField_25() const { return ___U3CStartFocusYU3Ek__BackingField_25; }
	inline float* get_address_of_U3CStartFocusYU3Ek__BackingField_25() { return &___U3CStartFocusYU3Ek__BackingField_25; }
	inline void set_U3CStartFocusYU3Ek__BackingField_25(float value)
	{
		___U3CStartFocusYU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaXU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDeltaXU3Ek__BackingField_26)); }
	inline float get_U3CDeltaXU3Ek__BackingField_26() const { return ___U3CDeltaXU3Ek__BackingField_26; }
	inline float* get_address_of_U3CDeltaXU3Ek__BackingField_26() { return &___U3CDeltaXU3Ek__BackingField_26; }
	inline void set_U3CDeltaXU3Ek__BackingField_26(float value)
	{
		___U3CDeltaXU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaYU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDeltaYU3Ek__BackingField_27)); }
	inline float get_U3CDeltaYU3Ek__BackingField_27() const { return ___U3CDeltaYU3Ek__BackingField_27; }
	inline float* get_address_of_U3CDeltaYU3Ek__BackingField_27() { return &___U3CDeltaYU3Ek__BackingField_27; }
	inline void set_U3CDeltaYU3Ek__BackingField_27(float value)
	{
		___U3CDeltaYU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceXU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDistanceXU3Ek__BackingField_28)); }
	inline float get_U3CDistanceXU3Ek__BackingField_28() const { return ___U3CDistanceXU3Ek__BackingField_28; }
	inline float* get_address_of_U3CDistanceXU3Ek__BackingField_28() { return &___U3CDistanceXU3Ek__BackingField_28; }
	inline void set_U3CDistanceXU3Ek__BackingField_28(float value)
	{
		___U3CDistanceXU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceYU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CDistanceYU3Ek__BackingField_29)); }
	inline float get_U3CDistanceYU3Ek__BackingField_29() const { return ___U3CDistanceYU3Ek__BackingField_29; }
	inline float* get_address_of_U3CDistanceYU3Ek__BackingField_29() { return &___U3CDistanceYU3Ek__BackingField_29; }
	inline void set_U3CDistanceYU3Ek__BackingField_29(float value)
	{
		___U3CDistanceYU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CPressureU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPressureU3Ek__BackingField_30)); }
	inline float get_U3CPressureU3Ek__BackingField_30() const { return ___U3CPressureU3Ek__BackingField_30; }
	inline float* get_address_of_U3CPressureU3Ek__BackingField_30() { return &___U3CPressureU3Ek__BackingField_30; }
	inline void set_U3CPressureU3Ek__BackingField_30(float value)
	{
		___U3CPressureU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CPlatformSpecificViewU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPlatformSpecificViewU3Ek__BackingField_31)); }
	inline RuntimeObject * get_U3CPlatformSpecificViewU3Ek__BackingField_31() const { return ___U3CPlatformSpecificViewU3Ek__BackingField_31; }
	inline RuntimeObject ** get_address_of_U3CPlatformSpecificViewU3Ek__BackingField_31() { return &___U3CPlatformSpecificViewU3Ek__BackingField_31; }
	inline void set_U3CPlatformSpecificViewU3Ek__BackingField_31(RuntimeObject * value)
	{
		___U3CPlatformSpecificViewU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformSpecificViewU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CPlatformSpecificViewScaleU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CPlatformSpecificViewScaleU3Ek__BackingField_32)); }
	inline float get_U3CPlatformSpecificViewScaleU3Ek__BackingField_32() const { return ___U3CPlatformSpecificViewScaleU3Ek__BackingField_32; }
	inline float* get_address_of_U3CPlatformSpecificViewScaleU3Ek__BackingField_32() { return &___U3CPlatformSpecificViewScaleU3Ek__BackingField_32; }
	inline void set_U3CPlatformSpecificViewScaleU3Ek__BackingField_32(float value)
	{
		___U3CPlatformSpecificViewScaleU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33)); }
	inline bool get_U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33() const { return ___U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33() { return &___U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33; }
	inline void set_U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33(bool value)
	{
		___U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34)); }
	inline bool get_U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34() const { return ___U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34() { return &___U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34; }
	inline void set_U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34(bool value)
	{
		___U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CReceivedAdditionalTouchesU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681, ___U3CReceivedAdditionalTouchesU3Ek__BackingField_35)); }
	inline bool get_U3CReceivedAdditionalTouchesU3Ek__BackingField_35() const { return ___U3CReceivedAdditionalTouchesU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CReceivedAdditionalTouchesU3Ek__BackingField_35() { return &___U3CReceivedAdditionalTouchesU3Ek__BackingField_35; }
	inline void set_U3CReceivedAdditionalTouchesU3Ek__BackingField_35(bool value)
	{
		___U3CReceivedAdditionalTouchesU3Ek__BackingField_35 = value;
	}
};

struct GestureRecognizer_t3684029681_StaticFields
{
public:
	// DigitalRubyShared.GestureRecognizer DigitalRubyShared.GestureRecognizer::allGesturesReference
	GestureRecognizer_t3684029681 * ___allGesturesReference_0;
	// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.GestureRecognizer::ActiveGestures
	HashSet_1_t2248979155 * ___ActiveGestures_19;
	// DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate DigitalRubyShared.GestureRecognizer::MainThreadCallback
	CallbackMainThreadDelegate_t469493312 * ___MainThreadCallback_36;

public:
	inline static int32_t get_offset_of_allGesturesReference_0() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681_StaticFields, ___allGesturesReference_0)); }
	inline GestureRecognizer_t3684029681 * get_allGesturesReference_0() const { return ___allGesturesReference_0; }
	inline GestureRecognizer_t3684029681 ** get_address_of_allGesturesReference_0() { return &___allGesturesReference_0; }
	inline void set_allGesturesReference_0(GestureRecognizer_t3684029681 * value)
	{
		___allGesturesReference_0 = value;
		Il2CppCodeGenWriteBarrier((&___allGesturesReference_0), value);
	}

	inline static int32_t get_offset_of_ActiveGestures_19() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681_StaticFields, ___ActiveGestures_19)); }
	inline HashSet_1_t2248979155 * get_ActiveGestures_19() const { return ___ActiveGestures_19; }
	inline HashSet_1_t2248979155 ** get_address_of_ActiveGestures_19() { return &___ActiveGestures_19; }
	inline void set_ActiveGestures_19(HashSet_1_t2248979155 * value)
	{
		___ActiveGestures_19 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveGestures_19), value);
	}

	inline static int32_t get_offset_of_MainThreadCallback_36() { return static_cast<int32_t>(offsetof(GestureRecognizer_t3684029681_StaticFields, ___MainThreadCallback_36)); }
	inline CallbackMainThreadDelegate_t469493312 * get_MainThreadCallback_36() const { return ___MainThreadCallback_36; }
	inline CallbackMainThreadDelegate_t469493312 ** get_address_of_MainThreadCallback_36() { return &___MainThreadCallback_36; }
	inline void set_MainThreadCallback_36(CallbackMainThreadDelegate_t469493312 * value)
	{
		___MainThreadCallback_36 = value;
		Il2CppCodeGenWriteBarrier((&___MainThreadCallback_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZER_T3684029681_H
#ifndef GESTURETOUCH_T1992402133_H
#define GESTURETOUCH_T1992402133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureTouch
struct  GestureTouch_t1992402133 
{
public:
	// System.Int32 DigitalRubyShared.GestureTouch::id
	int32_t ___id_1;
	// System.Single DigitalRubyShared.GestureTouch::x
	float ___x_2;
	// System.Single DigitalRubyShared.GestureTouch::y
	float ___y_3;
	// System.Single DigitalRubyShared.GestureTouch::previousX
	float ___previousX_4;
	// System.Single DigitalRubyShared.GestureTouch::previousY
	float ___previousY_5;
	// System.Single DigitalRubyShared.GestureTouch::pressure
	float ___pressure_6;
	// System.Single DigitalRubyShared.GestureTouch::screenX
	float ___screenX_7;
	// System.Single DigitalRubyShared.GestureTouch::screenY
	float ___screenY_8;
	// System.Object DigitalRubyShared.GestureTouch::platformSpecificTouch
	RuntimeObject * ___platformSpecificTouch_9;
	// DigitalRubyShared.TouchPhase DigitalRubyShared.GestureTouch::touchPhase
	int32_t ___touchPhase_10;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_previousX_4() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___previousX_4)); }
	inline float get_previousX_4() const { return ___previousX_4; }
	inline float* get_address_of_previousX_4() { return &___previousX_4; }
	inline void set_previousX_4(float value)
	{
		___previousX_4 = value;
	}

	inline static int32_t get_offset_of_previousY_5() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___previousY_5)); }
	inline float get_previousY_5() const { return ___previousY_5; }
	inline float* get_address_of_previousY_5() { return &___previousY_5; }
	inline void set_previousY_5(float value)
	{
		___previousY_5 = value;
	}

	inline static int32_t get_offset_of_pressure_6() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___pressure_6)); }
	inline float get_pressure_6() const { return ___pressure_6; }
	inline float* get_address_of_pressure_6() { return &___pressure_6; }
	inline void set_pressure_6(float value)
	{
		___pressure_6 = value;
	}

	inline static int32_t get_offset_of_screenX_7() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___screenX_7)); }
	inline float get_screenX_7() const { return ___screenX_7; }
	inline float* get_address_of_screenX_7() { return &___screenX_7; }
	inline void set_screenX_7(float value)
	{
		___screenX_7 = value;
	}

	inline static int32_t get_offset_of_screenY_8() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___screenY_8)); }
	inline float get_screenY_8() const { return ___screenY_8; }
	inline float* get_address_of_screenY_8() { return &___screenY_8; }
	inline void set_screenY_8(float value)
	{
		___screenY_8 = value;
	}

	inline static int32_t get_offset_of_platformSpecificTouch_9() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___platformSpecificTouch_9)); }
	inline RuntimeObject * get_platformSpecificTouch_9() const { return ___platformSpecificTouch_9; }
	inline RuntimeObject ** get_address_of_platformSpecificTouch_9() { return &___platformSpecificTouch_9; }
	inline void set_platformSpecificTouch_9(RuntimeObject * value)
	{
		___platformSpecificTouch_9 = value;
		Il2CppCodeGenWriteBarrier((&___platformSpecificTouch_9), value);
	}

	inline static int32_t get_offset_of_touchPhase_10() { return static_cast<int32_t>(offsetof(GestureTouch_t1992402133, ___touchPhase_10)); }
	inline int32_t get_touchPhase_10() const { return ___touchPhase_10; }
	inline int32_t* get_address_of_touchPhase_10() { return &___touchPhase_10; }
	inline void set_touchPhase_10(int32_t value)
	{
		___touchPhase_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DigitalRubyShared.GestureTouch
struct GestureTouch_t1992402133_marshaled_pinvoke
{
	int32_t ___id_1;
	float ___x_2;
	float ___y_3;
	float ___previousX_4;
	float ___previousY_5;
	float ___pressure_6;
	float ___screenX_7;
	float ___screenY_8;
	Il2CppIUnknown* ___platformSpecificTouch_9;
	int32_t ___touchPhase_10;
};
// Native definition for COM marshalling of DigitalRubyShared.GestureTouch
struct GestureTouch_t1992402133_marshaled_com
{
	int32_t ___id_1;
	float ___x_2;
	float ___y_3;
	float ___previousX_4;
	float ___previousY_5;
	float ___pressure_6;
	float ___screenX_7;
	float ___screenY_8;
	Il2CppIUnknown* ___platformSpecificTouch_9;
	int32_t ___touchPhase_10;
};
#endif // GESTURETOUCH_T1992402133_H
#ifndef ADVERTISINGRESULT_T1229207569_H
#define ADVERTISINGRESULT_T1229207569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct  AdvertisingResult_t1229207569 
{
public:
	// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mStatus
	int32_t ___mStatus_0;
	// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mLocalEndpointName
	String_t* ___mLocalEndpointName_1;

public:
	inline static int32_t get_offset_of_mStatus_0() { return static_cast<int32_t>(offsetof(AdvertisingResult_t1229207569, ___mStatus_0)); }
	inline int32_t get_mStatus_0() const { return ___mStatus_0; }
	inline int32_t* get_address_of_mStatus_0() { return &___mStatus_0; }
	inline void set_mStatus_0(int32_t value)
	{
		___mStatus_0 = value;
	}

	inline static int32_t get_offset_of_mLocalEndpointName_1() { return static_cast<int32_t>(offsetof(AdvertisingResult_t1229207569, ___mLocalEndpointName_1)); }
	inline String_t* get_mLocalEndpointName_1() const { return ___mLocalEndpointName_1; }
	inline String_t** get_address_of_mLocalEndpointName_1() { return &___mLocalEndpointName_1; }
	inline void set_mLocalEndpointName_1(String_t* value)
	{
		___mLocalEndpointName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mLocalEndpointName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t1229207569_marshaled_pinvoke
{
	int32_t ___mStatus_0;
	char* ___mLocalEndpointName_1;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t1229207569_marshaled_com
{
	int32_t ___mStatus_0;
	Il2CppChar* ___mLocalEndpointName_1;
};
#endif // ADVERTISINGRESULT_T1229207569_H
#ifndef CONNECTIONRESPONSE_T735328601_H
#define CONNECTIONRESPONSE_T735328601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct  ConnectionResponse_t735328601 
{
public:
	// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mLocalClientId
	int64_t ___mLocalClientId_1;
	// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mRemoteEndpointId
	String_t* ___mRemoteEndpointId_2;
	// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mResponseStatus
	int32_t ___mResponseStatus_3;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mPayload
	ByteU5BU5D_t4116647657* ___mPayload_4;

public:
	inline static int32_t get_offset_of_mLocalClientId_1() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mLocalClientId_1)); }
	inline int64_t get_mLocalClientId_1() const { return ___mLocalClientId_1; }
	inline int64_t* get_address_of_mLocalClientId_1() { return &___mLocalClientId_1; }
	inline void set_mLocalClientId_1(int64_t value)
	{
		___mLocalClientId_1 = value;
	}

	inline static int32_t get_offset_of_mRemoteEndpointId_2() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mRemoteEndpointId_2)); }
	inline String_t* get_mRemoteEndpointId_2() const { return ___mRemoteEndpointId_2; }
	inline String_t** get_address_of_mRemoteEndpointId_2() { return &___mRemoteEndpointId_2; }
	inline void set_mRemoteEndpointId_2(String_t* value)
	{
		___mRemoteEndpointId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mRemoteEndpointId_2), value);
	}

	inline static int32_t get_offset_of_mResponseStatus_3() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mResponseStatus_3)); }
	inline int32_t get_mResponseStatus_3() const { return ___mResponseStatus_3; }
	inline int32_t* get_address_of_mResponseStatus_3() { return &___mResponseStatus_3; }
	inline void set_mResponseStatus_3(int32_t value)
	{
		___mResponseStatus_3 = value;
	}

	inline static int32_t get_offset_of_mPayload_4() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mPayload_4)); }
	inline ByteU5BU5D_t4116647657* get_mPayload_4() const { return ___mPayload_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_mPayload_4() { return &___mPayload_4; }
	inline void set_mPayload_4(ByteU5BU5D_t4116647657* value)
	{
		___mPayload_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPayload_4), value);
	}
};

struct ConnectionResponse_t735328601_StaticFields
{
public:
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EmptyPayload
	ByteU5BU5D_t4116647657* ___EmptyPayload_0;

public:
	inline static int32_t get_offset_of_EmptyPayload_0() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601_StaticFields, ___EmptyPayload_0)); }
	inline ByteU5BU5D_t4116647657* get_EmptyPayload_0() const { return ___EmptyPayload_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_EmptyPayload_0() { return &___EmptyPayload_0; }
	inline void set_EmptyPayload_0(ByteU5BU5D_t4116647657* value)
	{
		___EmptyPayload_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyPayload_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_t735328601_marshaled_pinvoke
{
	int64_t ___mLocalClientId_1;
	char* ___mRemoteEndpointId_2;
	int32_t ___mResponseStatus_3;
	uint8_t* ___mPayload_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_t735328601_marshaled_com
{
	int64_t ___mLocalClientId_1;
	Il2CppChar* ___mRemoteEndpointId_2;
	int32_t ___mResponseStatus_3;
	uint8_t* ___mPayload_4;
};
#endif // CONNECTIONRESPONSE_T735328601_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CALLBACKMAINTHREADDELEGATE_T469493312_H
#define CALLBACKMAINTHREADDELEGATE_T469493312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate
struct  CallbackMainThreadDelegate_t469493312  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKMAINTHREADDELEGATE_T469493312_H
#ifndef GESTURERECOGNIZERSTATEUPDATEDDELEGATE_T837748355_H
#define GESTURERECOGNIZERSTATEUPDATEDDELEGATE_T837748355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerStateUpdatedDelegate
struct  GestureRecognizerStateUpdatedDelegate_t837748355  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERSTATEUPDATEDDELEGATE_T837748355_H
#ifndef GESTURERECOGNIZERUPDATED_T601711085_H
#define GESTURERECOGNIZERUPDATED_T601711085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerUpdated
struct  GestureRecognizerUpdated_t601711085  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERUPDATED_T601711085_H
#ifndef IMAGEGESTURERECOGNIZER_T4233185475_H
#define IMAGEGESTURERECOGNIZER_T4233185475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureRecognizer
struct  ImageGestureRecognizer_t4233185475  : public GestureRecognizer_t3684029681
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point>> DigitalRubyShared.ImageGestureRecognizer::points
	List_1_t1060306332 * ___points_43;
	// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizer/Point> DigitalRubyShared.ImageGestureRecognizer::currentList
	List_1_t3883198886 * ___currentList_44;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::minX
	int32_t ___minX_45;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::minY
	int32_t ___minY_46;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::maxX
	int32_t ___maxX_47;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::maxY
	int32_t ___maxY_48;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::<MaximumPathCount>k__BackingField
	int32_t ___U3CMaximumPathCountU3Ek__BackingField_49;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<DirectionTolerance>k__BackingField
	float ___U3CDirectionToleranceU3Ek__BackingField_50;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_51;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<MinimumDistanceBetweenPointsUnits>k__BackingField
	float ___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<SimilarityMinimum>k__BackingField
	float ___U3CSimilarityMinimumU3Ek__BackingField_53;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::<MinimumPointsToRecognize>k__BackingField
	int32_t ___U3CMinimumPointsToRecognizeU3Ek__BackingField_54;
	// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureImage> DigitalRubyShared.ImageGestureRecognizer::<GestureImages>k__BackingField
	List_1_t62703717 * ___U3CGestureImagesU3Ek__BackingField_55;
	// System.EventHandler DigitalRubyShared.ImageGestureRecognizer::MaximumPathCountExceeded
	EventHandler_t1348719766 * ___MaximumPathCountExceeded_56;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.ImageGestureRecognizer::<Image>k__BackingField
	ImageGestureImage_t2885596271 * ___U3CImageU3Ek__BackingField_57;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.ImageGestureRecognizer::<MatchedGestureImage>k__BackingField
	ImageGestureImage_t2885596271 * ___U3CMatchedGestureImageU3Ek__BackingField_58;
	// System.Single DigitalRubyShared.ImageGestureRecognizer::<MatchedGestureCalculationTimeMilliseconds>k__BackingField
	float ___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizer::<PathCount>k__BackingField
	int32_t ___U3CPathCountU3Ek__BackingField_60;

public:
	inline static int32_t get_offset_of_points_43() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___points_43)); }
	inline List_1_t1060306332 * get_points_43() const { return ___points_43; }
	inline List_1_t1060306332 ** get_address_of_points_43() { return &___points_43; }
	inline void set_points_43(List_1_t1060306332 * value)
	{
		___points_43 = value;
		Il2CppCodeGenWriteBarrier((&___points_43), value);
	}

	inline static int32_t get_offset_of_currentList_44() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___currentList_44)); }
	inline List_1_t3883198886 * get_currentList_44() const { return ___currentList_44; }
	inline List_1_t3883198886 ** get_address_of_currentList_44() { return &___currentList_44; }
	inline void set_currentList_44(List_1_t3883198886 * value)
	{
		___currentList_44 = value;
		Il2CppCodeGenWriteBarrier((&___currentList_44), value);
	}

	inline static int32_t get_offset_of_minX_45() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___minX_45)); }
	inline int32_t get_minX_45() const { return ___minX_45; }
	inline int32_t* get_address_of_minX_45() { return &___minX_45; }
	inline void set_minX_45(int32_t value)
	{
		___minX_45 = value;
	}

	inline static int32_t get_offset_of_minY_46() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___minY_46)); }
	inline int32_t get_minY_46() const { return ___minY_46; }
	inline int32_t* get_address_of_minY_46() { return &___minY_46; }
	inline void set_minY_46(int32_t value)
	{
		___minY_46 = value;
	}

	inline static int32_t get_offset_of_maxX_47() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___maxX_47)); }
	inline int32_t get_maxX_47() const { return ___maxX_47; }
	inline int32_t* get_address_of_maxX_47() { return &___maxX_47; }
	inline void set_maxX_47(int32_t value)
	{
		___maxX_47 = value;
	}

	inline static int32_t get_offset_of_maxY_48() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___maxY_48)); }
	inline int32_t get_maxY_48() const { return ___maxY_48; }
	inline int32_t* get_address_of_maxY_48() { return &___maxY_48; }
	inline void set_maxY_48(int32_t value)
	{
		___maxY_48 = value;
	}

	inline static int32_t get_offset_of_U3CMaximumPathCountU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMaximumPathCountU3Ek__BackingField_49)); }
	inline int32_t get_U3CMaximumPathCountU3Ek__BackingField_49() const { return ___U3CMaximumPathCountU3Ek__BackingField_49; }
	inline int32_t* get_address_of_U3CMaximumPathCountU3Ek__BackingField_49() { return &___U3CMaximumPathCountU3Ek__BackingField_49; }
	inline void set_U3CMaximumPathCountU3Ek__BackingField_49(int32_t value)
	{
		___U3CMaximumPathCountU3Ek__BackingField_49 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionToleranceU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CDirectionToleranceU3Ek__BackingField_50)); }
	inline float get_U3CDirectionToleranceU3Ek__BackingField_50() const { return ___U3CDirectionToleranceU3Ek__BackingField_50; }
	inline float* get_address_of_U3CDirectionToleranceU3Ek__BackingField_50() { return &___U3CDirectionToleranceU3Ek__BackingField_50; }
	inline void set_U3CDirectionToleranceU3Ek__BackingField_50(float value)
	{
		___U3CDirectionToleranceU3Ek__BackingField_50 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CThresholdUnitsU3Ek__BackingField_51)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_51() const { return ___U3CThresholdUnitsU3Ek__BackingField_51; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_51() { return &___U3CThresholdUnitsU3Ek__BackingField_51; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_51(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52)); }
	inline float get_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52() const { return ___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52; }
	inline float* get_address_of_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52() { return &___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52; }
	inline void set_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52(float value)
	{
		___U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_U3CSimilarityMinimumU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CSimilarityMinimumU3Ek__BackingField_53)); }
	inline float get_U3CSimilarityMinimumU3Ek__BackingField_53() const { return ___U3CSimilarityMinimumU3Ek__BackingField_53; }
	inline float* get_address_of_U3CSimilarityMinimumU3Ek__BackingField_53() { return &___U3CSimilarityMinimumU3Ek__BackingField_53; }
	inline void set_U3CSimilarityMinimumU3Ek__BackingField_53(float value)
	{
		___U3CSimilarityMinimumU3Ek__BackingField_53 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumPointsToRecognizeU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMinimumPointsToRecognizeU3Ek__BackingField_54)); }
	inline int32_t get_U3CMinimumPointsToRecognizeU3Ek__BackingField_54() const { return ___U3CMinimumPointsToRecognizeU3Ek__BackingField_54; }
	inline int32_t* get_address_of_U3CMinimumPointsToRecognizeU3Ek__BackingField_54() { return &___U3CMinimumPointsToRecognizeU3Ek__BackingField_54; }
	inline void set_U3CMinimumPointsToRecognizeU3Ek__BackingField_54(int32_t value)
	{
		___U3CMinimumPointsToRecognizeU3Ek__BackingField_54 = value;
	}

	inline static int32_t get_offset_of_U3CGestureImagesU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CGestureImagesU3Ek__BackingField_55)); }
	inline List_1_t62703717 * get_U3CGestureImagesU3Ek__BackingField_55() const { return ___U3CGestureImagesU3Ek__BackingField_55; }
	inline List_1_t62703717 ** get_address_of_U3CGestureImagesU3Ek__BackingField_55() { return &___U3CGestureImagesU3Ek__BackingField_55; }
	inline void set_U3CGestureImagesU3Ek__BackingField_55(List_1_t62703717 * value)
	{
		___U3CGestureImagesU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureImagesU3Ek__BackingField_55), value);
	}

	inline static int32_t get_offset_of_MaximumPathCountExceeded_56() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___MaximumPathCountExceeded_56)); }
	inline EventHandler_t1348719766 * get_MaximumPathCountExceeded_56() const { return ___MaximumPathCountExceeded_56; }
	inline EventHandler_t1348719766 ** get_address_of_MaximumPathCountExceeded_56() { return &___MaximumPathCountExceeded_56; }
	inline void set_MaximumPathCountExceeded_56(EventHandler_t1348719766 * value)
	{
		___MaximumPathCountExceeded_56 = value;
		Il2CppCodeGenWriteBarrier((&___MaximumPathCountExceeded_56), value);
	}

	inline static int32_t get_offset_of_U3CImageU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CImageU3Ek__BackingField_57)); }
	inline ImageGestureImage_t2885596271 * get_U3CImageU3Ek__BackingField_57() const { return ___U3CImageU3Ek__BackingField_57; }
	inline ImageGestureImage_t2885596271 ** get_address_of_U3CImageU3Ek__BackingField_57() { return &___U3CImageU3Ek__BackingField_57; }
	inline void set_U3CImageU3Ek__BackingField_57(ImageGestureImage_t2885596271 * value)
	{
		___U3CImageU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageU3Ek__BackingField_57), value);
	}

	inline static int32_t get_offset_of_U3CMatchedGestureImageU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMatchedGestureImageU3Ek__BackingField_58)); }
	inline ImageGestureImage_t2885596271 * get_U3CMatchedGestureImageU3Ek__BackingField_58() const { return ___U3CMatchedGestureImageU3Ek__BackingField_58; }
	inline ImageGestureImage_t2885596271 ** get_address_of_U3CMatchedGestureImageU3Ek__BackingField_58() { return &___U3CMatchedGestureImageU3Ek__BackingField_58; }
	inline void set_U3CMatchedGestureImageU3Ek__BackingField_58(ImageGestureImage_t2885596271 * value)
	{
		___U3CMatchedGestureImageU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMatchedGestureImageU3Ek__BackingField_58), value);
	}

	inline static int32_t get_offset_of_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59)); }
	inline float get_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59() const { return ___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59; }
	inline float* get_address_of_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59() { return &___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59; }
	inline void set_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59(float value)
	{
		___U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of_U3CPathCountU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475, ___U3CPathCountU3Ek__BackingField_60)); }
	inline int32_t get_U3CPathCountU3Ek__BackingField_60() const { return ___U3CPathCountU3Ek__BackingField_60; }
	inline int32_t* get_address_of_U3CPathCountU3Ek__BackingField_60() { return &___U3CPathCountU3Ek__BackingField_60; }
	inline void set_U3CPathCountU3Ek__BackingField_60(int32_t value)
	{
		___U3CPathCountU3Ek__BackingField_60 = value;
	}
};

struct ImageGestureRecognizer_t4233185475_StaticFields
{
public:
	// System.UInt64[] DigitalRubyShared.ImageGestureRecognizer::RowBitMasks
	UInt64U5BU5D_t1659327989* ___RowBitMasks_37;
	// System.UInt64 DigitalRubyShared.ImageGestureRecognizer::RowBitmask
	uint64_t ___RowBitmask_38;

public:
	inline static int32_t get_offset_of_RowBitMasks_37() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475_StaticFields, ___RowBitMasks_37)); }
	inline UInt64U5BU5D_t1659327989* get_RowBitMasks_37() const { return ___RowBitMasks_37; }
	inline UInt64U5BU5D_t1659327989** get_address_of_RowBitMasks_37() { return &___RowBitMasks_37; }
	inline void set_RowBitMasks_37(UInt64U5BU5D_t1659327989* value)
	{
		___RowBitMasks_37 = value;
		Il2CppCodeGenWriteBarrier((&___RowBitMasks_37), value);
	}

	inline static int32_t get_offset_of_RowBitmask_38() { return static_cast<int32_t>(offsetof(ImageGestureRecognizer_t4233185475_StaticFields, ___RowBitmask_38)); }
	inline uint64_t get_RowBitmask_38() const { return ___RowBitmask_38; }
	inline uint64_t* get_address_of_RowBitmask_38() { return &___RowBitmask_38; }
	inline void set_RowBitmask_38(uint64_t value)
	{
		___RowBitmask_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEGESTURERECOGNIZER_T4233185475_H
#ifndef LONGPRESSGESTURERECOGNIZER_T3980777482_H
#define LONGPRESSGESTURERECOGNIZER_T3980777482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.LongPressGestureRecognizer
struct  LongPressGestureRecognizer_t3980777482  : public GestureRecognizer_t3684029681
{
public:
	// System.Diagnostics.Stopwatch DigitalRubyShared.LongPressGestureRecognizer::stopWatch
	Stopwatch_t305734070 * ___stopWatch_37;
	// System.Single DigitalRubyShared.LongPressGestureRecognizer::<MinimumDurationSeconds>k__BackingField
	float ___U3CMinimumDurationSecondsU3Ek__BackingField_38;
	// System.Single DigitalRubyShared.LongPressGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_stopWatch_37() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizer_t3980777482, ___stopWatch_37)); }
	inline Stopwatch_t305734070 * get_stopWatch_37() const { return ___stopWatch_37; }
	inline Stopwatch_t305734070 ** get_address_of_stopWatch_37() { return &___stopWatch_37; }
	inline void set_stopWatch_37(Stopwatch_t305734070 * value)
	{
		___stopWatch_37 = value;
		Il2CppCodeGenWriteBarrier((&___stopWatch_37), value);
	}

	inline static int32_t get_offset_of_U3CMinimumDurationSecondsU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizer_t3980777482, ___U3CMinimumDurationSecondsU3Ek__BackingField_38)); }
	inline float get_U3CMinimumDurationSecondsU3Ek__BackingField_38() const { return ___U3CMinimumDurationSecondsU3Ek__BackingField_38; }
	inline float* get_address_of_U3CMinimumDurationSecondsU3Ek__BackingField_38() { return &___U3CMinimumDurationSecondsU3Ek__BackingField_38; }
	inline void set_U3CMinimumDurationSecondsU3Ek__BackingField_38(float value)
	{
		___U3CMinimumDurationSecondsU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizer_t3980777482, ___U3CThresholdUnitsU3Ek__BackingField_39)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_39() const { return ___U3CThresholdUnitsU3Ek__BackingField_39; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_39() { return &___U3CThresholdUnitsU3Ek__BackingField_39; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_39(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPRESSGESTURERECOGNIZER_T3980777482_H
#ifndef ONETOUCHSCALEGESTURERECOGNIZER_T1313669683_H
#define ONETOUCHSCALEGESTURERECOGNIZER_T1313669683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.OneTouchScaleGestureRecognizer
struct  OneTouchScaleGestureRecognizer_t1313669683  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ScaleMultiplier>k__BackingField
	float ___U3CScaleMultiplierU3Ek__BackingField_37;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ScaleMultiplierX>k__BackingField
	float ___U3CScaleMultiplierXU3Ek__BackingField_38;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ScaleMultiplierY>k__BackingField
	float ___U3CScaleMultiplierYU3Ek__BackingField_39;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ZoomSpeed>k__BackingField
	float ___U3CZoomSpeedU3Ek__BackingField_40;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_41;

public:
	inline static int32_t get_offset_of_U3CScaleMultiplierU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CScaleMultiplierU3Ek__BackingField_37)); }
	inline float get_U3CScaleMultiplierU3Ek__BackingField_37() const { return ___U3CScaleMultiplierU3Ek__BackingField_37; }
	inline float* get_address_of_U3CScaleMultiplierU3Ek__BackingField_37() { return &___U3CScaleMultiplierU3Ek__BackingField_37; }
	inline void set_U3CScaleMultiplierU3Ek__BackingField_37(float value)
	{
		___U3CScaleMultiplierU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CScaleMultiplierXU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CScaleMultiplierXU3Ek__BackingField_38)); }
	inline float get_U3CScaleMultiplierXU3Ek__BackingField_38() const { return ___U3CScaleMultiplierXU3Ek__BackingField_38; }
	inline float* get_address_of_U3CScaleMultiplierXU3Ek__BackingField_38() { return &___U3CScaleMultiplierXU3Ek__BackingField_38; }
	inline void set_U3CScaleMultiplierXU3Ek__BackingField_38(float value)
	{
		___U3CScaleMultiplierXU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CScaleMultiplierYU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CScaleMultiplierYU3Ek__BackingField_39)); }
	inline float get_U3CScaleMultiplierYU3Ek__BackingField_39() const { return ___U3CScaleMultiplierYU3Ek__BackingField_39; }
	inline float* get_address_of_U3CScaleMultiplierYU3Ek__BackingField_39() { return &___U3CScaleMultiplierYU3Ek__BackingField_39; }
	inline void set_U3CScaleMultiplierYU3Ek__BackingField_39(float value)
	{
		___U3CScaleMultiplierYU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3CZoomSpeedU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CZoomSpeedU3Ek__BackingField_40)); }
	inline float get_U3CZoomSpeedU3Ek__BackingField_40() const { return ___U3CZoomSpeedU3Ek__BackingField_40; }
	inline float* get_address_of_U3CZoomSpeedU3Ek__BackingField_40() { return &___U3CZoomSpeedU3Ek__BackingField_40; }
	inline void set_U3CZoomSpeedU3Ek__BackingField_40(float value)
	{
		___U3CZoomSpeedU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizer_t1313669683, ___U3CThresholdUnitsU3Ek__BackingField_41)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_41() const { return ___U3CThresholdUnitsU3Ek__BackingField_41; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_41() { return &___U3CThresholdUnitsU3Ek__BackingField_41; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_41(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONETOUCHSCALEGESTURERECOGNIZER_T1313669683_H
#ifndef PANGESTURERECOGNIZER_T195762396_H
#define PANGESTURERECOGNIZER_T195762396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.PanGestureRecognizer
struct  PanGestureRecognizer_t195762396  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.PanGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_37;

public:
	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(PanGestureRecognizer_t195762396, ___U3CThresholdUnitsU3Ek__BackingField_37)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_37() const { return ___U3CThresholdUnitsU3Ek__BackingField_37; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_37() { return &___U3CThresholdUnitsU3Ek__BackingField_37; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_37(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANGESTURERECOGNIZER_T195762396_H
#ifndef ROTATEGESTURERECOGNIZER_T4100246528_H
#define ROTATEGESTURERECOGNIZER_T4100246528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.RotateGestureRecognizer
struct  RotateGestureRecognizer_t4100246528  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.RotateGestureRecognizer::startAngle
	float ___startAngle_38;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::previousAngle
	float ___previousAngle_39;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::previousAngleSign
	float ___previousAngleSign_40;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<AngleThreshold>k__BackingField
	float ___U3CAngleThresholdU3Ek__BackingField_41;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_42;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<RotationRadians>k__BackingField
	float ___U3CRotationRadiansU3Ek__BackingField_43;
	// System.Single DigitalRubyShared.RotateGestureRecognizer::<RotationRadiansDelta>k__BackingField
	float ___U3CRotationRadiansDeltaU3Ek__BackingField_44;

public:
	inline static int32_t get_offset_of_startAngle_38() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___startAngle_38)); }
	inline float get_startAngle_38() const { return ___startAngle_38; }
	inline float* get_address_of_startAngle_38() { return &___startAngle_38; }
	inline void set_startAngle_38(float value)
	{
		___startAngle_38 = value;
	}

	inline static int32_t get_offset_of_previousAngle_39() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___previousAngle_39)); }
	inline float get_previousAngle_39() const { return ___previousAngle_39; }
	inline float* get_address_of_previousAngle_39() { return &___previousAngle_39; }
	inline void set_previousAngle_39(float value)
	{
		___previousAngle_39 = value;
	}

	inline static int32_t get_offset_of_previousAngleSign_40() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___previousAngleSign_40)); }
	inline float get_previousAngleSign_40() const { return ___previousAngleSign_40; }
	inline float* get_address_of_previousAngleSign_40() { return &___previousAngleSign_40; }
	inline void set_previousAngleSign_40(float value)
	{
		___previousAngleSign_40 = value;
	}

	inline static int32_t get_offset_of_U3CAngleThresholdU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CAngleThresholdU3Ek__BackingField_41)); }
	inline float get_U3CAngleThresholdU3Ek__BackingField_41() const { return ___U3CAngleThresholdU3Ek__BackingField_41; }
	inline float* get_address_of_U3CAngleThresholdU3Ek__BackingField_41() { return &___U3CAngleThresholdU3Ek__BackingField_41; }
	inline void set_U3CAngleThresholdU3Ek__BackingField_41(float value)
	{
		___U3CAngleThresholdU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CThresholdUnitsU3Ek__BackingField_42)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_42() const { return ___U3CThresholdUnitsU3Ek__BackingField_42; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_42() { return &___U3CThresholdUnitsU3Ek__BackingField_42; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_42(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CRotationRadiansU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CRotationRadiansU3Ek__BackingField_43)); }
	inline float get_U3CRotationRadiansU3Ek__BackingField_43() const { return ___U3CRotationRadiansU3Ek__BackingField_43; }
	inline float* get_address_of_U3CRotationRadiansU3Ek__BackingField_43() { return &___U3CRotationRadiansU3Ek__BackingField_43; }
	inline void set_U3CRotationRadiansU3Ek__BackingField_43(float value)
	{
		___U3CRotationRadiansU3Ek__BackingField_43 = value;
	}

	inline static int32_t get_offset_of_U3CRotationRadiansDeltaU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(RotateGestureRecognizer_t4100246528, ___U3CRotationRadiansDeltaU3Ek__BackingField_44)); }
	inline float get_U3CRotationRadiansDeltaU3Ek__BackingField_44() const { return ___U3CRotationRadiansDeltaU3Ek__BackingField_44; }
	inline float* get_address_of_U3CRotationRadiansDeltaU3Ek__BackingField_44() { return &___U3CRotationRadiansDeltaU3Ek__BackingField_44; }
	inline void set_U3CRotationRadiansDeltaU3Ek__BackingField_44(float value)
	{
		___U3CRotationRadiansDeltaU3Ek__BackingField_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEGESTURERECOGNIZER_T4100246528_H
#ifndef SCALEGESTURERECOGNIZER_T1137887245_H
#define SCALEGESTURERECOGNIZER_T1137887245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ScaleGestureRecognizer
struct  ScaleGestureRecognizer_t1137887245  : public GestureRecognizer_t3684029681
{
public:
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::previousDistanceDirection
	float ___previousDistanceDirection_44;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::previousDistance
	float ___previousDistance_45;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::previousDistanceX
	float ___previousDistanceX_46;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::previousDistanceY
	float ___previousDistanceY_47;
	// System.Diagnostics.Stopwatch DigitalRubyShared.ScaleGestureRecognizer::timer
	Stopwatch_t305734070 * ___timer_48;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ScaleMultiplier>k__BackingField
	float ___U3CScaleMultiplierU3Ek__BackingField_49;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ScaleMultiplierX>k__BackingField
	float ___U3CScaleMultiplierXU3Ek__BackingField_50;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ScaleMultiplierY>k__BackingField
	float ___U3CScaleMultiplierYU3Ek__BackingField_51;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ZoomSpeed>k__BackingField
	float ___U3CZoomSpeedU3Ek__BackingField_52;
	// System.Single DigitalRubyShared.ScaleGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_53;

public:
	inline static int32_t get_offset_of_previousDistanceDirection_44() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___previousDistanceDirection_44)); }
	inline float get_previousDistanceDirection_44() const { return ___previousDistanceDirection_44; }
	inline float* get_address_of_previousDistanceDirection_44() { return &___previousDistanceDirection_44; }
	inline void set_previousDistanceDirection_44(float value)
	{
		___previousDistanceDirection_44 = value;
	}

	inline static int32_t get_offset_of_previousDistance_45() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___previousDistance_45)); }
	inline float get_previousDistance_45() const { return ___previousDistance_45; }
	inline float* get_address_of_previousDistance_45() { return &___previousDistance_45; }
	inline void set_previousDistance_45(float value)
	{
		___previousDistance_45 = value;
	}

	inline static int32_t get_offset_of_previousDistanceX_46() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___previousDistanceX_46)); }
	inline float get_previousDistanceX_46() const { return ___previousDistanceX_46; }
	inline float* get_address_of_previousDistanceX_46() { return &___previousDistanceX_46; }
	inline void set_previousDistanceX_46(float value)
	{
		___previousDistanceX_46 = value;
	}

	inline static int32_t get_offset_of_previousDistanceY_47() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___previousDistanceY_47)); }
	inline float get_previousDistanceY_47() const { return ___previousDistanceY_47; }
	inline float* get_address_of_previousDistanceY_47() { return &___previousDistanceY_47; }
	inline void set_previousDistanceY_47(float value)
	{
		___previousDistanceY_47 = value;
	}

	inline static int32_t get_offset_of_timer_48() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___timer_48)); }
	inline Stopwatch_t305734070 * get_timer_48() const { return ___timer_48; }
	inline Stopwatch_t305734070 ** get_address_of_timer_48() { return &___timer_48; }
	inline void set_timer_48(Stopwatch_t305734070 * value)
	{
		___timer_48 = value;
		Il2CppCodeGenWriteBarrier((&___timer_48), value);
	}

	inline static int32_t get_offset_of_U3CScaleMultiplierU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CScaleMultiplierU3Ek__BackingField_49)); }
	inline float get_U3CScaleMultiplierU3Ek__BackingField_49() const { return ___U3CScaleMultiplierU3Ek__BackingField_49; }
	inline float* get_address_of_U3CScaleMultiplierU3Ek__BackingField_49() { return &___U3CScaleMultiplierU3Ek__BackingField_49; }
	inline void set_U3CScaleMultiplierU3Ek__BackingField_49(float value)
	{
		___U3CScaleMultiplierU3Ek__BackingField_49 = value;
	}

	inline static int32_t get_offset_of_U3CScaleMultiplierXU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CScaleMultiplierXU3Ek__BackingField_50)); }
	inline float get_U3CScaleMultiplierXU3Ek__BackingField_50() const { return ___U3CScaleMultiplierXU3Ek__BackingField_50; }
	inline float* get_address_of_U3CScaleMultiplierXU3Ek__BackingField_50() { return &___U3CScaleMultiplierXU3Ek__BackingField_50; }
	inline void set_U3CScaleMultiplierXU3Ek__BackingField_50(float value)
	{
		___U3CScaleMultiplierXU3Ek__BackingField_50 = value;
	}

	inline static int32_t get_offset_of_U3CScaleMultiplierYU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CScaleMultiplierYU3Ek__BackingField_51)); }
	inline float get_U3CScaleMultiplierYU3Ek__BackingField_51() const { return ___U3CScaleMultiplierYU3Ek__BackingField_51; }
	inline float* get_address_of_U3CScaleMultiplierYU3Ek__BackingField_51() { return &___U3CScaleMultiplierYU3Ek__BackingField_51; }
	inline void set_U3CScaleMultiplierYU3Ek__BackingField_51(float value)
	{
		___U3CScaleMultiplierYU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CZoomSpeedU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CZoomSpeedU3Ek__BackingField_52)); }
	inline float get_U3CZoomSpeedU3Ek__BackingField_52() const { return ___U3CZoomSpeedU3Ek__BackingField_52; }
	inline float* get_address_of_U3CZoomSpeedU3Ek__BackingField_52() { return &___U3CZoomSpeedU3Ek__BackingField_52; }
	inline void set_U3CZoomSpeedU3Ek__BackingField_52(float value)
	{
		___U3CZoomSpeedU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizer_t1137887245, ___U3CThresholdUnitsU3Ek__BackingField_53)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_53() const { return ___U3CThresholdUnitsU3Ek__BackingField_53; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_53() { return &___U3CThresholdUnitsU3Ek__BackingField_53; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_53(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEGESTURERECOGNIZER_T1137887245_H
#ifndef SWIPEGESTURERECOGNIZER_T2328511861_H
#define SWIPEGESTURERECOGNIZER_T2328511861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.SwipeGestureRecognizer
struct  SwipeGestureRecognizer_t2328511861  : public GestureRecognizer_t3684029681
{
public:
	// DigitalRubyShared.SwipeGestureRecognizerDirection DigitalRubyShared.SwipeGestureRecognizer::<Direction>k__BackingField
	int32_t ___U3CDirectionU3Ek__BackingField_37;
	// System.Single DigitalRubyShared.SwipeGestureRecognizer::<MinimumDistanceUnits>k__BackingField
	float ___U3CMinimumDistanceUnitsU3Ek__BackingField_38;
	// System.Single DigitalRubyShared.SwipeGestureRecognizer::<MinimumSpeedUnits>k__BackingField
	float ___U3CMinimumSpeedUnitsU3Ek__BackingField_39;
	// System.Single DigitalRubyShared.SwipeGestureRecognizer::<DirectionThreshold>k__BackingField
	float ___U3CDirectionThresholdU3Ek__BackingField_40;
	// DigitalRubyShared.SwipeGestureRecognizerEndMode DigitalRubyShared.SwipeGestureRecognizer::<EndMode>k__BackingField
	int32_t ___U3CEndModeU3Ek__BackingField_41;
	// System.Boolean DigitalRubyShared.SwipeGestureRecognizer::<FailOnDirectionChange>k__BackingField
	bool ___U3CFailOnDirectionChangeU3Ek__BackingField_42;
	// DigitalRubyShared.SwipeGestureRecognizerDirection DigitalRubyShared.SwipeGestureRecognizer::<EndDirection>k__BackingField
	int32_t ___U3CEndDirectionU3Ek__BackingField_43;

public:
	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CDirectionU3Ek__BackingField_37)); }
	inline int32_t get_U3CDirectionU3Ek__BackingField_37() const { return ___U3CDirectionU3Ek__BackingField_37; }
	inline int32_t* get_address_of_U3CDirectionU3Ek__BackingField_37() { return &___U3CDirectionU3Ek__BackingField_37; }
	inline void set_U3CDirectionU3Ek__BackingField_37(int32_t value)
	{
		___U3CDirectionU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDistanceUnitsU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CMinimumDistanceUnitsU3Ek__BackingField_38)); }
	inline float get_U3CMinimumDistanceUnitsU3Ek__BackingField_38() const { return ___U3CMinimumDistanceUnitsU3Ek__BackingField_38; }
	inline float* get_address_of_U3CMinimumDistanceUnitsU3Ek__BackingField_38() { return &___U3CMinimumDistanceUnitsU3Ek__BackingField_38; }
	inline void set_U3CMinimumDistanceUnitsU3Ek__BackingField_38(float value)
	{
		___U3CMinimumDistanceUnitsU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumSpeedUnitsU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CMinimumSpeedUnitsU3Ek__BackingField_39)); }
	inline float get_U3CMinimumSpeedUnitsU3Ek__BackingField_39() const { return ___U3CMinimumSpeedUnitsU3Ek__BackingField_39; }
	inline float* get_address_of_U3CMinimumSpeedUnitsU3Ek__BackingField_39() { return &___U3CMinimumSpeedUnitsU3Ek__BackingField_39; }
	inline void set_U3CMinimumSpeedUnitsU3Ek__BackingField_39(float value)
	{
		___U3CMinimumSpeedUnitsU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionThresholdU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CDirectionThresholdU3Ek__BackingField_40)); }
	inline float get_U3CDirectionThresholdU3Ek__BackingField_40() const { return ___U3CDirectionThresholdU3Ek__BackingField_40; }
	inline float* get_address_of_U3CDirectionThresholdU3Ek__BackingField_40() { return &___U3CDirectionThresholdU3Ek__BackingField_40; }
	inline void set_U3CDirectionThresholdU3Ek__BackingField_40(float value)
	{
		___U3CDirectionThresholdU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CEndModeU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CEndModeU3Ek__BackingField_41)); }
	inline int32_t get_U3CEndModeU3Ek__BackingField_41() const { return ___U3CEndModeU3Ek__BackingField_41; }
	inline int32_t* get_address_of_U3CEndModeU3Ek__BackingField_41() { return &___U3CEndModeU3Ek__BackingField_41; }
	inline void set_U3CEndModeU3Ek__BackingField_41(int32_t value)
	{
		___U3CEndModeU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CFailOnDirectionChangeU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CFailOnDirectionChangeU3Ek__BackingField_42)); }
	inline bool get_U3CFailOnDirectionChangeU3Ek__BackingField_42() const { return ___U3CFailOnDirectionChangeU3Ek__BackingField_42; }
	inline bool* get_address_of_U3CFailOnDirectionChangeU3Ek__BackingField_42() { return &___U3CFailOnDirectionChangeU3Ek__BackingField_42; }
	inline void set_U3CFailOnDirectionChangeU3Ek__BackingField_42(bool value)
	{
		___U3CFailOnDirectionChangeU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CEndDirectionU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizer_t2328511861, ___U3CEndDirectionU3Ek__BackingField_43)); }
	inline int32_t get_U3CEndDirectionU3Ek__BackingField_43() const { return ___U3CEndDirectionU3Ek__BackingField_43; }
	inline int32_t* get_address_of_U3CEndDirectionU3Ek__BackingField_43() { return &___U3CEndDirectionU3Ek__BackingField_43; }
	inline void set_U3CEndDirectionU3Ek__BackingField_43(int32_t value)
	{
		___U3CEndDirectionU3Ek__BackingField_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTURERECOGNIZER_T2328511861_H
#ifndef TAPGESTURERECOGNIZER_T3178883670_H
#define TAPGESTURERECOGNIZER_T3178883670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.TapGestureRecognizer
struct  TapGestureRecognizer_t3178883670  : public GestureRecognizer_t3684029681
{
public:
	// System.Int32 DigitalRubyShared.TapGestureRecognizer::tapCount
	int32_t ___tapCount_37;
	// System.Diagnostics.Stopwatch DigitalRubyShared.TapGestureRecognizer::timer
	Stopwatch_t305734070 * ___timer_38;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.TapGestureRecognizer::tapTouches
	List_1_t3464476875 * ___tapTouches_39;
	// System.Int32 DigitalRubyShared.TapGestureRecognizer::<NumberOfTapsRequired>k__BackingField
	int32_t ___U3CNumberOfTapsRequiredU3Ek__BackingField_40;
	// System.Single DigitalRubyShared.TapGestureRecognizer::<ThresholdSeconds>k__BackingField
	float ___U3CThresholdSecondsU3Ek__BackingField_41;
	// System.Single DigitalRubyShared.TapGestureRecognizer::<ThresholdUnits>k__BackingField
	float ___U3CThresholdUnitsU3Ek__BackingField_42;
	// System.Boolean DigitalRubyShared.TapGestureRecognizer::<SendBeginState>k__BackingField
	bool ___U3CSendBeginStateU3Ek__BackingField_43;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.TapGestureRecognizer::<TapTouches>k__BackingField
	ReadOnlyCollection_1_t3204978420 * ___U3CTapTouchesU3Ek__BackingField_44;

public:
	inline static int32_t get_offset_of_tapCount_37() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___tapCount_37)); }
	inline int32_t get_tapCount_37() const { return ___tapCount_37; }
	inline int32_t* get_address_of_tapCount_37() { return &___tapCount_37; }
	inline void set_tapCount_37(int32_t value)
	{
		___tapCount_37 = value;
	}

	inline static int32_t get_offset_of_timer_38() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___timer_38)); }
	inline Stopwatch_t305734070 * get_timer_38() const { return ___timer_38; }
	inline Stopwatch_t305734070 ** get_address_of_timer_38() { return &___timer_38; }
	inline void set_timer_38(Stopwatch_t305734070 * value)
	{
		___timer_38 = value;
		Il2CppCodeGenWriteBarrier((&___timer_38), value);
	}

	inline static int32_t get_offset_of_tapTouches_39() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___tapTouches_39)); }
	inline List_1_t3464476875 * get_tapTouches_39() const { return ___tapTouches_39; }
	inline List_1_t3464476875 ** get_address_of_tapTouches_39() { return &___tapTouches_39; }
	inline void set_tapTouches_39(List_1_t3464476875 * value)
	{
		___tapTouches_39 = value;
		Il2CppCodeGenWriteBarrier((&___tapTouches_39), value);
	}

	inline static int32_t get_offset_of_U3CNumberOfTapsRequiredU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CNumberOfTapsRequiredU3Ek__BackingField_40)); }
	inline int32_t get_U3CNumberOfTapsRequiredU3Ek__BackingField_40() const { return ___U3CNumberOfTapsRequiredU3Ek__BackingField_40; }
	inline int32_t* get_address_of_U3CNumberOfTapsRequiredU3Ek__BackingField_40() { return &___U3CNumberOfTapsRequiredU3Ek__BackingField_40; }
	inline void set_U3CNumberOfTapsRequiredU3Ek__BackingField_40(int32_t value)
	{
		___U3CNumberOfTapsRequiredU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdSecondsU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CThresholdSecondsU3Ek__BackingField_41)); }
	inline float get_U3CThresholdSecondsU3Ek__BackingField_41() const { return ___U3CThresholdSecondsU3Ek__BackingField_41; }
	inline float* get_address_of_U3CThresholdSecondsU3Ek__BackingField_41() { return &___U3CThresholdSecondsU3Ek__BackingField_41; }
	inline void set_U3CThresholdSecondsU3Ek__BackingField_41(float value)
	{
		___U3CThresholdSecondsU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CThresholdUnitsU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CThresholdUnitsU3Ek__BackingField_42)); }
	inline float get_U3CThresholdUnitsU3Ek__BackingField_42() const { return ___U3CThresholdUnitsU3Ek__BackingField_42; }
	inline float* get_address_of_U3CThresholdUnitsU3Ek__BackingField_42() { return &___U3CThresholdUnitsU3Ek__BackingField_42; }
	inline void set_U3CThresholdUnitsU3Ek__BackingField_42(float value)
	{
		___U3CThresholdUnitsU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CSendBeginStateU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CSendBeginStateU3Ek__BackingField_43)); }
	inline bool get_U3CSendBeginStateU3Ek__BackingField_43() const { return ___U3CSendBeginStateU3Ek__BackingField_43; }
	inline bool* get_address_of_U3CSendBeginStateU3Ek__BackingField_43() { return &___U3CSendBeginStateU3Ek__BackingField_43; }
	inline void set_U3CSendBeginStateU3Ek__BackingField_43(bool value)
	{
		___U3CSendBeginStateU3Ek__BackingField_43 = value;
	}

	inline static int32_t get_offset_of_U3CTapTouchesU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(TapGestureRecognizer_t3178883670, ___U3CTapTouchesU3Ek__BackingField_44)); }
	inline ReadOnlyCollection_1_t3204978420 * get_U3CTapTouchesU3Ek__BackingField_44() const { return ___U3CTapTouchesU3Ek__BackingField_44; }
	inline ReadOnlyCollection_1_t3204978420 ** get_address_of_U3CTapTouchesU3Ek__BackingField_44() { return &___U3CTapTouchesU3Ek__BackingField_44; }
	inline void set_U3CTapTouchesU3Ek__BackingField_44(ReadOnlyCollection_1_t3204978420 * value)
	{
		___U3CTapTouchesU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTapTouchesU3Ek__BackingField_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPGESTURERECOGNIZER_T3178883670_H
#ifndef CONFLICTCALLBACK_T4045994657_H
#define CONFLICTCALLBACK_T4045994657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct  ConflictCallback_t4045994657  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTCALLBACK_T4045994657_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ONETOUCHROTATEGESTURERECOGNIZER_T3272893959_H
#define ONETOUCHROTATEGESTURERECOGNIZER_T3272893959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.OneTouchRotateGestureRecognizer
struct  OneTouchRotateGestureRecognizer_t3272893959  : public RotateGestureRecognizer_t4100246528
{
public:
	// System.Single DigitalRubyShared.OneTouchRotateGestureRecognizer::AnglePointOverrideX
	float ___AnglePointOverrideX_45;
	// System.Single DigitalRubyShared.OneTouchRotateGestureRecognizer::AnglePointOverrideY
	float ___AnglePointOverrideY_46;

public:
	inline static int32_t get_offset_of_AnglePointOverrideX_45() { return static_cast<int32_t>(offsetof(OneTouchRotateGestureRecognizer_t3272893959, ___AnglePointOverrideX_45)); }
	inline float get_AnglePointOverrideX_45() const { return ___AnglePointOverrideX_45; }
	inline float* get_address_of_AnglePointOverrideX_45() { return &___AnglePointOverrideX_45; }
	inline void set_AnglePointOverrideX_45(float value)
	{
		___AnglePointOverrideX_45 = value;
	}

	inline static int32_t get_offset_of_AnglePointOverrideY_46() { return static_cast<int32_t>(offsetof(OneTouchRotateGestureRecognizer_t3272893959, ___AnglePointOverrideY_46)); }
	inline float get_AnglePointOverrideY_46() const { return ___AnglePointOverrideY_46; }
	inline float* get_address_of_AnglePointOverrideY_46() { return &___AnglePointOverrideY_46; }
	inline void set_AnglePointOverrideY_46(float value)
	{
		___AnglePointOverrideY_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONETOUCHROTATEGESTURERECOGNIZER_T3272893959_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEMOSCRIPTIMAGEHELPER_T36059542_H
#define DEMOSCRIPTIMAGEHELPER_T36059542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptImageHelper
struct  DemoScriptImageHelper_t36059542  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersImageGestureHelperComponentScript DigitalRubyShared.DemoScriptImageHelper::ImageScript
	FingersImageGestureHelperComponentScript_t4247928971 * ___ImageScript_4;
	// UnityEngine.ParticleSystem DigitalRubyShared.DemoScriptImageHelper::MatchParticleSystem
	ParticleSystem_t1800779281 * ___MatchParticleSystem_5;
	// UnityEngine.AudioSource DigitalRubyShared.DemoScriptImageHelper::AudioSourceOnMatch
	AudioSource_t3935305588 * ___AudioSourceOnMatch_6;

public:
	inline static int32_t get_offset_of_ImageScript_4() { return static_cast<int32_t>(offsetof(DemoScriptImageHelper_t36059542, ___ImageScript_4)); }
	inline FingersImageGestureHelperComponentScript_t4247928971 * get_ImageScript_4() const { return ___ImageScript_4; }
	inline FingersImageGestureHelperComponentScript_t4247928971 ** get_address_of_ImageScript_4() { return &___ImageScript_4; }
	inline void set_ImageScript_4(FingersImageGestureHelperComponentScript_t4247928971 * value)
	{
		___ImageScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___ImageScript_4), value);
	}

	inline static int32_t get_offset_of_MatchParticleSystem_5() { return static_cast<int32_t>(offsetof(DemoScriptImageHelper_t36059542, ___MatchParticleSystem_5)); }
	inline ParticleSystem_t1800779281 * get_MatchParticleSystem_5() const { return ___MatchParticleSystem_5; }
	inline ParticleSystem_t1800779281 ** get_address_of_MatchParticleSystem_5() { return &___MatchParticleSystem_5; }
	inline void set_MatchParticleSystem_5(ParticleSystem_t1800779281 * value)
	{
		___MatchParticleSystem_5 = value;
		Il2CppCodeGenWriteBarrier((&___MatchParticleSystem_5), value);
	}

	inline static int32_t get_offset_of_AudioSourceOnMatch_6() { return static_cast<int32_t>(offsetof(DemoScriptImageHelper_t36059542, ___AudioSourceOnMatch_6)); }
	inline AudioSource_t3935305588 * get_AudioSourceOnMatch_6() const { return ___AudioSourceOnMatch_6; }
	inline AudioSource_t3935305588 ** get_address_of_AudioSourceOnMatch_6() { return &___AudioSourceOnMatch_6; }
	inline void set_AudioSourceOnMatch_6(AudioSource_t3935305588 * value)
	{
		___AudioSourceOnMatch_6 = value;
		Il2CppCodeGenWriteBarrier((&___AudioSourceOnMatch_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTIMAGEHELPER_T36059542_H
#ifndef DEMOSCRIPTJOYSTICK_T699687243_H
#define DEMOSCRIPTJOYSTICK_T699687243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptJoystick
struct  DemoScriptJoystick_t699687243  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersJoystickScript DigitalRubyShared.DemoScriptJoystick::JoystickScript
	FingersJoystickScript_t2468414040 * ___JoystickScript_4;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptJoystick::Mover
	GameObject_t1113636619 * ___Mover_5;
	// System.Single DigitalRubyShared.DemoScriptJoystick::Speed
	float ___Speed_6;
	// System.Boolean DigitalRubyShared.DemoScriptJoystick::MoveJoystickToGestureStartLocation
	bool ___MoveJoystickToGestureStartLocation_7;

public:
	inline static int32_t get_offset_of_JoystickScript_4() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___JoystickScript_4)); }
	inline FingersJoystickScript_t2468414040 * get_JoystickScript_4() const { return ___JoystickScript_4; }
	inline FingersJoystickScript_t2468414040 ** get_address_of_JoystickScript_4() { return &___JoystickScript_4; }
	inline void set_JoystickScript_4(FingersJoystickScript_t2468414040 * value)
	{
		___JoystickScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickScript_4), value);
	}

	inline static int32_t get_offset_of_Mover_5() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___Mover_5)); }
	inline GameObject_t1113636619 * get_Mover_5() const { return ___Mover_5; }
	inline GameObject_t1113636619 ** get_address_of_Mover_5() { return &___Mover_5; }
	inline void set_Mover_5(GameObject_t1113636619 * value)
	{
		___Mover_5 = value;
		Il2CppCodeGenWriteBarrier((&___Mover_5), value);
	}

	inline static int32_t get_offset_of_Speed_6() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___Speed_6)); }
	inline float get_Speed_6() const { return ___Speed_6; }
	inline float* get_address_of_Speed_6() { return &___Speed_6; }
	inline void set_Speed_6(float value)
	{
		___Speed_6 = value;
	}

	inline static int32_t get_offset_of_MoveJoystickToGestureStartLocation_7() { return static_cast<int32_t>(offsetof(DemoScriptJoystick_t699687243, ___MoveJoystickToGestureStartLocation_7)); }
	inline bool get_MoveJoystickToGestureStartLocation_7() const { return ___MoveJoystickToGestureStartLocation_7; }
	inline bool* get_address_of_MoveJoystickToGestureStartLocation_7() { return &___MoveJoystickToGestureStartLocation_7; }
	inline void set_MoveJoystickToGestureStartLocation_7(bool value)
	{
		___MoveJoystickToGestureStartLocation_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTJOYSTICK_T699687243_H
#ifndef DEMOSCRIPTMULTIDRAG_T759341426_H
#define DEMOSCRIPTMULTIDRAG_T759341426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptMultiDrag
struct  DemoScriptMultiDrag_t759341426  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptMultiDrag::AsteroidPrefab
	GameObject_t1113636619 * ___AsteroidPrefab_4;
	// System.Int32 DigitalRubyShared.DemoScriptMultiDrag::SpawnCount
	int32_t ___SpawnCount_5;

public:
	inline static int32_t get_offset_of_AsteroidPrefab_4() { return static_cast<int32_t>(offsetof(DemoScriptMultiDrag_t759341426, ___AsteroidPrefab_4)); }
	inline GameObject_t1113636619 * get_AsteroidPrefab_4() const { return ___AsteroidPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_AsteroidPrefab_4() { return &___AsteroidPrefab_4; }
	inline void set_AsteroidPrefab_4(GameObject_t1113636619 * value)
	{
		___AsteroidPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___AsteroidPrefab_4), value);
	}

	inline static int32_t get_offset_of_SpawnCount_5() { return static_cast<int32_t>(offsetof(DemoScriptMultiDrag_t759341426, ___SpawnCount_5)); }
	inline int32_t get_SpawnCount_5() const { return ___SpawnCount_5; }
	inline int32_t* get_address_of_SpawnCount_5() { return &___SpawnCount_5; }
	inline void set_SpawnCount_5(int32_t value)
	{
		___SpawnCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTMULTIDRAG_T759341426_H
#ifndef DEMOSCRIPTMULTIFINGERTAP_T3973590414_H
#define DEMOSCRIPTMULTIFINGERTAP_T3973590414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptMultiFingerTap
struct  DemoScriptMultiFingerTap_t3973590414  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DigitalRubyShared.DemoScriptMultiFingerTap::statusText
	Text_t1901882714 * ___statusText_4;

public:
	inline static int32_t get_offset_of_statusText_4() { return static_cast<int32_t>(offsetof(DemoScriptMultiFingerTap_t3973590414, ___statusText_4)); }
	inline Text_t1901882714 * get_statusText_4() const { return ___statusText_4; }
	inline Text_t1901882714 ** get_address_of_statusText_4() { return &___statusText_4; }
	inline void set_statusText_4(Text_t1901882714 * value)
	{
		___statusText_4 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTMULTIFINGERTAP_T3973590414_H
#ifndef DEMOSCRIPTONEFINGER_T2754275351_H
#define DEMOSCRIPTONEFINGER_T2754275351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptOneFinger
struct  DemoScriptOneFinger_t2754275351  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptOneFinger::RotateIcon
	GameObject_t1113636619 * ___RotateIcon_4;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptOneFinger::ScaleIcon
	GameObject_t1113636619 * ___ScaleIcon_5;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptOneFinger::Earth
	GameObject_t1113636619 * ___Earth_6;
	// DigitalRubyShared.OneTouchRotateGestureRecognizer DigitalRubyShared.DemoScriptOneFinger::rotationGesture
	OneTouchRotateGestureRecognizer_t3272893959 * ___rotationGesture_7;
	// DigitalRubyShared.OneTouchScaleGestureRecognizer DigitalRubyShared.DemoScriptOneFinger::scaleGesture
	OneTouchScaleGestureRecognizer_t1313669683 * ___scaleGesture_8;

public:
	inline static int32_t get_offset_of_RotateIcon_4() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___RotateIcon_4)); }
	inline GameObject_t1113636619 * get_RotateIcon_4() const { return ___RotateIcon_4; }
	inline GameObject_t1113636619 ** get_address_of_RotateIcon_4() { return &___RotateIcon_4; }
	inline void set_RotateIcon_4(GameObject_t1113636619 * value)
	{
		___RotateIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___RotateIcon_4), value);
	}

	inline static int32_t get_offset_of_ScaleIcon_5() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___ScaleIcon_5)); }
	inline GameObject_t1113636619 * get_ScaleIcon_5() const { return ___ScaleIcon_5; }
	inline GameObject_t1113636619 ** get_address_of_ScaleIcon_5() { return &___ScaleIcon_5; }
	inline void set_ScaleIcon_5(GameObject_t1113636619 * value)
	{
		___ScaleIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleIcon_5), value);
	}

	inline static int32_t get_offset_of_Earth_6() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___Earth_6)); }
	inline GameObject_t1113636619 * get_Earth_6() const { return ___Earth_6; }
	inline GameObject_t1113636619 ** get_address_of_Earth_6() { return &___Earth_6; }
	inline void set_Earth_6(GameObject_t1113636619 * value)
	{
		___Earth_6 = value;
		Il2CppCodeGenWriteBarrier((&___Earth_6), value);
	}

	inline static int32_t get_offset_of_rotationGesture_7() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___rotationGesture_7)); }
	inline OneTouchRotateGestureRecognizer_t3272893959 * get_rotationGesture_7() const { return ___rotationGesture_7; }
	inline OneTouchRotateGestureRecognizer_t3272893959 ** get_address_of_rotationGesture_7() { return &___rotationGesture_7; }
	inline void set_rotationGesture_7(OneTouchRotateGestureRecognizer_t3272893959 * value)
	{
		___rotationGesture_7 = value;
		Il2CppCodeGenWriteBarrier((&___rotationGesture_7), value);
	}

	inline static int32_t get_offset_of_scaleGesture_8() { return static_cast<int32_t>(offsetof(DemoScriptOneFinger_t2754275351, ___scaleGesture_8)); }
	inline OneTouchScaleGestureRecognizer_t1313669683 * get_scaleGesture_8() const { return ___scaleGesture_8; }
	inline OneTouchScaleGestureRecognizer_t1313669683 ** get_address_of_scaleGesture_8() { return &___scaleGesture_8; }
	inline void set_scaleGesture_8(OneTouchScaleGestureRecognizer_t1313669683 * value)
	{
		___scaleGesture_8 = value;
		Il2CppCodeGenWriteBarrier((&___scaleGesture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTONEFINGER_T2754275351_H
#ifndef DEMOSCRIPTPAN_T3102497188_H
#define DEMOSCRIPTPAN_T3102497188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptPan
struct  DemoScriptPan_t3102497188  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.FingersScript DigitalRubyShared.DemoScriptPan::FingersScript
	FingersScript_t1857011421 * ___FingersScript_4;
	// UnityEngine.UI.Text DigitalRubyShared.DemoScriptPan::log
	Text_t1901882714 * ___log_5;
	// UnityEngine.UI.ScrollRect DigitalRubyShared.DemoScriptPan::logView
	ScrollRect_t4137855814 * ___logView_6;

public:
	inline static int32_t get_offset_of_FingersScript_4() { return static_cast<int32_t>(offsetof(DemoScriptPan_t3102497188, ___FingersScript_4)); }
	inline FingersScript_t1857011421 * get_FingersScript_4() const { return ___FingersScript_4; }
	inline FingersScript_t1857011421 ** get_address_of_FingersScript_4() { return &___FingersScript_4; }
	inline void set_FingersScript_4(FingersScript_t1857011421 * value)
	{
		___FingersScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___FingersScript_4), value);
	}

	inline static int32_t get_offset_of_log_5() { return static_cast<int32_t>(offsetof(DemoScriptPan_t3102497188, ___log_5)); }
	inline Text_t1901882714 * get_log_5() const { return ___log_5; }
	inline Text_t1901882714 ** get_address_of_log_5() { return &___log_5; }
	inline void set_log_5(Text_t1901882714 * value)
	{
		___log_5 = value;
		Il2CppCodeGenWriteBarrier((&___log_5), value);
	}

	inline static int32_t get_offset_of_logView_6() { return static_cast<int32_t>(offsetof(DemoScriptPan_t3102497188, ___logView_6)); }
	inline ScrollRect_t4137855814 * get_logView_6() const { return ___logView_6; }
	inline ScrollRect_t4137855814 ** get_address_of_logView_6() { return &___logView_6; }
	inline void set_logView_6(ScrollRect_t4137855814 * value)
	{
		___logView_6 = value;
		Il2CppCodeGenWriteBarrier((&___logView_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTPAN_T3102497188_H
#ifndef DEMOSCRIPTPLATFORMSPECIFICVIEW_T3383617572_H
#define DEMOSCRIPTPLATFORMSPECIFICVIEW_T3383617572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptPlatformSpecificView
struct  DemoScriptPlatformSpecificView_t3383617572  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptPlatformSpecificView::LeftPanel
	GameObject_t1113636619 * ___LeftPanel_4;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptPlatformSpecificView::Cube
	GameObject_t1113636619 * ___Cube_5;

public:
	inline static int32_t get_offset_of_LeftPanel_4() { return static_cast<int32_t>(offsetof(DemoScriptPlatformSpecificView_t3383617572, ___LeftPanel_4)); }
	inline GameObject_t1113636619 * get_LeftPanel_4() const { return ___LeftPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_LeftPanel_4() { return &___LeftPanel_4; }
	inline void set_LeftPanel_4(GameObject_t1113636619 * value)
	{
		___LeftPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___LeftPanel_4), value);
	}

	inline static int32_t get_offset_of_Cube_5() { return static_cast<int32_t>(offsetof(DemoScriptPlatformSpecificView_t3383617572, ___Cube_5)); }
	inline GameObject_t1113636619 * get_Cube_5() const { return ___Cube_5; }
	inline GameObject_t1113636619 ** get_address_of_Cube_5() { return &___Cube_5; }
	inline void set_Cube_5(GameObject_t1113636619 * value)
	{
		___Cube_5 = value;
		Il2CppCodeGenWriteBarrier((&___Cube_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTPLATFORMSPECIFICVIEW_T3383617572_H
#ifndef DEMOSCRIPTRAYCAST_T1876970581_H
#define DEMOSCRIPTRAYCAST_T1876970581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptRaycast
struct  DemoScriptRaycast_t1876970581  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] DigitalRubyShared.DemoScriptRaycast::Asteroids
	GameObjectU5BU5D_t3328599146* ___Asteroids_4;
	// UnityEngine.GameObject[] DigitalRubyShared.DemoScriptRaycast::OtherObjects
	GameObjectU5BU5D_t3328599146* ___OtherObjects_5;
	// DigitalRubyShared.TapGestureRecognizer[] DigitalRubyShared.DemoScriptRaycast::TapGestures
	TapGestureRecognizerU5BU5D_t2106712979* ___TapGestures_6;

public:
	inline static int32_t get_offset_of_Asteroids_4() { return static_cast<int32_t>(offsetof(DemoScriptRaycast_t1876970581, ___Asteroids_4)); }
	inline GameObjectU5BU5D_t3328599146* get_Asteroids_4() const { return ___Asteroids_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Asteroids_4() { return &___Asteroids_4; }
	inline void set_Asteroids_4(GameObjectU5BU5D_t3328599146* value)
	{
		___Asteroids_4 = value;
		Il2CppCodeGenWriteBarrier((&___Asteroids_4), value);
	}

	inline static int32_t get_offset_of_OtherObjects_5() { return static_cast<int32_t>(offsetof(DemoScriptRaycast_t1876970581, ___OtherObjects_5)); }
	inline GameObjectU5BU5D_t3328599146* get_OtherObjects_5() const { return ___OtherObjects_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_OtherObjects_5() { return &___OtherObjects_5; }
	inline void set_OtherObjects_5(GameObjectU5BU5D_t3328599146* value)
	{
		___OtherObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___OtherObjects_5), value);
	}

	inline static int32_t get_offset_of_TapGestures_6() { return static_cast<int32_t>(offsetof(DemoScriptRaycast_t1876970581, ___TapGestures_6)); }
	inline TapGestureRecognizerU5BU5D_t2106712979* get_TapGestures_6() const { return ___TapGestures_6; }
	inline TapGestureRecognizerU5BU5D_t2106712979** get_address_of_TapGestures_6() { return &___TapGestures_6; }
	inline void set_TapGestures_6(TapGestureRecognizerU5BU5D_t2106712979* value)
	{
		___TapGestures_6 = value;
		Il2CppCodeGenWriteBarrier((&___TapGestures_6), value);
	}
};

struct DemoScriptRaycast_t1876970581_StaticFields
{
public:
	// System.Func`2<UnityEngine.EventSystems.RaycastResult,System.String> DigitalRubyShared.DemoScriptRaycast::<>f__am$cache0
	Func_2_t3794335314 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(DemoScriptRaycast_t1876970581_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t3794335314 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t3794335314 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t3794335314 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTRAYCAST_T1876970581_H
#ifndef DEMOSCRIPTSWIPE_T1996673777_H
#define DEMOSCRIPTSWIPE_T1996673777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptSwipe
struct  DemoScriptSwipe_t1996673777  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem DigitalRubyShared.DemoScriptSwipe::SwipeParticleSystem
	ParticleSystem_t1800779281 * ___SwipeParticleSystem_4;
	// System.Int32 DigitalRubyShared.DemoScriptSwipe::SwipeTouchCount
	int32_t ___SwipeTouchCount_5;
	// DigitalRubyShared.SwipeGestureRecognizerEndMode DigitalRubyShared.DemoScriptSwipe::SwipeMode
	int32_t ___SwipeMode_6;
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptSwipe::Image
	GameObject_t1113636619 * ___Image_7;
	// DigitalRubyShared.SwipeGestureRecognizer DigitalRubyShared.DemoScriptSwipe::swipe
	SwipeGestureRecognizer_t2328511861 * ___swipe_8;

public:
	inline static int32_t get_offset_of_SwipeParticleSystem_4() { return static_cast<int32_t>(offsetof(DemoScriptSwipe_t1996673777, ___SwipeParticleSystem_4)); }
	inline ParticleSystem_t1800779281 * get_SwipeParticleSystem_4() const { return ___SwipeParticleSystem_4; }
	inline ParticleSystem_t1800779281 ** get_address_of_SwipeParticleSystem_4() { return &___SwipeParticleSystem_4; }
	inline void set_SwipeParticleSystem_4(ParticleSystem_t1800779281 * value)
	{
		___SwipeParticleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___SwipeParticleSystem_4), value);
	}

	inline static int32_t get_offset_of_SwipeTouchCount_5() { return static_cast<int32_t>(offsetof(DemoScriptSwipe_t1996673777, ___SwipeTouchCount_5)); }
	inline int32_t get_SwipeTouchCount_5() const { return ___SwipeTouchCount_5; }
	inline int32_t* get_address_of_SwipeTouchCount_5() { return &___SwipeTouchCount_5; }
	inline void set_SwipeTouchCount_5(int32_t value)
	{
		___SwipeTouchCount_5 = value;
	}

	inline static int32_t get_offset_of_SwipeMode_6() { return static_cast<int32_t>(offsetof(DemoScriptSwipe_t1996673777, ___SwipeMode_6)); }
	inline int32_t get_SwipeMode_6() const { return ___SwipeMode_6; }
	inline int32_t* get_address_of_SwipeMode_6() { return &___SwipeMode_6; }
	inline void set_SwipeMode_6(int32_t value)
	{
		___SwipeMode_6 = value;
	}

	inline static int32_t get_offset_of_Image_7() { return static_cast<int32_t>(offsetof(DemoScriptSwipe_t1996673777, ___Image_7)); }
	inline GameObject_t1113636619 * get_Image_7() const { return ___Image_7; }
	inline GameObject_t1113636619 ** get_address_of_Image_7() { return &___Image_7; }
	inline void set_Image_7(GameObject_t1113636619 * value)
	{
		___Image_7 = value;
		Il2CppCodeGenWriteBarrier((&___Image_7), value);
	}

	inline static int32_t get_offset_of_swipe_8() { return static_cast<int32_t>(offsetof(DemoScriptSwipe_t1996673777, ___swipe_8)); }
	inline SwipeGestureRecognizer_t2328511861 * get_swipe_8() const { return ___swipe_8; }
	inline SwipeGestureRecognizer_t2328511861 ** get_address_of_swipe_8() { return &___swipe_8; }
	inline void set_swipe_8(SwipeGestureRecognizer_t2328511861 * value)
	{
		___swipe_8 = value;
		Il2CppCodeGenWriteBarrier((&___swipe_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTSWIPE_T1996673777_H
#ifndef DEMOSCRIPTTAPANDDOUBLETAP_T888085429_H
#define DEMOSCRIPTTAPANDDOUBLETAP_T888085429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptTapAndDoubleTap
struct  DemoScriptTapAndDoubleTap_t888085429  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScriptTapAndDoubleTap::tapGesture
	TapGestureRecognizer_t3178883670 * ___tapGesture_4;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.DemoScriptTapAndDoubleTap::doubleTapGesture
	TapGestureRecognizer_t3178883670 * ___doubleTapGesture_5;

public:
	inline static int32_t get_offset_of_tapGesture_4() { return static_cast<int32_t>(offsetof(DemoScriptTapAndDoubleTap_t888085429, ___tapGesture_4)); }
	inline TapGestureRecognizer_t3178883670 * get_tapGesture_4() const { return ___tapGesture_4; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tapGesture_4() { return &___tapGesture_4; }
	inline void set_tapGesture_4(TapGestureRecognizer_t3178883670 * value)
	{
		___tapGesture_4 = value;
		Il2CppCodeGenWriteBarrier((&___tapGesture_4), value);
	}

	inline static int32_t get_offset_of_doubleTapGesture_5() { return static_cast<int32_t>(offsetof(DemoScriptTapAndDoubleTap_t888085429, ___doubleTapGesture_5)); }
	inline TapGestureRecognizer_t3178883670 * get_doubleTapGesture_5() const { return ___doubleTapGesture_5; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_doubleTapGesture_5() { return &___doubleTapGesture_5; }
	inline void set_doubleTapGesture_5(TapGestureRecognizer_t3178883670 * value)
	{
		___doubleTapGesture_5 = value;
		Il2CppCodeGenWriteBarrier((&___doubleTapGesture_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTTAPANDDOUBLETAP_T888085429_H
#ifndef DEMOSCRIPTZOOMPANCAMERA_T3831420416_H
#define DEMOSCRIPTZOOMPANCAMERA_T3831420416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptZoomPanCamera
struct  DemoScriptZoomPanCamera_t3831420416  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTZOOMPANCAMERA_T3831420416_H
#ifndef DEMOSCRIPTZOOMABLESCROLLVIEW_T163084534_H
#define DEMOSCRIPTZOOMABLESCROLLVIEW_T163084534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptZoomableScrollView
struct  DemoScriptZoomableScrollView_t163084534  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.DemoScriptZoomableScrollView::ScrollContent
	GameObject_t1113636619 * ___ScrollContent_4;
	// UnityEngine.UI.Text DigitalRubyShared.DemoScriptZoomableScrollView::DebugText
	Text_t1901882714 * ___DebugText_5;
	// UnityEngine.Canvas DigitalRubyShared.DemoScriptZoomableScrollView::Canvas
	Canvas_t3310196443 * ___Canvas_6;
	// UnityEngine.RectTransform DigitalRubyShared.DemoScriptZoomableScrollView::contentRectTransform
	RectTransform_t3704657025 * ___contentRectTransform_7;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::scaleStart
	float ___scaleStart_8;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::scaleEnd
	float ___scaleEnd_9;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::scaleTime
	float ___scaleTime_10;
	// System.Single DigitalRubyShared.DemoScriptZoomableScrollView::elapsedScaleTime
	float ___elapsedScaleTime_11;
	// UnityEngine.Vector2 DigitalRubyShared.DemoScriptZoomableScrollView::scalePosStart
	Vector2_t2156229523  ___scalePosStart_12;
	// UnityEngine.Vector2 DigitalRubyShared.DemoScriptZoomableScrollView::scalePosEnd
	Vector2_t2156229523  ___scalePosEnd_13;
	// UnityEngine.Vector2 DigitalRubyShared.DemoScriptZoomableScrollView::panVelocity
	Vector2_t2156229523  ___panVelocity_14;

public:
	inline static int32_t get_offset_of_ScrollContent_4() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___ScrollContent_4)); }
	inline GameObject_t1113636619 * get_ScrollContent_4() const { return ___ScrollContent_4; }
	inline GameObject_t1113636619 ** get_address_of_ScrollContent_4() { return &___ScrollContent_4; }
	inline void set_ScrollContent_4(GameObject_t1113636619 * value)
	{
		___ScrollContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollContent_4), value);
	}

	inline static int32_t get_offset_of_DebugText_5() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___DebugText_5)); }
	inline Text_t1901882714 * get_DebugText_5() const { return ___DebugText_5; }
	inline Text_t1901882714 ** get_address_of_DebugText_5() { return &___DebugText_5; }
	inline void set_DebugText_5(Text_t1901882714 * value)
	{
		___DebugText_5 = value;
		Il2CppCodeGenWriteBarrier((&___DebugText_5), value);
	}

	inline static int32_t get_offset_of_Canvas_6() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___Canvas_6)); }
	inline Canvas_t3310196443 * get_Canvas_6() const { return ___Canvas_6; }
	inline Canvas_t3310196443 ** get_address_of_Canvas_6() { return &___Canvas_6; }
	inline void set_Canvas_6(Canvas_t3310196443 * value)
	{
		___Canvas_6 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_6), value);
	}

	inline static int32_t get_offset_of_contentRectTransform_7() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___contentRectTransform_7)); }
	inline RectTransform_t3704657025 * get_contentRectTransform_7() const { return ___contentRectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_contentRectTransform_7() { return &___contentRectTransform_7; }
	inline void set_contentRectTransform_7(RectTransform_t3704657025 * value)
	{
		___contentRectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___contentRectTransform_7), value);
	}

	inline static int32_t get_offset_of_scaleStart_8() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scaleStart_8)); }
	inline float get_scaleStart_8() const { return ___scaleStart_8; }
	inline float* get_address_of_scaleStart_8() { return &___scaleStart_8; }
	inline void set_scaleStart_8(float value)
	{
		___scaleStart_8 = value;
	}

	inline static int32_t get_offset_of_scaleEnd_9() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scaleEnd_9)); }
	inline float get_scaleEnd_9() const { return ___scaleEnd_9; }
	inline float* get_address_of_scaleEnd_9() { return &___scaleEnd_9; }
	inline void set_scaleEnd_9(float value)
	{
		___scaleEnd_9 = value;
	}

	inline static int32_t get_offset_of_scaleTime_10() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scaleTime_10)); }
	inline float get_scaleTime_10() const { return ___scaleTime_10; }
	inline float* get_address_of_scaleTime_10() { return &___scaleTime_10; }
	inline void set_scaleTime_10(float value)
	{
		___scaleTime_10 = value;
	}

	inline static int32_t get_offset_of_elapsedScaleTime_11() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___elapsedScaleTime_11)); }
	inline float get_elapsedScaleTime_11() const { return ___elapsedScaleTime_11; }
	inline float* get_address_of_elapsedScaleTime_11() { return &___elapsedScaleTime_11; }
	inline void set_elapsedScaleTime_11(float value)
	{
		___elapsedScaleTime_11 = value;
	}

	inline static int32_t get_offset_of_scalePosStart_12() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scalePosStart_12)); }
	inline Vector2_t2156229523  get_scalePosStart_12() const { return ___scalePosStart_12; }
	inline Vector2_t2156229523 * get_address_of_scalePosStart_12() { return &___scalePosStart_12; }
	inline void set_scalePosStart_12(Vector2_t2156229523  value)
	{
		___scalePosStart_12 = value;
	}

	inline static int32_t get_offset_of_scalePosEnd_13() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___scalePosEnd_13)); }
	inline Vector2_t2156229523  get_scalePosEnd_13() const { return ___scalePosEnd_13; }
	inline Vector2_t2156229523 * get_address_of_scalePosEnd_13() { return &___scalePosEnd_13; }
	inline void set_scalePosEnd_13(Vector2_t2156229523  value)
	{
		___scalePosEnd_13 = value;
	}

	inline static int32_t get_offset_of_panVelocity_14() { return static_cast<int32_t>(offsetof(DemoScriptZoomableScrollView_t163084534, ___panVelocity_14)); }
	inline Vector2_t2156229523  get_panVelocity_14() const { return ___panVelocity_14; }
	inline Vector2_t2156229523 * get_address_of_panVelocity_14() { return &___panVelocity_14; }
	inline void set_panVelocity_14(Vector2_t2156229523  value)
	{
		___panVelocity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTZOOMABLESCROLLVIEW_T163084534_H
#ifndef FINGERSDPADSCRIPT_T3801874975_H
#define FINGERSDPADSCRIPT_T3801874975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersDPadScript
struct  FingersDPadScript_t3801874975  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadBackgroundImage
	Image_t2670269651 * ___DPadBackgroundImage_4;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadUpImageSelected
	Image_t2670269651 * ___DPadUpImageSelected_5;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadRightImageSelected
	Image_t2670269651 * ___DPadRightImageSelected_6;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadDownImageSelected
	Image_t2670269651 * ___DPadDownImageSelected_7;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadLeftImageSelected
	Image_t2670269651 * ___DPadLeftImageSelected_8;
	// UnityEngine.UI.Image DigitalRubyShared.FingersDPadScript::DPadCenterImageSelected
	Image_t2670269651 * ___DPadCenterImageSelected_9;
	// System.Single DigitalRubyShared.FingersDPadScript::TouchRadiusInUnits
	float ___TouchRadiusInUnits_10;
	// UnityEngine.Collider2D[] DigitalRubyShared.FingersDPadScript::overlap
	Collider2DU5BU5D_t1693969295* ___overlap_11;
	// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.TapGestureRecognizer> DigitalRubyShared.FingersDPadScript::DPadItemTapped
	Action_3_t3049541042 * ___DPadItemTapped_12;
	// System.Action`3<DigitalRubyShared.FingersDPadScript,DigitalRubyShared.FingersDPadItem,DigitalRubyShared.PanGestureRecognizer> DigitalRubyShared.FingersDPadScript::DPadItemPanned
	Action_3_t66419768 * ___DPadItemPanned_13;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersDPadScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_14;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersDPadScript::<TapGesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CTapGestureU3Ek__BackingField_15;
	// System.Boolean DigitalRubyShared.FingersDPadScript::<MoveDPadToGestureStartLocation>k__BackingField
	bool ___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_DPadBackgroundImage_4() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadBackgroundImage_4)); }
	inline Image_t2670269651 * get_DPadBackgroundImage_4() const { return ___DPadBackgroundImage_4; }
	inline Image_t2670269651 ** get_address_of_DPadBackgroundImage_4() { return &___DPadBackgroundImage_4; }
	inline void set_DPadBackgroundImage_4(Image_t2670269651 * value)
	{
		___DPadBackgroundImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___DPadBackgroundImage_4), value);
	}

	inline static int32_t get_offset_of_DPadUpImageSelected_5() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadUpImageSelected_5)); }
	inline Image_t2670269651 * get_DPadUpImageSelected_5() const { return ___DPadUpImageSelected_5; }
	inline Image_t2670269651 ** get_address_of_DPadUpImageSelected_5() { return &___DPadUpImageSelected_5; }
	inline void set_DPadUpImageSelected_5(Image_t2670269651 * value)
	{
		___DPadUpImageSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___DPadUpImageSelected_5), value);
	}

	inline static int32_t get_offset_of_DPadRightImageSelected_6() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadRightImageSelected_6)); }
	inline Image_t2670269651 * get_DPadRightImageSelected_6() const { return ___DPadRightImageSelected_6; }
	inline Image_t2670269651 ** get_address_of_DPadRightImageSelected_6() { return &___DPadRightImageSelected_6; }
	inline void set_DPadRightImageSelected_6(Image_t2670269651 * value)
	{
		___DPadRightImageSelected_6 = value;
		Il2CppCodeGenWriteBarrier((&___DPadRightImageSelected_6), value);
	}

	inline static int32_t get_offset_of_DPadDownImageSelected_7() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadDownImageSelected_7)); }
	inline Image_t2670269651 * get_DPadDownImageSelected_7() const { return ___DPadDownImageSelected_7; }
	inline Image_t2670269651 ** get_address_of_DPadDownImageSelected_7() { return &___DPadDownImageSelected_7; }
	inline void set_DPadDownImageSelected_7(Image_t2670269651 * value)
	{
		___DPadDownImageSelected_7 = value;
		Il2CppCodeGenWriteBarrier((&___DPadDownImageSelected_7), value);
	}

	inline static int32_t get_offset_of_DPadLeftImageSelected_8() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadLeftImageSelected_8)); }
	inline Image_t2670269651 * get_DPadLeftImageSelected_8() const { return ___DPadLeftImageSelected_8; }
	inline Image_t2670269651 ** get_address_of_DPadLeftImageSelected_8() { return &___DPadLeftImageSelected_8; }
	inline void set_DPadLeftImageSelected_8(Image_t2670269651 * value)
	{
		___DPadLeftImageSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___DPadLeftImageSelected_8), value);
	}

	inline static int32_t get_offset_of_DPadCenterImageSelected_9() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadCenterImageSelected_9)); }
	inline Image_t2670269651 * get_DPadCenterImageSelected_9() const { return ___DPadCenterImageSelected_9; }
	inline Image_t2670269651 ** get_address_of_DPadCenterImageSelected_9() { return &___DPadCenterImageSelected_9; }
	inline void set_DPadCenterImageSelected_9(Image_t2670269651 * value)
	{
		___DPadCenterImageSelected_9 = value;
		Il2CppCodeGenWriteBarrier((&___DPadCenterImageSelected_9), value);
	}

	inline static int32_t get_offset_of_TouchRadiusInUnits_10() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___TouchRadiusInUnits_10)); }
	inline float get_TouchRadiusInUnits_10() const { return ___TouchRadiusInUnits_10; }
	inline float* get_address_of_TouchRadiusInUnits_10() { return &___TouchRadiusInUnits_10; }
	inline void set_TouchRadiusInUnits_10(float value)
	{
		___TouchRadiusInUnits_10 = value;
	}

	inline static int32_t get_offset_of_overlap_11() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___overlap_11)); }
	inline Collider2DU5BU5D_t1693969295* get_overlap_11() const { return ___overlap_11; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_overlap_11() { return &___overlap_11; }
	inline void set_overlap_11(Collider2DU5BU5D_t1693969295* value)
	{
		___overlap_11 = value;
		Il2CppCodeGenWriteBarrier((&___overlap_11), value);
	}

	inline static int32_t get_offset_of_DPadItemTapped_12() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadItemTapped_12)); }
	inline Action_3_t3049541042 * get_DPadItemTapped_12() const { return ___DPadItemTapped_12; }
	inline Action_3_t3049541042 ** get_address_of_DPadItemTapped_12() { return &___DPadItemTapped_12; }
	inline void set_DPadItemTapped_12(Action_3_t3049541042 * value)
	{
		___DPadItemTapped_12 = value;
		Il2CppCodeGenWriteBarrier((&___DPadItemTapped_12), value);
	}

	inline static int32_t get_offset_of_DPadItemPanned_13() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___DPadItemPanned_13)); }
	inline Action_3_t66419768 * get_DPadItemPanned_13() const { return ___DPadItemPanned_13; }
	inline Action_3_t66419768 ** get_address_of_DPadItemPanned_13() { return &___DPadItemPanned_13; }
	inline void set_DPadItemPanned_13(Action_3_t66419768 * value)
	{
		___DPadItemPanned_13 = value;
		Il2CppCodeGenWriteBarrier((&___DPadItemPanned_13), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___U3CPanGestureU3Ek__BackingField_14)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_14() const { return ___U3CPanGestureU3Ek__BackingField_14; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_14() { return &___U3CPanGestureU3Ek__BackingField_14; }
	inline void set_U3CPanGestureU3Ek__BackingField_14(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CTapGestureU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___U3CTapGestureU3Ek__BackingField_15)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CTapGestureU3Ek__BackingField_15() const { return ___U3CTapGestureU3Ek__BackingField_15; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CTapGestureU3Ek__BackingField_15() { return &___U3CTapGestureU3Ek__BackingField_15; }
	inline void set_U3CTapGestureU3Ek__BackingField_15(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CTapGestureU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTapGestureU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(FingersDPadScript_t3801874975, ___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16)); }
	inline bool get_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16() const { return ___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16() { return &___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16; }
	inline void set_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16(bool value)
	{
		___U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSDPADSCRIPT_T3801874975_H
#ifndef FINGERSDRAGDROPCOMPONENTSCRIPT_T1907472512_H
#define FINGERSDRAGDROPCOMPONENTSCRIPT_T1907472512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersDragDropComponentScript
struct  FingersDragDropComponentScript_t1907472512  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera DigitalRubyShared.FingersDragDropComponentScript::Camera
	Camera_t4157153871 * ___Camera_4;
	// System.Boolean DigitalRubyShared.FingersDragDropComponentScript::BringToFront
	bool ___BringToFront_5;
	// System.Single DigitalRubyShared.FingersDragDropComponentScript::DragScale
	float ___DragScale_6;
	// DigitalRubyShared.LongPressGestureRecognizer DigitalRubyShared.FingersDragDropComponentScript::<LongPressGesture>k__BackingField
	LongPressGestureRecognizer_t3980777482 * ___U3CLongPressGestureU3Ek__BackingField_7;
	// UnityEngine.Rigidbody2D DigitalRubyShared.FingersDragDropComponentScript::rigidBody
	Rigidbody2D_t939494601 * ___rigidBody_8;
	// UnityEngine.SpriteRenderer DigitalRubyShared.FingersDragDropComponentScript::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_9;
	// System.Int32 DigitalRubyShared.FingersDragDropComponentScript::startSortOrder
	int32_t ___startSortOrder_10;
	// System.Single DigitalRubyShared.FingersDragDropComponentScript::panZ
	float ___panZ_11;
	// UnityEngine.Vector3 DigitalRubyShared.FingersDragDropComponentScript::panOffset
	Vector3_t3722313464  ___panOffset_12;
	// System.EventHandler DigitalRubyShared.FingersDragDropComponentScript::DragStarted
	EventHandler_t1348719766 * ___DragStarted_13;
	// System.EventHandler DigitalRubyShared.FingersDragDropComponentScript::DragUpdated
	EventHandler_t1348719766 * ___DragUpdated_14;
	// System.EventHandler DigitalRubyShared.FingersDragDropComponentScript::DragEnded
	EventHandler_t1348719766 * ___DragEnded_15;

public:
	inline static int32_t get_offset_of_Camera_4() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___Camera_4)); }
	inline Camera_t4157153871 * get_Camera_4() const { return ___Camera_4; }
	inline Camera_t4157153871 ** get_address_of_Camera_4() { return &___Camera_4; }
	inline void set_Camera_4(Camera_t4157153871 * value)
	{
		___Camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_4), value);
	}

	inline static int32_t get_offset_of_BringToFront_5() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___BringToFront_5)); }
	inline bool get_BringToFront_5() const { return ___BringToFront_5; }
	inline bool* get_address_of_BringToFront_5() { return &___BringToFront_5; }
	inline void set_BringToFront_5(bool value)
	{
		___BringToFront_5 = value;
	}

	inline static int32_t get_offset_of_DragScale_6() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___DragScale_6)); }
	inline float get_DragScale_6() const { return ___DragScale_6; }
	inline float* get_address_of_DragScale_6() { return &___DragScale_6; }
	inline void set_DragScale_6(float value)
	{
		___DragScale_6 = value;
	}

	inline static int32_t get_offset_of_U3CLongPressGestureU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___U3CLongPressGestureU3Ek__BackingField_7)); }
	inline LongPressGestureRecognizer_t3980777482 * get_U3CLongPressGestureU3Ek__BackingField_7() const { return ___U3CLongPressGestureU3Ek__BackingField_7; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_U3CLongPressGestureU3Ek__BackingField_7() { return &___U3CLongPressGestureU3Ek__BackingField_7; }
	inline void set_U3CLongPressGestureU3Ek__BackingField_7(LongPressGestureRecognizer_t3980777482 * value)
	{
		___U3CLongPressGestureU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLongPressGestureU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_rigidBody_8() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___rigidBody_8)); }
	inline Rigidbody2D_t939494601 * get_rigidBody_8() const { return ___rigidBody_8; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidBody_8() { return &___rigidBody_8; }
	inline void set_rigidBody_8(Rigidbody2D_t939494601 * value)
	{
		___rigidBody_8 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_8), value);
	}

	inline static int32_t get_offset_of_spriteRenderer_9() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___spriteRenderer_9)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_9() const { return ___spriteRenderer_9; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_9() { return &___spriteRenderer_9; }
	inline void set_spriteRenderer_9(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_9 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_9), value);
	}

	inline static int32_t get_offset_of_startSortOrder_10() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___startSortOrder_10)); }
	inline int32_t get_startSortOrder_10() const { return ___startSortOrder_10; }
	inline int32_t* get_address_of_startSortOrder_10() { return &___startSortOrder_10; }
	inline void set_startSortOrder_10(int32_t value)
	{
		___startSortOrder_10 = value;
	}

	inline static int32_t get_offset_of_panZ_11() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___panZ_11)); }
	inline float get_panZ_11() const { return ___panZ_11; }
	inline float* get_address_of_panZ_11() { return &___panZ_11; }
	inline void set_panZ_11(float value)
	{
		___panZ_11 = value;
	}

	inline static int32_t get_offset_of_panOffset_12() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___panOffset_12)); }
	inline Vector3_t3722313464  get_panOffset_12() const { return ___panOffset_12; }
	inline Vector3_t3722313464 * get_address_of_panOffset_12() { return &___panOffset_12; }
	inline void set_panOffset_12(Vector3_t3722313464  value)
	{
		___panOffset_12 = value;
	}

	inline static int32_t get_offset_of_DragStarted_13() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___DragStarted_13)); }
	inline EventHandler_t1348719766 * get_DragStarted_13() const { return ___DragStarted_13; }
	inline EventHandler_t1348719766 ** get_address_of_DragStarted_13() { return &___DragStarted_13; }
	inline void set_DragStarted_13(EventHandler_t1348719766 * value)
	{
		___DragStarted_13 = value;
		Il2CppCodeGenWriteBarrier((&___DragStarted_13), value);
	}

	inline static int32_t get_offset_of_DragUpdated_14() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___DragUpdated_14)); }
	inline EventHandler_t1348719766 * get_DragUpdated_14() const { return ___DragUpdated_14; }
	inline EventHandler_t1348719766 ** get_address_of_DragUpdated_14() { return &___DragUpdated_14; }
	inline void set_DragUpdated_14(EventHandler_t1348719766 * value)
	{
		___DragUpdated_14 = value;
		Il2CppCodeGenWriteBarrier((&___DragUpdated_14), value);
	}

	inline static int32_t get_offset_of_DragEnded_15() { return static_cast<int32_t>(offsetof(FingersDragDropComponentScript_t1907472512, ___DragEnded_15)); }
	inline EventHandler_t1348719766 * get_DragEnded_15() const { return ___DragEnded_15; }
	inline EventHandler_t1348719766 ** get_address_of_DragEnded_15() { return &___DragEnded_15; }
	inline void set_DragEnded_15(EventHandler_t1348719766 * value)
	{
		___DragEnded_15 = value;
		Il2CppCodeGenWriteBarrier((&___DragEnded_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSDRAGDROPCOMPONENTSCRIPT_T1907472512_H
#ifndef FINGERSIMAGEAUTOMATIONSCRIPT_T4118243268_H
#define FINGERSIMAGEAUTOMATIONSCRIPT_T4118243268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersImageAutomationScript
struct  FingersImageAutomationScript_t4118243268  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.RawImage DigitalRubyShared.FingersImageAutomationScript::Image
	RawImage_t3182918964 * ___Image_4;
	// DigitalRubyShared.ImageGestureRecognizer DigitalRubyShared.FingersImageAutomationScript::ImageGesture
	ImageGestureRecognizer_t4233185475 * ___ImageGesture_5;
	// UnityEngine.Material DigitalRubyShared.FingersImageAutomationScript::LineMaterial
	Material_t340375123 * ___LineMaterial_6;
	// UnityEngine.UI.Text DigitalRubyShared.FingersImageAutomationScript::MatchLabel
	Text_t1901882714 * ___MatchLabel_7;
	// UnityEngine.UI.InputField DigitalRubyShared.FingersImageAutomationScript::ScriptText
	InputField_t3762917431 * ___ScriptText_8;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.FingersImageAutomationScript::<LastImage>k__BackingField
	ImageGestureImage_t2885596271 * ___U3CLastImageU3Ek__BackingField_9;
	// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String> DigitalRubyShared.FingersImageAutomationScript::RecognizableImages
	Dictionary_2_t3194170630 * ___RecognizableImages_10;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>> DigitalRubyShared.FingersImageAutomationScript::lineSet
	List_1_t805411711 * ___lineSet_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> DigitalRubyShared.FingersImageAutomationScript::currentPointList
	List_1_t3628304265 * ___currentPointList_12;

public:
	inline static int32_t get_offset_of_Image_4() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___Image_4)); }
	inline RawImage_t3182918964 * get_Image_4() const { return ___Image_4; }
	inline RawImage_t3182918964 ** get_address_of_Image_4() { return &___Image_4; }
	inline void set_Image_4(RawImage_t3182918964 * value)
	{
		___Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___Image_4), value);
	}

	inline static int32_t get_offset_of_ImageGesture_5() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___ImageGesture_5)); }
	inline ImageGestureRecognizer_t4233185475 * get_ImageGesture_5() const { return ___ImageGesture_5; }
	inline ImageGestureRecognizer_t4233185475 ** get_address_of_ImageGesture_5() { return &___ImageGesture_5; }
	inline void set_ImageGesture_5(ImageGestureRecognizer_t4233185475 * value)
	{
		___ImageGesture_5 = value;
		Il2CppCodeGenWriteBarrier((&___ImageGesture_5), value);
	}

	inline static int32_t get_offset_of_LineMaterial_6() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___LineMaterial_6)); }
	inline Material_t340375123 * get_LineMaterial_6() const { return ___LineMaterial_6; }
	inline Material_t340375123 ** get_address_of_LineMaterial_6() { return &___LineMaterial_6; }
	inline void set_LineMaterial_6(Material_t340375123 * value)
	{
		___LineMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_6), value);
	}

	inline static int32_t get_offset_of_MatchLabel_7() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___MatchLabel_7)); }
	inline Text_t1901882714 * get_MatchLabel_7() const { return ___MatchLabel_7; }
	inline Text_t1901882714 ** get_address_of_MatchLabel_7() { return &___MatchLabel_7; }
	inline void set_MatchLabel_7(Text_t1901882714 * value)
	{
		___MatchLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___MatchLabel_7), value);
	}

	inline static int32_t get_offset_of_ScriptText_8() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___ScriptText_8)); }
	inline InputField_t3762917431 * get_ScriptText_8() const { return ___ScriptText_8; }
	inline InputField_t3762917431 ** get_address_of_ScriptText_8() { return &___ScriptText_8; }
	inline void set_ScriptText_8(InputField_t3762917431 * value)
	{
		___ScriptText_8 = value;
		Il2CppCodeGenWriteBarrier((&___ScriptText_8), value);
	}

	inline static int32_t get_offset_of_U3CLastImageU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___U3CLastImageU3Ek__BackingField_9)); }
	inline ImageGestureImage_t2885596271 * get_U3CLastImageU3Ek__BackingField_9() const { return ___U3CLastImageU3Ek__BackingField_9; }
	inline ImageGestureImage_t2885596271 ** get_address_of_U3CLastImageU3Ek__BackingField_9() { return &___U3CLastImageU3Ek__BackingField_9; }
	inline void set_U3CLastImageU3Ek__BackingField_9(ImageGestureImage_t2885596271 * value)
	{
		___U3CLastImageU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastImageU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_RecognizableImages_10() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___RecognizableImages_10)); }
	inline Dictionary_2_t3194170630 * get_RecognizableImages_10() const { return ___RecognizableImages_10; }
	inline Dictionary_2_t3194170630 ** get_address_of_RecognizableImages_10() { return &___RecognizableImages_10; }
	inline void set_RecognizableImages_10(Dictionary_2_t3194170630 * value)
	{
		___RecognizableImages_10 = value;
		Il2CppCodeGenWriteBarrier((&___RecognizableImages_10), value);
	}

	inline static int32_t get_offset_of_lineSet_11() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___lineSet_11)); }
	inline List_1_t805411711 * get_lineSet_11() const { return ___lineSet_11; }
	inline List_1_t805411711 ** get_address_of_lineSet_11() { return &___lineSet_11; }
	inline void set_lineSet_11(List_1_t805411711 * value)
	{
		___lineSet_11 = value;
		Il2CppCodeGenWriteBarrier((&___lineSet_11), value);
	}

	inline static int32_t get_offset_of_currentPointList_12() { return static_cast<int32_t>(offsetof(FingersImageAutomationScript_t4118243268, ___currentPointList_12)); }
	inline List_1_t3628304265 * get_currentPointList_12() const { return ___currentPointList_12; }
	inline List_1_t3628304265 ** get_address_of_currentPointList_12() { return &___currentPointList_12; }
	inline void set_currentPointList_12(List_1_t3628304265 * value)
	{
		___currentPointList_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentPointList_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSIMAGEAUTOMATIONSCRIPT_T4118243268_H
#ifndef FINGERSJOYSTICKSCRIPT_T2468414040_H
#define FINGERSJOYSTICKSCRIPT_T2468414040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersJoystickScript
struct  FingersJoystickScript_t2468414040  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image DigitalRubyShared.FingersJoystickScript::JoystickImage
	Image_t2670269651 * ___JoystickImage_4;
	// System.Single DigitalRubyShared.FingersJoystickScript::JoystickPower
	float ___JoystickPower_5;
	// System.Single DigitalRubyShared.FingersJoystickScript::MaxExtentPercent
	float ___MaxExtentPercent_6;
	// System.Boolean DigitalRubyShared.FingersJoystickScript::EightAxisMode
	bool ___EightAxisMode_7;
	// UnityEngine.Vector2 DigitalRubyShared.FingersJoystickScript::startCenter
	Vector2_t2156229523  ___startCenter_8;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersJoystickScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_9;
	// System.Action`2<DigitalRubyShared.FingersJoystickScript,UnityEngine.Vector2> DigitalRubyShared.FingersJoystickScript::JoystickExecuted
	Action_2_t1565375025 * ___JoystickExecuted_10;
	// System.Boolean DigitalRubyShared.FingersJoystickScript::<MoveJoystickToGestureStartLocation>k__BackingField
	bool ___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_JoystickImage_4() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___JoystickImage_4)); }
	inline Image_t2670269651 * get_JoystickImage_4() const { return ___JoystickImage_4; }
	inline Image_t2670269651 ** get_address_of_JoystickImage_4() { return &___JoystickImage_4; }
	inline void set_JoystickImage_4(Image_t2670269651 * value)
	{
		___JoystickImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickImage_4), value);
	}

	inline static int32_t get_offset_of_JoystickPower_5() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___JoystickPower_5)); }
	inline float get_JoystickPower_5() const { return ___JoystickPower_5; }
	inline float* get_address_of_JoystickPower_5() { return &___JoystickPower_5; }
	inline void set_JoystickPower_5(float value)
	{
		___JoystickPower_5 = value;
	}

	inline static int32_t get_offset_of_MaxExtentPercent_6() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___MaxExtentPercent_6)); }
	inline float get_MaxExtentPercent_6() const { return ___MaxExtentPercent_6; }
	inline float* get_address_of_MaxExtentPercent_6() { return &___MaxExtentPercent_6; }
	inline void set_MaxExtentPercent_6(float value)
	{
		___MaxExtentPercent_6 = value;
	}

	inline static int32_t get_offset_of_EightAxisMode_7() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___EightAxisMode_7)); }
	inline bool get_EightAxisMode_7() const { return ___EightAxisMode_7; }
	inline bool* get_address_of_EightAxisMode_7() { return &___EightAxisMode_7; }
	inline void set_EightAxisMode_7(bool value)
	{
		___EightAxisMode_7 = value;
	}

	inline static int32_t get_offset_of_startCenter_8() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___startCenter_8)); }
	inline Vector2_t2156229523  get_startCenter_8() const { return ___startCenter_8; }
	inline Vector2_t2156229523 * get_address_of_startCenter_8() { return &___startCenter_8; }
	inline void set_startCenter_8(Vector2_t2156229523  value)
	{
		___startCenter_8 = value;
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___U3CPanGestureU3Ek__BackingField_9)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_9() const { return ___U3CPanGestureU3Ek__BackingField_9; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_9() { return &___U3CPanGestureU3Ek__BackingField_9; }
	inline void set_U3CPanGestureU3Ek__BackingField_9(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_JoystickExecuted_10() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___JoystickExecuted_10)); }
	inline Action_2_t1565375025 * get_JoystickExecuted_10() const { return ___JoystickExecuted_10; }
	inline Action_2_t1565375025 ** get_address_of_JoystickExecuted_10() { return &___JoystickExecuted_10; }
	inline void set_JoystickExecuted_10(Action_2_t1565375025 * value)
	{
		___JoystickExecuted_10 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickExecuted_10), value);
	}

	inline static int32_t get_offset_of_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FingersJoystickScript_t2468414040, ___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11)); }
	inline bool get_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11() const { return ___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11() { return &___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11; }
	inline void set_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11(bool value)
	{
		___U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSJOYSTICKSCRIPT_T2468414040_H
#ifndef FINGERSMULTIDRAGCOMPONENTSCRIPT_T4189350000_H
#define FINGERSMULTIDRAGCOMPONENTSCRIPT_T4189350000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersMultiDragComponentScript
struct  FingersMultiDragComponentScript_t4189350000  : public MonoBehaviour_t3962482529
{
public:
	// System.String DigitalRubyShared.FingersMultiDragComponentScript::TagToDrag
	String_t* ___TagToDrag_4;
	// System.String DigitalRubyShared.FingersMultiDragComponentScript::NameToDrag
	String_t* ___NameToDrag_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,DigitalRubyShared.FingersMultiDragComponentScript/DragState> DigitalRubyShared.FingersMultiDragComponentScript::draggingObjectsByGameObject
	Dictionary_2_t672118465 * ___draggingObjectsByGameObject_6;
	// System.Collections.Generic.Dictionary`2<System.Int32,DigitalRubyShared.FingersMultiDragComponentScript/DragState> DigitalRubyShared.FingersMultiDragComponentScript::draggingObjectsByTouchId
	Dictionary_2_t2023732987 * ___draggingObjectsByTouchId_7;
	// System.Collections.Generic.HashSet`1<System.Int32> DigitalRubyShared.FingersMultiDragComponentScript::allTouchIds
	HashSet_1_t1515895227 * ___allTouchIds_8;
	// System.Collections.Generic.List`1<System.Int32> DigitalRubyShared.FingersMultiDragComponentScript::currentTouchIds
	List_1_t128053199 * ___currentTouchIds_9;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersMultiDragComponentScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_TagToDrag_4() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___TagToDrag_4)); }
	inline String_t* get_TagToDrag_4() const { return ___TagToDrag_4; }
	inline String_t** get_address_of_TagToDrag_4() { return &___TagToDrag_4; }
	inline void set_TagToDrag_4(String_t* value)
	{
		___TagToDrag_4 = value;
		Il2CppCodeGenWriteBarrier((&___TagToDrag_4), value);
	}

	inline static int32_t get_offset_of_NameToDrag_5() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___NameToDrag_5)); }
	inline String_t* get_NameToDrag_5() const { return ___NameToDrag_5; }
	inline String_t** get_address_of_NameToDrag_5() { return &___NameToDrag_5; }
	inline void set_NameToDrag_5(String_t* value)
	{
		___NameToDrag_5 = value;
		Il2CppCodeGenWriteBarrier((&___NameToDrag_5), value);
	}

	inline static int32_t get_offset_of_draggingObjectsByGameObject_6() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___draggingObjectsByGameObject_6)); }
	inline Dictionary_2_t672118465 * get_draggingObjectsByGameObject_6() const { return ___draggingObjectsByGameObject_6; }
	inline Dictionary_2_t672118465 ** get_address_of_draggingObjectsByGameObject_6() { return &___draggingObjectsByGameObject_6; }
	inline void set_draggingObjectsByGameObject_6(Dictionary_2_t672118465 * value)
	{
		___draggingObjectsByGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___draggingObjectsByGameObject_6), value);
	}

	inline static int32_t get_offset_of_draggingObjectsByTouchId_7() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___draggingObjectsByTouchId_7)); }
	inline Dictionary_2_t2023732987 * get_draggingObjectsByTouchId_7() const { return ___draggingObjectsByTouchId_7; }
	inline Dictionary_2_t2023732987 ** get_address_of_draggingObjectsByTouchId_7() { return &___draggingObjectsByTouchId_7; }
	inline void set_draggingObjectsByTouchId_7(Dictionary_2_t2023732987 * value)
	{
		___draggingObjectsByTouchId_7 = value;
		Il2CppCodeGenWriteBarrier((&___draggingObjectsByTouchId_7), value);
	}

	inline static int32_t get_offset_of_allTouchIds_8() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___allTouchIds_8)); }
	inline HashSet_1_t1515895227 * get_allTouchIds_8() const { return ___allTouchIds_8; }
	inline HashSet_1_t1515895227 ** get_address_of_allTouchIds_8() { return &___allTouchIds_8; }
	inline void set_allTouchIds_8(HashSet_1_t1515895227 * value)
	{
		___allTouchIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___allTouchIds_8), value);
	}

	inline static int32_t get_offset_of_currentTouchIds_9() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___currentTouchIds_9)); }
	inline List_1_t128053199 * get_currentTouchIds_9() const { return ___currentTouchIds_9; }
	inline List_1_t128053199 ** get_address_of_currentTouchIds_9() { return &___currentTouchIds_9; }
	inline void set_currentTouchIds_9(List_1_t128053199 * value)
	{
		___currentTouchIds_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentTouchIds_9), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FingersMultiDragComponentScript_t4189350000, ___U3CPanGestureU3Ek__BackingField_10)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_10() const { return ___U3CPanGestureU3Ek__BackingField_10; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_10() { return &___U3CPanGestureU3Ek__BackingField_10; }
	inline void set_U3CPanGestureU3Ek__BackingField_10(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSMULTIDRAGCOMPONENTSCRIPT_T4189350000_H
#ifndef FINGERSPANORBITCOMPONENTSCRIPT_T1753982191_H
#define FINGERSPANORBITCOMPONENTSCRIPT_T1753982191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersPanOrbitComponentScript
struct  FingersPanOrbitComponentScript_t1753982191  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform DigitalRubyShared.FingersPanOrbitComponentScript::OrbitTarget
	Transform_t3600365921 * ___OrbitTarget_4;
	// UnityEngine.Transform DigitalRubyShared.FingersPanOrbitComponentScript::Orbiter
	Transform_t3600365921 * ___Orbiter_5;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::MinimumDistance
	float ___MinimumDistance_6;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::MaximumDistance
	float ___MaximumDistance_7;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::ZoomSpeed
	float ___ZoomSpeed_8;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::ZoomLookAtSpeed
	float ___ZoomLookAtSpeed_9;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::ZoomThresholdUnits
	float ___ZoomThresholdUnits_10;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitXSpeed
	float ___OrbitXSpeed_11;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitXMaxDegrees
	float ___OrbitXMaxDegrees_12;
	// DigitalRubyShared.FingersPanOrbitComponentScript/PanOrbitMovementType DigitalRubyShared.FingersPanOrbitComponentScript::XAxisMovementType
	int32_t ___XAxisMovementType_13;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitXPanSpeed
	float ___OrbitXPanSpeed_14;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitXPanLimit
	float ___OrbitXPanLimit_15;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitYSpeed
	float ___OrbitYSpeed_16;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitYMaxDegrees
	float ___OrbitYMaxDegrees_17;
	// DigitalRubyShared.FingersPanOrbitComponentScript/PanOrbitMovementType DigitalRubyShared.FingersPanOrbitComponentScript::YAxisMovementType
	int32_t ___YAxisMovementType_18;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitYPanSpeed
	float ___OrbitYPanSpeed_19;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitYPanLimit
	float ___OrbitYPanLimit_20;
	// System.Boolean DigitalRubyShared.FingersPanOrbitComponentScript::AllowOrbitWhileZooming
	bool ___AllowOrbitWhileZooming_21;
	// System.Boolean DigitalRubyShared.FingersPanOrbitComponentScript::allowOrbitWhileZooming
	bool ___allowOrbitWhileZooming_22;
	// System.Boolean DigitalRubyShared.FingersPanOrbitComponentScript::AllowMovementOnBothAxisSimultaneously
	bool ___AllowMovementOnBothAxisSimultaneously_23;
	// System.Int32 DigitalRubyShared.FingersPanOrbitComponentScript::lockedAxis
	int32_t ___lockedAxis_24;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::OrbitInertia
	float ___OrbitInertia_25;
	// UnityEngine.Vector3 DigitalRubyShared.FingersPanOrbitComponentScript::OrbitMaximumSize
	Vector3_t3722313464  ___OrbitMaximumSize_26;
	// System.Boolean DigitalRubyShared.FingersPanOrbitComponentScript::RequireOrbitGesturesToStartOnTarget
	bool ___RequireOrbitGesturesToStartOnTarget_27;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.FingersPanOrbitComponentScript::<ScaleGesture>k__BackingField
	ScaleGestureRecognizer_t1137887245 * ___U3CScaleGestureU3Ek__BackingField_28;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersPanOrbitComponentScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_29;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersPanOrbitComponentScript::<TapGesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CTapGestureU3Ek__BackingField_30;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::xDegrees
	float ___xDegrees_31;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::yDegrees
	float ___yDegrees_32;
	// UnityEngine.Vector2 DigitalRubyShared.FingersPanOrbitComponentScript::panVelocity
	Vector2_t2156229523  ___panVelocity_33;
	// System.Single DigitalRubyShared.FingersPanOrbitComponentScript::zoomSpeed
	float ___zoomSpeed_34;
	// System.Action DigitalRubyShared.FingersPanOrbitComponentScript::OrbitTargetTapped
	Action_t1264377477 * ___OrbitTargetTapped_35;

public:
	inline static int32_t get_offset_of_OrbitTarget_4() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitTarget_4)); }
	inline Transform_t3600365921 * get_OrbitTarget_4() const { return ___OrbitTarget_4; }
	inline Transform_t3600365921 ** get_address_of_OrbitTarget_4() { return &___OrbitTarget_4; }
	inline void set_OrbitTarget_4(Transform_t3600365921 * value)
	{
		___OrbitTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitTarget_4), value);
	}

	inline static int32_t get_offset_of_Orbiter_5() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___Orbiter_5)); }
	inline Transform_t3600365921 * get_Orbiter_5() const { return ___Orbiter_5; }
	inline Transform_t3600365921 ** get_address_of_Orbiter_5() { return &___Orbiter_5; }
	inline void set_Orbiter_5(Transform_t3600365921 * value)
	{
		___Orbiter_5 = value;
		Il2CppCodeGenWriteBarrier((&___Orbiter_5), value);
	}

	inline static int32_t get_offset_of_MinimumDistance_6() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___MinimumDistance_6)); }
	inline float get_MinimumDistance_6() const { return ___MinimumDistance_6; }
	inline float* get_address_of_MinimumDistance_6() { return &___MinimumDistance_6; }
	inline void set_MinimumDistance_6(float value)
	{
		___MinimumDistance_6 = value;
	}

	inline static int32_t get_offset_of_MaximumDistance_7() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___MaximumDistance_7)); }
	inline float get_MaximumDistance_7() const { return ___MaximumDistance_7; }
	inline float* get_address_of_MaximumDistance_7() { return &___MaximumDistance_7; }
	inline void set_MaximumDistance_7(float value)
	{
		___MaximumDistance_7 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeed_8() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___ZoomSpeed_8)); }
	inline float get_ZoomSpeed_8() const { return ___ZoomSpeed_8; }
	inline float* get_address_of_ZoomSpeed_8() { return &___ZoomSpeed_8; }
	inline void set_ZoomSpeed_8(float value)
	{
		___ZoomSpeed_8 = value;
	}

	inline static int32_t get_offset_of_ZoomLookAtSpeed_9() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___ZoomLookAtSpeed_9)); }
	inline float get_ZoomLookAtSpeed_9() const { return ___ZoomLookAtSpeed_9; }
	inline float* get_address_of_ZoomLookAtSpeed_9() { return &___ZoomLookAtSpeed_9; }
	inline void set_ZoomLookAtSpeed_9(float value)
	{
		___ZoomLookAtSpeed_9 = value;
	}

	inline static int32_t get_offset_of_ZoomThresholdUnits_10() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___ZoomThresholdUnits_10)); }
	inline float get_ZoomThresholdUnits_10() const { return ___ZoomThresholdUnits_10; }
	inline float* get_address_of_ZoomThresholdUnits_10() { return &___ZoomThresholdUnits_10; }
	inline void set_ZoomThresholdUnits_10(float value)
	{
		___ZoomThresholdUnits_10 = value;
	}

	inline static int32_t get_offset_of_OrbitXSpeed_11() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitXSpeed_11)); }
	inline float get_OrbitXSpeed_11() const { return ___OrbitXSpeed_11; }
	inline float* get_address_of_OrbitXSpeed_11() { return &___OrbitXSpeed_11; }
	inline void set_OrbitXSpeed_11(float value)
	{
		___OrbitXSpeed_11 = value;
	}

	inline static int32_t get_offset_of_OrbitXMaxDegrees_12() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitXMaxDegrees_12)); }
	inline float get_OrbitXMaxDegrees_12() const { return ___OrbitXMaxDegrees_12; }
	inline float* get_address_of_OrbitXMaxDegrees_12() { return &___OrbitXMaxDegrees_12; }
	inline void set_OrbitXMaxDegrees_12(float value)
	{
		___OrbitXMaxDegrees_12 = value;
	}

	inline static int32_t get_offset_of_XAxisMovementType_13() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___XAxisMovementType_13)); }
	inline int32_t get_XAxisMovementType_13() const { return ___XAxisMovementType_13; }
	inline int32_t* get_address_of_XAxisMovementType_13() { return &___XAxisMovementType_13; }
	inline void set_XAxisMovementType_13(int32_t value)
	{
		___XAxisMovementType_13 = value;
	}

	inline static int32_t get_offset_of_OrbitXPanSpeed_14() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitXPanSpeed_14)); }
	inline float get_OrbitXPanSpeed_14() const { return ___OrbitXPanSpeed_14; }
	inline float* get_address_of_OrbitXPanSpeed_14() { return &___OrbitXPanSpeed_14; }
	inline void set_OrbitXPanSpeed_14(float value)
	{
		___OrbitXPanSpeed_14 = value;
	}

	inline static int32_t get_offset_of_OrbitXPanLimit_15() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitXPanLimit_15)); }
	inline float get_OrbitXPanLimit_15() const { return ___OrbitXPanLimit_15; }
	inline float* get_address_of_OrbitXPanLimit_15() { return &___OrbitXPanLimit_15; }
	inline void set_OrbitXPanLimit_15(float value)
	{
		___OrbitXPanLimit_15 = value;
	}

	inline static int32_t get_offset_of_OrbitYSpeed_16() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitYSpeed_16)); }
	inline float get_OrbitYSpeed_16() const { return ___OrbitYSpeed_16; }
	inline float* get_address_of_OrbitYSpeed_16() { return &___OrbitYSpeed_16; }
	inline void set_OrbitYSpeed_16(float value)
	{
		___OrbitYSpeed_16 = value;
	}

	inline static int32_t get_offset_of_OrbitYMaxDegrees_17() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitYMaxDegrees_17)); }
	inline float get_OrbitYMaxDegrees_17() const { return ___OrbitYMaxDegrees_17; }
	inline float* get_address_of_OrbitYMaxDegrees_17() { return &___OrbitYMaxDegrees_17; }
	inline void set_OrbitYMaxDegrees_17(float value)
	{
		___OrbitYMaxDegrees_17 = value;
	}

	inline static int32_t get_offset_of_YAxisMovementType_18() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___YAxisMovementType_18)); }
	inline int32_t get_YAxisMovementType_18() const { return ___YAxisMovementType_18; }
	inline int32_t* get_address_of_YAxisMovementType_18() { return &___YAxisMovementType_18; }
	inline void set_YAxisMovementType_18(int32_t value)
	{
		___YAxisMovementType_18 = value;
	}

	inline static int32_t get_offset_of_OrbitYPanSpeed_19() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitYPanSpeed_19)); }
	inline float get_OrbitYPanSpeed_19() const { return ___OrbitYPanSpeed_19; }
	inline float* get_address_of_OrbitYPanSpeed_19() { return &___OrbitYPanSpeed_19; }
	inline void set_OrbitYPanSpeed_19(float value)
	{
		___OrbitYPanSpeed_19 = value;
	}

	inline static int32_t get_offset_of_OrbitYPanLimit_20() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitYPanLimit_20)); }
	inline float get_OrbitYPanLimit_20() const { return ___OrbitYPanLimit_20; }
	inline float* get_address_of_OrbitYPanLimit_20() { return &___OrbitYPanLimit_20; }
	inline void set_OrbitYPanLimit_20(float value)
	{
		___OrbitYPanLimit_20 = value;
	}

	inline static int32_t get_offset_of_AllowOrbitWhileZooming_21() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___AllowOrbitWhileZooming_21)); }
	inline bool get_AllowOrbitWhileZooming_21() const { return ___AllowOrbitWhileZooming_21; }
	inline bool* get_address_of_AllowOrbitWhileZooming_21() { return &___AllowOrbitWhileZooming_21; }
	inline void set_AllowOrbitWhileZooming_21(bool value)
	{
		___AllowOrbitWhileZooming_21 = value;
	}

	inline static int32_t get_offset_of_allowOrbitWhileZooming_22() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___allowOrbitWhileZooming_22)); }
	inline bool get_allowOrbitWhileZooming_22() const { return ___allowOrbitWhileZooming_22; }
	inline bool* get_address_of_allowOrbitWhileZooming_22() { return &___allowOrbitWhileZooming_22; }
	inline void set_allowOrbitWhileZooming_22(bool value)
	{
		___allowOrbitWhileZooming_22 = value;
	}

	inline static int32_t get_offset_of_AllowMovementOnBothAxisSimultaneously_23() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___AllowMovementOnBothAxisSimultaneously_23)); }
	inline bool get_AllowMovementOnBothAxisSimultaneously_23() const { return ___AllowMovementOnBothAxisSimultaneously_23; }
	inline bool* get_address_of_AllowMovementOnBothAxisSimultaneously_23() { return &___AllowMovementOnBothAxisSimultaneously_23; }
	inline void set_AllowMovementOnBothAxisSimultaneously_23(bool value)
	{
		___AllowMovementOnBothAxisSimultaneously_23 = value;
	}

	inline static int32_t get_offset_of_lockedAxis_24() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___lockedAxis_24)); }
	inline int32_t get_lockedAxis_24() const { return ___lockedAxis_24; }
	inline int32_t* get_address_of_lockedAxis_24() { return &___lockedAxis_24; }
	inline void set_lockedAxis_24(int32_t value)
	{
		___lockedAxis_24 = value;
	}

	inline static int32_t get_offset_of_OrbitInertia_25() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitInertia_25)); }
	inline float get_OrbitInertia_25() const { return ___OrbitInertia_25; }
	inline float* get_address_of_OrbitInertia_25() { return &___OrbitInertia_25; }
	inline void set_OrbitInertia_25(float value)
	{
		___OrbitInertia_25 = value;
	}

	inline static int32_t get_offset_of_OrbitMaximumSize_26() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitMaximumSize_26)); }
	inline Vector3_t3722313464  get_OrbitMaximumSize_26() const { return ___OrbitMaximumSize_26; }
	inline Vector3_t3722313464 * get_address_of_OrbitMaximumSize_26() { return &___OrbitMaximumSize_26; }
	inline void set_OrbitMaximumSize_26(Vector3_t3722313464  value)
	{
		___OrbitMaximumSize_26 = value;
	}

	inline static int32_t get_offset_of_RequireOrbitGesturesToStartOnTarget_27() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___RequireOrbitGesturesToStartOnTarget_27)); }
	inline bool get_RequireOrbitGesturesToStartOnTarget_27() const { return ___RequireOrbitGesturesToStartOnTarget_27; }
	inline bool* get_address_of_RequireOrbitGesturesToStartOnTarget_27() { return &___RequireOrbitGesturesToStartOnTarget_27; }
	inline void set_RequireOrbitGesturesToStartOnTarget_27(bool value)
	{
		___RequireOrbitGesturesToStartOnTarget_27 = value;
	}

	inline static int32_t get_offset_of_U3CScaleGestureU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___U3CScaleGestureU3Ek__BackingField_28)); }
	inline ScaleGestureRecognizer_t1137887245 * get_U3CScaleGestureU3Ek__BackingField_28() const { return ___U3CScaleGestureU3Ek__BackingField_28; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_U3CScaleGestureU3Ek__BackingField_28() { return &___U3CScaleGestureU3Ek__BackingField_28; }
	inline void set_U3CScaleGestureU3Ek__BackingField_28(ScaleGestureRecognizer_t1137887245 * value)
	{
		___U3CScaleGestureU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScaleGestureU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___U3CPanGestureU3Ek__BackingField_29)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_29() const { return ___U3CPanGestureU3Ek__BackingField_29; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_29() { return &___U3CPanGestureU3Ek__BackingField_29; }
	inline void set_U3CPanGestureU3Ek__BackingField_29(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CTapGestureU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___U3CTapGestureU3Ek__BackingField_30)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CTapGestureU3Ek__BackingField_30() const { return ___U3CTapGestureU3Ek__BackingField_30; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CTapGestureU3Ek__BackingField_30() { return &___U3CTapGestureU3Ek__BackingField_30; }
	inline void set_U3CTapGestureU3Ek__BackingField_30(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CTapGestureU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTapGestureU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_xDegrees_31() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___xDegrees_31)); }
	inline float get_xDegrees_31() const { return ___xDegrees_31; }
	inline float* get_address_of_xDegrees_31() { return &___xDegrees_31; }
	inline void set_xDegrees_31(float value)
	{
		___xDegrees_31 = value;
	}

	inline static int32_t get_offset_of_yDegrees_32() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___yDegrees_32)); }
	inline float get_yDegrees_32() const { return ___yDegrees_32; }
	inline float* get_address_of_yDegrees_32() { return &___yDegrees_32; }
	inline void set_yDegrees_32(float value)
	{
		___yDegrees_32 = value;
	}

	inline static int32_t get_offset_of_panVelocity_33() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___panVelocity_33)); }
	inline Vector2_t2156229523  get_panVelocity_33() const { return ___panVelocity_33; }
	inline Vector2_t2156229523 * get_address_of_panVelocity_33() { return &___panVelocity_33; }
	inline void set_panVelocity_33(Vector2_t2156229523  value)
	{
		___panVelocity_33 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_34() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___zoomSpeed_34)); }
	inline float get_zoomSpeed_34() const { return ___zoomSpeed_34; }
	inline float* get_address_of_zoomSpeed_34() { return &___zoomSpeed_34; }
	inline void set_zoomSpeed_34(float value)
	{
		___zoomSpeed_34 = value;
	}

	inline static int32_t get_offset_of_OrbitTargetTapped_35() { return static_cast<int32_t>(offsetof(FingersPanOrbitComponentScript_t1753982191, ___OrbitTargetTapped_35)); }
	inline Action_t1264377477 * get_OrbitTargetTapped_35() const { return ___OrbitTargetTapped_35; }
	inline Action_t1264377477 ** get_address_of_OrbitTargetTapped_35() { return &___OrbitTargetTapped_35; }
	inline void set_OrbitTargetTapped_35(Action_t1264377477 * value)
	{
		___OrbitTargetTapped_35 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitTargetTapped_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSPANORBITCOMPONENTSCRIPT_T1753982191_H
#ifndef FINGERSPANROTATESCALECOMPONENTSCRIPT_T154562084_H
#define FINGERSPANROTATESCALECOMPONENTSCRIPT_T154562084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersPanRotateScaleComponentScript
struct  FingersPanRotateScaleComponentScript_t154562084  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera DigitalRubyShared.FingersPanRotateScaleComponentScript::Camera
	Camera_t4157153871 * ___Camera_4;
	// System.Boolean DigitalRubyShared.FingersPanRotateScaleComponentScript::BringToFront
	bool ___BringToFront_5;
	// System.Boolean DigitalRubyShared.FingersPanRotateScaleComponentScript::AllowExecutionWithAllGestures
	bool ___AllowExecutionWithAllGestures_6;
	// DigitalRubyShared.GestureRecognizerComponentScriptBase/GestureObjectMode DigitalRubyShared.FingersPanRotateScaleComponentScript::Mode
	int32_t ___Mode_7;
	// UnityEngine.Vector2 DigitalRubyShared.FingersPanRotateScaleComponentScript::MinMaxScale
	Vector2_t2156229523  ___MinMaxScale_8;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersPanRotateScaleComponentScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_9;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.FingersPanRotateScaleComponentScript::<ScaleGesture>k__BackingField
	ScaleGestureRecognizer_t1137887245 * ___U3CScaleGestureU3Ek__BackingField_10;
	// DigitalRubyShared.RotateGestureRecognizer DigitalRubyShared.FingersPanRotateScaleComponentScript::<RotateGesture>k__BackingField
	RotateGestureRecognizer_t4100246528 * ___U3CRotateGestureU3Ek__BackingField_11;
	// UnityEngine.Rigidbody2D DigitalRubyShared.FingersPanRotateScaleComponentScript::rigidBody2D
	Rigidbody2D_t939494601 * ___rigidBody2D_12;
	// UnityEngine.Rigidbody DigitalRubyShared.FingersPanRotateScaleComponentScript::rigidBody
	Rigidbody_t3916780224 * ___rigidBody_13;
	// UnityEngine.SpriteRenderer DigitalRubyShared.FingersPanRotateScaleComponentScript::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_14;
	// UnityEngine.CanvasRenderer DigitalRubyShared.FingersPanRotateScaleComponentScript::canvasRenderer
	CanvasRenderer_t2598313366 * ___canvasRenderer_15;
	// UnityEngine.Transform DigitalRubyShared.FingersPanRotateScaleComponentScript::_transform
	Transform_t3600365921 * ____transform_16;
	// System.Int32 DigitalRubyShared.FingersPanRotateScaleComponentScript::startSortOrder
	int32_t ___startSortOrder_17;
	// System.Single DigitalRubyShared.FingersPanRotateScaleComponentScript::panZ
	float ___panZ_18;
	// UnityEngine.Vector3 DigitalRubyShared.FingersPanRotateScaleComponentScript::panOffset
	Vector3_t3722313464  ___panOffset_19;

public:
	inline static int32_t get_offset_of_Camera_4() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___Camera_4)); }
	inline Camera_t4157153871 * get_Camera_4() const { return ___Camera_4; }
	inline Camera_t4157153871 ** get_address_of_Camera_4() { return &___Camera_4; }
	inline void set_Camera_4(Camera_t4157153871 * value)
	{
		___Camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_4), value);
	}

	inline static int32_t get_offset_of_BringToFront_5() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___BringToFront_5)); }
	inline bool get_BringToFront_5() const { return ___BringToFront_5; }
	inline bool* get_address_of_BringToFront_5() { return &___BringToFront_5; }
	inline void set_BringToFront_5(bool value)
	{
		___BringToFront_5 = value;
	}

	inline static int32_t get_offset_of_AllowExecutionWithAllGestures_6() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___AllowExecutionWithAllGestures_6)); }
	inline bool get_AllowExecutionWithAllGestures_6() const { return ___AllowExecutionWithAllGestures_6; }
	inline bool* get_address_of_AllowExecutionWithAllGestures_6() { return &___AllowExecutionWithAllGestures_6; }
	inline void set_AllowExecutionWithAllGestures_6(bool value)
	{
		___AllowExecutionWithAllGestures_6 = value;
	}

	inline static int32_t get_offset_of_Mode_7() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___Mode_7)); }
	inline int32_t get_Mode_7() const { return ___Mode_7; }
	inline int32_t* get_address_of_Mode_7() { return &___Mode_7; }
	inline void set_Mode_7(int32_t value)
	{
		___Mode_7 = value;
	}

	inline static int32_t get_offset_of_MinMaxScale_8() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___MinMaxScale_8)); }
	inline Vector2_t2156229523  get_MinMaxScale_8() const { return ___MinMaxScale_8; }
	inline Vector2_t2156229523 * get_address_of_MinMaxScale_8() { return &___MinMaxScale_8; }
	inline void set_MinMaxScale_8(Vector2_t2156229523  value)
	{
		___MinMaxScale_8 = value;
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___U3CPanGestureU3Ek__BackingField_9)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_9() const { return ___U3CPanGestureU3Ek__BackingField_9; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_9() { return &___U3CPanGestureU3Ek__BackingField_9; }
	inline void set_U3CPanGestureU3Ek__BackingField_9(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CScaleGestureU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___U3CScaleGestureU3Ek__BackingField_10)); }
	inline ScaleGestureRecognizer_t1137887245 * get_U3CScaleGestureU3Ek__BackingField_10() const { return ___U3CScaleGestureU3Ek__BackingField_10; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_U3CScaleGestureU3Ek__BackingField_10() { return &___U3CScaleGestureU3Ek__BackingField_10; }
	inline void set_U3CScaleGestureU3Ek__BackingField_10(ScaleGestureRecognizer_t1137887245 * value)
	{
		___U3CScaleGestureU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScaleGestureU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CRotateGestureU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___U3CRotateGestureU3Ek__BackingField_11)); }
	inline RotateGestureRecognizer_t4100246528 * get_U3CRotateGestureU3Ek__BackingField_11() const { return ___U3CRotateGestureU3Ek__BackingField_11; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_U3CRotateGestureU3Ek__BackingField_11() { return &___U3CRotateGestureU3Ek__BackingField_11; }
	inline void set_U3CRotateGestureU3Ek__BackingField_11(RotateGestureRecognizer_t4100246528 * value)
	{
		___U3CRotateGestureU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRotateGestureU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_rigidBody2D_12() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___rigidBody2D_12)); }
	inline Rigidbody2D_t939494601 * get_rigidBody2D_12() const { return ___rigidBody2D_12; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidBody2D_12() { return &___rigidBody2D_12; }
	inline void set_rigidBody2D_12(Rigidbody2D_t939494601 * value)
	{
		___rigidBody2D_12 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody2D_12), value);
	}

	inline static int32_t get_offset_of_rigidBody_13() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___rigidBody_13)); }
	inline Rigidbody_t3916780224 * get_rigidBody_13() const { return ___rigidBody_13; }
	inline Rigidbody_t3916780224 ** get_address_of_rigidBody_13() { return &___rigidBody_13; }
	inline void set_rigidBody_13(Rigidbody_t3916780224 * value)
	{
		___rigidBody_13 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_13), value);
	}

	inline static int32_t get_offset_of_spriteRenderer_14() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___spriteRenderer_14)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_14() const { return ___spriteRenderer_14; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_14() { return &___spriteRenderer_14; }
	inline void set_spriteRenderer_14(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_14 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_14), value);
	}

	inline static int32_t get_offset_of_canvasRenderer_15() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___canvasRenderer_15)); }
	inline CanvasRenderer_t2598313366 * get_canvasRenderer_15() const { return ___canvasRenderer_15; }
	inline CanvasRenderer_t2598313366 ** get_address_of_canvasRenderer_15() { return &___canvasRenderer_15; }
	inline void set_canvasRenderer_15(CanvasRenderer_t2598313366 * value)
	{
		___canvasRenderer_15 = value;
		Il2CppCodeGenWriteBarrier((&___canvasRenderer_15), value);
	}

	inline static int32_t get_offset_of__transform_16() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ____transform_16)); }
	inline Transform_t3600365921 * get__transform_16() const { return ____transform_16; }
	inline Transform_t3600365921 ** get_address_of__transform_16() { return &____transform_16; }
	inline void set__transform_16(Transform_t3600365921 * value)
	{
		____transform_16 = value;
		Il2CppCodeGenWriteBarrier((&____transform_16), value);
	}

	inline static int32_t get_offset_of_startSortOrder_17() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___startSortOrder_17)); }
	inline int32_t get_startSortOrder_17() const { return ___startSortOrder_17; }
	inline int32_t* get_address_of_startSortOrder_17() { return &___startSortOrder_17; }
	inline void set_startSortOrder_17(int32_t value)
	{
		___startSortOrder_17 = value;
	}

	inline static int32_t get_offset_of_panZ_18() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___panZ_18)); }
	inline float get_panZ_18() const { return ___panZ_18; }
	inline float* get_address_of_panZ_18() { return &___panZ_18; }
	inline void set_panZ_18(float value)
	{
		___panZ_18 = value;
	}

	inline static int32_t get_offset_of_panOffset_19() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084, ___panOffset_19)); }
	inline Vector3_t3722313464  get_panOffset_19() const { return ___panOffset_19; }
	inline Vector3_t3722313464 * get_address_of_panOffset_19() { return &___panOffset_19; }
	inline void set_panOffset_19(Vector3_t3722313464  value)
	{
		___panOffset_19 = value;
	}
};

struct FingersPanRotateScaleComponentScript_t154562084_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> DigitalRubyShared.FingersPanRotateScaleComponentScript::captureRaycastResults
	List_1_t537414295 * ___captureRaycastResults_20;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> DigitalRubyShared.FingersPanRotateScaleComponentScript::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_21;

public:
	inline static int32_t get_offset_of_captureRaycastResults_20() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084_StaticFields, ___captureRaycastResults_20)); }
	inline List_1_t537414295 * get_captureRaycastResults_20() const { return ___captureRaycastResults_20; }
	inline List_1_t537414295 ** get_address_of_captureRaycastResults_20() { return &___captureRaycastResults_20; }
	inline void set_captureRaycastResults_20(List_1_t537414295 * value)
	{
		___captureRaycastResults_20 = value;
		Il2CppCodeGenWriteBarrier((&___captureRaycastResults_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_21() { return static_cast<int32_t>(offsetof(FingersPanRotateScaleComponentScript_t154562084_StaticFields, ___U3CU3Ef__mgU24cache0_21)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_21() const { return ___U3CU3Ef__mgU24cache0_21; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_21() { return &___U3CU3Ef__mgU24cache0_21; }
	inline void set_U3CU3Ef__mgU24cache0_21(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSPANROTATESCALECOMPONENTSCRIPT_T154562084_H
#ifndef FINGERSPLATFORMMOVEJUMPCOMPONENTSCRIPT_T377169379_H
#define FINGERSPLATFORMMOVEJUMPCOMPONENTSCRIPT_T377169379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersPlatformMoveJumpComponentScript
struct  FingersPlatformMoveJumpComponentScript_t377169379  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D DigitalRubyShared.FingersPlatformMoveJumpComponentScript::playerBody
	Rigidbody2D_t939494601 * ___playerBody_4;
	// System.Single DigitalRubyShared.FingersPlatformMoveJumpComponentScript::MaxSpeed
	float ___MaxSpeed_5;
	// System.Single DigitalRubyShared.FingersPlatformMoveJumpComponentScript::JumpForce
	float ___JumpForce_6;
	// System.Single DigitalRubyShared.FingersPlatformMoveJumpComponentScript::JumpThresholdUnits
	float ___JumpThresholdUnits_7;
	// System.Single DigitalRubyShared.FingersPlatformMoveJumpComponentScript::JumpThresholdSeconds
	float ___JumpThresholdSeconds_8;
	// System.Single DigitalRubyShared.FingersPlatformMoveJumpComponentScript::MoveSpeed
	float ___MoveSpeed_9;
	// System.Single DigitalRubyShared.FingersPlatformMoveJumpComponentScript::MoveThresholdUnits
	float ___MoveThresholdUnits_10;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersPlatformMoveJumpComponentScript::<TapGesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CTapGestureU3Ek__BackingField_11;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersPlatformMoveJumpComponentScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_12;
	// DigitalRubyShared.SwipeGestureRecognizer DigitalRubyShared.FingersPlatformMoveJumpComponentScript::<SwipeGesture>k__BackingField
	SwipeGestureRecognizer_t2328511861 * ___U3CSwipeGestureU3Ek__BackingField_13;
	// UnityEngine.Collider2D[] DigitalRubyShared.FingersPlatformMoveJumpComponentScript::overlapArray
	Collider2DU5BU5D_t1693969295* ___overlapArray_14;

public:
	inline static int32_t get_offset_of_playerBody_4() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___playerBody_4)); }
	inline Rigidbody2D_t939494601 * get_playerBody_4() const { return ___playerBody_4; }
	inline Rigidbody2D_t939494601 ** get_address_of_playerBody_4() { return &___playerBody_4; }
	inline void set_playerBody_4(Rigidbody2D_t939494601 * value)
	{
		___playerBody_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerBody_4), value);
	}

	inline static int32_t get_offset_of_MaxSpeed_5() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___MaxSpeed_5)); }
	inline float get_MaxSpeed_5() const { return ___MaxSpeed_5; }
	inline float* get_address_of_MaxSpeed_5() { return &___MaxSpeed_5; }
	inline void set_MaxSpeed_5(float value)
	{
		___MaxSpeed_5 = value;
	}

	inline static int32_t get_offset_of_JumpForce_6() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___JumpForce_6)); }
	inline float get_JumpForce_6() const { return ___JumpForce_6; }
	inline float* get_address_of_JumpForce_6() { return &___JumpForce_6; }
	inline void set_JumpForce_6(float value)
	{
		___JumpForce_6 = value;
	}

	inline static int32_t get_offset_of_JumpThresholdUnits_7() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___JumpThresholdUnits_7)); }
	inline float get_JumpThresholdUnits_7() const { return ___JumpThresholdUnits_7; }
	inline float* get_address_of_JumpThresholdUnits_7() { return &___JumpThresholdUnits_7; }
	inline void set_JumpThresholdUnits_7(float value)
	{
		___JumpThresholdUnits_7 = value;
	}

	inline static int32_t get_offset_of_JumpThresholdSeconds_8() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___JumpThresholdSeconds_8)); }
	inline float get_JumpThresholdSeconds_8() const { return ___JumpThresholdSeconds_8; }
	inline float* get_address_of_JumpThresholdSeconds_8() { return &___JumpThresholdSeconds_8; }
	inline void set_JumpThresholdSeconds_8(float value)
	{
		___JumpThresholdSeconds_8 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_9() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___MoveSpeed_9)); }
	inline float get_MoveSpeed_9() const { return ___MoveSpeed_9; }
	inline float* get_address_of_MoveSpeed_9() { return &___MoveSpeed_9; }
	inline void set_MoveSpeed_9(float value)
	{
		___MoveSpeed_9 = value;
	}

	inline static int32_t get_offset_of_MoveThresholdUnits_10() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___MoveThresholdUnits_10)); }
	inline float get_MoveThresholdUnits_10() const { return ___MoveThresholdUnits_10; }
	inline float* get_address_of_MoveThresholdUnits_10() { return &___MoveThresholdUnits_10; }
	inline void set_MoveThresholdUnits_10(float value)
	{
		___MoveThresholdUnits_10 = value;
	}

	inline static int32_t get_offset_of_U3CTapGestureU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___U3CTapGestureU3Ek__BackingField_11)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CTapGestureU3Ek__BackingField_11() const { return ___U3CTapGestureU3Ek__BackingField_11; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CTapGestureU3Ek__BackingField_11() { return &___U3CTapGestureU3Ek__BackingField_11; }
	inline void set_U3CTapGestureU3Ek__BackingField_11(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CTapGestureU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTapGestureU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___U3CPanGestureU3Ek__BackingField_12)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_12() const { return ___U3CPanGestureU3Ek__BackingField_12; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_12() { return &___U3CPanGestureU3Ek__BackingField_12; }
	inline void set_U3CPanGestureU3Ek__BackingField_12(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CSwipeGestureU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___U3CSwipeGestureU3Ek__BackingField_13)); }
	inline SwipeGestureRecognizer_t2328511861 * get_U3CSwipeGestureU3Ek__BackingField_13() const { return ___U3CSwipeGestureU3Ek__BackingField_13; }
	inline SwipeGestureRecognizer_t2328511861 ** get_address_of_U3CSwipeGestureU3Ek__BackingField_13() { return &___U3CSwipeGestureU3Ek__BackingField_13; }
	inline void set_U3CSwipeGestureU3Ek__BackingField_13(SwipeGestureRecognizer_t2328511861 * value)
	{
		___U3CSwipeGestureU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSwipeGestureU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_overlapArray_14() { return static_cast<int32_t>(offsetof(FingersPlatformMoveJumpComponentScript_t377169379, ___overlapArray_14)); }
	inline Collider2DU5BU5D_t1693969295* get_overlapArray_14() const { return ___overlapArray_14; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_overlapArray_14() { return &___overlapArray_14; }
	inline void set_overlapArray_14(Collider2DU5BU5D_t1693969295* value)
	{
		___overlapArray_14 = value;
		Il2CppCodeGenWriteBarrier((&___overlapArray_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSPLATFORMMOVEJUMPCOMPONENTSCRIPT_T377169379_H
#ifndef FINGERSROTATEORBITCOMPONENTSCRIPT_T3348659367_H
#define FINGERSROTATEORBITCOMPONENTSCRIPT_T3348659367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersRotateOrbitComponentScript
struct  FingersRotateOrbitComponentScript_t3348659367  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform DigitalRubyShared.FingersRotateOrbitComponentScript::OrbitTarget
	Transform_t3600365921 * ___OrbitTarget_4;
	// UnityEngine.Transform DigitalRubyShared.FingersRotateOrbitComponentScript::Orbiter
	Transform_t3600365921 * ___Orbiter_5;
	// UnityEngine.Vector3 DigitalRubyShared.FingersRotateOrbitComponentScript::Axis
	Vector3_t3722313464  ___Axis_6;
	// System.Single DigitalRubyShared.FingersRotateOrbitComponentScript::RotationSpeed
	float ___RotationSpeed_7;
	// DigitalRubyShared.RotateGestureRecognizer DigitalRubyShared.FingersRotateOrbitComponentScript::<RotationGesture>k__BackingField
	RotateGestureRecognizer_t4100246528 * ___U3CRotationGestureU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_OrbitTarget_4() { return static_cast<int32_t>(offsetof(FingersRotateOrbitComponentScript_t3348659367, ___OrbitTarget_4)); }
	inline Transform_t3600365921 * get_OrbitTarget_4() const { return ___OrbitTarget_4; }
	inline Transform_t3600365921 ** get_address_of_OrbitTarget_4() { return &___OrbitTarget_4; }
	inline void set_OrbitTarget_4(Transform_t3600365921 * value)
	{
		___OrbitTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitTarget_4), value);
	}

	inline static int32_t get_offset_of_Orbiter_5() { return static_cast<int32_t>(offsetof(FingersRotateOrbitComponentScript_t3348659367, ___Orbiter_5)); }
	inline Transform_t3600365921 * get_Orbiter_5() const { return ___Orbiter_5; }
	inline Transform_t3600365921 ** get_address_of_Orbiter_5() { return &___Orbiter_5; }
	inline void set_Orbiter_5(Transform_t3600365921 * value)
	{
		___Orbiter_5 = value;
		Il2CppCodeGenWriteBarrier((&___Orbiter_5), value);
	}

	inline static int32_t get_offset_of_Axis_6() { return static_cast<int32_t>(offsetof(FingersRotateOrbitComponentScript_t3348659367, ___Axis_6)); }
	inline Vector3_t3722313464  get_Axis_6() const { return ___Axis_6; }
	inline Vector3_t3722313464 * get_address_of_Axis_6() { return &___Axis_6; }
	inline void set_Axis_6(Vector3_t3722313464  value)
	{
		___Axis_6 = value;
	}

	inline static int32_t get_offset_of_RotationSpeed_7() { return static_cast<int32_t>(offsetof(FingersRotateOrbitComponentScript_t3348659367, ___RotationSpeed_7)); }
	inline float get_RotationSpeed_7() const { return ___RotationSpeed_7; }
	inline float* get_address_of_RotationSpeed_7() { return &___RotationSpeed_7; }
	inline void set_RotationSpeed_7(float value)
	{
		___RotationSpeed_7 = value;
	}

	inline static int32_t get_offset_of_U3CRotationGestureU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FingersRotateOrbitComponentScript_t3348659367, ___U3CRotationGestureU3Ek__BackingField_8)); }
	inline RotateGestureRecognizer_t4100246528 * get_U3CRotationGestureU3Ek__BackingField_8() const { return ___U3CRotationGestureU3Ek__BackingField_8; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_U3CRotationGestureU3Ek__BackingField_8() { return &___U3CRotationGestureU3Ek__BackingField_8; }
	inline void set_U3CRotationGestureU3Ek__BackingField_8(RotateGestureRecognizer_t4100246528 * value)
	{
		___U3CRotationGestureU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRotationGestureU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSROTATEORBITCOMPONENTSCRIPT_T3348659367_H
#ifndef FINGERSSCRIPT_T1857011421_H
#define FINGERSSCRIPT_T1857011421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScript
struct  FingersScript_t1857011421  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DigitalRubyShared.FingersScript::TreatMousePointerAsFinger
	bool ___TreatMousePointerAsFinger_4;
	// System.Boolean DigitalRubyShared.FingersScript::SimulateMouseWithTouches
	bool ___SimulateMouseWithTouches_5;
	// System.Boolean DigitalRubyShared.FingersScript::RequireControlKeyForMouseZoom
	bool ___RequireControlKeyForMouseZoom_6;
	// System.Single DigitalRubyShared.FingersScript::MouseDistanceInUnitsForScaleAndRotate
	float ___MouseDistanceInUnitsForScaleAndRotate_7;
	// System.Single DigitalRubyShared.FingersScript::MouseWheelDeltaMultiplier
	float ___MouseWheelDeltaMultiplier_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DigitalRubyShared.FingersScript::PassThroughObjects
	List_1_t2585711361 * ___PassThroughObjects_9;
	// System.Boolean DigitalRubyShared.FingersScript::ShowTouches
	bool ___ShowTouches_10;
	// UnityEngine.GameObject[] DigitalRubyShared.FingersScript::TouchCircles
	GameObjectU5BU5D_t3328599146* ___TouchCircles_11;
	// UnityEngine.GameObject[] DigitalRubyShared.FingersScript::origTouchCircles
	GameObjectU5BU5D_t3328599146* ___origTouchCircles_12;
	// System.Int32 DigitalRubyShared.FingersScript::DefaultDPI
	int32_t ___DefaultDPI_13;
	// DigitalRubyShared.FingersScript/GestureLevelUnloadOption DigitalRubyShared.FingersScript::LevelUnloadOption
	int32_t ___LevelUnloadOption_14;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.FingersScript::gestures
	List_1_t861137127 * ___gestures_18;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizer> DigitalRubyShared.FingersScript::gesturesTemp
	List_1_t861137127 * ___gesturesTemp_19;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touchesBegan
	List_1_t3464476875 * ___touchesBegan_20;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touchesMoved
	List_1_t3464476875 * ___touchesMoved_21;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touchesEnded
	List_1_t3464476875 * ___touchesEnded_22;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> DigitalRubyShared.FingersScript::gameObjectsForTouch
	Dictionary_2_t1474424692 * ___gameObjectsForTouch_23;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> DigitalRubyShared.FingersScript::captureRaycastResults
	List_1_t537414295 * ___captureRaycastResults_24;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::filteredTouches
	List_1_t3464476875 * ___filteredTouches_25;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::touches
	List_1_t3464476875 * ___touches_26;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DigitalRubyShared.FingersScript::availableShowTouches
	List_1_t2585711361 * ___availableShowTouches_27;
	// System.Collections.Generic.Dictionary`2<System.Int32,DigitalRubyShared.FingersScript/ShownTouch> DigitalRubyShared.FingersScript::shownTouches
	Dictionary_2_t629320345 * ___shownTouches_28;
	// System.Collections.Generic.List`1<System.Int32> DigitalRubyShared.FingersScript::shownTouchesToRemove
	List_1_t128053199 * ___shownTouchesToRemove_29;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Vector2> DigitalRubyShared.FingersScript::previousTouchPositions
	Dictionary_2_t1044942854 * ___previousTouchPositions_30;
	// System.Collections.Generic.List`1<UnityEngine.Component> DigitalRubyShared.FingersScript::components
	List_1_t3395709193 * ___components_31;
	// System.Collections.Generic.HashSet`1<System.Type> DigitalRubyShared.FingersScript::componentTypesToDenyPassThrough
	HashSet_1_t1048894234 * ___componentTypesToDenyPassThrough_32;
	// System.Collections.Generic.HashSet`1<System.Type> DigitalRubyShared.FingersScript::componentTypesToIgnorePassThrough
	HashSet_1_t1048894234 * ___componentTypesToIgnorePassThrough_33;
	// UnityEngine.Collider2D[] DigitalRubyShared.FingersScript::hackResults
	Collider2DU5BU5D_t1693969295* ___hackResults_34;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::previousTouches
	List_1_t3464476875 * ___previousTouches_35;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::currentTouches
	List_1_t3464476875 * ___currentTouches_36;
	// System.Collections.Generic.HashSet`1<DigitalRubyShared.GestureTouch> DigitalRubyShared.FingersScript::tempTouches
	HashSet_1_t557351607 * ___tempTouches_37;
	// System.Single DigitalRubyShared.FingersScript::rotateAngle
	float ___rotateAngle_38;
	// System.Single DigitalRubyShared.FingersScript::pinchScale
	float ___pinchScale_39;
	// DigitalRubyShared.GestureTouch DigitalRubyShared.FingersScript::rotatePinch1
	GestureTouch_t1992402133  ___rotatePinch1_40;
	// DigitalRubyShared.GestureTouch DigitalRubyShared.FingersScript::rotatePinch2
	GestureTouch_t1992402133  ___rotatePinch2_41;
	// System.DateTime DigitalRubyShared.FingersScript::lastMouseWheelTime
	DateTime_t3738529785  ___lastMouseWheelTime_42;
	// System.Func`2<UnityEngine.GameObject,System.Nullable`1<System.Boolean>> DigitalRubyShared.FingersScript::CaptureGestureHandler
	Func_2_t1671534078 * ___CaptureGestureHandler_44;

public:
	inline static int32_t get_offset_of_TreatMousePointerAsFinger_4() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___TreatMousePointerAsFinger_4)); }
	inline bool get_TreatMousePointerAsFinger_4() const { return ___TreatMousePointerAsFinger_4; }
	inline bool* get_address_of_TreatMousePointerAsFinger_4() { return &___TreatMousePointerAsFinger_4; }
	inline void set_TreatMousePointerAsFinger_4(bool value)
	{
		___TreatMousePointerAsFinger_4 = value;
	}

	inline static int32_t get_offset_of_SimulateMouseWithTouches_5() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___SimulateMouseWithTouches_5)); }
	inline bool get_SimulateMouseWithTouches_5() const { return ___SimulateMouseWithTouches_5; }
	inline bool* get_address_of_SimulateMouseWithTouches_5() { return &___SimulateMouseWithTouches_5; }
	inline void set_SimulateMouseWithTouches_5(bool value)
	{
		___SimulateMouseWithTouches_5 = value;
	}

	inline static int32_t get_offset_of_RequireControlKeyForMouseZoom_6() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___RequireControlKeyForMouseZoom_6)); }
	inline bool get_RequireControlKeyForMouseZoom_6() const { return ___RequireControlKeyForMouseZoom_6; }
	inline bool* get_address_of_RequireControlKeyForMouseZoom_6() { return &___RequireControlKeyForMouseZoom_6; }
	inline void set_RequireControlKeyForMouseZoom_6(bool value)
	{
		___RequireControlKeyForMouseZoom_6 = value;
	}

	inline static int32_t get_offset_of_MouseDistanceInUnitsForScaleAndRotate_7() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___MouseDistanceInUnitsForScaleAndRotate_7)); }
	inline float get_MouseDistanceInUnitsForScaleAndRotate_7() const { return ___MouseDistanceInUnitsForScaleAndRotate_7; }
	inline float* get_address_of_MouseDistanceInUnitsForScaleAndRotate_7() { return &___MouseDistanceInUnitsForScaleAndRotate_7; }
	inline void set_MouseDistanceInUnitsForScaleAndRotate_7(float value)
	{
		___MouseDistanceInUnitsForScaleAndRotate_7 = value;
	}

	inline static int32_t get_offset_of_MouseWheelDeltaMultiplier_8() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___MouseWheelDeltaMultiplier_8)); }
	inline float get_MouseWheelDeltaMultiplier_8() const { return ___MouseWheelDeltaMultiplier_8; }
	inline float* get_address_of_MouseWheelDeltaMultiplier_8() { return &___MouseWheelDeltaMultiplier_8; }
	inline void set_MouseWheelDeltaMultiplier_8(float value)
	{
		___MouseWheelDeltaMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_PassThroughObjects_9() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___PassThroughObjects_9)); }
	inline List_1_t2585711361 * get_PassThroughObjects_9() const { return ___PassThroughObjects_9; }
	inline List_1_t2585711361 ** get_address_of_PassThroughObjects_9() { return &___PassThroughObjects_9; }
	inline void set_PassThroughObjects_9(List_1_t2585711361 * value)
	{
		___PassThroughObjects_9 = value;
		Il2CppCodeGenWriteBarrier((&___PassThroughObjects_9), value);
	}

	inline static int32_t get_offset_of_ShowTouches_10() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___ShowTouches_10)); }
	inline bool get_ShowTouches_10() const { return ___ShowTouches_10; }
	inline bool* get_address_of_ShowTouches_10() { return &___ShowTouches_10; }
	inline void set_ShowTouches_10(bool value)
	{
		___ShowTouches_10 = value;
	}

	inline static int32_t get_offset_of_TouchCircles_11() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___TouchCircles_11)); }
	inline GameObjectU5BU5D_t3328599146* get_TouchCircles_11() const { return ___TouchCircles_11; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_TouchCircles_11() { return &___TouchCircles_11; }
	inline void set_TouchCircles_11(GameObjectU5BU5D_t3328599146* value)
	{
		___TouchCircles_11 = value;
		Il2CppCodeGenWriteBarrier((&___TouchCircles_11), value);
	}

	inline static int32_t get_offset_of_origTouchCircles_12() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___origTouchCircles_12)); }
	inline GameObjectU5BU5D_t3328599146* get_origTouchCircles_12() const { return ___origTouchCircles_12; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_origTouchCircles_12() { return &___origTouchCircles_12; }
	inline void set_origTouchCircles_12(GameObjectU5BU5D_t3328599146* value)
	{
		___origTouchCircles_12 = value;
		Il2CppCodeGenWriteBarrier((&___origTouchCircles_12), value);
	}

	inline static int32_t get_offset_of_DefaultDPI_13() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___DefaultDPI_13)); }
	inline int32_t get_DefaultDPI_13() const { return ___DefaultDPI_13; }
	inline int32_t* get_address_of_DefaultDPI_13() { return &___DefaultDPI_13; }
	inline void set_DefaultDPI_13(int32_t value)
	{
		___DefaultDPI_13 = value;
	}

	inline static int32_t get_offset_of_LevelUnloadOption_14() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___LevelUnloadOption_14)); }
	inline int32_t get_LevelUnloadOption_14() const { return ___LevelUnloadOption_14; }
	inline int32_t* get_address_of_LevelUnloadOption_14() { return &___LevelUnloadOption_14; }
	inline void set_LevelUnloadOption_14(int32_t value)
	{
		___LevelUnloadOption_14 = value;
	}

	inline static int32_t get_offset_of_gestures_18() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___gestures_18)); }
	inline List_1_t861137127 * get_gestures_18() const { return ___gestures_18; }
	inline List_1_t861137127 ** get_address_of_gestures_18() { return &___gestures_18; }
	inline void set_gestures_18(List_1_t861137127 * value)
	{
		___gestures_18 = value;
		Il2CppCodeGenWriteBarrier((&___gestures_18), value);
	}

	inline static int32_t get_offset_of_gesturesTemp_19() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___gesturesTemp_19)); }
	inline List_1_t861137127 * get_gesturesTemp_19() const { return ___gesturesTemp_19; }
	inline List_1_t861137127 ** get_address_of_gesturesTemp_19() { return &___gesturesTemp_19; }
	inline void set_gesturesTemp_19(List_1_t861137127 * value)
	{
		___gesturesTemp_19 = value;
		Il2CppCodeGenWriteBarrier((&___gesturesTemp_19), value);
	}

	inline static int32_t get_offset_of_touchesBegan_20() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touchesBegan_20)); }
	inline List_1_t3464476875 * get_touchesBegan_20() const { return ___touchesBegan_20; }
	inline List_1_t3464476875 ** get_address_of_touchesBegan_20() { return &___touchesBegan_20; }
	inline void set_touchesBegan_20(List_1_t3464476875 * value)
	{
		___touchesBegan_20 = value;
		Il2CppCodeGenWriteBarrier((&___touchesBegan_20), value);
	}

	inline static int32_t get_offset_of_touchesMoved_21() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touchesMoved_21)); }
	inline List_1_t3464476875 * get_touchesMoved_21() const { return ___touchesMoved_21; }
	inline List_1_t3464476875 ** get_address_of_touchesMoved_21() { return &___touchesMoved_21; }
	inline void set_touchesMoved_21(List_1_t3464476875 * value)
	{
		___touchesMoved_21 = value;
		Il2CppCodeGenWriteBarrier((&___touchesMoved_21), value);
	}

	inline static int32_t get_offset_of_touchesEnded_22() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touchesEnded_22)); }
	inline List_1_t3464476875 * get_touchesEnded_22() const { return ___touchesEnded_22; }
	inline List_1_t3464476875 ** get_address_of_touchesEnded_22() { return &___touchesEnded_22; }
	inline void set_touchesEnded_22(List_1_t3464476875 * value)
	{
		___touchesEnded_22 = value;
		Il2CppCodeGenWriteBarrier((&___touchesEnded_22), value);
	}

	inline static int32_t get_offset_of_gameObjectsForTouch_23() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___gameObjectsForTouch_23)); }
	inline Dictionary_2_t1474424692 * get_gameObjectsForTouch_23() const { return ___gameObjectsForTouch_23; }
	inline Dictionary_2_t1474424692 ** get_address_of_gameObjectsForTouch_23() { return &___gameObjectsForTouch_23; }
	inline void set_gameObjectsForTouch_23(Dictionary_2_t1474424692 * value)
	{
		___gameObjectsForTouch_23 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectsForTouch_23), value);
	}

	inline static int32_t get_offset_of_captureRaycastResults_24() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___captureRaycastResults_24)); }
	inline List_1_t537414295 * get_captureRaycastResults_24() const { return ___captureRaycastResults_24; }
	inline List_1_t537414295 ** get_address_of_captureRaycastResults_24() { return &___captureRaycastResults_24; }
	inline void set_captureRaycastResults_24(List_1_t537414295 * value)
	{
		___captureRaycastResults_24 = value;
		Il2CppCodeGenWriteBarrier((&___captureRaycastResults_24), value);
	}

	inline static int32_t get_offset_of_filteredTouches_25() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___filteredTouches_25)); }
	inline List_1_t3464476875 * get_filteredTouches_25() const { return ___filteredTouches_25; }
	inline List_1_t3464476875 ** get_address_of_filteredTouches_25() { return &___filteredTouches_25; }
	inline void set_filteredTouches_25(List_1_t3464476875 * value)
	{
		___filteredTouches_25 = value;
		Il2CppCodeGenWriteBarrier((&___filteredTouches_25), value);
	}

	inline static int32_t get_offset_of_touches_26() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___touches_26)); }
	inline List_1_t3464476875 * get_touches_26() const { return ___touches_26; }
	inline List_1_t3464476875 ** get_address_of_touches_26() { return &___touches_26; }
	inline void set_touches_26(List_1_t3464476875 * value)
	{
		___touches_26 = value;
		Il2CppCodeGenWriteBarrier((&___touches_26), value);
	}

	inline static int32_t get_offset_of_availableShowTouches_27() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___availableShowTouches_27)); }
	inline List_1_t2585711361 * get_availableShowTouches_27() const { return ___availableShowTouches_27; }
	inline List_1_t2585711361 ** get_address_of_availableShowTouches_27() { return &___availableShowTouches_27; }
	inline void set_availableShowTouches_27(List_1_t2585711361 * value)
	{
		___availableShowTouches_27 = value;
		Il2CppCodeGenWriteBarrier((&___availableShowTouches_27), value);
	}

	inline static int32_t get_offset_of_shownTouches_28() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___shownTouches_28)); }
	inline Dictionary_2_t629320345 * get_shownTouches_28() const { return ___shownTouches_28; }
	inline Dictionary_2_t629320345 ** get_address_of_shownTouches_28() { return &___shownTouches_28; }
	inline void set_shownTouches_28(Dictionary_2_t629320345 * value)
	{
		___shownTouches_28 = value;
		Il2CppCodeGenWriteBarrier((&___shownTouches_28), value);
	}

	inline static int32_t get_offset_of_shownTouchesToRemove_29() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___shownTouchesToRemove_29)); }
	inline List_1_t128053199 * get_shownTouchesToRemove_29() const { return ___shownTouchesToRemove_29; }
	inline List_1_t128053199 ** get_address_of_shownTouchesToRemove_29() { return &___shownTouchesToRemove_29; }
	inline void set_shownTouchesToRemove_29(List_1_t128053199 * value)
	{
		___shownTouchesToRemove_29 = value;
		Il2CppCodeGenWriteBarrier((&___shownTouchesToRemove_29), value);
	}

	inline static int32_t get_offset_of_previousTouchPositions_30() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___previousTouchPositions_30)); }
	inline Dictionary_2_t1044942854 * get_previousTouchPositions_30() const { return ___previousTouchPositions_30; }
	inline Dictionary_2_t1044942854 ** get_address_of_previousTouchPositions_30() { return &___previousTouchPositions_30; }
	inline void set_previousTouchPositions_30(Dictionary_2_t1044942854 * value)
	{
		___previousTouchPositions_30 = value;
		Il2CppCodeGenWriteBarrier((&___previousTouchPositions_30), value);
	}

	inline static int32_t get_offset_of_components_31() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___components_31)); }
	inline List_1_t3395709193 * get_components_31() const { return ___components_31; }
	inline List_1_t3395709193 ** get_address_of_components_31() { return &___components_31; }
	inline void set_components_31(List_1_t3395709193 * value)
	{
		___components_31 = value;
		Il2CppCodeGenWriteBarrier((&___components_31), value);
	}

	inline static int32_t get_offset_of_componentTypesToDenyPassThrough_32() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___componentTypesToDenyPassThrough_32)); }
	inline HashSet_1_t1048894234 * get_componentTypesToDenyPassThrough_32() const { return ___componentTypesToDenyPassThrough_32; }
	inline HashSet_1_t1048894234 ** get_address_of_componentTypesToDenyPassThrough_32() { return &___componentTypesToDenyPassThrough_32; }
	inline void set_componentTypesToDenyPassThrough_32(HashSet_1_t1048894234 * value)
	{
		___componentTypesToDenyPassThrough_32 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypesToDenyPassThrough_32), value);
	}

	inline static int32_t get_offset_of_componentTypesToIgnorePassThrough_33() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___componentTypesToIgnorePassThrough_33)); }
	inline HashSet_1_t1048894234 * get_componentTypesToIgnorePassThrough_33() const { return ___componentTypesToIgnorePassThrough_33; }
	inline HashSet_1_t1048894234 ** get_address_of_componentTypesToIgnorePassThrough_33() { return &___componentTypesToIgnorePassThrough_33; }
	inline void set_componentTypesToIgnorePassThrough_33(HashSet_1_t1048894234 * value)
	{
		___componentTypesToIgnorePassThrough_33 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypesToIgnorePassThrough_33), value);
	}

	inline static int32_t get_offset_of_hackResults_34() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___hackResults_34)); }
	inline Collider2DU5BU5D_t1693969295* get_hackResults_34() const { return ___hackResults_34; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_hackResults_34() { return &___hackResults_34; }
	inline void set_hackResults_34(Collider2DU5BU5D_t1693969295* value)
	{
		___hackResults_34 = value;
		Il2CppCodeGenWriteBarrier((&___hackResults_34), value);
	}

	inline static int32_t get_offset_of_previousTouches_35() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___previousTouches_35)); }
	inline List_1_t3464476875 * get_previousTouches_35() const { return ___previousTouches_35; }
	inline List_1_t3464476875 ** get_address_of_previousTouches_35() { return &___previousTouches_35; }
	inline void set_previousTouches_35(List_1_t3464476875 * value)
	{
		___previousTouches_35 = value;
		Il2CppCodeGenWriteBarrier((&___previousTouches_35), value);
	}

	inline static int32_t get_offset_of_currentTouches_36() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___currentTouches_36)); }
	inline List_1_t3464476875 * get_currentTouches_36() const { return ___currentTouches_36; }
	inline List_1_t3464476875 ** get_address_of_currentTouches_36() { return &___currentTouches_36; }
	inline void set_currentTouches_36(List_1_t3464476875 * value)
	{
		___currentTouches_36 = value;
		Il2CppCodeGenWriteBarrier((&___currentTouches_36), value);
	}

	inline static int32_t get_offset_of_tempTouches_37() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___tempTouches_37)); }
	inline HashSet_1_t557351607 * get_tempTouches_37() const { return ___tempTouches_37; }
	inline HashSet_1_t557351607 ** get_address_of_tempTouches_37() { return &___tempTouches_37; }
	inline void set_tempTouches_37(HashSet_1_t557351607 * value)
	{
		___tempTouches_37 = value;
		Il2CppCodeGenWriteBarrier((&___tempTouches_37), value);
	}

	inline static int32_t get_offset_of_rotateAngle_38() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___rotateAngle_38)); }
	inline float get_rotateAngle_38() const { return ___rotateAngle_38; }
	inline float* get_address_of_rotateAngle_38() { return &___rotateAngle_38; }
	inline void set_rotateAngle_38(float value)
	{
		___rotateAngle_38 = value;
	}

	inline static int32_t get_offset_of_pinchScale_39() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___pinchScale_39)); }
	inline float get_pinchScale_39() const { return ___pinchScale_39; }
	inline float* get_address_of_pinchScale_39() { return &___pinchScale_39; }
	inline void set_pinchScale_39(float value)
	{
		___pinchScale_39 = value;
	}

	inline static int32_t get_offset_of_rotatePinch1_40() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___rotatePinch1_40)); }
	inline GestureTouch_t1992402133  get_rotatePinch1_40() const { return ___rotatePinch1_40; }
	inline GestureTouch_t1992402133 * get_address_of_rotatePinch1_40() { return &___rotatePinch1_40; }
	inline void set_rotatePinch1_40(GestureTouch_t1992402133  value)
	{
		___rotatePinch1_40 = value;
	}

	inline static int32_t get_offset_of_rotatePinch2_41() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___rotatePinch2_41)); }
	inline GestureTouch_t1992402133  get_rotatePinch2_41() const { return ___rotatePinch2_41; }
	inline GestureTouch_t1992402133 * get_address_of_rotatePinch2_41() { return &___rotatePinch2_41; }
	inline void set_rotatePinch2_41(GestureTouch_t1992402133  value)
	{
		___rotatePinch2_41 = value;
	}

	inline static int32_t get_offset_of_lastMouseWheelTime_42() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___lastMouseWheelTime_42)); }
	inline DateTime_t3738529785  get_lastMouseWheelTime_42() const { return ___lastMouseWheelTime_42; }
	inline DateTime_t3738529785 * get_address_of_lastMouseWheelTime_42() { return &___lastMouseWheelTime_42; }
	inline void set_lastMouseWheelTime_42(DateTime_t3738529785  value)
	{
		___lastMouseWheelTime_42 = value;
	}

	inline static int32_t get_offset_of_CaptureGestureHandler_44() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421, ___CaptureGestureHandler_44)); }
	inline Func_2_t1671534078 * get_CaptureGestureHandler_44() const { return ___CaptureGestureHandler_44; }
	inline Func_2_t1671534078 ** get_address_of_CaptureGestureHandler_44() { return &___CaptureGestureHandler_44; }
	inline void set_CaptureGestureHandler_44(Func_2_t1671534078 * value)
	{
		___CaptureGestureHandler_44 = value;
		Il2CppCodeGenWriteBarrier((&___CaptureGestureHandler_44), value);
	}
};

struct FingersScript_t1857011421_StaticFields
{
public:
	// DigitalRubyShared.FingersScript DigitalRubyShared.FingersScript::singleton
	FingersScript_t1857011421 * ___singleton_43;

public:
	inline static int32_t get_offset_of_singleton_43() { return static_cast<int32_t>(offsetof(FingersScript_t1857011421_StaticFields, ___singleton_43)); }
	inline FingersScript_t1857011421 * get_singleton_43() const { return ___singleton_43; }
	inline FingersScript_t1857011421 ** get_address_of_singleton_43() { return &___singleton_43; }
	inline void set_singleton_43(FingersScript_t1857011421 * value)
	{
		___singleton_43 = value;
		Il2CppCodeGenWriteBarrier((&___singleton_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSSCRIPT_T1857011421_H
#ifndef FINGERSSCROLLVIEWCOMPONENTSCRIPT_T3503049574_H
#define FINGERSSCROLLVIEWCOMPONENTSCRIPT_T3503049574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersScrollViewComponentScript
struct  FingersScrollViewComponentScript_t3503049574  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DigitalRubyShared.FingersScrollViewComponentScript::ScrollContent
	GameObject_t1113636619 * ___ScrollContent_4;
	// UnityEngine.GameObject DigitalRubyShared.FingersScrollViewComponentScript::ScrollContentContainer
	GameObject_t1113636619 * ___ScrollContentContainer_5;
	// UnityEngine.Camera DigitalRubyShared.FingersScrollViewComponentScript::CanvasCamera
	Camera_t4157153871 * ___CanvasCamera_6;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::MaxSpeed
	float ___MaxSpeed_7;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::DoubleTapZoomOutThreshold
	float ___DoubleTapZoomOutThreshold_8;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::DoubleTapZoomOutValue
	float ___DoubleTapZoomOutValue_9;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::DoubleTapZoomInValue
	float ___DoubleTapZoomInValue_10;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::DoubleTapAnimationTimeSeconds
	float ___DoubleTapAnimationTimeSeconds_11;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::PanDampening
	float ___PanDampening_12;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::ScaleDampening
	float ___ScaleDampening_13;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::ScaleSpeed
	float ___ScaleSpeed_14;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::MinimumScale
	float ___MinimumScale_15;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::MaximumScale
	float ___MaximumScale_16;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::BounceModifier
	float ___BounceModifier_17;
	// UnityEngine.UI.Text DigitalRubyShared.FingersScrollViewComponentScript::DebugText
	Text_t1901882714 * ___DebugText_18;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.FingersScrollViewComponentScript::<ScaleGesture>k__BackingField
	ScaleGestureRecognizer_t1137887245 * ___U3CScaleGestureU3Ek__BackingField_19;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersScrollViewComponentScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_20;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersScrollViewComponentScript::<DoubleTapGesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CDoubleTapGestureU3Ek__BackingField_21;
	// UnityEngine.RectTransform DigitalRubyShared.FingersScrollViewComponentScript::contentRectTransform
	RectTransform_t3704657025 * ___contentRectTransform_22;
	// UnityEngine.RectTransform DigitalRubyShared.FingersScrollViewComponentScript::containerRectTransform
	RectTransform_t3704657025 * ___containerRectTransform_23;
	// UnityEngine.Vector2 DigitalRubyShared.FingersScrollViewComponentScript::panVelocity
	Vector2_t2156229523  ___panVelocity_24;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::zoomSpeed
	float ___zoomSpeed_25;
	// UnityEngine.Vector2 DigitalRubyShared.FingersScrollViewComponentScript::lastScaleFocus
	Vector2_t2156229523  ___lastScaleFocus_26;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::doubleTapScaleTimeSecondsRemaining
	float ___doubleTapScaleTimeSecondsRemaining_27;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::doubleTapScaleStart
	float ___doubleTapScaleStart_28;
	// System.Single DigitalRubyShared.FingersScrollViewComponentScript::doubleTapScaleEnd
	float ___doubleTapScaleEnd_29;
	// UnityEngine.Vector2 DigitalRubyShared.FingersScrollViewComponentScript::doubleTapPosStart
	Vector2_t2156229523  ___doubleTapPosStart_30;
	// UnityEngine.Vector2 DigitalRubyShared.FingersScrollViewComponentScript::doubleTapPosEnd
	Vector2_t2156229523  ___doubleTapPosEnd_31;

public:
	inline static int32_t get_offset_of_ScrollContent_4() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___ScrollContent_4)); }
	inline GameObject_t1113636619 * get_ScrollContent_4() const { return ___ScrollContent_4; }
	inline GameObject_t1113636619 ** get_address_of_ScrollContent_4() { return &___ScrollContent_4; }
	inline void set_ScrollContent_4(GameObject_t1113636619 * value)
	{
		___ScrollContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollContent_4), value);
	}

	inline static int32_t get_offset_of_ScrollContentContainer_5() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___ScrollContentContainer_5)); }
	inline GameObject_t1113636619 * get_ScrollContentContainer_5() const { return ___ScrollContentContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_ScrollContentContainer_5() { return &___ScrollContentContainer_5; }
	inline void set_ScrollContentContainer_5(GameObject_t1113636619 * value)
	{
		___ScrollContentContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollContentContainer_5), value);
	}

	inline static int32_t get_offset_of_CanvasCamera_6() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___CanvasCamera_6)); }
	inline Camera_t4157153871 * get_CanvasCamera_6() const { return ___CanvasCamera_6; }
	inline Camera_t4157153871 ** get_address_of_CanvasCamera_6() { return &___CanvasCamera_6; }
	inline void set_CanvasCamera_6(Camera_t4157153871 * value)
	{
		___CanvasCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___CanvasCamera_6), value);
	}

	inline static int32_t get_offset_of_MaxSpeed_7() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___MaxSpeed_7)); }
	inline float get_MaxSpeed_7() const { return ___MaxSpeed_7; }
	inline float* get_address_of_MaxSpeed_7() { return &___MaxSpeed_7; }
	inline void set_MaxSpeed_7(float value)
	{
		___MaxSpeed_7 = value;
	}

	inline static int32_t get_offset_of_DoubleTapZoomOutThreshold_8() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___DoubleTapZoomOutThreshold_8)); }
	inline float get_DoubleTapZoomOutThreshold_8() const { return ___DoubleTapZoomOutThreshold_8; }
	inline float* get_address_of_DoubleTapZoomOutThreshold_8() { return &___DoubleTapZoomOutThreshold_8; }
	inline void set_DoubleTapZoomOutThreshold_8(float value)
	{
		___DoubleTapZoomOutThreshold_8 = value;
	}

	inline static int32_t get_offset_of_DoubleTapZoomOutValue_9() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___DoubleTapZoomOutValue_9)); }
	inline float get_DoubleTapZoomOutValue_9() const { return ___DoubleTapZoomOutValue_9; }
	inline float* get_address_of_DoubleTapZoomOutValue_9() { return &___DoubleTapZoomOutValue_9; }
	inline void set_DoubleTapZoomOutValue_9(float value)
	{
		___DoubleTapZoomOutValue_9 = value;
	}

	inline static int32_t get_offset_of_DoubleTapZoomInValue_10() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___DoubleTapZoomInValue_10)); }
	inline float get_DoubleTapZoomInValue_10() const { return ___DoubleTapZoomInValue_10; }
	inline float* get_address_of_DoubleTapZoomInValue_10() { return &___DoubleTapZoomInValue_10; }
	inline void set_DoubleTapZoomInValue_10(float value)
	{
		___DoubleTapZoomInValue_10 = value;
	}

	inline static int32_t get_offset_of_DoubleTapAnimationTimeSeconds_11() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___DoubleTapAnimationTimeSeconds_11)); }
	inline float get_DoubleTapAnimationTimeSeconds_11() const { return ___DoubleTapAnimationTimeSeconds_11; }
	inline float* get_address_of_DoubleTapAnimationTimeSeconds_11() { return &___DoubleTapAnimationTimeSeconds_11; }
	inline void set_DoubleTapAnimationTimeSeconds_11(float value)
	{
		___DoubleTapAnimationTimeSeconds_11 = value;
	}

	inline static int32_t get_offset_of_PanDampening_12() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___PanDampening_12)); }
	inline float get_PanDampening_12() const { return ___PanDampening_12; }
	inline float* get_address_of_PanDampening_12() { return &___PanDampening_12; }
	inline void set_PanDampening_12(float value)
	{
		___PanDampening_12 = value;
	}

	inline static int32_t get_offset_of_ScaleDampening_13() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___ScaleDampening_13)); }
	inline float get_ScaleDampening_13() const { return ___ScaleDampening_13; }
	inline float* get_address_of_ScaleDampening_13() { return &___ScaleDampening_13; }
	inline void set_ScaleDampening_13(float value)
	{
		___ScaleDampening_13 = value;
	}

	inline static int32_t get_offset_of_ScaleSpeed_14() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___ScaleSpeed_14)); }
	inline float get_ScaleSpeed_14() const { return ___ScaleSpeed_14; }
	inline float* get_address_of_ScaleSpeed_14() { return &___ScaleSpeed_14; }
	inline void set_ScaleSpeed_14(float value)
	{
		___ScaleSpeed_14 = value;
	}

	inline static int32_t get_offset_of_MinimumScale_15() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___MinimumScale_15)); }
	inline float get_MinimumScale_15() const { return ___MinimumScale_15; }
	inline float* get_address_of_MinimumScale_15() { return &___MinimumScale_15; }
	inline void set_MinimumScale_15(float value)
	{
		___MinimumScale_15 = value;
	}

	inline static int32_t get_offset_of_MaximumScale_16() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___MaximumScale_16)); }
	inline float get_MaximumScale_16() const { return ___MaximumScale_16; }
	inline float* get_address_of_MaximumScale_16() { return &___MaximumScale_16; }
	inline void set_MaximumScale_16(float value)
	{
		___MaximumScale_16 = value;
	}

	inline static int32_t get_offset_of_BounceModifier_17() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___BounceModifier_17)); }
	inline float get_BounceModifier_17() const { return ___BounceModifier_17; }
	inline float* get_address_of_BounceModifier_17() { return &___BounceModifier_17; }
	inline void set_BounceModifier_17(float value)
	{
		___BounceModifier_17 = value;
	}

	inline static int32_t get_offset_of_DebugText_18() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___DebugText_18)); }
	inline Text_t1901882714 * get_DebugText_18() const { return ___DebugText_18; }
	inline Text_t1901882714 ** get_address_of_DebugText_18() { return &___DebugText_18; }
	inline void set_DebugText_18(Text_t1901882714 * value)
	{
		___DebugText_18 = value;
		Il2CppCodeGenWriteBarrier((&___DebugText_18), value);
	}

	inline static int32_t get_offset_of_U3CScaleGestureU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___U3CScaleGestureU3Ek__BackingField_19)); }
	inline ScaleGestureRecognizer_t1137887245 * get_U3CScaleGestureU3Ek__BackingField_19() const { return ___U3CScaleGestureU3Ek__BackingField_19; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_U3CScaleGestureU3Ek__BackingField_19() { return &___U3CScaleGestureU3Ek__BackingField_19; }
	inline void set_U3CScaleGestureU3Ek__BackingField_19(ScaleGestureRecognizer_t1137887245 * value)
	{
		___U3CScaleGestureU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScaleGestureU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___U3CPanGestureU3Ek__BackingField_20)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_20() const { return ___U3CPanGestureU3Ek__BackingField_20; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_20() { return &___U3CPanGestureU3Ek__BackingField_20; }
	inline void set_U3CPanGestureU3Ek__BackingField_20(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CDoubleTapGestureU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___U3CDoubleTapGestureU3Ek__BackingField_21)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CDoubleTapGestureU3Ek__BackingField_21() const { return ___U3CDoubleTapGestureU3Ek__BackingField_21; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CDoubleTapGestureU3Ek__BackingField_21() { return &___U3CDoubleTapGestureU3Ek__BackingField_21; }
	inline void set_U3CDoubleTapGestureU3Ek__BackingField_21(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CDoubleTapGestureU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDoubleTapGestureU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_contentRectTransform_22() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___contentRectTransform_22)); }
	inline RectTransform_t3704657025 * get_contentRectTransform_22() const { return ___contentRectTransform_22; }
	inline RectTransform_t3704657025 ** get_address_of_contentRectTransform_22() { return &___contentRectTransform_22; }
	inline void set_contentRectTransform_22(RectTransform_t3704657025 * value)
	{
		___contentRectTransform_22 = value;
		Il2CppCodeGenWriteBarrier((&___contentRectTransform_22), value);
	}

	inline static int32_t get_offset_of_containerRectTransform_23() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___containerRectTransform_23)); }
	inline RectTransform_t3704657025 * get_containerRectTransform_23() const { return ___containerRectTransform_23; }
	inline RectTransform_t3704657025 ** get_address_of_containerRectTransform_23() { return &___containerRectTransform_23; }
	inline void set_containerRectTransform_23(RectTransform_t3704657025 * value)
	{
		___containerRectTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___containerRectTransform_23), value);
	}

	inline static int32_t get_offset_of_panVelocity_24() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___panVelocity_24)); }
	inline Vector2_t2156229523  get_panVelocity_24() const { return ___panVelocity_24; }
	inline Vector2_t2156229523 * get_address_of_panVelocity_24() { return &___panVelocity_24; }
	inline void set_panVelocity_24(Vector2_t2156229523  value)
	{
		___panVelocity_24 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_25() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___zoomSpeed_25)); }
	inline float get_zoomSpeed_25() const { return ___zoomSpeed_25; }
	inline float* get_address_of_zoomSpeed_25() { return &___zoomSpeed_25; }
	inline void set_zoomSpeed_25(float value)
	{
		___zoomSpeed_25 = value;
	}

	inline static int32_t get_offset_of_lastScaleFocus_26() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___lastScaleFocus_26)); }
	inline Vector2_t2156229523  get_lastScaleFocus_26() const { return ___lastScaleFocus_26; }
	inline Vector2_t2156229523 * get_address_of_lastScaleFocus_26() { return &___lastScaleFocus_26; }
	inline void set_lastScaleFocus_26(Vector2_t2156229523  value)
	{
		___lastScaleFocus_26 = value;
	}

	inline static int32_t get_offset_of_doubleTapScaleTimeSecondsRemaining_27() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___doubleTapScaleTimeSecondsRemaining_27)); }
	inline float get_doubleTapScaleTimeSecondsRemaining_27() const { return ___doubleTapScaleTimeSecondsRemaining_27; }
	inline float* get_address_of_doubleTapScaleTimeSecondsRemaining_27() { return &___doubleTapScaleTimeSecondsRemaining_27; }
	inline void set_doubleTapScaleTimeSecondsRemaining_27(float value)
	{
		___doubleTapScaleTimeSecondsRemaining_27 = value;
	}

	inline static int32_t get_offset_of_doubleTapScaleStart_28() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___doubleTapScaleStart_28)); }
	inline float get_doubleTapScaleStart_28() const { return ___doubleTapScaleStart_28; }
	inline float* get_address_of_doubleTapScaleStart_28() { return &___doubleTapScaleStart_28; }
	inline void set_doubleTapScaleStart_28(float value)
	{
		___doubleTapScaleStart_28 = value;
	}

	inline static int32_t get_offset_of_doubleTapScaleEnd_29() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___doubleTapScaleEnd_29)); }
	inline float get_doubleTapScaleEnd_29() const { return ___doubleTapScaleEnd_29; }
	inline float* get_address_of_doubleTapScaleEnd_29() { return &___doubleTapScaleEnd_29; }
	inline void set_doubleTapScaleEnd_29(float value)
	{
		___doubleTapScaleEnd_29 = value;
	}

	inline static int32_t get_offset_of_doubleTapPosStart_30() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___doubleTapPosStart_30)); }
	inline Vector2_t2156229523  get_doubleTapPosStart_30() const { return ___doubleTapPosStart_30; }
	inline Vector2_t2156229523 * get_address_of_doubleTapPosStart_30() { return &___doubleTapPosStart_30; }
	inline void set_doubleTapPosStart_30(Vector2_t2156229523  value)
	{
		___doubleTapPosStart_30 = value;
	}

	inline static int32_t get_offset_of_doubleTapPosEnd_31() { return static_cast<int32_t>(offsetof(FingersScrollViewComponentScript_t3503049574, ___doubleTapPosEnd_31)); }
	inline Vector2_t2156229523  get_doubleTapPosEnd_31() const { return ___doubleTapPosEnd_31; }
	inline Vector2_t2156229523 * get_address_of_doubleTapPosEnd_31() { return &___doubleTapPosEnd_31; }
	inline void set_doubleTapPosEnd_31(Vector2_t2156229523  value)
	{
		___doubleTapPosEnd_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSSCROLLVIEWCOMPONENTSCRIPT_T3503049574_H
#ifndef FINGERSZOOMPANCAMERACOMPONENTSCRIPT_T2768970116_H
#define FINGERSZOOMPANCAMERACOMPONENTSCRIPT_T2768970116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersZoomPanCameraComponentScript
struct  FingersZoomPanCameraComponentScript_t2768970116  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider DigitalRubyShared.FingersZoomPanCameraComponentScript::VisibleArea
	Collider_t1773347010 * ___VisibleArea_4;
	// System.Single DigitalRubyShared.FingersZoomPanCameraComponentScript::Dampening
	float ___Dampening_5;
	// System.Single DigitalRubyShared.FingersZoomPanCameraComponentScript::RotationSpeed
	float ___RotationSpeed_6;
	// DigitalRubyShared.ScaleGestureRecognizer DigitalRubyShared.FingersZoomPanCameraComponentScript::<ScaleGesture>k__BackingField
	ScaleGestureRecognizer_t1137887245 * ___U3CScaleGestureU3Ek__BackingField_7;
	// DigitalRubyShared.PanGestureRecognizer DigitalRubyShared.FingersZoomPanCameraComponentScript::<PanGesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CPanGestureU3Ek__BackingField_8;
	// DigitalRubyShared.TapGestureRecognizer DigitalRubyShared.FingersZoomPanCameraComponentScript::<TapGesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CTapGestureU3Ek__BackingField_9;
	// DigitalRubyShared.RotateGestureRecognizer DigitalRubyShared.FingersZoomPanCameraComponentScript::<RotateGesture>k__BackingField
	RotateGestureRecognizer_t4100246528 * ___U3CRotateGestureU3Ek__BackingField_10;
	// UnityEngine.Vector3 DigitalRubyShared.FingersZoomPanCameraComponentScript::cameraAnimationTargetPosition
	Vector3_t3722313464  ___cameraAnimationTargetPosition_11;
	// UnityEngine.Vector3 DigitalRubyShared.FingersZoomPanCameraComponentScript::velocity
	Vector3_t3722313464  ___velocity_12;
	// UnityEngine.Camera DigitalRubyShared.FingersZoomPanCameraComponentScript::_camera
	Camera_t4157153871 * ____camera_13;

public:
	inline static int32_t get_offset_of_VisibleArea_4() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___VisibleArea_4)); }
	inline Collider_t1773347010 * get_VisibleArea_4() const { return ___VisibleArea_4; }
	inline Collider_t1773347010 ** get_address_of_VisibleArea_4() { return &___VisibleArea_4; }
	inline void set_VisibleArea_4(Collider_t1773347010 * value)
	{
		___VisibleArea_4 = value;
		Il2CppCodeGenWriteBarrier((&___VisibleArea_4), value);
	}

	inline static int32_t get_offset_of_Dampening_5() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___Dampening_5)); }
	inline float get_Dampening_5() const { return ___Dampening_5; }
	inline float* get_address_of_Dampening_5() { return &___Dampening_5; }
	inline void set_Dampening_5(float value)
	{
		___Dampening_5 = value;
	}

	inline static int32_t get_offset_of_RotationSpeed_6() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___RotationSpeed_6)); }
	inline float get_RotationSpeed_6() const { return ___RotationSpeed_6; }
	inline float* get_address_of_RotationSpeed_6() { return &___RotationSpeed_6; }
	inline void set_RotationSpeed_6(float value)
	{
		___RotationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_U3CScaleGestureU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___U3CScaleGestureU3Ek__BackingField_7)); }
	inline ScaleGestureRecognizer_t1137887245 * get_U3CScaleGestureU3Ek__BackingField_7() const { return ___U3CScaleGestureU3Ek__BackingField_7; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_U3CScaleGestureU3Ek__BackingField_7() { return &___U3CScaleGestureU3Ek__BackingField_7; }
	inline void set_U3CScaleGestureU3Ek__BackingField_7(ScaleGestureRecognizer_t1137887245 * value)
	{
		___U3CScaleGestureU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScaleGestureU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPanGestureU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___U3CPanGestureU3Ek__BackingField_8)); }
	inline PanGestureRecognizer_t195762396 * get_U3CPanGestureU3Ek__BackingField_8() const { return ___U3CPanGestureU3Ek__BackingField_8; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CPanGestureU3Ek__BackingField_8() { return &___U3CPanGestureU3Ek__BackingField_8; }
	inline void set_U3CPanGestureU3Ek__BackingField_8(PanGestureRecognizer_t195762396 * value)
	{
		___U3CPanGestureU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPanGestureU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CTapGestureU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___U3CTapGestureU3Ek__BackingField_9)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CTapGestureU3Ek__BackingField_9() const { return ___U3CTapGestureU3Ek__BackingField_9; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CTapGestureU3Ek__BackingField_9() { return &___U3CTapGestureU3Ek__BackingField_9; }
	inline void set_U3CTapGestureU3Ek__BackingField_9(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CTapGestureU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTapGestureU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CRotateGestureU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___U3CRotateGestureU3Ek__BackingField_10)); }
	inline RotateGestureRecognizer_t4100246528 * get_U3CRotateGestureU3Ek__BackingField_10() const { return ___U3CRotateGestureU3Ek__BackingField_10; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_U3CRotateGestureU3Ek__BackingField_10() { return &___U3CRotateGestureU3Ek__BackingField_10; }
	inline void set_U3CRotateGestureU3Ek__BackingField_10(RotateGestureRecognizer_t4100246528 * value)
	{
		___U3CRotateGestureU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRotateGestureU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_cameraAnimationTargetPosition_11() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___cameraAnimationTargetPosition_11)); }
	inline Vector3_t3722313464  get_cameraAnimationTargetPosition_11() const { return ___cameraAnimationTargetPosition_11; }
	inline Vector3_t3722313464 * get_address_of_cameraAnimationTargetPosition_11() { return &___cameraAnimationTargetPosition_11; }
	inline void set_cameraAnimationTargetPosition_11(Vector3_t3722313464  value)
	{
		___cameraAnimationTargetPosition_11 = value;
	}

	inline static int32_t get_offset_of_velocity_12() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ___velocity_12)); }
	inline Vector3_t3722313464  get_velocity_12() const { return ___velocity_12; }
	inline Vector3_t3722313464 * get_address_of_velocity_12() { return &___velocity_12; }
	inline void set_velocity_12(Vector3_t3722313464  value)
	{
		___velocity_12 = value;
	}

	inline static int32_t get_offset_of__camera_13() { return static_cast<int32_t>(offsetof(FingersZoomPanCameraComponentScript_t2768970116, ____camera_13)); }
	inline Camera_t4157153871 * get__camera_13() const { return ____camera_13; }
	inline Camera_t4157153871 ** get_address_of__camera_13() { return &____camera_13; }
	inline void set__camera_13(Camera_t4157153871 * value)
	{
		____camera_13 = value;
		Il2CppCodeGenWriteBarrier((&____camera_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSZOOMPANCAMERACOMPONENTSCRIPT_T2768970116_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPTBASE_T275484950_H
#define GESTURERECOGNIZERCOMPONENTSCRIPTBASE_T275484950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScriptBase
struct  GestureRecognizerComponentScriptBase_t275484950  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.GestureRecognizer DigitalRubyShared.GestureRecognizerComponentScriptBase::<GestureBase>k__BackingField
	GestureRecognizer_t3684029681 * ___U3CGestureBaseU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CGestureBaseU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScriptBase_t275484950, ___U3CGestureBaseU3Ek__BackingField_4)); }
	inline GestureRecognizer_t3684029681 * get_U3CGestureBaseU3Ek__BackingField_4() const { return ___U3CGestureBaseU3Ek__BackingField_4; }
	inline GestureRecognizer_t3684029681 ** get_address_of_U3CGestureBaseU3Ek__BackingField_4() { return &___U3CGestureBaseU3Ek__BackingField_4; }
	inline void set_U3CGestureBaseU3Ek__BackingField_4(GestureRecognizer_t3684029681 * value)
	{
		___U3CGestureBaseU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureBaseU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPTBASE_T275484950_H
#ifndef DEMOSCRIPTIMAGE_T4046729131_H
#define DEMOSCRIPTIMAGE_T4046729131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.DemoScriptImage
struct  DemoScriptImage_t4046729131  : public FingersImageAutomationScript_t4118243268
{
public:

public:
};

struct DemoScriptImage_t4046729131_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String> DigitalRubyShared.DemoScriptImage::recognizableImages
	Dictionary_2_t3194170630 * ___recognizableImages_15;

public:
	inline static int32_t get_offset_of_recognizableImages_15() { return static_cast<int32_t>(offsetof(DemoScriptImage_t4046729131_StaticFields, ___recognizableImages_15)); }
	inline Dictionary_2_t3194170630 * get_recognizableImages_15() const { return ___recognizableImages_15; }
	inline Dictionary_2_t3194170630 ** get_address_of_recognizableImages_15() { return &___recognizableImages_15; }
	inline void set_recognizableImages_15(Dictionary_2_t3194170630 * value)
	{
		___recognizableImages_15 = value;
		Il2CppCodeGenWriteBarrier((&___recognizableImages_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTIMAGE_T4046729131_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1289304367_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1289304367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.ImageGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t1289304367  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	ImageGestureRecognizer_t4233185475 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1289304367, ___U3CGestureU3Ek__BackingField_12)); }
	inline ImageGestureRecognizer_t4233185475 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline ImageGestureRecognizer_t4233185475 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(ImageGestureRecognizer_t4233185475 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1289304367_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1036896374_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1036896374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.LongPressGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t1036896374  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	LongPressGestureRecognizer_t3980777482 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1036896374, ___U3CGestureU3Ek__BackingField_12)); }
	inline LongPressGestureRecognizer_t3980777482 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline LongPressGestureRecognizer_t3980777482 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(LongPressGestureRecognizer_t3980777482 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1036896374_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T329012851_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T329012851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.OneTouchRotateGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t329012851  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	OneTouchRotateGestureRecognizer_t3272893959 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t329012851, ___U3CGestureU3Ek__BackingField_12)); }
	inline OneTouchRotateGestureRecognizer_t3272893959 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline OneTouchRotateGestureRecognizer_t3272893959 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(OneTouchRotateGestureRecognizer_t3272893959 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T329012851_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T2664755871_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T2664755871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.OneTouchScaleGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t2664755871  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	OneTouchScaleGestureRecognizer_t1313669683 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2664755871, ___U3CGestureU3Ek__BackingField_12)); }
	inline OneTouchScaleGestureRecognizer_t1313669683 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline OneTouchScaleGestureRecognizer_t1313669683 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(OneTouchScaleGestureRecognizer_t1313669683 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T2664755871_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1546848584_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1546848584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.PanGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t1546848584  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	PanGestureRecognizer_t195762396 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1546848584, ___U3CGestureU3Ek__BackingField_12)); }
	inline PanGestureRecognizer_t195762396 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline PanGestureRecognizer_t195762396 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(PanGestureRecognizer_t195762396 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1546848584_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1156365420_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1156365420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.RotateGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t1156365420  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	RotateGestureRecognizer_t4100246528 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t1156365420, ___U3CGestureU3Ek__BackingField_12)); }
	inline RotateGestureRecognizer_t4100246528 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline RotateGestureRecognizer_t4100246528 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(RotateGestureRecognizer_t4100246528 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T1156365420_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T2488973433_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T2488973433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.ScaleGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t2488973433  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	ScaleGestureRecognizer_t1137887245 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t2488973433, ___U3CGestureU3Ek__BackingField_12)); }
	inline ScaleGestureRecognizer_t1137887245 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline ScaleGestureRecognizer_t1137887245 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(ScaleGestureRecognizer_t1137887245 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T2488973433_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T3679598049_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T3679598049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.SwipeGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t3679598049  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	SwipeGestureRecognizer_t2328511861 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t3679598049, ___U3CGestureU3Ek__BackingField_12)); }
	inline SwipeGestureRecognizer_t2328511861 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline SwipeGestureRecognizer_t2328511861 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(SwipeGestureRecognizer_t2328511861 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T3679598049_H
#ifndef GESTURERECOGNIZERCOMPONENTSCRIPT_1_T235002562_H
#define GESTURERECOGNIZERCOMPONENTSCRIPT_1_T235002562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.GestureRecognizerComponentScript`1<DigitalRubyShared.TapGestureRecognizer>
struct  GestureRecognizerComponentScript_1_t235002562  : public GestureRecognizerComponentScriptBase_t275484950
{
public:
	// DigitalRubyShared.GestureRecognizerComponentStateUpdatedEvent DigitalRubyShared.GestureRecognizerComponentScript`1::GestureStateUpdated
	GestureRecognizerComponentStateUpdatedEvent_t1428864973 * ___GestureStateUpdated_5;
	// UnityEngine.GameObject DigitalRubyShared.GestureRecognizerComponentScript`1::GestureView
	GameObject_t1113636619 * ___GestureView_6;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MinimumNumberOfTouchesToTrack
	int32_t ___MinimumNumberOfTouchesToTrack_7;
	// System.Int32 DigitalRubyShared.GestureRecognizerComponentScript`1::MaximumNumberOfTouchesToTrack
	int32_t ___MaximumNumberOfTouchesToTrack_8;
	// System.Collections.Generic.List`1<DigitalRubyShared.GestureRecognizerComponentScriptBase> DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWith
	List_1_t1747559692 * ___AllowSimultaneousExecutionWith_9;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::AllowSimultaneousExecutionWithAllGestures
	bool ___AllowSimultaneousExecutionWithAllGestures_10;
	// System.Boolean DigitalRubyShared.GestureRecognizerComponentScript`1::ClearTrackedTouchesOnEndOrFail
	bool ___ClearTrackedTouchesOnEndOrFail_11;
	// T DigitalRubyShared.GestureRecognizerComponentScript`1::<Gesture>k__BackingField
	TapGestureRecognizer_t3178883670 * ___U3CGestureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_GestureStateUpdated_5() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___GestureStateUpdated_5)); }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 * get_GestureStateUpdated_5() const { return ___GestureStateUpdated_5; }
	inline GestureRecognizerComponentStateUpdatedEvent_t1428864973 ** get_address_of_GestureStateUpdated_5() { return &___GestureStateUpdated_5; }
	inline void set_GestureStateUpdated_5(GestureRecognizerComponentStateUpdatedEvent_t1428864973 * value)
	{
		___GestureStateUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___GestureStateUpdated_5), value);
	}

	inline static int32_t get_offset_of_GestureView_6() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___GestureView_6)); }
	inline GameObject_t1113636619 * get_GestureView_6() const { return ___GestureView_6; }
	inline GameObject_t1113636619 ** get_address_of_GestureView_6() { return &___GestureView_6; }
	inline void set_GestureView_6(GameObject_t1113636619 * value)
	{
		___GestureView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GestureView_6), value);
	}

	inline static int32_t get_offset_of_MinimumNumberOfTouchesToTrack_7() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___MinimumNumberOfTouchesToTrack_7)); }
	inline int32_t get_MinimumNumberOfTouchesToTrack_7() const { return ___MinimumNumberOfTouchesToTrack_7; }
	inline int32_t* get_address_of_MinimumNumberOfTouchesToTrack_7() { return &___MinimumNumberOfTouchesToTrack_7; }
	inline void set_MinimumNumberOfTouchesToTrack_7(int32_t value)
	{
		___MinimumNumberOfTouchesToTrack_7 = value;
	}

	inline static int32_t get_offset_of_MaximumNumberOfTouchesToTrack_8() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___MaximumNumberOfTouchesToTrack_8)); }
	inline int32_t get_MaximumNumberOfTouchesToTrack_8() const { return ___MaximumNumberOfTouchesToTrack_8; }
	inline int32_t* get_address_of_MaximumNumberOfTouchesToTrack_8() { return &___MaximumNumberOfTouchesToTrack_8; }
	inline void set_MaximumNumberOfTouchesToTrack_8(int32_t value)
	{
		___MaximumNumberOfTouchesToTrack_8 = value;
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWith_9() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___AllowSimultaneousExecutionWith_9)); }
	inline List_1_t1747559692 * get_AllowSimultaneousExecutionWith_9() const { return ___AllowSimultaneousExecutionWith_9; }
	inline List_1_t1747559692 ** get_address_of_AllowSimultaneousExecutionWith_9() { return &___AllowSimultaneousExecutionWith_9; }
	inline void set_AllowSimultaneousExecutionWith_9(List_1_t1747559692 * value)
	{
		___AllowSimultaneousExecutionWith_9 = value;
		Il2CppCodeGenWriteBarrier((&___AllowSimultaneousExecutionWith_9), value);
	}

	inline static int32_t get_offset_of_AllowSimultaneousExecutionWithAllGestures_10() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___AllowSimultaneousExecutionWithAllGestures_10)); }
	inline bool get_AllowSimultaneousExecutionWithAllGestures_10() const { return ___AllowSimultaneousExecutionWithAllGestures_10; }
	inline bool* get_address_of_AllowSimultaneousExecutionWithAllGestures_10() { return &___AllowSimultaneousExecutionWithAllGestures_10; }
	inline void set_AllowSimultaneousExecutionWithAllGestures_10(bool value)
	{
		___AllowSimultaneousExecutionWithAllGestures_10 = value;
	}

	inline static int32_t get_offset_of_ClearTrackedTouchesOnEndOrFail_11() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___ClearTrackedTouchesOnEndOrFail_11)); }
	inline bool get_ClearTrackedTouchesOnEndOrFail_11() const { return ___ClearTrackedTouchesOnEndOrFail_11; }
	inline bool* get_address_of_ClearTrackedTouchesOnEndOrFail_11() { return &___ClearTrackedTouchesOnEndOrFail_11; }
	inline void set_ClearTrackedTouchesOnEndOrFail_11(bool value)
	{
		___ClearTrackedTouchesOnEndOrFail_11 = value;
	}

	inline static int32_t get_offset_of_U3CGestureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GestureRecognizerComponentScript_1_t235002562, ___U3CGestureU3Ek__BackingField_12)); }
	inline TapGestureRecognizer_t3178883670 * get_U3CGestureU3Ek__BackingField_12() const { return ___U3CGestureU3Ek__BackingField_12; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_U3CGestureU3Ek__BackingField_12() { return &___U3CGestureU3Ek__BackingField_12; }
	inline void set_U3CGestureU3Ek__BackingField_12(TapGestureRecognizer_t3178883670 * value)
	{
		___U3CGestureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURERECOGNIZERCOMPONENTSCRIPT_1_T235002562_H
#ifndef IMAGEGESTURERECOGNIZERCOMPONENTSCRIPT_T448644263_H
#define IMAGEGESTURERECOGNIZERCOMPONENTSCRIPT_T448644263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ImageGestureRecognizerComponentScript
struct  ImageGestureRecognizerComponentScript_t448644263  : public GestureRecognizerComponentScript_1_t1289304367
{
public:
	// System.Int32 DigitalRubyShared.ImageGestureRecognizerComponentScript::MaximumPathCount
	int32_t ___MaximumPathCount_13;
	// System.Single DigitalRubyShared.ImageGestureRecognizerComponentScript::DirectionTolerance
	float ___DirectionTolerance_14;
	// System.Single DigitalRubyShared.ImageGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_15;
	// System.Single DigitalRubyShared.ImageGestureRecognizerComponentScript::MinimumDistanceBetweenPointsUnits
	float ___MinimumDistanceBetweenPointsUnits_16;
	// System.Single DigitalRubyShared.ImageGestureRecognizerComponentScript::SimilarityMinimum
	float ___SimilarityMinimum_17;
	// System.Int32 DigitalRubyShared.ImageGestureRecognizerComponentScript::MinimumPointsToRecognize
	int32_t ___MinimumPointsToRecognize_18;
	// System.Collections.Generic.List`1<DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry> DigitalRubyShared.ImageGestureRecognizerComponentScript::GestureImages
	List_1_t4185350021 * ___GestureImages_19;
	// System.Collections.Generic.Dictionary`2<DigitalRubyShared.ImageGestureImage,System.String> DigitalRubyShared.ImageGestureRecognizerComponentScript::<GestureImagesToKey>k__BackingField
	Dictionary_2_t3194170630 * ___U3CGestureImagesToKeyU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_MaximumPathCount_13() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___MaximumPathCount_13)); }
	inline int32_t get_MaximumPathCount_13() const { return ___MaximumPathCount_13; }
	inline int32_t* get_address_of_MaximumPathCount_13() { return &___MaximumPathCount_13; }
	inline void set_MaximumPathCount_13(int32_t value)
	{
		___MaximumPathCount_13 = value;
	}

	inline static int32_t get_offset_of_DirectionTolerance_14() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___DirectionTolerance_14)); }
	inline float get_DirectionTolerance_14() const { return ___DirectionTolerance_14; }
	inline float* get_address_of_DirectionTolerance_14() { return &___DirectionTolerance_14; }
	inline void set_DirectionTolerance_14(float value)
	{
		___DirectionTolerance_14 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_15() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___ThresholdUnits_15)); }
	inline float get_ThresholdUnits_15() const { return ___ThresholdUnits_15; }
	inline float* get_address_of_ThresholdUnits_15() { return &___ThresholdUnits_15; }
	inline void set_ThresholdUnits_15(float value)
	{
		___ThresholdUnits_15 = value;
	}

	inline static int32_t get_offset_of_MinimumDistanceBetweenPointsUnits_16() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___MinimumDistanceBetweenPointsUnits_16)); }
	inline float get_MinimumDistanceBetweenPointsUnits_16() const { return ___MinimumDistanceBetweenPointsUnits_16; }
	inline float* get_address_of_MinimumDistanceBetweenPointsUnits_16() { return &___MinimumDistanceBetweenPointsUnits_16; }
	inline void set_MinimumDistanceBetweenPointsUnits_16(float value)
	{
		___MinimumDistanceBetweenPointsUnits_16 = value;
	}

	inline static int32_t get_offset_of_SimilarityMinimum_17() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___SimilarityMinimum_17)); }
	inline float get_SimilarityMinimum_17() const { return ___SimilarityMinimum_17; }
	inline float* get_address_of_SimilarityMinimum_17() { return &___SimilarityMinimum_17; }
	inline void set_SimilarityMinimum_17(float value)
	{
		___SimilarityMinimum_17 = value;
	}

	inline static int32_t get_offset_of_MinimumPointsToRecognize_18() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___MinimumPointsToRecognize_18)); }
	inline int32_t get_MinimumPointsToRecognize_18() const { return ___MinimumPointsToRecognize_18; }
	inline int32_t* get_address_of_MinimumPointsToRecognize_18() { return &___MinimumPointsToRecognize_18; }
	inline void set_MinimumPointsToRecognize_18(int32_t value)
	{
		___MinimumPointsToRecognize_18 = value;
	}

	inline static int32_t get_offset_of_GestureImages_19() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___GestureImages_19)); }
	inline List_1_t4185350021 * get_GestureImages_19() const { return ___GestureImages_19; }
	inline List_1_t4185350021 ** get_address_of_GestureImages_19() { return &___GestureImages_19; }
	inline void set_GestureImages_19(List_1_t4185350021 * value)
	{
		___GestureImages_19 = value;
		Il2CppCodeGenWriteBarrier((&___GestureImages_19), value);
	}

	inline static int32_t get_offset_of_U3CGestureImagesToKeyU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ImageGestureRecognizerComponentScript_t448644263, ___U3CGestureImagesToKeyU3Ek__BackingField_20)); }
	inline Dictionary_2_t3194170630 * get_U3CGestureImagesToKeyU3Ek__BackingField_20() const { return ___U3CGestureImagesToKeyU3Ek__BackingField_20; }
	inline Dictionary_2_t3194170630 ** get_address_of_U3CGestureImagesToKeyU3Ek__BackingField_20() { return &___U3CGestureImagesToKeyU3Ek__BackingField_20; }
	inline void set_U3CGestureImagesToKeyU3Ek__BackingField_20(Dictionary_2_t3194170630 * value)
	{
		___U3CGestureImagesToKeyU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureImagesToKeyU3Ek__BackingField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEGESTURERECOGNIZERCOMPONENTSCRIPT_T448644263_H
#ifndef LONGPRESSGESTURERECOGNIZERCOMPONENTSCRIPT_T3685016334_H
#define LONGPRESSGESTURERECOGNIZERCOMPONENTSCRIPT_T3685016334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.LongPressGestureRecognizerComponentScript
struct  LongPressGestureRecognizerComponentScript_t3685016334  : public GestureRecognizerComponentScript_1_t1036896374
{
public:
	// System.Single DigitalRubyShared.LongPressGestureRecognizerComponentScript::MinimumDurationSeconds
	float ___MinimumDurationSeconds_13;
	// System.Single DigitalRubyShared.LongPressGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_14;

public:
	inline static int32_t get_offset_of_MinimumDurationSeconds_13() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizerComponentScript_t3685016334, ___MinimumDurationSeconds_13)); }
	inline float get_MinimumDurationSeconds_13() const { return ___MinimumDurationSeconds_13; }
	inline float* get_address_of_MinimumDurationSeconds_13() { return &___MinimumDurationSeconds_13; }
	inline void set_MinimumDurationSeconds_13(float value)
	{
		___MinimumDurationSeconds_13 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_14() { return static_cast<int32_t>(offsetof(LongPressGestureRecognizerComponentScript_t3685016334, ___ThresholdUnits_14)); }
	inline float get_ThresholdUnits_14() const { return ___ThresholdUnits_14; }
	inline float* get_address_of_ThresholdUnits_14() { return &___ThresholdUnits_14; }
	inline void set_ThresholdUnits_14(float value)
	{
		___ThresholdUnits_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPRESSGESTURERECOGNIZERCOMPONENTSCRIPT_T3685016334_H
#ifndef ONETOUCHROTATEGESTURERECOGNIZERCOMPONENTSCRIPT_T2094839816_H
#define ONETOUCHROTATEGESTURERECOGNIZERCOMPONENTSCRIPT_T2094839816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.OneTouchRotateGestureRecognizerComponentScript
struct  OneTouchRotateGestureRecognizerComponentScript_t2094839816  : public GestureRecognizerComponentScript_1_t329012851
{
public:
	// System.Single DigitalRubyShared.OneTouchRotateGestureRecognizerComponentScript::AngleThreshold
	float ___AngleThreshold_13;
	// System.Single DigitalRubyShared.OneTouchRotateGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_14;

public:
	inline static int32_t get_offset_of_AngleThreshold_13() { return static_cast<int32_t>(offsetof(OneTouchRotateGestureRecognizerComponentScript_t2094839816, ___AngleThreshold_13)); }
	inline float get_AngleThreshold_13() const { return ___AngleThreshold_13; }
	inline float* get_address_of_AngleThreshold_13() { return &___AngleThreshold_13; }
	inline void set_AngleThreshold_13(float value)
	{
		___AngleThreshold_13 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_14() { return static_cast<int32_t>(offsetof(OneTouchRotateGestureRecognizerComponentScript_t2094839816, ___ThresholdUnits_14)); }
	inline float get_ThresholdUnits_14() const { return ___ThresholdUnits_14; }
	inline float* get_address_of_ThresholdUnits_14() { return &___ThresholdUnits_14; }
	inline void set_ThresholdUnits_14(float value)
	{
		___ThresholdUnits_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONETOUCHROTATEGESTURERECOGNIZERCOMPONENTSCRIPT_T2094839816_H
#ifndef ONETOUCHSCALEGESTURERECOGNIZERCOMPONENTSCRIPT_T3319081723_H
#define ONETOUCHSCALEGESTURERECOGNIZERCOMPONENTSCRIPT_T3319081723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.OneTouchScaleGestureRecognizerComponentScript
struct  OneTouchScaleGestureRecognizerComponentScript_t3319081723  : public GestureRecognizerComponentScript_1_t2664755871
{
public:
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizerComponentScript::ZoomSpeed
	float ___ZoomSpeed_13;
	// System.Single DigitalRubyShared.OneTouchScaleGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_14;

public:
	inline static int32_t get_offset_of_ZoomSpeed_13() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizerComponentScript_t3319081723, ___ZoomSpeed_13)); }
	inline float get_ZoomSpeed_13() const { return ___ZoomSpeed_13; }
	inline float* get_address_of_ZoomSpeed_13() { return &___ZoomSpeed_13; }
	inline void set_ZoomSpeed_13(float value)
	{
		___ZoomSpeed_13 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_14() { return static_cast<int32_t>(offsetof(OneTouchScaleGestureRecognizerComponentScript_t3319081723, ___ThresholdUnits_14)); }
	inline float get_ThresholdUnits_14() const { return ___ThresholdUnits_14; }
	inline float* get_address_of_ThresholdUnits_14() { return &___ThresholdUnits_14; }
	inline void set_ThresholdUnits_14(float value)
	{
		___ThresholdUnits_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONETOUCHSCALEGESTURERECOGNIZERCOMPONENTSCRIPT_T3319081723_H
#ifndef PANGESTURERECOGNIZERCOMPONENTSCRIPT_T1777692607_H
#define PANGESTURERECOGNIZERCOMPONENTSCRIPT_T1777692607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.PanGestureRecognizerComponentScript
struct  PanGestureRecognizerComponentScript_t1777692607  : public GestureRecognizerComponentScript_1_t1546848584
{
public:
	// System.Single DigitalRubyShared.PanGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_13;

public:
	inline static int32_t get_offset_of_ThresholdUnits_13() { return static_cast<int32_t>(offsetof(PanGestureRecognizerComponentScript_t1777692607, ___ThresholdUnits_13)); }
	inline float get_ThresholdUnits_13() const { return ___ThresholdUnits_13; }
	inline float* get_address_of_ThresholdUnits_13() { return &___ThresholdUnits_13; }
	inline void set_ThresholdUnits_13(float value)
	{
		___ThresholdUnits_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANGESTURERECOGNIZERCOMPONENTSCRIPT_T1777692607_H
#ifndef ROTATEGESTURERECOGNIZERCOMPONENTSCRIPT_T638097468_H
#define ROTATEGESTURERECOGNIZERCOMPONENTSCRIPT_T638097468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.RotateGestureRecognizerComponentScript
struct  RotateGestureRecognizerComponentScript_t638097468  : public GestureRecognizerComponentScript_1_t1156365420
{
public:
	// System.Single DigitalRubyShared.RotateGestureRecognizerComponentScript::AngleThreshold
	float ___AngleThreshold_13;
	// System.Single DigitalRubyShared.RotateGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_14;

public:
	inline static int32_t get_offset_of_AngleThreshold_13() { return static_cast<int32_t>(offsetof(RotateGestureRecognizerComponentScript_t638097468, ___AngleThreshold_13)); }
	inline float get_AngleThreshold_13() const { return ___AngleThreshold_13; }
	inline float* get_address_of_AngleThreshold_13() { return &___AngleThreshold_13; }
	inline void set_AngleThreshold_13(float value)
	{
		___AngleThreshold_13 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_14() { return static_cast<int32_t>(offsetof(RotateGestureRecognizerComponentScript_t638097468, ___ThresholdUnits_14)); }
	inline float get_ThresholdUnits_14() const { return ___ThresholdUnits_14; }
	inline float* get_address_of_ThresholdUnits_14() { return &___ThresholdUnits_14; }
	inline void set_ThresholdUnits_14(float value)
	{
		___ThresholdUnits_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEGESTURERECOGNIZERCOMPONENTSCRIPT_T638097468_H
#ifndef SCALEGESTURERECOGNIZERCOMPONENTSCRIPT_T2181606707_H
#define SCALEGESTURERECOGNIZERCOMPONENTSCRIPT_T2181606707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.ScaleGestureRecognizerComponentScript
struct  ScaleGestureRecognizerComponentScript_t2181606707  : public GestureRecognizerComponentScript_1_t2488973433
{
public:
	// System.Single DigitalRubyShared.ScaleGestureRecognizerComponentScript::ZoomSpeed
	float ___ZoomSpeed_13;
	// System.Single DigitalRubyShared.ScaleGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_14;

public:
	inline static int32_t get_offset_of_ZoomSpeed_13() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizerComponentScript_t2181606707, ___ZoomSpeed_13)); }
	inline float get_ZoomSpeed_13() const { return ___ZoomSpeed_13; }
	inline float* get_address_of_ZoomSpeed_13() { return &___ZoomSpeed_13; }
	inline void set_ZoomSpeed_13(float value)
	{
		___ZoomSpeed_13 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_14() { return static_cast<int32_t>(offsetof(ScaleGestureRecognizerComponentScript_t2181606707, ___ThresholdUnits_14)); }
	inline float get_ThresholdUnits_14() const { return ___ThresholdUnits_14; }
	inline float* get_address_of_ThresholdUnits_14() { return &___ThresholdUnits_14; }
	inline void set_ThresholdUnits_14(float value)
	{
		___ThresholdUnits_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEGESTURERECOGNIZERCOMPONENTSCRIPT_T2181606707_H
#ifndef SWIPEGESTURERECOGNIZERCOMPONENTSCRIPT_T1446920306_H
#define SWIPEGESTURERECOGNIZERCOMPONENTSCRIPT_T1446920306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.SwipeGestureRecognizerComponentScript
struct  SwipeGestureRecognizerComponentScript_t1446920306  : public GestureRecognizerComponentScript_1_t3679598049
{
public:
	// DigitalRubyShared.SwipeGestureRecognizerDirection DigitalRubyShared.SwipeGestureRecognizerComponentScript::Direction
	int32_t ___Direction_13;
	// System.Single DigitalRubyShared.SwipeGestureRecognizerComponentScript::MinimumDistanceUnits
	float ___MinimumDistanceUnits_14;
	// System.Single DigitalRubyShared.SwipeGestureRecognizerComponentScript::MinimumSpeedUnits
	float ___MinimumSpeedUnits_15;
	// System.Single DigitalRubyShared.SwipeGestureRecognizerComponentScript::DirectionThreshold
	float ___DirectionThreshold_16;
	// DigitalRubyShared.SwipeGestureRecognizerEndMode DigitalRubyShared.SwipeGestureRecognizerComponentScript::EndMode
	int32_t ___EndMode_17;
	// System.Boolean DigitalRubyShared.SwipeGestureRecognizerComponentScript::FailOnDirectionChange
	bool ___FailOnDirectionChange_18;

public:
	inline static int32_t get_offset_of_Direction_13() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerComponentScript_t1446920306, ___Direction_13)); }
	inline int32_t get_Direction_13() const { return ___Direction_13; }
	inline int32_t* get_address_of_Direction_13() { return &___Direction_13; }
	inline void set_Direction_13(int32_t value)
	{
		___Direction_13 = value;
	}

	inline static int32_t get_offset_of_MinimumDistanceUnits_14() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerComponentScript_t1446920306, ___MinimumDistanceUnits_14)); }
	inline float get_MinimumDistanceUnits_14() const { return ___MinimumDistanceUnits_14; }
	inline float* get_address_of_MinimumDistanceUnits_14() { return &___MinimumDistanceUnits_14; }
	inline void set_MinimumDistanceUnits_14(float value)
	{
		___MinimumDistanceUnits_14 = value;
	}

	inline static int32_t get_offset_of_MinimumSpeedUnits_15() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerComponentScript_t1446920306, ___MinimumSpeedUnits_15)); }
	inline float get_MinimumSpeedUnits_15() const { return ___MinimumSpeedUnits_15; }
	inline float* get_address_of_MinimumSpeedUnits_15() { return &___MinimumSpeedUnits_15; }
	inline void set_MinimumSpeedUnits_15(float value)
	{
		___MinimumSpeedUnits_15 = value;
	}

	inline static int32_t get_offset_of_DirectionThreshold_16() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerComponentScript_t1446920306, ___DirectionThreshold_16)); }
	inline float get_DirectionThreshold_16() const { return ___DirectionThreshold_16; }
	inline float* get_address_of_DirectionThreshold_16() { return &___DirectionThreshold_16; }
	inline void set_DirectionThreshold_16(float value)
	{
		___DirectionThreshold_16 = value;
	}

	inline static int32_t get_offset_of_EndMode_17() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerComponentScript_t1446920306, ___EndMode_17)); }
	inline int32_t get_EndMode_17() const { return ___EndMode_17; }
	inline int32_t* get_address_of_EndMode_17() { return &___EndMode_17; }
	inline void set_EndMode_17(int32_t value)
	{
		___EndMode_17 = value;
	}

	inline static int32_t get_offset_of_FailOnDirectionChange_18() { return static_cast<int32_t>(offsetof(SwipeGestureRecognizerComponentScript_t1446920306, ___FailOnDirectionChange_18)); }
	inline bool get_FailOnDirectionChange_18() const { return ___FailOnDirectionChange_18; }
	inline bool* get_address_of_FailOnDirectionChange_18() { return &___FailOnDirectionChange_18; }
	inline void set_FailOnDirectionChange_18(bool value)
	{
		___FailOnDirectionChange_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTURERECOGNIZERCOMPONENTSCRIPT_T1446920306_H
#ifndef TAPGESTURERECOGNIZERCOMPONENTSCRIPT_T536059327_H
#define TAPGESTURERECOGNIZERCOMPONENTSCRIPT_T536059327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.TapGestureRecognizerComponentScript
struct  TapGestureRecognizerComponentScript_t536059327  : public GestureRecognizerComponentScript_1_t235002562
{
public:
	// System.Int32 DigitalRubyShared.TapGestureRecognizerComponentScript::NumberOfTapsRequired
	int32_t ___NumberOfTapsRequired_13;
	// System.Single DigitalRubyShared.TapGestureRecognizerComponentScript::ThresholdSeconds
	float ___ThresholdSeconds_14;
	// System.Single DigitalRubyShared.TapGestureRecognizerComponentScript::ThresholdUnits
	float ___ThresholdUnits_15;
	// System.Boolean DigitalRubyShared.TapGestureRecognizerComponentScript::SendBeginState
	bool ___SendBeginState_16;

public:
	inline static int32_t get_offset_of_NumberOfTapsRequired_13() { return static_cast<int32_t>(offsetof(TapGestureRecognizerComponentScript_t536059327, ___NumberOfTapsRequired_13)); }
	inline int32_t get_NumberOfTapsRequired_13() const { return ___NumberOfTapsRequired_13; }
	inline int32_t* get_address_of_NumberOfTapsRequired_13() { return &___NumberOfTapsRequired_13; }
	inline void set_NumberOfTapsRequired_13(int32_t value)
	{
		___NumberOfTapsRequired_13 = value;
	}

	inline static int32_t get_offset_of_ThresholdSeconds_14() { return static_cast<int32_t>(offsetof(TapGestureRecognizerComponentScript_t536059327, ___ThresholdSeconds_14)); }
	inline float get_ThresholdSeconds_14() const { return ___ThresholdSeconds_14; }
	inline float* get_address_of_ThresholdSeconds_14() { return &___ThresholdSeconds_14; }
	inline void set_ThresholdSeconds_14(float value)
	{
		___ThresholdSeconds_14 = value;
	}

	inline static int32_t get_offset_of_ThresholdUnits_15() { return static_cast<int32_t>(offsetof(TapGestureRecognizerComponentScript_t536059327, ___ThresholdUnits_15)); }
	inline float get_ThresholdUnits_15() const { return ___ThresholdUnits_15; }
	inline float* get_address_of_ThresholdUnits_15() { return &___ThresholdUnits_15; }
	inline void set_ThresholdUnits_15(float value)
	{
		___ThresholdUnits_15 = value;
	}

	inline static int32_t get_offset_of_SendBeginState_16() { return static_cast<int32_t>(offsetof(TapGestureRecognizerComponentScript_t536059327, ___SendBeginState_16)); }
	inline bool get_SendBeginState_16() const { return ___SendBeginState_16; }
	inline bool* get_address_of_SendBeginState_16() { return &___SendBeginState_16; }
	inline void set_SendBeginState_16(bool value)
	{
		___SendBeginState_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPGESTURERECOGNIZERCOMPONENTSCRIPT_T536059327_H
#ifndef FINGERSIMAGEGESTUREHELPERCOMPONENTSCRIPT_T4247928971_H
#define FINGERSIMAGEGESTUREHELPERCOMPONENTSCRIPT_T4247928971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRubyShared.FingersImageGestureHelperComponentScript
struct  FingersImageGestureHelperComponentScript_t4247928971  : public ImageGestureRecognizerComponentScript_t448644263
{
public:
	// UnityEngine.LineRenderer[] DigitalRubyShared.FingersImageGestureHelperComponentScript::LineRenderers
	LineRendererU5BU5D_t976293515* ___LineRenderers_21;
	// UnityEngine.UI.Text DigitalRubyShared.FingersImageGestureHelperComponentScript::MatchText
	Text_t1901882714 * ___MatchText_22;
	// System.EventHandler DigitalRubyShared.FingersImageGestureHelperComponentScript::LinesUpdated
	EventHandler_t1348719766 * ___LinesUpdated_23;
	// System.EventHandler DigitalRubyShared.FingersImageGestureHelperComponentScript::LinesCleared
	EventHandler_t1348719766 * ___LinesCleared_24;
	// UnityEngine.Color DigitalRubyShared.FingersImageGestureHelperComponentScript::savedColor
	Color_t2555686324  ___savedColor_25;
	// DigitalRubyShared.ImageGestureImage DigitalRubyShared.FingersImageGestureHelperComponentScript::matchedImage
	ImageGestureImage_t2885596271 * ___matchedImage_26;
	// System.Single DigitalRubyShared.FingersImageGestureHelperComponentScript::animationTime
	float ___animationTime_28;

public:
	inline static int32_t get_offset_of_LineRenderers_21() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___LineRenderers_21)); }
	inline LineRendererU5BU5D_t976293515* get_LineRenderers_21() const { return ___LineRenderers_21; }
	inline LineRendererU5BU5D_t976293515** get_address_of_LineRenderers_21() { return &___LineRenderers_21; }
	inline void set_LineRenderers_21(LineRendererU5BU5D_t976293515* value)
	{
		___LineRenderers_21 = value;
		Il2CppCodeGenWriteBarrier((&___LineRenderers_21), value);
	}

	inline static int32_t get_offset_of_MatchText_22() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___MatchText_22)); }
	inline Text_t1901882714 * get_MatchText_22() const { return ___MatchText_22; }
	inline Text_t1901882714 ** get_address_of_MatchText_22() { return &___MatchText_22; }
	inline void set_MatchText_22(Text_t1901882714 * value)
	{
		___MatchText_22 = value;
		Il2CppCodeGenWriteBarrier((&___MatchText_22), value);
	}

	inline static int32_t get_offset_of_LinesUpdated_23() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___LinesUpdated_23)); }
	inline EventHandler_t1348719766 * get_LinesUpdated_23() const { return ___LinesUpdated_23; }
	inline EventHandler_t1348719766 ** get_address_of_LinesUpdated_23() { return &___LinesUpdated_23; }
	inline void set_LinesUpdated_23(EventHandler_t1348719766 * value)
	{
		___LinesUpdated_23 = value;
		Il2CppCodeGenWriteBarrier((&___LinesUpdated_23), value);
	}

	inline static int32_t get_offset_of_LinesCleared_24() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___LinesCleared_24)); }
	inline EventHandler_t1348719766 * get_LinesCleared_24() const { return ___LinesCleared_24; }
	inline EventHandler_t1348719766 ** get_address_of_LinesCleared_24() { return &___LinesCleared_24; }
	inline void set_LinesCleared_24(EventHandler_t1348719766 * value)
	{
		___LinesCleared_24 = value;
		Il2CppCodeGenWriteBarrier((&___LinesCleared_24), value);
	}

	inline static int32_t get_offset_of_savedColor_25() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___savedColor_25)); }
	inline Color_t2555686324  get_savedColor_25() const { return ___savedColor_25; }
	inline Color_t2555686324 * get_address_of_savedColor_25() { return &___savedColor_25; }
	inline void set_savedColor_25(Color_t2555686324  value)
	{
		___savedColor_25 = value;
	}

	inline static int32_t get_offset_of_matchedImage_26() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___matchedImage_26)); }
	inline ImageGestureImage_t2885596271 * get_matchedImage_26() const { return ___matchedImage_26; }
	inline ImageGestureImage_t2885596271 ** get_address_of_matchedImage_26() { return &___matchedImage_26; }
	inline void set_matchedImage_26(ImageGestureImage_t2885596271 * value)
	{
		___matchedImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___matchedImage_26), value);
	}

	inline static int32_t get_offset_of_animationTime_28() { return static_cast<int32_t>(offsetof(FingersImageGestureHelperComponentScript_t4247928971, ___animationTime_28)); }
	inline float get_animationTime_28() const { return ___animationTime_28; }
	inline float* get_address_of_animationTime_28() { return &___animationTime_28; }
	inline void set_animationTime_28(float value)
	{
		___animationTime_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINGERSIMAGEGESTUREHELPERCOMPONENTSCRIPT_T4247928971_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5000 = { sizeof (DemoScriptImage_t4046729131), -1, sizeof(DemoScriptImage_t4046729131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5000[3] = 
{
	0,
	0,
	DemoScriptImage_t4046729131_StaticFields::get_offset_of_recognizableImages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5001 = { sizeof (DemoScriptImageHelper_t36059542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5001[3] = 
{
	DemoScriptImageHelper_t36059542::get_offset_of_ImageScript_4(),
	DemoScriptImageHelper_t36059542::get_offset_of_MatchParticleSystem_5(),
	DemoScriptImageHelper_t36059542::get_offset_of_AudioSourceOnMatch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5002 = { sizeof (DemoScriptJoystick_t699687243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5002[4] = 
{
	DemoScriptJoystick_t699687243::get_offset_of_JoystickScript_4(),
	DemoScriptJoystick_t699687243::get_offset_of_Mover_5(),
	DemoScriptJoystick_t699687243::get_offset_of_Speed_6(),
	DemoScriptJoystick_t699687243::get_offset_of_MoveJoystickToGestureStartLocation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5003 = { sizeof (DemoScriptMultiDrag_t759341426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5003[2] = 
{
	DemoScriptMultiDrag_t759341426::get_offset_of_AsteroidPrefab_4(),
	DemoScriptMultiDrag_t759341426::get_offset_of_SpawnCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5004 = { sizeof (DemoScriptMultiFingerTap_t3973590414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5004[1] = 
{
	DemoScriptMultiFingerTap_t3973590414::get_offset_of_statusText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5005 = { sizeof (DemoScriptOneFinger_t2754275351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5005[5] = 
{
	DemoScriptOneFinger_t2754275351::get_offset_of_RotateIcon_4(),
	DemoScriptOneFinger_t2754275351::get_offset_of_ScaleIcon_5(),
	DemoScriptOneFinger_t2754275351::get_offset_of_Earth_6(),
	DemoScriptOneFinger_t2754275351::get_offset_of_rotationGesture_7(),
	DemoScriptOneFinger_t2754275351::get_offset_of_scaleGesture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5006 = { sizeof (DemoScriptPan_t3102497188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5006[3] = 
{
	DemoScriptPan_t3102497188::get_offset_of_FingersScript_4(),
	DemoScriptPan_t3102497188::get_offset_of_log_5(),
	DemoScriptPan_t3102497188::get_offset_of_logView_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5007 = { sizeof (DemoScriptPlatformSpecificView_t3383617572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5007[2] = 
{
	DemoScriptPlatformSpecificView_t3383617572::get_offset_of_LeftPanel_4(),
	DemoScriptPlatformSpecificView_t3383617572::get_offset_of_Cube_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5008 = { sizeof (DemoScriptRaycast_t1876970581), -1, sizeof(DemoScriptRaycast_t1876970581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5008[4] = 
{
	DemoScriptRaycast_t1876970581::get_offset_of_Asteroids_4(),
	DemoScriptRaycast_t1876970581::get_offset_of_OtherObjects_5(),
	DemoScriptRaycast_t1876970581::get_offset_of_TapGestures_6(),
	DemoScriptRaycast_t1876970581_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5009 = { sizeof (DemoScriptSwipe_t1996673777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5009[5] = 
{
	DemoScriptSwipe_t1996673777::get_offset_of_SwipeParticleSystem_4(),
	DemoScriptSwipe_t1996673777::get_offset_of_SwipeTouchCount_5(),
	DemoScriptSwipe_t1996673777::get_offset_of_SwipeMode_6(),
	DemoScriptSwipe_t1996673777::get_offset_of_Image_7(),
	DemoScriptSwipe_t1996673777::get_offset_of_swipe_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5010 = { sizeof (DemoScriptTapAndDoubleTap_t888085429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5010[2] = 
{
	DemoScriptTapAndDoubleTap_t888085429::get_offset_of_tapGesture_4(),
	DemoScriptTapAndDoubleTap_t888085429::get_offset_of_doubleTapGesture_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5011 = { sizeof (DemoScriptZoomableScrollView_t163084534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5011[11] = 
{
	DemoScriptZoomableScrollView_t163084534::get_offset_of_ScrollContent_4(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_DebugText_5(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_Canvas_6(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_contentRectTransform_7(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scaleStart_8(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scaleEnd_9(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scaleTime_10(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_elapsedScaleTime_11(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scalePosStart_12(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_scalePosEnd_13(),
	DemoScriptZoomableScrollView_t163084534::get_offset_of_panVelocity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5012 = { sizeof (DemoScriptZoomPanCamera_t3831420416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5013 = { sizeof (FingersDragDropComponentScript_t1907472512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5013[12] = 
{
	FingersDragDropComponentScript_t1907472512::get_offset_of_Camera_4(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_BringToFront_5(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_DragScale_6(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_U3CLongPressGestureU3Ek__BackingField_7(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_rigidBody_8(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_spriteRenderer_9(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_startSortOrder_10(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_panZ_11(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_panOffset_12(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_DragStarted_13(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_DragUpdated_14(),
	FingersDragDropComponentScript_t1907472512::get_offset_of_DragEnded_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5014 = { sizeof (FingersImageGestureHelperComponentScript_t4247928971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5014[8] = 
{
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_LineRenderers_21(),
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_MatchText_22(),
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_LinesUpdated_23(),
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_LinesCleared_24(),
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_savedColor_25(),
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_matchedImage_26(),
	0,
	FingersImageGestureHelperComponentScript_t4247928971::get_offset_of_animationTime_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5015 = { sizeof (FingersMultiDragComponentScript_t4189350000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5015[7] = 
{
	FingersMultiDragComponentScript_t4189350000::get_offset_of_TagToDrag_4(),
	FingersMultiDragComponentScript_t4189350000::get_offset_of_NameToDrag_5(),
	FingersMultiDragComponentScript_t4189350000::get_offset_of_draggingObjectsByGameObject_6(),
	FingersMultiDragComponentScript_t4189350000::get_offset_of_draggingObjectsByTouchId_7(),
	FingersMultiDragComponentScript_t4189350000::get_offset_of_allTouchIds_8(),
	FingersMultiDragComponentScript_t4189350000::get_offset_of_currentTouchIds_9(),
	FingersMultiDragComponentScript_t4189350000::get_offset_of_U3CPanGestureU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5016 = { sizeof (DragState_t3135019656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5016[2] = 
{
	DragState_t3135019656::get_offset_of_GameObject_0(),
	DragState_t3135019656::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5017 = { sizeof (FingersPanOrbitComponentScript_t1753982191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5017[32] = 
{
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitTarget_4(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_Orbiter_5(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_MinimumDistance_6(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_MaximumDistance_7(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_ZoomSpeed_8(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_ZoomLookAtSpeed_9(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_ZoomThresholdUnits_10(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitXSpeed_11(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitXMaxDegrees_12(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_XAxisMovementType_13(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitXPanSpeed_14(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitXPanLimit_15(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitYSpeed_16(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitYMaxDegrees_17(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_YAxisMovementType_18(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitYPanSpeed_19(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitYPanLimit_20(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_AllowOrbitWhileZooming_21(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_allowOrbitWhileZooming_22(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_AllowMovementOnBothAxisSimultaneously_23(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_lockedAxis_24(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitInertia_25(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitMaximumSize_26(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_RequireOrbitGesturesToStartOnTarget_27(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_U3CScaleGestureU3Ek__BackingField_28(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_U3CPanGestureU3Ek__BackingField_29(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_U3CTapGestureU3Ek__BackingField_30(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_xDegrees_31(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_yDegrees_32(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_panVelocity_33(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_zoomSpeed_34(),
	FingersPanOrbitComponentScript_t1753982191::get_offset_of_OrbitTargetTapped_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5018 = { sizeof (PanOrbitMovementType_t3736241033)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5018[4] = 
{
	PanOrbitMovementType_t3736241033::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5019 = { sizeof (FingersPanRotateScaleComponentScript_t154562084), -1, sizeof(FingersPanRotateScaleComponentScript_t154562084_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5019[18] = 
{
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_Camera_4(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_BringToFront_5(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_AllowExecutionWithAllGestures_6(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_Mode_7(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_MinMaxScale_8(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_U3CPanGestureU3Ek__BackingField_9(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_U3CScaleGestureU3Ek__BackingField_10(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_U3CRotateGestureU3Ek__BackingField_11(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_rigidBody2D_12(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_rigidBody_13(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_spriteRenderer_14(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_canvasRenderer_15(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of__transform_16(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_startSortOrder_17(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_panZ_18(),
	FingersPanRotateScaleComponentScript_t154562084::get_offset_of_panOffset_19(),
	FingersPanRotateScaleComponentScript_t154562084_StaticFields::get_offset_of_captureRaycastResults_20(),
	FingersPanRotateScaleComponentScript_t154562084_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5020 = { sizeof (FingersPlatformMoveJumpComponentScript_t377169379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5020[11] = 
{
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_playerBody_4(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_MaxSpeed_5(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_JumpForce_6(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_JumpThresholdUnits_7(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_JumpThresholdSeconds_8(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_MoveSpeed_9(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_MoveThresholdUnits_10(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_U3CTapGestureU3Ek__BackingField_11(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_U3CPanGestureU3Ek__BackingField_12(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_U3CSwipeGestureU3Ek__BackingField_13(),
	FingersPlatformMoveJumpComponentScript_t377169379::get_offset_of_overlapArray_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5021 = { sizeof (U3CStopFallThroughU3Ec__Iterator0_t1183277613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5021[4] = 
{
	U3CStopFallThroughU3Ec__Iterator0_t1183277613::get_offset_of_effector_0(),
	U3CStopFallThroughU3Ec__Iterator0_t1183277613::get_offset_of_U24current_1(),
	U3CStopFallThroughU3Ec__Iterator0_t1183277613::get_offset_of_U24disposing_2(),
	U3CStopFallThroughU3Ec__Iterator0_t1183277613::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5022 = { sizeof (FingersRotateOrbitComponentScript_t3348659367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5022[5] = 
{
	FingersRotateOrbitComponentScript_t3348659367::get_offset_of_OrbitTarget_4(),
	FingersRotateOrbitComponentScript_t3348659367::get_offset_of_Orbiter_5(),
	FingersRotateOrbitComponentScript_t3348659367::get_offset_of_Axis_6(),
	FingersRotateOrbitComponentScript_t3348659367::get_offset_of_RotationSpeed_7(),
	FingersRotateOrbitComponentScript_t3348659367::get_offset_of_U3CRotationGestureU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5023 = { sizeof (FingersScrollViewComponentScript_t3503049574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5023[28] = 
{
	FingersScrollViewComponentScript_t3503049574::get_offset_of_ScrollContent_4(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_ScrollContentContainer_5(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_CanvasCamera_6(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_MaxSpeed_7(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_DoubleTapZoomOutThreshold_8(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_DoubleTapZoomOutValue_9(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_DoubleTapZoomInValue_10(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_DoubleTapAnimationTimeSeconds_11(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_PanDampening_12(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_ScaleDampening_13(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_ScaleSpeed_14(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_MinimumScale_15(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_MaximumScale_16(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_BounceModifier_17(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_DebugText_18(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_U3CScaleGestureU3Ek__BackingField_19(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_U3CPanGestureU3Ek__BackingField_20(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_U3CDoubleTapGestureU3Ek__BackingField_21(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_contentRectTransform_22(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_containerRectTransform_23(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_panVelocity_24(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_zoomSpeed_25(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_lastScaleFocus_26(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_doubleTapScaleTimeSecondsRemaining_27(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_doubleTapScaleStart_28(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_doubleTapScaleEnd_29(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_doubleTapPosStart_30(),
	FingersScrollViewComponentScript_t3503049574::get_offset_of_doubleTapPosEnd_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5024 = { sizeof (FingersZoomPanCameraComponentScript_t2768970116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5024[10] = 
{
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_VisibleArea_4(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_Dampening_5(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_RotationSpeed_6(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_U3CScaleGestureU3Ek__BackingField_7(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_U3CPanGestureU3Ek__BackingField_8(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_U3CTapGestureU3Ek__BackingField_9(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_U3CRotateGestureU3Ek__BackingField_10(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_cameraAnimationTargetPosition_11(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of_velocity_12(),
	FingersZoomPanCameraComponentScript_t2768970116::get_offset_of__camera_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5025 = { sizeof (U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5025[6] = 
{
	U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250::get_offset_of_U3CstartU3E__0_0(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250::get_offset_of_U3CaccumTimeU3E__1_1(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250::get_offset_of_U24this_2(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250::get_offset_of_U24current_3(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250::get_offset_of_U24disposing_4(),
	U3CAnimationCoRoutineU3Ec__Iterator0_t3218050250::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5026 = { sizeof (GestureRecognizerComponentStateUpdatedEvent_t1428864973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5027 = { sizeof (GestureRecognizerComponentScriptBase_t275484950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5027[1] = 
{
	GestureRecognizerComponentScriptBase_t275484950::get_offset_of_U3CGestureBaseU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5028 = { sizeof (GestureObjectMode_t1423348675)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5028[3] = 
{
	GestureObjectMode_t1423348675::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5029 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5029[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5030 = { sizeof (ImageGestureRecognizerComponentScript_t448644263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5030[8] = 
{
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_MaximumPathCount_13(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_DirectionTolerance_14(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_ThresholdUnits_15(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_MinimumDistanceBetweenPointsUnits_16(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_SimilarityMinimum_17(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_MinimumPointsToRecognize_18(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_GestureImages_19(),
	ImageGestureRecognizerComponentScript_t448644263::get_offset_of_U3CGestureImagesToKeyU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5031 = { sizeof (ImageGestureRecognizerComponentScriptImageEntry_t2713275279)+ sizeof (RuntimeObject), sizeof(ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5031[3] = 
{
	ImageGestureRecognizerComponentScriptImageEntry_t2713275279::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageGestureRecognizerComponentScriptImageEntry_t2713275279::get_offset_of_ScorePadding_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageGestureRecognizerComponentScriptImageEntry_t2713275279::get_offset_of_Images_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5032 = { sizeof (LongPressGestureRecognizerComponentScript_t3685016334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5032[2] = 
{
	LongPressGestureRecognizerComponentScript_t3685016334::get_offset_of_MinimumDurationSeconds_13(),
	LongPressGestureRecognizerComponentScript_t3685016334::get_offset_of_ThresholdUnits_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5033 = { sizeof (OneTouchRotateGestureRecognizerComponentScript_t2094839816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5033[2] = 
{
	OneTouchRotateGestureRecognizerComponentScript_t2094839816::get_offset_of_AngleThreshold_13(),
	OneTouchRotateGestureRecognizerComponentScript_t2094839816::get_offset_of_ThresholdUnits_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5034 = { sizeof (OneTouchScaleGestureRecognizerComponentScript_t3319081723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5034[2] = 
{
	OneTouchScaleGestureRecognizerComponentScript_t3319081723::get_offset_of_ZoomSpeed_13(),
	OneTouchScaleGestureRecognizerComponentScript_t3319081723::get_offset_of_ThresholdUnits_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5035 = { sizeof (PanGestureRecognizerComponentScript_t1777692607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5035[1] = 
{
	PanGestureRecognizerComponentScript_t1777692607::get_offset_of_ThresholdUnits_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5036 = { sizeof (RotateGestureRecognizerComponentScript_t638097468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5036[2] = 
{
	RotateGestureRecognizerComponentScript_t638097468::get_offset_of_AngleThreshold_13(),
	RotateGestureRecognizerComponentScript_t638097468::get_offset_of_ThresholdUnits_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5037 = { sizeof (ScaleGestureRecognizerComponentScript_t2181606707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5037[2] = 
{
	ScaleGestureRecognizerComponentScript_t2181606707::get_offset_of_ZoomSpeed_13(),
	ScaleGestureRecognizerComponentScript_t2181606707::get_offset_of_ThresholdUnits_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5038 = { sizeof (SwipeGestureRecognizerComponentScript_t1446920306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5038[6] = 
{
	SwipeGestureRecognizerComponentScript_t1446920306::get_offset_of_Direction_13(),
	SwipeGestureRecognizerComponentScript_t1446920306::get_offset_of_MinimumDistanceUnits_14(),
	SwipeGestureRecognizerComponentScript_t1446920306::get_offset_of_MinimumSpeedUnits_15(),
	SwipeGestureRecognizerComponentScript_t1446920306::get_offset_of_DirectionThreshold_16(),
	SwipeGestureRecognizerComponentScript_t1446920306::get_offset_of_EndMode_17(),
	SwipeGestureRecognizerComponentScript_t1446920306::get_offset_of_FailOnDirectionChange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5039 = { sizeof (TapGestureRecognizerComponentScript_t536059327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5039[4] = 
{
	TapGestureRecognizerComponentScript_t536059327::get_offset_of_NumberOfTapsRequired_13(),
	TapGestureRecognizerComponentScript_t536059327::get_offset_of_ThresholdSeconds_14(),
	TapGestureRecognizerComponentScript_t536059327::get_offset_of_ThresholdUnits_15(),
	TapGestureRecognizerComponentScript_t536059327::get_offset_of_SendBeginState_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5040 = { sizeof (FingersDPadItem_t4100703432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5040[7] = 
{
	FingersDPadItem_t4100703432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5041 = { sizeof (FingersDPadScript_t3801874975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5041[13] = 
{
	FingersDPadScript_t3801874975::get_offset_of_DPadBackgroundImage_4(),
	FingersDPadScript_t3801874975::get_offset_of_DPadUpImageSelected_5(),
	FingersDPadScript_t3801874975::get_offset_of_DPadRightImageSelected_6(),
	FingersDPadScript_t3801874975::get_offset_of_DPadDownImageSelected_7(),
	FingersDPadScript_t3801874975::get_offset_of_DPadLeftImageSelected_8(),
	FingersDPadScript_t3801874975::get_offset_of_DPadCenterImageSelected_9(),
	FingersDPadScript_t3801874975::get_offset_of_TouchRadiusInUnits_10(),
	FingersDPadScript_t3801874975::get_offset_of_overlap_11(),
	FingersDPadScript_t3801874975::get_offset_of_DPadItemTapped_12(),
	FingersDPadScript_t3801874975::get_offset_of_DPadItemPanned_13(),
	FingersDPadScript_t3801874975::get_offset_of_U3CPanGestureU3Ek__BackingField_14(),
	FingersDPadScript_t3801874975::get_offset_of_U3CTapGestureU3Ek__BackingField_15(),
	FingersDPadScript_t3801874975::get_offset_of_U3CMoveDPadToGestureStartLocationU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5042 = { sizeof (FingersImageAutomationScript_t4118243268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5042[9] = 
{
	FingersImageAutomationScript_t4118243268::get_offset_of_Image_4(),
	FingersImageAutomationScript_t4118243268::get_offset_of_ImageGesture_5(),
	FingersImageAutomationScript_t4118243268::get_offset_of_LineMaterial_6(),
	FingersImageAutomationScript_t4118243268::get_offset_of_MatchLabel_7(),
	FingersImageAutomationScript_t4118243268::get_offset_of_ScriptText_8(),
	FingersImageAutomationScript_t4118243268::get_offset_of_U3CLastImageU3Ek__BackingField_9(),
	FingersImageAutomationScript_t4118243268::get_offset_of_RecognizableImages_10(),
	FingersImageAutomationScript_t4118243268::get_offset_of_lineSet_11(),
	FingersImageAutomationScript_t4118243268::get_offset_of_currentPointList_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5043 = { sizeof (FingersJoystickScript_t2468414040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5043[8] = 
{
	FingersJoystickScript_t2468414040::get_offset_of_JoystickImage_4(),
	FingersJoystickScript_t2468414040::get_offset_of_JoystickPower_5(),
	FingersJoystickScript_t2468414040::get_offset_of_MaxExtentPercent_6(),
	FingersJoystickScript_t2468414040::get_offset_of_EightAxisMode_7(),
	FingersJoystickScript_t2468414040::get_offset_of_startCenter_8(),
	FingersJoystickScript_t2468414040::get_offset_of_U3CPanGestureU3Ek__BackingField_9(),
	FingersJoystickScript_t2468414040::get_offset_of_JoystickExecuted_10(),
	FingersJoystickScript_t2468414040::get_offset_of_U3CMoveJoystickToGestureStartLocationU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5044 = { sizeof (FingersScript_t1857011421), -1, sizeof(FingersScript_t1857011421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5044[41] = 
{
	FingersScript_t1857011421::get_offset_of_TreatMousePointerAsFinger_4(),
	FingersScript_t1857011421::get_offset_of_SimulateMouseWithTouches_5(),
	FingersScript_t1857011421::get_offset_of_RequireControlKeyForMouseZoom_6(),
	FingersScript_t1857011421::get_offset_of_MouseDistanceInUnitsForScaleAndRotate_7(),
	FingersScript_t1857011421::get_offset_of_MouseWheelDeltaMultiplier_8(),
	FingersScript_t1857011421::get_offset_of_PassThroughObjects_9(),
	FingersScript_t1857011421::get_offset_of_ShowTouches_10(),
	FingersScript_t1857011421::get_offset_of_TouchCircles_11(),
	FingersScript_t1857011421::get_offset_of_origTouchCircles_12(),
	FingersScript_t1857011421::get_offset_of_DefaultDPI_13(),
	FingersScript_t1857011421::get_offset_of_LevelUnloadOption_14(),
	0,
	0,
	0,
	FingersScript_t1857011421::get_offset_of_gestures_18(),
	FingersScript_t1857011421::get_offset_of_gesturesTemp_19(),
	FingersScript_t1857011421::get_offset_of_touchesBegan_20(),
	FingersScript_t1857011421::get_offset_of_touchesMoved_21(),
	FingersScript_t1857011421::get_offset_of_touchesEnded_22(),
	FingersScript_t1857011421::get_offset_of_gameObjectsForTouch_23(),
	FingersScript_t1857011421::get_offset_of_captureRaycastResults_24(),
	FingersScript_t1857011421::get_offset_of_filteredTouches_25(),
	FingersScript_t1857011421::get_offset_of_touches_26(),
	FingersScript_t1857011421::get_offset_of_availableShowTouches_27(),
	FingersScript_t1857011421::get_offset_of_shownTouches_28(),
	FingersScript_t1857011421::get_offset_of_shownTouchesToRemove_29(),
	FingersScript_t1857011421::get_offset_of_previousTouchPositions_30(),
	FingersScript_t1857011421::get_offset_of_components_31(),
	FingersScript_t1857011421::get_offset_of_componentTypesToDenyPassThrough_32(),
	FingersScript_t1857011421::get_offset_of_componentTypesToIgnorePassThrough_33(),
	FingersScript_t1857011421::get_offset_of_hackResults_34(),
	FingersScript_t1857011421::get_offset_of_previousTouches_35(),
	FingersScript_t1857011421::get_offset_of_currentTouches_36(),
	FingersScript_t1857011421::get_offset_of_tempTouches_37(),
	FingersScript_t1857011421::get_offset_of_rotateAngle_38(),
	FingersScript_t1857011421::get_offset_of_pinchScale_39(),
	FingersScript_t1857011421::get_offset_of_rotatePinch1_40(),
	FingersScript_t1857011421::get_offset_of_rotatePinch2_41(),
	FingersScript_t1857011421::get_offset_of_lastMouseWheelTime_42(),
	FingersScript_t1857011421_StaticFields::get_offset_of_singleton_43(),
	FingersScript_t1857011421::get_offset_of_CaptureGestureHandler_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5045 = { sizeof (GestureLevelUnloadOption_t11098870)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5045[4] = 
{
	GestureLevelUnloadOption_t11098870::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5046 = { sizeof (CaptureResult_t3073219507)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5046[5] = 
{
	CaptureResult_t3073219507::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5047 = { sizeof (ShownTouch_t1740607014)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5047[2] = 
{
	ShownTouch_t1740607014::get_offset_of_U3CGameObjectU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShownTouch_t1740607014::get_offset_of_U3CTimestampU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5048 = { sizeof (U3CMainThreadCallbackU3Ec__Iterator0_t2673350864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5048[6] = 
{
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_action_0(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U3CtimerU3E__1_1(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_delay_2(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U24current_3(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U24disposing_4(),
	U3CMainThreadCallbackU3Ec__Iterator0_t2673350864::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5049 = { sizeof (U3CPopulateGameObjectsForTouchU3Ec__AnonStorey1_t2702075252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5049[1] = 
{
	U3CPopulateGameObjectsForTouchU3Ec__AnonStorey1_t2702075252::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5050 = { sizeof (DeviceInfo_t3428345325), -1, sizeof(DeviceInfo_t3428345325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5050[3] = 
{
	DeviceInfo_t3428345325_StaticFields::get_offset_of_pixelsPerInch_0(),
	DeviceInfo_t3428345325_StaticFields::get_offset_of_unitMultiplier_1(),
	DeviceInfo_t3428345325_StaticFields::get_offset_of_oneOverUnitMultiplier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5051 = { sizeof (GestureRecognizerState_t650110565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5051[7] = 
{
	GestureRecognizerState_t650110565::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5052 = { sizeof (TouchPhase_t3693698977)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5052[7] = 
{
	TouchPhase_t3693698977::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5053 = { sizeof (GestureTouch_t1992402133)+ sizeof (RuntimeObject), sizeof(GestureTouch_t1992402133_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5053[11] = 
{
	0,
	GestureTouch_t1992402133::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_x_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_y_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_previousX_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_previousY_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_pressure_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_screenX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_screenY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_platformSpecificTouch_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GestureTouch_t1992402133::get_offset_of_touchPhase_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5054 = { sizeof (GestureRecognizerStateUpdatedDelegate_t837748355), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5055 = { sizeof (GestureRecognizerUpdated_t601711085), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5056 = { sizeof (GestureVelocityTracker_t806949657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5056[7] = 
{
	0,
	GestureVelocityTracker_t806949657::get_offset_of_history_1(),
	GestureVelocityTracker_t806949657::get_offset_of_timer_2(),
	GestureVelocityTracker_t806949657::get_offset_of_previousX_3(),
	GestureVelocityTracker_t806949657::get_offset_of_previousY_4(),
	GestureVelocityTracker_t806949657::get_offset_of_U3CVelocityXU3Ek__BackingField_5(),
	GestureVelocityTracker_t806949657::get_offset_of_U3CVelocityYU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5057 = { sizeof (VelocityHistory_t1265565051)+ sizeof (RuntimeObject), sizeof(VelocityHistory_t1265565051 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5057[3] = 
{
	VelocityHistory_t1265565051::get_offset_of_VelocityX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VelocityHistory_t1265565051::get_offset_of_VelocityY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VelocityHistory_t1265565051::get_offset_of_Seconds_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5058 = { sizeof (GestureRecognizer_t3684029681), -1, sizeof(GestureRecognizer_t3684029681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5058[37] = 
{
	GestureRecognizer_t3684029681_StaticFields::get_offset_of_allGesturesReference_0(),
	GestureRecognizer_t3684029681::get_offset_of_state_1(),
	GestureRecognizer_t3684029681::get_offset_of_currentTrackedTouches_2(),
	GestureRecognizer_t3684029681::get_offset_of_currentTrackedTouchesReadOnly_3(),
	GestureRecognizer_t3684029681::get_offset_of_requireGestureRecognizersToFail_4(),
	GestureRecognizer_t3684029681::get_offset_of_failGestures_5(),
	GestureRecognizer_t3684029681::get_offset_of_simultaneousGestures_6(),
	GestureRecognizer_t3684029681::get_offset_of_velocityTracker_7(),
	GestureRecognizer_t3684029681::get_offset_of_touchStartLocations_8(),
	GestureRecognizer_t3684029681::get_offset_of_ignoreTouchIds_9(),
	GestureRecognizer_t3684029681::get_offset_of_tempTouches_10(),
	GestureRecognizer_t3684029681::get_offset_of_minimumNumberOfTouchesToTrack_11(),
	GestureRecognizer_t3684029681::get_offset_of_maximumNumberOfTouchesToTrack_12(),
	GestureRecognizer_t3684029681::get_offset_of_justFailed_13(),
	GestureRecognizer_t3684029681::get_offset_of_justEnded_14(),
	GestureRecognizer_t3684029681::get_offset_of_isRestarting_15(),
	GestureRecognizer_t3684029681::get_offset_of_lastTrackTouchCount_16(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPrevFocusXU3Ek__BackingField_17(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPrevFocusYU3Ek__BackingField_18(),
	GestureRecognizer_t3684029681_StaticFields::get_offset_of_ActiveGestures_19(),
	GestureRecognizer_t3684029681::get_offset_of_Updated_20(),
	GestureRecognizer_t3684029681::get_offset_of_StateUpdated_21(),
	GestureRecognizer_t3684029681::get_offset_of_U3CFocusXU3Ek__BackingField_22(),
	GestureRecognizer_t3684029681::get_offset_of_U3CFocusYU3Ek__BackingField_23(),
	GestureRecognizer_t3684029681::get_offset_of_U3CStartFocusXU3Ek__BackingField_24(),
	GestureRecognizer_t3684029681::get_offset_of_U3CStartFocusYU3Ek__BackingField_25(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDeltaXU3Ek__BackingField_26(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDeltaYU3Ek__BackingField_27(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDistanceXU3Ek__BackingField_28(),
	GestureRecognizer_t3684029681::get_offset_of_U3CDistanceYU3Ek__BackingField_29(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPressureU3Ek__BackingField_30(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPlatformSpecificViewU3Ek__BackingField_31(),
	GestureRecognizer_t3684029681::get_offset_of_U3CPlatformSpecificViewScaleU3Ek__BackingField_32(),
	GestureRecognizer_t3684029681::get_offset_of_U3CClearTrackedTouchesOnEndOrFailU3Ek__BackingField_33(),
	GestureRecognizer_t3684029681::get_offset_of_U3CAllowSimultaneousExecutionIfPlatformSpecificViewsAreDifferentU3Ek__BackingField_34(),
	GestureRecognizer_t3684029681::get_offset_of_U3CReceivedAdditionalTouchesU3Ek__BackingField_35(),
	GestureRecognizer_t3684029681_StaticFields::get_offset_of_MainThreadCallback_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5059 = { sizeof (CallbackMainThreadDelegate_t469493312), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5060 = { sizeof (GestureLogger_t3995736004), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5061 = { sizeof (ImageGestureImage_t2885596271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5061[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ImageGestureImage_t2885596271::get_offset_of_U3CWidthU3Ek__BackingField_8(),
	ImageGestureImage_t2885596271::get_offset_of_U3CHeightU3Ek__BackingField_9(),
	ImageGestureImage_t2885596271::get_offset_of_U3CSizeU3Ek__BackingField_10(),
	ImageGestureImage_t2885596271::get_offset_of_U3CRowsU3Ek__BackingField_11(),
	ImageGestureImage_t2885596271::get_offset_of_U3CPixelsU3Ek__BackingField_12(),
	ImageGestureImage_t2885596271::get_offset_of_U3CSimilarityPaddingU3Ek__BackingField_13(),
	ImageGestureImage_t2885596271::get_offset_of_U3CScoreU3Ek__BackingField_14(),
	ImageGestureImage_t2885596271::get_offset_of_U3CNameU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5062 = { sizeof (ImageGestureRecognizer_t4233185475), -1, sizeof(ImageGestureRecognizer_t4233185475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5062[24] = 
{
	ImageGestureRecognizer_t4233185475_StaticFields::get_offset_of_RowBitMasks_37(),
	ImageGestureRecognizer_t4233185475_StaticFields::get_offset_of_RowBitmask_38(),
	0,
	0,
	0,
	0,
	ImageGestureRecognizer_t4233185475::get_offset_of_points_43(),
	ImageGestureRecognizer_t4233185475::get_offset_of_currentList_44(),
	ImageGestureRecognizer_t4233185475::get_offset_of_minX_45(),
	ImageGestureRecognizer_t4233185475::get_offset_of_minY_46(),
	ImageGestureRecognizer_t4233185475::get_offset_of_maxX_47(),
	ImageGestureRecognizer_t4233185475::get_offset_of_maxY_48(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMaximumPathCountU3Ek__BackingField_49(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CDirectionToleranceU3Ek__BackingField_50(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_51(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMinimumDistanceBetweenPointsUnitsU3Ek__BackingField_52(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CSimilarityMinimumU3Ek__BackingField_53(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMinimumPointsToRecognizeU3Ek__BackingField_54(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CGestureImagesU3Ek__BackingField_55(),
	ImageGestureRecognizer_t4233185475::get_offset_of_MaximumPathCountExceeded_56(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CImageU3Ek__BackingField_57(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMatchedGestureImageU3Ek__BackingField_58(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CMatchedGestureCalculationTimeMillisecondsU3Ek__BackingField_59(),
	ImageGestureRecognizer_t4233185475::get_offset_of_U3CPathCountU3Ek__BackingField_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5063 = { sizeof (Point_t2411124144)+ sizeof (RuntimeObject), sizeof(Point_t2411124144 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5063[2] = 
{
	Point_t2411124144::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Point_t2411124144::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5064 = { sizeof (LongPressGestureRecognizer_t3980777482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5064[3] = 
{
	LongPressGestureRecognizer_t3980777482::get_offset_of_stopWatch_37(),
	LongPressGestureRecognizer_t3980777482::get_offset_of_U3CMinimumDurationSecondsU3Ek__BackingField_38(),
	LongPressGestureRecognizer_t3980777482::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5065 = { sizeof (OneTouchRotateGestureRecognizer_t3272893959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5065[2] = 
{
	OneTouchRotateGestureRecognizer_t3272893959::get_offset_of_AnglePointOverrideX_45(),
	OneTouchRotateGestureRecognizer_t3272893959::get_offset_of_AnglePointOverrideY_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5066 = { sizeof (OneTouchScaleGestureRecognizer_t1313669683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5066[5] = 
{
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CScaleMultiplierU3Ek__BackingField_37(),
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CScaleMultiplierXU3Ek__BackingField_38(),
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CScaleMultiplierYU3Ek__BackingField_39(),
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CZoomSpeedU3Ek__BackingField_40(),
	OneTouchScaleGestureRecognizer_t1313669683::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5067 = { sizeof (PanGestureRecognizer_t195762396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5067[1] = 
{
	PanGestureRecognizer_t195762396::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5068 = { sizeof (RotateGestureRecognizer_t4100246528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5068[8] = 
{
	0,
	RotateGestureRecognizer_t4100246528::get_offset_of_startAngle_38(),
	RotateGestureRecognizer_t4100246528::get_offset_of_previousAngle_39(),
	RotateGestureRecognizer_t4100246528::get_offset_of_previousAngleSign_40(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CAngleThresholdU3Ek__BackingField_41(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_42(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CRotationRadiansU3Ek__BackingField_43(),
	RotateGestureRecognizer_t4100246528::get_offset_of_U3CRotationRadiansDeltaU3Ek__BackingField_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5069 = { sizeof (ScaleGestureRecognizer_t1137887245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5069[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ScaleGestureRecognizer_t1137887245::get_offset_of_previousDistanceDirection_44(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_previousDistance_45(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_previousDistanceX_46(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_previousDistanceY_47(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_timer_48(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CScaleMultiplierU3Ek__BackingField_49(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CScaleMultiplierXU3Ek__BackingField_50(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CScaleMultiplierYU3Ek__BackingField_51(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CZoomSpeedU3Ek__BackingField_52(),
	ScaleGestureRecognizer_t1137887245::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5070 = { sizeof (SwipeGestureRecognizerDirection_t3225897881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5070[6] = 
{
	SwipeGestureRecognizerDirection_t3225897881::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5071 = { sizeof (SwipeGestureRecognizerEndMode_t3668288691)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5071[4] = 
{
	SwipeGestureRecognizerEndMode_t3668288691::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5072 = { sizeof (SwipeGestureRecognizer_t2328511861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5072[7] = 
{
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CDirectionU3Ek__BackingField_37(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CMinimumDistanceUnitsU3Ek__BackingField_38(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CMinimumSpeedUnitsU3Ek__BackingField_39(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CDirectionThresholdU3Ek__BackingField_40(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CEndModeU3Ek__BackingField_41(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CFailOnDirectionChangeU3Ek__BackingField_42(),
	SwipeGestureRecognizer_t2328511861::get_offset_of_U3CEndDirectionU3Ek__BackingField_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5073 = { sizeof (TapGestureRecognizer_t3178883670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5073[8] = 
{
	TapGestureRecognizer_t3178883670::get_offset_of_tapCount_37(),
	TapGestureRecognizer_t3178883670::get_offset_of_timer_38(),
	TapGestureRecognizer_t3178883670::get_offset_of_tapTouches_39(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CNumberOfTapsRequiredU3Ek__BackingField_40(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CThresholdSecondsU3Ek__BackingField_41(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CThresholdUnitsU3Ek__BackingField_42(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CSendBeginStateU3Ek__BackingField_43(),
	TapGestureRecognizer_t3178883670::get_offset_of_U3CTapTouchesU3Ek__BackingField_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5074 = { sizeof (DataSource_t833220627)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5074[3] = 
{
	DataSource_t833220627::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5075 = { sizeof (ResponseStatus_t4073530167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5075[8] = 
{
	ResponseStatus_t4073530167::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5076 = { sizeof (UIStatus_t582920877)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5076[9] = 
{
	UIStatus_t582920877::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5077 = { sizeof (LeaderboardStart_t3259090716)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5077[3] = 
{
	LeaderboardStart_t3259090716::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5078 = { sizeof (LeaderboardTimeSpan_t1503936786)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5078[4] = 
{
	LeaderboardTimeSpan_t1503936786::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5079 = { sizeof (LeaderboardCollection_t3003544407)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5079[3] = 
{
	LeaderboardCollection_t3003544407::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5080 = { sizeof (VideoCaptureMode_t1984088482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5080[4] = 
{
	VideoCaptureMode_t1984088482::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5081 = { sizeof (VideoQualityLevel_t4283418091)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5081[6] = 
{
	VideoQualityLevel_t4283418091::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5082 = { sizeof (VideoCaptureOverlayState_t4111180056)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5082[6] = 
{
	VideoCaptureOverlayState_t4111180056::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5083 = { sizeof (Gravity_t1500868723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5083[6] = 
{
	Gravity_t1500868723::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5084 = { sizeof (CommonTypesUtil_t3521372089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5085 = { sizeof (EventVisibility_t3702936362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5085[3] = 
{
	EventVisibility_t3702936362::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5086 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5087 = { sizeof (AdvertisingResult_t1229207569)+ sizeof (RuntimeObject), sizeof(AdvertisingResult_t1229207569_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5087[2] = 
{
	AdvertisingResult_t1229207569::get_offset_of_mStatus_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AdvertisingResult_t1229207569::get_offset_of_mLocalEndpointName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5088 = { sizeof (ConnectionRequest_t684574500)+ sizeof (RuntimeObject), sizeof(ConnectionRequest_t684574500_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5088[2] = 
{
	ConnectionRequest_t684574500::get_offset_of_mRemoteEndpoint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionRequest_t684574500::get_offset_of_mPayload_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5089 = { sizeof (ConnectionResponse_t735328601)+ sizeof (RuntimeObject), sizeof(ConnectionResponse_t735328601_marshaled_pinvoke), sizeof(ConnectionResponse_t735328601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5089[5] = 
{
	ConnectionResponse_t735328601_StaticFields::get_offset_of_EmptyPayload_0(),
	ConnectionResponse_t735328601::get_offset_of_mLocalClientId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionResponse_t735328601::get_offset_of_mRemoteEndpointId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionResponse_t735328601::get_offset_of_mResponseStatus_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionResponse_t735328601::get_offset_of_mPayload_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5090 = { sizeof (Status_t465938950)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5090[7] = 
{
	Status_t465938950::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5091 = { sizeof (EndpointDetails_t3891698496)+ sizeof (RuntimeObject), sizeof(EndpointDetails_t3891698496_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5091[3] = 
{
	EndpointDetails_t3891698496::get_offset_of_mEndpointId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EndpointDetails_t3891698496::get_offset_of_mName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EndpointDetails_t3891698496::get_offset_of_mServiceId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5093 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5094 = { sizeof (InitializationStatus_t2437428114)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5094[4] = 
{
	InitializationStatus_t2437428114::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5095 = { sizeof (NearbyConnectionConfiguration_t2019425596)+ sizeof (RuntimeObject), sizeof(NearbyConnectionConfiguration_t2019425596_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5095[4] = 
{
	0,
	0,
	NearbyConnectionConfiguration_t2019425596::get_offset_of_mInitializationCallback_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NearbyConnectionConfiguration_t2019425596::get_offset_of_mLocalClientId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5096 = { sizeof (ConflictResolutionStrategy_t4255039905)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5096[7] = 
{
	ConflictResolutionStrategy_t4255039905::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5097 = { sizeof (SavedGameRequestStatus_t3745141777)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5097[6] = 
{
	SavedGameRequestStatus_t3745141777::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5098 = { sizeof (SelectUIStatus_t1293076877)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5098[7] = 
{
	SelectUIStatus_t1293076877::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5099 = { sizeof (ConflictCallback_t4045994657), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
