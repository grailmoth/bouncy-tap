﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry[]
struct SpawningPoolEntryU5BU5D_t1628840718;
// DigitalRubyShared.TapGestureRecognizer
struct TapGestureRecognizer_t3178883670;
// GameCanvas
struct GameCanvas_t3794123731;
// MusicManager
struct MusicManager_t3024629483;
// ObjectPool/PoolPrefabs[]
struct PoolPrefabsU5BU5D_t3081559410;
// PieSlice
struct PieSlice_t1066222354;
// Player
struct Player_t3266647312;
// System.Action
struct Action_t1264377477;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t2370967660;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,DigitalRuby.Pooling.IPooledObject>
struct Dictionary_2_t546511151;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String>
struct Dictionary_2_t3679516794;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct List_1_t1741830302;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Func`2<System.Boolean,System.String>
struct Func_2_t1267953766;
// System.Random
struct Random_t108471755;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// Tutorial
struct Tutorial_t2275885037;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ENUMS_T1053268879_H
#define ENUMS_T1053268879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums
struct  Enums_t1053268879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMS_T1053268879_H
#ifndef VIDEOCAPABILITIES_T1298735124_H
#define VIDEOCAPABILITIES_T1298735124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Video.VideoCapabilities
struct  VideoCapabilities_t1298735124  : public RuntimeObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsCameraSupported
	bool ___mIsCameraSupported_0;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsMicSupported
	bool ___mIsMicSupported_1;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsWriteStorageSupported
	bool ___mIsWriteStorageSupported_2;
	// System.Boolean[] GooglePlayGames.BasicApi.Video.VideoCapabilities::mCaptureModesSupported
	BooleanU5BU5D_t2897418192* ___mCaptureModesSupported_3;
	// System.Boolean[] GooglePlayGames.BasicApi.Video.VideoCapabilities::mQualityLevelsSupported
	BooleanU5BU5D_t2897418192* ___mQualityLevelsSupported_4;

public:
	inline static int32_t get_offset_of_mIsCameraSupported_0() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mIsCameraSupported_0)); }
	inline bool get_mIsCameraSupported_0() const { return ___mIsCameraSupported_0; }
	inline bool* get_address_of_mIsCameraSupported_0() { return &___mIsCameraSupported_0; }
	inline void set_mIsCameraSupported_0(bool value)
	{
		___mIsCameraSupported_0 = value;
	}

	inline static int32_t get_offset_of_mIsMicSupported_1() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mIsMicSupported_1)); }
	inline bool get_mIsMicSupported_1() const { return ___mIsMicSupported_1; }
	inline bool* get_address_of_mIsMicSupported_1() { return &___mIsMicSupported_1; }
	inline void set_mIsMicSupported_1(bool value)
	{
		___mIsMicSupported_1 = value;
	}

	inline static int32_t get_offset_of_mIsWriteStorageSupported_2() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mIsWriteStorageSupported_2)); }
	inline bool get_mIsWriteStorageSupported_2() const { return ___mIsWriteStorageSupported_2; }
	inline bool* get_address_of_mIsWriteStorageSupported_2() { return &___mIsWriteStorageSupported_2; }
	inline void set_mIsWriteStorageSupported_2(bool value)
	{
		___mIsWriteStorageSupported_2 = value;
	}

	inline static int32_t get_offset_of_mCaptureModesSupported_3() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mCaptureModesSupported_3)); }
	inline BooleanU5BU5D_t2897418192* get_mCaptureModesSupported_3() const { return ___mCaptureModesSupported_3; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mCaptureModesSupported_3() { return &___mCaptureModesSupported_3; }
	inline void set_mCaptureModesSupported_3(BooleanU5BU5D_t2897418192* value)
	{
		___mCaptureModesSupported_3 = value;
		Il2CppCodeGenWriteBarrier((&___mCaptureModesSupported_3), value);
	}

	inline static int32_t get_offset_of_mQualityLevelsSupported_4() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mQualityLevelsSupported_4)); }
	inline BooleanU5BU5D_t2897418192* get_mQualityLevelsSupported_4() const { return ___mQualityLevelsSupported_4; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mQualityLevelsSupported_4() { return &___mQualityLevelsSupported_4; }
	inline void set_mQualityLevelsSupported_4(BooleanU5BU5D_t2897418192* value)
	{
		___mQualityLevelsSupported_4 = value;
		Il2CppCodeGenWriteBarrier((&___mQualityLevelsSupported_4), value);
	}
};

struct VideoCapabilities_t1298735124_StaticFields
{
public:
	// System.Func`2<System.Boolean,System.String> GooglePlayGames.BasicApi.Video.VideoCapabilities::<>f__am$cache0
	Func_2_t1267953766 * ___U3CU3Ef__amU24cache0_5;
	// System.Func`2<System.Boolean,System.String> GooglePlayGames.BasicApi.Video.VideoCapabilities::<>f__am$cache1
	Func_2_t1267953766 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t1267953766 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t1267953766 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t1267953766 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Func_2_t1267953766 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Func_2_t1267953766 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Func_2_t1267953766 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPABILITIES_T1298735124_H
#ifndef LOGGER_T3934082555_H
#define LOGGER_T3934082555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger
struct  Logger_t3934082555  : public RuntimeObject
{
public:

public:
};

struct Logger_t3934082555_StaticFields
{
public:
	// System.Boolean GooglePlayGames.OurUtils.Logger::debugLogEnabled
	bool ___debugLogEnabled_0;
	// System.Boolean GooglePlayGames.OurUtils.Logger::warningLogEnabled
	bool ___warningLogEnabled_1;

public:
	inline static int32_t get_offset_of_debugLogEnabled_0() { return static_cast<int32_t>(offsetof(Logger_t3934082555_StaticFields, ___debugLogEnabled_0)); }
	inline bool get_debugLogEnabled_0() const { return ___debugLogEnabled_0; }
	inline bool* get_address_of_debugLogEnabled_0() { return &___debugLogEnabled_0; }
	inline void set_debugLogEnabled_0(bool value)
	{
		___debugLogEnabled_0 = value;
	}

	inline static int32_t get_offset_of_warningLogEnabled_1() { return static_cast<int32_t>(offsetof(Logger_t3934082555_StaticFields, ___warningLogEnabled_1)); }
	inline bool get_warningLogEnabled_1() const { return ___warningLogEnabled_1; }
	inline bool* get_address_of_warningLogEnabled_1() { return &___warningLogEnabled_1; }
	inline void set_warningLogEnabled_1(bool value)
	{
		___warningLogEnabled_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T3934082555_H
#ifndef U3CDU3EC__ANONSTOREY0_T2350509859_H
#define U3CDU3EC__ANONSTOREY0_T2350509859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey0
struct  U3CdU3Ec__AnonStorey0_t2350509859  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey0::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CdU3Ec__AnonStorey0_t2350509859, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((&___msg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDU3EC__ANONSTOREY0_T2350509859_H
#ifndef U3CEU3EC__ANONSTOREY2_T2346119983_H
#define U3CEU3EC__ANONSTOREY2_T2346119983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey2
struct  U3CeU3Ec__AnonStorey2_t2346119983  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey2::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CeU3Ec__AnonStorey2_t2346119983, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((&___msg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEU3EC__ANONSTOREY2_T2346119983_H
#ifndef U3CWU3EC__ANONSTOREY1_T2080961746_H
#define U3CWU3EC__ANONSTOREY1_T2080961746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey1
struct  U3CwU3Ec__AnonStorey1_t2080961746  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey1::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CwU3Ec__AnonStorey1_t2080961746, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((&___msg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWU3EC__ANONSTOREY1_T2080961746_H
#ifndef MISC_T4208016214_H
#define MISC_T4208016214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Misc
struct  Misc_t4208016214  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISC_T4208016214_H
#ifndef U3CRUNCOROUTINEU3EC__ANONSTOREY0_T3592917427_H
#define U3CRUNCOROUTINEU3EC__ANONSTOREY0_T3592917427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey0
struct  U3CRunCoroutineU3Ec__AnonStorey0_t3592917427  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey0::action
	RuntimeObject* ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CRunCoroutineU3Ec__AnonStorey0_t3592917427, ___action_0)); }
	inline RuntimeObject* get_action_0() const { return ___action_0; }
	inline RuntimeObject** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(RuntimeObject* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNCOROUTINEU3EC__ANONSTOREY0_T3592917427_H
#ifndef PLUGINVERSION_T2872281160_H
#define PLUGINVERSION_T2872281160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.PluginVersion
struct  PluginVersion_t2872281160  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINVERSION_T2872281160_H
#ifndef U3CFADEOUTU3EC__ITERATOR0_T1868787335_H
#define U3CFADEOUTU3EC__ITERATOR0_T1868787335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicManager/<FadeOut>c__Iterator0
struct  U3CFadeOutU3Ec__Iterator0_t1868787335  : public RuntimeObject
{
public:
	// System.Single MusicManager/<FadeOut>c__Iterator0::FadeTime
	float ___FadeTime_0;
	// MusicManager MusicManager/<FadeOut>c__Iterator0::$this
	MusicManager_t3024629483 * ___U24this_1;
	// System.Object MusicManager/<FadeOut>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MusicManager/<FadeOut>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MusicManager/<FadeOut>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_FadeTime_0() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t1868787335, ___FadeTime_0)); }
	inline float get_FadeTime_0() const { return ___FadeTime_0; }
	inline float* get_address_of_FadeTime_0() { return &___FadeTime_0; }
	inline void set_FadeTime_0(float value)
	{
		___FadeTime_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t1868787335, ___U24this_1)); }
	inline MusicManager_t3024629483 * get_U24this_1() const { return ___U24this_1; }
	inline MusicManager_t3024629483 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MusicManager_t3024629483 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t1868787335, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t1868787335, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t1868787335, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEOUTU3EC__ITERATOR0_T1868787335_H
#ifndef U3CWAITFORSECONDSTHENEXECUTEU3EC__ITERATOR0_T792342596_H
#define U3CWAITFORSECONDSTHENEXECUTEU3EC__ITERATOR0_T792342596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PieSlice/<WaitForSecondsThenExecute>c__Iterator0
struct  U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596  : public RuntimeObject
{
public:
	// System.Single PieSlice/<WaitForSecondsThenExecute>c__Iterator0::waitTime
	float ___waitTime_0;
	// System.Action PieSlice/<WaitForSecondsThenExecute>c__Iterator0::method
	Action_t1264377477 * ___method_1;
	// PieSlice PieSlice/<WaitForSecondsThenExecute>c__Iterator0::$this
	PieSlice_t1066222354 * ___U24this_2;
	// System.Object PieSlice/<WaitForSecondsThenExecute>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PieSlice/<WaitForSecondsThenExecute>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PieSlice/<WaitForSecondsThenExecute>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596, ___method_1)); }
	inline Action_t1264377477 * get_method_1() const { return ___method_1; }
	inline Action_t1264377477 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(Action_t1264377477 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596, ___U24this_2)); }
	inline PieSlice_t1066222354 * get_U24this_2() const { return ___U24this_2; }
	inline PieSlice_t1066222354 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PieSlice_t1066222354 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSECONDSTHENEXECUTEU3EC__ITERATOR0_T792342596_H
#ifndef U3CFLARELIGHTU3EC__ITERATOR0_T1332935211_H
#define U3CFLARELIGHTU3EC__ITERATOR0_T1332935211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/<FlareLight>c__Iterator0
struct  U3CFlareLightU3Ec__Iterator0_t1332935211  : public RuntimeObject
{
public:
	// System.Single Player/<FlareLight>c__Iterator0::<f>__1
	float ___U3CfU3E__1_0;
	// Player Player/<FlareLight>c__Iterator0::$this
	Player_t3266647312 * ___U24this_1;
	// System.Object Player/<FlareLight>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Player/<FlareLight>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Player/<FlareLight>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CfU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFlareLightU3Ec__Iterator0_t1332935211, ___U3CfU3E__1_0)); }
	inline float get_U3CfU3E__1_0() const { return ___U3CfU3E__1_0; }
	inline float* get_address_of_U3CfU3E__1_0() { return &___U3CfU3E__1_0; }
	inline void set_U3CfU3E__1_0(float value)
	{
		___U3CfU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFlareLightU3Ec__Iterator0_t1332935211, ___U24this_1)); }
	inline Player_t3266647312 * get_U24this_1() const { return ___U24this_1; }
	inline Player_t3266647312 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Player_t3266647312 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFlareLightU3Ec__Iterator0_t1332935211, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFlareLightU3Ec__Iterator0_t1332935211, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFlareLightU3Ec__Iterator0_t1332935211, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLARELIGHTU3EC__ITERATOR0_T1332935211_H
#ifndef U3CWAITFORSECONDSTHENEXECUTEU3EC__ITERATOR1_T417809513_H
#define U3CWAITFORSECONDSTHENEXECUTEU3EC__ITERATOR1_T417809513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/<WaitForSecondsThenExecute>c__Iterator1
struct  U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513  : public RuntimeObject
{
public:
	// System.Single Player/<WaitForSecondsThenExecute>c__Iterator1::waitTime
	float ___waitTime_0;
	// System.Action Player/<WaitForSecondsThenExecute>c__Iterator1::method
	Action_t1264377477 * ___method_1;
	// UnityEngine.SpriteRenderer Player/<WaitForSecondsThenExecute>c__Iterator1::<s>__0
	SpriteRenderer_t3235626157 * ___U3CsU3E__0_2;
	// Player Player/<WaitForSecondsThenExecute>c__Iterator1::$this
	Player_t3266647312 * ___U24this_3;
	// System.Object Player/<WaitForSecondsThenExecute>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Player/<WaitForSecondsThenExecute>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Player/<WaitForSecondsThenExecute>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___method_1)); }
	inline Action_t1264377477 * get_method_1() const { return ___method_1; }
	inline Action_t1264377477 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(Action_t1264377477 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}

	inline static int32_t get_offset_of_U3CsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___U3CsU3E__0_2)); }
	inline SpriteRenderer_t3235626157 * get_U3CsU3E__0_2() const { return ___U3CsU3E__0_2; }
	inline SpriteRenderer_t3235626157 ** get_address_of_U3CsU3E__0_2() { return &___U3CsU3E__0_2; }
	inline void set_U3CsU3E__0_2(SpriteRenderer_t3235626157 * value)
	{
		___U3CsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___U24this_3)); }
	inline Player_t3266647312 * get_U24this_3() const { return ___U24this_3; }
	inline Player_t3266647312 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Player_t3266647312 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSECONDSTHENEXECUTEU3EC__ITERATOR1_T417809513_H
#ifndef PLAYERDATA_T220878115_H
#define PLAYERDATA_T220878115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t220878115  : public RuntimeObject
{
public:
	// System.Int32 PlayerData::<highScore>k__BackingField
	int32_t ___U3ChighScoreU3Ek__BackingField_0;
	// System.Int32 PlayerData::<deathsSinceAd>k__BackingField
	int32_t ___U3CdeathsSinceAdU3Ek__BackingField_1;
	// System.Int32 PlayerData::<stars>k__BackingField
	int32_t ___U3CstarsU3Ek__BackingField_2;
	// System.Boolean PlayerData::<hasSeenTutorial>k__BackingField
	bool ___U3ChasSeenTutorialU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3ChighScoreU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___U3ChighScoreU3Ek__BackingField_0)); }
	inline int32_t get_U3ChighScoreU3Ek__BackingField_0() const { return ___U3ChighScoreU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ChighScoreU3Ek__BackingField_0() { return &___U3ChighScoreU3Ek__BackingField_0; }
	inline void set_U3ChighScoreU3Ek__BackingField_0(int32_t value)
	{
		___U3ChighScoreU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CdeathsSinceAdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___U3CdeathsSinceAdU3Ek__BackingField_1)); }
	inline int32_t get_U3CdeathsSinceAdU3Ek__BackingField_1() const { return ___U3CdeathsSinceAdU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CdeathsSinceAdU3Ek__BackingField_1() { return &___U3CdeathsSinceAdU3Ek__BackingField_1; }
	inline void set_U3CdeathsSinceAdU3Ek__BackingField_1(int32_t value)
	{
		___U3CdeathsSinceAdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CstarsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___U3CstarsU3Ek__BackingField_2)); }
	inline int32_t get_U3CstarsU3Ek__BackingField_2() const { return ___U3CstarsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CstarsU3Ek__BackingField_2() { return &___U3CstarsU3Ek__BackingField_2; }
	inline void set_U3CstarsU3Ek__BackingField_2(int32_t value)
	{
		___U3CstarsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3ChasSeenTutorialU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___U3ChasSeenTutorialU3Ek__BackingField_3)); }
	inline bool get_U3ChasSeenTutorialU3Ek__BackingField_3() const { return ___U3ChasSeenTutorialU3Ek__BackingField_3; }
	inline bool* get_address_of_U3ChasSeenTutorialU3Ek__BackingField_3() { return &___U3ChasSeenTutorialU3Ek__BackingField_3; }
	inline void set_U3ChasSeenTutorialU3Ek__BackingField_3(bool value)
	{
		___U3ChasSeenTutorialU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T220878115_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U24ARRAYTYPEU3D128_T4235014459_H
#define U24ARRAYTYPEU3D128_T4235014459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=128
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D128_t4235014459 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D128_t4235014459__padding[128];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D128_T4235014459_H
#ifndef U24ARRAYTYPEU3D520_T2265645983_H
#define U24ARRAYTYPEU3D520_T2265645983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=520
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D520_t2265645983 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D520_t2265645983__padding[520];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D520_T2265645983_H
#ifndef U24ARRAYTYPEU3D68_T2499083377_H
#define U24ARRAYTYPEU3D68_T2499083377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=68
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D68_t2499083377 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D68_t2499083377__padding[68];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D68_T2499083377_H
#ifndef SPAWNINGPOOLENTRY_T2116637239_H
#define SPAWNINGPOOLENTRY_T2116637239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry
struct  SpawningPoolEntry_t2116637239 
{
public:
	// System.String DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry::Key
	String_t* ___Key_0;
	// UnityEngine.GameObject DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry::Prefab
	GameObject_t1113636619 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(SpawningPoolEntry_t2116637239, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(SpawningPoolEntry_t2116637239, ___Prefab_1)); }
	inline GameObject_t1113636619 * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_t1113636619 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry
struct SpawningPoolEntry_t2116637239_marshaled_pinvoke
{
	char* ___Key_0;
	GameObject_t1113636619 * ___Prefab_1;
};
// Native definition for COM marshalling of DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry
struct SpawningPoolEntry_t2116637239_marshaled_com
{
	Il2CppChar* ___Key_0;
	GameObject_t1113636619 * ___Prefab_1;
};
#endif // SPAWNINGPOOLENTRY_T2116637239_H
#ifndef POOLPREFABS_T1376732323_H
#define POOLPREFABS_T1376732323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool/PoolPrefabs
struct  PoolPrefabs_t1376732323 
{
public:
	// System.String ObjectPool/PoolPrefabs::Key
	String_t* ___Key_0;
	// UnityEngine.GameObject ObjectPool/PoolPrefabs::Prefab
	GameObject_t1113636619 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PoolPrefabs_t1376732323, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PoolPrefabs_t1376732323, ___Prefab_1)); }
	inline GameObject_t1113636619 * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_t1113636619 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ObjectPool/PoolPrefabs
struct PoolPrefabs_t1376732323_marshaled_pinvoke
{
	char* ___Key_0;
	GameObject_t1113636619 * ___Prefab_1;
};
// Native definition for COM marshalling of ObjectPool/PoolPrefabs
struct PoolPrefabs_t1376732323_marshaled_com
{
	Il2CppChar* ___Key_0;
	GameObject_t1113636619 * ___Prefab_1;
};
#endif // POOLPREFABS_T1376732323_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef KEYVALUEPAIR_2_T1782221665_H
#define KEYVALUEPAIR_2_T1782221665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.String>
struct  KeyValuePair_2_t1782221665 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	GameObject_t1113636619 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1782221665, ___key_0)); }
	inline GameObject_t1113636619 * get_key_0() const { return ___key_0; }
	inline GameObject_t1113636619 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(GameObject_t1113636619 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1782221665, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1782221665_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255376_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255376  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=68 <PrivateImplementationDetails>::$field-F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF
	U24ArrayTypeU3D68_t2499083377  ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5DFEE31002BF167E0453CB8643D3E1D44FE3F325
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-885DDD59952941A3E98DD105FDAADBE334B79776
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-6EB03E1E021F75A722B15DD1E230C3794339E58F
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-82C0D5F32D3B4A90C255DF4203A719570A4A71C6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-DD84CB646A0ED87DE965197F182EA6E70D7710FB
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-EEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-7553F65BB6AEF17B8E9BEB262884FD704C46DC9D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8727E44B94789414DE204CBA117AD05443BDFF85
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-CD9B3692D323300F91E71C04211EA451047039D6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-F3D84C957438E86E3A03BA8266CEFF9B039760AC
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-34EDEEBC5EB29562EF18C5F3AA190B324187790C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-A6E8715255241F47AD0B3D93E1BA359D52D4F773
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-7EA0120ED2A607EE02EE175E161E3AB6678EAEF3
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-C63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-5568A7B56666FACA81071906CAD5B68BEF9CBA7D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-80837B69D4B41AE55078725B3ACCF0A27C21EA8C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-F1562B156A30ABF23C48AEEB151115973E5E0E68
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-21F3C52377932DF2C57E466D2DF531566BF21C8E
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-78B13EFFF801B0EEDCD91D16B161747601E46D7F
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-9EECA0B3D49F88F27A544B19940E1E8375C54E90
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-B1C28103C273F13F1776F8909515A1B1E42EFA96
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-408A4836C23B92A6BAD4820B377880F6CC1FA2A8
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-457EC0A425D11B4CCAB4205123D64045679F8AB3
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-12536FB84378CA4223BBD8FB3833394D625A09D3
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-2903EDF33AFE43744D8D4676E80AE98CA93318D6
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-05A61FB9C1B133245C69208185ADD4E2668799FD
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-FD39839203F719185D79D5FB10889EBD650235A8
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-E11E7676B42F5205585524A542DC08BC21B6BC74
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-0AA3902D236F027C82FC3F064812EF11C90B8873
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-3D84862348B9A730AE6F506EA7FB70C660C88A2A
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-8456C8142FC382C9C7D5CC2BA87FD135D90889B1
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-45507F8AFFC24037D807B05461DD6C77DEC72C1F
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-F14497D0866AAE006DAA1A34D21DCA4E99BE3CB4
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-183F34689D23DCB29B9F650AAE12A25144D57EA5
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-1B9653E9DAF24F1B6E8E0646A5426E8207CAC175
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-655283041A648AE2C424D8E20F6905C22E8834E8
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-23468C894E1BC8AFF095FB390877A8EAE07CAD38
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-450ADA4BD48BD2F5C266223142DBC1AFC68C533C
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-98B6C8EFF0078C310BF5655A7FF296752695FD7D
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-D238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-CFE35FBC0A074388D82D17E334381E3CB97AF384
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59;
	// <PrivateImplementationDetails>/$ArrayType=128 <PrivateImplementationDetails>::$field-64B4EE118F3DF0EC4723AC0E41203320BCC36752
	U24ArrayTypeU3D128_t4235014459  ___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60;
	// <PrivateImplementationDetails>/$ArrayType=520 <PrivateImplementationDetails>::$field-B51C0DEB10EE1C5E9888F5306B744ACD1F38D767
	U24ArrayTypeU3D520_t2265645983  ___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61;

public:
	inline static int32_t get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0)); }
	inline U24ArrayTypeU3D68_t2499083377  get_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0() const { return ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0; }
	inline U24ArrayTypeU3D68_t2499083377 * get_address_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0() { return &___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0; }
	inline void set_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0(U24ArrayTypeU3D68_t2499083377  value)
	{
		___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1() const { return ___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1() { return &___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1; }
	inline void set_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2() const { return ___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2() { return &___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2; }
	inline void set_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3() const { return ___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3() { return &___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3; }
	inline void set_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4() const { return ___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4() { return &___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4; }
	inline void set_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5() const { return ___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5() { return &___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5; }
	inline void set_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6() const { return ___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6() { return &___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6; }
	inline void set_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7() const { return ___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7() { return &___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7; }
	inline void set_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8() const { return ___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8() { return &___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8; }
	inline void set_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9() const { return ___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9() { return &___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9; }
	inline void set_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10() const { return ___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10() { return &___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10; }
	inline void set_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11() const { return ___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11() { return &___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11; }
	inline void set_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12() const { return ___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12() { return &___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12; }
	inline void set_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13() const { return ___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13() { return &___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13; }
	inline void set_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14() const { return ___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14() { return &___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14; }
	inline void set_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15() const { return ___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15() { return &___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15; }
	inline void set_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16() const { return ___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16() { return &___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16; }
	inline void set_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17() const { return ___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17() { return &___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17; }
	inline void set_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18() const { return ___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18() { return &___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18; }
	inline void set_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19() const { return ___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19() { return &___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19; }
	inline void set_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20() const { return ___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20() { return &___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20; }
	inline void set_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21() const { return ___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21() { return &___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21; }
	inline void set_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22() const { return ___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22() { return &___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22; }
	inline void set_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23() const { return ___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23() { return &___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23; }
	inline void set_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24() const { return ___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24() { return &___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24; }
	inline void set_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25() const { return ___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25() { return &___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25; }
	inline void set_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26() const { return ___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26() { return &___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26; }
	inline void set_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27() const { return ___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27() { return &___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27; }
	inline void set_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28() const { return ___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28() { return &___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28; }
	inline void set_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29() const { return ___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29() { return &___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29; }
	inline void set_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30() const { return ___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30() { return &___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30; }
	inline void set_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31() const { return ___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31() { return &___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31; }
	inline void set_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32() const { return ___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32() { return &___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32; }
	inline void set_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33() const { return ___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33() { return &___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33; }
	inline void set_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34() const { return ___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34() { return &___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34; }
	inline void set_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35() const { return ___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35() { return &___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35; }
	inline void set_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36() const { return ___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36() { return &___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36; }
	inline void set_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37() const { return ___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37() { return &___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37; }
	inline void set_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38() const { return ___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38() { return &___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38; }
	inline void set_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39() const { return ___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39() { return &___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39; }
	inline void set_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40() const { return ___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40() { return &___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40; }
	inline void set_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41() const { return ___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41() { return &___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41; }
	inline void set_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42() const { return ___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42() { return &___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42; }
	inline void set_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43() const { return ___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43() { return &___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43; }
	inline void set_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44() const { return ___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44() { return &___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44; }
	inline void set_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45() const { return ___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45() { return &___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45; }
	inline void set_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46() const { return ___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46() { return &___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46; }
	inline void set_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47() const { return ___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47() { return &___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47; }
	inline void set_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48() const { return ___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48() { return &___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48; }
	inline void set_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49() const { return ___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49() { return &___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49; }
	inline void set_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50() const { return ___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50() { return &___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50; }
	inline void set_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51() const { return ___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51() { return &___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51; }
	inline void set_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52() const { return ___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52() { return &___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52; }
	inline void set_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53() const { return ___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53() { return &___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53; }
	inline void set_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54() const { return ___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54() { return &___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54; }
	inline void set_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55() const { return ___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55() { return &___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55; }
	inline void set_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56() const { return ___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56() { return &___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56; }
	inline void set_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57() const { return ___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57() { return &___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57; }
	inline void set_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58() const { return ___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58() { return &___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58; }
	inline void set_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59() const { return ___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59() { return &___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59; }
	inline void set_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60)); }
	inline U24ArrayTypeU3D128_t4235014459  get_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60() const { return ___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60; }
	inline U24ArrayTypeU3D128_t4235014459 * get_address_of_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60() { return &___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60; }
	inline void set_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60(U24ArrayTypeU3D128_t4235014459  value)
	{
		___U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields, ___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61)); }
	inline U24ArrayTypeU3D520_t2265645983  get_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61() const { return ___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61; }
	inline U24ArrayTypeU3D520_t2265645983 * get_address_of_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61() { return &___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61; }
	inline void set_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61(U24ArrayTypeU3D520_t2265645983  value)
	{
		___U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255376_H
#ifndef GAMECOLOR_T1347690756_H
#define GAMECOLOR_T1347690756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/gameColor
struct  gameColor_t1347690756 
{
public:
	// System.Int32 Enums/gameColor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(gameColor_t1347690756, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECOLOR_T1347690756_H
#ifndef VIDEOCAPTUREMODE_T1984088482_H
#define VIDEOCAPTUREMODE_T1984088482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoCaptureMode
struct  VideoCaptureMode_t1984088482 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoCaptureMode_t1984088482, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTUREMODE_T1984088482_H
#ifndef VIDEOQUALITYLEVEL_T4283418091_H
#define VIDEOQUALITYLEVEL_T4283418091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoQualityLevel
struct  VideoQualityLevel_t4283418091 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoQualityLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoQualityLevel_t4283418091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOQUALITYLEVEL_T4283418091_H
#ifndef ENUMERATOR_T1338732273_H
#define ENUMERATOR_T1338732273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,System.String>
struct  Enumerator_t1338732273 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3679516794 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1782221665  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1338732273, ___dictionary_0)); }
	inline Dictionary_2_t3679516794 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3679516794 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3679516794 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_t1338732273, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t1338732273, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1338732273, ___current_3)); }
	inline KeyValuePair_2_t1782221665  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1782221665 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1782221665  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_t1338732273, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1338732273_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_19)); }
	inline TimeSpan_t881159249  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t881159249 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t881159249  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t881159249  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t881159249  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t881159249  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t881159249  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SPAWNINGPOOL_T4119068062_H
#define SPAWNINGPOOL_T4119068062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Pooling.SpawningPool
struct  SpawningPool_t4119068062  : public RuntimeObject
{
public:

public:
};

struct SpawningPool_t4119068062_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> DigitalRuby.Pooling.SpawningPool::prefabs
	Dictionary_2_t898892918 * ___prefabs_0;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String> DigitalRuby.Pooling.SpawningPool::activeGameObjectsAndKeys
	Dictionary_2_t3679516794 * ___activeGameObjectsAndKeys_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>> DigitalRuby.Pooling.SpawningPool::cachedKeysAndGameObjects
	Dictionary_2_t2370967660 * ___cachedKeysAndGameObjects_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>> DigitalRuby.Pooling.SpawningPool::activeKeysAndGameObjects
	Dictionary_2_t2370967660 * ___activeKeysAndGameObjects_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,DigitalRuby.Pooling.IPooledObject> DigitalRuby.Pooling.SpawningPool::interfaces
	Dictionary_2_t546511151 * ___interfaces_4;
	// System.Int32 DigitalRuby.Pooling.SpawningPool::cacheCount
	int32_t ___cacheCount_5;
	// UnityEngine.HideFlags DigitalRuby.Pooling.SpawningPool::DefaultHideFlags
	int32_t ___DefaultHideFlags_6;

public:
	inline static int32_t get_offset_of_prefabs_0() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___prefabs_0)); }
	inline Dictionary_2_t898892918 * get_prefabs_0() const { return ___prefabs_0; }
	inline Dictionary_2_t898892918 ** get_address_of_prefabs_0() { return &___prefabs_0; }
	inline void set_prefabs_0(Dictionary_2_t898892918 * value)
	{
		___prefabs_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabs_0), value);
	}

	inline static int32_t get_offset_of_activeGameObjectsAndKeys_1() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___activeGameObjectsAndKeys_1)); }
	inline Dictionary_2_t3679516794 * get_activeGameObjectsAndKeys_1() const { return ___activeGameObjectsAndKeys_1; }
	inline Dictionary_2_t3679516794 ** get_address_of_activeGameObjectsAndKeys_1() { return &___activeGameObjectsAndKeys_1; }
	inline void set_activeGameObjectsAndKeys_1(Dictionary_2_t3679516794 * value)
	{
		___activeGameObjectsAndKeys_1 = value;
		Il2CppCodeGenWriteBarrier((&___activeGameObjectsAndKeys_1), value);
	}

	inline static int32_t get_offset_of_cachedKeysAndGameObjects_2() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___cachedKeysAndGameObjects_2)); }
	inline Dictionary_2_t2370967660 * get_cachedKeysAndGameObjects_2() const { return ___cachedKeysAndGameObjects_2; }
	inline Dictionary_2_t2370967660 ** get_address_of_cachedKeysAndGameObjects_2() { return &___cachedKeysAndGameObjects_2; }
	inline void set_cachedKeysAndGameObjects_2(Dictionary_2_t2370967660 * value)
	{
		___cachedKeysAndGameObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedKeysAndGameObjects_2), value);
	}

	inline static int32_t get_offset_of_activeKeysAndGameObjects_3() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___activeKeysAndGameObjects_3)); }
	inline Dictionary_2_t2370967660 * get_activeKeysAndGameObjects_3() const { return ___activeKeysAndGameObjects_3; }
	inline Dictionary_2_t2370967660 ** get_address_of_activeKeysAndGameObjects_3() { return &___activeKeysAndGameObjects_3; }
	inline void set_activeKeysAndGameObjects_3(Dictionary_2_t2370967660 * value)
	{
		___activeKeysAndGameObjects_3 = value;
		Il2CppCodeGenWriteBarrier((&___activeKeysAndGameObjects_3), value);
	}

	inline static int32_t get_offset_of_interfaces_4() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___interfaces_4)); }
	inline Dictionary_2_t546511151 * get_interfaces_4() const { return ___interfaces_4; }
	inline Dictionary_2_t546511151 ** get_address_of_interfaces_4() { return &___interfaces_4; }
	inline void set_interfaces_4(Dictionary_2_t546511151 * value)
	{
		___interfaces_4 = value;
		Il2CppCodeGenWriteBarrier((&___interfaces_4), value);
	}

	inline static int32_t get_offset_of_cacheCount_5() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___cacheCount_5)); }
	inline int32_t get_cacheCount_5() const { return ___cacheCount_5; }
	inline int32_t* get_address_of_cacheCount_5() { return &___cacheCount_5; }
	inline void set_cacheCount_5(int32_t value)
	{
		___cacheCount_5 = value;
	}

	inline static int32_t get_offset_of_DefaultHideFlags_6() { return static_cast<int32_t>(offsetof(SpawningPool_t4119068062_StaticFields, ___DefaultHideFlags_6)); }
	inline int32_t get_DefaultHideFlags_6() const { return ___DefaultHideFlags_6; }
	inline int32_t* get_address_of_DefaultHideFlags_6() { return &___DefaultHideFlags_6; }
	inline void set_DefaultHideFlags_6(int32_t value)
	{
		___DefaultHideFlags_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNINGPOOL_T4119068062_H
#ifndef U3CENUMERATEACTIVEOBJECTSU3EC__ITERATOR0_T265290795_H
#define U3CENUMERATEACTIVEOBJECTSU3EC__ITERATOR0_T265290795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Pooling.SpawningPool/<EnumerateActiveObjects>c__Iterator0
struct  U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,System.String> DigitalRuby.Pooling.SpawningPool/<EnumerateActiveObjects>c__Iterator0::$locvar0
	Enumerator_t1338732273  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.String> DigitalRuby.Pooling.SpawningPool/<EnumerateActiveObjects>c__Iterator0::<v>__1
	KeyValuePair_2_t1782221665  ___U3CvU3E__1_1;
	// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.String> DigitalRuby.Pooling.SpawningPool/<EnumerateActiveObjects>c__Iterator0::$current
	KeyValuePair_2_t1782221665  ___U24current_2;
	// System.Boolean DigitalRuby.Pooling.SpawningPool/<EnumerateActiveObjects>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 DigitalRuby.Pooling.SpawningPool/<EnumerateActiveObjects>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795, ___U24locvar0_0)); }
	inline Enumerator_t1338732273  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t1338732273 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t1338732273  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CvU3E__1_1() { return static_cast<int32_t>(offsetof(U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795, ___U3CvU3E__1_1)); }
	inline KeyValuePair_2_t1782221665  get_U3CvU3E__1_1() const { return ___U3CvU3E__1_1; }
	inline KeyValuePair_2_t1782221665 * get_address_of_U3CvU3E__1_1() { return &___U3CvU3E__1_1; }
	inline void set_U3CvU3E__1_1(KeyValuePair_2_t1782221665  value)
	{
		___U3CvU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795, ___U24current_2)); }
	inline KeyValuePair_2_t1782221665  get_U24current_2() const { return ___U24current_2; }
	inline KeyValuePair_2_t1782221665 * get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(KeyValuePair_2_t1782221665  value)
	{
		___U24current_2 = value;
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEACTIVEOBJECTSU3EC__ITERATOR0_T265290795_H
#ifndef VIDEOCAPTURESTATE_T2350658599_H
#define VIDEOCAPTURESTATE_T2350658599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Video.VideoCaptureState
struct  VideoCaptureState_t2350658599  : public RuntimeObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsCapturing
	bool ___mIsCapturing_0;
	// GooglePlayGames.BasicApi.VideoCaptureMode GooglePlayGames.BasicApi.Video.VideoCaptureState::mCaptureMode
	int32_t ___mCaptureMode_1;
	// GooglePlayGames.BasicApi.VideoQualityLevel GooglePlayGames.BasicApi.Video.VideoCaptureState::mQualityLevel
	int32_t ___mQualityLevel_2;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsOverlayVisible
	bool ___mIsOverlayVisible_3;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsPaused
	bool ___mIsPaused_4;

public:
	inline static int32_t get_offset_of_mIsCapturing_0() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mIsCapturing_0)); }
	inline bool get_mIsCapturing_0() const { return ___mIsCapturing_0; }
	inline bool* get_address_of_mIsCapturing_0() { return &___mIsCapturing_0; }
	inline void set_mIsCapturing_0(bool value)
	{
		___mIsCapturing_0 = value;
	}

	inline static int32_t get_offset_of_mCaptureMode_1() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mCaptureMode_1)); }
	inline int32_t get_mCaptureMode_1() const { return ___mCaptureMode_1; }
	inline int32_t* get_address_of_mCaptureMode_1() { return &___mCaptureMode_1; }
	inline void set_mCaptureMode_1(int32_t value)
	{
		___mCaptureMode_1 = value;
	}

	inline static int32_t get_offset_of_mQualityLevel_2() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mQualityLevel_2)); }
	inline int32_t get_mQualityLevel_2() const { return ___mQualityLevel_2; }
	inline int32_t* get_address_of_mQualityLevel_2() { return &___mQualityLevel_2; }
	inline void set_mQualityLevel_2(int32_t value)
	{
		___mQualityLevel_2 = value;
	}

	inline static int32_t get_offset_of_mIsOverlayVisible_3() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mIsOverlayVisible_3)); }
	inline bool get_mIsOverlayVisible_3() const { return ___mIsOverlayVisible_3; }
	inline bool* get_address_of_mIsOverlayVisible_3() { return &___mIsOverlayVisible_3; }
	inline void set_mIsOverlayVisible_3(bool value)
	{
		___mIsOverlayVisible_3 = value;
	}

	inline static int32_t get_offset_of_mIsPaused_4() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mIsPaused_4)); }
	inline bool get_mIsPaused_4() const { return ___mIsPaused_4; }
	inline bool* get_address_of_mIsPaused_4() { return &___mIsPaused_4; }
	inline void set_mIsPaused_4(bool value)
	{
		___mIsPaused_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTURESTATE_T2350658599_H
#ifndef NULLABLE_1_T2603721331_H
#define NULLABLE_1_T2603721331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t2603721331 
{
public:
	// T System.Nullable`1::value
	TimeSpan_t881159249  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2603721331, ___value_0)); }
	inline TimeSpan_t881159249  get_value_0() const { return ___value_0; }
	inline TimeSpan_t881159249 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_t881159249  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2603721331, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2603721331_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SAVEDGAMEMETADATAUPDATE_T1775293339_H
#define SAVEDGAMEMETADATAUPDATE_T1775293339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct  SavedGameMetadataUpdate_t1775293339 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mDescriptionUpdated
	bool ___mDescriptionUpdated_0;
	// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewDescription
	String_t* ___mNewDescription_1;
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mCoverImageUpdated
	bool ___mCoverImageUpdated_2;
	// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewPngCoverImage
	ByteU5BU5D_t4116647657* ___mNewPngCoverImage_3;
	// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewPlayedTime
	Nullable_1_t2603721331  ___mNewPlayedTime_4;

public:
	inline static int32_t get_offset_of_mDescriptionUpdated_0() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mDescriptionUpdated_0)); }
	inline bool get_mDescriptionUpdated_0() const { return ___mDescriptionUpdated_0; }
	inline bool* get_address_of_mDescriptionUpdated_0() { return &___mDescriptionUpdated_0; }
	inline void set_mDescriptionUpdated_0(bool value)
	{
		___mDescriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of_mNewDescription_1() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mNewDescription_1)); }
	inline String_t* get_mNewDescription_1() const { return ___mNewDescription_1; }
	inline String_t** get_address_of_mNewDescription_1() { return &___mNewDescription_1; }
	inline void set_mNewDescription_1(String_t* value)
	{
		___mNewDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNewDescription_1), value);
	}

	inline static int32_t get_offset_of_mCoverImageUpdated_2() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mCoverImageUpdated_2)); }
	inline bool get_mCoverImageUpdated_2() const { return ___mCoverImageUpdated_2; }
	inline bool* get_address_of_mCoverImageUpdated_2() { return &___mCoverImageUpdated_2; }
	inline void set_mCoverImageUpdated_2(bool value)
	{
		___mCoverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mNewPngCoverImage_3() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mNewPngCoverImage_3)); }
	inline ByteU5BU5D_t4116647657* get_mNewPngCoverImage_3() const { return ___mNewPngCoverImage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_mNewPngCoverImage_3() { return &___mNewPngCoverImage_3; }
	inline void set_mNewPngCoverImage_3(ByteU5BU5D_t4116647657* value)
	{
		___mNewPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNewPngCoverImage_3), value);
	}

	inline static int32_t get_offset_of_mNewPlayedTime_4() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mNewPlayedTime_4)); }
	inline Nullable_1_t2603721331  get_mNewPlayedTime_4() const { return ___mNewPlayedTime_4; }
	inline Nullable_1_t2603721331 * get_address_of_mNewPlayedTime_4() { return &___mNewPlayedTime_4; }
	inline void set_mNewPlayedTime_4(Nullable_1_t2603721331  value)
	{
		___mNewPlayedTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_t1775293339_marshaled_pinvoke
{
	int32_t ___mDescriptionUpdated_0;
	char* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_t1775293339_marshaled_com
{
	int32_t ___mDescriptionUpdated_0;
	Il2CppChar* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
#endif // SAVEDGAMEMETADATAUPDATE_T1775293339_H
#ifndef BUILDER_T140438593_H
#define BUILDER_T140438593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct  Builder_t140438593 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mDescriptionUpdated
	bool ___mDescriptionUpdated_0;
	// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mNewDescription
	String_t* ___mNewDescription_1;
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mCoverImageUpdated
	bool ___mCoverImageUpdated_2;
	// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mNewPngCoverImage
	ByteU5BU5D_t4116647657* ___mNewPngCoverImage_3;
	// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mNewPlayedTime
	Nullable_1_t2603721331  ___mNewPlayedTime_4;

public:
	inline static int32_t get_offset_of_mDescriptionUpdated_0() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mDescriptionUpdated_0)); }
	inline bool get_mDescriptionUpdated_0() const { return ___mDescriptionUpdated_0; }
	inline bool* get_address_of_mDescriptionUpdated_0() { return &___mDescriptionUpdated_0; }
	inline void set_mDescriptionUpdated_0(bool value)
	{
		___mDescriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of_mNewDescription_1() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mNewDescription_1)); }
	inline String_t* get_mNewDescription_1() const { return ___mNewDescription_1; }
	inline String_t** get_address_of_mNewDescription_1() { return &___mNewDescription_1; }
	inline void set_mNewDescription_1(String_t* value)
	{
		___mNewDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNewDescription_1), value);
	}

	inline static int32_t get_offset_of_mCoverImageUpdated_2() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mCoverImageUpdated_2)); }
	inline bool get_mCoverImageUpdated_2() const { return ___mCoverImageUpdated_2; }
	inline bool* get_address_of_mCoverImageUpdated_2() { return &___mCoverImageUpdated_2; }
	inline void set_mCoverImageUpdated_2(bool value)
	{
		___mCoverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mNewPngCoverImage_3() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mNewPngCoverImage_3)); }
	inline ByteU5BU5D_t4116647657* get_mNewPngCoverImage_3() const { return ___mNewPngCoverImage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_mNewPngCoverImage_3() { return &___mNewPngCoverImage_3; }
	inline void set_mNewPngCoverImage_3(ByteU5BU5D_t4116647657* value)
	{
		___mNewPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNewPngCoverImage_3), value);
	}

	inline static int32_t get_offset_of_mNewPlayedTime_4() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mNewPlayedTime_4)); }
	inline Nullable_1_t2603721331  get_mNewPlayedTime_4() const { return ___mNewPlayedTime_4; }
	inline Nullable_1_t2603721331 * get_address_of_mNewPlayedTime_4() { return &___mNewPlayedTime_4; }
	inline void set_mNewPlayedTime_4(Nullable_1_t2603721331  value)
	{
		___mNewPlayedTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t140438593_marshaled_pinvoke
{
	int32_t ___mDescriptionUpdated_0;
	char* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t140438593_marshaled_com
{
	int32_t ___mDescriptionUpdated_0;
	Il2CppChar* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
#endif // BUILDER_T140438593_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BUTTONUTILS_T4203118290_H
#define BUTTONUTILS_T4203118290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonUtils
struct  ButtonUtils_t4203118290  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONUTILS_T4203118290_H
#ifndef COLORCHANGER_T3353685796_H
#define COLORCHANGER_T3353685796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChanger
struct  ColorChanger_t3353685796  : public MonoBehaviour_t3962482529
{
public:
	// Enums/gameColor ColorChanger::myColor
	int32_t ___myColor_4;

public:
	inline static int32_t get_offset_of_myColor_4() { return static_cast<int32_t>(offsetof(ColorChanger_t3353685796, ___myColor_4)); }
	inline int32_t get_myColor_4() const { return ___myColor_4; }
	inline int32_t* get_address_of_myColor_4() { return &___myColor_4; }
	inline void set_myColor_4(int32_t value)
	{
		___myColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGER_T3353685796_H
#ifndef POOLEDOBJECTSCRIPT_T3251621882_H
#define POOLEDOBJECTSCRIPT_T3251621882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Pooling.PooledObjectScript
struct  PooledObjectScript_t3251621882  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct PooledObjectScript_t3251621882_StaticFields
{
public:
	// System.Int32 DigitalRuby.Pooling.PooledObjectScript::instantiatedCount
	int32_t ___instantiatedCount_4;
	// System.Int32 DigitalRuby.Pooling.PooledObjectScript::spawnCount
	int32_t ___spawnCount_5;
	// System.Int32 DigitalRuby.Pooling.PooledObjectScript::returnToPoolCount
	int32_t ___returnToPoolCount_6;

public:
	inline static int32_t get_offset_of_instantiatedCount_4() { return static_cast<int32_t>(offsetof(PooledObjectScript_t3251621882_StaticFields, ___instantiatedCount_4)); }
	inline int32_t get_instantiatedCount_4() const { return ___instantiatedCount_4; }
	inline int32_t* get_address_of_instantiatedCount_4() { return &___instantiatedCount_4; }
	inline void set_instantiatedCount_4(int32_t value)
	{
		___instantiatedCount_4 = value;
	}

	inline static int32_t get_offset_of_spawnCount_5() { return static_cast<int32_t>(offsetof(PooledObjectScript_t3251621882_StaticFields, ___spawnCount_5)); }
	inline int32_t get_spawnCount_5() const { return ___spawnCount_5; }
	inline int32_t* get_address_of_spawnCount_5() { return &___spawnCount_5; }
	inline void set_spawnCount_5(int32_t value)
	{
		___spawnCount_5 = value;
	}

	inline static int32_t get_offset_of_returnToPoolCount_6() { return static_cast<int32_t>(offsetof(PooledObjectScript_t3251621882_StaticFields, ___returnToPoolCount_6)); }
	inline int32_t get_returnToPoolCount_6() const { return ___returnToPoolCount_6; }
	inline int32_t* get_address_of_returnToPoolCount_6() { return &___returnToPoolCount_6; }
	inline void set_returnToPoolCount_6(int32_t value)
	{
		___returnToPoolCount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEDOBJECTSCRIPT_T3251621882_H
#ifndef SPAWNINGPOOLDEMOSCRIPT_T4019697319_H
#define SPAWNINGPOOLDEMOSCRIPT_T4019697319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Pooling.SpawningPoolDemoScript
struct  SpawningPoolDemoScript_t4019697319  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Dropdown DigitalRuby.Pooling.SpawningPoolDemoScript::Options
	Dropdown_t2274391225 * ___Options_4;
	// UnityEngine.UI.Button DigitalRuby.Pooling.SpawningPoolDemoScript::SpawnButton
	Button_t4055032469 * ___SpawnButton_5;
	// UnityEngine.UI.Button DigitalRuby.Pooling.SpawningPoolDemoScript::RegularButton
	Button_t4055032469 * ___RegularButton_6;
	// UnityEngine.UI.Text DigitalRuby.Pooling.SpawningPoolDemoScript::Results
	Text_t1901882714 * ___Results_7;
	// UnityEngine.GameObject[] DigitalRuby.Pooling.SpawningPoolDemoScript::Prefabs
	GameObjectU5BU5D_t3328599146* ___Prefabs_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DigitalRuby.Pooling.SpawningPoolDemoScript::regular
	List_1_t2585711361 * ___regular_9;

public:
	inline static int32_t get_offset_of_Options_4() { return static_cast<int32_t>(offsetof(SpawningPoolDemoScript_t4019697319, ___Options_4)); }
	inline Dropdown_t2274391225 * get_Options_4() const { return ___Options_4; }
	inline Dropdown_t2274391225 ** get_address_of_Options_4() { return &___Options_4; }
	inline void set_Options_4(Dropdown_t2274391225 * value)
	{
		___Options_4 = value;
		Il2CppCodeGenWriteBarrier((&___Options_4), value);
	}

	inline static int32_t get_offset_of_SpawnButton_5() { return static_cast<int32_t>(offsetof(SpawningPoolDemoScript_t4019697319, ___SpawnButton_5)); }
	inline Button_t4055032469 * get_SpawnButton_5() const { return ___SpawnButton_5; }
	inline Button_t4055032469 ** get_address_of_SpawnButton_5() { return &___SpawnButton_5; }
	inline void set_SpawnButton_5(Button_t4055032469 * value)
	{
		___SpawnButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnButton_5), value);
	}

	inline static int32_t get_offset_of_RegularButton_6() { return static_cast<int32_t>(offsetof(SpawningPoolDemoScript_t4019697319, ___RegularButton_6)); }
	inline Button_t4055032469 * get_RegularButton_6() const { return ___RegularButton_6; }
	inline Button_t4055032469 ** get_address_of_RegularButton_6() { return &___RegularButton_6; }
	inline void set_RegularButton_6(Button_t4055032469 * value)
	{
		___RegularButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___RegularButton_6), value);
	}

	inline static int32_t get_offset_of_Results_7() { return static_cast<int32_t>(offsetof(SpawningPoolDemoScript_t4019697319, ___Results_7)); }
	inline Text_t1901882714 * get_Results_7() const { return ___Results_7; }
	inline Text_t1901882714 ** get_address_of_Results_7() { return &___Results_7; }
	inline void set_Results_7(Text_t1901882714 * value)
	{
		___Results_7 = value;
		Il2CppCodeGenWriteBarrier((&___Results_7), value);
	}

	inline static int32_t get_offset_of_Prefabs_8() { return static_cast<int32_t>(offsetof(SpawningPoolDemoScript_t4019697319, ___Prefabs_8)); }
	inline GameObjectU5BU5D_t3328599146* get_Prefabs_8() const { return ___Prefabs_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Prefabs_8() { return &___Prefabs_8; }
	inline void set_Prefabs_8(GameObjectU5BU5D_t3328599146* value)
	{
		___Prefabs_8 = value;
		Il2CppCodeGenWriteBarrier((&___Prefabs_8), value);
	}

	inline static int32_t get_offset_of_regular_9() { return static_cast<int32_t>(offsetof(SpawningPoolDemoScript_t4019697319, ___regular_9)); }
	inline List_1_t2585711361 * get_regular_9() const { return ___regular_9; }
	inline List_1_t2585711361 ** get_address_of_regular_9() { return &___regular_9; }
	inline void set_regular_9(List_1_t2585711361 * value)
	{
		___regular_9 = value;
		Il2CppCodeGenWriteBarrier((&___regular_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNINGPOOLDEMOSCRIPT_T4019697319_H
#ifndef SPAWNINGPOOLSCRIPT_T3963462420_H
#define SPAWNINGPOOLSCRIPT_T3963462420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.Pooling.SpawningPoolScript
struct  SpawningPoolScript_t3963462420  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DigitalRuby.Pooling.SpawningPoolScript::awake
	bool ___awake_5;
	// DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry[] DigitalRuby.Pooling.SpawningPoolScript::Prefabs
	SpawningPoolEntryU5BU5D_t1628840718* ___Prefabs_6;
	// System.Boolean DigitalRuby.Pooling.SpawningPoolScript::ReturnToCacheOnLevelLoad
	bool ___ReturnToCacheOnLevelLoad_7;

public:
	inline static int32_t get_offset_of_awake_5() { return static_cast<int32_t>(offsetof(SpawningPoolScript_t3963462420, ___awake_5)); }
	inline bool get_awake_5() const { return ___awake_5; }
	inline bool* get_address_of_awake_5() { return &___awake_5; }
	inline void set_awake_5(bool value)
	{
		___awake_5 = value;
	}

	inline static int32_t get_offset_of_Prefabs_6() { return static_cast<int32_t>(offsetof(SpawningPoolScript_t3963462420, ___Prefabs_6)); }
	inline SpawningPoolEntryU5BU5D_t1628840718* get_Prefabs_6() const { return ___Prefabs_6; }
	inline SpawningPoolEntryU5BU5D_t1628840718** get_address_of_Prefabs_6() { return &___Prefabs_6; }
	inline void set_Prefabs_6(SpawningPoolEntryU5BU5D_t1628840718* value)
	{
		___Prefabs_6 = value;
		Il2CppCodeGenWriteBarrier((&___Prefabs_6), value);
	}

	inline static int32_t get_offset_of_ReturnToCacheOnLevelLoad_7() { return static_cast<int32_t>(offsetof(SpawningPoolScript_t3963462420, ___ReturnToCacheOnLevelLoad_7)); }
	inline bool get_ReturnToCacheOnLevelLoad_7() const { return ___ReturnToCacheOnLevelLoad_7; }
	inline bool* get_address_of_ReturnToCacheOnLevelLoad_7() { return &___ReturnToCacheOnLevelLoad_7; }
	inline void set_ReturnToCacheOnLevelLoad_7(bool value)
	{
		___ReturnToCacheOnLevelLoad_7 = value;
	}
};

struct SpawningPoolScript_t3963462420_StaticFields
{
public:
	// DigitalRuby.Pooling.SpawningPoolScript DigitalRuby.Pooling.SpawningPoolScript::instance
	SpawningPoolScript_t3963462420 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(SpawningPoolScript_t3963462420_StaticFields, ___instance_4)); }
	inline SpawningPoolScript_t3963462420 * get_instance_4() const { return ___instance_4; }
	inline SpawningPoolScript_t3963462420 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SpawningPoolScript_t3963462420 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNINGPOOLSCRIPT_T3963462420_H
#ifndef GAMECANVAS_T3794123731_H
#define GAMECANVAS_T3794123731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCanvas
struct  GameCanvas_t3794123731  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameCanvas::scoreTexts
	List_1_t2585711361 * ___scoreTexts_4;
	// UnityEngine.GameObject GameCanvas::highScoreText
	GameObject_t1113636619 * ___highScoreText_5;
	// UnityEngine.GameObject GameCanvas::finalScorePanel
	GameObject_t1113636619 * ___finalScorePanel_6;

public:
	inline static int32_t get_offset_of_scoreTexts_4() { return static_cast<int32_t>(offsetof(GameCanvas_t3794123731, ___scoreTexts_4)); }
	inline List_1_t2585711361 * get_scoreTexts_4() const { return ___scoreTexts_4; }
	inline List_1_t2585711361 ** get_address_of_scoreTexts_4() { return &___scoreTexts_4; }
	inline void set_scoreTexts_4(List_1_t2585711361 * value)
	{
		___scoreTexts_4 = value;
		Il2CppCodeGenWriteBarrier((&___scoreTexts_4), value);
	}

	inline static int32_t get_offset_of_highScoreText_5() { return static_cast<int32_t>(offsetof(GameCanvas_t3794123731, ___highScoreText_5)); }
	inline GameObject_t1113636619 * get_highScoreText_5() const { return ___highScoreText_5; }
	inline GameObject_t1113636619 ** get_address_of_highScoreText_5() { return &___highScoreText_5; }
	inline void set_highScoreText_5(GameObject_t1113636619 * value)
	{
		___highScoreText_5 = value;
		Il2CppCodeGenWriteBarrier((&___highScoreText_5), value);
	}

	inline static int32_t get_offset_of_finalScorePanel_6() { return static_cast<int32_t>(offsetof(GameCanvas_t3794123731, ___finalScorePanel_6)); }
	inline GameObject_t1113636619 * get_finalScorePanel_6() const { return ___finalScorePanel_6; }
	inline GameObject_t1113636619 ** get_address_of_finalScorePanel_6() { return &___finalScorePanel_6; }
	inline void set_finalScorePanel_6(GameObject_t1113636619 * value)
	{
		___finalScorePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___finalScorePanel_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECANVAS_T3794123731_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GameManager::paused
	bool ___paused_6;
	// Player GameManager::player
	Player_t3266647312 * ___player_7;
	// System.Single GameManager::PLAYER_GRAVITY_SCALE
	float ___PLAYER_GRAVITY_SCALE_8;
	// System.Single GameManager::TILE_WIDTH
	float ___TILE_WIDTH_9;
	// UnityEngine.GameObject GameManager::gameCameraObj
	GameObject_t1113636619 * ___gameCameraObj_10;
	// UnityEngine.Camera GameManager::gameCamera
	Camera_t4157153871 * ___gameCamera_11;
	// UnityEngine.Vector3 GameManager::_lowerLeftCornerWP
	Vector3_t3722313464  ____lowerLeftCornerWP_12;
	// UnityEngine.Vector3 GameManager::_upperRightCornerWP
	Vector3_t3722313464  ____upperRightCornerWP_13;
	// UnityEngine.Vector3 GameManager::_lowerRightCornerWP
	Vector3_t3722313464  ____lowerRightCornerWP_14;
	// UnityEngine.Vector3 GameManager::_upperLefCornerWP
	Vector3_t3722313464  ____upperLefCornerWP_15;
	// System.Single GameManager::spaceBetweenObstacles
	float ___spaceBetweenObstacles_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::floor
	List_1_t2585711361 * ___floor_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::ceiling
	List_1_t2585711361 * ___ceiling_18;
	// UnityEngine.GameObject[] GameManager::tilePrefabs
	GameObjectU5BU5D_t3328599146* ___tilePrefabs_19;
	// System.String[] GameManager::obstacleKeys
	StringU5BU5D_t1281789340* ___obstacleKeys_20;
	// UnityEngine.GameObject[] GameManager::obstaclePrefabs
	GameObjectU5BU5D_t3328599146* ___obstaclePrefabs_21;
	// UnityEngine.GameObject[] GameManager::pieSlicePrefabs
	GameObjectU5BU5D_t3328599146* ___pieSlicePrefabs_22;
	// UnityEngine.GameObject GameManager::currentPieSlicePrefab
	GameObject_t1113636619 * ___currentPieSlicePrefab_23;
	// System.Int32 GameManager::currentPieColorCount
	int32_t ___currentPieColorCount_24;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::upcomingPieSlices
	List_1_t2585711361 * ___upcomingPieSlices_25;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::upcomingObstacles
	List_1_t2585711361 * ___upcomingObstacles_26;
	// System.Single GameManager::gameSpeed
	float ___gameSpeed_27;
	// UnityEngine.Color32 GameManager::Red
	Color32_t2600501292  ___Red_28;
	// UnityEngine.Color32 GameManager::Yellow
	Color32_t2600501292  ___Yellow_29;
	// UnityEngine.Color32 GameManager::Blue
	Color32_t2600501292  ___Blue_30;
	// UnityEngine.Color32 GameManager::ErrorPink
	Color32_t2600501292  ___ErrorPink_31;
	// System.Single GameManager::objPos2
	float ___objPos2_33;
	// System.Single GameManager::objPos3
	float ___objPos3_34;
	// System.Single GameManager::objPos4
	float ___objPos4_35;
	// System.Single GameManager::objPos1
	float ___objPos1_36;
	// System.Single GameManager::objPos5
	float ___objPos5_37;
	// UnityEngine.GameObject[] GameManager::scoreTexts
	GameObjectU5BU5D_t3328599146* ___scoreTexts_38;
	// System.Int32 GameManager::currentScore
	int32_t ___currentScore_39;
	// UnityEngine.GameObject GameManager::finalScorePanel
	GameObject_t1113636619 * ___finalScorePanel_40;
	// UnityEngine.GameObject GameManager::highScoreText
	GameObject_t1113636619 * ___highScoreText_41;
	// GameCanvas GameManager::currentCanvas
	GameCanvas_t3794123731 * ___currentCanvas_43;

public:
	inline static int32_t get_offset_of_paused_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___paused_6)); }
	inline bool get_paused_6() const { return ___paused_6; }
	inline bool* get_address_of_paused_6() { return &___paused_6; }
	inline void set_paused_6(bool value)
	{
		___paused_6 = value;
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___player_7)); }
	inline Player_t3266647312 * get_player_7() const { return ___player_7; }
	inline Player_t3266647312 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Player_t3266647312 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((&___player_7), value);
	}

	inline static int32_t get_offset_of_PLAYER_GRAVITY_SCALE_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___PLAYER_GRAVITY_SCALE_8)); }
	inline float get_PLAYER_GRAVITY_SCALE_8() const { return ___PLAYER_GRAVITY_SCALE_8; }
	inline float* get_address_of_PLAYER_GRAVITY_SCALE_8() { return &___PLAYER_GRAVITY_SCALE_8; }
	inline void set_PLAYER_GRAVITY_SCALE_8(float value)
	{
		___PLAYER_GRAVITY_SCALE_8 = value;
	}

	inline static int32_t get_offset_of_TILE_WIDTH_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___TILE_WIDTH_9)); }
	inline float get_TILE_WIDTH_9() const { return ___TILE_WIDTH_9; }
	inline float* get_address_of_TILE_WIDTH_9() { return &___TILE_WIDTH_9; }
	inline void set_TILE_WIDTH_9(float value)
	{
		___TILE_WIDTH_9 = value;
	}

	inline static int32_t get_offset_of_gameCameraObj_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___gameCameraObj_10)); }
	inline GameObject_t1113636619 * get_gameCameraObj_10() const { return ___gameCameraObj_10; }
	inline GameObject_t1113636619 ** get_address_of_gameCameraObj_10() { return &___gameCameraObj_10; }
	inline void set_gameCameraObj_10(GameObject_t1113636619 * value)
	{
		___gameCameraObj_10 = value;
		Il2CppCodeGenWriteBarrier((&___gameCameraObj_10), value);
	}

	inline static int32_t get_offset_of_gameCamera_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___gameCamera_11)); }
	inline Camera_t4157153871 * get_gameCamera_11() const { return ___gameCamera_11; }
	inline Camera_t4157153871 ** get_address_of_gameCamera_11() { return &___gameCamera_11; }
	inline void set_gameCamera_11(Camera_t4157153871 * value)
	{
		___gameCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___gameCamera_11), value);
	}

	inline static int32_t get_offset_of__lowerLeftCornerWP_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____lowerLeftCornerWP_12)); }
	inline Vector3_t3722313464  get__lowerLeftCornerWP_12() const { return ____lowerLeftCornerWP_12; }
	inline Vector3_t3722313464 * get_address_of__lowerLeftCornerWP_12() { return &____lowerLeftCornerWP_12; }
	inline void set__lowerLeftCornerWP_12(Vector3_t3722313464  value)
	{
		____lowerLeftCornerWP_12 = value;
	}

	inline static int32_t get_offset_of__upperRightCornerWP_13() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____upperRightCornerWP_13)); }
	inline Vector3_t3722313464  get__upperRightCornerWP_13() const { return ____upperRightCornerWP_13; }
	inline Vector3_t3722313464 * get_address_of__upperRightCornerWP_13() { return &____upperRightCornerWP_13; }
	inline void set__upperRightCornerWP_13(Vector3_t3722313464  value)
	{
		____upperRightCornerWP_13 = value;
	}

	inline static int32_t get_offset_of__lowerRightCornerWP_14() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____lowerRightCornerWP_14)); }
	inline Vector3_t3722313464  get__lowerRightCornerWP_14() const { return ____lowerRightCornerWP_14; }
	inline Vector3_t3722313464 * get_address_of__lowerRightCornerWP_14() { return &____lowerRightCornerWP_14; }
	inline void set__lowerRightCornerWP_14(Vector3_t3722313464  value)
	{
		____lowerRightCornerWP_14 = value;
	}

	inline static int32_t get_offset_of__upperLefCornerWP_15() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ____upperLefCornerWP_15)); }
	inline Vector3_t3722313464  get__upperLefCornerWP_15() const { return ____upperLefCornerWP_15; }
	inline Vector3_t3722313464 * get_address_of__upperLefCornerWP_15() { return &____upperLefCornerWP_15; }
	inline void set__upperLefCornerWP_15(Vector3_t3722313464  value)
	{
		____upperLefCornerWP_15 = value;
	}

	inline static int32_t get_offset_of_spaceBetweenObstacles_16() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___spaceBetweenObstacles_16)); }
	inline float get_spaceBetweenObstacles_16() const { return ___spaceBetweenObstacles_16; }
	inline float* get_address_of_spaceBetweenObstacles_16() { return &___spaceBetweenObstacles_16; }
	inline void set_spaceBetweenObstacles_16(float value)
	{
		___spaceBetweenObstacles_16 = value;
	}

	inline static int32_t get_offset_of_floor_17() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___floor_17)); }
	inline List_1_t2585711361 * get_floor_17() const { return ___floor_17; }
	inline List_1_t2585711361 ** get_address_of_floor_17() { return &___floor_17; }
	inline void set_floor_17(List_1_t2585711361 * value)
	{
		___floor_17 = value;
		Il2CppCodeGenWriteBarrier((&___floor_17), value);
	}

	inline static int32_t get_offset_of_ceiling_18() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ceiling_18)); }
	inline List_1_t2585711361 * get_ceiling_18() const { return ___ceiling_18; }
	inline List_1_t2585711361 ** get_address_of_ceiling_18() { return &___ceiling_18; }
	inline void set_ceiling_18(List_1_t2585711361 * value)
	{
		___ceiling_18 = value;
		Il2CppCodeGenWriteBarrier((&___ceiling_18), value);
	}

	inline static int32_t get_offset_of_tilePrefabs_19() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___tilePrefabs_19)); }
	inline GameObjectU5BU5D_t3328599146* get_tilePrefabs_19() const { return ___tilePrefabs_19; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_tilePrefabs_19() { return &___tilePrefabs_19; }
	inline void set_tilePrefabs_19(GameObjectU5BU5D_t3328599146* value)
	{
		___tilePrefabs_19 = value;
		Il2CppCodeGenWriteBarrier((&___tilePrefabs_19), value);
	}

	inline static int32_t get_offset_of_obstacleKeys_20() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___obstacleKeys_20)); }
	inline StringU5BU5D_t1281789340* get_obstacleKeys_20() const { return ___obstacleKeys_20; }
	inline StringU5BU5D_t1281789340** get_address_of_obstacleKeys_20() { return &___obstacleKeys_20; }
	inline void set_obstacleKeys_20(StringU5BU5D_t1281789340* value)
	{
		___obstacleKeys_20 = value;
		Il2CppCodeGenWriteBarrier((&___obstacleKeys_20), value);
	}

	inline static int32_t get_offset_of_obstaclePrefabs_21() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___obstaclePrefabs_21)); }
	inline GameObjectU5BU5D_t3328599146* get_obstaclePrefabs_21() const { return ___obstaclePrefabs_21; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_obstaclePrefabs_21() { return &___obstaclePrefabs_21; }
	inline void set_obstaclePrefabs_21(GameObjectU5BU5D_t3328599146* value)
	{
		___obstaclePrefabs_21 = value;
		Il2CppCodeGenWriteBarrier((&___obstaclePrefabs_21), value);
	}

	inline static int32_t get_offset_of_pieSlicePrefabs_22() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___pieSlicePrefabs_22)); }
	inline GameObjectU5BU5D_t3328599146* get_pieSlicePrefabs_22() const { return ___pieSlicePrefabs_22; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_pieSlicePrefabs_22() { return &___pieSlicePrefabs_22; }
	inline void set_pieSlicePrefabs_22(GameObjectU5BU5D_t3328599146* value)
	{
		___pieSlicePrefabs_22 = value;
		Il2CppCodeGenWriteBarrier((&___pieSlicePrefabs_22), value);
	}

	inline static int32_t get_offset_of_currentPieSlicePrefab_23() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___currentPieSlicePrefab_23)); }
	inline GameObject_t1113636619 * get_currentPieSlicePrefab_23() const { return ___currentPieSlicePrefab_23; }
	inline GameObject_t1113636619 ** get_address_of_currentPieSlicePrefab_23() { return &___currentPieSlicePrefab_23; }
	inline void set_currentPieSlicePrefab_23(GameObject_t1113636619 * value)
	{
		___currentPieSlicePrefab_23 = value;
		Il2CppCodeGenWriteBarrier((&___currentPieSlicePrefab_23), value);
	}

	inline static int32_t get_offset_of_currentPieColorCount_24() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___currentPieColorCount_24)); }
	inline int32_t get_currentPieColorCount_24() const { return ___currentPieColorCount_24; }
	inline int32_t* get_address_of_currentPieColorCount_24() { return &___currentPieColorCount_24; }
	inline void set_currentPieColorCount_24(int32_t value)
	{
		___currentPieColorCount_24 = value;
	}

	inline static int32_t get_offset_of_upcomingPieSlices_25() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___upcomingPieSlices_25)); }
	inline List_1_t2585711361 * get_upcomingPieSlices_25() const { return ___upcomingPieSlices_25; }
	inline List_1_t2585711361 ** get_address_of_upcomingPieSlices_25() { return &___upcomingPieSlices_25; }
	inline void set_upcomingPieSlices_25(List_1_t2585711361 * value)
	{
		___upcomingPieSlices_25 = value;
		Il2CppCodeGenWriteBarrier((&___upcomingPieSlices_25), value);
	}

	inline static int32_t get_offset_of_upcomingObstacles_26() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___upcomingObstacles_26)); }
	inline List_1_t2585711361 * get_upcomingObstacles_26() const { return ___upcomingObstacles_26; }
	inline List_1_t2585711361 ** get_address_of_upcomingObstacles_26() { return &___upcomingObstacles_26; }
	inline void set_upcomingObstacles_26(List_1_t2585711361 * value)
	{
		___upcomingObstacles_26 = value;
		Il2CppCodeGenWriteBarrier((&___upcomingObstacles_26), value);
	}

	inline static int32_t get_offset_of_gameSpeed_27() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___gameSpeed_27)); }
	inline float get_gameSpeed_27() const { return ___gameSpeed_27; }
	inline float* get_address_of_gameSpeed_27() { return &___gameSpeed_27; }
	inline void set_gameSpeed_27(float value)
	{
		___gameSpeed_27 = value;
	}

	inline static int32_t get_offset_of_Red_28() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Red_28)); }
	inline Color32_t2600501292  get_Red_28() const { return ___Red_28; }
	inline Color32_t2600501292 * get_address_of_Red_28() { return &___Red_28; }
	inline void set_Red_28(Color32_t2600501292  value)
	{
		___Red_28 = value;
	}

	inline static int32_t get_offset_of_Yellow_29() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Yellow_29)); }
	inline Color32_t2600501292  get_Yellow_29() const { return ___Yellow_29; }
	inline Color32_t2600501292 * get_address_of_Yellow_29() { return &___Yellow_29; }
	inline void set_Yellow_29(Color32_t2600501292  value)
	{
		___Yellow_29 = value;
	}

	inline static int32_t get_offset_of_Blue_30() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___Blue_30)); }
	inline Color32_t2600501292  get_Blue_30() const { return ___Blue_30; }
	inline Color32_t2600501292 * get_address_of_Blue_30() { return &___Blue_30; }
	inline void set_Blue_30(Color32_t2600501292  value)
	{
		___Blue_30 = value;
	}

	inline static int32_t get_offset_of_ErrorPink_31() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ErrorPink_31)); }
	inline Color32_t2600501292  get_ErrorPink_31() const { return ___ErrorPink_31; }
	inline Color32_t2600501292 * get_address_of_ErrorPink_31() { return &___ErrorPink_31; }
	inline void set_ErrorPink_31(Color32_t2600501292  value)
	{
		___ErrorPink_31 = value;
	}

	inline static int32_t get_offset_of_objPos2_33() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___objPos2_33)); }
	inline float get_objPos2_33() const { return ___objPos2_33; }
	inline float* get_address_of_objPos2_33() { return &___objPos2_33; }
	inline void set_objPos2_33(float value)
	{
		___objPos2_33 = value;
	}

	inline static int32_t get_offset_of_objPos3_34() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___objPos3_34)); }
	inline float get_objPos3_34() const { return ___objPos3_34; }
	inline float* get_address_of_objPos3_34() { return &___objPos3_34; }
	inline void set_objPos3_34(float value)
	{
		___objPos3_34 = value;
	}

	inline static int32_t get_offset_of_objPos4_35() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___objPos4_35)); }
	inline float get_objPos4_35() const { return ___objPos4_35; }
	inline float* get_address_of_objPos4_35() { return &___objPos4_35; }
	inline void set_objPos4_35(float value)
	{
		___objPos4_35 = value;
	}

	inline static int32_t get_offset_of_objPos1_36() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___objPos1_36)); }
	inline float get_objPos1_36() const { return ___objPos1_36; }
	inline float* get_address_of_objPos1_36() { return &___objPos1_36; }
	inline void set_objPos1_36(float value)
	{
		___objPos1_36 = value;
	}

	inline static int32_t get_offset_of_objPos5_37() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___objPos5_37)); }
	inline float get_objPos5_37() const { return ___objPos5_37; }
	inline float* get_address_of_objPos5_37() { return &___objPos5_37; }
	inline void set_objPos5_37(float value)
	{
		___objPos5_37 = value;
	}

	inline static int32_t get_offset_of_scoreTexts_38() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___scoreTexts_38)); }
	inline GameObjectU5BU5D_t3328599146* get_scoreTexts_38() const { return ___scoreTexts_38; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_scoreTexts_38() { return &___scoreTexts_38; }
	inline void set_scoreTexts_38(GameObjectU5BU5D_t3328599146* value)
	{
		___scoreTexts_38 = value;
		Il2CppCodeGenWriteBarrier((&___scoreTexts_38), value);
	}

	inline static int32_t get_offset_of_currentScore_39() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___currentScore_39)); }
	inline int32_t get_currentScore_39() const { return ___currentScore_39; }
	inline int32_t* get_address_of_currentScore_39() { return &___currentScore_39; }
	inline void set_currentScore_39(int32_t value)
	{
		___currentScore_39 = value;
	}

	inline static int32_t get_offset_of_finalScorePanel_40() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___finalScorePanel_40)); }
	inline GameObject_t1113636619 * get_finalScorePanel_40() const { return ___finalScorePanel_40; }
	inline GameObject_t1113636619 ** get_address_of_finalScorePanel_40() { return &___finalScorePanel_40; }
	inline void set_finalScorePanel_40(GameObject_t1113636619 * value)
	{
		___finalScorePanel_40 = value;
		Il2CppCodeGenWriteBarrier((&___finalScorePanel_40), value);
	}

	inline static int32_t get_offset_of_highScoreText_41() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___highScoreText_41)); }
	inline GameObject_t1113636619 * get_highScoreText_41() const { return ___highScoreText_41; }
	inline GameObject_t1113636619 ** get_address_of_highScoreText_41() { return &___highScoreText_41; }
	inline void set_highScoreText_41(GameObject_t1113636619 * value)
	{
		___highScoreText_41 = value;
		Il2CppCodeGenWriteBarrier((&___highScoreText_41), value);
	}

	inline static int32_t get_offset_of_currentCanvas_43() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___currentCanvas_43)); }
	inline GameCanvas_t3794123731 * get_currentCanvas_43() const { return ___currentCanvas_43; }
	inline GameCanvas_t3794123731 ** get_address_of_currentCanvas_43() { return &___currentCanvas_43; }
	inline void set_currentCanvas_43(GameCanvas_t3794123731 * value)
	{
		___currentCanvas_43 = value;
		Il2CppCodeGenWriteBarrier((&___currentCanvas_43), value);
	}
};

struct GameManager_t1536523654_StaticFields
{
public:
	// GameManager GameManager::gameManager
	GameManager_t1536523654 * ___gameManager_4;
	// System.Random GameManager::getrandom
	Random_t108471755 * ___getrandom_32;

public:
	inline static int32_t get_offset_of_gameManager_4() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___gameManager_4)); }
	inline GameManager_t1536523654 * get_gameManager_4() const { return ___gameManager_4; }
	inline GameManager_t1536523654 ** get_address_of_gameManager_4() { return &___gameManager_4; }
	inline void set_gameManager_4(GameManager_t1536523654 * value)
	{
		___gameManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_4), value);
	}

	inline static int32_t get_offset_of_getrandom_32() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___getrandom_32)); }
	inline Random_t108471755 * get_getrandom_32() const { return ___getrandom_32; }
	inline Random_t108471755 ** get_address_of_getrandom_32() { return &___getrandom_32; }
	inline void set_getrandom_32(Random_t108471755 * value)
	{
		___getrandom_32 = value;
		Il2CppCodeGenWriteBarrier((&___getrandom_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef PLAYGAMESHELPEROBJECT_T4023745847_H
#define PLAYGAMESHELPEROBJECT_T4023745847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct  PlayGamesHelperObject_t4023745847  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::localQueue
	List_1_t2736452219 * ___localQueue_7;

public:
	inline static int32_t get_offset_of_localQueue_7() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847, ___localQueue_7)); }
	inline List_1_t2736452219 * get_localQueue_7() const { return ___localQueue_7; }
	inline List_1_t2736452219 ** get_address_of_localQueue_7() { return &___localQueue_7; }
	inline void set_localQueue_7(List_1_t2736452219 * value)
	{
		___localQueue_7 = value;
		Il2CppCodeGenWriteBarrier((&___localQueue_7), value);
	}
};

struct PlayGamesHelperObject_t4023745847_StaticFields
{
public:
	// GooglePlayGames.OurUtils.PlayGamesHelperObject GooglePlayGames.OurUtils.PlayGamesHelperObject::instance
	PlayGamesHelperObject_t4023745847 * ___instance_4;
	// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::sIsDummy
	bool ___sIsDummy_5;
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueue
	List_1_t2736452219 * ___sQueue_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueueEmpty
	bool ___sQueueEmpty_8;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> GooglePlayGames.OurUtils.PlayGamesHelperObject::sPauseCallbackList
	List_1_t1741830302 * ___sPauseCallbackList_9;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> GooglePlayGames.OurUtils.PlayGamesHelperObject::sFocusCallbackList
	List_1_t1741830302 * ___sFocusCallbackList_10;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___instance_4)); }
	inline PlayGamesHelperObject_t4023745847 * get_instance_4() const { return ___instance_4; }
	inline PlayGamesHelperObject_t4023745847 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayGamesHelperObject_t4023745847 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_sIsDummy_5() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sIsDummy_5)); }
	inline bool get_sIsDummy_5() const { return ___sIsDummy_5; }
	inline bool* get_address_of_sIsDummy_5() { return &___sIsDummy_5; }
	inline void set_sIsDummy_5(bool value)
	{
		___sIsDummy_5 = value;
	}

	inline static int32_t get_offset_of_sQueue_6() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sQueue_6)); }
	inline List_1_t2736452219 * get_sQueue_6() const { return ___sQueue_6; }
	inline List_1_t2736452219 ** get_address_of_sQueue_6() { return &___sQueue_6; }
	inline void set_sQueue_6(List_1_t2736452219 * value)
	{
		___sQueue_6 = value;
		Il2CppCodeGenWriteBarrier((&___sQueue_6), value);
	}

	inline static int32_t get_offset_of_sQueueEmpty_8() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sQueueEmpty_8)); }
	inline bool get_sQueueEmpty_8() const { return ___sQueueEmpty_8; }
	inline bool* get_address_of_sQueueEmpty_8() { return &___sQueueEmpty_8; }
	inline void set_sQueueEmpty_8(bool value)
	{
		___sQueueEmpty_8 = value;
	}

	inline static int32_t get_offset_of_sPauseCallbackList_9() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sPauseCallbackList_9)); }
	inline List_1_t1741830302 * get_sPauseCallbackList_9() const { return ___sPauseCallbackList_9; }
	inline List_1_t1741830302 ** get_address_of_sPauseCallbackList_9() { return &___sPauseCallbackList_9; }
	inline void set_sPauseCallbackList_9(List_1_t1741830302 * value)
	{
		___sPauseCallbackList_9 = value;
		Il2CppCodeGenWriteBarrier((&___sPauseCallbackList_9), value);
	}

	inline static int32_t get_offset_of_sFocusCallbackList_10() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sFocusCallbackList_10)); }
	inline List_1_t1741830302 * get_sFocusCallbackList_10() const { return ___sFocusCallbackList_10; }
	inline List_1_t1741830302 ** get_address_of_sFocusCallbackList_10() { return &___sFocusCallbackList_10; }
	inline void set_sFocusCallbackList_10(List_1_t1741830302 * value)
	{
		___sFocusCallbackList_10 = value;
		Il2CppCodeGenWriteBarrier((&___sFocusCallbackList_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYGAMESHELPEROBJECT_T4023745847_H
#ifndef LASERPARTICLESYSTEM_T3216800139_H
#define LASERPARTICLESYSTEM_T3216800139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaserParticleSystem
struct  LaserParticleSystem_t3216800139  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LASERPARTICLESYSTEM_T3216800139_H
#ifndef LASERSCRIPT_T3521506685_H
#define LASERSCRIPT_T3521506685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaserScript
struct  LaserScript_t3521506685  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform LaserScript::startPoint
	Transform_t3600365921 * ___startPoint_4;
	// UnityEngine.Transform LaserScript::endPoint
	Transform_t3600365921 * ___endPoint_5;
	// UnityEngine.LineRenderer LaserScript::laserLine
	LineRenderer_t3154350270 * ___laserLine_6;

public:
	inline static int32_t get_offset_of_startPoint_4() { return static_cast<int32_t>(offsetof(LaserScript_t3521506685, ___startPoint_4)); }
	inline Transform_t3600365921 * get_startPoint_4() const { return ___startPoint_4; }
	inline Transform_t3600365921 ** get_address_of_startPoint_4() { return &___startPoint_4; }
	inline void set_startPoint_4(Transform_t3600365921 * value)
	{
		___startPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___startPoint_4), value);
	}

	inline static int32_t get_offset_of_endPoint_5() { return static_cast<int32_t>(offsetof(LaserScript_t3521506685, ___endPoint_5)); }
	inline Transform_t3600365921 * get_endPoint_5() const { return ___endPoint_5; }
	inline Transform_t3600365921 ** get_address_of_endPoint_5() { return &___endPoint_5; }
	inline void set_endPoint_5(Transform_t3600365921 * value)
	{
		___endPoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___endPoint_5), value);
	}

	inline static int32_t get_offset_of_laserLine_6() { return static_cast<int32_t>(offsetof(LaserScript_t3521506685, ___laserLine_6)); }
	inline LineRenderer_t3154350270 * get_laserLine_6() const { return ___laserLine_6; }
	inline LineRenderer_t3154350270 ** get_address_of_laserLine_6() { return &___laserLine_6; }
	inline void set_laserLine_6(LineRenderer_t3154350270 * value)
	{
		___laserLine_6 = value;
		Il2CppCodeGenWriteBarrier((&___laserLine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LASERSCRIPT_T3521506685_H
#ifndef LEADERBOARD_T2917364101_H
#define LEADERBOARD_T2917364101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Leaderboard
struct  Leaderboard_t2917364101  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Leaderboard_t2917364101_StaticFields
{
public:
	// Leaderboard Leaderboard::leaderboard
	Leaderboard_t2917364101 * ___leaderboard_4;

public:
	inline static int32_t get_offset_of_leaderboard_4() { return static_cast<int32_t>(offsetof(Leaderboard_t2917364101_StaticFields, ___leaderboard_4)); }
	inline Leaderboard_t2917364101 * get_leaderboard_4() const { return ___leaderboard_4; }
	inline Leaderboard_t2917364101 ** get_address_of_leaderboard_4() { return &___leaderboard_4; }
	inline void set_leaderboard_4(Leaderboard_t2917364101 * value)
	{
		___leaderboard_4 = value;
		Il2CppCodeGenWriteBarrier((&___leaderboard_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARD_T2917364101_H
#ifndef MOVEABLE_T2836551010_H
#define MOVEABLE_T2836551010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Moveable
struct  Moveable_t2836551010  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Moveable::shouldMove
	bool ___shouldMove_4;
	// UnityEngine.Vector3 Moveable::moveVector
	Vector3_t3722313464  ___moveVector_5;

public:
	inline static int32_t get_offset_of_shouldMove_4() { return static_cast<int32_t>(offsetof(Moveable_t2836551010, ___shouldMove_4)); }
	inline bool get_shouldMove_4() const { return ___shouldMove_4; }
	inline bool* get_address_of_shouldMove_4() { return &___shouldMove_4; }
	inline void set_shouldMove_4(bool value)
	{
		___shouldMove_4 = value;
	}

	inline static int32_t get_offset_of_moveVector_5() { return static_cast<int32_t>(offsetof(Moveable_t2836551010, ___moveVector_5)); }
	inline Vector3_t3722313464  get_moveVector_5() const { return ___moveVector_5; }
	inline Vector3_t3722313464 * get_address_of_moveVector_5() { return &___moveVector_5; }
	inline void set_moveVector_5(Vector3_t3722313464  value)
	{
		___moveVector_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEABLE_T2836551010_H
#ifndef MUSICMANAGER_T3024629483_H
#define MUSICMANAGER_T3024629483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicManager
struct  MusicManager_t3024629483  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] MusicManager::levelMusicArray
	AudioClipU5BU5D_t143221404* ___levelMusicArray_5;
	// UnityEngine.AudioSource MusicManager::audioSource
	AudioSource_t3935305588 * ___audioSource_6;
	// System.Single MusicManager::startVolume
	float ___startVolume_7;
	// System.Int32 MusicManager::currentMusicSceneIndex
	int32_t ___currentMusicSceneIndex_8;

public:
	inline static int32_t get_offset_of_levelMusicArray_5() { return static_cast<int32_t>(offsetof(MusicManager_t3024629483, ___levelMusicArray_5)); }
	inline AudioClipU5BU5D_t143221404* get_levelMusicArray_5() const { return ___levelMusicArray_5; }
	inline AudioClipU5BU5D_t143221404** get_address_of_levelMusicArray_5() { return &___levelMusicArray_5; }
	inline void set_levelMusicArray_5(AudioClipU5BU5D_t143221404* value)
	{
		___levelMusicArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___levelMusicArray_5), value);
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(MusicManager_t3024629483, ___audioSource_6)); }
	inline AudioSource_t3935305588 * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t3935305588 * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_6), value);
	}

	inline static int32_t get_offset_of_startVolume_7() { return static_cast<int32_t>(offsetof(MusicManager_t3024629483, ___startVolume_7)); }
	inline float get_startVolume_7() const { return ___startVolume_7; }
	inline float* get_address_of_startVolume_7() { return &___startVolume_7; }
	inline void set_startVolume_7(float value)
	{
		___startVolume_7 = value;
	}

	inline static int32_t get_offset_of_currentMusicSceneIndex_8() { return static_cast<int32_t>(offsetof(MusicManager_t3024629483, ___currentMusicSceneIndex_8)); }
	inline int32_t get_currentMusicSceneIndex_8() const { return ___currentMusicSceneIndex_8; }
	inline int32_t* get_address_of_currentMusicSceneIndex_8() { return &___currentMusicSceneIndex_8; }
	inline void set_currentMusicSceneIndex_8(int32_t value)
	{
		___currentMusicSceneIndex_8 = value;
	}
};

struct MusicManager_t3024629483_StaticFields
{
public:
	// MusicManager MusicManager::musicManager
	MusicManager_t3024629483 * ___musicManager_4;

public:
	inline static int32_t get_offset_of_musicManager_4() { return static_cast<int32_t>(offsetof(MusicManager_t3024629483_StaticFields, ___musicManager_4)); }
	inline MusicManager_t3024629483 * get_musicManager_4() const { return ___musicManager_4; }
	inline MusicManager_t3024629483 ** get_address_of_musicManager_4() { return &___musicManager_4; }
	inline void set_musicManager_4(MusicManager_t3024629483 * value)
	{
		___musicManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___musicManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUSICMANAGER_T3024629483_H
#ifndef OBJECTPOOL_T251773294_H
#define OBJECTPOOL_T251773294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool
struct  ObjectPool_t251773294  : public MonoBehaviour_t3962482529
{
public:
	// ObjectPool/PoolPrefabs[] ObjectPool::objectPrefabs
	PoolPrefabsU5BU5D_t3081559410* ___objectPrefabs_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ObjectPool::cachedObjects
	List_1_t2585711361 * ___cachedObjects_6;

public:
	inline static int32_t get_offset_of_objectPrefabs_5() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___objectPrefabs_5)); }
	inline PoolPrefabsU5BU5D_t3081559410* get_objectPrefabs_5() const { return ___objectPrefabs_5; }
	inline PoolPrefabsU5BU5D_t3081559410** get_address_of_objectPrefabs_5() { return &___objectPrefabs_5; }
	inline void set_objectPrefabs_5(PoolPrefabsU5BU5D_t3081559410* value)
	{
		___objectPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&___objectPrefabs_5), value);
	}

	inline static int32_t get_offset_of_cachedObjects_6() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___cachedObjects_6)); }
	inline List_1_t2585711361 * get_cachedObjects_6() const { return ___cachedObjects_6; }
	inline List_1_t2585711361 ** get_address_of_cachedObjects_6() { return &___cachedObjects_6; }
	inline void set_cachedObjects_6(List_1_t2585711361 * value)
	{
		___cachedObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedObjects_6), value);
	}
};

struct ObjectPool_t251773294_StaticFields
{
public:
	// ObjectPool ObjectPool::pool
	ObjectPool_t251773294 * ___pool_4;

public:
	inline static int32_t get_offset_of_pool_4() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294_StaticFields, ___pool_4)); }
	inline ObjectPool_t251773294 * get_pool_4() const { return ___pool_4; }
	inline ObjectPool_t251773294 ** get_address_of_pool_4() { return &___pool_4; }
	inline void set_pool_4(ObjectPool_t251773294 * value)
	{
		___pool_4 = value;
		Il2CppCodeGenWriteBarrier((&___pool_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_T251773294_H
#ifndef OBSTACLE_T162511623_H
#define OBSTACLE_T162511623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Obstacle
struct  Obstacle_t162511623  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Obstacle::generateNewWhenDone
	bool ___generateNewWhenDone_4;

public:
	inline static int32_t get_offset_of_generateNewWhenDone_4() { return static_cast<int32_t>(offsetof(Obstacle_t162511623, ___generateNewWhenDone_4)); }
	inline bool get_generateNewWhenDone_4() const { return ___generateNewWhenDone_4; }
	inline bool* get_address_of_generateNewWhenDone_4() { return &___generateNewWhenDone_4; }
	inline void set_generateNewWhenDone_4(bool value)
	{
		___generateNewWhenDone_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSTACLE_T162511623_H
#ifndef PARTICLESYSTEMAUTODESTROY_T798598573_H
#define PARTICLESYSTEMAUTODESTROY_T798598573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleSystemAutoDestroy
struct  ParticleSystemAutoDestroy_t798598573  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem ParticleSystemAutoDestroy::ps
	ParticleSystem_t1800779281 * ___ps_4;

public:
	inline static int32_t get_offset_of_ps_4() { return static_cast<int32_t>(offsetof(ParticleSystemAutoDestroy_t798598573, ___ps_4)); }
	inline ParticleSystem_t1800779281 * get_ps_4() const { return ___ps_4; }
	inline ParticleSystem_t1800779281 ** get_address_of_ps_4() { return &___ps_4; }
	inline void set_ps_4(ParticleSystem_t1800779281 * value)
	{
		___ps_4 = value;
		Il2CppCodeGenWriteBarrier((&___ps_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMAUTODESTROY_T798598573_H
#ifndef PIESLICE_T1066222354_H
#define PIESLICE_T1066222354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PieSlice
struct  PieSlice_t1066222354  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PieSlice::used
	bool ___used_4;
	// UnityEngine.GameObject PieSlice::scoreText
	GameObject_t1113636619 * ___scoreText_5;
	// UnityEngine.GameObject PieSlice::myCanvas
	GameObject_t1113636619 * ___myCanvas_6;
	// UnityEngine.Animator PieSlice::textAnimator
	Animator_t434523843 * ___textAnimator_7;
	// UnityEngine.GameObject PieSlice::pieTin
	GameObject_t1113636619 * ___pieTin_8;
	// System.Boolean PieSlice::moving
	bool ___moving_9;
	// UnityEngine.AudioSource PieSlice::coinSound
	AudioSource_t3935305588 * ___coinSound_10;
	// Enums/gameColor PieSlice::_myColor
	int32_t ____myColor_11;

public:
	inline static int32_t get_offset_of_used_4() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___used_4)); }
	inline bool get_used_4() const { return ___used_4; }
	inline bool* get_address_of_used_4() { return &___used_4; }
	inline void set_used_4(bool value)
	{
		___used_4 = value;
	}

	inline static int32_t get_offset_of_scoreText_5() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___scoreText_5)); }
	inline GameObject_t1113636619 * get_scoreText_5() const { return ___scoreText_5; }
	inline GameObject_t1113636619 ** get_address_of_scoreText_5() { return &___scoreText_5; }
	inline void set_scoreText_5(GameObject_t1113636619 * value)
	{
		___scoreText_5 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_5), value);
	}

	inline static int32_t get_offset_of_myCanvas_6() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___myCanvas_6)); }
	inline GameObject_t1113636619 * get_myCanvas_6() const { return ___myCanvas_6; }
	inline GameObject_t1113636619 ** get_address_of_myCanvas_6() { return &___myCanvas_6; }
	inline void set_myCanvas_6(GameObject_t1113636619 * value)
	{
		___myCanvas_6 = value;
		Il2CppCodeGenWriteBarrier((&___myCanvas_6), value);
	}

	inline static int32_t get_offset_of_textAnimator_7() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___textAnimator_7)); }
	inline Animator_t434523843 * get_textAnimator_7() const { return ___textAnimator_7; }
	inline Animator_t434523843 ** get_address_of_textAnimator_7() { return &___textAnimator_7; }
	inline void set_textAnimator_7(Animator_t434523843 * value)
	{
		___textAnimator_7 = value;
		Il2CppCodeGenWriteBarrier((&___textAnimator_7), value);
	}

	inline static int32_t get_offset_of_pieTin_8() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___pieTin_8)); }
	inline GameObject_t1113636619 * get_pieTin_8() const { return ___pieTin_8; }
	inline GameObject_t1113636619 ** get_address_of_pieTin_8() { return &___pieTin_8; }
	inline void set_pieTin_8(GameObject_t1113636619 * value)
	{
		___pieTin_8 = value;
		Il2CppCodeGenWriteBarrier((&___pieTin_8), value);
	}

	inline static int32_t get_offset_of_moving_9() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___moving_9)); }
	inline bool get_moving_9() const { return ___moving_9; }
	inline bool* get_address_of_moving_9() { return &___moving_9; }
	inline void set_moving_9(bool value)
	{
		___moving_9 = value;
	}

	inline static int32_t get_offset_of_coinSound_10() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ___coinSound_10)); }
	inline AudioSource_t3935305588 * get_coinSound_10() const { return ___coinSound_10; }
	inline AudioSource_t3935305588 ** get_address_of_coinSound_10() { return &___coinSound_10; }
	inline void set_coinSound_10(AudioSource_t3935305588 * value)
	{
		___coinSound_10 = value;
		Il2CppCodeGenWriteBarrier((&___coinSound_10), value);
	}

	inline static int32_t get_offset_of__myColor_11() { return static_cast<int32_t>(offsetof(PieSlice_t1066222354, ____myColor_11)); }
	inline int32_t get__myColor_11() const { return ____myColor_11; }
	inline int32_t* get_address_of__myColor_11() { return &____myColor_11; }
	inline void set__myColor_11(int32_t value)
	{
		____myColor_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIESLICE_T1066222354_H
#ifndef PIETIN_T3174492024_H
#define PIETIN_T3174492024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PieTin
struct  PieTin_t3174492024  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PieTin::scoreTextPrefab
	GameObject_t1113636619 * ___scoreTextPrefab_4;
	// UnityEngine.GameObject PieTin::myCanvas
	GameObject_t1113636619 * ___myCanvas_5;
	// UnityEngine.AudioSource PieTin::audioSource
	AudioSource_t3935305588 * ___audioSource_6;
	// UnityEngine.AudioClip[] PieTin::sounds
	AudioClipU5BU5D_t143221404* ___sounds_7;

public:
	inline static int32_t get_offset_of_scoreTextPrefab_4() { return static_cast<int32_t>(offsetof(PieTin_t3174492024, ___scoreTextPrefab_4)); }
	inline GameObject_t1113636619 * get_scoreTextPrefab_4() const { return ___scoreTextPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_scoreTextPrefab_4() { return &___scoreTextPrefab_4; }
	inline void set_scoreTextPrefab_4(GameObject_t1113636619 * value)
	{
		___scoreTextPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___scoreTextPrefab_4), value);
	}

	inline static int32_t get_offset_of_myCanvas_5() { return static_cast<int32_t>(offsetof(PieTin_t3174492024, ___myCanvas_5)); }
	inline GameObject_t1113636619 * get_myCanvas_5() const { return ___myCanvas_5; }
	inline GameObject_t1113636619 ** get_address_of_myCanvas_5() { return &___myCanvas_5; }
	inline void set_myCanvas_5(GameObject_t1113636619 * value)
	{
		___myCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___myCanvas_5), value);
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(PieTin_t3174492024, ___audioSource_6)); }
	inline AudioSource_t3935305588 * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t3935305588 * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_6), value);
	}

	inline static int32_t get_offset_of_sounds_7() { return static_cast<int32_t>(offsetof(PieTin_t3174492024, ___sounds_7)); }
	inline AudioClipU5BU5D_t143221404* get_sounds_7() const { return ___sounds_7; }
	inline AudioClipU5BU5D_t143221404** get_address_of_sounds_7() { return &___sounds_7; }
	inline void set_sounds_7(AudioClipU5BU5D_t143221404* value)
	{
		___sounds_7 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIETIN_T3174492024_H
#ifndef PLAYER_T3266647312_H
#define PLAYER_T3266647312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t3266647312  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D Player::rb
	Rigidbody2D_t939494601 * ___rb_4;
	// System.Single Player::jump
	float ___jump_5;
	// Enums/gameColor Player::_currentColor
	int32_t ____currentColor_6;
	// UnityEngine.Quaternion Player::nextSliceRotation
	Quaternion_t2301928331  ___nextSliceRotation_7;
	// UnityEngine.Quaternion Player::negativeNextSliceRotation
	Quaternion_t2301928331  ___negativeNextSliceRotation_8;
	// System.Int32 Player::highScore
	int32_t ___highScore_9;
	// System.Boolean Player::alive
	bool ___alive_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Player::slices
	List_1_t2585711361 * ___slices_11;
	// UnityEngine.GameObject Player::myLight
	GameObject_t1113636619 * ___myLight_12;
	// System.Single Player::brightLight
	float ___brightLight_13;
	// UnityEngine.GameObject Player::scoreTextPrefab
	GameObject_t1113636619 * ___scoreTextPrefab_14;
	// UnityEngine.GameObject Player::myCanvas
	GameObject_t1113636619 * ___myCanvas_15;
	// UnityEngine.GameObject Player::blueSparksPrefab
	GameObject_t1113636619 * ___blueSparksPrefab_16;
	// UnityEngine.GameObject Player::redSparksPrefab
	GameObject_t1113636619 * ___redSparksPrefab_17;
	// UnityEngine.GameObject Player::yellowSparksPrefab
	GameObject_t1113636619 * ___yellowSparksPrefab_18;
	// UnityEngine.GameObject Player::skull
	GameObject_t1113636619 * ___skull_19;
	// UnityEngine.GameObject Player::pieTin
	GameObject_t1113636619 * ___pieTin_20;
	// System.Int32 Player::deathsSinceAd
	int32_t ___deathsSinceAd_21;
	// System.Int32 Player::stars
	int32_t ___stars_22;
	// UnityEngine.AudioSource Player::audioSource
	AudioSource_t3935305588 * ___audioSource_23;
	// UnityEngine.ParticleSystem Player::redExplosion
	ParticleSystem_t1800779281 * ___redExplosion_24;
	// UnityEngine.ParticleSystem Player::blueExplosion
	ParticleSystem_t1800779281 * ___blueExplosion_25;
	// UnityEngine.ParticleSystem Player::yellowExplosion
	ParticleSystem_t1800779281 * ___yellowExplosion_26;
	// UnityEngine.AudioClip[] Player::sounds
	AudioClipU5BU5D_t143221404* ___sounds_27;
	// System.Boolean Player::hasSeenTutorial
	bool ___hasSeenTutorial_28;
	// UnityEngine.Coroutine Player::currentFlare
	Coroutine_t3829159415 * ___currentFlare_29;

public:
	inline static int32_t get_offset_of_rb_4() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___rb_4)); }
	inline Rigidbody2D_t939494601 * get_rb_4() const { return ___rb_4; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb_4() { return &___rb_4; }
	inline void set_rb_4(Rigidbody2D_t939494601 * value)
	{
		___rb_4 = value;
		Il2CppCodeGenWriteBarrier((&___rb_4), value);
	}

	inline static int32_t get_offset_of_jump_5() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___jump_5)); }
	inline float get_jump_5() const { return ___jump_5; }
	inline float* get_address_of_jump_5() { return &___jump_5; }
	inline void set_jump_5(float value)
	{
		___jump_5 = value;
	}

	inline static int32_t get_offset_of__currentColor_6() { return static_cast<int32_t>(offsetof(Player_t3266647312, ____currentColor_6)); }
	inline int32_t get__currentColor_6() const { return ____currentColor_6; }
	inline int32_t* get_address_of__currentColor_6() { return &____currentColor_6; }
	inline void set__currentColor_6(int32_t value)
	{
		____currentColor_6 = value;
	}

	inline static int32_t get_offset_of_nextSliceRotation_7() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___nextSliceRotation_7)); }
	inline Quaternion_t2301928331  get_nextSliceRotation_7() const { return ___nextSliceRotation_7; }
	inline Quaternion_t2301928331 * get_address_of_nextSliceRotation_7() { return &___nextSliceRotation_7; }
	inline void set_nextSliceRotation_7(Quaternion_t2301928331  value)
	{
		___nextSliceRotation_7 = value;
	}

	inline static int32_t get_offset_of_negativeNextSliceRotation_8() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___negativeNextSliceRotation_8)); }
	inline Quaternion_t2301928331  get_negativeNextSliceRotation_8() const { return ___negativeNextSliceRotation_8; }
	inline Quaternion_t2301928331 * get_address_of_negativeNextSliceRotation_8() { return &___negativeNextSliceRotation_8; }
	inline void set_negativeNextSliceRotation_8(Quaternion_t2301928331  value)
	{
		___negativeNextSliceRotation_8 = value;
	}

	inline static int32_t get_offset_of_highScore_9() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___highScore_9)); }
	inline int32_t get_highScore_9() const { return ___highScore_9; }
	inline int32_t* get_address_of_highScore_9() { return &___highScore_9; }
	inline void set_highScore_9(int32_t value)
	{
		___highScore_9 = value;
	}

	inline static int32_t get_offset_of_alive_10() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___alive_10)); }
	inline bool get_alive_10() const { return ___alive_10; }
	inline bool* get_address_of_alive_10() { return &___alive_10; }
	inline void set_alive_10(bool value)
	{
		___alive_10 = value;
	}

	inline static int32_t get_offset_of_slices_11() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___slices_11)); }
	inline List_1_t2585711361 * get_slices_11() const { return ___slices_11; }
	inline List_1_t2585711361 ** get_address_of_slices_11() { return &___slices_11; }
	inline void set_slices_11(List_1_t2585711361 * value)
	{
		___slices_11 = value;
		Il2CppCodeGenWriteBarrier((&___slices_11), value);
	}

	inline static int32_t get_offset_of_myLight_12() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___myLight_12)); }
	inline GameObject_t1113636619 * get_myLight_12() const { return ___myLight_12; }
	inline GameObject_t1113636619 ** get_address_of_myLight_12() { return &___myLight_12; }
	inline void set_myLight_12(GameObject_t1113636619 * value)
	{
		___myLight_12 = value;
		Il2CppCodeGenWriteBarrier((&___myLight_12), value);
	}

	inline static int32_t get_offset_of_brightLight_13() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___brightLight_13)); }
	inline float get_brightLight_13() const { return ___brightLight_13; }
	inline float* get_address_of_brightLight_13() { return &___brightLight_13; }
	inline void set_brightLight_13(float value)
	{
		___brightLight_13 = value;
	}

	inline static int32_t get_offset_of_scoreTextPrefab_14() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___scoreTextPrefab_14)); }
	inline GameObject_t1113636619 * get_scoreTextPrefab_14() const { return ___scoreTextPrefab_14; }
	inline GameObject_t1113636619 ** get_address_of_scoreTextPrefab_14() { return &___scoreTextPrefab_14; }
	inline void set_scoreTextPrefab_14(GameObject_t1113636619 * value)
	{
		___scoreTextPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___scoreTextPrefab_14), value);
	}

	inline static int32_t get_offset_of_myCanvas_15() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___myCanvas_15)); }
	inline GameObject_t1113636619 * get_myCanvas_15() const { return ___myCanvas_15; }
	inline GameObject_t1113636619 ** get_address_of_myCanvas_15() { return &___myCanvas_15; }
	inline void set_myCanvas_15(GameObject_t1113636619 * value)
	{
		___myCanvas_15 = value;
		Il2CppCodeGenWriteBarrier((&___myCanvas_15), value);
	}

	inline static int32_t get_offset_of_blueSparksPrefab_16() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___blueSparksPrefab_16)); }
	inline GameObject_t1113636619 * get_blueSparksPrefab_16() const { return ___blueSparksPrefab_16; }
	inline GameObject_t1113636619 ** get_address_of_blueSparksPrefab_16() { return &___blueSparksPrefab_16; }
	inline void set_blueSparksPrefab_16(GameObject_t1113636619 * value)
	{
		___blueSparksPrefab_16 = value;
		Il2CppCodeGenWriteBarrier((&___blueSparksPrefab_16), value);
	}

	inline static int32_t get_offset_of_redSparksPrefab_17() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___redSparksPrefab_17)); }
	inline GameObject_t1113636619 * get_redSparksPrefab_17() const { return ___redSparksPrefab_17; }
	inline GameObject_t1113636619 ** get_address_of_redSparksPrefab_17() { return &___redSparksPrefab_17; }
	inline void set_redSparksPrefab_17(GameObject_t1113636619 * value)
	{
		___redSparksPrefab_17 = value;
		Il2CppCodeGenWriteBarrier((&___redSparksPrefab_17), value);
	}

	inline static int32_t get_offset_of_yellowSparksPrefab_18() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___yellowSparksPrefab_18)); }
	inline GameObject_t1113636619 * get_yellowSparksPrefab_18() const { return ___yellowSparksPrefab_18; }
	inline GameObject_t1113636619 ** get_address_of_yellowSparksPrefab_18() { return &___yellowSparksPrefab_18; }
	inline void set_yellowSparksPrefab_18(GameObject_t1113636619 * value)
	{
		___yellowSparksPrefab_18 = value;
		Il2CppCodeGenWriteBarrier((&___yellowSparksPrefab_18), value);
	}

	inline static int32_t get_offset_of_skull_19() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___skull_19)); }
	inline GameObject_t1113636619 * get_skull_19() const { return ___skull_19; }
	inline GameObject_t1113636619 ** get_address_of_skull_19() { return &___skull_19; }
	inline void set_skull_19(GameObject_t1113636619 * value)
	{
		___skull_19 = value;
		Il2CppCodeGenWriteBarrier((&___skull_19), value);
	}

	inline static int32_t get_offset_of_pieTin_20() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___pieTin_20)); }
	inline GameObject_t1113636619 * get_pieTin_20() const { return ___pieTin_20; }
	inline GameObject_t1113636619 ** get_address_of_pieTin_20() { return &___pieTin_20; }
	inline void set_pieTin_20(GameObject_t1113636619 * value)
	{
		___pieTin_20 = value;
		Il2CppCodeGenWriteBarrier((&___pieTin_20), value);
	}

	inline static int32_t get_offset_of_deathsSinceAd_21() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___deathsSinceAd_21)); }
	inline int32_t get_deathsSinceAd_21() const { return ___deathsSinceAd_21; }
	inline int32_t* get_address_of_deathsSinceAd_21() { return &___deathsSinceAd_21; }
	inline void set_deathsSinceAd_21(int32_t value)
	{
		___deathsSinceAd_21 = value;
	}

	inline static int32_t get_offset_of_stars_22() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___stars_22)); }
	inline int32_t get_stars_22() const { return ___stars_22; }
	inline int32_t* get_address_of_stars_22() { return &___stars_22; }
	inline void set_stars_22(int32_t value)
	{
		___stars_22 = value;
	}

	inline static int32_t get_offset_of_audioSource_23() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___audioSource_23)); }
	inline AudioSource_t3935305588 * get_audioSource_23() const { return ___audioSource_23; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_23() { return &___audioSource_23; }
	inline void set_audioSource_23(AudioSource_t3935305588 * value)
	{
		___audioSource_23 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_23), value);
	}

	inline static int32_t get_offset_of_redExplosion_24() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___redExplosion_24)); }
	inline ParticleSystem_t1800779281 * get_redExplosion_24() const { return ___redExplosion_24; }
	inline ParticleSystem_t1800779281 ** get_address_of_redExplosion_24() { return &___redExplosion_24; }
	inline void set_redExplosion_24(ParticleSystem_t1800779281 * value)
	{
		___redExplosion_24 = value;
		Il2CppCodeGenWriteBarrier((&___redExplosion_24), value);
	}

	inline static int32_t get_offset_of_blueExplosion_25() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___blueExplosion_25)); }
	inline ParticleSystem_t1800779281 * get_blueExplosion_25() const { return ___blueExplosion_25; }
	inline ParticleSystem_t1800779281 ** get_address_of_blueExplosion_25() { return &___blueExplosion_25; }
	inline void set_blueExplosion_25(ParticleSystem_t1800779281 * value)
	{
		___blueExplosion_25 = value;
		Il2CppCodeGenWriteBarrier((&___blueExplosion_25), value);
	}

	inline static int32_t get_offset_of_yellowExplosion_26() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___yellowExplosion_26)); }
	inline ParticleSystem_t1800779281 * get_yellowExplosion_26() const { return ___yellowExplosion_26; }
	inline ParticleSystem_t1800779281 ** get_address_of_yellowExplosion_26() { return &___yellowExplosion_26; }
	inline void set_yellowExplosion_26(ParticleSystem_t1800779281 * value)
	{
		___yellowExplosion_26 = value;
		Il2CppCodeGenWriteBarrier((&___yellowExplosion_26), value);
	}

	inline static int32_t get_offset_of_sounds_27() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___sounds_27)); }
	inline AudioClipU5BU5D_t143221404* get_sounds_27() const { return ___sounds_27; }
	inline AudioClipU5BU5D_t143221404** get_address_of_sounds_27() { return &___sounds_27; }
	inline void set_sounds_27(AudioClipU5BU5D_t143221404* value)
	{
		___sounds_27 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_27), value);
	}

	inline static int32_t get_offset_of_hasSeenTutorial_28() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___hasSeenTutorial_28)); }
	inline bool get_hasSeenTutorial_28() const { return ___hasSeenTutorial_28; }
	inline bool* get_address_of_hasSeenTutorial_28() { return &___hasSeenTutorial_28; }
	inline void set_hasSeenTutorial_28(bool value)
	{
		___hasSeenTutorial_28 = value;
	}

	inline static int32_t get_offset_of_currentFlare_29() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___currentFlare_29)); }
	inline Coroutine_t3829159415 * get_currentFlare_29() const { return ___currentFlare_29; }
	inline Coroutine_t3829159415 ** get_address_of_currentFlare_29() { return &___currentFlare_29; }
	inline void set_currentFlare_29(Coroutine_t3829159415 * value)
	{
		___currentFlare_29 = value;
		Il2CppCodeGenWriteBarrier((&___currentFlare_29), value);
	}
};

struct Player_t3266647312_StaticFields
{
public:
	// System.Action Player::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_30() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___U3CU3Ef__amU24cache0_30)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_30() const { return ___U3CU3Ef__amU24cache0_30; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_30() { return &___U3CU3Ef__amU24cache0_30; }
	inline void set_U3CU3Ef__amU24cache0_30(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T3266647312_H
#ifndef PURCHASEMANAGER_T1431997891_H
#define PURCHASEMANAGER_T1431997891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PurchaseManager
struct  PurchaseManager_t1431997891  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PurchaseManager::noAds
	bool ___noAds_5;

public:
	inline static int32_t get_offset_of_noAds_5() { return static_cast<int32_t>(offsetof(PurchaseManager_t1431997891, ___noAds_5)); }
	inline bool get_noAds_5() const { return ___noAds_5; }
	inline bool* get_address_of_noAds_5() { return &___noAds_5; }
	inline void set_noAds_5(bool value)
	{
		___noAds_5 = value;
	}
};

struct PurchaseManager_t1431997891_StaticFields
{
public:
	// PurchaseManager PurchaseManager::purchaseManager
	PurchaseManager_t1431997891 * ___purchaseManager_4;

public:
	inline static int32_t get_offset_of_purchaseManager_4() { return static_cast<int32_t>(offsetof(PurchaseManager_t1431997891_StaticFields, ___purchaseManager_4)); }
	inline PurchaseManager_t1431997891 * get_purchaseManager_4() const { return ___purchaseManager_4; }
	inline PurchaseManager_t1431997891 ** get_address_of_purchaseManager_4() { return &___purchaseManager_4; }
	inline void set_purchaseManager_4(PurchaseManager_t1431997891 * value)
	{
		___purchaseManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEMANAGER_T1431997891_H
#ifndef SCREENMANAGER_T3266275672_H
#define SCREENMANAGER_T3266275672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenManager
struct  ScreenManager_t3266275672  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct ScreenManager_t3266275672_StaticFields
{
public:
	// ScreenManager ScreenManager::screenManager
	ScreenManager_t3266275672 * ___screenManager_4;

public:
	inline static int32_t get_offset_of_screenManager_4() { return static_cast<int32_t>(offsetof(ScreenManager_t3266275672_StaticFields, ___screenManager_4)); }
	inline ScreenManager_t3266275672 * get_screenManager_4() const { return ___screenManager_4; }
	inline ScreenManager_t3266275672 ** get_address_of_screenManager_4() { return &___screenManager_4; }
	inline void set_screenManager_4(ScreenManager_t3266275672 * value)
	{
		___screenManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenManager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMANAGER_T3266275672_H
#ifndef SOUNDCONTROL_T2118504000_H
#define SOUNDCONTROL_T2118504000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundControl
struct  SoundControl_t2118504000  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator SoundControl::animator
	Animator_t434523843 * ___animator_4;
	// System.Boolean SoundControl::active
	bool ___active_5;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(SoundControl_t2118504000, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(SoundControl_t2118504000, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDCONTROL_T2118504000_H
#ifndef SPARKSSOUND_T2601108065_H
#define SPARKSSOUND_T2601108065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SparksSound
struct  SparksSound_t2601108065  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPARKSSOUND_T2601108065_H
#ifndef TINYMOTION_T1798430399_H
#define TINYMOTION_T1798430399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TinyMotion
struct  TinyMotion_t1798430399  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TINYMOTION_T1798430399_H
#ifndef TOUCHHANDLER_T3441426771_H
#define TOUCHHANDLER_T3441426771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchHandler
struct  TouchHandler_t3441426771  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRubyShared.TapGestureRecognizer TouchHandler::tap
	TapGestureRecognizer_t3178883670 * ___tap_4;
	// Player TouchHandler::player
	Player_t3266647312 * ___player_5;
	// System.Boolean TouchHandler::tapEnd
	bool ___tapEnd_6;

public:
	inline static int32_t get_offset_of_tap_4() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___tap_4)); }
	inline TapGestureRecognizer_t3178883670 * get_tap_4() const { return ___tap_4; }
	inline TapGestureRecognizer_t3178883670 ** get_address_of_tap_4() { return &___tap_4; }
	inline void set_tap_4(TapGestureRecognizer_t3178883670 * value)
	{
		___tap_4 = value;
		Il2CppCodeGenWriteBarrier((&___tap_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___player_5)); }
	inline Player_t3266647312 * get_player_5() const { return ___player_5; }
	inline Player_t3266647312 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player_t3266647312 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_tapEnd_6() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___tapEnd_6)); }
	inline bool get_tapEnd_6() const { return ___tapEnd_6; }
	inline bool* get_address_of_tapEnd_6() { return &___tapEnd_6; }
	inline void set_tapEnd_6(bool value)
	{
		___tapEnd_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHANDLER_T3441426771_H
#ifndef TUTORIAL_T2275885037_H
#define TUTORIAL_T2275885037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tutorial
struct  Tutorial_t2275885037  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Tutorial::currentSlideIndex
	int32_t ___currentSlideIndex_4;
	// UnityEngine.GameObject[] Tutorial::tutorialSlides
	GameObjectU5BU5D_t3328599146* ___tutorialSlides_5;
	// UnityEngine.GameObject Tutorial::nextButton
	GameObject_t1113636619 * ___nextButton_6;
	// UnityEngine.GameObject Tutorial::skipButton
	GameObject_t1113636619 * ___skipButton_7;

public:
	inline static int32_t get_offset_of_currentSlideIndex_4() { return static_cast<int32_t>(offsetof(Tutorial_t2275885037, ___currentSlideIndex_4)); }
	inline int32_t get_currentSlideIndex_4() const { return ___currentSlideIndex_4; }
	inline int32_t* get_address_of_currentSlideIndex_4() { return &___currentSlideIndex_4; }
	inline void set_currentSlideIndex_4(int32_t value)
	{
		___currentSlideIndex_4 = value;
	}

	inline static int32_t get_offset_of_tutorialSlides_5() { return static_cast<int32_t>(offsetof(Tutorial_t2275885037, ___tutorialSlides_5)); }
	inline GameObjectU5BU5D_t3328599146* get_tutorialSlides_5() const { return ___tutorialSlides_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_tutorialSlides_5() { return &___tutorialSlides_5; }
	inline void set_tutorialSlides_5(GameObjectU5BU5D_t3328599146* value)
	{
		___tutorialSlides_5 = value;
		Il2CppCodeGenWriteBarrier((&___tutorialSlides_5), value);
	}

	inline static int32_t get_offset_of_nextButton_6() { return static_cast<int32_t>(offsetof(Tutorial_t2275885037, ___nextButton_6)); }
	inline GameObject_t1113636619 * get_nextButton_6() const { return ___nextButton_6; }
	inline GameObject_t1113636619 ** get_address_of_nextButton_6() { return &___nextButton_6; }
	inline void set_nextButton_6(GameObject_t1113636619 * value)
	{
		___nextButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_6), value);
	}

	inline static int32_t get_offset_of_skipButton_7() { return static_cast<int32_t>(offsetof(Tutorial_t2275885037, ___skipButton_7)); }
	inline GameObject_t1113636619 * get_skipButton_7() const { return ___skipButton_7; }
	inline GameObject_t1113636619 ** get_address_of_skipButton_7() { return &___skipButton_7; }
	inline void set_skipButton_7(GameObject_t1113636619 * value)
	{
		___skipButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___skipButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIAL_T2275885037_H
#ifndef TUTORIALMANAGER_T3418541267_H
#define TUTORIALMANAGER_T3418541267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager
struct  TutorialManager_t3418541267  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TutorialManager::viewedTutorial
	bool ___viewedTutorial_4;
	// Tutorial TutorialManager::tutorial
	Tutorial_t2275885037 * ___tutorial_5;

public:
	inline static int32_t get_offset_of_viewedTutorial_4() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___viewedTutorial_4)); }
	inline bool get_viewedTutorial_4() const { return ___viewedTutorial_4; }
	inline bool* get_address_of_viewedTutorial_4() { return &___viewedTutorial_4; }
	inline void set_viewedTutorial_4(bool value)
	{
		___viewedTutorial_4 = value;
	}

	inline static int32_t get_offset_of_tutorial_5() { return static_cast<int32_t>(offsetof(TutorialManager_t3418541267, ___tutorial_5)); }
	inline Tutorial_t2275885037 * get_tutorial_5() const { return ___tutorial_5; }
	inline Tutorial_t2275885037 ** get_address_of_tutorial_5() { return &___tutorial_5; }
	inline void set_tutorial_5(Tutorial_t2275885037 * value)
	{
		___tutorial_5 = value;
		Il2CppCodeGenWriteBarrier((&___tutorial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALMANAGER_T3418541267_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { sizeof (SavedGameMetadataUpdate_t1775293339)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5103[5] = 
{
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { sizeof (Builder_t140438593)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5104[5] = 
{
	Builder_t140438593::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { sizeof (VideoCapabilities_t1298735124), -1, sizeof(VideoCapabilities_t1298735124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5107[7] = 
{
	VideoCapabilities_t1298735124::get_offset_of_mIsCameraSupported_0(),
	VideoCapabilities_t1298735124::get_offset_of_mIsMicSupported_1(),
	VideoCapabilities_t1298735124::get_offset_of_mIsWriteStorageSupported_2(),
	VideoCapabilities_t1298735124::get_offset_of_mCaptureModesSupported_3(),
	VideoCapabilities_t1298735124::get_offset_of_mQualityLevelsSupported_4(),
	VideoCapabilities_t1298735124_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	VideoCapabilities_t1298735124_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (VideoCaptureState_t2350658599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5108[5] = 
{
	VideoCaptureState_t2350658599::get_offset_of_mIsCapturing_0(),
	VideoCaptureState_t2350658599::get_offset_of_mCaptureMode_1(),
	VideoCaptureState_t2350658599::get_offset_of_mQualityLevel_2(),
	VideoCaptureState_t2350658599::get_offset_of_mIsOverlayVisible_3(),
	VideoCaptureState_t2350658599::get_offset_of_mIsPaused_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (Logger_t3934082555), -1, sizeof(Logger_t3934082555_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5109[2] = 
{
	Logger_t3934082555_StaticFields::get_offset_of_debugLogEnabled_0(),
	Logger_t3934082555_StaticFields::get_offset_of_warningLogEnabled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (U3CdU3Ec__AnonStorey0_t2350509859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5110[1] = 
{
	U3CdU3Ec__AnonStorey0_t2350509859::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (U3CwU3Ec__AnonStorey1_t2080961746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[1] = 
{
	U3CwU3Ec__AnonStorey1_t2080961746::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { sizeof (U3CeU3Ec__AnonStorey2_t2346119983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5112[1] = 
{
	U3CeU3Ec__AnonStorey2_t2346119983::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { sizeof (Misc_t4208016214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { sizeof (PlayGamesHelperObject_t4023745847), -1, sizeof(PlayGamesHelperObject_t4023745847_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5114[7] = 
{
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_instance_4(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sIsDummy_5(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sQueue_6(),
	PlayGamesHelperObject_t4023745847::get_offset_of_localQueue_7(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sQueueEmpty_8(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sPauseCallbackList_9(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sFocusCallbackList_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { sizeof (U3CRunCoroutineU3Ec__AnonStorey0_t3592917427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5115[1] = 
{
	U3CRunCoroutineU3Ec__AnonStorey0_t3592917427::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { sizeof (PluginVersion_t2872281160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5116[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (ButtonUtils_t4203118290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { sizeof (ColorChanger_t3353685796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5118[1] = 
{
	ColorChanger_t3353685796::get_offset_of_myColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { sizeof (Enums_t1053268879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { sizeof (gameColor_t1347690756)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5120[4] = 
{
	gameColor_t1347690756::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (GameCanvas_t3794123731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5121[3] = 
{
	GameCanvas_t3794123731::get_offset_of_scoreTexts_4(),
	GameCanvas_t3794123731::get_offset_of_highScoreText_5(),
	GameCanvas_t3794123731::get_offset_of_finalScorePanel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (GameManager_t1536523654), -1, sizeof(GameManager_t1536523654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5122[40] = 
{
	GameManager_t1536523654_StaticFields::get_offset_of_gameManager_4(),
	0,
	GameManager_t1536523654::get_offset_of_paused_6(),
	GameManager_t1536523654::get_offset_of_player_7(),
	GameManager_t1536523654::get_offset_of_PLAYER_GRAVITY_SCALE_8(),
	GameManager_t1536523654::get_offset_of_TILE_WIDTH_9(),
	GameManager_t1536523654::get_offset_of_gameCameraObj_10(),
	GameManager_t1536523654::get_offset_of_gameCamera_11(),
	GameManager_t1536523654::get_offset_of__lowerLeftCornerWP_12(),
	GameManager_t1536523654::get_offset_of__upperRightCornerWP_13(),
	GameManager_t1536523654::get_offset_of__lowerRightCornerWP_14(),
	GameManager_t1536523654::get_offset_of__upperLefCornerWP_15(),
	GameManager_t1536523654::get_offset_of_spaceBetweenObstacles_16(),
	GameManager_t1536523654::get_offset_of_floor_17(),
	GameManager_t1536523654::get_offset_of_ceiling_18(),
	GameManager_t1536523654::get_offset_of_tilePrefabs_19(),
	GameManager_t1536523654::get_offset_of_obstacleKeys_20(),
	GameManager_t1536523654::get_offset_of_obstaclePrefabs_21(),
	GameManager_t1536523654::get_offset_of_pieSlicePrefabs_22(),
	GameManager_t1536523654::get_offset_of_currentPieSlicePrefab_23(),
	GameManager_t1536523654::get_offset_of_currentPieColorCount_24(),
	GameManager_t1536523654::get_offset_of_upcomingPieSlices_25(),
	GameManager_t1536523654::get_offset_of_upcomingObstacles_26(),
	GameManager_t1536523654::get_offset_of_gameSpeed_27(),
	GameManager_t1536523654::get_offset_of_Red_28(),
	GameManager_t1536523654::get_offset_of_Yellow_29(),
	GameManager_t1536523654::get_offset_of_Blue_30(),
	GameManager_t1536523654::get_offset_of_ErrorPink_31(),
	GameManager_t1536523654_StaticFields::get_offset_of_getrandom_32(),
	GameManager_t1536523654::get_offset_of_objPos2_33(),
	GameManager_t1536523654::get_offset_of_objPos3_34(),
	GameManager_t1536523654::get_offset_of_objPos4_35(),
	GameManager_t1536523654::get_offset_of_objPos1_36(),
	GameManager_t1536523654::get_offset_of_objPos5_37(),
	GameManager_t1536523654::get_offset_of_scoreTexts_38(),
	GameManager_t1536523654::get_offset_of_currentScore_39(),
	GameManager_t1536523654::get_offset_of_finalScorePanel_40(),
	GameManager_t1536523654::get_offset_of_highScoreText_41(),
	0,
	GameManager_t1536523654::get_offset_of_currentCanvas_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { sizeof (LaserParticleSystem_t3216800139), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { sizeof (LaserScript_t3521506685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5124[3] = 
{
	LaserScript_t3521506685::get_offset_of_startPoint_4(),
	LaserScript_t3521506685::get_offset_of_endPoint_5(),
	LaserScript_t3521506685::get_offset_of_laserLine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (Leaderboard_t2917364101), -1, sizeof(Leaderboard_t2917364101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5125[1] = 
{
	Leaderboard_t2917364101_StaticFields::get_offset_of_leaderboard_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (Moveable_t2836551010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5126[2] = 
{
	Moveable_t2836551010::get_offset_of_shouldMove_4(),
	Moveable_t2836551010::get_offset_of_moveVector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (MusicManager_t3024629483), -1, sizeof(MusicManager_t3024629483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5127[7] = 
{
	MusicManager_t3024629483_StaticFields::get_offset_of_musicManager_4(),
	MusicManager_t3024629483::get_offset_of_levelMusicArray_5(),
	MusicManager_t3024629483::get_offset_of_audioSource_6(),
	MusicManager_t3024629483::get_offset_of_startVolume_7(),
	MusicManager_t3024629483::get_offset_of_currentMusicSceneIndex_8(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (U3CFadeOutU3Ec__Iterator0_t1868787335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5128[5] = 
{
	U3CFadeOutU3Ec__Iterator0_t1868787335::get_offset_of_FadeTime_0(),
	U3CFadeOutU3Ec__Iterator0_t1868787335::get_offset_of_U24this_1(),
	U3CFadeOutU3Ec__Iterator0_t1868787335::get_offset_of_U24current_2(),
	U3CFadeOutU3Ec__Iterator0_t1868787335::get_offset_of_U24disposing_3(),
	U3CFadeOutU3Ec__Iterator0_t1868787335::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { sizeof (ObjectPool_t251773294), -1, sizeof(ObjectPool_t251773294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5129[3] = 
{
	ObjectPool_t251773294_StaticFields::get_offset_of_pool_4(),
	ObjectPool_t251773294::get_offset_of_objectPrefabs_5(),
	ObjectPool_t251773294::get_offset_of_cachedObjects_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (PoolPrefabs_t1376732323)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5130[2] = 
{
	PoolPrefabs_t1376732323::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoolPrefabs_t1376732323::get_offset_of_Prefab_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (Obstacle_t162511623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5131[1] = 
{
	Obstacle_t162511623::get_offset_of_generateNewWhenDone_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (ParticleSystemAutoDestroy_t798598573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5132[1] = 
{
	ParticleSystemAutoDestroy_t798598573::get_offset_of_ps_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { sizeof (PieSlice_t1066222354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5133[8] = 
{
	PieSlice_t1066222354::get_offset_of_used_4(),
	PieSlice_t1066222354::get_offset_of_scoreText_5(),
	PieSlice_t1066222354::get_offset_of_myCanvas_6(),
	PieSlice_t1066222354::get_offset_of_textAnimator_7(),
	PieSlice_t1066222354::get_offset_of_pieTin_8(),
	PieSlice_t1066222354::get_offset_of_moving_9(),
	PieSlice_t1066222354::get_offset_of_coinSound_10(),
	PieSlice_t1066222354::get_offset_of__myColor_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5134[6] = 
{
	U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596::get_offset_of_waitTime_0(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596::get_offset_of_method_1(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596::get_offset_of_U24this_2(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596::get_offset_of_U24current_3(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596::get_offset_of_U24disposing_4(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator0_t792342596::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { sizeof (PieTin_t3174492024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5135[4] = 
{
	PieTin_t3174492024::get_offset_of_scoreTextPrefab_4(),
	PieTin_t3174492024::get_offset_of_myCanvas_5(),
	PieTin_t3174492024::get_offset_of_audioSource_6(),
	PieTin_t3174492024::get_offset_of_sounds_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { sizeof (Player_t3266647312), -1, sizeof(Player_t3266647312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5136[27] = 
{
	Player_t3266647312::get_offset_of_rb_4(),
	Player_t3266647312::get_offset_of_jump_5(),
	Player_t3266647312::get_offset_of__currentColor_6(),
	Player_t3266647312::get_offset_of_nextSliceRotation_7(),
	Player_t3266647312::get_offset_of_negativeNextSliceRotation_8(),
	Player_t3266647312::get_offset_of_highScore_9(),
	Player_t3266647312::get_offset_of_alive_10(),
	Player_t3266647312::get_offset_of_slices_11(),
	Player_t3266647312::get_offset_of_myLight_12(),
	Player_t3266647312::get_offset_of_brightLight_13(),
	Player_t3266647312::get_offset_of_scoreTextPrefab_14(),
	Player_t3266647312::get_offset_of_myCanvas_15(),
	Player_t3266647312::get_offset_of_blueSparksPrefab_16(),
	Player_t3266647312::get_offset_of_redSparksPrefab_17(),
	Player_t3266647312::get_offset_of_yellowSparksPrefab_18(),
	Player_t3266647312::get_offset_of_skull_19(),
	Player_t3266647312::get_offset_of_pieTin_20(),
	Player_t3266647312::get_offset_of_deathsSinceAd_21(),
	Player_t3266647312::get_offset_of_stars_22(),
	Player_t3266647312::get_offset_of_audioSource_23(),
	Player_t3266647312::get_offset_of_redExplosion_24(),
	Player_t3266647312::get_offset_of_blueExplosion_25(),
	Player_t3266647312::get_offset_of_yellowExplosion_26(),
	Player_t3266647312::get_offset_of_sounds_27(),
	Player_t3266647312::get_offset_of_hasSeenTutorial_28(),
	Player_t3266647312::get_offset_of_currentFlare_29(),
	Player_t3266647312_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { sizeof (U3CFlareLightU3Ec__Iterator0_t1332935211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5137[5] = 
{
	U3CFlareLightU3Ec__Iterator0_t1332935211::get_offset_of_U3CfU3E__1_0(),
	U3CFlareLightU3Ec__Iterator0_t1332935211::get_offset_of_U24this_1(),
	U3CFlareLightU3Ec__Iterator0_t1332935211::get_offset_of_U24current_2(),
	U3CFlareLightU3Ec__Iterator0_t1332935211::get_offset_of_U24disposing_3(),
	U3CFlareLightU3Ec__Iterator0_t1332935211::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { sizeof (U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5138[7] = 
{
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_waitTime_0(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_method_1(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_U3CsU3E__0_2(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_U24this_3(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_U24current_4(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_U24disposing_5(),
	U3CWaitForSecondsThenExecuteU3Ec__Iterator1_t417809513::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (PlayerData_t220878115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5139[4] = 
{
	PlayerData_t220878115::get_offset_of_U3ChighScoreU3Ek__BackingField_0(),
	PlayerData_t220878115::get_offset_of_U3CdeathsSinceAdU3Ek__BackingField_1(),
	PlayerData_t220878115::get_offset_of_U3CstarsU3Ek__BackingField_2(),
	PlayerData_t220878115::get_offset_of_U3ChasSeenTutorialU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (PurchaseManager_t1431997891), -1, sizeof(PurchaseManager_t1431997891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5140[2] = 
{
	PurchaseManager_t1431997891_StaticFields::get_offset_of_purchaseManager_4(),
	PurchaseManager_t1431997891::get_offset_of_noAds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (ScreenManager_t3266275672), -1, sizeof(ScreenManager_t3266275672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5141[1] = 
{
	ScreenManager_t3266275672_StaticFields::get_offset_of_screenManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (SoundControl_t2118504000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5142[2] = 
{
	SoundControl_t2118504000::get_offset_of_animator_4(),
	SoundControl_t2118504000::get_offset_of_active_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (SparksSound_t2601108065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (TinyMotion_t1798430399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (TouchHandler_t3441426771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5145[3] = 
{
	TouchHandler_t3441426771::get_offset_of_tap_4(),
	TouchHandler_t3441426771::get_offset_of_player_5(),
	TouchHandler_t3441426771::get_offset_of_tapEnd_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (Tutorial_t2275885037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5146[4] = 
{
	Tutorial_t2275885037::get_offset_of_currentSlideIndex_4(),
	Tutorial_t2275885037::get_offset_of_tutorialSlides_5(),
	Tutorial_t2275885037::get_offset_of_nextButton_6(),
	Tutorial_t2275885037::get_offset_of_skipButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (TutorialManager_t3418541267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5147[2] = 
{
	TutorialManager_t3418541267::get_offset_of_viewedTutorial_4(),
	TutorialManager_t3418541267::get_offset_of_tutorial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (PooledObjectScript_t3251621882), -1, sizeof(PooledObjectScript_t3251621882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5148[3] = 
{
	PooledObjectScript_t3251621882_StaticFields::get_offset_of_instantiatedCount_4(),
	PooledObjectScript_t3251621882_StaticFields::get_offset_of_spawnCount_5(),
	PooledObjectScript_t3251621882_StaticFields::get_offset_of_returnToPoolCount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { sizeof (SpawningPoolDemoScript_t4019697319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5149[6] = 
{
	SpawningPoolDemoScript_t4019697319::get_offset_of_Options_4(),
	SpawningPoolDemoScript_t4019697319::get_offset_of_SpawnButton_5(),
	SpawningPoolDemoScript_t4019697319::get_offset_of_RegularButton_6(),
	SpawningPoolDemoScript_t4019697319::get_offset_of_Results_7(),
	SpawningPoolDemoScript_t4019697319::get_offset_of_Prefabs_8(),
	SpawningPoolDemoScript_t4019697319::get_offset_of_regular_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { sizeof (SpawningPoolScript_t3963462420), -1, sizeof(SpawningPoolScript_t3963462420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5150[4] = 
{
	SpawningPoolScript_t3963462420_StaticFields::get_offset_of_instance_4(),
	SpawningPoolScript_t3963462420::get_offset_of_awake_5(),
	SpawningPoolScript_t3963462420::get_offset_of_Prefabs_6(),
	SpawningPoolScript_t3963462420::get_offset_of_ReturnToCacheOnLevelLoad_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { sizeof (SpawningPoolEntry_t2116637239)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5151[2] = 
{
	SpawningPoolEntry_t2116637239::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpawningPoolEntry_t2116637239::get_offset_of_Prefab_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { sizeof (SpawningPool_t4119068062), -1, sizeof(SpawningPool_t4119068062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5152[7] = 
{
	SpawningPool_t4119068062_StaticFields::get_offset_of_prefabs_0(),
	SpawningPool_t4119068062_StaticFields::get_offset_of_activeGameObjectsAndKeys_1(),
	SpawningPool_t4119068062_StaticFields::get_offset_of_cachedKeysAndGameObjects_2(),
	SpawningPool_t4119068062_StaticFields::get_offset_of_activeKeysAndGameObjects_3(),
	SpawningPool_t4119068062_StaticFields::get_offset_of_interfaces_4(),
	SpawningPool_t4119068062_StaticFields::get_offset_of_cacheCount_5(),
	SpawningPool_t4119068062_StaticFields::get_offset_of_DefaultHideFlags_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5153[5] = 
{
	U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795::get_offset_of_U24locvar0_0(),
	U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795::get_offset_of_U3CvU3E__1_1(),
	U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795::get_offset_of_U24current_2(),
	U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795::get_offset_of_U24disposing_3(),
	U3CEnumerateActiveObjectsU3Ec__Iterator0_t265290795::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255376), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5155[62] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_0(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D5DFEE31002BF167E0453CB8643D3E1D44FE3F325_1(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D5BEBC138B151DE05C24CA6FF9FEEBDFADEDD6590_2(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D885DDD59952941A3E98DD105FDAADBE334B79776_3(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D6EB03E1E021F75A722B15DD1E230C3794339E58F_4(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D82C0D5F32D3B4A90C255DF4203A719570A4A71C6_5(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DDD84CB646A0ED87DE965197F182EA6E70D7710FB_6(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D5EAD1B560E3C4BBB1891EBD798CDF67E9A1E9258_7(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DEEDA7ECC31BEC73AC6BD760F7A50568CFB4013AB_8(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D7553F65BB6AEF17B8E9BEB262884FD704C46DC9D_9(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D8727E44B94789414DE204CBA117AD05443BDFF85_10(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DCD9B3692D323300F91E71C04211EA451047039D6_11(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D8D84ADE47A7F6C5BD6E58AD77989E68B131CE5C2_12(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D4A3ECE00BC914DE6DFAD954BACCA8E709CF9D2D9_13(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DF3D84C957438E86E3A03BA8266CEFF9B039760AC_14(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D25F3E2A2859DD25BCDE352AF7DE458D9F6F16ECE_15(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D8B2EABB0EC5F5A28752896B1DCF12E2336FD88EE_16(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D34EDEEBC5EB29562EF18C5F3AA190B324187790C_17(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DA6E8715255241F47AD0B3D93E1BA359D52D4F773_18(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D7EA0120ED2A607EE02EE175E161E3AB6678EAEF3_19(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DC63EC63DA66DB8BE3FC6B453EB4BE89CD99CEBC2_20(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D5568A7B56666FACA81071906CAD5B68BEF9CBA7D_21(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D80837B69D4B41AE55078725B3ACCF0A27C21EA8C_22(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DF1562B156A30ABF23C48AEEB151115973E5E0E68_23(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D21F3C52377932DF2C57E466D2DF531566BF21C8E_24(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D78B13EFFF801B0EEDCD91D16B161747601E46D7F_25(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D3F6D74DE7585081F2D19E26F1EE1DA761BDEF9AB_26(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D99B5F0E7E735A12759FD9F5EC78F60965B0D2B9D_27(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D631DB71B9AD2B5F8451E4E1C3EEFA69BEEBFE6B5_28(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D89B8D8C3F2ECF125BD35E36A6C60018CDBEA21FD_29(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D9EECA0B3D49F88F27A544B19940E1E8375C54E90_30(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DB1C28103C273F13F1776F8909515A1B1E42EFA96_31(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D408A4836C23B92A6BAD4820B377880F6CC1FA2A8_32(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D28B6A5CADCB8F8876EFB87B91DFDB82D64AFE3B6_33(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D457EC0A425D11B4CCAB4205123D64045679F8AB3_34(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D99826E7C05B8D36F8B35AD6ACE003A1BB707F7D6_35(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D4A13B3A60A8E56CEFBE3B2ADD2797F46A98DB4B6_36(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D3C3374FD7237FEFDDB8803792A0A6474B7A7DFE4_37(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D12536FB84378CA4223BBD8FB3833394D625A09D3_38(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D2903EDF33AFE43744D8D4676E80AE98CA93318D6_39(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D05A61FB9C1B133245C69208185ADD4E2668799FD_40(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DFD39839203F719185D79D5FB10889EBD650235A8_41(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DE11E7676B42F5205585524A542DC08BC21B6BC74_42(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D0AA3902D236F027C82FC3F064812EF11C90B8873_43(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D3DCA1BB2BAF067A0E9524B94CDD71DAC4D3E6A80_44(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D3D84862348B9A730AE6F506EA7FB70C660C88A2A_45(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D22C266A1AB69BE7A83C9C6F871BD3A5279AE38B9_46(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D8456C8142FC382C9C7D5CC2BA87FD135D90889B1_47(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D45507F8AFFC24037D807B05461DD6C77DEC72C1F_48(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D4AEB1BBADE6C364AF1EFBBEDF250D88620C4DB3C_49(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DF14497D0866AAE006DAA1A34D21DCA4E99BE3CB4_50(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D2278FF5409ABE24639F0DDD4E83AA8D807BA0A0C_51(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D183F34689D23DCB29B9F650AAE12A25144D57EA5_52(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D1B9653E9DAF24F1B6E8E0646A5426E8207CAC175_53(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D655283041A648AE2C424D8E20F6905C22E8834E8_54(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D23468C894E1BC8AFF095FB390877A8EAE07CAD38_55(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D450ADA4BD48BD2F5C266223142DBC1AFC68C533C_56(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D98B6C8EFF0078C310BF5655A7FF296752695FD7D_57(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DD238C52ED4F2F71965E88D2C02CBA5D46BBB6EF7_58(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DCFE35FBC0A074388D82D17E334381E3CB97AF384_59(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2D64B4EE118F3DF0EC4723AC0E41203320BCC36752_60(),
	U3CPrivateImplementationDetailsU3E_t3057255376_StaticFields::get_offset_of_U24fieldU2DB51C0DEB10EE1C5E9888F5306B744ACD1F38D767_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { sizeof (U24ArrayTypeU3D68_t2499083377)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D68_t2499083377 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (U24ArrayTypeU3D128_t4235014459)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D128_t4235014459 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { sizeof (U24ArrayTypeU3D520_t2265645983)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D520_t2265645983 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
