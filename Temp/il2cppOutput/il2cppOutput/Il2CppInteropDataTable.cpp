﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t2148130204_0_0_0;
extern "C" void RuntimeClassHandle_t4196268768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeClassHandle_t4196268768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeClassHandle_t4196268768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RuntimeClassHandle_t4196268768_0_0_0;
extern "C" void RuntimeGPtrArrayHandle_t2690631865_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeGPtrArrayHandle_t2690631865_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeGPtrArrayHandle_t2690631865_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RuntimeGPtrArrayHandle_t2690631865_0_0_0;
extern "C" void RuntimeGenericParamInfoHandle_t3649234556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeGenericParamInfoHandle_t3649234556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeGenericParamInfoHandle_t3649234556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RuntimeGenericParamInfoHandle_t3649234556_0_0_0;
extern "C" void RuntimeRemoteClassHandle_t2479637696_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RuntimeRemoteClassHandle_t2479637696_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RuntimeRemoteClassHandle_t2479637696_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RuntimeRemoteClassHandle_t2479637696_0_0_0;
extern "C" void GPtrArray_t1503373745_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GPtrArray_t1503373745_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GPtrArray_t1503373745_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GPtrArray_t1503373745_0_0_0;
extern "C" void GenericParamInfo_t1825140006_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GenericParamInfo_t1825140006_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GenericParamInfo_t1825140006_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GenericParamInfo_t1825140006_0_0_0;
extern "C" void RemoteClass_t4006407480_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RemoteClass_t4006407480_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RemoteClass_t4006407480_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RemoteClass_t4006407480_0_0_0;
extern "C" void SafeGPtrArrayHandle_t2768407725_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SafeGPtrArrayHandle_t2768407725_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SafeGPtrArrayHandle_t2768407725_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SafeGPtrArrayHandle_t2768407725_0_0_0;
extern "C" void SafeStringMarshal_t1394540657_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SafeStringMarshal_t1394540657_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SafeStringMarshal_t1394540657_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SafeStringMarshal_t1394540657_0_0_0;
extern "C" void UriScheme_t2867806342_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t2867806342_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t2867806342_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t2867806342_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const RuntimeType Action_t1264377477_0_0_0;
extern "C" void AppDomain_t1571427825_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AppDomain_t1571427825_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AppDomain_t1571427825_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AppDomain_t1571427825_0_0_0;
extern "C" void AppDomainSetup_t123196401_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AppDomainSetup_t123196401_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AppDomainSetup_t123196401_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AppDomainSetup_t123196401_0_0_0;
extern "C" void SorterGenericArray_t3557937876_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SorterGenericArray_t3557937876_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SorterGenericArray_t3557937876_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SorterGenericArray_t3557937876_0_0_0;
extern "C" void SorterObjectArray_t2043158284_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SorterObjectArray_t2043158284_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SorterObjectArray_t2043158284_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SorterObjectArray_t2043158284_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3123975638_0_0_0;
extern "C" void bucket_t758131704_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void bucket_t758131704_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void bucket_t758131704_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType bucket_t758131704_0_0_0;
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t872516951();
extern const RuntimeType InternalCancelHandler_t872516951_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowsCancelHandler_t1006184794();
extern const RuntimeType WindowsCancelHandler_t1006184794_0_0_0;
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConsoleKeyInfo_t1802691652_0_0_0;
extern "C" void DTSubString_t967826655_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DTSubString_t967826655_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DTSubString_t967826655_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DTSubString_t967826655_0_0_0;
extern "C" void DateTimeRawInfo_t1172279960_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DateTimeRawInfo_t1172279960_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DateTimeRawInfo_t1172279960_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DateTimeRawInfo_t1172279960_0_0_0;
extern "C" void DateTimeResult_t2747711438_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DateTimeResult_t2747711438_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DateTimeResult_t2747711438_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DateTimeResult_t2747711438_0_0_0;
extern "C" void Delegate_t1188392813_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Delegate_t1188392813_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Delegate_t1188392813_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Delegate_t1188392813_0_0_0;
extern "C" void StackFrame_t3217253059_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StackFrame_t3217253059_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StackFrame_t3217253059_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StackFrame_t3217253059_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t4135868527_0_0_0;
extern "C" void EnumResult_t2373072967_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EnumResult_t2373072967_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EnumResult_t2373072967_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EnumResult_t2373072967_0_0_0;
extern "C" void Exception_t_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Exception_t_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Exception_t_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Exception_t_0_0_0;
extern "C" void CalendarData_t473118650_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CalendarData_t473118650_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CalendarData_t473118650_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CalendarData_t473118650_0_0_0;
extern "C" void CultureData_t1899656083_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CultureData_t1899656083_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CultureData_t1899656083_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CultureData_t1899656083_0_0_0;
extern "C" void CultureInfo_t4157843068_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CultureInfo_t4157843068_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CultureInfo_t4157843068_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CultureInfo_t4157843068_0_0_0;
extern "C" void Data_t191216285_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Data_t191216285_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Data_t191216285_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Data_t191216285_0_0_0;
extern "C" void InternalCodePageDataItem_t2575532933_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InternalCodePageDataItem_t2575532933_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InternalCodePageDataItem_t2575532933_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InternalCodePageDataItem_t2575532933_0_0_0;
extern "C" void InternalEncodingDataItem_t3158859817_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InternalEncodingDataItem_t3158859817_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InternalEncodingDataItem_t3158859817_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InternalEncodingDataItem_t3158859817_0_0_0;
extern "C" void RegionInfo_t1090270226_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RegionInfo_t1090270226_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RegionInfo_t1090270226_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RegionInfo_t1090270226_0_0_0;
extern "C" void SortKey_t3955336732_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SortKey_t3955336732_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SortKey_t3955336732_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SortKey_t3955336732_0_0_0;
extern "C" void FormatLiterals_t1376140638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FormatLiterals_t1376140638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FormatLiterals_t1376140638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FormatLiterals_t1376140638_0_0_0;
extern "C" void GuidResult_t1584437374_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GuidResult_t1584437374_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GuidResult_t1584437374_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GuidResult_t1584437374_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const RuntimeType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const RuntimeType WriteDelegate_t4270993571_0_0_0;
extern "C" void ReadWriteParameters_t1050632132_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ReadWriteParameters_t1050632132_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ReadWriteParameters_t1050632132_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ReadWriteParameters_t1050632132_0_0_0;
extern "C" void InputRecord_t2660212290_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InputRecord_t2660212290_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InputRecord_t2660212290_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InputRecord_t2660212290_0_0_0;
extern "C" void MarshalByRefObject_t2760389100_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MarshalByRefObject_t2760389100_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MarshalByRefObject_t2760389100_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MarshalByRefObject_t2760389100_0_0_0;
extern "C" void MonoAsyncCall_t3023670838_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoAsyncCall_t3023670838_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoAsyncCall_t3023670838_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoAsyncCall_t3023670838_0_0_0;
extern "C" void MonoTypeInfo_t3366989025_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoTypeInfo_t3366989025_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoTypeInfo_t3366989025_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoTypeInfo_t3366989025_0_0_0;
extern "C" void MulticastDelegate_t_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MulticastDelegate_t_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MulticastDelegate_t_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MulticastDelegate_t_0_0_0;
extern "C" void NumberBuffer_t2021063877_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NumberBuffer_t2021063877_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NumberBuffer_t2021063877_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NumberBuffer_t2021063877_0_0_0;
extern "C" void FormatParam_t4194474082_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FormatParam_t4194474082_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FormatParam_t4194474082_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FormatParam_t4194474082_0_0_0;
extern "C" void ParamsArray_t4224664136_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParamsArray_t4224664136_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParamsArray_t4224664136_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParamsArray_t4224664136_0_0_0;
extern "C" void ParsingInfo_t1001632991_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParsingInfo_t1001632991_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParsingInfo_t1001632991_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParsingInfo_t1001632991_0_0_0;
extern "C" void Assembly_t_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Assembly_t_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Assembly_t_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Assembly_t_0_0_0;
extern "C" void AssemblyName_t270931938_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssemblyName_t270931938_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssemblyName_t270931938_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssemblyName_t270931938_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void LocalBuilder_t3562264111_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LocalBuilder_t3562264111_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LocalBuilder_t3562264111_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LocalBuilder_t3562264111_0_0_0;
extern "C" void ExceptionHandlingClause_t2620275417_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ExceptionHandlingClause_t2620275417_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ExceptionHandlingClause_t2620275417_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ExceptionHandlingClause_t2620275417_0_0_0;
extern "C" void InterfaceMapping_t1267958657_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InterfaceMapping_t1267958657_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InterfaceMapping_t1267958657_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InterfaceMapping_t1267958657_0_0_0;
extern "C" void LocalVariableInfo_t2426779395_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LocalVariableInfo_t2426779395_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LocalVariableInfo_t2426779395_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LocalVariableInfo_t2426779395_0_0_0;
extern "C" void MethodBody_t3098043463_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MethodBody_t3098043463_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MethodBody_t3098043463_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MethodBody_t3098043463_0_0_0;
extern "C" void Module_t2987026101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Module_t2987026101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Module_t2987026101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Module_t2987026101_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterInfo_t1861056598_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterInfo_t1861056598_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterInfo_t1861056598_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterInfo_t1861056598_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceLocator_t3723970807_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceLocator_t3723970807_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceLocator_t3723970807_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceLocator_t3723970807_0_0_0;
extern "C" void AsyncMethodBuilderCore_t2955600131_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncMethodBuilderCore_t2955600131_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncMethodBuilderCore_t2955600131_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncMethodBuilderCore_t2955600131_0_0_0;
extern "C" void AsyncTaskMethodBuilder_t3536885450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncTaskMethodBuilder_t3536885450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncTaskMethodBuilder_t3536885450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncTaskMethodBuilder_t3536885450_0_0_0;
extern "C" void ConfiguredTaskAwaitable_t166429847_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConfiguredTaskAwaitable_t166429847_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConfiguredTaskAwaitable_t166429847_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConfiguredTaskAwaitable_t166429847_0_0_0;
extern "C" void ConfiguredTaskAwaiter_t555647845_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConfiguredTaskAwaiter_t555647845_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConfiguredTaskAwaiter_t555647845_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConfiguredTaskAwaiter_t555647845_0_0_0;
extern "C" void Ephemeron_t1602596362_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Ephemeron_t1602596362_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Ephemeron_t1602596362_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Ephemeron_t1602596362_0_0_0;
extern "C" void TaskAwaiter_t919683548_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TaskAwaiter_t919683548_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TaskAwaiter_t919683548_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TaskAwaiter_t919683548_0_0_0;
extern "C" void ProcessMessageRes_t3710547145_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcessMessageRes_t3710547145_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcessMessageRes_t3710547145_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcessMessageRes_t3710547145_0_0_0;
extern "C" void Context_t3285446944_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t3285446944_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t3285446944_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t3285446944_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const RuntimeType CrossContextDelegate_t387175271_0_0_0;
extern "C" void AsyncResult_t4194309572_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncResult_t4194309572_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncResult_t4194309572_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncResult_t4194309572_0_0_0;
extern "C" void Reader_t983665205_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Reader_t983665205_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Reader_t983665205_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Reader_t983665205_0_0_0;
extern "C" void MonoMethodMessage_t2807636944_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodMessage_t2807636944_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodMessage_t2807636944_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodMessage_t2807636944_0_0_0;
extern "C" void RealProxy_t2312050253_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RealProxy_t2312050253_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RealProxy_t2312050253_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RealProxy_t2312050253_0_0_0;
extern "C" void TransparentProxy_t431418284_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransparentProxy_t431418284_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransparentProxy_t431418284_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TransparentProxy_t431418284_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t648286436_0_0_0;
extern "C" void DelegatePInvokeWrapper_SerializationEventHandler_t2446424469();
extern const RuntimeType SerializationEventHandler_t2446424469_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1885824122_0_0_0;
extern "C" void HashAlgorithmName_t3637476002_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HashAlgorithmName_t3637476002_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HashAlgorithmName_t3637476002_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HashAlgorithmName_t3637476002_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1728406613_0_0_0;
extern "C" void CancellationCallbackCoreWorkArguments_t3786748137_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationCallbackCoreWorkArguments_t3786748137_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationCallbackCoreWorkArguments_t3786748137_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CancellationCallbackCoreWorkArguments_t3786748137_0_0_0;
extern "C" void CancellationToken_t784455623_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationToken_t784455623_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationToken_t784455623_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CancellationToken_t784455623_0_0_0;
extern "C" void CancellationTokenRegistration_t2813424904_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CancellationTokenRegistration_t2813424904_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CancellationTokenRegistration_t2813424904_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CancellationTokenRegistration_t2813424904_0_0_0;
extern "C" void Reader_t1133934476_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Reader_t1133934476_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Reader_t1133934476_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Reader_t1133934476_0_0_0;
extern "C" void ExecutionContextSwitcher_t2326006776_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ExecutionContextSwitcher_t2326006776_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ExecutionContextSwitcher_t2326006776_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ExecutionContextSwitcher_t2326006776_0_0_0;
extern "C" void SpinLock_t508764825_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpinLock_t508764825_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpinLock_t508764825_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpinLock_t508764825_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const RuntimeType ThreadStart_t1006689297_0_0_0;
extern "C" void WaitHandle_t1743403487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitHandle_t1743403487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitHandle_t1743403487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitHandle_t1743403487_0_0_0;
extern "C" void DYNAMIC_TIME_ZONE_INFORMATION_t933351445_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DYNAMIC_TIME_ZONE_INFORMATION_t933351445_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DYNAMIC_TIME_ZONE_INFORMATION_t933351445_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DYNAMIC_TIME_ZONE_INFORMATION_t933351445_0_0_0;
extern "C" void TIME_ZONE_INFORMATION_t691679524_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TIME_ZONE_INFORMATION_t691679524_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TIME_ZONE_INFORMATION_t691679524_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TIME_ZONE_INFORMATION_t691679524_0_0_0;
extern "C" void TransitionTime_t449921781_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransitionTime_t449921781_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransitionTime_t449921781_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TransitionTime_t449921781_0_0_0;
extern "C" void UnSafeCharBuffer_t2176740272_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnSafeCharBuffer_t2176740272_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnSafeCharBuffer_t2176740272_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnSafeCharBuffer_t2176740272_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3640485471_0_0_0;
extern "C" void __DTString_t2014593278_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void __DTString_t2014593278_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void __DTString_t2014593278_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType __DTString_t2014593278_0_0_0;
extern "C" void XmlCharType_t2277243275_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void XmlCharType_t2277243275_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void XmlCharType_t2277243275_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType XmlCharType_t2277243275_0_0_0;
extern "C" void DelegatePInvokeWrapper_CFProxyAutoConfigurationResultCallback_t949084125();
extern const RuntimeType CFProxyAutoConfigurationResultCallback_t949084125_0_0_0;
extern "C" void unitytls_interface_struct_t2105669693_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void unitytls_interface_struct_t2105669693_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void unitytls_interface_struct_t2105669693_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType unitytls_interface_struct_t2105669693_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_errorstate_create_t_t663213720();
extern const RuntimeType unitytls_errorstate_create_t_t663213720_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_errorstate_raise_error_t_t3653623215();
extern const RuntimeType unitytls_errorstate_raise_error_t_t3653623215_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_key_free_t_t769506560();
extern const RuntimeType unitytls_key_free_t_t769506560_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_key_get_ref_t_t3903963684();
extern const RuntimeType unitytls_key_get_ref_t_t3903963684_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_key_parse_der_t_t352753247();
extern const RuntimeType unitytls_key_parse_der_t_t352753247_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_key_parse_pem_t_t422811231();
extern const RuntimeType unitytls_key_parse_pem_t_t422811231_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_random_generate_bytes_t_t1261472879();
extern const RuntimeType unitytls_random_generate_bytes_t_t1261472879_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_create_client_t_t4224747280();
extern const RuntimeType unitytls_tlsctx_create_client_t_t4224747280_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_create_server_t_t697296614();
extern const RuntimeType unitytls_tlsctx_create_server_t_t697296614_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_free_t_t2945881544();
extern const RuntimeType unitytls_tlsctx_free_t_t2945881544_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_get_ciphersuite_t_t2849089842();
extern const RuntimeType unitytls_tlsctx_get_ciphersuite_t_t2849089842_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_get_protocol_t_t3836938866();
extern const RuntimeType unitytls_tlsctx_get_protocol_t_t3836938866_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_notify_close_t_t823845960();
extern const RuntimeType unitytls_tlsctx_notify_close_t_t823845960_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_process_handshake_t_t2729115783();
extern const RuntimeType unitytls_tlsctx_process_handshake_t_t2729115783_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_read_t_t2716224336();
extern const RuntimeType unitytls_tlsctx_read_t_t2716224336_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_server_require_client_authentication_t_t2757305746();
extern const RuntimeType unitytls_tlsctx_server_require_client_authentication_t_t2757305746_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_set_certificate_callback_t_t928657621();
extern const RuntimeType unitytls_tlsctx_set_certificate_callback_t_t928657621_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_set_supported_ciphersuites_t_t674237385();
extern const RuntimeType unitytls_tlsctx_set_supported_ciphersuites_t_t674237385_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_set_trace_callback_t_t1728780939();
extern const RuntimeType unitytls_tlsctx_set_trace_callback_t_t1728780939_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_set_x509verify_callback_t_t3126135024();
extern const RuntimeType unitytls_tlsctx_set_x509verify_callback_t_t3126135024_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_write_t_t2414968268();
extern const RuntimeType unitytls_tlsctx_write_t_t2414968268_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509_export_der_t_t3165820883();
extern const RuntimeType unitytls_x509_export_der_t_t3165820883_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509list_append_der_t_t361658413();
extern const RuntimeType unitytls_x509list_append_der_t_t361658413_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509list_append_t_t1135075908();
extern const RuntimeType unitytls_x509list_append_t_t1135075908_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509list_create_t_t249995399();
extern const RuntimeType unitytls_x509list_create_t_t249995399_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509list_free_t_t1015882467();
extern const RuntimeType unitytls_x509list_free_t_t1015882467_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509list_get_ref_t_t3178515812();
extern const RuntimeType unitytls_x509list_get_ref_t_t3178515812_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509list_get_x509_t_t413983797();
extern const RuntimeType unitytls_x509list_get_x509_t_t413983797_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509verify_default_ca_t_t671865852();
extern const RuntimeType unitytls_x509verify_default_ca_t_t671865852_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509verify_explicit_ca_t_t1629606714();
extern const RuntimeType unitytls_x509verify_explicit_ca_t_t1629606714_0_0_0;
extern "C" void unitytls_tlsctx_callbacks_t3626000702_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void unitytls_tlsctx_callbacks_t3626000702_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void unitytls_tlsctx_callbacks_t3626000702_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType unitytls_tlsctx_callbacks_t3626000702_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_certificate_callback_t2034592268();
extern const RuntimeType unitytls_tlsctx_certificate_callback_t2034592268_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_read_callback_t2806525115();
extern const RuntimeType unitytls_tlsctx_read_callback_t2806525115_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_trace_callback_t2576344559();
extern const RuntimeType unitytls_tlsctx_trace_callback_t2576344559_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_write_callback_t2121017372();
extern const RuntimeType unitytls_tlsctx_write_callback_t2121017372_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_tlsctx_x509verify_callback_t194548070();
extern const RuntimeType unitytls_tlsctx_x509verify_callback_t194548070_0_0_0;
extern "C" void DelegatePInvokeWrapper_unitytls_x509verify_callback_t321808703();
extern const RuntimeType unitytls_x509verify_callback_t321808703_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t893206259();
extern const RuntimeType ReadMethod_t893206259_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t2538911768();
extern const RuntimeType WriteMethod_t2538911768_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1975956110();
extern const RuntimeType UnmanagedReadOrWrite_t1975956110_0_0_0;
extern "C" void IOAsyncResult_t3640145766_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IOAsyncResult_t3640145766_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IOAsyncResult_t3640145766_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IOAsyncResult_t3640145766_0_0_0;
extern "C" void IOSelectorJob_t2199748873_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IOSelectorJob_t2199748873_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IOSelectorJob_t2199748873_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IOSelectorJob_t2199748873_0_0_0;
extern "C" void RecognizedAttribute_t632074220_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RecognizedAttribute_t632074220_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RecognizedAttribute_t632074220_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RecognizedAttribute_t632074220_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4266946825();
extern const RuntimeType ReadDelegate_t4266946825_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2016697242();
extern const RuntimeType WriteDelegate_t2016697242_0_0_0;
extern "C" void DelegatePInvokeWrapper_HeaderParser_t1081925738();
extern const RuntimeType HeaderParser_t1081925738_0_0_0;
extern "C" void HeaderVariantInfo_t1935685601_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HeaderVariantInfo_t1935685601_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HeaderVariantInfo_t1935685601_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HeaderVariantInfo_t1935685601_0_0_0;
extern "C" void AuthorizationState_t3141350760_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AuthorizationState_t3141350760_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AuthorizationState_t3141350760_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AuthorizationState_t3141350760_0_0_0;
extern "C" void IPv6AddressFormatter_t878312391_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IPv6AddressFormatter_t878312391_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IPv6AddressFormatter_t878312391_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IPv6AddressFormatter_t878312391_0_0_0;
extern "C" void Win32_FIXED_INFO_t1299345856_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_FIXED_INFO_t1299345856_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_FIXED_INFO_t1299345856_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_FIXED_INFO_t1299345856_0_0_0;
extern "C" void Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IP_ADDR_STRING_t1213417184_0_0_0;
extern "C" void SocketAsyncResult_t3523156467_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t3523156467_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t3523156467_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketAsyncResult_t3523156467_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t133602714_0_0_0;
extern "C" void LowerCaseMapping_t2910317575_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LowerCaseMapping_t2910317575_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LowerCaseMapping_t2910317575_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LowerCaseMapping_t2910317575_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3046754366_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const RuntimeType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const RuntimeType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1445031843_0_0_0;
extern "C" void OrderBlock_t1585977831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OrderBlock_t1585977831_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3829159415_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const RuntimeType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const RuntimeType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const RuntimeType UnityAction_t3245792599_0_0_0;
extern "C" void PlayerLoopSystem_t105772105_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayerLoopSystem_t105772105_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayerLoopSystem_t105772105_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayerLoopSystem_t105772105_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdateFunction_t377278577();
extern const RuntimeType UpdateFunction_t377278577_0_0_0;
extern "C" void PlayerLoopSystemInternal_t2185485283_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayerLoopSystemInternal_t2185485283_0_0_0;
extern "C" void CullResults_t76141518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullResults_t76141518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullResults_t76141518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullResults_t76141518_0_0_0;
extern "C" void SpriteBone_t303320096_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteBone_t303320096_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteBone_t303320096_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteBone_t303320096_0_0_0;
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t547604379_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3067099924_0_0_0;
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Internal_DrawTextureArguments_t1705718261_0_0_0;
extern "C" void LightBakingOutput_t989813037_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LightBakingOutput_t989813037_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LightBakingOutput_t989813037_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LightBakingOutput_t989813037_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t631007953_0_0_0;
extern "C" void PlayableBinding_t354260709_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t354260709_0_0_0;
extern "C" void DelegatePInvokeWrapper_CreateOutputMethod_t2301811773();
extern const RuntimeType CreateOutputMethod_t2301811773_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1369453676_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t3229609740_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t1199777556_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279();
extern const RuntimeType RequestAtlasCallback_t3100554279_0_0_0;
extern "C" void WorkRequest_t1354518612_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1354518612_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t1699091251_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t403091072_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const RuntimeType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const RuntimeType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const RuntimeType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_ConsumeSampleFramesNativeFunction_t1497769677();
extern const RuntimeType ConsumeSampleFramesNativeFunction_t1497769677_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const RuntimeType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t3211863866_0_0_0;
extern "C" void CertificateHandler_t2739891000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CertificateHandler_t2739891000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CertificateHandler_t2739891000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CertificateHandler_t2739891000_0_0_0;
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandler_t2937767557_0_0_0;
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerBuffer_t2928496527_0_0_0;
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequest_t463507806_0_0_0;
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequestAsyncOperation_t3852015985_0_0_0;
extern "C" void UploadHandler_t2993558019_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandler_t2993558019_0_0_0;
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandlerRaw_t25761545_0_0_0;
extern "C" void Subsystem_t89723475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Subsystem_t89723475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Subsystem_t89723475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Subsystem_t89723475_0_0_0;
extern "C" void SubsystemDescriptorBase_t2374447182_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SubsystemDescriptorBase_t2374447182_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SubsystemDescriptorBase_t2374447182_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SubsystemDescriptorBase_t2374447182_0_0_0;
extern "C" void FrameReceivedEventArgs_t2588080103_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FrameReceivedEventArgs_t2588080103_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FrameReceivedEventArgs_t2588080103_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FrameReceivedEventArgs_t2588080103_0_0_0;
extern "C" void PlaneAddedEventArgs_t2550175725_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlaneAddedEventArgs_t2550175725_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlaneAddedEventArgs_t2550175725_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlaneAddedEventArgs_t2550175725_0_0_0;
extern "C" void PlaneRemovedEventArgs_t1567129782_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlaneRemovedEventArgs_t1567129782_0_0_0;
extern "C" void PlaneUpdatedEventArgs_t349485851_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlaneUpdatedEventArgs_t349485851_0_0_0;
extern "C" void PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PointCloudUpdatedEventArgs_t3436657348_0_0_0;
extern "C" void SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SessionTrackingStateChangedEventArgs_t2343035655_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t1536042487_0_0_0;
extern "C" void AnimatorControllerParameter_t1758260042_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorControllerParameter_t1758260042_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorControllerParameter_t1758260042_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorControllerParameter_t1758260042_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnOverrideControllerDirtyCallback_t1307045488();
extern const RuntimeType OnOverrideControllerDirtyCallback_t1307045488_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t2465339518_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t4134054672_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t2719720026_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const RuntimeType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const RuntimeType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t1397964415_0_0_0;
extern "C" void EmitParams_t2216423628_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmitParams_t2216423628_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmitParams_t2216423628_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmitParams_t2216423628_0_0_0;
extern "C" void Collision2D_t2842956331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t2842956331_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t3805203441_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t4262080450_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t240592346_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const RuntimeType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void DelegatePInvokeWrapper_BasicResponseDelegate_t2196726690();
extern const RuntimeType BasicResponseDelegate_t2196726690_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t3163629820();
extern const RuntimeType SessionStateChanged_t3163629820_0_0_0;
extern "C" void CustomEventData_t317522481_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomEventData_t317522481_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomEventData_t317522481_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomEventData_t317522481_0_0_0;
extern "C" void UnityAnalyticsHandler_t3011359618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityAnalyticsHandler_t3011359618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityAnalyticsHandler_t3011359618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityAnalyticsHandler_t3011359618_0_0_0;
extern "C" void RemoteConfigSettings_t1247263429_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RemoteConfigSettings_t1247263429_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RemoteConfigSettings_t1247263429_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RemoteConfigSettings_t1247263429_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const RuntimeType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityPurchasingCallback_t953216184();
extern const RuntimeType UnityPurchasingCallback_t953216184_0_0_0;
extern "C" void ValueDropdownItem_t3457888829_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueDropdownItem_t3457888829_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueDropdownItem_t3457888829_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueDropdownItem_t3457888829_0_0_0;
extern "C" void PathStep_t1211216146_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PathStep_t1211216146_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PathStep_t1211216146_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PathStep_t1211216146_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3388716826();
extern const RuntimeType UnityNativePurchasingCallback_t3388716826_0_0_0;
extern "C" void ChannelPacket_t1579824718_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ChannelPacket_t1579824718_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ChannelPacket_t1579824718_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ChannelPacket_t1579824718_0_0_0;
extern "C" void InternalMsg_t2371755407_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InternalMsg_t2371755407_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InternalMsg_t2371755407_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InternalMsg_t2371755407_0_0_0;
extern "C" void NetworkBroadcastResult_t2174414888_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NetworkBroadcastResult_t2174414888_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NetworkBroadcastResult_t2174414888_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NetworkBroadcastResult_t2174414888_0_0_0;
extern "C" void PendingPlayer_t306375494_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PendingPlayer_t306375494_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PendingPlayer_t306375494_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PendingPlayer_t306375494_0_0_0;
extern "C" void ConnectionPendingPlayers_t878091664_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConnectionPendingPlayers_t878091664_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConnectionPendingPlayers_t878091664_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConnectionPendingPlayers_t878091664_0_0_0;
extern "C" void PendingPlayerInfo_t2391300657_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PendingPlayerInfo_t2391300657_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PendingPlayerInfo_t2391300657_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PendingPlayerInfo_t2391300657_0_0_0;
extern "C" void CRCMessageEntry_t1041239249_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CRCMessageEntry_t1041239249_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CRCMessageEntry_t1041239249_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CRCMessageEntry_t1041239249_0_0_0;
extern "C" void DelegatePInvokeWrapper_ClientMoveCallback2D_t270751497();
extern const RuntimeType ClientMoveCallback2D_t270751497_0_0_0;
extern "C" void DelegatePInvokeWrapper_ClientMoveCallback3D_t1836835438();
extern const RuntimeType ClientMoveCallback3D_t1836835438_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const RuntimeType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t3049316579_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRayIntersectionAllCallback_t3913627115();
extern const RuntimeType GetRayIntersectionAllCallback_t3913627115_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRayIntersectionAllNonAllocCallback_t2311174851();
extern const RuntimeType GetRayIntersectionAllNonAllocCallback_t2311174851_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRaycastNonAllocCallback_t3841783507();
extern const RuntimeType GetRaycastNonAllocCallback_t3841783507_0_0_0;
extern "C" void DelegatePInvokeWrapper_Raycast2DCallback_t768590915();
extern const RuntimeType Raycast2DCallback_t768590915_0_0_0;
extern "C" void DelegatePInvokeWrapper_Raycast3DCallback_t701940803();
extern const RuntimeType Raycast3DCallback_t701940803_0_0_0;
extern "C" void DelegatePInvokeWrapper_RaycastAllCallback_t1884415901();
extern const RuntimeType RaycastAllCallback_t1884415901_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t1362986479_0_0_0;
extern "C" void FormatterInfo_t2891647851_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FormatterInfo_t2891647851_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FormatterInfo_t2891647851_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FormatterInfo_t2891647851_0_0_0;
extern "C" void FormatterLocatorInfo_t371893538_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FormatterLocatorInfo_t371893538_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FormatterLocatorInfo_t371893538_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FormatterLocatorInfo_t371893538_0_0_0;
extern "C" void NodeInfo_t1254526976_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NodeInfo_t1254526976_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NodeInfo_t1254526976_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NodeInfo_t1254526976_0_0_0;
extern "C" void SerializationData_t3163410965_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationData_t3163410965_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationData_t3163410965_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationData_t3163410965_0_0_0;
extern "C" void SerializationNode_t389653619_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationNode_t389653619_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationNode_t389653619_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationNode_t389653619_0_0_0;
extern "C" void ShadowData_t3587884416_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ShadowData_t3587884416_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ShadowData_t3587884416_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ShadowData_t3587884416_0_0_0;
extern "C" void Frustum_t2039891733_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Frustum_t2039891733_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Frustum_t2039891733_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Frustum_t2039891733_0_0_0;
extern "C" void CopyOperation_t3389193622_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CopyOperation_t3389193622_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CopyOperation_t3389193622_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CopyOperation_t3389193622_0_0_0;
extern "C" void ProfilingSample_t2938638080_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProfilingSample_t2938638080_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProfilingSample_t2938638080_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProfilingSample_t2938638080_0_0_0;
extern "C" void DelegatePInvokeWrapper_ScaleFunc_t890021287();
extern const RuntimeType ScaleFunc_t890021287_0_0_0;
extern "C" void AtlasInit_t852154556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AtlasInit_t852154556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AtlasInit_t852154556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AtlasInit_t852154556_0_0_0;
extern "C" void CtxtInit_t1321886867_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CtxtInit_t1321886867_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CtxtInit_t1321886867_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CtxtInit_t1321886867_0_0_0;
extern "C" void Dels_t361700601_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Dels_t361700601_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Dels_t361700601_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Dels_t361700601_0_0_0;
extern "C" void Entry_t1827551715_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Entry_t1827551715_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Entry_t1827551715_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Entry_t1827551715_0_0_0;
extern "C" void Override_t1415021437_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Override_t1415021437_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Override_t1415021437_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Override_t1415021437_0_0_0;
extern "C" void DelegatePInvokeWrapper_unityAdsDidError_t1993223595();
extern const RuntimeType unityAdsDidError_t1993223595_0_0_0;
extern "C" void DelegatePInvokeWrapper_unityAdsDidFinish_t3747416149();
extern const RuntimeType unityAdsDidFinish_t3747416149_0_0_0;
extern "C" void DelegatePInvokeWrapper_unityAdsDidStart_t1058412932();
extern const RuntimeType unityAdsDidStart_t1058412932_0_0_0;
extern "C" void DelegatePInvokeWrapper_unityAdsReady_t96934738();
extern const RuntimeType unityAdsReady_t96934738_0_0_0;
extern "C" void OptOutResponse_t2266495071_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OptOutResponse_t2266495071_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OptOutResponse_t2266495071_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OptOutResponse_t2266495071_0_0_0;
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OptOutStatus_t3541615854_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OptOutStatus_t3541615854_0_0_0;
extern "C" void RequestData_t3101179086_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RequestData_t3101179086_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RequestData_t3101179086_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RequestData_t3101179086_0_0_0;
extern "C" void TokenData_t2077495713_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TokenData_t2077495713_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TokenData_t2077495713_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TokenData_t2077495713_0_0_0;
extern "C" void UserPostData_t478425489_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UserPostData_t478425489_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UserPostData_t478425489_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UserPostData_t478425489_0_0_0;
extern "C" void CameraData_t2373231130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraData_t2373231130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraData_t2373231130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CameraData_t2373231130_0_0_0;
extern "C" void LightData_t718835846_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LightData_t718835846_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LightData_t718835846_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LightData_t718835846_0_0_0;
extern "C" void RenderingData_t2420043686_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RenderingData_t2420043686_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RenderingData_t2420043686_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RenderingData_t2420043686_0_0_0;
extern "C" void ShadowData_t294369652_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ShadowData_t294369652_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ShadowData_t294369652_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ShadowData_t294369652_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnTrigger_t4184125570();
extern const RuntimeType OnTrigger_t4184125570_0_0_0;
extern "C" void SpawningPoolEntry_t2116637239_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpawningPoolEntry_t2116637239_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpawningPoolEntry_t2116637239_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpawningPoolEntry_t2116637239_0_0_0;
extern "C" void ShownTouch_t1740607014_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ShownTouch_t1740607014_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ShownTouch_t1740607014_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ShownTouch_t1740607014_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackMainThreadDelegate_t469493312();
extern const RuntimeType CallbackMainThreadDelegate_t469493312_0_0_0;
extern "C" void GestureTouch_t1992402133_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GestureTouch_t1992402133_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GestureTouch_t1992402133_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GestureTouch_t1992402133_0_0_0;
extern "C" void ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ImageGestureRecognizerComponentScriptImageEntry_t2713275279_0_0_0;
extern "C" void LoadScoreRequest_t1342227622_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LoadScoreRequest_t1342227622_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LoadScoreRequest_t1342227622_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LoadScoreRequest_t1342227622_0_0_0;
extern "C" void GiphyUploadParams_t2791732282_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GiphyUploadParams_t2791732282_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GiphyUploadParams_t2791732282_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GiphyUploadParams_t2791732282_0_0_0;
extern "C" void DelegatePInvokeWrapper_DeleteSavedGameCallback_t2032035250();
extern const RuntimeType DeleteSavedGameCallback_t2032035250_0_0_0;
extern "C" void DelegatePInvokeWrapper_FetchSavedGamesCallback_t3515465237();
extern const RuntimeType FetchSavedGamesCallback_t3515465237_0_0_0;
extern "C" void DelegatePInvokeWrapper_LoadSavedGameDataCallback_t1958165752();
extern const RuntimeType LoadSavedGameDataCallback_t1958165752_0_0_0;
extern "C" void DelegatePInvokeWrapper_OpenSavedGameCallback_t2349091444();
extern const RuntimeType OpenSavedGameCallback_t2349091444_0_0_0;
extern "C" void DelegatePInvokeWrapper_ResolveConflictingSavedGamesCallback_t4045204290();
extern const RuntimeType ResolveConflictingSavedGamesCallback_t4045204290_0_0_0;
extern "C" void DelegatePInvokeWrapper_SaveGameDataCallback_t3612996042();
extern const RuntimeType SaveGameDataCallback_t3612996042_0_0_0;
extern "C" void DelegatePInvokeWrapper_GifExportCompletedDelegate_t4082577070();
extern const RuntimeType GifExportCompletedDelegate_t4082577070_0_0_0;
extern "C" void DelegatePInvokeWrapper_GifExportProgressDelegate_t324897231();
extern const RuntimeType GifExportProgressDelegate_t324897231_0_0_0;
extern "C" void DelegatePInvokeWrapper_NativeNotificationHandler_t1892928298();
extern const RuntimeType NativeNotificationHandler_t1892928298_0_0_0;
extern "C" void iOSNotificationAction_t1537218362_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void iOSNotificationAction_t1537218362_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void iOSNotificationAction_t1537218362_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType iOSNotificationAction_t1537218362_0_0_0;
extern "C" void iOSNotificationCategory_t4187379170_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void iOSNotificationCategory_t4187379170_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void iOSNotificationCategory_t4187379170_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType iOSNotificationCategory_t4187379170_0_0_0;
extern "C" void iOSNotificationContent_t1040386259_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void iOSNotificationContent_t1040386259_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void iOSNotificationContent_t1040386259_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType iOSNotificationContent_t1040386259_0_0_0;
extern "C" void iOSNotificationListenerInfo_t996772639_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void iOSNotificationListenerInfo_t996772639_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void iOSNotificationListenerInfo_t996772639_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType iOSNotificationListenerInfo_t996772639_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetNotificationResponseCallback_t3346532913();
extern const RuntimeType GetNotificationResponseCallback_t3346532913_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetPendingNotificationRequestsCallback_t3407937767();
extern const RuntimeType GetPendingNotificationRequestsCallback_t3407937767_0_0_0;
extern "C" void IconName_t2443857936_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IconName_t2443857936_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IconName_t2443857936_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IconName_t2443857936_0_0_0;
extern "C" void iOSConsentDialogListenerInfo_t124266453_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void iOSConsentDialogListenerInfo_t124266453_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void iOSConsentDialogListenerInfo_t124266453_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType iOSConsentDialogListenerInfo_t124266453_0_0_0;
extern "C" void ShareData_t888004449_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ShareData_t888004449_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ShareData_t888004449_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ShareData_t888004449_0_0_0;
extern "C" void ActionButton_t554814726_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ActionButton_t554814726_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ActionButton_t554814726_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ActionButton_t554814726_0_0_0;
extern "C" void SavedGameInfoUpdate_t1021132738_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SavedGameInfoUpdate_t1021132738_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SavedGameInfoUpdate_t1021132738_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SavedGameInfoUpdate_t1021132738_0_0_0;
extern "C" void Builder_t2048762101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Builder_t2048762101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Builder_t2048762101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Builder_t2048762101_0_0_0;
extern "C" void AdvertisingResult_t1229207569_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AdvertisingResult_t1229207569_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AdvertisingResult_t1229207569_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AdvertisingResult_t1229207569_0_0_0;
extern "C" void ConnectionRequest_t684574500_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConnectionRequest_t684574500_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConnectionRequest_t684574500_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConnectionRequest_t684574500_0_0_0;
extern "C" void ConnectionResponse_t735328601_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConnectionResponse_t735328601_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConnectionResponse_t735328601_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConnectionResponse_t735328601_0_0_0;
extern "C" void EndpointDetails_t3891698496_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EndpointDetails_t3891698496_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EndpointDetails_t3891698496_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EndpointDetails_t3891698496_0_0_0;
extern "C" void NearbyConnectionConfiguration_t2019425596_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NearbyConnectionConfiguration_t2019425596_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NearbyConnectionConfiguration_t2019425596_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NearbyConnectionConfiguration_t2019425596_0_0_0;
extern "C" void SavedGameMetadataUpdate_t1775293339_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SavedGameMetadataUpdate_t1775293339_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SavedGameMetadataUpdate_t1775293339_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SavedGameMetadataUpdate_t1775293339_0_0_0;
extern "C" void Builder_t140438593_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Builder_t140438593_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Builder_t140438593_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Builder_t140438593_0_0_0;
extern "C" void PoolPrefabs_t1376732323_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PoolPrefabs_t1376732323_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PoolPrefabs_t1376732323_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PoolPrefabs_t1376732323_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[334] = 
{
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ NULL, RuntimeClassHandle_t4196268768_marshal_pinvoke, RuntimeClassHandle_t4196268768_marshal_pinvoke_back, RuntimeClassHandle_t4196268768_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeClassHandle_t4196268768_0_0_0 } /* Mono.RuntimeClassHandle */,
	{ NULL, RuntimeGPtrArrayHandle_t2690631865_marshal_pinvoke, RuntimeGPtrArrayHandle_t2690631865_marshal_pinvoke_back, RuntimeGPtrArrayHandle_t2690631865_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeGPtrArrayHandle_t2690631865_0_0_0 } /* Mono.RuntimeGPtrArrayHandle */,
	{ NULL, RuntimeGenericParamInfoHandle_t3649234556_marshal_pinvoke, RuntimeGenericParamInfoHandle_t3649234556_marshal_pinvoke_back, RuntimeGenericParamInfoHandle_t3649234556_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeGenericParamInfoHandle_t3649234556_0_0_0 } /* Mono.RuntimeGenericParamInfoHandle */,
	{ NULL, RuntimeRemoteClassHandle_t2479637696_marshal_pinvoke, RuntimeRemoteClassHandle_t2479637696_marshal_pinvoke_back, RuntimeRemoteClassHandle_t2479637696_marshal_pinvoke_cleanup, NULL, NULL, &RuntimeRemoteClassHandle_t2479637696_0_0_0 } /* Mono.RuntimeRemoteClassHandle */,
	{ NULL, GPtrArray_t1503373745_marshal_pinvoke, GPtrArray_t1503373745_marshal_pinvoke_back, GPtrArray_t1503373745_marshal_pinvoke_cleanup, NULL, NULL, &GPtrArray_t1503373745_0_0_0 } /* Mono.RuntimeStructs/GPtrArray */,
	{ NULL, GenericParamInfo_t1825140006_marshal_pinvoke, GenericParamInfo_t1825140006_marshal_pinvoke_back, GenericParamInfo_t1825140006_marshal_pinvoke_cleanup, NULL, NULL, &GenericParamInfo_t1825140006_0_0_0 } /* Mono.RuntimeStructs/GenericParamInfo */,
	{ NULL, RemoteClass_t4006407480_marshal_pinvoke, RemoteClass_t4006407480_marshal_pinvoke_back, RemoteClass_t4006407480_marshal_pinvoke_cleanup, NULL, NULL, &RemoteClass_t4006407480_0_0_0 } /* Mono.RuntimeStructs/RemoteClass */,
	{ NULL, SafeGPtrArrayHandle_t2768407725_marshal_pinvoke, SafeGPtrArrayHandle_t2768407725_marshal_pinvoke_back, SafeGPtrArrayHandle_t2768407725_marshal_pinvoke_cleanup, NULL, NULL, &SafeGPtrArrayHandle_t2768407725_0_0_0 } /* Mono.SafeGPtrArrayHandle */,
	{ NULL, SafeStringMarshal_t1394540657_marshal_pinvoke, SafeStringMarshal_t1394540657_marshal_pinvoke_back, SafeStringMarshal_t1394540657_marshal_pinvoke_cleanup, NULL, NULL, &SafeStringMarshal_t1394540657_0_0_0 } /* Mono.SafeStringMarshal */,
	{ NULL, UriScheme_t2867806342_marshal_pinvoke, UriScheme_t2867806342_marshal_pinvoke_back, UriScheme_t2867806342_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t2867806342_0_0_0 } /* Mono.Security.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ NULL, AppDomain_t1571427825_marshal_pinvoke, AppDomain_t1571427825_marshal_pinvoke_back, AppDomain_t1571427825_marshal_pinvoke_cleanup, NULL, NULL, &AppDomain_t1571427825_0_0_0 } /* System.AppDomain */,
	{ NULL, AppDomainSetup_t123196401_marshal_pinvoke, AppDomainSetup_t123196401_marshal_pinvoke_back, AppDomainSetup_t123196401_marshal_pinvoke_cleanup, NULL, NULL, &AppDomainSetup_t123196401_0_0_0 } /* System.AppDomainSetup */,
	{ NULL, SorterGenericArray_t3557937876_marshal_pinvoke, SorterGenericArray_t3557937876_marshal_pinvoke_back, SorterGenericArray_t3557937876_marshal_pinvoke_cleanup, NULL, NULL, &SorterGenericArray_t3557937876_0_0_0 } /* System.Array/SorterGenericArray */,
	{ NULL, SorterObjectArray_t2043158284_marshal_pinvoke, SorterObjectArray_t2043158284_marshal_pinvoke_back, SorterObjectArray_t2043158284_marshal_pinvoke_cleanup, NULL, NULL, &SorterObjectArray_t2043158284_0_0_0 } /* System.Array/SorterObjectArray */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, bucket_t758131704_marshal_pinvoke, bucket_t758131704_marshal_pinvoke_back, bucket_t758131704_marshal_pinvoke_cleanup, NULL, NULL, &bucket_t758131704_0_0_0 } /* System.Collections.Hashtable/bucket */,
	{ DelegatePInvokeWrapper_InternalCancelHandler_t872516951, NULL, NULL, NULL, NULL, NULL, &InternalCancelHandler_t872516951_0_0_0 } /* System.Console/InternalCancelHandler */,
	{ DelegatePInvokeWrapper_WindowsCancelHandler_t1006184794, NULL, NULL, NULL, NULL, NULL, &WindowsCancelHandler_t1006184794_0_0_0 } /* System.Console/WindowsConsole/WindowsCancelHandler */,
	{ NULL, ConsoleKeyInfo_t1802691652_marshal_pinvoke, ConsoleKeyInfo_t1802691652_marshal_pinvoke_back, ConsoleKeyInfo_t1802691652_marshal_pinvoke_cleanup, NULL, NULL, &ConsoleKeyInfo_t1802691652_0_0_0 } /* System.ConsoleKeyInfo */,
	{ NULL, DTSubString_t967826655_marshal_pinvoke, DTSubString_t967826655_marshal_pinvoke_back, DTSubString_t967826655_marshal_pinvoke_cleanup, NULL, NULL, &DTSubString_t967826655_0_0_0 } /* System.DTSubString */,
	{ NULL, DateTimeRawInfo_t1172279960_marshal_pinvoke, DateTimeRawInfo_t1172279960_marshal_pinvoke_back, DateTimeRawInfo_t1172279960_marshal_pinvoke_cleanup, NULL, NULL, &DateTimeRawInfo_t1172279960_0_0_0 } /* System.DateTimeRawInfo */,
	{ NULL, DateTimeResult_t2747711438_marshal_pinvoke, DateTimeResult_t2747711438_marshal_pinvoke_back, DateTimeResult_t2747711438_marshal_pinvoke_cleanup, NULL, NULL, &DateTimeResult_t2747711438_0_0_0 } /* System.DateTimeResult */,
	{ NULL, Delegate_t1188392813_marshal_pinvoke, Delegate_t1188392813_marshal_pinvoke_back, Delegate_t1188392813_marshal_pinvoke_cleanup, NULL, NULL, &Delegate_t1188392813_0_0_0 } /* System.Delegate */,
	{ NULL, StackFrame_t3217253059_marshal_pinvoke, StackFrame_t3217253059_marshal_pinvoke_back, StackFrame_t3217253059_marshal_pinvoke_cleanup, NULL, NULL, &StackFrame_t3217253059_0_0_0 } /* System.Diagnostics.StackFrame */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ NULL, EnumResult_t2373072967_marshal_pinvoke, EnumResult_t2373072967_marshal_pinvoke_back, EnumResult_t2373072967_marshal_pinvoke_cleanup, NULL, NULL, &EnumResult_t2373072967_0_0_0 } /* System.Enum/EnumResult */,
	{ NULL, Exception_t_marshal_pinvoke, Exception_t_marshal_pinvoke_back, Exception_t_marshal_pinvoke_cleanup, NULL, NULL, &Exception_t_0_0_0 } /* System.Exception */,
	{ NULL, CalendarData_t473118650_marshal_pinvoke, CalendarData_t473118650_marshal_pinvoke_back, CalendarData_t473118650_marshal_pinvoke_cleanup, NULL, NULL, &CalendarData_t473118650_0_0_0 } /* System.Globalization.CalendarData */,
	{ NULL, CultureData_t1899656083_marshal_pinvoke, CultureData_t1899656083_marshal_pinvoke_back, CultureData_t1899656083_marshal_pinvoke_cleanup, NULL, NULL, &CultureData_t1899656083_0_0_0 } /* System.Globalization.CultureData */,
	{ NULL, CultureInfo_t4157843068_marshal_pinvoke, CultureInfo_t4157843068_marshal_pinvoke_back, CultureInfo_t4157843068_marshal_pinvoke_cleanup, NULL, NULL, &CultureInfo_t4157843068_0_0_0 } /* System.Globalization.CultureInfo */,
	{ NULL, Data_t191216285_marshal_pinvoke, Data_t191216285_marshal_pinvoke_back, Data_t191216285_marshal_pinvoke_cleanup, NULL, NULL, &Data_t191216285_0_0_0 } /* System.Globalization.CultureInfo/Data */,
	{ NULL, InternalCodePageDataItem_t2575532933_marshal_pinvoke, InternalCodePageDataItem_t2575532933_marshal_pinvoke_back, InternalCodePageDataItem_t2575532933_marshal_pinvoke_cleanup, NULL, NULL, &InternalCodePageDataItem_t2575532933_0_0_0 } /* System.Globalization.InternalCodePageDataItem */,
	{ NULL, InternalEncodingDataItem_t3158859817_marshal_pinvoke, InternalEncodingDataItem_t3158859817_marshal_pinvoke_back, InternalEncodingDataItem_t3158859817_marshal_pinvoke_cleanup, NULL, NULL, &InternalEncodingDataItem_t3158859817_0_0_0 } /* System.Globalization.InternalEncodingDataItem */,
	{ NULL, RegionInfo_t1090270226_marshal_pinvoke, RegionInfo_t1090270226_marshal_pinvoke_back, RegionInfo_t1090270226_marshal_pinvoke_cleanup, NULL, NULL, &RegionInfo_t1090270226_0_0_0 } /* System.Globalization.RegionInfo */,
	{ NULL, SortKey_t3955336732_marshal_pinvoke, SortKey_t3955336732_marshal_pinvoke_back, SortKey_t3955336732_marshal_pinvoke_cleanup, NULL, NULL, &SortKey_t3955336732_0_0_0 } /* System.Globalization.SortKey */,
	{ NULL, FormatLiterals_t1376140638_marshal_pinvoke, FormatLiterals_t1376140638_marshal_pinvoke_back, FormatLiterals_t1376140638_marshal_pinvoke_cleanup, NULL, NULL, &FormatLiterals_t1376140638_0_0_0 } /* System.Globalization.TimeSpanFormat/FormatLiterals */,
	{ NULL, GuidResult_t1584437374_marshal_pinvoke, GuidResult_t1584437374_marshal_pinvoke_back, GuidResult_t1584437374_marshal_pinvoke_cleanup, NULL, NULL, &GuidResult_t1584437374_0_0_0 } /* System.Guid/GuidResult */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, ReadWriteParameters_t1050632132_marshal_pinvoke, ReadWriteParameters_t1050632132_marshal_pinvoke_back, ReadWriteParameters_t1050632132_marshal_pinvoke_cleanup, NULL, NULL, &ReadWriteParameters_t1050632132_0_0_0 } /* System.IO.Stream/ReadWriteParameters */,
	{ NULL, InputRecord_t2660212290_marshal_pinvoke, InputRecord_t2660212290_marshal_pinvoke_back, InputRecord_t2660212290_marshal_pinvoke_cleanup, NULL, NULL, &InputRecord_t2660212290_0_0_0 } /* System.InputRecord */,
	{ NULL, MarshalByRefObject_t2760389100_marshal_pinvoke, MarshalByRefObject_t2760389100_marshal_pinvoke_back, MarshalByRefObject_t2760389100_marshal_pinvoke_cleanup, NULL, NULL, &MarshalByRefObject_t2760389100_0_0_0 } /* System.MarshalByRefObject */,
	{ NULL, MonoAsyncCall_t3023670838_marshal_pinvoke, MonoAsyncCall_t3023670838_marshal_pinvoke_back, MonoAsyncCall_t3023670838_marshal_pinvoke_cleanup, NULL, NULL, &MonoAsyncCall_t3023670838_0_0_0 } /* System.MonoAsyncCall */,
	{ NULL, MonoTypeInfo_t3366989025_marshal_pinvoke, MonoTypeInfo_t3366989025_marshal_pinvoke_back, MonoTypeInfo_t3366989025_marshal_pinvoke_cleanup, NULL, NULL, &MonoTypeInfo_t3366989025_0_0_0 } /* System.MonoTypeInfo */,
	{ NULL, MulticastDelegate_t_marshal_pinvoke, MulticastDelegate_t_marshal_pinvoke_back, MulticastDelegate_t_marshal_pinvoke_cleanup, NULL, NULL, &MulticastDelegate_t_0_0_0 } /* System.MulticastDelegate */,
	{ NULL, NumberBuffer_t2021063877_marshal_pinvoke, NumberBuffer_t2021063877_marshal_pinvoke_back, NumberBuffer_t2021063877_marshal_pinvoke_cleanup, NULL, NULL, &NumberBuffer_t2021063877_0_0_0 } /* System.Number/NumberBuffer */,
	{ NULL, FormatParam_t4194474082_marshal_pinvoke, FormatParam_t4194474082_marshal_pinvoke_back, FormatParam_t4194474082_marshal_pinvoke_cleanup, NULL, NULL, &FormatParam_t4194474082_0_0_0 } /* System.ParameterizedStrings/FormatParam */,
	{ NULL, ParamsArray_t4224664136_marshal_pinvoke, ParamsArray_t4224664136_marshal_pinvoke_back, ParamsArray_t4224664136_marshal_pinvoke_cleanup, NULL, NULL, &ParamsArray_t4224664136_0_0_0 } /* System.ParamsArray */,
	{ NULL, ParsingInfo_t1001632991_marshal_pinvoke, ParsingInfo_t1001632991_marshal_pinvoke_back, ParsingInfo_t1001632991_marshal_pinvoke_cleanup, NULL, NULL, &ParsingInfo_t1001632991_0_0_0 } /* System.ParsingInfo */,
	{ NULL, Assembly_t_marshal_pinvoke, Assembly_t_marshal_pinvoke_back, Assembly_t_marshal_pinvoke_cleanup, NULL, NULL, &Assembly_t_0_0_0 } /* System.Reflection.Assembly */,
	{ NULL, AssemblyName_t270931938_marshal_pinvoke, AssemblyName_t270931938_marshal_pinvoke_back, AssemblyName_t270931938_marshal_pinvoke_cleanup, NULL, NULL, &AssemblyName_t270931938_0_0_0 } /* System.Reflection.AssemblyName */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, LocalBuilder_t3562264111_marshal_pinvoke, LocalBuilder_t3562264111_marshal_pinvoke_back, LocalBuilder_t3562264111_marshal_pinvoke_cleanup, NULL, NULL, &LocalBuilder_t3562264111_0_0_0 } /* System.Reflection.Emit.LocalBuilder */,
	{ NULL, ExceptionHandlingClause_t2620275417_marshal_pinvoke, ExceptionHandlingClause_t2620275417_marshal_pinvoke_back, ExceptionHandlingClause_t2620275417_marshal_pinvoke_cleanup, NULL, NULL, &ExceptionHandlingClause_t2620275417_0_0_0 } /* System.Reflection.ExceptionHandlingClause */,
	{ NULL, InterfaceMapping_t1267958657_marshal_pinvoke, InterfaceMapping_t1267958657_marshal_pinvoke_back, InterfaceMapping_t1267958657_marshal_pinvoke_cleanup, NULL, NULL, &InterfaceMapping_t1267958657_0_0_0 } /* System.Reflection.InterfaceMapping */,
	{ NULL, LocalVariableInfo_t2426779395_marshal_pinvoke, LocalVariableInfo_t2426779395_marshal_pinvoke_back, LocalVariableInfo_t2426779395_marshal_pinvoke_cleanup, NULL, NULL, &LocalVariableInfo_t2426779395_0_0_0 } /* System.Reflection.LocalVariableInfo */,
	{ NULL, MethodBody_t3098043463_marshal_pinvoke, MethodBody_t3098043463_marshal_pinvoke_back, MethodBody_t3098043463_marshal_pinvoke_cleanup, NULL, NULL, &MethodBody_t3098043463_0_0_0 } /* System.Reflection.MethodBody */,
	{ NULL, Module_t2987026101_marshal_pinvoke, Module_t2987026101_marshal_pinvoke_back, Module_t2987026101_marshal_pinvoke_cleanup, NULL, NULL, &Module_t2987026101_0_0_0 } /* System.Reflection.Module */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterInfo_t1861056598_marshal_pinvoke, ParameterInfo_t1861056598_marshal_pinvoke_back, ParameterInfo_t1861056598_marshal_pinvoke_cleanup, NULL, NULL, &ParameterInfo_t1861056598_0_0_0 } /* System.Reflection.ParameterInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceLocator_t3723970807_marshal_pinvoke, ResourceLocator_t3723970807_marshal_pinvoke_back, ResourceLocator_t3723970807_marshal_pinvoke_cleanup, NULL, NULL, &ResourceLocator_t3723970807_0_0_0 } /* System.Resources.ResourceLocator */,
	{ NULL, AsyncMethodBuilderCore_t2955600131_marshal_pinvoke, AsyncMethodBuilderCore_t2955600131_marshal_pinvoke_back, AsyncMethodBuilderCore_t2955600131_marshal_pinvoke_cleanup, NULL, NULL, &AsyncMethodBuilderCore_t2955600131_0_0_0 } /* System.Runtime.CompilerServices.AsyncMethodBuilderCore */,
	{ NULL, AsyncTaskMethodBuilder_t3536885450_marshal_pinvoke, AsyncTaskMethodBuilder_t3536885450_marshal_pinvoke_back, AsyncTaskMethodBuilder_t3536885450_marshal_pinvoke_cleanup, NULL, NULL, &AsyncTaskMethodBuilder_t3536885450_0_0_0 } /* System.Runtime.CompilerServices.AsyncTaskMethodBuilder */,
	{ NULL, ConfiguredTaskAwaitable_t166429847_marshal_pinvoke, ConfiguredTaskAwaitable_t166429847_marshal_pinvoke_back, ConfiguredTaskAwaitable_t166429847_marshal_pinvoke_cleanup, NULL, NULL, &ConfiguredTaskAwaitable_t166429847_0_0_0 } /* System.Runtime.CompilerServices.ConfiguredTaskAwaitable */,
	{ NULL, ConfiguredTaskAwaiter_t555647845_marshal_pinvoke, ConfiguredTaskAwaiter_t555647845_marshal_pinvoke_back, ConfiguredTaskAwaiter_t555647845_marshal_pinvoke_cleanup, NULL, NULL, &ConfiguredTaskAwaiter_t555647845_0_0_0 } /* System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter */,
	{ NULL, Ephemeron_t1602596362_marshal_pinvoke, Ephemeron_t1602596362_marshal_pinvoke_back, Ephemeron_t1602596362_marshal_pinvoke_cleanup, NULL, NULL, &Ephemeron_t1602596362_0_0_0 } /* System.Runtime.CompilerServices.Ephemeron */,
	{ NULL, TaskAwaiter_t919683548_marshal_pinvoke, TaskAwaiter_t919683548_marshal_pinvoke_back, TaskAwaiter_t919683548_marshal_pinvoke_cleanup, NULL, NULL, &TaskAwaiter_t919683548_0_0_0 } /* System.Runtime.CompilerServices.TaskAwaiter */,
	{ NULL, ProcessMessageRes_t3710547145_marshal_pinvoke, ProcessMessageRes_t3710547145_marshal_pinvoke_back, ProcessMessageRes_t3710547145_marshal_pinvoke_cleanup, NULL, NULL, &ProcessMessageRes_t3710547145_0_0_0 } /* System.Runtime.Remoting.Channels.CrossAppDomainSink/ProcessMessageRes */,
	{ NULL, Context_t3285446944_marshal_pinvoke, Context_t3285446944_marshal_pinvoke_back, Context_t3285446944_marshal_pinvoke_cleanup, NULL, NULL, &Context_t3285446944_0_0_0 } /* System.Runtime.Remoting.Contexts.Context */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ NULL, AsyncResult_t4194309572_marshal_pinvoke, AsyncResult_t4194309572_marshal_pinvoke_back, AsyncResult_t4194309572_marshal_pinvoke_cleanup, NULL, NULL, &AsyncResult_t4194309572_0_0_0 } /* System.Runtime.Remoting.Messaging.AsyncResult */,
	{ NULL, Reader_t983665205_marshal_pinvoke, Reader_t983665205_marshal_pinvoke_back, Reader_t983665205_marshal_pinvoke_cleanup, NULL, NULL, &Reader_t983665205_0_0_0 } /* System.Runtime.Remoting.Messaging.LogicalCallContext/Reader */,
	{ NULL, MonoMethodMessage_t2807636944_marshal_pinvoke, MonoMethodMessage_t2807636944_marshal_pinvoke_back, MonoMethodMessage_t2807636944_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodMessage_t2807636944_0_0_0 } /* System.Runtime.Remoting.Messaging.MonoMethodMessage */,
	{ NULL, RealProxy_t2312050253_marshal_pinvoke, RealProxy_t2312050253_marshal_pinvoke_back, RealProxy_t2312050253_marshal_pinvoke_cleanup, NULL, NULL, &RealProxy_t2312050253_0_0_0 } /* System.Runtime.Remoting.Proxies.RealProxy */,
	{ NULL, TransparentProxy_t431418284_marshal_pinvoke, TransparentProxy_t431418284_marshal_pinvoke_back, TransparentProxy_t431418284_marshal_pinvoke_cleanup, NULL, NULL, &TransparentProxy_t431418284_0_0_0 } /* System.Runtime.Remoting.Proxies.TransparentProxy */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ DelegatePInvokeWrapper_SerializationEventHandler_t2446424469, NULL, NULL, NULL, NULL, NULL, &SerializationEventHandler_t2446424469_0_0_0 } /* System.Runtime.Serialization.SerializationEventHandler */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, HashAlgorithmName_t3637476002_marshal_pinvoke, HashAlgorithmName_t3637476002_marshal_pinvoke_back, HashAlgorithmName_t3637476002_marshal_pinvoke_cleanup, NULL, NULL, &HashAlgorithmName_t3637476002_0_0_0 } /* System.Security.Cryptography.HashAlgorithmName */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, CancellationCallbackCoreWorkArguments_t3786748137_marshal_pinvoke, CancellationCallbackCoreWorkArguments_t3786748137_marshal_pinvoke_back, CancellationCallbackCoreWorkArguments_t3786748137_marshal_pinvoke_cleanup, NULL, NULL, &CancellationCallbackCoreWorkArguments_t3786748137_0_0_0 } /* System.Threading.CancellationCallbackCoreWorkArguments */,
	{ NULL, CancellationToken_t784455623_marshal_pinvoke, CancellationToken_t784455623_marshal_pinvoke_back, CancellationToken_t784455623_marshal_pinvoke_cleanup, NULL, NULL, &CancellationToken_t784455623_0_0_0 } /* System.Threading.CancellationToken */,
	{ NULL, CancellationTokenRegistration_t2813424904_marshal_pinvoke, CancellationTokenRegistration_t2813424904_marshal_pinvoke_back, CancellationTokenRegistration_t2813424904_marshal_pinvoke_cleanup, NULL, NULL, &CancellationTokenRegistration_t2813424904_0_0_0 } /* System.Threading.CancellationTokenRegistration */,
	{ NULL, Reader_t1133934476_marshal_pinvoke, Reader_t1133934476_marshal_pinvoke_back, Reader_t1133934476_marshal_pinvoke_cleanup, NULL, NULL, &Reader_t1133934476_0_0_0 } /* System.Threading.ExecutionContext/Reader */,
	{ NULL, ExecutionContextSwitcher_t2326006776_marshal_pinvoke, ExecutionContextSwitcher_t2326006776_marshal_pinvoke_back, ExecutionContextSwitcher_t2326006776_marshal_pinvoke_cleanup, NULL, NULL, &ExecutionContextSwitcher_t2326006776_0_0_0 } /* System.Threading.ExecutionContextSwitcher */,
	{ NULL, SpinLock_t508764825_marshal_pinvoke, SpinLock_t508764825_marshal_pinvoke_back, SpinLock_t508764825_marshal_pinvoke_cleanup, NULL, NULL, &SpinLock_t508764825_0_0_0 } /* System.Threading.SpinLock */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, WaitHandle_t1743403487_marshal_pinvoke, WaitHandle_t1743403487_marshal_pinvoke_back, WaitHandle_t1743403487_marshal_pinvoke_cleanup, NULL, NULL, &WaitHandle_t1743403487_0_0_0 } /* System.Threading.WaitHandle */,
	{ NULL, DYNAMIC_TIME_ZONE_INFORMATION_t933351445_marshal_pinvoke, DYNAMIC_TIME_ZONE_INFORMATION_t933351445_marshal_pinvoke_back, DYNAMIC_TIME_ZONE_INFORMATION_t933351445_marshal_pinvoke_cleanup, NULL, NULL, &DYNAMIC_TIME_ZONE_INFORMATION_t933351445_0_0_0 } /* System.TimeZoneInfo/DYNAMIC_TIME_ZONE_INFORMATION */,
	{ NULL, TIME_ZONE_INFORMATION_t691679524_marshal_pinvoke, TIME_ZONE_INFORMATION_t691679524_marshal_pinvoke_back, TIME_ZONE_INFORMATION_t691679524_marshal_pinvoke_cleanup, NULL, NULL, &TIME_ZONE_INFORMATION_t691679524_0_0_0 } /* System.TimeZoneInfo/TIME_ZONE_INFORMATION */,
	{ NULL, TransitionTime_t449921781_marshal_pinvoke, TransitionTime_t449921781_marshal_pinvoke_back, TransitionTime_t449921781_marshal_pinvoke_cleanup, NULL, NULL, &TransitionTime_t449921781_0_0_0 } /* System.TimeZoneInfo/TransitionTime */,
	{ NULL, UnSafeCharBuffer_t2176740272_marshal_pinvoke, UnSafeCharBuffer_t2176740272_marshal_pinvoke_back, UnSafeCharBuffer_t2176740272_marshal_pinvoke_cleanup, NULL, NULL, &UnSafeCharBuffer_t2176740272_0_0_0 } /* System.UnSafeCharBuffer */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ NULL, __DTString_t2014593278_marshal_pinvoke, __DTString_t2014593278_marshal_pinvoke_back, __DTString_t2014593278_marshal_pinvoke_cleanup, NULL, NULL, &__DTString_t2014593278_0_0_0 } /* System.__DTString */,
	{ NULL, XmlCharType_t2277243275_marshal_pinvoke, XmlCharType_t2277243275_marshal_pinvoke_back, XmlCharType_t2277243275_marshal_pinvoke_cleanup, NULL, NULL, &XmlCharType_t2277243275_0_0_0 } /* System.Xml.XmlCharType */,
	{ DelegatePInvokeWrapper_CFProxyAutoConfigurationResultCallback_t949084125, NULL, NULL, NULL, NULL, NULL, &CFProxyAutoConfigurationResultCallback_t949084125_0_0_0 } /* Mono.Net.CFNetwork/CFProxyAutoConfigurationResultCallback */,
	{ NULL, unitytls_interface_struct_t2105669693_marshal_pinvoke, unitytls_interface_struct_t2105669693_marshal_pinvoke_back, unitytls_interface_struct_t2105669693_marshal_pinvoke_cleanup, NULL, NULL, &unitytls_interface_struct_t2105669693_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct */,
	{ DelegatePInvokeWrapper_unitytls_errorstate_create_t_t663213720, NULL, NULL, NULL, NULL, NULL, &unitytls_errorstate_create_t_t663213720_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_create_t */,
	{ DelegatePInvokeWrapper_unitytls_errorstate_raise_error_t_t3653623215, NULL, NULL, NULL, NULL, NULL, &unitytls_errorstate_raise_error_t_t3653623215_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_raise_error_t */,
	{ DelegatePInvokeWrapper_unitytls_key_free_t_t769506560, NULL, NULL, NULL, NULL, NULL, &unitytls_key_free_t_t769506560_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_free_t */,
	{ DelegatePInvokeWrapper_unitytls_key_get_ref_t_t3903963684, NULL, NULL, NULL, NULL, NULL, &unitytls_key_get_ref_t_t3903963684_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_get_ref_t */,
	{ DelegatePInvokeWrapper_unitytls_key_parse_der_t_t352753247, NULL, NULL, NULL, NULL, NULL, &unitytls_key_parse_der_t_t352753247_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_der_t */,
	{ DelegatePInvokeWrapper_unitytls_key_parse_pem_t_t422811231, NULL, NULL, NULL, NULL, NULL, &unitytls_key_parse_pem_t_t422811231_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_pem_t */,
	{ DelegatePInvokeWrapper_unitytls_random_generate_bytes_t_t1261472879, NULL, NULL, NULL, NULL, NULL, &unitytls_random_generate_bytes_t_t1261472879_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_random_generate_bytes_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_create_client_t_t4224747280, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_create_client_t_t4224747280_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_client_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_create_server_t_t697296614, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_create_server_t_t697296614_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_server_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_free_t_t2945881544, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_free_t_t2945881544_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_free_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_get_ciphersuite_t_t2849089842, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_get_ciphersuite_t_t2849089842_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_ciphersuite_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_get_protocol_t_t3836938866, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_get_protocol_t_t3836938866_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_protocol_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_notify_close_t_t823845960, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_notify_close_t_t823845960_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_notify_close_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_process_handshake_t_t2729115783, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_process_handshake_t_t2729115783_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_process_handshake_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_read_t_t2716224336, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_read_t_t2716224336_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_read_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_server_require_client_authentication_t_t2757305746, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_server_require_client_authentication_t_t2757305746_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_server_require_client_authentication_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_set_certificate_callback_t_t928657621, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_set_certificate_callback_t_t928657621_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_certificate_callback_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_set_supported_ciphersuites_t_t674237385, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_set_supported_ciphersuites_t_t674237385_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_supported_ciphersuites_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_set_trace_callback_t_t1728780939, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_set_trace_callback_t_t1728780939_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_trace_callback_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_set_x509verify_callback_t_t3126135024, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_set_x509verify_callback_t_t3126135024_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_x509verify_callback_t */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_write_t_t2414968268, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_write_t_t2414968268_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_write_t */,
	{ DelegatePInvokeWrapper_unitytls_x509_export_der_t_t3165820883, NULL, NULL, NULL, NULL, NULL, &unitytls_x509_export_der_t_t3165820883_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509_export_der_t */,
	{ DelegatePInvokeWrapper_unitytls_x509list_append_der_t_t361658413, NULL, NULL, NULL, NULL, NULL, &unitytls_x509list_append_der_t_t361658413_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t */,
	{ DelegatePInvokeWrapper_unitytls_x509list_append_t_t1135075908, NULL, NULL, NULL, NULL, NULL, &unitytls_x509list_append_t_t1135075908_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_t */,
	{ DelegatePInvokeWrapper_unitytls_x509list_create_t_t249995399, NULL, NULL, NULL, NULL, NULL, &unitytls_x509list_create_t_t249995399_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_create_t */,
	{ DelegatePInvokeWrapper_unitytls_x509list_free_t_t1015882467, NULL, NULL, NULL, NULL, NULL, &unitytls_x509list_free_t_t1015882467_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_free_t */,
	{ DelegatePInvokeWrapper_unitytls_x509list_get_ref_t_t3178515812, NULL, NULL, NULL, NULL, NULL, &unitytls_x509list_get_ref_t_t3178515812_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_ref_t */,
	{ DelegatePInvokeWrapper_unitytls_x509list_get_x509_t_t413983797, NULL, NULL, NULL, NULL, NULL, &unitytls_x509list_get_x509_t_t413983797_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_x509_t */,
	{ DelegatePInvokeWrapper_unitytls_x509verify_default_ca_t_t671865852, NULL, NULL, NULL, NULL, NULL, &unitytls_x509verify_default_ca_t_t671865852_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_default_ca_t */,
	{ DelegatePInvokeWrapper_unitytls_x509verify_explicit_ca_t_t1629606714, NULL, NULL, NULL, NULL, NULL, &unitytls_x509verify_explicit_ca_t_t1629606714_0_0_0 } /* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_explicit_ca_t */,
	{ NULL, unitytls_tlsctx_callbacks_t3626000702_marshal_pinvoke, unitytls_tlsctx_callbacks_t3626000702_marshal_pinvoke_back, unitytls_tlsctx_callbacks_t3626000702_marshal_pinvoke_cleanup, NULL, NULL, &unitytls_tlsctx_callbacks_t3626000702_0_0_0 } /* Mono.Unity.UnityTls/unitytls_tlsctx_callbacks */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_certificate_callback_t2034592268, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_certificate_callback_t2034592268_0_0_0 } /* Mono.Unity.UnityTls/unitytls_tlsctx_certificate_callback */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_read_callback_t2806525115, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_read_callback_t2806525115_0_0_0 } /* Mono.Unity.UnityTls/unitytls_tlsctx_read_callback */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_trace_callback_t2576344559, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_trace_callback_t2576344559_0_0_0 } /* Mono.Unity.UnityTls/unitytls_tlsctx_trace_callback */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_write_callback_t2121017372, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_write_callback_t2121017372_0_0_0 } /* Mono.Unity.UnityTls/unitytls_tlsctx_write_callback */,
	{ DelegatePInvokeWrapper_unitytls_tlsctx_x509verify_callback_t194548070, NULL, NULL, NULL, NULL, NULL, &unitytls_tlsctx_x509verify_callback_t194548070_0_0_0 } /* Mono.Unity.UnityTls/unitytls_tlsctx_x509verify_callback */,
	{ DelegatePInvokeWrapper_unitytls_x509verify_callback_t321808703, NULL, NULL, NULL, NULL, NULL, &unitytls_x509verify_callback_t321808703_0_0_0 } /* Mono.Unity.UnityTls/unitytls_x509verify_callback */,
	{ DelegatePInvokeWrapper_ReadMethod_t893206259, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t893206259_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_WriteMethod_t2538911768, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t2538911768_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1975956110, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t1975956110_0_0_0 } /* System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite */,
	{ NULL, IOAsyncResult_t3640145766_marshal_pinvoke, IOAsyncResult_t3640145766_marshal_pinvoke_back, IOAsyncResult_t3640145766_marshal_pinvoke_cleanup, NULL, NULL, &IOAsyncResult_t3640145766_0_0_0 } /* System.IOAsyncResult */,
	{ NULL, IOSelectorJob_t2199748873_marshal_pinvoke, IOSelectorJob_t2199748873_marshal_pinvoke_back, IOSelectorJob_t2199748873_marshal_pinvoke_cleanup, NULL, NULL, &IOSelectorJob_t2199748873_0_0_0 } /* System.IOSelectorJob */,
	{ NULL, RecognizedAttribute_t632074220_marshal_pinvoke, RecognizedAttribute_t632074220_marshal_pinvoke_back, RecognizedAttribute_t632074220_marshal_pinvoke_cleanup, NULL, NULL, &RecognizedAttribute_t632074220_0_0_0 } /* System.Net.CookieTokenizer/RecognizedAttribute */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4266946825, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4266946825_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t2016697242, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t2016697242_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_HeaderParser_t1081925738, NULL, NULL, NULL, NULL, NULL, &HeaderParser_t1081925738_0_0_0 } /* System.Net.HeaderParser */,
	{ NULL, HeaderVariantInfo_t1935685601_marshal_pinvoke, HeaderVariantInfo_t1935685601_marshal_pinvoke_back, HeaderVariantInfo_t1935685601_marshal_pinvoke_cleanup, NULL, NULL, &HeaderVariantInfo_t1935685601_0_0_0 } /* System.Net.HeaderVariantInfo */,
	{ NULL, AuthorizationState_t3141350760_marshal_pinvoke, AuthorizationState_t3141350760_marshal_pinvoke_back, AuthorizationState_t3141350760_marshal_pinvoke_cleanup, NULL, NULL, &AuthorizationState_t3141350760_0_0_0 } /* System.Net.HttpWebRequest/AuthorizationState */,
	{ NULL, IPv6AddressFormatter_t878312391_marshal_pinvoke, IPv6AddressFormatter_t878312391_marshal_pinvoke_back, IPv6AddressFormatter_t878312391_marshal_pinvoke_cleanup, NULL, NULL, &IPv6AddressFormatter_t878312391_0_0_0 } /* System.Net.IPv6AddressFormatter */,
	{ NULL, Win32_FIXED_INFO_t1299345856_marshal_pinvoke, Win32_FIXED_INFO_t1299345856_marshal_pinvoke_back, Win32_FIXED_INFO_t1299345856_marshal_pinvoke_cleanup, NULL, NULL, &Win32_FIXED_INFO_t1299345856_0_0_0 } /* System.Net.NetworkInformation.Win32_FIXED_INFO */,
	{ NULL, Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke, Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_back, Win32_IP_ADDR_STRING_t1213417184_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADDR_STRING_t1213417184_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADDR_STRING */,
	{ NULL, SocketAsyncResult_t3523156467_marshal_pinvoke, SocketAsyncResult_t3523156467_marshal_pinvoke_back, SocketAsyncResult_t3523156467_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t3523156467_0_0_0 } /* System.Net.Sockets.SocketAsyncResult */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, LowerCaseMapping_t2910317575_marshal_pinvoke, LowerCaseMapping_t2910317575_marshal_pinvoke_back, LowerCaseMapping_t2910317575_marshal_pinvoke_cleanup, NULL, NULL, &LowerCaseMapping_t2910317575_0_0_0 } /* System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, OrderBlock_t1585977831_marshal_pinvoke, OrderBlock_t1585977831_marshal_pinvoke_back, OrderBlock_t1585977831_marshal_pinvoke_cleanup, NULL, NULL, &OrderBlock_t1585977831_0_0_0 } /* UnityEngine.BeforeRenderHelper/OrderBlock */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, PlayerLoopSystem_t105772105_marshal_pinvoke, PlayerLoopSystem_t105772105_marshal_pinvoke_back, PlayerLoopSystem_t105772105_marshal_pinvoke_cleanup, NULL, NULL, &PlayerLoopSystem_t105772105_0_0_0 } /* UnityEngine.Experimental.LowLevel.PlayerLoopSystem */,
	{ DelegatePInvokeWrapper_UpdateFunction_t377278577, NULL, NULL, NULL, NULL, NULL, &UpdateFunction_t377278577_0_0_0 } /* UnityEngine.Experimental.LowLevel.PlayerLoopSystem/UpdateFunction */,
	{ NULL, PlayerLoopSystemInternal_t2185485283_marshal_pinvoke, PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_back, PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_cleanup, NULL, NULL, &PlayerLoopSystemInternal_t2185485283_0_0_0 } /* UnityEngine.Experimental.LowLevel.PlayerLoopSystemInternal */,
	{ NULL, CullResults_t76141518_marshal_pinvoke, CullResults_t76141518_marshal_pinvoke_back, CullResults_t76141518_marshal_pinvoke_cleanup, NULL, NULL, &CullResults_t76141518_0_0_0 } /* UnityEngine.Experimental.Rendering.CullResults */,
	{ NULL, SpriteBone_t303320096_marshal_pinvoke, SpriteBone_t303320096_marshal_pinvoke_back, SpriteBone_t303320096_marshal_pinvoke_cleanup, NULL, NULL, &SpriteBone_t303320096_0_0_0 } /* UnityEngine.Experimental.U2D.SpriteBone */,
	{ NULL, FailedToLoadScriptObject_t547604379_marshal_pinvoke, FailedToLoadScriptObject_t547604379_marshal_pinvoke_back, FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t547604379_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup, NULL, NULL, &Internal_DrawTextureArguments_t1705718261_0_0_0 } /* UnityEngine.Internal_DrawTextureArguments */,
	{ NULL, LightBakingOutput_t989813037_marshal_pinvoke, LightBakingOutput_t989813037_marshal_pinvoke_back, LightBakingOutput_t989813037_marshal_pinvoke_cleanup, NULL, NULL, &LightBakingOutput_t989813037_0_0_0 } /* UnityEngine.LightBakingOutput */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t354260709_marshal_pinvoke, PlayableBinding_t354260709_marshal_pinvoke_back, PlayableBinding_t354260709_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t354260709_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ DelegatePInvokeWrapper_CreateOutputMethod_t2301811773, NULL, NULL, NULL, NULL, NULL, &CreateOutputMethod_t2301811773_0_0_0 } /* UnityEngine.Playables.PlayableBinding/CreateOutputMethod */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3100554279_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1354518612_marshal_pinvoke, WorkRequest_t1354518612_marshal_pinvoke_back, WorkRequest_t1354518612_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1354518612_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_ConsumeSampleFramesNativeFunction_t1497769677, NULL, NULL, NULL, NULL, NULL, &ConsumeSampleFramesNativeFunction_t1497769677_0_0_0 } /* UnityEngine.Experimental.Audio.AudioSampleProvider/ConsumeSampleFramesNativeFunction */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, CertificateHandler_t2739891000_marshal_pinvoke, CertificateHandler_t2739891000_marshal_pinvoke_back, CertificateHandler_t2739891000_marshal_pinvoke_cleanup, NULL, NULL, &CertificateHandler_t2739891000_0_0_0 } /* UnityEngine.Networking.CertificateHandler */,
	{ NULL, DownloadHandler_t2937767557_marshal_pinvoke, DownloadHandler_t2937767557_marshal_pinvoke_back, DownloadHandler_t2937767557_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t2937767557_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerBuffer_t2928496527_marshal_pinvoke, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t2928496527_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, UnityWebRequest_t463507806_marshal_pinvoke, UnityWebRequest_t463507806_marshal_pinvoke_back, UnityWebRequest_t463507806_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t463507806_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequestAsyncOperation_t3852015985_0_0_0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation */,
	{ NULL, UploadHandler_t2993558019_marshal_pinvoke, UploadHandler_t2993558019_marshal_pinvoke_back, UploadHandler_t2993558019_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t2993558019_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, UploadHandlerRaw_t25761545_marshal_pinvoke, UploadHandlerRaw_t25761545_marshal_pinvoke_back, UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandlerRaw_t25761545_0_0_0 } /* UnityEngine.Networking.UploadHandlerRaw */,
	{ NULL, Subsystem_t89723475_marshal_pinvoke, Subsystem_t89723475_marshal_pinvoke_back, Subsystem_t89723475_marshal_pinvoke_cleanup, NULL, NULL, &Subsystem_t89723475_0_0_0 } /* UnityEngine.Experimental.Subsystem */,
	{ NULL, SubsystemDescriptorBase_t2374447182_marshal_pinvoke, SubsystemDescriptorBase_t2374447182_marshal_pinvoke_back, SubsystemDescriptorBase_t2374447182_marshal_pinvoke_cleanup, NULL, NULL, &SubsystemDescriptorBase_t2374447182_0_0_0 } /* UnityEngine.Experimental.SubsystemDescriptorBase */,
	{ NULL, FrameReceivedEventArgs_t2588080103_marshal_pinvoke, FrameReceivedEventArgs_t2588080103_marshal_pinvoke_back, FrameReceivedEventArgs_t2588080103_marshal_pinvoke_cleanup, NULL, NULL, &FrameReceivedEventArgs_t2588080103_0_0_0 } /* UnityEngine.Experimental.XR.FrameReceivedEventArgs */,
	{ NULL, PlaneAddedEventArgs_t2550175725_marshal_pinvoke, PlaneAddedEventArgs_t2550175725_marshal_pinvoke_back, PlaneAddedEventArgs_t2550175725_marshal_pinvoke_cleanup, NULL, NULL, &PlaneAddedEventArgs_t2550175725_0_0_0 } /* UnityEngine.Experimental.XR.PlaneAddedEventArgs */,
	{ NULL, PlaneRemovedEventArgs_t1567129782_marshal_pinvoke, PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_back, PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_cleanup, NULL, NULL, &PlaneRemovedEventArgs_t1567129782_0_0_0 } /* UnityEngine.Experimental.XR.PlaneRemovedEventArgs */,
	{ NULL, PlaneUpdatedEventArgs_t349485851_marshal_pinvoke, PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_back, PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_cleanup, NULL, NULL, &PlaneUpdatedEventArgs_t349485851_0_0_0 } /* UnityEngine.Experimental.XR.PlaneUpdatedEventArgs */,
	{ NULL, PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke, PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_back, PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_cleanup, NULL, NULL, &PointCloudUpdatedEventArgs_t3436657348_0_0_0 } /* UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs */,
	{ NULL, SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke, SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_back, SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_cleanup, NULL, NULL, &SessionTrackingStateChangedEventArgs_t2343035655_0_0_0 } /* UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorControllerParameter_t1758260042_marshal_pinvoke, AnimatorControllerParameter_t1758260042_marshal_pinvoke_back, AnimatorControllerParameter_t1758260042_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorControllerParameter_t1758260042_0_0_0 } /* UnityEngine.AnimatorControllerParameter */,
	{ DelegatePInvokeWrapper_OnOverrideControllerDirtyCallback_t1307045488, NULL, NULL, NULL, NULL, NULL, &OnOverrideControllerDirtyCallback_t1307045488_0_0_0 } /* UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, EmitParams_t2216423628_marshal_pinvoke, EmitParams_t2216423628_marshal_pinvoke_back, EmitParams_t2216423628_marshal_pinvoke_cleanup, NULL, NULL, &EmitParams_t2216423628_0_0_0 } /* UnityEngine.ParticleSystem/EmitParams */,
	{ NULL, Collision2D_t2842956331_marshal_pinvoke, Collision2D_t2842956331_marshal_pinvoke_back, Collision2D_t2842956331_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t2842956331_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_BasicResponseDelegate_t2196726690, NULL, NULL, NULL, NULL, NULL, &BasicResponseDelegate_t2196726690_0_0_0 } /* UnityEngine.Networking.Match.NetworkMatch/BasicResponseDelegate */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t3163629820, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t3163629820_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ NULL, CustomEventData_t317522481_marshal_pinvoke, CustomEventData_t317522481_marshal_pinvoke_back, CustomEventData_t317522481_marshal_pinvoke_cleanup, NULL, NULL, &CustomEventData_t317522481_0_0_0 } /* UnityEngine.Analytics.CustomEventData */,
	{ NULL, UnityAnalyticsHandler_t3011359618_marshal_pinvoke, UnityAnalyticsHandler_t3011359618_marshal_pinvoke_back, UnityAnalyticsHandler_t3011359618_marshal_pinvoke_cleanup, NULL, NULL, &UnityAnalyticsHandler_t3011359618_0_0_0 } /* UnityEngine.Analytics.UnityAnalyticsHandler */,
	{ NULL, RemoteConfigSettings_t1247263429_marshal_pinvoke, RemoteConfigSettings_t1247263429_marshal_pinvoke_back, RemoteConfigSettings_t1247263429_marshal_pinvoke_cleanup, NULL, NULL, &RemoteConfigSettings_t1247263429_0_0_0 } /* UnityEngine.RemoteConfigSettings */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ DelegatePInvokeWrapper_UnityPurchasingCallback_t953216184, NULL, NULL, NULL, NULL, NULL, &UnityPurchasingCallback_t953216184_0_0_0 } /* UnityEngine.Purchasing.UnityPurchasingCallback */,
	{ NULL, ValueDropdownItem_t3457888829_marshal_pinvoke, ValueDropdownItem_t3457888829_marshal_pinvoke_back, ValueDropdownItem_t3457888829_marshal_pinvoke_cleanup, NULL, NULL, &ValueDropdownItem_t3457888829_0_0_0 } /* Sirenix.OdinInspector.ValueDropdownItem */,
	{ NULL, PathStep_t1211216146_marshal_pinvoke, PathStep_t1211216146_marshal_pinvoke_back, PathStep_t1211216146_marshal_pinvoke_cleanup, NULL, NULL, &PathStep_t1211216146_0_0_0 } /* Sirenix.Utilities.DeepReflection/PathStep */,
	{ DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3388716826, NULL, NULL, NULL, NULL, NULL, &UnityNativePurchasingCallback_t3388716826_0_0_0 } /* UnityEngine.Purchasing.UnityNativePurchasingCallback */,
	{ NULL, ChannelPacket_t1579824718_marshal_pinvoke, ChannelPacket_t1579824718_marshal_pinvoke_back, ChannelPacket_t1579824718_marshal_pinvoke_cleanup, NULL, NULL, &ChannelPacket_t1579824718_0_0_0 } /* UnityEngine.Networking.ChannelPacket */,
	{ NULL, InternalMsg_t2371755407_marshal_pinvoke, InternalMsg_t2371755407_marshal_pinvoke_back, InternalMsg_t2371755407_marshal_pinvoke_cleanup, NULL, NULL, &InternalMsg_t2371755407_0_0_0 } /* UnityEngine.Networking.LocalClient/InternalMsg */,
	{ NULL, NetworkBroadcastResult_t2174414888_marshal_pinvoke, NetworkBroadcastResult_t2174414888_marshal_pinvoke_back, NetworkBroadcastResult_t2174414888_marshal_pinvoke_cleanup, NULL, NULL, &NetworkBroadcastResult_t2174414888_0_0_0 } /* UnityEngine.Networking.NetworkBroadcastResult */,
	{ NULL, PendingPlayer_t306375494_marshal_pinvoke, PendingPlayer_t306375494_marshal_pinvoke_back, PendingPlayer_t306375494_marshal_pinvoke_cleanup, NULL, NULL, &PendingPlayer_t306375494_0_0_0 } /* UnityEngine.Networking.NetworkLobbyManager/PendingPlayer */,
	{ NULL, ConnectionPendingPlayers_t878091664_marshal_pinvoke, ConnectionPendingPlayers_t878091664_marshal_pinvoke_back, ConnectionPendingPlayers_t878091664_marshal_pinvoke_cleanup, NULL, NULL, &ConnectionPendingPlayers_t878091664_0_0_0 } /* UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers */,
	{ NULL, PendingPlayerInfo_t2391300657_marshal_pinvoke, PendingPlayerInfo_t2391300657_marshal_pinvoke_back, PendingPlayerInfo_t2391300657_marshal_pinvoke_cleanup, NULL, NULL, &PendingPlayerInfo_t2391300657_0_0_0 } /* UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo */,
	{ NULL, CRCMessageEntry_t1041239249_marshal_pinvoke, CRCMessageEntry_t1041239249_marshal_pinvoke_back, CRCMessageEntry_t1041239249_marshal_pinvoke_cleanup, NULL, NULL, &CRCMessageEntry_t1041239249_0_0_0 } /* UnityEngine.Networking.NetworkSystem.CRCMessageEntry */,
	{ DelegatePInvokeWrapper_ClientMoveCallback2D_t270751497, NULL, NULL, NULL, NULL, NULL, &ClientMoveCallback2D_t270751497_0_0_0 } /* UnityEngine.Networking.NetworkTransform/ClientMoveCallback2D */,
	{ DelegatePInvokeWrapper_ClientMoveCallback3D_t1836835438, NULL, NULL, NULL, NULL, NULL, &ClientMoveCallback3D_t1836835438_0_0_0 } /* UnityEngine.Networking.NetworkTransform/ClientMoveCallback3D */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ DelegatePInvokeWrapper_GetRayIntersectionAllCallback_t3913627115, NULL, NULL, NULL, NULL, NULL, &GetRayIntersectionAllCallback_t3913627115_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback */,
	{ DelegatePInvokeWrapper_GetRayIntersectionAllNonAllocCallback_t2311174851, NULL, NULL, NULL, NULL, NULL, &GetRayIntersectionAllNonAllocCallback_t2311174851_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback */,
	{ DelegatePInvokeWrapper_GetRaycastNonAllocCallback_t3841783507, NULL, NULL, NULL, NULL, NULL, &GetRaycastNonAllocCallback_t3841783507_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback */,
	{ DelegatePInvokeWrapper_Raycast2DCallback_t768590915, NULL, NULL, NULL, NULL, NULL, &Raycast2DCallback_t768590915_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback */,
	{ DelegatePInvokeWrapper_Raycast3DCallback_t701940803, NULL, NULL, NULL, NULL, NULL, &Raycast3DCallback_t701940803_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback */,
	{ DelegatePInvokeWrapper_RaycastAllCallback_t1884415901, NULL, NULL, NULL, NULL, NULL, &RaycastAllCallback_t1884415901_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, FormatterInfo_t2891647851_marshal_pinvoke, FormatterInfo_t2891647851_marshal_pinvoke_back, FormatterInfo_t2891647851_marshal_pinvoke_cleanup, NULL, NULL, &FormatterInfo_t2891647851_0_0_0 } /* Sirenix.Serialization.FormatterLocator/FormatterInfo */,
	{ NULL, FormatterLocatorInfo_t371893538_marshal_pinvoke, FormatterLocatorInfo_t371893538_marshal_pinvoke_back, FormatterLocatorInfo_t371893538_marshal_pinvoke_cleanup, NULL, NULL, &FormatterLocatorInfo_t371893538_0_0_0 } /* Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo */,
	{ NULL, NodeInfo_t1254526976_marshal_pinvoke, NodeInfo_t1254526976_marshal_pinvoke_back, NodeInfo_t1254526976_marshal_pinvoke_cleanup, NULL, NULL, &NodeInfo_t1254526976_0_0_0 } /* Sirenix.Serialization.NodeInfo */,
	{ NULL, SerializationData_t3163410965_marshal_pinvoke, SerializationData_t3163410965_marshal_pinvoke_back, SerializationData_t3163410965_marshal_pinvoke_cleanup, NULL, NULL, &SerializationData_t3163410965_0_0_0 } /* Sirenix.Serialization.SerializationData */,
	{ NULL, SerializationNode_t389653619_marshal_pinvoke, SerializationNode_t389653619_marshal_pinvoke_back, SerializationNode_t389653619_marshal_pinvoke_cleanup, NULL, NULL, &SerializationNode_t389653619_0_0_0 } /* Sirenix.Serialization.SerializationNode */,
	{ NULL, ShadowData_t3587884416_marshal_pinvoke, ShadowData_t3587884416_marshal_pinvoke_back, ShadowData_t3587884416_marshal_pinvoke_cleanup, NULL, NULL, &ShadowData_t3587884416_0_0_0 } /* UnityEngine.Experimental.Rendering.AdditionalShadowData/ShadowData */,
	{ NULL, Frustum_t2039891733_marshal_pinvoke, Frustum_t2039891733_marshal_pinvoke_back, Frustum_t2039891733_marshal_pinvoke_cleanup, NULL, NULL, &Frustum_t2039891733_0_0_0 } /* UnityEngine.Experimental.Rendering.Frustum */,
	{ NULL, CopyOperation_t3389193622_marshal_pinvoke, CopyOperation_t3389193622_marshal_pinvoke_back, CopyOperation_t3389193622_marshal_pinvoke_cleanup, NULL, NULL, &CopyOperation_t3389193622_0_0_0 } /* UnityEngine.Experimental.Rendering.GPUCopyAsset/CopyOperation */,
	{ NULL, ProfilingSample_t2938638080_marshal_pinvoke, ProfilingSample_t2938638080_marshal_pinvoke_back, ProfilingSample_t2938638080_marshal_pinvoke_cleanup, NULL, NULL, &ProfilingSample_t2938638080_0_0_0 } /* UnityEngine.Experimental.Rendering.ProfilingSample */,
	{ DelegatePInvokeWrapper_ScaleFunc_t890021287, NULL, NULL, NULL, NULL, NULL, &ScaleFunc_t890021287_0_0_0 } /* UnityEngine.Experimental.Rendering.ScaleFunc */,
	{ NULL, AtlasInit_t852154556_marshal_pinvoke, AtlasInit_t852154556_marshal_pinvoke_back, AtlasInit_t852154556_marshal_pinvoke_cleanup, NULL, NULL, &AtlasInit_t852154556_0_0_0 } /* UnityEngine.Experimental.Rendering.ShadowAtlas/AtlasInit */,
	{ NULL, CtxtInit_t1321886867_marshal_pinvoke, CtxtInit_t1321886867_marshal_pinvoke_back, CtxtInit_t1321886867_marshal_pinvoke_cleanup, NULL, NULL, &CtxtInit_t1321886867_0_0_0 } /* UnityEngine.Experimental.Rendering.ShadowContext/CtxtInit */,
	{ NULL, Dels_t361700601_marshal_pinvoke, Dels_t361700601_marshal_pinvoke_back, Dels_t361700601_marshal_pinvoke_cleanup, NULL, NULL, &Dels_t361700601_0_0_0 } /* UnityEngine.Experimental.Rendering.ShadowRegistry/Dels */,
	{ NULL, Entry_t1827551715_marshal_pinvoke, Entry_t1827551715_marshal_pinvoke_back, Entry_t1827551715_marshal_pinvoke_cleanup, NULL, NULL, &Entry_t1827551715_0_0_0 } /* UnityEngine.Experimental.Rendering.ShadowRegistry/Entry */,
	{ NULL, Override_t1415021437_marshal_pinvoke, Override_t1415021437_marshal_pinvoke_back, Override_t1415021437_marshal_pinvoke_cleanup, NULL, NULL, &Override_t1415021437_0_0_0 } /* UnityEngine.Experimental.Rendering.ShadowRegistry/Override */,
	{ DelegatePInvokeWrapper_unityAdsDidError_t1993223595, NULL, NULL, NULL, NULL, NULL, &unityAdsDidError_t1993223595_0_0_0 } /* UnityEngine.Advertisements.iOS.Platform/unityAdsDidError */,
	{ DelegatePInvokeWrapper_unityAdsDidFinish_t3747416149, NULL, NULL, NULL, NULL, NULL, &unityAdsDidFinish_t3747416149_0_0_0 } /* UnityEngine.Advertisements.iOS.Platform/unityAdsDidFinish */,
	{ DelegatePInvokeWrapper_unityAdsDidStart_t1058412932, NULL, NULL, NULL, NULL, NULL, &unityAdsDidStart_t1058412932_0_0_0 } /* UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart */,
	{ DelegatePInvokeWrapper_unityAdsReady_t96934738, NULL, NULL, NULL, NULL, NULL, &unityAdsReady_t96934738_0_0_0 } /* UnityEngine.Advertisements.iOS.Platform/unityAdsReady */,
	{ NULL, OptOutResponse_t2266495071_marshal_pinvoke, OptOutResponse_t2266495071_marshal_pinvoke_back, OptOutResponse_t2266495071_marshal_pinvoke_cleanup, NULL, NULL, &OptOutResponse_t2266495071_0_0_0 } /* UnityEngine.Analytics.DataPrivacy/OptOutResponse */,
	{ NULL, OptOutStatus_t3541615854_marshal_pinvoke, OptOutStatus_t3541615854_marshal_pinvoke_back, OptOutStatus_t3541615854_marshal_pinvoke_cleanup, NULL, NULL, &OptOutStatus_t3541615854_0_0_0 } /* UnityEngine.Analytics.DataPrivacy/OptOutStatus */,
	{ NULL, RequestData_t3101179086_marshal_pinvoke, RequestData_t3101179086_marshal_pinvoke_back, RequestData_t3101179086_marshal_pinvoke_cleanup, NULL, NULL, &RequestData_t3101179086_0_0_0 } /* UnityEngine.Analytics.DataPrivacy/RequestData */,
	{ NULL, TokenData_t2077495713_marshal_pinvoke, TokenData_t2077495713_marshal_pinvoke_back, TokenData_t2077495713_marshal_pinvoke_cleanup, NULL, NULL, &TokenData_t2077495713_0_0_0 } /* UnityEngine.Analytics.DataPrivacy/TokenData */,
	{ NULL, UserPostData_t478425489_marshal_pinvoke, UserPostData_t478425489_marshal_pinvoke_back, UserPostData_t478425489_marshal_pinvoke_cleanup, NULL, NULL, &UserPostData_t478425489_0_0_0 } /* UnityEngine.Analytics.DataPrivacy/UserPostData */,
	{ NULL, CameraData_t2373231130_marshal_pinvoke, CameraData_t2373231130_marshal_pinvoke_back, CameraData_t2373231130_marshal_pinvoke_cleanup, NULL, NULL, &CameraData_t2373231130_0_0_0 } /* UnityEngine.Experimental.Rendering.LightweightPipeline.CameraData */,
	{ NULL, LightData_t718835846_marshal_pinvoke, LightData_t718835846_marshal_pinvoke_back, LightData_t718835846_marshal_pinvoke_cleanup, NULL, NULL, &LightData_t718835846_0_0_0 } /* UnityEngine.Experimental.Rendering.LightweightPipeline.LightData */,
	{ NULL, RenderingData_t2420043686_marshal_pinvoke, RenderingData_t2420043686_marshal_pinvoke_back, RenderingData_t2420043686_marshal_pinvoke_cleanup, NULL, NULL, &RenderingData_t2420043686_0_0_0 } /* UnityEngine.Experimental.Rendering.LightweightPipeline.RenderingData */,
	{ NULL, ShadowData_t294369652_marshal_pinvoke, ShadowData_t294369652_marshal_pinvoke_back, ShadowData_t294369652_marshal_pinvoke_cleanup, NULL, NULL, &ShadowData_t294369652_0_0_0 } /* UnityEngine.Experimental.Rendering.LightweightPipeline.ShadowData */,
	{ DelegatePInvokeWrapper_OnTrigger_t4184125570, NULL, NULL, NULL, NULL, NULL, &OnTrigger_t4184125570_0_0_0 } /* UnityEngine.Analytics.EventTrigger/OnTrigger */,
	{ NULL, SpawningPoolEntry_t2116637239_marshal_pinvoke, SpawningPoolEntry_t2116637239_marshal_pinvoke_back, SpawningPoolEntry_t2116637239_marshal_pinvoke_cleanup, NULL, NULL, &SpawningPoolEntry_t2116637239_0_0_0 } /* DigitalRuby.Pooling.SpawningPoolScript/SpawningPoolEntry */,
	{ NULL, ShownTouch_t1740607014_marshal_pinvoke, ShownTouch_t1740607014_marshal_pinvoke_back, ShownTouch_t1740607014_marshal_pinvoke_cleanup, NULL, NULL, &ShownTouch_t1740607014_0_0_0 } /* DigitalRubyShared.FingersScript/ShownTouch */,
	{ DelegatePInvokeWrapper_CallbackMainThreadDelegate_t469493312, NULL, NULL, NULL, NULL, NULL, &CallbackMainThreadDelegate_t469493312_0_0_0 } /* DigitalRubyShared.GestureRecognizer/CallbackMainThreadDelegate */,
	{ NULL, GestureTouch_t1992402133_marshal_pinvoke, GestureTouch_t1992402133_marshal_pinvoke_back, GestureTouch_t1992402133_marshal_pinvoke_cleanup, NULL, NULL, &GestureTouch_t1992402133_0_0_0 } /* DigitalRubyShared.GestureTouch */,
	{ NULL, ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshal_pinvoke, ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshal_pinvoke_back, ImageGestureRecognizerComponentScriptImageEntry_t2713275279_marshal_pinvoke_cleanup, NULL, NULL, &ImageGestureRecognizerComponentScriptImageEntry_t2713275279_0_0_0 } /* DigitalRubyShared.ImageGestureRecognizerComponentScriptImageEntry */,
	{ NULL, LoadScoreRequest_t1342227622_marshal_pinvoke, LoadScoreRequest_t1342227622_marshal_pinvoke_back, LoadScoreRequest_t1342227622_marshal_pinvoke_cleanup, NULL, NULL, &LoadScoreRequest_t1342227622_0_0_0 } /* EasyMobile.GameServices/LoadScoreRequest */,
	{ NULL, GiphyUploadParams_t2791732282_marshal_pinvoke, GiphyUploadParams_t2791732282_marshal_pinvoke_back, GiphyUploadParams_t2791732282_marshal_pinvoke_cleanup, NULL, NULL, &GiphyUploadParams_t2791732282_0_0_0 } /* EasyMobile.GiphyUploadParams */,
	{ DelegatePInvokeWrapper_DeleteSavedGameCallback_t2032035250, NULL, NULL, NULL, NULL, NULL, &DeleteSavedGameCallback_t2032035250_0_0_0 } /* EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/DeleteSavedGameCallback */,
	{ DelegatePInvokeWrapper_FetchSavedGamesCallback_t3515465237, NULL, NULL, NULL, NULL, NULL, &FetchSavedGamesCallback_t3515465237_0_0_0 } /* EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/FetchSavedGamesCallback */,
	{ DelegatePInvokeWrapper_LoadSavedGameDataCallback_t1958165752, NULL, NULL, NULL, NULL, NULL, &LoadSavedGameDataCallback_t1958165752_0_0_0 } /* EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/LoadSavedGameDataCallback */,
	{ DelegatePInvokeWrapper_OpenSavedGameCallback_t2349091444, NULL, NULL, NULL, NULL, NULL, &OpenSavedGameCallback_t2349091444_0_0_0 } /* EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/OpenSavedGameCallback */,
	{ DelegatePInvokeWrapper_ResolveConflictingSavedGamesCallback_t4045204290, NULL, NULL, NULL, NULL, NULL, &ResolveConflictingSavedGamesCallback_t4045204290_0_0_0 } /* EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/ResolveConflictingSavedGamesCallback */,
	{ DelegatePInvokeWrapper_SaveGameDataCallback_t3612996042, NULL, NULL, NULL, NULL, NULL, &SaveGameDataCallback_t3612996042_0_0_0 } /* EasyMobile.Internal.GameServices.iOS.iOSSavedGameNative/SaveGameDataCallback */,
	{ DelegatePInvokeWrapper_GifExportCompletedDelegate_t4082577070, NULL, NULL, NULL, NULL, NULL, &GifExportCompletedDelegate_t4082577070_0_0_0 } /* EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportCompletedDelegate */,
	{ DelegatePInvokeWrapper_GifExportProgressDelegate_t324897231, NULL, NULL, NULL, NULL, NULL, &GifExportProgressDelegate_t324897231_0_0_0 } /* EasyMobile.Internal.Gif.iOS.iOSNativeGif/GifExportProgressDelegate */,
	{ DelegatePInvokeWrapper_NativeNotificationHandler_t1892928298, NULL, NULL, NULL, NULL, NULL, &NativeNotificationHandler_t1892928298_0_0_0 } /* EasyMobile.Internal.Notifications.NativeNotificationHandler */,
	{ NULL, iOSNotificationAction_t1537218362_marshal_pinvoke, iOSNotificationAction_t1537218362_marshal_pinvoke_back, iOSNotificationAction_t1537218362_marshal_pinvoke_cleanup, NULL, NULL, &iOSNotificationAction_t1537218362_0_0_0 } /* EasyMobile.Internal.Notifications.iOS.iOSNotificationAction */,
	{ NULL, iOSNotificationCategory_t4187379170_marshal_pinvoke, iOSNotificationCategory_t4187379170_marshal_pinvoke_back, iOSNotificationCategory_t4187379170_marshal_pinvoke_cleanup, NULL, NULL, &iOSNotificationCategory_t4187379170_0_0_0 } /* EasyMobile.Internal.Notifications.iOS.iOSNotificationCategory */,
	{ NULL, iOSNotificationContent_t1040386259_marshal_pinvoke, iOSNotificationContent_t1040386259_marshal_pinvoke_back, iOSNotificationContent_t1040386259_marshal_pinvoke_cleanup, NULL, NULL, &iOSNotificationContent_t1040386259_0_0_0 } /* EasyMobile.Internal.Notifications.iOS.iOSNotificationContent */,
	{ NULL, iOSNotificationListenerInfo_t996772639_marshal_pinvoke, iOSNotificationListenerInfo_t996772639_marshal_pinvoke_back, iOSNotificationListenerInfo_t996772639_marshal_pinvoke_cleanup, NULL, NULL, &iOSNotificationListenerInfo_t996772639_0_0_0 } /* EasyMobile.Internal.Notifications.iOS.iOSNotificationListenerInfo */,
	{ DelegatePInvokeWrapper_GetNotificationResponseCallback_t3346532913, NULL, NULL, NULL, NULL, NULL, &GetNotificationResponseCallback_t3346532913_0_0_0 } /* EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetNotificationResponseCallback */,
	{ DelegatePInvokeWrapper_GetPendingNotificationRequestsCallback_t3407937767, NULL, NULL, NULL, NULL, NULL, &GetPendingNotificationRequestsCallback_t3407937767_0_0_0 } /* EasyMobile.Internal.Notifications.iOS.iOSNotificationNative/GetPendingNotificationRequestsCallback */,
	{ NULL, IconName_t2443857936_marshal_pinvoke, IconName_t2443857936_marshal_pinvoke_back, IconName_t2443857936_marshal_pinvoke_cleanup, NULL, NULL, &IconName_t2443857936_0_0_0 } /* EasyMobile.Internal.Privacy.EditorConsentDialogClickableText/IconName */,
	{ NULL, iOSConsentDialogListenerInfo_t124266453_marshal_pinvoke, iOSConsentDialogListenerInfo_t124266453_marshal_pinvoke_back, iOSConsentDialogListenerInfo_t124266453_marshal_pinvoke_cleanup, NULL, NULL, &iOSConsentDialogListenerInfo_t124266453_0_0_0 } /* EasyMobile.Internal.Privacy.iOSConsentDialog/iOSConsentDialogListenerInfo */,
	{ NULL, ShareData_t888004449_marshal_pinvoke, ShareData_t888004449_marshal_pinvoke_back, ShareData_t888004449_marshal_pinvoke_cleanup, NULL, NULL, &ShareData_t888004449_0_0_0 } /* EasyMobile.Internal.Sharing.iOS.iOSNativeShare/ShareData */,
	{ NULL, ActionButton_t554814726_marshal_pinvoke, ActionButton_t554814726_marshal_pinvoke_back, ActionButton_t554814726_marshal_pinvoke_cleanup, NULL, NULL, &ActionButton_t554814726_0_0_0 } /* EasyMobile.NotificationCategory/ActionButton */,
	{ NULL, SavedGameInfoUpdate_t1021132738_marshal_pinvoke, SavedGameInfoUpdate_t1021132738_marshal_pinvoke_back, SavedGameInfoUpdate_t1021132738_marshal_pinvoke_cleanup, NULL, NULL, &SavedGameInfoUpdate_t1021132738_0_0_0 } /* EasyMobile.SavedGameInfoUpdate */,
	{ NULL, Builder_t2048762101_marshal_pinvoke, Builder_t2048762101_marshal_pinvoke_back, Builder_t2048762101_marshal_pinvoke_cleanup, NULL, NULL, &Builder_t2048762101_0_0_0 } /* EasyMobile.SavedGameInfoUpdate/Builder */,
	{ NULL, AdvertisingResult_t1229207569_marshal_pinvoke, AdvertisingResult_t1229207569_marshal_pinvoke_back, AdvertisingResult_t1229207569_marshal_pinvoke_cleanup, NULL, NULL, &AdvertisingResult_t1229207569_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.AdvertisingResult */,
	{ NULL, ConnectionRequest_t684574500_marshal_pinvoke, ConnectionRequest_t684574500_marshal_pinvoke_back, ConnectionRequest_t684574500_marshal_pinvoke_cleanup, NULL, NULL, &ConnectionRequest_t684574500_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.ConnectionRequest */,
	{ NULL, ConnectionResponse_t735328601_marshal_pinvoke, ConnectionResponse_t735328601_marshal_pinvoke_back, ConnectionResponse_t735328601_marshal_pinvoke_cleanup, NULL, NULL, &ConnectionResponse_t735328601_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.ConnectionResponse */,
	{ NULL, EndpointDetails_t3891698496_marshal_pinvoke, EndpointDetails_t3891698496_marshal_pinvoke_back, EndpointDetails_t3891698496_marshal_pinvoke_cleanup, NULL, NULL, &EndpointDetails_t3891698496_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.EndpointDetails */,
	{ NULL, NearbyConnectionConfiguration_t2019425596_marshal_pinvoke, NearbyConnectionConfiguration_t2019425596_marshal_pinvoke_back, NearbyConnectionConfiguration_t2019425596_marshal_pinvoke_cleanup, NULL, NULL, &NearbyConnectionConfiguration_t2019425596_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration */,
	{ NULL, SavedGameMetadataUpdate_t1775293339_marshal_pinvoke, SavedGameMetadataUpdate_t1775293339_marshal_pinvoke_back, SavedGameMetadataUpdate_t1775293339_marshal_pinvoke_cleanup, NULL, NULL, &SavedGameMetadataUpdate_t1775293339_0_0_0 } /* GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate */,
	{ NULL, Builder_t140438593_marshal_pinvoke, Builder_t140438593_marshal_pinvoke_back, Builder_t140438593_marshal_pinvoke_cleanup, NULL, NULL, &Builder_t140438593_0_0_0 } /* GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder */,
	{ NULL, PoolPrefabs_t1376732323_marshal_pinvoke, PoolPrefabs_t1376732323_marshal_pinvoke_back, PoolPrefabs_t1376732323_marshal_pinvoke_cleanup, NULL, NULL, &PoolPrefabs_t1376732323_0_0_0 } /* ObjectPool/PoolPrefabs */,
	NULL,
};
