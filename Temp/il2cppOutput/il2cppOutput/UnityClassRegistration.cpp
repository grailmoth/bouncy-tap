template <typename T> void RegisterUnityClass(const char*);
template <typename T> void RegisterStrippedType(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_SharedInternals();
	RegisterModule_SharedInternals();

	void RegisterModule_Core();
	RegisterModule_Core();

	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_CloudWebServices();
	RegisterModule_CloudWebServices();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_UnityAnalytics();
	RegisterModule_UnityAnalytics();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_XR();
	RegisterModule_XR();

	void RegisterModule_ImageConversion();
	RegisterModule_ImageConversion();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

	void RegisterModule_CrashReporting();
	RegisterModule_CrashReporting();

	void RegisterModule_PerformanceReporting();
	RegisterModule_PerformanceReporting();

	void RegisterModule_UNET();
	RegisterModule_UNET();

	void RegisterModule_GameCenter();
	RegisterModule_GameCenter();

	void RegisterModule_Input();
	RegisterModule_Input();

	void RegisterModule_VR();
	RegisterModule_VR();

	void RegisterModule_Wind();
	RegisterModule_Wind();

	void RegisterModule_TLS();
	RegisterModule_TLS();

}

class EditorExtension; template <> void RegisterUnityClass<EditorExtension>(const char*);
namespace Unity { class Component; } template <> void RegisterUnityClass<Unity::Component>(const char*);
class Behaviour; template <> void RegisterUnityClass<Behaviour>(const char*);
class Animation; 
class Animator; template <> void RegisterUnityClass<Animator>(const char*);
class AudioBehaviour; template <> void RegisterUnityClass<AudioBehaviour>(const char*);
class AudioListener; template <> void RegisterUnityClass<AudioListener>(const char*);
class AudioSource; template <> void RegisterUnityClass<AudioSource>(const char*);
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterUnityClass<Camera>(const char*);
namespace UI { class Canvas; } template <> void RegisterUnityClass<UI::Canvas>(const char*);
namespace UI { class CanvasGroup; } template <> void RegisterUnityClass<UI::CanvasGroup>(const char*);
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterUnityClass<Collider2D>(const char*);
class BoxCollider2D; template <> void RegisterUnityClass<BoxCollider2D>(const char*);
class CapsuleCollider2D; 
class CircleCollider2D; template <> void RegisterUnityClass<CircleCollider2D>(const char*);
class CompositeCollider2D; 
class EdgeCollider2D; 
class PolygonCollider2D; template <> void RegisterUnityClass<PolygonCollider2D>(const char*);
class TilemapCollider2D; 
class ConstantForce; 
class Effector2D; template <> void RegisterUnityClass<Effector2D>(const char*);
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; template <> void RegisterUnityClass<PlatformEffector2D>(const char*);
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; 
class GUIElement; template <> void RegisterUnityClass<GUIElement>(const char*);
namespace TextRenderingPrivate { class GUIText; } 
class GUITexture; 
class GUILayer; template <> void RegisterUnityClass<GUILayer>(const char*);
class GridLayout; 
class Grid; 
class Tilemap; 
class Halo; 
class HaloLayer; 
class IConstraint; 
class AimConstraint; 
class LookAtConstraint; 
class ParentConstraint; 
class PositionConstraint; 
class RotationConstraint; 
class ScaleConstraint; 
class Joint2D; 
class AnchoredJoint2D; 
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; 
class SliderJoint2D; 
class SpringJoint2D; 
class WheelJoint2D; 
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterUnityClass<Light>(const char*);
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterUnityClass<MonoBehaviour>(const char*);
class NavMeshAgent; 
class NavMeshObstacle; 
class OffMeshLink; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class PlayableDirector; 
class Projector; 
class ReflectionProbe; template <> void RegisterUnityClass<ReflectionProbe>(const char*);
class Skybox; 
class SortingGroup; 
class StreamingController; 
class Terrain; 
class VideoPlayer; 
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterUnityClass<UI::CanvasRenderer>(const char*);
class Collider; template <> void RegisterUnityClass<Collider>(const char*);
class BoxCollider; template <> void RegisterUnityClass<BoxCollider>(const char*);
class CapsuleCollider; 
class CharacterController; template <> void RegisterUnityClass<CharacterController>(const char*);
class MeshCollider; template <> void RegisterUnityClass<MeshCollider>(const char*);
class SphereCollider; template <> void RegisterUnityClass<SphereCollider>(const char*);
class TerrainCollider; 
class WheelCollider; 
namespace Unity { class Joint; } 
namespace Unity { class CharacterJoint; } 
namespace Unity { class ConfigurableJoint; } 
namespace Unity { class FixedJoint; } 
namespace Unity { class HingeJoint; } 
namespace Unity { class SpringJoint; } 
class LODGroup; 
class MeshFilter; template <> void RegisterUnityClass<MeshFilter>(const char*);
class OcclusionArea; 
class OcclusionPortal; 
class ParticleAnimator; 
class ParticleEmitter; 
class EllipsoidParticleEmitter; 
class MeshParticleEmitter; 
class ParticleSystem; template <> void RegisterUnityClass<ParticleSystem>(const char*);
class Renderer; template <> void RegisterUnityClass<Renderer>(const char*);
class BillboardRenderer; 
class LineRenderer; template <> void RegisterUnityClass<LineRenderer>(const char*);
class MeshRenderer; template <> void RegisterUnityClass<MeshRenderer>(const char*);
class ParticleRenderer; 
class ParticleSystemRenderer; template <> void RegisterUnityClass<ParticleSystemRenderer>(const char*);
class SkinnedMeshRenderer; 
class SpriteMask; 
class SpriteRenderer; template <> void RegisterUnityClass<SpriteRenderer>(const char*);
class SpriteShapeRenderer; 
class TilemapRenderer; 
class TrailRenderer; 
class Rigidbody; template <> void RegisterUnityClass<Rigidbody>(const char*);
class Rigidbody2D; template <> void RegisterUnityClass<Rigidbody2D>(const char*);
namespace TextRenderingPrivate { class TextMesh; } 
class Transform; template <> void RegisterUnityClass<Transform>(const char*);
namespace UI { class RectTransform; } template <> void RegisterUnityClass<UI::RectTransform>(const char*);
class Tree; 
class WorldAnchor; 
class WorldParticleCollider; 
class GameObject; template <> void RegisterUnityClass<GameObject>(const char*);
class NamedObject; template <> void RegisterUnityClass<NamedObject>(const char*);
class AssetBundle; 
class AssetBundleManifest; 
class ScriptedImporter; 
class AssetImporterLog; 
class AudioMixer; 
class AudioMixerController; 
class AudioMixerGroup; 
class AudioMixerGroupController; 
class AudioMixerSnapshot; 
class AudioMixerSnapshotController; 
class Avatar; 
class AvatarMask; 
class BillboardAsset; 
class ComputeShader; template <> void RegisterUnityClass<ComputeShader>(const char*);
class Flare; 
namespace TextRendering { class Font; } template <> void RegisterUnityClass<TextRendering::Font>(const char*);
class GameObjectRecorder; 
class LightProbes; 
class LocalizationAsset; 
class Material; template <> void RegisterUnityClass<Material>(const char*);
class ProceduralMaterial; 
class Mesh; template <> void RegisterUnityClass<Mesh>(const char*);
class Motion; template <> void RegisterUnityClass<Motion>(const char*);
class AnimationClip; template <> void RegisterUnityClass<AnimationClip>(const char*);
class PreviewAnimationClip; 
class NavMeshData; 
class OcclusionCullingData; 
class PhysicMaterial; 
class PhysicsMaterial2D; 
class PreloadData; template <> void RegisterUnityClass<PreloadData>(const char*);
class RuntimeAnimatorController; template <> void RegisterUnityClass<RuntimeAnimatorController>(const char*);
class AnimatorController; template <> void RegisterUnityClass<AnimatorController>(const char*);
class AnimatorOverrideController; template <> void RegisterUnityClass<AnimatorOverrideController>(const char*);
class SampleClip; template <> void RegisterUnityClass<SampleClip>(const char*);
class AudioClip; template <> void RegisterUnityClass<AudioClip>(const char*);
class Shader; template <> void RegisterUnityClass<Shader>(const char*);
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterUnityClass<Sprite>(const char*);
class SpriteAtlas; template <> void RegisterUnityClass<SpriteAtlas>(const char*);
class SubstanceArchive; 
class TerrainData; 
class TextAsset; template <> void RegisterUnityClass<TextAsset>(const char*);
class CGProgram; 
class MonoScript; template <> void RegisterUnityClass<MonoScript>(const char*);
class Texture; template <> void RegisterUnityClass<Texture>(const char*);
class BaseVideoTexture; 
class MovieTexture; 
class WebCamTexture; 
class CubemapArray; template <> void RegisterUnityClass<CubemapArray>(const char*);
class LowerResBlitTexture; template <> void RegisterUnityClass<LowerResBlitTexture>(const char*);
class ProceduralTexture; 
class RenderTexture; template <> void RegisterUnityClass<RenderTexture>(const char*);
class CustomRenderTexture; 
class SparseTexture; 
class Texture2D; template <> void RegisterUnityClass<Texture2D>(const char*);
class Cubemap; template <> void RegisterUnityClass<Cubemap>(const char*);
class Texture2DArray; template <> void RegisterUnityClass<Texture2DArray>(const char*);
class Texture3D; template <> void RegisterUnityClass<Texture3D>(const char*);
class VideoClip; 
class GameManager; template <> void RegisterUnityClass<GameManager>(const char*);
class GlobalGameManager; template <> void RegisterUnityClass<GlobalGameManager>(const char*);
class AudioManager; template <> void RegisterUnityClass<AudioManager>(const char*);
class BuildSettings; template <> void RegisterUnityClass<BuildSettings>(const char*);
class CloudWebServicesManager; template <> void RegisterUnityClass<CloudWebServicesManager>(const char*);
class CrashReportManager; template <> void RegisterUnityClass<CrashReportManager>(const char*);
class DelayedCallManager; template <> void RegisterUnityClass<DelayedCallManager>(const char*);
class GraphicsSettings; template <> void RegisterUnityClass<GraphicsSettings>(const char*);
class InputManager; template <> void RegisterUnityClass<InputManager>(const char*);
class MonoManager; template <> void RegisterUnityClass<MonoManager>(const char*);
class NavMeshProjectSettings; 
class PerformanceReportingManager; template <> void RegisterUnityClass<PerformanceReportingManager>(const char*);
class Physics2DSettings; template <> void RegisterUnityClass<Physics2DSettings>(const char*);
class PhysicsManager; template <> void RegisterUnityClass<PhysicsManager>(const char*);
class PlayerSettings; template <> void RegisterUnityClass<PlayerSettings>(const char*);
class QualitySettings; template <> void RegisterUnityClass<QualitySettings>(const char*);
class ResourceManager; template <> void RegisterUnityClass<ResourceManager>(const char*);
class RuntimeInitializeOnLoadManager; template <> void RegisterUnityClass<RuntimeInitializeOnLoadManager>(const char*);
class ScriptMapper; template <> void RegisterUnityClass<ScriptMapper>(const char*);
class StreamingManager; 
class TagManager; template <> void RegisterUnityClass<TagManager>(const char*);
class TimeManager; template <> void RegisterUnityClass<TimeManager>(const char*);
class UnityAnalyticsManager; template <> void RegisterUnityClass<UnityAnalyticsManager>(const char*);
class UnityConnectSettings; template <> void RegisterUnityClass<UnityConnectSettings>(const char*);
class LevelGameManager; template <> void RegisterUnityClass<LevelGameManager>(const char*);
class LightmapSettings; template <> void RegisterUnityClass<LightmapSettings>(const char*);
class NavMeshSettings; 
class OcclusionCullingSettings; 
class RenderSettings; template <> void RegisterUnityClass<RenderSettings>(const char*);
class RenderPassAttachment; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 90 non stripped classes
	//0. ParticleSystem
	RegisterUnityClass<ParticleSystem>("ParticleSystem");
	//1. Unity::Component
	RegisterUnityClass<Unity::Component>("Core");
	//2. EditorExtension
	RegisterUnityClass<EditorExtension>("Core");
	//3. ParticleSystemRenderer
	RegisterUnityClass<ParticleSystemRenderer>("ParticleSystem");
	//4. Renderer
	RegisterUnityClass<Renderer>("Core");
	//5. Camera
	RegisterUnityClass<Camera>("Core");
	//6. Behaviour
	RegisterUnityClass<Behaviour>("Core");
	//7. Light
	RegisterUnityClass<Light>("Core");
	//8. Material
	RegisterUnityClass<Material>("Core");
	//9. NamedObject
	RegisterUnityClass<NamedObject>("Core");
	//10. Texture2D
	RegisterUnityClass<Texture2D>("Core");
	//11. Texture
	RegisterUnityClass<Texture>("Core");
	//12. Cubemap
	RegisterUnityClass<Cubemap>("Core");
	//13. Texture3D
	RegisterUnityClass<Texture3D>("Core");
	//14. Texture2DArray
	RegisterUnityClass<Texture2DArray>("Core");
	//15. CubemapArray
	RegisterUnityClass<CubemapArray>("Core");
	//16. RenderTexture
	RegisterUnityClass<RenderTexture>("Core");
	//17. LowerResBlitTexture
	RegisterUnityClass<LowerResBlitTexture>("Core");
	//18. PreloadData
	RegisterUnityClass<PreloadData>("Core");
	//19. GameObject
	RegisterUnityClass<GameObject>("Core");
	//20. QualitySettings
	RegisterUnityClass<QualitySettings>("Core");
	//21. GlobalGameManager
	RegisterUnityClass<GlobalGameManager>("Core");
	//22. GameManager
	RegisterUnityClass<GameManager>("Core");
	//23. LineRenderer
	RegisterUnityClass<LineRenderer>("Core");
	//24. RenderSettings
	RegisterUnityClass<RenderSettings>("Core");
	//25. LevelGameManager
	RegisterUnityClass<LevelGameManager>("Core");
	//26. Shader
	RegisterUnityClass<Shader>("Core");
	//27. MeshFilter
	RegisterUnityClass<MeshFilter>("Core");
	//28. MeshRenderer
	RegisterUnityClass<MeshRenderer>("Core");
	//29. GraphicsSettings
	RegisterUnityClass<GraphicsSettings>("Core");
	//30. GUIElement
	RegisterUnityClass<GUIElement>("Core");
	//31. GUILayer
	RegisterUnityClass<GUILayer>("Core");
	//32. Mesh
	RegisterUnityClass<Mesh>("Core");
	//33. MonoBehaviour
	RegisterUnityClass<MonoBehaviour>("Core");
	//34. ReflectionProbe
	RegisterUnityClass<ReflectionProbe>("Core");
	//35. ComputeShader
	RegisterUnityClass<ComputeShader>("Core");
	//36. TextAsset
	RegisterUnityClass<TextAsset>("Core");
	//37. UI::RectTransform
	RegisterUnityClass<UI::RectTransform>("Core");
	//38. Transform
	RegisterUnityClass<Transform>("Core");
	//39. SpriteRenderer
	RegisterUnityClass<SpriteRenderer>("Core");
	//40. Sprite
	RegisterUnityClass<Sprite>("Core");
	//41. SpriteAtlas
	RegisterUnityClass<SpriteAtlas>("Core");
	//42. Rigidbody
	RegisterUnityClass<Rigidbody>("Physics");
	//43. Collider
	RegisterUnityClass<Collider>("Physics");
	//44. CharacterController
	RegisterUnityClass<CharacterController>("Physics");
	//45. MeshCollider
	RegisterUnityClass<MeshCollider>("Physics");
	//46. BoxCollider
	RegisterUnityClass<BoxCollider>("Physics");
	//47. SphereCollider
	RegisterUnityClass<SphereCollider>("Physics");
	//48. Animator
	RegisterUnityClass<Animator>("Animation");
	//49. AnimatorOverrideController
	RegisterUnityClass<AnimatorOverrideController>("Animation");
	//50. RuntimeAnimatorController
	RegisterUnityClass<RuntimeAnimatorController>("Animation");
	//51. UI::Canvas
	RegisterUnityClass<UI::Canvas>("UI");
	//52. UI::CanvasGroup
	RegisterUnityClass<UI::CanvasGroup>("UI");
	//53. UI::CanvasRenderer
	RegisterUnityClass<UI::CanvasRenderer>("UI");
	//54. TextRendering::Font
	RegisterUnityClass<TextRendering::Font>("TextRendering");
	//55. AudioClip
	RegisterUnityClass<AudioClip>("Audio");
	//56. SampleClip
	RegisterUnityClass<SampleClip>("Audio");
	//57. AudioListener
	RegisterUnityClass<AudioListener>("Audio");
	//58. AudioBehaviour
	RegisterUnityClass<AudioBehaviour>("Audio");
	//59. AudioSource
	RegisterUnityClass<AudioSource>("Audio");
	//60. Rigidbody2D
	RegisterUnityClass<Rigidbody2D>("Physics2D");
	//61. Collider2D
	RegisterUnityClass<Collider2D>("Physics2D");
	//62. PlatformEffector2D
	RegisterUnityClass<PlatformEffector2D>("Physics2D");
	//63. Effector2D
	RegisterUnityClass<Effector2D>("Physics2D");
	//64. TagManager
	RegisterUnityClass<TagManager>("Core");
	//65. DelayedCallManager
	RegisterUnityClass<DelayedCallManager>("Core");
	//66. InputManager
	RegisterUnityClass<InputManager>("Core");
	//67. TimeManager
	RegisterUnityClass<TimeManager>("Core");
	//68. BuildSettings
	RegisterUnityClass<BuildSettings>("Core");
	//69. PlayerSettings
	RegisterUnityClass<PlayerSettings>("Core");
	//70. ResourceManager
	RegisterUnityClass<ResourceManager>("Core");
	//71. RuntimeInitializeOnLoadManager
	RegisterUnityClass<RuntimeInitializeOnLoadManager>("Core");
	//72. ScriptMapper
	RegisterUnityClass<ScriptMapper>("Core");
	//73. PhysicsManager
	RegisterUnityClass<PhysicsManager>("Physics");
	//74. MonoManager
	RegisterUnityClass<MonoManager>("Core");
	//75. MonoScript
	RegisterUnityClass<MonoScript>("Core");
	//76. AudioManager
	RegisterUnityClass<AudioManager>("Audio");
	//77. UnityConnectSettings
	RegisterUnityClass<UnityConnectSettings>("UnityConnect");
	//78. CloudWebServicesManager
	RegisterUnityClass<CloudWebServicesManager>("CloudWebServices");
	//79. CrashReportManager
	RegisterUnityClass<CrashReportManager>("CrashReporting");
	//80. PerformanceReportingManager
	RegisterUnityClass<PerformanceReportingManager>("PerformanceReporting");
	//81. Physics2DSettings
	RegisterUnityClass<Physics2DSettings>("Physics2D");
	//82. UnityAnalyticsManager
	RegisterUnityClass<UnityAnalyticsManager>("UnityAnalytics");
	//83. Motion
	RegisterUnityClass<Motion>("Animation");
	//84. AnimationClip
	RegisterUnityClass<AnimationClip>("Animation");
	//85. AnimatorController
	RegisterUnityClass<AnimatorController>("Animation");
	//86. LightmapSettings
	RegisterUnityClass<LightmapSettings>("Core");
	//87. PolygonCollider2D
	RegisterUnityClass<PolygonCollider2D>("Physics2D");
	//88. CircleCollider2D
	RegisterUnityClass<CircleCollider2D>("Physics2D");
	//89. BoxCollider2D
	RegisterUnityClass<BoxCollider2D>("Physics2D");

}
