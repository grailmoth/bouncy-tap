void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.Advertisements.UnityAdsSettings

		//System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_enabled()
		void Register_UnityEngine_Advertisements_UnityAdsSettings_get_enabled();
		Register_UnityEngine_Advertisements_UnityAdsSettings_get_enabled();

		//System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_initializeOnStartup()
		void Register_UnityEngine_Advertisements_UnityAdsSettings_get_initializeOnStartup();
		Register_UnityEngine_Advertisements_UnityAdsSettings_get_initializeOnStartup();

		//System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_testMode()
		void Register_UnityEngine_Advertisements_UnityAdsSettings_get_testMode();
		Register_UnityEngine_Advertisements_UnityAdsSettings_get_testMode();

		//System.String UnityEngine.Advertisements.UnityAdsSettings::GetGameId(UnityEngine.RuntimePlatform)
		void Register_UnityEngine_Advertisements_UnityAdsSettings_GetGameId();
		Register_UnityEngine_Advertisements_UnityAdsSettings_GetGameId();

	//End Registrations for type : UnityEngine.Advertisements.UnityAdsSettings

	//Start Registrations for type : UnityEngine.Analytics.AnalyticsSessionInfo

		//System.Int64 UnityEngine.Analytics.AnalyticsSessionInfo::get_sessionId()
		void Register_UnityEngine_Analytics_AnalyticsSessionInfo_get_sessionId();
		Register_UnityEngine_Analytics_AnalyticsSessionInfo_get_sessionId();

		//System.String UnityEngine.Analytics.AnalyticsSessionInfo::get_userId()
		void Register_UnityEngine_Analytics_AnalyticsSessionInfo_get_userId();
		Register_UnityEngine_Analytics_AnalyticsSessionInfo_get_userId();

	//End Registrations for type : UnityEngine.Analytics.AnalyticsSessionInfo

	//Start Registrations for type : UnityEngine.Analytics.CustomEventData

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddBool(System.String,System.Boolean)
		void Register_UnityEngine_Analytics_CustomEventData_AddBool();
		Register_UnityEngine_Analytics_CustomEventData_AddBool();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddDouble(System.String,System.Double)
		void Register_UnityEngine_Analytics_CustomEventData_AddDouble();
		Register_UnityEngine_Analytics_CustomEventData_AddDouble();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt32(System.String,System.Int32)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt32();
		Register_UnityEngine_Analytics_CustomEventData_AddInt32();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt64(System.String,System.Int64)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt64();
		Register_UnityEngine_Analytics_CustomEventData_AddInt64();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddString(System.String,System.String)
		void Register_UnityEngine_Analytics_CustomEventData_AddString();
		Register_UnityEngine_Analytics_CustomEventData_AddString();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt32(System.String,System.UInt32)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt32();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt32();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt64(System.String,System.UInt64)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt64();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt64();

		//System.IntPtr UnityEngine.Analytics.CustomEventData::Internal_Create(UnityEngine.Analytics.CustomEventData,System.String)
		void Register_UnityEngine_Analytics_CustomEventData_Internal_Create();
		Register_UnityEngine_Analytics_CustomEventData_Internal_Create();

		//System.Void UnityEngine.Analytics.CustomEventData::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_Analytics_CustomEventData_Internal_Destroy();
		Register_UnityEngine_Analytics_CustomEventData_Internal_Destroy();

	//End Registrations for type : UnityEngine.Analytics.CustomEventData

	//Start Registrations for type : UnityEngine.Analytics.PerformanceReporting

		//System.Boolean UnityEngine.Analytics.PerformanceReporting::get_enabled()
		void Register_UnityEngine_Analytics_PerformanceReporting_get_enabled();
		Register_UnityEngine_Analytics_PerformanceReporting_get_enabled();

		//System.Void UnityEngine.Analytics.PerformanceReporting::set_enabled(System.Boolean)
		void Register_UnityEngine_Analytics_PerformanceReporting_set_enabled();
		Register_UnityEngine_Analytics_PerformanceReporting_set_enabled();

	//End Registrations for type : UnityEngine.Analytics.PerformanceReporting

	//Start Registrations for type : UnityEngine.Analytics.UnityAnalyticsHandler

		//System.Boolean UnityEngine.Analytics.UnityAnalyticsHandler::get_deviceStatsEnabled()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_get_deviceStatsEnabled();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_get_deviceStatsEnabled();

		//System.Boolean UnityEngine.Analytics.UnityAnalyticsHandler::get_enabled()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_get_enabled();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_get_enabled();

		//System.Boolean UnityEngine.Analytics.UnityAnalyticsHandler::get_limitUserTracking()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_get_limitUserTracking();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_get_limitUserTracking();

		//System.IntPtr UnityEngine.Analytics.UnityAnalyticsHandler::Internal_Create(UnityEngine.Analytics.UnityAnalyticsHandler)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_Internal_Create();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_Internal_Create();

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_Internal_Destroy();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_Internal_Destroy();

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::set_deviceStatsEnabled(System.Boolean)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_set_deviceStatsEnabled();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_set_deviceStatsEnabled();

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::set_enabled(System.Boolean)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_set_enabled();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_set_enabled();

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::set_limitUserTracking(System.Boolean)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_set_limitUserTracking();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_set_limitUserTracking();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::SendCustomEvent(UnityEngine.Analytics.CustomEventData)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEvent();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEvent();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::SendCustomEventName(System.String)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEventName();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEventName();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::Transaction(System.String,System.Double,System.String,System.String,System.String,System.Boolean)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_Transaction();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_Transaction();

	//End Registrations for type : UnityEngine.Analytics.UnityAnalyticsHandler

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Int32 UnityEngine.AnimationCurve::AddKey_Internal_Injected(UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_AddKey_Internal_Injected();
		Register_UnityEngine_AnimationCurve_AddKey_Internal_Injected();

		//System.Int32 UnityEngine.AnimationCurve::get_length()
		void Register_UnityEngine_AnimationCurve_get_length();
		Register_UnityEngine_AnimationCurve_get_length();

		//System.IntPtr UnityEngine.AnimationCurve::Internal_Create(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Internal_Create();
		Register_UnityEngine_AnimationCurve_Internal_Create();

		//System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
		void Register_UnityEngine_AnimationCurve_Evaluate();
		Register_UnityEngine_AnimationCurve_Evaluate();

		//System.Void UnityEngine.AnimationCurve::GetKey_Injected(System.Int32,UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_GetKey_Injected();
		Register_UnityEngine_AnimationCurve_GetKey_Injected();

		//System.Void UnityEngine.AnimationCurve::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_AnimationCurve_Internal_Destroy();
		Register_UnityEngine_AnimationCurve_Internal_Destroy();

		//System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_SetKeys();
		Register_UnityEngine_AnimationCurve_SetKeys();

		//System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_postWrapMode();
		Register_UnityEngine_AnimationCurve_set_postWrapMode();

		//System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_preWrapMode();
		Register_UnityEngine_AnimationCurve_set_preWrapMode();

		//UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
		void Register_UnityEngine_AnimationCurve_GetKeys();
		Register_UnityEngine_AnimationCurve_GetKeys();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
		void Register_UnityEngine_AnimationCurve_get_postWrapMode();
		Register_UnityEngine_AnimationCurve_get_postWrapMode();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
		void Register_UnityEngine_AnimationCurve_get_preWrapMode();
		Register_UnityEngine_AnimationCurve_get_preWrapMode();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.Animator

		//System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
		void Register_UnityEngine_Animator_GetBoolID();
		Register_UnityEngine_Animator_GetBoolID();

		//System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
		void Register_UnityEngine_Animator_IsInTransition();
		Register_UnityEngine_Animator_IsInTransition();

		//System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
		void Register_UnityEngine_Animator_get_hasBoundPlayables();
		Register_UnityEngine_Animator_get_hasBoundPlayables();

		//System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
		void Register_UnityEngine_Animator_GetIntegerID();
		Register_UnityEngine_Animator_GetIntegerID();

		//System.Int32 UnityEngine.Animator::StringToHash(System.String)
		void Register_UnityEngine_Animator_StringToHash();
		Register_UnityEngine_Animator_StringToHash();

		//System.Single UnityEngine.Animator::GetFloatID(System.Int32)
		void Register_UnityEngine_Animator_GetFloatID();
		Register_UnityEngine_Animator_GetFloatID();

		//System.Void UnityEngine.Animator::GetAnimatorStateInfo(System.Int32,UnityEngine.StateInfoIndex,UnityEngine.AnimatorStateInfo&)
		void Register_UnityEngine_Animator_GetAnimatorStateInfo();
		Register_UnityEngine_Animator_GetAnimatorStateInfo();

		//System.Void UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32,UnityEngine.AnimatorTransitionInfo&)
		void Register_UnityEngine_Animator_GetAnimatorTransitionInfo();
		Register_UnityEngine_Animator_GetAnimatorTransitionInfo();

		//System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Animator_Play();
		Register_UnityEngine_Animator_Play();

		//System.Void UnityEngine.Animator::ResetTriggerString(System.String)
		void Register_UnityEngine_Animator_ResetTriggerString();
		Register_UnityEngine_Animator_ResetTriggerString();

		//System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolID();
		Register_UnityEngine_Animator_SetBoolID();

		//System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolString();
		Register_UnityEngine_Animator_SetBoolString();

		//System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
		void Register_UnityEngine_Animator_SetFloatID();
		Register_UnityEngine_Animator_SetFloatID();

		//System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
		void Register_UnityEngine_Animator_SetIntegerID();
		Register_UnityEngine_Animator_SetIntegerID();

		//System.Void UnityEngine.Animator::SetTriggerID(System.Int32)
		void Register_UnityEngine_Animator_SetTriggerID();
		Register_UnityEngine_Animator_SetTriggerID();

		//System.Void UnityEngine.Animator::SetTriggerString(System.String)
		void Register_UnityEngine_Animator_SetTriggerString();
		Register_UnityEngine_Animator_SetTriggerString();

		//UnityEngine.AnimatorControllerParameter[] UnityEngine.Animator::get_parameters()
		void Register_UnityEngine_Animator_get_parameters();
		Register_UnityEngine_Animator_get_parameters();

	//End Registrations for type : UnityEngine.Animator

	//Start Registrations for type : UnityEngine.Application

		//System.Boolean UnityEngine.Application::get_isPlaying()
		void Register_UnityEngine_Application_get_isPlaying();
		Register_UnityEngine_Application_get_isPlaying();

		//System.String UnityEngine.Application::get_cloudProjectId()
		void Register_UnityEngine_Application_get_cloudProjectId();
		Register_UnityEngine_Application_get_cloudProjectId();

		//System.String UnityEngine.Application::get_dataPath()
		void Register_UnityEngine_Application_get_dataPath();
		Register_UnityEngine_Application_get_dataPath();

		//System.String UnityEngine.Application::get_identifier()
		void Register_UnityEngine_Application_get_identifier();
		Register_UnityEngine_Application_get_identifier();

		//System.String UnityEngine.Application::get_persistentDataPath()
		void Register_UnityEngine_Application_get_persistentDataPath();
		Register_UnityEngine_Application_get_persistentDataPath();

		//System.String UnityEngine.Application::get_productName()
		void Register_UnityEngine_Application_get_productName();
		Register_UnityEngine_Application_get_productName();

		//System.String UnityEngine.Application::get_unityVersion()
		void Register_UnityEngine_Application_get_unityVersion();
		Register_UnityEngine_Application_get_unityVersion();

		//System.String UnityEngine.Application::get_version()
		void Register_UnityEngine_Application_get_version();
		Register_UnityEngine_Application_get_version();

		//System.Void UnityEngine.Application::OpenURL(System.String)
		void Register_UnityEngine_Application_OpenURL();
		Register_UnityEngine_Application_OpenURL();

		//System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
		void Register_UnityEngine_Application_set_runInBackground();
		Register_UnityEngine_Application_set_runInBackground();

		//System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
		void Register_UnityEngine_Application_set_targetFrameRate();
		Register_UnityEngine_Application_set_targetFrameRate();

		//UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
		void Register_UnityEngine_Application_get_platform();
		Register_UnityEngine_Application_get_platform();

	//End Registrations for type : UnityEngine.Application

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Boolean UnityEngine.AsyncOperation::get_isDone()
		void Register_UnityEngine_AsyncOperation_get_isDone();
		Register_UnityEngine_AsyncOperation_get_isDone();

		//System.Void UnityEngine.AsyncOperation::InternalDestroy(System.IntPtr)
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

		//System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
		void Register_UnityEngine_AsyncOperation_set_allowSceneActivation();
		Register_UnityEngine_AsyncOperation_set_allowSceneActivation();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.AudioClip

		//System.Boolean UnityEngine.AudioClip::get_ambisonic()
		void Register_UnityEngine_AudioClip_get_ambisonic();
		Register_UnityEngine_AudioClip_get_ambisonic();

	//End Registrations for type : UnityEngine.AudioClip

	//Start Registrations for type : UnityEngine.AudioExtensionManager

		//UnityEngine.Object UnityEngine.AudioExtensionManager::GetAudioListener()
		void Register_UnityEngine_AudioExtensionManager_GetAudioListener();
		Register_UnityEngine_AudioExtensionManager_GetAudioListener();

	//End Registrations for type : UnityEngine.AudioExtensionManager

	//Start Registrations for type : UnityEngine.AudioListener

		//System.Int32 UnityEngine.AudioListener::GetNumExtensionProperties()
		void Register_UnityEngine_AudioListener_GetNumExtensionProperties();
		Register_UnityEngine_AudioListener_GetNumExtensionProperties();

		//System.Single UnityEngine.AudioListener::ReadExtensionPropertyValue(System.Int32)
		void Register_UnityEngine_AudioListener_ReadExtensionPropertyValue();
		Register_UnityEngine_AudioListener_ReadExtensionPropertyValue();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioListener,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ClearExtensionProperties();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ClearExtensionProperties();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionName();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionName();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName();

	//End Registrations for type : UnityEngine.AudioListener

	//Start Registrations for type : UnityEngine.AudioSettings

		//System.String UnityEngine.AudioSettings::GetAmbisonicDecoderPluginName()
		void Register_UnityEngine_AudioSettings_GetAmbisonicDecoderPluginName();
		Register_UnityEngine_AudioSettings_GetAmbisonicDecoderPluginName();

		//System.String UnityEngine.AudioSettings::GetSpatializerPluginName()
		void Register_UnityEngine_AudioSettings_GetSpatializerPluginName();
		Register_UnityEngine_AudioSettings_GetSpatializerPluginName();

	//End Registrations for type : UnityEngine.AudioSettings

	//Start Registrations for type : UnityEngine.AudioSource

		//System.Boolean UnityEngine.AudioSource::get_isPlaying()
		void Register_UnityEngine_AudioSource_get_isPlaying();
		Register_UnityEngine_AudioSource_get_isPlaying();

		//System.Boolean UnityEngine.AudioSource::get_spatializeInternal()
		void Register_UnityEngine_AudioSource_get_spatializeInternal();
		Register_UnityEngine_AudioSource_get_spatializeInternal();

		//System.Int32 UnityEngine.AudioSource::GetNumExtensionProperties()
		void Register_UnityEngine_AudioSource_GetNumExtensionProperties();
		Register_UnityEngine_AudioSource_GetNumExtensionProperties();

		//System.Single UnityEngine.AudioSource::ReadExtensionPropertyValue(System.Int32)
		void Register_UnityEngine_AudioSource_ReadExtensionPropertyValue();
		Register_UnityEngine_AudioSource_ReadExtensionPropertyValue();

		//System.Single UnityEngine.AudioSource::get_volume()
		void Register_UnityEngine_AudioSource_get_volume();
		Register_UnityEngine_AudioSource_get_volume();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioSource,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ClearExtensionProperties();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ClearExtensionProperties();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionName();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionName();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName();

		//System.Void UnityEngine.AudioSource::Play(System.UInt64)
		void Register_UnityEngine_AudioSource_Play();
		Register_UnityEngine_AudioSource_Play();

		//System.Void UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioClip,System.Single)
		void Register_UnityEngine_AudioSource_PlayOneShotHelper();
		Register_UnityEngine_AudioSource_PlayOneShotHelper();

		//System.Void UnityEngine.AudioSource::Stop()
		void Register_UnityEngine_AudioSource_Stop();
		Register_UnityEngine_AudioSource_Stop();

		//System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
		void Register_UnityEngine_AudioSource_set_clip();
		Register_UnityEngine_AudioSource_set_clip();

		//System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
		void Register_UnityEngine_AudioSource_set_loop();
		Register_UnityEngine_AudioSource_set_loop();

		//System.Void UnityEngine.AudioSource::set_volume(System.Single)
		void Register_UnityEngine_AudioSource_set_volume();
		Register_UnityEngine_AudioSource_set_volume();

		//UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
		void Register_UnityEngine_AudioSource_get_clip();
		Register_UnityEngine_AudioSource_get_clip();

	//End Registrations for type : UnityEngine.AudioSource

	//Start Registrations for type : UnityEngine.Behaviour

		//System.Boolean UnityEngine.Behaviour::get_enabled()
		void Register_UnityEngine_Behaviour_get_enabled();
		Register_UnityEngine_Behaviour_get_enabled();

		//System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
		void Register_UnityEngine_Behaviour_get_isActiveAndEnabled();
		Register_UnityEngine_Behaviour_get_isActiveAndEnabled();

		//System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
		void Register_UnityEngine_Behaviour_set_enabled();
		Register_UnityEngine_Behaviour_set_enabled();

	//End Registrations for type : UnityEngine.Behaviour

	//Start Registrations for type : UnityEngine.BoxCollider

		//System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_get_center_Injected();
		Register_UnityEngine_BoxCollider_get_center_Injected();

		//System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_get_size_Injected();
		Register_UnityEngine_BoxCollider_get_size_Injected();

	//End Registrations for type : UnityEngine.BoxCollider

	//Start Registrations for type : UnityEngine.Camera

		//System.Boolean UnityEngine.Camera::get_allowHDR()
		void Register_UnityEngine_Camera_get_allowHDR();
		Register_UnityEngine_Camera_get_allowHDR();

		//System.Boolean UnityEngine.Camera::get_allowMSAA()
		void Register_UnityEngine_Camera_get_allowMSAA();
		Register_UnityEngine_Camera_get_allowMSAA();

		//System.Boolean UnityEngine.Camera::get_orthographic()
		void Register_UnityEngine_Camera_get_orthographic();
		Register_UnityEngine_Camera_get_orthographic();

		//System.Boolean UnityEngine.Camera::get_stereoEnabled()
		void Register_UnityEngine_Camera_get_stereoEnabled();
		Register_UnityEngine_Camera_get_stereoEnabled();

		//System.Int32 UnityEngine.Camera::GetAllCamerasCount()
		void Register_UnityEngine_Camera_GetAllCamerasCount();
		Register_UnityEngine_Camera_GetAllCamerasCount();

		//System.Int32 UnityEngine.Camera::GetAllCamerasImpl(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCamerasImpl();
		Register_UnityEngine_Camera_GetAllCamerasImpl();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Int32 UnityEngine.Camera::get_pixelHeight()
		void Register_UnityEngine_Camera_get_pixelHeight();
		Register_UnityEngine_Camera_get_pixelHeight();

		//System.Int32 UnityEngine.Camera::get_pixelWidth()
		void Register_UnityEngine_Camera_get_pixelWidth();
		Register_UnityEngine_Camera_get_pixelWidth();

		//System.Int32 UnityEngine.Camera::get_targetDisplay()
		void Register_UnityEngine_Camera_get_targetDisplay();
		Register_UnityEngine_Camera_get_targetDisplay();

		//System.Single UnityEngine.Camera::get_aspect()
		void Register_UnityEngine_Camera_get_aspect();
		Register_UnityEngine_Camera_get_aspect();

		//System.Single UnityEngine.Camera::get_depth()
		void Register_UnityEngine_Camera_get_depth();
		Register_UnityEngine_Camera_get_depth();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_fieldOfView()
		void Register_UnityEngine_Camera_get_fieldOfView();
		Register_UnityEngine_Camera_get_fieldOfView();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Single UnityEngine.Camera::get_orthographicSize()
		void Register_UnityEngine_Camera_get_orthographicSize();
		Register_UnityEngine_Camera_get_orthographicSize();

		//System.Void UnityEngine.Camera::AddCommandBufferImpl(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Camera_AddCommandBufferImpl();
		Register_UnityEngine_Camera_AddCommandBufferImpl();

		//System.Void UnityEngine.Camera::CopyStereoDeviceProjectionMatrixToNonJittered(UnityEngine.Camera/StereoscopicEye)
		void Register_UnityEngine_Camera_CopyStereoDeviceProjectionMatrixToNonJittered();
		Register_UnityEngine_Camera_CopyStereoDeviceProjectionMatrixToNonJittered();

		//System.Void UnityEngine.Camera::GetStereoNonJitteredProjectionMatrix_Injected(UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_GetStereoNonJitteredProjectionMatrix_Injected();
		Register_UnityEngine_Camera_GetStereoNonJitteredProjectionMatrix_Injected();

		//System.Void UnityEngine.Camera::RemoveCommandBufferImpl(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Camera_RemoveCommandBufferImpl();
		Register_UnityEngine_Camera_RemoveCommandBufferImpl();

		//System.Void UnityEngine.Camera::ResetProjectionMatrix()
		void Register_UnityEngine_Camera_ResetProjectionMatrix();
		Register_UnityEngine_Camera_ResetProjectionMatrix();

		//System.Void UnityEngine.Camera::ResetStereoProjectionMatrices()
		void Register_UnityEngine_Camera_ResetStereoProjectionMatrices();
		Register_UnityEngine_Camera_ResetStereoProjectionMatrices();

		//System.Void UnityEngine.Camera::ScreenPointToRay_Injected(UnityEngine.Vector2&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Ray&)
		void Register_UnityEngine_Camera_ScreenPointToRay_Injected();
		Register_UnityEngine_Camera_ScreenPointToRay_Injected();

		//System.Void UnityEngine.Camera::ScreenToViewportPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_ScreenToViewportPoint_Injected();
		Register_UnityEngine_Camera_ScreenToViewportPoint_Injected();

		//System.Void UnityEngine.Camera::ScreenToWorldPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_ScreenToWorldPoint_Injected();
		Register_UnityEngine_Camera_ScreenToWorldPoint_Injected();

		//System.Void UnityEngine.Camera::SetStereoProjectionMatrix_Injected(UnityEngine.Camera/StereoscopicEye,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_SetStereoProjectionMatrix_Injected();
		Register_UnityEngine_Camera_SetStereoProjectionMatrix_Injected();

		//System.Void UnityEngine.Camera::ViewportToWorldPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_ViewportToWorldPoint_Injected();
		Register_UnityEngine_Camera_ViewportToWorldPoint_Injected();

		//System.Void UnityEngine.Camera::WorldToScreenPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera/MonoOrStereoscopicEye,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_WorldToScreenPoint_Injected();
		Register_UnityEngine_Camera_WorldToScreenPoint_Injected();

		//System.Void UnityEngine.Camera::get_backgroundColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_Camera_get_backgroundColor_Injected();
		Register_UnityEngine_Camera_get_backgroundColor_Injected();

		//System.Void UnityEngine.Camera::get_pixelRect_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_get_pixelRect_Injected();
		Register_UnityEngine_Camera_get_pixelRect_Injected();

		//System.Void UnityEngine.Camera::get_projectionMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_get_projectionMatrix_Injected();
		Register_UnityEngine_Camera_get_projectionMatrix_Injected();

		//System.Void UnityEngine.Camera::get_rect_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_get_rect_Injected();
		Register_UnityEngine_Camera_get_rect_Injected();

		//System.Void UnityEngine.Camera::get_worldToCameraMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_get_worldToCameraMatrix_Injected();
		Register_UnityEngine_Camera_get_worldToCameraMatrix_Injected();

		//System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
		void Register_UnityEngine_Camera_set_depthTextureMode();
		Register_UnityEngine_Camera_set_depthTextureMode();

		//System.Void UnityEngine.Camera::set_forceIntoRenderTexture(System.Boolean)
		void Register_UnityEngine_Camera_set_forceIntoRenderTexture();
		Register_UnityEngine_Camera_set_forceIntoRenderTexture();

		//System.Void UnityEngine.Camera::set_nonJitteredProjectionMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_set_nonJitteredProjectionMatrix_Injected();
		Register_UnityEngine_Camera_set_nonJitteredProjectionMatrix_Injected();

		//System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
		void Register_UnityEngine_Camera_set_orthographic();
		Register_UnityEngine_Camera_set_orthographic();

		//System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
		void Register_UnityEngine_Camera_set_orthographicSize();
		Register_UnityEngine_Camera_set_orthographicSize();

		//System.Void UnityEngine.Camera::set_projectionMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_set_projectionMatrix_Injected();
		Register_UnityEngine_Camera_set_projectionMatrix_Injected();

		//System.Void UnityEngine.Camera::set_useJitteredProjectionMatrixForTransparentRendering(System.Boolean)
		void Register_UnityEngine_Camera_set_useJitteredProjectionMatrixForTransparentRendering();
		Register_UnityEngine_Camera_set_useJitteredProjectionMatrixForTransparentRendering();

		//UnityEngine.Camera UnityEngine.Camera::get_main()
		void Register_UnityEngine_Camera_get_main();
		Register_UnityEngine_Camera_get_main();

		//UnityEngine.Camera/MonoOrStereoscopicEye UnityEngine.Camera::get_stereoActiveEye()
		void Register_UnityEngine_Camera_get_stereoActiveEye();
		Register_UnityEngine_Camera_get_stereoActiveEye();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.CameraType UnityEngine.Camera::get_cameraType()
		void Register_UnityEngine_Camera_get_cameraType();
		Register_UnityEngine_Camera_get_cameraType();

		//UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
		void Register_UnityEngine_Camera_get_depthTextureMode();
		Register_UnityEngine_Camera_get_depthTextureMode();

		//UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D_Injected(UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_RaycastTry2D_Injected();
		Register_UnityEngine_Camera_RaycastTry2D_Injected();

		//UnityEngine.GameObject UnityEngine.Camera::RaycastTry_Injected(UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_RaycastTry_Injected();
		Register_UnityEngine_Camera_RaycastTry_Injected();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

		//UnityEngine.RenderingPath UnityEngine.Camera::get_actualRenderingPath()
		void Register_UnityEngine_Camera_get_actualRenderingPath();
		Register_UnityEngine_Camera_get_actualRenderingPath();

		//UnityEngine.StereoTargetEyeMask UnityEngine.Camera::get_stereoTargetEye()
		void Register_UnityEngine_Camera_get_stereoTargetEye();
		Register_UnityEngine_Camera_get_stereoTargetEye();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Canvas

		//System.Boolean UnityEngine.Canvas::get_isRootCanvas()
		void Register_UnityEngine_Canvas_get_isRootCanvas();
		Register_UnityEngine_Canvas_get_isRootCanvas();

		//System.Boolean UnityEngine.Canvas::get_overrideSorting()
		void Register_UnityEngine_Canvas_get_overrideSorting();
		Register_UnityEngine_Canvas_get_overrideSorting();

		//System.Boolean UnityEngine.Canvas::get_pixelPerfect()
		void Register_UnityEngine_Canvas_get_pixelPerfect();
		Register_UnityEngine_Canvas_get_pixelPerfect();

		//System.Int32 UnityEngine.Canvas::get_renderOrder()
		void Register_UnityEngine_Canvas_get_renderOrder();
		Register_UnityEngine_Canvas_get_renderOrder();

		//System.Int32 UnityEngine.Canvas::get_sortingLayerID()
		void Register_UnityEngine_Canvas_get_sortingLayerID();
		Register_UnityEngine_Canvas_get_sortingLayerID();

		//System.Int32 UnityEngine.Canvas::get_sortingOrder()
		void Register_UnityEngine_Canvas_get_sortingOrder();
		Register_UnityEngine_Canvas_get_sortingOrder();

		//System.Int32 UnityEngine.Canvas::get_targetDisplay()
		void Register_UnityEngine_Canvas_get_targetDisplay();
		Register_UnityEngine_Canvas_get_targetDisplay();

		//System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
		void Register_UnityEngine_Canvas_get_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_get_referencePixelsPerUnit();

		//System.Single UnityEngine.Canvas::get_scaleFactor()
		void Register_UnityEngine_Canvas_get_scaleFactor();
		Register_UnityEngine_Canvas_get_scaleFactor();

		//System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
		void Register_UnityEngine_Canvas_set_overrideSorting();
		Register_UnityEngine_Canvas_set_overrideSorting();

		//System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
		void Register_UnityEngine_Canvas_set_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_set_referencePixelsPerUnit();

		//System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
		void Register_UnityEngine_Canvas_set_scaleFactor();
		Register_UnityEngine_Canvas_set_scaleFactor();

		//System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingLayerID();
		Register_UnityEngine_Canvas_set_sortingLayerID();

		//System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingOrder();
		Register_UnityEngine_Canvas_set_sortingOrder();

		//UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
		void Register_UnityEngine_Canvas_get_worldCamera();
		Register_UnityEngine_Canvas_get_worldCamera();

		//UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
		void Register_UnityEngine_Canvas_get_rootCanvas();
		Register_UnityEngine_Canvas_get_rootCanvas();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();

		//UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
		void Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();
		Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();

		//UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
		void Register_UnityEngine_Canvas_get_renderMode();
		Register_UnityEngine_Canvas_get_renderMode();

	//End Registrations for type : UnityEngine.Canvas

	//Start Registrations for type : UnityEngine.CanvasGroup

		//System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
		void Register_UnityEngine_CanvasGroup_get_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_get_blocksRaycasts();

		//System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
		void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();

		//System.Boolean UnityEngine.CanvasGroup::get_interactable()
		void Register_UnityEngine_CanvasGroup_get_interactable();
		Register_UnityEngine_CanvasGroup_get_interactable();

		//System.Single UnityEngine.CanvasGroup::get_alpha()
		void Register_UnityEngine_CanvasGroup_get_alpha();
		Register_UnityEngine_CanvasGroup_get_alpha();

		//System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
		void Register_UnityEngine_CanvasGroup_set_alpha();
		Register_UnityEngine_CanvasGroup_set_alpha();

	//End Registrations for type : UnityEngine.CanvasGroup

	//Start Registrations for type : UnityEngine.CanvasRenderer

		//System.Boolean UnityEngine.CanvasRenderer::get_cull()
		void Register_UnityEngine_CanvasRenderer_get_cull();
		Register_UnityEngine_CanvasRenderer_get_cull();

		//System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
		void Register_UnityEngine_CanvasRenderer_get_hasMoved();
		Register_UnityEngine_CanvasRenderer_get_hasMoved();

		//System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
		void Register_UnityEngine_CanvasRenderer_get_absoluteDepth();
		Register_UnityEngine_CanvasRenderer_get_absoluteDepth();

		//System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
		void Register_UnityEngine_CanvasRenderer_get_materialCount();
		Register_UnityEngine_CanvasRenderer_get_materialCount();

		//System.Void UnityEngine.CanvasRenderer::Clear()
		void Register_UnityEngine_CanvasRenderer_Clear();
		Register_UnityEngine_CanvasRenderer_Clear();

		//System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();
		Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();

		//System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
		void Register_UnityEngine_CanvasRenderer_DisableRectClipping();
		Register_UnityEngine_CanvasRenderer_DisableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::EnableRectClipping_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_CanvasRenderer_EnableRectClipping_Injected();
		Register_UnityEngine_CanvasRenderer_EnableRectClipping_Injected();

		//System.Void UnityEngine.CanvasRenderer::GetColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_GetColor_Injected();
		Register_UnityEngine_CanvasRenderer_GetColor_Injected();

		//System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetAlphaTexture();
		Register_UnityEngine_CanvasRenderer_SetAlphaTexture();

		//System.Void UnityEngine.CanvasRenderer::SetColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_SetColor_Injected();
		Register_UnityEngine_CanvasRenderer_SetColor_Injected();

		//System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetMaterial();
		Register_UnityEngine_CanvasRenderer_SetMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
		void Register_UnityEngine_CanvasRenderer_SetMesh();
		Register_UnityEngine_CanvasRenderer_SetMesh();

		//System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetPopMaterial();
		Register_UnityEngine_CanvasRenderer_SetPopMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetTexture();
		Register_UnityEngine_CanvasRenderer_SetTexture();

		//System.Void UnityEngine.CanvasRenderer::SplitIndicesStreamsInternal(System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitIndicesStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitIndicesStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_cull();
		Register_UnityEngine_CanvasRenderer_set_cull();

		//System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();
		Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();

		//System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_materialCount();
		Register_UnityEngine_CanvasRenderer_set_materialCount();

		//System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_popMaterialCount();
		Register_UnityEngine_CanvasRenderer_set_popMaterialCount();

	//End Registrations for type : UnityEngine.CanvasRenderer

	//Start Registrations for type : UnityEngine.CharacterController

		//System.Void UnityEngine.CharacterController::get_velocity_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_get_velocity_Injected();
		Register_UnityEngine_CharacterController_get_velocity_Injected();

		//UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_Move_Injected();
		Register_UnityEngine_CharacterController_Move_Injected();

	//End Registrations for type : UnityEngine.CharacterController

	//Start Registrations for type : UnityEngine.Collider

		//System.Boolean UnityEngine.Collider::get_enabled()
		void Register_UnityEngine_Collider_get_enabled();
		Register_UnityEngine_Collider_get_enabled();

		//System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Collider_ClosestPoint_Injected();
		Register_UnityEngine_Collider_ClosestPoint_Injected();

		//System.Void UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)
		void Register_UnityEngine_Collider_get_bounds_Injected();
		Register_UnityEngine_Collider_get_bounds_Injected();

		//UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
		void Register_UnityEngine_Collider_get_attachedRigidbody();
		Register_UnityEngine_Collider_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider

	//Start Registrations for type : UnityEngine.Collider2D

		//UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
		void Register_UnityEngine_Collider2D_get_attachedRigidbody();
		Register_UnityEngine_Collider2D_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider2D

	//Start Registrations for type : UnityEngine.ColorUtility

		//System.Boolean UnityEngine.ColorUtility::DoTryParseHtmlColor(System.String,UnityEngine.Color32&)
		void Register_UnityEngine_ColorUtility_DoTryParseHtmlColor();
		Register_UnityEngine_ColorUtility_DoTryParseHtmlColor();

	//End Registrations for type : UnityEngine.ColorUtility

	//Start Registrations for type : UnityEngine.Component

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
		void Register_UnityEngine_Component_GetComponentsForListInternal();
		Register_UnityEngine_Component_GetComponentsForListInternal();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

		//UnityEngine.Transform UnityEngine.Component::get_transform()
		void Register_UnityEngine_Component_get_transform();
		Register_UnityEngine_Component_get_transform();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.ComputeBuffer

		//System.Int32 UnityEngine.ComputeBuffer::get_count()
		void Register_UnityEngine_ComputeBuffer_get_count();
		Register_UnityEngine_ComputeBuffer_get_count();

		//System.Void UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)
		void Register_UnityEngine_ComputeBuffer_DestroyBuffer();
		Register_UnityEngine_ComputeBuffer_DestroyBuffer();

		//System.Void UnityEngine.ComputeBuffer::InitBuffer(UnityEngine.ComputeBuffer,System.Int32,System.Int32,UnityEngine.ComputeBufferType)
		void Register_UnityEngine_ComputeBuffer_InitBuffer();
		Register_UnityEngine_ComputeBuffer_InitBuffer();

	//End Registrations for type : UnityEngine.ComputeBuffer

	//Start Registrations for type : UnityEngine.ComputeShader

		//System.Int32 UnityEngine.ComputeShader::FindKernel(System.String)
		void Register_UnityEngine_ComputeShader_FindKernel();
		Register_UnityEngine_ComputeShader_FindKernel();

		//System.Void UnityEngine.ComputeShader::GetKernelThreadGroupSizes(System.Int32,System.UInt32&,System.UInt32&,System.UInt32&)
		void Register_UnityEngine_ComputeShader_GetKernelThreadGroupSizes();
		Register_UnityEngine_ComputeShader_GetKernelThreadGroupSizes();

	//End Registrations for type : UnityEngine.ComputeShader

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine(System.IntPtr)
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.Cubemap

		//System.Boolean UnityEngine.Cubemap::Internal_CreateImpl(UnityEngine.Cubemap,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
		void Register_UnityEngine_Cubemap_Internal_CreateImpl();
		Register_UnityEngine_Cubemap_Internal_CreateImpl();

		//System.Boolean UnityEngine.Cubemap::IsReadable()
		void Register_UnityEngine_Cubemap_IsReadable();
		Register_UnityEngine_Cubemap_IsReadable();

		//System.Void UnityEngine.Cubemap::ApplyImpl(System.Boolean,System.Boolean)
		void Register_UnityEngine_Cubemap_ApplyImpl();
		Register_UnityEngine_Cubemap_ApplyImpl();

		//System.Void UnityEngine.Cubemap::SetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Cubemap_SetPixelImpl_Injected();
		Register_UnityEngine_Cubemap_SetPixelImpl_Injected();

		//UnityEngine.TextureFormat UnityEngine.Cubemap::get_format()
		void Register_UnityEngine_Cubemap_get_format();
		Register_UnityEngine_Cubemap_get_format();

	//End Registrations for type : UnityEngine.Cubemap

	//Start Registrations for type : UnityEngine.CubemapArray

		//System.Boolean UnityEngine.CubemapArray::Internal_CreateImpl(UnityEngine.CubemapArray,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
		void Register_UnityEngine_CubemapArray_Internal_CreateImpl();
		Register_UnityEngine_CubemapArray_Internal_CreateImpl();

		//UnityEngine.TextureFormat UnityEngine.CubemapArray::get_format()
		void Register_UnityEngine_CubemapArray_get_format();
		Register_UnityEngine_CubemapArray_get_format();

	//End Registrations for type : UnityEngine.CubemapArray

	//Start Registrations for type : UnityEngine.CullingGroup

		//System.Void UnityEngine.CullingGroup::DisposeInternal()
		void Register_UnityEngine_CullingGroup_DisposeInternal();
		Register_UnityEngine_CullingGroup_DisposeInternal();

		//System.Void UnityEngine.CullingGroup::FinalizerFailure()
		void Register_UnityEngine_CullingGroup_FinalizerFailure();
		Register_UnityEngine_CullingGroup_FinalizerFailure();

	//End Registrations for type : UnityEngine.CullingGroup

	//Start Registrations for type : UnityEngine.Cursor

		//UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
		void Register_UnityEngine_Cursor_get_lockState();
		Register_UnityEngine_Cursor_get_lockState();

	//End Registrations for type : UnityEngine.Cursor

	//Start Registrations for type : UnityEngine.Debug

		//System.Boolean UnityEngine.Debug::get_isDebugBuild()
		void Register_UnityEngine_Debug_get_isDebugBuild();
		Register_UnityEngine_Debug_get_isDebugBuild();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.DebugLogHandler

		//System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_Log();
		Register_UnityEngine_DebugLogHandler_Internal_Log();

		//System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_LogException();
		Register_UnityEngine_DebugLogHandler_Internal_LogException();

	//End Registrations for type : UnityEngine.DebugLogHandler

	//Start Registrations for type : UnityEngine.Display

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.Event

		//System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
		void Register_UnityEngine_Event_PopEvent();
		Register_UnityEngine_Event_PopEvent();

		//System.Char UnityEngine.Event::get_character()
		void Register_UnityEngine_Event_get_character();
		Register_UnityEngine_Event_get_character();

		//System.Int32 UnityEngine.Event::get_clickCount()
		void Register_UnityEngine_Event_get_clickCount();
		Register_UnityEngine_Event_get_clickCount();

		//System.IntPtr UnityEngine.Event::Internal_Create(System.Int32)
		void Register_UnityEngine_Event_Internal_Create();
		Register_UnityEngine_Event_Internal_Create();

		//System.String UnityEngine.Event::get_commandName()
		void Register_UnityEngine_Event_get_commandName();
		Register_UnityEngine_Event_get_commandName();

		//System.Void UnityEngine.Event::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_Event_Internal_Destroy();
		Register_UnityEngine_Event_Internal_Destroy();

		//System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
		void Register_UnityEngine_Event_Internal_SetNativeEvent();
		Register_UnityEngine_Event_Internal_SetNativeEvent();

		//System.Void UnityEngine.Event::Internal_Use()
		void Register_UnityEngine_Event_Internal_Use();
		Register_UnityEngine_Event_Internal_Use();

		//System.Void UnityEngine.Event::get_mousePosition_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_get_mousePosition_Injected();
		Register_UnityEngine_Event_get_mousePosition_Injected();

		//System.Void UnityEngine.Event::set_character(System.Char)
		void Register_UnityEngine_Event_set_character();
		Register_UnityEngine_Event_set_character();

		//System.Void UnityEngine.Event::set_displayIndex(System.Int32)
		void Register_UnityEngine_Event_set_displayIndex();
		Register_UnityEngine_Event_set_displayIndex();

		//System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
		void Register_UnityEngine_Event_set_keyCode();
		Register_UnityEngine_Event_set_keyCode();

		//System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
		void Register_UnityEngine_Event_set_modifiers();
		Register_UnityEngine_Event_set_modifiers();

		//System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
		void Register_UnityEngine_Event_set_type();
		Register_UnityEngine_Event_set_type();

		//UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
		void Register_UnityEngine_Event_get_modifiers();
		Register_UnityEngine_Event_get_modifiers();

		//UnityEngine.EventType UnityEngine.Event::get_rawType()
		void Register_UnityEngine_Event_get_rawType();
		Register_UnityEngine_Event_get_rawType();

		//UnityEngine.EventType UnityEngine.Event::get_type()
		void Register_UnityEngine_Event_get_type();
		Register_UnityEngine_Event_get_type();

		//UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
		void Register_UnityEngine_Event_get_keyCode();
		Register_UnityEngine_Event_get_keyCode();

	//End Registrations for type : UnityEngine.Event

	//Start Registrations for type : UnityEngine.Experimental.Audio.AudioSampleProvider

		//System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InternalSetScriptingPtr(System.UInt32,UnityEngine.Experimental.Audio.AudioSampleProvider)
		void Register_UnityEngine_Experimental_Audio_AudioSampleProvider_InternalSetScriptingPtr();
		Register_UnityEngine_Experimental_Audio_AudioSampleProvider_InternalSetScriptingPtr();

	//End Registrations for type : UnityEngine.Experimental.Audio.AudioSampleProvider

	//Start Registrations for type : UnityEngine.Experimental.Rendering.CullResults

		//System.Boolean UnityEngine.Experimental.Rendering.CullResults::ComputeSpotShadowMatricesAndCullingPrimitives(System.IntPtr,System.Int32,UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&,UnityEngine.Experimental.Rendering.ShadowSplitData&)
		void Register_UnityEngine_Experimental_Rendering_CullResults_ComputeSpotShadowMatricesAndCullingPrimitives();
		Register_UnityEngine_Experimental_Rendering_CullResults_ComputeSpotShadowMatricesAndCullingPrimitives();

		//System.Boolean UnityEngine.Experimental.Rendering.CullResults::GetCullingParameters_Internal(UnityEngine.Camera,System.Boolean,UnityEngine.Experimental.Rendering.ScriptableCullingParameters&,System.Int32)
		void Register_UnityEngine_Experimental_Rendering_CullResults_GetCullingParameters_Internal();
		Register_UnityEngine_Experimental_Rendering_CullResults_GetCullingParameters_Internal();

		//System.Boolean UnityEngine.Experimental.Rendering.CullResults::GetShadowCasterBounds(System.IntPtr,System.Int32,UnityEngine.Bounds&)
		void Register_UnityEngine_Experimental_Rendering_CullResults_GetShadowCasterBounds();
		Register_UnityEngine_Experimental_Rendering_CullResults_GetShadowCasterBounds();

		//System.Boolean UnityEngine.Experimental.Rendering.CullResults::INTERNAL_CALL_ComputeDirectionalShadowMatricesAndCullingPrimitives(System.IntPtr,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3&,System.Int32,System.Single,UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&,UnityEngine.Experimental.Rendering.ShadowSplitData&)
		void Register_UnityEngine_Experimental_Rendering_CullResults_INTERNAL_CALL_ComputeDirectionalShadowMatricesAndCullingPrimitives();
		Register_UnityEngine_Experimental_Rendering_CullResults_INTERNAL_CALL_ComputeDirectionalShadowMatricesAndCullingPrimitives();

		//System.Int32 UnityEngine.Experimental.Rendering.CullResults::GetLightIndicesCount(System.IntPtr)
		void Register_UnityEngine_Experimental_Rendering_CullResults_GetLightIndicesCount();
		Register_UnityEngine_Experimental_Rendering_CullResults_GetLightIndicesCount();

		//System.Int32[] UnityEngine.Experimental.Rendering.CullResults::GetLightIndexMap(System.IntPtr)
		void Register_UnityEngine_Experimental_Rendering_CullResults_GetLightIndexMap();
		Register_UnityEngine_Experimental_Rendering_CullResults_GetLightIndexMap();

		//System.Void UnityEngine.Experimental.Rendering.CullResults::FillLightIndices(System.IntPtr,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Experimental_Rendering_CullResults_FillLightIndices();
		Register_UnityEngine_Experimental_Rendering_CullResults_FillLightIndices();

		//System.Void UnityEngine.Experimental.Rendering.CullResults::INTERNAL_CALL_Internal_Cull(UnityEngine.Experimental.Rendering.ScriptableCullingParameters&,UnityEngine.Experimental.Rendering.ScriptableRenderContext&,UnityEngine.Experimental.Rendering.CullResults&)
		void Register_UnityEngine_Experimental_Rendering_CullResults_INTERNAL_CALL_Internal_Cull();
		Register_UnityEngine_Experimental_Rendering_CullResults_INTERNAL_CALL_Internal_Cull();

		//System.Void UnityEngine.Experimental.Rendering.CullResults::SetLightIndexMap(System.IntPtr,System.Int32[])
		void Register_UnityEngine_Experimental_Rendering_CullResults_SetLightIndexMap();
		Register_UnityEngine_Experimental_Rendering_CullResults_SetLightIndexMap();

	//End Registrations for type : UnityEngine.Experimental.Rendering.CullResults

	//Start Registrations for type : UnityEngine.Experimental.Rendering.DrawRendererSettings

		//System.Void UnityEngine.Experimental.Rendering.DrawRendererSettings::InitializeSortSettings(UnityEngine.Camera,UnityEngine.Experimental.Rendering.DrawRendererSortSettings&)
		void Register_UnityEngine_Experimental_Rendering_DrawRendererSettings_InitializeSortSettings();
		Register_UnityEngine_Experimental_Rendering_DrawRendererSettings_InitializeSortSettings();

	//End Registrations for type : UnityEngine.Experimental.Rendering.DrawRendererSettings

	//Start Registrations for type : UnityEngine.Experimental.Rendering.GraphicsFormatUtility

		//System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsSRGBFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
		void Register_UnityEngine_Experimental_Rendering_GraphicsFormatUtility_IsSRGBFormat();
		Register_UnityEngine_Experimental_Rendering_GraphicsFormatUtility_IsSRGBFormat();

		//UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_TextureFormat(UnityEngine.TextureFormat,System.Boolean)
		void Register_UnityEngine_Experimental_Rendering_GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat();
		Register_UnityEngine_Experimental_Rendering_GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat();

		//UnityEngine.RenderTextureFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetRenderTextureFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
		void Register_UnityEngine_Experimental_Rendering_GraphicsFormatUtility_GetRenderTextureFormat();
		Register_UnityEngine_Experimental_Rendering_GraphicsFormatUtility_GetRenderTextureFormat();

	//End Registrations for type : UnityEngine.Experimental.Rendering.GraphicsFormatUtility

	//Start Registrations for type : UnityEngine.Experimental.Rendering.ScriptableRenderContext

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::DrawShadows_Internal(UnityEngine.Experimental.Rendering.DrawShadowsSettings&)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_DrawShadows_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_DrawShadows_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::DrawSkybox_Internal(UnityEngine.Camera)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_DrawSkybox_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_DrawSkybox_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::ExecuteCommandBuffer_Internal(UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_ExecuteCommandBuffer_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_ExecuteCommandBuffer_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::INTERNAL_CALL_DrawRenderers_Internal(UnityEngine.Experimental.Rendering.ScriptableRenderContext&,UnityEngine.Experimental.Rendering.FilterResults&,UnityEngine.Experimental.Rendering.DrawRendererSettings&,UnityEngine.Experimental.Rendering.FilterRenderersSettings&)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_INTERNAL_CALL_DrawRenderers_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_INTERNAL_CALL_DrawRenderers_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::SetupCameraProperties_Internal(UnityEngine.Camera,System.Boolean)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_SetupCameraProperties_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_SetupCameraProperties_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::StartMultiEye_Internal(UnityEngine.Camera)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_StartMultiEye_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_StartMultiEye_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::StereoEndRender_Internal(UnityEngine.Camera)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_StereoEndRender_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_StereoEndRender_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::StopMultiEye_Internal(UnityEngine.Camera)
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_StopMultiEye_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_StopMultiEye_Internal();

		//System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::Submit_Internal()
		void Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_Submit_Internal();
		Register_UnityEngine_Experimental_Rendering_ScriptableRenderContext_Submit_Internal();

	//End Registrations for type : UnityEngine.Experimental.Rendering.ScriptableRenderContext

	//Start Registrations for type : UnityEngine.Experimental.Rendering.ShaderPassName

		//System.Int32 UnityEngine.Experimental.Rendering.ShaderPassName::Init(System.String)
		void Register_UnityEngine_Experimental_Rendering_ShaderPassName_Init();
		Register_UnityEngine_Experimental_Rendering_ShaderPassName_Init();

	//End Registrations for type : UnityEngine.Experimental.Rendering.ShaderPassName

	//Start Registrations for type : UnityEngine.Experimental.Rendering.VisibleLight

		//UnityEngine.Light UnityEngine.Experimental.Rendering.VisibleLight::GetLightObject(System.Int32)
		void Register_UnityEngine_Experimental_Rendering_VisibleLight_GetLightObject();
		Register_UnityEngine_Experimental_Rendering_VisibleLight_GetLightObject();

	//End Registrations for type : UnityEngine.Experimental.Rendering.VisibleLight

	//Start Registrations for type : UnityEngine.Experimental.Subsystem

		//System.Void UnityEngine.Experimental.Subsystem::SetHandle(UnityEngine.Experimental.Subsystem)
		void Register_UnityEngine_Experimental_Subsystem_SetHandle();
		Register_UnityEngine_Experimental_Subsystem_SetHandle();

	//End Registrations for type : UnityEngine.Experimental.Subsystem

	//Start Registrations for type : UnityEngine.Font

		//System.Boolean UnityEngine.Font::HasCharacter(System.Int32)
		void Register_UnityEngine_Font_HasCharacter();
		Register_UnityEngine_Font_HasCharacter();

		//System.Boolean UnityEngine.Font::get_dynamic()
		void Register_UnityEngine_Font_get_dynamic();
		Register_UnityEngine_Font_get_dynamic();

		//System.Int32 UnityEngine.Font::get_fontSize()
		void Register_UnityEngine_Font_get_fontSize();
		Register_UnityEngine_Font_get_fontSize();

		//System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
		void Register_UnityEngine_Font_Internal_CreateFont();
		Register_UnityEngine_Font_Internal_CreateFont();

		//UnityEngine.Material UnityEngine.Font::get_material()
		void Register_UnityEngine_Font_get_material();
		Register_UnityEngine_Font_get_material();

	//End Registrations for type : UnityEngine.Font

	//Start Registrations for type : UnityEngine.GameObject

		//System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
		void Register_UnityEngine_GameObject_GetComponentsInternal();
		Register_UnityEngine_GameObject_GetComponentsInternal();

		//System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
		void Register_UnityEngine_GameObject_get_activeInHierarchy();
		Register_UnityEngine_GameObject_get_activeInHierarchy();

		//System.Boolean UnityEngine.GameObject::get_activeSelf()
		void Register_UnityEngine_GameObject_get_activeSelf();
		Register_UnityEngine_GameObject_get_activeSelf();

		//System.Int32 UnityEngine.GameObject::get_layer()
		void Register_UnityEngine_GameObject_get_layer();
		Register_UnityEngine_GameObject_get_layer();

		//System.String UnityEngine.GameObject::get_tag()
		void Register_UnityEngine_GameObject_get_tag();
		Register_UnityEngine_GameObject_get_tag();

		//System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_GameObject_GetComponentFastPath();
		Register_UnityEngine_GameObject_GetComponentFastPath();

		//System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_GameObject_Internal_CreateGameObject();
		Register_UnityEngine_GameObject_Internal_CreateGameObject();

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

		//System.Void UnityEngine.GameObject::SetActive(System.Boolean)
		void Register_UnityEngine_GameObject_SetActive();
		Register_UnityEngine_GameObject_SetActive();

		//System.Void UnityEngine.GameObject::set_layer(System.Int32)
		void Register_UnityEngine_GameObject_set_layer();
		Register_UnityEngine_GameObject_set_layer();

		//UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
		void Register_UnityEngine_GameObject_GetComponent();
		Register_UnityEngine_GameObject_GetComponent();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
		void Register_UnityEngine_GameObject_GetComponentInChildren();
		Register_UnityEngine_GameObject_GetComponentInChildren();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
		void Register_UnityEngine_GameObject_GetComponentInParent();
		Register_UnityEngine_GameObject_GetComponentInParent();

		//UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
		void Register_UnityEngine_GameObject_Internal_AddComponentWithType();
		Register_UnityEngine_GameObject_Internal_AddComponentWithType();

		//UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
		void Register_UnityEngine_GameObject_Find();
		Register_UnityEngine_GameObject_Find();

		//UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectWithTag();
		Register_UnityEngine_GameObject_FindGameObjectWithTag();

		//UnityEngine.Transform UnityEngine.GameObject::get_transform()
		void Register_UnityEngine_GameObject_get_transform();
		Register_UnityEngine_GameObject_get_transform();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.GeometryUtility

		//System.Void UnityEngine.GeometryUtility::Internal_ExtractPlanes_Injected(UnityEngine.Plane[],UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GeometryUtility_Internal_ExtractPlanes_Injected();
		Register_UnityEngine_GeometryUtility_Internal_ExtractPlanes_Injected();

	//End Registrations for type : UnityEngine.GeometryUtility

	//Start Registrations for type : UnityEngine.Gizmos

		//System.Void UnityEngine.Gizmos::DrawCube_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_DrawCube_Injected();
		Register_UnityEngine_Gizmos_DrawCube_Injected();

		//System.Void UnityEngine.Gizmos::DrawMesh_Injected(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_DrawMesh_Injected();
		Register_UnityEngine_Gizmos_DrawMesh_Injected();

		//System.Void UnityEngine.Gizmos::DrawSphere_Injected(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_DrawSphere_Injected();
		Register_UnityEngine_Gizmos_DrawSphere_Injected();

		//System.Void UnityEngine.Gizmos::DrawWireCube_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_DrawWireCube_Injected();
		Register_UnityEngine_Gizmos_DrawWireCube_Injected();

		//System.Void UnityEngine.Gizmos::DrawWireMesh_Injected(UnityEngine.Mesh,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_DrawWireMesh_Injected();
		Register_UnityEngine_Gizmos_DrawWireMesh_Injected();

		//System.Void UnityEngine.Gizmos::DrawWireSphere_Injected(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_DrawWireSphere_Injected();
		Register_UnityEngine_Gizmos_DrawWireSphere_Injected();

		//System.Void UnityEngine.Gizmos::set_matrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Gizmos_set_matrix_Injected();
		Register_UnityEngine_Gizmos_set_matrix_Injected();

	//End Registrations for type : UnityEngine.Gizmos

	//Start Registrations for type : UnityEngine.GL

		//System.Void UnityEngine.GL::Begin(System.Int32)
		void Register_UnityEngine_GL_Begin();
		Register_UnityEngine_GL_Begin();

		//System.Void UnityEngine.GL::End()
		void Register_UnityEngine_GL_End();
		Register_UnityEngine_GL_End();

		//System.Void UnityEngine.GL::GetGPUProjectionMatrix_Injected(UnityEngine.Matrix4x4&,System.Boolean,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_GetGPUProjectionMatrix_Injected();
		Register_UnityEngine_GL_GetGPUProjectionMatrix_Injected();

		//System.Void UnityEngine.GL::ImmediateColor(System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_ImmediateColor();
		Register_UnityEngine_GL_ImmediateColor();

		//System.Void UnityEngine.GL::LoadProjectionMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_LoadProjectionMatrix_Injected();
		Register_UnityEngine_GL_LoadProjectionMatrix_Injected();

		//System.Void UnityEngine.GL::PopMatrix()
		void Register_UnityEngine_GL_PopMatrix();
		Register_UnityEngine_GL_PopMatrix();

		//System.Void UnityEngine.GL::PushMatrix()
		void Register_UnityEngine_GL_PushMatrix();
		Register_UnityEngine_GL_PushMatrix();

		//System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_Vertex3();
		Register_UnityEngine_GL_Vertex3();

	//End Registrations for type : UnityEngine.GL

	//Start Registrations for type : UnityEngine.Gradient

		//System.IntPtr UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::set_alphaKeys(UnityEngine.GradientAlphaKey[])
		void Register_UnityEngine_Gradient_set_alphaKeys();
		Register_UnityEngine_Gradient_set_alphaKeys();

		//System.Void UnityEngine.Gradient::set_colorKeys(UnityEngine.GradientColorKey[])
		void Register_UnityEngine_Gradient_set_colorKeys();
		Register_UnityEngine_Gradient_set_colorKeys();

		//System.Void UnityEngine.Gradient::set_mode(UnityEngine.GradientMode)
		void Register_UnityEngine_Gradient_set_mode();
		Register_UnityEngine_Gradient_set_mode();

		//UnityEngine.GradientAlphaKey[] UnityEngine.Gradient::get_alphaKeys()
		void Register_UnityEngine_Gradient_get_alphaKeys();
		Register_UnityEngine_Gradient_get_alphaKeys();

		//UnityEngine.GradientColorKey[] UnityEngine.Gradient::get_colorKeys()
		void Register_UnityEngine_Gradient_get_colorKeys();
		Register_UnityEngine_Gradient_get_colorKeys();

		//UnityEngine.GradientMode UnityEngine.Gradient::get_mode()
		void Register_UnityEngine_Gradient_get_mode();
		Register_UnityEngine_Gradient_get_mode();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.Graphics

		//System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
		void Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();
		Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();

		//System.Void UnityEngine.Graphics::Blit2(UnityEngine.Texture,UnityEngine.RenderTexture)
		void Register_UnityEngine_Graphics_Blit2();
		Register_UnityEngine_Graphics_Blit2();

		//System.Void UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)
		void Register_UnityEngine_Graphics_Internal_DrawTexture();
		Register_UnityEngine_Graphics_Internal_DrawTexture();

		//UnityEngine.Rendering.GraphicsTier UnityEngine.Graphics::get_activeTier()
		void Register_UnityEngine_Graphics_get_activeTier();
		Register_UnityEngine_Graphics_get_activeTier();

	//End Registrations for type : UnityEngine.Graphics

	//Start Registrations for type : UnityEngine.GUI

		//System.Boolean UnityEngine.GUI::DoButton_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_DoButton_Injected();
		Register_UnityEngine_GUI_DoButton_Injected();

		//System.Boolean UnityEngine.GUI::DoToggle_Injected(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_DoToggle_Injected();
		Register_UnityEngine_GUI_DoToggle_Injected();

		//System.Void UnityEngine.GUI::DoLabel_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_DoLabel_Injected();
		Register_UnityEngine_GUI_DoLabel_Injected();

		//System.Void UnityEngine.GUI::Internal_DoModalWindow_Injected(System.Int32,System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,System.Object,UnityEngine.Rect&)
		void Register_UnityEngine_GUI_Internal_DoModalWindow_Injected();
		Register_UnityEngine_GUI_Internal_DoModalWindow_Injected();

		//System.Void UnityEngine.GUI::get_color_Injected(UnityEngine.Color&)
		void Register_UnityEngine_GUI_get_color_Injected();
		Register_UnityEngine_GUI_get_color_Injected();

		//System.Void UnityEngine.GUI::set_changed(System.Boolean)
		void Register_UnityEngine_GUI_set_changed();
		Register_UnityEngine_GUI_set_changed();

		//System.Void UnityEngine.GUI::set_color_Injected(UnityEngine.Color&)
		void Register_UnityEngine_GUI_set_color_Injected();
		Register_UnityEngine_GUI_set_color_Injected();

		//UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
		void Register_UnityEngine_GUI_get_blendMaterial();
		Register_UnityEngine_GUI_get_blendMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
		void Register_UnityEngine_GUI_get_blitMaterial();
		Register_UnityEngine_GUI_get_blitMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_roundedRectMaterial()
		void Register_UnityEngine_GUI_get_roundedRectMaterial();
		Register_UnityEngine_GUI_get_roundedRectMaterial();

	//End Registrations for type : UnityEngine.GUI

	//Start Registrations for type : UnityEngine.GUIClip

		//System.Void UnityEngine.GUIClip::Internal_Pop()
		void Register_UnityEngine_GUIClip_Internal_Pop();
		Register_UnityEngine_GUIClip_Internal_Pop();

		//System.Void UnityEngine.GUIClip::Internal_Push_Injected(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
		void Register_UnityEngine_GUIClip_Internal_Push_Injected();
		Register_UnityEngine_GUIClip_Internal_Push_Injected();

	//End Registrations for type : UnityEngine.GUIClip

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::HitTest_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_GUILayer_HitTest_Injected();
		Register_UnityEngine_GUILayer_HitTest_Injected();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.GUILayoutUtility

		//System.Void UnityEngine.GUILayoutUtility::Internal_GetWindowRect_Injected(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_Internal_GetWindowRect_Injected();
		Register_UnityEngine_GUILayoutUtility_Internal_GetWindowRect_Injected();

		//System.Void UnityEngine.GUILayoutUtility::Internal_MoveWindow_Injected(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_Internal_MoveWindow_Injected();
		Register_UnityEngine_GUILayoutUtility_Internal_MoveWindow_Injected();

	//End Registrations for type : UnityEngine.GUILayoutUtility

	//Start Registrations for type : UnityEngine.GUISettings

		//System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
		void Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed();
		Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed();

	//End Registrations for type : UnityEngine.GUISettings

	//Start Registrations for type : UnityEngine.GUIStyle

		//System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
		void Register_UnityEngine_GUIStyle_get_stretchHeight();
		Register_UnityEngine_GUIStyle_get_stretchHeight();

		//System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
		void Register_UnityEngine_GUIStyle_get_stretchWidth();
		Register_UnityEngine_GUIStyle_get_stretchWidth();

		//System.Boolean UnityEngine.GUIStyle::get_wordWrap()
		void Register_UnityEngine_GUIStyle_get_wordWrap();
		Register_UnityEngine_GUIStyle_get_wordWrap();

		//System.Int32 UnityEngine.GUIStyle::Internal_GetCursorStringIndex_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_GetCursorStringIndex_Injected();
		Register_UnityEngine_GUIStyle_Internal_GetCursorStringIndex_Injected();

		//System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
		void Register_UnityEngine_GUIStyle_GetRectOffsetPtr();
		Register_UnityEngine_GUIStyle_GetRectOffsetPtr();

		//System.IntPtr UnityEngine.GUIStyle::Internal_Copy(UnityEngine.GUIStyle,UnityEngine.GUIStyle)
		void Register_UnityEngine_GUIStyle_Internal_Copy();
		Register_UnityEngine_GUIStyle_Internal_Copy();

		//System.IntPtr UnityEngine.GUIStyle::Internal_Create(UnityEngine.GUIStyle)
		void Register_UnityEngine_GUIStyle_Internal_Create();
		Register_UnityEngine_GUIStyle_Internal_Create();

		//System.Single UnityEngine.GUIStyle::Internal_CalcHeight(UnityEngine.GUIContent,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_CalcHeight();
		Register_UnityEngine_GUIStyle_Internal_CalcHeight();

		//System.Single UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()
		void Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset();
		Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset();

		//System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
		void Register_UnityEngine_GUIStyle_Internal_GetLineHeight();
		Register_UnityEngine_GUIStyle_Internal_GetLineHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedHeight()
		void Register_UnityEngine_GUIStyle_get_fixedHeight();
		Register_UnityEngine_GUIStyle_get_fixedHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedWidth()
		void Register_UnityEngine_GUIStyle_get_fixedWidth();
		Register_UnityEngine_GUIStyle_get_fixedWidth();

		//System.String UnityEngine.GUIStyle::get_name()
		void Register_UnityEngine_GUIStyle_get_name();
		Register_UnityEngine_GUIStyle_get_name();

		//System.Void UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignRectOffset();
		Register_UnityEngine_GUIStyle_AssignRectOffset();

		//System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth_Injected(UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth_Injected();
		Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth_Injected();

		//System.Void UnityEngine.GUIStyle::Internal_CalcSizeWithConstraints_Injected(UnityEngine.GUIContent,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_CalcSizeWithConstraints_Injected();
		Register_UnityEngine_GUIStyle_Internal_CalcSizeWithConstraints_Injected();

		//System.Void UnityEngine.GUIStyle::Internal_CalcSize_Injected(UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_CalcSize_Injected();
		Register_UnityEngine_GUIStyle_Internal_CalcSize_Injected();

		//System.Void UnityEngine.GUIStyle::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_GUIStyle_Internal_Destroy();
		Register_UnityEngine_GUIStyle_Internal_Destroy();

		//System.Void UnityEngine.GUIStyle::Internal_Draw2_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_Internal_Draw2_Injected();
		Register_UnityEngine_GUIStyle_Internal_Draw2_Injected();

		//System.Void UnityEngine.GUIStyle::Internal_DrawCursor_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_GUIStyle_Internal_DrawCursor_Injected();
		Register_UnityEngine_GUIStyle_Internal_DrawCursor_Injected();

		//System.Void UnityEngine.GUIStyle::Internal_DrawWithTextSelection_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.Color&,UnityEngine.Color&)
		void Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection_Injected();
		Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection_Injected();

		//System.Void UnityEngine.GUIStyle::Internal_GetCursorPixelPosition_Injected(UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_GetCursorPixelPosition_Injected();
		Register_UnityEngine_GUIStyle_Internal_GetCursorPixelPosition_Injected();

		//System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetDefaultFont();
		Register_UnityEngine_GUIStyle_SetDefaultFont();

		//System.Void UnityEngine.GUIStyle::get_contentOffset_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_get_contentOffset_Injected();
		Register_UnityEngine_GUIStyle_get_contentOffset_Injected();

		//System.Void UnityEngine.GUIStyle::set_Internal_clipOffset_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_set_Internal_clipOffset_Injected();
		Register_UnityEngine_GUIStyle_set_Internal_clipOffset_Injected();

		//System.Void UnityEngine.GUIStyle::set_clipping(UnityEngine.TextClipping)
		void Register_UnityEngine_GUIStyle_set_clipping();
		Register_UnityEngine_GUIStyle_set_clipping();

		//System.Void UnityEngine.GUIStyle::set_contentOffset_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_set_contentOffset_Injected();
		Register_UnityEngine_GUIStyle_set_contentOffset_Injected();

		//System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
		void Register_UnityEngine_GUIStyle_set_fontSize();
		Register_UnityEngine_GUIStyle_set_fontSize();

		//System.Void UnityEngine.GUIStyle::set_richText(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_richText();
		Register_UnityEngine_GUIStyle_set_richText();

		//System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchWidth();
		Register_UnityEngine_GUIStyle_set_stretchWidth();

		//System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_wordWrap();
		Register_UnityEngine_GUIStyle_set_wordWrap();

		//UnityEngine.Font UnityEngine.GUIStyle::get_font()
		void Register_UnityEngine_GUIStyle_get_font();
		Register_UnityEngine_GUIStyle_get_font();

		//UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
		void Register_UnityEngine_GUIStyle_get_imagePosition();
		Register_UnityEngine_GUIStyle_get_imagePosition();

	//End Registrations for type : UnityEngine.GUIStyle

	//Start Registrations for type : UnityEngine.GUIStyleState

		//System.IntPtr UnityEngine.GUIStyleState::Init()
		void Register_UnityEngine_GUIStyleState_Init();
		Register_UnityEngine_GUIStyleState_Init();

		//System.Void UnityEngine.GUIStyleState::Cleanup()
		void Register_UnityEngine_GUIStyleState_Cleanup();
		Register_UnityEngine_GUIStyleState_Cleanup();

	//End Registrations for type : UnityEngine.GUIStyleState

	//Start Registrations for type : UnityEngine.GUIUtility

		//System.Int32 UnityEngine.GUIUtility::GetControlID_Injected(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
		void Register_UnityEngine_GUIUtility_GetControlID_Injected();
		Register_UnityEngine_GUIUtility_GetControlID_Injected();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
		void Register_UnityEngine_GUIUtility_Internal_GetHotControl();
		Register_UnityEngine_GUIUtility_Internal_GetHotControl();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetKeyboardControl()
		void Register_UnityEngine_GUIUtility_Internal_GetKeyboardControl();
		Register_UnityEngine_GUIUtility_Internal_GetKeyboardControl();

		//System.Int32 UnityEngine.GUIUtility::get_guiDepth()
		void Register_UnityEngine_GUIUtility_get_guiDepth();
		Register_UnityEngine_GUIUtility_get_guiDepth();

		//System.Object UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();
		Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();

		//System.Single UnityEngine.GUIUtility::get_pixelsPerPoint()
		void Register_UnityEngine_GUIUtility_get_pixelsPerPoint();
		Register_UnityEngine_GUIUtility_get_pixelsPerPoint();

		//System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
		void Register_UnityEngine_GUIUtility_get_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_get_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
		void Register_UnityEngine_GUIUtility_Internal_ExitGUI();
		Register_UnityEngine_GUIUtility_Internal_ExitGUI();

		//System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetHotControl();
		Register_UnityEngine_GUIUtility_Internal_SetHotControl();

		//System.Void UnityEngine.GUIUtility::Internal_SetKeyboardControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetKeyboardControl();
		Register_UnityEngine_GUIUtility_Internal_SetKeyboardControl();

		//System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_mouseUsed();
		Register_UnityEngine_GUIUtility_set_mouseUsed();

		//System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
		void Register_UnityEngine_GUIUtility_set_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_set_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_textFieldInput();
		Register_UnityEngine_GUIUtility_set_textFieldInput();

	//End Registrations for type : UnityEngine.GUIUtility

	//Start Registrations for type : UnityEngine.ImageConversion

		//System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[],System.Boolean)
		void Register_UnityEngine_ImageConversion_LoadImage();
		Register_UnityEngine_ImageConversion_LoadImage();

		//System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
		void Register_UnityEngine_ImageConversion_EncodeToPNG();
		Register_UnityEngine_ImageConversion_EncodeToPNG();

	//End Registrations for type : UnityEngine.ImageConversion

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetButton(System.String)
		void Register_UnityEngine_Input_GetButton();
		Register_UnityEngine_Input_GetButton();

		//System.Boolean UnityEngine.Input::GetButtonDown(System.String)
		void Register_UnityEngine_Input_GetButtonDown();
		Register_UnityEngine_Input_GetButtonDown();

		//System.Boolean UnityEngine.Input::GetButtonUp(System.String)
		void Register_UnityEngine_Input_GetButtonUp();
		Register_UnityEngine_Input_GetButtonUp();

		//System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyDownInt();
		Register_UnityEngine_Input_GetKeyDownInt();

		//System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyInt();
		Register_UnityEngine_Input_GetKeyInt();

		//System.Boolean UnityEngine.Input::GetKeyUpInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyUpInt();
		Register_UnityEngine_Input_GetKeyUpInt();

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonUp();
		Register_UnityEngine_Input_GetMouseButtonUp();

		//System.Boolean UnityEngine.Input::get_mousePresent()
		void Register_UnityEngine_Input_get_mousePresent();
		Register_UnityEngine_Input_get_mousePresent();

		//System.Boolean UnityEngine.Input::get_multiTouchEnabled()
		void Register_UnityEngine_Input_get_multiTouchEnabled();
		Register_UnityEngine_Input_get_multiTouchEnabled();

		//System.Boolean UnityEngine.Input::get_touchSupported()
		void Register_UnityEngine_Input_get_touchSupported();
		Register_UnityEngine_Input_get_touchSupported();

		//System.Int32 UnityEngine.Input::get_touchCount()
		void Register_UnityEngine_Input_get_touchCount();
		Register_UnityEngine_Input_get_touchCount();

		//System.Single UnityEngine.Input::GetAxis(System.String)
		void Register_UnityEngine_Input_GetAxis();
		Register_UnityEngine_Input_GetAxis();

		//System.Single UnityEngine.Input::GetAxisRaw(System.String)
		void Register_UnityEngine_Input_GetAxisRaw();
		Register_UnityEngine_Input_GetAxisRaw();

		//System.String UnityEngine.Input::get_compositionString()
		void Register_UnityEngine_Input_get_compositionString();
		Register_UnityEngine_Input_get_compositionString();

		//System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
		void Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();
		Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();

		//System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();

		//System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_mousePosition();
		Register_UnityEngine_Input_INTERNAL_get_mousePosition();

		//System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();
		Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();

		//System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();

		//System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
		void Register_UnityEngine_Input_set_imeCompositionMode();
		Register_UnityEngine_Input_set_imeCompositionMode();

		//System.Void UnityEngine.Input::set_multiTouchEnabled(System.Boolean)
		void Register_UnityEngine_Input_set_multiTouchEnabled();
		Register_UnityEngine_Input_set_multiTouchEnabled();

		//System.Void UnityEngine.Input::set_simulateMouseWithTouches(System.Boolean)
		void Register_UnityEngine_Input_set_simulateMouseWithTouches();
		Register_UnityEngine_Input_set_simulateMouseWithTouches();

		//UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
		void Register_UnityEngine_Input_get_imeCompositionMode();
		Register_UnityEngine_Input_get_imeCompositionMode();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.iOS.NotificationHelper

		//System.Void UnityEngine.iOS.NotificationHelper::DestroyLocal(System.IntPtr)
		void Register_UnityEngine_iOS_NotificationHelper_DestroyLocal();
		Register_UnityEngine_iOS_NotificationHelper_DestroyLocal();

		//System.Void UnityEngine.iOS.NotificationHelper::DestroyRemote(System.IntPtr)
		void Register_UnityEngine_iOS_NotificationHelper_DestroyRemote();
		Register_UnityEngine_iOS_NotificationHelper_DestroyRemote();

	//End Registrations for type : UnityEngine.iOS.NotificationHelper

	//Start Registrations for type : UnityEngine.JsonUtility

		//System.Object UnityEngine.JsonUtility::FromJsonInternal(System.String,System.Object,System.Type)
		void Register_UnityEngine_JsonUtility_FromJsonInternal();
		Register_UnityEngine_JsonUtility_FromJsonInternal();

		//System.String UnityEngine.JsonUtility::ToJsonInternal(System.Object,System.Boolean)
		void Register_UnityEngine_JsonUtility_ToJsonInternal();
		Register_UnityEngine_JsonUtility_ToJsonInternal();

	//End Registrations for type : UnityEngine.JsonUtility

	//Start Registrations for type : UnityEngine.Light

		//System.Single UnityEngine.Light::get_cookieSize()
		void Register_UnityEngine_Light_get_cookieSize();
		Register_UnityEngine_Light_get_cookieSize();

		//System.Single UnityEngine.Light::get_range()
		void Register_UnityEngine_Light_get_range();
		Register_UnityEngine_Light_get_range();

		//System.Single UnityEngine.Light::get_shadowBias()
		void Register_UnityEngine_Light_get_shadowBias();
		Register_UnityEngine_Light_get_shadowBias();

		//System.Single UnityEngine.Light::get_shadowNearPlane()
		void Register_UnityEngine_Light_get_shadowNearPlane();
		Register_UnityEngine_Light_get_shadowNearPlane();

		//System.Single UnityEngine.Light::get_shadowNormalBias()
		void Register_UnityEngine_Light_get_shadowNormalBias();
		Register_UnityEngine_Light_get_shadowNormalBias();

		//System.Single UnityEngine.Light::get_shadowStrength()
		void Register_UnityEngine_Light_get_shadowStrength();
		Register_UnityEngine_Light_get_shadowStrength();

		//System.Single UnityEngine.Light::get_spotAngle()
		void Register_UnityEngine_Light_get_spotAngle();
		Register_UnityEngine_Light_get_spotAngle();

		//System.Void UnityEngine.Light::get_bakingOutput_Injected(UnityEngine.LightBakingOutput&)
		void Register_UnityEngine_Light_get_bakingOutput_Injected();
		Register_UnityEngine_Light_get_bakingOutput_Injected();

		//System.Void UnityEngine.Light::set_color_Injected(UnityEngine.Color&)
		void Register_UnityEngine_Light_set_color_Injected();
		Register_UnityEngine_Light_set_color_Injected();

		//System.Void UnityEngine.Light::set_intensity(System.Single)
		void Register_UnityEngine_Light_set_intensity();
		Register_UnityEngine_Light_set_intensity();

		//UnityEngine.LightShadows UnityEngine.Light::get_shadows()
		void Register_UnityEngine_Light_get_shadows();
		Register_UnityEngine_Light_get_shadows();

		//UnityEngine.LightType UnityEngine.Light::get_type()
		void Register_UnityEngine_Light_get_type();
		Register_UnityEngine_Light_get_type();

		//UnityEngine.Texture UnityEngine.Light::get_cookie()
		void Register_UnityEngine_Light_get_cookie();
		Register_UnityEngine_Light_get_cookie();

	//End Registrations for type : UnityEngine.Light

	//Start Registrations for type : UnityEngine.LineRenderer

		//System.Int32 UnityEngine.LineRenderer::get_positionCount()
		void Register_UnityEngine_LineRenderer_get_positionCount();
		Register_UnityEngine_LineRenderer_get_positionCount();

		//System.Void UnityEngine.LineRenderer::SetPosition_Injected(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_LineRenderer_SetPosition_Injected();
		Register_UnityEngine_LineRenderer_SetPosition_Injected();

		//System.Void UnityEngine.LineRenderer::get_startColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_LineRenderer_get_startColor_Injected();
		Register_UnityEngine_LineRenderer_get_startColor_Injected();

		//System.Void UnityEngine.LineRenderer::set_endColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_LineRenderer_set_endColor_Injected();
		Register_UnityEngine_LineRenderer_set_endColor_Injected();

		//System.Void UnityEngine.LineRenderer::set_endWidth(System.Single)
		void Register_UnityEngine_LineRenderer_set_endWidth();
		Register_UnityEngine_LineRenderer_set_endWidth();

		//System.Void UnityEngine.LineRenderer::set_positionCount(System.Int32)
		void Register_UnityEngine_LineRenderer_set_positionCount();
		Register_UnityEngine_LineRenderer_set_positionCount();

		//System.Void UnityEngine.LineRenderer::set_startColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_LineRenderer_set_startColor_Injected();
		Register_UnityEngine_LineRenderer_set_startColor_Injected();

		//System.Void UnityEngine.LineRenderer::set_startWidth(System.Single)
		void Register_UnityEngine_LineRenderer_set_startWidth();
		Register_UnityEngine_LineRenderer_set_startWidth();

	//End Registrations for type : UnityEngine.LineRenderer

	//Start Registrations for type : UnityEngine.Material

		//System.Boolean UnityEngine.Material::HasProperty(System.Int32)
		void Register_UnityEngine_Material_HasProperty();
		Register_UnityEngine_Material_HasProperty();

		//System.Boolean UnityEngine.Material::SetPass(System.Int32)
		void Register_UnityEngine_Material_SetPass();
		Register_UnityEngine_Material_SetPass();

		//System.Int32 UnityEngine.Material::FindPass(System.String)
		void Register_UnityEngine_Material_FindPass();
		Register_UnityEngine_Material_FindPass();

		//System.Void UnityEngine.Material::CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
		void Register_UnityEngine_Material_CreateWithMaterial();
		Register_UnityEngine_Material_CreateWithMaterial();

		//System.Void UnityEngine.Material::CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
		void Register_UnityEngine_Material_CreateWithShader();
		Register_UnityEngine_Material_CreateWithShader();

		//System.Void UnityEngine.Material::CreateWithString(UnityEngine.Material)
		void Register_UnityEngine_Material_CreateWithString();
		Register_UnityEngine_Material_CreateWithString();

		//System.Void UnityEngine.Material::DisableKeyword(System.String)
		void Register_UnityEngine_Material_DisableKeyword();
		Register_UnityEngine_Material_DisableKeyword();

		//System.Void UnityEngine.Material::EnableKeyword(System.String)
		void Register_UnityEngine_Material_EnableKeyword();
		Register_UnityEngine_Material_EnableKeyword();

		//System.Void UnityEngine.Material::GetColorImpl_Injected(System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_GetColorImpl_Injected();
		Register_UnityEngine_Material_GetColorImpl_Injected();

		//System.Void UnityEngine.Material::SetColorImpl_Injected(System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_SetColorImpl_Injected();
		Register_UnityEngine_Material_SetColorImpl_Injected();

		//System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_Material_SetFloatImpl();
		Register_UnityEngine_Material_SetFloatImpl();

		//System.Void UnityEngine.Material::SetShaderKeywords(System.String[])
		void Register_UnityEngine_Material_SetShaderKeywords();
		Register_UnityEngine_Material_SetShaderKeywords();

		//System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Material_SetTextureImpl();
		Register_UnityEngine_Material_SetTextureImpl();

		//UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
		void Register_UnityEngine_Material_GetTextureImpl();
		Register_UnityEngine_Material_GetTextureImpl();

	//End Registrations for type : UnityEngine.Material

	//Start Registrations for type : UnityEngine.MaterialPropertyBlock

		//System.IntPtr UnityEngine.MaterialPropertyBlock::CreateImpl()
		void Register_UnityEngine_MaterialPropertyBlock_CreateImpl();
		Register_UnityEngine_MaterialPropertyBlock_CreateImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::Clear(System.Boolean)
		void Register_UnityEngine_MaterialPropertyBlock_Clear();
		Register_UnityEngine_MaterialPropertyBlock_Clear();

		//System.Void UnityEngine.MaterialPropertyBlock::DestroyImpl(System.IntPtr)
		void Register_UnityEngine_MaterialPropertyBlock_DestroyImpl();
		Register_UnityEngine_MaterialPropertyBlock_DestroyImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::SetBufferImpl(System.Int32,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_MaterialPropertyBlock_SetBufferImpl();
		Register_UnityEngine_MaterialPropertyBlock_SetBufferImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl_Injected(System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_MaterialPropertyBlock_SetColorImpl_Injected();
		Register_UnityEngine_MaterialPropertyBlock_SetColorImpl_Injected();

		//System.Void UnityEngine.MaterialPropertyBlock::SetFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_MaterialPropertyBlock_SetFloatImpl();
		Register_UnityEngine_MaterialPropertyBlock_SetFloatImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::SetMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_MaterialPropertyBlock_SetMatrixImpl_Injected();
		Register_UnityEngine_MaterialPropertyBlock_SetMatrixImpl_Injected();

		//System.Void UnityEngine.MaterialPropertyBlock::SetTextureImpl(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_MaterialPropertyBlock_SetTextureImpl();
		Register_UnityEngine_MaterialPropertyBlock_SetTextureImpl();

		//System.Void UnityEngine.MaterialPropertyBlock::SetVectorImpl_Injected(System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_MaterialPropertyBlock_SetVectorImpl_Injected();
		Register_UnityEngine_MaterialPropertyBlock_SetVectorImpl_Injected();

	//End Registrations for type : UnityEngine.MaterialPropertyBlock

	//Start Registrations for type : UnityEngine.Mathf

		//System.Int32 UnityEngine.Mathf::ClosestPowerOfTwo(System.Int32)
		void Register_UnityEngine_Mathf_ClosestPowerOfTwo();
		Register_UnityEngine_Mathf_ClosestPowerOfTwo();

		//System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
		void Register_UnityEngine_Mathf_GammaToLinearSpace();
		Register_UnityEngine_Mathf_GammaToLinearSpace();

		//System.Single UnityEngine.Mathf::LinearToGammaSpace(System.Single)
		void Register_UnityEngine_Mathf_LinearToGammaSpace();
		Register_UnityEngine_Mathf_LinearToGammaSpace();

	//End Registrations for type : UnityEngine.Mathf

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Void UnityEngine.Matrix4x4::DecomposeProjection_Injected(UnityEngine.Matrix4x4&,UnityEngine.FrustumPlanes&)
		void Register_UnityEngine_Matrix4x4_DecomposeProjection_Injected();
		Register_UnityEngine_Matrix4x4_DecomposeProjection_Injected();

		//System.Void UnityEngine.Matrix4x4::Frustum_Injected(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_Frustum_Injected();
		Register_UnityEngine_Matrix4x4_Frustum_Injected();

		//System.Void UnityEngine.Matrix4x4::Inverse_Injected(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_Inverse_Injected();
		Register_UnityEngine_Matrix4x4_Inverse_Injected();

		//System.Void UnityEngine.Matrix4x4::Ortho_Injected(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_Ortho_Injected();
		Register_UnityEngine_Matrix4x4_Ortho_Injected();

		//System.Void UnityEngine.Matrix4x4::Perspective_Injected(System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_Perspective_Injected();
		Register_UnityEngine_Matrix4x4_Perspective_Injected();

		//System.Void UnityEngine.Matrix4x4::TRS_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_TRS_Injected();
		Register_UnityEngine_Matrix4x4_TRS_Injected();

		//System.Void UnityEngine.Matrix4x4::Transpose_Injected(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_Transpose_Injected();
		Register_UnityEngine_Matrix4x4_Transpose_Injected();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Mesh

		//System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
		void Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();
		Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();

		//System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_HasChannel();
		Register_UnityEngine_Mesh_HasChannel();

		//System.Boolean UnityEngine.Mesh::get_canAccess()
		void Register_UnityEngine_Mesh_get_canAccess();
		Register_UnityEngine_Mesh_get_canAccess();

		//System.Int32 UnityEngine.Mesh::get_subMeshCount()
		void Register_UnityEngine_Mesh_get_subMeshCount();
		Register_UnityEngine_Mesh_get_subMeshCount();

		//System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32,System.Boolean)
		void Register_UnityEngine_Mesh_GetIndicesImpl();
		Register_UnityEngine_Mesh_GetIndicesImpl();

		//System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
		void Register_UnityEngine_Mesh_ClearImpl();
		Register_UnityEngine_Mesh_ClearImpl();

		//System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
		void Register_UnityEngine_Mesh_Internal_Create();
		Register_UnityEngine_Mesh_Internal_Create();

		//System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_PrintErrorCantAccessChannel();
		Register_UnityEngine_Mesh_PrintErrorCantAccessChannel();

		//System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
		void Register_UnityEngine_Mesh_RecalculateBoundsImpl();
		Register_UnityEngine_Mesh_RecalculateBoundsImpl();

		//System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
		void Register_UnityEngine_Mesh_SetArrayForChannelImpl();
		Register_UnityEngine_Mesh_SetArrayForChannelImpl();

		//System.Void UnityEngine.Mesh::SetIndicesImpl(System.Int32,UnityEngine.MeshTopology,System.Array,System.Int32,System.Boolean,System.Int32)
		void Register_UnityEngine_Mesh_SetIndicesImpl();
		Register_UnityEngine_Mesh_SetIndicesImpl();

		//System.Void UnityEngine.Mesh::UploadMeshDataImpl(System.Boolean)
		void Register_UnityEngine_Mesh_UploadMeshDataImpl();
		Register_UnityEngine_Mesh_UploadMeshDataImpl();

	//End Registrations for type : UnityEngine.Mesh

	//Start Registrations for type : UnityEngine.MeshCollider

		//System.Boolean UnityEngine.MeshCollider::get_convex()
		void Register_UnityEngine_MeshCollider_get_convex();
		Register_UnityEngine_MeshCollider_get_convex();

		//System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
		void Register_UnityEngine_MeshCollider_set_convex();
		Register_UnityEngine_MeshCollider_set_convex();

		//UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh()
		void Register_UnityEngine_MeshCollider_get_sharedMesh();
		Register_UnityEngine_MeshCollider_get_sharedMesh();

	//End Registrations for type : UnityEngine.MeshCollider

	//Start Registrations for type : UnityEngine.MonoBehaviour

		//System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll(UnityEngine.MonoBehaviour)
		void Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll();
		Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll();

		//System.Boolean UnityEngine.MonoBehaviour::IsInvoking(UnityEngine.MonoBehaviour,System.String)
		void Register_UnityEngine_MonoBehaviour_IsInvoking();
		Register_UnityEngine_MonoBehaviour_IsInvoking();

		//System.Boolean UnityEngine.MonoBehaviour::IsObjectMonoBehaviour(UnityEngine.Object)
		void Register_UnityEngine_MonoBehaviour_IsObjectMonoBehaviour();
		Register_UnityEngine_MonoBehaviour_IsObjectMonoBehaviour();

		//System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
		void Register_UnityEngine_MonoBehaviour_get_useGUILayout();
		Register_UnityEngine_MonoBehaviour_get_useGUILayout();

		//System.String UnityEngine.MonoBehaviour::GetScriptClassName()
		void Register_UnityEngine_MonoBehaviour_GetScriptClassName();
		Register_UnityEngine_MonoBehaviour_GetScriptClassName();

		//System.Void UnityEngine.MonoBehaviour::CancelInvoke(UnityEngine.MonoBehaviour,System.String)
		void Register_UnityEngine_MonoBehaviour_CancelInvoke();
		Register_UnityEngine_MonoBehaviour_CancelInvoke();

		//System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll(UnityEngine.MonoBehaviour)
		void Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();
		Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();

		//System.Void UnityEngine.MonoBehaviour::InvokeDelayed(UnityEngine.MonoBehaviour,System.String,System.Single,System.Single)
		void Register_UnityEngine_MonoBehaviour_InvokeDelayed();
		Register_UnityEngine_MonoBehaviour_InvokeDelayed();

		//System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
		void Register_UnityEngine_MonoBehaviour_StopAllCoroutines();
		Register_UnityEngine_MonoBehaviour_StopAllCoroutines();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine();
		Register_UnityEngine_MonoBehaviour_StopCoroutine();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineFromEnumeratorManaged(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineFromEnumeratorManaged();
		Register_UnityEngine_MonoBehaviour_StopCoroutineFromEnumeratorManaged();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineManaged(UnityEngine.Coroutine)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineManaged();
		Register_UnityEngine_MonoBehaviour_StopCoroutineManaged();

		//System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
		void Register_UnityEngine_MonoBehaviour_set_useGUILayout();
		Register_UnityEngine_MonoBehaviour_set_useGUILayout();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged(System.String,System.Object)
		void Register_UnityEngine_MonoBehaviour_StartCoroutineManaged();
		Register_UnityEngine_MonoBehaviour_StartCoroutineManaged();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged2(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StartCoroutineManaged2();
		Register_UnityEngine_MonoBehaviour_StartCoroutineManaged2();

	//End Registrations for type : UnityEngine.MonoBehaviour

	//Start Registrations for type : UnityEngine.Networking.CertificateHandler

		//System.Void UnityEngine.Networking.CertificateHandler::Release()
		void Register_UnityEngine_Networking_CertificateHandler_Release();
		Register_UnityEngine_Networking_CertificateHandler_Release();

	//End Registrations for type : UnityEngine.Networking.CertificateHandler

	//Start Registrations for type : UnityEngine.Networking.ConnectionConfigInternal

		//System.Boolean UnityEngine.Networking.ConnectionConfigInternal::MakeChannelsSharedOrder(System.Byte[])
		void Register_UnityEngine_Networking_ConnectionConfigInternal_MakeChannelsSharedOrder();
		Register_UnityEngine_Networking_ConnectionConfigInternal_MakeChannelsSharedOrder();

		//System.Byte UnityEngine.Networking.ConnectionConfigInternal::AddChannel(UnityEngine.Networking.QosType)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_AddChannel();
		Register_UnityEngine_Networking_ConnectionConfigInternal_AddChannel();

		//System.Int32 UnityEngine.Networking.ConnectionConfigInternal::InitSSLCAFilePath(System.String)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitSSLCAFilePath();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitSSLCAFilePath();

		//System.Int32 UnityEngine.Networking.ConnectionConfigInternal::InitSSLCertFilePath(System.String)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitSSLCertFilePath();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitSSLCertFilePath();

		//System.Int32 UnityEngine.Networking.ConnectionConfigInternal::InitSSLPrivateKeyFilePath(System.String)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitSSLPrivateKeyFilePath();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitSSLPrivateKeyFilePath();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::Dispose()
		void Register_UnityEngine_Networking_ConnectionConfigInternal_Dispose();
		Register_UnityEngine_Networking_ConnectionConfigInternal_Dispose();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitAckDelay(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitAckDelay();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitAckDelay();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitAcksType(System.Int32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitAcksType();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitAcksType();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitAllCostTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitAllCostTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitAllCostTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitBandwidthPeakFactor(System.Single)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitBandwidthPeakFactor();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitBandwidthPeakFactor();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitConnectTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitConnectTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitConnectTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitDisconnectTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitDisconnectTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitDisconnectTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitFragmentSize(System.UInt16)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitFragmentSize();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitFragmentSize();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitInitialBandwidth(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitInitialBandwidth();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitInitialBandwidth();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxCombinedReliableMessageCount(System.UInt16)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageCount();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageCount();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxCombinedReliableMessageSize(System.UInt16)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageSize();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxCombinedReliableMessageSize();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxConnectionAttempt(System.Byte)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxConnectionAttempt();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxConnectionAttempt();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxSentMessageQueueSize(System.UInt16)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxSentMessageQueueSize();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitMaxSentMessageQueueSize();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMinUpdateTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitMinUpdateTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitMinUpdateTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitNetworkDropThreshold(System.Byte)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitNetworkDropThreshold();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitNetworkDropThreshold();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitOverflowDropThreshold(System.Byte)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitOverflowDropThreshold();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitOverflowDropThreshold();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitPacketSize(System.UInt16)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitPacketSize();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitPacketSize();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitPingTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitPingTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitPingTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitReducedPingTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitReducedPingTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitReducedPingTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitResendTimeout(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitResendTimeout();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitResendTimeout();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitSendDelay(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitSendDelay();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitSendDelay();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitUdpSocketReceiveBufferMaxSize(System.UInt32)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitUdpSocketReceiveBufferMaxSize();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitUdpSocketReceiveBufferMaxSize();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitUsePlatformSpecificProtocols(System.Boolean)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitUsePlatformSpecificProtocols();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitUsePlatformSpecificProtocols();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitWebSocketReceiveBufferMaxSize(System.UInt16)
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitWebSocketReceiveBufferMaxSize();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitWebSocketReceiveBufferMaxSize();

		//System.Void UnityEngine.Networking.ConnectionConfigInternal::InitWrapper()
		void Register_UnityEngine_Networking_ConnectionConfigInternal_InitWrapper();
		Register_UnityEngine_Networking_ConnectionConfigInternal_InitWrapper();

	//End Registrations for type : UnityEngine.Networking.ConnectionConfigInternal

	//Start Registrations for type : UnityEngine.Networking.ConnectionSimulatorConfig

		//System.Void UnityEngine.Networking.ConnectionSimulatorConfig::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Networking_ConnectionSimulatorConfig__ctor();
		Register_UnityEngine_Networking_ConnectionSimulatorConfig__ctor();

		//System.Void UnityEngine.Networking.ConnectionSimulatorConfig::Dispose()
		void Register_UnityEngine_Networking_ConnectionSimulatorConfig_Dispose();
		Register_UnityEngine_Networking_ConnectionSimulatorConfig_Dispose();

	//End Registrations for type : UnityEngine.Networking.ConnectionSimulatorConfig

	//Start Registrations for type : UnityEngine.Networking.DownloadHandler

		//System.Byte[] UnityEngine.Networking.DownloadHandler::InternalGetByteArray(UnityEngine.Networking.DownloadHandler)
		void Register_UnityEngine_Networking_DownloadHandler_InternalGetByteArray();
		Register_UnityEngine_Networking_DownloadHandler_InternalGetByteArray();

		//System.String UnityEngine.Networking.DownloadHandler::GetContentType()
		void Register_UnityEngine_Networking_DownloadHandler_GetContentType();
		Register_UnityEngine_Networking_DownloadHandler_GetContentType();

		//System.Void UnityEngine.Networking.DownloadHandler::Release()
		void Register_UnityEngine_Networking_DownloadHandler_Release();
		Register_UnityEngine_Networking_DownloadHandler_Release();

	//End Registrations for type : UnityEngine.Networking.DownloadHandler

	//Start Registrations for type : UnityEngine.Networking.DownloadHandlerBuffer

		//System.IntPtr UnityEngine.Networking.DownloadHandlerBuffer::Create(UnityEngine.Networking.DownloadHandlerBuffer)
		void Register_UnityEngine_Networking_DownloadHandlerBuffer_Create();
		Register_UnityEngine_Networking_DownloadHandlerBuffer_Create();

	//End Registrations for type : UnityEngine.Networking.DownloadHandlerBuffer

	//Start Registrations for type : UnityEngine.Networking.GlobalConfigInternal

		//System.Void UnityEngine.Networking.GlobalConfigInternal::Dispose()
		void Register_UnityEngine_Networking_GlobalConfigInternal_Dispose();
		Register_UnityEngine_Networking_GlobalConfigInternal_Dispose();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitMaxHosts(System.UInt16)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxHosts();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxHosts();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitMaxNetSimulatorTimeout(System.UInt32)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxNetSimulatorTimeout();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxNetSimulatorTimeout();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitMaxPacketSize(System.UInt16)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxPacketSize();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxPacketSize();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitMaxTimerTimeout(System.UInt32)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxTimerTimeout();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitMaxTimerTimeout();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitMinNetSimulatorTimeout(System.UInt32)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitMinNetSimulatorTimeout();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitMinNetSimulatorTimeout();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitMinTimerTimeout(System.UInt32)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitMinTimerTimeout();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitMinTimerTimeout();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitReactorMaximumReceivedMessages(System.UInt16)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumReceivedMessages();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumReceivedMessages();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitReactorMaximumSentMessages(System.UInt16)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumSentMessages();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorMaximumSentMessages();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitReactorModel(System.Byte)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorModel();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitReactorModel();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitThreadAwakeTimeout(System.UInt32)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitThreadAwakeTimeout();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitThreadAwakeTimeout();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitThreadPoolSize(System.Byte)
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitThreadPoolSize();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitThreadPoolSize();

		//System.Void UnityEngine.Networking.GlobalConfigInternal::InitWrapper()
		void Register_UnityEngine_Networking_GlobalConfigInternal_InitWrapper();
		Register_UnityEngine_Networking_GlobalConfigInternal_InitWrapper();

	//End Registrations for type : UnityEngine.Networking.GlobalConfigInternal

	//Start Registrations for type : UnityEngine.Networking.HostTopologyInternal

		//System.Int32 UnityEngine.Networking.HostTopologyInternal::AddSpecialConnectionConfigWrapper(UnityEngine.Networking.ConnectionConfigInternal)
		void Register_UnityEngine_Networking_HostTopologyInternal_AddSpecialConnectionConfigWrapper();
		Register_UnityEngine_Networking_HostTopologyInternal_AddSpecialConnectionConfigWrapper();

		//System.Void UnityEngine.Networking.HostTopologyInternal::Dispose()
		void Register_UnityEngine_Networking_HostTopologyInternal_Dispose();
		Register_UnityEngine_Networking_HostTopologyInternal_Dispose();

		//System.Void UnityEngine.Networking.HostTopologyInternal::InitMessagePoolSizeGrowthFactor(System.Single)
		void Register_UnityEngine_Networking_HostTopologyInternal_InitMessagePoolSizeGrowthFactor();
		Register_UnityEngine_Networking_HostTopologyInternal_InitMessagePoolSizeGrowthFactor();

		//System.Void UnityEngine.Networking.HostTopologyInternal::InitReceivedPoolSize(System.UInt16)
		void Register_UnityEngine_Networking_HostTopologyInternal_InitReceivedPoolSize();
		Register_UnityEngine_Networking_HostTopologyInternal_InitReceivedPoolSize();

		//System.Void UnityEngine.Networking.HostTopologyInternal::InitSentMessagePoolSize(System.UInt16)
		void Register_UnityEngine_Networking_HostTopologyInternal_InitSentMessagePoolSize();
		Register_UnityEngine_Networking_HostTopologyInternal_InitSentMessagePoolSize();

		//System.Void UnityEngine.Networking.HostTopologyInternal::InitWrapper(UnityEngine.Networking.ConnectionConfigInternal,System.Int32)
		void Register_UnityEngine_Networking_HostTopologyInternal_InitWrapper();
		Register_UnityEngine_Networking_HostTopologyInternal_InitWrapper();

	//End Registrations for type : UnityEngine.Networking.HostTopologyInternal

	//Start Registrations for type : UnityEngine.Networking.NetworkTransport

		//System.Boolean UnityEngine.Networking.NetworkTransport::Disconnect(System.Int32,System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_Disconnect();
		Register_UnityEngine_Networking_NetworkTransport_Disconnect();

		//System.Boolean UnityEngine.Networking.NetworkTransport::RemoveHost(System.Int32)
		void Register_UnityEngine_Networking_NetworkTransport_RemoveHost();
		Register_UnityEngine_Networking_NetworkTransport_RemoveHost();

		//System.Boolean UnityEngine.Networking.NetworkTransport::SendWrapper(System.Int32,System.Int32,System.Int32,System.Byte[],System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_SendWrapper();
		Register_UnityEngine_Networking_NetworkTransport_SendWrapper();

		//System.Boolean UnityEngine.Networking.NetworkTransport::StartBroadcastDiscoveryWithData(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Byte[],System.Int32,System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithData();
		Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithData();

		//System.Boolean UnityEngine.Networking.NetworkTransport::StartBroadcastDiscoveryWithoutData(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithoutData();
		Register_UnityEngine_Networking_NetworkTransport_StartBroadcastDiscoveryWithoutData();

		//System.Boolean UnityEngine.Networking.NetworkTransport::get_IsStarted()
		void Register_UnityEngine_Networking_NetworkTransport_get_IsStarted();
		Register_UnityEngine_Networking_NetworkTransport_get_IsStarted();

		//System.Int32 UnityEngine.Networking.NetworkTransport::AddHostWrapper(UnityEngine.Networking.HostTopologyInternal,System.String,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Networking_NetworkTransport_AddHostWrapper();
		Register_UnityEngine_Networking_NetworkTransport_AddHostWrapper();

		//System.Int32 UnityEngine.Networking.NetworkTransport::AddHostWrapperWithoutIp(UnityEngine.Networking.HostTopologyInternal,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Networking_NetworkTransport_AddHostWrapperWithoutIp();
		Register_UnityEngine_Networking_NetworkTransport_AddHostWrapperWithoutIp();

		//System.Int32 UnityEngine.Networking.NetworkTransport::AddWsHostWrapper(UnityEngine.Networking.HostTopologyInternal,System.String,System.Int32)
		void Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapper();
		Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapper();

		//System.Int32 UnityEngine.Networking.NetworkTransport::AddWsHostWrapperWithoutIp(UnityEngine.Networking.HostTopologyInternal,System.Int32)
		void Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapperWithoutIp();
		Register_UnityEngine_Networking_NetworkTransport_AddWsHostWrapperWithoutIp();

		//System.Int32 UnityEngine.Networking.NetworkTransport::Connect(System.Int32,System.String,System.Int32,System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_Connect();
		Register_UnityEngine_Networking_NetworkTransport_Connect();

		//System.Int32 UnityEngine.Networking.NetworkTransport::ConnectToNetworkPeer(System.Int32,System.String,System.Int32,System.Int32,System.Int32,UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.SourceID,UnityEngine.Networking.Types.NodeID,System.Int32,System.Single,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_ConnectToNetworkPeer();
		Register_UnityEngine_Networking_NetworkTransport_ConnectToNetworkPeer();

		//System.Int32 UnityEngine.Networking.NetworkTransport::ConnectWithSimulator(System.Int32,System.String,System.Int32,System.Int32,System.Byte&,UnityEngine.Networking.ConnectionSimulatorConfig)
		void Register_UnityEngine_Networking_NetworkTransport_ConnectWithSimulator();
		Register_UnityEngine_Networking_NetworkTransport_ConnectWithSimulator();

		//System.Int32 UnityEngine.Networking.NetworkTransport::GetMaxPacketSize()
		void Register_UnityEngine_Networking_NetworkTransport_GetMaxPacketSize();
		Register_UnityEngine_Networking_NetworkTransport_GetMaxPacketSize();

		//System.Int32 UnityEngine.Networking.NetworkTransport::Internal_ConnectEndPoint(System.Int32,System.IntPtr,System.Int32,System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_Internal_ConnectEndPoint();
		Register_UnityEngine_Networking_NetworkTransport_Internal_ConnectEndPoint();

		//System.String UnityEngine.Networking.NetworkTransport::GetBroadcastConnectionInfo(System.Int32,System.Int32&,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionInfo();
		Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionInfo();

		//System.String UnityEngine.Networking.NetworkTransport::GetConnectionInfo(System.Int32,System.Int32,System.Int32&,System.UInt64&,System.UInt16&,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_GetConnectionInfo();
		Register_UnityEngine_Networking_NetworkTransport_GetConnectionInfo();

		//System.Void UnityEngine.Networking.NetworkTransport::ConnectAsNetworkHost(System.Int32,System.String,System.Int32,UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.SourceID,UnityEngine.Networking.Types.NodeID,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_ConnectAsNetworkHost();
		Register_UnityEngine_Networking_NetworkTransport_ConnectAsNetworkHost();

		//System.Void UnityEngine.Networking.NetworkTransport::GetBroadcastConnectionMessage(System.Int32,System.Byte[],System.Int32,System.Int32&,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionMessage();
		Register_UnityEngine_Networking_NetworkTransport_GetBroadcastConnectionMessage();

		//System.Void UnityEngine.Networking.NetworkTransport::InitWithNoParameters()
		void Register_UnityEngine_Networking_NetworkTransport_InitWithNoParameters();
		Register_UnityEngine_Networking_NetworkTransport_InitWithNoParameters();

		//System.Void UnityEngine.Networking.NetworkTransport::InitWithParameters(UnityEngine.Networking.GlobalConfigInternal)
		void Register_UnityEngine_Networking_NetworkTransport_InitWithParameters();
		Register_UnityEngine_Networking_NetworkTransport_InitWithParameters();

		//System.Void UnityEngine.Networking.NetworkTransport::SetBroadcastCredentials(System.Int32,System.Int32,System.Int32,System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_SetBroadcastCredentials();
		Register_UnityEngine_Networking_NetworkTransport_SetBroadcastCredentials();

		//System.Void UnityEngine.Networking.NetworkTransport::SetConnectionReadyForSendCallback(System.Action`2<System.Int32,System.Int32>)
		void Register_UnityEngine_Networking_NetworkTransport_SetConnectionReadyForSendCallback();
		Register_UnityEngine_Networking_NetworkTransport_SetConnectionReadyForSendCallback();

		//System.Void UnityEngine.Networking.NetworkTransport::SetNetworkEventAvailableCallback(System.Action`1<System.Int32>)
		void Register_UnityEngine_Networking_NetworkTransport_SetNetworkEventAvailableCallback();
		Register_UnityEngine_Networking_NetworkTransport_SetNetworkEventAvailableCallback();

		//System.Void UnityEngine.Networking.NetworkTransport::StopBroadcastDiscovery()
		void Register_UnityEngine_Networking_NetworkTransport_StopBroadcastDiscovery();
		Register_UnityEngine_Networking_NetworkTransport_StopBroadcastDiscovery();

		//UnityEngine.Networking.NetworkEventType UnityEngine.Networking.NetworkTransport::ReceiveFromHost(System.Int32,System.Int32&,System.Int32&,System.Byte[],System.Int32,System.Int32&,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_ReceiveFromHost();
		Register_UnityEngine_Networking_NetworkTransport_ReceiveFromHost();

		//UnityEngine.Networking.NetworkEventType UnityEngine.Networking.NetworkTransport::ReceiveRelayEventFromHost(System.Int32,System.Byte&)
		void Register_UnityEngine_Networking_NetworkTransport_ReceiveRelayEventFromHost();
		Register_UnityEngine_Networking_NetworkTransport_ReceiveRelayEventFromHost();

	//End Registrations for type : UnityEngine.Networking.NetworkTransport

	//Start Registrations for type : UnityEngine.Networking.UnityWebRequest

		//System.Boolean UnityEngine.Networking.UnityWebRequest::IsExecuting()
		void Register_UnityEngine_Networking_UnityWebRequest_IsExecuting();
		Register_UnityEngine_Networking_UnityWebRequest_IsExecuting();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isDone();
		Register_UnityEngine_Networking_UnityWebRequest_get_isDone();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isHttpError()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isHttpError();
		Register_UnityEngine_Networking_UnityWebRequest_get_isHttpError();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isModifiable();
		Register_UnityEngine_Networking_UnityWebRequest_get_isModifiable();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isNetworkError();
		Register_UnityEngine_Networking_UnityWebRequest_get_isNetworkError();

		//System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
		void Register_UnityEngine_Networking_UnityWebRequest_get_responseCode();
		Register_UnityEngine_Networking_UnityWebRequest_get_responseCode();

		//System.IntPtr UnityEngine.Networking.UnityWebRequest::Create()
		void Register_UnityEngine_Networking_UnityWebRequest_Create();
		Register_UnityEngine_Networking_UnityWebRequest_Create();

		//System.Single UnityEngine.Networking.UnityWebRequest::GetUploadProgress()
		void Register_UnityEngine_Networking_UnityWebRequest_GetUploadProgress();
		Register_UnityEngine_Networking_UnityWebRequest_GetUploadProgress();

		//System.String UnityEngine.Networking.UnityWebRequest::GetUrl()
		void Register_UnityEngine_Networking_UnityWebRequest_GetUrl();
		Register_UnityEngine_Networking_UnityWebRequest_GetUrl();

		//System.String UnityEngine.Networking.UnityWebRequest::GetWebErrorString(UnityEngine.Networking.UnityWebRequest/UnityWebRequestError)
		void Register_UnityEngine_Networking_UnityWebRequest_GetWebErrorString();
		Register_UnityEngine_Networking_UnityWebRequest_GetWebErrorString();

		//System.Void UnityEngine.Networking.UnityWebRequest::Abort()
		void Register_UnityEngine_Networking_UnityWebRequest_Abort();
		Register_UnityEngine_Networking_UnityWebRequest_Abort();

		//System.Void UnityEngine.Networking.UnityWebRequest::Release()
		void Register_UnityEngine_Networking_UnityWebRequest_Release();
		Register_UnityEngine_Networking_UnityWebRequest_Release();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::GetError()
		void Register_UnityEngine_Networking_UnityWebRequest_GetError();
		Register_UnityEngine_Networking_UnityWebRequest_GetError();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_InternalSetRequestHeader();
		Register_UnityEngine_Networking_UnityWebRequest_InternalSetRequestHeader();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetChunked(System.Boolean)
		void Register_UnityEngine_Networking_UnityWebRequest_SetChunked();
		Register_UnityEngine_Networking_UnityWebRequest_SetChunked();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCustomMethod(System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_SetCustomMethod();
		Register_UnityEngine_Networking_UnityWebRequest_SetCustomMethod();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetDownloadHandler(UnityEngine.Networking.DownloadHandler)
		void Register_UnityEngine_Networking_UnityWebRequest_SetDownloadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_SetDownloadHandler();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)
		void Register_UnityEngine_Networking_UnityWebRequest_SetMethod();
		Register_UnityEngine_Networking_UnityWebRequest_SetMethod();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUploadHandler(UnityEngine.Networking.UploadHandler)
		void Register_UnityEngine_Networking_UnityWebRequest_SetUploadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_SetUploadHandler();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUrl(System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_SetUrl();
		Register_UnityEngine_Networking_UnityWebRequest_SetUrl();

		//UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::BeginWebRequest()
		void Register_UnityEngine_Networking_UnityWebRequest_BeginWebRequest();
		Register_UnityEngine_Networking_UnityWebRequest_BeginWebRequest();

	//End Registrations for type : UnityEngine.Networking.UnityWebRequest

	//Start Registrations for type : UnityEngine.Networking.UploadHandler

		//System.Void UnityEngine.Networking.UploadHandler::Release()
		void Register_UnityEngine_Networking_UploadHandler_Release();
		Register_UnityEngine_Networking_UploadHandler_Release();

	//End Registrations for type : UnityEngine.Networking.UploadHandler

	//Start Registrations for type : UnityEngine.Networking.UploadHandlerRaw

		//System.IntPtr UnityEngine.Networking.UploadHandlerRaw::Create(UnityEngine.Networking.UploadHandlerRaw,System.Byte[])
		void Register_UnityEngine_Networking_UploadHandlerRaw_Create();
		Register_UnityEngine_Networking_UploadHandlerRaw_Create();

		//System.Void UnityEngine.Networking.UploadHandlerRaw::InternalSetContentType(System.String)
		void Register_UnityEngine_Networking_UploadHandlerRaw_InternalSetContentType();
		Register_UnityEngine_Networking_UploadHandlerRaw_InternalSetContentType();

	//End Registrations for type : UnityEngine.Networking.UploadHandlerRaw

	//Start Registrations for type : UnityEngine.NoAllocHelpers

		//System.Array UnityEngine.NoAllocHelpers::ExtractArrayFromList(System.Object)
		void Register_UnityEngine_NoAllocHelpers_ExtractArrayFromList();
		Register_UnityEngine_NoAllocHelpers_ExtractArrayFromList();

	//End Registrations for type : UnityEngine.NoAllocHelpers

	//Start Registrations for type : UnityEngine.Object

		//System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
		void Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();
		Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();

		//System.String UnityEngine.Object::GetName(UnityEngine.Object)
		void Register_UnityEngine_Object_GetName();
		Register_UnityEngine_Object_GetName();

		//System.String UnityEngine.Object::ToString(UnityEngine.Object)
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

		//System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_Destroy();
		Register_UnityEngine_Object_Destroy();

		//System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
		void Register_UnityEngine_Object_DestroyImmediate();
		Register_UnityEngine_Object_DestroyImmediate();

		//System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
		void Register_UnityEngine_Object_DontDestroyOnLoad();
		Register_UnityEngine_Object_DontDestroyOnLoad();

		//System.Void UnityEngine.Object::SetName(UnityEngine.Object,System.String)
		void Register_UnityEngine_Object_SetName();
		Register_UnityEngine_Object_SetName();

		//System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
		void Register_UnityEngine_Object_set_hideFlags();
		Register_UnityEngine_Object_set_hideFlags();

		//UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
		void Register_UnityEngine_Object_get_hideFlags();
		Register_UnityEngine_Object_get_hideFlags();

		//UnityEngine.Object UnityEngine.Object::FindObjectFromInstanceID(System.Int32)
		void Register_UnityEngine_Object_FindObjectFromInstanceID();
		Register_UnityEngine_Object_FindObjectFromInstanceID();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
		void Register_UnityEngine_Object_Internal_CloneSingle();
		Register_UnityEngine_Object_Internal_CloneSingle();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Object_Internal_CloneSingleWithParent();
		Register_UnityEngine_Object_Internal_CloneSingleWithParent();

		//UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle_Injected(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Object_Internal_InstantiateSingle_Injected();
		Register_UnityEngine_Object_Internal_InstantiateSingle_Injected();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfType();
		Register_UnityEngine_Object_FindObjectsOfType();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.ParticleSystem

		//System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
		void Register_UnityEngine_ParticleSystem_IsAlive();
		Register_UnityEngine_ParticleSystem_IsAlive();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
		void Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();
		Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();

		//System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
		void Register_UnityEngine_ParticleSystem_Internal_Emit();
		Register_UnityEngine_ParticleSystem_Internal_Emit();

		//System.Void UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)
		void Register_UnityEngine_ParticleSystem_Internal_EmitOld();
		Register_UnityEngine_ParticleSystem_Internal_EmitOld();

		//System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
		void Register_UnityEngine_ParticleSystem_Play();
		Register_UnityEngine_ParticleSystem_Play();

	//End Registrations for type : UnityEngine.ParticleSystem

	//Start Registrations for type : UnityEngine.ParticleSystemRenderer

		//System.Int32 UnityEngine.ParticleSystemRenderer::Internal_GetMeshCount()
		void Register_UnityEngine_ParticleSystemRenderer_Internal_GetMeshCount();
		Register_UnityEngine_ParticleSystemRenderer_Internal_GetMeshCount();

	//End Registrations for type : UnityEngine.ParticleSystemRenderer

	//Start Registrations for type : UnityEngine.Physics

		//System.Boolean UnityEngine.Physics::Internal_Raycast_Injected(UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_Internal_Raycast_Injected();
		Register_UnityEngine_Physics_Internal_Raycast_Injected();

		//System.Boolean UnityEngine.Physics::Query_RaycastTest_Injected(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_Query_RaycastTest_Injected();
		Register_UnityEngine_Physics_Query_RaycastTest_Injected();

		//UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Injected(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_OverlapSphere_Injected();
		Register_UnityEngine_Physics_OverlapSphere_Injected();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_Internal_RaycastAll_Injected();
		Register_UnityEngine_Physics_Internal_RaycastAll_Injected();

	//End Registrations for type : UnityEngine.Physics

	//Start Registrations for type : UnityEngine.Physics2D

		//System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
		void Register_UnityEngine_Physics2D_get_queriesHitTriggers();
		Register_UnityEngine_Physics2D_get_queriesHitTriggers();

		//System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
		void Register_UnityEngine_Physics2D_GetRayIntersectionNonAlloc_Internal_Injected();
		Register_UnityEngine_Physics2D_GetRayIntersectionNonAlloc_Internal_Injected();

		//System.Int32 UnityEngine.Physics2D::OverlapCircleNonAlloc_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.Collider2D[])
		void Register_UnityEngine_Physics2D_OverlapCircleNonAlloc_Internal_Injected();
		Register_UnityEngine_Physics2D_OverlapCircleNonAlloc_Internal_Injected();

		//System.Int32 UnityEngine.Physics2D::OverlapPointNonAlloc_Internal_Injected(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&,UnityEngine.Collider2D[])
		void Register_UnityEngine_Physics2D_OverlapPointNonAlloc_Internal_Injected();
		Register_UnityEngine_Physics2D_OverlapPointNonAlloc_Internal_Injected();

		//System.Int32 UnityEngine.Physics2D::RaycastNonAlloc_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
		void Register_UnityEngine_Physics2D_RaycastNonAlloc_Internal_Injected();
		Register_UnityEngine_Physics2D_RaycastNonAlloc_Internal_Injected();

		//System.Void UnityEngine.Physics2D::CircleCast_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_CircleCast_Internal_Injected();
		Register_UnityEngine_Physics2D_CircleCast_Internal_Injected();

		//System.Void UnityEngine.Physics2D::Raycast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_Raycast_Internal_Injected();
		Register_UnityEngine_Physics2D_Raycast_Internal_Injected();

		//UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint_Internal_Injected(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_OverlapPoint_Internal_Injected();
		Register_UnityEngine_Physics2D_OverlapPoint_Internal_Injected();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_OverlapCircleAll_Internal_Injected();
		Register_UnityEngine_Physics2D_OverlapCircleAll_Internal_Injected();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::CircleCastAll_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_CircleCastAll_Internal_Injected();
		Register_UnityEngine_Physics2D_CircleCastAll_Internal_Injected();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_GetRayIntersectionAll_Internal_Injected();
		Register_UnityEngine_Physics2D_GetRayIntersectionAll_Internal_Injected();

	//End Registrations for type : UnityEngine.Physics2D

	//Start Registrations for type : UnityEngine.PlatformEffector2D

		//System.Void UnityEngine.PlatformEffector2D::set_rotationalOffset(System.Single)
		void Register_UnityEngine_PlatformEffector2D_set_rotationalOffset();
		Register_UnityEngine_PlatformEffector2D_set_rotationalOffset();

	//End Registrations for type : UnityEngine.PlatformEffector2D

	//Start Registrations for type : UnityEngine.Playables.PlayableHandle

		//System.Boolean UnityEngine.Playables.PlayableHandle::IsValid_Injected(UnityEngine.Playables.PlayableHandle&)
		void Register_UnityEngine_Playables_PlayableHandle_IsValid_Injected();
		Register_UnityEngine_Playables_PlayableHandle_IsValid_Injected();

		//System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType_Injected(UnityEngine.Playables.PlayableHandle&)
		void Register_UnityEngine_Playables_PlayableHandle_GetPlayableType_Injected();
		Register_UnityEngine_Playables_PlayableHandle_GetPlayableType_Injected();

	//End Registrations for type : UnityEngine.Playables.PlayableHandle

	//Start Registrations for type : UnityEngine.PlayerConnectionInternal

		//System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
		void Register_UnityEngine_PlayerConnectionInternal_IsConnected();
		Register_UnityEngine_PlayerConnectionInternal_IsConnected();

		//System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
		void Register_UnityEngine_PlayerConnectionInternal_DisconnectAll();
		Register_UnityEngine_PlayerConnectionInternal_DisconnectAll();

		//System.Void UnityEngine.PlayerConnectionInternal::Initialize()
		void Register_UnityEngine_PlayerConnectionInternal_Initialize();
		Register_UnityEngine_PlayerConnectionInternal_Initialize();

		//System.Void UnityEngine.PlayerConnectionInternal::PollInternal()
		void Register_UnityEngine_PlayerConnectionInternal_PollInternal();
		Register_UnityEngine_PlayerConnectionInternal_PollInternal();

		//System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
		void Register_UnityEngine_PlayerConnectionInternal_RegisterInternal();
		Register_UnityEngine_PlayerConnectionInternal_RegisterInternal();

		//System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
		void Register_UnityEngine_PlayerConnectionInternal_SendMessage();
		Register_UnityEngine_PlayerConnectionInternal_SendMessage();

		//System.Void UnityEngine.PlayerConnectionInternal::UnregisterInternal(System.String)
		void Register_UnityEngine_PlayerConnectionInternal_UnregisterInternal();
		Register_UnityEngine_PlayerConnectionInternal_UnregisterInternal();

	//End Registrations for type : UnityEngine.PlayerConnectionInternal

	//Start Registrations for type : UnityEngine.PlayerPrefs

		//System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
		void Register_UnityEngine_PlayerPrefs_HasKey();
		Register_UnityEngine_PlayerPrefs_HasKey();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_TrySetFloat();
		Register_UnityEngine_PlayerPrefs_TrySetFloat();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_TrySetInt();
		Register_UnityEngine_PlayerPrefs_TrySetInt();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_TrySetSetString();
		Register_UnityEngine_PlayerPrefs_TrySetSetString();

		//System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_GetInt();
		Register_UnityEngine_PlayerPrefs_GetInt();

		//System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_GetFloat();
		Register_UnityEngine_PlayerPrefs_GetFloat();

		//System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_GetString();
		Register_UnityEngine_PlayerPrefs_GetString();

		//System.Void UnityEngine.PlayerPrefs::DeleteAll()
		void Register_UnityEngine_PlayerPrefs_DeleteAll();
		Register_UnityEngine_PlayerPrefs_DeleteAll();

		//System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
		void Register_UnityEngine_PlayerPrefs_DeleteKey();
		Register_UnityEngine_PlayerPrefs_DeleteKey();

		//System.Void UnityEngine.PlayerPrefs::Save()
		void Register_UnityEngine_PlayerPrefs_Save();
		Register_UnityEngine_PlayerPrefs_Save();

	//End Registrations for type : UnityEngine.PlayerPrefs

	//Start Registrations for type : UnityEngine.Profiling.CustomSampler

		//UnityEngine.Profiling.CustomSampler UnityEngine.Profiling.CustomSampler::CreateInternal(System.String)
		void Register_UnityEngine_Profiling_CustomSampler_CreateInternal();
		Register_UnityEngine_Profiling_CustomSampler_CreateInternal();

	//End Registrations for type : UnityEngine.Profiling.CustomSampler

	//Start Registrations for type : UnityEngine.Profiling.Profiler

		//System.Int64 UnityEngine.Profiling.Profiler::GetAllocatedMemoryForGraphicsDriver()
		void Register_UnityEngine_Profiling_Profiler_GetAllocatedMemoryForGraphicsDriver();
		Register_UnityEngine_Profiling_Profiler_GetAllocatedMemoryForGraphicsDriver();

		//System.Int64 UnityEngine.Profiling.Profiler::GetTotalAllocatedMemoryLong()
		void Register_UnityEngine_Profiling_Profiler_GetTotalAllocatedMemoryLong();
		Register_UnityEngine_Profiling_Profiler_GetTotalAllocatedMemoryLong();

		//System.Int64 UnityEngine.Profiling.Profiler::GetTotalReservedMemoryLong()
		void Register_UnityEngine_Profiling_Profiler_GetTotalReservedMemoryLong();
		Register_UnityEngine_Profiling_Profiler_GetTotalReservedMemoryLong();

		//System.Int64 UnityEngine.Profiling.Profiler::GetTotalUnusedReservedMemoryLong()
		void Register_UnityEngine_Profiling_Profiler_GetTotalUnusedReservedMemoryLong();
		Register_UnityEngine_Profiling_Profiler_GetTotalUnusedReservedMemoryLong();

	//End Registrations for type : UnityEngine.Profiling.Profiler

	//Start Registrations for type : UnityEngine.Profiling.Recorder

		//System.Int32 UnityEngine.Profiling.Recorder::get_sampleBlockCount()
		void Register_UnityEngine_Profiling_Recorder_get_sampleBlockCount();
		Register_UnityEngine_Profiling_Recorder_get_sampleBlockCount();

		//System.Int64 UnityEngine.Profiling.Recorder::get_elapsedNanoseconds()
		void Register_UnityEngine_Profiling_Recorder_get_elapsedNanoseconds();
		Register_UnityEngine_Profiling_Recorder_get_elapsedNanoseconds();

		//System.Void UnityEngine.Profiling.Recorder::DisposeNative()
		void Register_UnityEngine_Profiling_Recorder_DisposeNative();
		Register_UnityEngine_Profiling_Recorder_DisposeNative();

	//End Registrations for type : UnityEngine.Profiling.Recorder

	//Start Registrations for type : UnityEngine.Profiling.Sampler

		//UnityEngine.Profiling.Recorder UnityEngine.Profiling.Sampler::GetRecorderInternal()
		void Register_UnityEngine_Profiling_Sampler_GetRecorderInternal();
		Register_UnityEngine_Profiling_Sampler_GetRecorderInternal();

		//UnityEngine.Profiling.Sampler UnityEngine.Profiling.Sampler::GetSamplerInternal(System.String)
		void Register_UnityEngine_Profiling_Sampler_GetSamplerInternal();
		Register_UnityEngine_Profiling_Sampler_GetSamplerInternal();

	//End Registrations for type : UnityEngine.Profiling.Sampler

	//Start Registrations for type : UnityEngine.PropertyNameUtils

		//System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
		void Register_UnityEngine_PropertyNameUtils_PropertyNameFromString_Injected();
		Register_UnityEngine_PropertyNameUtils_PropertyNameFromString_Injected();

	//End Registrations for type : UnityEngine.PropertyNameUtils

	//Start Registrations for type : UnityEngine.QualitySettings

		//System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
		void Register_UnityEngine_QualitySettings_get_antiAliasing();
		Register_UnityEngine_QualitySettings_get_antiAliasing();

		//System.Single UnityEngine.QualitySettings::get_shadowNearPlaneOffset()
		void Register_UnityEngine_QualitySettings_get_shadowNearPlaneOffset();
		Register_UnityEngine_QualitySettings_get_shadowNearPlaneOffset();

		//System.Void UnityEngine.QualitySettings::set_antiAliasing(System.Int32)
		void Register_UnityEngine_QualitySettings_set_antiAliasing();
		Register_UnityEngine_QualitySettings_set_antiAliasing();

		//UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
		void Register_UnityEngine_QualitySettings_get_activeColorSpace();
		Register_UnityEngine_QualitySettings_get_activeColorSpace();

	//End Registrations for type : UnityEngine.QualitySettings

	//Start Registrations for type : UnityEngine.Quaternion

		//System.Void UnityEngine.Quaternion::AngleAxis_Injected(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_AngleAxis_Injected();
		Register_UnityEngine_Quaternion_AngleAxis_Injected();

		//System.Void UnityEngine.Quaternion::Internal_FromEulerRad_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_Internal_FromEulerRad_Injected();
		Register_UnityEngine_Quaternion_Internal_FromEulerRad_Injected();

		//System.Void UnityEngine.Quaternion::Internal_ToEulerRad_Injected(UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_Internal_ToEulerRad_Injected();
		Register_UnityEngine_Quaternion_Internal_ToEulerRad_Injected();

		//System.Void UnityEngine.Quaternion::Inverse_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_Inverse_Injected();
		Register_UnityEngine_Quaternion_Inverse_Injected();

		//System.Void UnityEngine.Quaternion::Lerp_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_Lerp_Injected();
		Register_UnityEngine_Quaternion_Lerp_Injected();

		//System.Void UnityEngine.Quaternion::LookRotation_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_LookRotation_Injected();
		Register_UnityEngine_Quaternion_LookRotation_Injected();

		//System.Void UnityEngine.Quaternion::Slerp_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_Slerp_Injected();
		Register_UnityEngine_Quaternion_Slerp_Injected();

	//End Registrations for type : UnityEngine.Quaternion

	//Start Registrations for type : UnityEngine.Random

		//System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
		void Register_UnityEngine_Random_RandomRangeInt();
		Register_UnityEngine_Random_RandomRangeInt();

		//System.Single UnityEngine.Random::Range(System.Single,System.Single)
		void Register_UnityEngine_Random_Range();
		Register_UnityEngine_Random_Range();

		//System.Single UnityEngine.Random::get_value()
		void Register_UnityEngine_Random_get_value();
		Register_UnityEngine_Random_get_value();

		//System.Void UnityEngine.Random::GetRandomUnitCircle(UnityEngine.Vector2&)
		void Register_UnityEngine_Random_GetRandomUnitCircle();
		Register_UnityEngine_Random_GetRandomUnitCircle();

		//System.Void UnityEngine.Random::get_onUnitSphere_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Random_get_onUnitSphere_Injected();
		Register_UnityEngine_Random_get_onUnitSphere_Injected();

		//System.Void UnityEngine.Random::get_rotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Random_get_rotation_Injected();
		Register_UnityEngine_Random_get_rotation_Injected();

	//End Registrations for type : UnityEngine.Random

	//Start Registrations for type : UnityEngine.RectOffset

		//System.Int32 UnityEngine.RectOffset::get_bottom()
		void Register_UnityEngine_RectOffset_get_bottom();
		Register_UnityEngine_RectOffset_get_bottom();

		//System.Int32 UnityEngine.RectOffset::get_horizontal()
		void Register_UnityEngine_RectOffset_get_horizontal();
		Register_UnityEngine_RectOffset_get_horizontal();

		//System.Int32 UnityEngine.RectOffset::get_left()
		void Register_UnityEngine_RectOffset_get_left();
		Register_UnityEngine_RectOffset_get_left();

		//System.Int32 UnityEngine.RectOffset::get_right()
		void Register_UnityEngine_RectOffset_get_right();
		Register_UnityEngine_RectOffset_get_right();

		//System.Int32 UnityEngine.RectOffset::get_top()
		void Register_UnityEngine_RectOffset_get_top();
		Register_UnityEngine_RectOffset_get_top();

		//System.Int32 UnityEngine.RectOffset::get_vertical()
		void Register_UnityEngine_RectOffset_get_vertical();
		Register_UnityEngine_RectOffset_get_vertical();

		//System.IntPtr UnityEngine.RectOffset::InternalCreate()
		void Register_UnityEngine_RectOffset_InternalCreate();
		Register_UnityEngine_RectOffset_InternalCreate();

		//System.Void UnityEngine.RectOffset::InternalDestroy(System.IntPtr)
		void Register_UnityEngine_RectOffset_InternalDestroy();
		Register_UnityEngine_RectOffset_InternalDestroy();

		//System.Void UnityEngine.RectOffset::Remove_Injected(UnityEngine.Rect&,UnityEngine.Rect&)
		void Register_UnityEngine_RectOffset_Remove_Injected();
		Register_UnityEngine_RectOffset_Remove_Injected();

		//System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
		void Register_UnityEngine_RectOffset_set_bottom();
		Register_UnityEngine_RectOffset_set_bottom();

		//System.Void UnityEngine.RectOffset::set_left(System.Int32)
		void Register_UnityEngine_RectOffset_set_left();
		Register_UnityEngine_RectOffset_set_left();

		//System.Void UnityEngine.RectOffset::set_right(System.Int32)
		void Register_UnityEngine_RectOffset_set_right();
		Register_UnityEngine_RectOffset_set_right();

		//System.Void UnityEngine.RectOffset::set_top(System.Int32)
		void Register_UnityEngine_RectOffset_set_top();
		Register_UnityEngine_RectOffset_set_top();

	//End Registrations for type : UnityEngine.RectOffset

	//Start Registrations for type : UnityEngine.RectTransform

		//System.Void UnityEngine.RectTransform::get_anchorMax_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_get_anchorMax_Injected();
		Register_UnityEngine_RectTransform_get_anchorMax_Injected();

		//System.Void UnityEngine.RectTransform::get_anchorMin_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_get_anchorMin_Injected();
		Register_UnityEngine_RectTransform_get_anchorMin_Injected();

		//System.Void UnityEngine.RectTransform::get_anchoredPosition_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_get_anchoredPosition_Injected();
		Register_UnityEngine_RectTransform_get_anchoredPosition_Injected();

		//System.Void UnityEngine.RectTransform::get_pivot_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_get_pivot_Injected();
		Register_UnityEngine_RectTransform_get_pivot_Injected();

		//System.Void UnityEngine.RectTransform::get_rect_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_RectTransform_get_rect_Injected();
		Register_UnityEngine_RectTransform_get_rect_Injected();

		//System.Void UnityEngine.RectTransform::get_sizeDelta_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_get_sizeDelta_Injected();
		Register_UnityEngine_RectTransform_get_sizeDelta_Injected();

		//System.Void UnityEngine.RectTransform::set_anchorMax_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_set_anchorMax_Injected();
		Register_UnityEngine_RectTransform_set_anchorMax_Injected();

		//System.Void UnityEngine.RectTransform::set_anchorMin_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_set_anchorMin_Injected();
		Register_UnityEngine_RectTransform_set_anchorMin_Injected();

		//System.Void UnityEngine.RectTransform::set_anchoredPosition_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_set_anchoredPosition_Injected();
		Register_UnityEngine_RectTransform_set_anchoredPosition_Injected();

		//System.Void UnityEngine.RectTransform::set_pivot_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_set_pivot_Injected();
		Register_UnityEngine_RectTransform_set_pivot_Injected();

		//System.Void UnityEngine.RectTransform::set_sizeDelta_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_set_sizeDelta_Injected();
		Register_UnityEngine_RectTransform_set_sizeDelta_Injected();

	//End Registrations for type : UnityEngine.RectTransform

	//Start Registrations for type : UnityEngine.RectTransformUtility

		//System.Boolean UnityEngine.RectTransformUtility::PointInRectangle_Injected(UnityEngine.Vector2&,UnityEngine.RectTransform,UnityEngine.Camera)
		void Register_UnityEngine_RectTransformUtility_PointInRectangle_Injected();
		Register_UnityEngine_RectTransformUtility_PointInRectangle_Injected();

		//System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint_Injected(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransformUtility_PixelAdjustPoint_Injected();
		Register_UnityEngine_RectTransformUtility_PixelAdjustPoint_Injected();

		//System.Void UnityEngine.RectTransformUtility::PixelAdjustRect_Injected(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
		void Register_UnityEngine_RectTransformUtility_PixelAdjustRect_Injected();
		Register_UnityEngine_RectTransformUtility_PixelAdjustRect_Injected();

	//End Registrations for type : UnityEngine.RectTransformUtility

	//Start Registrations for type : UnityEngine.RemoteConfigSettings

		//System.Void UnityEngine.RemoteConfigSettings::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_RemoteConfigSettings_Internal_Destroy();
		Register_UnityEngine_RemoteConfigSettings_Internal_Destroy();

	//End Registrations for type : UnityEngine.RemoteConfigSettings

	//Start Registrations for type : UnityEngine.Renderer

		//System.Boolean UnityEngine.Renderer::get_isVisible()
		void Register_UnityEngine_Renderer_get_isVisible();
		Register_UnityEngine_Renderer_get_isVisible();

		//System.Int32 UnityEngine.Renderer::get_sortingLayerID()
		void Register_UnityEngine_Renderer_get_sortingLayerID();
		Register_UnityEngine_Renderer_get_sortingLayerID();

		//System.Int32 UnityEngine.Renderer::get_sortingOrder()
		void Register_UnityEngine_Renderer_get_sortingOrder();
		Register_UnityEngine_Renderer_get_sortingOrder();

		//System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
		void Register_UnityEngine_Renderer_set_enabled();
		Register_UnityEngine_Renderer_set_enabled();

		//System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingOrder();
		Register_UnityEngine_Renderer_set_sortingOrder();

		//UnityEngine.Material UnityEngine.Renderer::GetMaterial()
		void Register_UnityEngine_Renderer_GetMaterial();
		Register_UnityEngine_Renderer_GetMaterial();

	//End Registrations for type : UnityEngine.Renderer

	//Start Registrations for type : UnityEngine.Rendering.CommandBuffer

		//System.Int32 UnityEngine.Rendering.CommandBuffer::get_sizeInBytes()
		void Register_UnityEngine_Rendering_CommandBuffer_get_sizeInBytes();
		Register_UnityEngine_Rendering_CommandBuffer_get_sizeInBytes();

		//System.String UnityEngine.Rendering.CommandBuffer::get_name()
		void Register_UnityEngine_Rendering_CommandBuffer_get_name();
		Register_UnityEngine_Rendering_CommandBuffer_get_name();

		//System.Void UnityEngine.Rendering.CommandBuffer::BeginSample(System.String)
		void Register_UnityEngine_Rendering_CommandBuffer_BeginSample();
		Register_UnityEngine_Rendering_CommandBuffer_BeginSample();

		//System.Void UnityEngine.Rendering.CommandBuffer::Clear()
		void Register_UnityEngine_Rendering_CommandBuffer_Clear();
		Register_UnityEngine_Rendering_CommandBuffer_Clear();

		//System.Void UnityEngine.Rendering.CommandBuffer::ClearRandomWriteTargets()
		void Register_UnityEngine_Rendering_CommandBuffer_ClearRandomWriteTargets();
		Register_UnityEngine_Rendering_CommandBuffer_ClearRandomWriteTargets();

		//System.Void UnityEngine.Rendering.CommandBuffer::ConvertTexture_Internal_Injected(UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32)
		void Register_UnityEngine_Rendering_CommandBuffer_ConvertTexture_Internal_Injected();
		Register_UnityEngine_Rendering_CommandBuffer_ConvertTexture_Internal_Injected();

		//System.Void UnityEngine.Rendering.CommandBuffer::CopyTexture_Internal(UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Rendering_CommandBuffer_CopyTexture_Internal();
		Register_UnityEngine_Rendering_CommandBuffer_CopyTexture_Internal();

		//System.Void UnityEngine.Rendering.CommandBuffer::DisableScissorRect()
		void Register_UnityEngine_Rendering_CommandBuffer_DisableScissorRect();
		Register_UnityEngine_Rendering_CommandBuffer_DisableScissorRect();

		//System.Void UnityEngine.Rendering.CommandBuffer::DisableShaderKeyword(System.String)
		void Register_UnityEngine_Rendering_CommandBuffer_DisableShaderKeyword();
		Register_UnityEngine_Rendering_CommandBuffer_DisableShaderKeyword();

		//System.Void UnityEngine.Rendering.CommandBuffer::EnableShaderKeyword(System.String)
		void Register_UnityEngine_Rendering_CommandBuffer_EnableShaderKeyword();
		Register_UnityEngine_Rendering_CommandBuffer_EnableShaderKeyword();

		//System.Void UnityEngine.Rendering.CommandBuffer::EndSample(System.String)
		void Register_UnityEngine_Rendering_CommandBuffer_EndSample();
		Register_UnityEngine_Rendering_CommandBuffer_EndSample();

		//System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean,UnityEngine.RenderTextureMemoryless,System.Boolean)
		void Register_UnityEngine_Rendering_CommandBuffer_GetTemporaryRT();
		Register_UnityEngine_Rendering_CommandBuffer_GetTemporaryRT();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Identifier(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Blit_Identifier();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Blit_Identifier();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Blit_Texture();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Blit_Texture();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_ClearRenderTarget(UnityEngine.Rendering.CommandBuffer,System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_ClearRenderTarget();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_ClearRenderTarget();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_CreateGPUFence_Internal(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.SynchronisationStage,System.IntPtr&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_CreateGPUFence_Internal();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_CreateGPUFence_Internal();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_EnableScissorRect(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rect&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_EnableScissorRect();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_EnableScissorRect();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_GetTemporaryRT(UnityEngine.Rendering.CommandBuffer,System.Int32,UnityEngine.RenderTextureDescriptor&,UnityEngine.FilterMode)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_GetTemporaryRT();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_GetTemporaryRT();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Internal_DrawMesh(UnityEngine.Rendering.CommandBuffer,UnityEngine.Mesh,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Internal_DrawMesh();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Internal_DrawMesh();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Internal_DrawProcedural(UnityEngine.Rendering.CommandBuffer,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,UnityEngine.MeshTopology,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Internal_DrawProcedural();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_Internal_DrawProcedural();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_SetComputeVectorParam(UnityEngine.Rendering.CommandBuffer,UnityEngine.ComputeShader,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetComputeVectorParam();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetComputeVectorParam();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_SetGlobalMatrix(UnityEngine.Rendering.CommandBuffer,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetGlobalMatrix();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetGlobalMatrix();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_SetGlobalVector(UnityEngine.Rendering.CommandBuffer,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetGlobalVector();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetGlobalVector();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_SetViewProjectionMatrices(UnityEngine.Rendering.CommandBuffer,UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetViewProjectionMatrices();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetViewProjectionMatrices();

		//System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_SetViewport(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rect&)
		void Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetViewport();
		Register_UnityEngine_Rendering_CommandBuffer_INTERNAL_CALL_SetViewport();

		//System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
		void Register_UnityEngine_Rendering_CommandBuffer_InitBuffer();
		Register_UnityEngine_Rendering_CommandBuffer_InitBuffer();

		//System.Void UnityEngine.Rendering.CommandBuffer::Internal_DispatchCompute(UnityEngine.ComputeShader,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Rendering_CommandBuffer_Internal_DispatchCompute();
		Register_UnityEngine_Rendering_CommandBuffer_Internal_DispatchCompute();

		//System.Void UnityEngine.Rendering.CommandBuffer::Internal_SetComputeFloats(UnityEngine.ComputeShader,System.Int32,System.Single[])
		void Register_UnityEngine_Rendering_CommandBuffer_Internal_SetComputeFloats();
		Register_UnityEngine_Rendering_CommandBuffer_Internal_SetComputeFloats();

		//System.Void UnityEngine.Rendering.CommandBuffer::Internal_SetComputeInts(UnityEngine.ComputeShader,System.Int32,System.Int32[])
		void Register_UnityEngine_Rendering_CommandBuffer_Internal_SetComputeInts();
		Register_UnityEngine_Rendering_CommandBuffer_Internal_SetComputeInts();

		//System.Void UnityEngine.Rendering.CommandBuffer::Internal_SetComputeTextureParam(UnityEngine.ComputeShader,System.Int32,System.Int32,UnityEngine.Rendering.RenderTargetIdentifier&)
		void Register_UnityEngine_Rendering_CommandBuffer_Internal_SetComputeTextureParam();
		Register_UnityEngine_Rendering_CommandBuffer_Internal_SetComputeTextureParam();

		//System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
		void Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer();
		Register_UnityEngine_Rendering_CommandBuffer_ReleaseBuffer();

		//System.Void UnityEngine.Rendering.CommandBuffer::ReleaseTemporaryRT(System.Int32)
		void Register_UnityEngine_Rendering_CommandBuffer_ReleaseTemporaryRT();
		Register_UnityEngine_Rendering_CommandBuffer_ReleaseTemporaryRT();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetComputeBufferParam(UnityEngine.ComputeShader,System.Int32,System.Int32,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Rendering_CommandBuffer_SetComputeBufferParam();
		Register_UnityEngine_Rendering_CommandBuffer_SetComputeBufferParam();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetComputeFloatParam(UnityEngine.ComputeShader,System.Int32,System.Single)
		void Register_UnityEngine_Rendering_CommandBuffer_SetComputeFloatParam();
		Register_UnityEngine_Rendering_CommandBuffer_SetComputeFloatParam();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetComputeIntParam(UnityEngine.ComputeShader,System.Int32,System.Int32)
		void Register_UnityEngine_Rendering_CommandBuffer_SetComputeIntParam();
		Register_UnityEngine_Rendering_CommandBuffer_SetComputeIntParam();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalBuffer(System.Int32,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Rendering_CommandBuffer_SetGlobalBuffer();
		Register_UnityEngine_Rendering_CommandBuffer_SetGlobalBuffer();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalFloat(System.Int32,System.Single)
		void Register_UnityEngine_Rendering_CommandBuffer_SetGlobalFloat();
		Register_UnityEngine_Rendering_CommandBuffer_SetGlobalFloat();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalFloatArray(System.Int32,System.Single[])
		void Register_UnityEngine_Rendering_CommandBuffer_SetGlobalFloatArray();
		Register_UnityEngine_Rendering_CommandBuffer_SetGlobalFloatArray();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalMatrixArray(System.Int32,UnityEngine.Matrix4x4[])
		void Register_UnityEngine_Rendering_CommandBuffer_SetGlobalMatrixArray();
		Register_UnityEngine_Rendering_CommandBuffer_SetGlobalMatrixArray();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalTexture_Impl(System.Int32,UnityEngine.Rendering.RenderTargetIdentifier&)
		void Register_UnityEngine_Rendering_CommandBuffer_SetGlobalTexture_Impl();
		Register_UnityEngine_Rendering_CommandBuffer_SetGlobalTexture_Impl();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalVectorArray(System.Int32,UnityEngine.Vector4[])
		void Register_UnityEngine_Rendering_CommandBuffer_SetGlobalVectorArray();
		Register_UnityEngine_Rendering_CommandBuffer_SetGlobalVectorArray();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTargetColorDepth_Internal_Injected(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction,UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
		void Register_UnityEngine_Rendering_CommandBuffer_SetRenderTargetColorDepth_Internal_Injected();
		Register_UnityEngine_Rendering_CommandBuffer_SetRenderTargetColorDepth_Internal_Injected();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTargetMulti_Internal_Injected(UnityEngine.Rendering.RenderTargetIdentifier[],UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderBufferLoadAction[],UnityEngine.Rendering.RenderBufferStoreAction[],UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
		void Register_UnityEngine_Rendering_CommandBuffer_SetRenderTargetMulti_Internal_Injected();
		Register_UnityEngine_Rendering_CommandBuffer_SetRenderTargetMulti_Internal_Injected();

		//System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTargetSingle_Internal_Injected(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction,UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
		void Register_UnityEngine_Rendering_CommandBuffer_SetRenderTargetSingle_Internal_Injected();
		Register_UnityEngine_Rendering_CommandBuffer_SetRenderTargetSingle_Internal_Injected();

		//System.Void UnityEngine.Rendering.CommandBuffer::set_name(System.String)
		void Register_UnityEngine_Rendering_CommandBuffer_set_name();
		Register_UnityEngine_Rendering_CommandBuffer_set_name();

	//End Registrations for type : UnityEngine.Rendering.CommandBuffer

	//Start Registrations for type : UnityEngine.Rendering.GPUFence

		//System.Int32 UnityEngine.Rendering.GPUFence::GetVersionNumber(System.IntPtr)
		void Register_UnityEngine_Rendering_GPUFence_GetVersionNumber();
		Register_UnityEngine_Rendering_GPUFence_GetVersionNumber();

	//End Registrations for type : UnityEngine.Rendering.GPUFence

	//Start Registrations for type : UnityEngine.Rendering.GraphicsSettings

		//System.Boolean UnityEngine.Rendering.GraphicsSettings::HasShaderDefine(UnityEngine.Rendering.GraphicsTier,UnityEngine.Rendering.BuiltinShaderDefine)
		void Register_UnityEngine_Rendering_GraphicsSettings_HasShaderDefine();
		Register_UnityEngine_Rendering_GraphicsSettings_HasShaderDefine();

		//System.Boolean UnityEngine.Rendering.GraphicsSettings::get_useScriptableRenderPipelineBatching()
		void Register_UnityEngine_Rendering_GraphicsSettings_get_useScriptableRenderPipelineBatching();
		Register_UnityEngine_Rendering_GraphicsSettings_get_useScriptableRenderPipelineBatching();

		//System.Void UnityEngine.Rendering.GraphicsSettings::set_INTERNAL_renderPipelineAsset(UnityEngine.ScriptableObject)
		void Register_UnityEngine_Rendering_GraphicsSettings_set_INTERNAL_renderPipelineAsset();
		Register_UnityEngine_Rendering_GraphicsSettings_set_INTERNAL_renderPipelineAsset();

		//System.Void UnityEngine.Rendering.GraphicsSettings::set_lightsUseLinearIntensity(System.Boolean)
		void Register_UnityEngine_Rendering_GraphicsSettings_set_lightsUseLinearIntensity();
		Register_UnityEngine_Rendering_GraphicsSettings_set_lightsUseLinearIntensity();

		//System.Void UnityEngine.Rendering.GraphicsSettings::set_useScriptableRenderPipelineBatching(System.Boolean)
		void Register_UnityEngine_Rendering_GraphicsSettings_set_useScriptableRenderPipelineBatching();
		Register_UnityEngine_Rendering_GraphicsSettings_set_useScriptableRenderPipelineBatching();

		//UnityEngine.ScriptableObject UnityEngine.Rendering.GraphicsSettings::get_INTERNAL_renderPipelineAsset()
		void Register_UnityEngine_Rendering_GraphicsSettings_get_INTERNAL_renderPipelineAsset();
		Register_UnityEngine_Rendering_GraphicsSettings_get_INTERNAL_renderPipelineAsset();

	//End Registrations for type : UnityEngine.Rendering.GraphicsSettings

	//Start Registrations for type : UnityEngine.RenderSettings

		//System.Boolean UnityEngine.RenderSettings::get_fog()
		void Register_UnityEngine_RenderSettings_get_fog();
		Register_UnityEngine_RenderSettings_get_fog();

		//System.Single UnityEngine.RenderSettings::get_fogDensity()
		void Register_UnityEngine_RenderSettings_get_fogDensity();
		Register_UnityEngine_RenderSettings_get_fogDensity();

		//System.Single UnityEngine.RenderSettings::get_fogEndDistance()
		void Register_UnityEngine_RenderSettings_get_fogEndDistance();
		Register_UnityEngine_RenderSettings_get_fogEndDistance();

		//System.Single UnityEngine.RenderSettings::get_fogStartDistance()
		void Register_UnityEngine_RenderSettings_get_fogStartDistance();
		Register_UnityEngine_RenderSettings_get_fogStartDistance();

		//System.Single UnityEngine.RenderSettings::get_reflectionIntensity()
		void Register_UnityEngine_RenderSettings_get_reflectionIntensity();
		Register_UnityEngine_RenderSettings_get_reflectionIntensity();

		//System.Void UnityEngine.RenderSettings::get_ambientProbe_Injected(UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_RenderSettings_get_ambientProbe_Injected();
		Register_UnityEngine_RenderSettings_get_ambientProbe_Injected();

		//System.Void UnityEngine.RenderSettings::get_fogColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_get_fogColor_Injected();
		Register_UnityEngine_RenderSettings_get_fogColor_Injected();

		//System.Void UnityEngine.RenderSettings::get_subtractiveShadowColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_get_subtractiveShadowColor_Injected();
		Register_UnityEngine_RenderSettings_get_subtractiveShadowColor_Injected();

	//End Registrations for type : UnityEngine.RenderSettings

	//Start Registrations for type : UnityEngine.RenderTexture

		//System.Boolean UnityEngine.RenderTexture::Create()
		void Register_UnityEngine_RenderTexture_Create();
		Register_UnityEngine_RenderTexture_Create();

		//System.Boolean UnityEngine.RenderTexture::IsCreated()
		void Register_UnityEngine_RenderTexture_IsCreated();
		Register_UnityEngine_RenderTexture_IsCreated();

		//System.Boolean UnityEngine.RenderTexture::get_autoGenerateMips()
		void Register_UnityEngine_RenderTexture_get_autoGenerateMips();
		Register_UnityEngine_RenderTexture_get_autoGenerateMips();

		//System.Boolean UnityEngine.RenderTexture::get_enableRandomWrite()
		void Register_UnityEngine_RenderTexture_get_enableRandomWrite();
		Register_UnityEngine_RenderTexture_get_enableRandomWrite();

		//System.Boolean UnityEngine.RenderTexture::get_sRGB()
		void Register_UnityEngine_RenderTexture_get_sRGB();
		Register_UnityEngine_RenderTexture_get_sRGB();

		//System.Boolean UnityEngine.RenderTexture::get_useDynamicScale()
		void Register_UnityEngine_RenderTexture_get_useDynamicScale();
		Register_UnityEngine_RenderTexture_get_useDynamicScale();

		//System.Boolean UnityEngine.RenderTexture::get_useMipMap()
		void Register_UnityEngine_RenderTexture_get_useMipMap();
		Register_UnityEngine_RenderTexture_get_useMipMap();

		//System.Int32 UnityEngine.RenderTexture::get_antiAliasing()
		void Register_UnityEngine_RenderTexture_get_antiAliasing();
		Register_UnityEngine_RenderTexture_get_antiAliasing();

		//System.Int32 UnityEngine.RenderTexture::get_depth()
		void Register_UnityEngine_RenderTexture_get_depth();
		Register_UnityEngine_RenderTexture_get_depth();

		//System.Int32 UnityEngine.RenderTexture::get_height()
		void Register_UnityEngine_RenderTexture_get_height();
		Register_UnityEngine_RenderTexture_get_height();

		//System.Int32 UnityEngine.RenderTexture::get_volumeDepth()
		void Register_UnityEngine_RenderTexture_get_volumeDepth();
		Register_UnityEngine_RenderTexture_get_volumeDepth();

		//System.Int32 UnityEngine.RenderTexture::get_width()
		void Register_UnityEngine_RenderTexture_get_width();
		Register_UnityEngine_RenderTexture_get_width();

		//System.Void UnityEngine.RenderTexture::DiscardContents(System.Boolean,System.Boolean)
		void Register_UnityEngine_RenderTexture_DiscardContents();
		Register_UnityEngine_RenderTexture_DiscardContents();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_GetDescriptor(UnityEngine.RenderTexture,UnityEngine.RenderTextureDescriptor&)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_GetDescriptor();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_GetDescriptor();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_SetRenderTextureDescriptor(UnityEngine.RenderTexture,UnityEngine.RenderTextureDescriptor&)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_SetRenderTextureDescriptor();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_SetRenderTextureDescriptor();

		//System.Void UnityEngine.RenderTexture::Internal_Create(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_Create();
		Register_UnityEngine_RenderTexture_Internal_Create();

		//System.Void UnityEngine.RenderTexture::Release()
		void Register_UnityEngine_RenderTexture_Release();
		Register_UnityEngine_RenderTexture_Release();

		//System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_ReleaseTemporary();
		Register_UnityEngine_RenderTexture_ReleaseTemporary();

		//System.Void UnityEngine.RenderTexture::SetActive(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_SetActive();
		Register_UnityEngine_RenderTexture_SetActive();

		//System.Void UnityEngine.RenderTexture::SetSRGBReadWrite(System.Boolean)
		void Register_UnityEngine_RenderTexture_SetSRGBReadWrite();
		Register_UnityEngine_RenderTexture_SetSRGBReadWrite();

		//System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
		void Register_UnityEngine_RenderTexture_set_antiAliasing();
		Register_UnityEngine_RenderTexture_set_antiAliasing();

		//System.Void UnityEngine.RenderTexture::set_autoGenerateMips(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_autoGenerateMips();
		Register_UnityEngine_RenderTexture_set_autoGenerateMips();

		//System.Void UnityEngine.RenderTexture::set_bindTextureMS(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_bindTextureMS();
		Register_UnityEngine_RenderTexture_set_bindTextureMS();

		//System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
		void Register_UnityEngine_RenderTexture_set_depth();
		Register_UnityEngine_RenderTexture_set_depth();

		//System.Void UnityEngine.RenderTexture::set_dimension(UnityEngine.Rendering.TextureDimension)
		void Register_UnityEngine_RenderTexture_set_dimension();
		Register_UnityEngine_RenderTexture_set_dimension();

		//System.Void UnityEngine.RenderTexture::set_enableRandomWrite(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_enableRandomWrite();
		Register_UnityEngine_RenderTexture_set_enableRandomWrite();

		//System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_RenderTexture_set_format();
		Register_UnityEngine_RenderTexture_set_format();

		//System.Void UnityEngine.RenderTexture::set_height(System.Int32)
		void Register_UnityEngine_RenderTexture_set_height();
		Register_UnityEngine_RenderTexture_set_height();

		//System.Void UnityEngine.RenderTexture::set_memorylessMode(UnityEngine.RenderTextureMemoryless)
		void Register_UnityEngine_RenderTexture_set_memorylessMode();
		Register_UnityEngine_RenderTexture_set_memorylessMode();

		//System.Void UnityEngine.RenderTexture::set_useDynamicScale(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_useDynamicScale();
		Register_UnityEngine_RenderTexture_set_useDynamicScale();

		//System.Void UnityEngine.RenderTexture::set_useMipMap(System.Boolean)
		void Register_UnityEngine_RenderTexture_set_useMipMap();
		Register_UnityEngine_RenderTexture_set_useMipMap();

		//System.Void UnityEngine.RenderTexture::set_volumeDepth(System.Int32)
		void Register_UnityEngine_RenderTexture_set_volumeDepth();
		Register_UnityEngine_RenderTexture_set_volumeDepth();

		//System.Void UnityEngine.RenderTexture::set_vrUsage(UnityEngine.VRTextureUsage)
		void Register_UnityEngine_RenderTexture_set_vrUsage();
		Register_UnityEngine_RenderTexture_set_vrUsage();

		//System.Void UnityEngine.RenderTexture::set_width(System.Int32)
		void Register_UnityEngine_RenderTexture_set_width();
		Register_UnityEngine_RenderTexture_set_width();

		//UnityEngine.RenderTexture UnityEngine.RenderTexture::INTERNAL_CALL_GetTemporary_Internal(UnityEngine.RenderTextureDescriptor&)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_GetTemporary_Internal();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_GetTemporary_Internal();

		//UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
		void Register_UnityEngine_RenderTexture_get_format();
		Register_UnityEngine_RenderTexture_get_format();

		//UnityEngine.RenderTextureMemoryless UnityEngine.RenderTexture::get_memorylessMode()
		void Register_UnityEngine_RenderTexture_get_memorylessMode();
		Register_UnityEngine_RenderTexture_get_memorylessMode();

		//UnityEngine.Rendering.TextureDimension UnityEngine.RenderTexture::get_dimension()
		void Register_UnityEngine_RenderTexture_get_dimension();
		Register_UnityEngine_RenderTexture_get_dimension();

		//UnityEngine.VRTextureUsage UnityEngine.RenderTexture::get_vrUsage()
		void Register_UnityEngine_RenderTexture_get_vrUsage();
		Register_UnityEngine_RenderTexture_get_vrUsage();

	//End Registrations for type : UnityEngine.RenderTexture

	//Start Registrations for type : UnityEngine.Resources

		//UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
		void Register_UnityEngine_Resources_GetBuiltinResource();
		Register_UnityEngine_Resources_GetBuiltinResource();

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

		//UnityEngine.Object[] UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)
		void Register_UnityEngine_Resources_FindObjectsOfTypeAll();
		Register_UnityEngine_Resources_FindObjectsOfTypeAll();

		//UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
		void Register_UnityEngine_Resources_LoadAll();
		Register_UnityEngine_Resources_LoadAll();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.Rigidbody

		//System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_MovePosition_Injected();
		Register_UnityEngine_Rigidbody_MovePosition_Injected();

		//System.Void UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_MoveRotation_Injected();
		Register_UnityEngine_Rigidbody_MoveRotation_Injected();

		//System.Void UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_get_angularVelocity_Injected();
		Register_UnityEngine_Rigidbody_get_angularVelocity_Injected();

		//System.Void UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_get_position_Injected();
		Register_UnityEngine_Rigidbody_get_position_Injected();

		//System.Void UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_get_rotation_Injected();
		Register_UnityEngine_Rigidbody_get_rotation_Injected();

		//System.Void UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_get_velocity_Injected();
		Register_UnityEngine_Rigidbody_get_velocity_Injected();

		//System.Void UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_set_angularVelocity_Injected();
		Register_UnityEngine_Rigidbody_set_angularVelocity_Injected();

		//System.Void UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_set_position_Injected();
		Register_UnityEngine_Rigidbody_set_position_Injected();

		//System.Void UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_set_rotation_Injected();
		Register_UnityEngine_Rigidbody_set_rotation_Injected();

		//System.Void UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_set_velocity_Injected();
		Register_UnityEngine_Rigidbody_set_velocity_Injected();

	//End Registrations for type : UnityEngine.Rigidbody

	//Start Registrations for type : UnityEngine.Rigidbody2D

		//System.Int32 UnityEngine.Rigidbody2D::OverlapCollider_Injected(UnityEngine.ContactFilter2D&,UnityEngine.Collider2D[])
		void Register_UnityEngine_Rigidbody2D_OverlapCollider_Injected();
		Register_UnityEngine_Rigidbody2D_OverlapCollider_Injected();

		//System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
		void Register_UnityEngine_Rigidbody2D_get_angularVelocity();
		Register_UnityEngine_Rigidbody2D_get_angularVelocity();

		//System.Single UnityEngine.Rigidbody2D::get_gravityScale()
		void Register_UnityEngine_Rigidbody2D_get_gravityScale();
		Register_UnityEngine_Rigidbody2D_get_gravityScale();

		//System.Single UnityEngine.Rigidbody2D::get_mass()
		void Register_UnityEngine_Rigidbody2D_get_mass();
		Register_UnityEngine_Rigidbody2D_get_mass();

		//System.Single UnityEngine.Rigidbody2D::get_rotation()
		void Register_UnityEngine_Rigidbody2D_get_rotation();
		Register_UnityEngine_Rigidbody2D_get_rotation();

		//System.Void UnityEngine.Rigidbody2D::AddForceAtPosition_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_AddForceAtPosition_Injected();
		Register_UnityEngine_Rigidbody2D_AddForceAtPosition_Injected();

		//System.Void UnityEngine.Rigidbody2D::MovePosition_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_MovePosition_Injected();
		Register_UnityEngine_Rigidbody2D_MovePosition_Injected();

		//System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
		void Register_UnityEngine_Rigidbody2D_MoveRotation();
		Register_UnityEngine_Rigidbody2D_MoveRotation();

		//System.Void UnityEngine.Rigidbody2D::get_position_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_get_position_Injected();
		Register_UnityEngine_Rigidbody2D_get_position_Injected();

		//System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_get_velocity_Injected();
		Register_UnityEngine_Rigidbody2D_get_velocity_Injected();

		//System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_angularVelocity();
		Register_UnityEngine_Rigidbody2D_set_angularVelocity();

		//System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
		void Register_UnityEngine_Rigidbody2D_set_constraints();
		Register_UnityEngine_Rigidbody2D_set_constraints();

		//System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_mass();
		Register_UnityEngine_Rigidbody2D_set_mass();

		//System.Void UnityEngine.Rigidbody2D::set_position_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_set_position_Injected();
		Register_UnityEngine_Rigidbody2D_set_position_Injected();

		//System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_rotation();
		Register_UnityEngine_Rigidbody2D_set_rotation();

		//System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_set_velocity_Injected();
		Register_UnityEngine_Rigidbody2D_set_velocity_Injected();

	//End Registrations for type : UnityEngine.Rigidbody2D

	//Start Registrations for type : UnityEngine.SceneManagement.Scene

		//System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal();
		Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal();

		//System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetNameInternal();
		Register_UnityEngine_SceneManagement_Scene_GetNameInternal();

	//End Registrations for type : UnityEngine.SceneManagement.Scene

	//Start Registrations for type : UnityEngine.SceneManagement.SceneManager

		//System.Void UnityEngine.SceneManagement.SceneManager::GetActiveScene_Injected(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_GetActiveScene_Injected();
		Register_UnityEngine_SceneManagement_SceneManager_GetActiveScene_Injected();

		//System.Void UnityEngine.SceneManagement.SceneManager::GetSceneAt_Injected(System.Int32,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_GetSceneAt_Injected();
		Register_UnityEngine_SceneManagement_SceneManager_GetSceneAt_Injected();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
		void Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();

	//End Registrations for type : UnityEngine.SceneManagement.SceneManager

	//Start Registrations for type : UnityEngine.Screen

		//System.Int32 UnityEngine.Screen::get_height()
		void Register_UnityEngine_Screen_get_height();
		Register_UnityEngine_Screen_get_height();

		//System.Int32 UnityEngine.Screen::get_width()
		void Register_UnityEngine_Screen_get_width();
		Register_UnityEngine_Screen_get_width();

		//System.Single UnityEngine.Screen::get_dpi()
		void Register_UnityEngine_Screen_get_dpi();
		Register_UnityEngine_Screen_get_dpi();

		//UnityEngine.ScreenOrientation UnityEngine.Screen::GetScreenOrientation()
		void Register_UnityEngine_Screen_GetScreenOrientation();
		Register_UnityEngine_Screen_GetScreenOrientation();

	//End Registrations for type : UnityEngine.Screen

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateScriptableObjectInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateScriptableObjectInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateScriptableObjectInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.Shader

		//System.Boolean UnityEngine.Shader::get_isSupported()
		void Register_UnityEngine_Shader_get_isSupported();
		Register_UnityEngine_Shader_get_isSupported();

		//System.Int32 UnityEngine.Shader::PropertyToID(System.String)
		void Register_UnityEngine_Shader_PropertyToID();
		Register_UnityEngine_Shader_PropertyToID();

		//System.Void UnityEngine.Shader::SetGlobalFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_Shader_SetGlobalFloatImpl();
		Register_UnityEngine_Shader_SetGlobalFloatImpl();

		//System.Void UnityEngine.Shader::SetGlobalVectorImpl_Injected(System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Shader_SetGlobalVectorImpl_Injected();
		Register_UnityEngine_Shader_SetGlobalVectorImpl_Injected();

		//System.Void UnityEngine.Shader::set_globalRenderPipeline(System.String)
		void Register_UnityEngine_Shader_set_globalRenderPipeline();
		Register_UnityEngine_Shader_set_globalRenderPipeline();

		//UnityEngine.Shader UnityEngine.Shader::Find(System.String)
		void Register_UnityEngine_Shader_Find();
		Register_UnityEngine_Shader_Find();

	//End Registrations for type : UnityEngine.Shader

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetAuthenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_GetAuthenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_GetAuthenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetIsUnderage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_GetIsUnderage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_GetIsUnderage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::InternalLoadAchievementDescriptions(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalLoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalLoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::InternalLoadAchievements(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalLoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalLoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::InternalLoadScores(System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalLoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalLoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::InternalReportProgress(System.String,System.Double,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::InternalReportScore(System.Int64,System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_InternalReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[],System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadFriends(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetUserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_GetUserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_GetUserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::GcLeaderboard_Loading(System.IntPtr)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_GcLeaderboard_Loading();

		//System.IntPtr UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::GcLeaderboard_LoadScores(System.Object,System.String,System.Int32,System.Int32,System.String[],System.Int32,System.Int32,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_GcLeaderboard_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_GcLeaderboard_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::GcLeaderboard_Dispose(System.IntPtr)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_GcLeaderboard_Dispose();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.SortingLayer

		//System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
		void Register_UnityEngine_SortingLayer_GetLayerValueFromID();
		Register_UnityEngine_SortingLayer_GetLayerValueFromID();

	//End Registrations for type : UnityEngine.SortingLayer

	//Start Registrations for type : UnityEngine.SphereCollider

		//System.Single UnityEngine.SphereCollider::get_radius()
		void Register_UnityEngine_SphereCollider_get_radius();
		Register_UnityEngine_SphereCollider_get_radius();

		//System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_SphereCollider_get_center_Injected();
		Register_UnityEngine_SphereCollider_get_center_Injected();

	//End Registrations for type : UnityEngine.SphereCollider

	//Start Registrations for type : UnityEngine.Sprite

		//System.Int32 UnityEngine.Sprite::GetPacked()
		void Register_UnityEngine_Sprite_GetPacked();
		Register_UnityEngine_Sprite_GetPacked();

		//System.Int32 UnityEngine.Sprite::GetPackingMode()
		void Register_UnityEngine_Sprite_GetPackingMode();
		Register_UnityEngine_Sprite_GetPackingMode();

		//System.Single UnityEngine.Sprite::get_pixelsPerUnit()
		void Register_UnityEngine_Sprite_get_pixelsPerUnit();
		Register_UnityEngine_Sprite_get_pixelsPerUnit();

		//System.Void UnityEngine.Sprite::GetInnerUVs_Injected(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_GetInnerUVs_Injected();
		Register_UnityEngine_Sprite_GetInnerUVs_Injected();

		//System.Void UnityEngine.Sprite::GetOuterUVs_Injected(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_GetOuterUVs_Injected();
		Register_UnityEngine_Sprite_GetOuterUVs_Injected();

		//System.Void UnityEngine.Sprite::GetPadding_Injected(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_GetPadding_Injected();
		Register_UnityEngine_Sprite_GetPadding_Injected();

		//System.Void UnityEngine.Sprite::GetTextureRect_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_GetTextureRect_Injected();
		Register_UnityEngine_Sprite_GetTextureRect_Injected();

		//System.Void UnityEngine.Sprite::get_border_Injected(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_get_border_Injected();
		Register_UnityEngine_Sprite_get_border_Injected();

		//System.Void UnityEngine.Sprite::get_rect_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_get_rect_Injected();
		Register_UnityEngine_Sprite_get_rect_Injected();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
		void Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();
		Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
		void Register_UnityEngine_Sprite_get_texture();
		Register_UnityEngine_Sprite_get_texture();

	//End Registrations for type : UnityEngine.Sprite

	//Start Registrations for type : UnityEngine.SpriteRenderer

		//System.Void UnityEngine.SpriteRenderer::set_color_Injected(UnityEngine.Color&)
		void Register_UnityEngine_SpriteRenderer_set_color_Injected();
		Register_UnityEngine_SpriteRenderer_set_color_Injected();

		//System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
		void Register_UnityEngine_SpriteRenderer_set_sprite();
		Register_UnityEngine_SpriteRenderer_set_sprite();

	//End Registrations for type : UnityEngine.SpriteRenderer

	//Start Registrations for type : UnityEngine.SystemInfo

		//System.Boolean UnityEngine.SystemInfo::GetGraphicsUVStartsAtTop()
		void Register_UnityEngine_SystemInfo_GetGraphicsUVStartsAtTop();
		Register_UnityEngine_SystemInfo_GetGraphicsUVStartsAtTop();

		//System.Boolean UnityEngine.SystemInfo::HasRenderTextureNative(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_SystemInfo_HasRenderTextureNative();
		Register_UnityEngine_SystemInfo_HasRenderTextureNative();

		//System.Boolean UnityEngine.SystemInfo::IsFormatSupported(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
		void Register_UnityEngine_SystemInfo_IsFormatSupported();
		Register_UnityEngine_SystemInfo_IsFormatSupported();

		//System.Boolean UnityEngine.SystemInfo::Supports3DRenderTextures()
		void Register_UnityEngine_SystemInfo_Supports3DRenderTextures();
		Register_UnityEngine_SystemInfo_Supports3DRenderTextures();

		//System.Boolean UnityEngine.SystemInfo::SupportsComputeShaders()
		void Register_UnityEngine_SystemInfo_SupportsComputeShaders();
		Register_UnityEngine_SystemInfo_SupportsComputeShaders();

		//System.Boolean UnityEngine.SystemInfo::SupportsGPUFence()
		void Register_UnityEngine_SystemInfo_SupportsGPUFence();
		Register_UnityEngine_SystemInfo_SupportsGPUFence();

		//System.Boolean UnityEngine.SystemInfo::SupportsMotionVectors()
		void Register_UnityEngine_SystemInfo_SupportsMotionVectors();
		Register_UnityEngine_SystemInfo_SupportsMotionVectors();

		//System.Boolean UnityEngine.SystemInfo::SupportsTextureFormatNative(UnityEngine.TextureFormat)
		void Register_UnityEngine_SystemInfo_SupportsTextureFormatNative();
		Register_UnityEngine_SystemInfo_SupportsTextureFormatNative();

		//System.Boolean UnityEngine.SystemInfo::UsesReversedZBuffer()
		void Register_UnityEngine_SystemInfo_UsesReversedZBuffer();
		Register_UnityEngine_SystemInfo_UsesReversedZBuffer();

		//System.Int32 UnityEngine.SystemInfo::GetGraphicsShaderLevel()
		void Register_UnityEngine_SystemInfo_GetGraphicsShaderLevel();
		Register_UnityEngine_SystemInfo_GetGraphicsShaderLevel();

		//System.Int32 UnityEngine.SystemInfo::SupportedRenderTargetCount()
		void Register_UnityEngine_SystemInfo_SupportedRenderTargetCount();
		Register_UnityEngine_SystemInfo_SupportedRenderTargetCount();

		//System.String UnityEngine.SystemInfo::GetDeviceModel()
		void Register_UnityEngine_SystemInfo_GetDeviceModel();
		Register_UnityEngine_SystemInfo_GetDeviceModel();

		//System.String UnityEngine.SystemInfo::GetDeviceName()
		void Register_UnityEngine_SystemInfo_GetDeviceName();
		Register_UnityEngine_SystemInfo_GetDeviceName();

		//System.String UnityEngine.SystemInfo::GetDeviceUniqueIdentifier()
		void Register_UnityEngine_SystemInfo_GetDeviceUniqueIdentifier();
		Register_UnityEngine_SystemInfo_GetDeviceUniqueIdentifier();

		//System.String UnityEngine.SystemInfo::GetOperatingSystem()
		void Register_UnityEngine_SystemInfo_GetOperatingSystem();
		Register_UnityEngine_SystemInfo_GetOperatingSystem();

		//UnityEngine.DeviceType UnityEngine.SystemInfo::GetDeviceType()
		void Register_UnityEngine_SystemInfo_GetDeviceType();
		Register_UnityEngine_SystemInfo_GetDeviceType();

		//UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::GetOperatingSystemFamily()
		void Register_UnityEngine_SystemInfo_GetOperatingSystemFamily();
		Register_UnityEngine_SystemInfo_GetOperatingSystemFamily();

		//UnityEngine.Rendering.CopyTextureSupport UnityEngine.SystemInfo::GetCopyTextureSupport()
		void Register_UnityEngine_SystemInfo_GetCopyTextureSupport();
		Register_UnityEngine_SystemInfo_GetCopyTextureSupport();

		//UnityEngine.Rendering.GraphicsDeviceType UnityEngine.SystemInfo::GetGraphicsDeviceType()
		void Register_UnityEngine_SystemInfo_GetGraphicsDeviceType();
		Register_UnityEngine_SystemInfo_GetGraphicsDeviceType();

	//End Registrations for type : UnityEngine.SystemInfo

	//Start Registrations for type : UnityEngine.TextAsset

		//System.String UnityEngine.TextAsset::get_text()
		void Register_UnityEngine_TextAsset_get_text();
		Register_UnityEngine_TextAsset_get_text();

	//End Registrations for type : UnityEngine.TextAsset

	//Start Registrations for type : UnityEngine.TextGenerator

		//System.Boolean UnityEngine.TextGenerator::Populate_Internal_Injected(System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
		void Register_UnityEngine_TextGenerator_Populate_Internal_Injected();
		Register_UnityEngine_TextGenerator_Populate_Internal_Injected();

		//System.Int32 UnityEngine.TextGenerator::get_characterCount()
		void Register_UnityEngine_TextGenerator_get_characterCount();
		Register_UnityEngine_TextGenerator_get_characterCount();

		//System.Int32 UnityEngine.TextGenerator::get_lineCount()
		void Register_UnityEngine_TextGenerator_get_lineCount();
		Register_UnityEngine_TextGenerator_get_lineCount();

		//System.IntPtr UnityEngine.TextGenerator::Internal_Create()
		void Register_UnityEngine_TextGenerator_Internal_Create();
		Register_UnityEngine_TextGenerator_Internal_Create();

		//System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetCharactersInternal();
		Register_UnityEngine_TextGenerator_GetCharactersInternal();

		//System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetLinesInternal();
		Register_UnityEngine_TextGenerator_GetLinesInternal();

		//System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetVerticesInternal();
		Register_UnityEngine_TextGenerator_GetVerticesInternal();

		//System.Void UnityEngine.TextGenerator::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_TextGenerator_Internal_Destroy();
		Register_UnityEngine_TextGenerator_Internal_Destroy();

		//System.Void UnityEngine.TextGenerator::get_rectExtents_Injected(UnityEngine.Rect&)
		void Register_UnityEngine_TextGenerator_get_rectExtents_Injected();
		Register_UnityEngine_TextGenerator_get_rectExtents_Injected();

	//End Registrations for type : UnityEngine.TextGenerator

	//Start Registrations for type : UnityEngine.Texture

		//System.Int32 UnityEngine.Texture::GetDataHeight()
		void Register_UnityEngine_Texture_GetDataHeight();
		Register_UnityEngine_Texture_GetDataHeight();

		//System.Int32 UnityEngine.Texture::GetDataWidth()
		void Register_UnityEngine_Texture_GetDataWidth();
		Register_UnityEngine_Texture_GetDataWidth();

		//System.Int32 UnityEngine.Texture::get_anisoLevel()
		void Register_UnityEngine_Texture_get_anisoLevel();
		Register_UnityEngine_Texture_get_anisoLevel();

		//System.Single UnityEngine.Texture::get_mipMapBias()
		void Register_UnityEngine_Texture_get_mipMapBias();
		Register_UnityEngine_Texture_get_mipMapBias();

		//System.UInt32 UnityEngine.Texture::get_updateCount()
		void Register_UnityEngine_Texture_get_updateCount();
		Register_UnityEngine_Texture_get_updateCount();

		//System.Void UnityEngine.Texture::get_texelSize_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Texture_get_texelSize_Injected();
		Register_UnityEngine_Texture_get_texelSize_Injected();

		//System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
		void Register_UnityEngine_Texture_set_anisoLevel();
		Register_UnityEngine_Texture_set_anisoLevel();

		//System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
		void Register_UnityEngine_Texture_set_filterMode();
		Register_UnityEngine_Texture_set_filterMode();

		//System.Void UnityEngine.Texture::set_mipMapBias(System.Single)
		void Register_UnityEngine_Texture_set_mipMapBias();
		Register_UnityEngine_Texture_set_mipMapBias();

		//System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
		void Register_UnityEngine_Texture_set_wrapMode();
		Register_UnityEngine_Texture_set_wrapMode();

		//System.Void UnityEngine.Texture::set_wrapModeV(UnityEngine.TextureWrapMode)
		void Register_UnityEngine_Texture_set_wrapModeV();
		Register_UnityEngine_Texture_set_wrapModeV();

		//UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
		void Register_UnityEngine_Texture_get_filterMode();
		Register_UnityEngine_Texture_get_filterMode();

		//UnityEngine.Rendering.TextureDimension UnityEngine.Texture::GetDimension()
		void Register_UnityEngine_Texture_GetDimension();
		Register_UnityEngine_Texture_GetDimension();

		//UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
		void Register_UnityEngine_Texture_get_wrapMode();
		Register_UnityEngine_Texture_get_wrapMode();

	//End Registrations for type : UnityEngine.Texture

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Boolean UnityEngine.Texture2D::Internal_CreateImpl(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_CreateImpl();
		Register_UnityEngine_Texture2D_Internal_CreateImpl();

		//System.Boolean UnityEngine.Texture2D::IsReadable()
		void Register_UnityEngine_Texture2D_IsReadable();
		Register_UnityEngine_Texture2D_IsReadable();

		//System.Void UnityEngine.Texture2D::ApplyImpl(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture2D_ApplyImpl();
		Register_UnityEngine_Texture2D_ApplyImpl();

		//System.Void UnityEngine.Texture2D::GetPixelBilinearImpl_Injected(System.Int32,System.Single,System.Single,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_GetPixelBilinearImpl_Injected();
		Register_UnityEngine_Texture2D_GetPixelBilinearImpl_Injected();

		//System.Void UnityEngine.Texture2D::ReadPixelsImpl_Injected(UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Texture2D_ReadPixelsImpl_Injected();
		Register_UnityEngine_Texture2D_ReadPixelsImpl_Injected();

		//System.Void UnityEngine.Texture2D::SetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_SetPixelImpl_Injected();
		Register_UnityEngine_Texture2D_SetPixelImpl_Injected();

		//System.Void UnityEngine.Texture2D::SetPixelsImpl(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32,System.Int32)
		void Register_UnityEngine_Texture2D_SetPixelsImpl();
		Register_UnityEngine_Texture2D_SetPixelsImpl();

		//UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
		void Register_UnityEngine_Texture2D_GetPixels32();
		Register_UnityEngine_Texture2D_GetPixels32();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
		void Register_UnityEngine_Texture2D_get_whiteTexture();
		Register_UnityEngine_Texture2D_get_whiteTexture();

		//UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
		void Register_UnityEngine_Texture2D_get_format();
		Register_UnityEngine_Texture2D_get_format();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Texture2DArray

		//System.Boolean UnityEngine.Texture2DArray::Internal_CreateImpl(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
		void Register_UnityEngine_Texture2DArray_Internal_CreateImpl();
		Register_UnityEngine_Texture2DArray_Internal_CreateImpl();

		//UnityEngine.TextureFormat UnityEngine.Texture2DArray::get_format()
		void Register_UnityEngine_Texture2DArray_get_format();
		Register_UnityEngine_Texture2DArray_get_format();

	//End Registrations for type : UnityEngine.Texture2DArray

	//Start Registrations for type : UnityEngine.Texture3D

		//System.Boolean UnityEngine.Texture3D::Internal_CreateImpl(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
		void Register_UnityEngine_Texture3D_Internal_CreateImpl();
		Register_UnityEngine_Texture3D_Internal_CreateImpl();

		//System.Boolean UnityEngine.Texture3D::IsReadable()
		void Register_UnityEngine_Texture3D_IsReadable();
		Register_UnityEngine_Texture3D_IsReadable();

		//System.Int32 UnityEngine.Texture3D::get_depth()
		void Register_UnityEngine_Texture3D_get_depth();
		Register_UnityEngine_Texture3D_get_depth();

		//System.Void UnityEngine.Texture3D::ApplyImpl(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture3D_ApplyImpl();
		Register_UnityEngine_Texture3D_ApplyImpl();

		//System.Void UnityEngine.Texture3D::SetPixels(UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture3D_SetPixels();
		Register_UnityEngine_Texture3D_SetPixels();

	//End Registrations for type : UnityEngine.Texture3D

	//Start Registrations for type : UnityEngine.Time

		//System.Int32 UnityEngine.Time::get_renderedFrameCount()
		void Register_UnityEngine_Time_get_renderedFrameCount();
		Register_UnityEngine_Time_get_renderedFrameCount();

		//System.Single UnityEngine.Time::get_deltaTime()
		void Register_UnityEngine_Time_get_deltaTime();
		Register_UnityEngine_Time_get_deltaTime();

		//System.Single UnityEngine.Time::get_fixedDeltaTime()
		void Register_UnityEngine_Time_get_fixedDeltaTime();
		Register_UnityEngine_Time_get_fixedDeltaTime();

		//System.Single UnityEngine.Time::get_realtimeSinceStartup()
		void Register_UnityEngine_Time_get_realtimeSinceStartup();
		Register_UnityEngine_Time_get_realtimeSinceStartup();

		//System.Single UnityEngine.Time::get_time()
		void Register_UnityEngine_Time_get_time();
		Register_UnityEngine_Time_get_time();

		//System.Single UnityEngine.Time::get_timeSinceLevelLoad()
		void Register_UnityEngine_Time_get_timeSinceLevelLoad();
		Register_UnityEngine_Time_get_timeSinceLevelLoad();

		//System.Single UnityEngine.Time::get_unscaledDeltaTime()
		void Register_UnityEngine_Time_get_unscaledDeltaTime();
		Register_UnityEngine_Time_get_unscaledDeltaTime();

		//System.Single UnityEngine.Time::get_unscaledTime()
		void Register_UnityEngine_Time_get_unscaledTime();
		Register_UnityEngine_Time_get_unscaledTime();

	//End Registrations for type : UnityEngine.Time

	//Start Registrations for type : UnityEngine.TouchScreenKeyboard

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
		void Register_UnityEngine_TouchScreenKeyboard_get_active();
		Register_UnityEngine_TouchScreenKeyboard_get_active();

		//System.IntPtr UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
		void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();
		Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();

		//System.String UnityEngine.TouchScreenKeyboard::get_text()
		void Register_UnityEngine_TouchScreenKeyboard_get_text();
		Register_UnityEngine_TouchScreenKeyboard_get_text();

		//System.Void UnityEngine.TouchScreenKeyboard::GetSelection(System.Int32&,System.Int32&)
		void Register_UnityEngine_TouchScreenKeyboard_GetSelection();
		Register_UnityEngine_TouchScreenKeyboard_GetSelection();

		//System.Void UnityEngine.TouchScreenKeyboard::Internal_Destroy(System.IntPtr)
		void Register_UnityEngine_TouchScreenKeyboard_Internal_Destroy();
		Register_UnityEngine_TouchScreenKeyboard_Internal_Destroy();

		//System.Void UnityEngine.TouchScreenKeyboard::SetSelection(System.Int32,System.Int32)
		void Register_UnityEngine_TouchScreenKeyboard_SetSelection();
		Register_UnityEngine_TouchScreenKeyboard_SetSelection();

		//System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_active();
		Register_UnityEngine_TouchScreenKeyboard_set_active();

		//System.Void UnityEngine.TouchScreenKeyboard::set_characterLimit(System.Int32)
		void Register_UnityEngine_TouchScreenKeyboard_set_characterLimit();
		Register_UnityEngine_TouchScreenKeyboard_set_characterLimit();

		//System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
		void Register_UnityEngine_TouchScreenKeyboard_set_text();
		Register_UnityEngine_TouchScreenKeyboard_set_text();

		//UnityEngine.TouchScreenKeyboard/Status UnityEngine.TouchScreenKeyboard::get_status()
		void Register_UnityEngine_TouchScreenKeyboard_get_status();
		Register_UnityEngine_TouchScreenKeyboard_get_status();

	//End Registrations for type : UnityEngine.TouchScreenKeyboard

	//Start Registrations for type : UnityEngine.Transform

		//System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
		void Register_UnityEngine_Transform_IsChildOf();
		Register_UnityEngine_Transform_IsChildOf();

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//System.Void UnityEngine.Transform::Internal_LookAt_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_Internal_LookAt_Injected();
		Register_UnityEngine_Transform_Internal_LookAt_Injected();

		//System.Void UnityEngine.Transform::InverseTransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_InverseTransformPoint_Injected();
		Register_UnityEngine_Transform_InverseTransformPoint_Injected();

		//System.Void UnityEngine.Transform::RotateAroundInternal_Injected(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Transform_RotateAroundInternal_Injected();
		Register_UnityEngine_Transform_RotateAroundInternal_Injected();

		//System.Void UnityEngine.Transform::SetAsFirstSibling()
		void Register_UnityEngine_Transform_SetAsFirstSibling();
		Register_UnityEngine_Transform_SetAsFirstSibling();

		//System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Transform_SetParent();
		Register_UnityEngine_Transform_SetParent();

		//System.Void UnityEngine.Transform::TransformDirection_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_TransformDirection_Injected();
		Register_UnityEngine_Transform_TransformDirection_Injected();

		//System.Void UnityEngine.Transform::TransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_TransformPoint_Injected();
		Register_UnityEngine_Transform_TransformPoint_Injected();

		//System.Void UnityEngine.Transform::get_localPosition_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_get_localPosition_Injected();
		Register_UnityEngine_Transform_get_localPosition_Injected();

		//System.Void UnityEngine.Transform::get_localRotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_get_localRotation_Injected();
		Register_UnityEngine_Transform_get_localRotation_Injected();

		//System.Void UnityEngine.Transform::get_localScale_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_get_localScale_Injected();
		Register_UnityEngine_Transform_get_localScale_Injected();

		//System.Void UnityEngine.Transform::get_localToWorldMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_get_localToWorldMatrix_Injected();
		Register_UnityEngine_Transform_get_localToWorldMatrix_Injected();

		//System.Void UnityEngine.Transform::get_lossyScale_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_get_lossyScale_Injected();
		Register_UnityEngine_Transform_get_lossyScale_Injected();

		//System.Void UnityEngine.Transform::get_position_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_get_position_Injected();
		Register_UnityEngine_Transform_get_position_Injected();

		//System.Void UnityEngine.Transform::get_rotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_get_rotation_Injected();
		Register_UnityEngine_Transform_get_rotation_Injected();

		//System.Void UnityEngine.Transform::get_worldToLocalMatrix_Injected(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_get_worldToLocalMatrix_Injected();
		Register_UnityEngine_Transform_get_worldToLocalMatrix_Injected();

		//System.Void UnityEngine.Transform::set_localPosition_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_set_localPosition_Injected();
		Register_UnityEngine_Transform_set_localPosition_Injected();

		//System.Void UnityEngine.Transform::set_localRotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_set_localRotation_Injected();
		Register_UnityEngine_Transform_set_localRotation_Injected();

		//System.Void UnityEngine.Transform::set_localScale_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_set_localScale_Injected();
		Register_UnityEngine_Transform_set_localScale_Injected();

		//System.Void UnityEngine.Transform::set_position_Injected(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_set_position_Injected();
		Register_UnityEngine_Transform_set_position_Injected();

		//System.Void UnityEngine.Transform::set_rotation_Injected(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_set_rotation_Injected();
		Register_UnityEngine_Transform_set_rotation_Injected();

		//UnityEngine.Transform UnityEngine.Transform::FindRelativeTransformWithPath(UnityEngine.Transform,System.String,System.Boolean)
		void Register_UnityEngine_Transform_FindRelativeTransformWithPath();
		Register_UnityEngine_Transform_FindRelativeTransformWithPath();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

		//UnityEngine.Transform UnityEngine.Transform::GetParent()
		void Register_UnityEngine_Transform_GetParent();
		Register_UnityEngine_Transform_GetParent();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.U2D.SpriteAtlas

		//System.Boolean UnityEngine.U2D.SpriteAtlas::CanBindTo(UnityEngine.Sprite)
		void Register_UnityEngine_U2D_SpriteAtlas_CanBindTo();
		Register_UnityEngine_U2D_SpriteAtlas_CanBindTo();

	//End Registrations for type : UnityEngine.U2D.SpriteAtlas

	//Start Registrations for type : UnityEngine.U2D.SpriteAtlasManager

		//System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
		void Register_UnityEngine_U2D_SpriteAtlasManager_Register();
		Register_UnityEngine_U2D_SpriteAtlasManager_Register();

	//End Registrations for type : UnityEngine.U2D.SpriteAtlasManager

	//Start Registrations for type : UnityEngine.UISystemProfilerApi

		//System.Void UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)
		void Register_UnityEngine_UISystemProfilerApi_AddMarker();
		Register_UnityEngine_UISystemProfilerApi_AddMarker();

		//System.Void UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)
		void Register_UnityEngine_UISystemProfilerApi_BeginSample();
		Register_UnityEngine_UISystemProfilerApi_BeginSample();

		//System.Void UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)
		void Register_UnityEngine_UISystemProfilerApi_EndSample();
		Register_UnityEngine_UISystemProfilerApi_EndSample();

	//End Registrations for type : UnityEngine.UISystemProfilerApi

	//Start Registrations for type : UnityEngine.UnhandledExceptionHandler

		//System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
		void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();
		Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();

	//End Registrations for type : UnityEngine.UnhandledExceptionHandler

	//Start Registrations for type : UnityEngine.UnityLogWriter

		//System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLogImpl(System.String)
		void Register_UnityEngine_UnityLogWriter_WriteStringToUnityLogImpl();
		Register_UnityEngine_UnityLogWriter_WriteStringToUnityLogImpl();

	//End Registrations for type : UnityEngine.UnityLogWriter

	//Start Registrations for type : UnityEngine.XR.XRSettings

		//System.Boolean UnityEngine.XR.XRSettings::get_enabled()
		void Register_UnityEngine_XR_XRSettings_get_enabled();
		Register_UnityEngine_XR_XRSettings_get_enabled();

		//System.Boolean UnityEngine.XR.XRSettings::get_showDeviceView()
		void Register_UnityEngine_XR_XRSettings_get_showDeviceView();
		Register_UnityEngine_XR_XRSettings_get_showDeviceView();

		//System.Boolean UnityEngine.XR.XRSettings::get_useOcclusionMesh()
		void Register_UnityEngine_XR_XRSettings_get_useOcclusionMesh();
		Register_UnityEngine_XR_XRSettings_get_useOcclusionMesh();

		//System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight()
		void Register_UnityEngine_XR_XRSettings_get_eyeTextureHeight();
		Register_UnityEngine_XR_XRSettings_get_eyeTextureHeight();

		//System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth()
		void Register_UnityEngine_XR_XRSettings_get_eyeTextureWidth();
		Register_UnityEngine_XR_XRSettings_get_eyeTextureWidth();

		//System.Single UnityEngine.XR.XRSettings::get_eyeTextureResolutionScale()
		void Register_UnityEngine_XR_XRSettings_get_eyeTextureResolutionScale();
		Register_UnityEngine_XR_XRSettings_get_eyeTextureResolutionScale();

		//System.Single UnityEngine.XR.XRSettings::get_occlusionMaskScale()
		void Register_UnityEngine_XR_XRSettings_get_occlusionMaskScale();
		Register_UnityEngine_XR_XRSettings_get_occlusionMaskScale();

		//System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal()
		void Register_UnityEngine_XR_XRSettings_get_renderViewportScaleInternal();
		Register_UnityEngine_XR_XRSettings_get_renderViewportScaleInternal();

		//System.String[] UnityEngine.XR.XRSettings::get_supportedDevices()
		void Register_UnityEngine_XR_XRSettings_get_supportedDevices();
		Register_UnityEngine_XR_XRSettings_get_supportedDevices();

		//System.Void UnityEngine.XR.XRSettings::get_eyeTextureDesc_Injected(UnityEngine.RenderTextureDescriptor&)
		void Register_UnityEngine_XR_XRSettings_get_eyeTextureDesc_Injected();
		Register_UnityEngine_XR_XRSettings_get_eyeTextureDesc_Injected();

		//System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single)
		void Register_UnityEngine_XR_XRSettings_set_eyeTextureResolutionScale();
		Register_UnityEngine_XR_XRSettings_set_eyeTextureResolutionScale();

		//System.Void UnityEngine.XR.XRSettings::set_gameViewRenderMode(UnityEngine.XR.GameViewRenderMode)
		void Register_UnityEngine_XR_XRSettings_set_gameViewRenderMode();
		Register_UnityEngine_XR_XRSettings_set_gameViewRenderMode();

		//System.Void UnityEngine.XR.XRSettings::set_occlusionMaskScale(System.Single)
		void Register_UnityEngine_XR_XRSettings_set_occlusionMaskScale();
		Register_UnityEngine_XR_XRSettings_set_occlusionMaskScale();

		//System.Void UnityEngine.XR.XRSettings::set_renderViewportScaleInternal(System.Single)
		void Register_UnityEngine_XR_XRSettings_set_renderViewportScaleInternal();
		Register_UnityEngine_XR_XRSettings_set_renderViewportScaleInternal();

		//System.Void UnityEngine.XR.XRSettings::set_showDeviceView(System.Boolean)
		void Register_UnityEngine_XR_XRSettings_set_showDeviceView();
		Register_UnityEngine_XR_XRSettings_set_showDeviceView();

		//System.Void UnityEngine.XR.XRSettings::set_useOcclusionMesh(System.Boolean)
		void Register_UnityEngine_XR_XRSettings_set_useOcclusionMesh();
		Register_UnityEngine_XR_XRSettings_set_useOcclusionMesh();

		//UnityEngine.XR.GameViewRenderMode UnityEngine.XR.XRSettings::get_gameViewRenderMode()
		void Register_UnityEngine_XR_XRSettings_get_gameViewRenderMode();
		Register_UnityEngine_XR_XRSettings_get_gameViewRenderMode();

	//End Registrations for type : UnityEngine.XR.XRSettings

	//Start Registrations for type : UnityEngineInternal.Input.NativeInputSystem

		//System.Void UnityEngineInternal.Input.NativeInputSystem::set_hasDeviceDiscoveredCallback(System.Boolean)
		void Register_UnityEngineInternal_Input_NativeInputSystem_set_hasDeviceDiscoveredCallback();
		Register_UnityEngineInternal_Input_NativeInputSystem_set_hasDeviceDiscoveredCallback();

	//End Registrations for type : UnityEngineInternal.Input.NativeInputSystem

}
